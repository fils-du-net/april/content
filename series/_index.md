---
title: Revue de Presse hebdomadaire
author: Fils du Net
href: filsdu.net
cascade:
  featured_image: /cover.jpg
---

Cette revue de presse sur Internet fait partie du travail de veille mené par l'April dans le cadre de son action de défense et de promotion du logiciel libre. Les positions exposées dans les articles sont celles de leurs auteurs et ne rejoignent pas forcément celles de l'April.
