# Revue de presse de l'april

Objectifs : Faire une revue de presse au sujet des logiciels libres, principalement sur la presse en ligne. Poster cette revue de presse sur la liste liste-infos AT april POINT org, sur le site Web et sur les sites qui relayent la revue de presse comme LinuxFr : http://linuxfr.org/.

La revue de presse donne lieu à une émission de radio hebdomadaire de commentaire Podcast des enregistrements.

Le podcast est diffusé sur l'émission Divergence Numérique reprise sur Radio Escapades et Radio Larzac, Radio Fil de l'Eau et sur Ici et Maintenant.

Mise en garde : Cette revue de presse sur Internet fait partie du travail de veille mené par l'April dans le cadre de son action de défense et de promotion du logiciel libre. Les positions exposées dans les articles sont celles de leurs auteurs et ne rejoignent pas forcément celles de l'April.
