---
title: Revue de presse de l'April
description: Promouvoir et défendre le logiciel libre
---

La revue de presse de l'[April](https://www.april.org) est régulièrement éditée par les membres de l'association. Elle couvre l'actualité de la presse en ligne, liée au logiciel libre. Il s'agit donc d'une sélection d'articles de presse et non de prises de position de l'association de promotion et de défense du logiciel libre.
