La participation au groupe Revue de presse est libre.

Les propositions d'articles pourront se faire via un envoi à la liste rp AT april POINT org ou par IRC via un script (sur le canal #april).

Il est important de respecter un niveau de qualité élevé en ce qui concerne les articles proposés, d'où quelques règles :
* La règle principale pour les articles c'est que cela concerne les logiciels libres.
* Avant de proposer un article, vérifiez s'il n'est pas déjà posté sur la revue de presse.
* Ne poster qu'un article par sujet, si possible l'article le plus complet (mais c'est difficile parfois à déterminer).
* Ne pas poster d'article sur la sortie d'un nouveau logiciel, d'une nouvelle version ou d'une distribution.
* Respecter la forme de la revue de presse : le titre commence par le nom du site entre crochets suivi du titre original de l'article. Exemple : "[toto.com] Ceci est le titre".
* Dans la zone Extrait : copiez une partie de l'article, généralement le début (appelé chapeau). Ne pas mettre les textes complets des articles pour respecter le droit d'auteur. Mettre éventuellement une citation pertinente.
* Si l'article est dans une langue étrangère, proposez une traduction de l'extrait.