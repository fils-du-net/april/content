---
site: RTL.fr
title: "Snowden: contrairement à Belloubet, l'Élysée n'envisage pas de l'accueillir en France"
author: Benjamin Sportouch
date: 2019-09-16
href: https://www.rtl.fr/actu/politique/snowden-contrairement-a-belloubet-l-elysee-n-envisage-pas-de-l-accueillir-en-france-7798344087
featured_image: https://cdn-media.rtl.fr/cache/Lta8RFoOofIjB7YGhlDZtQ/880v587-0/online/image/2016/0802/7784308369_le-consultant-de-la-nsa-edward-snowden-le-6-juin-2013.jpg
tags:
- Vie privée
series:
- 201938
---

> INFO RTL - Nicole Belloubet serait prête à ce que la France accueille Edward Snowden, mais l'Élysée n'est pas sur la même ligne." 
