---
site: Next INpact
title: "Pourquoi Google ne paiera pas les droits voisins des éditeurs et agences de presse"
author: Marc Rees
date: 2019-09-25
href: https://www.nextinpact.com/news/108234-pourquoi-google-ne-paiera-pas-droits-voisins-editeurs-et-agences-presse.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/20937.jpg
tags:
- Droit d'auteur
series:
- 201939
---

> Google va changer la présentation des articles de presse placardés sur sa page News (ou « actualités »). Finis les extraits! Mais les éditeurs et agences pourront les réactiver s’ils le souhaitent. Avec cette réforme, Google va éviter de payer les droits voisins issus de la directive Droit d’auteur. Explications.
