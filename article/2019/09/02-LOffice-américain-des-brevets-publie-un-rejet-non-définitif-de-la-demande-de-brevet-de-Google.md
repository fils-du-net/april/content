---
site: Developpez.com
title: "L'Office américain des brevets publie un rejet non définitif de la demande de brevet de Google"
date: 2019-08-02
author: Stéphane le calme
href: https://algo.developpez.com/actu/275669/L-Office-americain-des-brevets-publie-un-rejet-non-definitif-de-la-demande-de-brevet-de-Google-concernant-sa-technique-de-compression-de-video-basee-sur-l-ANS
featured_image: https://www.developpez.net/forums/attachments/p500940d1/a/a/a
tags:
- Brevets logiciels
series:
- 201936
series_weight: 0
---

> Très largement utilisée, la compression de données est une opération de codage nécessaire pour réduire la taille de transmission ou de stockage des données. Cette opération informatique consiste à transformer une suite de bits donnée en une suite de bits plus courte pouvant restituer les mêmes informations, ou des informations voisines, en utilisant un algorithme de décompression. Il existe donc deux grandes familles d'algorithmes de compression: les algorithmes de compression sans perte qui restituent après décompression une suite de bits strictement identique à l'originale. Et les algorithmes de compression avec perte qui restituent une suite de bits qui est plus ou moins voisine de l'originale selon la qualité désirée. Les premiers sont donc utilisés pour les archives, les fichiers exécutables ou les textes, alors que les derniers sont utiles pour les images, le son et la vidéo.
