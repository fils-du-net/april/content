---
site: ZDNet France
title: "Framasoft va fermer une partie de ses services de 'Dégooglisons Internet'"
author: Thierry Noisette
date: 2019-09-29
href: https://www.zdnet.fr/blogs/l-esprit-libre/framasoft-va-fermer-une-partie-de-ses-services-de-degooglisons-internet-39891319.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2019/09/Framasoft-services-degooglisons-internet.jpg
tags:
- Associations
- Internet
series:
- 201939
series_weight: 0
---

> 'Amap du numérique' et pas ersatz de géant du Web, Framasoft annonce la fermeture à venir d'une partie de ses services alternatifs aux GAFAM. Et escompte la reprise de plusieurs, ses logiciels étant libres.
