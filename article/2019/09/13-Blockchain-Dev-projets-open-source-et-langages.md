---
site: L'Informaticien
title: "Blockchain & Dev: projets open source et langages"
date: 2019-09-13
author: Thierry Thaureaux
href: https://www.linformaticien.com/actualites/id/52830/blockchain-dev-projets-open-source-et-langages.aspx
featured_image: https://www.linformaticien.com/Portals/0/2019/09_septembre/inf179_blockchaindev_01.png
tags:
- Économie
series:
- 201937
---

> Après l’engouement rencontré par les projets de développements de Blockchain en 2017, notamment sur Ethereum, où en est la technologie aujourd’hui? Quels projets ont débouché sur des applications réelles et avec quels langages? C’est ce que nous allons voir dans cet article.
