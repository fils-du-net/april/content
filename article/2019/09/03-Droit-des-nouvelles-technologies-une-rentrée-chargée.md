---
site: Next INpact
title: "Droit des nouvelles technologies: une rentrée chargée (€)"
author: Marc Rees
date: 2019-09-03
href: https://www.nextinpact.com/news/108141-droit-nouvelles-technologies-rentree-chargee.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/23678.jpg
tags:
- Institutions
series:
- 201936
---

> La rentrée s’annonce une nouvelle fois dense sur le plan législatif et judiciaire, avec de nombreux débats à venir autour de la régulation des contenus sur Internet. Panorama des évènements à venir, en complément de notre récapitulatif des actualités de cet été.
