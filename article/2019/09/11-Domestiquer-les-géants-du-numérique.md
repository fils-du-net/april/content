---
site: Le Monde.fr
title: "Domestiquer les géants du numérique"
date: 2019-09-11
href: https://www.lemonde.fr/idees/article/2019/09/11/domestiquer-les-geants-du-numerique_5509090_3232.html
featured_image: https://img.lemde.fr/2019/09/09/0/0/5162/3438/688/0/60/0/ff4b92b_420b3ca6b97b4b68919b023a37a61eec-420b3ca6b97b4b68919b023a37a61eec-0.jpg
tags:
- Entreprise
- Institutions
- Vie privée
series:
- 201937
series_weight: 0
---

> L'ouverture, le 9 septembre, aux Etats-Unis, d'une enquête antitrust contre Google montre que la justice américaine a enfin décidé, sous la pression de l'opinion publique, de questionner les monopoles des GAFA.
