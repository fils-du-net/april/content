---
site: Le Monde.fr
title: "La France exclut toujours d'accorder l'asile à Edward Snowden"
date: 2019-09-19
href: https://www.lemonde.fr/pixels/article/2019/09/19/la-france-exclut-toujours-d-accorder-l-asile-a-edward-snowden_5512319_4408996.html
featured_image: https://img.lemde.fr/2019/09/18/0/0/5568/3712/688/0/60/0/facf59c_5631875-01-06.jpg
tags:
- Vie privée
series:
- 201938
---

> Paris, qui n’avait pas accédé à la demande d’asile politique du lanceur d’alerte américain Edward Snowden en 2013, n’a pas «changé d’avis», selon le ministre des affaires étrangères, Jean-Yves Le Drian.
