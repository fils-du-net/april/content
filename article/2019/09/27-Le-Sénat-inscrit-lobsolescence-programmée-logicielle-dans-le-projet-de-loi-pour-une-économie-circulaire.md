---
site: Developpez.com
title: "Le Sénat inscrit l'obsolescence programmée logicielle dans le projet de loi pour une économie circulaire"
author: Michael Guilloux
date: 2019-09-27
href: https://droit.developpez.com/actu/278780/Le-Senat-inscrit-l-obsolescence-programmee-logicielle-dans-le-projet-de-loi-pour-une-economie-circulaire-en-rejetant-toutefois-les-amendements-les-plus-concrets
featured_image: https://www.developpez.com/public/images/news/obsolescence.jpg
seeAlso: "[Le Sénat inscrit l'obsolescence logicielle dans le projet de loi pour une économie circulaire](https://www.april.org/le-senat-inscrit-l-obsolescence-logicielle-dans-le-projet-de-loi-pour-une-economie-circulaire)"
tags:
- Institutions
series:
- 201939
series_weight: 0
---

> En juillet dernier, le gouvernement français a dévoilé son projet de loi «relatif à la lutte contre le gaspillage et à l'économie circulaire». Présenté par M. François de Rugy, ministre de la transition écologique et solidaire, et Mme Brune Poirson, secrétaire d'État auprès du ministre de la transition écologique et solidaire, il cible notamment les constructeurs de matériels informatiques et produits high-tech. Le gouvernement veut les contraindre à payer plus pour la gestion des déchets élec...
