---
site: WebTimesMedia
title: "Tristan Nitot, un fervent du logiciel libre, à la tête de Qwant"
author: Jean-Pierre Largillet
date: 2019-09-20
href: http://www.webtimemedias.com/article/tristan-nitot-un-fervent-du-logiciel-libre-la-tete-de-qwant-20190920-64700
featured_image: http://www.webtimemedias.com/sites/files/gallery/wtm/photos/Nitot_Tristan_Qwant_500x250.jpg
tags:
- Entreprise
series:
- 201938
---

> Le moteur de recherche européen, dont les équipes de R&D sont basées à Nice, a annoncé hier la nomination de Tristan Nitot, ancien président de Mozilla Europe (Firefox) en tant que Directeur Général. S'il est placé sous la présidence d'Eric Léandri, il est chargé de la communication et de l’ensemble de la direction opérationnelle.
