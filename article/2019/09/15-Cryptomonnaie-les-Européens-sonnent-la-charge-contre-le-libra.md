---
site: Les Echos
title: "Cryptomonnaie: les Européens sonnent la charge contre le libra"
author: Raphaël Bloch, Nessim Aït-Kacimi, Anaïs Moutot
date: 2019-09-15
href: https://www.lesechos.fr/finance-marches/marches-financiers/cryptomonnaie-les-europeens-sonnent-la-charge-contre-le-libra-1131738
featured_image: https://media.lesechos.com/api/v1/images/view/5d7e11118fe56f66f13c5345/1280x720/0601861716465-web-tete.jpg
tags:
- Économie
series:
- 201937
---

> Pour la première fois, ce lundi, à Bâle, des représentants du libra, la future cryptomonnaie de Facebook rencontreront des responsables de 26 banques centrales. Vendredi, déjà, Paris et Berlin avaient affirmé qu'ils s'opposeraient au lancement en Europe du libra. Ils prônent la création d'une «devise numérique» à l'échelle du continent.
