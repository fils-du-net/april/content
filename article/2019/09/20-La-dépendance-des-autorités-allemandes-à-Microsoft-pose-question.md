---
site: ZDNet France
title: "La dépendance des autorités allemandes à Microsoft pose question"
author: Cathrin Schaer
date: 2019-09-20
href: https://www.zdnet.fr/actualites/la-dependance-des-autorites-allemandes-a-microsoft-pose-question-39890935.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/02/Microsoft%20Illustration%20A%20620.jpg
tags:
- Administration
- Logiciels privateurs
series:
- 201938
series_weight: 0
---

> Les autorités allemandes sont trop dépendantes aux logiciels et au matériel de Microsoft, a fait savoir une étude. Un problème auquel Berlin n'a pas encore trouvé de parade.
