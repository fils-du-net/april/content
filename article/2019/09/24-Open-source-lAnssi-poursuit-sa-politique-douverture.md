---
site: LeMagIT
title: "Open source: l'Anssi poursuit sa politique d'ouverture"
author: Valéry Marchive
date: 2019-09-24
href: https://www.lemagit.fr/actualites/252471195/Open-source-lAnssi-poursuit-sa-politique-douverture
featured_image: https://cdn.ttgtmedia.com/visuals/LeMagIT/hero_article/IMG_1450.jpg
tags:
- Administration
series:
- 201939
series_weight: 0
---

> L’agence vient de rendre disponible à tous l’un des outils que ses équipes ont développés en interne. La continuité d’une stratégie dont les bénéfices s’étendent au-delà de son image.
