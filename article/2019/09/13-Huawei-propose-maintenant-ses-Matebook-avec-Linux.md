---
site: tom's guide
title: "Huawei propose maintenant ses Matebook avec Linux"
date: 2019-09-13
author: Guillaume
href: https://www.tomsguide.fr/huawei-propose-maintenant-ses-matebook-avec-linux
tags:
- International
- Entreprise
series:
- 201937
series_weight: 0
---

> Dans la guerre que se livrent les États-Unis et la Chine, l’équipementier réseau Huawei est en ligne de mire. Aujourd’hui, le constructeur chinois se distancie plus encore en proposant Linux sur ses ordinateurs portables.
