---
site: ZDNet France
title: "Microsoft organise la première conférence pour le sous-système Linux de Windows"
author: Steven J. Vaughan-Nichols
date: 2019-09-10
href: https://www.zdnet.fr/actualites/microsoft-organise-la-premiere-conference-pour-le-sous-systeme-linux-de-windows-39890263.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2018/11/windowsandlinux.jpg
tags:
- Entreprise
series:
- 201937
series_weight: 0
---

> Microsoft organisera une conférence  sur le sous-système Linux pour Windows et les technologies qui lui sont associées en mars 2020.
