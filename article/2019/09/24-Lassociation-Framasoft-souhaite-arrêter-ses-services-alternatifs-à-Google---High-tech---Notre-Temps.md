---
site: Notre temps
title: "L'association Framasoft souhaite arrêter ses services alternatifs à Google"
date: 2019-09-24
href: https://www.notretemps.com/high-tech/actualites/l-association-framasoft-souhaite-afp-201909,i203358
tags:
- Associations
series:
- 201939
series_weight: 0
---

> L'association pour la promotion du logiciel libre Framasoft a annoncé mardi vouloir arrêter progressivement certains services numériques qu'elle offrait depuis plusieurs années comme une alternative aux outils de Google, mais encourage leur "décentralisation".
