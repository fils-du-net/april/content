---
site: Next INpact
title: "«Déframasoftisons Internet»: Framasoft fermera progressivement 30 services"
author: Vincent Hermann
date: 2019-09-24
href: https://www.nextinpact.com/news/108225-deframatisons-internet-framasoft-fermera-progressivement-30-services.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/23819.jpg
tags:
- Associations
series:
- 201939
---

> Au risque de choquer une partie des utilisateurs, Framasoft vient d’annoncer la fin programmée de bon nombre de ses services. La nouvelle ne devrait surprendre personne: l’association veut seulement montrer des alternatives existantes. Entretien avec Pierre-Yves Gosset, son secrétaire général.
