---
site: FrAndroid
title: "Des PC sous Linux: Huawei prépare déjà l'alternative à Windows"
author: Omar Belkaab
date: 2019-09-13
href: https://www.frandroid.com/marques/huawei/623638_des-pc-sous-linux-huawei-prepare-deja-lalternative-a-windows
featured_image: https://images.frandroid.com/wp-content/uploads/2018/10/c_huawei-matebook-x-frandroid-dsc04469.jpg
tags:
- International
series:
- 201937
---

> Huawei a commencé à vendre des ordinateurs Matebook tournants sous Linux au lieu de Windows en Chine. La firme semble se préparer à une éventuelle privation de l’OS de Microsoft.
