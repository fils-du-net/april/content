---
site: RFI
title: "Les biens communs: vers un nouvel univers économique"
date: 2019-09-28
href: http://www.rfi.fr/emission/20190928-biens-communs-univers-economique-forets-air-climat-agriculture-demographie
featured_image: http://scd.rfi.fr/sites/filesrfi/aefimagesnew/dynimagecache/0/73/1920/1056/187/103/sites/images.rfi.fr/files/aefimagesnew/aef_image/2019-09-28t075325z_315183209_rc13c5ecd900_rtrmadp_3_afghanistan-election_0.jpg
tags:
- Économie
series:
- 201939
---

> À qui appartient l'air que nous respirons ou l'eau que nous consommons? Et ces océans sur lesquels nous naviguons? Qui sont les propriétaires des ressources du sous-sol? À qui appartiennent les grands massifs forestiers? À l'heure où ces denrées deviennent rares face à la croissance démographique, où le réchauffement climatique menace l’agriculture, ces questions peuvent donner lieu à des développements historiques, philosophiques, sociologiques ou économiques. Une expression est venue résumer tout cela. On parle dorénavant et peut-être en avez-vous entendu parler des «biens communs». Mais, cela concerne aussi les logiciels libres ou l'indépendance des rédactions.
