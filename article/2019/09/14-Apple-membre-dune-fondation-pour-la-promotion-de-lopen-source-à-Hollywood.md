---
site: MacGeneration
title: "Apple membre d'une fondation pour la promotion de l'open source à Hollywood"
date: 2019-09-14
href: https://www.macg.co/aapl/2019/09/apple-membre-dune-fondation-pour-la-promotion-de-lopen-source-hollywood-108249
featured_image: https://cdn.mgig.fr/2019/09/mg-05b86bd5-da6c-4af0-b0d2-w1000h563-sc.jpg
tags:
- Promotion
- Entreprise
series:
- 201937
series_weight: 0
---

> Apple veut promouvoir l'open source au sein de l'industrie du cinéma et de la télévision. Le constructeur a acheté son rond de serviette auprès de l'Academy Software Foundation, une fondation créée l'an dernier par l'Academy of Motion Picture Arts and Sciences (l'organisation des Oscars) et la Linux Foundation.
