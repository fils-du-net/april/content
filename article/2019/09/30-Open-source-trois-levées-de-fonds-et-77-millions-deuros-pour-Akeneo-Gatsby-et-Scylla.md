---
site: ZDNet France
title: "Open source: trois levées de fonds et 77 millions d'euros pour Akeneo, Gatsby et Scylla"
author: Thierry Noisette
date: 2019-09-30
href: https://www.zdnet.fr/blogs/l-esprit-libre/open-source-trois-levees-de-fonds-et-77-millions-d-euros-pour-akeneo-gatsby-et-scylla-39891397.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2019/09/finance-calculette.jpg
tags:
- Entreprise
- Économie
series:
- 201940
series_weight: 0
---

> Ces trois start-up de l'open source, dont le français Akeneo qui à lui seul a obtenu 41 millions d'euros, ont annoncé des levées de fonds en septembre.
