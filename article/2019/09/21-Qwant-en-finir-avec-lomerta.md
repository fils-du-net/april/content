---
site: Next INpact
title: "Qwant: en finir avec l'omerta (€)"
author: Jean-Marc Manach
date: 2019-09-21
href: https://www.nextinpact.com/news/108195-qwant-en-finir-avec-omerta.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/23755.jpg
tags:
- Entreprise
- Vie privée
series:
- 201938
series_weight: 0
---

> Le président de Qwant au eu des mots très durs suite à notre enquête sur les problèmes rencontrés par son moteur de recherche. Nous avons longtemps hésité à rendre publiques les « pressions » dont nous avons fait l'objet. Les témoignages concordants de plusieurs de ses ex-salariés nous ont convaincu qu'il nous fallait le faire.
