---
site: ZDNet France
title: "Richard Stallman chez Microsoft (non, on ne rêve pas)"
author: Thierry Noisette
date: 2019-09-05
href: https://www.zdnet.fr/blogs/l-esprit-libre/richard-stallman-chez-microsoft-non-on-ne-reve-pas-39890151.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2019/09/stallmanatmicrosoft_vue-partielle.jpg
tags:
- Entreprise
- Sensibilisation
- Informatique en nuage
series:
- 201936
series_weight: 0
---

> Le fondateur du mouvement des logiciels libres a été invité chez Microsoft, où il a donné une conférence.
