---
site: cryptonews
title: "Personnalité Crypto: Jonathan Hamel"
date: 2019-09-11
author: David Nathan
href: https://fr.cryptonews.com/exclusives/personnalite-crypto-jonathan-hamel-4309.htm
featured_image: https://cimg.co/w/articles-cover/0/5d7/7d2259120c.jpg
tags:
- Économie
series:
- 201937
---

> Fondateur de l’Académie Bitcoin, chercheur associé à l’Institut Économique de Montréal, Jonathan Hamel est un pionnier de la communauté Bitcoin au Canada qui contribue depuis 2013 à la sensibilisation autour de la technologie Bitcoin.
