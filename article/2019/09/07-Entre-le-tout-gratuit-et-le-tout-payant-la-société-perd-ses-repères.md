---
site: La Tribune
title: "Entre le «tout gratuit» et le «tout payant» la société perd ses repères"
author: Eric Vernier et L'Hocine Houanti
date: 2019-09-07
href: https://www.latribune.fr/opinions/tribunes/entre-le-tout-gratuit-et-le-tout-payant-la-societe-perd-ses-reperes-827362.html
featured_image: https://static.latribune.fr/full_width/1014247/bus-dunkerque.jpg
tags:
- Sensibilisation
- Économie
series:
- 201936
series_weight: 0
---

> Suppression totale des tickets de transport en commun, comme pour les Parisiens seniors, les personnes handicapées et les enfants , logiciels offerts tels que LibreOffice, OpenOffice, VLC Media Player, Blender ou encore Clementine, petits déjeuners et goûters donnés à l’école, cartes bancaires gratuites … De plus en plus d’initiatives publiques et privées tendent vers une généralisation de la gratuité.
