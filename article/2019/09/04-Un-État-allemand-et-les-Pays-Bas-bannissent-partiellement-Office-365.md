---
site: Next INpact
title: "Un État allemand et les Pays-Bas bannissent partiellement Office 365"
date: 2019-09-04
href: https://www.nextinpact.com/brief/un-etat-allemand-et-les-pays-bas-bannissent-partiellement-office-365-9387.htm
featured_image: https://cdn2.nextinpact.com/images/bd/wide-linked-media/21815.jpg
tags:
- Vie privée
- International
series:
- 201936
---

> En Allemagne, le débat de savoir si Office 365 est adapté aux usages fédéraux n'est pas neuf. À la lumière du RGPD, il a pris une tournure nouvelle. En juillet, il est encore monté en intensité après la décision, par l'État de Hesse, de bannir Office 365 des écoles publiques.
