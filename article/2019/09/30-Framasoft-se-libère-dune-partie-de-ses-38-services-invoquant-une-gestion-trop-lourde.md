---
site: Clubic.com
title: "Framasoft se libère d'une partie de ses 38 services, invoquant une gestion trop lourde"
author: Mathieu Grumiaux
date: 2019-09-30
href: https://www.clubic.com/pro/legislation-loi-internet/propriete-intellectuelle/logiciel-libre-open-source/actualite-871297-framasoft-libere-partie-38-services-invoquant-gestion-lourde.html
featured_image: https://pic.clubic.com/v1/images/1746797/raw
tags:
- Associations
- Internet
series:
- 201940
---

> L'association, composée de neuf salariés, ne parvient plus à maintenir le vaste écosystème de services alternatifs mis en place depuis 2014. Les projets abandonnés pourront être poursuivis par des développeurs volontaires.
