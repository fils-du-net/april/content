---
site: Le Monde.fr
title: "Richard Stallman, précurseur du logiciel libre, démissionne du MIT et de la Free Software Foundation"
date: 2019-09-17
href: https://www.lemonde.fr/pixels/article/2019/09/17/richard-stallman-precurseur-du-logiciel-libre-demissionne-du-mit-et-de-la-free-software-foundation_5511466_4408996.html
featured_image: https://img.lemde.fr/2019/09/17/0/0/2956/1971/688/0/60/0/2c6d8f3_9HdbzJ-Qsz6buzCpb9V4v3Uw.jpg
seeAlso: "[Richard Stallman démissionne de la direction de la Fondation pour le Logiciel Libre](https://www.april.org/richard-stallman-demissionne-de-la-direction-de-la-fondation-pour-le-logiciel-libre)"
tags:
- Promotion
series:
- 201938
series_weight: 0
---

> M. Stallman a démissionné à la suite de la publication d’échanges liés aux conséquences de l’affaire Epstein dans la célèbre université, en partie déformés par la presse américaine.
