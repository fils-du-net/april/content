---
site: Next INpact
title: "L'Arcom, fusion de la Hadopi et du CSA"
author: Marc Rees
date: 2019-09-25
href: https://www.nextinpact.com/news/108235-larcom-fusion-hadopi-et-csa.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/17375.jpg
tags:
- HADOPI
- Internet
series:
- 201939
series_weight: 0
---

> Dans les colonnes du Figaro, Franck Riester a dévoilé le nom de la nouvelle autorité fusionnant les compétences de la Hadopi et du CSA: l’Arcom. Nous avons pu trouver un nom de domaine éponyme, déposé par le Conseil supérieur de l’audiovisuel.
