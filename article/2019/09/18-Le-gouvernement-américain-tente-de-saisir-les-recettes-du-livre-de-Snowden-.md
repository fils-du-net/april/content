---
site: Slate.fr
title: "Le gouvernement américain tente de saisir les recettes du livre de Snowden"
description: "Le département de la Justice intente un procès au lanceur d'alerte"
author: Claire Levenson
date: 2019-09-18
href: https://www.slate.fr/story/181821/etats-unis-gouvernement-departement-justice-action-justice-edward-snowden-memoires-permanent-record-saisie-recettes
featured_image: https://www.slate.fr/sites/default/files/styles/1060x523/public/edward_snowden_memoires.jpg
tags:
- Vie privée
- International
series:
- 201938
series_weight: 0
---

> Selon le département de la Justice américain, les bénéfices des mémoires d'Edward Snowden, qui viennent de sortir aux États-Unis sous le titre Permanent Record, devront tous être reversés au gouvernement.
