---
site: Numerama
title: "Franck Riester confirme la fusion entre la CSA et la Hadopi"
author: Julien Lausson
date: 2019-09-03
href: https://www.numerama.com/politique/545356-franck-riester-confirme-la-fusion-entre-la-csa-et-la-hadopi.html
featured_image: https://c1.lestechnophiles.com/www.numerama.com/content/uploads/2019/09/franck-riester.jpg
tags:
- Institutions
- HADOPI
series:
- 201936
series_weight: 0
---

> Dans les cartons depuis 2010, la fusion entre le CSA et la Hadopi se fera. C'est en tout cas ce qu'annonce Franck Riester, le ministre de la Culture et de la Communication.
