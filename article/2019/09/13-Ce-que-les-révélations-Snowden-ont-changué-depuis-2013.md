---
site: Le Monde.fr
title: "Ce que les «révélations Snowden» ont changé depuis 2013"
author: Martin Untersinger
date: 2019-09-13
href: https://www.lemonde.fr/pixels/article/2019/09/13/ce-que-les-revelations-snowden-ont-change-depuis-2013_5509864_4408996.html
featured_image: https://img.lemde.fr/2016/12/05/0/0/3500/2337/688/0/60/0/e04cdd6_13242-11j4k6s.bvk2kmlsor.png
tags:
- Vie privée
series:
- 201937
series_weight: 0
---

> Relations internationales, entreprises du numérique, vie en ligne... L’onde de choc provoquée par la plus importante fuite de documents de l’histoire des services de renseignements américains se mesure encore aujourd’hui.
