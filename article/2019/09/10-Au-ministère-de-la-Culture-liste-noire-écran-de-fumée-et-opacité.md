---
site: Next INpact
title: "Au ministère de la Culture, liste noire, écran de fumée et opacité (€)"
author: Marc Rees
date: 2019-09-10
href: https://www.nextinpact.com/news/108184-au-ministere-culture-liste-noire-ecran-fumee-et-opacite.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/5762.jpg
tags:
- Droit d'auteur
series:
- 201937
---

> De nouveaux documents témoignent de l’opacité dans laquelle baignent les accords «follow the money». Signés au ministère de la Culture, ils veulent appauvrir les sites considérés comme «pirates». La Rue de Valois affirme néanmoins qu’elle n’a connaissance d’aucune liste noire. Listes qui existent pourtant. Explications.
