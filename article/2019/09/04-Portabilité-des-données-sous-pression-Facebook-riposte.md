---
site: Le Monde.fr
title: "«Portabilité des données»: sous pression, Facebook riposte"
author: Alexandre Piquard
date: 2019-09-04
href: https://www.lemonde.fr/economie/article/2019/09/04/portabilite-des-donnees-sous-pression-facebook-riposte_5506480_3234.html
featured_image: https://img.lemde.fr/2019/09/03/0/0/3154/2253/688/0/60/0/5cb99a5_ef4547d0af7a4ec1b37398e4b0ab2e41-ef4547d0af7a4ec1b37398e4b0ab2e41-0.jpg
tags:
- Interopérabilité
- Entreprise
series:
- 201936
---

> Sommée de rendre possible de quitter le réseau social en emmenant ses contacts et ses contenus, l'entreprise publie un Livre blanc qui soulève des objections.
