---
site: Ligue des droits de l'Homme
title: "La LDH soutient le film «LoL - Logiciel libre, une affaire sérieuse» de François Zaïdi"
date: 2019-09-17
href: https://www.ldh-france.org/la-ldh-soutient-le-film-lol-logiciel-libre-une-affaire-serieuse-de-francois-zaidi/
featured_image: https://www.ldh-france.org/wp-content/uploads/2019/09/Affiche-LoL_LDH-717x1024.jpg
tags:
- Promotion
series:
- 201938
series_weight: 0
---

> Il n’est pas facile d’appréhender les concepts qui fondent les logiciels libres. En effet nous sommes habitués, sans même le réaliser, aux logiciels dits propriétaires, ceux qui sont fournis avec nos appareils (ordinateurs, tablettes, ordiphones…) ou que nous achetons. Ce documentaire a donc pour objectif de vulgariser le concept, la philosophie et les enjeux du logiciel libre, au travers d’interviews de différents spécialistes ou adeptes du «Libre» (les «libristes») entrecoupées de courts extraits de films pour souligner ou illustrer leurs propos.
