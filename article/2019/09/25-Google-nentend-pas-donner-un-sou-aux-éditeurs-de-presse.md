---
site: L'OBS
title: "Google n’entend pas donner un sou aux éditeurs de presse"
author: Thierry Noisette
date: 2019-09-25
href: https://www.nouvelobs.com/economie/20190925.OBS18914/google-n-entend-pas-donner-un-sou-aux-editeurs-de-presse.html
featured_image: https://focus.nouvelobs.com/2019/09/25/215/0/912/455/633/306/75/0/84d4c77_0EkAQPYRULEwWGwxTif4LGLo.jpg
tags:
- Droit d'auteur
series:
- 201939
series_weight: 0
---

> Google annonce que son application de la loi sur le droit d’auteur, qui a instauré un «droit voisin» pour les éditeurs de presse, leur laissera le choix entre gratuité et affichage réduit – également gratuit…
