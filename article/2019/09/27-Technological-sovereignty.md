---
site: Joinup
title: "France’s Gendarmerie: “Freedom of choice is priceless”"
author: Gijs Hillenius
date: 2019-09-27
href: https://joinup.ec.europa.eu/collection/open-source-observatory-osor/news/technological-sovereignty
featured_image: https://joinup.ec.europa.eu/sites/default/files/styles/wysiwyg_full_width/public/inline-images/click2.png
seeAlso: "[Le podcast de l'émission «Libre à vous!» avec la gendarmerie nationale](https://www.april.org/libre-a-vous-diffusee-mardi-3-septembre-2019-sur-radio-cause-commune-gendarmerie-nationale-la-pituit)"
tags:
- april
- Administration
- English
series:
- 201939
series_weight: 0
---

> Politicians should value highly the technological independence provided by using free and open source software, recommends Lieutenant colonel Stéphane Dumond, head of IT at the Gendarmerie in France. Using such software puts public services in control of their technology decisions and reduces strictly pecuniary constraints, he says.
