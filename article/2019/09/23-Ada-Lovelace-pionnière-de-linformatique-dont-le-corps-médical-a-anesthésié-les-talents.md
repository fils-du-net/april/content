---
site: France Inter
title: "Ada Lovelace, pionnière de l'informatique dont le corps médical a anesthésié les talents"
author: Baptiste Beaulieu
date: 2019-09-23
href: https://www.franceinter.fr/emissions/alors-voila/alors-voila-23-septembre-2019
featured_image: https://cdn.radiofrance.fr/s3/cruiser-production/2019/09/87d0043c-3a85-4c80-9b55-37ef32c2318e/640_gettyimages-923548586.webp
tags:
- Partage du savoir
series:
- 201939
series_weight: 0
---

> La romancière Catherine Dufour publie un ouvrage intitulé 'Ada ou la beauté des Nombres' dans lequel elle dresse le portrait de cette femme (Mathématicienne, programmeuse, poétesse, inventrice, traductrice, écrivaine, ingénieure...) oubliée par l'Histoire et qui fut pourtant LA pionnière de la science informatique.
