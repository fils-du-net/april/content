---
site: Silicon
title: "Open source: GitHub rassure la communauté face aux restrictions commerciales américaines"
author: Clément Bohic
date: 2019-09-16
href: https://www.silicon.fr/github-restrictions-commerciales-americaines-260953.html
featured_image: https://www.silicon.fr/wp-content/uploads/2019/09/github-restrictions-us.jpg
tags:
- International
series:
- 201938
series_weight: 0
---

> Les restrictions commerciales américaines, coup dur pour l'open source? GitHub fait le point sur cette problématique à laquelle il est confronté.
