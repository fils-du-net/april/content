---
site: Numerama
title: "Bruno Le Maire refuse d'autoriser «le développement de Libra sur le sol européen»"
author: Julien Lausson
date: 2019-09-12
href: https://www.numerama.com/politique/547734-bruno-le-maire-refuse-dautoriser-le-developpement-de-libra-sur-le-sol-europeen.html
featured_image: https://c1.lestechnophiles.com/www.numerama.com/content/uploads/2019/09/bruno-le-maire.jpg?fit=1024,1024
tags:
- Économie
- Institutions
series:
- 201937
series_weight: 0
---

> C'est en 2020 que Facebook souhaite lancer son porte-monnaie numérique Calibra, qui repose sur le projet Libra. Mais la France et l'Europe pourraient leur fermer la porte.
