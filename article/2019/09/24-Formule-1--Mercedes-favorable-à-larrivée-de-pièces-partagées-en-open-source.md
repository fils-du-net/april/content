---
site: Nextgen-Auto.com
title: "Mercedes favorable à l'arrivée de pièces partagées en 'open source'"
description: "Mais un débat doit d’abord avoir lieu entre les équipes"
author: Olivier Ferret 
date: 2019-09-24
href: https://motorsport.nextgen-auto.com/fr/formule-1/mercedes-favorable-a-l-arrivee-de-pieces-partagees-en-open-source,142052.html
featured_image: https://motorsport.nextgen-auto.com/IMG/arton142052.jpg
tags:
- Matériel libre
series:
- 201939
series_weight: 0
---

> Devant le refus de plusieurs équipes de standardiser plusieurs pièces pour cette saison qui s’annonce révolutionnaire pour le design des monoplaces, un autre concept a été dégainé: celui du design partagé des pièces, dit design en open source.
