---
site: Next INpact
title: "La France affûte sa transposition de l'article 17 de la directive Droit d'auteur (€)"
author: Marc Rees
date: 2019-09-11
href: https://www.nextinpact.com/news/108190-la-france-affute-sa-transposition-larticle-17-directive-droit-dauteur.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/5617.jpg
tags:
- Droit d'auteur
- Europe
- HADOPI
series:
- 201937
series_weight: 0
---

> La directive sur le droit d’auteur sera transposée en France par la future loi sur l’audiovisuel. En préparation, Paris aiguise l’adaptation de son précieux article 17 sur la reconnaissance et le filtrage des contenus.
