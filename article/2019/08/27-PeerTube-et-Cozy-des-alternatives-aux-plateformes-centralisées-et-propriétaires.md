---
site: La revue européenne des médias et du numérique
title: "PeerTube et Cozy, des alternatives aux plateformes centralisées et propriétaires"
author: Jacques-André Fines Schlumberger
date: 2019-08-27
href: https://la-rem.eu/2019/08/peertube-et-cozy-des-alternatives-aux-plateformes-centralisees-et-proprietaires
tags:
- april
- Sensibilisation
- Associations
series:
- 201935
series_weight: 0
---

> À chaque logiciel propriétaire ou service en ligne centralisé correspond un logiciel libre ou open source équivalent, et parfois décentralisé lorsqu’il s’agit de services en ligne: YouTube vs PeerTube, Twitter vs Mastodon, Facebook vs Diaspora*, Chrome vs Firefox, Windows ou Mac vs GNU/Linux, Microsoft Word vs LibreOffice, Google Docs vs Etherpad, Google Maps vs OpenStreetMap ou encore Dropbox vs Cozy: la liste s’allonge continuellement.
