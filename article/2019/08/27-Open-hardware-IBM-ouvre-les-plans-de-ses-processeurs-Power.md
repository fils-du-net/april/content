---
site: ZDNet France
title: "Open hardware: IBM ouvre les plans de ses processeurs Power"
author: Louis Adam
date: 2019-08-27
href: https://www.zdnet.fr/actualites/open-hardware-ibm-ouvre-les-plans-de-ses-processeurs-power-39889601.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2018/08/Processeur.jpg
tags:
- Matériel libre
series:
- 201935
series_weight: 0
---

> IBM annonce l'ouverture de son jeu d'instruction pour sa gamme de processeurs Power. Cela signifie que des sociétés tierces pourront développer leurs propres implémentations de ces processeurs destinés aux serveurs et datacenters.
