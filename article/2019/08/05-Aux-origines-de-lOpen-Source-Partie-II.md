---
site: Nticweb.com
title: "Aux origines de l'Open Source (Partie II)"
author: Samir Roubahi
date: 2019-08-05
href: http://www.nticweb.com/it/9783-aux-origines-de-l%E2%80%99open-source-partie-ii.html
featured_image: http://www.nticweb.com/images/resized/thumbs_l/images/opensource1_580_320.png
tags:
- Sensibilisation
- Éducation
series:
- 201932
series_weight: 0
---

> Nous avons précédemment parlé des enjeux historiques de l’Open Source et les conditions qui ont présidé à son apparition. Un groupe de gens visionnaires, un tantinet idéalistes et très motivés ont activement œuvré à en faire une réalité. Ceci part de l’idée que le code source des applications doit être un patrimoine commun à l’humanité et ne devrait pas être la propriété d’un cartel. Le revenu des sociétés informatiques devrait ainsi plutôt venir des services autour du logiciel : intégration, formation, support,…
