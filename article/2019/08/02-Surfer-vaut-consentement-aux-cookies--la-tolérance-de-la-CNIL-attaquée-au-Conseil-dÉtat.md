---
site: Next INpact
title: "Surfer vaut consentement aux cookies: la tolérance de la CNIL attaquée au Conseil d'État (€)"
author: Marc Rees
date: 2019-08-02
href: https://www.nextinpact.com/news/108110-surfer-vaut-consentement-aux-cookies-tolerance-cnil-attaquee-au-conseil-detat.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/16642.jpg
tags:
- Vie privée
- Internet
series:
- 201931
---

> La CNIL a décidé que pendant une période transitoire d’un an, la poursuite de la navigation vaudra expression du consentement à l’installation des cookies. Intolérables pour la Quadrature du Net et Caliopen qui, dans un recours au Conseil d’État, lui opposent le RGPD.  
