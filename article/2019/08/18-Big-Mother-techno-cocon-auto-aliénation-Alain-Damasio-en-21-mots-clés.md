---
site: L'OBS
title: Big Mother, techno-cocon, auto-aliénation... Alain Damasio en 21 mots-clés
author: Anne Crignon
date: 2019-08-18
href: https://www.nouvelobs.com/bibliobs/20190818.OBS17289/big-mother-techno-cocon-auto-alienation-alain-damasio-en-21-mots-cles.html
featured_image: https://focus.nouvelobs.com/2019/04/30/379/0/7181/3582/633/306/60/0/dc88adc_VvPmx89XUJa94koCD9kWqc1H.jpg
tags:
- Vie privée
series:
- 201934
series_weight: 0
---

> En trois romans, Alain Damasio s'est imposé comme un auteur culte de la SF française, doublé d'un critique acerbe du pouvoir. Voici sa pensée décryptée sous forme d'abécédaire.
