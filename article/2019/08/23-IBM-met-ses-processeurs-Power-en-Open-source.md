---
site: LeMagIT
title: "IBM met ses processeurs Power en Open source"
author: Ed Scannell
date: 2019-08-23
href: https://www.lemagit.fr/actualites/252469115/IBM-met-ses-processeurs-Power-en-Open-source
featured_image: https://cdn.ttgtmedia.com/visuals/ComputerWeekly/Hero%20Images/cpu-processor-chip-hardware-2-adobe.jpeg
tags:
- Matériel libre
series:
- 201934
---

> Désormais, tout fournisseur a le droit de concevoir, sans payer de royalties, un processeur personnalisé basé sur le Power. AWS, Azure et Google pourraient être les premiers intéressés.
