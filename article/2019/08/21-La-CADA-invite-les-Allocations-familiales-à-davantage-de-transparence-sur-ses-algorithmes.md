---
site: Next INpact
title: "La CADA invite les Allocations familiales à davantage de transparence sur ses algorithmes (€)"
author: Xavier Berne
date: 2019-08-21
href: https://www.nextinpact.com/news/108143-la-cada-invite-allocations-familiales-a-davantage-transparence-sur-ses-algorithmes.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/23614.jpg
tags:
- Open Data
- Administration
series:
- 201934
series_weight: 0
---

> Alors que les administrations sont théoriquement tenues de publier les principales règles de fonctionnement de leurs algorithmes, rares sont les acteurs publics à se plier à leurs nouvelles obligations (en vigueur depuis octobre 2017). La CADA vient ainsi d’inviter les Allocations familiales à davantage de transparence, à des fins pédagogiques.
