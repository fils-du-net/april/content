---
site: Le Monde.fr
title: "Copyright: la dernière bataille de l'Internet gratuit"
date: 2019-08-26
href: https://www.lemonde.fr/economie/article/2019/08/26/copyright-la-derniere-bataille-de-l-internet-gratuit_5502808_3234.html
featured_image: https://img.lemde.fr/2019/07/31/576/0/3500/1750/1440/0/60/0/a477585_zmF6YOWPQgOECyaFUqrQdbS-.jpg
tags:
- Entreprise
- Internet
- Économie
- Éducation
- Sciences
- Europe
- International
series:
- 201935
---

> «Désastre» pour les uns, «victoire» pour les autres: la directive sur les droits d'auteur divise les acteurs de l'industrie culturelle et ceux du numérique. Mais tous s'accordent sur le fait qu'elle change le visage du Web, dont la gratuité des contenus est un des fondements."> <meta property="og:site_name" content="Le Monde.fr