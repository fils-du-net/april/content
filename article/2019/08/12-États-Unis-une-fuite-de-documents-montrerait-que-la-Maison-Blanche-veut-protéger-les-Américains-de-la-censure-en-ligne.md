---
site: Developpez.com
title: "États-Unis: une fuite de documents montrerait que la Maison-Blanche veut «protéger les Américains de la censure en ligne»"
author: Bill Fassinou
date: 2019-08-12
href: https://www.developpez.com/actu/273429/Etats-Unis-une-fuite-de-documents-montrerait-que-la-Maison-Blanche-veut-censurer-Internet-a-travers-un-decret
featured_image: https://www.developpez.net/forums/attachments/p497231d1/a/a/a
tags:
- Droit d'auteur
- Institutions
series:
- 201933
series_weight: 0
---

> Une fuite de documents à la Maison-Blanche la semaine passée aurait révélé que le président américain Donald Trump serait en train de rédiger un décret visant à réglementer la censure des médias sociaux en ligne. Le projet de loi confierait à la FTC et à la FCC, la surveillance des échanges en ligne sur les plateformes de médias sociaux, les forums, etc. Le décret en question donnerait aux organismes fédéraux, le pouvoir de choisir quel type de données seraient acceptable ou non sur Internet. Pour beaucoup, ce projet témoigne de l’aversion profonde de Donald Trump pour les médias sociaux.
