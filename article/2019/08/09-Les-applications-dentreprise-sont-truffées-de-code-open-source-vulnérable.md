---
site: ICTjournal
title: "Les applications d'entreprise sont truffées de code open source vulnérable"
author: Yannick Chavanne
date: 2019-08-09
href: https://www.ictjournal.ch/etudes/2019-08-09/les-applications-dentreprise-sont-truffees-de-code-open-source-vulnerable
featured_image: https://www.ictjournal.ch/sites/default/files/styles/np8_full/public/media/2019/08/09/close-up-colors-concrete-1808275.jpg
tags:
- Entreprise
series:
- 201932
---

> Aujourd’hui largement présents dans le code des applications d'entreprise, les composants open source cachent fréquemment des vulnérabilités, prévient un rapport publié par Synopsys. En cause, des bouts de code devenu obsolète et des correctifs non appliqués.
