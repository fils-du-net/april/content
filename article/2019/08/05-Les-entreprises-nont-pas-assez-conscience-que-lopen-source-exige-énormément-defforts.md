---
site: Datanews.be
title: "'Les entreprises n'ont pas assez conscience que l'open source exige énormément d'efforts'"
author: Véronique Van Vlasselaer
date: 2019-08-05
href: https://datanews.levif.be/ict/actualite/les-entreprises-n-ont-pas-assez-conscience-que-l-open-source-exige-enormement-d-efforts/article-opinion-1173263.html
featured_image: https://web.roularta.be/if/c_crop,w_1779,h_1186,x_110,y_0,g_center/c_fit,w_620,h_413/3147f89706048775cf7196a04fad30e0.jpg
tags:
- Entreprise
series:
- 201932
---

> Les logiciels open source et les langages de programmations tels R et Python trouvent toujours plus facilement leur voie au sein des organisations. Selon l'experte en AI Véronique Van Vlasselaer, cela n'est pas sans risque. 'Il s'agit d'une erreur de la part de beaucoup de managers de croire qu'ils doivent migrer vers R et Python, parce qu'ils constatent que de la main d'oeuvre à peine sortie des études y est rompue', écrit-elle.
