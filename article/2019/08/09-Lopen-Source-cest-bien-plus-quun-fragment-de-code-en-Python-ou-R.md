---
site: Datanews.be
title: "'L'open Source, c'est bien plus qu'un fragment de code en Python ou R'"
author: Stef Schampaert
date: 2019-08-09
href: https://datanews.levif.be/ict/actualite/l-open-source-c-est-bien-plus-qu-un-fragment-de-code-en-python-ou-r/article-opinion-1175327.html
featured_image: https://web.roularta.be/if/c_crop,w_1999,h_1333,x_0,y_0,g_center/c_fit,w_620,h_413/a1d81ff4c218b7425c823ffebe03fafd.jpg
tags:
- Entreprise
series:
- 201932
---

> Dans une opinion récente, Véronique Van Vlasselaer s'est montrée critique à l'égard d'entreprises qui migrent vers R ou Python sans trop réfléchir aux implications. Stef Schampaert de Red Hat réagit: 'Il existe aussi ce qu'on appelle l'open source professionnel, qui permet aux entreprises d'opérer en pleine confiance, ce qui se fait du reste toujours plus souvent dans la pratique.'
