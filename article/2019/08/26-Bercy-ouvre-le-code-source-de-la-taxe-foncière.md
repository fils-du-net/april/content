---
site: Next INpact
title: "Bercy ouvre le code source de la taxe foncière"
author: Xavier Berne
date: 2019-08-26
href: https://www.nextinpact.com/news/108152-bercy-ouvre-code-source-taxe-fonciere.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/23622.jpg
tags:
- Internet
- Open Data
- Europe
- Vie privée
series:
- 201935
---

> La Direction générale des finances publiques (DGFiP) vient de mettre en ligne le code source utilisé pour le calcul de la taxe foncière. Une initiative qui fait suite à une procédure « CADA » lancée par Next INpact, il y a près d’un an et demi.