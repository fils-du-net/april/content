---
site: France Inter
title: "Scanner pour mieux consommer est-il bon pour votre santé?"
date: 2019-08-26
href: https://www.franceinter.fr/emissions/grand-bien-vous-fasse/grand-bien-vous-fasse-26-aout-2019
featured_image: https://cdn.radiofrance.fr/s3/cruiser-production/2019/08/ebd39088-8dd4-4959-bf3e-558fe7bc0613/640_application_sante_gettyimages-926538832.jpg
tags:
- Internet
- Économie
- Éducation
- Sciences
- International
series:
- 201935
---

> Leur objectif est d'évaluer la qualité nutritionnelle des produits alimentaires que nous achetons en supermarché. On s'intéresse à la fiabilité de ces applications-vedette de plus en plus présentent dans nos smartphones. Y a-t-il des risques à trop vouloir contrôler et personnaliser ce que nous consommons?
