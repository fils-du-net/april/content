---
site: Developpez.com
title: "Microsoft rend son système de fichiers exFAT open source et soutien l'intégration de cette technologie au noyau Linux"
description: Afin de faciliter l'interopérabilité entre Linux et Windows 10
author: Christian Olivier
date: 20190829
href: https://linux.developpez.com/actu/275341/Microsoft-rend-son-systeme-de-fichiers-exFAT-open-source-et-soutien-l-integration-de-cette-technologie-au-noyau-Linux-afin-de-faciliter-l-interoperabilite-entre-Linux-et-Windows-10
featured_image: https://www.developpez.net/forums/attachments/p500162d1/a/a/a
tags:
- Brevets logiciels
- Entreprise
series:
- 201935
---

> Microsoft vient d’annoncer qu’il facilitera encore davantage l’interopérabilité entre les OS basés sur Linux et Windows 10 en rendant publiques les spécifications de son système de gestion des fichiers exFAT. La firme de Redmond ne se contente pas de rendre sa spécification open source, mais concèdera également les brevets relatifs à cette technologie à l’Open Invention Network (OIN). Créé en 2005, l’OIN est une communauté axée sur Linux regroupant plus de 3040 entreprises du monde entier qui ont accepté de concéder sous licence leurs brevets, sans redevance, afin de protéger Linux contre les poursuites en justice.
