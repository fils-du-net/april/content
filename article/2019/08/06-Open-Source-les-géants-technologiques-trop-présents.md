---
site: Programmez!
title: "Open Source: les géants technologiques trop présents?"
author: ftonic
date: 2019-08-06
href: https://www.programmez.com/actualites/open-source-les-geants-technologiques-trop-presents-29214
tags:
- Entreprise
- Innovation
series:
- 201932
series_weight: 0
---

> Ce débat n'est pas réellement nouveau. Régulièrement, la question se pose: les géants de la tech sont-ils trop présents dans l'open source? Il n'y a qu'à voir les rachats de GitHub, de Red Hat et les milliers de développeurs de ces éditeurs / constructeurs dans les projets ouverts.
