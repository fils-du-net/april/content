---
site: ITforBusiness
title: "L'usage de l'open-source en entreprise se renforce"
author: Laurent Delattre
date: 2019-08-06
href: https://www.itforbusiness.fr/lusage-de-lopen-source-en-entreprise-se-renforce-22041
featured_image: https://www.itforbusiness.fr/wp-content/uploads/2019/08/shutterstock_566852098.jpg
tags:
- Entreprise
series:
- 201932
---

> L’emploi de solutions open-source s’intensifie et se généralise dans les entreprises et pas uniquement pour les développements et la gestion de l’infrastructure. BI et bases de données sont devenus les nouveaux terrains de croissance de l’open-source…
