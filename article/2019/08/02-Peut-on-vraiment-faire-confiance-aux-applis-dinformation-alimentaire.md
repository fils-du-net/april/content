---
site: Maddyness
title: "Peut-on (vraiment) faire confiance aux applis d'information alimentaire?"
date: 2019-08-02
href: https://www.maddyness.com/2019/08/09/consommation-applis-alimentaire
featured_image: https://www.maddyness.com/wp-content/uploads/2019/03/tracabilitealiment-e1554217998706-924x462.jpg
tags:
- Partage du savoir
- Innovation
series:
- 201931
---

> Pensées pour mieux informer les consommatrices et consommateurs sur ce qu'ils achètent, les applis conso se sont multipliées ces derniers mois, Yuka en tête. Mais celles-ci dépendent bien souvent de fichiers en open source dont la fiabilité est variable et, parfois, des industriels eux-mêmes.
