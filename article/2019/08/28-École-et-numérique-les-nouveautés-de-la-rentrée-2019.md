---
site: Next INpact
title: "École et numérique: les nouveautés de la rentrée 2019 (€)"
author: Xavier Berne
date: 2019-08-28
href: https://www.nextinpact.com/news/108158-ecole-et-numerique-nouveautes-rentree-2019.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/13447.jpg
tags:
- Éducation
series:
- 201935
---

> Alors que plusieurs millions d’élèves s’apprêtent à reprendre le chemin de l’école, Next INpact revient aujourd’hui sur les principaux changements à attendre de cette rentrée, notamment du côté des programmes. Au lycée, l’enseignement du numérique est ainsi appelé à être «profondément renouvelé», dixit le ministre de l’Éducation.
