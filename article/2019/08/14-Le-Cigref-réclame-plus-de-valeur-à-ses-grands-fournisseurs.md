---
site: ITforBusiness
title: "Le Cigref réclame plus de valeur à ses grands fournisseurs"
author: Pierre Landry
date: 2019-08-14
href: https://www.itforbusiness.fr/le-cigref-reclame-plus-de-valeur-a-ses-grands-fournisseurs-22445
featured_image: https://www.itforbusiness.fr/wp-content/uploads/2019/08/shutterstock_409129369.jpg
tags:
- Entreprise
- Logiciels privateurs
- Informatique en nuage
series:
- 201933
series_weight: 0
---

> Les rapports entre les entreprises membres du Cigref et les acteurs majeurs de l’IT se détendent progressivement. Des points d’achoppement subsistent, notamment sur certaines promesses non tenues du cloud.
