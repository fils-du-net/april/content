---
site: InformatiqueNews.fr
title: "IBM bascule l'architecture du jeu d'instruction de sa plateforme Power en open source"
date: 2019-08-23
href: https://www.informatiquenews.fr/ibm-bascule-larchitecture-du-jeu-dinstruction-de-sa-plateforme-power-en-open-source-63013
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2019/08/23-IBM-Une.jpg
tags:
- Matériel libre
series:
- 201934
---

> C’est par un simple tweet sur son site qu’IBM a tout d’abord annoncé qu’il franchissait un nouveau pas dans l’open source. «IBM met également en open source les bases techniques de la puce Power Series. Ce qu’il y a peut-être de plus important c’est que la société est en train d’ouvrir à tous Power ISA», peut-on lire.
