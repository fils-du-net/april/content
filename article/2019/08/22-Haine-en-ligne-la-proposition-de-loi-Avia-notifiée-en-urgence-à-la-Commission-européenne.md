---
site: Next INpact
title: "Haine en ligne: la proposition de loi Avia notifiée en urgence à la Commission européenne"
author: Marc Rees
date: 2019-08-22
href: https://www.nextinpact.com/news/108146-haine-en-ligne-proposition-loi-avia-notifiee-en-urgence-a-commission-europeenne.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/22603.jpg
tags:
- Internet
- Institutions
- Europe
series:
- 201934
series_weight: 0
---

> La France a notifié hier la proposition de loi de la députée LREM Laetitia Avia, adoptée début juillet à l’Assemblée nationale. Cette procédure est obligatoire dès lors qu’un État membre entend réguler le secteur des nouvelles technologies.
