---
site: Developpez.com
title: "Open hardware: IBM ouvre l'architecture de jeu d'instructions de sa famille de processeurs POWER"
author: Patrick Ruiz
date: 2019-08-21
href: https://hardware.developpez.com/actu/274448/Open-hardware-IBM-ouvre-l-architecture-de-jeu-d-instructions-de-sa-famille-de-processeurs-POWER-pour-serveurs-et-supercalculateurs
featured_image: https://www.developpez.net/forums/attachments/p498765d1/a/a/a
tags:
- Matériel libre
series:
- 201934
---

> IBM ouvre l’architecture de jeu d’instructions de sa famille de processeurs POWER dont l’un, le IBM POWER 9, est utilisé au sein de l’un des supercalculateurs les plus puissants au monde – le superordinateur Summit. L’annonce est tombée via divers canaux officiels de l’entreprise dont celui de la OpenPower Foundation – une organisation mise en place par IBM il y a 6 ans pour encourager l’adoption de la famille de puces.
