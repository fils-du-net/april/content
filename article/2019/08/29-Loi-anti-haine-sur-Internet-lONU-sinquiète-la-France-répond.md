---
site: Numerama
title: "Loi anti-haine sur Internet: l'ONU s'inquiète, la France répond"
author: Julien Lausson
date: 2019-08-29
href: https://www.numerama.com/politique/543760-loi-anti-haine-sur-internet-lonu-sinquiete-la-france-repond.html
featured_image: https://www.numerama.com/content/uploads/2019/03/david-kaye-1024x579.jpg
tags:
- Institutions
- Internet
series:
- 201935
series_weight: 0
---

> La proposition de loi contre la haine sur Internet inquiète à l'international. Un rapporteur spécial de l'ONU sur la liberté d'expression a fait part de ses inquiétudes à la France, qui a pris la plume pour répondre et dissiper ce qui apparaît comme des malentendus.
