---
site: ZDNet France
title: "Microsoft libère son système de fichiers exFAT pour Linux et open source"
author: Steven J. Vaughan-Nichols
date: 2019-08-29
href: https://www.zdnet.fr/actualites/microsoft-libere-son-systeme-de-fichiers-exfat-pour-linux-et-open-source-39889725.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/06/MicrosoftOIN_620.jpg
tags:
- Brevets logiciels
- Entreprise
series:
- 201935
series_weight: 0
---

> Pendant des années, Microsoft a profité de ses brevets pour son système de fichiers FAT. Aujourd'hui, l'entreprise déclare explicitement libérer ses brevets restants exFAT aux membres de l'Open Invention Network.
