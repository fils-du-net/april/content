---
site: Usbek & Rica
title: "«La création collaborative de jeux est utile pour les élèves»"
author: Isabelle Arvers
date: 2019-08-15
href: https://usbeketrica.com/article/creation-collaborative-de-jeux-utile-pour-les-eleves
featured_image: https://static.usbeketrica.com/images/thumb_840xh/5d4432f220e50.jpg
tags:
- Éducation
- International
series:
- 201933
series_weight: 0
---

> Rencontre à Taïwan avec Audrey Tang, ministre en charge des questions numériques et chantre de la transparence.
