---
site: Next INpact
title: "Directive Droit d'auteur: la Pologne attaque l'article 17 sur fond de liberté d'expression (€)"
author: Marc Rees
date: 2019-08-21
href: https://www.nextinpact.com/news/108144-directive-droit-dauteur-pologne-attaque-larticle-17-sur-fond-liberte-dexpression.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/16183.jpg
tags:
- Droit d'auteur
- Europe
series:
- 201934
---

> La Pologne a officiellement attaqué l’article 17 de la directive sur le droit d’auteur devant la Cour de justice de l'Union européenne. Selon l’État membre, la disposition relative au filtrage des contenus viole la liberté d’expression et d’information. Un argument maintes fois porté par les opposants au texte, toujours contesté par ses partisans.
