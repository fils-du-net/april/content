---
site: Silicon
title: "Open Source: IBM ouvre Power ISA"
author: Ariane Beky
date: 2019-08-21
href: https://www.silicon.fr/open-source-ibm-ouvre-power-isa-259225.html
featured_image: https://www.silicon.fr/wp-content/uploads/2019/01/ibm-bnp-paribas.jpg
tags:
- Matériel libre
series:
- 201934
series_weight: 0
---

> IBM ouvre l'architecture de jeu d'instructions (ISA) de sa famille de processeurs Power et se rapproche un peu plus de la Fondation Linux.
