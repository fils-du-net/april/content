---
site: Gridam
title: "PeerTube: l'alternative libre à YouTube"
author: Emeric
date: 2019-08-12
href: https://www.gridam.com/2019/08/peertube-alternative-libre-youtube
featured_image: https://www.gridam.com/wp-content/uploads/2019/08/peertube-660x400.png
tags:
- Innovation
series:
- 201933
series_weight: 0
---

> L'association française Framasoft, qui milite pour le développement de logiciels libres, a lancé PeerTube, une alternative libre à YouTube.
