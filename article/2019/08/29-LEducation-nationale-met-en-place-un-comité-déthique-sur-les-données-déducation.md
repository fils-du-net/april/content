---
site: Le Monde Informatique
title: "L'Education nationale met en place un comité d'éthique sur les données d'éducation"
date: 2019-08-29
href: https://www.lemondeinformatique.fr/actualites/lire-l-education-nationale-met-en-place-un-comite-d-ethique-sur-les-donnees-d-education-76253.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000067985.jpg
tags:
- Éducation
series:
- 201935
series_weight: 0
---

> Dans une vidéo, Jean-Michel Blanquer a évoqué la production importante des données par le système éducatif. Afin de garantir une protection maximale de ces données, le ministre de l'Education nationale va mettre en place à la rentrée scolaire un comité d'éthique sur les données d'éducation. Il sera présidé par Claudie Haigneré.
