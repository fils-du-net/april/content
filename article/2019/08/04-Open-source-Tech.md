---
site: Futura
title: "Open source Tech"
date: 2019-08-04
href: https://www.futura-sciences.com/tech/definitions/informatique-open-source-18154
featured_image: https://fr.cdn.v5.futura-sciences.com/buildsv6/images/wide1280/5/4/a/54ae58505a_50153232_open-source-1.jpg
seeAlso: "[Position sur la terminologie logiciel libre - open source](https://www.april.org/position-sur-la-terminologie-logiciel-libre-open-source)<br />[En quoi l'open source perd de vue l'éthique du logiciel libre](https://www.gnu.org/philosophy/open-source-misses-the-point.html)"
tags:
- Sensibilisation
- Licenses
series:
- 201931
series_weight: 0
---

> L'open source est un adjectif donné aux logiciels qui sont développés en respectant les principes de l'OSI, l'Open Source Initiative. La notion d'open source est apparue à la fin des années 1990, lorsque certains utilisateurs de la communauté du logiciel libre ont décidé d'adapter leurs dogmes aux réalités économiques.
