---
site: Techdirt.
title: "It's On: Details Emerge Of Polish Government's Formal Request For Top EU Court To Throw Out Upload Filters"
date: 2019-08-20
href: https://www.techdirt.com/articles/20190819/08344542817/details-emerge-polish-governments-formal-request-top-eu-court-to-throw-out-upload-filters.shtml
tags:
- Droit d'auteur
- Europe
- English
series:
- 201934
---

> Earlier this year, Techdirt wrote about an intriguing tweet from the account of the Chancellery of the Prime Minister of Poland, which announced: "Tomorrow #Poland brings action against copyright directive to CJEU". The hashtags for the tweet made clear what Poland was worried about: "#Article13 #Article17".
