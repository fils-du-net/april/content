---
site: La Libre.be
title: "Bienvenue dans les \"labos\" de l'ère numérique"
author: Pierre-François Lovens et François Mathieu
date: 2019-08-09
href: https://www.lalibre.be/economie/entreprises-startup/bienvenue-dans-les-labos-de-l-ere-numerique-5d4d774dd8ad5859353fb178
featured_image: https://t3.llb.be/y4Ubfi1dRLgUh5j82-TWy5_Wuqw=/0x330:5396x3028/940x470/5d4d77e9d8ad5859353fb17b.jpg
tags:
- Innovation
series:
- 201932
series_weight: 0
---

> Il aura fallu un peu de temps pour que la marée des "fablabs" touche la Belgique. Mais, cette fois, elle a pris suffisamment d’ampleur pour commencer à irriguer l’ensemble du plat pays. Et si on en croit les experts, ce ne serait que le début d’une véritable révolution.
