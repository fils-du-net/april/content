---
site: Le nouvel Economiste
title: "Logiciel libre: Les leçons de Firefox et Mozilla aux géants de la tech (€)"
date: 2019-08-17
href: https://www.lenouveleconomiste.fr/ce-que-la-culture-du-logiciel-libre-peut-enseigner-aux-geants-de-la-tech-71946
featured_image: https://www.lenouveleconomiste.fr/wp-content/uploads/2019/08/Les-le%C3%A7ons-de-Firefox-et-Mozilla-aux-g%C3%A9ants-de-la-tech.jpg
tags:
- Entreprise
- Internet
series:
- 201933
---

> Mozilla a fait la preuve qu’une alternative non commerciale orientée sur la défense des intérêts de l’utilisateur est une bonne chose
