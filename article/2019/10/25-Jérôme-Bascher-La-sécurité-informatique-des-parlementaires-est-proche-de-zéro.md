---
site: Public Sénat
title: "Jérôme Bascher: «La sécurité informatique des parlementaires est proche de zéro»"
date: 2019-10-25
href: https://www.publicsenat.fr/article/parlementaire/jerome-bascher-la-securite-informatique-des-parlementaires-est-proche-de-zero
featured_image: https://www.publicsenat.fr/sites/default/files/styles/pse_contenu_entete_16_9/public/thumbnails/image/ordinateur.jpg
tags:
- Institutions
series:
- 201943
---

> Dans un rapport sur la sécurité informatique des pouvoirs publics, le sénateur LR de l’Oise, Jérôme Bascher, alerte sur le niveau de sécurité informatique à l’Assemblée nationale et au Sénat.
