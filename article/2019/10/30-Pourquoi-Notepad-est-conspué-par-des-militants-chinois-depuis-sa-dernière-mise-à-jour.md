---
site: Numerama
title: "Pourquoi Notepad++ est conspué par des militants chinois depuis sa dernière mise à jour"
author: Julien Lausson
date: 2019-10-30
href: https://www.numerama.com/politique/565643-pourquoi-notepad-est-conspue-par-des-militants-chinois-depuis-sa-derniere-mise-a-jour.html
featured_image: https://www.numerama.com/content/uploads/2015/12/notepad2.jpg
tags:
- International
series:
- 201944
series_weight: 0
---

> Le développeur de Notepad++ a décidé de baptiser la dernière version de son logiciel en honneur de la population ouïghour, qui subi une répression de la part de la Chine. Un engagement qui a fortement déplu à des internautes chinois.
