---
site: Le Monde.fr
title: "Cédric O: «Expérimenter la reconnaissance faciale est nécessaire pour que nos industriels progressent»"
author: Martin Untersinger
date: 2019-10-14
href: https://www.lemonde.fr/economie/article/2019/10/14/cedric-o-experimenter-la-reconnaissance-faciale-est-necessaire-pour-que-nos-industriels-progressent_6015395_3234.html
featured_image: https://img.lemde.fr/2019/10/13/0/0/5084/3366/688/0/60/0/5d265ee_oLXPbm-z26LoSaWf_wSlDoM3.jpg
tags:
- Vie privée
- Institutions
series:
- 201942
---

> Le secrétaire d'Etat au numérique annonce au «Monde» vouloir créer, en coordination avec la CNIL, une instance de supervision et d'évaluation.
