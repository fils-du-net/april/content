---
site: Le Monde Informatique
title: "Open source et Défense: les liaisons dangereuses sur fond d'éthique"
author: Tamlin Magee
date: 2019-10-06
href: https://www.lemondeinformatique.fr/actualites/lire-open-source-et-defense-les-liaisons-dangereuses-sur-fond-d-ethique-76658.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000068632.jpg
tags:
- Licenses
- Philosophie GNU
series:
- 201940
series_weight: 0
---

> Le complexe militaro-industriel raffole des logiciels libres. Le Département de la Défense des États-Unis intègre avec boulimie de l'open source dans l'armée et dans les armes fabriquées pour son compte, depuis les avions de combat jusqu'aux lance-roquettes. Mais l'open source doit-il servir à tuer?
