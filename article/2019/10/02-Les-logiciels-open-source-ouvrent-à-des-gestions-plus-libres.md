---
site: La gazette.fr
title: "Les logiciels open source ouvrent à des gestions plus libres (€)"
author: Baptiste Cessieux
date: 2019-10-02
href: https://www.lagazettedescommunes.com/640150/les-logiciels-open-source-ouvrent-a-des-gestions-plus-libres
featured_image: https://www.lagazettedescommunes.com/wp-content/uploads/2019/09/logiciels.jpg
tags:
- Marchés publics
series:
- 201940
series_weight: 0
---

> Comment choisir ses outils numériques? Etre sûr de conserver les informations et suivre les évolutions technologiques entre deux marchés publics? Editeurs et prestataires transforment leurs offres pour les rendre interopérables. De plus en plus, ils se tournent vers des logiciels ouverts.Les collectivités ont le choix entre la gestion interne ou externe de leurs logiciels et des données qu’ils produisent.
