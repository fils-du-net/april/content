---
site: ZDNet France
title: "Souveraineté numérique et logiciel libre: un rapport du Sénat invite l'Etat à plus de volontarisme"
author: Thierry Noisette
date: 2019-10-14
href: https://www.zdnet.fr/blogs/l-esprit-libre/souverainete-numerique-et-logiciel-libre-un-rapport-du-senat-invite-l-etat-a-plus-de-volontarisme-39892183.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2019/10/Mistral_bateau_WMC_02.jpg
seeAlso: "[Rapport du Sénat sur la souveraineté numérique: il est urgent d'engager la réflexion sur le recours aux logiciels libres au sein de l'État](https://april.org/rapport-du-senat-sur-la-souverainete-numerique-il-est-urgent-d-engager-la-reflexion-sur-le-recours-a)"
tags:
- april
- Institutions
- Marchés publics
series:
- 201942
series_weight: 0
---

> Le rapport Longuet sur "le devoir de souveraineté numérique" regrette l'absence de doctrine de l'Etat en matière de logiciels libres, et l'incite à "engager rapidement une réflexion au niveau interministériel sur ce sujet".
