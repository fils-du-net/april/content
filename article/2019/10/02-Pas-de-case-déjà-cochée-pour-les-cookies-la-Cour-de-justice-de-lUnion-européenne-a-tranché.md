---
site: BDM 
title: "Pas de case déjà cochée pour les cookies: la Cour de justice de l'Union européenne a tranché"
author: Thomas Coëffé
date: 2019-10-02
href: https://www.blogdumoderateur.com/cookies-case-deja-cochee-cour-justice-europe/
featured_image: https://www.blogdumoderateur.com/wp-content/themes/moderateur-2018/img/svg/logo-bdm-job-menu.svg
tags:
- Institutions
- Vie privée
series:
- 201940
---

> La Cour de justice de l'Union européenne considère que les cases déjà cochées ne permettent pas d'obtenir le consentement actif des utilisateurs.
