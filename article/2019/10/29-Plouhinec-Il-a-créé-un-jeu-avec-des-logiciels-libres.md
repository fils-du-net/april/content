---
site: ouest-france.fr
title: "Plouhinec. Il a créé un jeu avec des logiciels libres"
date: 2019-10-29
href: https://www.ouest-france.fr/bretagne/plouhinec-56680/plouhinec-il-cree-un-jeu-avec-des-logiciels-libres-6586953
featured_image: https://media.ouest-france.fr/v1/pictures/MjAxOTEwMzQwODlmOTE4MzY0MTJlM2RkNGZmMTc0ZDU1MTY2MjM
tags:
- Sensibilisation
series:
- 201944
series_weight: 0
---

> Avec son association Armaludis, Cyrille RW Chaussepied, graphiste, développeur et auteur, a créé un jeu de cartes avec dés, intitulé Chaos 2D6.
