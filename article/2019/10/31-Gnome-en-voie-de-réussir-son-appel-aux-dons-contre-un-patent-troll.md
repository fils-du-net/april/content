---
site: ZDNet France
title: "Gnome en voie de réussir son appel aux dons contre un 'patent troll'"
author: Thierry Noisette
date: 2019-10-31
href: https://www.zdnet.fr/blogs/l-esprit-libre/gnome-en-voie-de-reussir-son-appel-aux-dons-contre-un-patent-troll-39893175.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2019/10/patent%20trolls-machine%20a%20ecrire_Flickr.jpg
tags:
- Brevets logiciels
series:
- 201944
series_weight: 0
---

> La fondation Gnome a lancé un appel à financement participatif pour sa contre-attaque judiciaire face à un prétendu brevet allégué par Rothschild Patent Imaging (RPI). Ce jeudi, il a récolté 117.000 dollars.
