---
site: Next INpact
title: "Plus de 300 députés prêts à mettre en ligne leurs agendas (€)"
author: Xavier Berne
date: 2019-10-10
href: https://www.nextinpact.com/news/108294-plus-300-deputes-prets-a-mettre-en-ligne-leurs-agendas.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/11402.jpg
tags:
- Institutions
series:
- 201941
---

> Au travers d’une tribune, plus de trois cent députés de la majorité laissent entrevoir des «pratiques radicalement nouvelles et volontaristes en matière de transparence». Ces élus pourraient notamment mettre en ligne leurs agendas, afin de faire la lumière sur les lobbys qu’ils rencontrent.
