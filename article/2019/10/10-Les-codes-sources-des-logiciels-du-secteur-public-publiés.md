---
site: Le Monde Informatique
title: "Les codes sources des logiciels du secteur public publiés"
author: Dominique Filippone
date: 2019-10-10
href: https://www.lemondeinformatique.fr/actualites/lire-les-codes-sources-des-logiciels-du-secteur-public-publies-76714.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000068705.jpg
tags:
- Open Data
series:
- 201941
series_weight: 0
---

> Lancé par Etalab, le site code.etalab.gouv.fr permet d'accéder aux codes sources des logiciels utilisés par les organismes publics. Ce référencement rassemble à ce jour près de 2 400 dépôts issus d'organismes variés dont le CNRS, l'IGN ou encore la DINSIC et l'ANSSI.
