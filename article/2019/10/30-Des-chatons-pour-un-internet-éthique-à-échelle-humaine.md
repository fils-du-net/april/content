---
site: Le Courrier
title: "Des chatons pour un internet éthique à échelle humaine"
author: Susana Jourdan, Jacques Mirenowicz
date: 2019-10-30
href: https://lecourrier.ch/2019/10/30/des-chatons-pour-un-internet-ethique-a-echelle-humaine
tags:
- Associations
- Internet
series:
- 201944
series_weight: 0
---

> Les grandes plates-formes internet ne sont pas irremplaçables. Depuis 2014, une petite association française, Framasoft, rassemble et forge des alternatives libres, éthiques et sans surveillance aux services les plus populaires sur internet. Pour décentraliser ces services, elle promeut l’émergence d’hébergeurs alternatifs, transparents, ouverts, neutres et solidaires: les chatons!
