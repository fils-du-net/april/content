---
site: Télérama.fr
title: "La reconnaissance faciale débarque en France et vous n'y échapperez pas"
date: 2019-10-08
author: Olivier Tesquet
href: https://www.telerama.fr/medias/la-reconnaissance-faciale-debarque-en-france-et-vous-ny-echapperez-pas,n6459175.php
featured_image: https://www.telerama.fr/sites/tr_master/files/styles/simplecrop1000/public/30188201497_c72e2fd9ef_b_0.jpg
tags:
- Vie privée
series:
- 201941
---

> Alors que l’ombre de l’exemple chinois et de son “crédit social” plane sur les libertés publiques, la France s’apprête à déployer ALICEM, un système de reconnaissance faciale pour accéder aux services publics en ligne. Malgré de nombreuses réticences...
