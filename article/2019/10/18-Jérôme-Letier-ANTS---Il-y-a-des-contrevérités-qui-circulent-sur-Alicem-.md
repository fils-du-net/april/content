---
site: Next INpact
title: "Jérôme Letier (ANTS): «Il y a des contrevérités qui circulent sur Alicem» (€)"
author: Marc Rees
date: 2019-10-18
href: https://www.nextinpact.com/news/108194-jerome-letier-ants-il-y-a-contreverites-qui-circulent-sur-alicem.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/13008.jpg
tags:
- Vie privée
- Institutions
- Associations
series:
- 201942
series_weight: 0
---

> Jérôme Letier, directeur de l’Agence nationale des titres sécurisés, revient dans nos colonnes sur le projet Alicem ou «Authentification en ligne certifiée sur mobile». Un projet biométrique en passe de devenir réalité, mais qui subit plusieurs critiques liées déjà à l’usage de la reconnaissance faciale.
