---
site: Les Echos
title: "Intelligence artificielle: peut-on se passer des Gafam ?"
author: Jean-Luc Marini
date: 2019-10-08
href: https://www.lesechos.fr/idees-debats/cercle/intelligence-artificielle-peut-on-se-passer-des-gafam-1138142
featured_image: https://media.lesechos.com/api/v1/images/view/5d9c3f70d286c21bfb222e15/1280x720/200029-1.jpg
tags:
- Innovation
- Entreprise
- International
series:
- 201941
series_weight: 0
---

> Pour Jean-Luc Marini, directeur du Laboratoire d'intelligence artificielle, nous devons gagner notre indépendance technologique pour ne pas subir le contrecoup de l'aliénation aux outils numériques étrangers.
