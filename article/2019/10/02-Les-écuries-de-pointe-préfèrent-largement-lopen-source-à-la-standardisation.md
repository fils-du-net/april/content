---
site: Nextgen-Auto.com
title: "Les écuries de pointe préfèrent largement l'open source à la standardisation"
description: Une porte de sortie pour la FIA?
date: 2019-10-02
href: https://motorsport.nextgen-auto.com/fr/formule-1/les-ecuries-de-pointe-preferent-largement-l-open-source-a-la-standardisation,142235.html
featured_image: https://motorsport.nextgen-auto.com/IMG/arton142235.jpg
tags:
- Matériel libre
series:
- 201940
---

> En particulier du côté des écuries de pointe mais aussi du côté de Haas, le projet de standardisation accrue de certaines pièces des monoplaces prévues pour 2021 a suscité de sérieuses réserves.
