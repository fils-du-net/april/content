---
site: Les Echos
title: "Le Sénat veut faire de l'Arcep le gendarme des smartphones"
author: Raphaël Balenieri
date: 2019-10-22
href: https://www.lesechos.fr/tech-medias/hightech/le-senat-veut-faire-de-larcep-le-gendarme-des-smartphones-1142048
featured_image: https://media.lesechos.com/api/v1/images/view/5daf27e63e45462954138605/1280x720/0602105116436-web-tete.jpg
tags:
- Institutions
series:
- 201943
series_weight: 0
---

> Une proposition de loi, présentée par la Commission des affaires économiques, souhaite renforcer le régulateur des télécoms en lui confiant le contrôle des systèmes d'exploitation et des boutiques d'application sur les téléphones.
