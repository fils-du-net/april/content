---
site: ZDNet France
title: "Linus Torvalds n'a pas peur de Microsoft"
author: Steven J. Vaughan-Nichols
date: 2019-10-08
href: https://www.zdnet.fr/actualites/linus-torvalds-n-a-pas-peur-de-microsoft-39891855.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2018/11/windowsandlinux.jpg
tags:
- Entreprise
- Brevets logiciels
series:
- 201941
series_weight: 0
---

> De nombreuses voix s'élèvent pour dénoncer l'influence grandissante de Microsoft sur Linux. Linus Torvalds ne s'en inquiète pas et vous ne devriez pas non plus.  De nombreuses entreprises veulent contrôler Linux mais aucune d'entre elles ne le fera.
