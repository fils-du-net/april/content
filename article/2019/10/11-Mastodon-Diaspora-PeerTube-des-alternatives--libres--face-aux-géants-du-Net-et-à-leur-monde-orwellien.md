---
site: Basta!
title: "Mastodon, Diaspora, PeerTube...: des alternatives «libres» face aux géants du Net et à leur monde orwellien"
author: Alexis Moreau, Rachel Knaebel
date: 2019-10-11
href: https://www.bastamag.net/Mastodon-Diaspora-PeerTube-Qwant-framasoft-logiciels-libres-open-street-map-alternatives-aux-Gafam
tags:
- Sensibilisation
- Internet
series:
- 201941
series_weight: 0
---

> En vingt ans, Google, Apple, Facebook, Amazon et Microsoft ont investi notre quotidien, colonisé nos imaginaires, mis la main sur nos données personnelles. Peut-on leur échapper? Pour chacun de ces services, les alternatives aux «Gafam» existent déjà, et rassemblent, pour certaines d’entre elles, plusieurs millions d’utilisateurs. Basta! vous les présente.
