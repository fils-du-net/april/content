---
site: Le Monde.fr
title: "Publicité, hébergement, open-source: comment le Web est devenu dépendant des GAFA"
author: Gary Dagorn et Maxime Ferrer
date: 2019-10-27
href: https://www.lemonde.fr/les-decodeurs/article/2019/10/27/publicite-hebergement-open-source-comment-le-web-est-devenu-dependant-des-gafa_6017082_4355770.html
featured_image: https://img.lemde.fr/2019/10/25/0/0/2400/1600/688/0/60/0/8aac019_wP64w2P4c1PmZdx1mtacbjOL.jpg
tags:
- Institutions
series:
- 201943
---

> Bien connus du grand public pour leurs services aux internautes, Google, Amazon et Facebook sont aussi très présents dans les technologies du Web, les rendant quasi indispensables.
