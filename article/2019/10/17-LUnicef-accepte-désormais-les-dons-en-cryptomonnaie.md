---
site: ZDNet France
title: "L'Unicef accepte désormais les dons en cryptomonnaie"
author: Asha Barbaschow
date: 2019-10-17
href: https://www.zdnet.fr/actualites/l-unicef-accepte-desormais-les-dons-en-cryptomonnaie-39892361.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2019/02/ONU%20620.jpg
tags:
- Économie
series:
- 201942
series_weight: 0
---

> L'Unicef ne se contente plus d'accepter des dons en monnaie sonnante et trébuchante. L'organisation internationale s'est désormais convertie aux dons en cryptomonnaie.
