---
site: Developpez.com
title: "Personne, ni même Microsoft, ne peut prendre le contrôle à lui tout seul du projet Linux,"
description: "A déclaré Linus Torvalds"
author: Bill Fassinou
href: https://linux.developpez.com/actu/280116/Personne-ni-meme-Microsoft-ne-peut-prendre-le-controle-a-lui-tout-seul-du-projet-Linux-a-declare-Linus-Torvalds
featured_image: https://www.developpez.net/forums/attachments/p508438d1/a/a/a
tags:
- Entreprise
series:
- 201941
---

> Le noyau Linux a été secoué à ses débuts par des géants comme Apple avec Mac OS et Microsoft avec Windows. Mais Linux a fini par atteindre une popularité inédite, même s’il peine à s’imposer sur les ordinateurs de bureau. Microsoft, qui autrefois voyait le noyau comme un cancer, est maintenant devenu l’un de ses plus grands contributeurs, et continue à s’imposer dans la communauté Linux. Pour cela, certains ont commencé à s'inquiéter sur le fait que Microsoft arrive à contrôler Linux totalement. Mais à la conférence Linux Plumbers 2019, Linus Torvalds a assuré que non, et que cela n’arrivera jamais.
