---
site: Joinup
title: "Cooperative interaction"
author: Gijs Hillenius
date: 2019-10-19
href: https://joinup.ec.europa.eu/collection/open-source-observatory-osor/news/cooperative-interaction
featured_image: https://joinup.ec.europa.eu/sites/default/files/styles/wysiwyg_half_width/public/inline-images/cz_0.png
tags:
- Open Data
- English
series:
- 201942
series_weight: 0
---

> The Swiss Canton of Zürich will make software developed for or by its public services available as open source. On 30 September the Canton parliament accepted a proposal to change the way it develops software to increase the role of open source. The government now has two years to work out how to adapt its rules and regulations.
