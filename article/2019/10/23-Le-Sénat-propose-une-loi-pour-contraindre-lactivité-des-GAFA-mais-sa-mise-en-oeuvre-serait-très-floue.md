---
site: Business Insider France
title: "Le Sénat propose une loi pour contraindre l'activité des GAFA mais sa mise en oeuvre serait très floue"
author: Thomas Giraudet
date: 2019-10-23
href: https://www.businessinsider.fr/le-senat-propose-une-loi-pour-contraindre-lactivite-des-gafa-mais-sa-mise-en-oeuvre-serait-tres-floue
featured_image: https://www.businessinsider.fr/content/uploads/2019/10/lutte-contrainte-785x588.jpg
tags:
- Institutions
series:
- 201943
---

> La sénatrice Sophie Primas (LR), présidente de la commission des affaires économiques de la chambre haute, vient de déposer une proposition de loi (PPL) pour réguler et contrôler l'activité des géants de la tech en France. Le texte donnerait notamment un pouvoir supplémentaire au régulateur des télécoms, l'Arcep, qui pourrait sanctionner jusqu'à 4 % du chiffre d'affaires mondial une entreprise en cas de récidive à un manquement. L'objectif final vise à garantir le libre choix du consommateur sur trois points : utiliser les applications sur son smartphone sans passer par les magasins d'applications d'Apple et Android; la possibilité de passer de Facebook à Twitter ou Instagram sans changer de plateforme et enfin le contrôle de rachats de startups. Le texte revêt avant tout une portée politique. Les géants du numérique, l'Arcep, l'Autorité de la concurrence n'ont d'ailleurs pas été rencontrés pour sa rédaction.
