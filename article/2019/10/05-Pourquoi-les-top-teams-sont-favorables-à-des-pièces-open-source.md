---
site: motorsport.com
title: "Pourquoi les top teams sont favorables à des pièces \"open source\""
author: Fabien Gaillard
date: 2019-10-05
href: https://fr.motorsport.com/f1/news/mercedes-ferrari-favorables-pieces-open-source/4552688
featured_image: https://cdn-1.motorsport.com/images/amp/6zQa4vlY/s6/the-cars-in-parc-ferme-after-t.jpg
tags:
- Matériel libre
- Standards
- Innovation
series:
- 201940
series_weight: 0
---

> Mercedes, Ferrari et Red Bull soutiennent l'idée des pièces open source en Formule 1, comme alternative à la standardisation souhaitée en 2021.
