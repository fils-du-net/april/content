---
site: Next INpact
title: "Publication du guide CNIL/CADA sur l'Open Data"
date: 2019-10-18
href: https://www.nextinpact.com/brief/publication-du-guide-cnil-cada-sur-l-open-data-10019.htm
featured_image: https://cdn2.nextinpact.com/images/bd/wide-linked-media/5002.jpg
tags:
- Open Data
series:
- 201942
---

> Après une phase de consultation organisée en début d'année, la Commission d'accès aux documents administratifs (CADA) et la Commission nationale de l'informatique et des libertés (CNIL) ont publié hier leur «guide pratique de la publication en ligne et de la réutilisation des données publiques».
