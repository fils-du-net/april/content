---
site: ZDNet France
title: "L'agence allemande de cybersécurité recommande Firefox"
author: Catalin Cimpanu
date: 2019-10-18
href: https://www.zdnet.fr/actualites/l-agence-allemande-de-cybersecurite-recommande-firefox-39892463.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/10/firefox.png
tags:
- Institutions
series:
- 201942
series_weight: 0
---

> Le BSI, équivalent allemand de l'Anssi, a testé Firefox, Chrome, IE et Edge. Firefox était le seul navigateur à satisfaire toutes les exigences minimales pour des fonctionnalités de sécurité obligatoires.
