---
site: Rude Baguette
title: "Dans la tempête, Qwant nomme DG Tristant Nitot, un ponte de l'open source"
date: 2019-10-01
href: https://www.rudebaguette.com/2019/10/qwant-moteur-recherche-tristant-nitot
featured_image: https://www.rudebaguette.com/wp-content/uploads/thumbs/qwant-moteur-recherche-tristant-nitot-Rude-Baguette-39etuayem85d87b6hxjgn4.png
tags:
- Entreprise
series:
- 201940
---

> Le moteur de recherche français Qwant vient d'annoncer que son nouveau DG est Tristant Nitot, un ancien de Mozilla, chante de l'open source.
