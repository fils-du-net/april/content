---
site: BFMtv
title: "Worteks, l'expertise en logiciels libres et open source"
date: 2019-10-17
href: https://bfmbusiness.bfmtv.com/le-tete-a-tete-decideurs/worteks-l-expertise-en-logiciels-libres-et-open-source-1788984.html
featured_image: https://img.bfmtv.com/c/1000/600/847/1b3599c9f3d042c9b5def4db69058.png
tags:
- Sensibilisation
- Entreprise
series:
- 201942
series_weight: 0
---

> L'open source et les logiciels libres sont des concepts dont le partage et la collaboration sont les principales bases. Laurent Marie, Président Fondateur de la société Worteks, œuvre dans l'open source, l'intégration, le support, l'accompagnement, le conseil et l'audit des clients qui ont décidé de faire confiance à ces concepts.
