---
site: Le Point
title: "Droits voisins: enquête «exploratoire» de l'Autorité de la concurrence"
date: 2019-10-03
href: https://www.lepoint.fr/economie/droits-voisins-enquete-exploratoire-de-l-autorite-de-la-concurrence-03-10-2019-2339208_28.php
featured_image: https://static.lpnt.fr/images/2019/10/03/19445108lpw-19445335-article-droits-voisins-google-internet-jpg_6552692_660x281.jpg
tags:
- Droit d'auteur
series:
- 201940
series_weight: 0
---

> L'enquête de l'autorité administrative indépendante vise les nouvelles règles qu'appliquera Google concernant la mise en œuvre des droits voisins.
