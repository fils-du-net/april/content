---
site: LeMagIT
title: "L’Open Source vit une période de «renaissance» selon Michael Howard (MariaDB)"
author: Gaétan Raoul,
date: 2019-10-23
href: https://www.lemagit.fr/tribune/LOpen-Source-vit-une-periode-de-renaissance-MariaDB
featured_image: https://cdn.ttgtmedia.com/visuals/ComputerWeekly/Hero%20Images/database-management-1-fotolia.jpg
tags:
- Sensibilisation
series:
- 201943
---

> Dans cet entretien réalisé à Paris, Michael Howard donne sa vision de la «renaissance de l’open source» provoquée par les géants du cloud tout en rappelant son peu d’estime pour certaines de leurs pratiques.
