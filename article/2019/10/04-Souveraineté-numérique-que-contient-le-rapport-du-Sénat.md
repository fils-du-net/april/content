---
site: Public Sénat
title: "Souveraineté numérique: que contient le rapport du Sénat?"
date: 2019-10-04
href: https://www.publicsenat.fr/article/parlementaire/souverainete-numerique-que-contient-le-rapport-du-senat-146031
featured_image: https://www.publicsenat.fr/sites/default/files/styles/pse_contenu_entete_16_9/public/thumbnails/image/ordinateur.jpg
tags:
- Institutions
series:
- 201940
series_weight: 0
---

> La commission d’enquête du Sénat sur la souveraineté numérique remettait son rapport ce jeudi. Elle livre une série de recommandations pour que l’État se dote d’une doctrine, notamment grâce à une loi d’orientation et de suivi de la défense du numérique.
