---
site: ZDNet France
title: "Justice: GNOME envoie un missile aux 'patent trolls'"
author: Campbell Kwan
date: 2019-10-22
href: https://www.zdnet.fr/actualites/justice-gnome-envoie-un-missile-aux-patent-trolls-39892627.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/09/patent_troll_620.jpg
seeAlso: "[Position sur les brevets logiciels](https://www.april.org/position-sur-les-brevets-logiciels)"
tags:
- Brevets logiciels
series:
- 201943
series_weight: 0
---

> La fondation a décidé de se battre contre ces sociétés qui pillent sa propriété intellectuelle, et de nombreuses autres. Elle ira en justice. Et compte bien coincer Rothschild Patent Imaging.
