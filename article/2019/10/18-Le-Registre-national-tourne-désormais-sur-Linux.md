---
site: Datanews.be
title: "Le Registre national tourne désormais sur Linux"
author: Pieterjan Van Leemputten
date: 2019-10-18
href: https://datanews.levif.be/ict/actualite/le-registre-national-tourne-desormais-sur-linux/article-news-1205079.html
featured_image: https://web.roularta.be/if/c_crop,w_600,h_486,x_0,y_0,g_center/c_fit,w_620,h_502/455fe87379322efde7439db866358c0f.jpg
tags:
- Institutions
- Administration
series:
- 201942
---

> Le Registre national vient de subir une importante modernisation. Le service prend en effet congé de son mainframe et tourne dorénavant sur une technologie open source. Cela ouvre la porte à de nouvelles applications et à une utilisation plus intensive.
