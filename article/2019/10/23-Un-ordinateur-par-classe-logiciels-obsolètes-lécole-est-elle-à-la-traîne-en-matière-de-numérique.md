---
site: Numerama
title: "Un ordinateur par classe, logiciels obsolètes: l'école est-elle à la traîne en matière de numérique?"
author: Perrine Signoret
date: 2019-10-23
href: https://www.numerama.com/politique/560981-un-ordinateur-par-classe-logiciels-obsoletes-lecole-est-elle-a-la-traine-en-matiere-de-numerique.html
featured_image: https://www.numerama.com/content/uploads/2019/10/ordinateur-ancien.jpg
tags:
- Éducation
series:
- 201943
series_weight: 0
---

> Il y a quelques années, le gouvernement annonçait le lancement d'un grand plan de modernisation du numérique dans les écoles primaires, collèges et lycée. Mais il reste encore beaucoup à améliorer.
