---
site: Cryptonaute 
title: "L'UNICEF lance un crypto-fonds pour financer des technologies open source - Cryptonaute"
author: Vivien Coronel
date: 2019-10-09
href: https://cryptonaute.fr/lunicef-lance-crypto-fonds-soutenir-technologies-open-source
featured_image: https://cryptonaute.fr/wp-content/uploads/2019/10/fsefefunicef-750x400.jpg
tags:
- Économie
series:
- 201941
series_weight: 0
---

> Le Fonds des Nations unies pour l’enfance a présenté l’«UNICEF Cryptocurrency Fund», un fonds alimenté par des dons en Bitcoin et Ethereum et qui investit dans des technologies open source bénéficiant aux enfants du monde entier.
