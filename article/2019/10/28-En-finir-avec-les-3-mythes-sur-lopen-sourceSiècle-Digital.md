---
site: Siècle Digital
title: "En finir avec les 3 mythes sur l'open source"
description: Dans cette tribune, Qunkai Liu, directeur général Alibaba Cloud France, Europe du Sud et Israël revient sur 3 grands mythes de l'Open Source.
author: Qunkai Liu
date: 2019-10-28
href: https://siecledigital.fr/2019/10/28/3-mythes-open-source
featured_image: https://thumbor.sd-cdn.fr/Q0RSmVDRvTjgL4VPP2i9jmv6kto=/fit-in/940x550/cdn.sd-cdn.fr/wp-content/uploads/2019/10/Mythe-open-source.jpg
tags:
- Sensibilisation
series:
- 201944
series_weight: 0
---

> Où en est l’Open Source? C’est la question que beaucoup se posent aujourd’hui, partagés entre les idées reçues et les déploiements éprouvés.
