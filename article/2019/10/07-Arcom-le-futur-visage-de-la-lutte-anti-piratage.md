---
site: Next INpact
title: "Arcom: le futur visage de la lutte anti-piratage (€)"
author: Marc Rees
date: 2019-10-07
href: https://www.nextinpact.com/news/108273-arcom-futur-visage-lutte-anti-piratage.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/23280.jpg
tags:
- HADOPI
series:
- 201941
---

> La future grande loi sur l’audiovisuel va consacrer le mariage entre la Hadopi et le CSA. À l’occasion de cette fusion, le gouvernement entend aiguiser les outils de lutte contre le piratage. Tour d’horizon actualisé des principales mesures.
