---
site: Next INpact
title: "Quand la France se lance dans la reconnaissance faciale (€)"
author: Pierre Januel
date: 2019-10-02
href: https://www.nextinpact.com/news/108256-quand-france-se-lance-dans-reconnaissance-faciale.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/21975.jpg
tags:
- Vie privée
series:
- 201940
---

> Si des expériences locales de reconnaissance faciale ont été fortement médiatisées, d’autres pratiques restent plus discrètes. En coulisses, les industriels poussent pour que la France ne soit pas à la traîne et l’Intérieur est sensible aux arguments. L’idée d’une loi pour encadrer les expérimentations progresse rapidement et selon nos informations, un texte pourrait être déposé dès cet automne. Enquête.
