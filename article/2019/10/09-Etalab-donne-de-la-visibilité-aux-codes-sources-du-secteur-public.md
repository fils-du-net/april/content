---
site: Silicon
title: "Etalab donne de la visibilité aux codes sources du secteur public"
author: Clément Bohic
date: 2019-10-09
href: https://www.silicon.fr/etalab-codes-sources-secteur-public-263325.html
featured_image: https://www.silicon.fr/wp-content/uploads/2019/10/etalab-code-source-secteur-public-684x513.jpg
tags:
- Open Data
series:
- 201941
---

> La mission Etalab lance un site qui répertorie une partie des codes sources ouverts par les organismes publics en France.
