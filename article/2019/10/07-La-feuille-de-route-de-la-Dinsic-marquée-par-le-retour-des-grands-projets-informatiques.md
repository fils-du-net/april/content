---
site: Banque des Territoires
title: "La feuille de route de la Dinsic marquée par le retour des grands projets informatiques"
date: 2019-10-07
href: https://www.banquedesterritoires.fr/la-feuille-de-route-de-la-dinsic-marquee-par-le-retour-des-grands-projets-informatiques
featured_image: https://www.banquedesterritoires.fr/sites/default/files/styles/landscape_auto_crop/public/2019-10/tech.gif
tags:
- Institutions
- Open Data
series:
- 201941
series_weight: 0
---

> Adieu l'État plateforme et l'open data, la feuille de route publiée par la Dinsic la semaine dernière marque un retour en force d'une approche informatique et centralisée de la transformation numérique de l'État. Pour l'heure, les collectivités semblent absentes des 'instances de gouvernance'.
