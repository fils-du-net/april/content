---
site: Developpez.com
title: "Les développeurs du projet GNU s'opposent au maintien du leadership de Richard M. Stallman,"
description: "Mais il reste à la tête du projet GNU et continuera à «travailler avec la FSF»"
author: Stan Adkens
href: https://open-source.developpez.com/actu/280622/Les-developpeurs-du-projet-GNU-s-opposent-au-maintien-du-leadership-de-Richard-M-Stallman-mais-il-reste-a-la-tete-du-projet-GNU-et-continuera-a-travailler-avec-la-FSF
featured_image: https://www.developpez.net/forums/attachments/p509205d1/a/a/a
tags:
- Promotion
series:
- 201941
series_weight: 0
---

> Richard Matthew Stallman (RMS) partira-t-il de la direction du projet GNU? C’est ce que souhaitent un groupe de développeurs GNU, même si l’initiateur du projet GNU semble vouloir conserver encore pour longtemps son rôle principal dans le projet. En septembre, le principal protagoniste du mouvement du logiciel libre RMS a démissionné de son poste de président de la Free Software Foundation (FSF) et de son conseil d’administration après avoir, un peu plus tôt dans la journée, quitté son poste au sein du CSAIL, le laboratoire d’informatique et d’intelligence artificielle du MIT.
