---
site: Next INpact
title: "Le Sénat adopte une batterie de mesures pour lutter contre l'obsolescence programmée (€)"
author: Xavier Berne
date: 2019-10-03
href: https://www.nextinpact.com/news/108257-le-senat-adopte-batterie-mesures-pour-lutter-contre-obsolescence-programmee.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/23884.jpg
tags:
- Institutions
- Innovation
series:
- 201940
---

> Initiation des collégiens à la réparation, introduction d’un «indice de durabilité» des produits, mises à jour obligatoires pendant dix ans pour les smartphones et tablettes, etc. Le Sénat a adopté de nombreuses mesures contre l’obsolescence programmée, la semaine dernière, dans le cadre du projet de loi «anti-gaspillage». Explications.
