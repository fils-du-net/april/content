---
site: usine-digitale.fr
title: "L'UNICEF lance un fonds en crypto-monnaies pour financer des technologies open-source"
author: Alice Vitard
date: 2019-10-10
href: https://www.usine-digitale.fr/article/l-unicef-lance-un-fonds-en-crypto-monnaies-pour-financer-des-technologies-open-source.N893169
featured_image: https://www.usine-digitale.fr/mediatheque/4/1/7/000810714_homePageUne/bitcoin.jpg
tags:
- Économie
series:
- 201941
---

> Le Fonds des Nations-Unies pour l'enfance a lancé son fonds en crypto-monnaies baptisé "UNICEF Crypto Fund", le 9 octobre 2019. Son objectif est de financer "des technologies open-source bénéficiant aux jeunes du monde entier" et plus largement de nourrir les recherches sur la blockchain.
