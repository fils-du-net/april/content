---
site: Le Monde.fr
title: "Algorithmes: «Nous devons créer les conditions d'une intelligence humaine responsable» (€)"
date: 2019-10-17
href: https://www.lemonde.fr/idees/article/2019/10/17/algorithmes-nous-devons-creer-les-conditions-d-une-intelligence-humaine-responsable_6015811_3232.html
featured_image: https://img.lemde.fr/2019/10/15/952/0/5669/2828/688/0/60/0/70b5271_WBXRv7IPvO0kQQZV1Tdx1gNP.jpg
tags:
- Innovation
series:
- 201942
series_weight: 0
---

> L'expert en numérique Guillaume Buffet estime, dans une tribune au «Monde» que l'urgence est de former les codeurs au politique et vice-versa, pour arriver à concilier éthique numérique et régulation.
