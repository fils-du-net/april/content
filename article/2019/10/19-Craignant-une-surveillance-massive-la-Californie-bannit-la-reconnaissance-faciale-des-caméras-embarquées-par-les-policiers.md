---
site: Sciencepost 
title: "Craignant une surveillance massive, la Californie bannit la reconnaissance faciale des caméras embarquées par les policiers"
author: Yohan Demeure
date: 2019-10-19
href: https://sciencepost.fr/craignant-une-surveillance-massive-la-californie-bannit-la-reconnaissance-faciale-des-cameras-embarquees-par-les-policiers
featured_image: https://sciencepost.fr/wp-content/uploads/2019/10/caméra-embarquée-facial-recognition.jpg
tags:
- Vie privée
series:
- 201942
---

> Il y a peu, la Californie a adopté une loi interdisant l’utilisation de la reconnaissance faciale via les caméras embarquées par les officiers de police. Selon le texte, cette technologie comporte des risques de surveillance massive.
