---
site: L'Informaticien
title: "L’Open Source entre dans la normalité"
date: 2019-10-29
href: https://www.linformaticien.com/dossiers/lopen-source-entre-dans-la-normalite.aspx
featured_image: https://www.linformaticien.com/Portals/0/2019/10_octobre/inf180_opensource_01.jpg
tags:
- Entreprise
- Sensibilisation
series:
- 201944
series_weight: 0
---

> L’Open Source a, sans conteste, de plus en plus le vent en poupe. Il n’est plus question de discuter de son utilité, mais plutôt, pour les entreprises, de voir si elles n’ont pas intérêt à y passer. L’Open Source entre dans la normalité, et deviendrait même le standard de fait.
