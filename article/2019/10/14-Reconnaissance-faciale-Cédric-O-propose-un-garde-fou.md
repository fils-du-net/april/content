---
site: Le Figaro.fr
title: "Reconnaissance faciale: Cédric O propose un garde-fou"
author: Samuel Kahn
date: 2019-10-14
href: https://www.lefigaro.fr/flash-eco/reconnaissance-faciale-cedric-o-propose-une-instance-de-supervision-20191014
featured_image: https://i.f1g.fr/media/cms/616x347_crop/2019/10/14/3e32085f0a2e22ff5e9452714113c25f584cba40f3abbe6fa856a658ed2a747f.png
tags:
- Vie privée
- Institutions
series:
- 201942
---

> Le secrétaire d'État au Numérique souhaite créer avec la Cnil une instance d'évaluation des projets de reconnaissance faciale en France.
