---
site: Siècle Digital
title: "Royaume-Uni: les données biométriques d'un million de personnes ont été exposées"
author: Valentin Cimino
date: 2019-10-27
href: https://siecledigital.fr/2019/10/27/royaume-uni-les-donnees-biometriques-dun-million-de-personnes-ont-ete-exposees
featured_image: https://cdn.sd-cdn.fr/wp-content/uploads/2019/10/8477734222_27bba43f0b_o.jpg
tags:
- Vie privée
series:
- 201943
series_weight: 0
---

> Au Royaume-Uni, les empreintes digitales et des données de reconnaissance faciale d'un million de personnes ont été exposées.
