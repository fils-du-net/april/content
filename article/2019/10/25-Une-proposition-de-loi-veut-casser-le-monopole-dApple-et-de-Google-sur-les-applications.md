---
site: Le Figaro.fr
title: "Une proposition de loi veut casser le monopole d'Apple et de Google sur les applications"
author: Laurent Pahla
date: 2019-10-25
href: https://www.lefigaro.fr/secteur/high-tech/une-proposition-de-loi-veut-casser-le-monopole-d-apple-et-de-google-sur-les-applications-20191022
featured_image: https://i.f1g.fr/media/eidos/616x347_crop/2019/10/22/XVM9af79bc2-f41b-11e9-96e8-30eb748539ad.jpg
tags:
- Institutions
series:
- 201943
---

> Le texte, proposé par des sénateurs, donnerait au régulateur des plus grandes capacités de sanction envers les géants du web.
