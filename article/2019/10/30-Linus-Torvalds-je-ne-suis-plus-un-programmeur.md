---
site: MacGeneration
title: "Linus Torvalds: «je ne suis plus un programmeur»"
date: 2019-10-30
href: https://www.macg.co/ailleurs/2019/10/linus-torvalds-je-ne-suis-plus-un-programmeur-109379
featured_image: https://cdn.mgig.fr/2019/10/mg-fa75eb79-6c1f-4cf1-a66a-w1000h666-sc.jpg
tags:
- Sensibilisation
series:
- 201944
series_weight: 0
---

> À quoi Linus Torvalds, créateur du noyau Linux et du logiciel de gestion de versions Git, occupe-t-il ses journées? À «lire des e-mails». À l'occasion de l'Open Source Summit Europe, le «dictateur bienveillant à vie» du projet Linux explique qu'il «ne code plus».
