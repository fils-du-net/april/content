---
site: Next INpact
title: "Vincent Strubel nous parle de la politique open source de l'ANSSI (€)"
author: Vincent Hermann
date: 2019-10-10
href: https://www.nextinpact.com/news/108288-vincent-strubel-nous-parle-politique-open-source-anssi.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/15254.jpg
tags:
- Institutions
series:
- 201941
---

> Dans le sillage de la libération du code de l'outil DFIR Orc, nous nous sommes entretenus avec l'un des sous-directeurs de l'agence française pour un point d'étape sur ses rapports avec l'open source.
