---
site: L'OBS
title: "Le mode d’emploi anti-Gafam des sénateurs"
date: 2019-10-03
author: Thierry Noisette
href: https://www.nouvelobs.com/economie/20191003.OBS19296/le-mode-d-emploi-anti-gafam-des-senateurs.html
featured_image: https://focus.nouvelobs.com/2019/10/03/85/0/1024/512/633/306/75/0/8e72f7d_91BXHXnvc7t2Hv7jnF5MR-Dq.jpg
tags:
- Institutions
series:
- 201940
---

> Face au poids des Gafam et de trois grandes puissances (Etats-Unis, Chine et Russie) aux valeurs différentes des nôtres, le Sénat prône une stratégie numérique plus active: droit de la concurrence renforcé, loi triennale, marchés publics…
