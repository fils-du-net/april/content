---
site: Le Monde Informatique
title: "Pierre Baudracco devient co-président du CNLL"
author: Véronique Arène
date: 2019-10-22
href: https://www.lemondeinformatique.fr/actualites/lire-pierre-baudracco-devient-co-president-du-cnll-76849.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000068912.jpg
tags:
- Entreprise
series:
- 201943
series_weight: 0
---

> Après quatre années passées à la co-présidence du CNLL, Philippe Montarges a cédé son siège. Les administrateurs ont élu Pierre Baudracco, président de l'éditeur de la solution de messagerie collaborative open source BlueMind pour prendre le relais. A cette occasion, des actions seront menées pour renforcer la dimension européenne de l'association.
