---
site: Siècle Digital
title: "France: la reconnaissance faciale bientôt imposée pour accéder aux services publics?"
description: La mise est en place de la reconnaissance faciale avec Alicem est prévue pour le mois de novembre
author: Geneviève Fournier
date: 2019-10-07
href: https://siecledigital.fr/2019/10/07/france-la-reconnaissance-faciale-bientot-imposee-pour-acceder-aux-services-publics
featured_image: https://thumbor.sd-cdn.fr/Higp6Fsv-dZLZnPi80k70g5584Q=/770x515/cdn.sd-cdn.fr/wp-content/uploads/2019/10/Alicem-la-premiere-solution-d-identite-numerique-regalienne-securisee_largeur_760-1.jpg
tags:
- Vie privée
- Institutions
- Associations
series:
- 201941
series_weight: 0
---

> D’après le journal Bloomberg, la France se prépare à devenir le premier pays européen à utiliser la reconnaissance faciale pour ses services publics. Il suffira pour cela de créer un compte Alicem depuis son smartphone. L’utilisation de cette application n’est pas obligatoire pour le moment, mais elle entre dans un projet plus large, qui est celui de la dématérialisation administrative globale d’ici 2022. Certains organismes ont exprimé leurs réticences face à cette décision, car les utilisateurs de l’application n’auront pas d’autre d’alternative possible que l’identification par reconnaissance faciale. Le libre consentement sera donc ignoré.
