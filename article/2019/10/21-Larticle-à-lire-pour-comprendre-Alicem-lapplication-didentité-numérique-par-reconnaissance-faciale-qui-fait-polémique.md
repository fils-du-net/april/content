---
site: francetv info
title: "L'article à lire pour comprendre Alicem, l'application d'identité numérique par reconnaissance faciale qui fait polémique"
author: Alice Galopin
date: 2019-10-21
href: https://www.francetvinfo.fr/sciences/high-tech/l-article-a-lire-pour-comprendre-alicem-l-application-d-identite-numerique-par-reconnaissance-faciale-qui-fait-polemique_3660027.html
featured_image: https://www.francetvinfo.fr/image/75nmiu6bf-5a59/1500/843/20263471.png
tags:
- Vie privée
series:
- 201943
series_weight: 0
---

> Alicem, l'application mobile lancée par l'Etat, doit faciliter l'accès aux services publics en ligne. En test depuis juin, son système de reconnaissance faciale suscite les craintes de l'émergence d'une "société de surveillance".
