---
site: Next INpact
title: "Richard Stallman: «pas de changements radicaux» à prévoir dans le projet GNU"
date: 2019-10-11
href: https://www.nextinpact.com/brief/richard-stallman-----pas-de-changements-radicaux---a-prevoir-dans-le-projet-gnu-9932.htm
featured_image: https://cdn2.nextinpact.com/images/bd/square-linked-media/16824.jpg
tags:
- Promotion
series:
- 201941
---

> Stallman, qui a démissionné de la Free Software Foundation (qu'il avait fondée) et du MIT à la suite de propos en marge de l'affaire Epstein, est toujours à la tête du projet GNU.
