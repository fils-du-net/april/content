---
site: ZDNet France
title: "La bataille entre vrai open source et faux open source s'intensifie"
author: Steven J. Vaughan-Nichols
date: 2019-02-21
href: https://www.zdnet.fr/actualites/la-bataille-entre-vrai-open-source-et-faux-open-source-s-intensifie-39881007.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/02/open-source-codes-620.jpg
tags:
- Licenses
- Entreprise
- Informatique en nuage
series:
- 201908
series_weight: 0
---

> L'open source tel que nous l'avons connu est-il désormais obsolète? Faut-il le remplacer? Certaines entreprises affirment que oui, alors que d'autres dénoncent un non-sens!
