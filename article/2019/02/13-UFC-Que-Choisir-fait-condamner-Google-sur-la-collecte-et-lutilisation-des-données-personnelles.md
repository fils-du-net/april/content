---
site: Numerama
title: "UFC-Que Choisir fait condamner Google sur la collecte et l'utilisation des données personnelles"
author: Maxime Claudel
date: 2019-02-13
href: https://www.numerama.com/tech/463837-ufc-que-choisir-fait-condamner-google-sur-la-collecte-et-lutilisation-des-donnees-personnelles.html
featured_image: https://www.numerama.com/content/uploads/2018/09/google-1.jpg
tags:
- Vie privée
- Entreprise
- Institutions
series:
- 201907
series_weight: 0
---

> Après avoir fait plier Twitter, UFC-Que Choisir continue son combat pour la protection des données personnelles.
