---
site: Le Telegramme
title: "Install Party. Créer sa liberté informatique"
date: 2019-02-17
href: https://www.letelegramme.fr/ille-et-vilaine/saint-malo/install-party-creer-sa-liberte-informatique-17-02-2019-12211019.php
tags:
- Associations
- Promotion
series:
- 201907
---

>  L’atelier de la Flibuste a réuni une quinzaine de personnes, samedi 16 février, à la médiathèque, pour une initiation au Raspberry Pi, une façon de faire ses premiers pas en programmation informatique. En parallèle, des bénévoles proposaient des alternatives libres et gratuites pour changer de système d’exploitation et tous les outils nécessaires aux activités numériques.
