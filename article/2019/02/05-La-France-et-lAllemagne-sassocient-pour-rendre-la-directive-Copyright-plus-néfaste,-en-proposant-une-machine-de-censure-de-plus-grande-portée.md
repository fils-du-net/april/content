---
site: Developpez.com
title: "La France et l'Allemagne s'associent pour rendre la directive Copyright plus néfaste"
description: "en proposant une machine de censure de plus grande portée"
author: Michael Guilloux
date: 2019-02-05
href: https://www.developpez.com/actu/244882/La-France-et-l-Allemagne-s-associent-pour-rendre-la-directive-Copyright-plus-nefaste-en-proposant-une-machine-de-censure-de-plus-grande-portee
featured_image: https://www.developpez.com/public/images/news/copyright-reloaded.PNG
tags:
- Droit d'auteur
- Institutions
- Europe
- Internet
series:
- 201906
series_weight: 0
---

> Comme nous l'avons rapporté le mois dernier, les négociations sur la réforme européenne sur le droit d'auteur ont été interrompues après que les gouvernements des États membres n'ont pas réussi à adopter une position commune sur l'article 13, qui vise à obliger les plateformes Internet à installer des machines de censure qui filtrent automatiquement les contenus mis en ligne par leurs utilisateurs.
