---
site: Next INpact
title: "Au Sénat, la Hadopi plaide pour une grande réforme de la lutte anti-piratage"
date: 2019-02-11
author: Marc Rees
href: https://www.nextinpact.com/news/107591-au-senat-hadopi-plaide-pour-grande-reforme-lutte-anti-piratage.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/2712.jpg
tags:
- HADOPI
- Institutions
- Internet
- Droit d'auteur
series:
- 201907
---

> À l’occasion d’une conférence au Sénat sur les stratégies internationales de lutte contre le piratage des contenus culturels et sportifs, Denis Rapone, président de la Hadopi, a rédigé sa liste de Noël à l’attention du législateur. En préparation de la future loi sur l’audiovisuel, il plaide pour une modernisation de ses moyens d’action.
