---
site: Contrepoints
title: "Si un logiciel est financé avec des fonds publics, son code source devrait être ouvert"
author: Glyn Moody
date: 2019-02-18
href: https://www.contrepoints.org/2019/02/18/337382-logiciel-est-finance-avec-des-fonds-publics-son-open-source
featured_image: https://www.contrepoints.org/wp-content/uploads/2019/02/Mario-and-Luigi-meet-linux-terminal-by-Jess-AlineCC-BY-NC-2.0--640x374.jpg
tags:
- Administration
- Interopérabilité
- Standards
- International
series:
- 201908
series_weight: 0
---

> Si nous payons pour le développement de logiciels libres, nous devons pouvoir nous en servir.
