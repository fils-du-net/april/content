---
site: Next INpact
title: "L'Assemblée rejette les amendements sur le logiciel libre à l'école"
author: Xavier Berne
date: 2019-02-18
href: https://www.nextinpact.com/news/107618-lassemblee-rejette-amendements-sur-logiciel-libre-a-ecole.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/22441.jpg
tags:
- Éducation
- april
- Institutions
series:
- 201908
---

> Suivant l’avis du gouvernement, les députés ont rejeté plusieurs amendements qui auraient contraint l’Éducation nationale à utiliser uniquement (ou même «en priorité») des logiciels libres. Et ce au grand dam de l'April, qui a dénoncé le «manque de volonté politique» de l'exécutif sur ce dossier.
