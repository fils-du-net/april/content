---
site: Numerama
title: "Une bonne raison de changer de navigateur? Firefox va empêcher la lecture automatique sur toutes les vidéos"
author: Julien Lausson
date: 2019-02-05
href: https://www.numerama.com/tech/460859-une-bonne-raison-de-changer-de-navigateur-firefox-va-empecher-la-lecture-automatique-sur-toutes-les-videos.html
featured_image: https://www.numerama.com/content/uploads/2019/02/firefox.jpg
tags:
- Internet
series:
- 201906
---

> Une prochaine version de Firefox prévoit de bloquer la lecture automatique des vidéos qui ont du son.
