---
site: Next INpact
title: "Droit d'auteur: ce que prévoit l'article 13 en fin de négociations européennes"
author: Marc Rees
date: 2019-02-15
href: https://www.nextinpact.com/news/107599-droit-dauteur-ce-que-prevoit-larticle13-en-fin-negociations-europeennes.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/16183.jpg
tags:
- Droit d'auteur
- Internet
- Institutions
- Europe
- Internet
series:
- 201907
---

> Le projet de directive sur le droit d’auteur touche à sa phase finale après accord entre les institutions européennes. Pilier de ce dispositif, l’article 13 est socle de toutes les critiques, de toutes les envies. Que prévoit réellement cette disposition? Next INpact vous propose une explication détaillée, avant passage en revue des autres dispositions.
