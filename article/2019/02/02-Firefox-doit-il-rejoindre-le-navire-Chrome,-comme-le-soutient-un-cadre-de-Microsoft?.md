---
site: ZDNet France
title: "Firefox doit-il rejoindre le navire Chrome, comme le soutient un cadre de Microsoft?"
author: Stephen Shankland
date: 2019-02-02
href: https://www.zdnet.fr/actualites/firefox-doit-il-rejoindre-le-navire-chrome-comme-le-soutient-un-cadre-de-microsoft-39879925.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/01/mozilla-firefox-620.jpg
tags:
- Internet
- Entreprise
- Standards
series:
- 201905
series_weight: 0
---

> Un responsable de Microsoft, Kenneth Auchenberg, estime sur Twitter que Mozilla devrait à son tour adopter Chromium pour son navigateur Firefox. Est-ce réellement la seule alternative voire même une véritable option pour Firefox?
