---
site: Le Telegramme
title: "Logiciels libres. Des participants avides de connaissances"
date: 2019-02-17
href: https://www.letelegramme.fr/finistere/quemeneven/logiciels-libres-des-participants-avides-de-connaissances-17-02-2019-12211131.php
featured_image: https://www.letelegramme.fr/images/2019/02/17/logiciels-libres-des-participants-avides-de-connaissances_4422984_540x310p.jpg
tags:
- Associations
- Promotion
series:
- 201907
series_weight: 0
---

>  La bibliothèque municipale propose, jusqu'au 15 mars, une exposition sur les logiciels libres. Vendredi, Sylvie Péron, de l'association Les ordis libres, était dans les locaux pour présenter...
