---
site: Le Monde.fr
title: "Parcoursup: la justice enjoint à une université de publier son algorithme de tri"
date: 2019-02-06
href: https://www.lemonde.fr/campus/article/2019/02/06/parcoursup-la-justice-enjoint-une-universite-a-publier-son-algorithme-de-tri_5419762_4401467.html
tags:
- Éducation
- Institutions
- Partage du savoir
series:
- 201906
series_weight: 0
---

> Selon un jugement du tribunal administratif de la Guadeloupe que «Le Monde» s'est procuré, l'université des Antilles doit communiquer le détail de ses critères de classement des candidats. Une première.
