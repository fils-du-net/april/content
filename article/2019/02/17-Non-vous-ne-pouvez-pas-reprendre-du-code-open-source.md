---
site: ZDNet France
title: "Non, vous ne pouvez pas reprendre du code open source"
author: Steven J. Vaughan-Nichols
date: 2019-02-17
href: https://www.zdnet.fr/actualites/non-vous-ne-pouvez-pas-reprendre-du-code-open-source-39880565.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/02/open-source-code-620.jpg
tags:
- Licenses
- Droit d'auteur
series:
- 201907
series_weight: 0
---

> Une note populaire sur la liste de diffusion du noyau Linux affirme que l'auteur d'un programme peut empêcher des tiers d'utiliser son code selon son bon vouloir. C'est faux.
