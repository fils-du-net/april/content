---
site: Le Monde.fr
title: "Grand débat en ligne et démocratie: l'analyse et la transparence des données en question"
date: 2019-02-01
href: https://www.lemonde.fr/pixels/article/2019/02/01/grand-debat-en-ligne-et-democratie-l-analyse-et-la-transparence-des-donnees-en-question_5417911_4408996.html
featured_image: https://img.lemde.fr/2019/02/01/0/0/1062/518/688/0/60/0/db53e65_OQdY0aenkM1xJyaof6ZAdo2d.JPG
tags:
- Institutions
- Open Data
- Associations
series:
- 201905
series_weight: 0
---

> Cette consultation des Français met en lumière la nécessité de règles de gouvernance communes et validées par des chercheurs en matière de démocratie en ligne.
