---
site: Next INpact
title: "Article 13 de la directive droit d'auteur: quand la France milite pour un filtrage généralisé"
date: 2019-02-11
href: https://www.nextinpact.com/news/107596-article-13-directive-droit-dauteur-quand-france-milite-pour-filtrage-generalise.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/6129.jpg
tags:
- Droit d'auteur
- Internet
- Institutions
- Europe
series:
- 201907
---

> Dans le cadre actuel des négociations autour de la proposition de directive sur le droit d'auteur, et en particulier l’article 13, la France plaide pour un sévère tour de vis. En jeu ? Tout simplement un filtrage généralisé des contenus culturels.
