---
site: Next INpact
title: "Loi contre la haine: quand le gouvernement rêve d'hygiène sur les réseaux sociaux"
author: Marc Rees
date: 2019-02-18
href: https://www.nextinpact.com/news/107620-loi-contre-haine-quand-gouvernement-reve-dhygiene-sur-reseaux-sociaux.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/19153.jpg
tags:
- Internet
- Institutions
- Vie privée
series:
- 201908
---

> La loi de modération des contenus haineux est le prochain grand texte de régulation des contenus en ligne. Le projet sera présenté au deuxième trimestre 2019. Dans une interview accordée au Figaro et un post sur le site américain Medium, Mounir Mahjoubi en a détaillé les grandes lignes.
