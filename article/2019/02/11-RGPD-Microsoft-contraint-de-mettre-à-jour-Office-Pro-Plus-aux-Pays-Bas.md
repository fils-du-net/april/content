---
site: Silicon
title: "RGPD: Microsoft contraint de mettre à jour Office Pro Plus aux Pays-Bas"
date: 2019-02-11
href: https://www.silicon.fr/rgpd-microsoft-contraint-mettre-a-jour-office-pro-plus-232229.html
featured_image: https://www.silicon.fr/wp-content/uploads/2018/05/shutterstock_338610344-684x513.jpg
tags:
- Vie privée
- Institutions
- Entreprise
- Logiciels privateurs
- International
series:
- 201907
---

> Après un audit concluant à des problèmes de confidentialité des données, Microsoft est contraint de mettre à jour Office Pro Plus avant la fin avril.
