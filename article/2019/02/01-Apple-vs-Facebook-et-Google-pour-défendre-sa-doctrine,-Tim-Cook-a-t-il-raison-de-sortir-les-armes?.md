---
site: Numerama
title: "Apple vs Facebook et Google: pour défendre sa doctrine, Tim Cook a-t-il raison de sortir les armes?"
author: Victoria Castro
date: 2019-02-01
href: https://www.numerama.com/politique/459847-apple-vs-facebook-et-google-pour-defendre-sa-doctrine-tim-cook-avait-il-raison-de-sortir-les-armes.html
featured_image: https://www.numerama.com/content/uploads/2018/04/tim-cook-apple-e1542637663270.jpg
tags:
- Entreprise
- Vie privée
- Logiciels privateurs
series:
- 201905
series_weight: 0
---

> La guerre froide entre Apple, Google et Facebook sur la vie privée a franchi un cap. En désactivant à distance les applications internes de Facebook et Google, Apple joue un difficile équilibre, digne de l'époque de la dissuasion nucléaire.
