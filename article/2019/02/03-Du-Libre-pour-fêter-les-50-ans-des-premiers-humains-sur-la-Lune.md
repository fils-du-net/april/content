---
site: ZDNet France
title: "Du Libre pour fêter les 50 ans des premiers humains sur la Lune"
author: Thierry Noisette
date: 2019-02-03
href: https://www.zdnet.fr/blogs/l-esprit-libre/du-libre-pour-feter-les-50-ans-des-premiers-humains-sur-la-lune-39879993.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2019/01/NASA-Apollo8-Dec24-Earthrise_02.jpg
tags:
- Promotion
- Sciences
series:
- 201905
series_weight: 0
---

> Pour célébrer les 50 ans d'Apollo 11 sur la Lune, des logiciels libres d'astronomie et des images libres.
