---
site: Next INpact
title: "CopyComic: Twitter réactive les deux vidéos anti-plagiats dénoncées par Gad Elmaleh"
author: Marc Rees
date: 2019-02-23
href: https://www.nextinpact.com/news/107649-copycomic-twitter-reactive-deux-videos-anti-plagiats-denoncees-par-gad-elmaleh.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/22464.jpg
tags:
- Droit d'auteur
- Internet
series:
- 201908
---

> Twitter a finalement réactivé les deux tweets de @CopyComicVideos dénoncés par Gad Elmaleh. Selon nos informations, la décision fait suite à un réexamen des demandes de retrait. Dans ces vidéos, l'humoriste est épinglé pour plagiat de plusieurs comiques, extraits à l'appui.
