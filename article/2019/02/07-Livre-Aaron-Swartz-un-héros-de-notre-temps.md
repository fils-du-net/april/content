---
site: Le Point
title: "Livre: Aaron Swartz, un héros de notre temps"
date: 2019-02-07
href: https://www.lepoint.fr/culture/livre-aaron-swartz-un-heros-de-notre-temps-07-02-2019-2292039_3.php
featured_image: https://www.lepoint.fr/img-l/articles/2292039.png
featured_image: https://www.lepoint.fr/images/2019/02/07/18035703lpw-18081667-libre-jpg_5946403.jpg
tags:
- Internet
- Partage du savoir
series:
- 201906
---

> Dans un récit ardent, «Ce qu'il reste de nos rêves», Flore Vasseur ressuscite l'ange déchu de l'Internet libre, qui s'est suicidé à l'âge de 26 ans.
