---
site: France Culture
title: "Vasarely: vendre aux riches pour donner aux pauvres"
author: Mathilde Serrell
date: 2019-02-05
href: https://www.franceculture.fr/emissions/le-billet-culturel/le-billet-culturel-du-mardi-05-fevrier-2019
featured_image: https://cdn.radiofrance.fr/s3/cruiser-production/2019/02/a530e67f-e7f1-4b86-8761-e85cef590274/838_gettyimages-1039932698.jpg
tags:
- Partage du savoir
- Droit d'auteur
series:
- 201906
series_weight: 0
---

> La grande exposition qui s'ouvre au Centre Pompidou est non seulement la première en France depuis 50 ans mais aussi la première tout court. L'occasion de déchiffrer un message radical.
