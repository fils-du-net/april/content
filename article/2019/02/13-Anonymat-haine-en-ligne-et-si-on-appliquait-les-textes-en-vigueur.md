---
site: Next INpact
title: "«Anonymat», haine en ligne… et si on appliquait les textes en vigueur?"
author: Marc Rees
date: 2019-02-13
href: https://www.nextinpact.com/news/107601-anonymat-haine-en-ligne-et-si-on-appliquait-textes-en-vigueur.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/5713.jpg
tags:
- Internet
- Vie privée
- Institutions
series:
- 201907
---

> Édouard Philippe a annoncé hier qu’un texte de loi «sera présenté avant l’été» pour lutter contre les contenus haineux en ligne. L’idée? «Responsabiliser ceux qui n’ont pas le droit de dire qu’ils sont responsables de rien de ce qui est publié». Une excellente occasion de relire le droit en vigueur.
