---
site: 01net.
title: "Adoptez un Chatons et offrez-vous le cloud que vous méritez"
author: Marion Simon-Rainaud
date: 2019-02-19
href: https://www.01net.com/actualites/adoptez-un-chatons-et-offrez-vous-le-cloud-que-vous-meritez-1619018.html
featured_image: https://img.bfmtv.com/c/630/420/9a9/e4a5088deffefe29d79168a56afcd.PNG
tags:
- Informatique en nuage
- Associations
- Internet
series:
- 201908
series_weight: 0
---

> Grand zélateur du logiciel libre en France, l'association Framasoft est à l'origine d'un «Collectif d'Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires», les Chatons, qui se déploie sur tout le territoire.
