---
site: InformatiqueNews.fr
title: "L'Unesco publie l'appel de Paris sur le code source"
date: 2019-02-27
href: https://www.informatiquenews.fr/lunesco-publie-lappel-de-paris-sur-le-code-source-60549
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2019/02/27-Unesco-Une.jpg
tags:
- Partage du savoir
- Institutions
- Promotion
series:
- 201909
series_weight: 0
---

> L’Unesco, en partenariat avec Inria et Software Heritage, publie l’Appel de Paris, pour sensibiliser à la préservation et à l’accès au code source des logiciels.
