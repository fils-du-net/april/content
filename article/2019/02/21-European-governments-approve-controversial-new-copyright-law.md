---
site: Ars Technica
title: "European governments approve controversial new copyright law"
author: Timothy B. Lee
date: 2019-02-21
href: https://arstechnica.com/tech-policy/2019/02/european-governments-approve-controversial-new-copyright-law/
featured_image: https://cdn.arstechnica.net/wp-content/uploads/2011/11/european_parliament-4ecb0de-intro.jpg
tags:
- Droit d'auteur
- Internet
- Institutions
- Europe
- English
series:
- 201908
---

> Copyright overhaul could effectively mandate automated content filtering.
