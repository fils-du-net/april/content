---
site: Developpez.com
title: "Les États membres de l'UE approuvent la version finale de la directive Copyright"
description: "Il ne reste plus que la décision du Parlement"
author: Michael Guilloux
date: 2019-02-21
href: https://www.developpez.com/actu/248017/Les-Etats-membres-de-l-UE-approuvent-la-version-finale-de-la-directive-Copyright-il-ne-reste-plus-que-la-decision-du-Parlement
featured_image: https://www.developpez.com/public/images/news/eu-copy.PNG
seeAlso: "[Directive droit d'auteur: le Parlement européen sera la dernière ligne de défense contre la censure automatisée](https://www.april.org/directive-droit-d-auteur-le-parlement-europeen-sera-la-derniere-ligne-de-defense-contre-la-censure-a)"
tags:
- Droit d'auteur
- Internet
- Institutions
- Europe
series:
- 201908
series_weight: 0
---

> Le Conseil de l'UE, qui représente les gouvernements des États membres, a adopté hier la version finale de la réforme sur le droit d'auteur. Le projet, qui a connu un long parcours après avoir été proposé par la Commission européenne en 2016, arrive donc de manière imminente à son terme, avec de grandes chances d'être érigé en loi.
