---
site: Copybuzz
title: "Article 13 is Not Just Criminally Irresponsible, It's Irresponsibly Criminal"
date: 2019-02-12
href: https://copybuzz.com/copyright/article-13-is-not-just-criminally-irresponsible-its-irresponsibly-criminal/
featured_image: https://copybuzz.com/wp-content/uploads/2019/02/ransom-note-1050x713.jpg
tags:
- Droit d'auteur
- Internet
- Europe
- English
series:
- 201907
---

> In a previous editorial, I pointed out that at the heart of Article 13 in the proposed EU Copyright Directive there’s a great lie: that it is possible to check for unauthorised uploads of material without inspecting every single file. The EU has ended up in this absurd position because it knows that many MEPs would reject the idea of imposing a general monitoring obligation on online services – not least because the e-Commerce Directive explicitly forbids it. Instead, the text of Article 13 simply pretends that technical alternatives can be found, without specifying them. The recently-issued “Q and A on the draft digital copyright directive” from the European Parliament then goes on to explain that if services aren’t clever enough to come up with other ways, and use upload filters, then obviously it’s their own fault.
