---
site: Silicon
title: "Open Source: Linux domine le top 5 des compétences les plus prisées"
author: Ariane Beky
date: 2019-02-12
href: https://www.silicon.fr/open-source-linux-top-5-competences-232447.html
featured_image: https://www.silicon.fr/wp-content/uploads/2019/02/open-source-obscur-684x443-684x443.jpg
tags:
- Innovation
- Entreprise
- Informatique en nuage
series:
- 201907
series_weight: 0
---

> Linux devance le cloud et la sécurité au top des domaines de compétences les plus demandés par les recruteurs de profils open source.
