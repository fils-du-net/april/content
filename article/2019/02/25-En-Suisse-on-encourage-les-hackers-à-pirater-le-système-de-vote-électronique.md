---
site: Le Monde.fr
title: "En Suisse, on encourage les hackers à pirater le système de vote électronique"
author: Simon Auffret
date: 2019-02-25
href: https://www.lemonde.fr/pixels/article/2019/02/25/les-hackers-invites-a-pirater-le-systeme-de-vote-electronique-suisse_5428208_4408996.html
featured_image: https://img.lemde.fr/2015/10/18/0/0/4517/3011/688/0/60/0/e7980da_5547444-01-06.jpg
tags:
- Vote électronique
- Institutions
- International
- Logiciels privateurs
series:
- 201909
series_weight: 0
---

> Pour rassurer ceux qui critiquent la fiabilité de ce dispositif, les autorités font appel à des hackers pour trouver la moindre faille.
