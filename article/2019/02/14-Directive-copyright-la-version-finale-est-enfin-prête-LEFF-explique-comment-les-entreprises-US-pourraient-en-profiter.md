---
site: Developpez.com
title: "Directive copyright: la version finale est enfin prête. L'EFF explique comment les entreprises US pourraient en profiter"
description: "Pour écraser la concurrence"
author: Stéphane le calme
date: 2019-02-14
href: https://www.developpez.com/actu/246187/Directive-copyright-la-version-finale-est-enfin-prete-L-EFF-explique-comment-les-entreprises-US-pourraient-en-profiter-pour-ecraser-la-concurrence
featured_image: https://www.developpez.net/forums/attachments/p449902d1/a/a/a
seeAlso: "[Directive droit d'auteur: le Parlement européen sera la dernière ligne de défense contre la censure automatisée](https://www.april.org/directive-droit-d-auteur-le-parlement-europeen-sera-la-derniere-ligne-de-defense-contre-la-censure-a)"
tags:
- Droit d'auteur
- Internet
- Institutions
- Europe
series:
- 201907
series_weight: 0
---

> L'Union européenne est sur le point de parvenir à réécrire ses règles de copyright datant de deux décennies, ce qui obligera Google et Facebook inc. d'Alphabet inc. à partager leurs revenus avec les industries de la création et à supprimer le contenu protégé par copyright sur YouTube ou Instagram.Les négociateurs des pays de l'UE, le Parlement européen et la Commission européenne ont conclu un accord après des négociations d'une journée.
