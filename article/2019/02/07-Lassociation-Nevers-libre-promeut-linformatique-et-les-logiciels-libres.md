---
site: Le Journal du Centre
title: "L'association Nevers libre promeut l'informatique et les logiciels libres"
author: Gwénola-Champalaune
date: 2019-02-07
href: https://www.lejdc.fr/nevers/economie/innovation/2019/02/07/l-association-nevers-libre-promeut-l-informatique-et-les-logiciels-libres_13126339.html
featured_image: https://image1.lejdc.fr/photoSRC/VVNUJ1paUTgIBhVOCRAHHQ4zRSkXaldfVR5dW1sXVA49/nevers-libre_4197316.jpeg
tags:
- Associations
- Promotion
series:
- 201906
series_weight: 0
---

> Fondée en 2016, l'association Nevers libre, qui veut donner la voix à une informatique alternative et éthique face aux grosses machines de logiciels, revient sur le devant de la scène.
