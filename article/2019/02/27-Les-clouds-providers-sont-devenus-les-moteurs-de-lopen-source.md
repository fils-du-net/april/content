---
site: Le Monde Informatique
title: "Les clouds providers sont devenus les moteurs de l'open source"
author: Matt Asay
date: 2019-02-27
href: https://www.lemondeinformatique.fr/actualites/lire-les-clouds-providers%C2%A0sont-devenus-les-moteurs-de-l-open-source-74479.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000065523.jpg
tags:
- Informatique en nuage
- Entreprise
series:
- 201909
series_weight: 0
---

> Open Source: Non, ces «diaboliques» entreprises du cloud ne nuisent pas à l'open source: elles en sont les plus gros contributeurs.
