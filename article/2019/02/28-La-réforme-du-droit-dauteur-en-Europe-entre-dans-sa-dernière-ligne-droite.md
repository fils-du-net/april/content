---
site: Numerama
title: "La réforme du droit d'auteur en Europe entre dans sa dernière ligne droite"
author: Julien Lausson
date: 2019-02-28
href: https://www.numerama.com/politique/468131-la-reforme-du-droit-dauteur-en-europe-entre-dans-sa-derniere-ligne-droite.html
featured_image: https://www.numerama.com/content/uploads/2018/06/eu-copyright.png
seeAlso: "[Directive droit d'auteur: le Parlement européen sera la dernière ligne de défense contre la censure automatisée](https://www.april.org/directive-droit-d-auteur-le-parlement-europeen-sera-la-derniere-ligne-de-defense-contre-la-censure-a)"
tags:
- Droit d'auteur
- Europe
- Institutions
series:
- 201909
series_weight: 0
---

> Le projet de réforme de directive du droit d'auteur en Europe a franchi une étape-clé fin février, avec l'approbation d'une commission au Parlement européen.
