---
site: 24 heures
title: "Achats publics: L'informatique de l'État jugée pas assez écolo par les Verts"
author: Lise Bourgeois
date: 2019-02-10
href: https://www.24heures.ch/vaud-regions/informatique-letat-jugee-ecolo-verts/story/21205915
featured_image: https://files.newsnetz.ch/story/2/1/2/21205915/4/topelement.jpg
tags:
- Administration
- Institutions
series:
- 201906
series_weight: 0
---

> Dans le sillage du combat de l'ancien magistrat François Marthaler, une élue Verte questionne les choix du Canton pour le numérique.
