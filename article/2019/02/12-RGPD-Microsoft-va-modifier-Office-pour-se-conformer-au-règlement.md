---
site: ZDNet France
title: "RGPD: Microsoft va modifier Office pour se conformer au règlement"
date: 2019-02-12
href: https://www.zdnet.fr/actualites/rgpd-microsoft-va-modifier-office-pour-se-conformer-au-reglement-39880457.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2016/01/detective-windows-102.jpg
tags:
- Vie privée
- Institutions
- Entreprise
- Logiciels privateurs
- International
series:
- 201907
---

> Microsoft modifiera Office ProPlus d'ici fin avril pour répondre aux préoccupations du ministère néerlandais en matière de protection de la vie privée.
