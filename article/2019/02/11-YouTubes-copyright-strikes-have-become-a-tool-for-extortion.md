---
site: The Verge
title: "YouTube's copyright strikes have become a tool for extortion"
author: Shoshana Wodinsky
date: 2019-02-11
href: https://www.theverge.com/2019/2/11/18220032/youtube-copystrike-blackmail-three-strikes-copyright-violation
featured_image: https://cdn.vox-cdn.com/thumbor/OhWqvAAJteDQrWQXMfJs0uuOH3I=/0x0:2040x1360/1520x1013/filters:focal(857x517:1183x843):format(webp)/cdn.vox-cdn.com/uploads/chorus_image/image/63033457/acastro_180403_1777_youtube_0002.0.jpg
tags:
- Droit d'auteur
- Internet
- Entreprise
- English
series:
- 201907
---

> YouTube's three-strikes copyright system lets anyone report a content violation — known in the community as 'copystriking' — but some users are putting that system to use for blackmail.
