---
site: LeMagIT
title: "Open Core: Redis Labs en finit avec Apache et crée sa propre licence…non open source"
author: Cyrille Chausson
date: 2019-02-26
href: https://www.lemagit.fr/actualites/252458345/Open-Core-Redis-Labs-en-finit-avec-Apache-et-cree-sa-propre-licencenon-open-source
featured_image: https://cdn.ttgtmedia.com/visuals/ComputerWeekly/Hero%20Images/open-source-fotolia.jpg
tags:
- Licenses
- Informatique en nuage
- Entreprise
- Économie
series:
- 201909
series_weight: 0
---

> La licence RSLA, qui n’est pas une licence open source, encadrera désormais les modules premium de Redis Labs afin de clarifier les limites trop confuses des Commons Clauses. Le cœur open source Redis reste sous une licence BSD 3.
