---
site: Developpez.com
title: "GNU/Linux: après plus de 25 ans d'existence, retour sur l'histoire mouvementée de l'OS libre"
author: Coriolan
date: 2019-02-05
href: https://www.developpez.com/actu/244826/GNU-Linux-apres-plus-de-25-ans-d-existence-retour-sur-l-histoire-mouvementee-de-l-OS-libre
featured_image: https://www.developpez.net/forums/attachments/p447467d1/a/a/a
tags:
- Entreprise
- Administration
- Informatique en nuage
- Europe
series:
- 201906
series_weight: 0
---

> Linux est, au sens restreint, le noyau de système d'exploitation Linux, et au sens large, tout système d'exploitation fondé sur le noyau Linux. Créé en 1991 par Linus Torvalds, c'est un logiciel libre destiné en premier lieu pour les ordinateurs personnels compatibles PC, qui avec des logiciels GNU devait constituer un système d'exploitation à part entière.En termes de parts de marché des OS, Linux peine à s'imposer. Selon les derniers chiffres de Statcounter et Net Applications, Linux contrôle ...
