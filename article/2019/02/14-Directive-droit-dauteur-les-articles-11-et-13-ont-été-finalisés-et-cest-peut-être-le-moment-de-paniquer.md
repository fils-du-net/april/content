---
site: Numerama
title: "Directive droit d'auteur: les articles 11 et 13 ont été finalisés et c'est peut-être le moment de paniquer"
author: Perrine Signoret
date: 2019-02-14
href: https://www.numerama.com/politique/464025-directive-droit-dauteur-les-articles-11-et-13-ont-ete-valides-et-cest-peut-etre-le-moment-de-paniquer.html
featured_image: https://www.numerama.com/content/uploads/2019/02/article-13-panique.jpg
tags:
- Droit d'auteur
- Internet
- Institutions
- Europe
series:
- 201907
---

> Les articles 11 et 13 de la directive européenne sur les droits d'auteur ont été finalisés. Ils avaient été beaucoup critiqués, mais leur contenu ne change finalement pas.
