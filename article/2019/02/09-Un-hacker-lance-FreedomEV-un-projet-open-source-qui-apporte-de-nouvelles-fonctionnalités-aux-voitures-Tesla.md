---
site: Developpez.com
title: "Un hacker lance FreedomEV, un projet open source qui apporte de nouvelles fonctionnalités aux voitures Tesla"
author: Bill Fassinou
date: 2019-02-09
href: https://www.developpez.com/actu/245614/Un-hacker-lance-FreedomEV-un-projet-open-source-qui-apporte-de-nouvelles-fonctionnalites-aux-voitures-Tesla-dont-un-mode-de-confidentialite
featured_image: http://www.freedomev.com/wiki/resources/assets/freedomev-splash-150.jpg
tags:
- Innovation
- Vie privée
series:
- 201907
series_weight: 0
---

> Jasper Nuyens, celui qu'on surnomme avec un groupe de hackers “les pirates Tesla” a profité de l'événement FOSDEM de ce cette année pour présenter son projet de libération, dit-il, de la pleine puissance des voitures Tesla. Le FOSDEM est un événement gratuit qui permet aux développeurs de logiciels de se rencontrer, d'échanger des idées et de collaborer. Il regroupe ainsi chaque année à Bruxelles, des milliers de développeurs de logiciels libres et open source du monde entier. Jasper Nuyens est le directeur général de Linux Belgium, une entreprise qui fournit des services de conseil, de formation et de support Linux à d’autres entreprises utilisant Linux de manière professionnelle dans le contexte de serveurs et de Linux embarqué.
