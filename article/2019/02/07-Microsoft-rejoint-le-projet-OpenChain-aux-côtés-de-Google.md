---
site: Developpez.com
title: "Microsoft rejoint le projet OpenChain aux côtés de Google"
description: "pour participer à la définition de normes pour la conformité des logiciels open source"
author: Stéphane le calme
date: 2019-02-07
href: https://www.developpez.com/actu/245264/Microsoft-rejoint-le-projet-OpenChain-aux-cotes-de-Google-pour-participer-a-la-definition-de-normes-pour-la-conformite-des-logiciels-open-source
featured_image: https://www.developpez.net/forums/attachments/p448194d1/a/a/a
tags:
- Licenses
- Entreprise
series:
- 201906
series_weight: 0
---

> Le projet OpenChain, qui renforce la confiance dans l'open source en rendant la conformité de licence open source plus simple et plus cohérente, a annoncé que Microsoft avait rejoint le groupe en tant que membre platine. Cela fait suite à plusieurs autres grandes entreprises qui ont rejoint OpenChain le mois dernier, notamment Uber, Google et Facebook.
