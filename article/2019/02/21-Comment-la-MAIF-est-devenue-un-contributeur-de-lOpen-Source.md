---
site: LeMagIT
title: "Comment la MAIF est devenue un contributeur de l'Open Source"
author: Alain Clapaud
date: 2019-02-21
href: https://www.lemagit.fr/etude/Comment-la-MAIF-est-devenue-contributeur-Open-Source
featured_image: https://cdn.ttgtmedia.com/visuals/LeMagIT/hero_article/MAIF02.jpg
tags:
- Entreprise
- Innovation
series:
- 201908
series_weight: 0
---

> Comme dans beaucoup d’entreprises françaises, l’open source s’est taillé une place de choix dans le système d’information, mais la MAIF est allée plus loin. Celle-ci publie désormais ses propres projets au format ouvert.
