---
site: Les Echos
title: "Données personnelles: Cisco rejoint Apple dans sa lutte pour une loi aux Etats-Unis"
author: Lucas Mediavilla   
date: 2019-02-04
href: https://www.lesechos.fr/tech-medias/hightech/0600632675887-donnees-personnelles-cisco-rejoint-apple-dans-sa-lutte-pour-une-loi-aux-etats-unis-2241843.php
featured_image: https://www.lesechos.fr/medias/2019/02/04/2241843_donnees-personnelles-cisco-rejoint-apple-dans-sa-lutte-pour-une-loi-aux-etats-unis-web-tete-060632844260.jpg
tags:
- Vie privée
- Entreprise
- Internet
series:
- 201906
series_weight: 0
---

> Le «plombier du Net» appelle à son tour à la mise en place d'un RGPD d'initiative américaine. Les soutiens à la mesure se multiplient outre-Atlantique.
