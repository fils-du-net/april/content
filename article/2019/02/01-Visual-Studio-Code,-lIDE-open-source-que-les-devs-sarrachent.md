---
site: Journal du Net
title: "Visual Studio Code, l'IDE open source que les devs s'arrachent"
author: Antoine Crochet-Damais
date: 2019-02-01
href: https://www.journaldunet.com/web-tech/developpeur/1204957-visual-studio-code-l-ide-open-source-que-les-devs-s-arrachent/
featured_image: https://img-0.journaldunet.com/tVE-iKZZEtf6uCv5REjwe_b_86E=/540x/smart/039cacf4c630461fb31e18cf09c7fde3/ccmcms-jdn/10668815.jpg
tags:
- Entreprise
series:
- 201905
---

> C'est le projet affichant le plus grand nombre de contributeurs sur GitHub. Avec cette initiative, Microsoft se positionne r&eacute;solument comme un acteur des logiciels communautaires.
