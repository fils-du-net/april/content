---
site: Developpez.com
title: "La part de marché de Firefox augmente pour la deuxième fois consécutive en 2 mois"
description: "le navigateur libre pourrait-il survivre auprès de Chrome?"
author: Coriolan
date: 2019-02-12
href: https://www.developpez.com/actu/245839
featured_image: https://www.developpez.net/forums/attachments/p449302d1/a/a/a
tags:
- Internet
- Standards
- Vie privée
series:
- 201907
---

> En novembre dernier, Firefox est tombé en dessous des 9 % de parts de marché sur desktop, une situation qui a poussé certains à se demander si le navigateur avait encore une place auprès de Chrome et la panoplie de navigateurs basés sur Chromium. Depuis des années déjà, la part de marché de Firefox s’est érodée, désormais, le navigateur se place derrière Chrome et Internet Explorer.
