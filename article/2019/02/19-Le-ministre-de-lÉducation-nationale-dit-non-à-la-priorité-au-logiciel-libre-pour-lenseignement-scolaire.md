---
site: Programmez!
title: "Le ministre de l'Éducation nationale dit non à la priorité au logiciel libre pour l'enseignement scolaire"
author: fredericmazue
date: 2019-02-19
href: https://www.programmez.com/actualites/le-ministre-de-leducation-nationale-dit-non-la-priorite-au-logiciel-libre-pour-lenseignement-28601
featured_image: https://www.programmez.com/sites/all/themes/programmez/images/logo.png
tags:
- Éducation
- Institutions
- april
series:
- 201908
series_weight: 0
---

> En ce moment nos instances gouvernantes discutent du projet de loi dit 'pour une école de la confiance'.
