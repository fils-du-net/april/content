---
site: The Conversation
title: "Mobiliser les sciences de gestion pour réussir la transition écologique et sociale"
author: Philippe Eynaud
date: 2019-01-31
href: https://theconversation.com/mobiliser-les-sciences-de-gestion-pour-reussir-la-transition-ecologique-et-sociale-110383
featured_image: https://images.theconversation.com/files/255547/original/file-20190125-108342-1aubzhd.jpg
tags:
- Innovation
- Économie
series:
- 201905
---

> Si la discipline parvient à s'émanciper du modèle de l'entreprise marchande, elle pourra apporter des réponses essentielles face aux enjeux actuels. Certains auteurs ont déjà ouvert la voie.
