---
site: internet ACTU.net
title: "Des effets des outils sur nos pratiques: pourquoi les médecins détestent-ils leurs ordinateurs?"
date: 2019-01-31
author: Claire Richard
href: http://www.internetactu.net/2019/01/31/des-effets-des-outils-dans-nos-pratiques-pourquoi-les-medecins-detestent-ils-leurs-ordinateurs
featured_image: http://www.internetactu.net/wp-content/uploads/2019/01/NewYorkerGawande.png
tags:
- Innovation
series:
- 201905
series_weight: 0
---

> Le chirurgien et journaliste Atul Gawande (@atul_gawande), célèbre pour son Checklist Manifesto («Le manifeste de la liste de contrôle», un bestseller qui a permis aux chirurgiens de réduire les erreurs en salle d’opération par la pratique de la liste de contrôle pré-opératoire, cf. «Concrètement, comment rendre les algorithmes responsables et équitables?») est un infatigable défenseur de l’amélioration de la santé publique. Dans une récente tribune pour le New Yorker, il décrivait les effets de l’informatisation sur le travail des praticiens hospitaliers, en soulignant et expliquant le conflit entre la logique informatique et les pratiques. Il y montrait très concrètement combien la transformation numérique, comme souvent, remplace une logique par une autre. Retour sur cette tribune, traduite et synthétisée par Claire Richard pour le Digital Society Forum.
