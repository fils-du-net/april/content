---
site: Capital.fr
title: "Java, Firefox, Linux... leurs inventeurs ont rendu le Web populaire"
author: Benjamin Janssens, Benjamin Saragagli
date: 2019-01-07
href: https://www.capital.fr/entreprises-marches/java-firefox-linux-leurs-inventeurs-ont-rendu-le-web-populaire-1322185
featured_image: https://cap.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2Fcap.2F2019.2F01.2F02.2Fbcbb451f-337d-47f6-95b4-89eae145e0b1.2Ejpeg/750x375/background-color/ffffff/quality/70/java-firefox-linux-leurs-inventeurs-ont-rendu-le-web-populaire-1322185.jpg
tags:
- Internet
- Innovation
series:
- 201902
series_weight: 0
---

> Découvrez les portraits de 5 pionniers du numérique., James Gosling: avec Java, il a créé l'espéranto du WebEn mettant au point le langage de programmation Java, il a rendu le Web universel, en permettant aux sites d’être accessibles sur toutes les machines.
