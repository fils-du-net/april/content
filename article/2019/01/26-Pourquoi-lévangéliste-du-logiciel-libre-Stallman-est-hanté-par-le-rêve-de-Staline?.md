---
site: Developpez.com
title: "Pourquoi l'évangéliste du logiciel libre Stallman est hanté par le rêve de Staline?"
description: Il s'attend toujours à de nouvelles fonctionnalités hostiles
author: Stan Adkens
date: 2019-01-26
href: https://www.developpez.com/actu/243300/Pourquoi-l-evangeliste-du-logiciel-libre-Stallman-est-hante-par-le-reve-de-Staline-Il-s-attend-toujours-a-de-nouvelles-fonctionnalites-hostiles
featured_image: https://www.developpez.net/forums/attachments/p444671d1/a/a/a
tags:
- Vie privée
- Institutions
- Entreprise
series:
- 201904
---

> Richard Stallman s'est récemment rendu à Mandya, une petite ville située à une centaine de kilomètres de Bengaluru, en Inde, pour donner une conférence, organisée par le Free Software Movement-Karnataka, qui a regroupé des centaines d'élèves et quelques enseignants. Richard Stallman est l’initiateur du projet GNU, un système d'exploitation de logiciel libre et fondateur du mouvement du logiciel libre qui compte maintenant des milliers de volontaires et de nombreux autres partisans à travers le monde. Les interventions de cet idéaliste, l'ultime évangéliste du logiciel libre, ont toujours attiré la foule, malgré ses opinions que d’aucuns considèrent comme extrêmes.
