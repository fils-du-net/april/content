---
site: 20minutes.fr
title: "Google soutient Wikipédia avec un don de 2 millions de dollars et des outils gratuits"
date: 2019-01-25
href: https://www.20minutes.fr/high-tech/2435883-20190125-google-soutient-wikipedia-don-2-millions-dollars-outils-gratuits
featured_image: https://img.20mn.fr/kjp4tj01R8iISt_wWto6gw/310x190_illustration-encyclopedie-libre-wikipedia.jpg
tags:
- Entreprise
- Internet
- Partage du savoir
series:
- 201904
---

> Google explique partager «depuis longtemps» avec Wikimédia une «mission de rendre l'information accessible aux gens dans le monde entier»…
