---
site: Usbek & Rica
title: "Et si le monde numérique devenait vraiment «libre»?"
author: Chrystèle Bazin
date: 2019-01-28
href: https://usbeketrica.com/article/open-source-logiciel-libre-inclusion
featured_image: https://static.usbeketrica.com/images/thumb_840xh/5c4ec91c8f67e.jpg
tags:
- Sensibilisation
- Partage du savoir
series:
- 201905
series_weight: 0
---

> Le monde du «libre» ne devrait-il pas engager une mutation pour rendre plus tangible sa dimension solidaire et toucher un public plus large?
