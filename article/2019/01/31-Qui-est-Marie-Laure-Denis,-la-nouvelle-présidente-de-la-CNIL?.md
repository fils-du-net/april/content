---
site: Numerama
title: "Qui est Marie-Laure Denis, la nouvelle présidente de la CNIL?"
author: Marie Turcan
date: 2019-01-31
href: https://www.numerama.com/politique/459638-qui-est-marie-laure-denis-la-nouvelle-presidente-de-la-cnil.html
featured_image: https://www.numerama.com/content/uploads/2019/01/m-l_denis-02-hd-1024x620.jpg
tags:
- Institutions
- Vie privée
series:
- 201905
---

> Isabelle Falque-Pierrotin a cédé sa place à la tête de la Commission nationale de l'informatique et des libertés. C'est Marie-Laure Denis, membre du Conseil d'Etat, qui a été nommée par le président Emmanuel Macron pour la remplacer.
