---
site: Numerama
title: "La CNIL inflige à Google une amende record de 50 millions d'euros pour violation du RGPD"
author: Julien Lausson
date: 2019-01-21
href: https://www.numerama.com/politique/457010-la-cnil-inflige-a-google-une-amende-record-de-50-millions-deuros-pour-violation-du-rgpd.html
featured_image: https://www.numerama.com/content/uploads/2018/01/isabelle-falque-pierrotin-cnil-1024x579.jpg
tags:
- Vie privée
- Entreprise
- Institutions
series:
- 201904
series_weight: 0
---

> La CNIL inflige une sanction administrative de 50 millions d"euros à Google pour infraction au RGPD.
