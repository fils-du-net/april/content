---
site: Le Point
title: "TRIBUNE. Comment la France s'est vendue aux Gafam"
date: 2019-01-05
href: https://www.lepoint.fr/invites-du-point/tribune-comment-la-france-s-est-vendue-aux-gafam-05-01-2019-2283510_420.php
featured_image: https://www.lepoint.fr/images/2019/01/05/17841904lpw-17841961-article-jpg_5850979_660x281.jpg
tags:
- International
- Entreprise
- Internet
- Institutions
series:
- 201901
---

> Pour le pionnier du Web français Tariq Krim, l'histoire du déclin du numérique français est une tragédie en 3 actes. Il existe pourtant une sortie de crise.
