---
site: ZDNet France
title: "MongoDB: la nouvelle licence SSPL fait grincer des dents dans l’open source"
author: Steven J. Vaughan-Nichols
date: 2019-01-17
href: https://www.zdnet.fr/actualites/mongodb-la-nouvelle-licence-sspl-fait-grincer-des-dents-dans-l-open-source-39879413.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2015/03/opensource-600.jpg
tags:
- Licenses
- Informatique en nuage
series:
- 201903
series_weight: 0
---

> Red Hat n'utilisera pas MongoDB dans Red Hat Enterprise Linux ou Fedora à cause de la nouvelle licence publique côté serveur de MongoDB.
