---
site: Contrepoints
title: "Makers: la nouvelle révolution industrielle"
author: Farid Gueham
date: 2019-01-08
href: https://www.contrepoints.org/2019/01/08/334158-makers-la-nouvelle-revolution-industrielle
featured_image: https://www.contrepoints.org/wp-content/themes/cp2016/css/static/CP-logo-desktop-tablette-512x92.png
tags:
- Économie
- Matériel libre
series:
- 201902
series_weight: 0
---

> Une nouvelle révolution industrielle est en marche: elle se nourrit des Makers qui utilisent les outils numériques, l’open source, et la professionnalisation qui disruptent notre modèle économique.
