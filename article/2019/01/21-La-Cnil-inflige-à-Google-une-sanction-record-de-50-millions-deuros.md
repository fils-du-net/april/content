---
site: L'OBS
title: "La Cnil inflige à Google une sanction record de 50 millions d'euros"
author: Thierry Noisette
date: 2019-01-21
href: https://www.nouvelobs.com/economie/20190121.OBS8851/la-cnil-inflige-a-google-une-sanction-record-de-50-millions-d-euros.html
featured_image: https://media.nouvelobs.com/referentiel/633x306/16714072.jpg
tags:
- Vie privée
- Institutions
- Entreprise
series:
- 201904
---

> La Commission informatique et libertés utilise contre Google le nouveau plafond de sanction prévu par le règlement européen (RGPD), pour manque de transparence et absence de consentement valable.
