---
site: Usbek & Rica
title: "Aaron Swartz, martyr éternel de l'Internet libre"
date: 2019-01-27
author: Vincent Lucchese
href: https://usbeketrica.com/article/aaron-swartz-martyr-internet-libre
featured_image: https://static.usbeketrica.com/images/thumb_840xh/5c4b48ec273c6.jpg
tags:
- Contenus libres
- Internet
- Innovation
series:
- 201904
series_weight: 0
---

> Dans «Ce qu'il reste de nos rêves», Flore Vasseur part sur les traces du génie précoce qui voulait sauver le monde, avant de se suicider.
