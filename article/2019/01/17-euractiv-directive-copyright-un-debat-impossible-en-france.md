---
site: EurActiv
title: "Directive copyright: un débat impossible en France"
author: Pierre-Yves Beaudouin
date: 2019-01-17
href: https://www.euractiv.fr/section/economie/opinion/directive-copyright-un-debat-impossible-en-france
featured_image: https://fr.euractiv.eu/wp-content/uploads/sites/3/2019/01/Wikimedia_800x450.png
tags:
- Droit d'auteur
- Internet
- Partage du savoir
- Institutions
- Europe
series:
- 201903
series_weight: 0
---

> Durant les débats de ces derniers mois, Wikipedia a été présenté comme un anti-droit d’auteur par de nombreux partisans de l’article 13. Née en pleine période de piratage massif de la musique, l’encyclopédie Wikipédia repose sur un système juridique basé sur le droit d’auteur et la création de son propre contenu. Le piratage n’a jamais été la règle et la communauté passe de nombreuses heures à expliquer et faire respecter le droit d’auteur. Les wikipédiens sont des créateurs comme les autres et sont régulièrement victimes de plagiat.
