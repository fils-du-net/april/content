---
site: Le Monde Informatique
title: "Linagora vs BlueMind: 1e manche gagnée par le second"
author: Bastien Lion
date: 2019-01-17
href: https://www.lemondeinformatique.fr/actualites/lire-linagora-vs-bluemind-1e-manche-gagnee-par-le-second-74024.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000064913.png
tags:
- Entreprise
- Institutions
series:
- 201903
series_weight: 0
---

> Une première décision a été rendue dans le conflit entre Linagora et deux dirigeants de BlueMind, en faveur de ce dernier. Ces deux acteurs du logiciel libre en France s'opposent depuis 2012.
