---
site: l'Humanité.fr
title: "Rémi Boulle, «L’État ne fait rien pour résister à l’entrisme de ces grandes firmes»"
author: Laurent Mouloud
date: 2019-01-18
href: https://www.humanite.fr/letat-ne-fait-rien-pour-resister-lentrisme-de-ces-grandes-firmes-666571
featured_image: https://www.humanite.fr/sites/default/files/styles/1048x350/public/images/63137.HR.jpg?itok=yAB53ARb
tags:
- Éducation
- Entreprise
- Logiciels privateurs
- April
series:
- 201903
series_weight: 0
---

> Rémi Boulle, de l’Association pour la promotion et la recherche en informatique libre, déplore le renoncement du ministère face à Microsoft. Entretien.
