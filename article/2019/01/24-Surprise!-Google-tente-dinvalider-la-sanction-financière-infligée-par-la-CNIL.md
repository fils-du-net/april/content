---
site: Numerama
title: "Surprise! Google tente d'invalider la sanction financière infligée par la CNIL"
author: Julien Lausson
date: 2019-01-24
href: https://www.numerama.com/politique/457848-surprise-google-tente-dinvalider-la-sanction-financiere-infligee-par-la-cnil.html
featured_image: https://www.numerama.com/content/uploads/2016/11/conseil-etat-1024x579.jpg
tags:
- Vie privée
- Institutions
- Entreprise
series:
- 201904
---

> Google saisit le Conseil d'État pour tenter d'invalider la sanction financière de la CNIL, qui atteint 50 millions d'euros.
