---
site: leParisien.fr
title: "VLC fête ses 3 milliards de téléchargements: retour sur une «success story» française"
author: Damien Licata Caruso
date: 2019-01-10
href: http://www.leparisien.fr/high-tech/vlc-fete-ses-3-milliards-de-telechargements-retour-sur-une-success-story-francaise-10-01-2019-7985283.php
featured_image: http://s1.lprs1.fr/images/2019/01/10/7985283_e143b1fc-1432-11e9-8e42-4020b3851909-1_1000x625.jpg
tags:
- Sensibilisation
series:
- 201902
series_weight: 0
---

> Le logiciel gratuit de lecture de vidéo, imaginé en 2001 par des étudiants de l’Ecole Centrale, est aujourd’hui utilisé par 450 millions de personnes. Retour sur un projet collaboratif qui résiste toujours aux sirènes des géants de l’Internet.
