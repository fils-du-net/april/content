---
site: Developpez.com
title: "Richard Stallman: la Free Software Foundation a reçu deux dons d'un million $ chacun"
author: Jonathan
date: 2019-01-02
href: https://www.developpez.com/actu/239522/Richard-Stallman-la-Free-Software-Foundation-a-recu-deux-dons-d-un-million-chacun-ce-qui-permettra-de-continuer-a-promouvoir-le-logiciel-libre
featured_image: https://www.developpez.net/forums/attachments/p438176d1/a/a/a
tags:
- Associations
- Promotion
series:
- 201901
series_weight: 0
---

> Plus connu par ses initiales RMS, Richard Matthew Stallman est un activiste américain du mouvement du logiciel libre. Il fait campagne pour que le logiciel soit distribué de manière à ce que ses utilisateurs aient la liberté de l’utiliser, de l’étudier, de le distribuer et le modifier. Il a lancé le projet GNU, fondé la Free Software Foundation, développé la collection de compilateurs GNU et GNU Emacs et rédigé la licence publique générale GNU.
