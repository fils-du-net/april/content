---
site: EurActiv
title: "Rebondissement de dernière minute sur la directive droit d'auteur"
date: 2019-01-21
href: https://www.euractiv.fr/section/economie/news/copyright-directive-faces-further-setback-as-final-trilogue-cancelled
featured_image: https://www.euractiv.fr/wp-content/uploads/sites/3/2019/01/h_53155819-800x450.jpg
tags:
- Droit d'auteur
- Institutions
- Europe
series:
- 201904
---

> La directive tant controversée sur le droit d'auteur a accusé un nouveau revers. Les négociations finales prévues le lundi 21 janvier ont été annulées in extremis.
