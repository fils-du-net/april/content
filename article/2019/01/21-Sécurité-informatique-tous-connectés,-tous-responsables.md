---
site: Libération
title: "Sécurité informatique: tous connectés, tous responsables"
date: 2019-01-21
href: https://www.liberation.fr/debats/2019/01/21/securite-informatique-tous-connectes-tous-responsables_1704228
featured_image: https://medias.liberation.fr/photo/859048-la-fuite-massive-d-emails-embarrassants-voles-au-parti-democrate-fait-craindre-aux-experts-en-securi.jpg
tags:
- Innovation
- Institutions
series:
- 201904
series_weight: 0
---

> Une culture de la sécurité informatique est nécessaire en France, ne serait-ce que pour arbitrer des choix comme la facilité d'accès aux données pour les services de renseignements: ces facilités ne sont souvent que des failles informatiques, susceptibles d'être utilisées de façon moins légitime.
