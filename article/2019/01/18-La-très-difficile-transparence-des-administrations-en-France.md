---
site: Le Monde.fr
title: La très difficile transparence des administrations en France
author: Maxime Ferrer
date: 2019-01-18
href: https://www.lemonde.fr/les-decodeurs/article/2019/01/18/la-cada-la-transparence-au-rabais_5411293_4355770.html
tags:
- Open Data
- Administration
series:
- 201904
series_weight: 0
---

> La Commission d'accès aux documents administratifs (CADA ) est censée assurer la transparence de la vie publique, mais manque de moyens pour le faire.
