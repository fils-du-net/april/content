---
site: Le Monde.fr
title: "L'irrésistible ascension du lecteur vidéo «VLC», une révolution française"
author: Corentin Lamy
date: 2019-01-28
href: https://www.lemonde.fr/pixels/article/2019/01/28/l-irresistible-ascension-du-lecteur-video-vlc-une-revolution-francaise_5415762_4408996.html
featured_image: https://img.lemde.fr/2019/01/28/0/0/2000/1333/600/0/60/0/b095d2a_YIrr9Xg7ABu_j3qoTlmyhJ3L.jpg
tags:
- Sensibilisation
- Innovation
series:
- 201905
series_weight: 0
---

> Capable de lire n'importe quelle vidéo sur n'importe quel support, le petit logiciel gratuit VLC s'est imposé en vingt ans comme un indispensable. Il est développé par des Français.
