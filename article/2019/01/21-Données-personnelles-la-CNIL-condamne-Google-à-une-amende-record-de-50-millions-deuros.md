---
site: Le Monde.fr
title: "Données personnelles: la CNIL condamne Google à une amende record de 50 millions d'euros"
author: Vincent Fagot
date: 2019-01-21
href: https://www.lemonde.fr/pixels/article/2019/01/21/donnees-personnelles-la-cnil-condamne-google-a-une-amende-record-de-50-millions-d-euros_5412337_4408996.html
featured_image: https://img.lemde.fr/2019/01/16/0/0/4256/2832/688/0/60/0/7680eb2_j_qER9_nFVJGecfe7wMIzcVL.jpg
tags:
- Vie privée
- Entreprise
- Institutions
series:
- 201904
---

> Le gendarme français de la vie privée reproche au géant américain de ne pas informer assez clairement ses utilisateurs.
