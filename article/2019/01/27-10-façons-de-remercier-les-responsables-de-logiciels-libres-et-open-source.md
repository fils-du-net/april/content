---
site: Contrepoints
title: "10 façons de remercier les responsables de logiciels libres et open source"
author: Moshe Zadka
date: 2019-01-27
href: https://www.contrepoints.org/2019/01/27/335668-10-facons-de-remercier-les-responsables-de-logiciels-libres-et-open-source
featured_image: https://www.contrepoints.org/wp-content/uploads/2019/01/The-Open-Source-Crew-by-Quinn-DombrowskiCC-BY-SA-2.0.jpg
tags:
- Sensibilisation
- Promotion
series:
- 201904
series_weight: 0
---

> Chaque jour on utilise des logiciels de bonne qualité développés par des gens qui ne demandent pas de paiement, qui respectent les libertés et qui sont généreux de leur temps et de leur énergie.
Comment remercier les responsables de logiciel libres et open source?
