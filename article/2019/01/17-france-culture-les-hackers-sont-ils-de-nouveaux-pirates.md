---
site: France Culture
title: "Les hackers sont-ils de nouveaux pirates?"
author: Antoine Garapon
date: 2019-01-17
href: https://www.franceculture.fr/emissions/matieres-a-penser/lactualite-des-pirates-45-les-hackers-sont-ils-de-nouveaux-pirates
featured_image: https://cdn.radiofrance.fr/s3/cruiser-production/2019/01/7f906c1f-3026-4e06-b34e-97d6df610933/838_4_hackers.jpg
tags:
- Partage du savoir
- Sensibilisation
series:
- 201903
series_weight: 0
---

> Cette semaine propose de prendre au sérieux les courants contemporains qui se réclament du pirate, des partis pirates aux hackers en passant par les terroristes ou la finance internationale, pour comprendre un peu mieux notre monde si troublé. Ce soir, avec Benjamin Loveluck, chercheur au CERSA.
