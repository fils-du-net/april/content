---
site: Usbek & Rica
title: "La Cnil inflige une amende de 50 millions d'euros à Google"
author: Guillaume Ledit
date: 2019-01-21
href: https://usbeketrica.com/article/google-cnil-amende-rgpd
featured_image: https://static.usbeketrica.com/images/thumb_840xh/5c45ebbc68556.jpg
tags:
- Vie privée
- Institutions
- Entreprise
series:
- 201904
---

> L'autorité administrative française a infligé une sanction de 50 millions d'euros à l'entreprise. Un record, établi en application du RGPD.
