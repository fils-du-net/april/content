---
site: Rude Baguette
title: "L'Union Européenne lance une chasse aux bugs pour les logiciels libres"
date: 2019-01-08
href: http://www.rudebaguette.com/2019/01/08/ue-chasse-bugs-logiciels-libres
featured_image: http://www.rudebaguette.com/assets//UE-FOSSA.jpg
tags:
- Institutions
- Europe
series:
- 201902
series_weight: 0
---

> Dans sa volonté de défendre les logiciels open source et la sécurité sur Internet, la Commission Européenne, via le programme FOSSA, vient de lancer une chasse aux bugs. Elle concerne les quinze logiciels libres les plus utilisés par les institutions européennes.
