---
site: La Tribune
title: "En plus du Grand débat, l'Etat veut aussi votre avis pour réguler le numérique"
author: Anaïs Cherif
date: 2019-01-16
href: https://www.latribune.fr/technos-medias/regulation-du-numerique-comment-le-gouvernement-veut-batir-un-projet-europeen-804011.html
featured_image: https://static.latribune.fr/full_width/1083782/mounir-mahjoubi.jpg
tags:
- Internet
- Institutions
- Associations
- Europe
- International
series:
- 201903
series_weight: 0
---

> Mounir Mahjoubi, secrétaire d'Etat au numérique, et le CNNum lancent cette semaine une consultation nationale citoyenne dans le cadre des "Etats généraux des nouvelles régulations numériques". Le but: porter les propositions au niveau européen à l'issue des prochaines élections en mai, pour contrer "le laisser-faire californien et le contrôle chinois".
