---
site: Numerama
title: "Article 13, YouTube, droit d'auteur...: quelles sont les étapes à venir pour la future directive européenne?"
author: Julien Lausson
date: 2019-01-12
href: https://www.numerama.com/politique/454735-article-13-youtube-droit-dauteur-quelles-sont-les-etapes-a-venir-pour-la-future-directive-europeenne.html
featured_image: https://www.numerama.com/content/uploads/2018/06/eu-copyright-1024x576.png
tags:
- Institutions
- Droit d'auteur
- Europe
series:
- 201903
---

> Le sort de la nouvelle directive sur le droit d'auteur en Europe doit être fixé ce printemps.
