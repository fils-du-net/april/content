---
site: L'Informaticien
title: "La Commission européenne lance plusieurs bug bounties sur des logiciels open source"
author: Bertrand Garé
date: 2019-01-02
href: https://www.linformaticien.com/actualites/id/51057/la-commission-europeenne-lance-plusieurs-bug-bounties-sur-des-logiciels-open-source.aspx
featured_image: https://www.linformaticien.com/Portals/0/2018/12_dec/planning%20bug%20bounties%20fossa.png
tags:
- Institutions
- Innovation
- Europe
series:
- 201901
series_weight: 0
---

> Pas moins de 14 logiciels open source vont être passés au peigne fin de plusieurs programmes de recherches de bugs. La campagne va s’étaler sur plusieurs mois.
