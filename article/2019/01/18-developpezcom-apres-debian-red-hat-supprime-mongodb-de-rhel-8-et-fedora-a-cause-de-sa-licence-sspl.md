---
site: Developpez.com
title: "Après Debian, Red Hat supprime MongoDB de RHEL 8 et Fedora à cause de sa licence SSPL"
author: Michael Guilloux
date: 2019-01-18
href: https://www.developpez.com/actu/241843/Apres-Debian-Red-Hat-supprime-MongoDB-de-RHEL-8-et-Fedora-a-cause-de-sa-licence-SSPL-dont-le-statut-de-licence-libre-ou-open-source-est-conteste
featured_image: https://www.developpez.com/public/images/news/mongodb.jpg
tags:
- Licenses
- Informatique en nuage
series:
- 201903
---

> En octobre dernier, Eliot Horowitz, le directeur technique et cofondateur de MongoDB a fait une annonce qui a créé des tumultes dans la communauté open source: la création d’une nouvelle licence open source, la Server Side Public Licence (SSPL) pour le célèbre système de gestion de base de données orientée documents. Cette licence s'applique à toutes les versions de MongoDB publiées ou qui seront publiées après le 16 octobre 2018 et vient en remplacement de la licence AGPLv3. Elle s'applique également aux correctifs pour les versions antérieures du SGBD.
