---
site: France Culture
title: "Civic tech: de la marge au centre du jeu politique?"
author: Anne Fauquembergue
date: 2019-01-11
href: https://www.franceculture.fr/emissions/hashtag/civic-tech-de-la-marge-au-centre-du-jeu-politique
featured_image: https://cdn.radiofrance.fr/s3/cruiser-production/2019/01/d92b8da7-8722-4030-a64a-902a65201fc5/838_gettyimages-510464802.jpg
tags:
- Partage du savoir
- Institutions
series:
- 201902
series_weight: 0
---

> Les civic tech existent depuis dix ans en France. Grâce à des innovations technologiques, elles cherchent à améliorer le système politique. La crise des "gilets jaunes" et surtout le grand débat national les placent au centre du jeu politique.
