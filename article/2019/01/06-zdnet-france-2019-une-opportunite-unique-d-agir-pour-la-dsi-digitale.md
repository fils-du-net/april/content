---
site: ZDNet France
title: "2019, une opportunité unique d'agir pour la DSI digitale"
author: Frédéric Charles
date: 2019-01-06
href: https://www.zdnet.fr/blogs/green-si/2019-une-opportunite-unique-d-agir-pour-la-dsi-digitale-39878745.htm
featured_image: https://www.zdnet.fr/i/edit/2019/01/39878745/image04.png
tags:
- Entreprise
- Innovation
series:
- 201901
series_weight: 0
---

> GreenSI a creusé le sujet de la relation DG-DSI dans ce billet, en revenant sur l'historique de cette relation, afin de comprendre pourquoi 2019 est une année d'opportunités pour la DSI.
