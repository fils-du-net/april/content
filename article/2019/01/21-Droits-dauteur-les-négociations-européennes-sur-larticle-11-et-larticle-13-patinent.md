---
site: Le Monde.fr
title: "Droits d'auteur: les négociations européennes sur l'article 11 et l'article 13 patinent"
date: 2019-01-21
href: https://www.lemonde.fr/pixels/article/2019/01/21/droits-d-auteur-les-negociations-europeennes-sur-l-article-11-et-l-article-13-patinent_5412290_4408996.html
featured_image: https://img.lemde.fr/2019/01/18/0/0/3500/2481/688/0/60/0/f71a5b4_GGGYH100_EU-COMMISSION-_0118_11.JPG
tags:
- Droit d'auteur
- april
- Europe
- Institutions
series:
- 201904
series_weight: 0
---

> Le «trilogue» européen n'est pas parvenu à se mettre d'accord sur une version finale du texte, objet d'une intense bataille de lobbying.
