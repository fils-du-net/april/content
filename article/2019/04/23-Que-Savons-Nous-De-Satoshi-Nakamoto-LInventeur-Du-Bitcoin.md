---
site: Forbes France
title: "Que Savons-Nous De Satoshi Nakamoto, L’Inventeur Du Bitcoin?"
author: Olivier Bossard
date: 2019-04-23
href: https://www.forbes.fr/finance/que-savons-nous-de-satoshi-nakamoto-linventeur-du-bitcoin
featured_image: https://www.forbes.fr/wp-content/uploads/2019/04/olivier-bossard-msc-finance-hec-fintech-blockchain-740x370.jpg
tags:
- Innovation
series:
- 201917
series_weight: 0
---

> Bien évidemment, cette question n’est absolument pas pertinente pour comprendre le Bitcoin et la technologie de blockchain, mais elle est vraiment excitante pour beaucoup de passionnés des crypto-monnaies! Laissez-moi tenter d’y apporter quelques éléments de réponse.
