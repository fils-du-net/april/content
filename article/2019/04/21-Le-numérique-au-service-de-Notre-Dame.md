---
site: Techniques de l'Ingénieur
title: "Le numérique au service de Notre-Dame"
author: Philippe Richard
href: https://www.techniques-ingenieur.fr/actualite/articles/le-numerique-au-service-de-notre-dame-65628
featured_image: https://www.techniques-ingenieur.fr/actualite/wp-content/uploads/2019/04/notre-dame-laser-1140.jpg
tags:
- Partage du savoir
- Innovation
series:
- 201916
---

> Dévastée dans la soirée du 15 avril, la Cathédrale Notre-Dame de Paris a perdu une bonne partie de sa toiture et de sa voûte. Quant à sa flèche, elle s'est effondrée. Les architectes s'appuieront peut être sur les «copies numériques» pour relever le défi de la reconstruction.
