---
site: DNA
title: "Logiciels libres: la résistance s’organise"
author: Lola Scandella
date: 2019-04-02
href: https://www.dna.fr/edition-de-selestat-centre-alsace/2019/04/02/logiciels-libres-la-resistance-s-organise
featured_image: https://cdn-s-www.dna.fr/images/F10D5E33-2D34-4A08-8AA8-1A12C9935B48/DNA_03/apres-une-presentation-de-l-association-on-passe-a-la-pratique-photo-dna-jean-paul-kaiser-1554135481.jpg
tags:
- Associations
- Promotion
series:
- 201906
series_weight: 0
---

> Une association nouvellement créée à Sélestat veut faire découvrir au plus grand nombre un système d’exploitation libre, Linux. Selon ses membres, ce logiciel gratuit et ouvert aux modifications serait une alternative aux systèmes informatiques dominants.
