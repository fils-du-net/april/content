---
site: EurActiv
title: "Copyright rules will turn the EU into a French digital colony"
date: 2019-04-09
author: Amelia Andersdotter
href: https://www.euractiv.com/section/copyright/opinion/copyright-rules-will-turn-the-eu-into-a-french-digital-colony/
featured_image: https://www.euractiv.com/wp-content/uploads/sites/2/2019/03/h_55082312-800x450.jpg
tags:
- Droit d'auteur
- Europe
- English
series:
- 201915
series_weight: 0
---

> France, with its strict regulations of digital markets and its industries, has been allowed to drive the oppressive changes in copyright that are now about to be European law. It is not too late to save European citizens from these reforms that will do serious damage to the internet, writes Amelia Andersdotter.
