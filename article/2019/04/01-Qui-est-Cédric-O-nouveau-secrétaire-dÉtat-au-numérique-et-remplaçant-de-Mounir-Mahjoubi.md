---
site: Numerama
title: "Qui est Cédric O, nouveau secrétaire d'État au numérique et remplaçant de Mounir Mahjoubi?"
author: Julien Lausson
date: 2019-04-01
href: https://www.numerama.com/politique/477082-qui-est-cedric-o-nouveau-secretaire-detat-au-numerique-et-remplacant-de-mounir-mahjoubi.html
featured_image: https://www.numerama.com/content/uploads/2019/04/image-dios-2.jpg
tags:
- Institutions
series:
- 201914
---

> Trois jours après la démission de Mounir Mahjoubi, Cédric O a été nommé au secrétariat d’État au numérique. Il a décrit quelques unes de ses priorités.
