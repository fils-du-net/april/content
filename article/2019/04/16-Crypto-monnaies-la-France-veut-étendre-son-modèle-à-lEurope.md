---
site: Silicon
title: "Crypto-monnaies: la France veut étendre son modèle à l'Europe"
date: 2019-04-16
href: https://www.silicon.fr/crypto-monnaies-la-france-veut-etendre-son-modele-a-leurope-238571.html
featured_image: https://www.silicon.fr/wp-content/uploads/2018/05/blockchain.jpg
tags:
- Économie
- Institutions
series:
- 201916
---

> La France est le premier pays de l'Union européenne à encadrer légalement l'utilisation, la création et la circulation de crypto-monnaies.
