---
site: L'ADN 
title: "Écrans publicitaires: comment éviter d'envoyer ses données aux marques"
date: 2019-04-02
href: https://www.ladn.eu/tech-a-suivre/ecrans-publicitaires-espionnent-embrouiller
featured_image: https://www.ladn.eu/wp-content/uploads/2019/04/GettyImages-952082472-2-1140x480.jpg
tags:
- Vie privée
- Innovation
series:
- 201914
series_weight: 0
---

> Des écrans publicitaires récoltent l'adresse des smartphones des passants afin de mesurer l'efficacité d'une pub. Le dispositif inquiète depuis quelques jours malgré sa légalité. Un codeur a développé un programme visant à gêner l'opération.
