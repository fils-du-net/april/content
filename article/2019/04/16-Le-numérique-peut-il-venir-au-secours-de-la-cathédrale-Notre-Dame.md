---
site: L'usine Nouvelle
title: "Le numérique peut-il venir au secours de la cathédrale Notre-Dame?"
author: Sylvain Arnulf
date: 2019-04-16
href: https://www.usinenouvelle.com/editorial/le-numerique-peut-il-venir-au-secours-de-la-cathedrale-notre-dame.N832175
featured_image: https://www.usinenouvelle.com/mediatheque/0/1/0/000760010_image_896x598/cathedrale-notre-dame-numerisee.jpg
tags:
- Partage du savoir
- Innovation
series:
- 201916
series_weight: 0
---

> La numérisation partielle de la structure de Notre-Dame de Paris pourrait permettre d’accélérer sa reconstruction.
