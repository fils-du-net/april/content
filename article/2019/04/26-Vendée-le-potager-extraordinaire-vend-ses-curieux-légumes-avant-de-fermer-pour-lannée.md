---
site: france bleu
title: "Vendée: le potager extraordinaire vend ses curieux légumes avant de fermer pour l'année"
author: Marc Bertrand
date: 2019-04-26
href: https://www.francebleu.fr/infos/societe/vendee-le-potager-extraordinaire-vend-ses-curieux-legumes-avant-de-fermer-pour-l-annee-1556231117
featured_image: https://s1-ssl.dmcdn.net/v/Pv1YP1SmjA1ry0DEI/x480
tags:
- Innovation
- Partage du savoir
series:
- 201917
series_weight: 0
---

> Le potager extraordinaire, à la Mothe-Achard va fermer ses portes pour l'année à partir de la fin juin, avant un déménagement encore incertain vers La Roche-sur-Yon. Avant de quitter le terrain, les jardiniers mettent en vente 10.000 plants potagers plus curieux les uns que les autres.
