---
site: Le Monde.fr
title: "Google: des employées et activistes anti-harcèlement sexuel dénoncent des représailles en interne"
author: Alexandre Piquard
date: 2019-04-22
href: https://www.lemonde.fr/pixels/article/2019/04/23/google-des-employes-leaders-d-une-mobilisation-interne-denoncent-des-represailles_5453607_4408996.html
featured_image: https://img.lemde.fr/2018/11/08/0/0/3000/2000/688/0/60/0/b58f25e_dee5bb99e575445f918e61743ad5ebdc-dee5bb99e575445f918e61743ad5ebdc-0.jpg
tags:
- Entreprise
series:
- 201917
---

> Deux salariées à l’origine de ce mouvement dans l’entreprise racontent les menaces dont elles ont fait l’objet depuis.
