---
site: Le Monde Informatique
title: "Les licences open source sont-elles aussi importantes qu'avant?"
author: Matt Asay
date: 2019-04-26
href: https://www.lemondeinformatique.fr/actualites/lire-les-licences-open-source-sont-elles-aussi-importantes-qu-avant-74859.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000066085.jpg
tags:
- Licenses
- Entreprise
series:
- 201917
series_weight: 0
---

> Les efforts de MongoDB pour obtenir l'approbation de l'Open source initiative en vue d'une licence, SSPL, plus favorable aux entreprises ont échoué. L'entreprise a donc choisi de faire sans, et cela pourrait bien donner lieu à un moment charnière dans l'histoire de l'open source.
