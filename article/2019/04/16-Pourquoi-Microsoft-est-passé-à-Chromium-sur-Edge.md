---
site: Le Monde Informatique
title: "Pourquoi Microsoft est passé à Chromium sur Edge"
author: Preston Gralla
date: 2019-04-16
href: https://www.lemondeinformatique.fr/actualites/lire-pourquoi-microsoft-est-passe-a-chromium-sur-edge-75001.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000066295.jpg
tags:
- Internet
series:
- 201916
---

> En acceptant de s'ouvrir aux standards open source - en l'occurrence Chromium - pour son navigateur web Edge, Microsoft enterre une grande partie de sa stratégie historique. Une métamorphose que ses anciens PDG n'auraient jamais pu imaginer.
