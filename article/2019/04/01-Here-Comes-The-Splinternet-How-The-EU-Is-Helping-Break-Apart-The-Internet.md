---
site: Techdirt.
title: "Here Comes The Splinternet: How The EU Is Helping Break Apart The Internet"
date: 2019-04-01
href: https://www.techdirt.com/articles/20190329/16255841904/here-comes-splinternet-how-eu-is-helping-break-apart-internet.shtml
tags:
- Droit d'auteur
- Europe
- Internet
- English
series:
- 201914
---

>  In the wake of last week's unfortunate decision by the EU Parliament to vote for the terrible EU Copyright Directive, Casey Newton over at the Verge has a thoughtful piece about how this could lead to the internet splitting into three.
