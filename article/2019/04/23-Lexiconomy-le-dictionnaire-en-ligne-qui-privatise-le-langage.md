---
site: korii.
title: "Lexiconomy, le dictionnaire en ligne qui privatise le langage"
author: Thibault Prévost
date: 2019-04-23
href: https://korii.slate.fr/et-caetera/lexiconomy-dictionnaire-privatisation-langage-blockchain-ethereum
featured_image: https://korii.slate.fr/sites/default/files/styles/1440x600/public/pisit-heng-642891-unsplash.jpg
tags:
- Droit d'auteur
- HADOPI
- Partage du savoir
series:
- 201917
series_weight: 0
---

> Cet absurde projet permet à quiconque d'ajouter sa contribution, appelée «lemma», puis de la vendre ou de l'échanger sur une sorte de marché du signe.
