---
site: TF1 news
title: "Immobilier: on a testé la nouvelle appli qui vous donne les prix de vente réels à côté de chez vous"
author: Laurence Valdés
date: 2019-04-26
href: https://www.lci.fr/immobilier/immobilier-on-a-teste-la-nouvelle-appli-de-bercy-pour-connaitre-les-prix-de-vente-reels-a-cote-de-chez-vous-cadastre-data-gouv-fr-dvf-2119439.html
featured_image: https://photos.lci.fr/images/1024/576/immobilier-vente-achat-prix-fec9cd-0@1x.jpeg
tags:
- Open Data
series:
- 201917
series_weight: 0
---

> OPEN DATA - Le gouvernement ouvre ses données portant sur les transactions immobilières. Depuis ce mercredi, vous pouvez consulter autant de fois que vous le souhaitez -et sans inscription ni identifiant- les prix des biens qui se sont vendus quasiment partout en France. Mode d'emploi.
