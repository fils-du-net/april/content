---
site: ZDNet France
title: "Un ancien cadre de Mozilla accuse: Google a saboté Firefox pendant des années"
author: Catalin Cimpanu
date: 2019-04-16
href: https://www.zdnet.fr/actualites/un-ancien-cadre-de-mozilla-accuse-google-a-sabote-firefox-pendant-des-annees-39883529.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/03/mozilla_620.jpg
tags:
- Internet
- Entreprise
series:
- 201916
---

> Les ingénieurs de Mozilla lâchent quelques bombes sur la relation viciée avec Google qui a amené Firefox au désastre. En cause, les multiples opérations de sabotage menées par les employés du géant californien.
