---
site: Libération
title: "Grand débat et IA: quelle transparence pour les données?"
author: SIF et Aurélien Bellet
date: 2019-04-07
href: https://www.liberation.fr/debats/2019/04/07/grand-debat-et-ia-quelle-transparence-pour-les-donnees_1719944
featured_image: https://medias.liberation.fr/photo/1209262-participants-a-une-des-reunions-du-grand-debat-national-le-28-fevrier-2019-a-bollene-vaucluse.jpg
tags:
- Open Data
- Institutions
series:
- 201914
---

> Les contributions citoyennes sont analysées par un algorithme  qui construit automatiquement les catégories d'analyse, ce qui risque d'influencer la tonalité des résultats. Il faut donc rendre publiques les modalités de fabrication de la synthèse.
