---
site: Commentçamarche.net
title: "La nouvelle loi sur le droit d'auteur renforce les devoirs des plateformes"
author: Hervé Didier
date: 2019-04-02
href: https://www.commentcamarche.net/news/5872561-la-nouvelle-loi-sur-le-droit-d-auteur-renforce-les-devoirs-des-plateformes
tags:
- Droit d'auteur
- Europe
- Internet
series:
- 201914
---

> La réforme du droit d'auteur en Europe veut aider les créateurs de contenus à défendre leurs droits vis-à-vis des plateformes numériques.
