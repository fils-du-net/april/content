---
site: The Verge
title: "Europe is splitting the internet into three"
author: Casey Newton
date: 2019-04-01
href: https://www.theverge.com/2019/3/27/18283541/european-union-copyright-directive-internet-article-13
tags:
- Droit d'auteur
- Europe
- Internet
- English
series:
- 201913
---

> The Copyright Directive reshapes the open web, creating a different version of the internet for Europe and for everyone else
