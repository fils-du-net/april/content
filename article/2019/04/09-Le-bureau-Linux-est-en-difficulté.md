---
site: ZDNet France
title: "Le poste de travail Linux est en difficulté"
author: Steven J. Vaughan-Nichols
date: 2019-04-09
href: https://www.zdnet.fr/actualites/le-bureau-linux-est-en-difficulte-39883239.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/04/linux-mint-desktop-620.jpg
tags:
- Innovation
- Sensibilisation
series:
- 201915
series_weight: 0
---

> Linus Torvalds se tourne vers Chromebooks et Android pour l'avenir du bureau Linux, tandis que les développeurs de Linux Mint ne parviennent pas à s'entendre entre eux.
