---
site: ZDNet France
title: "Notre-Dame de Paris: les scanners numériques comme espoir pour la reconstruction?"
author: Stephanie Condon
date: 2019-04-16
href: https://www.zdnet.fr/actualites/notre-dame-de-paris-les-scanners-numeriques-comme-espoir-pour-la-reconstruction-39883517.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/02/Notre-Dame%20de%20Paris%20620.jpg
tags:
- Partage du savoir
- Innovation
series:
- 201916
---

> Andrew Tallon, récemment décédé, a finalisé un travail minutieux de numérisation laser de la cathédrale Notre-Dame de Paris, créant ainsi de riches données qui pourraient contribuer aux efforts de reconstruction du monument.
