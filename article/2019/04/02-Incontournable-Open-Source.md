---
site: CXP
title: "Incontournable Open Source"
date: 2019-04-02
href: https://www.cxp.fr/content/news/edito-incontournable-open-source-0
tags:
- Entreprise
- Informatique en nuage
series:
- 201914
series_weight: 0
---

> 2018 a marqué un tournant pour les logiciels Open Source, avec notamment le rachat de Red Hat par IBM pour 34 milliards de dollars et celui de Github par Microsoft pour 7,5 milliards de dollars. Des entreprises privées qui misent sur les acteurs du Libre. Et ce de façon massive, si l'on en juge par le montant de ces transactions.
