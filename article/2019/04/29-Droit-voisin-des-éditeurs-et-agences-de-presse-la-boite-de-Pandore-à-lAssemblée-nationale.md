---
site: Next INpact
title: "Droit voisin des éditeurs et agences de presse: la boite de Pandore à l'Assemblée nationale"
author: Marc Rees
date: 2019-04-29
href: https://www.nextinpact.com/news/107827-droit-voisin-editeurs-et-agences-presse-boite-pandore-a-lassemblee-nationale.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/17617.jpg
tags:
- Droit d'auteur
- Institutions
series:
- 201918
---

> Les premiers amendements autour de la proposition de loi sur les droits voisins ont été déposés à l’Assemblée nationale. Le texte d’origine socialiste, déjà adopté à l'unanimité au Sénat, doit être adapté pour tenir compte du vote, intervenu entre-temps, de la directive sur le droit d’auteur. Tour d’horizon.
