---
site: Next INpact
title: "Clauses abusives: victoire de l'UFC contre Facebook, ce que disent les 293 pages du jugement"
author: Xavier Berne
date: 2019-04-11
href: https://www.nextinpact.com/news/107780-clauses-abusives-victoire-ufc-contre-facebook-ce-que-disent-293-pages-jugement.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/22328.jpg
tags:
- Entreprise
- Vie privée
series:
- 201915
---

> Twitter, Google, et maintenant Facebook. Après cinq années de procédure, l’UFC-Que Choisir a réussi à obtenir la condamnation du célèbre réseau social, pour de multiples violations du Code de la consommation et de la loi «Informatique et libertés». Explications.
