---
site: Le Républicain Lorrain
title: "Informatique à Insming: les bons conseils pour bien surfer"
date: 2019-04-09
href: https://www.republicain-lorrain.fr/edition-de-sarrebourg-chateau-salins/2019/04/09/informatique-les-bons-conseils-pour-bien-surfer
featured_image: https://cdn-s-www.republicain-lorrain.fr/images/49A1FF97-0021-463D-974C-BDEC38AD865B/LRL_v0_03/de-nombreux-seniors-pourraient-rejoindre-les-rangs-photo-rl-1554736758.jpg
tags:
- Associations
- april
- Promotion
series:
- 201915
series_weight: 0
---

> L’Association de formation informatique sur logiciels libres va permettre aux personnes débutantes de s’initier sans complexe et dans la bonne humeur à l’informatique. Elle est ouverte à tous et de nombreux adhérents devraient y trouver de bons conseils.
