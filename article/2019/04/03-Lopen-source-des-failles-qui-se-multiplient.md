---
site: Techniques de l'Ingénieur
title: "L'open source: des failles qui se multiplient"
author: Philippe Richard
href: https://www.techniques-ingenieur.fr/actualite/articles/lopen-source-des-failles-qui-se-multiplient-64675
featured_image: https://cdn.techniques-ingenieur.fr/lucy/ti/cdn.prod.v1.3.0/images/logo-eti.png
tags:
- Innovation
- Sensibilisation
series:
- 201914
series_weight: 0
---

> De plus en plus d'entreprises s'appuient sur la richesse de l'open source pour développer leurs propres applications. Mais l'open source est, au même titre que les logiciels propriétaires, touché par des failles de sécurité. Faute de temps, de moyens ou d'expérience, des développeurs ne corrigent pas ou ne détectent pas ces vulnérabilités.
