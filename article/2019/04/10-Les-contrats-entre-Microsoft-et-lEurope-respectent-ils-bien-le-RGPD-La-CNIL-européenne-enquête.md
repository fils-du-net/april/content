---
site: Numerama
title: "Les contrats entre Microsoft et l'Europe respectent-ils bien le RGPD? La «CNIL européenne» enquête"
author: Julien Lausson
date: 2019-04-10
href: https://www.numerama.com/politique/479851-les-contrats-entre-microsoft-et-leurope-respectent-ils-bien-le-rgpd-la-cnil-europeenne-enquete.html
featured_image: https://www.numerama.com/content/uploads/2019/02/rgpd.jpg
tags:
- Entreprise
- Europe
series:
- 201915
---

> Le Comité européen de la protection des données lance une enquête sur les accords entre Microsoft et les institutions européennes. Objectif? Vérifier leur compatibilité avec le RGPD.
