---
site: GinjFo
title: "Distribution Scientific Linux, c'est la fin après 14 ans d'existence, détails"
author: Jérôme Gianoli
date: 2019-04-26
href: https://www.ginjfo.com/actualites/logiciels/linux/distribution-scientific-linux-cest-la-fin-apres-14-ans-dexistence-details-20190426
featured_image: https://www.ginjfo.com/wp-content/uploads/2019/04/Scientific_Linux_7-01.jpg
tags:
- Innovation
series:
- 201917
series_weight: 0
---

> Fermilab annonce que la distribution Scientific Linux n’évoluera plus. Le projet est abandonné après 14 ans. La première version a été publiée le 10 mai 2004.
