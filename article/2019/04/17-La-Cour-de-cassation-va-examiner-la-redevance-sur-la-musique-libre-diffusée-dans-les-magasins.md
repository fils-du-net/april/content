---
site: Next INpact
title: "La Cour de cassation va examiner la redevance sur la musique libre diffusée dans les magasins"
author: Marc Rees
date: 2019-04-17
href: https://www.nextinpact.com/news/107795-la-cour-cassation-va-examiner-redevance-sur-musique-libre-diffusee-dans-magasins.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/2019.jpg
tags:
- Institutions
- Droit d'auteur
series:
- 201916
series_weight: 0
---

> Jamendo SA, Audiovalley SA (ex Musicmatic SA) et Storever France SAS (ex Musicmatic France SAS) ont formé pourvoi en cassation contre l’arrêt Saint Maclou de la cour d’appel de Paris. Arrêt consacrant l’obligation de payer la rémunération équitable même lorsqu’une enseigne diffuse de la musique libre, hors catalogue des sociétés de gestion collective
