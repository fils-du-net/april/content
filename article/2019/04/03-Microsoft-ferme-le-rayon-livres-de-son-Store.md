---
site: Les Numeriques
title: "Microsoft ferme le rayon “livres” de son Store"
description: Tous les achats remboursés
author: Mathieu Chartier 
date: 2019-04-03
href: https://www.lesnumeriques.com/loisirs/microsoft-ferme-rayon-livres-son-store-n85547.html
featured_image: https://dyw7ncnq1en5l.cloudfront.net/optim/news/85/85547/microsoft-books-techhive-900.jpg
tags:
- DRM
- Entreprise
series:
- 201914
series_weight: 0
---

> En fermant sa librairie en ligne, Microsoft va aussi priver ses clients des ouvrages pour lesquels ils avaient payé. Une décision radicale qui trahit le manque de succès de cette boutique et donnera droit à des remboursements, voire (dans certains cas) à des dédommagements.
