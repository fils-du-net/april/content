---
site: Sciences et avenir
title: "Enquête sur les logiciels de Microsoft utilisés en Europe"
date: 2019-04-08
href: https://www.sciencesetavenir.fr/high-tech/enquete-sur-les-logiciels-de-microsoft-utilises-par-l-ue_132823
featured_image: https://www.sciencesetavenir.fr/assets/img/2019/04/08/cover-r4x3w1000-5cab617c536d6-enquete-sur-les-logiciels-de-microsoft-utilises-par-l-ue.jpg
tags:
- Entreprise
- Europe
series:
- 201915
---

> Le Contrôleur européen de la protection des données a ouvert une enquête afin de déterminer si les produits et services fournis par Microsoft aux institutions de l'UE respectaient les nouvelles règles en matière de protection de la vie privée
