---
site: leJDD.fr
title: "Paula Forteza, députée LREM: 'Mes réponses à Mark Zuckerberg'"
date: 2019-04-06
href: https://www.lejdd.fr/Politique/paula-forteza-deputee-lrem-mes-reponses-a-mark-zuckerberg-3887501
featured_image: https://resize-lejdd.lanmedia.fr/rcrop/620,310/img/var/europe1/storage/images/lejdd/politique/paula-forteza-deputee-lrem-mes-reponses-a-mark-zuckerberg-3887501/52799750-1-fre-FR/Paula-Forteza-deputee-LREM-Mes-reponses-a-Mark-Zuckerberg.jpg
tags:
- Entreprise
series:
- 201915
---

> Paula Forteza, députée LREM des Français de l’étranger et spécialiste du numérique au sein de la majorité, répond aux idées et à la tribune publiée par Mark Zuckerberg, le PDG de Facebook, la semaine dernière dans le Journal du Dimanche.
