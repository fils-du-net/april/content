---
site: 20 minutes online
title: "Google a saboté Firefox, selon un ex de Mozilla"
date: 2019-04-16
href: https://www.20min.ch/ro/multimedia/stories/story/Google-a-sabote-Firefox--selon-un-ex-de-Mozilla-31394294
featured_image: https://www.20min.ch/dyim/7d1a8f/M600,1000/images/content/3/1/3/31394294/5/topelement.jpg
tags:
- Internet
- Entreprise
series:
- 201916
series_weight: 0
---

> Johnathan Nightingale accuse le géant du web d'avoir altéré l'utilisation du navigateur web ces dix dernières années afin de favoriser Google Chrome.
