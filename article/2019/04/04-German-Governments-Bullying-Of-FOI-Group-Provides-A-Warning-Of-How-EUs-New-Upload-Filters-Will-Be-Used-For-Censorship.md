---
site: Techdirt
title: "German Government's Bullying Of FOI Group Provides A Warning Of How EU's New Upload Filters Will Be Used For Censorship"
date: 2019-04-04
href: https://www.techdirt.com/articles/20190402/09114041924/german-governments-bullying-foi-group-provides-warning-how-eus-new-upload-filters-will-be-used-censorship.shtml
tags:
- Droit d'auteur
- Europe
- English
series:
- 201914
---

> One of the many concerns about the upload filters of the EU's Copyright Directive is that they could lead to censorship, even if that is not the intention. The problem is that once a filtering mechanism is in place to block unauthorized copies of materials, it is very hard to stop its scope being widened beyond copyright infringement. As it happens, the German government has just provided a good example of the kind of abuse that is likely to become a commonplace.
