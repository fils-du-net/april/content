---
site: 20 minutes online
title: "«Le mobile, je l'appelle le rêve de Staline»"
author: Michel Annese
date: 2019-04-04
href: https://www.20min.ch/ro/multimedia/stories/story/-Le-mobile--je-l-appelle-le-r-ve-de-Staline--13665778
featured_image: https://www.20min.ch/2010/img/navigation/20min_logo_ro.png
tags:
- Vie privée
- Philosophie GNU
- Promotion
series:
- 201914
series_weight: 0
---

> De passage en Suisse pour des conférences, Richard Stallman, le pape du logiciel libre, encourage à se détourner progressivement des «logiciels privateurs».
