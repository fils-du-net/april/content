---
site: ZDNet France
title: "Pourquoi nous vivons dans un monde de plus en plus open-source"
author: Steven J. Vaughan-Nichols
date: 2019-04-17
href: https://www.zdnet.fr/actualites/pourquoi-nous-vivons-dans-un-monde-de-plus-en-plus-open-source-39883585.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2015/03/opensource-600.jpg
tags:
- Entreprise
- Innovation
series:
- 201916
series_weight: 0
---

> La nouvelle enquête de Red Hat sur l'utilisation de l'informatique en entreprises révèle un monde où le mouvement de l'open-source est ultra dominant.
