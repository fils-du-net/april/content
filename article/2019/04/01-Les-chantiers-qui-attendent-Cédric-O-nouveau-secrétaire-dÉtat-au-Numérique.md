---
site: Acteurs publics
title: "Les chantiers qui attendent Cédric O, nouveau secrétaire d'État au Numérique"
author: Emile Marzolf
href: https://www.acteurspublics.com/2019/04/01/cedric-o-remplace-mounir-mahjoubi-au-secretariat-d-etat-au-numerique
featured_image: https://www.acteurspublics.com/img/uploaded/article/2019-04-5ca2150e9ed09.jpg
tags:
- Institutions
series:
- 201914
---

> L’ancien conseiller du chef de l’État, nommé secrétaire d’État en charge du numérique, a la réputation d’avoir un profil plus “start-up qu’hacktiviste”. Cédric O a repris la main dès ce lundi 1er avril sur les dossiers laissés par Mounir Mahjoubi.
