---
site: Usbek & Rica
title: "Deux tiers des Français préoccupés par leurs données personnelles"
author: Lila Meghraoua
date: 2019-04-03
href: https://usbeketrica.com/article/deux-tiers-francais-preoccupes-donnees-personnelles
featured_image: https://static.usbeketrica.com/images/thumb_840xh/5ca498a0985f1.jpg
tags:
- Vie privée
- Internet
series:
- 201914
series_weight: 0
---

> D'après une étude de la société Norton Lifelock, les scandales à répétition liés à l’accès aux données personnelles de Facebook et l’explosion du nombre de cyberattaques finissent par avoir raison du «naturisme numérique» des Français. Deux tiers d'entre eux seraient même particulièrement soucieux de la sécurité de leurs données personnelles.
