---
site: InformatiqueNews.fr
title: "Une nouvelle ère pour l'Open Source?"
author: Paul Farrington
date: 2019-04-18
href: https://www.informatiquenews.fr/une-nouvelle-ere-pour-lopen-source-paul-farrington-veracode-61374
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2019/04/18-Farrington-Veracode-.jpg
tags:
- Entreprise
series:
- 201916
---

> Le code source ouvert, dit «Open Source», émerge. Chaque société conçoit désormais ses produits et services à partir de cette méthode d’ingénierie logicielle. Lorsqu’un développeur édite une application Open Source, il signifie son intention d’accorder à la communauté tech un accès gratuit au code source, ainsi qu’une opportunité de l’enrichir et de l’améliorer. Les multiples collaborations que ces projets engendrent permettent des percées technologiques significatives tout en rendant les logiciels plus accessibles aux individus qui ne peuvent se permettre les droits de licence.
