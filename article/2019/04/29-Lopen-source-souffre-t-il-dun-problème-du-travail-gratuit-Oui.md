---
site: Developpez.com
title: "L'open source souffre-t-il d'un problème du «travail gratuit»? Oui"
description: "Selon Havoc Pennington"
author: Bill Fassinou
date: 2019-04-29
href: https://open-source.developpez.com/actu/258078/L-open-source-souffre-t-il-d-un-probleme-du-travail-gratuit-Oui-selon-Havoc-Pennington/
featured_image: https://www.developpez.net/forums/attachments/p470717d1/a/a/a
tags:
- Entreprise
- Économie
series:
- 201918
series_weight: 0
---

> Le mouvement open source s’est rapidement développé depuis sa création il y a environ vingt ans. L’open source est aujourd’hui présent dans presque tous les domaines de l’informatique et les entreprises technologiques s’y intéressent de plus en plus. Les solutions open source sont désormais au même rang que les solutions propriétaires dans le paysage des logiciels du secteur public. Les décideurs effectuent d’ailleurs de plus en plus leur choix à partir d’un jugement éclairé, en comparant systématiquement solutions propriétaires et solutions libres.
