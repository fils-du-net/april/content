---
site: Next INpact
title: "Le gouvernement ne prévoit pas de toucher à la «loi CADA»"
date: 2019-04-17
href: https://www.nextinpact.com/brief/le-gouvernement-ne-prevoit-pas-de-toucher-a-la---loi-cada---8454.htm
featured_image: https://cdn2.nextinpact.com/images/bd/wide-linked-media/13492.jpg
tags:
- Open Data
series:
- 201916
series_weight: 0
---

> Interpellé par une sénatrice qui s'inquiétait du fait que certaines collectivités territoriales refusent de communiquer des documents administratifs en dépit du feu vert de la Commission d'accès aux documents administratifs (CADA), le ministre de l'Intérieur vient de laisser entendre que le gouvernement n'envisageait pas de toucher à ce pan de la législation.
