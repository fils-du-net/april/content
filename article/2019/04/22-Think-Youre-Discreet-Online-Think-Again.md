---
site: The New York Times
title: "Think You're Discreet Online? Think Again"
author: Zeynep Tufekci
date: 2019-04-22
href: https://www.nytimes.com/2019/04/21/opinion/computational-inference.html
featured_image: https://static01.nyt.com/images/2019/04/21/opinion/sunday/21tufekci-privacy/4a84828ed823463eadf2c778d9ab4c24-superJumbo.jpg
tags:
- Vie privée
- English
series:
- 201917
---

> Thanks to “data inference” technology, companies know more about you than you disclose.
