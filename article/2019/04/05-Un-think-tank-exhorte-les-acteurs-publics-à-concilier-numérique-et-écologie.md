---
site: Acteurs publics
title: "Un “think tank” exhorte les acteurs publics à concilier numérique et écologie"
author: Emile Marzolf
date: 2019-04-05
href: https://www.acteurspublics.com/2019/04/05/un-think-tank-exhorte-les-acteurs-publics-a-concilier-numerique-et-ecologie
featured_image: https://www.acteurspublics.com/img/uploaded/article/2019-04-5ca7397a08c06.jpeg
tags:
- Internet
- Institutions
series:
- 201914
---

> La Fondation Internet nouvelle génération a publié hier son “agenda pour un futur numérique et écologique”. Elle y développe des pistes pour que les acteurs publics fassent du numérique un levier de la transition écologique.
