---
site: Next INpact
title: "Directive Copyright: le ministère de la Culture enjolive la mission Reconnaissance des contenus"
author: Marc Rees
date: 2019-04-01
href: https://www.nextinpact.com/news/107753-directive-copyright-ministere-culture-enjolive-mission-reconnaissance-contenus.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/5762.jpg
tags:
- Droit d'auteur
- Institutions
series:
- 201914
---

> Imbroglio au Conseil supérieur de la propriété littéraire et artistique. Le fameux CSPLA vient de corriger la lettre de mission sur la reconnaissance de contenus en adressant une nouvelle version à ses membres, d'apparence beaucoup plus nuancée. 
