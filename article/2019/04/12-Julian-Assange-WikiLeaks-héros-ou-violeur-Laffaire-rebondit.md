---
site: lintern@ute
title: "Julian Assange (WikiLeaks): héros ou violeur? L'affaire rebondit"
date: 2019-04-12
href: https://www.linternaute.com/actualite/biographie/1776840-julian-assange-wikileaks-heros-ou-violeur-l-affaire-rebondit
featured_image: https://img-4.linternaute.com/0e5wHyqKLWTN6g9VC2TnLpkbR6I=/540x/smart/b29f7a9926e549df8f4228b645a516ce/ccmcms-linternaute/11104316.jpg
tags:
- Institutions
series:
- 201915
---

> La fin de l'asile politique a tout changé. Le cofondateur de WikiLeaks doit à nouveau faire face à de nombreuses accusations, au Royaume-Uni, aux Etats-Unis et en Suède.
