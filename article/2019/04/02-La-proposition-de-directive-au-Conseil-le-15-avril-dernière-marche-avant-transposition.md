---
site: Next INpact
title: "La proposition de directive au Conseil le 15 avril, dernière marche avant transposition"
date: 2019-04-02
href: https://www.nextinpact.com/brief/la-proposition-de-directive-au-conseil-le-15-avril--derniere-marche-avant-transposition-8311.htm
featured_image: https://cdn2.nextinpact.com/images/bd/wide-linked-media/6446.jpg
tags:
- Droit d'auteur
- Europe
- Institutions
series:
- 201914
---

> Le Conseil examinera le 15 avril à Luxembourg la proposition de directive sur le droit d'auteur et les droits voisins à l'heure du numérique. L'épisode est la dernière étape avant la finalisation du processus d'adoption, laissant place, le cas échéant, à la phase de transposition dans chacun des États membres.
