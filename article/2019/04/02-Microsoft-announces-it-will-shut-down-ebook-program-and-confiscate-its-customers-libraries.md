---
site: Boing Boing
title: "Microsoft announces it will shut down ebook program and confiscate its customers' libraries"
author: Cory Doctorow
date: 2019-04-02
href: https://boingboing.net/2019/04/02/burning-libraries.html
featured_image: https://boingboing.net/wp-content/themes/bng/i/logo.png
tags:
- DRM
- English
series:
- 201914
---

> Microsoft has a DRM-locked ebook store that isn't making enough money, so they're shutting it down and taking away every book that every one of its customers acquired effective July 1. Customers will receive refunds. This puts the difference between DRM-locked media and unencumbered media into sharp contrast. I have bought a lot of MP3s&hellip;
