---
site: ouest-france.fr
title: "La réforme du droit d’auteur numérique définitivement validée par l'Union européenne"
date: 2019-04-15
href: https://www.ouest-france.fr/medias/la-reforme-du-droit-d-auteur-europeen-definitivement-validee-6310459
featured_image: https://media.ouest-france.fr/v1/pictures/cdb04270aeb295548a98f9c0212ffc1b-la-reforme-du-droit-d-auteur-europeen-definitivement-validee.jpg
tags:
- Droit d'auteur
- Europe
series:
- 201916
---

> La directive européenne destinée à moderniser le droit d’auteur a été définitivement validée, ce lundi 15 avril, après un vote des ministres de l’Union européenne.
