---
site: Le Monde.fr
title: "Des données de 540 millions d'utilisateurs de Facebook librement accessibles"
date: 2019-04-04
href: https://www.lemonde.fr/pixels/article/2019/04/04/les-donnees-de-540-millions-d-utilisateurs-de-facebook-librement-accessibles_5445690_4408996.html
featured_image: https://img.lemde.fr/2019/03/28/0/0/4381/2920/688/0/60/0/4827e8f_a52c05d0c61b48b18017569f76191024-a52c05d0c61b48b18017569f76191024-0.jpg
tags:
- Vie privée
series:
- 201914
---

> Récoltées par des entreprises tierces, ces données étaient stockées sur des serveurs accessibles sur Internet sans aucune protection.
