---
site: Next INpact
title: "La directive Droit d'auteur passe définitivement le cap européen"
author: Marc Rees
date: 2019-04-15
href: https://www.nextinpact.com/news/107791-la-directive-droit-dauteur-passe-definitivement-cap-europeen.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/22603.jpg
tags:
- Droit d'auteur
- Europe
series:
- 201916
---

> Les États membres ont adopté aujourd’hui la directive sur le droit d’auteur. Le texte est désormais fin prêt pour être transposé dans chacun des pays européens dans le délai imparti. La France, à l’avant-poste, dispose déjà d’une sérieuse longueur d’avance s’agissant de la redevance frappant les sites au profit des éditeurs de presse.
