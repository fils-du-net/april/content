---
site: Le Courrier
title: "La liberté à l'ère des GAFA"
author: Philippe Bach
date: 2019-04-10
href: https://lecourrier.ch/2019/04/10/la-liberte-a-lere-des-gafa
featured_image: https://lecourrier.ch/app/uploads/2019/04/la-liberte-a-lere-des-gafa-936x546.jpg
tags:
- Entreprise
- Vie privée
- Promotion
- Vote életronique
series:
- 201915
series_weight: 0
---

> Infatigable activiste du logiciel libre, Richard Stallman est en Suisse romande pour une série de conférences sur les géants d’internet, les GAFA. Il sera jeudi soir à l’EPFL à Lausanne.
