---
site: Acteurs publics
title: "Une nouvelle tête pensante pour l'action publique et le numérique à l'Élysée"
author: Bastien Scordia
href: https://www.acteurspublics.com/2019/04/01/une-nouvelle-tete-pensante-pour-l-action-publique-et-le-numerique-a-l-elysee
featured_image: https://www.acteurspublics.com/img/uploaded/article/2019-04-5ca21bfe06d0d.jpg
tags:
- Institutions
series:
- 201914
---

> Le magistrat de la Cour des comptes Mohammed Adnène Trojette, expert des questions numériques, va reprendre le dossier de l’action publique et du numérique au cabinet d’Emmanuel Macron, suite à la nomination au gouvernement de Cédric O, ex-conseiller “participations publiques et économie numérique” et au départ de Fabrice Aubert, conseiller “institutions, action publique et transition numérique”.
