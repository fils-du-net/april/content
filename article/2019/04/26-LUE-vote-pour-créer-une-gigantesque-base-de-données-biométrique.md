---
site: ZDNet France
title: "L'UE vote pour créer une gigantesque base de données biométrique"
author: Catalin Cimpanu
date: 2019-04-26
href: https://www.zdnet.fr/actualites/l-ue-vote-pour-creer-une-gigantesque-base-de-donnees-biometrique-39883995.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/04/eu-flag-biometrics2.jpg
tags:
- Vie privée
- Europe
series:
- 201917
series_weight: 0
---

> Le Parlement européen donne son feu vert à la création du Common Identity Repository (CIR), une gigantesque base de données biométrique.
