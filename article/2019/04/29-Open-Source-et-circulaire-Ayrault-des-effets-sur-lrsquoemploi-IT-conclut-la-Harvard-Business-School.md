---
site: LeMagIT
title: "Open Source et circulaire Ayrault: des effets sur l'emploi IT, conclut la Harvard Business School"
author: Cyrille Chausson
date: 2019-04-29
href: https://www.lemagit.fr/actualites/252462413/Open-Source-et-circulaire-Ayrault-des-effets-sur-lemploi-IT-conclut-la-Harvard-Business-School
featured_image: https://cdn.ttgtmedia.com/visuals/German/article/open-source-2-fotolia.jpg
tags:
- Administration
series:
- 201918
series_weight: 0
---

> La circulaire Ayrault de 2012 aurait eu des effets sur le nombre de contributions et de contributeurs individuels en France mais aussi sur des enjeux sociétaux comme le nombre de start-ups et l’emploi IT. Harvard Business School y voit un moteur social et de compétitivité nationale.
