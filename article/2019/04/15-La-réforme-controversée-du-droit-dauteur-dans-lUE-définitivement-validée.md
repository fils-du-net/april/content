---
site: Le Monde.fr
title: "La réforme controversée du droit d'auteur dans l'UE définitivement validée"
date: 2019-04-15
href: https://www.lemonde.fr/pixels/article/2019/04/15/la-reforme-controversee-du-droit-d-auteur-dans-l-ue-definitivement-validee_5450413_4408996.html
featured_image: https://img.lemde.fr/2018/09/12/0/0/3500/2337/688/0/60/0/13bd3ba_4pkCQJUVWCIBtq75kRdPsln_.PNG
tags:
- Droit d'auteur
- Europe
series:
- 201916
series_weight: 0
---

> Cette validation intervient après deux années de négociations très difficiles de la réforme, sur fond de lobbyisme de la part de ses partisans comme de ses opposants.
