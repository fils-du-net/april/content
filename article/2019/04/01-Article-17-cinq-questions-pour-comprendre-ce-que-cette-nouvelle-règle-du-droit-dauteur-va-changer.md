---
site: 01net.
title: "Article 17: cinq questions pour comprendre ce que cette nouvelle règle du droit d'auteur va changer"
author: Amélie Charnay
date: 2019-04-01
href: https://www.01net.com/actualites/article-17-cinq-questions-pour-comprendre-ce-que-cette-nouvelle-regle-du-droit-d-auteur-va-changer-1661380.html
featured_image: https://img.bfmtv.com/c/630/420/e73/334f95d8e12c3a03c76b68ddb1990.jpeg
tags:
- Droit d'auteur
- Europe
- Internet
series:
- 201914
series_weight: 0
---

> Les députés européens viennent de voter une nouvelle directive droit d'auteur pour mieux protéger les œuvres en ligne. Mais l'article 13 du texte, devenu article 17 dans la version finale, suscite la polémique. On vous explique pourquoi.
