---
site: estrepublicain.fr
title: "Julian Assange: ce qu'on lui reproche, ce qu'il risque"
date: 2019-04-12
href: https://www.estrepublicain.fr/faits-divers/2019/04/12/julian-assange-ce-qu-on-lui-reproche-ce-qu-il-risque
featured_image: https://cdn-s-www.estrepublicain.fr/images/217650B0-91E0-4D80-B325-D2A6F614BBA4/LER_v1_04/julian-assange-juste-apres-son-arrestation-ce-jeudi-photo-afp-1555013662.jpg
tags:
- Institutions
series:
- 201915
series_weight: 0
---

> Réfugié depuis 2 487 jours dans l’ambassade d’Equateur à Londres, le fondateur de WikiLeaks Julian Assange a finalement été arrêté ce jeudi par Scotland Yard.
