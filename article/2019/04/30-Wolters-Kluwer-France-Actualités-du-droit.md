---
site: Actualités du droit
title: "Marchés informatiques: l’acheteur ne doit pas favoriser le concepteur du logiciel propriétaire"
author: Christine Emlek
date: 2019-04-30
href: https://www.actualitesdudroit.fr/browse/public/droit-public-des-affaires/21433/marches-informatiques-l-acheteur-ne-doit-pas-favoriser-le-concepteur-du-logiciel-proprietaire
featured_image: https://webservices.wkf.fr/editorial/medias/images/actu-67780-marches-informatiques-.jpg
tags:
- Marchés publics
- Logiciels privateurs
series:
- 201918
---

> Le tribunal administratif de Strasbourg a, dans une décision du 16 avril 2019, jugé que lorsqu’une collectivité souhaite imposer, dans le cadre d’un marché, l’utilisation d’un logiciel propriétaire, elle ne peut favoriser le concepteur du logiciel par rapport à ses concurrents, sous peine de voir sa procédure de passation annulée.
