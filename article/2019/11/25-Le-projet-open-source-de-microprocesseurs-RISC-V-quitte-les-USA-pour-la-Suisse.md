---
site: ICTjournal
title: "Le projet open source de microprocesseurs RISC-V quitte les USA pour la Suisse"
author: Yannick Chavanne
date: 2019-11-25
href: https://www.ictjournal.ch/articles/2019-11-25/le-projet-open-source-de-microprocesseurs-risc-v-quitte-les-usa-pour-la-suisse
featured_image: https://www.ictjournal.ch/sites/default/files/styles/np8_full/public/media/2019/11/25/flag-999687_1920.jpg
tags:
- International
series:
- 201948
series_weight: 0
---

> La fondation RISC-V, qui développe une architecture open source de microprocesseurs, désire relocaliser son siège en Suisse par craintes d'éventuelles restrictions commerciales imposées par les Etats-Unis. La fondation compte parmi ses membres des compagnies chinoises dont Huawei.
