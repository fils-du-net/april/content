---
site: Mode(s) d'Emploi 
title: "Recrutement et méthodes de travail: ce que la communauté open source nous apprend"
author: Stéphanie Davalo
date: 2019-11-14
href: https://www.blog-emploi.com/recrutement-open-source
featured_image: https://46db671t3c2iwfvrg2i342sw-wpengine.netdna-ssl.com/wp-content/uploads/2019/11/blendwebmix.jpg
tags:
- Entreprise
series:
- 201946
series_weight: 0
---

> Dédié cette année aux questions liées à la 'tech for good', le Blend Web Mix est aussi l'occasion d'aborder des sujets comme l'organisation du travail. Lors de sa conférence, Bertrand Delacretaz, Principal Scientist chez Adobe à Bâle, s'est demandé si l'open source changeait vraiment le monde. De quoi nourrir quelques réflexions sur le monde du travail!
