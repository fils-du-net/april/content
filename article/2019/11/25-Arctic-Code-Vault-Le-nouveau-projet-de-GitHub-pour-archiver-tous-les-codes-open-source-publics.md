---
site: FZN
title: "Arctic Code Vault: Le nouveau projet de GitHub pour archiver tous les codes open-source publics"
author: Richard
date: 2019-11-25
href: https://www.fredzone.org/arctic-code-vault-le-nouveau-projet-de-github-pour-archiver-tous-les-codes-open-source-publics-998
featured_image: https://www.fredzone.org/wp-content/uploads/2017/10/github.jpg
tags:
- Innovation
- Entreprise
series:
- 201948
series_weight: 0
---

> GitHub, la fameuse plateforme de développement prévoit de lancer un projet d’envergure dénommé «Arctic Code Vault». L’idée consiste à stocker et à préserver tous les logiciels open source dans le monde, dans un lieu sûr, à l’abri des intempéries pour une durée estimée à 1000 ans.
