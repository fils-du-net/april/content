---
site: BFMtv
title: "Nom de domaine historique, le «.org» est-il en danger?"
author: Elsa Trujillo
date: 2019-11-17
href: https://www.bfmtv.com/tech/inquietudes-sur-l-avenir-de-l-un-des-plus-vieux-noms-de-domaine-le-org-1806917.html
featured_image: https://www.bfmtv.com/i/0/0/6f8/bb499e240ae63fdd880ff76ebe493.jpeg
tags:
- Internet
series:
- 201946
---

> Le «.org», longtemps détenu par une association, passe entre les mains d'un fonds d'investissement privé. Derrière cette passation, des doutes surgissent quant à l'avenir de ce nom de domaine presque aussi vieux que le Web.
