---
site: Numerama
title: "«.org»: l'un des plus vieux domaines du net en danger?"
author: Julien Lausson
date: 2019-11-14
href: https://www.numerama.com/tech/572175-org-lun-des-plus-vieux-domaines-du-web-en-danger.html
featured_image: https://www.numerama.com/content/uploads/2019/11/dotorg-org-nom-de-domaine.jpg
tags:
- Internet
- april
series:
- 201946
series_weight: 0
---

> Le célèbre domaine «.org», qui s'adresse avant tout aux organisations à but non lucratif, va connaître un important bouleversement. Administré jusqu'à présent par un organisme sans but lucratif, il va passer sous la coupe d'une société à but lucratif. L'opération suscite stupeurs et suspicions.
