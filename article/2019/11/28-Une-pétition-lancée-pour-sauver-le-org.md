---
site: 01net.
title: "Une pétition lancée pour «sauver» le .org"
author: Amélie Charnay
date: 2019-11-28
href: https://www.01net.com/actualites/une-petition-lancee-pour-sauver-le-org-1814728.html
featured_image: https://img.bfmtv.com/c/630/420/36b/76fc890008c3771ce83c5e30f0ee7.jpg
seeAlso: "[Sauvons le .org!](https://www.april.org/sauvons-le-org)"
tags:
- Internet
series:
- 201948
series_weight: 0
---

> Le Public Interest Registry, qui gère les noms de domaine en .org, va être vendu à la société d'investissement Ethos Capital. Ce qui révolte les organisations qui en dépendent.
