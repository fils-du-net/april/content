---
site: Le Monde Informatique
title: "Les logiciels libres progressent dans les collectivités territoriales"
author: Aurélie Chandeze
date: 2019-11-25
href: https://www.lemondeinformatique.fr/actualites/lire-les-logiciels-libres-progressent-dans-les-collectivites-territoriales-77171.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000069384.jpg
tags:
- Administration
- april
- Open Data
series:
- 201948
series_weight: 0
---

> Pour l'édition 2019 de la remise des labels Territoire Numérique Libre, initiative de l'Adullact, 31 collectivités et organismes mutualisés ont vu leur engagement en faveur des logiciels libres récompensé par un label.
