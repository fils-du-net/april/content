---
site: Midi Libre
title: "Le 1er Café numérique à la Ruche: le web dans tous ses états…"
date: 2019-11-16
href: https://www.midilibre.fr/2019/11/16/le-1er-cafe-numerique-a-la-ruche-le-web-dans-tous-ses-etats,8544227.php
featured_image: https://images.midilibre.fr/api/v1/images/view/5dcf52f58fe56f7a222c4f74/large/image.jpg
tags:
- Sensibilisation
- Associations
- Internet
series:
- 201946
series_weight: 0
---

> L’association Culture numérique Occitanie (CN’Oc) a organisé il y a quelques jours son premier café numérique à la Ruche sur le thème de la captation des données personnelles par les géants du web, et des logiciels libres pour limiter cette mainmise sur internet, intitulé “Vos données valent de l’or” dans les locaux de la Ruche.
