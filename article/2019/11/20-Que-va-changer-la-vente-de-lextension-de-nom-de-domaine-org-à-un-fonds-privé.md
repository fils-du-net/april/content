---
site: Le Figaro.fr
title: "Que va changer la vente de l'extension de nom de domaine .org à un fonds privé?"
author: Samuel Kahn
date: 2019-11-20
href: https://www.lefigaro.fr/secteur/high-tech/que-va-changer-la-vente-de-l-extension-de-nom-de-domaine-org-a-un-fonds-prive-20191120
featured_image: https://i.f1g.fr/media/eidos/767x431_crop/2019/11/20/XVM5bb32bac-0a28-11ea-8243-0d97cc684455.jpg
tags:
- Internet
series:
- 201947
series_weight: 0
---

> Il était jusqu’à présent administré par The Internet Society, une organisation américaine à but non lucratif.
