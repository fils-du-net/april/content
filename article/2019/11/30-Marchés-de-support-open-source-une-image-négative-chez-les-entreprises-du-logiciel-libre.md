---
site: ZDNet France
title: "Marchés de support open source: une image négative chez les entreprises du logiciel libre"
author: Thierry Noisette
date: 2019-11-30
href: https://www.zdnet.fr/blogs/l-esprit-libre/marches-de-support-open-source-une-image-negative-chez-les-entreprises-du-logiciel-libre-39895051.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2019/11/argent-etau-resserrement_Pixabay.jpg
tags:
- Entreprise
series:
- 201948
---

> Ces marchés ne sont généralement pas adaptés à l'offre des entreprises du logiciel libre, selon une étude du CNLL.
