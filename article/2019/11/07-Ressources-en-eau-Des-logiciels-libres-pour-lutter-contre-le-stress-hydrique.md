---
site: LesEco.ma
title: "Ressources en eau. Des logiciels libres pour lutter contre le stress hydrique"
author: Aziz Diouf
date: 2019-11-07
href: https://www.leseco.ma/economie/82068-ressources-en-eau-des-logiciels-libres-pour-lutter-contre-le-stress-hydrique.html
featured_image: https://www.leseco.ma/images/stories/Ressources_en_eau.jpg
tags:
- International
series:
- 201945
---

> C'est l'expérience que vient de montrer la Division des sciences de l'eau de l'UNESCO lors de la 4e édition de l'Open Water Symposium qu'elle a organisé du 28 octobre au 1er novembre à Rabat.
