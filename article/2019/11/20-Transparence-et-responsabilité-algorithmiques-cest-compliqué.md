---
site: ICTjournal
title: "Transparence et responsabilité algorithmiques: c’est compliqué…"
author: Rodolphe Koller
date: 2019-11-20
href: https://www.ictjournal.ch/articles/2019-11-20/transparence-et-responsabilite-algorithmiques-cest-complique
featured_image: https://www.ictjournal.ch/sites/default/files/styles/np8_full/public/media/2019/11/18/window_clean_mike-ledray-fotolia_24893124_xl.jpg
tags:
- Innovation
- Open Data
series:
- 201947
series_weight: 0
---

> La transparence des algorithmes est un exercice compliqué et contraignant, comme le montre l’expérience de la plateforme française Parcoursup. Idem pour la question de qui est responsable de l’éthique des IA, ces dernières étant le produit d’une chaîne complexe de rôles, activités et composants.
