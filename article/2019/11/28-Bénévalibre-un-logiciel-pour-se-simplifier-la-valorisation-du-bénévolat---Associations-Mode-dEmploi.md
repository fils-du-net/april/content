---
site: Association mode d'emploi
title: "Bénévalibre: un logiciel pour se simplifier la valorisation du bénévolat"
author: Laurent Costy
date: 2019-11-28
href: https://www.associationmodeemploi.fr/article/benevalibre-libere-le-benevolat.70074
featured_image: https://www.associationmodeemploi.fr/mediatheque/9/7/0/000012079_600x400_c.jpg
tags:
- Associations
series:
- 201948
series_weight: 0
---

> Le Crajep (Comité régional des associations de jeunesse et d’éducation populaire) de Bourgogne-Franche-Comté vient de présenter la première version de Bénévalibre. Ce logiciel libre et gratuit a été développé pour faciliter la valorisation du bénévolat dans les associations.
