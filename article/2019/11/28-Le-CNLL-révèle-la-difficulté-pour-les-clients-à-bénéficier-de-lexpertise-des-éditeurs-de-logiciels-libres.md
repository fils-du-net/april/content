---
site: Programmez!
title: "Le CNLL révèle la difficulté pour les clients à bénéficier de l'expertise des éditeurs de logiciels libres"
author: fredericmazue
date: 2019-11-28
href: https://www.programmez.com/actualites/le-cnll-revele-la-difficulte-pour-les-clients-beneficier-de-lexpertise-des-editeurs-de-logiciels-29653
tags:
- Entreprise
series:
- 201948
series_weight: 0
---

> Le CNLL (Union des Entreprises du Logiciel Libre et du Numérique Ouvert) présente une synthèse de la consultation menée auprès des entreprises du logiciel libre (ENL) sur les grands marchés de support open source (GMSOS) et les appels d'offres afférents.
