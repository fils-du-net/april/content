---
site: L'ADN 
title: "Comment Lush rend ses pratiques numériques plus éthiques"
author: Marine Protais
date: 2019-11-07
href: https://www.ladn.eu/tech-a-suivre/lush-numerique-ethique-case-study
featured_image: https://www.ladn.eu/wp-content/uploads/2019/11/Lush-smartphone-fairphone-1140x480.png
tags:
- Entreprise
series:
- 201945
series_weight: 0
---

> La marque de cosmétique Lush a notamment entrepris de fabriquer ses propres tablettes tactiles pour contrôler les conditions dans lesquelles elles sont produites.
