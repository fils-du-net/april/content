---
site: Le Monde Informatique
title: "L'open source et le syndrome du parasite"
author: Dries Buytaert
date: 2019-11-11
href: https://www.lemondeinformatique.fr/actualites/lire-l-open-source-et-le-syndrome-du-parasite-77030.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000069186.jpg
tags:
- Économie
- Entreprise
series:
- 201946
series_weight: 0
---

> Un projet open source est à la fois un bien commun et un bien public. Une dichotomie idéale pour la présence de parasites, qui veulent utiliser la technologie sans y participer ou capter des clients en contribuant au projet. Il existe cependant des moyens pour remédier à ce syndrome.
