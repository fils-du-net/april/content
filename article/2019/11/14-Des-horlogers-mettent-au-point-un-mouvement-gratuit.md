---
site: 24 heures
title: "Des horlogers mettent au point un mouvement gratuit"
description: Les plans de ce mouvement, conçu en «open source», seront mis librement à disposition.
author: Ivan Radja
date: 2019-11-14
href: https://www.24heures.ch/economie/horlogers-mettent-point-mouvement-gratuit/story/23094732
featured_image: https://files.newsnetz.ch/story/2/3/0/23094732/5/topelement.jpg
tags:
- Matériel libre
series:
- 201946
series_weight: 0
---

> Difficile, pour ne pas dire impossible, pour des petites marques indépendantes de se passer des services d’ETA, qui appartient à Swatch Group, ou de Sellita, les deux principaux fournisseurs de mouvements horlogers. La mise au point de ses propres calibres est une opération de longue haleine, excessivement coûteuse, que ne peuvent se permettre que de très grandes maisons, comme Audemars Piguet par exemple.
