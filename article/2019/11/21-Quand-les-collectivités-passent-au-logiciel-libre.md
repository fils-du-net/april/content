---
site: Archimag
title: "Quand les collectivités passent au logiciel libre"
date: 2019-11-21
href: https://www.archimag.com/vie-numerique/2019/11/21/quand-collectivit%C3%A9s-passent-logiciel-libre
featured_image: https://www.archimag.com/sites/archimag.com/files/styles/article/public/web_articles/image/congres_maires.jpeg
tags:
- Administration
series:
- 201947
series_weight: 0
---

> A l'occasion du Congrès des Maires qui se tient à Paris, l'Adullact a remis ses labels 'Territoire numérique libre' aux collectivités les plus engagées dans le mouvement du logiciel libre.
