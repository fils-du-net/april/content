---
site: LeMagIT
title: "Jim Zemlin: «La seule chose qui peut ralentir l’open source, ce sont les problèmes de sécurité»"
author: Gaétan Raoul
date: 2019-11-07
href: https://www.lemagit.fr/tribune/Jim-Zemlin-La-seule-chose-qui-peut-ralentir-lopen-source-ce-sont-les-problemes-de-securite
featured_image: https://cdn.ttgtmedia.com/visuals/LeMagIT/jim_zemlin_linux.jpg
tags:
- Innovation
series:
- 201945
series_weight: 0
---

> Lors de l’Open Source Summit Europe se déroulant à Lyon, Jim Zemlin, le directeur de la Fondation Linux a fortement appuyé sur la nécessité de relever le niveau de sécurité des systèmes et logiciels open source.
