---
site: 01net.
title: "RISC V déménage en Suisse pour échapper à la guerre commerciale sino-américaine"
author: Adrian Branco
date: 2019-11-27
href: https://www.01net.com/actualites/risc-v-demenage-en-suisse-pour-echapper-a-la-guerre-commerciale-sino-americaine-1813890.html
featured_image: https://img.bfmtv.com/c/630/420/b58/eda0551053407853755497cf83c0c.jpg
tags:
- International
series:
- 201948
---

> Après le précédent de la suspension de la licence ARM de Huawei par l'administration Trump, la fondation qui gère l'architecture processeur open source RISC V a décidé de se domicilier en Suisse.
