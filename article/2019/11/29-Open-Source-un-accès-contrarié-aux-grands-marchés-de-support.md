---
site: Silicon
title: "Open Source: un accès contrarié aux grands marchés de support"
author: Ariane Beky
date: 2019-11-29
href: https://www.silicon.fr/open-source-acces-grands-marches-support-327739.html
featured_image: https://www.silicon.fr/wp-content/uploads/2018/09/data-business-pexels-photo-245618.jpg
tags:
- Entreprise
series:
- 201948
---

> Les grands marchés de support open source peuvent financer le développement de logiciels libres. Mais l'accés des PME reste problématique.
