---
site: Libération
title: "L'UNESCO apporte son aide au Maroc en matière de gestion des ressources hydriques"
date: 2019-11-05
href: https://www.libe.ma/L-UNESCO-apporte-son-aide-au-Maroc-en-matiere-de-gestion-des-ressources-hydriques_a113152.html
featured_image: https://www.libe.ma/photo/art/default/39085752-33798028.jpg
tags:
- International
series:
- 201945
series_weight: 0
---

> Mettre à profit les logiciels libres et/ou open source comme outils durables pour soutenir la gestion des ressources en eau dans tous ses domaines d’activités, tel a été le principal objectif de l’Open Water Symposium dédié aux pays africains et arabes, organisé du 28 au 1er novembre 2019 à Rabat par la Division des sciences de l’eau de l’UNESCO avec l’appui du Bureau de l’UNESCO pour le Maghreb.
