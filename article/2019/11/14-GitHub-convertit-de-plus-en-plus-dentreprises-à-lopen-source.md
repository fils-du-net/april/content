---
site: Les Echos
title: "GitHub convertit de plus en plus d'entreprises à l'open source (€)"
author: Anaïs Moutot
date: 2019-11-14
href: https://www.lesechos.fr/tech-medias/hightech/github-convertit-de-plus-en-plus-dentreprises-a-lopen-source-1147791
featured_image: https://media.lesechos.com/api/v1/images/view/5dcd3f793e45460731117417/1280x720/0602237006635-web-tete.jpg
tags:
- Entreprise
series:
- 201946
---

> La plate-forme californienne rachetée par Microsoft et qui héberge les travaux informatiques de 40 millions de développeurs élargit sa palette de services.
