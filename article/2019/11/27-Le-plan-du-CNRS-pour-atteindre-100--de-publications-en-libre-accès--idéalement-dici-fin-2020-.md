---
site: Next INpact
title: "Le plan du CNRS pour atteindre 100 % de publications en libre accès, «idéalement d'ici fin 2020» (€)"
author: Sébastien Gavois
date: 2019-11-27
href: https://www.nextinpact.com/news/108449-le-plan-cnrs-pour-atteindre-100-publications-en-libre-acces-idealement-dici-fin-2020.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/24343.jpg
tags:
- Sciences
- Partage du savoir
series:
- 201948
series_weight: 0
---

> Emboîtant le pas au gouvernement, le CNRS veut arriver à 100 % de publications scientifiques en libre accès, qui seront à terme regroupées sur la plateforme HAL. Pour y arriver, le Centre national de la recherche a dévoilé sa feuille de route. Elle comprend de nombreux chantiers, aussi bien sur les publications, les données et l'évaluation des chercheurs.
