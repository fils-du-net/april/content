---
site: cio-online.com
title: "Les logiciels libres progressent dans les collectivités territoriales"
author: Aurélie Chandeze
date: 2019-11-22
href: https://www.cio-online.com/actualites/lire-les-logiciels-libres-progressent-dans-les-collectivites-territoriales-11698.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000014511.jpg
tags:
- Administration
series:
- 201947
---

> Pour la quatrième édition des labels Territoire Numérique Libre, organisée par l'Adullact, 31 collectivités ont été récompensées.
