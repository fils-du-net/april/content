---
site: Le Monde Informatique
title: "Microsoft est prêt à contribuer à Java"
author: Paul Krill
date: 2019-11-04
href: https://www.lemondeinformatique.fr/actualites/lire-microsoft-est-pret-a-contribuer-a-java-76966.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000069065.jpg
tags:
- Entreprise
series:
- 201945
series_weight: 0
---

> Open Source: La firme de Redmond a rejoint le projet OpenJDK pour résoudre des petits bugs et devenir un membre influent sur Java.
