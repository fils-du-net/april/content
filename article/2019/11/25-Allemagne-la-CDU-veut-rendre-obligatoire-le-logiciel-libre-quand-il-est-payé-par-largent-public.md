---
site: ZDNet France
title: "Allemagne: la CDU veut rendre obligatoire le logiciel libre quand il est payé par l'argent public"
author: Thierry Noisette
date: 2019-11-25
href: https://www.zdnet.fr/blogs/l-esprit-libre/allemagne-la-cdu-veut-rendre-obligatoire-le-logiciel-libre-quand-il-est-paye-par-l-argent-public-39894579.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2019/11/PublicMoneyPublicCode_FSFE_logo.jpg
tags:
- Institutions
- april
- Standards
series:
- 201948
series_weight: 0
---

> La CDU, le parti d'Angela Merkel, opte pour le passage sous licence libre des logiciels développés avec un financement public.
