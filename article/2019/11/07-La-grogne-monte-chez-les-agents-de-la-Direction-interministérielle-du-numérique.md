---
site: Next INpact
title: "La grogne monte chez les agents de la Direction interministérielle du numérique (€)"
author: Xavier Berne
date: 2019-11-07
href: https://www.nextinpact.com/news/108368-la-grogne-monte-chez-agents-direction-interministerielle-numerique.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/6129.jpg
tags:
- Institutions
series:
- 201945
series_weight: 0
---

> La réorganisation de la Direction interministérielle du numérique (DINSIC), transformée la semaine dernière en «DINUM», continue de provoquer de vifs remous en interne. Les organisations syndicales n’ont pas hésité à monter au créneau pour dénoncer le manque de concertation et l'abandon de certaines missions historiques.
