---
site: Les Echos
title: "'J'étais frigoriste et je suis devenu développeur logiciel'"
author: Gwénolé Le Henaff
date: 2019-11-20
href: https://start.lesechos.fr/emploi-stages/temoignages-jobs-stages/j-etais-frigoriste-et-je-suis-devenu-developpeur-logiciel-16686.php
featured_image: https://start.lesechos.fr/images/2019/11/20/16686_1574272106_sans-titre-63_970x545p.png
tags:
- Sensibilisation
series:
- 201947
series_weight: 0
---

> Avant, il était frigoriste, un métier qu’il aimait. Mais ça ne l’a pas empêché de revenir à sa première passion: le code.
