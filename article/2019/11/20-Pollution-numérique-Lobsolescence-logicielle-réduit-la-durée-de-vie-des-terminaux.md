---
site: Archimag
title: "Pollution numérique: 'L'obsolescence logicielle réduit la durée de vie des terminaux' (€)"
date: 2019-11-20
href: https://www.archimag.com/demat-cloud/2019/11/20/pollution-numerique-obsolescence-logicielle-reduit-duree-vie-terminaux
featured_image: https://www.archimag.com/sites/archimag.com/files/styles/article/public/web_articles/image/telephone_casse.jpg
tags:
- april
- Institutions
series:
- 201947
series_weight: 0
---

> Pour Étienne Gonnu, chargé de mission affaires publiques pour l'April (association de promotion et de défense du logiciel libre), l'obsolescence logicielle doit être combattue si l'on veut réduire l'empreinte environnementale de l'informatique et, plus largement la pollution numérique. Les sénateurs viennent de s'emparer du sujet.
