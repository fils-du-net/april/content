---
site: Slice42
title: "GitHub Archive Program: préserver les logiciels Open Source pour le futur"
author: Arnaud
date: 2019-11-14
href: https://slice42.com/technologies/2019/11/github-archive-program-preserver-les-logiciels-open-source-pour-le-futur-84103
featured_image: https://slice42.com/wp-content/uploads/2019/11/Github-archive-696x418.jpg
tags:
- Entreprise
- Innovation
series:
- 201946
series_weight: 0
---

> Le service d’hébergement de code GitHub ne se contente pas, aujourd’hui, d’annoncer ses apps mobiles. Il lance également un programme original intitulé GitHub Archive Program qui, avec un accent survivaliste, entend préserver les logiciels Open Source pour les génération futures, dans la crainte d’un effondrement global.
