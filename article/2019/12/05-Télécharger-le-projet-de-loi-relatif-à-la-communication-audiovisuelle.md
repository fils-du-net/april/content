---
site: Next INpact
title: "Télécharger le projet de loi relatif à la communication audiovisuelle"
author: Marc Rees
date: 2019-12-05
href: https://www.nextinpact.com/news/108478-telecharger-leprojet-loirelatif-a-communication-audiovisuelle.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/6645.jpg
tags:
- Institutions
series:
- 201949
---

> On pourra télécharger ci-dessous le projet de loi sur l'audiovisuel. Nous reviendrons ces prochains jours sur ses principales dispositions. Le texte consacre notamment la disparition de la Hadopi et le transfert de ses compétences au CSA. Celui-ci est rebaptisé pour l'occasion Arcom, doté de nouvelles missions.
