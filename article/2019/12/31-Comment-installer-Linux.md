---
site: leParisien.fr
title: "Comment installer Linux?"
date: 2019-12-31
href: http://www.leparisien.fr/guide-shopping/pratique/comment-installer-linux-19-11-2019-8196933.php
featured_image: http://www.leparisien.fr/resizer/WmqtmYWDu3FZdXy82jzKoXvgUg4=/932x582/arc-anglerfish-eu-central-1-prod-leparisien.s3.amazonaws.com/public/5CEWJB2TW53SBBOYNLOA3MSUWU.jpg
seeAlso: "L'article concerne le [système d'exploitation GNU/Linux](https://www.gnu.org/gnu/linux-and-gnu.html) mais le nomme «Linux»"
tags:
- Sensibilisation
series:
- 202001
series_weight: 0
---

> Envie de passer au logiciel libre pour administrer votre ordinateur? Le système d'exploitation Linux est un OS (Operating System) qui est devenu très convivial, et qui ne nécessite plus de grosses connaissances informatiques.
