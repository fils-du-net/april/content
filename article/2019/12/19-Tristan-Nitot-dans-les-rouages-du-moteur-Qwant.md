---
site: Les Echos
title: "Tristan Nitot, dans les rouages du moteur Qwant (€)"
author: Sébastien Dumoulin
date: 2019-12-19
href: https://www.lesechos.fr/tech-medias/hightech/tristan-nitot-dans-les-rouages-du-moteur-qwant-1157720
featured_image: https://media.lesechos.com/api/v1/images/view/5dfb23fa3e45462dda70dff5/1280x720/0602309944352-web-tete.jpg
tags:
- Internet
series:
- 201951
series_weight: 0
---

> L'ex-président de la fondation Mozilla est, depuis peu, le directeur général du petit concurrent français de Google. Figure de la tech tricolore, Tristan Nitot apporte une caution bienvenue au moteur de recherche, récemment mis en cause sur ses compétences techniques, sa viabilité financière ou l'ambiance interne. Mais son poste pourrait être déjà menacé.
