---
site: Silicon
title: "Open Source: des développeurs toujours plus impliqués"
author: Ariane Beky
date: 2019-12-06
href: https://www.silicon.fr/open-source-developpeurs-impliques-328755.html
featured_image: https://www.silicon.fr/wp-content/uploads/2019/04/computer-developer-via-pexels.com-1181675.jpg
tags:
- Sensibilisation
series:
- 201949
series_weight: 0
---

> 6 développeurs sur 10 ont renforcé leur implication dans l'open source en 2019. L'optimisme prévaut, malgré les doutes sur l'avenir, relève DigitalOcean.
