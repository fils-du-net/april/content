---
site: Silicon
title: "Logiciels: violer un contrat de licence peut valoir contrefaçon"
author: Clément Bohic
date: 2019-12-19
href: https://www.silicon.fr/logiciels-licence-contrefacon-330277.html
featured_image: https://www.silicon.fr/wp-content/uploads/2019/12/contrefacon-logiciels.jpg
seeAlso: "[Une décision importante de la CJUE avec effet bientôt sur un cas de violation de licence libre?](https://www.april.org/une-decision-importante-de-la-cjue-avec-effet-bientot-sur-un-cas-violation-de-licence-libre)"
tags:
- Licenses
series:
- 201951
series_weight: 0
---

> La violation du contrat de licence d'un logiciel peut justifier des poursuites en contrefaçon, d'après la CJUE.
