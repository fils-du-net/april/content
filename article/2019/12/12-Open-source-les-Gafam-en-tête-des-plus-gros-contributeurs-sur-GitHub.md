---
site: Journal du Net
title: "Open source: les Gafam en tête des plus gros contributeurs sur GitHub"
author: Antoine Crochet-Damais
date: 2019-12-12
href: https://www.journaldunet.com/solutions/dsi/1487296-open-source-les-gafam-en-tete-des-plus-gros-contributeurs-sur-github
featured_image: https://img-0.journaldunet.com/1SqwGO_xRQKfDnyAEDSPjA1fVcQ=/540x/smart/5bc6d44e6a7c4263952b67a8affdce98/ccmcms-jdn/12163371.jpg
tags:
- Entreprise
- Licenses
series:
- 201950
---

> Open source: les Gafam en tête des plus gros contributeurs sur GitHub Tous se hissent en tête du palmarès dressé par une équipe de chercheurs franco-australienne... hormis Amazon. Un indicateur présenté lors de l'Open CIO Summit 2019.
