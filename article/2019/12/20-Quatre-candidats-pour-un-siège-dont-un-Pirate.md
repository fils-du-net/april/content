---
site: Le Temps
title: "Quatre candidats pour un siège, dont un Pirate"
author: Chams Iaz
date: 2019-12-20
href: https://www.letemps.ch/suisse/quatre-candidats-un-siege-dont-un-pirate
featured_image: https://assets.letemps.ch/sites/default/files/styles/np8_full/public/media/2019/12/20/file78hfbnrj59x162aoxk31.jpg
tags:
- Institutions
- International
series:
- 201951
series_weight: 0
---

> Le Parti pirate se lance dans la course à l’élection complémentaire au Conseil d’Etat de Vaud. Son candidat, Jean-Marc Vandel, mise sur la protection des données et des mesures écologiques contraignantes
