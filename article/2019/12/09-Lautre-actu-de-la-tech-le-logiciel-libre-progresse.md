---
site: Boursorama
title: "L'autre actu de la tech: le logiciel libre progresse"
date: 2019-12-09
href: https://www.boursorama.com/actualite-economique/actualites/l-autre-actu-de-la-tech-le-logiciel-libre-progresse-3faec35f457ad1faa3a48b195d26f7b4
tags:
- Entreprise
series:
- 201950
---

> Le logiciel libre qui progresse en France, des TPE/PME qui devraient plus penser à s'équiper d'un site internet: voici une sélection d'actualités de la semaine écoulée dans les nouvelles technologies.
