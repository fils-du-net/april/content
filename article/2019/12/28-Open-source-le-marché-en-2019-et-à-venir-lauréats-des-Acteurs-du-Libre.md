---
site: ZDNet France
title: "Open source: le marché en 2019 et à venir, lauréats des Acteurs du Libre"
author: Thierry Noisette
date: 2019-12-28
href: https://www.zdnet.fr/blogs/l-esprit-libre/open-source-le-marche-en-2019-et-a-venir-laureats-des-acteurs-du-libre-39896507.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2019/12/Open%20for%20business_Flickr.jpg
tags:
- Entreprise
series:
- 201952
---

> Un marché en belle croissance, qui fait de la France le premier pays open source en Europe devant l'Allemagne et le Royaume-Uni, et cinq projets ou organismes distingués par les prix Acteurs du Libre.
