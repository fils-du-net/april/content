---
site: Le courrier des maires
title: "Travailler avec l'Adullact, pour des logiciels libres au sein de la collectivité (€)"
author: Clément Pouré
date: 2019-12-13
href: http://www.courrierdesmaires.fr/84800/ladullact-pour-des-logiciels-libres-au-sein-de-la-collectivite
featured_image: http://www.courrierdesmaires.fr/wp-content/uploads/2019/12/logiciel-libre-300x214.jpg
tags:
- Administration
- Marchés publics
- Associations
series:
- 201950
series_weight: 0
---

> Délivrer les collectivités locales de la relation contractuelle - qui peut s'avérer pesante, notamment financièrement… - les liant avec les éditeurs des logiciels nécessaires au bon fonctionnement de services: tel est le credo de l'Adullact, Association des développeurs et utilisateurs de logiciels libres pour les administrations et les collectivités territoriales. Explications et mode d'emploi.
