---
site: ITforBusiness
title: "L'open source continue de croître plus vite que le marché de l'IT."
author: Laurent Delattre
date: 2019-12-12
href: https://www.itforbusiness.fr/lopen-source-continue-de-croitre-plus-vite-que-le-marche-de-lit-29365
featured_image: https://www.itforbusiness.fr/wp-content/uploads/2019/12/open-source-2-450x201.jpg
tags:
- Entreprise
- Économie
series:
- 201950
series_weight: 0
---

> Dans la foulée de l’Open Source Summit qui s’est tenu à Paris les 10 et 11 décembre 2019, plusieurs études viennent éclairer l’impact croissant du logiciel libre dans l’écosystème applicatif des entreprises.
