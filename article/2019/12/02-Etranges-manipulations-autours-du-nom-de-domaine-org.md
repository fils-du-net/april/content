---
site: Rude Baguette
title: "Etranges manipulations autours du nom de domaine .org..."
date: 2019-12-02
href: https://www.rudebaguette.com/2019/12/domaine-org-societe-commerciale-icann
featured_image: https://www.rudebaguette.com/wp-content/uploads/thumbs/domaine-org-societe-commerciale-ICANN-Rude-Baguette-39rlvely6xris7moyyrc3k.jpg
seeAlso: "[Sauvons le .org!](https://www.april.org/sauvons-le-org)"
tags:
- Internet
series:
- 201949
series_weight: 0
---

> Géré jusqu'ici par une société à but non lucratif, le domaine .org vient de passer entre les mains d'une société commerciale.
