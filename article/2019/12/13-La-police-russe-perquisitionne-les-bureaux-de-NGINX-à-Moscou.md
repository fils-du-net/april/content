---
site: ZDNet France
title: "La police russe perquisitionne les bureaux de NGINX à Moscou"
author: Catalin Cimpanu
date: 2019-12-13
href: https://www.zdnet.fr/actualites/la-police-russe-perquisitionne-les-bureaux-de-nginx-a-moscou-39895955.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/12/nginx.png
tags:
- Droit d'auteur
- Europe
series:
- 201950
series_weight: 0
---

> Le moteur de recherche russe Rambler.ru revendique la pleine propriété du code source de NGINX.
