---
site: Silicon
title: "Open Source: la France a une longueur d'avance sur ses voisins"
author: Ariane Beky
date: 2019-12-10
href: https://www.silicon.fr/open-source-france-longueur-davance-328967.html
featured_image: https://www.silicon.fr/wp-content/uploads/2019/02/open-source-obscur-684x443.jpg
tags:
- Économie
- Entreprise
series:
- 201950
series_weight: 0
---

> Paris Open Source Summit 2019: l'open source représente plus de 10% du marché logiciels et services IT en France, contre 7% en Allemagne.
