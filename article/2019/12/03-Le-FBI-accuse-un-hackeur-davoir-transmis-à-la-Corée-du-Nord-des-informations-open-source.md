---
site: Toms Guide
title: "Le FBI accuse un hackeur d'avoir transmis à la Corée du Nord des informations.... open source"
author: Guillaume Pigeard
date: 2019-12-03
href: https://www.tomsguide.fr/le-fbi-accuse-un-hackeur-davoir-transmis-a-la-coree-du-nord-des-informations-open-source
featured_image: https://cdn.tomsguide.fr/content/uploads/sites/2/2019/12/coree-du-nord-ethereum.jpg
tags:
- Partage du savoir
- International
series:
- 201949
series_weight: 0
---

> Un développeur et hackeur risque la prison aux États-Unis pour avoir donné une présentation du fonctionnement de la blockchain en Corée du Nord. Il se défend de tout transfert d'informations sensibles, tout étant open source.
