---
site: Le Monde.fr
title: "Gaël Duval, l'adepte de Linux qui veut libérer les smartphones"
author: Bastien Lion
date: 2019-12-02
href: https://www.lemonde.fr/pixels/article/2019/12/02/gael-duval-l-adepte-de-linux-qui-veut-liberer-les-smartphones_6021357_4408996.html
featured_image: https://img.lemde.fr/2019/11/06/83/0/4275/2133/1024/0/60/0/569243a_n72YiQe9fra9qs-500meLrV4.jpg
tags:
- Innovation
series:
- 201949
series_weight: 0
---

> Face au duopole de Google et d’Apple sur les systèmes d’exploitation mobiles, ce Normand développe «/e/», un système d’exploitation affranchi des Gafam.
