---
site: cio-online.com
title: "Les grands contrats de support Open Source plombent la filière"
date: 2019-12-04
href: https://www.cio-online.com/actualites/lire-les-grands-contrats-de-support-open-source-plombent-la-filiere-11731.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000014576.jpg
tags:
- Entreprise
- Vie privée
series:
- 201949
---

> Dans une enquête du CNLL, les acteurs français du logiciel libre estiment que les grands marchés de support Open Source pénalisent le secteur." 