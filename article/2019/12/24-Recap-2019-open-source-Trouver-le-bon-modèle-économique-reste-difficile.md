---
site: Le Monde Informatique
title: "Recap 2019 open source: Trouver le bon modèle économique reste difficile"
author: Maryse Gros
date: 2019-12-24
href: https://www.lemondeinformatique.fr/actualites/lire-recap-2019-open-source-trouver-le-bon-modele-economique-reste-difficile-77460.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000069854.jpg
tags:
- Économie
series:
- 201952
series_weight: 0
---

> Toujours aussi bouillonnant, le monde de l'open source prospère et voit progresser le chiffre d'affaires généré autour de ses technologies. Pourtant, dans bien des cas, l'équilibre n'est pas satisfaisant entre ceux qui développent les projets et ceux qui en tirent profit.
