---
site: Le Monde Informatique
title: "La violation d'une licence de logiciel est juridiquement une contrefaçon"
date: 2019-12-19
href: https://www.lemondeinformatique.fr/actualites/lire-la-violation-d-une-licence-de-logiciel-est-juridiquement-une-contrefacon-77462.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000069847.jpg
tags:
- Licenses
series:
- 201951
---

> La violation d'une licence de logiciel relève-t-elle du manquement contractuel ou du délit pénal de contrefaçon? La CJUE a tranché en faveur du second.
