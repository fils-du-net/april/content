---
site: Next INpact
title: "Pour la justice européenne, la violation d'une licence de logiciel est une contrefaçon (€)"
author: Marc Rees
date: 2019-12-31
href: https://www.nextinpact.com/news/108548-pour-justice-europeenne-violation-dune-licence-logiciel-est-contrefacon.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/17632.jpg
seeAlso: "[Une décision importante de la CJUE avec effet bientôt sur un cas de violation de licence libre?](https://www.april.org/une-decision-importante-de-la-cjue-avec-effet-bientot-sur-un-cas-violation-de-licence-libre)"
tags:
- Licenses
- Institutions
series:
- 202001
series_weight: 0
---

> Le 18 décembre dernier, la Cour de justice a tranché une épineuse question: la violation d’une licence de logiciels relève-t-elle de la contrefaçon ou bien du droit des contrats? L’affaire opposait la société IT Development à Free Mobile.
