---
site: Journal du Net
title: "Projets IoT: l'open source fait encore peur aux industriels"
author: Célia Garcia-Montero
date: 2019-12-09
href: https://www.journaldunet.com/ebusiness/internet-mobile/1487309-projet-iot-l-open-source-fait-encore-peur-aux-industriels
featured_image: https://img-0.journaldunet.com/5dhdcCC0thbxaVqe75x6pi2V0BQ=/540x/smart/27cc1086171a4fbeb5d7d6e16d9c060e/ccmcms-jdn/12163876.jpg
tags:
- Entreprise
series:
- 201950
---

> Projets IoT: l'open source fait encore peur aux industriels Les verticales de l'IoT font habituellement appel à des modèles propriétaires. Pourtant, entre un prix attractif et les possibilités en matière de sécurité, les avantages de l'open source sont nombreux.
