---
site: LaDepeche.fr
title: "Monflanquin. Succès pour l'install-partie de logiciels libres"
author: Marie Paule Rabez
date: 2019-12-05
href: https://www.ladepeche.fr/2019/12/05/succes-pour-linstall-partie-de-logiciels-libres,8582700.php
featured_image: https://images.ladepeche.fr/api/v1/images/view/5de883a0d286c216896c24b6/large/image.jpg
tags:
- Associations
- Promotion
series:
- 201949
series_weight: 0
---

> Les bénévoles de l’association aGeNUX sont venus samedi dernier à l’épicerie gourmande d’Aurélie et Pierre, rue Saint-Pierre, et le public les y a rejoints pour prendre des renseignements informatiques, pour installer le système GNU/Linux tout autant que des logiciels libres.
