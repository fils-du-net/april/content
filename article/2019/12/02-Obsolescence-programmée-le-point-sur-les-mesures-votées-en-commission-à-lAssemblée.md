---
site: Next INpact
title: "Obsolescence programmée: le point sur les mesures votées en commission, à l'Assemblée (€)"
author: Xavier Berne
date: 2019-12-02
href: https://www.nextinpact.com/news/108447-obsolescence-programmee-point-sur-mesures-votees-en-commission-a-lassemblee.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/2624.jpg
tags:
- Institutions
- Open Data
series:
- 201949
series_weight: 0
---

> Le projet de loi «anti-gaspillage» a été adopté la semaine dernière par les députés de la commission du développement durable. Retour sur les principales mesures adoptées, ou, au contraire, supprimées: indice de durabilité, extensions de garantie, disponibilité des pièces détachées, lutte contre l’obsolescence logicielle, etc.
