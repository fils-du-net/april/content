---
site: Les Echos
title: "Un informaticien américain accusé d'avoir initié Pyongyang à la blockchain"
author: Yann Rousseau
date: 2019-12-05
href: https://www.lesechos.fr/finance-marches/marches-financiers/un-informaticien-americain-accuse-davoir-initie-pyongyang-a-la-blockchain-1153832
featured_image: https://media.lesechos.com/api/v1/images/view/5de899e18fe56f42d27a76c7/1280x720/0602356076759-web-tete.jpg
tags:
- Partage du savoir
series:
- 201949
---

> Le FBI assure qu'un employé de la Fondation Ethereum s'est rendu en Corée du Nord en avril dernier pour enseigner à des officiels du régime de Kim Jong-un comment contourner les sanctions internationales ou blanchir de l'argent. Des accusations réfutées par le jeune informaticien et les spécialistes des cryptomonnaies.
