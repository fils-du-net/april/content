---
site: Next INpact
title: "Projet de loi « anti-gaspillage »: les (nombreuses) idées des députés (€)"
author: Xavier Berne
date: 2019-12-10
href: https://www.nextinpact.com/news/108494-projet-loi-anti-gaspillage-nombreuses-idees-deputes.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/11402.jpg
tags:
- Institutions
series:
- 201950
---

> L’Assemblée nationale a entamé hier l’examen, en séance publique, du projet de loi «anti-gaspillage». Renforcement des garanties, «droit au remplacement» des batteries par l’utilisateur, informations sur l’empreinte carbone des activités numériques... De nombreux amendements seront débattus au fil des prochains jours.
