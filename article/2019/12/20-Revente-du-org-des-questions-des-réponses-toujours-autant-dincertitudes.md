---
site: Next INpact
title: "Revente du .org: des questions, des réponses, toujours autant d'incertitudes (€)"
author: Vincent Hermann
date: 2019-12-20
href: https://www.nextinpact.com/news/108524-revente-org-questions-reponses-toujours-autant-dincertitudes.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/24603.jpg
seeAlso: "[Sauvons le .org!](https://www.april.org/sauvons-le-org)"
tags:
- Internet
series:
- 201951
series_weight: 0
---

> La revente de Public Interest Registry et du .org au fonds d'investissement Ethos fait couler de l'encre. Malgré la création d'un site dédié, l'entreprise est pressée de questions. Les réponses sont malheureusement incomplètes, les peurs principales étant loin d'être apaisées.
