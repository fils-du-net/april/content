---
site: Archimag
title: "L'open source français et européen boosté par la transformation numérique des entreprises"
date: 2019-12-16
href: https://www.archimag.com/demat-cloud/2019/12/16/open-source-fran%C3%A7ais-europeen-booste-transformation-numerique-entreprises-0
featured_image: https://www.archimag.com/sites/archimag.com/files/styles/article/public/web_articles/image/croissance_open_souce_france.jpg
tags:
- Entreprise
series:
- 201951
series_weight: 0
---

> Les chiffres de l'année 2019 et les prévisions de croissance du marché européen de l'open source ont été divulgués par Teknowlogy Group. Le marché français est l'un des plus prolifiques, il enregistre une croissance de près de 9 % en 2019.
