---
site: Le Monde.fr
title: "Pour lutter contre les GAFA, Framasoft veut aller plus loin dans la décentralisation du Web"
author: Bastien Lion
date: 2019-12-27
href: https://www.lemonde.fr/pixels/article/2019/12/27/chez-framasoft-des-chatons-pour-sortir-des-gafa_6024230_4408996.html
featured_image: https://img.lemde.fr/2019/12/27/0/0/5184/3456/688/0/60/0/47ea8aa_yJzFl7eaxhAy4yMiJV3vyCkZ.JPG
tags:
- Associations
- Internet
- Promotion
series:
- 201952
series_weight: 0
---

> Après avoir promu de nombreux outils pour «sortir de Google», l’association française de défense du logiciel libre se concentre maintenant sur des plates-formes décentralisées.
