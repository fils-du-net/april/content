---
site: Le Monde.fr
title: "A Leipzig, hackeurs et militants pour le climat font front commun"
author: Damien Leloup
date: 2019-12-30
href: https://www.lemonde.fr/pixels/article/2019/12/30/a-leipzig-hackers-et-militants-pour-le-climat-font-front-commun_6024362_4408996.html
featured_image: https://img.lemde.fr/2019/12/30/710/0/8256/4125/1024/0/60/0/17ac762_Q9vyukbEstUJgA4ZbPpCtD0x.jpg
tags:
- Innovation
series:
- 202001
series_weight: 0
---

> Le Chaos Communication Congress, plus grand événement mondial autogéré consacré à la sécurité informatique, accueillait ce week-end en Allemagne sa 36e édition.
