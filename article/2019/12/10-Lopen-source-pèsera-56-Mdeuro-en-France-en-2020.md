---
site: Le Monde Informatique
title: "L'open source pèsera 5,6 Md€ en France en 2020"
author: Maryse Gros
date: 2019-12-10
href: https://www.lemondeinformatique.fr/actualites/lire-l-open-source-pesera-5-6-mdeteuro-en-france-en-2020-77359.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000069706.jpg
tags:
- Entreprise
series:
- 201950
---

> En France, le chiffre d'affaires généré par l'open source progresse plus de deux fois plus vite que le marché IT dans son ensemble. Selon l'étude présentée ce matin lors de l'événement Paris Open Source Summit, cette croissance sera de 8,8% par an jusqu'en 2023.
