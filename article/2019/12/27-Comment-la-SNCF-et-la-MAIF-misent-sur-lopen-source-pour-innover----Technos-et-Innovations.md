---
site: L'usine Nouvelle
title: "Comment la SNCF et la MAIF misent sur l'open source pour innover"
author: Nathan Mann
date: 2019-12-27
href: https://www.usinenouvelle.com/editorial/comment-la-sncf-et-la-maif-misent-sur-l-open-source-pour-innover.N912354
featured_image: https://www.usinenouvelle.com/mediatheque/0/2/9/000779920_image_896x598/assistant-sncf.jpg
tags:
- Entreprise
series:
- 201952
series_weight: 0
---

> A l’image de la Maif et de la SNCF, certaines grandes entreprises se digitalisent et innovent en open source, convaincues par les gains de maîtrise et d’efficacité qu'apportent ces solutions.
