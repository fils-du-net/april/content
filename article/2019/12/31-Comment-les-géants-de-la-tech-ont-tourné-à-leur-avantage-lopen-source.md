---
site: ZDNet France
title: "Comment les géants de la tech ont tourné à leur avantage l'open source"
author: Thierry Noisette
date: 2019-12-31
href: https://www.zdnet.fr/blogs/l-esprit-libre/comment-les-geants-de-la-tech-ont-tourne-a-leur-avantage-l-open-source-39896585.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2019/12/Facebook%20livre%20McNamee.jpg
tags:
- Entreprise
series:
- 202001
series_weight: 0
---

> Ex-conseiller de Zuckerberg et capital-risqueur dans la tech, Roger McNamee pointe les limites de l'open source et la façon dont le Libre a fourni des briques pour développer des entreprises.
