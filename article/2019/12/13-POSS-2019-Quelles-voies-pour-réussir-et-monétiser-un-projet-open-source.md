---
site: Le Monde Informatique
title: "POSS 2019: Quelles voies pour réussir et monétiser un projet open source?"
author: Maryse Gros
date: 2019-12-13
href: https://www.lemondeinformatique.fr/actualites/lire-poss-2019-quelles-voies-pour-reussir-et-monetiser-un-projet-open-source-77390.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000069752.jpg
tags:
- Économie
- Entreprise
series:
- 201950
series_weight: 0
---

> Au cours d'une table ronde sur le Paris Open Source Summit, le 10 décembre aux Docks de Paris, co-fondateurs, architecte ou CTO des projets Symfony, Matrix, PHP et Magento ont rappelé le rôle des communautés créées autour des projets open source et la difficulté de trouver le bon modèle économique pour les entreprises créées au-dessus de ces projets.
