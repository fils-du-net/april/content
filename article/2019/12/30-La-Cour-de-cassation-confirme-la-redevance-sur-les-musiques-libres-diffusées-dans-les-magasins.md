---
site: Next INpact
title: "La Cour de cassation confirme la redevance sur les musiques libres diffusées dans les magasins"
description: Pour qui sonne le La
author: Marc Rees
date: 2019-12-30
href: https://www.nextinpact.com/news/108542-la-cour-cassation-confirme-redevance-sur-musiques-libres-diffusees-dans-magasins.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/2019.jpg
tags:
- Droit d'auteur
- Institutions
series:
- 202001
series_weight: 0
---

> Jamendo SA, Audiovalley SA (ex-Musicmatic SA) et Storever France SAS (ex-Musicmatic France SAS) avaient formé un pourvoi en cassation. Selon eux, la cour d’appel de Paris ne pouvait exiger de Saint Maclou le paiement d’une redevance alors que l’enseigne diffusait de la musique libre. La Cour de cassation a pourtant validé cette obligation.
