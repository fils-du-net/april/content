---
site: L'Alterpresse68
title: "Dossier machines à voter: comment les municipalités cassent encore les urnes aux citoyens de Mulhouse et Riedisheim"
author: Camille Dumeunier
date: 2019-12-24
href: https://www.alterpresse68.info/2019/12/24/dossier-machines-a-voter-comment-les-municipalites-cassent-encore-les-urnes-aux-citoyens-de-mulhouse-et-riedisheim
featured_image: https://www.alterpresse68.info/wp-content/uploads/2019/12/couvnedap-991x640.jpg
tags:
- Vote électronique
series:
- 201952
series_weight: 0
---

> Le vote électronique est un système de vote dématérialisé, à comptage automatisé, notamment des scrutins, à l’aide de systèmes informatiques.
