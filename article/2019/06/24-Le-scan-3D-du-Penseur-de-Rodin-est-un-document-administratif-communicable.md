---
site: Next INpact
title: "Le scan 3D du Penseur de Rodin est un document administratif communicable"
author: Marc Rees
date: 2019-06-24
href: https://www.nextinpact.com/news/107996-le-scan-3d-penseur-rodin-est-document-administratif-communicable.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/23259.jpg
tags:
- Open Data
- Droit d'auteur
series:
- 201926
series_weight: 0
---

> En mars 2019, la justice a estimé que l’interdiction de photographier les expositions temporaires au Louvre était aussi légale que justifiée. Un nouveau bras de fer concerne cette fois les données 3D issues des sculptures du musée Rodin. La commission d’accès aux documents administratifs considère ces informations parfaitement communicables.
