---
site: ZDNet France
title: "Le CERN délaisse Microsoft pour le logiciel libre"
author: Steven J. Vaughan-Nichols
date: 2019-06-13
href: https://www.zdnet.fr/actualites/le-cern-delaisse-microsoft-pour-le-logiciel-libre-39885945.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2015/03/opensource-600.jpg
tags:
- Logiciels privateurs
- Informatique en nuage
series:
- 201924
series_weight: 0
---

> Le CERN, l'un des principaux organismes de recherche scientifique au monde, a délaissé les programmes Microsoft pour utiliser des logiciels open source plus abordable.
