---
site: Usbek & Rica
title: "«J'espère qu'ils appréhenderont la technologie autrement que par la fascination»"
author: Mathieu Brand
date: 2019-06-27
href: https://usbeketrica.com/article/technologie-fascination-classe-futur-proces-jean-baptiste-souday
featured_image: https://static.usbeketrica.com/images/thumb_840xh/5d1389ed4d4ca.png
tags:
- Innovation
series:
- 201926
series_weight: 0
---

> Après avoir imaginé les futurs de l'intelligence artificielle avec sa classe, Jean-Baptiste Souday poursuit son enseignement du futur auprès de ses élèves.
