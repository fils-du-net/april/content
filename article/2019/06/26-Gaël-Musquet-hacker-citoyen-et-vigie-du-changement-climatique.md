---
site: Le Monde.fr
title: "Gaël Musquet, «hacker citoyen» et vigie du changement climatique (€)"
author: Claire Legros
date: 2019-06-26
href: https://www.lemonde.fr/festival/article/2019/06/26/gael-musquet-hacker-citoyen-et-vigie-du-changement-climatique_5481584_4415198.html
featured_image: https://img.lemde.fr/2019/06/24/0/0/3264/2176/688/0/60/0/8faf66f_rdAjfJfoPyk93OeWTVZd9xxS.jpg
tags:
- Innovation
- Sciences
series:
- 201926
series_weight: 0
---

> Pour répondre à la recrudescence des épisodes météorologiques hors normes liés au réchauffement climatique, ce passionné de logiciel libre mise sur l'autonomie des habitants en «outillant les solidarités locales». Deuxième volet de notre série sur celles et ceux qui préparent l'«après-effondrement».
