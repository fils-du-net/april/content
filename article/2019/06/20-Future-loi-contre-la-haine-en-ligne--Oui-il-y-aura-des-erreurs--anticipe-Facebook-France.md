---
site: Next INpact
title: "Future loi contre la haine en ligne: «Oui, il y aura des erreurs» anticipe Facebook France (€)"
author: Marc Rees
date: 2019-06-20
href: https://www.nextinpact.com/news/107991-future-loi-contre-haine-en-ligne-oui-il-y-aura-erreurs-anticipe-facebook-france.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/2490.jpg
tags:
- Institutions
- Internet
series:
- 201925
---

> La proposition de loi contre la haine en ligne a été adoptée hier en commission des lois. L’ensemble des amendements de sa rapporteur, Laetitia Avia (LREM), ont été adoptés, ainsi qu’une vingtaine d’autres. Le texte est désormais programmé pour les séances des 3 et 4 juillet.
