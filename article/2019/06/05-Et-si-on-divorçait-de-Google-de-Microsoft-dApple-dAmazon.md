---
site: La Libre.be
title: "Et si on divorçait de Google, de Microsoft, d'Apple, d'Amazon…"
author: Scholasse Etienne
date: 2019-06-05
href: https://www.lalibre.be/economie/digital/et-si-on-divorcait-de-google-de-microsoft-d-apple-d-amazon-5ce5437f9978e26db0631b48
featured_image: https://t3.llb.be/tXD4DTJfGHkmfiybpdAXlankPJw=/0x378:1920x1338/940x470/5cf7b4a8d8ad580bf03ac7c8.jpg
tags:
- Vie privée
- Associations
- Internet
series:
- 201923
series_weight: 0
---

> Laisser vos données de vos activités sur Internet aux géants du net n'est pas une fatalité. L'association Framasoft propose une alternative.
