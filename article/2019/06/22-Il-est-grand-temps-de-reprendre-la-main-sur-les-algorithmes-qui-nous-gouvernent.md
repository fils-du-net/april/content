---
site: Le Monde.fr
title: "«Il est grand temps de reprendre la main sur les algorithmes qui nous gouvernent» (€)"
date: 2019-06-22
href: https://www.lemonde.fr/idees/article/2019/06/22/il-est-grand-temps-de-reprendre-la-main-sur-les-algorithmes-qui-nous-gouvernent_5480123_3232.html
featured_image: https://img.lemde.fr/2019/06/21/0/0/3573/4707/688/0/60/0/c1fb344_pkSUxxqGBM7bmJfJNonG857L.jpg
tags:
- Innovation
- Open Data
- Administration
series:
- 201925
series_weight: 0
---

> TRIBUNE. Le chercheur en informatique Hugues Bersini plaide dans une tribune au «Monde» pour un «codage citoyen», qui ferait des algorithmes les outils d'une gestion collective de notre existence commune.
