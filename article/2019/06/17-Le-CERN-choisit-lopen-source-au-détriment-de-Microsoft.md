---
site: ChannelNews
title: "Le CERN choisit l'open source au détriment de Microsoft"
author: Dirk Basyn
date: 2019-06-17
href: https://www.channelnews.fr/le-cern-choisit-lopen-source-au-detriment-de-microsoft-89862
featured_image: https://www.channelnews.fr/wp-content/uploads/2019/06/CERN.jpg
tags:
- Logiciels privateurs
series:
- 201925
series_weight: 0
---

> Microsoft chercherait à faire basculer les organismes de recherche scientifique vers l’open source? La question peut paraître incongrue et l’est certainement. Toujours est-il que la firme de Redmond a décidé l’an dernier de multiplier par 10 le prix des licences accordées au CERN. En effet, le laboratoire européen de recherche sur la physique des particules – connu pour son accélérateur – bénéficiait jusqu’alors de la tarification réduite accordée aux instituts de recherche publics, un avantage que Microsoft a décidé de révoquer, mettant le CERN, de même que d’autres organismes de recherche, au même niveau qu’une entreprise.
