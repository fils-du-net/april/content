---
site: France Culture
title: "GAFAM: le déclin de l'empire américain?"
author: Nicolas Martin
date: 2019-06-05
href: https://www.franceculture.fr/emissions/la-methode-scientifique/la-methode-scientifique-emission-du-mercredi-05-juin-2019
featured_image: https://cdn.radiofrance.fr/s3/cruiser-production/2019/06/7843d738-ea0d-406a-ab35-0a5749734afd/838_gettyimages-964466112.webp
tags:
- Internet
series:
- 201923
series_weight: 0
---

> Dans quelle mesure la puissance acquise par les géants du numérique d'aujourd'hui doit-elle nous inquiéter? Comment l'Europe tente-t-elle de s'emparer du problème de la possibilité de démanteler les GAFA aujourd'hui?
