---
site: Next INpact
title: "Le Conseil d'État s'oppose à la communication des «algorithmes locaux» de Parcoursup (€)"
author: Xavier Berne
date: 2019-06-13
href: https://www.nextinpact.com/news/107971-le-conseil-detat-soppose-a-communication-algorithmes-locaux-parcoursup.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/22084.jpg
tags:
- Open Data
- Éducation
series:
- 201924
---

> Le Conseil d’État a tranché: contrairement à ce qu’avait jugé le tribunal administratif de Guadeloupe, début février, les algorithmes dits « locaux » de Parcoursup ne sont pas communicables. Et ce quand bien même le dispositif en vigueur relève à certains égards d'un «tour de passe-passe», dixit le rapporteur public.
