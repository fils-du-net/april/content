---
site: TV5MONDE
title: "Vidéosurveillance: Castaner veut un débat sur la reconnaissance faciale"
date: 2019-06-21
href: https://information.tv5monde.com/info/videosurveillance-castaner-veut-un-debat-sur-la-reconnaissance-faciale-307443
featured_image: https://information.tv5monde.com/sites/info.tv5monde.com/files/styles/large/public/c893c2664e78e1d806979e9a5d720ef17621d2a4.jpg?itok=Nks6S0hU
tags:
- Vie privée
- Institutions
series:
- 201925
---

> Le ministre de l'Intérieur Christophe Castaner a jugé "important" vendredi d'ouvrir le débat sur la reconnaissance faciale utilisée pour la vidéosurveillance, saluant une expérimentation menée à Nice où le maire LR Christian Estrosi a triplé le nombre de caméras en dix ans.
