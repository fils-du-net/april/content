---
site: Next INpact
title: "Le Sénat impose une sensibilisation des fonctionnaires aux enjeux numériques et aux logiciels libres (€)"
author: Xavier Berne
date: 2019-06-27
href: https://www.nextinpact.com/news/108009-le-senat-impose-sensibilisation-fonctionnaires-aux-enjeux-numeriques-et-aux-logiciels-libres.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/3935.jpg
seeAlso: "[Adoption d'un amendement pour une meilleure formation des fonctionnaires à l'utilisation des logiciels libres](https://april.org/adoption-d-un-amendement-pour-une-meilleure-formation-des-fonctionnaires-a-l-utilisation-des-logicie)"
tags:
- Administration
- Sensibilisation
series:
- 201926
series_weight: 0
---

> Contre l'avis du gouvernement, le Sénat vient d’adopter un amendement prévoyant une sensibilisation des fonctionnaires aux «enjeux liés à l'écosystème numérique», tels les données personnelles et les logiciels libres. Certains élus ont profité des débats pour dénoncer l’absence de «doctrine» de l’exécutif sur ce dernier dossier.
