---
site: Industrie et Technologies
title: "Pour ses dix ans, l'autorité française de la cybersécurité affiche ses ambitions"
author: Kevin Poireault
date: 2019-06-05
href: https://www.industrie-techno.com/article/pour-ses-10-ans-l-anssi-veut-monter-en-competence.56430
featured_image: https://www.industrie-techno.com/mediatheque/5/9/4/000041495_600x400_c.jpeg
tags:
- Institutions
series:
- 201923
series_weight: 0
---

> L’agence nationale de la sécurité des systèmes d'information (Anssi) a célébré ses 10 ans d’existence en grande pompe, dans le 12e arrondissement de Paris, le 4 mai. L'occasion pour Guillaume Poupard, son directeur général, de présenter les grandes orientations de l'agence pour les années à venir.
