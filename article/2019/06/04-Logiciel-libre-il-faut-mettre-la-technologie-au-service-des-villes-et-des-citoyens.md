---
site: Libération
title: "Logiciel libre: il faut mettre la technologie au service des villes et des citoyens"
author: Emmanuel Grégoire, Jean-Christophe Becquet, François Elie, Jacob Green, Jean-Baptiste Kempf et Cédric Thomas
date: 2019-06-04
href: https://www.liberation.fr/debats/2019/06/04/logiciel-libre-il-faut-mettre-la-technologie-au-service-des-villes-et-des-citoyens_1731392
featured_image: https://medias.liberation.fr/photo/1192984-000_km792.jpg
tags:
- Administration
- april
- Entreprise
- Internet
series:
- 201923
series_weight: 0
---

> Face à l'expansion des Gafam, le logiciel libre est une approche essentielle pour repenser la technologie au bénéfice des citoyens.
