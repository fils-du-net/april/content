---
site: Next INpact
title: "Droits voisins de la presse: les «GAFA» ne seront pas les seuls à devoir payer (€)"
author: Marc Rees
date: 2019-06-28
href: https://www.nextinpact.com/news/108012-droits-voisins-presse-les-gafa-ne-seront-pas-seuls-a-devoir-payer.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/3674.jpg
tags:
- Droit d'auteur
- Europe
series:
- 201926
---

> C’est le 3 juillet 2019 que la proposition de loi visant à instaurer un droit voisin au profit des éditeurs et agences de presse sera examinée en deuxième lecture au Sénat. Une compensation que devront payer les GAFA, mais pas seulement... Explication de ce texte transposant l’article 15 de la directive sur le droit d’auteur.
