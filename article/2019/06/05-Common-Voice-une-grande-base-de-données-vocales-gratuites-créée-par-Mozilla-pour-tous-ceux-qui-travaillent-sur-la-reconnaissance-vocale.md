---
site: Archimag
title: "Common Voice: une grande base de données vocales gratuites créée par Mozilla pour tous ceux qui travaillent sur la reconnaissance vocale"
author: Clémence Jost
date: 2019-06-05
href: https://www.archimag.com/univers-data/2019/06/05/common-voice-mozilla-donnees-reconnaissance-vocale-gratuites
featured_image: https://www.archimag.com/sites/archimag.com/files/styles/article/public/web_articles/image/bulles_bd.jpg?itok=iPfGRTdL
tags:
- Innovation
series:
- 201923
series_weight: 0
---

> La fondation Mozilla a mis en ligne Common Voice en février 2019. Les jeux de cette grande base de données vocales sont dans le domaine public, c'est à dire téléchargeables et réutilisables gratuitement. Les startups comme les chercheurs peuvent s'emparer de cette matière pour faire avancer leurs projets en reconnaissance vocale.
