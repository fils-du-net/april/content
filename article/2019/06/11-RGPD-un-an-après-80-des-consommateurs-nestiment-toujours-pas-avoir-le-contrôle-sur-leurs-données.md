---
site: Global Security Mag
title: "RGPD, un an après: 80% des consommateurs n'estiment toujours pas avoir le contrôle sur leurs données"
author: SUZE
date: 2019-06-11
href: http://www.globalsecuritymag.fr/RGPD-un-an-apres-80-des,20190611,87989.html
tags:
- Vie privée
series:
- 201924
series_weight: 0
---

> Si l’entrée en vigueur du RGPD le 25 mai 2018 est venu bouleverser la gestion des données pour les entreprises, elle a aussi ouvert la voie à une prise de conscience des consommateurs sur l’utilisation qui est faite de leurs données. Des consommateurs qui demandent aujourd’hui toujours plus de transparence et de contrôle. C’est ce que révèle une étude menée par SUSE (via Google Survey) auprès de 2000 personnes en Europe (France, Allemagne et Royaume-Uni).
