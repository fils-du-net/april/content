---
site: Next INpact
title: "Haine en ligne: tour d'horizon des 265 amendements (€)"
author: Marc Rees
date: 2019-06-18
href: https://www.nextinpact.com/news/107982-haine-en-ligne-tour-dhorizon-265-amendements.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/1631.jpg
tags:
- Institutions
- Internet
series:
- 201925
---

> La proposition de loi contre la haine sur Internet sera examinée en commission des lois le 19 juin prochain. De ces travaux préparatoires, aboutira le texte destiné à être ausculté puis voté en séance. En prévision de ce premier rendez-vous, plus de 260 amendements ont été déposés. Tour d’horizon.
