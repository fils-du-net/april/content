---
site: l'Humanité.fr
title: "Olivier Ertzscheid: «Il faut faire de Facebook un bien commun»"
author: Cédric Clérin
href: https://www.humanite.fr/il-faut-faire-de-facebook-un-bien-commun-674052
tags:
- Économie
series:
- 201926
---

> Les «Gafam poussent jusqu'au bout la logique du capitalisme de surveillance et du capitalisme tout court». Démonstration et alternative avec Olivier Ertzscheid, maître de conférences en sciences de l'information à l'université de Nantes.
