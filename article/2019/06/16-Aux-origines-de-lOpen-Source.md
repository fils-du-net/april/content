---
site: Nticweb.com
title: "Aux origines de l'Open Source"
author:  Samir Roubahi
date: 2019-06-16
href: http://www.nticweb.com/it/9742-aux-origines-de-l%E2%80%99open-source.html
featured_image: http://www.nticweb.com/images/resized/thumbs_l/images/open-source_580_320.jpg
tags:
- Sensibilisation
series:
- 201924
series_weight: 0
---

> Quand je travaillais en Allemagne, il y a quelques années, j’ai été surpris d’y trouver un autre rapport au logiciel que celui que je voyais en Algérie et même en France. J’ai notamment vu que la plupart des établissements et institutions du pays utilisaient des applications libres. Et j’ai découvert au passage une multitude de softwares inconnus pour moi jusqu’à présent.
