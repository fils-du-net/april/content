---
site: Numerama
title: "Au Parlement européen, les députés pirates s'allient avec les Verts"
author: Julien Lausson
date: 2019-06-05
href: https://www.numerama.com/politique/523049-au-parlement-europeen-les-deputes-pirates-sallient-avec-les-verts.html
featured_image: https://www.numerama.com/content/uploads/2019/06/parit-pirate-europe.jpg
tags:
- Europe
series:
- 201923
series_weight: 0
---

> Les quatre nouveaux élus pirates ont décidé de rejoindre le groupe des Verts/Alliance libre européenne. Ils se positionnent pour la vice-présidence du groupe et certaines commissions stratégiques.
