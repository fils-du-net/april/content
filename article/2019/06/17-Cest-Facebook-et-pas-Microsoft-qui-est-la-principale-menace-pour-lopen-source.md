---
site: Contrepoints
title: "C'est Facebook, et pas Microsoft, qui est la principale menace pour l'open source"
author: Glyn Moody
date: 2019-06-17
href: https://www.contrepoints.org/2019/06/17/346969-cest-facebook-et-pas-microsoft-qui-est-la-principale-menace-pour-lopen-source
featured_image: https://www.contrepoints.org/wp-content/uploads/2019/06/Mark-Zuckerberg-by-JD-LasicaCC-BY-2.0-1-1.jpg
tags:
- Entreprise
- Internet
- Vie privée
series:
- 201925
series_weight: 0
---

> Facebook va-t-il devenir le nouveau Windows?
> Un article du Linux Journal
