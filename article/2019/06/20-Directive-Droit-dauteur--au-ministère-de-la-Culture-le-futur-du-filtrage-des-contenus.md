---
site: Next INpact
title: "Directive Droit d'auteur: au ministère de la Culture, le futur du filtrage des contenus (€)"
author: Marc Rees
date: 2019-06-20
href: https://www.nextinpact.com/news/107935-directive-droit-dauteur-au-ministere-culture-futur-filtrage-plateformes.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/17595.jpg
tags:
- Institutions
- Droit d'auteur
series:
- 201925
---

> Exclusif. Hier, Édouard Philippe a annoncé que la future grande loi audiovisuelle serait présentée à l’automne pour une adoption en 2020. En marge de la conférence sur les 30 ans du CSA, Next INpact s’est procuré une pièce maitresse de ce texte: l’avant-projet de transposition de l’article 17 de la directive droit d’auteur. Celui relatif au filtrage.
