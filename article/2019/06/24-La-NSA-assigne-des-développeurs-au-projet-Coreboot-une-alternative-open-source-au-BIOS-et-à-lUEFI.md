---
site: Developpez.com
title: "La NSA assigne des développeurs au projet Coreboot, une alternative open source au BIOS et à l'UEFI,"
description: Ne faudrait-il pas s'en inquiéter?
author: Christian Olivier
date: 2019-06-24
href: https://open-source.developpez.com/actu/267132/La-NSA-assigne-des-developpeurs-au-projet-Coreboot-une-alternative-open-source-au-BIOS-et-a-l-UEFI-ne-faudrait-il-pas-s-en-inquieter
featured_image: https://www.developpez.net/forums/attachments/p485855d1/a/a/a
tags:
- Vie privée
- Institutions
series:
- 201926
series_weight: 0
---

> La NSA a commencé à assigner des développeurs au projet Coreboot, une alternative open source au BIOS (Basic Input-Output System) traditionnel qu’on trouvait déjà sur les PC MS-DOS des années 80 et à son remplaçant l’UEFI (Unified Extensible Firmware Interface) lancé en 2007. Eugene Myers de la NSA a commencé à fournir le code d’implémentation du SMI Transfer Monitor (STM) ciblant les CPU x86. Myers travaille pour le Trusted Systems Research Group de la NSA, un groupe qui d’après le site Web de l’agence est destiné à «mener et parrainer des recherches sur les technologies et techniques qui sécuriseront les systèmes d’information de l’Amérique de demain».
