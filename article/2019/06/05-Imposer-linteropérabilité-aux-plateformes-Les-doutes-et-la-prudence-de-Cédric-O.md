---
site: Next INpact
title: "Imposer l'interopérabilité aux plateformes? Les doutes et la prudence de Cédric O"
author: Marc Rees
date: 2019-06-05
href: https://www.nextinpact.com/news/107951-imposer-linteroperabilite-aux-plateformes-les-doutes-et-prudence-cedric-o.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/23136.jpg
tags:
- Interopérabilité
- april
- Internet
series:
- 201923
series_weight: 0
---

> 70 organisations ont signé une lettre ouverte pour demander au gouvernement et au législateur d’imposer l’interopérabilité aux grandes plateformes. Le sujet est toutefois jugé «excessivement agressif pour le modèle économique des grandes plateformes» estime Cédric O, secrétaire d’État au Numérique.
