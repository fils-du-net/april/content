---
site: Next INpact
title: "Hadopi, putain 10 ans!"
author: Marc Rees
date: 2019-06-12
href: https://www.nextinpact.com/news/107961-hadopi-putain-10-ans.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/21375.jpg
tags:
- HADOPI
- Droit d'auteur
- Institutions
series:
- 201924
series_weight: 0
---

> Il y a dix ans jour pour jour, après une longue bataille parlementaire, la loi instituant la Hadopi était promulguée par Nicolas Sarkozy. Le début d'une longue histoire, parsemée de surprises et d'une bonne dose de LOL...
