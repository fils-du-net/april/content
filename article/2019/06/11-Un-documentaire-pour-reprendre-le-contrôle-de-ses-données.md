---
site: Rude Baguette
title: "Un documentaire pour reprendre le contrôle de ses données"
author: Margaux Duquesne
date: 2019-06-11
href: https://www.rudebaguette.com/2019/06/un-documentaire-pour-reprendre-le-controle-de-ses-donnees
featured_image: https://www.rudebaguette.com/wp-content/uploads/2019/02/rudebaguette-logo.png
tags:
- Vie privée
series:
- 201924
---

> Disparaître est un projet de documentaire qui présente les outils nécessaires à tout un chacun pour reprendre le contrôle de sa vie privée.
