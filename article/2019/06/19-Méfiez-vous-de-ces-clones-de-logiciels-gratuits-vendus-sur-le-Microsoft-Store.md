---
site: 01net.
title: "Méfiez-vous de ces clones de logiciels gratuits vendus sur le Microsoft Store"
author: Geoffroy Ondet
date: 2019-06-19
href: https://www.01net.com/actualites/mefiez-vous-de-ces-clones-de-logiciels-gratuits-vendus-sur-le-microsoft-store-1715433.html
featured_image: https://img.bfmtv.com/c/0/708/9f2/2f950456dca79d33d016aff97f990.png
tags:
- Économie
- Logiciels privateurs
series:
- 201925
series_weight: 0
---

> Des développeurs peu regardants copient des logiciels open source pour les vendre sur le magasin d'applications de Microsoft.
