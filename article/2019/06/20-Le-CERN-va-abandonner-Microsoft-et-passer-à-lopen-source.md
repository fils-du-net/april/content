---
site: Rude Baguette
title: "Le CERN va abandonner Microsoft et passer à l'open source"
date: 2019-06-20
href: https://www.rudebaguette.com/2019/06/cern-open-source-microsoft
featured_image: https://www.rudebaguette.com/wp-content/uploads/thumbs/CERN-open-source-Microsoft-Rude-Baguette-38r62yk1rl79on3l7ptzwg.jpg
tags:
- Logiciels privateurs
series:
- 201925
---

> Le CERN vient d'annoncer qu'il allait migrer l'ensemble des logiciels de ses équipes de Microsoft vers des solutions open source.
