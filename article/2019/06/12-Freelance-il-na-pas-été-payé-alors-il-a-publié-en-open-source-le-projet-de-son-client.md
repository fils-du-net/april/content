---
site: Developpez.com
title: "Freelance: il n'a pas été payé, alors il a publié en open source le projet de son client,"
description: "Comment s'assurer d'être payé pour son travail?"
author: Michael Guilloux
date: 2019-06-12
href: https://emploi.developpez.com/actu/265017/Freelance-il-n-a-pas-ete-paye-alors-il-a-publie-en-open-source-le-projet-de-son-client-comment-s-assurer-d-etre-paye-pour-son-travail
featured_image: https://www.developpez.com/public/images/news/nopay01.PNG
tags:
- Entreprise
- Économie
series:
- 201924
series_weight: 0
---

> Cela vous êtes peut-être déjà arrivé si vous êtes un travailleur indépendant. Après que son client ait signé "le contrat" et qu'il a commencé à développer la plateforme demandée, le client a décidé de rompre le contrat et refusé de le payer pour le travail qu'il a fait. «Parfois, on se fait avoir dans les affaires, mais au moins, maintenant, j'ai trouvé une solution», explique Jason Werner, freelancer et développeur de Cosmo, un gestionnaire mobile de portefeuille de cryptomonnaie. La solution dont il parle, c'est de publier en open source la plateforme qu'il avait commencé à développer pour son client.
