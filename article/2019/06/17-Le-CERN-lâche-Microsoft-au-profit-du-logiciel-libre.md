---
site: L'Informaticien
title: "Le CERN lâche Microsoft au profit du logiciel libre"
author: Guillaume Périssat
date: 2019-06-17
href: https://www.linformaticien.com/actualites/id/52246/le-cern-lache-microsoft-au-profit-du-logiciel-libre.aspx
tags:
- Logiciels privateurs
series:
- 201925
---

> Le centre de recherche, pourtant client de Redmond depuis plus de vingt ans, va passer au libre. La recherche d’alternatives, en projet depuis l’an dernier, connait une brusque accélération alors que l’éditeur a révoqué le statut académique du CERN et le fait payer plein pot.
