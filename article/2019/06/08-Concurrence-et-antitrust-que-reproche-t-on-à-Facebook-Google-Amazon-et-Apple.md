---
site: Le Monde.fr
title: "Concurrence et antitrust: que reproche-t-on à Facebook, Google, Amazon et Apple? (€)"
author: Alexandre Piquard
date: 2019-06-08
href: https://www.lemonde.fr/economie/article/2019/06/08/concurrence-et-antitrust-que-reproche-t-on-a-facebook-google-amazon-et-apple_5473496_3234.html
featured_image: https://img.lemde.fr/2019/06/05/0/0/4762/3175/688/0/60/0/fbab71f_f25351d6b840491ebd5f54f743396b36-f25351d6b840491ebd5f54f743396b36-0.jpg
tags:
- Internet
- Entreprise
series:
- 201923
---

> Deux points concentrent les critiques: le rachat d'entreprise, qui renforcerait leur domination, et les plates-formes qui peuvent être utilisées pour discriminer des concurrents.
