---
site: Le Monde.fr
title: "«Shoshana Zuboff oublie d'inscrire le “capitalisme de surveillance” dans une histoire plus large» (€)"
author: Frédéric Joignot
date: 2019-06-14
href: https://www.lemonde.fr/idees/article/2019/06/14/shoshana-zuboff-oublie-d-inscrire-le-capitalisme-de-surveillance-dans-une-histoire-plus-large_5476062_3232.html
tags:
- Sensibilisation
- Vie privée
series:
- 201924
---

> Le sociologue Sébastien Broca reproche notamment à l'universitaire américaine de s'en tenir à des préconisations générales en matière de lutte contre la surveillance.
