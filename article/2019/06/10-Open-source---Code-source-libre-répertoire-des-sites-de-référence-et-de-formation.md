---
site: Thot Cursus
title: "Open source - Code source libre: répertoire des sites de référence et de formation"
date: 2019-06-10
href: https://cursus.edu/technologies/43080/open-source-code-source-libre-repertoire-des-sites-de-reference-et-de-formation
featured_image: https://cursus.edu/uploads/fiche_img/1245b9503756dbd72237d9511406430e.jpg
tags:
- april
- Sensibilisation
- Partage du savoir
series:
- 201924
series_weight: 0
---

> Une sélection de ressources pratiques pour les personnes curieuses d’en connaître un peu plus sur le code source libre, les logiciels libres et leur évolution.
