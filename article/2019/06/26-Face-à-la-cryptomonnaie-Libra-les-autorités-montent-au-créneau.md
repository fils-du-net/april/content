---
site: Numerama
title: "Face à la cryptomonnaie Libra, les autorités montent au créneau"
author: Julien Lausson
date: 2019-06-26
href: https://www.numerama.com/business/528894-face-a-la-cryptomonnaie-libra-les-autorites-montent-au-creneau.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/content/uploads/2019/02/banque-de-france.jpg
tags:
- Économie
- Entreprise
- Vie privée
series:
- 201926
series_weight: 0
---

> Depuis l'annonce de la Libra, les autorités politiques et bancaires se sont succédé pour émettre des mises en garde. Dernière sortie date, celle du gouverneur de la Banque de France.
