---
site: Le Monde.fr
title: "A Barcelone, un modèle original de «commun numérique»"
date: 2019-06-07
href: https://www.lemonde.fr/economie/article/2019/06/07/a-barcelone-un-modele-original-de-commun-numerique_5473033_3234.html
featured_image: https://img.lemde.fr/2019/05/27/0/0/5624/3621/688/0/60/0/2a74c5e_645627073f2f4e43a344751accad353d-645627073f2f4e43a344751accad353d-0.jpg
tags:
- Administration
- Partage du savoir
series:
- 201923
---

> La municipalité catalane a développé une plate-forme de participation citoyenne sous logiciel libre qui peut être utilisée par d'autres collectivités dans le monde.
