---
site: ZDNet France
title: "Des défenseurs du Libre demandent à l'INPI de permettre le format ouvert ODF pour le dépôt de brevet"
author: Thierry Noisette
date: 2019-06-30
href: https://www.zdnet.fr/blogs/l-esprit-libre/des-defenseurs-du-libre-demandent-a-l-inpi-de-permettre-le-format-ouvert-odf-pour-le-depot-de-brevet-39886835.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2019/06/INPI-depot%20brevet.jpg
tags:
- Standards
- Interopérabilité
- Institutions
series:
- 201927
series_weight: 0
---

> L'Union des entreprises du logiciel libre (CNLL), le Ploss Auvergne-Rhone-Alpes et l'association La Mouette demandent à l'INPI de ne pas imposer l'usage du format propriétaire bureautique de Microsoft.
