---
site: l'Humanité.fr
title: "Gafam, futurs maîtres du monde à stopper net!"
href: https://www.humanite.fr/en-finir-avec-les-gafam-674054
featured_image: https://www.humanite.fr/sites/default/files/logo.svg
tags:
- Économie
- Entreprise
- Internet
series:
- 201926
---

> Les Google, Apple, Facebook, Amazon, Microsoft voulaient dominer le monde. Et n'en sont pas si loin. Mais leur voracité commence à faire grincer des dents, même outre-Atlantique. Un sursaut salvateur?
