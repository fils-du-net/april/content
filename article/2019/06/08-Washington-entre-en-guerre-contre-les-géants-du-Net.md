---
site: Le Monde.fr
title: "Washington entre en guerre contre les géants du Net (€)"
author: Gilles Paris
date: 2019-06-08
href: https://www.lemonde.fr/economie/article/2019/06/08/washington-entre-en-guerre-contre-les-geants-du-net_5473495_3234.html
featured_image: https://img.lemde.fr/2019/06/08/0/0/5472/3648/688/0/60/0/335a5a2_lHKYbjf_M5YxnRCYOYOqpV6K.jpg
tags:
- Internet
- Entreprise
series:
- 201923
---

> Le département américain de la justice, la Commission fédérale du commerce et certains élus ont engagé des procédures visant les GAFA.
