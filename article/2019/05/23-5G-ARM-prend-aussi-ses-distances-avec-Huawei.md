---
site: Silicon
title: "5G: ARM prend aussi ses distances avec Huawei"
date: 2019-05-23
href: https://www.silicon.fr/5g-arm-prend-aussi-ses-distances-avec-huawei-241919.html
featured_image: https://www.silicon.fr/wp-content/uploads/2019/04/huawei-logo-entreprise-684x513.jpg
tags:
- Entreprise
series:
- 201921
---

> Pourquoi ARM, société britannique, a-t-elle décidé de se plier au décret américain qui a conduit au boycott de Huawei?
