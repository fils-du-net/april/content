---
site: Forbes France
title: "Satya Nadella: Un Nouveau Souffle Pour Microsoft "
date: 2019-05-26
href: https://www.forbes.fr/business/satya-nadella-un-nouveau-souffle-pour-microsoft
featured_image: https://www.forbes.fr/wp-content/uploads/2019/05/gettyimages-1132529735-740x370.jpg
tags:
- Économie
series:
- 201921
---

> Autrefois qualifié d’has-been et de désuet, Microsoft est aujourd’hui en plein essor. Le secret de Satya Nadella, son directeur général: une réinitialisation culturelle qui a abaissé les portes de la forteresse du fabricant de logiciels (et du cloud à volonté).
