---
site: Clubic.com
title: "Notre-Dame: quand le partage de données aide à la restauration"
author: Garance Cherubini
date: 2019-05-22
href: https://www.clubic.com/telecharger/Microsoft/actualite-857672-dame-partage-donnees-aide-restauration.html
featured_image: https://pic.clubic.com/v1/images/1709691/raw
tags:
- Open Data
- Partage du savoir
- Entreprise
series:
- 201921
series_weight: 0
---

> Avec Open Notre Dame, Microsoft et Iconem espèrent faciliter la restauration de la cathédrale en créant une immense base de données en open source, qui rassemblera des documents de tout type sur le monument: ses fondations, son apparence, etc.
