---
site: Be Geek
title: "Open source: voici les logiciels recommandés par l'état"
date: 2019-05-07
href: https://www.begeek.fr/open-source-voici-les-logiciels-recommandes-par-letat-315105
featured_image: https://media.begeek.fr/2019/05/Etat-France-SILL-2019-logiciel-libre.jpg
tags:
- Référentiel
- Administration
series:
- 201919
series_weight: 0
---

> Voici la petite parution 2019 de la liste des logiciels libres de la SILL et recommandés par l'État français
