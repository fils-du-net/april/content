---
site: Developpez.com
title: "Le gouvernement sud-coréen veut migrer vers Linux à la fin du support de Windows 7"
author: Michael Guilloux
date: 2019-05-18
href: https://linux.developpez.com/actu/261652/Le-gouvernement-sud-coreen-veut-migrer-vers-Linux-a-la-fin-du-support-de-Windows-7-va-t-il-aller-jusqu-au-bout-sans-faire-demi-tour-comme-Munich
featured_image: https://www.developpez.com/public/images/news/windowslinux21.jpg
tags:
- International
- Logiciels privateurs
series:
- 201920
series_weight: 0
---

> À quelques mois de la fin du support de Windows 7, le gouvernement de la Corée du Sud envisage de passer à Linux plutôt que de continuer vers Windows 10, une option qui semble logiquement la moins douloureuse vu la comptabilité des OS.
