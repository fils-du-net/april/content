---
site: 20minutes.fr
title: "Huawei: Google coupe les ponts avec le constructeur chinois, qui ne pourra plus se servir d'Android"
author: 20 Minutes avec agences
date: 2019-05-20
href: https://www.20minutes.fr/monde/2522067-20190520-huawei-google-coupe-ponts-constructeur-chinois-pourra-plus-servir-android
featured_image: https://img.20mn.fr/-BlYTqIpTIiLjVegh3qybA/830x532_image-illustration-marque-chinoise-telecoms-huawei.jpg
tags:
- Entreprise
series:
- 201921
---

> Huawei pourra au mieux installer sur ses téléphones la version «open source» d'Android
