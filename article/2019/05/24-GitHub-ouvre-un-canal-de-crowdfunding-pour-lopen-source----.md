---
site: ITespresso
title: "GitHub ouvre un canal de crowdfunding pour l'open source"
author: Clément Bohic
date: 2019-05-24
href: https://www.itespresso.fr/github-crowdfunding-open-source-206945.html
featured_image: https://www.itespresso.fr/wp-content/uploads/2019/05/github-berlin-684x513.jpg
tags:
- Économie
series:
- 201921
---

> GitHub annonce, entre autres nouveautés, un système de financement participatif des projets open source hébergés sur sa plate-forme.
