---
site: Developpez.com
title: "L'April se mobilise pour donner la priorité au logiciel libre dans l'éducation"
author: Michael Guilloux
date: 2019-05-13
href: https://open-source.developpez.com/actu/260829/L-April-se-mobilise-pour-donner-la-priorite-au-logiciel-libre-dans-l-education-les-discussions-s-ouvrent-au-Senat-a-partir-du-mardi-14-mai-2019/
featured_image: https://www.developpez.com/public/images/news/april-libre.png
seeAlso: "[Urgent, mobilisez-vous, contactez sénateurs et sénatrices pour la priorité au logiciel libre dans l'éducation](https://april.org/urgent-mobilisez-vous-contactez-senateurs-et-senatrices-pour-la-priorite-au-logiciel-libre-dans-l-ed)"
tags:
- april
- Éducation
- Institutions
- Marchés publics
series:
- 201920
series_weight: 0
---

> Contacter les membres du Sénat par email ou par téléphone pour leur expliquer l'importance du logiciel libre dans le système éducatif, c'est l'appel lancé par l'April afin que soit inscrite dans la loi la priorité au logiciel libre dans le cadre du service public de l'enseignement.
