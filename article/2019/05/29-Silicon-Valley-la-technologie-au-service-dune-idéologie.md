---
site: France Culture
title: "Silicon Valley: la technologie au service d'une idéologie?"
author: Olivia Gesbert
date: 2019-05-29
href: https://www.franceculture.fr/emissions/la-grande-table-2eme-partie/silicon-valley-la-technologie-au-service-dune-ideologie
featured_image: https://cdn.radiofrance.fr/s3/cruiser-production/2019/05/876fc9d3-64a0-4c6e-9498-e71e1d8c81f4/838_gettyimages-73071141.jpg.webp
tags:
- Internet
- Économie
- Éducation
- Sciences
series:
- 201922
series_weight: 0
---

> Entre capitalisme et hyper individualisme, quel est ce nouvel ordre du monde que la Silicon Valley entend installer? On en parle avec Fabien Benoit, auteur de 'The Valley' (Les Arènes, 2019), et Emmanuel Alloa, maître de conférences en philosophie à l'Université de Saint-Gall (Suisse).
