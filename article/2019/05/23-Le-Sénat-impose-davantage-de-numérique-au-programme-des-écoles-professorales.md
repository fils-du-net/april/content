---
site: Next INpact
title: "Le Sénat impose davantage de numérique au programme des écoles professorales (€)"
author: Xavier Berne
date: 2019-05-23
href: https://www.nextinpact.com/news/107905-le-senat-impose-davantage-numerique-au-programme-ecoles-professorales.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/23056.jpg
tags:
- Éducation
series:
- 201921
---

> Alors que la formation des enseignants en matière de numérique est souvent pointée du doigt, le Sénat vient d’adopter un amendement inscrivant notamment «la maîtrise des outils et ressources numériques» au programme des écoles professorales.
