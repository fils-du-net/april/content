---
site: Commentçamarche.net
title: "CC Search se lance avec 300 millions d'images gratuites"
author: Félix Marciano
date: 2019-05-06
href: https://www.commentcamarche.net/news/5872677-cc-search-se-lance-avec-300-millions-d-images-gratuites
featured_image: https://img-19.ccm2.net/MLuv680QYEWb6sPhTEgbAYvKGr0=/200x/2bacb84cac9744e29e78c9f4bb585f6d/ccm-news/cc-search-37.png
tags:
- Contenus libres
series:
- 201919
---

> Creative Commons vient de lancer un moteur de recherche permettant de trouver facilement des images utilisables gratuitement.
