---
site: Next INpact
title: "Directive droit d'auteur: pas de blocage mais une forme de filtrage, assure Emmanuel Macron (€)"
author: Marc Rees
date: 2019-05-28
href: https://www.nextinpact.com/news/107915-directive-droit-dauteur-pas-blocage-mais-forme-filtrage-assure-emmanuel-macron.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/23077.jpg
tags:
- Droit d'auteur
series:
- 201922
---

> Une fois mis en œuvre l’article 13 de la directive sur le droit d’auteur (devenu article 17 au fil des débats), le chef de l’État a promis qu’il n’y aurait pas de «blocage» des contenus. Pas de blocage, mais «une forme de filtrage», a-t-il nuancé.
