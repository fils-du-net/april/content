---
site: Developpez.com
title: "Pourquoi Linux n'a-t-il pas de succès sur desktop? Entretien avec Mark Shuttleworth"
description: Fondateur et PDG de Canonical, éditeur d'Ubuntu
author: Michael Guilloux
date: 2019-05-16
href: https://www.developpez.com/actu/261202/Pourquoi-Linux-n-a-t-il-pas-de-succes-sur-desktop-Entretien-avec-Mark-Shuttleworth-fondateur-et-PDG-de-Canonical-editeur-d-Ubuntu
featured_image: https://www.developpez.com/public/images/news/linuxlogo.png
tags:
- Innovation
series:
- 201920
series_weight: 0
---

> Linux est le plus grand projet communautaire dans le monde du développement. S'il est populaire dans de nombreux domaines (serveurs, cloud, mobiles, etc.), sur le marché des PC, il a beaucoup de retard sur ses concurrents Windows et macOS. Actuellement, Linux détient par exemple moins de 3 % de parts de marché d'après les statistiques du baromètre Net Applications. L'adoption de l'OS sur le marché des PC est freinée par de nombreux problèmes, y compris le manque de constructeurs proposant des PC avec Linux préinstallé; le support des pilotes et des logiciels propriétaires; des interfaces utilisateur que les gens trouvent parfois très basiques; ou encore le problème de fragmentation de l'écosystème. Pour Linus Torvalds en tout cas, la fragmentation est l'un des principaux freins à l'adoption de Linux sur desktop.
