---
site: Siècle Digital
title: "GitHub Sponsors: le nouvel outil pour financer les développeurs open source"
date: 2019-05-23
href: https://siecledigital.fr/2019/05/23/github-sponsors-le-nouvel-outil-pour-financer-les-developpeurs-open-source/
featured_image: https://sd-cdn.fr/wp-content/uploads/2019/05/github-logo-770x515.jpeg
tags:
- Économie
series:
- 201921
---

> GitHub lance 'GitHub Sponsors', un nouvel outil d'aide financière pour les développeurs.
