---
site: Le Monde Informatique
title: "L'ANSSI dévoile en open hardware un disque dur chiffrant les données"
author: Bastien Lion
date: 2019-05-21
href: https://www.lemondeinformatique.fr/actualites/lire-l-anssi-devoile-en-open-hardware-un-disque-dur-chiffrant-les-donnees-75375.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000066789.jpg
tags:
- Innovation
- Vie privée
- Matériel libre
series:
- 201921
series_weight: 0
---

> L'Agence nationale de la sécurité des systèmes d'information (ANSSI) a profité de la conférence Ready for IT, à Monaco (du 20 au 22 mai), pour mettre en avant ses engagements en matière de solutions open source. C'est dans ce sens qu'elle a dévoilé le projet WooKey, un disque dur USB chiffrant et sécurisé.
