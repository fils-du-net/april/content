---
site: Developpez.com
title: "DuckDuckGo propose le Do Not Track Act, un projet de loi qui obligerait les sites"
description: à ne pas pister les internautes qui utilisent Do Not Track
author: Stéphane le Calme
date: 2019-05-02
href: https://web.developpez.com/actu/258389/DuckDuckGo-propose-le-Do-Not-Track-Act-un-projet-de-loi-qui-obligerait-les-sites-a-ne-pas-pister-les-internautes-qui-utilisent-Do-Not-Track/
featured_image: https://www.developpez.net/forums/attachments/p471402d1/a/a/a
tags:
- Vie privée
- Internet
series:
- 201918
series_weight: 0
---

> S’il fallait reconnaître l’une des caractéristiques des grandes régies publicitaires, c’est qu’elles se servent de plus en plus d’outils invasifs pour diffuser de la publicité ciblée. Un site ou une application unique peut contenir des dizaines de trackers, créant ainsi un profil détaillé de qui vous êtes et de ce que vous faites en ligne. Après Cambridge Analytica et d'innombrables autres scandales liés à la protection des données, le danger de l’utilisation à mauvais escient de ces profils a trouvé son illustration.
