---
site: Challenges
title: "Huawei est \"en discussion\" avec Google"
date: 2019-05-21
href: https://www.challenges.fr/high-tech/huawei-est-en-discussion-avec-google_656055
featured_image: https://www.challenges.fr/img/cha/logo.svg
tags:
- Entreprise
series:
- 201921
---

> Huawei est en discussion avec Google pour tenter de trouver une solution à la rupture de ses liens avec le géant américain de l'internet, a déclaré mardi le fondateur du groupe chinois de télécommunications.
