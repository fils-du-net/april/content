---
site: Numerama
title: "CC Search: un site pour trouver facilement des photos libres de droits sur le net"
author: Julien Lausson
date: 2019-05-08
href: https://www.numerama.com/tech/511060-cc-search-un-site-pour-trouver-des-photos-libres-de-droits-sur-le-net.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/content/uploads/2019/05/creative-commons.png
tags:
- Contenus libres
series:
- 201919
series_weight: 0
---

> L'organisation Creative Commons propose un méta-moteur de recherche pour dénicher des photos libres de droits sur le net.
