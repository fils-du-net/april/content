---
site: La Tribune
title: "Open source: quand internet révolutionne le partage"
date: 2019-05-19
href: https://www.latribune.fr/economie/international/open-source-quand-internet-revolutionne-le-partage-816804.html
featured_image: https://static.latribune.fr/supermaxlittle_riverlittle_new/113774/imprimante-3d.jpg
tags:
- Promotion
series:
- 201920
series_weight: 0
---

> Si le numérique fait de chaque citoyen un client captif, certains défendent le logiciel libre comme éthique philosophique. État des lieux de la liberté informatique aux quatre coins du monde.
