---
site: Next INpact
title: "La directive droit d'auteur aboutira bien à un filtrage, applaudit le gouvernement français"
date: 2019-05-17
href: https://www.nextinpact.com/brief/-la-directive-droit-d-auteur-aboutira-bien-a-un-filtrage--applaudit-le-gouvernement-francais-8647.htm
featured_image: https://cdn2.nextinpact.com/images/bd/square-linked-media/22987.jpg
tags:
- Droit d'auteur
series:
- 201920
---

> Pendant des mois, les sociétés de gestion collective ont assuré que la directive sur le droit d'auteur n'engendrera aucune obligation de filtrage. C'est encore ce qu'affirme ce site, financé par Europe For Creators, groupe de lobbying de l'industrie culturelle qui compte dans ses rangs une ribambelle de sociétés de gestion collectives, groupes d'intérêts de la musique, ou d'éditeurs (ADAGP, l'ADAMI, CISAC, le GESAC, la SACEM, la SDRM, l'IFPI, la SCAM, la SABAM, l'UPFI, etc.).
