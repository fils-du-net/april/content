---
site: Le Monde Informatique
title: "La Dinsic actualise son référentiel de logiciels libres"
author: Jacques Cheminat
date: 2019-05-02
href: https://www.lemondeinformatique.fr/actualites/lire-la-dinsic-actualise-son-referentiel-de-logiciels-libres-75165.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000066516.jpg
tags:
- Référentiel
- Administration
series:
- 201918
series_weight: 0
---

> Après un peu d'attente, la Dinsic a publié le socle interministériel des logiciels libres (SILL) pour l'année 2019. Quelques entrées sont à souligner notamment sur l'anonymisation des données et Kubernetes.

Précision de l'équipe revue de presse: La version du SILL qui est sur github (https://disic.github.io/sill/index.html) est présentée comme une version de travail. Donc, non encore validée.
