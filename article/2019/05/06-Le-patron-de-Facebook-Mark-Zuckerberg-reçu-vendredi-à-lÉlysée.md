---
site: ouest-france.fr
title: "Le patron de Facebook Mark Zuckerberg reçu vendredi à l’Élysée"
date: 2019-05-06
href: https://www.ouest-france.fr/economie/personnalites/mark-zuckerberg/le-patron-de-facebook-mark-zuckerberg-recu-vendredi-l-elysee-6337975
featured_image: https://media.ouest-france.fr/v1/pictures/a9dccdab000f9a4c2ea079d580708280-le-patron-de-facebook-mark-zuckerberg-recu-vendredi-l-elysee.jpg
tags:
- Internet
- Entreprise
- Institutions
series:
- 201919
series_weight: 0
---

> Le Président de la république Emmanuel Macron recevra le patron de Facebook Mark Zuckerberg, ce vendredi, pour discuter de la régulation d’Internet. Les deux hommes s’étaient déjà rencontrés il y a tout juste un an en marge du sommet «Tech for good».
