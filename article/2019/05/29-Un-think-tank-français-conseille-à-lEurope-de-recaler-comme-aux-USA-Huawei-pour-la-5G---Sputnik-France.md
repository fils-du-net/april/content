---
site: Sputnik
title: "Un think tank français conseille à l'Europe de recaler, comme aux USA, Huawei pour la 5G"
date: 2019-05-29
href: https://fr.sputniknews.com/economie/201905291041281518-think-tank-francais-conseille-europe-recaler-usa-huawei-5g
featured_image: https://cdnfr2.img.sputniknews.com/images/104118/94/1041189497.jpg
tags:
- Entreprise
series:
- 201922
---

> Deux chercheurs de l'Institut Montaigne estiment que l'Europe doit recaler Huawei pour la 5G. Le think tank français insiste sur des alternatives européennes, rapporte Les Échos. Donald Trump avait déjà pris de telles dispositions récemment aux USA.
