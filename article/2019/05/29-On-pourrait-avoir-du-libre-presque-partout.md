---
site: Politis.fr
title: "«On pourrait avoir du libre presque partout» (€)"
author: Erwan Manac'h
date: 2019-05-29
href: https://www.politis.fr/articles/2019/05/on-pourrait-avoir-du-libre-presque-partout-40474/
featured_image: https://static.politis.fr/medias/articles/2019/05/on-pourrait-avoir-du-libre-presque-partout-40474/thumbnail_large-40474.jpg
tags:
- Sensibilisation
series:
- 201922
---

> Selon le juriste Lionel Maurel, derrière l'open source, c'est un questionnement économique et politique qui bouillonne.
