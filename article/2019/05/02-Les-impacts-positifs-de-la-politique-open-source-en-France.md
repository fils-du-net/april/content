---
site: Le Monde Informatique
title: "Les impacts positifs de la politique open source en France"
author: Maryse Gros
date: 2019-05-02
href: https://www.lemondeinformatique.fr/actualites/lire-les-impacts-positifs-de-la-politique-open-source-en-france-75160.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000066512.jpg
tags:
- Administration
series:
- 201918
---

> Une étude de la Harvard Business School portant sur la préférence donnée à l'open source dans l'administration française - suite à une circulaire de 2012 - montre les impacts positifs de cette décision sur la compétitivité nationale intérieure de la France.
