---
site: leParisien.fr
title: "Google suspend ses relations avec Huawei"
author: Alain Jocard et Christof Stache
date: 2019-05-20
href: http://www.leparisien.fr/economie/google-suspend-ses-relations-avec-huawei-20-05-2019-8075577.php
featured_image: http://s1.lprs1.fr/images/2019/05/20/8075577_d3346710-7aca-11e9-9d3e-87d2b70a96f5-1_1000x625.jpg
tags:
- Entreprise
series:
- 201921
---

> Cette décision aura des conséquences pour les smartphones fonctionnant avec le système Android.
