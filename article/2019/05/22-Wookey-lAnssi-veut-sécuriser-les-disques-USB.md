---
site: ZDNet France
title: "Wookey: l'Anssi veut sécuriser les disques USB"
author: Louis Adam
date: 2019-05-22
href: https://www.zdnet.fr/actualites/wookey-l-anssi-veut-securiser-les-disques-usb-39884961.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/05/BadUsb.jpg
tags:
- Innovation
- Vie privée
series:
- 201921
---

> L'Agence a présenté son disque dur «chiffrant» qui se présente comme une solution open source et open hardware, dans la continuité des efforts de l'Anssi en matière de logiciel libre.
