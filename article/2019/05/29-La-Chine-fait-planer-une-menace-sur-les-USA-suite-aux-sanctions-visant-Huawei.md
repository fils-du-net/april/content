---
site: Sputnik
title: "La Chine fait planer une menace sur les USA suite aux sanctions visant Huawei"
date: 2019-05-29
href: https://fr.sputniknews.com/international/201905291041281424-chine-menace-usa-sanctions-huawei
featured_image: https://cdnfr1.img.sputniknews.com/images/103308/21/1033082108.jpg
tags:
- Entreprise
series:
- 201922
---

> Une récente déclaration d'un responsable chinois, relayée par la Télévision centrale de Chine, semble faire la lumière sur les allégations d'après lesquelles l'Empire du milieu pourrait couper l'accès des États-Unis aux terres rares. En effet, la Chine a un quasi-monopole sur ces minéraux utilisés pour la fabrication des smartphones.
