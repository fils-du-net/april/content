---
site: LaProvence.com
title: "Google, et son système mobile Android, coupe les ponts avec Huawei"
date: 2019-05-20
href: https://www.laprovence.com/actu/en-direct/5509425/google-et-son-systeme-mobile-android-coupe-les-ponts-avec-huawei.html
featured_image: https://medias.laprovence.com/I3RI-LTkgJjNPjmtaJz6sFKi5q8=/850x575/top/smart/2ab2e8c30fc441c485ece2ed74397290/c403b184fb4db26e3618972727f01906cfe2a279.jpg
tags:
- Entreprise
series:
- 201921
---

> L'américain Google, dont le système mobile Android équipe l'immense majorité des smartphones dans le monde, a indiqué dimanche commencer à suspendre ses relations avec le chinois Huawei, qui fait partie des entreprises jugées "à risque" par Washington.
