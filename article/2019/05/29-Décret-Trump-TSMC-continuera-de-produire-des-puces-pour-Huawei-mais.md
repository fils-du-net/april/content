---
site: KultureGeek
title: "Décret «Trump»: TSMC continuera de produire des puces pour Huawei, mais…"
author: Frederic L.
date: 2019-05-29
href: http://kulturegeek.fr/news-169347/decret-trump-tsmc-continuera-produire-puces-huawei
featured_image: http://cdn.kulturegeek.fr/wp-content/uploads/2018/08/Kirin-980.jpg
tags:
- Entreprise
series:
- 201922
---

> Depuis la signature du décret «Trump», Huawei a perdu de nombreux fournisseurs, matériels ou logiciels: Google, ARM, Toshiba, Qualcomm, SD Association, la liste des défections s’allonge jour après jour. TSMC vient de stopper, sans doute temporairement, cette spirale de mauvaises nouvelles; le fondeur taiwanais affirme en effet qu’il continuera à fabriquer des processeurs Kirin pour Huawei; la production des Kirin 710 (sur les modèles Honor), des Kirin 980 (P30, P30 Pro, Mate 20 Pro) et même du prochain Kirin 985 ne serait pas affectée par le ban commercial.
