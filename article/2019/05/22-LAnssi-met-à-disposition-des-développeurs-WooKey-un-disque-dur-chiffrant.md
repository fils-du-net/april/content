---
site: L'Informaticien
title: "L'Anssi met à disposition des développeurs WooKey, un disque dur chiffrant"
author: Charlie Braume
href: https://www.linformaticien.com/actualites/id/52055/l-anssi-met-a-disposition-des-developpeurs-wookey-un-disque-dur-chiffrant.aspx
featured_image: https://www.linformaticien.com/Portals/0/2019/05_mai/wookey_01.jpg
tags:
- Innovation
- Vie privée
series:
- 201921
---

> A l'occasion de l'événement Ready for IT qui se tient actuellement à Monaco, l'Anssi a dévoilé WooKey un prototype de disque dur "chiffrant" entièrement basé sur le logiciel libre, l'open source et l'open hardware.
