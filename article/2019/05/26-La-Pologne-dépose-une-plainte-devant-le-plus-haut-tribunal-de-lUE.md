---
site: Developpez.com
title: "La Pologne dépose une plainte devant le plus haut tribunal de l'UE"
author: Stéphane le calme
date: 2019-05-26
href: https://www.developpez.com/actu/263105/La-Pologne-depose-une-plainte-devant-le-plus-haut-tribunal-de-l-UE-pour-demander-une-modification-de-la-reglementation-sur-le-droit-d-auteur
featured_image: https://www.developpez.net/forums/attachments/p478151d1/a/a/a
tags:
- Droit d'auteur
- Europe
series:
- 201921
series_weight: 0
---

> La Pologne a officiellement contesté la directive controversée sur le droit d’auteur récemment approuvée par l’Union européenne, selon Reuters, affirmant que cette législation entraînerait une censure non souhaitée. Le pays a déposé sa plainte devant la Cour de justice de l'Union européenne.
