---
site: Politis.fr
title: "Vers une révolution de l’«open source»? (€)"
author: Erwan Manac'h
date: 2019-05-29
href: https://www.politis.fr/articles/2019/05/vers-une-revolution-de-l-open-source-40473/
featured_image: https://static.politis.fr/medias/articles/2019/05/vers-une-revolution-de-l-open-source-40473/thumbnail_large-40473.jpg
tags:
- Sensibilisation
series:
- 201922
---

> Les licences libres gagnent du terrain hors de leur nid historique du numérique pour protéger les biens communs contre toutes les formes d'accaparement. Cet essor du «libre» peut-il dynamiter le capitalisme?
