---
site: Basta!
title: "Logiciels, semences, éducation: à la rencontre des activistes du «Libre», pionniers d’une société de partage"
href: https://www.bastamag.net/Logiciels-semences-education-a-la-rencontre-des-activistes-du-Libre-pionniers-d
featured_image: https://www.bastamag.net/local/adapt-img/740/15x/IMG/arton7308.png
tags:
- Partage du savoir
- Sensibilisation
series:
- 201919
series_weight: 0
---

> Tous connectés! Tous captifs? Deux logiques s’affrontent au cœur de la technologie: les principes émancipateurs du logiciel libre s’attaquent à ceux, exclusifs, du droit de la propriété intellectuelle. La révolution du partage ne concerne pas que le numérique: des pratiques collectives «non propriétaires» essaiment dans l’alimentation avec les semences libres, dans la santé avec des médicaments «open source», ou dans l’éducation grâce au libre accès à la connaissance… Le nouveau documentaire de Philippe Borrel, «Internet ou la révolution du partage», part à la rencontre de celles et ceux qui, de l’Inde aux Etats-Unis en passant par l’Europe, expérimentent ces outils d’émancipation. Il est diffusé ce mardi 7 mai à 23h55 sur Arte. Retrouvez sur Basta! une série de bonus.
