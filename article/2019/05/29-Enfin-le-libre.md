---
site: Politis.fr
title: "Enfin (le) libre!"
author: Erwan Manac'h
date: 2019-05-29
href: https://www.politis.fr/articles/2019/05/enfin-le-libre-40471/
featured_image: https://static.politis.fr/medias/articles/2019/05/enfin-le-libre-40471/thumbnail_large-40471.jpg
tags:
- Sensibilisation
series:
- 201922
series_weight: 0
---

> Initié au milieu des années 1980 par l'informaticien Richard Stallman, le mouvement du logiciel libre et de l'open source s'étend désormais à de multiples domaines, parfois éloignés de l'informatique.
