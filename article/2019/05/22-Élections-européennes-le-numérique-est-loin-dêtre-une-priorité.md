---
site: Capital.fr
title: "Élections européennes: le numérique est loin d’être une priorité"
author: Thomas Romanacce
date: 2019-05-22
href: https://www.capital.fr/economie-politique/elections-europeennes-le-numerique-est-loin-detre-une-priorite-1339178
featured_image: https://cap.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2Fcap.2F2019.2F05.2F22.2F8f163062-6c95-4051-a715-d520b2faae01.2Ejpeg/750x375/background-color/ffffff/quality/70/elections-europeennes-le-numerique-est-loin-detre-une-priorite-1339178.jpg
tags:
- Europe
- Institutions
series:
- 201921
series_weight: 0
---

> La transformation numérique affecte l’économie de l'ensemble des pays européens. Pourtant pour Antoine Ferrier, membre du Collectif pour les acteurs du marketing digital (CPA) et fondateur d'Adback, les principaux partis qui se présentent aux élections européennes n’ont pas assez intégré dans leur programme les enjeux du numérique.
