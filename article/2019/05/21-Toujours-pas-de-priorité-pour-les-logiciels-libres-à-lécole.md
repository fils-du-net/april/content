---
site: Next INpact
title: "Toujours pas de «priorité» pour les logiciels libres à l'école"
date: 2019-05-21
href: https://www.nextinpact.com/brief/toujours-pas-de---priorite---pour-les-logiciels-libres-a-l-ecole-8709.htm
featured_image: https://cdn2.nextinpact.com/images/bd/square-linked-media/22441.jpg
tags:
- Éducation
series:
- 201921
---

> Encore loupé. Les amendements visant à ce que les logiciels libres soient utilisés «en priorité» au sein des écoles et collèges n'ont pas été adoptés par le Sénat. Ils n'ont même pas été débattus, puisque les services de la Haute assemblée les ont jugé irrecevables, car dépourvus de lien direct avec le projet de loi « pour une école de la confiance » porté par le gouvernement.
