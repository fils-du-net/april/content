---
site: Le Café du Geek
title: "Un jour après, Huawei regagne sa licence Android pour 3 mois."
author: Patrick Lin
date: 2019-05-23
href: https://lecafedugeek.fr/un-jour-apres-huawei-regagne-sa-licence-android-pour-3-mois
featured_image: https://lecafedugeek.fr/wp-content/uploads/2019/05/IMG_20190521_162744-1500x1125.jpg
tags:
- Entreprise
series:
- 201921
---

> La guerre commerciale entre les États-Unis et la Chine continue avec Huawei et ZTE. Malgré leur licence Android retirée par Google suite au récent décret de Donald Trump, tout n’est pas perdu… Le 21/05/2019, on apprend que le fabricant chinois obtient de la part de Google une licence Android temporaire de 3 mois..
