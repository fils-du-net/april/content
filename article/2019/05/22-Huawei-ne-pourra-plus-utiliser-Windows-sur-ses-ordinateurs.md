---
site: cnet France
title: "Huawei ne pourra plus utiliser Windows sur ses ordinateurs"
author: Roch Arène
date: 2019-05-26
href: https://www.cnetfrance.fr/news/huawei-ne-pourra-plus-utiliser-windows-sur-ses-ordinateurs-39885009.htm
featured_image: https://www.cnetfrance.fr/i/edit/2019/02/matebook-13-new-big.jpg
tags:
- Entreprise
series:
- 201921
---

> Les ordinateurs Huawei seront privés de Windows et des composants Intel.
