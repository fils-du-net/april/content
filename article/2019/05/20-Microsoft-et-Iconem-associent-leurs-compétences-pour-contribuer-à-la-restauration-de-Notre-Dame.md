---
site: Programmez!
title: "Microsoft et Iconem associent leurs compétences pour contribuer à la restauration de Notre Dame"
author: fredericmazue
date: 2019-05-20
href: https://www.programmez.com/actualites/microsoft-et-iconem-associent-leurs-competences-pour-contribuer-la-restauration-de-notre-dame-28958
tags:
- Open Data
- Partage du savoir
- Entreprise
series:
- 201921
---

> Microsoft et Iconem annoncent le lancement de “Open Notre Dame”, une mise à disposition des données visuelles en open source, destinée à mieux comprendre et analyser l'édifice dans son histoire.
