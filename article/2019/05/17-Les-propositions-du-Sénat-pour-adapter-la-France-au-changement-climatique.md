---
site: Public Sénat
title: "Les propositions du Sénat pour adapter la France au changement climatique"
author: Ariel Guez
date: 2019-05-17
href: https://www.publicsenat.fr/article/parlementaire/les-propositions-du-senat-pour-adapter-la-france-au-changement-climatique
featured_image: https://www.publicsenat.fr/sites/default/files/styles/pse_rte_moyen/public/thumbnails/image/carte_temperature.png
tags:
- Open Data
- Institutions
series:
- 201920
series_weight: 0
---

> Les sénateurs proposent 18 mesures pour «amplifier l’effort d’adaptation de la France face aux défis du changement climatique». Parmi-elles, un plan national d’adaptation de l’agriculture et la mise en open-source des données de Météo-France.
