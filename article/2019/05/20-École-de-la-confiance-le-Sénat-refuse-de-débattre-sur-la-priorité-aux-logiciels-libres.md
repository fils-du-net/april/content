---
site: Developpez.com
title: "École de la confiance: le Sénat refuse de débattre sur la priorité aux logiciels libres"
description: "Car n'ayant aucun lien direct ou indirect avec le projet"
author: Michael Guilloux
date: 2019-05-20
href: https://www.developpez.com/actu/261868/Ecole-de-la-confiance-le-Senat-refuse-de-debattre-sur-la-priorite-aux-logiciels-libres-car-n-ayant-aucun-lien-direct-ou-indirect-avec-le-projet
featured_image: https://www.developpez.com/public/images/news/ecole-confiance.jpg
seeAlso: "- [Projet de loi école de la confiance: amendement 187 sur la priorité au logiciel libre déclaré irrecevable](https://april.org/projet-de-loi-ecole-de-la-confiance-amendement-187-sur-la-priorite-au-logiciel-libre-declare-irrecev)\n- [Projet de loi école de la confiance - Discussion au Sénat sur la priorité aux logiciels libres - 17 mai 2019](https://april.org/projet-de-loi-ecole-de-la-confiance-discussion-au-senat-sur-la-priorite-aux-logiciels-libres-17-mai)"
tags:
- Éducation
- april
series:
- 201921
series_weight: 0
---

> Après le "non" du ministre de l'Éducation et l'Assemblée nationale en février dernier, les sénateurs devaient à leur tour se prononcer sur la question de donner ou non la priorité au logiciel libre dans le cadre du projet de loi «pour une école de la confiance». Examinés du 14 au 17 mai dernier, les articles du projet de loi comprenaient en effet deux amendements particuliers pour lesquels April, l'association pour promouvoir et défendre le logiciel libre dans l'espace francophone, a demandé une forte mobilisation.
