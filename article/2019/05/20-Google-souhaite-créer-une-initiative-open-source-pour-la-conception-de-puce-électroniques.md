---
site: Clubic.com
title: "Google souhaite créer une initiative open source pour la conception de puce électroniques"
author: Mathieu Grumiaux
date: 2019-05-20
href: https://www.clubic.com/processeur/processeur-arm/actualite-857272-google-souhaite-creer-initiative-open-source-conception-puce-electroniques.html
featured_image: https://pic.clubic.com/v1/images/1699230/raw.webp?width=1200&fit=max&hash=71fcaab59ba12f96e6aeb499ba8c4123d3eabdb9
tags:
- Matériel libre
- Innovation
- Entreprise
series:
- 201921
series_weight: 0
---

> L'entreprise souhaite mettre en place une communauté open source chargée de concevoir des processeurs afin de créer des designs sur-mesure, selon les appareils équipés.
