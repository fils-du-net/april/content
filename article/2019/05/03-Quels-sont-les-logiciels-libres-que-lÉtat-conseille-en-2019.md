---
site: Numerama
title: "Quels sont les logiciels libres que l'État conseille en 2019?"
author: Julien Lausson
date: 2019-05-03
href: https://www.numerama.com/tech/510361-quels-sont-les-logiciels-libres-que-letat-conseille-en-2019.html
featured_image: https://www.numerama.com/content/uploads/2017/06/softwaves_login.png
tags:
- Référentiel
series:
- 201918
---

> Comme chaque année, l'État prépare sa liste des logiciels libres qu'il recommande. Si l'édition 2019 n'est pas encore tout à fait arrêtée, elle peut d'ores et déjà être consultée.
