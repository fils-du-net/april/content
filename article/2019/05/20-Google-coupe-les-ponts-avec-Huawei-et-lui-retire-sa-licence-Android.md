---
site: rts.ch
title: "Google coupe les ponts avec Huawei et lui retire sa licence Android"
date: 2019-05-20
href: https://www.rts.ch/info/economie/10445384-google-coupe-les-ponts-avec-huawei-et-lui-retire-sa-licence-android.html
featured_image: https://www.rts.ch/2019/05/20/09/45/10445475.image
tags:
- Entreprise
series:
- 201921
---

> Huawei sera privé d'accès aux applications et services de Google, comme Gmail ou Youtube. Propriétaire du système d'exploitation Android, Google a indiqué dimanche suspendre ses relations avec le géant chinois après un récent décret de Donald Trump.
