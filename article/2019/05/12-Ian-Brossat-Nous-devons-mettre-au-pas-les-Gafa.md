---
site: leJDD.fr
title: "Ian Brossat: 'Nous devons mettre au pas les Gafa'"
author: Ian Brossat
date: 2019-05-11
href: https://www.lejdd.fr/Politique/ian-brossat-nous-devons-mettre-au-pas-les-gafa-3898012
featured_image: https://resize-lejdd.lanmedia.fr/rcrop/940,470/img/var/europe1/storage/images/lejdd/politique/ian-brossat-nous-devons-mettre-au-pas-les-gafa-3898012/53016474-1-fre-FR/Ian-Brossat-Nous-devons-mettre-au-pas-les-Gafa.jpg
tags:
- Internet
series:
- 201919
---

> TRIBUNE - L'élu parisien communiste Ian Brossat, tête de liste de son parti aux élections européennes, souhaite que les Etats utilisent leur pouvoir pour mieux protéger l'emploi, les citoyens et la vie privée de l'influence des Gafa."
