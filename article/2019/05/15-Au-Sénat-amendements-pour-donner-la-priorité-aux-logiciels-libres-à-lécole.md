---
site: Next INpact
title: "Au Sénat, amendements pour donner la priorité aux logiciels libres à l'école"
date: 2019-05-15
href: https://www.nextinpact.com/brief/au-senat--amendements-pour-donner-la-priorite-aux-logiciels-libres-a-l-ecole-8649.htm
featured_image: https://cdn2.nextinpact.com/images/bd/wide-linked-media/22441.jpg
tags:
- april
- Éducation
series:
- 201920
---

> C'est aujourd'hui que la Haute assemblée entame l'examen du projet de loi «pour une école de la confiance», porté par le ministre de l'Éducation nationale, Jean-Michel Blanquer.
