---
site: Reporterre
title: "Le logiciel libre, un enjeu philosophique autant qu’un choix de société"
author: Philippe Borrel
date: 2019-05-07
href: https://reporterre.net/A-LA-TELE-Le-logiciel-libre-un-enjeu-philosophique-autant-qu-un-choix-de-societe
featured_image: https://reporterre.net/local/cache-vignettes/L354xH500/arton17519-ac70a.jpg?1557216274
tags:
- Promotion
- Sensibilisation
series:
- 201919
series_weight: 0
---

> Philippe Borrel, dans son film documentaire « Internet ou la révolution du partage », propose un état des lieux de la liberté informatique aux quatre coins du monde et montre comment le logiciel libre peut apporter des solutions concrètes dans une multitude de domaines. CE MARDI, À 23 H 55, SUR ARTE
