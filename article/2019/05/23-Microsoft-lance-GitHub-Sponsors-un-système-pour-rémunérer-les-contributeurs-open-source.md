---
site: Presse Citron
title: "Microsoft lance GitHub Sponsors, un système pour rémunérer les contributeurs open source"
author: Setra
date: 2019-05-23
href: https://www.presse-citron.net/microsoft-lance-github-sponsors-un-systeme-pour-remunerer-les-contributeurs-open-source
featured_image: https://www.presse-citron.net/wordpress_prod/wp-content/uploads/2019/05/Du-code-informatique.jpg
tags:
- Économie
series:
- 201921
---

> Avec GitHub Sponsors, Microsoft veut encourager les contributeurs de projets open source via un système de dons.
