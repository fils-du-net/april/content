---
site: LeMagIT
title: "L’Union Européenne veut pousser ses entreprises vers l’Open Data"
author: Alain Clapaud
date: 2019-05-31
href: https://www.lemagit.fr/actualites/252464283/LUnion-Europeenne-veut-pousser-ses-entreprises-vers-lOpen-Data
featured_image: https://cdn.ttgtmedia.com/visuals/German/EU-european-flags-fotolia.jpg
tags:
- Open Data
- Europe
- Entreprise
series:
- 201922
series_weight: 0
---

> L’Open Data comme moteur de croissance, c’est la position de la commission européenne qui a mis en place un portail de partage des données nationales depuis plusieurs années. Désormais, celle-ci cherche à aider les entreprises à emboîter le pas des Etats.
