---
site: Developpez.com
title: "France: quels sont les logiciels libres que l'État recommande en 2019?"
description: La liste des logiciels conseillés publiée officiellement avec le SILL 2019
author: Olivier Famien
date: 2019-05-16
href: https://www.developpez.com/actu/259501/France-quels-sont-les-logiciels-libres-que-l-Etat-recommande-en-2019-La-liste-des-logiciels-conseilles-publiee-officiellement-avec-le-SILL-2019
featured_image: https://www.developpez.net/forums/attachments/p472342d1/a/a/a
seeAlso: "[Publication officielle du SILL 2019](https://www.april.org/publication-officielle-du-sill-2019-socle-interministeriel-de-logiciels-libres)"
tags:
- Référentiel
series:
- 201920
series_weight: 0
---

> Dans un souci de minimiser les couts tout en conservant la qualité des logiciels utilisés, l’État français a défini depuis 2012, un cadre de convergence des logiciels libres à privilégier ou à abandonner dans le développement des systèmes d’information de ses ministères. L’ensemble des logiciels libres préconisés se présente sous la forme du socle interministériel de logiciels libres (SILL), c’est-à-dire sous la forme d’un tableau actualisé annuellement et répertoriant les logiciels conseillés ou abandonnés par l’Etat français.
