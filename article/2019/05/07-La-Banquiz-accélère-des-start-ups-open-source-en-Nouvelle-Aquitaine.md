---
site: Le Monde Informatique
title: "La Banquiz accélère des start-ups open source en Nouvelle-Aquitaine"
author: Véronique Arène
date: 2019-05-07
href: https://www.lemondeinformatique.fr/actualites/lire-la-banquiz-accelere-des-start-ups-open-source-en-nouvelle-aquitaine-75215.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000066584.jpg
tags:
- Entreprise
series:
- 201919
series_weight: 0
---

> L'accélérateur La Banquiz lance un appel à candidatures pour contribuer au développement de jeunes pousses spécialisées dans les technologies du libre et l'open-source en Nouvelle Aquitaine. Objectif pour les candidats: Passer en 9 mois, du stade d'idée à celui de business model pérenne
