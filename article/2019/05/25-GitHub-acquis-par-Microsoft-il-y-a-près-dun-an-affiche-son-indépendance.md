---
site: Le Monde.fr
title: "GitHub, acquis par Microsoft il y a près d'un an, affiche son indépendance (€)"
author: Vincent Fagot
date: 2019-05-25
href: https://www.lemonde.fr/economie/article/2019/05/25/github-acquis-par-microsoft-il-y-a-pres-d-un-an-affiche-son-independance_5467023_3234.html
featured_image: https://img.lemde.fr/2019/05/24/0/0/1279/853/688/0/60/0/f79dcca_R-RqrQqDsiyHbFoHkqtXf6Yn.jpeg
tags:
- Économie
series:
- 201921
---

> Le spécialiste du logiciel libre multiplie les gestes envers ses utilisateurs, inquiets depuis son rachat par la firme de Redmond.
