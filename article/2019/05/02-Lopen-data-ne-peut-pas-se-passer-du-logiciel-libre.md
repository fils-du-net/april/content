---
site: La gazette.fr
title: "L'open-data ne peut pas se passer du logiciel libre"
author: Baptiste Cessieux
date: 2019-05-02
href: https://www.lagazettedescommunes.com/620191/lopen-data-ne-peut-pas-se-passer-du-logiciel-libre
featured_image: https://www.lagazettedescommunes.com/wp-content/uploads/2019/05/richard-stallman-une-310x207.jpg
tags:
- Open Data
- Philosophie GNU
series:
- 201918
series_weight: 0
---

> Pionnier et défenseur du logiciel libre, Richard Stallman tenait conférence lors du salon InOut, à Rennes. Pour l’informaticien, les collectivités doivent faire attention à utiliser des logiciels qui respectent 4 libertés. L’enjeu est de taille: il s’agit ni plus ni moins de leur indépendance et de leur capacité à décider en concertation.
