---
site: Les Echos
title: "Logiciel libre: GitHub étoffe sa communauté et ses services"
author: Ninon Renaud
date: 2019-05-23
href: https://www.lesechos.fr/tech-medias/hightech/logiciel-libre-github-etoffe-sa-communaute-et-ses-services-1023490
featured_image: https://media.lesechos.com/api/v1/images/view/5ce6b6313e4546746f57b6e5/1280x720/0601285283170-web-tete.jpg
tags:
- Économie
- Entreprise
series:
- 201921
series_weight: 0
---

> Un an après son rachat par Microsoft, la plate-forme continue de séduire les rois du code. Ils sont 36 millions à utiliser GitHub, ce qui représente une augmentation de 25 % sur un an. Pour les séduire et attirer les entreprises, celle-ci renforce ses services en matière de sécurité.
