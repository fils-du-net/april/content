---
site: Next INpact
title: "Justice: un marché public ne peut favoriser un logiciel propriétaire"
author: Marc Rees
date: 2019-05-02
href: https://www.nextinpact.com/news/107840-justice-marche-public-ne-peut-favoriser-solution-proprietaire.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/22144.jpg
tags:
- Marchés publics
- Logiciels privateurs
series:
- 201918
series_weight: 0
---

> Selon une ordonnance diffusée par Next INpact, un marché public visant un logiciel propriétaire a été annulé par le tribunal administratif de Strasbourg, faute d'avoir respecté les règles de concurrence. La juridiction de référés a détaillé les conditions de cet encadrement sur fond de Code des marchés publics.
