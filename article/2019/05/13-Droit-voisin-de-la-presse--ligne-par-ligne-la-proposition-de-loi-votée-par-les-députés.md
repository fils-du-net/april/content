---
site: Next INpact
title: "Droit voisin de la presse: ligne par ligne, la proposition de loi votée par les députés (€)"
author: Marc Rees
date: 2019-05-13
href: https://www.nextinpact.com/news/107866-droit-voisin-presse-ligne-par-ligne-proposition-loi-votee-par-deputes.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/12405.jpg
tags:
- Droit d'auteur
- Institutions
series:
- 201920
---

> L’Assemblée nationale a adopté jeudi dernier la proposition de loi créant un droit voisin au profit des éditeurs et des agences de presse. 80 députés ont voté pour, une seule voix contre. Panorama des dispositions adoptées, ligne par ligne.
