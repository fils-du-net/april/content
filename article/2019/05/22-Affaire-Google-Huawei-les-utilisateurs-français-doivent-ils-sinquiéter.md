---
site: CNews
title: "Affaire Google-Huawei: les utilisateurs français doivent-ils s'inquiéter?"
date: 2019-05-22
href: https://www.cnews.fr/vie-numerique/2019-05-22/affaire-google-huawei-les-utilisateurs-francais-doivent-ils-sinquieter
featured_image: https://static.cnews.fr/sites/default/files/styles/image_640_360/public/huawei_0.jpg
tags:
- Entreprise
- International
series:
- 201921
series_weight: 0
---

> C'est un véritable coup dur pour Huawei. Le constructeur chinois de smartphones ne pourra plus utiliser le système Android pour ses mobiles, après que Google a annnoncé avoir choisi de lui retirer la licence d'exploitation.
