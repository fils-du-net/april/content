---
title: "Abandon d'Android, appel aux consommateurs: les plans B de Huawei pour se passer de Google "
date: 2019-05-23
href: http://www.lefigaro.fr/secteur/high-tech/abandon-d-android-appel-aux-consommateurs-les-plans-b-de-huawei-pour-se-passer-de-google-20190522
tags:
- Entreprise
series:
- 201921
---

> Alors que le groupe chinois risque de se voir privé du système d'exploitation de Google Android, il travaille déjà sur des pistes pour gagner en autonomie.
