---
site: Tribuca
title: "A Nice, open source en open bar"
author: Isabelle Auzias
date: 2019-05-06
href: https://tribuca.net/numerique_47410932-a-nice-open-source-en-open-bar
featured_image: https://tribuca.net/img/pictures/2019/05/20190506101037-nice-open-source-web.jpg
tags:
- Sensibilisation
series:
- 201919
---

> Au royaume de la tech, l'open source est reine, et le sera sans doute de plus en plus. Ce qui a poussé Nice Star(s)Up et le CEEI à organiser un afterwork dédié.
