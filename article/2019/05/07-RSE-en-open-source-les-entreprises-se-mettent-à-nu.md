---
site: novethic
title: "RSE en open source: les entreprises se mettent à nu"
author: Béatrice Héraud
date: 2019-05-07
href: https://www.novethic.fr/actualite/entreprise-responsable/isr-rse/la-rse-en-open-source-vers-un-nouveau-standard-de-transparence-147230.html
featured_image: https://www.novethic.fr/fileadmin/templates/novethic/img/logo-novethic.png
tags:
- Open Data
- Entreprise
series:
- 201919
series_weight: 0
---

> Laisser libre accès aux données brutes d'une entreprise concernant l’emploi des femmes ou l’impact environnemental de ses activités, c’est la nouvelle tendance. S'y mettent des sociétés publiques comme la SNCF, des industriels comme Vallourec, des banques et des assureurs. Cette transparence poussée à l'extrême vise améliorer le dialogue avec les parties prenantes et à accroître l'attractivité et l'innovation.
