---
site: Le Matin
title: "Guerre commerciale USA-Chine: La fin d'Android sur Huawei va bouleverser le marché"
date: 2019-05-20
href: https://www.lematin.ch/high-tech/La-fin-d-Android-sur-Huawei-va-bouleverser-le-marche/story/23448638
featured_image: https://files.newsnetz.ch/story/2/3/4/23448638/3/topelement.jpg
tags:
- Entreprise
series:
- 201921
---

> La décision de Donald Trump d'interdire aux sociétés américaines de commercer avec des entreprises «dangereuses» va avoir de grandes répercussions sur le marché du smartphone.
