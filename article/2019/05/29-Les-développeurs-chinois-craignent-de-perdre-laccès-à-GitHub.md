---
site: Siècle Digital
title: "Les développeurs chinois craignent de perdre l'accès à GitHub"
author: Mathias Lapierre
date: 2019-05-29
href: https://siecledigital.fr/2019/05/29/les-developpeurs-chinois-craignent-de-perdre-lacces-a-github/
featured_image: https://sd-cdn.fr/wp-content/uploads/2019/05/GitHub-770x515.jpg
tags:
- Entreprise
series:
- 201922
series_weight: 0
---

> Suite aux tensions avec les USA, les développeurs chinois craignent de perdre l'accès à GitHub, l'espace n°1 de collaboration open source.
