---
site: Nouvelle République
title: "Poitiers: Loïc Soulas, coworker de Cobalt, spécialiste en open source"
date: 2019-05-14
href: https://www.lanouvellerepublique.fr/poitiers/poitiers-loic-soulas-coworker-de-cobalt-specialiste-en-open-source
featured_image: https://images.lanouvellerepublique.fr/image/upload/t_1020w/f_auto/5cda0f8555c35bf5438b47ae.jpg
tags:
- Entreprise
- Sensibilisation
series:
- 201920
series_weight: 0
---

> Installé dans les locaux de Cobalt à Poitiers depuis un an, Loïc Soulas est un militant de l’informatique en général et de l’open source en particulier.
