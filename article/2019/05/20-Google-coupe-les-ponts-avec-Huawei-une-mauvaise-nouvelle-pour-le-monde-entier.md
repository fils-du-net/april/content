---
site: korii.
title: "Google coupe les ponts avec Huawei: une mauvaise nouvelle pour le monde entier"
author: Thomas Burgel
date: 2019-05-20
href: https://korii.slate.fr/biz/trump-interdiction-transferts-technologiques-google-huawei
featured_image: https://korii.slate.fr/sites/default/files/styles/1440x600/public/000_1go6ko.jpg
tags:
- Entreprise
series:
- 201921
---

> En privant l'entreprise chinoise de sa technologie, c'est toute la planète que Google entraîne dans la guerre commerciale entre la Chine et les États-Unis.
