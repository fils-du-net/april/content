---
site: ZDNet France
title: "Directive Copyright: Github apprend le lobbying"
author: Louis Adam
date: 2019-07-03
href: https://www.zdnet.fr/actualites/directive-copyright-github-apprend-le-lobbying-39887077.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2017/01/github_610.jpg
tags:
- Droit d'auteur
- Europe
series:
- 201927
series_weight: 0
---

> Si Github était jusqu'alors une entreprise plutôt discrète, la société s'est montrée particulièrement active sur le lobbying autour de la réforme européenne du copyright. Un tournant pour l'entreprise, désormais propriété de Microsoft. ZDNet a évoqué ce sujet avec Mike Linksvayer, de Github.
