---
site: KultureGeek
title: "GitHub confirme que l'accès au service n'est plus autorisé pour les développeurs en Iran, Syrie et Crimée"
author: Frederic L.
date: 2019-07-29
href: http://kulturegeek.fr/news-173871/github-confirme-lacces-service-nest-plus-autorise-developpeurs-iran-syrie-crimee
featured_image: http://cdn.kulturegeek.fr/wp-content/uploads/2018/06/GitHub-1024x497.jpg
tags:
- International
- Internet
series:
- 201931
---

> GitHub est aujourd’hui le plus gros service de stockage et de partage de code Open Source dans le monde; GitHub est aussi une société américaine rachetée il y a peu par Microsoft et qui en tant que telle doit se conformer aux lois de son pays. Nat Friedman, le CEO de GitHub, a par conséquent confirmé que l’accès au service (bibliothèques privées et accès payant) était désormais bloqué pour les développeurs basés en Iran, Syrie ou Crimée, un blocage qui fait suite à la décision du Département du Commerce américain de blacklister ces pays et de leur interdire toute forme de commerce avec des entreprises US. Nat Friedman avoue très franchement ne pas partager les décisions de son pays, mais explique qu’il ne peut pas faire autrement que de se conformer aux sanctions américaines.
