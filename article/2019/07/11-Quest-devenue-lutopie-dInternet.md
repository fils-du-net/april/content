---
site: Revue Projet
title: "Qu'est devenue l'utopie d'Internet?"
author: Anne Bellon
date: 2019-07-11
href: https://www.revue-projet.com/articles/2019-07-bellon-qu-est-devenue-l-utopie-d-internet/10306
featured_image: https://www.revue-projet.com/prod/file/ceras/article/img_resize/10306_large.jpg
tags:
- Internet
- Entreprise
series:
- 201928
series_weight: 0
---

> Aux débuts d’Internet, un idéal d’horizontalité, de gratuité, de liberté. Trente ans après, le web s’est centralisé, marchandisé, et a été colonisé par les géants du numérique. Ces derniers ont-ils trahi l’utopie des pionniers d’Internet?
