---
site: La Tribune
title: "Pour en finir définitivement avec le «numérique»"
author: Marcello Vitali-Rosati
date: 2019-07-30
href: https://www.latribune.fr/opinions/tribunes/pour-en-finir-definitivement-avec-le-numerique-824761.html
featured_image: https://static.latribune.fr/full_width/1189713/education-numerique.jpg
tags:
- Logiciels privateurs
- Sensibilisation
series:
- 201931
series_weight: 0
---

> Soit l’usager est le maître du code et il est donc le maître de la machine, soit il n’est pas le maître du code et il est donc à la merci d’une entreprise privée.
