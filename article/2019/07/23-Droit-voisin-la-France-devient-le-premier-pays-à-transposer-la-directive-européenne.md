---
site: Le Monde.fr
title: "«Droit voisin»: la France devient le premier pays à transposer la directive européenne"
date: 2019-07-23
href: https://www.lemonde.fr/actualite-medias/article/2019/07/23/droit-voisin-la-loi-ouvre-la-porte-a-une-dure-negociation-entre-medias-et-plates-formes_5492480_3236.html
featured_image: https://img.lemde.fr/2019/07/23/0/0/3936/2167/688/0/60/0/7a8e512_Xl7mL6ZLXlv_z-Izko324OPR.jpg
tags:
- Droit d'auteur
- Europe
series:
- 201930
series_weight: 0
---

> En transposant la loi européenne sur le droit d'auteur, le Parlement autorise la presse à négocier avec Google, Facebook ou Twitter une rémunération pour l'utilisation d'extraits d'articles et de vidéos.
