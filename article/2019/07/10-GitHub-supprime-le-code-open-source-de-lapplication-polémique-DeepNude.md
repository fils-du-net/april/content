---
site: Presse Citron
title: "GitHub supprime le code open source de l'application polémique DeepNude"
author: Louise Millon
date: 2019-07-10
href: https://www.presse-citron.net/github-supprime-code-open-source-application-polemique-deepnude
featured_image: https://www.presse-citron.net/wordpress_prod/wp-content/uploads/2018/10/github.jpg
tags:
- Sensibilisation
series:
- 201928
---

> Le service GitHub a pris la décision de supprimer toutes les copies du code permettant d'en arriver au même résultat que celui proposé par l'application DeepNude, à savoir réaliser de fausses images de femmes nues en se basant sur des photos.
