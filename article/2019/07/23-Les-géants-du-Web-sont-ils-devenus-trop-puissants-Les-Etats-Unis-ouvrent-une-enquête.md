---
site: Le Monde.fr
title: "Les géants du Web sont-ils devenus trop puissants? Les Etats-Unis ouvrent une enquête"
date: 2019-07-23
href: https://www.lemonde.fr/economie/article/2019/07/23/ouverture-d-une-enquete-antitrust-sur-les-societes-high-tech_5492664_3234.html
featured_image: https://img.lemde.fr/2019/07/24/0/0/3600/2300/688/0/60/0/47c81bf_jqtpWyPjF0OZ3b95LVLxZJDS.jpg
tags:
- Entreprise
series:
- 201930
series_weight: 0
---

> Le ministère de la justice mènera des investigations sur les pratiques anticoncurrentielles et quasi monopolistiques de certaines multinationales, qu'il n'a pas nommées.
