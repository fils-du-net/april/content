---
site: Wired
title: "Hackers Made an App That Kills to Prove a Point"
author: Lily Hay Newman
date: 2019-07-16
href: https://www.wired.com/story/medtronic-insulin-pump-hack-app
featured_image: https://media.wired.com/photos/5d28fe821da7e10008d6a9b0/master/w_942,c_limit/top%20art%20-%20insulin%20pump%20hack%20-%20alamby%20D2RPND.jpg
tags:
- Innovation
- English
series:
- 201929
---

> Two years ago, researchers Billy Rios and Jonathan Butts discovered disturbing vulnerabilities in Medtronic's popular MiniMed and MiniMed Paradigm insulin pump lines. An attacker could remotely target these pumps to withhold insulin from patients, or to trigger a potentially lethal overdose. And yet months of negotiations with Medtronic and regulators to implement a fix proved fruitless. So the researchers resorted to drastic measures. They built an Android app that could use the flaws to kill people.
