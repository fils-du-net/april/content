---
site: Le Monde.fr
title: "Taxation des GAFA: l'exemple français"
date: 2019-07-15
href: https://www.lemonde.fr/idees/article/2019/07/15/taxation-des-gafa-l-exemple-francais_5489598_3232.html
tags:
- Économie
- Entreprise
series:
- 201929
---

> ÉDITORIAL. Editorial. La taxe qui prévoit de prélever 3 % du chiffre d'affaires des géants du numérique est d'autant plus légitime et nécessaire que partout dans le monde, y compris aux Etats-Unis, la puissance de ces groupes inquiète.
