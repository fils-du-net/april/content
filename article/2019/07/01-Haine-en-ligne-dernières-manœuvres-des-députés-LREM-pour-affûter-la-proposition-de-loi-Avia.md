---
site: Next INpact
title: "Haine en ligne: dernières manœuvres des députés LREM pour affûter la proposition de loi Avia (€)"
author: Marc Rees
date: 2019-07-01
href: https://www.nextinpact.com/news/108014-haine-en-ligne-dernieres-manoeuvres-deputes-lrem-pour-affuter-proposition-loi-avia.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/17617.jpg
tags:
- Internet
- Institutions
series:
- 201927
---

> La proposition de loi Avia contre la cyberhaine sera discutée en séance dès 9h30 mercredi jusqu’à jeudi, pour un scrutin public le 9 juillet. 370 amendements ont pour l’instant été déposés pour l’heure. Tour d’horizon des principales dispositions soutenues ou défendues par la majorité.
