---
site: Next INpact
title: "Quand la Cour des comptes étrille le «plan tablettes» de François Hollande (€)"
author: Xavier Berne
date: 2019-07-18
href: https://www.nextinpact.com/news/108057-quand-cour-comptes-etrille-plan-tablettes-francois-hollande.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/22421.jpg
tags:
- Éducation
series:
- 201929
---

> Au fil d’un rapport accablant, la Cour des comptes épingle le «service public du numérique éducatif», et plus particulièrement le «plan tablettes» voulu par François Hollande. Si l’initiative s’est finalement révélée moins coûteuse que prévu, les magistrats en dénoncent les nombreuses lacunes.
