---
site: Developpez.com
title: "Microsoft demande à rejoindre la \"Linux distribution security contacts list\""
author: Stéphane le calme
date: 2019-07-06
href: https://linux.developpez.com/actu/268766/Microsoft-demande-a-rejoindre-la-Linux-distribution-security-contacts-list-pour-etre-informe-des-problemes-qui-ne-sont-pas-encore-rendus-publics
featured_image: https://www.developpez.net/forums/attachments/p489275d1/a/a/a
tags:
- Entreprise
series:
- 201927
---

> Une fois de plus, Microsoft montre son attachement à l'écosystème Linux. Il faut dire que presque tout le travail de développement de Linux est effectué en open source. L’une des rares exceptions est lorsque des entreprises ou des hackers révèlent des failles de sécurité non corrigées aux développeurs Linux. Dans ces cas, ces problèmes sont d'abord révélés dans la liste fermée appelée «Linux distribution security contacts» ou Linux-distro. C’est ce club fermé que Microsoft a demandé à rejoindre.
