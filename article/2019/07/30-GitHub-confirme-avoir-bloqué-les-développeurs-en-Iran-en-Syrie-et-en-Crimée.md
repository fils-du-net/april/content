---
site: Developpez.com
title: "GitHub confirme avoir bloqué les développeurs en Iran, en Syrie et en Crimée"
author: Bill Fassinou
date: 2019-07-30
href: https://open-source.developpez.com/actu/271822/GitHub-confirme-avoir-bloque-les-developpeurs-en-Iran-en-Syrie-et-en-Crimee-le-blocage-s-etend-desormais-au-Cuba-et-a-la-Coree-du-Nord
featured_image: https://www.developpez.net/forums/attachments/p494435d1/a/a/a
tags:
- International
- Internet
series:
- 201931
series_weight: 0
---

> Au cours de la semaine dernière, les utilisateurs de GitHub qui sont situés dans les zones comme la Crimée, la Syrie ou l’Iran ont constaté que leurs accès à la plateforme d’hébergement de code source avaient été restreints. Ils ont été nombreux à saisir Twitter pour exprimer leur consternation face à cette situation. Ces derniers ne pouvaient plus accéder à certains services de la plateforme. Pendant le week-end, GitHub a annoncé que les utilisateurs de GitHub de ces pays ont été bloqués, car l’entreprise devait se conformer aux restrictions établies par les lois US régissant le commerce, en particulier l’exportation.
