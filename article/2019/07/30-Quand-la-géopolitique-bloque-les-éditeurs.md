---
site: Programmez!
title: "Quand la géopolitique bloque les éditeurs"
author: ftonic
date: 2019-07-30
href: https://www.programmez.com/actualites/quand-la-geopolitique-bloque-les-editeurs-29209
tags:
- International
series:
- 201931
---

> Depuis quelques jours, des utilisateurs réagissent et les articles commencent à se multiplier: GitLab et GitHub coupent les accès aux utilisateurs de certains pays.
