---
site: ZDNet France
title: "Linux supplante désormais Windows sur le cloud Microsoft"
author: Steven J. Vaughan-Nichols
date: 2019-07-01
href: https://www.zdnet.fr/actualites/linux-supplante-desormais-windows-sur-le-cloud-microsoft-39886847.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2018/11/dockerwinserver2008migration.jpg
tags:
- Entreprise
series:
- 201927
---

> C'est fait! Les clients de la plateforme cloud Azure de Microsoft exploitent désormais plus de serveurs virtuels Linux que Windows. Et Microsoft ne trouve rien à y redire.
