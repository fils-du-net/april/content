---
site: The Next Web
title: "German schools ban Microsoft Office 365 amid privacy concerns"
author: Ravie Lakshmanan
date: 2019-07-15
href: https://thenextweb.com/privacy/2019/07/15/german-schools-ban-microsoft-office-365-amid-privacy-concerns/
featured_image: https://cdn0.tnwcdn.com/wp-content/blogs.dir/1/files/2019/07/office-365-hed-796x416.jpg
tags:
- Éducation
- Logiciels privateurs
- Vie privée
series:
- 201929
---

> The German state of Hesse has ruled it's illegal for its schools to use Microsoft's Windows 10 and Office 365 citing 'privacy concerns.'
