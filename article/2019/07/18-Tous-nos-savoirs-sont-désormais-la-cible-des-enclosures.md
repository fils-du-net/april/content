---
site: Usbek & Rica
title: "«Tous nos savoirs sont désormais la cible des enclosures»"
author: Fabien Benoit
date: 2019-07-18
href: https://usbeketrica.com/article/tous-nos-savoirs-sont-desormais-la-cible-des-enclosures
featured_image: https://static.usbeketrica.com/images/thumb_840xh/5d3064393d1ad.png
tags:
- Promotion
- Partage du savoir
series:
- 201929
series_weight: 0
---

> Rencontre avec le réalisateur Philippe Borrel, dont le nouveau film explore le potentiel du logiciel libre et des communs.
