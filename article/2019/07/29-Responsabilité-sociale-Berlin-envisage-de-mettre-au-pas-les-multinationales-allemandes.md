---
site: Le Monde.fr
title: "Responsabilité sociale: Berlin envisage de mettre au pas les multinationales allemandes"
date: 2019-07-29
href: https://www.lemonde.fr/economie/article/2019/07/29/responsabilite-sociale-berlin-envisage-de-mettre-au-pas-les-multinationales-allemandes_5494563_3234.html
tags:
- Entreprise
- International
- Institutions
series:
- 201931
---

> Le gouvernement d'Angela Merkel pourrait présenter un projet de loi contraignant à davantage de responsabilité éthique les groupes outre-Rhin ayant une activité à l'étranger.
