---
site: cio-online.com
title: "La nature contractuelle d'une licence de logiciel libre établie"
author: Jacques Cheminat
date: 2019-07-10
href: https://www.cio-online.com/actualites/lire-la-nature-contractuelle-d-une-licence-de-logiciel-libre-etablie-11383.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000013952.jpg
tags:
- Licenses
series:
- 201928
series_weight: 0
---

> Le TGI de Paris a jugé irrecevable une action en contrefaçon d'un éditeur sur un logiciel libre. La licence relève du régime contractuel.
