---
site: Contrepoints
title: "Directive copyright: la censure connectée arrive"
author: Glyn Moody
date: 2019-07-15
href: https://www.contrepoints.org/2019/07/15/349098-copyright-censure-connectee-arrive-voici-comment-larreter
featured_image: https://www.contrepoints.org/wp-content/uploads/2019/07/The-battle-of-copyright-by-Christopher-Dombres-640x374.jpg
tags:
- Droit d'auteur
- Europe
- Internet
series:
- 201929
series_weight: 0
---

> Les filtres sur le téléversement de l'UE arrivent. Pourquoi et comment le monde de l'Open Source doit les combattre.
