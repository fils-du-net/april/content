---
site: Contrepoints
title: "La guerre contre la culture geek"
author: Dern
date: 2019-07-16
href: https://www.contrepoints.org/2019/07/16/349155-la-guerre-contre-la-culture-geek
featured_image: https://www.contrepoints.org/wp-content/uploads/2019/07/Nerds-660x374.png
tags:
- Sensibilisation
series:
- 201929
series_weight: 0
---

> Les greffes de politiques communautaires, coercitives et visant à rallier les geeks à la cause des minorités via l'intersectionnalité, ne prennent pas au sein de la culture geek, basée sur la liberté, la rationalité et une certaine irrévérence.
