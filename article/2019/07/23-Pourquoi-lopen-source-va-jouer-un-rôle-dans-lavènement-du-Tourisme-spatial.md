---
site: TOM
title: "Pourquoi l'open source va jouer un rôle dans l'avènement du Tourisme spatial"
date: 2019-07-23
href: https://www.tom.travel/2019/07/23/pourquoi-open-source-va-jouer-role-avenement-tourisme-spatial
featured_image: https://i0.wp.com/www.tom.travel/wp-content/uploads/2019/07/yan-fisher-redhat.jpg
tags:
- Entreprise
- Internet
- International
series:
- 201930
series_weight: 0
---

> Il y a deux jours, on fêtait les 50 ans des premiers pas sur la Lune. Si l’open source n’était pas encore au cœur de la mission Apollo 11, la collaboration entre les nations et les entreprises est aujourd’hui indispensable pour réaliser des progrès significatifs et envisager Mars comme une future destination touristique. Red Hat, l’éditeur de solutions derrière Linux, a d’ailleurs installé sa solution RHEL à bord de l’ISS. Yan Fisher, Global Technology Evangelist chez Red Hat, nous a expliqué le rôle que jouait l’open source dans cette conquête de l’espace et l’avènement du Tourisme spatial.
