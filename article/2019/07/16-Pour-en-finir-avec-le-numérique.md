---
site: The Conversation
title: "Pour en finir avec le «numérique»"
author: Marcello Vitali-Rosati
date: 2019-07-22
href: https://theconversation.com/pour-en-finir-avec-le-numerique-120140
featured_image: https://cdn.theconversation.com/static/tc/tc-assets/logos/logo-en-2d9cbc0ce65fe7de13f8b2030307de62.svg
tags:
- Logiciels privateurs
- Sensibilisation
series:
- 201930
---

> Soit l'usager est le maître du code et il est donc le maître de la machine, soit il n'est pas le maître du code et il est donc à la merci d'une entreprise privée.
