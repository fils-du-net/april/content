---
site: Usbek & Rica
title: "«Créer une communauté technocritique est vital»"
author: Valentin Cebron, Nicolas Celnik et Sarah Lapied
date: 2019-07-03
href: https://usbeketrica.com/article/creer-communaute-technocritique-vital
featured_image: https://static.usbeketrica.com/images/thumb_840xh/5d1cc68383816.png
tags:
- Innovation
- Internet
series:
- 201927
series_weight: 0
---

> Pour les technocritiques «historiques», tant que la technique ne sera pas considérée comme un enjeu politique, les discussions sur le sujet resteront vaines.
