---
site: ZDNet France
title: "Epinglée pour corruption en Hongrie, Microsoft passe à la caisse"
author: Catalin Cimpanu
date: 2019-07-24
href: https://www.zdnet.fr/actualites/epinglee-pour-corruption-en-hongrie-microsoft-passe-a-la-caisse-39888151.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/02/Microsoft%20Office%20620.jpg
tags:
- Entreprise
series:
- 201930
---

> La filiale hongroise de Microsoft fait face à une affaire de corruption. Le géant américain a accepté de régler une amende de 25 millions de dollars auprès des autorités américaines pour s'éviter un procès à grand spectacle.
