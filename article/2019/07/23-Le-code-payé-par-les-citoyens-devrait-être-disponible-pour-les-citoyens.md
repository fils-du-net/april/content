---
site: Developpez.com
title: "Le code payé par les citoyens devrait être disponible pour les citoyens!"
author: Bruno
date: 2019-07-23
href: https://open-source.developpez.com/actu/271009/Le-code-paye-par-les-citoyens-devrait-etre-disponible-pour-les-citoyens-Une-petition-pour-l-open-source-lancee-par-la-Free-Software-Foundation
featured_image: https://www.developpez.net/forums/attachments/p492876d1/a/a/a
seeAlso: "[Signer la pétition](https://publiccode.eu)"
tags:
- Administration
series:
- 201930
series_weight: 0
---

>  «Nous voulons une législation qui requiert que le logiciel financé par le contribuable pour le secteur public soit disponible publiquement sous une licence de Logiciel libre et open source. S’il s’agit d’argent public, le code devrait être également public». Tels sont les mots qui colorent l’entrée du site d’une pétition lancée par la FSFE (Free Software Foundation Europe).
