---
site: Numerama
title: "Un pirate devient vice-président du Parlement européen: quels sont ses projets?"
author: Julien Lausson
date: 2019-07-05
href: https://www.numerama.com/politique/531305-un-pirate-devient-vice-president-du-parlement-europeen-quels-sont-ses-projets.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/content/uploads/2019/07/marcel-kolaja.jpg?fit=1024,1024
tags:
- Droit d'auteur
- Europe
series:
- 201927
series_weight: 0
---

> Le Tchèque Marcel Kolaja, élu au Parlement européen sur une liste du Parti pirate, sera l'un des vice-présidents de l'hémicycle.
