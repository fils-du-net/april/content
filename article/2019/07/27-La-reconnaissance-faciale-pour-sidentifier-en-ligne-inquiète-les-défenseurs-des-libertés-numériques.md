---
site: Le Monde.fr
title: "La reconnaissance faciale pour s'identifier en ligne inquiète les défenseurs des libertés numériques"
date: 2019-07-27
href: https://www.lemonde.fr/societe/article/2019/07/27/la-reconnaissance-faciale-pour-s-identifier-en-ligne-inquiete-les-defenseurs-des-libertes-numeriques_5494076_3224.html
featured_image: https://img.lemde.fr/2019/07/27/0/0/1618/1559/688/0/60/0/29ab8fb_t45cJSnTJI4DZWiD8HYG2TXN.jpg
tags:
- Vie privée
- Institutions
series:
- 201930
series_weight: 0
---

> L'association La Quadrature du Net a déposé un recours devant le Conseil d'Etat pour faire annuler le décret autorisant l'application AliceM qui permet de s'authentifier sur les sites administratifs en prenant une vidéo de soi.
