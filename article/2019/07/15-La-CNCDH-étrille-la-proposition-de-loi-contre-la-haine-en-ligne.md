---
site: Next INpact
title: "La CNCDH étrille la proposition de loi contre la haine en ligne"
author: Marc Rees
date: 2019-07-15
href: https://www.nextinpact.com/news/108044-la-cncdh-etrille-proposition-loi-contre-haine-en-ligne.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/5713.jpg
tags:
- Internet
- Institutions
series:
- 201929
---

> La commission nationale consultative des droits de l’homme (CNCDH) a rendu un avis pour le moins négatif sur le projet de loi contre la cyberhaine. Adoptée par l’Assemblée nationale à une large majorité, la proposition de loi sera examinée au Sénat à la rentrée.
