---
site: L'Informaticien
title: "Licence libre, un contrat comme un autre"
author: Guillaume Périssat
href: https://www.linformaticien.com/actualites/id/52414/licence-libre-un-contrat-comme-un-autre.aspx
tags:
- Licenses
series:
- 201928
---

> Le tribunal de Grande Instance de Paris a rendu son verdict dans une affaire opposant depuis 2011 Orange à Entr’ouvert, un éditeur de logiciels libres. Ce dernier accusait l’opérateur de contrefaçon quant à son utilisation d’un programme sous licence GNU, mais le juge lui a donné tort, estimant que sa relation avec Orange est «de nature contractuelle» et déclarant son action en justice irrecevable.
