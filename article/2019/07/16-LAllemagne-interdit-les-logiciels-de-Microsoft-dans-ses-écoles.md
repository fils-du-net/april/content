---
site: HITEK.fr
title: "L'Allemagne interdit les logiciels de Microsoft dans ses écoles"
author: Mickaël
date: 2019-07-16
href: https://hitek.fr/actualite/microsoft-windows-10-office-365_20111
featured_image: https://static.hitek.fr/img/actualite/ill_c/1669840787/office365.png
tags:
- Éducation
- Logiciels privateurs
- Vie privée
series:
- 201929
series_weight: 0
---

> Les pays européens se tournent de plus en plus vers les logiciels open source pour être indépendants. La France en est un bon exemple et migre l'ensemble de ses administrations et plus particulièrement les écoles vers l'open source. D'ailleurs l'État français a mis à disposition une liste des logiciels libres recommandés. Concernant l'Allemagne, c'est une tout autre histoire puisqu'elle a formellement interdit Windows 10 et Office 365 de ses écoles, une décision plutôt radicale.
