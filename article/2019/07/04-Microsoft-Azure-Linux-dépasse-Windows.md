---
site: GinjFo
title: "Microsoft Azure, Linux dépasse Windows"
date: 2019-07-04
href: https://www.ginjfo.com/actualites/logiciels/linux/microsoft-azure-linux-depasse-windows-20190703
featured_image: https://www.ginjfo.com/wp-content/uploads/2019/07/Azure_Linux.jpg
tags:
- Entreprise
series:
- 201927
series_weight: 0
---

> Dans un récent entretient Microsoft va bien plus loin que de réaffirmer son slogan «Microsoft loves Linux». Le géant déclare que ce système d’exploitation est utilisé massivement sur Microsoft Azure. La plate-forme open source s’est tellement développée qu’elle a finalement réussi à dépasser Windows Server.
