---
site: Next INpact
title: "Le département américain de la Justice ouvre une enquête antitrust contre les géants de la tech"
author: Marc Rees
date: 2019-07-24
href: https://www.nextinpact.com/news/108075-le-departement-americain-justice-ouvre-enquete-antitrust-contre-geants-tech.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/13581.jpg
tags:
- Entreprise
series:
- 201930
---

> Le département de la Justice a annoncé hier l’ouverture d’une enquête antitrust visant les pratiques des plateformes en ligne. Aucun nom n’est donné, mais en visant les «leaders du marché», autant dire qu’Amazon, Facebook ou encore Google et Apple devraient, sauf surprise, être dans la boucle.
