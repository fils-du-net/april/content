---
site: Developpez.com
title: "GitHub bloque les utilisateurs de Crimée et d'Iran à cause des sanctions US"
description: "L'open source doit-il souffrir de la politique de Trump?"
author: Patrick Ruiz
date: 2019-07-26
href: https://open-source.developpez.com/actu/271530/GitHub-bloque-les-utilisateurs-de-Crimee-et-d-Iran-a-cause-des-sanctions-US-l-open-source-doit-il-souffrir-de-la-politique-de-Trump
featured_image: https://www.developpez.net/forums/attachments/p493941d1/a/a/a
tags:
- International
series:
- 201930
series_weight: 0
---

> L'un des signalements est d’un utilisateur du service web d’hébergement et de gestion de logiciels touche en premier à ce qui font usage de GameHub sous Linux. L’application sert à centraliser dans une bibliothèque tous les jeux en provenance de Steam, GoG, Humble Bundle, Humble Trove ainsi que les jeux installés de manière indépendante en local. Grâce à GameHub, il est possible de voir d’un seul coup d’œil, mais aussi de télécharger, installer, désinstaller et lancer ses jeux, mais aussi les bonus ou les DLC en provenance de GoG.
