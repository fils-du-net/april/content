---
site: Numerama
title: "Pots-de-vin à l'étranger: Microsoft versera 25 millions de dollars de pénalités aux USA"
author: Julien Lausson
date: 2019-07-23
href: https://www.numerama.com/politique/535378-pots-de-vin-a-letranger-microsoft-versera-25-millions-de-dollars-de-penalites-aux-usa.html
featured_image: https://www.numerama.com/content/uploads/2019/07/microsoft.jpg
tags:
- Entreprise
series:
- 201930
---

> Microsoft a accepté de régler des pénalités à hauteur de 25 millions de dollars auprès des autorités américaines. L'entreprise était poursuivie pour des faits de corruption à l'étranger.
