---
site: FrAndroid
title: "Vie privée: Windows 10 et Office 365 bannis des écoles en Allemagne, quid de la France?"
author: Cassim Ketfi
date: 2019-07-15
href: https://www.frandroid.com/marques/microsoft/609553_vie-privee-windows-10-et-office-365-bannis-des-ecoles-en-allemagne-quid-de-la-france/
featured_image: https://images.frandroid.com/wp-content/uploads/2019/01/windows-7-vs-10.jpg
tags:
- Éducation
- Logiciels privateurs
- Vie privée
series:
- 201929
---

> La CNIL allemande vient de rendre un jugement en Allemagne interdisant l’utilisation de Windows 10 et Office 365 dans les écoles, car ces produits enfreignent le RGPD. Une situation qui pourrait donc se répéter en France.
