---
site: The Conversation
title: "Le «numérique», une notion qui ne veut rien dire"
author: Marcello Vitali-Rosati
date: 2019-07-22
href: https://theconversation.com/le-numerique-une-notion-qui-ne-veut-rien-dire-116333
featured_image: https://cdn.theconversation.com/static/tc/tc-assets/logos/logo-en-2d9cbc0ce65fe7de13f8b2030307de62.svg
tags:
- Logiciels privateurs
- Sensibilisation
series:
- 201930
---

> Nous parlons de plus en plus de «numérique» en substantivant un adjectif qui – initialement – comporte une signification technique précise.
