---
site: L'OBS
title: "Utopie ou dystopie? Le futur de la démocratie en question"
author: Tomas Statius
date: 2019-07-03
href: https://www.nouvelobs.com/2049/20190703.OBS15423/utopie-ou-dystopie-le-futur-de-la-democratie-en-question.html
featured_image: https://media.nouvelobs.com/cover/75/teleobs/1562529722/cover.png
tags:
- Innovation
series:
- 201927
---

> Pas facile de penser le futur de nos démocraties dans un monde traversé par des crises qui s'entremêlent. A Nantes, «l'Obs» s'est attelé à cette question dans le cadre de son cycle de conférences «2049».
