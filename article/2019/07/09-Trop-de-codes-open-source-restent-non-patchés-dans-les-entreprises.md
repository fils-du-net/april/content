---
site: ITforBusiness
title: "Trop de codes open source restent non patchés dans les entreprises"
author: Laurent Delattre
date: 2019-07-09
href: https://www.itforbusiness.fr/trop-de-codes-open-source-restent-non-patches-dans-les-entreprises-20752
featured_image: https://www.itforbusiness.fr/wp-content/uploads/2019/07/shutterstock_397445059.jpg
tags:
- Entreprise
series:
- 201928
series_weight: 0
---

> Les entreprises tardent beaucoup trop à corriger les codes en open source qu’elles utilisent. Une mauvaise pratique qui devient critique en termes de cybersécurité.
