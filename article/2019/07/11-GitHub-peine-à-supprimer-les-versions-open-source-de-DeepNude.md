---
site: LeBigData.fr
title: "GitHub peine à supprimer les versions open source de DeepNude"
author: Bastien L
date: 2019-07-11
href: https://www.lebigdata.fr/github-supprime-deepnude
featured_image: https://www.lebigdata.fr/wp-content/uploads/2019/07/deepnude-github-660x330.jpg
tags:
- Sensibilisation
series:
- 201928
series_weight: 0
---

> GitHub peine à supprimer les versions Open Source de DeepNude, l'outil d'intelligence artificielle qui permet de déshabiller les femmes.
