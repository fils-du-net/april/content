---
site: ZDNet France
title: "'LoL, une affaire sérieuse', documentaire sur le logiciel libre"
author: Thierry Noisette
date: 2019-07-31
href: https://www.zdnet.fr/blogs/l-esprit-libre/lol-une-affaire-serieuse-documentaire-sur-le-logiciel-libre-39888597.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2019/07/LoL-documentaire.jpg
tags:
- Sensibilisation
- april
series:
- 201931
series_weight: 0
---

> Ce documentaire sur le logiciel libre en présente, en une petite heure, la philosophie et les enjeux. Un film à faire connaître.
