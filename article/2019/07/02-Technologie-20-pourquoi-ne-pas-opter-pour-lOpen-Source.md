---
site: AndroidPIT
title: "Technologie 2.0: pourquoi ne pas opter pour l'Open Source?"
author: Benoit Pepicq
date: 2019-07-02
href: https://www.androidpit.fr/meilleures-alternatives-open-source-android
featured_image: https://fscl01.fonpit.de/userfiles/7043987/image/news/androidpit-open-source-software-w810h462.jpg
tags:
- Sensibilisation
- Licenses
series:
- 201927
series_weight: 0
---

> L'Open Source peut-il rivaliser avec les grands noms de la tech qui s'engouffrent toujours plus dans des problèmes de confidentialité et de sécurité?
