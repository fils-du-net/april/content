---
site: ZDNet France
title: "Microsoft Office 365 banni de certaines écoles allemandes"
author: Cathrin Schaer
date: 2019-07-16
href: https://www.zdnet.fr/actualites/microsoft-office-365-banni-de-certaines-ecoles-allemandes-39887741.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/07/istock-ecoles-office365-620.jpg
tags:
- Éducation
- Logiciels privateurs
- Vie privée
series:
- 201929
---

> En raison d'un risque d'espionnage de la part des services de renseignement américains, le Land de la Hesse a décidé d'interdire aux établissements scolaires l'utilisation de la suite cloud de Microsoft, Office 365.
