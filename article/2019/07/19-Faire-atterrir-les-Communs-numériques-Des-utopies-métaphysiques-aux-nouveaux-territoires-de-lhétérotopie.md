---
site: France Culture
title: "Faire atterrir les Communs numériques. Des utopies métaphysiques aux nouveaux territoires de l'hétérotopie"
date: 2019-07-19
href: https://www.franceculture.fr/conferences/maison-de-la-recherche-en-sciences-humaines/faire-atterrir-les-communs-numeriques-des-utopies-metaphysiques-aux-nouveaux-territoires-de
featured_image: https://cdn.radiofrance.fr/s3/cruiser-production/2019/07/c6188191-8033-4b19-8e5e-ce83c4f564ac/838_lionelmaurelfc.webp
tags:
- Contenus libres
series:
- 201929
series_weight: 0
---

> Nous sommes entrés dans 'une sorte de nouvel âge des communs, celle de l'enracinement des communs dans la société, de leur extension à des domaines sans cesse élargis de la vie sociale et de leur pérennisation dans le temps'.
