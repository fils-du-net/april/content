---
site: Next INpact
title: "Pas de sensibilisation obligatoire des fonctionnaires aux enjeux numériques"
date: 2019-07-10
href: https://www.nextinpact.com/brief/pas-de-sensibilisation-obligatoire-des-fonctionnaires-aux-enjeux-numeriques-9327.htm
featured_image: https://cdn2.nextinpact.com/images/bd/square-linked-media/21473.jpg
tags:
- Administration
- Sensibilisation
series:
- 201928
series_weight: 0
---

> Députés et sénateurs sont parvenus à un accord, jeudi 4 juillet, en commission mixte paritaire, au sujet du projet de loi relatif à la fonction publique (voir le texte). Aucun des amendements adoptés fin juin par le Sénat afin de protéger la souveraineté numérique de la France n'a néanmoins été retenu.
