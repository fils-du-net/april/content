---
site: Slate.fr
title: "Liberté d'expression et lutte contre la haine sur internet, un difficile exercice d'équilibriste"
date: 2019-07-24
href: https://www.slate.fr/story/180015/internet-proposition-loi-avia-lutte-haine-reseaux-sociaux-liberte-expression
featured_image: https://www.slate.fr/sites/default/files/styles/1060x523/public/liberte_expression_lutte_haine_en_ligne_loi_avia.jpg
tags:
- Internet
- Institutions
series:
- 201930
series_weight: 0
---

> Adoptée à l'Assemblée le 9 juillet, la proposition de loi visant à combattre les contenus haineux en ligne divise les internautes. L'examen du texte par le Sénat est prévu pour septembre.
