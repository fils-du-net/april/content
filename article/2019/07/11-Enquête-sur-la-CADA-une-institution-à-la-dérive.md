---
site: Next INpact
title: "Enquête sur la CADA, une institution à la dérive (€)"
author: Xavier Berne
date: 2019-07-11
href: https://www.nextinpact.com/news/107986-enquete-sur-cada-institution-a-derive.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/2141.jpg
tags:
- Open Data
- Administration
series:
- 201928
---

> Retards dans le traitement des dossiers, avis jugés «anti-transparence», problèmes internes... Depuis des mois, la Commission d’accès aux documents administratifs (CADA) fait l’objet de vives critiques. Afin de faire la lumière sur les dysfonctionnements de l’institution, Next INpact a pu recueillir de précieux témoignages.
