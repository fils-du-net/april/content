---
site: Next INpact
title: "La proposition de loi Avia passe le cap de l'Assemblée nationale (€)"
author: Marc Rees
date: 2019-07-05
href: https://www.nextinpact.com/news/108028-la-proposition-loi-avia-passe-cap-lassemblee-nationale.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/23231.jpg
tags:
- Internet
- Institutions
series:
- 201927
---

> Les députés ont achevé hier soir l’examen de la proposition de loi Avia contre les contenus haineux sur Internet. Tour d’horizon des principales dispositions, amendées puis adoptées au fil des débats.
