---
site: ZDNet France
title: "L'utilisation de logiciels libres, un critère du label 'numérique inclusif'"
author: Thierry Noisette
date: 2019-03-31
href: https://www.zdnet.fr/blogs/l-esprit-libre/l-utilisation-de-logiciels-libres-un-critere-du-label-numerique-inclusif-39882787.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2019/03/formation_ordinateurs_WP.jpg
tags:
- Référentiel
- Institutions
- Open Data
series:
- 201913
---

> Avant de quitter le gouvernement, le secrétaire d'État au Numérique Mounir Mahjoubi a créé  un label pour les dispositifs destinés à aider les publics 'loin du numérique'.
