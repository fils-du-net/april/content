---
site: Next INpact
title: "Une sénatrice américaine veut démanteler les GAFAM pour restaurer la concurrence"
date: 2019-03-15
href: https://www.nextinpact.com/brief/une-senatrice-americaine-veut-demanteler-les-gafam-pour-restaurer-la-concurrence-8045.htm
featured_image: https://cdn2.nextinpact.com/images/bd/wide-linked-media/22606.jpg
tags:
- Entreprise
- Institutions
series:
- 201911
---

> Les GAFAM sont dans l'œil de la sénatrice démocrate Elizabeth Warren, qui sera candidate aux prochaines élections présidentielles américaines. Elle estime que ces entreprises ont «trop de pouvoir sur notre économie, notre société et notre démocratie».
