---
site: LeBigData
title: "Apache Software Foundation: tout savoir sur l'ASF et ses projets Big Data"
date: 2019-03-08
author: Bastien L
href: https://www.lebigdata.fr/apache-software-foundation
featured_image: https://www.lebigdata.fr/wp-content/uploads/2019/03/apache-software-foundation-tout-savoir-1050x525.jpg
tags:
- Associations
- Informatique en nuage
- Internet
series:
- 201910
series_weight: 0
---

> La fondation Apache Software Foundation supervise le développement de logiciels Big Data comme Hadoop, Hive ou Cassandra. Découvrez tout ce que vous devez savoir à son sujet.
