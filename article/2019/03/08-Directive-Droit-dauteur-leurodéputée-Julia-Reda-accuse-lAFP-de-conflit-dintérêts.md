---
site: Next INpact
title: "Directive Droit d'auteur: l'eurodéputée Julia Reda accuse l'AFP de conflit d'intérêts"
author: Marc Rees
date: 2019-03-08
href: https://www.nextinpact.com/news/107692-directive-droit-dauteur-leurodeputee-julia-reda-accuse-lafp-conflit-dinterets.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/22600.jpg
tags:
- Droit d'auteur
- Europe
- Institutions
series:
- 201910
---

> La mise en cause est frontale: l'eurodéputée Julia Reda accuse l’AFP de conflits d’intérêts sur la proposition de directive relative au droit d’auteur. Retour en profondeur dans les méandres de cet épisode, alors que le texte est désormais en phase finale d’adoption au Parlement européen.
