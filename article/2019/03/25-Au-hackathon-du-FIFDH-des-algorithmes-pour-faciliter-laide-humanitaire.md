---
site: Le Temps
title: "Au hackathon du FIFDH, des algorithmes pour faciliter l'aide humanitaire"
author: Charles Foucault-Dumas
date: 2019-03-25
href: https://www.letemps.ch/societe/hackathon-fifdh-algorithmes-faciliter-laide-humanitaire
featured_image: https://assets.letemps.ch/sites/default/files/styles/np8_full/public/media/2019/03/25/file74mvuk95mphkmsf1lvb.jpg?itok=87pxsEn-
tags:
- Innovation
- Partage du savoir
series:
- 201913
series_weight: 0
---

> Leur projet, élaboré durant le hackathon du FIFDH, nous avait convaincus: Charles Foucault-Dumas et son équipe racontent la genèse de leur idée, germée entre deux poignées de bonbons
