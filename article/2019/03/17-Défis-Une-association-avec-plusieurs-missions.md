---
site: Le Telegramme
title: "Défis. Une association avec plusieurs missions"
date: 2019-03-17
href: https://www.letelegramme.fr/morbihan/lanester/defis-une-association-avec-plusieurs-missions-17-03-2019-12234007.php
featured_image: https://www.letelegramme.fr/images/2019/03/17/de-nombreuses-visites-ont-eu-lieu-vendredi-dans-les-locaux_4469218_440x330p.jpg
tags:
- Associations
- Promotion
series:
- 201911
series_weight: 0
---

> L’association Défis organisait, vendredi, la journée du Libre et une opération portes ouvertes de ses locaux, boulevard Leclerc. Durant toute la journée, les bénévoles et salariés de Défis ont reçu un nombreux public venu s’informer sur les différentes missions de l’association et les logiciels libres.
