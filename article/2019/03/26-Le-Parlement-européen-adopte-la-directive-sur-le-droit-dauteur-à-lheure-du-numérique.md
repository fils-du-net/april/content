---
site: Next INpact
title: "Le Parlement européen adopte la directive sur le droit d'auteur à l'heure du numérique"
author: Marc Rees
date: 2019-03-26
href: https://www.nextinpact.com/news/107739-le-parlement-europeen-adopte-directive-sur-droit-dauteur-a-lheure-numerique.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/13419.jpg
tags:
- Droit d'auteur
- Europe
- Institutions
series:
- 201913
---

> Le Parlement européen a adopté à une large majorité la proposition de directive sur le droit d’auteur. Par 348 voix pour et 274 contre. Premières réactions.
