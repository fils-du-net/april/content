---
site: Basta!
title: "Grand débat: pourquoi la plateforme de consultation en ligne est vivement critiquée pour son opacité "
date: 2019-03-07
author: Rachel Knaebel
href: https://www.bastamag.net/Grand-debat-pourquoi-la-plateforme-de-consultation-en-ligne-est-vivement
featured_image: https://www.bastamag.net/local/adapt-img/740/15x/IMG/arton7186.jpg?1551952571
tags:
- Logiciels privateurs
- Institutions
- Open Data
series:
- 201910
series_weight: 0
---

> Comment seront traitées et hiérarchisées les contributions des citoyens au grand débat? Les outils numériques utilisés sont-ils transparents? Des lobbies ou groupes de pression sont-ils en mesure d’influencer la synthèse de proposition qui sera rendue publique? Les opinions émises risquent-elles de servir à des campagnes de manipulation à l’approche des élections européennes? Plusieurs acteurs du numérique critiquent vivement le choix qui a été fait par le gouvernement et son prestataire, la startup Cap collectif qui gère la plateforme du grand débat. Pourtant, des outils transparents, qui permettent une véritable appropriation démocratique, existent.
