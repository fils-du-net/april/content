---
site: Numerama
title: "La directive controversée sur les droits d'auteur (et l'article 13) est adoptée par le Parlement européen"
author: Perrine Signoret
date: 2019-03-26
href: https://www.numerama.com/politique/475565-la-directive-controversee-sur-les-droits-dauteur-et-larticle-13-est-adoptee-par-le-parlement-europeen.html
featured_image: https://www.numerama.com/content/uploads/2019/03/drapeau-europeen.jpg
tags:
- Droit d'auteur
- Europe
- Institutions
series:
- 201913
---

> Le texte de la controversée directive sur les droits d'auteur a été débattu et voté au Parlement européen mardi 26 mars. Le vote est positif.
