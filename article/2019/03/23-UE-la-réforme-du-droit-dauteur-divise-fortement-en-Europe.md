---
site: francetv info
title: "UE: la réforme du droit d'auteur divise fortement en Europe"
date: 2019-03-23
href: https://culturebox.francetvinfo.fr/culture/ue-la-reforme-du-droit-d-auteur-divise-fortement-en-europe-286956
featured_image: https://culturebox.francetvinfo.fr/sites/default/files/assets/images/2019/03/parlement_strasbourg_afp.jpg
tags:
- Droit d'auteur
- Institutions
- Europe
series:
- 201912
---

> Les représentants des auteurs et traducteurs européens ont exhorté vendredi le Parlement européen à voter le 26 mars pour la directive sur la réforme du droit d'auteur, soutenue par des artistes, médias, photographes... Mais le texte a des détracteurs très influents. Des milliers de personnes ont manifesté samedi en Allemagne pour 'sauver internet', appelant les eurodéputés à rejeter la directive.
