---
site: Next INpact
title: "Edward Snowden s'oppose à l'article 13 de proposition de directive sur le droit d'auteur"
date: 2019-03-06
href: https://www.nextinpact.com/brief/edward-snowden-s-oppose-a-l-article-13-de-proposition-de-directive-sur-le-droit-d-auteur-7988.htm
featured_image: https://cdn2.nextinpact.com/images/bd/square-linked-media/16479.jpg
tags:
- Droit d'auteur
- Internet
- Europe
series:
- 201910
series_weight: 0
---

> «Si vous êtes de l'Union européenne, soyez actif dès maintenant, rendez-vous sur pledge2019.eu et demandez à votre représentant de "#SaveYourInternet"». Edward Snowden est intervenu sur son fil Twitter pour dire tout le mal qu'il pense de l'actuel article 13 de la proposition de directive sur le droit d'auteur.
