---
site: Next INpact
title: "Directive Droit d'auteur: déjà une mission Hadopi-CNC-CSPLA sur la reconnaissance des contenus"
author: Marc Rees
date: 2019-03-28
href: https://www.nextinpact.com/news/107746-directive-droit-dauteur-deja-mission-hadopi-cnc-cspla-sur-reconnaissance-contenus.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/17595.jpg
tags:
- Droit d'auteur
- Europe
- Institutions
- HADOPI
series:
- 201913
---

> Hier, à Lille, Franck Riester a donné de nouveaux détails sur les suites de l’adoption de la directive sur le droit d’auteur. La Hadopi va plancher avec le CNC et le CSPLA sur la reconnaissance automatisée des contenus pour rendre «efficace» l’article 17 (ex-article 13) du texte européen. Sujet que connaît très bien le ministre de la Culture.
