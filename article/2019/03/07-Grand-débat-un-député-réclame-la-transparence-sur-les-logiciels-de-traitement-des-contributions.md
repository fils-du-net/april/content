---
site: Next INpact
title: "Grand débat: un député réclame la transparence sur les logiciels de traitement des contributions"
author: Xavier Berne
date: 2019-03-07
href: https://www.nextinpact.com/news/107685-grand-debat-depute-reclame-transparence-sur-logiciels-traitement-contributions.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/16029.jpg
tags:
- Logiciels privateurs
- Institutions
- Open Data
series:
- 201910
---

> Alors que le gouvernement s’est résolu à publier les contributions issues du «grand débat national» en Open Data, le député Stéphane Peu réclame désormais que le code source des logiciels utilisés pour analyser ces données soit lui aussi ouvert.
