---
site: Le Monde.fr
title: "Primaires démocrates: Elizabeth Warren propose de démanteler Amazon, Google et Facebook"
date: 2019-03-11
author: Arnaud Leparmentier
href: https://www.lemonde.fr/international/article/2019/03/11/primaires-democrates-elizabeth-warren-propose-de-demanteler-amazon-google-et-facebook_5434246_3210.html
featured_image: https://img.lemde.fr/2019/03/11/0/0/5502/3668/688/0/60/0/72220bf_JCUvP5k4gZGCuNop52Emu7OO.jpg
tags:
- Entreprise
- Institutions
series:
- 201911
---

> La sénatrice du Massachusetts est en quatrième position dans les sondages, mais elle fait la course en tête des idées de gauche.
