---
site: Next INpact
title: "Les partisans de la directive Droit d'auteur, un lobbying jusqu'à saturation"
author: Marc Rees
date: 2019-03-25
href: https://www.nextinpact.com/news/107733-les-partisans-directive-droit-dauteur-lobbying-jusqua-saturation.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/15338.jpg
tags:
- Droit d'auteur
- Europe
- Institutions
series:
- 201913
---

> À quelques heures du vote au Parlement européen, les partisans de la proposition de directive sur le droit d’auteur saturent les principaux canaux d’information. Un exercice de style qui n’empêche pas quelques bourdes et autres oublis déontologiques.
