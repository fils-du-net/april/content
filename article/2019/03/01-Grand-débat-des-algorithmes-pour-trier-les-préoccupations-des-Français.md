---
site: BFMtv
title: "Grand débat: des algorithmes pour trier les préoccupations des Français"
author: Elsa Trujillo
date: 2019-03-01
href: https://www.bfmtv.com/tech/grand-debat-des-algorithmes-pour-trier-les-preoccupations-des-francais-1641892.html
featured_image: https://www.bfmtv.com/i/0/0/787/e1fce947ef52e3190dcb6d679d747.jpeg
tags:
- Open Data
- Institutions
series:
- 201909
series_weight: 0
---

> Plus d'un million de contributions au grand débat national seront brassées de façon automatique par des algorithmes, pour rendre compte des idées les plus plébiscitées. L'initiative laisse craindre quelques biais d'analyse.
