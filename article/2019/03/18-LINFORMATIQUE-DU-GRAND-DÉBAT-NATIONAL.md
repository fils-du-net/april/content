---
site: L'Informaticien
title: "L'INFORMATIQUE DU GRAND DÉBAT NATIONAL"
date: 2019-03-18
href: https://www.linformaticien.com/dossiers/linformatique-du-grand-debat-national.aspx
featured_image: https://www.linformaticien.com/Portals/0/2019/03_mars/inf175_gdn_01.jpg
tags:
- Logiciels privateurs
- Open Data
- Institutions
series:
- 201912
---

> Depuis fin janvier, les Françaises et les Français peuvent contribuer au Grand Débat National promis par l’exécutif. Au moment où nous écrivons ces lignes, 900 000 contributions ont été enregistrées sur granddebat.fr, ainsi que plus de 6 000 réunions publiques, elles bien physiques. Sur Internet et dans les collectivités, questionnaires ouverts et contributions libres, format dématérialisé et papier, l’hétérogénéité des contributions promet un sacré casse-tête pour les prestataires en charge d’en réaliser d’ici à la fin avril la synthèse. D’autant qu’il faudrait déjà comprendre qui fait quoi. De l’intérieur comme de l’extérieur, il y a de quoi se perdre dans l’informatique du Grand Débat!
