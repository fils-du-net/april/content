---
site: Reporterre, le quotidien de l'écologie
title: "Amers, déçus, enthousiastes... vos avis sur le «grand débat national»"
date: 2019-03-14
href: https://reporterre.net/Amers-decus-enthousiastes-vos-avis-sur-le-grand-debat-national
featured_image: https://reporterre.net/local/cache-vignettes/L720xH405/arton16966-67d1f.jpg
tags:
- Logiciels privateurs
- Institutions
series:
- 201911
series_weight: 0
---

> Le «grand débat national» lancé par Emmanuel Macron comme réponse au mouvement des Gilets jaunes se termine en fin de semaine. Que pouvons-nous en penser? Reporterre a posé la question à plusieurs personnes qui y ont participé, Gilet jaune ou pas.
