---
site: Le Monde.fr
title: "Comment le Web a «avalé» Internet"
author: William Audureau
date: 2019-03-12
href: https://www.lemonde.fr/pixels/article/2019/03/12/comment-le-web-a-avale-internet_5434770_4408996.html
featured_image: https://img.lemde.fr/2019/03/12/0/0/3500/2337/688/0/60/0/c3f48ce_1MxbjMObwU0mwP2tAfjb9gn-.jpg
tags:
- Internet
- Partage du savoir
series:
- 201911
---

> Le Web, qui fête ses 30ans mardi 12mars, s'est imposé comme l'application la plus iconique du célèbre réseau informatique mondial, éclipsant newsgroups, e-mails et tchats.
