---
site: Le Temps
title: "Hacker pour la science à Genève"
author: Florent Hiard
date: 2019-03-22
href: https://www.letemps.ch/sciences/hacker-science-geneve
featured_image: https://assets.letemps.ch/sites/default/files/styles/np8_full/public/media/2019/03/22/file74ldtvmqkth8o5sfgmy.jpg?itok=X-Ixr3eh
tags:
- Innovation
- Sciences
series:
- 201912
series_weight: 0
---

> Le hackathon est à l’honneur à Genève. Jusqu’à dimanche, l’Open Geneva Festival met en avant ce marathon de l’innovation collaborative sur de nombreux sites de l’agglomération
