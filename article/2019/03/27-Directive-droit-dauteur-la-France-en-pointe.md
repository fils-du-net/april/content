---
site: reflets.info
title: "Directive droit d'auteur: la France en pointe"
author: Jef Mathiot
date: 2019-03-27
href: https://reflets.info/articles/directive-droit-d-auteur-la-france-en-pointe
featured_image: https://reflets.info/system/articles/featured_images/1845217b-7810-4d06-ab9f-daf58b9926e8/big.JPG?1553699413
tags:
- Droit d'auteur
- Europe
- Institutions
series:
- 201913
---

> Les députés français se sont distingués au Parlement européen, en votant comme un seul homme — ou presque — la directive droit d'auteur.
