---
site: La revue européenne des médias et du numérique
title: "Microsoft, IBM: le logiciel libre s'impose chez les géants de l'informatique - La revue européenne des médias et du numérique"
date: 2019-03-28
href: https://la-rem.eu/2019/03/microsoft-ibm-le-logiciel-libre-simpose-chez-les-geants-de-linformatique
featured_image: https://la-rem.com/wp-content/uploads/2018/03/la-rem-tetiere-inseec-2.jpg
tags:
- Informatique en nuage
- Entreprise
series:
- 201913
---

> En s’emparant respectivement de GitHub et de RedHat, Microsoft et IBM ont révélé l’enjeu stratégique de l’open source. À chaque fois, c’est le succès du cloud hybride qui justifie l’intérêt pour les langages informatiques ouverts.
