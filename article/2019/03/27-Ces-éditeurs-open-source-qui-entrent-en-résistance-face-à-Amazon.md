---
site: Journal du Net
title: "Ces éditeurs open source qui entrent en résistance face à Amazon"
author: Xavier Biseul
date: 2019-03-27
href: https://www.journaldunet.com/solutions/cloud-computing/1422843-ces-editeurs-open-source-qui-entrent-en-resistance-face-a-amazon
featured_image: https://img-0.journaldunet.com/FIdU-YCpt2dW2NVBq5IlrbX3zrM=/540x/smart/1603bf4dbd964cfe99c5af953f8c0419/ccmcms-jdn/11092887.jpg
tags:
- Informatique en nuage
- Licenses
series:
- 201913
series_weight: 0
---

> Ces éditeurs open source qui entrent en résistance face à Amazon Confluent, Redis Labs et MongoDB ont modifié leur contrat de licence afin d'éviter que leur solution open source ne soit commercialisée par des fournisseurs tiers, comme AWS, sous la forme de services cloud managés.
