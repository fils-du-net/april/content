---
site: The Conversation
title: "Pourquoi les filles ont délaissé l'informatique"
author: Chantal Morley
date: 2019-03-10
href: https://theconversation.com/pourquoi-les-filles-ont-delaisse-linformatique-110940
featured_image: https://images.theconversation.com/files/262691/original/file-20190307-82665-xh9tzw.jpg
tags:
- Sensibilisation
series:
- 201910
series_weight: 0
---

> Quels sont les biais qui détournent les filles des métiers du numérique, alors que des mathématiciennes et des programmeuses ont largement contribué à l'invention de l'ordinateur moderne?
