---
site: Acteurs publics
title: "Après deux ans au gouvernement, Mounir Mahjoubi laisse un bilan controversé"
author: Emile Marzolf
href: https://www.acteurspublics.com/2019/03/28/apres-deux-ans-au-gouvernement-mounir-mahjoubi-laisse-un-bilan-controverse
featured_image: https://www.acteurspublics.com/img/uploaded/article/2019-03-5c9cef3a926c3.jpg
tags:
- Référentiel
- Institutions
- Promotion
series:
- 201913
---

> Bon communicant, le jeune ex-secrétaire d’État au Numérique, qui a démissionné ce jeudi 28 mars pour briguer la mairie de Paris, laisse derrière lui un bilan en demi-teinte, entre manque d’implication sur les grands sujets et critiques sur son comportement.
