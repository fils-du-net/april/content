---
site: Numerama
title: "Article 13: à l'ONU, les filtres à l'upload sont jugés excessifs pour combattre le piratage"
author: Julien Lausson
date: 2019-03-13
href: https://www.numerama.com/politique/471240-article-13-a-lonu-les-filtres-a-lupload-sont-juges-excessifs-pour-combattre-le-piratage.html
featured_image: https://www.numerama.com/content/uploads/2016/02/drapeau-onu.jpg
tags:
- Droit d'auteur
- Europe
- Institutions
series:
- 201911
---

> Le rapporteur spécial de l'ONU pour la liberté d'expression n'est pas ravi de l'article 13 de la future directive européenne sur le droit d'auteur.
