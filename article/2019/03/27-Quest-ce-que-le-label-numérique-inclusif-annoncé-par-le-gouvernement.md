---
site: Numerama
title: "Qu'est-ce que le label «numérique inclusif» annoncé par le gouvernement?"
author: Julien Lausson
date: 2019-03-27
href: https://www.numerama.com/politique/476224-quest-ce-que-le-label-numerique-inclusif-annonce-par-le-gouvernement.html
featured_image: https://www.numerama.com/content/uploads/2017/12/vieux-senior-personne-agee-ancien-senior.jpg
tags:
- Référentiel
- Institutions
- Promotion
series:
- 201913
---

> Au Journal officiel est paru un arrêté officialisant le label «numérique inclusif». Il vise à identifier toutes les initiatives destinées à aider les personnes éloignées du numérique.
