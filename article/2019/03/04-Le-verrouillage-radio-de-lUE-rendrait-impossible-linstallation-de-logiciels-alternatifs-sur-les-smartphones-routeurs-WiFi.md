---
site: Developpez.com
title: "Le verrouillage radio de l'UE rendrait impossible l'installation de logiciels alternatifs sur les smartphones, routeurs WiFi"
description: Et autres dispositifs
author: Stan Adkens
date: 2019-03-04
href: https://www.developpez.com/actu/249459/Le-verrouillage-radio-de-l-UE-rendrait-impossible-l-installation-de-logiciels-alternatifs-sur-les-smartphones-routeurs-WiFi-et-autres-dispositifs/
featured_image: https://www.developpez.net/forums/attachments/p454633d1/a/a/a
tags:
- Innovation
- Europe
- Institutions
series:
- 201910
series_weight: 0
---

> Les technologies d'appareils qui se connectent à des réseaux sans fil, mobiles ou au GPS explosent au fil des années et la propagation des ondes radio avec. Et les Etats prennent de plus en plus de dispositions pour réguler l'utilisation de ces ondes radio afin d'assurer un usage efficace du spectre radioélectrique qui garantit la santé, la sécurité et la compatibilité électromagnétique.
