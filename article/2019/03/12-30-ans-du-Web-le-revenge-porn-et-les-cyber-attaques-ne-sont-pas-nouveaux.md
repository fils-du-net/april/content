---
site: L'ADN
title: "30 ans du Web: le revenge porn et les cyber-attaques ne sont pas nouveaux"
author:  Marine Protais
date: 2019-03-12
href: https://www.ladn.eu/tech-a-suivre/meilleur-comme-pire-30-ans-web
featured_image: https://www.ladn.eu/wp-content/uploads/2019/03/GettyImages-538144021-2-1140x480.jpg
tags:
- Internet
- Partage du savoir
series:
- 201911
series_weight: 0
---

> Le Web fête ses trente ans. Son anniversaire fait resurgir l’idée que le Web utopique du départ aurait glissé vers une version cauchemardesque. Cette conviction doit être relativisée. L’historienne Valérie Schafer rappelle que les conduites criminelles existent depuis les débuts, tout comme les initiatives visant à en faire un espace où règne créativité et égalité.
