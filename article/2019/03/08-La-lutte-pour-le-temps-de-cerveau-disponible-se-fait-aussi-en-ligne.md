---
site: Le Vif.be
title: La lutte pour le "temps de cerveau disponible" se fait aussi en ligne
date: 2019-03-08
href: https://www.levif.be/actualite/sante/la-lutte-pour-le-temps-de-cerveau-disponible-se-fait-aussi-en-ligne/article-normal-1103091.html
featured_image: https://www.levif.be/medias/12930/6620323.jpg
tags:
- Internet
series:
- 201910
---

> Mais qu'est-ce que j'étais venu(e) faire sur cette page internet? En ligne, la capacité d'attention des utilisateurs est mise à rude épreuve, entre sollicitations intempestives et chausse-trappes. Certains développeurs et designers tentent d'apporter des solutions.
