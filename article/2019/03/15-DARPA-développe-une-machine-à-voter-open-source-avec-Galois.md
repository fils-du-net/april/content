---
site: Developpez.com
title: "DARPA développe une machine à voter open source avec Galois"
description: "Pour sécuriser les élections sans revenir aux bulletins de vote en papier"
author: Stéphane le calme
date: 2019-03-15
href: https://www.developpez.com/actu/251175/DARPA-developpe-une-machine-a-voter-open-source-avec-Galois-pour-securiser-les-elections-sans-revenir-aux-bulletins-de-vote-en-papier
featured_image: https://www.developpez.net/forums/attachments/p458153d1/a/a/a
tags:
- Vote électronique
- Innovation
series:
- 201911
---

> Depuis des années, des professionnels de la sécurité et des militants pour l'intégrité électorale ont poussé les vendeurs de machines à voter à mettre en place des systèmes électoraux plus sûrs et transparents, afin que les électeurs et les candidats puissent être assurés que les résultats des élections n'ont pas été manipulés.
