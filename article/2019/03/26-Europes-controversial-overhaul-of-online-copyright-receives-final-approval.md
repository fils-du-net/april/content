---
site: The Verge
title: "Europe's controversial overhaul of online copyright receives final approval"
author: James Vincent
date: 2019-03-26
href: https://www.theverge.com/2019/3/26/18280726/europe-copyright-directive
featured_image: https://cdn.vox-cdn.com/thumbor/p--Pd7ViKuU354tGmdFqWTgmgPk=/0x0:2040x1360/1520x1013/filters:focal(857x517:1183x843):format(webp)/cdn.vox-cdn.com/uploads/chorus_image/image/63301978/mdoying_180915_1777_0001.0.jpg
tags:
- Droit d'auteur
- Europe
- Institutions
- English
series:
- 201913
---

> The European Parliament has voted in favor of the final text of the Copyright Directive, controversial new legislation that redefines copyright in Europe for the internet age. Articles 11 and 13 — the ‘link tax' and ‘upload filter' — were both approved by European politicians
