---
site: Le Vent Se Lève
title: "La bataille des lobbies européens autour de la Directive Copyright"
date: 2019-03-23
href: https://lvsl.fr/la-bataille-des-lobbies-europeens-autour-de-la-directive-copyright
featured_image: https://lvsl.fr/wp-content/uploads/2019/03/Directive_Copyright-1068x710.png
seeAlso: "[#SaveYourInternet](https://saveyourinternet.eu)"
tags:
- Droit d'auteur
- Institutions
- Europe
- april
series:
- 201912
series_weight: 0
---

> La Directive Copyright sort de sa phase de négociation et aborde sa dernière ligne droite. Etat des lieux des actions de lobbying autour du texte depuis son entrée en négociation dans l'Union européenne en septembre 2018.
