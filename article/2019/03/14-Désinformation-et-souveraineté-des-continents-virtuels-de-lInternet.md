---
site: The Conversation
title: "Désinformation et souveraineté des continents virtuels de l'Internet"
author: Divina Frau-Meigs
date: 2019-03-14
href: https://theconversation.com/desinformation-et-souverainete-des-continents-virtuels-de-linternet-113523
featured_image: https://images.theconversation.com/files/263848/original/file-20190314-28512-1y81wh.jpg
tags:
- Internet
- Partage du savoir
series:
- 201911
---

> La géopolitique du net est ancrée dans des «continents virtuels» qui défendent des idéologies spécifiques.
