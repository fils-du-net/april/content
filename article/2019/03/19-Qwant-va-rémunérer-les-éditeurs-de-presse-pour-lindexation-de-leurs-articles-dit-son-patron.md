---
site: Les Echos
title: "«Qwant va rémunérer les éditeurs de presse pour l'indexation de leurs articles», dit son patron"
author: Fabienne Schmitt et Nicolas Madelaine
date: 2019-03-19
href: https://www.lesechos.fr/tech-medias/medias/0600930720982-qwant-va-remunerer-les-editeurs-de-presse-pour-lindexation-de-leurs-articles-dit-son-patron-2253671.php
featured_image: https://www.lesechos.fr/medias/2019/03/19/2253671_qwant-va-remunerer-les-editeurs-de-presse-pour-lindexation-de-leurs-articles-dit-son-patron-web-tete-060928687508.jpg
tags:
- Droit d'auteur
- Institutions
- Europe
series:
- 201912
---

> Dans une interview aux «Echos», Eric Léandri, le patron du challenger européen de Google, plaide en faveur de la directive européenne sur le droit d'auteur qui doit être votée la semaine prochaine car la presse et les autres éditeurs de contenus doivent être rémunérés. Sur ce point, Qwant montrera l'exemple. Il estime en revanche crucial que les acteurs du Web s'appuient sur une plate-forme publique et open source pour identifier les contenus à rémunérer au lieu de s'en remettre aux outils de Google et de Facebook.
