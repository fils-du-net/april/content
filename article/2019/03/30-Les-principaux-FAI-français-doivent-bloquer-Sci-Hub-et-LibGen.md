---
site: Next INpact
title: "Les principaux FAI français doivent bloquer Sci-Hub et LibGen"
author: Marc Rees
date: 2019-03-30
href: https://www.nextinpact.com/news/107689-les-principaux-fai-francais-doivent-bloquer-sci-hub-et-libgen.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/22757.jpg
tags:
- Internet
- Partage du savoir
series:
- 201913
---

> Paradis de la recherche scientifique ou de l'open access pour ses partisans, enfers de piratage pour les éditeurs du secteur… Sci-Hub et LibGen viennent finalement de faire l’objet d’une décision de blocage par le TGI de Paris à la demande d’Elsevier et Springer Nature. Bouygues, Free, Orange et SFR doivent empêcher l’accès à ces sites durant un an.
