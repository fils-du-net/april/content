---
site: usine-digitale.fr
title: 8 questions (et leurs réponses) que vous devriez vous poser sur la "taxe Gafa"
date: 2019-03-06
href: https://www.usine-digitale.fr/article/8-questions-que-vous-devriez-vous-poser-sur-la-nouvelle-taxe-sur-le-numerique.N814820
featured_image: https://www.usine-digitale.fr/mediatheque/7/9/2/000144297_homePageUne/papiers-administratifs.jpg
tags:
- Entreprise
- Internet
- Institutions
series:
- 201910
series_weight: 0
---

> Le gouvernement a présenté ce mercredi 6 mars 2019 au Conseil des ministres son projet de loi relatif à la taxation des grandes entreprises du numérique, familièrement appelée "taxe Gafa". Le ministre de l'Economie en a révélé les contours lors d'une conférence de presse en amont. Il a évoqué une fiscalité du 21e siècle, adaptée à l'économie du 21e siècle, où la data est une source de valeur centrale. Mais concrètement, ça donne quoi?
