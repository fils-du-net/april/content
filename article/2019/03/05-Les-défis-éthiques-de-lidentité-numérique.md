---
site: Educavox
title: "Les défis éthiques de l'identité numérique"
date: 2019-03-05
author: Armen Khatchatourov et Pierre-Antoine Chardel
href: https://www.educavox.fr/accueil/debats/les-defis-ethiques-de-l-identite-numerique
featured_image: https://www.educavox.fr/media/k2/items/cache/5e1b1619fa8ef5a86afc2897413e7d5d_L.jpg
tags:
- Vie privée
- Institutions
- Internet
series:
- 201910
series_weight: 0
---

> Si le RGPD est entré en application récemment, en plaçant l’Europe à l’avant-garde de la protection des données à caractère personnel, il ne doit pas nous dissuader de nous interroger en profondeur sur la question des identités, dont les contours se sont redéfinis à l’ère numérique. Il s’agit bel et bien de porter une réflexion critique sur des enjeux éthiques et philosophiques majeurs, au-delà de la seule question de la protection des informations personnelles et de la privacy.
