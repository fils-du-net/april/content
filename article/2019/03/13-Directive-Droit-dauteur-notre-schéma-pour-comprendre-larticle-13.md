---
site: Next INpact
title: "Directive Droit d'auteur: notre schéma pour comprendre l'article 13"
author: Marc Rees
date: 2019-03-13
href: https://www.nextinpact.com/news/107705-directive-droit-dauteur-notre-schema-pour-comprendre-larticle-13.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/22603.jpg
seeAlso: "[Un mois pour donner de la voix: voter pour l'article 13, c'est attaquer nos libertés](https://www.april.org/un-mois-pour-donner-de-la-voix-voter-pour-l-article-13-c-est-attaquer-nos-libertes)"
tags:
- Droit d'auteur
- Europe
- Institutions
series:
- 201911
series_weight: 0
---

> Alors que la proposition de directive sur le droit d’auteur sera soumise au vote du Parlement européen fin mars, David Kaye, rapporteur spécial des Nations Unies, s’inquiète des effets de l’article 13 sur la liberté d’expression. Les ayants droit contestent les risques de filtrage. Next INpact publie un schéma décrivant cette disposition phare.
