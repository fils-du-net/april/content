---
site: korii.
title: "Alexa va-t-elle devenir un membre de notre famille?"
author: Coralie Lemke
date: 2019-03-01
href: https://korii.slate.fr/tech/alexa-assistant-vocal-intelligence-artificielle-membre-famille
featured_image: https://korii.slate.fr/sites/default/files/styles/1440x600/public/000_17e9p1.jpg
tags:
- Vie privée
- Entreprise
series:
- 201909
series_weight: 0
---

> Nos assistants vocaux nous connaissent aussi bien (voire mieux) que les personnes qui composent notre foyer, où ils occupent une place bien particulière.
