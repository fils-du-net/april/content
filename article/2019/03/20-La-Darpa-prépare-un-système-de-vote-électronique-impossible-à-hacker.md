---
site: Usbek & Rica
title: "La Darpa prépare un système de vote électronique impossible à hacker"
author: Sophie Kloetzli
date: 2019-03-20
href: https://usbeketrica.com/article/darpa-vote-electronique-hacker
featured_image: https://static.usbeketrica.com/images/thumb_840xh/5c921229087e6.jpg
tags:
- Vote électronique
- Innovation
- Institutions
series:
- 201912
series_weight: 0
---

> L'agence de recherche de l'armée américaine veut mettre au point un système de vote électronique ultrasécurisé, notamment en vue de la présidentielle de 2020.
