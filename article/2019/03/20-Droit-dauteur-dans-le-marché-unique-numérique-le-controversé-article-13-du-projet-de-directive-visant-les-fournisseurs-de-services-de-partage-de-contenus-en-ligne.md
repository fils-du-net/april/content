---
site: Les Echos
title: "Droit d'auteur dans le marché unique numérique: le controversé article 13 du projet de directive visant les fournisseurs de services de partage de contenus en ligne"
author: Fabienne Panneau et Sophie Bencheikh-André
date: 2019-03-20
href: https://business.lesechos.fr/directions-juridiques/partenaire/partenaire-2121-droit-d-auteur-dans-le-marche-unique-numerique-le-controverse-article-13-du-projet-de-directive-visant-les-fournisseurs-de-services-de-partage-de-contenus-en-ligne-327970.php
featured_image: https://business.lesechos.fr/medias/2019/03/20/327970_droit-d-auteur-dans-le-marche-unique-numerique-le-controverse-article-13-du-projet-de-directive-visant-les-fournisseurs-de-services-de-partage-de-contenus-en-ligne-2121-1-part_1000x533.jpg
tags:
- Droit d'auteur
- Europe
- Institutions
series:
- 201912
---

> A l'issue d'intenses négociations et controverses, le Parlement, le Conseil et la Commission européenne, réunis dans un trilogue, sont parvenus à un accord le 13 février sur la Directive sur le droit d'auteur dans le marché unique numérique. Ce texte n'est encore qu'une simple proposition devant être définitivement adoptée par le Parlement, vraisemblablement avant la fin de l'actuelle mandature, dans le courant de la semaine du 25 mars 2019. Au coeur des débats entourant la directive, l'article 13 prévoit l'application d'un nouveau régime pour les fournisseurs de services de partage de contenus en ligne.
