---
site: The New York Times
title: "Elizabeth Warren Proposes Breaking Up Tech Giants Like Amazon and Facebook"
date: 2019-03-10
author: Astead W. Herndon
href: https://www.nytimes.com/2019/03/08/us/politics/elizabeth-warren-amazon.html
featured_image: https://static01.nyt.com/images/2019/03/08/us/08warren-update/08warren-update-superJumbo.jpg
tags:
- Entreprise
- English
series:
- 201910
---

> Senator Elizabeth Warren held a campaign rally in Long Island City, Queens, on Friday. She announced a plan aimed at breaking up some of America’s largest tech companies, including Amazon, Google, Apple and Facebook.
