---
site: Le Point
title: "Allemagne: manifestations contre la réforme du droit d'auteur"
date: 2019-03-23
href: https://www.lepoint.fr/monde/allemagne-manifestations-contre-la-reforme-du-droit-d-auteur-23-03-2019-2303429_24.php
featured_image: https://www.lepoint.fr/images/2019/03/23/18257001lpw-18257008-article-jpg_6080230.jpg
tags:
- Droit d'auteur
- Institutions
- Europe
series:
- 201912
---

> Des dizaines de milliers de manifestants ont défilé samedi partout en Allemagne pour 'sauver internet', exhortant les eurodéputés à voter contre la...
