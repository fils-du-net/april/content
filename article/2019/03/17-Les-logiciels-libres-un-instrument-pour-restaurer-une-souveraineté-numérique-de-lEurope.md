---
site: ZDNet France
title: "Les logiciels libres, un instrument pour restaurer une souveraineté numérique de l'Europe"
author: Thierry Noisette
date: 2019-03-17
href: https://www.zdnet.fr/blogs/l-esprit-libre/les-logiciels-libres-un-instrument-pour-restaurer-une-souverainete-numerique-de-l-europe-39882121.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2019/03/drapeaux_UE_Flags-Europe-Strasbourg-MaxPixel.jpg
tags:
- Entreprise
- Europe
series:
- 201911
---

> Un avis du Conseil économique, social et environnemental décline 17 propositions pour 'une politique de souveraineté européenne du numérique'. Il prône un recours accru aux logiciels libres.
