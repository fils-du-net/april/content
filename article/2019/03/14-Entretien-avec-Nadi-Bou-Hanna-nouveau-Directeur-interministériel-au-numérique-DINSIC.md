---
site: Next INpact
title: "Entretien avec Nadi Bou Hanna, nouveau Directeur interministériel au numérique (DINSIC)"
author: Xavier Berne
date: 2019-03-14
href: https://www.nextinpact.com/news/107707-entretien-avec-nadi-bou-hanna-nouveau-directeur-interministeriel-au-numerique-dinsic.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/5617.jpg
tags:
- Institutions
series:
- 201911
---

> Nadi Bou Hanna, qui a succédé en décembre dernier à Henri Verdier à la tête de la Direction interministérielle au numérique (DINSIC), a accepté de présenter à Next INpact ses priorités pour les mois à venir. L’occasion pour nous de l’interroger sur de nombreux sujets: logiciels libres, Open Data, carte d'identité numérique, etc.
