---
site: PhonAndroid
title: "L'UE veut nous empêcher d'installer des applications non certifiées sur nos smartphones"
author: Alexandre Schmid
date: 2019-03-05
href: https://www.phonandroid.com/lue-veut-nous-empecher-dinstaller-des-applications-non-certifiees-sur-nos-smartphones.html
featured_image: https://img.phonandroid.com/2019/03/lineageos.jpg
tags:
- Europe
- Institutions
- Innovation
series:
- 201910
---

> L'Union Européenne prévoit de limiter la possibilité d'installation sur les appareils radio (smartphones, routeurs...) de logiciels personnalisés.
