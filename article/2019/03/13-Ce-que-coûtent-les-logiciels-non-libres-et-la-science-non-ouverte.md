---
site: Acfas
title: "Ce que coûtent les logiciels «non libres» et la science «non ouverte»"
date: 2019-03-13
href: https://www.acfas.ca/publications/decouvrir/2019/03/ce-que-coutent-logiciels-non-libres-science-non-ouverte
featured_image: https://www.acfas.ca/sites/default/files/inline-images/j.-h.roy_acceuil_700x350.jpg
tags:
- Économie
- Logiciels privateurs
- Sciences
series:
- 201911
---

> Le libre ne pèse pas lourd dans les institutions d’enseignement supérieur du Québec. Au cours des dix dernières années, les cégeps et les universités de la province ont dépensé près de 270 millions de dollars pour des logiciels propriétaires (environ 190 millions $) et pour des abonnements à des publications scientifiques payantes (environ 80 millions $). En d’autres mots, de 2009 à 2018, l’accès à des logiciels produits par des entreprises comme Microsoft ou Oracle, ou encore à des ouvrages distribués par Elsevier, Taylor and Francis et d’autres grands éditeurs, a représenté environ 3,4% de l’ensemble des dépenses contractuelles des cégeps et des universités québécoises.
