---
site: les-infostrateges.com
title: "Le CESE prône une politique de souveraineté européenne du numérique"
author: Fabrice Molinaro
date: 2019-03-21
href: https://www.les-infostrateges.com/actu/le-cese-prone-une-politique-de-souverainete-europeenne-du-numerique
seeAlso: "[Le CESE préconise l'usage des logiciels libres pour une politique de souveraineté européenne](https://april.org/le-cese-preconise-l-usage-des-logiciels-libres-pour-une-politique-de-souverainete-europeenne)"
tags:
- Promotion
- Neutralité du Net
- Europe
- Institutions
series:
- 201912
series_weight: 0
---

> La révolution numérique, par le biais des smartphones, des réseaux sociaux ou encore de l’internet des objets en pleine croissance, fait profondément évoluer nos pratiques, s’immisçant aussi bien dans la sphère publique que dans l’intimité des personnes.
