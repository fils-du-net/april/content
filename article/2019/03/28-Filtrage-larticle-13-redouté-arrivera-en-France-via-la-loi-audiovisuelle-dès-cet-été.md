---
site: Numerama
title: "Filtrage: l'article 13 redouté arrivera en France via la loi audiovisuelle dès cet été"
author: Julien Lausson
date: 2019-03-28
href: https://www.numerama.com/politique/476521-filtrage-larticle-13-redoute-arrivera-en-france-via-la-loi-audiovisuelle-des-cet-ete.html
featured_image: https://www.numerama.com/content/uploads/2019/03/franck_riester_2019.jpg
tags:
- Droit d'auteur
- Europe
- Institutions
- HADOPI
series:
- 201913
---

> Le ministre de la Culture Franck Riester annonce que les dispositions contenues dans l'ex-article 13 de la directive européenne sur le droit d'auteur arriveront en France via la loi audiovisuelle. Celle-ci sera présentée cet été.
