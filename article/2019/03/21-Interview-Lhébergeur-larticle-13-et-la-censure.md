---
site: Next INpact
title: "[Interview] L'hébergeur, l'article 13 et la censure"
author: Marc Rees
date: 2019-03-21
href: https://www.nextinpact.com/news/107724-interview-lhebergeur-larticle-13-et-censure.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/6446.jpg
tags:
- Droit d'auteur
- Europe
- Institutions
series:
- 201912
---

> L’article 13 de la proposition de directive sur le droit d’auteur est-il la panacée ou bien un gouffre sans fonds où se noiera la liberté d’expression ? Me Ronan Hardouin, docteur en droit, auteur d’une thèse sur la responsabilité des hébergeurs, avocat au barreau de Paris au sein du Cabinet Ulys, a bien voulu répondre à nos questions en appui d’un article publié chez Lamy.
