---
site: the Guardian
title: "MEPs accidentally vote wrong way on copyright law"
author: Alex Hern
date: 2019-03-27
href: https://www.theguardian.com/media/2019/mar/27/mep-errors-mean-european-copyright-law-may-not-have-passed
featured_image: https://i.guim.co.uk/img/media/3a4f529101004361a2112524423d8e9cf1681563/0_197_4928_2958/master/4928.jpg?width=620&quality=45&auto=format&fit=max&dpr=2&s=913b1a487e5f3cc1fa4b0b55070a13d6
tags:
- Droit d'auteur
- Europe
- Institutions
- English
series:
- 201913
---

> Shortly after vote on amendments, 13 MEPs asked for vote to be recorded differently
