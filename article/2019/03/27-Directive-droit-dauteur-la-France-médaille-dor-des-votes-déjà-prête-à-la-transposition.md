---
site: Next INpact
title: "Directive droit d'auteur: la France, médaille d'or des votes, déjà prête à la transposition"
author: Marc Rees
date: 2019-03-27
href: https://www.nextinpact.com/news/107563-directive-droit-dauteur-france-medaille-dor-votes-deja-prete-a-transposition.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/5617.jpg
tags:
- Droit d'auteur
- Europe
- Institutions
series:
- 201913
---

> La directive sur le droit d’auteur a été adoptée hier au Parlement européen, réuni en séance plénière. Le texte est presque prêt à être transposé. La France, pays le plus partisan du texte, va profiter d’un véhicule législatif déjà adopté au Sénat pour introduire le droit voisin des éditeurs de presse au plus vite.
