---
site: Next INpact
title: "Franck Riester plaide (encore) pour des listes noires de sites contrefaisants"
date: 2019-03-06
href: https://www.nextinpact.com/brief/franck-riester-plaide--encore--pour-des-listes-noires-de-sites-contrefaisants-7992.htm
featured_image: https://cdn2.nextinpact.com/images/bd/wide-linked-media/21524.jpg
tags:
- Droit d'auteur
- HADOPI
series:
- 201910
---

> Questionné par le député Frédéric Reiss quant à l'opportunité d'un rapprochement entre la Hadopi et le CSA, le ministre de la Culture a rappelé que «chacune de ces autorités a une histoire propre et répond à un besoin caractérisé».
