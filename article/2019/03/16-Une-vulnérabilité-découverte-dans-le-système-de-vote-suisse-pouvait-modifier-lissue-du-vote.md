---
site: ZDNet France
title: "Une vulnérabilité découverte dans le système de vote suisse pouvait modifier l'issue du vote"
author: Catalin Cimpanu
date: 2019-03-16
href: https://www.zdnet.fr/actualites/une-vulnerabilite-decouverte-dans-le-systeme-de-vote-suisse-pouvait-modifier-l-issue-du-vote-39881969.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/03/switzerland-flag.jpg
tags:
- Vote électronique
- Logiciels privateurs
series:
- 201911
series_weight: 0
---

> Un correctif a été déployé dans le système de vote électronique de la Suisse, qui devrait être déployé plus tard cette année.
