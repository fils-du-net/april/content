---
site: The Verge
title: "European Wikipedias have been turned off for the day to protest dangerous copyright laws"
author: James Vincent
date: 2019-03-21
href: https://www.theverge.com/2019/3/21/18275462/eu-copyright-directive-protest-wikipedia-twitch-pornhub-final-vote
featured_image: https://cdn.vox-cdn.com/thumbor/TxE1lvlSg6f2kjYPwcGhi7I6oac=/0x0:1800x1000/1520x1013/filters:focal(756x356:1044x644):format(webp)/cdn.vox-cdn.com/uploads/chorus_image/image/63275078/wikipedia_dark.0.jpg
tags:
- Droit d'auteur
- Europe
- Institutions
- English
series:
- 201912
---

> Sites including Reddit, Twitch, and PornHub are also encouraging users in the EU to contact local politicians
