---
site: Techdirt.
title: "After Insisting That EU Copyright Directive Didn't Require Filters, France Immediately Starts Promoting Filters"
author: Mike Masnick
date: 2019-03-28
href: https://www.techdirt.com/articles/20190327/17141241885/after-insisting-that-eu-copyright-directive-didnt-require-filters-france-immediately-starts-promoting-filters.shtml
tags:
- Droit d'auteur
- Europe
- Institutions
- English
series:
- 201913
---

>  For months now we've all heard the refrain: Article 13 (now Article 17) of the EU Copyright Directive would not require filters. We all knew it was untrue. We pointed out many times that it was untrue, and that there was literally no way to comply unless you implemented filters (filters that wouldn't work and would ban legitimate speech), and were yelled at for pointing this out. Here's the MEP in charge of the Directive flat out insisting that it won't require filters last year: 
