---
site: L'Informaticien
title: "Mounir Mahjoubi quitte Bercy pour Paris"
author: Guillaume Périssat
href: https://www.linformaticien.com/actualites/id/51700/mounir-mahjoubi-quitte-bercy-pour-paris.aspx
featured_image: https://www.linformaticien.com/Portals/0/2019/03_mars/mahjoubi.jpg
tags:
- Référentiel
- Institutions
- Open Data
series:
- 201913
---

> Sans réelle surprise, le secrétaire d’Etat au Numérique a annoncé quitter le gouvernement. Un départ concomitant à celui de Benjamin Griveaux, le porte-parole du gouvernement, qui vise comme Mounir Mahjoubi la mairie de Paris. La députée LREM Paula Forteza est fortement pressentie pour prendre sa place.
