---
site: DW.COM
title: "Memes could be filtered out by EU copyright law"
author: James Jackson
date: 2019-03-12
href: https://www.dw.com/en/memes-could-be-filtered-out-by-eu-copyright-law/a-47858247
featured_image: https://www.dw.com/image/47470496_303.jpg
tags:
- Droit d'auteur
- Europe
- Institutions
- English
series:
- 201911
---

> Experts warn about EU law that could change the architecture of the internet, forcing websites to install flawed and expensive filters that would block satirical content like memes and lead to digital monopolization.
