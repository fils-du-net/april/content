---
site: Usbek & Rica
title: "«La small tech sera le poison de la Silicon Valley»"
author: Millie Servant
date: 2019-03-13
href: https://usbeketrica.com/article/small-tech-poison-silicon-valley
featured_image: https://static.usbeketrica.com/images/thumb_840xh/5c8920125fd4d.jpg
tags:
- Entreprise
series:
- 201911
---

> Nous sommes tous des cyborgs. C'est en tous cas le postulat d'Aral Balkan, que nous avons rencontré pour parler vie privée et capitalisme de surveillance.
