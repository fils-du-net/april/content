---
site: L'OBS
title: "Et si l'Europe défendait sa souveraineté numérique face aux Gafa?"
author: Thierry Noisette
date: 2019-03-15
href: https://www.nouvelobs.com/economie/20190314.OBS10750/pour-une-souverainete-europeenne-du-numerique-le-cese-invite-l-union-a-agir.html
tags:
- Entreprise
- Internet
- Europe
series:
- 201911
series_weight: 0
---

> Loin derrière les géants chinois et américains, l'Union européenne doit agir pour reprendre place dans la révolution numérique, plaide un avis du CESE.
