---
site: Nouvelle République
title: "De l'oxygène pour les 30 ans du web"
date: 2019-03-10
author: Agnès Aurousseau
href: https://www.lanouvellerepublique.fr/france-monde/de-l-oxygene-pour-les-30-ans-du-web
featured_image: https://images.lanouvellerepublique.fr/image/upload/t_1020w/5c829e8063824043418b456c.jpg
tags:
- Internet
series:
- 201910
series_weight: 0
---

> Créé dans un idéal d’échange libre et gratuit il y a trente ans, le web est désormais dominé par des géants marchands. Alors, quelle place pour la collaboration sur la Toile?
