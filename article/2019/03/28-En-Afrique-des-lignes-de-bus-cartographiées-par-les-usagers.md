---
site: Le Monde.fr
title: "En Afrique, des lignes de bus cartographiées par les usagers"
author: Claire Legros
date: 2019-03-28
href: https://www.lemonde.fr/economie/article/2019/03/28/en-afrique-des-lignes-de-bus-cartographiees-par-les-usagers_5442676_3234.html
featured_image: https://img.lemde.fr/2019/03/25/0/0/3482/5250/688/0/60/0/028ed83_ANpOcXjieuJNzA1i9wqbKKM5.jpg
tags:
- Partage du savoir
- Innovation
series:
- 201913
series_weight: 0
---

> A Accra (Ghana), Le Caire (Egypte) ou Nairobi (Kenya), des outils gratuits permettent aux citoyens de géolocaliser les lignes de transport qu'ils utilisent, et ainsi élaborer en commun les cartes qui font défaut.
