---
site: El Watan
title: "«L'ESS est une économie du réel et de plus équitable»"
date: 2019-03-11
href: https://www.elwatan.com/archives/supplement-economie/less-est-une-economie-du-reel-et-de-plus-equitable-11-03-2019
featured_image: https://www.elwatan.com/wp-content/uploads/2019/03/Sans-titre-1-43.gif
tags:
- Économie
- International
series:
- 201911
series_weight: 0
---

> Thierry Jeantet est le fondateur en 2004 et désormais ancien président du Forum international des dirigeants de l’économie sociale et solidaire / Les Rencontres du Mont-Blanc, devenues en 2017 ESS Forum international. Militant et acteur de l’économie sociale et solidaire (ESS), dans laquelle il voit une alternative au capitalisme, il est l’auteur de plusieurs ouvrages sur l’ESS et d’autres, à caractère politique. ll est depuis le 7 juin 2018, président de la Fondation Ag2r La Mondiale et, depuis sa création, administrateur de la Fondation Macif.
