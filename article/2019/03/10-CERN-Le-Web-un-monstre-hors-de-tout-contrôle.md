---
site: Le Matin
title: "CERN: Le Web, un «monstre hors de tout contrôle»?"
date: 2019-03-10
href: https://www.lematin.ch/high-tech/Le-Web-un-monstre-hors-de-tout-controle/story/10336042
featured_image: https://files.newsnetz.ch/story/1/0/3/10336042/3/topelement.jpg
tags:
- Internet
series:
- 201910
---

> Le Web va fêter ses 30 ans, son inventeur le physicien Tim Berners-Lee lance une campagne pour «sauver le Web» entre autres des fake news.
