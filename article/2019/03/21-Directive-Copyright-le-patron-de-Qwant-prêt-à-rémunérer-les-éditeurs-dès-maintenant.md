---
site: Clubic.com
title: "Directive Copyright: le patron de Qwant prêt à rémunérer les éditeurs dès maintenant"
author: Alexandre Boero
date: 2019-03-21
href: https://www.clubic.com/pro/legislation-loi-internet/actualite-852227-directive-copyright-patron-qwant-remunerer-editeurs.html
featured_image: https://pic.clubic.com/v1/images/1693564/raw?width=1200&fit=max&hash=ae80cea1c1f717b7ed50eafac85f714b3b5c85c0
tags:
- Droit d'auteur
- Institutions
- Europe
series:
- 201912
---

> Le patron du moteur de recherche respectueux de la vie privée, Éric Léandri, soutient l'Union européenne dans son entreprise de directive sur le droit d'auteur et prend d'ores et déjà de sérieux engagements.
