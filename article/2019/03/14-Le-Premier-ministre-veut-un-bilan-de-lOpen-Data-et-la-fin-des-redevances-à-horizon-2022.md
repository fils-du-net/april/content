---
site: Next INpact
title: "Le Premier ministre veut un bilan de l'Open Data et la fin des redevances à horizon 2022"
author: Xavier Berne
date: 2019-03-14
href: https://www.nextinpact.com/news/107708-le-premier-ministre-veut-bilan-lopen-data-et-fin-redevances-a-horizon-2022.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/19734.jpg
tags:
- Open Data
- Institutions
series:
- 201911
---

> En réponse à un référé de la Cour des comptes, Édouard Philippe souhaite qu’un «premier bilan de la mise en œuvre de l’ouverture des données [publiques] et de ses impacts» soit réalisé, dès cette année. Le Premier ministre a dans le même temps décidé de mettre définitivement fin aux redevances, notamment de l'IGN, à «horizon 2022».
