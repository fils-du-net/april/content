---
site: Copybuzz
title: "Article 13 must go: No desperate last-minute witchcraft can turn it into magic pixie dust"
href: https://copybuzz.com/copyright/article-13-must-go-no-desperate-last-minute-witchcraft-can-turn-it-into-magic-pixie-dust
featured_image: https://copybuzz.com/wp-content/uploads/2019/03/13.jpg
tags:
- Droit d'auteur
- Europe
- Institutions
- English
series:
- 201912
---

> After years of thrashing out the text of the proposed update to the EU Copyright Directive, we have come to what is almost certainly the final vote, in the European Parliament plenary early next week. You might think at this stage that it would be all over, with nothing new emerging, and most people simply accepting things as they are. Nothing could be further from the truth. The last few weeks have seen some of the most dramatic developments in the already fraught passage of the Directive through the legislative process.
