---
site: ITespresso
title: "«Numérique inclusif»: le gouvernement donne une prime au logiciel libre"
author: Clément Bohic
date: 2019-03-28
href: https://www.itespresso.fr/numerique-inclusif-logiciel-libre-204945.html
featured_image: https://www.itespresso.fr/wp-content/uploads/2019/03/numerique-inclusif-684x513.jpg
tags:
- Référentiel
- Institutions
- Promotion
series:
- 201913
series_weight: 0
---

> Utiliser «principalement des logiciels libres et ouverts» est l'un des critères qui conditionnent l'obtention du nouveau label «Numérique inclusif».
