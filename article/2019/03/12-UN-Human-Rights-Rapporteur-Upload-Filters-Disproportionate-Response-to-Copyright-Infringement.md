---
site: TorrentFreak
title: "UN Human Rights Rapporteur: Upload Filters 'Disproportionate Response' to Copyright Infringement"
author: Andy
date: 2019-03-12
href: https://torrentfreak.com/un-human-rights-rapporteur-upload-filters-disproportionate-response-to-copyright-infringement/
featured_image: https://torrentfreak.com/images/eu-copyright.jpg
tags:
- Droit d'auteur
- Europe
- Institutions
- English
series:
- 201911
---

> David Kaye, the UN's Special Rapporteur on freedom of opinion and expression, has raised the alarm over the EU's proposals for Article 13 and its de facto filtering requirements. 'Such sweeping pressure for pre-publication filtering is neither a necessary nor proportionate response to copyright infringement online,' Kaye warns.
