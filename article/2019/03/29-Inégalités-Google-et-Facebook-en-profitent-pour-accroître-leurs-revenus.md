---
site: L'ADN
title: "Inégalités: Google et Facebook en profitent pour accroître leurs revenus"
date: 2019-03-29
author: Marine Protais
href: https://www.ladn.eu/tech-a-suivre/inegalites-google-facebook-pommes-pourries-web
featured_image: https://www.ladn.eu/wp-content/uploads/2019/03/pomme-pourrie-gafa-1140x480.jpg
tags:
- Entreprise
- Internet
series:
- 201913
series_weight: 0
---

> Nanjira Sambuli est membre de la World wide web foundation. Son rôle est de promouvoir l’accès au Web pour tous. Elle estime que des politiques publiques incohérentes et la privatisation du web expliquent le ralentissement du rythme des connexions à internet ces dernières années. Rencontre.
