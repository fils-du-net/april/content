---
site: Libération
title: "Directive sur le droit d'auteur: une «victoire» pour qui?"
author: Amaelle Guiton
date: 2019-03-26
href: https://www.liberation.fr/planete/2019/03/26/directive-sur-le-droit-d-auteur-une-victoire-pour-qui_1717493
featured_image: https://medias.liberation.fr/photo/1206383--.jpg
seeAlso: "[Le Parlement européen valide la généralisation de la censure automatisée](https://www.april.org/le-parlement-europeen-valide-la-generalisation-de-la-censure-automatisee)"
tags:
- Droit d'auteur
- Europe
- Institutions
series:
- 201913
series_weight: 0
---

> Le texte adopté ce mardi par les eurodéputés avalise le filtrage algorithmique comme mode privilégié de gestion des contenus et signe une dépendance accrue aux grandes plateformes numériques.
