---
site: Forbes France
title: "Bitcoin, Un Projet Communiste?"
author: Lionel Meneghin
date: 2019-03-20
href: https://www.forbes.fr/technologie/bitcoin-un-projet-communiste
featured_image: https://www.forbes.fr/wp-content/uploads/2019/03/can-stock-photo-in8finity-bnb-740x370.jpg
tags:
- Économie
- Innovation
series:
- 201912
series_weight: 0
---

> Quoi de plus antagoniste a priori que les cryptomonnaies telles que le Bitcoin et le communisme? L’un évoque la finance et la spéculation, l’autre leur ennemi historique. Deux mondes étanches et antagonistes. En apparence seulement…
