---
site: Next INpact
title: "Éducation: des délégués académiques chargés du numérique au niveau local"
author: Xavier Berne
date: 2014-08-29
href: http://www.nextinpact.com/news/89522-education-delegues-academiques-charges-numerique-au-niveau-local.htm
tags:
- Institutions
- Éducation
---

> La loi pour la refondation de l’école de la République, censée faire «entrer l’école dans l’ère du numérique», continue de se décliner sur le terrain. En complément à la direction du numérique pour l’éducation, ce sont des «délégués académiques au numérique» qui sont désormais chargés de veiller à la mise en œuvre de la politique relative au numérique, à l’échelon local cette fois.
