---
site: Le Monde.fr
title: "Guerre des brevets: Microsoft porte plainte contre Samsung aux Etats-Unis"
date: 2014-08-01
href: http://www.lemonde.fr/economie/article/2014/08/01/guerre-des-brevets-microsoft-porte-plainte-contre-samsung-aux-etats-unis_4466009_3234.html
tags:
- Entreprise
- Économie
- Brevets logiciels
- Éducation
- International
---

> Microsoft a annoncé, vendredi 1er août, avoir porté plainte contre le fabricant sud-coréen d'appareils électroniques Samsung, qu'il accuse de ne plus respecter les conditions d'un accord sur des brevets. Dans un communiqué, l'avocat de Microsoft explique que «Microsoft a engagé une procédure légale contre Samsung» devant un tribunal new-yorkais «pour faire respecter notre contrat avec Samsung» passé en 2011.
