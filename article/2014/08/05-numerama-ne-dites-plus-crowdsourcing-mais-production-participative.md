---
site: Numerama
title: "Ne dites plus \"crowdsourcing\" mais \"production participative\""
author: Julien L.
date: 2014-08-05
href: http://www.numerama.com/magazine/30189-ne-dites-plus-crowdsourcing-mais-production-participative.html
tags:
- Administration
- Innovation
- Standards
---

> Vous voulez décrire votre activité au sein de Wikipédia ou dans le cadre du développement d'un logiciel libre? Ne dites plus "crowdsourcing" pour expliquer la manière dont ces projets fonctionnent, mais "production participative".
