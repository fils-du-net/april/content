---
site: Next INpact
title: "Un début de détente dans la guerre acharnée entre Apple et Samsung"
author: Vincent Hermann
date: 2014-08-06
href: http://www.nextinpact.com/news/89092-un-debut-detente-dans-guerre-acharnee-entre-apple-et-samsung.htm
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> Après des années de guerre acharnée, Apple et Samsung ont décidé de déposer partiellement les armes. Les deux entreprises se sont en effet mises d’accord sur l’abandon de l’ensemble des plaintes en cours ailleurs qu’aux États-Unis.
