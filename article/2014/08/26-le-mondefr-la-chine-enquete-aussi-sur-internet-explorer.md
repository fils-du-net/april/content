---
site: Le Monde.fr
title: "La Chine enquête aussi sur Internet Explorer"
date: 2014-08-26
href: http://www.lemonde.fr/asie-pacifique/article/2014/08/26/la-chine-enquete-aussi-sur-internet-explorer_4476881_3216.html
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Institutions
- International
---

> Alors que le régime souhaite lancer son propre système d'exploitation, en octobre, Pékin enquête sur la situation quasi monopolistique de Microsoft.
