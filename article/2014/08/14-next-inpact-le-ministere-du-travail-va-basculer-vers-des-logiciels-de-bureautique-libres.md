---
site: Next INpact
title: "Le ministère du Travail va basculer vers des logiciels de bureautique libres"
author: Xavier Berne
date: 2014-08-14
href: http://www.nextinpact.com/news/89239-le-ministere-travail-va-basculer-vers-logiciels-bureautique-libres.htm
tags:
- Administration
- Économie
- Institutions
- Sensibilisation
---

> Utilisant depuis 2009 des logiciels de bureautique et de messagerie propriétaires, le ministère du Travail se prépare à basculer vers des logiciels libres de type LibreOffice ou Thunderbird. Ce mouvement va cependant prendre du temps: quatre à six ans selon l’exécutif.
