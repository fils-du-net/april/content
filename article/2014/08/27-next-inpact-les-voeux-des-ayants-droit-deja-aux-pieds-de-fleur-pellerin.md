---
site: Next INpact
title: "Les vœux des ayants droit déjà aux pieds de Fleur Pellerin"
author: Marc Rees
date: 2014-08-27
href: http://www.nextinpact.com/news/89493-les-v%C5%93ux-ayants-droit-deja-aux-pieds-fleur-pellerin.htm
tags:
- Entreprise
- HADOPI
- Institutions
- Droit d'auteur
- Europe
---

> Après la Hadopi, la plupart des sociétés de gestion collective ont déjà félicité la nomination de Fleur Pellerin au ministère de la Culture. Ils se sont dans le même temps empressés de faire connaître leurs vœux à la nouvelle locataire de la Rue de Valois.
