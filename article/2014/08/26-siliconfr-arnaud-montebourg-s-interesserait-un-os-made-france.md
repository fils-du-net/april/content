---
site: Silicon.fr
title: "Arnaud Montebourg s’intéresserait à un OS made in France"
author: Jacques Cheminat
date: 2014-08-26
href: http://www.silicon.fr/arnaud-montebourg-sinteresserait-os-made-in-france-94632.html
tags:
- Entreprise
- Économie
- Institutions
---

> Arnaud Montebourg, connu pour son patriotisme économique, envisagerait la création d’un OS national pour contrer la dominance américaine.
