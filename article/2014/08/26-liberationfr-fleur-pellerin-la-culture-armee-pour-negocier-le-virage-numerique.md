---
site: Libération.fr
title: "Fleur Pellerin à la Culture, armée pour négocier le virage numérique"
date: 2014-08-26
href: http://www.liberation.fr/societe/2014/08/26/fleur-pellerin-a-la-culture-armee-pour-negocier-le-virage-numerique_1087573
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Licenses
---

> La nouvelle ministre de la Culture Fleur Pellerin, ex-ministre déléguée aux PME et à l’Economie numérique, est bien placée pour aider les acteurs de la culture et des médias à s’adapter au numérique, mais risque d’être vite absorbée par le dossier explosif des intermittents.
