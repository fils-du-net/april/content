---
site: Le Monde.fr
title: "Hadopi, surveillance, Netflix... le gouvernement Valls 2 est-il numérique?"
date: 2014-08-27
href: http://www.lemonde.fr/pixels/article/2014/08/27/valls-2-un-gouvernement-plus-digital-que-numerique_4477403_4408996.html
tags:
- Internet
- HADOPI
- Institutions
- Neutralité du Net
- Europe
- International
- Open Data
- Vie privée
---

> Emmanuel Macron, qui remplace poste pour poste Arnaud Montebourg à Bercy, est bien plus «digital» – le mot utilisé dans les milieux économiques – que numérique – le mot utilisé par la société civile. C’est lui, en collaboration avec le directeur de cabinet de Jean-Marc Ayrault, qui a par exemple décidé du recul du gouvernement sur les «pigeons», ces entrepreneurs du Web qui protestaient contre la taxation des plus-values de revente d’entreprises.
