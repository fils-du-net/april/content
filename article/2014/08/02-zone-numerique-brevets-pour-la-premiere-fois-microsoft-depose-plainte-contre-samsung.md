---
site: Zone Numerique
title: "Brevets: pour la première fois, Microsoft dépose plainte contre Samsung"
author: François Giraud      
date: 2014-08-02
href: http://www.zone-numerique.com/brevets-pour-la-premiere-fois-microsoft-depose-plainte-contre-samsung.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> Guerre des brevets: Microsoft et Samsung ont signé un accord sur divers brevets. Samsung ne respecterait pas cet accord. Microsoft vient de déposer plainte contre le géant sud-coréen ce vendredi devant un tribunal New-yorkais.
