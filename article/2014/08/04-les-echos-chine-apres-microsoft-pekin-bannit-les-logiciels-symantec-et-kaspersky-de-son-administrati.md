---
site: Les Echos
title: "Chine: après Microsoft, Pékin bannit les logiciels Symantec et Kaspersky de son administration"
author: Claude Fouquet
date: 2014-08-04
href: http://www.lesechos.fr/tech-medias/hightech/0203683643229-chine-apres-microsoft-pekin-bannit-symantec-et-kaspery-de-son-administration-1030239.php
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Institutions
- International
---

> Les deux logiciels de sécurité informatique ne pourront plus être utilisés sur les ordinateurs de l'administration chinoise. Seuls cinq logiciels chinois composent la liste des logiciels officiellement autorisés par les autorités de Pékin.
