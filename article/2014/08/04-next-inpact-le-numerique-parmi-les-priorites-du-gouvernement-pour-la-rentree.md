---
site: Next INpact
title: "Le numérique parmi les priorités du gouvernement pour la rentrée"
author: Xavier Berne
date: 2014-08-04
href: http://www.nextinpact.com/news/89028-le-numerique-parmi-priorites-gouvernement-pour-rentree.htm
tags:
- Institutions
- Éducation
---

> Alors que l’élaboration du grand projet de loi sur le numérique d’Axelle Lemaire peine à prendre son envol, Manuel Valls vient de confirmer que le gouvernement avait l’intention de lancer une «grande mobilisation collective» à propos du numérique - à l’évidence en référence à la consultation prochaine du Conseil national du numérique sur ce sujet. Le locataire de Matignon a insisté sur le fait que le numérique faisait partie des priorités de son équipe pour les mois à venir.
