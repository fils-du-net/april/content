---
site: ActuaLitté.com
title: "L'entrée du logiciel libre en bibliothèques se fait attendre"
author: Antoine Oury
date: 2014-08-19
href: https://www.actualitte.com/bibliotheques/l-entree-du-logiciel-libre-en-bibliotheques-se-fait-attendre-52046.htm
tags:
- Institutions
- Innovation
- Promotion
---

> Dans nos sociétés modernes, les bibliothèques sont perçues comme des lieux de savoir, plus soustrait aux impératifs commerciaux que tout autre lieu de nos sociétés. Et pourtant, les professionnels de l'information sont soumis aux impératifs et exigences de sociétés privées, créateurs des logiciels utilisés pour faire fonctionner la bibliothèque. Des alternatives, libres, existent.
