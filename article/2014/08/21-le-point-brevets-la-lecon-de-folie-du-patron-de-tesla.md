---
site: Le Point
title: "Brevets: la leçon de folie du patron de Tesla"
date: 2014-08-21
href: http://www.lepoint.fr/invites-du-point/idriss-j-aberkane/brevets-la-lecon-de-folie-du-patron-de-tesla-21-08-2014-1855413_2308.php
tags:
- Entreprise
- Économie
- Partage du savoir
- Innovation
- International
---

> "Tesla a fait quoi... !?" De Forbes à The Economist, en passant par Bloomberg, la presse économique n'en est pas revenue. Par un bref communiqué sur son site, Tesla a récemment annoncé qu'elle ne poursuivrait pas les entreprises qui utiliseraient ses brevets de bonne foi, en particulier ses concurrents!
