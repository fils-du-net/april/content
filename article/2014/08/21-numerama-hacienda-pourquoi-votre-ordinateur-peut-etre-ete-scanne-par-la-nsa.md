---
site: Numerama
title: "HACIENDA: pourquoi votre ordinateur a peut-être été scanné par la NSA"
author: Guillaume Champeau
date: 2014-08-21
href: http://www.numerama.com/magazine/30309-hacienda-pourquoi-votre-ordinateur-a-peut-etre-ete-scanne-par-la-nsa.html
tags:
- Internet
- Institutions
- Innovation
- Vie privée
---

> Appliquant à grande échelle les méthodes et outils de groupes de hackers criminels, la NSA et d'autres agences de renseignement alliées réalisent dans des dizaines de pays une cartographie complète des ordinateurs connectés à Internet, et de leurs vulnérabilités potentielles. Mais un remède a été mis au point.
