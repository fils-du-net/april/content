---
site: Developpez.com
title: "L'essor de l'open source, une menace pour les logiciels propriétaires?"
author: Francis Walter
date: 2014-08-15
href: http://www.developpez.com/actu/74136/L-essor-de-l-open-source-une-menace-pour-les-logiciels-proprietaires
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Sensibilisation
- International
---

> Ces derniers temps, beaucoup d'administrations optent pour les solutions open source. Elles ont compris qu'elles peuvent dépenser moins pour les technologies open source et avoir moins de difficulté pour la maintenance et les mises à jour, moins de risque d'espionnage et moins de menaces de cyberattaque. La fin du support de Windows XP par Microsoft serait l'une des sources de motivation des entreprises à pencher vers les solutions libres. Rappelons que la plupart des entreprises, jusqu'à l'abandon de l'OS, utilisaient Windows XP comme système d'exploitation.
