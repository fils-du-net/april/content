---
site: JDN
title: "Open source 2.0 ou l'ère de la maturité"
author: Marc Palazon
date: 2014-08-28
href: http://www.journaldunet.com/solutions/expert/58291/open-source-2-0-ou-l-ere-de-la-maturite.shtml
tags:
- Entreprise
- Économie
- Standards
---

> En cette année 2014, l’open source est en pleine effervescence et démontre encore une fois qu’il ne s’agit en rien d’un phénomène de mode mais bien d’une mutation profonde de l’écosystème mondial de l’informatique qui se joue.
