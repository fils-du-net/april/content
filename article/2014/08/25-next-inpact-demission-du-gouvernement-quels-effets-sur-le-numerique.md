---
site: Next INpact
title: "Démission du gouvernement: quels effets sur le numérique?"
author: Marc Rees
date: 2014-08-25
href: http://www.nextinpact.com/news/89416-demission-gouvernement-quels-effets-sur-numerique.htm
tags:
- Internet
- Institutions
---

> Manuel Valls vient de présenter la démission de son gouvernement, suite aux attaques portées par le ministre de l’Économie, Arnaud Montebourg, contre la politique de l’exécutif. Ce remaniement pourrait entraîner de lourdes conséquences sur le numérique.
