---
site: 01net.
title: "Black Hat 2014: peut-on encore sauver le cyberespace de ses innombrables failles?"
author: Gilbert Kallenborn
date: 2014-08-08
href: http://www.01net.com/editorial/624902/black-hat-2014-peut-on-encore-sauver-le-cyberespace-de-ses-innombrables-failles
tags:
- Internet
- Logiciels privateurs
- Économie
- Sensibilisation
---

> Face à la croissance rapide des vulnérabilités, il faut trouver de nouvelles solutions. Parmi les idées nouvelles qui émergent: le rachat et la publication de toutes les failles ou encore la dépossession des éditeurs de leurs codes source.
