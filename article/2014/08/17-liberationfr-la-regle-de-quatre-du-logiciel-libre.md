---
site: Libération.fr
title: "La règle de quatre du logiciel libre"
author: Erwan Cario
date: 2014-08-17
href: http://www.liberation.fr/culture/2014/08/17/la-regle-de-quatre-du-logiciel-libre_1082166
tags:
- Licenses
- Philosophie GNU
---

> Une des particularités de la licence publique générale GNU (autrement appelée par son acronyme anglophone GNU GPL), élaborée par Richard Stallman et la Free Software Foundation (FSF), est de s’appuyer sur les lois contraignantes du copyright pour établir les règles assurant le respect des principes du logiciel libre.
