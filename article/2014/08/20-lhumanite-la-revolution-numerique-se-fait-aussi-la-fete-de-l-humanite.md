---
site: L'Humanité
title: "La révolution numérique se fait aussi à la Fête de l‘Humanité"
date: 2014-08-20
href: http://www.humanite.fr/la-revolution-numerique-se-fait-aussi-la-fete-de-lhumanite-549497
tags:
- April
- Partage du savoir
- Associations
- Promotion
---

> La Fête de l’Humanité va accueillir cette année un espace dédié au monde du Libre, aux Hackers et au Fablabs. Au programme, des débats, des démonstrations, et une conférence exceptionnelle de Richard Stallman, le père du logiciel libre.
