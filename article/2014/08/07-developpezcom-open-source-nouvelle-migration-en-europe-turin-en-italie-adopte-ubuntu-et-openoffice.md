---
site: Developpez.com
title: "Open source: nouvelle migration en Europe, Turin en Italie adopte Ubuntu et OpenOffice"
author: imikado
date: 2014-08-07
href: http://www.developpez.com/actu/73972/Open-source-nouvelle-migration-en-Europe-Turin-en-Italie-adopte-Ubuntu-et-OpenOffice
tags:
- Logiciels privateurs
- Administration
- Économie
- International
---

> Encore une victoire pour l'Open source avec cette nouvelle d'Italie: Turin a décidé de faire un pas vers l’open source en abandonnant l’utilisation des logiciels propriétaires. La localité se donne 1 an et demi pour migrer 8 300 postes sous Ubuntu, avec l’adoption d’Open Office ou LibreOffice comme suite bureautique en lieu et place de Microsoft Office.
