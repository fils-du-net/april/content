---
site: Le Courrier
title: "Une alternative au droit d’auteur"
author: Laura drompt
date: 2014-08-28
href: http://www.lecourrier.ch/123348/une_alternative_au_droit_d_auteur
tags:
- Internet
- Partage du savoir
- Institutions
- Sensibilisation
- Associations
- Droit d'auteur
- Contenus libres
---

> Les licences Creative Commons encouragent le partage des contenus culturels, dans l’idée d’un Internet participatif. Un changement de paradigme.
