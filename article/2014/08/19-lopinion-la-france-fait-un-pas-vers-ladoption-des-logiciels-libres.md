---
site: L'Opinion
title: "La France fait un pas vers l'adoption des logiciels libres"
author: Hugo Sedouramane
date: 2014-08-19
href: http://www.lopinion.fr/19-aout-2014/france-fait-pas-vers-l-adoption-logiciels-libres-15428
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Interopérabilité
- April
- Institutions
---

> Des solutions permettraient à long terme de diviser par cinq le coût de certains postes de dépenses en technologies de l'information et de la communication
