---
site: Numerama
title: "Windows XP devrait passer open source, suggère un expert informatique"
author: Julien L.
date: 2014-08-11
href: http://www.numerama.com/magazine/30232-windows-xp-devrait-passer-open-source-suggere-un-expert-informatique.html
tags:
- Entreprise
- Licenses
---

> Un expert informatique a suggéré lors du Black Hat 2014 que Windows XP devrait devenir open source, puisque Microsoft a renoncé à poursuivre le support de son système d'exploitation. Il propose même que cette règle soit appliquée à l'ensemble des logiciels dont le code source est fermé, ce qui serait positif du point de vue de la sécurité.
