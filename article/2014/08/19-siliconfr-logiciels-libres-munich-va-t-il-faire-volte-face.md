---
site: Silicon.fr
title: "Logiciels libres: Munich va-t-il faire volte-face?"
author: David Feugey
date: 2014-08-19
href: http://www.silicon.fr/logiciels-libres-munich-va-t-il-faire-volte-face-96174.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- International
---

> La nouvelle municipalité de la ville de Munich envisagerait de rebasculer ses postes de travail de LiMux et LibreOffice vers Windows et Microsoft Office.
