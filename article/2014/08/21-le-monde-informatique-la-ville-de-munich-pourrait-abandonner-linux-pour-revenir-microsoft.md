---
site: Le Monde Informatique
title: "La ville de Munich pourrait abandonner Linux pour revenir à Microsoft"
author: Jean Elyan
date: 2014-08-21
href: http://www.lemondeinformatique.fr/actualites/lire-la-ville-de-munich-pourrait-abandonner-linux-pour-revenir-a-microsoft-58369.html
tags:
- Logiciels privateurs
- Administration
- International
---

> Après la migration vers Linux très médiatisée de la ville de Munich, les utilisateurs auraient trop de mal à s'adapter à la distribution installée de l'OS Open Source, selon le nouveau maire adjoint, Josef Schmid. Celui-ci envisage un retour possible à Microsoft, même s'il considère que le projet conduit en 2013 a été un succès.
