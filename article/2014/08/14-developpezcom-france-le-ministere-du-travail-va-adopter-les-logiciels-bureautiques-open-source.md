---
site: Developpez.com
title: "France: Le ministère du travail va adopter les logiciels bureautiques open source"
author: Francis Waller
date: 2014-08-14
href: http://open-source.developpez.com/actu/74132/France-Le-ministere-du-travail-va-adopter-les-logiciels-bureautiques-open-source-il-a-opte-pour-les-technologies-open-source-pour-ses-applications
tags:
- Logiciels privateurs
- Administration
- Économie
- Sensibilisation
---

> Dernièrement, il y a eu un vent de migration de plusieurs organismes publics vers les solutions open source. Il y a quelques années, il s'agissait de la gendarmerie nationale qui avait pris l'engagement de migrer tout son parc informatique vers Ubuntu, l'une des distributions Linux les plus populaires au monde, ensuite la ville de Valence (Espagne) sans oublier la ville de Munich (Allemagne).
