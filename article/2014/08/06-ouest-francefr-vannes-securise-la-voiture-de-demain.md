---
site: "ouest-france.fr"
title: "À Vannes, on sécurise la voiture de demain"
author: Patrick Croguennec
date: 2014-08-06
href: http://www.ouest-france.fr/vannes-securise-la-voiture-de-demain-2747973
tags:
- Entreprise
- Innovation
- International
---

> Assistance à la navigation, à la gestion automatique du freinage... Les véhicules ont toujours plus d'informatique. Qu'il faut sécuriser.
