---
site: Bulletins Electroniques
title: "Brevetabilité des logiciels et des \"business methods\" aux Etats-Unis"
date: 2014-08-29
href: http://www.bulletins-electroniques.com/actualites/76626.htm
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Innovation
- Sciences
- International
---

> Une décision de la Cour Suprême va faire évoluer la pratique américaine concernant les brevets sur les logicels, les méthodes, et forcer à un réexamen de leur brevetabilité. Une évolution qui va créer de l'incertitude à court terme, mais qui peut être bienvenue.
