---
site: Mediapart
title: "Logiciel Libre - juste une question humaine"
author: Frédéric Couchet
date: 2014-08-26
href: http://blogs.mediapart.fr/blog/frederic-couchet/260814/logiciel-libre-juste-une-question-humaine
tags:
- Internet
- April
- Innovation
- Promotion
---

> Pourquoi existe-il des personnes qui font des logiciels libres? Eben Moglen, l’une des figures les plus importantes dans l’univers du logiciel libre et de la culture numérique, propose une réponse simple et sans doute très juste
