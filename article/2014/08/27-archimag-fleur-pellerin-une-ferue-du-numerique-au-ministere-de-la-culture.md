---
site: Archimag
title: "Fleur Pellerin, une férue du numérique au ministère de la Culture"
author: Bruno Texier
date: 2014-08-27
href: http://www.archimag.com/vie-numerique/2014/08/27/fleur-pellerin-f%C3%A9rue-num%C3%A9rique-minist%C3%A8re-culture
tags:
- Institutions
---

> Avec Fleur Pellerin et Axelle Lemaire, le nouveau gouvernement joue la carte du numérique dans les domaines de la culture et du développement des territoires.
