---
site: KultureGeek
title: "Le gouvernement chinois pose son véto aux logiciels de sécurité étrangers"
author: Frederic L.
date: 2014-08-04
href: http://kulturegeek.fr/news-35767/gouvernement-chinois-pose-veto-logiciels-securite-etrangers
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- International
---

> L’affaire Snowden n’en finit plus de faire des vagues dont les remous agitent encore les plus hautes sphères. Depuis les révélations du lanceur d’alerte, et après quelques affaires croisées d’espionnage pas vraiment «saines», les rapports entre la Chine et les Etats-Unis se sont considérablement détériorés, au point que le gouvernement chinois envisage de totalement se passer des logiciels américains, y compris bien sûr lorsque ceux-ci concernent les questions de sécurité.
