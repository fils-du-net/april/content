---
site: Silicon.fr
title: "Skype lâche les utilisateurs de Mac PowerPC"
author: David Feugey
date: 2014-08-07
href: http://www.silicon.fr/lobsolescence-programmee-frappe-encore-skype-abandonne-les-utilisateurs-mac-powerpc-96025.html
tags:
- Entreprise
- Logiciels privateurs
- Sensibilisation
---

> Toutefois, nous pourrions nous attendre à mieux de la part d’un éditeur de logiciels propriétaires. En particulier quand ce dernier n’a pas manqué de souligner mainte et mainte fois son professionnalisme face à la communauté des logiciels libres. Une communauté qui arrive à faire ce que la firme de Redmond ne peut réaliser: maintenir une continuité dans le monde du logiciel.
