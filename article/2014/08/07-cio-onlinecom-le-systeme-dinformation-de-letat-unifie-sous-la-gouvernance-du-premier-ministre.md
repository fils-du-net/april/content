---
site: "cio-online.com"
title: "Le système d'information de l'Etat unifié sous la gouvernance du Premier Ministre"
author: Bertrand Lemaire
date: 2014-08-07
href: http://www.cio-online.com/actualites/lire-le-systeme-d-information-de-l-etat-unifie-sous-la-gouvernance-du-premier-ministre-7005.html
tags:
- Administration
- Institutions
---

> Malgré des dispositions transitoires prises dans un arrêté, le décret récemment paru implique désormais une gouvernance ferme pour l'ensemble de l'informatique d'Etat.
