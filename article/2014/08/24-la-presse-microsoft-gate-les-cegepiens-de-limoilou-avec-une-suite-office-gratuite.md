---
site: La Presse
title: "Microsoft gâte les cégépiens de Limoilou avec une suite Office gratuite"
date: 2014-08-24
href: http://www.lapresse.ca/le-soleil/actualites/education/201408/23/01-4794130-microsoft-gate-les-cegepiens-de-limoilou-avec-une-suite-office-gratuite.php
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Associations
- Éducation
- International
---

> Ardent militant pour l'implantation du logiciel libre, le professeur Daniel Pascot trouve «scandaleux» que des cégeps relaient la promotion de Microsoft auprès de leurs étudiants. M. Pascot, qui est professeur au Département des systèmes d'information organisationnels de l'Université Laval, ne s'étonne pas de la stratégie adoptée par le géant de l'informatique: «C'est un peu la stratégie du dealer de drogues. On vous donne le produit gratuitement pour que vous ne puissiez plus vous en passer et, par la suite, il devient payant.»
