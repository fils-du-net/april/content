---
site: Next INpact
title: "687 000 euros de logiciels libres pour le ministère de l’Agriculture en 2013"
author: Xavier Berne
date: 2014-08-07
href: http://www.nextinpact.com/news/89108-687-000-euros-logiciels-libres-pour-ministere-l-agriculture-en-2013.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- Sensibilisation
---

> Contrairement à l’idée reçue, logiciel libre n’est pas forcément synonyme de gratuité. Le ministère de l’Agriculture a par exemple dépensé 687 000 euros l’année dernière pour des programmes non privateurs. Pour autant, il a davantage ouvert son portefeuille pour des solutions propriétaires. Explications.
