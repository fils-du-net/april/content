---
site: Silicon.fr
title: "Desktop: Linus Torvalds espère toujours que Linux supplantera Windows"
author: David Feugey
date: 2014-08-21
href: http://www.silicon.fr/linus-torvalds-linux-supplantera-windows-96210.html
tags:
- Promotion
---

> «Je pense que nous y arriverons un jour», explique Linus Torvalds au sujet de Linux dans le monde desktop. Il lui faudra toutefois vaincre les réticences des constructeurs et des utilisateurs, accrochés à Windows ou OS X.
