---
site: Libération.fr
title: "Logiciels à l’ère libre"
author: Erwan Cario
date: 2014-08-17
href: http://www.liberation.fr/societe/2014/08/17/logiciels-a-l-ere-libre_1082175
tags:
- Internet
- Sensibilisation
- Associations
- Licenses
- Philosophie GNU
---

> Depuis les années 80, un système collaboratif permet à tous d’étudier, de modifier et de distribuer des programmes, parallèlement aux géants de l’informatique, dont les outils briment l’utilisateur.
