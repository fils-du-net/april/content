---
site: EconomieMatin
title: "Non, Tesla ne renonce (définitivement) pas à ses brevets!"
author: Julien Pillot
date: 2014-08-30
href: http://www.economiematin.fr/news-tesla-brevets-guerre-opensource-developpement
tags:
- Entreprise
- Économie
- Institutions
- Innovation
---

> En matière de protection de la propriété intellectuelle, il me semble plus que temps de replacer l'église au centre du village et de combattre l'idée malheureusement de plus en plus répandue selon laquelle les brevets constitueraient, en fait, une entrave à l'innovation. Cette idée saugrenue trouve aujourd'hui un écho particulier dans quelques récents cas d'utilisation abusive desdits brevets (patent trolls, guerre des brevets, etc.) que la presse spécialisée s'est bien évidemment empressée de relayer du fait de leur caractère, il est vrai, souvent spectaculaire.
