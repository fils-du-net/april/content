---
site: Next INpact
title: "La Hadopi va migrer vers la suite bureautique LibreOffice"
author: Xavier Berne
date: 2014-08-25
href: http://www.nextinpact.com/news/89421-la-hadopi-va-migrer-vers-suite-bureautique-libreoffice.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- HADOPI
---

> À l’image de plus en plus d’administrations, la Hadopi va claquer la porte à la suite bureautique de Microsoft, Office, au profit des logiciels libres (et gratuits) de la solution LibreOffice. Cette migration devrait s’effectuer progressivement au cours des prochains mois. Un plan d’accompagnement du personnel a en ce sens été élaboré par l'institution.
