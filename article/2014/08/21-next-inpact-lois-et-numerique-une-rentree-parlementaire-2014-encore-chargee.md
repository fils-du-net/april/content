---
site: Next INpact
title: "Lois et numérique: une rentrée parlementaire 2014 encore chargée"
author: Marc Rees
date: 2014-08-21
href: http://www.nextinpact.com/news/89346-lois-et-numerique-rentree-parlementaire-2014-chargee.htm
tags:
- Internet
- Économie
- HADOPI
- Institutions
- Open Data
---

> La rentrée parlementaire concernant les textes liés aux nouvelles technologies sera aussi riche cette année qu’en 2013. Tour d’horizon des textes bientôt débattus dans l’hémicycle et ceux dont le sort reste encore bien vague.
