---
site: Developpez.com
title: "Pour quelles raisons les entreprises devraient-elles opter pour des solutions libres?"
author: Francis Walter$authornbsp;$authornbsp;$authornbsp;
date: 2014-08-22
href: http://blog.developpez.com/francis-walter/open-source/pourquoi-entreprises-devraient-opter-solutions-libres
tags:
- Entreprise
- Administration
- Économie
- Sensibilisation
- Informatique en nuage
- Europe
---

> Dernièrement, l’actualité IT a été submergée de sujets relatifs aux logiciels libres et propriétaires (actualités que vous pouvez retrouvez sur Developpez.com). En effet, la part de marché des logiciels propriétaires dans les administrations seraient en régression, aussi lente qu’elle puisse être. Les administrations publiques ne cessent de migrer vers des logiciels libres ou «open source» au détriment de ceux propriétaires ou «closed» comme les développeurs aiment les appeler. Cela a suscité de sérieux débats entre les pro-libres et les pro-propriétaires.
