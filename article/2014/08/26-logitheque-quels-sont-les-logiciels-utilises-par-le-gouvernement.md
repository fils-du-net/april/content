---
site: Logitheque
title: "Quels sont les logiciels utilisés par le gouvernement?"
date: 2014-08-26
href: http://www.logitheque.com/articles/quels_sont_les_logiciels_utilises_par_le_gouvernement_704.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- Interopérabilité
- Marchés publics
- RGI
---

> A l'heure où le nouveau gouvernement se forme et que les aspirants (et réfractaires) guettent leur téléphone portable toutes les 15 secondes, revenons sur une question soulevée lors du début de mandat de Jean-Marc Ayraut, celle du budget du gouvernement alloué aux logiciels. Mais de quels logiciels s'agit-il exactement? Faites comme Manuel Valls, Najat Vallaud-Belkacem ou Ségolène Royal et utilisez les logiciels plébiscités par les différents ministères!
