---
site: Village de la Justice
title: "Brevet logiciel: USA = Europe?"
author: Laëtitia Le Metayer
date: 2014-08-12
href: http://www.village-justice.com/articles/Brevet-logiciel-USA-Europe,17536.html
tags:
- Institutions
- Brevets logiciels
---

> Le sujet de la brevetabilité du logiciel n’est pas appréhendé de la même façon en Europe et aux États-Unis.
