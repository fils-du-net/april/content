---
site: Libération.fr
title: "Les cryptoparties, sans laisser d’adresse"
author: Emmanuelle Chaze
date: 2014-08-31
href: http://ecrans.liberation.fr/ecrans/2014/08/31/les-cryptoparties-sans-laisser-d-adresse_1090893
tags:
- Internet
- Sensibilisation
- Vie privée
---

> A Berlin comme à Paris, des rencontrres entre citoyens sont organisées dans le but d’enseigner aux internautes à protéger leurs données sur la Toile.
