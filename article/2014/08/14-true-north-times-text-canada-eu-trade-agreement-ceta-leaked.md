---
site: The True North Times
title: "Text of Canada-EU Trade Agreement (CETA) Leaked"
author: Maxwell Stockton
date: 2014-08-14
href: http://www.truenorthtimes.ca/2014/08/14/text-canada-eu-trade-agreement-ceta-leaked/
tags:
- Économie
- Institutions
- Europe
- International
- ACTA
- English
---

> (Deux semaines après que l'Allemagne ait laissé entendre son rejet de dispositions au coeur de l'Accord Commercial Canada-Union européenne, ses défenseurs ont probablement pensé que le terrain sur lequel ils avançaient devenait de plus en plus incertain. Hier, il s'est dérobé de sous leurs pieds) Two weeks after Germany hinted at rejecting core provisions of the Canada-EU Trade Agreement (CETA), trade advocates probably thought that the ground they were breaking was shifting uneasily. Yesterday, it fell out from under them.
