---
site: Next INpact
title: "Le Royaume-Uni impose des standards ouverts à ses administrations"
author: Xavier Berne
date: 2014-07-25
href: http://www.nextinpact.com/news/88863-le-royaume-uni-impose-standards-ouverts-a-ses-administrations.htm
tags:
- Administration
- Économie
- Interopérabilité
- April
- Institutions
- Standards
- International
---

> Alors que la publication des déclarations d’intérêts des parlementaires a donné lieu hier à de nombreuses critiques, étant donné que les plus de 900 fichiers ainsi mis en ligne se sont avérés être des versions scannées de formulaires remplis au stylo, le gouvernement britannique semble avoir un train d’avance sur les pouvoirs publics français. Mardi, les services du 10 Downing Street ont en effet annoncé que deux types de standards ouverts seraient désormais imposés à toutes les administrations du pays dès lors qu’il serait question de documents publics.
