---
site: ZDNet
title: "Alliage, solution aquitaine en logiciels libres pour personnes âgées"
author: Thierry Noisette
date: 2014-07-23
href: http://www.zdnet.fr/actualites/alliage-solution-aquitaine-en-logiciels-libres-pour-personnes-agees-39804191.htm
tags:
- Internet
- Innovation
- Licenses
---

> Portée par le pôle Aquinetic, la solution Alliage a été développée et expérimentée pendant deux ans. Objectif: améliorer la qualité de vie à domicile des personnes âgées. Une cinquantaine de seniors l'ont testée pendant plusieurs mois.
