---
site: CitizenKane
title: "Sécurité informatique: 4 bombes atomiques en 12 mois"
author: David Nataf
date: 2014-07-10
href: http://citizenkane.fr/securite-informatique-4-bombes-atomiques-en-12-mois.html
tags:
- Internet
- Vie privée
---

> Nos systèmes de sécurité informatique sont bien plus nombreux et perfectionnés qu’il y a quelques années. Les normes de sécurité et leur intégration dans les entreprises sont de mieux en mieux implémentés.
