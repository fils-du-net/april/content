---
site: Le Monde.fr
title: "TiSA: un accord géant de libre-échange en discrètes négociations"
author: Maxime Vaudano
date: 2014-07-09
href: http://www.lemonde.fr/les-decodeurs/article/2014/07/09/tisa-quand-le-liberalisme-revient-par-la-porte-de-derriere_4452691_4355770.html
tags:
- Économie
- Institutions
- Sciences
- Europe
- International
---

> Réunis à Paris mardi 8 juillet pour préparer le prochain volet de la régulation financière européenne, les représentants du monde financier ne semblent pas être particulièrement au fait des négociations secrètes qui pourraient demain bloquer toute volonté régulatoire de 50 des plus grandes économies mondiales. Une perspective nommée TiSA, pour Trade in Services Agreement, ou Accord sur le commerce des services en français.
