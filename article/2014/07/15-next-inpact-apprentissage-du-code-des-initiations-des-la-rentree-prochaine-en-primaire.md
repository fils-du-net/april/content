---
site: Next INpact
title: "Apprentissage du code: des initiations dès la rentrée prochaine en primaire"
author: Xavier Berne
date: 2014-07-15
href: http://www.nextinpact.com/news/88689-apprentissage-code-initiations-des-rentree-prochaine-en-primaire.htm
tags:
- Institutions
- Associations
- Éducation
---

> Comme l’avait laissé entendre le ministre de l’Éducation nationale il y a une dizaine de jours à l’Assemblée nationale, une initiation au code informatique pourra être proposée aux écoliers français dès la rentrée prochaine. Cela se fera toutefois de manière facultative, et sur le temps périscolaire (c’est-à-dire en dehors des cours). C’est en tout cas ce qu’a confirmé Benoît Hamon ce week-end, tout en laissant entendre qu’un tel éveil pourrait en revanche devenir obligatoire au collège.
