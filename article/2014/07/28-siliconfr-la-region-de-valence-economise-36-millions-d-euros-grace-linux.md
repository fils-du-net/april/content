---
site: Silicon.fr
title: "La région de Valence a économisé 36 millions d’euros, grâce à Linux"
author: David Feugey
date: 2014-07-28
href: http://www.silicon.fr/region-valence-economise-36-millions-deuros-grace-linux-95837.html
tags:
- Administration
- Économie
- International
---

> En basculant les PC de ses écoles sous Linux, la région de Valence a économisé 4 millions d’euros par an. Il faut également ajouter un gain de 1,5 million d’euros par an via la bascule sous LibreOffice.
