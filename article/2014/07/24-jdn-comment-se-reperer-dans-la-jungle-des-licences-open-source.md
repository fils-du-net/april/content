---
site: JDN
title: "Comment se repérer dans la jungle des licences open source"
author: Alain Clapaud
date: 2014-07-24
href: http://www.journaldunet.com/solutions/dsi/comparatif-des-licences-open-source.shtml
tags:
- April
- Droit d'auteur
- Licenses
---

> N'est pas logiciel libre ou open source qui veut. Pour éviter le droit d'auteur classique, il faut choisir une licence. Pas si simple: il en existe pléthore avec des nuances subtiles.
