---
site: Le Courrier picard
title: "Beauvais, capitale mondiale du logiciel libre, l’été prochain"
author: Mélanie Carnot
date: 2014-07-18
href: http://www.courrier-picard.fr/region/beauvais-capitale-mondiale-du-logiciel-libre-l-ete-prochain-ia186b0n407085
tags:
- Entreprise
- Associations
- Promotion
---

> Les geeks de la planète ont rendez-vous à Beauvais l’année prochaine. L’association Oisux et la CCI de l’Oise organiseront les Rencontres mondiales du logiciel libre, en juillet.
