---
site: Le Point
title: "L'enseignement du langage informatique proposé en primaire dès la rentrée"
date: 2014-07-13
href: http://www.lepoint.fr/societe/l-enseignement-du-langage-informatique-propose-en-primaire-des-la-rentree-13-07-2014-1845757_23.php
tags:
- Partage du savoir
- Institutions
- Éducation
---

> L'enseignement du langage informatique sera proposé en primaire dès la rentrée de manière facultative, annonce dans une interview au Journal du dimanche le ministre de l'Education nationale Benoît Hamon, qui prévoit également de relier au haut débit hertzien 9.000 écoles dès septembre.
