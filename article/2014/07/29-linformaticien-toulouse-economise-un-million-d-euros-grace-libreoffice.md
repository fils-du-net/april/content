---
site: L'Informaticien
title: "Toulouse économise un million d’euros grâce à LibreOffice"
author: Guillaume Perissat
date: 2014-07-29
href: http://www.linformaticien.com/actualites/id/33753/toulouse-economise-un-million-d-euros-grace-a-libreoffice.aspx
tags:
- Administration
- Économie
- Neutralité du Net
---

> L’agglomération toulousaine a équipé la quasi-totalité de ses postes informatiques de la suite bureautique LibreOffice, réalisant ainsi une économie d’un million d’euros sur trois ans. Mais ce n’est pas la seule initiative de la ville rose en matière d’open source.
