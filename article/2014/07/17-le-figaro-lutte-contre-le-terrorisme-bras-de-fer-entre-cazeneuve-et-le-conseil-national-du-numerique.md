---
site: Le Figaro
title: "Lutte contre le terrorisme: bras de fer entre Cazeneuve et le conseil national du numérique"
author: Ivan Valerio
date: 2014-07-17
href: http://www.lefigaro.fr/politique/le-scan/decryptages/2014/07/17/25003-20140717ARTFIG00110-bras-de-fer-entre-cazeneuve-et-le-conseil-national-du-numerique.php
tags:
- Internet
- Institutions
---

> La méthode est originale. Alors que le Conseil national du numérique a rendu ce 15 juillet un avis sur l'article 9 du projet de loi renforçant les dispositions relatives à la lutte contre le terrorisme, le ministère de l'Intérieur a répondu immédiatement. Et il l'a fait par un communiqué reprenant l'avis du CNNum, l'amendant point par point.
