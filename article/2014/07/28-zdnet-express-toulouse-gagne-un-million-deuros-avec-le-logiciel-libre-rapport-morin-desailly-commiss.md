---
site: ZDNet
title: "Express: Toulouse gagne un million d'euros avec le logiciel libre, rapport Morin-Desailly, Commission européenne.."
author: Thierry Noisette
date: 2014-07-28
href: http://www.zdnet.fr/actualites/express-toulouse-gagne-un-million-d-euros-avec-le-logiciel-libre-rapport-morin-desailly-commission-europeenne-39804341.htm
tags:
- Entreprise
- Administration
- April
- Institutions
- Marchés publics
- Europe
---

> Bouquet de brèves: la métropole de Toulouse a fait passer 90% de ses postes à LibreOffice; un rapport sénatorial plaide pour plus de logiciels libres; la Commission européenne signe avec Microsoft; des cadres IBM sexistes et bavards; la Silicon Valley, utopie ou cauchemar.
