---
site: Developpez.com
title: "Le choix des logiciels Libres &amp; Open source entraine t-il des gains importants"
date: 2014-07-18
href: http://open-source.developpez.com/actu/73401/Le-choix-des-logiciels-Libres-Open-source-entraine-t-il-des-gains-importants-Quand-est-il-reellement
tags:
- Internet
- Sensibilisation
---

> Sur Internet, vous trouverez beaucoup d'études qui démontrent que les logiciels Libres &amp; Open source entrainent des gains financiers importants, qui peuvent aller jusqu'à un facteur de 3.
> Des institutions ont déjà franchi le pas: la Gendarmerie, la ville de munich, etc.
