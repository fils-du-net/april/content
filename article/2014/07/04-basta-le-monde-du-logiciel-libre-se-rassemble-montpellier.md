---
site: Basta !
title: "Le monde du logiciel libre se rassemble à Montpellier"
author: Collectif
date: 2014-07-04
href: http://www.bastamag.net/Le-monde-du-logiciel-libre-se
tags:
- Administration
- Partage du savoir
- Associations
- Éducation
- Open Data
---

> L’open data, la culture et les arts libres, l’entreprise et le logiciel libre: voici quelques uns des thèmes abordés lors des Rencontres mondiales du logiciel libre, qui se dérouleront du 5 au 11 juillet, à Montpellier. Au programme, des conférences, des ateliers, des tables-rondes, autour du monde du «libre», rassemblant des «geeks», des professionnels, des personnalités, de simples curieux, des utilisateurs et des politiques.
