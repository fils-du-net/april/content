---
site: Numerama
title: "LibreOffice fait économiser un million d'euros à Toulouse"
author: Julien L.
date: 2014-07-28
href: http://www.numerama.com/magazine/30128-libreoffice-fait-economiser-un-million-d-euros-a-toulouse.html
tags:
- Logiciels privateurs
- Administration
- Économie
---

> À Toulouse, le personnel de la ville a procédé à la migration de nombreux postes de travail afin de remplacer la suite bureautique propriétaire Microsoft Office par une solution libre. Cette bascule vers LibreOffice a permis de faire économiser à la ville pas moins d'un millions d'euros en trois ans.
