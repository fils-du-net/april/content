---
site: Numerama
title: "Le CSA veut \"en finir avec la neutralité du net\""
author: Guillaume Champeau
date: 2014-07-04
href: http://www.numerama.com/magazine/29911-le-csa-veut-en-finir-avec-la-neutralite-du-net.html
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
---

> Le président du CSA Olivier Schrameck a confirmé la volonté du Conseil Supérieur de l'Audiovisuel (CSA) de saboter les tuyaux des fournisseurs d'accès à internet pour favoriser les services audiovisuels français par rapport aux services étrangers qui ne se soumettent pas aux lois françaises.
