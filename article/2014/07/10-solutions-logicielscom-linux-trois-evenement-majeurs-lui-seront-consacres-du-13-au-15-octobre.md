---
site: "Solutions-Logiciels.com"
title: "Linux: trois événement majeurs lui seront consacrés du 13 au 15 octobre"
author: Juliette Paoli
date: 2014-07-10
href: http://www.solutions-logiciels.com/actualites.php?titre=Linux-trois-evenement-majeurs-lui-seront-consacres-du-13-au-15-octobre$urlactu=14806
tags:
- Entreprise
- Associations
- Innovation
- RGI
- Informatique en nuage
- International
---

> Le créateur de Linux, Linus Torvalds, et les dirigeants d'Amazon et de ownCloud ont répondu présents aux prochains événements organisés par la Fondation Linux du 13 au 15 octobre au Centre des Congrès de Düsseldorf. Trois événements en un avec LinuxCon, CloudOpen et Embedded Linux Conference Europe.
