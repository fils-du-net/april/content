---
site: ZDNet
title: "Code à l’ecole: les associations dans le flou"
author: Louis Adam
date: 2014-07-17
href: http://www.zdnet.fr/actualites/code-a-l-ecole-les-associations-dans-le-flou-39803961.htm
tags:
- Institutions
- Associations
- Éducation
- Innovation
---

> Benoit Hamon annonçait dans le JDD sa volonté d’intégrer l’apprentissage du code à l’école primaire dès septembre. Cette première étape, prévue pour la rentrée 2014, devrait être dévolues aux associations qui proposeront leurs activités sur les temps périscolaires. Mais ces dernières cherchent encore l'appel à projets évoqué...
