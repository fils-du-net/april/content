---
site: Numerama
title: "Nadine Morano experte d'Internet au Parlement Européen?"
author: Guillaume Champeau
date: 2014-07-02
href: http://www.numerama.com/magazine/29886-nadine-morano-experte-d-internet-au-parlement-europeen.html
tags:
- Internet
- Institutions
- Europe
---

> Il fallait y penser. Selon le site Contexte, qui est très au fait des coulisses parlementaires, l'UMP prévoirait d'envoyer Nadine Morano siéger au sein de la Commission permanente chargée de l'industrie, des télécoms et de l'énergie (ITRE) au Parlement européen. Une anecdote qui ne manque pas de sel quand on sait à quel point Nadine Morano est probablement la responsable politique la plus moquée par les internautes, en particulier sur Twitter où ses tweets sont régulièrement tournés au ridicule.
