---
site: 20minutes.fr
title: "Des logiciels libres aux atouts qui n'ont pas de prix"
author:  Nicolas Bonzom
date: 2014-07-03
href: http://www.20minutes.fr/montpellier/1414369-logiciels-libres-atouts-prix
tags:
- Partage du savoir
- Promotion
- Sciences
---

> Les 15e rencontres mondiales du logiciel libre s'installent à Montpellier, du 5 au 11 juillet. Au programme, conférences, ateliers et rencontres gratuites avec de grands noms du secteur (lire encadré). L'occasion de se pencher sur un phénomène difficilement chiffrable, mais qui «grossit à vue d'œil», selon certains professionnels : le logiciel libre, souvent gratuit, dont la diffusion et la modification sont permises, séduit de plus en plus d'entreprises, autour de Montpellier.
