---
site: "les-infostrateges.com"
title: "Rencontres mondiales du Logiciel libre - du 5 au 11 juillet à Montpellier"
author: Fabrice Molinaro
date: 2014-07-01
href: http://www.les-infostrateges.com/actu/14061838/rencontres-mondiales-du-logiciel-libre-du-5-au-11-juillet-a-montpellier
tags:
- Administration
- Associations
- Promotion
- Open Data
---

> Les RMLL (Rencontres Mondiales du Logiciel Libre) se dérouleront à Montpellier du 5 au 11 juillet 2014. L'objectif est de fournir un lieu d'échanges entre utilisateurs, développeurs et acteurs du Logiciel Libre.
