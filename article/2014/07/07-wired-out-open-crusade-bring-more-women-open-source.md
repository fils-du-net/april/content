---
site: Wired
title: "Out in the Open: The Crusade to Bring More Women to Open Source"
author: Klint Finley
date: 2014-07-07
href: http://www.wired.com/2014/07/openhatch
tags:
- Éducation
- Promotion
- International
- English
---

> (De récents rapports de Facebook et Google confirment ce que nous avons toujours su: les géants de la technologie ont un problème de diversité. Mais dans le monde open source, le problème est encore pire) Recent reports from Facebook and Google confirmed what we’ve known all along: the giants of tech have a diversity problem. But in the world of open source, the problem is even worse.
