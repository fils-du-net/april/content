---
site: Next INpact
title: "Un rapport parlementaire encourage au développement des logiciels libres"
author: Xavier Berne
date: 2014-07-15
href: http://www.nextinpact.com/news/88675-un-rapport-parlementaire-encourage-au-developpement-logiciels-libres.htm
tags:
- Administration
- Institutions
- Marchés publics
- Promotion
---

> Un récent rapport sénatorial vient d’apporter un soutien de poids au développement des logiciels libres en France. Il y est en effet préconisé d’encourager le déploiement de ces programmes dont l'étude, la reproduction et la modification est autorisée, et ce à la fois «par leur intégration dans les marchés publics et par l’imposition de standards ouverts». Ce coup d’accélérateur pourrait avoir lieu à condition toutefois «de développer les compétences pour l’utilisation de ces logiciels et standards».
