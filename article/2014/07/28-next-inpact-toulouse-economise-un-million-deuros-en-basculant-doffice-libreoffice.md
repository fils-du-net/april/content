---
site: Next INpact
title: "Toulouse économise un million d'euros en basculant d'Office à LibreOffice"
author: Vincent Hermann
date: 2014-07-28
href: http://www.nextinpact.com/news/88935-toulouse-economie-million-deuros-en-basculant-doffice-a-libreoffice.htm
tags:
- Administration
- Économie
- April
- Associations
---

> Toulouse est depuis peu l’une des villes de France les plus fournies en solutions libres pour son administration. Elle a ainsi annoncé la semaine dernière avoir économisé un million d’euros en basculant l’ensemble des postes de son administration vers LibreOffice. Mais ce mouvement est simplement le dernier en date d’une longue série de travaux.
