---
site: Next INpact
title: "Suite au «fail» de la DILA, Regards Citoyens publie un guide sur l’Open Data"
author: Xavier Berne
date: 2014-07-08
href: http://www.nextinpact.com/news/88585-suite-au-fail-dila-regards-citoyens-publie-guide-sur-l-open-data.htm
tags:
- Administration
- Associations
- Standards
- Open Data
---

> Suite aux différents problèmes qui ont accompagné la mise à disposition, gratuite, des données juridiques détenues par la Direction de l'information légale et administrative (DILA), l’association Regards Citoyens vient de publier un «petit guide à destination des administrations souhaitant basculer du payant à l’Open Data». L’occasion pour l’organisation de revenir de manière synthétique sur le processus d’ouverture et de partage de données publiques, tout en insistant sur le récent épisode de la DILA, qui est considéré comme «l’un des plus gros «fail» administratifs en matière d’Open Data».
