---
site: Les Echos
title: "Cultivez vos hackers!"
author: Frédéric Bardeau
date: 2014-07-31
href: http://business.lesechos.fr/directions-numeriques/0203672525409-cultivez-vos-hackers-102075.php
tags:
- Entreprise
- Internet
- Économie
- Sensibilisation
- Éducation
- Innovation
- Sciences
---

> L’apprentissage du code informatique à tous les niveaux de l’entreprise est une voie vers la croissance grâce à un usage créatif du numérique. Par Frédéric Bardeau, coauteur de Lire, Ecrire, Compter, Coder (éditions FYP).
