---
site: Next INpact
title: "La distribution Tails, spécialisée dans l'anonymat, comporte des failles 0-day"
author: Vincent Hermann
date: 2014-07-24
href: http://www.nextinpact.com/news/88877-la-distribution-tails-specialisee-dans-anonymat-comporte-failles-0-day.htm
tags:
- Entreprise
- Internet
- Innovation
- Vie privée
---

> La distribution Tails Linux, axée sur la sécurité, contiendrait plusieurs failles de sécurité 0-day qui exposeraient les données de l’utilisateur. Un vrai problème pour le chercheur Loc Nguyen, qui a fait la découverte, puisque ce système se veut justement nettement plus sécurisé que la moyenne.
