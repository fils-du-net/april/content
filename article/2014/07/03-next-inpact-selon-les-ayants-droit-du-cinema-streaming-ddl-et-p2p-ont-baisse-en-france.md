---
site: Next INpact
title: "Selon les ayants droit du cinéma, streaming, DDL et P2P ont baissé en France"
author: Xavier Berne
date: 2014-07-03
href: http://www.nextinpact.com/news/88501-selon-ayants-droit-cinema-streaming-ddl-et-p2p-ont-baisse-en-france.htm
tags:
- Internet
- Économie
- HADOPI
- Institutions
- Droit d'auteur
---

> Y a-t-il en France une explosion du piratage? Si l’Hadopi dit que non, certains ayants droit maintiennent le contraire, notamment depuis l’arrivée au pouvoir de François Hollande. L’ALPA, qui représente les principales majors du cinéma français, a dévoilé hier une étude censée mettre en avant cette «extension massive» du piratage de séries et de films. Pourtant, force est de constater au regard de ces chiffres que la tendance est plutôt à la baisse, au moins sur le long terme.
