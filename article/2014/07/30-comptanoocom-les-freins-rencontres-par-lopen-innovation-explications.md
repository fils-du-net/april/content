---
site: comptanoo.com
title: "Les freins rencontrés par l'Open Innovation: explications"
date: 2014-07-30
href: http://www.comptanoo.com/innovation/actualite-tpe-pme/22623/les-freins-rencontres-par-open-innovation-explications
tags:
- Entreprise
- Sensibilisation
- Innovation
- Open Data
---

> L'«innovation ouverte» est basée sur le partage, la coopération entre entreprises. Ce partage peut se faire sur l'intelligence économique, des savoir-faire ou de la R&amp;D.
