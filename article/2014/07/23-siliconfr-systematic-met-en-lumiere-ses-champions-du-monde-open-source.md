---
site: Silicon.fr
title: "Systematic met en lumière ses champions du monde Open Source"
author: David Feugey
date: 2014-07-23
href: http://www.silicon.fr/systematic-met-en-lumiere-ses-champions-du-monde-open-source-95717.html
tags:
- Entreprise
- Économie
- Innovation
---

> Le pôle de compétitivité Systematic Paris-Région soutient de nombreuses sociétés travaillant sur des projets open source. Découvrez-les dans notre article.
