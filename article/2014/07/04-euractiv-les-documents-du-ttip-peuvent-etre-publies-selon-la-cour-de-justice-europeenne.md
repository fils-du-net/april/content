---
site: EurActiv
title: "Les documents du TTIP peuvent être publiés, selon la Cour de justice européenne"
date: 2014-07-04
href: http://www.euractiv.fr/sections/euro-finances/les-documents-du-ttip-peuvent-etre-publies-selon-la-cour-de-justice
tags:
- Économie
- Institutions
- Europe
- International
- Open Data
- ACTA
---

> Un arrêt rendu par la Cour de justice de l'Union européenne le 3 juillet ouvre la voie à la publication des documents portant sur les négociations du TTIP. La Cour juge que les textes portant sur les questions internationales ne doivent pas être systématiquement confidentiels.
