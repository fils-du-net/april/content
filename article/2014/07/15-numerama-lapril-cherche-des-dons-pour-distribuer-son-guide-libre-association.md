---
site: Numerama
title: "L'April cherche des dons pour distribuer son Guide Libre Association"
author: Guillaume Champeau
date: 2014-07-15
href: http://www.numerama.com/magazine/30004-l-april-cherche-des-dons-pour-distribuer-son-guide-libre-association.html
tags:
- Économie
- April
- Sensibilisation
- Associations
---

> Alors que le gouvernement a fait de l'engagement associatif la grande cause nationale 2014, l'Association promouvoir et défendre le logiciel libre (April) aimerait faire distribuer auprès du maximum d'entre-elles le guide à destination des associations qu'elle a mis au point pour leur apprendre à connaître les logiciels libres qui peuvent leur être utile.
