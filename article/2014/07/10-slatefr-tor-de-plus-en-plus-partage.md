---
site: Slate.fr
title: "Tor, de plus en plus partagé"
author: Amaelle Guiton
date: 2014-07-10
href: http://www.slate.fr/story/89673/tor
tags:
- Internet
- Associations
- Innovation
- Vie privée
---

> Boosté par les scandales de surveillance numérique, le réseau d'anonymisation enregistre depuis un an un afflux conséquent d'utilisateurs. Ce changement d'échelle et de périmètre pose à ses développeurs des défis complexes.
