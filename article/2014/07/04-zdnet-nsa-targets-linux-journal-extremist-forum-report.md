---
site: ZDNet
title: "NSA targets Linux Journal as 'extremist forum': Report"
author: Leon Spencer
date: 2014-07-04
href: http://www.zdnet.com/nsa-targets-linux-journal-as-extremist-forum-report-7000031241
tags:
- Internet
- Institutions
- Associations
- International
- English
- Vie privée
---

> (La NSA cible "Linux Journal" comme un forum d'"extrémistes" et marquent ses lecteurs comme tel, d'après du code source fuité du radiodiffuseur publique Allemand ARD) The NSA is targeting the Linux Journal as an "extremist forum" and flagging its readers as 'extremists', according to source code leaked to German public broadcaster, ARD.
