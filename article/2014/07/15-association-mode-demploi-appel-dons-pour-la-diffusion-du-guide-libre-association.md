---
site: Association mode d'emploi
title: "Appel à dons pour la diffusion du Guide Libre Association"
author: Territorial
date: 2014-07-15
href: http://www.associationmodeemploi.fr/PAR_TPL_IDENTIFIANT/69264/TPL_CODE/TPL_ACTURES_FICHE/PAG_TITLE/Appel+%C3%A0+dons+pour+la+diffusion+du+Guide+Libre+Association/2464-a-la-une.htm
tags:
- Économie
- April
- Associations
---

> L'April est une association regroupant 4 000 membres utilisateurs et producteurs de logiciels libres. Aspirant à donner aux associations les clés de leur liberté informatique, elle a réalisé un guide spécifiquement pour elles afin de découvrir les outils libres appropriés à leurs besoins.
