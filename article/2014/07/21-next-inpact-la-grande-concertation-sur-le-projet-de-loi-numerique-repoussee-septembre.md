---
site: Next INpact
title: "La grande concertation sur le projet de loi numérique repoussée à septembre"
author: Xavier Berne
date: 2014-07-21
href: http://www.nextinpact.com/news/88776-la-grande-concertation-sur-projet-loi-numerique-repoussee-a-septembre.htm
tags:
- HADOPI
- Institutions
---

> Si le gouvernement promet depuis plusieurs mois qu’il va saisir «prochainement» le Conseil national du numérique afin que s’engage la concertation préalable au dépôt d’un grand projet de loi consacré à différents sujets relatifs au numérique (innovation, protection des données personnelles, modernisation de l’action publique...), l’institution attend encore le feu vert de l’exécutif. À tel point qu’il faudra désormais patientier jusqu'au mois de septembre afin que le CNNum puisse attaquer ses travaux.
