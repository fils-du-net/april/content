---
site: Next INpact
title: "Initiation des enfants au code: l'appel à projets du gouvernement"
author: Xavier Berne
date: 2014-07-17
href: http://www.nextinpact.com/news/88753-initiation-enfants-au-code-informatique-appel-a-projets-gouvernement.htm
tags:
- Institutions
- Associations
- Éducation
- Sciences
---

> Le ministère de l’Éducation nationale vient de nous confirmer que l’appel à projet évoqué par Benoît Hamon s’agissant de l’initiation des enfants au code informatique avait bien été publié le 19 juin dernier. Next INpact revient donc sur le contenu de ce document, au travers duquel il est bien davantage question de méthodes d'éveil que de contenu.
