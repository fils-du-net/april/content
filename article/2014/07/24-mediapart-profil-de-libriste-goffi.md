---
site: Mediapart
title: "Profil de libriste: Goffi"
author: André Ani
date: 2014-07-24
href: http://blogs.mediapart.fr/blog/andre-ani/240714/profil-de-libriste-goffi
tags:
- Internet
- Sensibilisation
- Innovation
- Vie privée
---

> Je m’appelle Jérôme Poisson, aussi connu sous le pseudonyme «Goffi», développeur et voyageur à mes heures, j’ai plusieurs centres d’intérêts, dont la politique. Difficile d’en dire plus: je préfère nettement une rencontre autour d’une bonne bière qu’une autobiographie
