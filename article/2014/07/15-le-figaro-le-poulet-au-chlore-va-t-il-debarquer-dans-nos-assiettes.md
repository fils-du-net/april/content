---
site: Le Figaro
title: "Le poulet au chlore va-t-il débarquer dans nos assiettes?"
author: Thomas Oliveau
date: 2014-07-15
href: http://www.lefigaro.fr/conjoncture/2014/07/15/20002-20140715ARTFIG00136-le-poulet-au-chlore-va-t-il-debarquer-dans-nos-assiettes.php
tags:
- Économie
- Institutions
- Europe
- International
---

> Si les États-Unis et l'Union européenne s'accordent sur le traité transatlantique, les désormais fameux poulets au chlore et bœufs aux hormones finiront-ils vraiment par atterrir dans nos assiettes? Réponse.
