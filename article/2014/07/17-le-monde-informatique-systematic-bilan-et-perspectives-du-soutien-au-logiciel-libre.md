---
site: Le Monde Informatique
title: "Systematic: bilan et perspectives du soutien au logiciel libre"
author: Dominique Filippone
date: 2014-07-17
href: http://www.lemondeinformatique.fr/actualites/lire-systematic-bilan-et-perspectives-du-soutien-au-logiciel-libre-58108.html
tags:
- Associations
- Innovation
- Promotion
---

> Après 7 ans d'existence, le Groupe Thématique Logiciel Libre (GTLL) du pôle Systematic paris Région a permis à 80 projets d'être labellisés et à près de la moitié d'entre eux d'obtenir des financements pour assurer leur développement. Big data, qualité logicielle et technologies de l'après PC constituent les principales priorités de soutien aux projets R&amp;D en logiciels libres de GTLL pour les années à venir.
