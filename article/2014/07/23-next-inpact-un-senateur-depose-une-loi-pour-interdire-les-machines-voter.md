---
site: Next INpact
title: "Un sénateur dépose une loi pour interdire les machines à voter"
author: Xavier Berne
date: 2014-07-23
href: http://www.nextinpact.com/news/88849-un-senateur-depose-loi-pour-interdire-machines-a-voter.htm
tags:
- Internet
- Institutions
- Vote électronique
---

> Alors qu’une députée a récemment suggéré au gouvernement d’interdire le vote électronique, un sénateur appartenant à la majorité socialiste vient de déposer une proposition de loi visant à supprimer le recours aux machines à voter en France. Depuis 2007, il existe un moratoire qui empêche toute nouvelle commune d’opter pour de tels dispositifs, mais l’élu voudrait que la soixantaine de villes utilisant encore ces appareils soient contraintes de les abandonner.
