---
site: LaDepeche.fr
title: "Toulouse économise un million d'euros en s'engageant dans le logiciel libre"
date: 2014-07-30
href: http://www.ladepeche.fr/article/2014/07/30/1926609-toulouse-economise-million-euros-engageant-logiciel-libre.html
tags:
- Administration
- Économie
- April
---

> Il n'y a pas de petites économies… La démarche avait été initialisée par l'ancienne municipalité, la nouvelle l'a concrétisée. En basculant l'ensemble des postes de son administration vers le logiciel LibreOffice, Toulouse a économisé un million d'€. «Toulouse Métropole regroupe 37 municipalités et 714 000 habitants, rappelle Bertrand Serp, vice-président de Toulouse Métropole en charge du numérique. Désormais les 10 000 postes qui composent son administration utilisent depuis peu le logiciel LibreOffice. Un logiciel bureautique à moindre coût.
