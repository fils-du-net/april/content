---
site: Mediapart
title: "Guide Libre Association, appel aux dons"
author: André Ani
date: 2014-07-04
href: http://blogs.mediapart.fr/blog/andre-ani/040714/guide-libre-association-appel-aux-dons
tags:
- Économie
- April
- Promotion
---

> L’April a décidé de relancer le Guide Libre Association, afin de le mettre à jour, de le compléter et de le diffuser à nouveau le plus possible dans le milieu associatif.
