---
site: Next INpact
title: "Un rapport confidentiel plaide pour renforcer l’informatique à l’école"
author: Xavier Berne
date: 2014-07-04
href: http://www.nextinpact.com/news/88541-un-rapport-confidentiel-plaide-pour-renforcer-l-informatique-a-l-ecole.htm
tags:
- Institutions
- Éducation
---

> Au travers d’un rapport à destination du ministre de l’Éducation nationale, les services de la Rue de Grenelle viennent de plaider de manière appuyée en faveur d’une revalorisation de l’enseignement de l’informatique à l’école. Ce document, qui était censé rester confidentiel, suggère entre autre de faire de l’informatique une discipline à part entière au collège.
