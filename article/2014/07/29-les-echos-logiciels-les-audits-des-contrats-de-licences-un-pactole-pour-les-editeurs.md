---
site: Les Echos
title: "Logiciels: les audits des contrats de licences, un pactole pour les éditeurs"
author: Jacques Henno
date: 2014-07-29
href: http://www.lesechos.fr/journal20140729/lec2_high_tech_et_medias/0203667138819-logiciels-les-audits-des-contrats-de-licences-un-pactole-pour-les-editeurs-1028513.php
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Licenses
---

> Certains éditeurs de logiciels infligeraient à leurs clients des amendes de plusieurs millions d'euros. Ils sanctionnent ainsi le non-respect des contrats de licences.
