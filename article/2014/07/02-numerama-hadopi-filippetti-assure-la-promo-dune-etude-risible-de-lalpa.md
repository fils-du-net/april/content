---
site: Numerama
title: "Hadopi: Filippetti assure la promo d'une étude risible de l'ALPA"
author: Guillaume Champeau
date: 2014-07-02
href: http://www.numerama.com/magazine/29890-hadopi-filippetti-assure-la-promo-d-une-etude-risible-de-l-alpa.html
tags:
- Internet
- HADOPI
- Institutions
- Droit d'auteur
---

> Ou comment Aurélie Filippetti s'appuie sur une étude d'un lobby privé pour lui faire dire le contraire de ce qu'elle démontre en matière de réalité du piratage en France, et demander que la lutte contre les pratiques des internautes soit encore intensifiée...
