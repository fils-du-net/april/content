---
site: Numerama
title: "L'arrêt du support de Windows XP alarme une sénatrice"
author: Julien L.
date: 2014-04-26
href: http://www.numerama.com/magazine/29204-l-arret-du-support-de-windows-xp-alarme-une-senatrice.html
tags:
- Logiciels privateurs
- Administration
- HADOPI
- Institutions
---

> Windows XP n'est plus mis à jour depuis le 8 avril, pourtant le système d'exploitation reste présent sur près d'une machine sur dix en France. Une situation qui n'a pas échappé à une sénatrice, qui demande au gouvernement de préciser ce qu'il a fait pour résoudre cette situation.
