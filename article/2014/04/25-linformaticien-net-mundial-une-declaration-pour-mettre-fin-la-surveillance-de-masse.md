---
site: L'Informaticien
title: "NET Mundial: une déclaration pour mettre fin à la surveillance de masse"
author: Margaux Duquesne
date: 2014-04-25
href: http://www.linformaticien.com/actualites/id/32964/net-mundial-une-declaration-pour-mettre-fin-a-la-surveillance-de-masse.aspx
tags:
- Internet
- Institutions
- Neutralité du Net
- Promotion
- Sciences
- Standards
- International
- Vie privée
---

> Le NET Mundial, organisé au Brésil, a abouti à un texte publié hier soir: il opte pour un contrôle multilatéral de l’utilisation d’Internet et condamne la surveillance de masse.
