---
site: LExpansion.com
title: "Wikipedia: encyclopédistes 2.0"
author: Julie de la Brosse
date: 2014-04-23
href: http://lexpansion.lexpress.fr/high-tech/wikipedia-encyclopedistes-2-0_1510773.html
tags:
- Internet
- Partage du savoir
- Associations
- Contenus libres
---

> Qui sont ces milliers de wikipédiens français, contributeurs de "l'encyclopédie libre" lancée en 2001 et devenue l'un des sites cultes de la Toile? Ils forment une fourmilière de bénévoles, passionnés, cultivés et parfois militants. Visite d'une étrange tribu avec ses lois et ses clans, ses rites et ses querelles.
