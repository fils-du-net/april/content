---
site: ZDNet
title: "Grenoble a une adjointe au maire déléguée à l'open data et veut utiliser des logiciels libres"
author: Thierry Noisette
date: 2014-04-05
href: http://www.zdnet.fr/actualites/grenoble-a-une-adjointe-au-maire-deleguee-a-l-open-data-et-veut-utiliser-des-logiciels-libres-39799519.htm
tags:
- Administration
- April
- Open Data
---

> Laurence Comparat est devenue adjointe avec en délégation l'accès à l'information et aux données publiques. La liste menée par EELV avait annoncé sa volonté de développer l'open data et l'usage des logiciels libres.
