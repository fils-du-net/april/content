---
site: "ouest-france.fr"
title: "La culture du «libre» pendant deux jours au lycée"
date: 2014-04-02
href: http://www.ouest-france.fr/la-culture-du-libre-pendant-deux-jours-au-lycee-2088812
tags:
- Internet
- Administration
- Éducation
- Promotion
---

> Vendredi et samedi, la culture du « libre » étaient à l'honneur. Libre comme les logiciels libres et comme la biodiversité, ou comme le marché libre du Sel (système d'échange local).
