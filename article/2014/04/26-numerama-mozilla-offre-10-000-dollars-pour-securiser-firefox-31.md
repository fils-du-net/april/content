---
site: Numerama
title: "Mozilla offre 10 000 dollars pour sécuriser Firefox 31"
author: Julien L.
date: 2014-04-26
href: http://www.numerama.com/magazine/29208-mozilla-offre-10-000-dollars-pour-securiser-firefox-31.html
tags:
- Internet
- Économie
- Associations
---

> La fondation Mozilla annonce une opération spéciale visant à sécuriser la version 31 de Firefox, qui doit arriver cet été. Il s'agit de contrôler la nouvelle librairie de vérification de certificat, baptisée libPKIX. Les récompenses peuvent atteindre 10 000 dollars, contre 3000 habituellement.
