---
site: "usine-digitale.fr"
title: "L'homme de demain sera connecté, imprimé en 3D et open source"
author: Nora Poggi
date: 2014-04-28
href: http://www.usine-digitale.fr/article/l-homme-de-demain-sera-connecte-imprime-en-3d-et-open-source.N258326
tags:
- Économie
- Matériel libre
- Innovation
---

> Des prothèses qui fonctionnent mieux que le corps humain, des lentilles de contact avec caméra intégrée... L'homme bionique pourrait devenir bientôt une réalité, à mesure que les nouvelles technologies deviennent plus performantes et bon marché.
