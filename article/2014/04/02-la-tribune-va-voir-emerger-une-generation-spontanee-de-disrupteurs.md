---
site: La Tribune
title: "«On va voir émerger une génération spontanée de disrupteurs»"
author: Erick Haehnsen
date: 2014-04-02
href: http://www.latribune.fr/entreprises-finance/industrie/20140402trib000823259/-on-va-voir-emerger-une-generation-spontanee-de-disrupteurs-.html
tags:
- Entreprise
- Internet
- Économie
- Innovation
---

> Jean-Luc Beylat, président d'Alcatel-Lucent Bell Labs France, du pôle Systematic Paris Région, et de l'Association française des pôles de compétitivité. Il considère que «le facteur temps est devenu critique» dans le processus d'innovation.
