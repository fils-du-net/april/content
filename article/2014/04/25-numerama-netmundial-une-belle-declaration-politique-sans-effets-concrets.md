---
site: Numerama
title: "NETmundial: une belle déclaration politique sans effets concrets"
author: Guillaume Champeau
date: 2014-04-25
href: http://www.numerama.com/magazine/29195-netmundial-une-belle-declaration-politique-sans-effets-concrets.html
tags:
- Internet
- Institutions
- Neutralité du Net
- International
---

> Applaudie par beaucoup d'observateurs et d'acteurs, la déclaration sur la gouvernance d'Internet issue de la conférence NETmundial au Brésil ne constitue qu'un engagement politique sans valeur juridique. Sa rédaction ménage par ailleurs d'inquiétantes zones d'ombres aux côtés des principes fondamentaux rappelés avec force.
