---
site: Numerama
title: "La surveillance de masse contestée à nouveau devant la CEDH"
author: Julien L.
date: 2014-04-04
href: http://www.numerama.com/magazine/28989-la-surveillance-de-masse-contestee-a-nouveau-devant-la-cedh.html
tags:
- Internet
- Institutions
- Associations
- Europe
- Vie privée
---

> Treize organisations de défense des droits et libertés sur Internet en Europe dont la Quadrature du Net ont joint leurs forces pour soutenir une action juridique lancée en octobre devant la CEDH et dont l'objet conteste la légalité de la surveillance de masse.
