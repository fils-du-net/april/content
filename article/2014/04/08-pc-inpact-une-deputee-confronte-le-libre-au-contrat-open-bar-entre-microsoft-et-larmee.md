---
site: PC INpact
title: "Une députée confronte le libre au contrat open bar entre Microsoft et l'Armée"
author: Marc Rees
date: 2014-04-08
href: http://www.pcinpact.com/news/86924-une-deputee-confronte-libre-au-contrat-open-bar-entre-microsoft-et-armee.htm
tags:
- Entreprise
- Logiciels privateurs
- Administration
- April
- Institutions
- Marchés publics
- Neutralité du Net
---

> Encore et toujours, le contrat open bar liant le ministère de la Défense à Microsoft interroge les députés. Dernier en date, Marie-Françoise Bechtel qui vient de questionner le nouveau ministre sur le renouvellement de cet accord-cadre.
