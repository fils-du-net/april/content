---
site: Le Point
title: "Steve Jobs voulait vous \"enfermer\" chez Apple"
author: Guerric Poncet
date: 2014-04-08
href: http://www.lepoint.fr/chroniqueurs-du-point/guerric-poncet/steve-jobs-voulait-vous-enfermer-chez-apple-08-04-2014-1811036_506.php
tags:
- Entreprise
- Logiciels privateurs
---

> Dans un courriel envoyé en 2010, le patron d'Apple détaillait la stratégie du groupe. Fascinant... et inquiétant.
