---
site: 01netPro.
title: "Peut-on sécuriser l'open-source?"
author: Hervé Sibert
date: 2014-04-16
href: http://pro.01net.com/editorial/618196/peut-on-securiser-l-open-source
tags:
- Entreprise
- Logiciels privateurs
---

> La désormais célèbre faille Heartbleed soulève à nouveau la question de la fiabilité du code open source. Les partisans de l’ouverture du code, permettant à chacun de rechercher et trouver des failles éventuelles afin de les corriger, s’opposent à ceux de la sécurité par l’obscurité, pour qui un code fermé est un rempart de plus.
