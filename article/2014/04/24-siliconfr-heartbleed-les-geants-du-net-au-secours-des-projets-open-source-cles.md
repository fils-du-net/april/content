---
site: Silicon.fr
title: "Heartbleed: les géants du Net au secours des projets Open Source clés"
author: Jacques Cheminat
date: 2014-04-24
href: http://www.silicon.fr/heartbleed-google-facebook-microsoft-financent-les-projets-open-source-cles-93970.html
tags:
- Entreprise
- Internet
- Économie
- Vie privée
---

> Après avoir chacun œuvré de leur côté pour colmater la faille Heartbleed dans OpenSSL, plusieurs acteurs de l’IT ont décidé d’investir collectivement sous la bannière de la Fondation Linux dans l’amélioration et la sécurisation des logiciels Open Source.
