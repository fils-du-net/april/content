---
site: atlantico
title: "Comment Heartbleed a mis en évidence les faiblesses de l’Internet open source"
author: Maxime Pinard
date: 2014-04-21
href: http://www.atlantico.fr/decryptage/comment-heartbleed-mis-en-evidence-faiblesses-internet-open-source-maxime-pinard-1047764.html
tags:
- Internet
- Vie privée
---

> Heartbleed a récemment secoué l'ensemble du web. Pour autant cette faille existe depuis plus de deux ans, et n'a été découverte que récemmentu. Que traduit-elle finalement, de l'état d'Internet et de l'open source? Est-ce vraiment le lieu sûr que l'on prétend?
