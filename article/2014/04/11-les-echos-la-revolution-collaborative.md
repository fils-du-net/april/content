---
site: Les Echos
title: "La révolution collaborative"
author: Benoit Georges
date: 2014-04-11
href: http://www.lesechos.fr/opinions/livres/0203435017481-la-revolution-collaborative-663926.php?xtor=RSS-2230
tags:
- Économie
- Innovation
---

> Après avoir prédit la fin du travail et la troisième révolution industrielle, le prospectiviste Jeremy Rifkin annonce rien de moins que le déclin du capitalisme, éclipsé par l'Internet des objets et l'économie solidaire.
