---
site: "Région Île-de-France"
title: "Open Week: liberté, ouverture, partage"
author: Xavier Frison
date: 2014-04-01
href: http://www.iledefrance.fr/open-week-liberte-ouverture-partage
tags:
- Administration
- Associations
- Innovation
- Open Data
---

> Du 4 au 11 avril, la première Open Week invite les Franciliens à découvrir le meilleur de la région en matière d’innovations liées aux données ouvertes, de projets collaboratifs et de consommation collaborative.
