---
site: Electron Libre
title: "Loi Création: vous reprendrez bien un peu de retard?"
author: Emmanuel Torregano				
date: 2014-04-29
href: http://electronlibre.info/loi-creation-reprendrez-bien-peu-retard
tags:
- HADOPI
- Institutions
- Europe
- International
- English
---

> Aurélie Filippetti s’est elle une fois de plus avancée un peu trop? Lors des Rencontres de l’Adami à Metz le 18 avril dernier, la ministre de la Culture et de la Communication avait promis aux créateurs que le brouillon du volet numérique du projet de loi Création (dont nous avions dévoilé les grands axes dans le numéro 5 de Haut Parleur) leur serait envoyé dès la semaine suivante
