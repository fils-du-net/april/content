---
site: Big Browser
title: "Les codes de la NASA rendus accessibles"
date: 2014-04-10
href: http://bigbrowser.blog.lemonde.fr/2014/04/10/open-source-les-codes-de-la-nasa-rendus-accessibles
tags:
- Administration
- Partage du savoir
- Institutions
- Sciences
- Open Data
---

> Aujourd'hui, la NASA rend public un catalogue des codes qui se cachent derrière plus de 1 000 projets développés depuis les années 1960 par l'agence spatiale américaine. Le magazine américain Wired rapporte qu'à partir de jeudi 10 avril les internautes pourront par exemple se procurer les lignes de code du système de guidage du module lunaire Apollo 11, qui ont permis à Buzz Aldrin et Neil Armstrong de marcher sur la Lune le 21 juillet 1969.
