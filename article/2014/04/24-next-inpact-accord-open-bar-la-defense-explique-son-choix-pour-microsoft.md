---
site: Next INpact
title: "Accord «Open Bar»: la Défense explique son choix pour Microsoft"
author: Marc Rees
date: 2014-04-24
href: http://www.nextinpact.com/news/87202-accord-open-bar-defense-explique-son-choix-pour-microsoft.htm
tags:
- Logiciels privateurs
- Administration
- Marchés publics
---

> Le ministère de la Défense vient finalement d’expliquer officiellement le choix Microsoft pour l’équipement de ses postes. La socialiste Marie-Françoise Bechtel et le communiste Jean-Jacques Candelier l’avaient interrogé sur l’opportunité de ce choix, alors qu’un accord-cadre de 2009 a été reconduit en 2013 pour quatre ans avec l’éditeur de Redmond.
