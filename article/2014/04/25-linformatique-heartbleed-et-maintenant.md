---
site: L'informatique
title: "Heartbleed: et maintenant?"
author: Hicham Alaoui
date: 2014-04-25
href: http://www.linformatique.org/heartbleed-maintenant
tags:
- Entreprise
- Internet
---

> Maintenant que le pic de l’épisode Heartbleed est passé, il ne faut surtout pas tourner la page et passer à autre chose, mais revenir sur cette faille pour en tirer des leçons afin que ce genre de situation ne se reproduise si possible plus.
