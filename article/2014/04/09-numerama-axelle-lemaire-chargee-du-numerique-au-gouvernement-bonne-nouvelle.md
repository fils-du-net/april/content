---
site: Numerama
title: "Axelle Lemaire, chargée du Numérique au Gouvernement. Bonne nouvelle?"
author: Guillaume Champeau
date: 2014-04-09
href: http://www.numerama.com/magazine/29035-axelle-lemaire-chargee-du-numerique-au-gouvernement-bonne-nouvelle.html
tags:
- Institutions
- Neutralité du Net
---

> Rattachée au ministre Arnaud Montebourg, Axelle Lemaire prend en charge le secrétariat d'Etat au Numérique. Une personnalité sensible à la question des droits des internautes.
