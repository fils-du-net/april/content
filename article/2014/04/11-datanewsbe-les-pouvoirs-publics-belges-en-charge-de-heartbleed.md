---
site: Datanews.be
title: "Les pouvoirs publics belges en charge de Heartbleed"
author: Guy Kindermans
date: 2014-04-11
href: http://datanews.levif.be/ict/actualite/les-pouvoirs-publics-belges-en-charge-de-heartbleed/article-4000589885779.htm
tags:
- Internet
- Administration
- International
---

> Aussitôt après l’annonce de Heartbleed, tant CERT.BE que Fedict ont fait savoir qu’ils prenaient des mesures pour évaluer la menace et la contrer le cas échéant.
