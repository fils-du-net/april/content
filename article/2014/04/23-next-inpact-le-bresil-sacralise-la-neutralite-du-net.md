---
site: Next INpact
title: "Le Brésil sacralise la Neutralité du Net"
author: Xavier Berne
date: 2014-04-23
href: http://www.nextinpact.com/news/87175-le-bresil-sacralise-neutralite-net.htm
tags:
- Internet
- Institutions
- Neutralité du Net
- International
---

> Les sénateurs brésiliens ont adopté hier le «Marco Civil da Internet», un projet de loi consacrant notamment la neutralité du Net. Le texte n’attend plus que la signature de la présidente, Dilma Roussef, pour être gravé dans le marbre. L'intéressée reçoit d’ailleurs à partir d’aujourd’hui et pour deux jours différents représentants de gouvernements, afin de discuter de la gouvernance d’Internet. Explications.
