---
site: We Demain
title: "Wikispeed, la première voiture open-source"
author: Benjamin Tincq
date: 2014-04-30
href: http://www.wedemain.fr/Wikispeed-la-premiere-voiture-open-source_a509.html
tags:
- Entreprise
- Économie
- Matériel libre
- Innovation
---

> Une équipe de bénévoles, un budget des plus modestes et, en à peine trois mois, une voiture à haute efficience énergétique. C'est le phénomène Wikispeed crée par l'Américain Joe Justice.
