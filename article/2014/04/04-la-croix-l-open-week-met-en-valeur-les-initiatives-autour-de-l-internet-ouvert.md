---
site: la Croix
title: "L'«Open week» met en valeur les initiatives autour de l’Internet ouvert"
author: Julien Duriez
date: 2014-04-04
href: http://www.la-croix.com/Solidarite/Actualite/L-Open-week-met-en-valeur-les-initiatives-autour-de-l-Internet-ouvert-2014-04-04-1131208
tags:
- Administration
- Associations
- Promotion
- Open Data
---

> La région francilienne organise du 3 au 11 avril la première édition de l’Open week pour mettre en valeur les initiatives autour des données libres (open data) et des logiciels ouverts.
