---
site: PC INpact
title: "Paquet Télécom: une «menace» pour la Fédération Française des Télécoms"
author: Nil Sanyas
date: 2014-04-04
href: http://www.pcinpact.com/news/86882-le-paquet-telecom-menace-pour-federation-francaise-telecoms.htm
tags:
- Entreprise
- Internet
- Institutions
- Innovation
- Neutralité du Net
- Europe
---

> Voté en première lecture par les eurodéputés hier, le Paquet Télécom a suscité un grand enthousiasme, notamment du côté des défenseurs de la neutralité du net. Les membres de la Fédération Française des Télécoms n'ont toutefois pas le même avis sur la question, estimant même qu'il s'agit d'une «menace pour l'investissement et l'innovation en Europe».
