---
site: Les Echos
title: "Firefox, le meilleur ennemi de Google, Décryptage"
author: Benoit Georges
date: 2014-04-03
href: http://www.lesechos.fr/opinions/decryptage/0203413119822-firefox-le-meilleur-ennemi-de-google-661892.php
tags:
- Entreprise
- Internet
- Associations
---

> En s’attaquant au marché des smartphones low cost, la Fondation Mozilla se pose en alternative au système Android de Google... qui lui apporte pourtant 90 % de ses revenus.
