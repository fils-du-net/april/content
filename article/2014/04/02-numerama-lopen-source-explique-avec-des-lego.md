---
site: Numerama
title: "L'Open Source expliqué avec des LEGO"
author: Guillaume Champeau
date: 2014-04-02
href: http://www.numerama.com/magazine/28958-l-open-source-explique-avec-des-lego.html
tags:
- Sensibilisation
- Promotion
---

> Comment expliquer l'intérêt de l'open-source aux enfants et aux moins jeunes ? L'organisation Bit Blueprint a eu une idée simple et très bien réalisée : utiliser des LEGO. "Soyons honnête, Open Source n'est pas un terme adopté massivement. Pourquoi? Et bien, parce que le message positif d'une histoire ne peut parvenir au public que si elle est racontée de la bonne façon", explique-t-elle pour justifier sa démarche. "Et la bonne façon de la raconter, c'est de le faire de façon à ce que le public puisse la comprendre.
