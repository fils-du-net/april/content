---
site: Logitheque
title: "Heartbleed: Passé, présent et futur d'une faille surmédiatisée"
author: Frederic Pailliot
date: 2014-04-22
href: http://www.logitheque.com/articles/heartbleed_passe_present_et_futur_d_une_faille_surmediatisee_605.htm
tags:
- Internet
- Vie privée
---

> Heartbleed ci, Heartbleed ça...Vous en avez assez de Heartbleed? Tant pis pour vous, car cela ne fait que commencer ( Et s'arrêter rapidement, rassurez-vous), sachant qu'à peu près tous les médias se sont emparés de cette faille de sécurité pour en tirer des articles plus ou moins fouillés. A notre tour de manger nôtre part du gâteau Heartbleed, de vous expliquer les tenants et les aboutissants de cette faille et de vous préparer à la prochaine faille qui va créer panique et paranoïa pendant 48 heures...
