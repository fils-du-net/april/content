---
site: PC INpact
title: "Municipales: 143 signataires du «Pacte du logiciel libre» ont été élus"
author: Xavier Berne
date: 2014-04-03
href: http://www.pcinpact.com/news/86853-municipales-143-signataires-pacte-logiciel-libre-ont-ete-elus.htm
tags:
- Administration
- April
- Institutions
---

> Parmi les 285 candidats aux élections municipales qui avaient signé le «Pacte du logiciel libre» 143 ont été élus conseillers municipaux, voire, pour certains, maires. Un nombre bien plus important que lors des dernières élections, en 2008, où seuls 23 candidats signataires avait été élus.
