---
site: Politis.fr
title: "Une maire adjointe Open data à Grenoble"
author: Aurélien Vernet
date: 2014-04-05
href: http://www.politis.fr/Une-maire-adjointe-Open-data-a,26469.html
tags:
- Économie
- Institutions
- Sensibilisation
- Open Data
- Vie privée
---

> A l’heure d’un nouveau gouvernement qui ne fait que jouer aux chaises musicales, sans bouger le fond d’une politique désavouée par les électeurs, le début de mandat d’Eric Piolle, maire écologiste, tête d’une liste sans grand parti, montre de nouvelles priorités et bouscule les habitudes.
