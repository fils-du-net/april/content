---
site: Next INpact
title: "Heartbleed: un député demande «une sécurisation maximale de la toile»"
author: David Legrand
date: 2014-04-22
href: http://www.nextinpact.com/news/87152-heartbleed-depute-demande-securisation-maximale-toile.htm
tags:
- Internet
- Institutions
---

> Alors que l'on commence à peine à voir les premières sociétés communiquer autour de la faille Heartbleed qui a été découverte il y a maintenant deux semaines, l'Assemblée nationale semble enfin réagir à la question. En effet, une première question d'un député vient d'être posée sur le sujet.
