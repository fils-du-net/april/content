---
site: Numerama
title: "Une ex de la mission Lescure chargée du numérique par Aurélie Filippetti"
author: Guillaume Champeau
date: 2014-04-29
href: http://www.numerama.com/magazine/29226-une-ex-de-la-mission-lescure-chargee-du-numerique-par-aurelie-filippetti.html
tags:
- Internet
- HADOPI
- Institutions
---

> Corédactrice du rapport Lescure, Juliette Mant a été nommée conseillère d'Aurélie Filippetti en charge du numérique au ministère de la culture.
