---
site: Numerama
title: "Le CSA dresse la liste de ce qu'il veut contrôler sur Internet"
author: Guillaume Champeau
date: 2014-04-14
href: http://www.numerama.com/magazine/29080-le-csa-dresse-la-liste-de-ce-qu-il-veut-controler-sur-internet.html
tags:
- Internet
- HADOPI
- Institutions
---

> Dans son rapport annuel, le CSA dresse la liste des domaines d'expressions dans lesquels il entend pouvoir exercer un droit de regard et de censure sur Internet.
