---
site: Numerama
title: "\"L'Europe doit reprendre la main sur le numérique en protégeant la neutralité du net\""
author: Guillaume Champeau
date: 2014-04-02
href: http://www.numerama.com/magazine/28948-l-europe-doit-reprendre-la-main-sur-le-numerique-en-protegeant-la-neutralite-du-net.html
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Neutralité du Net
- Europe
---

> Jeudi après-midi, le Parlement européen doit adopter le rapport Pilar Del Castillo, qui devrait apporter une définition à la neutralité du net. La rédaction retenue aura des conséquences fondamentales. C'est ce que martèlent l'eurodéputée Françoise Castex et le président de la Fédération des FAI associatifs, Benjamin Bayart, dans cette tribune pour Numerama
