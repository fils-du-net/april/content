---
site: ZDNet
title: "Open data: un rapport du Sénat veut l’encadrer sinon l’étouffer"
author: Thierry Noisette
date: 2014-04-18
href: http://www.zdnet.fr/actualites/open-data-un-rapport-du-senat-veut-l-encadrer-sinon-l-etouffer-39800161.htm
tags:
- Internet
- Institutions
- Informatique en nuage
- Open Data
- Vie privée
---

> Le rapport de deux sénateurs met en garde contre une ouverture des données publiques qui nuirait à la protection de la vie privée et appelle à une redevance sur la réutilisation pour financer l’anonymisation des données.
