---
site: PC INpact
title: "Les eurodéputés sacralisent la neutralité du net, sous la menace du Conseil"
author: Nil Sanyas
date: 2014-04-03
href: http://www.pcinpact.com/news/86851-les-eurodeputes-sacralisent-neutralite-net-sous-menace-conseil.htm
tags:
- Internet
- Institutions
- Associations
- Neutralité du Net
- Europe
---

> C'est fait. Les députés européens ont voté à une large majorité le Paquet Télécom initié par Neelie Kroes. Les frais d'itinérance mobile dans les pays de l'UE disparaitront d'ici le 15 décembre 2015. Quant à la neutralité du net, de nombreux amendements ont été votés afin de la renforcer. Une victoire pour ses défenseurs et une claque pour certains lobbies. Mais reste à savoir si le Conseil validera cette version.
