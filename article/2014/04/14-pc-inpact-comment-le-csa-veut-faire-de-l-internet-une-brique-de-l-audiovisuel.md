---
site: PC INpact
title: "Comment le CSA veut faire de l’Internet une brique de l’audiovisuel"
author: Marc Rees
date: 2014-04-14
href: http://www.pcinpact.com/news/87009-comment-csa-veut-faire-l-internet-brique-l-audiovisuel.htm
tags:
- Internet
- HADOPI
- Institutions
---

> Le CSA doit sortir aujourd’hui son rapport annuel. Dans ce document le gendarme de l’audiovisuel transmet du pied à Aurélie Filippetti ses recommandations législatives pour élargir sa mainmise sur les contenus en ligne. Explications.
