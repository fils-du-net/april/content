---
site: "usine-digitale.fr"
title: "Les géants du web tirent les leçons du fiasco Heartbleed et créent la \"Core infrastructure initiative\""
author: Sylvain Arnulf
date: 2014-04-24
href: http://www.usine-digitale.fr/article/les-geants-du-web-tirent-les-lecons-du-fiasco-heartbleed-et-creent-la-core-infrastructure-initiative.N257824
tags:
- Entreprise
- Internet
- Économie
- Neutralité du Net
- Open Data
---

> Les géants du web fondent la "Core infractrure initiative" pour participer au financement des projets open source dont l'importance est majeure... comme la bibliothèque OpenSSL prise en défaut dans le bug Heartbleed.
