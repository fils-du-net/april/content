---
site: Numerama
title: "La journée contre les DRM aura lieu le 6 mai"
author: Julien L.
date: 2014-04-25
href: http://www.numerama.com/magazine/29199-la-journee-contre-les-drm-aura-lieu-le-6-mai.html
tags:
- Internet
- Institutions
- Associations
- DRM
---

> La protestation contre les verrous numériques reprend le 6 mai. C'est en effet à cette date qu'aura lieu la journée internationale contre les DRM, ces mesures techniques de protection qui transforment certains usages en un véritable calvaire.
