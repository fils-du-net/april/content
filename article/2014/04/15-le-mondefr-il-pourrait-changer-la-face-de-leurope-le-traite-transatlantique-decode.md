---
site: Le Monde.fr
title: "Il pourrait changer la face de l'Europe: le traité transatlantique décodé"
author: Maxime Vaudano
date: 2014-04-15
href: http://www.lemonde.fr/les-decodeurs/article/2014/04/15/il-pourrait-changer-la-face-de-l-europe-le-traite-transatlantique-decode_4399476_4355770.html
tags:
- Entreprise
- Économie
- Institutions
- Standards
- Europe
- International
- ACTA
---

> Le traité TAFTA, négocié dans le secret par Bruxelles et Washington, a pour but de constituer un marché commun de 820 millions de consommateurs, qui représenterait la moitié du PIB mondial.
