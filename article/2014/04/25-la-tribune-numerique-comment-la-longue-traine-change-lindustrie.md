---
site: La Tribune
title: "Numérique: comment la longue traîne change l'industrie"
author: Edwin Mootoosamy
date: 2014-04-25
href: http://www.latribune.fr/opinions/tribunes/20140424trib000826802/numerique-comment-la-longue-traine-change-l-industrie.html
tags:
- Entreprise
- Internet
- Économie
- Innovation
---

> Pendant longtemps Internet a été vu comme un média de plus, il a été utilisé par les annonceurs pour communiquer sur leurs produits et augmenter leur chiffre d’affaires. Mais Internet n’est pas un secteur d’activité, Internet infuse dans toutes les sphères de notre société. Quelles sont les pistes d’évolutions pour l’industrie dans ce contexte?
