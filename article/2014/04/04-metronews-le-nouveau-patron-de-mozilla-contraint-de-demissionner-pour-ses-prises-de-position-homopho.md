---
site: metronews
title: "Le nouveau patron de Mozilla contraint de démissionner pour ses prises de position homophobes"
author: Frédéric Boutier
date: 2014-04-04
href: http://www.metronews.fr/high-tech/le-nouveau-patron-de-mozilla-contraint-de-demissionner-pour-ses-prises-de-position-homophobes/mndd!QX8QDrldZ2sw6
tags:
- Internet
- Associations
---

> Que retiendra t-on de Brendan Eich, l'ex nouveau patron de Mozilla? L'invention du langage Javascript, ou ses prises de position contre le mariage homosexuel?
