---
site: Slate.fr
title: "Moi ministre du numérique, par Benjamin Bayart: reprendre tout dans le bon sens"
author: Benjamin Bayart
date: 2014-04-08
href: http://www.slate.fr/tribune/85725/benjamin-bayart-moi-ministre-du-numerique-tribune
tags:
- Internet
- Institutions
- Droit d'auteur
- Éducation
- Neutralité du Net
- Standards
- Vie privée
---

> Quel serait mon programme si on me demandait de devenir ministre du Numérique? Le premier point qu'il faut comprendre, c'est que ça ne serait possible que dans un contexte bien particulier.
