---
site: Next INpact
title: "Une membre de la mission Lescure nommée conseillère d'Aurélie Filippetti"
author: Xavier Berne
date: 2014-04-29
href: http://www.nextinpact.com/breve/87282-une-membre-mission-lescure-nommee-conseillere-daurelie-filippetti.htm
tags:
- HADOPI
- Institutions
- Neutralité du Net
---

> Juliette Mant, ancienne membre de la mission Lescure, vient d’être nommée «conseillère chargée des politiques numériques» au sein du cabinet de la ministre de la Culture, Aurélie Filippetti.
