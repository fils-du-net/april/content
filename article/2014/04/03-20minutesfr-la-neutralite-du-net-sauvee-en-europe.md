---
site: 20minutes.fr
title: "La neutralité du Net sauvée en Europe"
author: P.B.
date: 2014-04-03
href: http://www.20minutes.fr/web/1342377-20140403-neutralite-net-sauvee-europe
tags:
- Institutions
- Associations
- Neutralité du Net
- Europe
---

> Le Parlement européen a adopté un texte incluant des amendements défendant un Internet ouvert et sans discrimination du trafic, un pas capital avant d'autres échéances législatives...
