---
site: Developpez.com
title: "Adoption de l'open source: les entreprises plus attirées par la qualité et la sécurité"
author: Hinault Romaric
date: 2014-04-08
href: http://www.developpez.com/actu/69861/Adoption-de-l-open-source-les-entreprises-plus-attirees-par-la-qualite-et-la-securite-que-par-la-gratuite-selon-un-recent-sondage
tags:
- Entreprise
- Innovation
---

> L’open source occupe désormais une place non négligeable au sein de l’écosystème de l’IT. L’évolution de l’open source a créé une mutation de la vision de cet environnement par les entreprises.
