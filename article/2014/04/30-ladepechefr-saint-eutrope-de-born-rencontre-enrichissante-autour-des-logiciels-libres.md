---
site: LaDepeche.fr
title: "Saint-Eutrope-de-Born. Rencontre enrichissante autour des logiciels libres"
date: 2014-04-30
href: http://www.ladepeche.fr/article/2014/04/30/1873010-saint-eutrope-de-born-rencontre-enrichissante-autour-des-logiciels-libres.html
tags:
- Sensibilisation
- Associations
---

> Parallèlement au Printemps des poètes, s'est déroulée la journée du logiciel libre en collaboration avec l'association de bénévoles Agenux venus d'Agen et Villeneuve
