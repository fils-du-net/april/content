---
site: PC INpact
title: "Jérémie Zimmermann nommé à la Hadopi par Aurélie Filippetti"
author: Xavier Berne
date: 2014-04-01
href: http://www.pcinpact.com/news/86796-jeremie-zimmermann-nomme-a-hadopi-par-aurelie-filippetti.htm
tags:
- April
- HADOPI
- Institutions
- Associations
---

> Donnée sur le départ suite au remaniement ministériel annoncé hier par le chef de l’État, la ministre de la Culture vient de sortir de son chapeau un joli cadeau de départ. Par un décret signé hier à la hâte, Aurélie Filippetti a en effet nommé les trois membres manquants au collège de l’institution depuis maintenant plus de trois mois. Son choix s’est porté sur Jérémie Zimmermann, Richard Stallman et Maxime Rouquet (Parti Pirate). Explications.
