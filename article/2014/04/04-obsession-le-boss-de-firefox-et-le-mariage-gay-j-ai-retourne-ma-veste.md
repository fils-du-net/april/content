---
site: Obsession
title: "Le boss de Firefox et le mariage gay: j’ai retourné ma veste"
author: Paul Laubacher
date: 2014-04-04
href: http://rue89.nouvelobs.com/2014/04/04/boss-firefox-mariage-gay-jai-retourne-veste-251234
tags:
- Internet
- Associations
---

> Que penser du départ de Brendan Eich de Mozilla? Comme on vous l’expliquait ce vendredi matin, après une semaine de polémique à son sujet, le nouveau directeur général de la fondation a fini par démissionner.
