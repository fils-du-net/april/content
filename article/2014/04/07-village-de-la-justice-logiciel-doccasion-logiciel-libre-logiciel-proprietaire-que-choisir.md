---
site: Village de la Justice
title: "Logiciel d'occasion, logiciel libre, logiciel propriétaire, que choisir?"
author: Dominique Damo
date: 2014-04-07
href: http://www.village-justice.com/articles/Logiciel-occasion-logiciel-libre,16632.html
tags:
- Logiciels privateurs
- Administration
- Économie
---

> Un nouveau choix s’offre désormais à l’acheteur public informatique, toujours enclin à identifier des sources potentielles d’économies: l’achat de logiciels d’occasion!
