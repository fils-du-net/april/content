---
site: Numerama
title: "Numérique : des attributions élargies pour Axelle Lemaire"
author: Guillaume Champeau
date: 2014-04-30
href: http://www.numerama.com/magazine/29241-numerique-des-attributions-elargies-pour-axelle-lemaire.html
tags:
- Internet
- HADOPI
- Institutions
- Open Data
---

> La nouvelle secrétaire d'Etat au numérique, Axelle Lemaire, est non seulement chargée de s'intéresser aux aspects économiques du numérique, mais aussi aux problématiques de respect des droits fondamentaux et de sécurité des communications.
