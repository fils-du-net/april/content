---
site: 01net.
title: "Mozilla promet 10 000 dollars pour protéger Firefox d'un futur Heartbleed"
author: Pierre Fontaine
date: 2014-04-29
href: http://www.01net.com/editorial/618884/mozilla-promet-10-000-dollars-pour-proteger-firefox-d-un-futur-heartbleed
tags:
- Internet
- Associations
- Innovation
---

> Après la grave faille qui a frappé OpenSSL, Mozilla ouvre un programme spécial pour renforcer la sécurité de Firefox. Celui qui trouvera un bug ou une faille pourra empocher 10 000 dollars.
