---
site: lesoir.be
title: "Fin de XP: et si on boostait les licences libres?"
author: Patrice Leprince
date: 2014-04-10
href: http://www.lesoir.be/517551/article/actualite/regions/2014-04-10/fin-xp-et-si-on-boostait-licences-libres
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Marchés publics
- Standards
- International
---

> Alors que Windows XP tire sa révérence, le député écolo Alain Maron prône l’usage des systèmes d’exploitation et des logiciels libres. A Munich, les solutions open source ont permis d’économiser 10 millions d’euros.
