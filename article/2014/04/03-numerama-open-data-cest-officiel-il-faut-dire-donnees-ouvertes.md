---
site: Numerama
title: "Open Data: c'est officiel, il faut dire \"Données Ouvertes\""
author: Guillaume Champeau
date: 2014-04-03
href: http://www.numerama.com/magazine/29270-open-data-c-est-officiel-il-faut-dire-donnees-ouvertes.html
tags:
- Internet
- Administration
- Open Data
---

> La Commission générale de terminologie et de néologie impose désormais aux administrations de ne plus utiliser le terme "Open Data", mais "Données Ouvertes". Elle en profite pour en donner une définition.
