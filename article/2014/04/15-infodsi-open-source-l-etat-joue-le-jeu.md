---
site: infoDSI
title: "Open Source: l’Etat joue le jeu"
author: Damien Clochard
date: 2014-04-15
href: http://www.infodsi.com/articles/147820/open-source-etat-joue-jeu-damien-clochard-directeur-operations-dalibo.html
tags:
- Administration
- Économie
- Innovation
- Marchés publics
---

> La DISIC vient de publier la dernière version du SILL (Socle Interministériel de Logiciels Libres) émettant des recommandations sur l'usage des logiciels libres au sein des ministères. Force est de constater que depuis la circulaire Ayrault de 2012, le passage au logiciel libre s’est accéléré. Non seulement l’Etat adopte des solutions libres, mais il contribue à leur développement pour le bénéfice des communautés mais aussi des autres institutions : un cercle vertueux qui bénéficie aussi à l’écosystème des sociétés de service informatique.
