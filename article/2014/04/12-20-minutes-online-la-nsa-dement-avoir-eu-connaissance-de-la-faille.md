---
site: 20 minutes online
title: "La NSA dément avoir eu connaissance de la faille"
date: 2014-04-12
href: http://www.20min.ch/ro/news/monde/story/La-NSA-d-ment-avoir-eu-connaissance-de-la-faille-12332777
tags:
- Internet
- Administration
---

> La NSA a démenti vendredi avoir eu connaissance et exploité à son profit l importante faille de sécurité «Heartbleed». Celle-ci touche certaines versions d OpenSSL, un logiciel libre très utilisé pour les connexions sécurisées sur Internet.
