---
site: Le Huffington Post
title: "Quelles répercussions politiques pour la faille Heartbleed?"
author: Marc Thiriez
date: 2014-04-17
href: http://www.huffingtonpost.fr/marc-thiriez/quelles-repercussions-politiques-pour-la-faille-heartbleed-_b_5158431.html
tags:
- Internet
- Institutions
- Sciences
- Vie privée
---

> La faille de sécurité Heartbleed liée à la technologie OpenSSL va-t-elle prendre une dimension politique? Considérée déjà comme une des plus importantes failles de sécurité de l'histoire (deux serveurs sur trois seraient concernés selon The Verge), Bloomberg révélait que la NSA, au courant depuis deux ans de l'existence de la faille de sécurité, aurait gardé l'information sous silence en utilisant Heartbleed pour collecter des informations sensibles. D'autres États, aux intentions plus agressives que les États-Unis, ont pu faire de même
