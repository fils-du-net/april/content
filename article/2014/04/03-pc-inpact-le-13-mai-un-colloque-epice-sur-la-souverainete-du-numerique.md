---
site: PC INpact
title: "Le 13 mai, un colloque épicé sur la souveraineté du numérique"
author: Marc Rees
date: 2014-04-03
href: http://www.pcinpact.com/news/86830-le-13-mai-colloque-epice-sur-souverainete-numerique.htm
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
---

> Le 13 mai prochain, à la Maison de la Chimie, doit se tenir un colloque intitulé «Numérique: notre souveraineté est-elle menacée?». Organisé par l’agence Aromates, le rendez-vous doit être présidé par Laure de la Raudière. Plusieurs ministères ont été sollicités, comme celui de l’Intérieur, Bercy ou les affaires étrangères. C'est du moins ce qu'annonce un document «transmis à titre confidentiel» à plusieurs acteurs du numérique que nous avons pu consulter.
