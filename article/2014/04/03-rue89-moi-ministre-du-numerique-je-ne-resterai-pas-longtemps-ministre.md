---
site: Rue89
title: "Moi ministre du Numérique, je ne resterai pas longtemps ministre"
author: Xavier de La Porte
date: 2014-04-03
href: http://rue89.nouvelobs.com/2014/04/03/ministre-numerique-resterai-longtemps-ministre-251192
tags:
- Internet
- Administration
- Institutions
- Innovation
---

> Qu’adviendra-t-il du numérique dans ce nouveau gouvernement? Pour l’instant, plus de ministère délégué aux PME, à l’Innovation et à l’Economie numérique, comme il en existait un sous le gouvernement Ayrault; dans le gouvernement de Manuel Valls, le numérique est rattaché directement à l’économie, Arnaud Montebourg devenant ministre de l’Economie, du Redressement productif et du Numérique.
