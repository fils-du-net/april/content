---
site: Numerama
title: "Des graines open-source pour jardiner librement"
author: Guillaume Champeau
date: 2014-04-18
href: http://www.numerama.com/magazine/29144-des-graines-open-source-pour-jardiner-librement.html
tags:
- Institutions
- Licenses
- Europe
---

> Aux Etats-Unis, des universitaires ont conçu l'Open Source Seed Initiative, une organisation qui vise à distribuer des graines libres d'utilisation et de reproduction, sur le modèle des licences Creative Commons.
