---
site: Framablog
title: "Logiciels et matériels libres font le bonheur de l'éducation"
author: Jonathan Muckell (traduction framablog)
date: 2014-04-01
href: http://www.framablog.org/index.php/post/2014/04/01/logiciel-materiel-libre-education
tags:
- Matériel libre
- Éducation
- Innovation
- Licenses
---

> Le cours de Physical computing associe l’utilisation du matériel et du logiciel pour détecter et contrôler les interactions entre les utilisateurs et l’environnement. Elle peut repérer et répondre à des actions, par exemple en détectant la localisation des véhicules à une intersection et en ajustant le réglage des feux. Le domaine de l’informatique physique est relativement vaste, englobant des spécialités telles que la robotique, les microcontrôleurs, l’impression 3D et les vêtements intelligents.
