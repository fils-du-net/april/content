---
site: basta!
title: "«Les biens communs nous offrent davantage de liberté et de pouvoir que ne le font l’État et le marché»"
author: Olivier Petitjean
date: 2014-04-23
href: http://www.bastamag.net/Les-communs-nous-offrent-davantage
tags:
- Internet
- Économie
- Institutions
- Innovation
---

> Qu’y a-t-il de commun entre une coopérative, un potager partagé, un collectif de hackers ou une communauté autochtone gérant une forêt? Tous «agissent et coopèrent avec leurs pairs, de manière auto-organisée, pour satisfaire leurs besoins essentiels», explique David Bollier.
