---
site: PC INpact
title: "Œuvres orphelines, restrictions d’accès et œuvres libres: les réactions"
author: Marc Rees
date: 2014-04-14
href: http://www.pcinpact.com/news/86925-%C5%93uvres-orphelines-restrictions-d-acces-et-%C5%93uvres-libres-reactions.htm
tags:
- April
- Partage du savoir
- Institutions
- Associations
- DRM
- Droit d'auteur
- Licenses
- Europe
---

> Le Conseil supérieur de la propriété littéraire et artistique poursuit sa mission sur les œuvres orphelines. Et pour cause, d'ici la fin de l'année, la France devra transposer comme les autres pays membres une directive sur ce sujet. Au ministère de la Culture, le CSPLA a rédigé un questionnaire interne où il se demande s’il faut ou non prévoir des restrictions d'accès sur ces œuvres. Il soulève aussi la question des oeuvres libres. Next INpact a recueilli les réactions de l'Aful, l'April et SavoirCom1 sur ces deux thèmes.
