---
site: clubic.com
title: "Tails, le système qui voulait vous rendre vraiment anonyme"
author: Alexandre Laurent
date: 2014-04-30
href: http://www.clubic.com/antivirus-securite-informatique/virus-hacker-piratage/anonyme-internet/actualite-699478-tails-systeme-voulait-surfer-facon-anonyme.html
tags:
- Internet
- Vie privée
---

> Basé sur Debian, le système d'exploitation Tails réunit tous les outils nécessaires à ceux qui souhaitent pouvoir surfer de façon vraiment anonyme sur Internet. C'est lui qu'a utilisé Edward Snowden pour communiquer au reste du monde ses révélations relatives à la surveillance généralisée mise en place par la NSA.
