---
site: ZDNet
title: "Faille HeartBleed, une chance qu'OpenSSL soit un logiciel libre"
author: Christophe Auffray
date: 2014-04-11
href: http://www.zdnet.fr/actualites/faille-heartbleed-une-chance-qu-openssl-soit-un-logiciel-libre-39799835.htm
tags:
- Internet
- April
---

> Pour l’April, l’affaire HeartBleed ne doit pas déboucher sur une critique acerbe et une remise en cause du libre. Le fait que le code soit ouvert à la vigilance de tous reste une force. Vraiment? Mais à condition que cette vigilance s’exerce réellement et que des moyens soient attribués.
