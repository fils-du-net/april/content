---
site: RTVE.es
title: "Arranca la campaña para que los candidatos a las europeas firmen el Pacto por el Software Libre"
author: VICKY BOLA$authorNtilde;OS
date: 2014-04-23
href: http://www.rtve.es/noticias/20140423/arranca-campana-para-candidatos-elecciones-europeas-firmen-pacto-software-libre/925281.shtml
tags:
- April
- Institutions
- Associations
- Europe
---

> En España, Hispalinux insta a los candidatos a que suscriban el pacto. Varias asociaciones piden para las elecciones europeas que se favorezca el software libre. Buscan promover estos programas y estándares abiertos en las instituciones
