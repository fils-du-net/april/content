---
site: LeDevoir.com
title: "Les ministères échouent à un test de FACIL"
author: Fabien Deglise
date: 2014-04-29
href: http://www.ledevoir.com/politique/quebec/406801/acc
tags:
- Administration
- Partage du savoir
- Institutions
- Associations
- International
---

> La preuve par l’expérimentation. Au terme d’une demande massive d’accès à l’information détenue par une vingtaine de ministères, l’Association pour l’appropriation de l’informatique libre (FACIL) est catégorique: «l’État québécois» est loin d’être «ouvert» et, du coup, «il reste encore beaucoup de verrous à faire sauter pour libérer l’information publique», résume le groupe qui vient toute juste de diffuser, de manière ouverte, les fruits de son enquête.
