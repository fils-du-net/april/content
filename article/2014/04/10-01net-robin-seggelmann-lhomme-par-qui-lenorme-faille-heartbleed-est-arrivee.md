---
site: 01net.
title: "Robin Seggelmann, l'homme par qui l'énorme faille Heartbleed est arrivée"
author: Gilbert Kallenborn
date: 2014-04-10
href: http://www.01net.com/editorial/618006/robin-seggelmann-lhomme-par-qui-lenorme-faille-heartbleed-est-arrivee
tags:
- Internet
---

> Un développeur allemand est à l’origine de la faille de sécurité OpenSSL. Mais il assure ne pas l’avoir introduit de manière délibérée, et encore moins pour une agence de renseignement.
