---
site: Le Figaro.fr
title: "Numérique: Lemaire veut \"un Internet ouvert\""
date: 2014-04-18
href: http://www.lefigaro.fr/flash-eco/2014/04/18/97002-20140418FILWWW00401-numerique-lemaire-veut-un-internet-ouvert.php
tags:
- Internet
- Administration
- Institutions
- Neutralité du Net
- Standards
- Open Data
- Vie privée
---

> Dans un entretien au Point.fr, la nouvelle secrétaire d'état au Numérique Axelle Lemaire a détaillé pour la première fois la politique qu'elle entend mener au gouvernement.
