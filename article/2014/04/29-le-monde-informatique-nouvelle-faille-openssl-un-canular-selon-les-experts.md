---
site: Le Monde Informatique
title: "Nouvelle faille OpenSSL? Un canular selon les experts"
author: Jean Elyan
date: 2014-04-29
href: http://www.lemondeinformatique.fr/actualites/lire-nouvelle-faille-openssl-un-canular-selon-les-experts-57330.html
tags:
- Internet
- Innovation
---

> Des pirates prétendent avoir trouvé une nouvelle vulnérabilité dans la bibliothèque OpenSSL corrigée début avril et ils vendent leur information 2,5 bitcoins. Mais les experts en sécurité sont très dubitatifs.
