---
site: Le Point
title: "Axelle Lemaire veut \"rétablir un Internet qui garantit les libertés fondamentales\""
date: 2014-04-18
href: http://www.lepoint.fr/chroniqueurs-du-point/guerric-poncet/axelle-lemaire-veut-retablir-un-internet-qui-garantit-les-libertes-fondamentales-18-04-2014-1814411_506.php
tags:
- Internet
- Institutions
- Neutralité du Net
- Open Data
- Vie privée
---

> Entretien avec la nouvelle secrétaire d'État au Numérique, qui détaille pour la première fois la politique qu'elle entend mener au gouvernement.
