---
site: PC INpact
title: "Remaniement: Filippetti reste à la Culture, Montebourg chargé du Numérique"
author: Marc Rees
date: 2014-04-02
href: http://www.pcinpact.com/news/86822-remaniement-filippetti-reste-a-culture-montebourg-charge-numerique.htm
tags:
- HADOPI
- Institutions
---

> Après la claque des élections municipales, François Hollande a donc désigné Manuel Valls comme nouveau Premier ministre. La composition du nouveau gouvernement, qui compte seize ministres, vient d’être annoncée dans la cour de l’Élysée. Tour d’horizon sous le prisme du numérique.
