---
site: clubic.com
title: "Smartphone: Bruxelles veut en finir avec les disputes de brevets"
author: Guillaume Belfiore
date: 2014-04-29
href: http://www.clubic.com/smartphone/actualite-699346-smartphone-bruxelles-finir-disputes-brevets.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Standards
- Europe
---

> Face aux nombreuses actions en justice menées par les plus gros acteurs sur le secteur de la téléphonie, la Commission Européenne a décidé de limiter l'ampleur des prochains dépôts de plaintes.
