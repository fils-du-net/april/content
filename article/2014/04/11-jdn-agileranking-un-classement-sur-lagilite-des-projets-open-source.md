---
site: JDN
title: "AgileRanking: un classement sur l'agilité des projets Open Source"
author: Michael Muller
date: 2014-04-11
href: http://www.journaldunet.com/developpeur/expert/57032/agileranking---un-classement-sur-l-agilite-des-projets-open-source.shtml
tags:
- Standards
---

> AgileRanking est un classement mensuel réalisé à partir d’une sélection de projets open source hébergés sur la plateforme GitHub.
