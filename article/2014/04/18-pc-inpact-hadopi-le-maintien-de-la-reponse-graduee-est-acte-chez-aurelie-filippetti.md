---
site: PC INpact
title: "Hadopi: «le maintien de la réponse graduée est acté» chez Aurélie Filippetti"
author: Marc Rees
date: 2014-04-18
href: http://www.pcinpact.com/news/87130-aurelie-filippetti-esquisse-grandes-lignes-projet-loi-creation.htm
tags:
- Internet
- HADOPI
- Institutions
- Neutralité du Net
---

> Hier, aux rencontres européennes de l’Adami de Metz, Aurélie Filippetti a dévoilé les grandes lignes du projet de loi Création. La ministre de la Culture est restée floue sur le calendrier alors que le projet de loi est attendu de pied ferme par les acteurs du secteur.
