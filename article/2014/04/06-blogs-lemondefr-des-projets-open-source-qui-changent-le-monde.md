---
site: Blogs LeMonde.fr
title: "Des projets open-source qui changent le monde"
author: Anne-Sophie Novel
date: 2014-04-06
href: http://alternatives.blog.lemonde.fr/2014/04/06/ces-projets-open-source-qui-changent-le-monde
tags:
- Partage du savoir
- Matériel libre
- Associations
- Innovation
---

> Ouverture, partage, transparence, inclusion et autonomie font partie des maîtres-mots de l'Open-Week qui se déroule en Ile-de-France jusqu'au 11 avril. Une occasion rêvée pour revenir sur de multiples projets développés dans un état d'esprit et une culture libres et ouverts.
