---
site: 20minutes.fr
title: "Heartbleed: Le programmeur responsable du bug parle d'une erreur «triviale»"
date: 2014-04-11
href: http://www.20minutes.fr/high-tech/1349037-20140411-heartbleed-erreur-triviale-selon-programmeur-responsable-bug
tags:
- Internet
---

> La faille a fait paniquer tous les experts Web. Et l'étendue de ses conséquences ne sera sans doute pas connue avec un moment. Jeudi, Robin Seggelmann, le programmeur allemand responsable du bug Heartbleed, s'est confié au Sydney Morning Herald. Et s'il affirme avoir fait une erreur «triviale» et «malheureuse», il jure qu'elle était «involontaire».
