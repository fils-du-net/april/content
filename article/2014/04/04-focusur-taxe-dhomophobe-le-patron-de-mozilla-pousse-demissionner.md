---
site: FocuSur
title: "Taxé d'homophobe, le patron de Mozilla poussé à démissionner"
author: Pierre-Anthony Triollet
date: 2014-04-04
href: http://www.focusur.fr/20140404/brendan-eich-patron-mozilla-homophobe-demission
tags:
- Entreprise
- Internet
- Associations
---

> Mozilla est open source. Alors ça ne colle pas vraiment avec l’image de l’entreprise quand le patron n’est pas très «open». En effet, Brendan Eich, nommé à la tête de la société le 23 mars a vite été rattrapé par son passé. En 2008 il aurait effectué un versement de 1 000 dollars pour soutenir la Proposition 8, un référendum sur l’interdiction du mariage homosexuel en Californie.
