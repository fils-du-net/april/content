---
site: ZDNet
title: "Le logiciel libre est de meilleure qualité que le code propriétaire, estime Coverity"
date: 2014-04-17
href: http://www.zdnet.fr/actualites/le-logiciel-libre-est-de-meilleure-qualite-que-le-code-proprietaire-estime-coverity-39800103.htm
tags:
- Innovation
---

> Le rapport annuel de Coverity, qui scanne des centaines de logiciels open source, note l'amélioration de la vitesse de réparation des bugs dans Linux au fil des ans.
