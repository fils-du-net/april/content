---
site: Libération.fr
title: "La neutralité du Net est un spot de combat"
author: Erwan CARIO
date: 2014-04-03
href: http://ecrans.liberation.fr/ecrans/2014/04/03/la-neutralite-du-net-est-un-spot-de-combat_993034
tags:
- Entreprise
- Internet
- Administration
- Institutions
- Innovation
- Neutralité du Net
- Europe
---

> C’est une définition simple et concise: «Le principe de "neutralité de l’Internet" signifie que tout le trafic devrait être traité de la même manière, sans discrimination, restriction ou interférence, quels que soient l’émetteur, le récepteur, le type, le contenu, l’appareil, le service ou l’application.» Rien de bien original, puisque c’est ainsi qu’a été explicité en 2003, par Tim Wu, professeur à l’université Columbia, à New York, ce principe fondateur du réseau mondial.
