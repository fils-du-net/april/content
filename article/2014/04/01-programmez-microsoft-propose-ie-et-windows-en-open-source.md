---
site: Programmez!
title: "Microsoft propose IE et Windows en Open Source"
author: francoistonic
date: 2014-04-01
href: http://www.programmez.com/actualites/microsoft-propose-ie-et-windows-en-open-source-20435
tags:
- Entreprise
- Logiciels privateurs
---

> Décidément le nouveau patron de Microsoft veut tout changer. Non content d’avoir livré Office sur iPad, Microsoft adopte une nouvelle stratégie pour IE et Windows. L’éditeur proposera dès cet automne deux versions: une version communautaire Open Source et gratuite et une version professionnelle payante
