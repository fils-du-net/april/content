---
site: PC INpact
title: "Le site Internet du gouvernement passe à l’heure des Creative Commons"
author: Xavier Berne
date: 2014-04-04
href: http://www.pcinpact.com/news/86881-le-site-internet-gouvernement-passe-a-l-heure-creative-commons.htm
tags:
- Internet
- Administration
- Licenses
- Contenus libres
- International
- Open Data
---

> Depuis quelques jours, le site du gouvernement indique que les vidéos, textes et infographies de ce portail officiel se trouvent d’office sous licence Creative Commons. Une évolution récente qui n’est toutefois pas encore à la mesure de ce qui peut se faire à l’étranger, par exemple aux États-Unis ou en Angleterre. Explications.
