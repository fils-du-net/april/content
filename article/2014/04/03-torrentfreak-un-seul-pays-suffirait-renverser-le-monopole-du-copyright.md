---
site: Framablog
title: "Un seul pays suffirait à renverser le monopole du copyright"
author: Rick Falkvinge (traduction framablog)
date: 2014-04-03
href: http://www.framablog.org/index.php/post/2014/04/03/un-pays-monopole-copyright
tags:
- Partage du savoir
- Institutions
- Droit d'auteur
- International
---

> Des zones autonomes expérimentales mais légales sont en train d’émerger au Honduras et ailleurs dans le monde. C’est l’une des choses les plus passionnantes qui se soit développée depuis longtemps et cela annonce l’effondrement de la tyrannie qu’exerce l’industrie du copyright sur la culture et la connaissance.
