---
site: "Solutions-Logiciels"
title: "L'open source, vivier des systèmes embarqués critiques"
author: Cyrille Comar
date: 2014-01-23
href: http://www.solutions-logiciels.com/actualites.php?actu=14336
tags:
- Entreprise
- Économie
- Innovation
---

> Quels sont les enjeux du développement de l’open source dans la conception de logiciels et systèmes embarqués critiques et sur la révolution tant attendue par les différents acteurs du secteur?
