---
site: Sud Ouest
title: "Mont-de-Marsan: François Pellegrini, président d'Aquinetic, est nommé à la CNIL"
author: Jean-Louis Hugon
date: 2014-01-24
href: http://www.sudouest.fr/2014/01/24/mont-de-marsant-francois-pellegrini-president-d-aquinetic-est-nomme-a-la-cnil-1439916-3452.php
tags:
- Internet
- Institutions
- Sensibilisation
- Associations
- Vie privée
---

> Le président de l'association qui réunit les partisans du logiciel libre en Aquitaine a été nommé à la Commission nationale informatique et liberté par le président du Sénat Jean-Pierre Bel
