---
site: ZDNet
title: "FIC: Snowden partout, cybersécurité nulle part?"
author: Antoine Duvauchelle
date: 2014-01-22
href: http://www.zdnet.fr/actualites/fic-snowden-partout-cybersecurite-nulle-part-39797283.htm
tags:
- Internet
- Institutions
- Informatique-deloyale
- Vie privée
---

> Le lanceur d'alertes est partout, et a marqué les esprits. On pouvait s'y attendre, mais son influence est telle que la première conférence plénière du FIC s'est posé la question d'un échec de la cybersécurité.
