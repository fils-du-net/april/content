---
site: L'Express.fr
title: "L'open source, clef d'un monde plus équitable?"
author: Anne-Sophie Novel
date: 2014-01-13
href: http://www.lexpress.fr/emploi-carriere/emploi/l-open-source-clef-d-un-monde-plus-equitable_1310900.html
tags:
- Partage du savoir
- Associations
- Innovation
---

> Musique, voitures, maisons, avions... l'esprit du libre essaime aujourd'hui de l'univers du logiciel au monde réel. Bonne nouvelle: cela va de pair avec la durabilité. Focus sur cinq exemples à suivre.
