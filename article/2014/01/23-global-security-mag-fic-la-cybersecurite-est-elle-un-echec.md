---
site: Global Security Mag
title: "FIC: la cybersécurité est-elle un échec?"
author: Emmanuelle Lamandé
date: 2014-01-23
href: http://www.globalsecuritymag.fr/FIC-la-cybersecurite-est-elle-un,20140124,42524.html
tags:
- Institutions
- International
- Vie privée
---

> La cybersécurité, telle qu’elle est conçue aujourd’hui, doit-elle être considérée comme un échec, au regard de l’actualité malveillante toujours plus prégnante qui sévit sur la Toile? Doit-on pour autant laisser place au «cyber fatalisme» ? Faut-il repenser la façon dont la cybersécurité est abordée actuellement? Le Forum International de la Cybersécurité ouvre le débat à l’occasion d’une conférence plénière, animée par le journaliste Michel Picot.
