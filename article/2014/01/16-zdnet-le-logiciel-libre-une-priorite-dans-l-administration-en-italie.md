---
site: ZDNet
title: "Le logiciel libre une priorité dans l’administration… en Italie"
date: 2014-01-16
href: http://www.zdnet.fr/actualites/le-logiciel-libre-une-priorite-dans-l-administration-en-italie-39797125.htm
tags:
- Administration
- April
- Marchés publics
- Promotion
- International
---

> Modifier le code des marchés publics pour faire du logiciel libre la priorité, c’est possible, mais en Italie où le choix du logiciel libre est désormais la règle pour l'administration publique se félicite l’April.
