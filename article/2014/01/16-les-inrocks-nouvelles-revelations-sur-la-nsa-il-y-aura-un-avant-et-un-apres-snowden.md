---
site: Les Inrocks
title: "Nouvelles révélations sur la NSA: \"Il y aura un avant et un après Snowden\""
author: Carole Boinet
date: 2014-01-16
href: http://www.lesinrocks.com/2014/01/16/actualite/nsa-avant-et-apres-snowden-11461153
tags:
- Institutions
- Informatique-deloyale
- Vie privée
---

> Selon le “New York Times”, la NSA pirate des ordinateurs non connectés à Internet via un système d’ondes radio. Vous n’y comprenez rien ? Explications avec Jérémie Zimmermann de la Quadrature du Net.
