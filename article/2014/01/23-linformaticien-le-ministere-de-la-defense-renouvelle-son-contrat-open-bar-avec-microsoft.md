---
site: L'Informaticien
title: "Le ministère de la Défense renouvelle son contrat «Open Bar» avec Microsoft"
author: Emilien Ercolani
date: 2014-01-23
href: http://www.linformaticien.com/actualites/id/31798/le-ministere-de-la-defense-renouvelle-son-contrat-open-bar-avec-microsoft.aspx
tags:
- Logiciels privateurs
- Administration
- Économie
- Interopérabilité
- April
- Institutions
- Marchés publics
---

> Le contrat «Open Bar» entre le ministère de la Défense et Microsoft a été renouvelé pour la période 2013-2017, dans des conditions toujours aussi opaques.
