---
site: Numerama
title: "Le Pacte du Logiciel Libre relancé pour les municipales 2014"
author: Julien L.
date: 2014-01-23
href: http://www.numerama.com/magazine/27309-le-pacte-du-logiciel-libre-relance-pour-les-municipales-2014-maj.html
tags:
- April
- Institutions
- Sensibilisation
---

> L'April s'apprête à se saisir à nouveau de son bâton de pèlerin. En vue des municipales de 2014, l'association va une fois encore battre la campagne pour promouvoir le logiciel libre. En conséquence, elle commence à mobiliser ses troupes.
