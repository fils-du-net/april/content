---
site: L'Atelier
title: "Les data centers des géants technologiques gérés en Open Source"
author: Thomas Meyer
date: 2014-01-31
href: http://www.atelier.net/trends/articles/data-centers-geants-technologiques-geres-open-source_427170
tags:
- Entreprise
- Partage du savoir
- Innovation
- Informatique en nuage
---

> Initié en 2011 par Facebook, l’Open Compute Project fédère des leaders de l’informatique pour ouvrir et mutualiser la gestion des données sur le cloud.
