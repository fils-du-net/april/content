---
site: Numerama
title: "Espace Loggia se lance dans les meubles open-source"
author: Guillaume Champeau
date: 2014-01-07
href: http://www.numerama.com/magazine/27970-espace-loggia-se-lance-dans-les-meubles-open-source.html
tags:
- Entreprise
- Partage du savoir
- Matériel libre
---

> La société française Espace Loggia, spécialisée dans la conception de meubles personnalisés destinés au gain de place dans l'aménagement intérieur, a décidé de rejoindre un mouvement naissant de meubles open-source.
