---
site: Le Figaro
title: "La France inflige une amende record à Google"
date: 2014-01-09
href: http://www.lefigaro.fr/secteur/high-tech/2014/01/09/01007-20140109ARTFIG00310-la-france-inflige-une-amende-record-a-google.php
tags:
- Entreprise
- Internet
- Institutions
- International
- Vie privée
---

> La Commission nationale de l'informatique et des libertés (Cnil) en France a infligé une amende de 150.000 euros au géant informatique américain pour avoir refusé de mettre en conformité avec le droit français sa politique de confidentialité des données sur Internet.
