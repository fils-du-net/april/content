---
site: Le Monde Informatique
title: "Un jeu pour apprendre à programmer en ligne"
author: Serge Leblal
date: 2014-01-08
href: http://www.lemondeinformatique.fr/actualites/lire-un-jeu-pour-apprendre-a-programmer-en-ligne-56198.html
tags:
- Internet
- Éducation
- Innovation
---

> Les initiatives se multiplient pour encourager les plus jeunes à se lancer dans la programmation. Avec Code Combat, le travail est à la fois studieux et ludique.
