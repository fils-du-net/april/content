---
site: PC INpact
title: "Gouvernance du Net: le rapport du Conseil économique et social"
author: Xavier Berne
date: 2014-01-14
href: http://www.pcinpact.com/news/85276-gouvernance-net-rapport-conseil-economique-et-socia.htm
tags:
- Internet
- Institutions
- Neutralité du Net
- Promotion
- International
---

> Pendant plusieurs mois, le Conseil économique, social et environnemental s’est penché sur les enjeux relatifs à la gouvernance d’Internet, fréquemment questionnée ces dernières années. Alors que son rapport doit officiellement être présenté cet après-midi, PC INpact vous le livre dès à présent.
