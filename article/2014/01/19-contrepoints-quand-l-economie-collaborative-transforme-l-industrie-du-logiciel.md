---
site: Contrepoints
title: "Quand l’économie collaborative transforme l’industrie du logiciel"
author: Par Sylvain Le Bon
date: 2014-01-19
href: http://www.contrepoints.org/2014/01/19/153785-quand-leconomie-collaborative-transforme-lindustrie-du-logiciel
tags:
- Entreprise
- Internet
- Économie
---

> Le secteur du logiciel s’est jusqu’alors développé en imitant le modèle industriel, dominant dans la société qui l’a vu naître. Pourtant, grâce à la puissance des réseaux pair-à-pair distribués, des alternatives sont aujourd’hui possibles
