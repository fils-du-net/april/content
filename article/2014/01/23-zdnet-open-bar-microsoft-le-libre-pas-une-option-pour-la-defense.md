---
site: ZDNet
title: "Open Bar Microsoft: le libre pas une option pour la Défense"
author: Christophe Auffray
date: 2014-01-23
href: http://www.zdnet.fr/actualites/open-bar-microsoft-le-libre-pas-une-option-pour-la-defense-39797302.htm
tags:
- Administration
- Économie
- April
- Marchés publics
---

> Le ministère de la Défense a reconduit jusqu’en 2017 son contrat Open Bar avec Microsoft Irlande, pour un montant confidentiel, et couvrant au moins 200.000 postes. Le rejet du libre est justifié par son coût au minimum équivalent à celui de Microsoft.
