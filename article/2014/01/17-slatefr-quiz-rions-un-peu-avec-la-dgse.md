---
site: Slate.fr
title: "Quiz: rions un peu avec la DGSE"
author: Jean-Marc Manach
date: 2014-01-17
href: http://www.slate.fr/story/82001/quiz-dgse-marches-publics
tags:
- Internet
- Administration
- Marchés publics
- Vie privée
---

> Quelle agence de renseignements utilise des mails @laposte.net? Les stations d'écoute françaises sont-elles protégées par des clôtures en bois, en plastique ou en pierre? C'est fou ce qu'on apprend sur les services de renseignements français rien qu'avec leurs marchés publics, disponibles pour tous sur Internet.
