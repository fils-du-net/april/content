---
site: PC INpact
title: "Les députés socialistes tentés par une initiative sur les libertés numériques"
author: Xavier Berne
date: 2014-01-08
href: http://www.pcinpact.com/news/85273-les-deputes-socialistes-tentes-par-initiative-sur-libertes-numeriques.htm
tags:
- Institutions
- Vie privée
---

> Quelques semaines après l’adoption par le Parlement de la contestée «loi de programmation militaire», le chef de file du groupe socialiste à l’Assemblée nationale vient d’annoncer que les députés de la majorité allaient reprendre la main sur le sujet des libertés numériques. Si rien n’est encore officiellement acté, l’on évoque pour l’instant une possible mission d’information ou une éventuelle proposition de loi.
