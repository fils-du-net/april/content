---
site: Le Huffington Post
title: "Le logiciel libre, un instrument du redressement productif national."
author: Romain Blachier
date: 2014-01-31
href: http://www.huffingtonpost.fr/romain-blachier/logiciel-libre-redressement-productif_b_4685993.html
tags:
- Entreprise
- Administration
- Économie
- Sensibilisation
---

> Dans nos sociétés où les savoirs disponibles n'ont jamais été si nombreux, existe encore une confusion sur un champ immense, pourtant un domaine labouré par des centaines de millions d'individus lorsqu'ils ouvrent leur navigateur Firefox ou tapent un texte sur Open Office: celui du champ des logiciels libres.
