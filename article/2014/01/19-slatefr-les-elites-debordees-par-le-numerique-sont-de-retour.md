---
site: Slate.fr
title: "Les élites, débordées par le numérique, sont de retour"
author: Vincent Glad
date: 2014-01-19
href: http://www.slate.fr/story/82157/elites-debordees-numerique-reponses
tags:
- Internet
- Institutions
---

> C'est un article du Monde, et sa –fraîche– réception, qui marque deux inflexions dans le débat autour du web en France: la prise en compte par l’élite traditionnelle de son retard dans le domaine et la fin de la naïveté pro-technologique chez les penseurs d’Internet.
