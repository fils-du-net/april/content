---
site: infoDSI
title: "Libre en Fête 2014: découvrir le logiciel libre à l'arrivée du printemps"
date: 2014-01-15
href: http://www.infodsi.com/articles/145904/libre-fete-2014-decouvrir-logiciel-libre-arrivee-printemps.html
tags:
- April
- Associations
- Promotion
---

> Pour la quatorzième année consécutive, l'initiative Libre en Fête est relancée par l'April, en partenariat avec la Délégation aux usages de l'Internet, le réseau Cyber-base de la Caisse des dépôts et l'Agenda du Libre. Entre le vendredi 7 mars et le dimanche 13 avril 2014 inclus, dans une dynamique conviviale et festive, des événements auront lieu partout en France pour permettre au grand public de découvrir les logiciels libres.
