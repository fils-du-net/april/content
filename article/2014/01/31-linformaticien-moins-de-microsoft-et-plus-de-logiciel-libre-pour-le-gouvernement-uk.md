---
site: L'Informaticien
title: "Moins de Microsoft et plus de logiciel libre pour le gouvernement UK"
author: Margaux Duquesne
date: 2014-01-31
href: http://www.linformaticien.com/actualites/id/31907/moins-de-microsoft-et-plus-de-logiciel-libre-pour-le-gouvernement-uk.aspx
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Marchés publics
- International
---

> Fini le monopole des puissants: laissons un peu de place aux petites et moyennes entreprises du secteur IT. En plus, ce sera moins cher! C’est en somme le message qu’a lancé le gouvernement britannique, ce mercredi, en annonçant délaisser les solutions Microsoft pour se tourner vers l'open source.
