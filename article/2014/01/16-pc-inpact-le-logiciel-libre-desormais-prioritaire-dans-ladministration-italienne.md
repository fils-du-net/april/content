---
site: PC INpact
title: "Le logiciel libre désormais prioritaire dans l'administration italienne"
author: Nil Sanyas
date: 2014-01-16
href: http://www.pcinpact.com/news/85408-le-logiciel-libre-desormais-prioritaire-dans-administration-italienne.htm
tags:
- Logiciels privateurs
- Administration
- April
- Associations
- Marchés publics
- Promotion
- International
---

> Hier, l'April, la fameuse association de promotion et de défense du logiciel libre, s'est félicitée de voir l'administration italienne imposer en priorité le libre et d'utiliser des logiciels propriétaires qu'en cas d'ultime recours. Une situation légèrement différente de celle de la France.
