---
site: ICTjournal
title: "Le Canton de Berne se penchera davantage sur les projets open source"
author: Mélanie  Haab
date: 2014-01-29
href: http://www.ictjournal.ch/News/2014/01/29/Le-Canton-de-Berne-se-penchera-davantage-sur-les-projets-open-source.aspx
tags:
- Entreprise
- Économie
- Institutions
- International
---

> Le Grand Conseil bernois a accepté, hier, une motion exigeant un recours systématique aux logiciels open source. Le canton pose toutefois ses conditions.
