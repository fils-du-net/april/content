---
site: Mondialisation.ca
title: "Les États-Unis persistent et signent: Nous continuerons à espionner"
author: Chems Eddine Chitour
date: 2014-01-23
href: http://www.mondialisation.ca/les-etats-unis-persistent-et-signent-nous-continuerons-a-espionner/5365914
tags:
- Institutions
- Europe
- International
- Vie privée
---

> Le dernier discours d’Obama a fait l’objet d’une analyse par de nombreux pays et médias. Si les pays occidentaux amis peuvent être «rassurés qu’ils ne seront plus espionnés,» – ils ne peuvent pas faire autrement que d’y croire- les autres, tout les autres savent à quoi s’en tenir. Ils continueront à être espionner sans d’ailleurs savoir qu’ils sont espionnés. En fait, l’espionnage américain est sélectif. S’agissant des nations développées technologiquement, cela sera surtout un espionnage économique et technologique.
