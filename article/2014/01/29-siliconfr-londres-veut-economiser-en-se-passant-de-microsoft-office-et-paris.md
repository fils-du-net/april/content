---
site: Silicon.fr
title: "Londres veut économiser en se passant de Microsoft Office… Et Paris?"
author: Ariane Beky
date: 2014-01-29
href: http://www.silicon.fr/londres-economiser-microsoft-office-paris-92392.html
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Associations
- Marchés publics
- Standards
- International
---

> Bien décidé à réduire ses coûts informatiques, le gouvernement britannique veut mettre un terme à la domination d’un petit nombre d’acteurs IT, dont Microsoft, sur les marchés publics. Une migration massive vers des solutions Open Source est envisagée. En France, 16 mois après la circulaire Ayrault sur les logiciels libres, la situation évolue peu.
