---
site: l'Humanité.fr
title: "Cybersécurité: un forum aux mains des vendeurs de solutions"
author: Pi.M.
date: 2014-01-21
href: http://www.humanite.fr/politique/cybersecurite-un-forum-aux-mains-des-vendeurs-de-s-557458
tags:
- Administration
---

> Alors que s’ouvre à Lille le Forum international de la Cybersécurité (FIC), les industriels du secteur et les militaires se serrent et se frottent les mains. Dans l’entre soi, les langues se délient: il faut faire peur afin de vendre très cher des solutions de sécurité et de passer des lois liberticides.
