---
site: atlantico
title: "Cyber-espionnage de la NSA: le directeur technique de Mozilla défend les navigateurs open source"
date: 2014-01-15
href: http://www.atlantico.fr/pepites/cyber-espionnage-nsa-directeur-technique-mozilla-defend-navigateurs-open-source-955022.html
tags:
- Internet
- Institutions
- Promotion
- Vie privée
---

> Selon lui, son navigateur est difficile à saboter, du fait de son code source vérifiable à tout moment.
