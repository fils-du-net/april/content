---
site: PC INpact
title: "La MPAA rejoint le W3C en plein travail sur la standardisation des DRM"
author: Vincent Hermann
date: 2014-01-08
href: http://www.pcinpact.com/news/85278-la-mpaa-rejoint-w3c-en-plein-travail-sur-standardisation-drm.htm
tags:
- Entreprise
- Internet
- DRM
- Standards
---

> C’est une annonce inattendue mais pas forcément surprenante : la MPAA a indiqué avoir rejoint le W3C, le consortium qui travaille et définit les standards du web ou tout du moins des recommandations. Une arrivée qui fait craindre le pire alors même que le W3C travaille justement sur une standardisation des mesures de protection (DRM) pour le web.
