---
site: clubic.com
title: "Face à l'affaire PRISM, Mozilla rappelle l'importance d'un navigateur open source"
author: Guillaume Belfiore
date: 2014-01-14
href: http://www.clubic.com/navigateur-internet/mozilla-firefox/actualite-612316-face-affaire-prism-mozilla-rappelle-importance-navigateur-open-source.html
tags:
- Institutions
- Vie privée
---

> Le directeur technique de Mozilla revient sur la nature open source du navigateur Firefox et notamment au regard de l'affaire PRISM.
