---
site: L'informatique
title: "Linux: avantages et inconvénients d'un OS alternatif"
author: Victoria Manon
date: 2014-01-26
href: http://www.linformatique.org/linux-avantages-inconvenients-dun-os-alternatif
tags:
- Sensibilisation
---

> Alors qu’il est possible de payer pour un système d’exploitation comme Windows ou Mac OS, il est aussi possible d’opter pour une solution alternative gratuite: Linux.
