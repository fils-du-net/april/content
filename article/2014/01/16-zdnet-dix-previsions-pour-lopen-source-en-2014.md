---
site: ZDNet
title: "Dix prévisions pour l'open source en 2014"
author: Jack Wallen
date: 2014-01-16
href: http://www.zdnet.fr/actualites/dix-previsions-pour-l-open-source-en-2014-39797129.htm
tags:
- Entreprise
- Promotion
- Informatique en nuage
---

> 2013 a été une année chargée pour l'open source, marquée par de nombreux hauts, mais aussi par quelques bas. Cependant, Linux (qui continue de tirer parti de sa base solide) devrait connaître sa meilleure année en 2014.
