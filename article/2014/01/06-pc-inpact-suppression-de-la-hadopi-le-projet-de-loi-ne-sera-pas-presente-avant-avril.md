---
site: PC INpact
title: "Suppression de la Hadopi: le projet de loi ne sera pas présenté avant avril"
author: Xavier Berne
date: 2014-01-06
href: http://www.pcinpact.com/news/85197-suppression-hadopi-projet-loi-ne-sera-pas-presente-avant-avri.htm
tags:
- Internet
- HADOPI
- Institutions
---

> Le projet de loi d’Aurélie Filippetti visant à transférer les compétences de la Hadopi vers le CSA tarde encore à pointer le bout de son nez... En effet, le gouvernement vient de reconnaître implicitement qu’il ne présenterait pas ce texte au Parlement avant le mois d’avril au plus tôt. En revanche, plusieurs textes importants devraient être examinés - voire votés - d’ici là: le projet de loi sur la consommation, celui pour l’égalité femmes-hommes de Najat Vallaud-Belkacem, ou bien encore la proposition de loi déposée au Sénat à propos de la contrefaçon.
