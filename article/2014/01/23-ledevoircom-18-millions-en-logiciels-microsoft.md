---
site: LeDevoir.com
title: "18 millions en logiciels Microsoft"
author: Fabien Deglise
date: 2014-01-23
href: http://www.ledevoir.com/politique/quebec/397985/18-millions-en-logiciels-microsoft
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Marchés publics
- International
---

> Québec contrevient à ses propres lois en matière d’ouverture aux logiciels libres
