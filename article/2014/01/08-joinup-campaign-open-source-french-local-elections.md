---
site: Joinup
title: "Campaign on open source French local elections"
author: Gijs Hillenius
date: 2014-01-08
href: https://joinup.ec.europa.eu/community/osor/news/campaign-open-source-french-local-elections
tags:
- Administration
- April
- Institutions
- Promotion
- English
---

> (l'April a relancé sa campagne pour informer les politiciens du pays sur ce type de solution)) April, France's free software advocacy group, has relaunched its campaign to make the country's politicians aware of this type of ICT solution, aiming to gather support statements from candidates for the municipal elections of 23 and 30 March. The group want politicians to defend the rights of developers and users of free software. They also hope to encourage public administrations to use, create and distribute such software.
