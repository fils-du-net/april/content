---
site: Afriquinfos
title: "Réduire la fracture numérique: Téléphones portables et médias sociaux"
author: Pauline Guillaud (traduite par Pauline Guillaud)
date: 2014-01-30
href: http://www.afriquinfos.com/articles/2014/1/30/reduire-fracture-numerique-telephones-portables-medias-sociaux-243224.asp
tags:
- Internet
- Partage du savoir
- Sensibilisation
- International
---

> Les experts de technologie ainsi que les activistes essaient depuis des années de combler le fossé de l'accès à la technologie, utilisant pour cela des produits et des initiatives innovantes, comme l'ordinateur portable à 100 dollars.
