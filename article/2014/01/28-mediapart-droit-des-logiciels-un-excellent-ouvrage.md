---
site: Mediapart
title: "Droit des logiciels: un excellent ouvrage"
author: Bruno Courcelle
date: 2014-01-28
href: http://blogs.mediapart.fr/blog/bruno-courcelle/280114/droit-des-logiciels-un-excellent-ouvrage
tags:
- Sensibilisation
- Brevets logiciels
- Droit d'auteur
---

> Depuis plus de 15 ans F. Pellegrini milite pour les logiciels libres et S. Canevet s'intéresse aux aspects juridiques de l'informatique en matière de liberté d'expression sur Internet et de droit des logiciels. Leur livre expose de façon précise et documentée les subtilités juridiques du droit d'auteur et de son adaptation aux logiciels, ainsi que celles des brevets.
