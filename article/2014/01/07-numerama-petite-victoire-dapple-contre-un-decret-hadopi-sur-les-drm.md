---
site: Numerama
title: "Petite victoire d'Apple contre un décret Hadopi sur les DRM"
author: Guillaume Champeau
date: 2014-01-07
href: http://www.numerama.com/magazine/27972-petite-victoire-d-apple-contre-un-decret-hadopi-sur-les-drm.html
tags:
- Entreprise
- Internet
- Interopérabilité
- HADOPI
- DRM
---

> Le Conseil d'Etat a rejeté l'essentiel des arguments formulés par Apple pour faire tomber les pouvoirs de régulation des DRM et des mesures techniques de protection confiés par la loi à la Hadopi. Il a simplement autorisé que des recours soient exercés contre le refus éventuel de classer confidentielles les informations transmises à la Haute Autorité.
