---
site: Mediapart
title: "Initiation à l’informatique ou conditionnement..."
author: Bruno Menan
date: 2014-01-04
href: http://blogs.mediapart.fr/blog/bruno-menan/040114/initiation-l-informatique-ou-conditionnement
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Éducation
- Promotion
---

> La ville d’Angers (mais ce n'est hélas pas la seule!) annonce fièrement sur son site internet qu’elle a commencé à acquérir des tablettes iPad pour en équiper progressivement toutes ses écoles. Si l’on en croit les auteurs de l’article, c’est le choix d’un outil moderne, fiable, pérenne et incontournable. Et si au contraire il s’agissait là d’une erreur monumentale et fondamentale quant à la place de l’informatique à l’école?
