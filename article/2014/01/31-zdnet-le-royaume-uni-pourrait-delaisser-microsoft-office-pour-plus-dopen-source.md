---
site: ZDNet
title: "Le Royaume-Uni pourrait délaisser Microsoft Office pour plus d'Open Source"
date: 2014-01-31
href: http://www.zdnet.fr/actualites/le-royaume-uni-pourrait-delaisser-microsoft-office-pour-plus-d-open-source-39797526.htm
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Marchés publics
- Standards
- International
---

> Le gouvernement britannique veut briser l’oligopole des fournisseurs IT en ouvrant plus la commande publique aux PME et en standardisant les formats de fichiers au travers d’une migration de la bureautique Office.
