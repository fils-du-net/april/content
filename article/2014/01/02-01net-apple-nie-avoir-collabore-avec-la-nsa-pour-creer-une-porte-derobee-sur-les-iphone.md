---
site: 01net.
title: "Apple nie avoir collaboré avec la NSA pour créer une porte dérobée sur les iPhone"
author: Gilbert Kallenborn
date: 2014-01-02
href: http://www.01net.com/editorial/611108/apple-nie-avoir-collabore-avec-la-nsa-pour-creer-une-porte-derobee-sur-les-iphone
tags:
- Entreprise
- Logiciels privateurs
- Informatique-deloyale
- Vie privée
---

> La firme californienne souligne qu’elle n’a jamais eu connaissance du programme d’intrusion révélé par le magazine Der Spiegel. L’activiste Jacob Applebaum, en revanche, croit à un certain niveau de coopération.
