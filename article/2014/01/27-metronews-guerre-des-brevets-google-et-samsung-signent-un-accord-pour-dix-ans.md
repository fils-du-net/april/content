---
site: metronews
title: "Guerre des brevets: Google et Samsung signent un accord pour dix ans"
author: Jean-Sébastien Zanchi
date: 2014-01-27
href: http://www.metronews.fr/high-tech/guerre-des-brevets-google-et-samsung-signent-un-accord-pour-dix-ans/mnaA!yeeFFr8PVxkvU
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Innovation
---

> Déjà partenaires via Android, les deux géants de l'électronique ont entériné un accord de licence sur leur brevets respectifs pour les dix prochaines années.
