---
site: PC INpact
title: "Cours universitaires en ligne: l’exécutif donne un coup d’accélérateur"
author: Xavier Berne
date: 2014-01-15
href: http://www.pcinpact.com/news/85362-cours-universitaires-en-ligne-l-executif-donne-coup-d-accelerateur.htm
tags:
- Internet
- Partage du savoir
- Institutions
- Accessibilité
---

> À partir de demain, les internautes du monde entier pourront profiter gratuitement des cours mis à disposition sur la plateforme gouvernementale «France Université Numérique», et ce quel que soit leur statut. Le ministère de l’Enseignement supérieur a pour l’occasion annoncé qu’une trentaine de nouveaux cours seraient proposés dans le courant de l’année 2014, et qu’un coup de pouce financier de 8 millions d’euros supplémentaires serait alloué par l’exécutif pour la production de davantage de «MOOCs».
