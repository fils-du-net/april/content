---
site: Miroir Mag
title: "Six clichés qui vous empêchent de vous mettre au logiciel libre"
author: Nicolas Boeuf
date: 2014-01-23
href: http://www.miroir-mag.fr/13674-six-cliches-qui-vous-empeche-de-vous-mettre-au-logiciel-libre
tags:
- Administration
- Économie
- Associations
- Promotion
---

> Encore mal connus, les logiciels libres continuent de faire peur aux utilisateurs. Doutes sur la fiabilité, nécessité de compétences techniques, habitude des autres systèmes… Autant de freins à leur généralisation. Pourtant, ces logiciels que chacun est libre d’utiliser, modifier ou partager pourraient amener de nombreux bienfaits, s’ils étaient utilisés de manière globale.
