---
site: Contrepoints
title: "GnuPG: 16 Ans de protection de la vie privée"
date: 2014-01-03
href: http://www.contrepoints.org/2014/01/03/152096-gnupg-16-ans-de-protection-de-la-vie-privee
tags:
- Économie
- Informatique-deloyale
- Innovation
- Vie privée
---

> GnuPG, célèbre logiciel libre de chiffrement des messages a fêté ses 16 ans le 20 décembre dernier.
