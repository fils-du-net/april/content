---
site: Decideo.fr
title: "Analytique: pour gagner plus, choisissez l’open source!"
author: Philippe Nieuwbourg
date: 2014-01-19
href: http://www.decideo.fr/Analytique-pour-gagner-plus-choisissez-l-open-source-_a6692.html
tags:
- Entreprise
- Économie
- Promotion
---

> Oui, c’est la conclusion étonnante à laquelle est arrivée l’équipe de O’Reilly, qui mène à l’occasion de la conférence Strata aux Etats-Unis, une enquête sur les salaires et les outils utilisés dans le domaine de l’analyse de données. Les informaticiens spécialistes de l’analyse de données sont mieux rémunérés s’ils utilisent des outils open source que des outils commerciaux.
