---
site: ZDNet
title: "Nicolas Colin et François Pellegrini nommés à la CNIL"
author: Antoine Duvauchelle
date: 2014-01-23
href: http://www.zdnet.fr/actualites/nicolas-colin-et-francois-pellegrini-nommes-a-la-cnil-39797319.htm
tags:
- Internet
- Institutions
- Associations
- Brevets logiciels
- Vote électronique
- Europe
- Vie privée
---

> Les deux nouvelles personnalités qualifiées doivent apporter connaissances techniques et juridiques à la CNIL, et ne devraient pas détonner dans l'institution.
