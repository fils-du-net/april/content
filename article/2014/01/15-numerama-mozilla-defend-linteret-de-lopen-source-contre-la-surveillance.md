---
site: Numerama
title: "Mozilla défend l'intérêt de l'open source contre la surveillance"
author: Julien L.
date: 2014-01-15
href: http://www.numerama.com/magazine/28067-mozilla-defend-l-interet-de-l-open-source-contre-la-surveillance.html
tags:
- Internet
- Promotion
- Vie privée
---

> Ce week-end, le directeur technique de la fondation Mozilla a publié un billet de blog pour défendre l'intérêt de l'open source contre la surveillance de masse sur Internet. Cela concerne les navigateurs web, mais pas seulement.
