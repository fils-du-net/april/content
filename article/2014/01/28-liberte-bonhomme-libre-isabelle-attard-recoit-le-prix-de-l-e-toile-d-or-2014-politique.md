---
site: Liberté Bonhomme Libre
title: "Isabelle Attard reçoit le prix de l’E-Toile d’Or 2014 «politique»"
author: Murielle Bouchard
date: 2014-01-28
href: http://www.libertebonhomme.fr/2014/01/28/isabelle-attard-recoit-le-prix-de-l%e2%80%99e-toile-d%e2%80%99or-2014-%c2%ab-politique-%c2%bb
tags:
- Institutions
- Sensibilisation
- Éducation
---

> La 10e cérémonie des Vœux de l’Internet a eu lieu aujourd’hui. La députée Nouvelle Donne du Calvados était nominée dans la catégorie politique.
