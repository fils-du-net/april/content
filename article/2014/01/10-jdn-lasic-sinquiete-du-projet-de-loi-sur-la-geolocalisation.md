---
site: JDN
title: "L'ASIC s'inquiète du projet de loi sur la géolocalisation"
author: Aude Fredouelle
date: 2014-01-10
href: http://www.journaldunet.com/ebusiness/le-net/asic-geolocalisation-0114.shtml
tags:
- Internet
- Institutions
- Associations
- Vie privée
---

> L'association demande aux Sénateurs de limiter le champ d'application du texte qui prévoit la possibilité de géolocaliser tout objet pour lutter contre la criminalité.
