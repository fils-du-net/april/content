---
site: Framablog
title: "Programme d'informatique dès l'école primaire?"
author: Cyrille Largillier
date: 2014-01-15
href: http://www.framablog.org/index.php/post/2014/01/15/informatique-ecole-primaire
tags:
- Éducation
---

> La France a fait le choix depuis de nombreuses années de considérer l’informatique à l’école et jusqu’au collège, uniquement à travers ses usages via le B2I. L’Éducation nationale perçoit le numérique comme un outil utile aux autres apprentissages.
