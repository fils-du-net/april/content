---
site: basta!
title: "Numérique: cette empreinte écologique que les consommateurs ont bien du mal à voir"
author: Nolwenn Weiler
date: 2014-01-20
href: http://www.bastamag.net/Numerique-cette-empreinte
tags:
- Internet
- Économie
- Innovation
---

> L’économie virtuelle consomme une énergie bien réelle. Les «data centers», qui regroupent les serveurs indispensables à la navigation sur le Web et à la circulation des 300 milliards de courriels, pourriels, photos ou vidéos envoyés quotidiennement, peuvent consommer autant d’énergie qu’une ville de 200 000 habitants. Sans compter le coût environnemental de la fabrication d’équipements toujours plus nombreux. De quoi relativiser l’apport apparemment écologique de l’économie dématérialisée, avec ses «télé-réunions», son commerce en ligne ou ses téléchargements.
