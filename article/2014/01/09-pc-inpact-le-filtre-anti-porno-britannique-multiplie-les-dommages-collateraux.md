---
site: PC INpact
title: "Le filtre anti-porno britannique multiplie les dommages collatéraux"
author: Nil Sanyas
date: 2014-01-09
href: http://www.pcinpact.com/news/85297-le-filtre-anti-porno-britannique-multiplie-dommages-collateraux.htm
tags:
- Internet
- Partage du savoir
- Institutions
- Informatique-deloyale
- International
---

> Déployé depuis quelques semaines, le filtre bloquant les sites pornographiques au Royaume-Uni fait régulièrement la une outre-Manche. Non pas pour ses résultats bénéfiques, mais pour ses effets secondaires. De nombreux sites n'ayant rien à voir avec la pornographie ont ainsi été touchés par le filtre mis en place par les opérateurs britanniques, dont des sites de fichiers BitTorrent, des pages officielles de distribution Linux, des sites de films indépendants ou encore notre confrère TorrentFreak.
