---
site: Obsession
title: "Une attaque informatique est une agression comme une autre"
author: Boris Manenti
date: 2014-01-18
href: http://obsession.nouvelobs.com/hacker-ouvert/20140122.OBS3338/une-attaque-informatique-est-une-agression-comme-une-autre.html
tags:
- Logiciels privateurs
- Administration
- Marchés publics
---

> Pourquoi cette indépendance n'a-t-elle pas été appliquée au ministère de la Défense qui a préféré renouveler son contrat avec Microsoft plutôt que d'opter pour des logiciels libres?
