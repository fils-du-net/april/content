---
site: "cio-online.com"
title: "Jacques Marzin (DISIC): «Nous gardons une approche raisonnée en matière de logiciels libres»"
author: Bertrand Lemaire
date: 2014-10-09
href: http://www.cio-online.com/actualites/lire--7114.html
tags:
- Administration
- Marchés publics
- RGI
- Standards
---

> Jacques Marzin, directeur de la Disic (Direction interministérielle des systèmes d'information et de communication) et ainsi «DSI groupe» de l'Etat, détaille la stratégie pragmatique qu'il défend en matière de logiciels libres. Cette prise de position intervient à environ deux semaines de l'Open CIO Summit et de l'Open World Forum.
