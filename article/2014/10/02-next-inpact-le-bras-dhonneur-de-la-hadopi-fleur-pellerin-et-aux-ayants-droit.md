---
site: Next INpact
title: "Le bras d'honneur de la Hadopi à Fleur Pellerin et aux ayants droit"
author: Marc Rees
date: 2014-10-02
href: http://www.nextinpact.com/news/90231-le-bras-dhonneur-hadopi-a-fleur-pellerin-et-aux-ayants-droit.htm
tags:
- HADOPI
- Institutions
---

> La Hadopi vient de modifier son budget prévisionnel pour le dernier trimestre 2014. Alors que Fleur Pellerin veut réduire sa voilure budgétaire afin que la Rue du Texel se concentre sur la riposte graduée, celle-ci lui a adressé un joli pied-de-nez.
