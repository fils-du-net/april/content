---
site: Numerama
title: "Andrus Ansip: \"la neutralité du net devrait être un droit\""
author: Guillaume Champeau
date: 2014-10-16
href: http://www.numerama.com/magazine/30964-andrus-ansip-34la-neutralite-du-net-devrait-etre-un-droit34.html
tags:
- Internet
- Institutions
- Droit d'auteur
- Neutralité du Net
- Europe
---

> Opération séduction pour Andrus Ansip, le futur commissaire européen chargé du marché unique numérique, qui travaillera en lien avec Günther Oettinger, chargé de l'économie et de la société numérique (voir notre article du 10 septembre 2014). Alors que ce dernier a envoyé des signes très inquiétants, le premier continue d'enregistrer des bons points.
