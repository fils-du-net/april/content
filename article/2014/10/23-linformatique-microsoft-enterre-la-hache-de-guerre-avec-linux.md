---
site: L'informatique
title: "Microsoft enterre la hache de guerre avec Linux"
author: Emilie Dubois
date: 2014-10-23
href: http://www.linformatique.org/microsoft-enterre-hache-guerre-linux
tags:
- Entreprise
- Logiciels privateurs
---

> En déclarant «Microsoft aime Linux» le discours de Satya Nadella détonne par rapport à celui de Steve Ballmer, il y a quelques années, qui avait déclaré que Linux était «un cancer».
