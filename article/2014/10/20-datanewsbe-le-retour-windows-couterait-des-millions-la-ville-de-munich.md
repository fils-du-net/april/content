---
site: Datanews.be
title: "Le retour à Windows coûterait des millions à la ville de Munich"
author: Pieterjan Van Leemputten
date: 2014-10-20
href: http://datanews.levif.be/ict/actualite/le-retour-a-windows-couterait-des-millions-a-la-ville-de-munich/article-normal-317265.html
tags:
- Logiciels privateurs
- Administration
- Marchés publics
---

> Si Munich en revenait effectivement à Windows au bout de dix ans, cela lui coûterait des millions d'euros supplémentaires.
