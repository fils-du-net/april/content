---
site: Next INpact
title: "Hadopi: le député Tardy s’inquiète de la stratégie du gouvernement"
author: Xavier Berne
date: 2014-10-03
href: http://www.nextinpact.com/news/90247-hadopi-depute-tardy-s-inquiete-strategie-gouvernement.htm
tags:
- Internet
- HADOPI
- Institutions
- Sciences
---

> Le gouvernement peut-il contraindre budgétairement la Hadopi à se concentrer sur la seule riposte graduée, au détriment des autres missions qui lui ont pourtant été confiées par le législateur en 2009? C’est la crainte actuelle du député UMP Lionel Tardy, qui interpelle aujourd’hui le ministère de la Culture sur ce qui pourrait constituer selon lui un «contournement du Parlement».
