---
site: Next INpact
title: "Une petite «révolution numérique» se dessine à l'Assemblée nationale"
author: Xavier Berne
date: 2014-10-09
href: http://www.nextinpact.com/news/90319-une-petite-revolution-numerique-se-dessine-a-l-assemblee-nationale.htm
tags:
- Internet
- Institutions
- Open Data
---

> L’Assemblée nationale devrait mettre en place avant la fin de l’année prochaine un programme d'« innovation fellows » qui permettra à des personnes rémunérées de circuler librement au sein des services parlementaires, et ce afin de formuler des recommandations visant à améliorer la diffusion du numérique au Palais Bourbon. C’est en tout cas ce que vient d'annoncer le président de l’assemblée, Claude Bartolone, qui a également indiqué que des consultations en ligne auraient lieu à titre expérimental sur différents projets de loi.
