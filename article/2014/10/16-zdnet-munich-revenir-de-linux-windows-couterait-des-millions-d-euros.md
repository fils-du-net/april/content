---
site: ZDNet
title: "Munich: revenir de Linux à Windows coûterait des millions d’euros"
date: 2014-10-16
href: http://www.zdnet.fr/actualites/munich-revenir-de-linux-a-windows-couterait-des-millions-d-euros-39807869.htm
tags:
- Administration
- Économie
- International
---

> Munich rebasculera-t-il vers Windows? Pour le maire de la ville, le seul fait de passer de Linux à Windows 7 couterait déjà 3,15 millions d’€ en achats de nouveaux PC. En ajoutant les licences Windows, ce sera encore des millions en plus, et un gâchis des 14 millions d’€ de LiMux.
