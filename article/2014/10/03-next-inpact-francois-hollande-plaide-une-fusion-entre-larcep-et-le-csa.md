---
site: Next INpact
title: "François Hollande plaide une fusion entre l'ARCEP et le CSA"
author: Marc Rees
date: 2014-10-03
href: http://www.nextinpact.com/news/90243-francois-hollande-plaide-fusion-entre-arcep-et-csa.htm
tags:
- Internet
- HADOPI
- Institutions
---

> Jeudi, depuis un séminaire organisé par le CSA portant sur «l’audiovisuel, enjeu économique», François Hollande a plaidé pour un rapprochement des compétences du CSA et de l'ARCEP. Il a également annoncé toute une série de mesures, en fait autant de ponts d’or pour les ayants droit de l’audiovisuel.
