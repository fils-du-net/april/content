---
site: JDN
title: "Florent Zara (Open World Forum): \"L'open source permet de reprendre le contrôle de l'innovation numérique\""
author: Antoine Crochet- Damais
date: 2014-10-20
href: http://www.journaldunet.com/solutions/dsi/florent-zara-open-world-forum-2014.shtml
tags:
- Entreprise
- Économie
- Innovation
- Open Data
---

> Open Data, Big Data, Internet des objets... L'Open World Forum 2014 se tiendra sous le signe de l'innovation. Le point sur le programme avec son président.
