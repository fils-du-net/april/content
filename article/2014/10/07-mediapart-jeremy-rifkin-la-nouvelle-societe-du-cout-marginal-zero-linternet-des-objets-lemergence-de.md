---
site: Mediapart
title: "Jeremy Rifkin. La nouvelle société du coût marginal zéro. L'internet des objets, l'émergence des communaux collaboratifs..."
author: Jean-Paul Baquiast
date: 2014-10-07
href: http://blogs.mediapart.fr/blog/jean-paul-baquiast/071014/jeremy-rifkin-la-nouvelle-societe-du-cout-marginal-zero-linternet-des-objets-lemergence-des
tags:
- Entreprise
- Internet
- Économie
---

> Sous ce long titre "La nouvelle société du coût marginal zéro. L'internet des objets, l'émergence des communaux collaboratifs et l'éclipse du capitalisme" (24 septembre 2014) le dernier livre de Jeremy Rifkin rassemble un certain nombre d'idées intéressantes, qui avaient été pour la plupart présentées dans ses précédents ouvrages.
