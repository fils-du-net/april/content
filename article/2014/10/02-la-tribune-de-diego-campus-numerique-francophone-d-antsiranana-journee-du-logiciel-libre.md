---
site: La Tribune de Diego
title: "Campus Numérique Francophone d’Antsiranana: journée du logiciel libre"
date: 2014-10-02
href: http://latribune.cyber-diego.com/societe/1401-campus-numerique-francophone-dantsiranana--journee-du-logiciel-libre.html
tags:
- Éducation
- Promotion
- International
---

> La journée du logiciel libre est une manifestation mondiale annuelle instaurée en 2004 dans le but d'initier le public au logiciel libre à l'échelle mondiale, par son utilisation personnelle, dans l'éducation, l'économie ou par les gouvernements. Cette journée est l'occasion de militer pour la reconnaissance de la dimension sociale du logiciel libre. Elle était organisée à Antsiranana par les membres du campus numérique francophone d'Antsiranana
