---
site: Numerama
title: "Plus de 50 % des brevets de Google et Microsoft seraient invalides"
author: Guillaume Champeau
date: 2014-10-02
href: http://www.numerama.com/magazine/30780-plus-de-50-des-brevets-de-google-et-microsoft-seraient-invalides.html
tags:
- Entreprise
- Brevets logiciels
- International
---

> Selon un cabinet spécialisé en propriété intellectuelle, un récent jugement de la Cour Suprême des Etats-Unis pourrait rendre invalide plus de la moitié des brevets détenus par les géants Google et Microsoft.
