---
site: Next INpact
title: "Des composants libres sur 81 % des ordinateurs du ministère de la Défense"
author: Xavier Berne
date: 2014-10-10
href: http://www.nextinpact.com/news/90360-des-composants-libres-sur-81-ordinateurs-ministere-defense.htm
tags:
- Administration
- Économie
---

> Suite à une étude menée en interne, le ministère de la Défense vient d’expliquer à la députée Isabelle Attard que 81 % de ses postes de travail étaient déployés avec des composants libres. Mais en l’état, cette affirmation est malheureusement bien vague pour être réellement évocatrice, aucun nom de logiciel n'étant par ailleurs mis en avant par la «Grande Muette».
