---
site: CNRS Le Journal
title: "Jean Tirole, Prix Nobel d’économie"
author: Matthieu Ravaud
date: 2014-10-13
href: https://lejournal.cnrs.fr/articles/jean-tirole-prix-nobel-deconomie
tags:
- Entreprise
- Économie
- Sciences
---

> Pour faire simple, notre économiste a développé des notions théoriques très importantes (lire notre article "Les jeux de l'économie selon Jean Tirole") qu’il a ensuite appliquées à une multitude de problèmes concrets. Son domaine de prédilection? La nouvelle économie industrielle, dont il est le père fondateur. Auteur en 1988 de l’ouvrage de référence en la matière, connu comme «Le Tirole» dans le monde entier, il a travaillé, entre autres, sur l’économie des logiciels libres, les regroupements de brevets, ou encore les ententes tacites entre entreprises.
