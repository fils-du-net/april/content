---
site: ZDNet
title: "Open World Forum: Quatre fantastiques de l'open source"
author: Guillaume Serries
date: 2014-10-31
href: http://www.zdnet.fr/actualites/open-world-forum-quatre-fantastiques-de-l-open-source-39808785.htm
tags:
- Entreprise
- Économie
- DRM
- Innovation
- Promotion
- Open Data
---

> Ce sont des combattants de l'open source, des militants de la cause. Logiciels libres, open data, creative commons, géolocalisation; ils portent des projets qui font grincer quelques dents, et se retouvent à l'Open World Forum, sous la bannière du bien commun. Portraits.
