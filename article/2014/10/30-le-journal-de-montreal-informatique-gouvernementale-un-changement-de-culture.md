---
site: Le Journal de Montréal
title: "Informatique gouvernementale: un changement de culture"
author: Pierre Bouchard
date: 2014-10-30
href: http://www.journaldemontreal.com/2014/10/30/informatique-gouvernementale-un-changement-de-culture
tags:
- Administration
- Institutions
- International
---

> L’État est un immense paquebot qui ne se manœuvre pas aussi facilement qu’une motomarine. Mais ce n’est pas une raison pour ne pas donner un sérieux coup de barre lorsqu’il est nécessaire de le faire. C’est pourquoi l’annonce de la démission du Dirigeant principal de l’information (DPI) offre au gouvernement une occasion parfaite pour opérer un changement de culture informatique au sein de l’État.
