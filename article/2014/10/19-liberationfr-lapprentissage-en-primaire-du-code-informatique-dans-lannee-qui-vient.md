---
site: Libération.fr
title: "L'apprentissage en primaire du code informatique «dans l'année qui vient»"
author: Hugo Pascual
date: 2014-10-19
href: http://www.liberation.fr/societe/2014/10/19/l-apprentissage-en-primaire-du-code-informatique-dans-l-annee-qui-vient_1122642
tags:
- Institutions
- Éducation
---

> A l'occasion de la «code week», la ministre de l'Education, Najat Vallaud-Belkacem, et la secrétaire d’Etat chargée du numérique, Axelle Lemaire, ont visité une session d'initiation à la programmation pour enfants.
