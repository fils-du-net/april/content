---
site: Mediapart
title: "Maîtriser ses données personnelles: Degooglisons l'internet (Framasoft)"
author: Dominique C
date: 2014-10-16
href: http://blogs.mediapart.fr/blog/dominique-c/161014/maitriser-ses-donnees-personnelles-degooglisons-linternet-framasoft
tags:
- Entreprise
- Internet
- Associations
- Promotion
- Vie privée
---

> Connaissez-vous l'association Framasoft?  Née officiellement en 2001, elle fait partie de ces groupes de bénévoles qui oeuvrent à propulser le "Logiciel Libre" hors de la zone de confidentialité où le maintiennent à la fois les grands "ténors" du numérique (Kro$oft, la Pomme, FB, Gogol et Cie)... et l'ignorance du grand public.
