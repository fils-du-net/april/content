---
site: Next INpact
title: "En appel, Microsoft accuse une start-up d'avoir cassé les secrets de Skype"
author: Marc Rees
date: 2014-10-08
href: http://www.nextinpact.com/news/90312-en-appel-microsoft-accuse-start-up-davoir-casse-secrets-skype.htm
tags:
- Entreprise
- Interopérabilité
- Institutions
---

> Une affaire a été plaidée ce matin devant la Cour d’appel de Caen. 5h30 d’audience opposant une PME à Microsoft, avec à la clef le périmètre du droit de décompilation du logiciel Skype. Jusqu’où peut-on plonger dans les algorithmes et le code source d’une telle solution?
