---
site: Numerama
title: "Microsoft clame son amour pour Linux"
author: Julien L.
date: 2014-10-21
href: http://www.numerama.com/magazine/31005-microsoft-clame-son-amour-pour-linux.html
tags:
- Entreprise
- Logiciels privateurs
- Informatique en nuage
---

> Lors d'une conférence sur Windows Azure, Microsoft a affiché son affection pour Linux, tranchant avec les vieilles controverses qui ont émaillé les relations entre la firme de Redmond et la communauté du logiciel libre. Mais cette déclaration n'est pas innocente: elle s'inscrit dans une évolution de fond de l'informatique grand public.
