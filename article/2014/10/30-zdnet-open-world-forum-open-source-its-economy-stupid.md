---
site: ZDNet
title: "Open World Forum: Open Source, \"It's the economy stupid!\""
author: Guillaume Serries
date: 2014-10-30
href: http://www.zdnet.fr/actualites/open-world-forum-open-source-it-s-the-economy-stupid-39808747.htm
tags:
- Entreprise
- Économie
- Innovation
- Open Data
---

> Sécurité et ouverture. L'équilibre fragile sur lequel chemine l'écosystème de l'open source français s'arrête pour deux jours à Paris, à l'Open World Forum. L'occasion de faire le point sur une économie en croissance.
