---
site: Libération.fr
title: "«Dégooglisons Internet»: ils sont fous ces libristes!"
author: Camille Gévaudan
date: 2014-10-16
href: http://ecrans.liberation.fr/ecrans/2014/10/16/degooglisons-internet-ils-sont-fous-ces-libristes_1123040
tags:
- Entreprise
- Internet
- Associations
- Promotion
---

> L'association Framasoft veut proposer une alternative «ouverte et respectueuse des internautes» à chaque service-star de Google, Microsoft et consorts.
