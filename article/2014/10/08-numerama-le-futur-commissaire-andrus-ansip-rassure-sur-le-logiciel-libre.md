---
site: Numerama
title: "Le futur commissaire Andrus Ansip rassure sur le logiciel libre"
author: Julien L.
date: 2014-10-08
href: http://www.numerama.com/magazine/30851-le-futur-commissaire-andrus-ansip-rassure-sur-le-logiciel-libre.html
tags:
- April
- Institutions
- Europe
- ACTA
---

> Lors de son audition devant les parlementaires européens, le futur commissaire en charge du marché unique numérique, Andrus Ansip, s'est montré favorable au logiciel libre. Mais l'ancien premier ministre estonien devra faire ses preuves, car son soutien au projet d'accord ACTA n'a pas été oublié.
