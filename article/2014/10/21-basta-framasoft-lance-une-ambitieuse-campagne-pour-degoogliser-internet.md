---
site: Basta!
title: "Framasoft lance une ambitieuse campagne pour dégoogliser Internet"
author: Rédaction
date: 2014-10-21
href: http://www.bastamag.net/Framasoft-lance-une-ambitieuse
tags:
- Entreprise
- Internet
- Associations
- International
- English
- Vie privée
---

> Framsoft s’attaque aux géants de la Silicon Valley. L’association qui promeut logiciel et culture libres vient de lancer une grande campagne de dons afin de poursuivre le développement de ses outils. Objectif: proposer des services alternatifs aux applications de Google (et d’autres), qui «menacent nos vies numériques». Basta! relaie son appel.
