---
site: Le Monde.fr
title: "Shellshock, la faille de sécurité majeure découverte «presque par hasard» par un Français"
author: Yves Eudes
date: 2014-10-02
href: http://www.lemonde.fr/pixels/article/2014/10/02/shellshock-la-faille-de-securite-majeure-decouverte-presque-par-hasard-par-un-francais_4498904_4408996.html
tags:
- Internet
---

> Les failles de sécurité informatiques sont un problème courant, mais la dernière en date, baptisée «Shellshock», est loin d'être anodine. Rendue publique le 26 septembre, elle a été trouvée dans le programme Bash, une pièce maîtresse de Unix, lui-même à la base du système d'exploitation libre et gratuit GNU/Linux, qui équipe près de 70 % des serveurs Internet dans le monde. Plus précisément, Bash est un shell, une interface qui traduit les lignes de commande tapées sur le clavier en un langage compréhensible par le système d'exploitation.
