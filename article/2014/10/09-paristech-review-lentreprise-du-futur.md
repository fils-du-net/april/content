---
site: ParisTech Review
title: "L'entreprise du futur"
date: 2014-10-09
href: http://www.paristechreview.com/2014/10/09/entreprise-nouveaux-talents
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Innovation
---

> À l’avenir, l’entreprise va continuer d’être profondément bouleversée par la hausse des matières premières, la concurrence des pays émergents, l’obsession de la réduction des coûts et les innovations technologiques. Son défi: faire plus avec moins, dans tous les domaines. Le travail subira naturellement les contrecoups de ces bouleversements. Toutefois, il y a, pour les salariés ambitieux, une bonne nouvelle.
