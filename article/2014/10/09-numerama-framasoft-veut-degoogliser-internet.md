---
site: Numerama
title: "Framasoft veut \"dégoogliser\" Internet"
author: Julien L.
date: 2014-10-09
href: http://www.numerama.com/magazine/30861-framasoft-veut-degoogliser-internet.html
tags:
- Internet
- Associations
- Promotion
- Vie privée
---

> L'association Framasoft a mis sur un pied un nouveau projet visant à "dégoogliser" Internet, qui consiste à encourager les utilisateurs à migrer vers des solutions libres, éthiques, décentralisées et solidaires. Et quoi de mieux, pour faire passer ce message, que de s'inspirer des aventures d'Astérix et Obélix, que tout le monde connaît?
