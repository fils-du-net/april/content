---
site: Next INpact
title: "Terrorisme: après le blocage administratif, le déréférencement administratif"
author: Marc Rees
date: 2014-10-16
href: http://www.nextinpact.com/news/90448-terrorisme-apres-blocage-administratif-dereferencement-administratif.htm
tags:
- Internet
- Institutions
---

> Hier, les sénateurs ont débuté l’examen du projet de loi sur le terrorisme. En pleine séance, le gouvernement a déposé un amendement de dernière minute sur l’article 9, celui relatif au blocage administratif. Son objet? Initier le déréférencement administratif des sites terroristes ou pédopornographiques.
