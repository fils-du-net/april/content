---
site: L'Expansion
title: "Benoît Thieulin: \"C'est aux citoyens de décider dans quelle société numérique ils veulent vivre\""
author: Raphaële Karayan
date: 2014-10-03
href: http://lexpansion.lexpress.fr/high-tech/benoit-thieulin-c-est-aux-citoyens-de-decider-dans-quelle-societe-numerique-ils-veulent-vivre_1607761.html
tags:
- Internet
- HADOPI
- Institutions
- Éducation
- Open Data
---

> La concertation nationale sur le numérique, lancée samedi par le Conseil national du numérique (CNNum), doit être le prélude au projet de loi numérique d'Axelle Lemaire et faire remonter les contributions de la société civile. Le président du CNNum détaille la démarche.
