---
site: Numerama
title: "Concertation sur le numérique: un bon début de démocratie participative"
author: Guillaume Champeau
date: 2014-10-06
href: http://www.numerama.com/magazine/30819-concertation-sur-le-numerique-un-bon-debut-de-democratie-participative.html
tags:
- Internet
- Institutions
---

> Avec la concertation sur le numérique ouverte samedi à la demande de Manuel Valls, le Conseil National du Numérique montre la voie de ce que devrait être la démocratie participative, en utilisant au mieux les outils modernes. Mais si la direction est bonne, il est encore bien trop tôt pour dire que le chemin est accompli.
