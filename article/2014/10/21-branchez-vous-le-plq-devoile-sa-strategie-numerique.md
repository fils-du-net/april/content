---
site: "Branchez-Vous!"
title: "Le PLQ dévoile sa stratégie numérique"
author: Nil Sanyas
date: 2014-10-21
href: http://branchez-vous.com/2014/10/21/le-plq-devoile-sa-strategie-numerique
tags:
- Internet
- Administration
- Institutions
- Sensibilisation
- Accessibilité
- Promotion
- Open Data
---

> Ce samedi à Trois-Rivières, le premier ministre Philippe Couillard a ouvert le Conseil général 2014 du Parti libéral du Québec.
