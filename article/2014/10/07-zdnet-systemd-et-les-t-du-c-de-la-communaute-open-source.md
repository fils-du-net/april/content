---
site: ZDNet
title: "Systemd et les «t**** du c**» de la communauté Open Source"
author: Christophe Auffray
date: 2014-10-07
href: http://www.zdnet.fr/actualites/systemd-et-les-t-du-c-de-la-communaute-open-source-39807395.htm
tags:
- Associations
---

> On ne peut pas plaire à tout le monde. C’est ce que constate le co-créateur de systemd, Lennart Poettering qui se dit sur Google+ victime d’une véritable campagne haineuse et d’appels à la violence, notamment au sein du premier cercle de Linus Torvalds.
