---
site: Regards Citoyens
title: "Projet de loi Données publiques: une transposition ambitieuse est encore possible pour l'Open Data!"
date: 2014-10-01
href: http://www.regardscitoyens.org/projet-de-loi-donnees-publiques-une-transposition-ambitieuse-est-encore-possible-pour-lopen-data
tags:
- Administration
- Institutions
- Europe
- Open Data
---

> Éclipsée par la communication du gouvernement sur sa consultation numérique, l’information est passée totalement inaperçue de beaucoup: le Parlement se penche en ce moment même sur la transposition de la directive européenne relative aux données publiques. Ce texte, discuté mardi 6 octobre en hémicycle à l’Assemblée, est l’occasion parfaite pour faire avancer concrètement l’Open Data: comme l’a rappelé le Conseil d’État dans son avis au Gouvernement, «la directive […] établit des règles […] fixées comme des plafonds ou des objectifs minimas».
