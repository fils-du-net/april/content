---
site: Slate.fr
title: "Les services municipaux de Munich songent à abandonner Linux"
author: Annabelle Georgen
date: 2014-10-26
href: http://www.slate.fr/story/93905/linux
tags:
- Logiciels privateurs
- Administration
- Marchés publics
---

> Après avoir surpris le monde entier il y a dix ans en abandonnant Windows pour passer sous Linux, la mairie de Munich envisage maintenant de faire marche arrière, rapporte l'hebdomadaire Der Spiegel, citant une dépêche de l'agence de presse DPA. En cause: les problèmes techniques récurrents dont se plaint le personnel administratif utilisant le célèbre logiciel open source.
