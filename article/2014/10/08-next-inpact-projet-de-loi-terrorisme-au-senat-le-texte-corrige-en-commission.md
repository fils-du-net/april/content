---
site: Next INpact
title: "Projet de loi Terrorisme: au Sénat, le texte corrigé en commission"
author: Marc Rees
date: 2014-10-08
href: http://www.nextinpact.com/news/90305-projet-loi-terrorisme-tour-dhorizon-premiers-amendements-au-senat.htm
tags:
- Internet
- Institutions
---

> Le projet de loi sur le terrorisme poursuit son cheminement parlementaire. Déjà adopté par l’Assemblée nationale (voir notre long compte-rendu), il attend désormais son examen en séance au Sénat. Avant cela, il doit passer en Commission des lois où les premiers amendements ont été déposés par les co-rapporteurs. Tour d’horizon.
