---
site: Direction Informatique
title: "Le logiciel libre, une priorité du gouvernement Couillard?"
author: Laurent Bounin
date: 2014-10-24
href: http://www.directioninformatique.com/blogue/le-logiciel-libre-une-priorite-du-gouvernement-couillard/31248
tags:
- Administration
- Économie
- Interopérabilité
- Institutions
- Marchés publics
- Standards
- International
---

> Le samedi 18 octobre 2014 a eu lieu à Trois-Rivières le Conseil général 2014 du Parti libéral du Québec. Le plan du numérique y a été largement abordé et le premier ministre du Québec, Philippe Couillard, a indiqué plusieurs mesures mettant en vedette le logiciel libre.
