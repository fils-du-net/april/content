---
site: France Info
title: "Crise à la Hadopi"
author: Jérôme Colombain
date: 2014-10-13
href: http://www.franceinfo.fr/emission/nouveau-monde/2014-2015/crise-la-hadopi-13-10-2014-06-50
tags:
- HADOPI
---

> La présidente de la Haute autorité contre le piratage rencontre aujourd'hui la ministre de la Culture Fleur Pellerin alors que la Hadopi s'oppose aux ayants-droits du cinéma.
