---
site: 01 BUsiness
title: "L'open source modernise en profondeur le SI de l'Etat"
author: Par Alain Merle, DISIC, Directeur du programme « Transformation des centres informatiques »
date: 2014-10-31
href: http://pro.01net.com/editorial/630062/l-open-source-modernise-en-profondeur-le-si-de-letat
tags:
- Administration
- Standards
- Informatique en nuage
---

> L’open source infuse de plus en plus les couches d’infrastructures de l’Administration. Ce mouvement est doublement bénéfique: il limite l’adhérence aux grandes technologies propriétaires et laisse libre court à l’expérimentation.
