---
site: JDN
title: "L'open source désormais au cœur des systèmes industriels stratégiques"
author: Antoine Crochet- Damais
date: 2014-10-31
href: http://www.journaldunet.com/solutions/dsi/open-cio-summit-2014.shtml
tags:
- Entreprise
- Administration
- Innovation
- Informatique en nuage
---

> Open CIO Summit 2014 Le JDN a pu assister à l'Open CIO Summit 2014. Organisé dans le cadre de l'Open World Forum, cet évènement pour DSI à huis clos a permis de découvrir des projets open source de haut vol.
