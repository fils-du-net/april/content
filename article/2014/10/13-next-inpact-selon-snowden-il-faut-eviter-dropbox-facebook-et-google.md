---
site: Next INpact
title: "Selon Snowden, il faut éviter Dropbox, Facebook et Google"
author: Vincent Hermann
date: 2014-10-13
href: http://www.nextinpact.com/news/90379-selon-snowden-il-faut-eviter-dropbox-facebook-et-google.htm
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> Dans le cadre du New York Film Festival, qui a débuté vendredi soir, Edward Snowden était interviewé par la journaliste Jane Meyer. Le lanceur d’alertes, à l’origine d’un très grand nombre de fuites sur les activités de surveillance et d’espionnage aux États-Unis, a répondu sur certains développements de son action. Mais il a surtout appelé à se méfier encore une fois de certains outils répandus, notamment Dropbox.
