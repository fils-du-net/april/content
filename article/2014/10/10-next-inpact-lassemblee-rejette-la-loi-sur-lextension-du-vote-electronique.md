---
site: Next INpact
title: "L'Assemblée rejette la loi sur l'extension du vote électronique"
author: Xavier Berne
date: 2014-10-10
href: http://www.nextinpact.com/news/90353-l-assemblee-rejette-loi-sur-l-extension-vote-electronique.htm
tags:
- Internet
- Institutions
- Vote électronique
---

> Après d’âpres débats, l’Assemblée nationale a rejeté hier une proposition de loi UMP visant à permettre aux Français de l’étranger de voter par Internet à l’occasion des élections présidentielles et européennes. Le texte s’est confronté à l’opposition du gouvernement et des députés socialistes.
