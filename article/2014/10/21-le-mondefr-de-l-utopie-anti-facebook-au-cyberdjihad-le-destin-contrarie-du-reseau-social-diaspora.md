---
site: Le Monde.fr
title: "De l’utopie anti-Facebook au cyberdjihad, le destin contrarié du réseau social Diaspora"
author: Martin Untersinger
date: 2014-10-21
href: http://www.lemonde.fr/pixels/article/2014/10/21/de-l-utopie-anti-facebook-au-cyberdjihad-le-destin-contrarie-du-reseau-social-diaspora_4504282_4408996.html
tags:
- Internet
- Associations
- Innovation
- Sciences
- Vie privée
---

> Le réseau axé sur les droits des utilisateurs a connu une histoire mouvementée.
