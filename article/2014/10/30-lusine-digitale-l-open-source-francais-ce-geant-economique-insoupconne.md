---
site: L'Usine Digitale
title: "L’open source français, ce géant économique insoupçonné"
author: Florent Zara
date: 2014-10-30
href: http://www.usine-digitale.fr/editorial/l-open-source-francais-ce-geant-economique-insoupconne.N294399
tags:
- Entreprise
- Économie
- Innovation
---

> Avec un peu moins de 4 milliards d'euros de chiffre d'affaires annuel au compteur en France, l'open source a plus de poids qu'on ne l'imagine. Florent Zara, président de l'Open world forum, un congrès réunissant à Paris les 30 et 31 octobre les acteurs du secteur, décrit pour L'Usine Digitale ce géant économique insoupçonné.
