---
site: Reporterre
title: "Contre l'obsolescence informatique, vivent les logiciels libres!"
author: Camille Lecomte
date: 2014-10-23
href: http://www.reporterre.net/spip.php?article6472
tags:
- Entreprise
- Logiciels privateurs
- April
- Associations
- Promotion
---

> En avril dernier, Microsoft a mis fin au support Windows XP, entraînant la fin prématurée de 500 millions d’ordinateurs qui en étaient équipés à travers le monde. Windows pense-t-il par cette décision nous imposer ses nouvelles machines? C’est sans compter sur les logiciels libres!
