---
site: Rue89
title: "Internet terrorise les députés. Tous? Non..."
author: Camille Polloni
date: 2014-10-23
href: http://rue89.nouvelobs.com/2014/10/23/a-reuni-les-trois-deputes-cyberoptimistes-255632
tags:
- Internet
- Institutions
- Associations
- Vie privée
---

> Ils sont déçus, mais prêts à recommencer quand il faudra (et ça ne manquera pas d’arriver). Pendant les débats sur la dernière loi antiterroriste, adoptée en commission mixte paritaire mardi, l’Assemblée nationale s’est transformée en théâtre de leur défaite.
