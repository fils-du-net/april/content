---
site: RTBF Info
title: "La ville de Liège bazarde ses logiciels libres"
author: Michel Gretry
date: 2014-10-07
href: http://www.rtbf.be/info/regions/liege/detail_la-ville-de-liege-bazarde-ses-logiciels-libres?id=8372046
tags:
- Logiciels privateurs
- Administration
- Interopérabilité
---

> Le conseil communal de Liège, ce lundi soir, a entériné l'achat de licences Microsoft; une décision qui peut paraître anodine, mais dont la portée symbolique n'échappe à personne: c'est la fin des logiciels libres dans l'administration de la cité ardente. L'oposition de gauche l'a amèrement déploré.
