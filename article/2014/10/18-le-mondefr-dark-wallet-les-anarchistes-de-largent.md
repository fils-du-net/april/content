---
site: Le Monde.fr
title: "Dark Wallet, les anarchistes de l'argent"
author: Yves Eudes
date: 2014-10-18
href: http://www.lemonde.fr/pixels/visuel/2014/10/18/dark-wallet-les-anarchistes-de-l-argent_4508228_4408996.html
tags:
- Internet
- Économie
- Associations
- Innovation
---

> Un groupe de techno-militants vivant dans des squats veut construire sur Internet un nouveau système monétaire libre et anonyme.
