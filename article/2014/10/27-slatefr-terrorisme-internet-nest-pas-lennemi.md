---
site: Slate.fr
title: "Terrorisme: Internet n'est pas l'ennemi"
author: Jillian C. York
date: 2014-10-27
href: http://www.slate.fr/story/93911/terrorisme-internet-ennemi
tags:
- Internet
- Institutions
- International
---

> Pourquoi les initiatives internationales visant à stopper l’organisation État islamique ne doivent pas s'opérer aux dépens des libertés numériques.
