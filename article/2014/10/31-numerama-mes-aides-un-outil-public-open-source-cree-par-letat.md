---
site: Numerama
title: "Mes-Aides, un outil public open-source créé par l'Etat"
author: Julien L.
date: 2014-10-31
href: http://www.numerama.com/magazine/31145-mes-aides-un-outil-public-open-source-cree-par-l-etat.html
tags:
- Internet
- Administration
- Innovation
- Open Data
---

> Dans le cadre de l'action de l'État visant à simplifier l'accès à l'administration et modernisation l'action publique, un service Mes-Aides permet de calculer les aides dont les individus peuvent bénéficier. Particularité, le service met à disposition son code source pour faciliter les contributions extérieures.
