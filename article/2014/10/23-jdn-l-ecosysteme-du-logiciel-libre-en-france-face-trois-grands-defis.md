---
site: JDN
title: "L’écosystème du logiciel libre en France face à trois grands défis"
author: Stéfane Fermigier
date: 2014-10-23
href: http://www.journaldunet.com/solutions/expert/58876/l-ecosysteme-du-logiciel-libre-en-france-face-a-trois-grands-defis.shtml
tags:
- Entreprise
- Économie
- Innovation
- Promotion
---

> C’est en se regroupant au travers de structures (Clusters, Pôles de Compétitivité, etc) favorisant l'innovation ouverte, l'accès au marché, et la formation aux technologies de demain, que l’écosystème du logiciel libre en France pourra bénéficier des meilleures opportunités de création de valeur.
