---
site: Metamedia
title: "Abolir la neutralité du Net, c’est revenir au Minitel"
author: Eric Scherer
date: 2014-10-09
href: http://meta-media.fr/2014/10/09/la-fin-de-la-neutralite-du-net-cest-le-retour-au-minitel.html
tags:
- Internet
- Institutions
- Associations
- Innovation
- Neutralité du Net
- Europe
---

> La fin de la neutralité du Net nous ferait retourner à l’époque ultra-contrôlée du Minitel, et poserait d’importants risques à notre future croissance économique, a averti jeudi à Budapest, l’expert français Bernard Benhamou.
