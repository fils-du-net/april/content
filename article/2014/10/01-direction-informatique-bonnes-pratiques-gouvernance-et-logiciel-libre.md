---
site: Direction Informatique
title: "Bonnes pratiques, gouvernance et logiciel libre"
author: Jonathan Le Lous
date: 2014-10-01
href: http://www.directioninformatique.com/blogue/bonnes-pratiques-gouvernance-et-logiciel-libre/30699
tags:
- Entreprise
- Innovation
- Vie privée
---

> Dans mes deux précédents billets, j’ai discuté de la notion de gouvernance informatique et de ses implications stratégiques. Le choix des technologies, entre autres, repose sur plusieurs éléments: l’application est-elle compatible avec l’existant? Quelles sont les évolutions qui sont envisagées dans le futur – infonuagique, intégration continue, agilité? La solution sera-t-elle compatible avec ces évolutions et à quels coûts?
