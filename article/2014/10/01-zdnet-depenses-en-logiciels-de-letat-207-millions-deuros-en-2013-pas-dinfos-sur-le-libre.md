---
site: ZDNet
title: "Dépenses en logiciels de l'Etat: 207 millions d'euros en 2013, pas d'infos sur le libre"
author: Louis Adam
date: 2014-10-01
href: http://www.zdnet.fr/actualites/depenses-en-logiciels-de-l-etat-207-millions-d-euros-en-2013-pas-d-infos-sur-le-libre-39807151.htm
tags:
- Administration
- Économie
- Institutions
---

> La députée Isabelle Attard a posé il y a quelques mois une question au gouvernement afin d’obtenir les chiffres précis des dépenses ministérielles en matière de logiciels. De chiffres précis nulle trace, mais la réponse du gouvernement éclaire un peu les pratiques déployées.
