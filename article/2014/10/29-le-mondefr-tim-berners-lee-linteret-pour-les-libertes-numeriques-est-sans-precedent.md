---
site: Le Monde.fr
title: "Tim Berners-Lee: «L'intérêt pour les libertés numériques est sans précédent»"
author: Damien Leloup
date: 2014-10-29
href: http://www.lemonde.fr/pixels/article/2014/10/29/tim-berners-lee-l-interet-pour-les-libertes-numeriques-est-sans-precedent_4513902_4408996.html
tags:
- Internet
- DRM
- Standards
- Vie privée
---

> Le World Wide Web Consortium (W3C), l'organisme chargé de fixer les standards du Web et de son principal langage, le HTML, fête ses 20 ans mercredi 29 octobre. Tim Berners-Lee, fondateur du W3C et principal inventeur du Web, revient sur les évolutions passées et à venir du réseau.
