---
site: Silicon.fr
title: "Linux: Linus Torvalds justifie son franc-parler face à la communauté"
author: Ariane Beky
date: 2014-10-17
href: http://www.silicon.fr/linux-linus-torvalds-erreurs-communaute-99623.html
tags:
- Économie
- Innovation
---

> Le créateur du noyau Linux admet la violence d’échanges au sein de la communauté de développeurs, mais ne regrette aucune décision prise en 23 années de développement.
