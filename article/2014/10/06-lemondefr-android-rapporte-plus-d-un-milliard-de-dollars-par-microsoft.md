---
site: LeMonde.fr
title: "Android rapporte plus d’un milliard de dollars par an à… Microsoft"
author: Jérôme Marin
date: 2014-10-06
href: http://siliconvalley.blog.lemonde.fr/2014/10/06/android-rapporte-plus-dun-milliard-de-dollars-par-an-a-microsoft
tags:
- Entreprise
- Économie
- Brevets logiciels
- Innovation
- Europe
---

> L'écrasante domination d'Android sur le marché des smartphones et des tablettes ne profite pas seulement à Google, son concepteur. Elle rapporte aussi très gros à... Microsoft ! L'éditeur américain de logiciels, qui a bien du mal à imposer son système d'exploitation Windows sur les supports mobiles, a en effet conclu il y a quelques années des accords de licences avec les principaux fabricants de terminaux Android, pour leur permettre d'utiliser une partie de ses brevets.
