---
site: Juritravail
title: "Validité d’une clause de contrat de travail sur la publication par l’employeur de logiciel sous licence libre"
author: Dalila Madjid
date: 2014-10-13
href: http://www.juritravail.com/Actualite/clauses-contrat/Id/159331
tags:
- Entreprise
- Institutions
- Licenses
---

> Le Conseil de Prud’hommes de Paris, par jugement en date du 4 juin 2014, n’a pas remis en cause la validité d’une clause de propriété intellectuelle d’un contrat de travail imposant à l’employeur de publier des logiciels sous licence libre, développés par des salariés.
