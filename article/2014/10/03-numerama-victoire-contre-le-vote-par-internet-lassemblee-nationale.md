---
site: Numerama
title: "Victoire contre le vote par Internet à l'Assemblée Nationale"
author: Guillaume Champeau
date: 2014-10-03
href: http://www.numerama.com/magazine/30800-victoire-contre-le-vote-par-internet-a-l-assemblee-nationale.html
tags:
- Internet
- HADOPI
- Vote électronique
---

> Mercredi, les députés de la commission des lois de l'Assemblée Nationale ont rejeté une proposition de loi qui devait étendre le vote par Internet à l'élection présidentielle, pour les Français établis hors de France. Les députés ont pris conscience du danger d'un scrutin aussi opaque, et commencent à envisager un retrait total.
