---
site: InformatiqueNews.fr
title: "Microsoft aime enfin Linux"
author: Thierry Outrebon
date: 2014-10-22
href: http://www.informatiquenews.fr/microsoft-aime-enfin-linux-23566
tags:
- Entreprise
- Logiciels privateurs
- Informatique en nuage
---

> En soulignant que «vingt pour cent d’Azure est déjà sous Linux» le PDG de Microsoft Satya Nadella a enterré la hache de guerre avec la communauté Linux.
