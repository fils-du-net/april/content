---
site: La Tribune
title: "«Dégoogliser Internet», le combat d’irréductibles Gaulois du logiciel libre"
author: Delphine Cuny
date: 2014-10-15
href: http://www.latribune.fr/technos-medias/internet/20141015trib00504cf3a/degoogliser-internet-le-combat-d-irreductibles-gaulois-du-logiciel-libre.html
tags:
- Entreprise
- Internet
- Associations
- Promotion
- Vie privée
---

> Proposer des alternatives aux services populaires des géants du Web, c’est la démarche de l’association de promotion des logiciels libres Framasoft. Un combat essentiel et salutaire après l’affaire Snowden, ou bien utopique et perdu d’avance? Décryptage.
