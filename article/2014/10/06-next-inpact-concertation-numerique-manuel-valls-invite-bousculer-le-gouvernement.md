---
site: Next INpact
title: "Concertation numérique: Manuel Valls invite à «bousculer» le gouvernement"
author: Xavier Berne
date: 2014-10-06
href: http://www.nextinpact.com/news/90260-concertation-numerique-manuel-valls-invite-a-bousculer-gouvernement.htm
tags:
- Internet
- Institutions
---

> Samedi matin, le Conseil national du numérique (CNNum) a donné comme prévu le coup d’envoi de sa grande concertation censée préfigurer le dépôt, devant le Parlement, d’un texte de loi consacré au numérique. Le Premier ministre a pour l’occasion détaillé sa vision politique du numérique.
