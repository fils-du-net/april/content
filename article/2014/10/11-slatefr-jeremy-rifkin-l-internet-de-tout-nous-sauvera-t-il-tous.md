---
site: Slate.fr
title: "Jeremy Rifkin: L’Internet de tout nous sauvera-t-il tous?"
author: Tech $author internet
date: 2014-10-11
href: http://www.slate.fr/story/92925/jeremy-rifkin-internet-nous-sauvera-t-il-tous
tags:
- Entreprise
- Internet
- Économie
- Innovation
- Europe
---

> Pour savoir si sa prophétie selon laquelle l'internet des objets allait renverser le système économique basé sur la propriété privée des moyens de production allait se réaliser, on a fait un pari avec Jeremy Rifkin
