---
site: 01netPro.
title: "L'Etat estime à 207 millions d'euros en 2013 sa dépense en logiciels"
author: Frédéric Bergé
date: 2014-10-02
href: http://pro.01net.com/editorial/627940/letat-estime-a-207-millions-deuros-en-2013-sa-depense-en-logiciels
tags:
- Administration
- Économie
- Institutions
- Open Data
---

> En réponse à une députée, le gouvernement a révélé que l'Etat avait dépensé 207 millions d'euros en 2013 en logiciels contre 308 millions d'euros en 2011.
