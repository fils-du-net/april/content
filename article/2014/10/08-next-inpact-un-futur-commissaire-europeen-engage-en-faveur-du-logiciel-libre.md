---
site: Next INpact
title: "Un futur commissaire européen engagé en faveur du logiciel libre"
author: Xavier Berne
date: 2014-10-08
href: http://www.nextinpact.com/news/90316-un-futur-commissaire-europeen-engage-en-faveur-logiciel-libre.htm
tags:
- April
- Institutions
- Europe
- ACTA
---

> Le très probable futur commissaire européen en charge du «Marché numérique unique» vient d'affirmer à l’occasion d’une audition devant le Parlement de Strasbourg qu’il soutiendrait le développement et l’utilisation des logiciels libres au sein de l’Union, sans toutefois préciser davantage ses intentions en la matière.
