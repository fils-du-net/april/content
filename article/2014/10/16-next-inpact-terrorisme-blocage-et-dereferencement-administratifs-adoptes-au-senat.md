---
site: Next INpact
title: "Terrorisme: blocage et déréférencement administratifs adoptés au Sénat"
author: Marc Rees
date: 2014-10-16
href: http://www.nextinpact.com/news/90454-terrorisme-blocage-et-dereferencement-administratifs-adoptes-au-senat.htm
tags:
- Internet
- Institutions
---

> Comme une lettre à la poste! Les sénateurs ont adopté le projet de loi sur le terrorisme et spécialement l’article 9, celui qui instaure le blocage administratif des sites. Comme annoncé ce matin dans nos colonnes, le gouvernement a fait adopter en séance un sous-amendement visant à étendre cette mesure administrative au déréférencement des sites.
