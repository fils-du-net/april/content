---
site: Next INpact
title: "CJUE : numérisations et copies sur clef USB autorisées en bibliothèque"
author: Marc Rees
date: 2014-09-11
href: http://www.nextinpact.com/news/89837-cjue-numerisations-et-copies-sur-clef-usb-autorisees-en-bibliotheque.htm
tags:
- Internet
- Partage du savoir
- Institutions
- Sciences
- Europe
---

> La Cour de Luxembourg vient de rendre un arrêt intéressant. Les juges européens considèrent en effet que les États membres peuvent autoriser les bibliothèques à numériser les oeuvres sans l'accord des titulaires de droits. De même, les utilisateurs peuvent imprimer sur papier ou stocker sur clé USB ces livres numérisés, en contrepartie d'une compensation équitable.
