---
site: Le Monde.fr
title: "Le cryptoanarchiste allemand et l'ancien cadre du CAC 40 alliés contre la NSA"
date: 2014-09-16
href: http://www.lemonde.fr/pixels/article/2014/09/16/le-cryptoanarchiste-allemand-et-l-ancien-cadre-du-cac-40-allies-contre-la-nsa_4486314_4408996.html
tags:
- Internet
- Innovation
- Vie privée
---

> Le projet «Pretty Easy Privacy» veut faire des communications chiffrées un standard utilisable par M. et Mme Tout-le-Monde, aussi bien dans l'entreprise que dans le cadre privé.
