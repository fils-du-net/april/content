---
site: Numerama
title: "Axelle Lemaire pas pressée d'imposer la neutralité du net"
author: Guillaume Champeau
date: 2014-09-10
href: http://www.numerama.com/magazine/30522-axelle-lemaire-pas-pressee-d-imposer-la-neutralite-du-net.html
tags:
- Internet
- Institutions
- Neutralité du Net
---

> Axelle Lemaire s'est dite favorable à l'inscription d'un principe de neutralité du net en loi française, mais pas sans traiter simultanément la question de la "loyauté" des plateformes qui empruntent les réseaux.
