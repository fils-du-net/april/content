---
site: Numerama
title: "Neutralité du net: le congrès américain submergé par les appels"
author: Julien L.
date: 2014-09-11
href: http://www.numerama.com/magazine/30528-neutralite-du-net-le-congres-americain-submerge-par-les-appels.html
tags:
- Internet
- Institutions
- Associations
- Neutralité du Net
- International
---

> Dans le cadre d'une journée d'action en faveur de la neutralité du net aux USA, de nombreux sites web ont lancé une opération visant à simuler un Internet lent. En outre, les Américains se sont mobilisés pour convaincre leurs élus de se mobiliser sur ce sujet. Le congrès a été submergé par les appels au cours de la journée.
