---
site: Le Monde.fr
title: "Le Conseil d'Etat veut un encadrement des activités des espions sur Internet"
author: Martin Untersinger
date: 2014-09-09
href: http://www.lemonde.fr/pixels/article/2014/09/09/le-conseil-d-etat-veut-un-encadrement-des-activites-des-espions-sur-internet_4484206_4408996.html
tags:
- Internet
- Institutions
- Neutralité du Net
- Vie privée
---

> Le Conseil d’Etat présente, mardi 9 septembre, son étude annuelle, consacrée cette année au numérique et aux droits fondamentaux. C’est loin d’être le premier rapport que l’administration produit à propos du numérique. Mais l’étude annuelle de la plus haute juridiction administrative française, qui fait généralement référence, est toujours étudiée de près par les professionnels du droit et les milieux du domaine concerné.
