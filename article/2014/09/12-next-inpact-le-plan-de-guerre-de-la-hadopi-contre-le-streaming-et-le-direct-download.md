---
site: Next INpact
title: "Le plan de guerre de la Hadopi contre le streaming et le direct download"
author: Marc Rees
date: 2014-09-12
href: http://www.nextinpact.com/news/89876-le-plan-guerre-hadopi-contre-streaming-et-direct-download.htm
tags:
- Internet
- HADOPI
---

> Un jour, la Hadopi planche sur la rémunération proportionnelle du partage, un autre jour, celle-ci se révèle dans sa plus exacte réalité: une autorité dédiée dans la lutte contre la contrefaçon. Son ADN, qu’elle veut focaliser contre les sites de streaming et de direct download.
