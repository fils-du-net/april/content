---
site: Silicon.fr
title: "Deux villes italiennes larguent Microsoft pour l’Open Source"
author: Jacques Cheminat
date: 2014-09-16
href: http://www.silicon.fr/villes-italiennes-larguent-microsoft-lopen-source-96755.html
tags:
- Administration
- Économie
- International
---

> Turin et Udine ont décidé de migrer leur parc informatique vers des solutions Open Source et de délaisser les services de Microsoft, pour des questions budgétaires.
