---
site: WSJ.com
title: "Hard Times for Software Patents"
author: Jacob Gershman
date: 2014-09-22
href: http://blogs.wsj.com/law/2014/09/22/hard-times-for-software-patents/
tags:
- Institutions
- Brevets logiciels
- International
- English
---

> (Les cours fédérales ont rejeté plus de brevets logiciels depuis que le jugement de la Cour Suprême de juin) Federal courts have rejected more software patents since a U.S. Supreme Court ruling in June tackled the question of whether—and when—computer programs can qualify for intellectual-property protection.
