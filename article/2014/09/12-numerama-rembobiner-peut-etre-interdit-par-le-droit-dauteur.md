---
site: Numerama
title: "Rembobiner peut être interdit par le droit d'auteur"
author: Guillaume Champeau
date: 2014-09-12
href: http://www.numerama.com/magazine/30540-rembobiner-peut-etre-interdit-par-le-droit-d-auteur.html
tags:
- Entreprise
- Internet
- HADOPI
- Droit d'auteur
- Promotion
- Europe
---

> Aux Etats-Unis, les utilisateurs de MixCloud n'ont pas le droit de revenir en arrière lors de l'écoute d'une chanson, pour des questions de droits d'auteur.
