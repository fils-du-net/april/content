---
site: Le Monde.fr
title: "Un «pirate» pour superviser les services secrets berlinois"
author: Martin Untersinger
date: 2014-09-03
href: http://www.lemonde.fr/pixels/article/2014/09/03/etre-a-l-interieur-du-systeme-a-change-ma-vision-des-services-secrets_4480503_4408996.html
tags:
- Internet
- Administration
- Innovation
- International
- Vie privée
---

> Elu au Parlement de Berlin, l'activiste numérique Pavel Mayer chapeaute les services de renseignement de la ville.
