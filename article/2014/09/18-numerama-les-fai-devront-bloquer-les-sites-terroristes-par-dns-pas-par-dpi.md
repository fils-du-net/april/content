---
site: Numerama
title: "Les FAI devront bloquer les sites terroristes par DNS, pas par DPI"
author: Guillaume Champeau
date: 2014-09-18
href: http://www.numerama.com/magazine/30608-les-fai-devront-bloquer-les-sites-terroristes-par-dns-pas-par-dpi.html
tags:
- Internet
- Institutions
---

> Poussé au bout de la nuit à livrer des détails sur les techniques à employer, Bernard Cazeneuve a exclu mercredi soir que les FAI aient l'obligation de recourir au Deep Packet Inspection (DPI) pour bloquer l'accès à des pages de propagande terroriste. Sa préférence va au blocage par DNS, inefficace et qui présente des risques de surblocage.
