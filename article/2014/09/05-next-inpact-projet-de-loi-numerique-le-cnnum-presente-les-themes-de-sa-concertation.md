---
site: Next INpact
title: "Projet de loi numérique: le CNNum présente les thèmes de sa concertation"
author: Xavier Berne
date: 2014-09-05
href: http://www.nextinpact.com/news/89709-projet-loi-numerique-cnnum-presente-themes-sa-concertation.htm
tags:
- Internet
- Institutions
- Neutralité du Net
---

> Alors que le gouvernement a promis mercredi que le projet de loi relatif au numérique porté par Axelle Lemaire serait présenté l’année prochaine devant le Parlement, la grande concertation préalable à l’élaboration de ce texte s’approche à grands pas. Le Conseil national du numérique vient ainsi de dévoiler les premiers grands thèmes de cette consultation nationale, ainsi que quelques précisions concernant cette opération qui devrait débuter avant la fin du mois.
