---
site: Rue89
title: "Pendant ce temps, à l’Assemblée, l’antiterrorisme a tous les droits"
author: Camille Polloni
date: 2014-09-18
href: http://rue89.nouvelobs.com/2014/09/18/pendant-temps-assemblee-vide-lantiterrorisme-a-tous-les-droits-254929
tags:
- Internet
- Institutions
---

> Entreprise individuelle, blocage de sites internet et interdiction de sortie du territoire: la loi adoptée jeudi sacrifie la liberté à un semblant de sécurité. Et personne ne bouge.
