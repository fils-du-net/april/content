---
site: Framablog
title: "Le combat pour Internet est un combat pour des personnes"
author: Cory Doctorow (transcription Marie-Alice et traduction Framalang)
date: 2014-09-15
href: http://www.framablog.org/index.php/post/2014/09/14/Le-combat-pour-Internet-pour-des-personnes
tags:
- Internet
- Économie
- HADOPI
- DRM
- Droit d'auteur
- ACTA
---

> Bon, il y a pas mal de chances que les personnes qui assistent à des événements comme celui-ci gagnent leur vie avec une activité en ligne et même si vous ne gagnez pas votre vie en ligne aujourd’hui, vous le ferez probablement demain, parce que tout ce que nous faisons aujourd’hui implique Internet et Internet sera nécessaire pour tout ce que nous ferons demain.
