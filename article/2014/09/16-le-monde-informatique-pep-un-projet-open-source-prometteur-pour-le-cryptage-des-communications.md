---
site: Le Monde Informatique
title: "PEP, un projet Open Source prometteur pour le cryptage des communications"
author: Jean Elyan
date: 2014-09-16
href: http://www.lemondeinformatique.fr/actualites/lire-pep-un-projet-open-source-prometteur-pour-le-cryptage-des-communications-58644.html
tags:
- Internet
- Innovation
- Vie privée
---

> Le système Pretty Easy Privacy (PEP) veut rendre le chiffrement des communications écrites en ligne accessible à tous.
