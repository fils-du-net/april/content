---
site: TF1 News
title: "La France entre dans les 5 premiers e-gouvernements"
author: Mélanie Longuet
date: 2014-09-04
href: http://lci.tf1.fr/high-tech/la-france-entre-dans-les-5-premiers-e-gouvernements-8480342.html
tags:
- Internet
- Administration
- Institutions
- International
- Open Data
---

> Quatrième mondiale et première en Europe: la France arrive dans le top 5 du classement des e-gouvernements de l'ONU. Cette enquête repose sur 3 critères, notamment les services en ligne proposés aux citoyens.
