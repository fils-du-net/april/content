---
site: Next INpact
title: "RPP: le projet de la Hadopi qui pourrait décupler la redevance copie privée"
author: Marc Rees
date: 2014-09-04
href: http://www.nextinpact.com/news/89677-rpp-projet-hadopi-qui-pourrait-decupler-redevance-copie-privee.htm
tags:
- Internet
- Économie
- HADOPI
- Institutions
---

> Le chantier de la rémunération proportionnelle du partage (RPP) lancé voilà plusieurs mois par la Hadopi a donné lieu aujourd’hui à la publication d’un rapport intermédiaire. L’autorité indépendante continue à creuser le sujet, sans trop savoir si elle trouvera un filon. Derrière, cependant, un autre gisement pointe déjà son nez doré: celui de la copie privée.
