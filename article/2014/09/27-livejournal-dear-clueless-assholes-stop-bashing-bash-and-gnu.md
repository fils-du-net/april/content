---
site: LiveJournal
title: "Dear clueless assholes: stop bashing bash and GNU"
author: Andrew Auernheimer
date: 2014-09-27
href: http://weev.livejournal.com/409835.html
tags:
- Internet
- English
---

> This is a defense of the most prolific and dedicated public servant that has graced the world in my lifetime. One man has added hundreds of billions, if not trillions of dollars of value to the global economy. This man has worked tirelessly for the benefit of everyone around him. It is impossible to name a publicly traded company that has not somehow benefitted from his contributions, and many have benefitted to the tune of billions. In return for the countless billions of wealth that people made from the fruits of his labor, he was rewarded with poverty and ridicule.
