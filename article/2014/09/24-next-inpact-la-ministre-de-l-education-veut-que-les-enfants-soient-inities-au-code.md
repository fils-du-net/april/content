---
site: Next INpact
title: "La ministre de l’Éducation veut que les enfants soient initiés au code"
author: Xavier Berne
date: 2014-09-24
href: http://www.nextinpact.com/news/90064-la-ministre-l-education-veut-que-enfants-soient-inities-au-code.htm
tags:
- Institutions
- Éducation
---

> Promue ministre de l’Éducation nationale suite au dernier remaniement ministériel, Najat Vallaud-Belkacem a assuré la semaine dernière qu’elle maintenait la position de son prédecesseur s’agissant de l’apprentissage de la programmation informatique à l’école. Devant l’Assemblée nationale, la benjamine du gouvernement s’est ainsi montrée déterminée à introduire une initiation obligatoire au code pour les élèves.
