---
site: Next INpact
title: "Les consommateurs devront être avertis de la présence de DRM"
author: Marc Rees
date: 2014-09-19
href: http://www.nextinpact.com/news/89976-les-consommateurs-devront-etre-avertis-presence-drm.htm
tags:
- Interopérabilité
- April
- Institutions
- DRM
---

> Outre l’encadrement du droit de rétractation, le décret publié ce matin au Journal officiel contient une autre disposition importante en matière de droit à la consommation. Elle vise les questions des verrous numériques et de l’interopérabilité, mettant sur le dos des professionnels de nouvelles obligations.
