---
site: LeMonde.fr
title: "Quand l’open-data aura-t-il sa loi?"
author: Alexandre Léchenet
date: 2014-09-10
href: http://data.blog.lemonde.fr/2014/09/10/quand-lopen-data-aura-t-il-sa-loi
tags:
- Administration
- Institutions
- Europe
- Open Data
---

> Quand la France va-t-elle modifier sa loi sur l'open-data? La directive européenne sur les données publiques (PSI) doit être transposée dans le droit français d'ici à juillet 2015. Le 10 septembre, la commission des finances a rejeté l'article prévoyant la transposition de cette directive par cet ordonnance.
