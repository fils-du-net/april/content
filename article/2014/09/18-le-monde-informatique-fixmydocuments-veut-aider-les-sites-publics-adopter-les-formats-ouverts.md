---
site: Le Monde Informatique
title: "FixMyDocuments veut aider les sites publics à adopter les formats ouverts"
author: Loek Essers
date: 2014-09-18
href: http://www.lemondeinformatique.fr/actualites/lire-fixmydocuments-veut-aider-les-sites-publics-a-adopter-les-formats-ouverts-58665.html
tags:
- Administration
- April
- Standards
- Europe
---

> A travers sa campagne FixMyDocuments, le groupement d'intérêt Open Forum Europe dit vouloir aider les administrations à utiliser les formats de documents ouverts sur leurs sites web. Neelie Kroes a apporté son soutien à l'initiative, mais on ne sait pas si ses successeurs lui emboîteront le pas.
