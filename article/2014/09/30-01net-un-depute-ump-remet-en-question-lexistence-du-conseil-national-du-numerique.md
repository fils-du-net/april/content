---
site: 01net.
title: "Un député UMP remet en question l'existence du Conseil National du Numérique"
author: Pascal Samama
date: 2014-09-30
href: http://www.01net.com/editorial/627818/un-depute-ump-remet-en-question-l-existence-du-conseil-national-du-numerique
tags:
- Internet
- Institutions
---

> Lors d’une question à Axelle Lemaire, secrétaire d’État au Numérique, Lionel Tardy, député de Haute-Savoie, suggère de supprimer le CNNum dont les avis ne sont plus beaucoup suivis par le gouvernement.
