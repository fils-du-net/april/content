---
site: Acteurs publics
title: "La France devient leader européen de l’“e-gouvernement”"
author: Raphaël Moreaux
date: 2014-09-03
href: http://www.acteurspublics.com/2014/09/03/la-france-devient-leader-europeen-de-l-e-gouvernement
tags:
- Internet
- Administration
- Institutions
- International
- Open Data
---

> La France est le pays européen où la gouvernance en ligne est la plus développée, devant les Pays-Bas et le Royaume-Uni, selon une étude de l’Organisation des nations unies. L’Hexagone décroche la première place mondiale en termes de services publics en ligne.
