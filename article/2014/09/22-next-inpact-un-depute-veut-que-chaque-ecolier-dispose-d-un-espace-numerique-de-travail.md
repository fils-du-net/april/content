---
site: Next INpact
title: "Un député veut que chaque écolier dispose d’un espace numérique de travail"
author: Xavier Berne
date: 2014-09-22
href: http://www.nextinpact.com/news/90008-un-depute-veut-que-chaque-ecolier-dispose-d-un-espace-numerique-travail.htm
tags:
- Institutions
- Éducation
---

> Alors que l'exécutif planche sur un nouveau plan pour le numérique à l'école, un député de l’opposition vient de déposer une proposition de loi visant à rendre obligatoire les espaces numériques de travail (ENT) au sein des écoles françaises. L’élu souhaite que tous les élèves, à partir du CE2, puissent profiter de ces outils qui permettent habituellement d’accéder à des ressources pédagogiques en ligne, à des agendas, etc.
