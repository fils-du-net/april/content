---
site: Next INpact
title: "L’ANSSI «très réservée» sur les mesures de blocage de sites"
author: Marc Rees
date: 2014-09-10
href: http://www.nextinpact.com/news/89802-l-anssi-tres-reservee-sur-mesures-blocage-sites.htm
tags:
- Internet
- Institutions
---

> Ce matin à Paris, lors de la conférence Cybercercle organisée par Défense et Stratégie, Guillaume Poupard, numéro un de l’ANSSI, a exprimé ses «réserves» sur les mécanismes techniques du blocage. Une mesure qu’entend justement mettre en œuvre le gouvernement dans le cadre du projet de loi sur le terrorisme, discuté la semaine prochaine à l’Assemblée.
