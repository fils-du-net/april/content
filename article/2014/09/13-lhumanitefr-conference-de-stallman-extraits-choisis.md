---
site: l'Humanité.fr
title: "Conférence de Stallman: extraits choisis"
date: 2014-09-13
href: http://www.humanite.fr/conference-de-stallman-extraits-choisis-551619
tags:
- Logiciels privateurs
- Philosophie GNU
- Promotion
---

> Richard Stallman, le père du logiciel libre tenait ce vendredi une conférence sur les droits de l’homme et l’informatique. Morceaux choisis de la conférence.
