---
site: Le Monde Informatique
title: "La ville de Turin passe à Linux pour économiser 6 M€"
author: Maryse Gros
date: 2014-09-17
href: http://www.lemondeinformatique.fr/actualites/lire-la-ville-de-turin-passe-a-linux-pour-economiser-6-meteuro-58660.html
tags:
- Administration
- April
- Innovation
- Standards
- Europe
- Open Data
---

> Adieu Windows, Office et Explorer, bienvenue Ubuntu, OpenOffice et Mozilla. C'est ce que vient de décider la ville italienne de Turin qui compte ainsi faire une économie de 6 millions d'euros sur cinq ans.
