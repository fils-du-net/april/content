---
site: Numerama
title: "Le Conseil d'Etat veut que Netflix vous recommande \"Plus Belle La Vie\""
author: Guillaume Champeau
date: 2014-09-09
href: http://www.numerama.com/magazine/30500-le-conseil-d-etat-veut-que-netflix-vous-recommande-plus-belle-la-vie.html
tags:
- Internet
- Institutions
---

> Le Conseil d'Etat suggère au Gouvernement de confier au CSA le droit d'imposer des inflexions aux algorithmes de services en ligne, pour que des contenus français et européens soient recommandés plus souvent.
