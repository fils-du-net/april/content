---
site: Next INpact
title: "Numérique et droits fondamentaux: toutes les propositions du Conseil d'État"
author: Marc Rees
date: 2014-09-09
href: http://www.nextinpact.com/news/89720-numerique-et-droits-fondamentaux-toutes-propositions-conseil-detat.htm
tags:
- Internet
- Institutions
- Neutralité du Net
- Open Data
---

> Aujourd’hui, le Conseil d’État publie son étude annuelle portant cette fois sur le numérique et les droits fondamentaux. Avant de plonger prochainement dans le détail de certaines de ses propositions, Next INpact dresse la liste des principales mesures que la haute juridiction administration recommande de voir instaurer.
