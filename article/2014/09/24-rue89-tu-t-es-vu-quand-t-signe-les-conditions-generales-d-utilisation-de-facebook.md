---
site: Rue89
title: "Tu t’es vu quand t’as signé les conditions générales d’utilisation de Facebook?"
author: Xavier de La Porte
date: 2014-09-24
href: http://rue89.nouvelobs.com/2014/09/24/vu-quand-tas-signe-les-conditions-generales-dutilisation-facebook-255037
tags:
- Entreprise
- Internet
- Vie privée
---

> Or, il y a un certain nombre de personnes qui cherchent en ce moment à créer un “privacyleft”, inspiré par le “copyleft” du logiciel libre [l’inverse du copyright, le fait de céder volontairement ses droits, ndlr], ou des “privacy commons” inspirés des Creative Commons.
