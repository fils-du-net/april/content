---
site: Numerama
title: "Comment le Conseil d'Etat veut éviter un Edward Snowden français"
author: Guillaume Champeau
date: 2014-09-10
href: http://www.numerama.com/magazine/30510-comment-le-conseil-d-etat-veut-eviter-un-edward-snowden-francais.html
tags:
- Internet
- Administration
- Institutions
- Vie privée
---

> Pour éviter qu'un agent des services de renseignement ne s'adresse à la presse pour dénoncer des activités illégales de l'Etat, le Conseil d'Etat propose de créer un droit d'alerte à n'exercer qu'auprès... de l'Etat.
