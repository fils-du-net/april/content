---
site: France Culture
title: "Nous et nos données"
date: 2014-09-19
href: http://www.franceculture.fr/emission-pixel-nous-et-nos-donnees-2014-09-19
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> Chaque jour, chacun d’entre nous produit de plus en plus de données. Même les plus fervents défenseurs de la déconnexion, ou les réseaux sociaux laissent des traces numériques. A chaque fois que nous nous connectons, utilisons notre abonnement pour un vélo en libre accès, prenons le métro ou le tram, envoyons des mails ou téléchargeons une application, nous laissons des informations sur nous. Elles deviennent un enjeu majeur qui mobilise de plus en plus, sauf peut-être les premiers concernés ! Enquête de Catherine Petillon.
