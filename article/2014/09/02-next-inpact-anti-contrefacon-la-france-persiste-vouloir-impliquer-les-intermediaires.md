---
site: Next INpact
title: "Anti-contrefaçon: la France persiste à vouloir impliquer les intermédiaires"
author: Marc Rees
date: 2014-09-02
href: http://www.nextinpact.com/news/89604-anti-contrefacon-france-persiste-a-vouloir-impliquer-intermediaires.htm
tags:
- Internet
- Économie
- Institutions
- Droit d'auteur
- Europe
---

> La Cour des comptes a rendu public aujourd’hui un avis portant sur «la politique publique de lutte contre la contrefaçon», du moins dans ses versants liés à la propriété industrielle (marques, dessins, modèles et brevets).
