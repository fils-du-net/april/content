---
site: Ludovia Magazine
title: "Numérique éducatif: les politiques des collectivités locales passées au crible"
author: Eric Fourcaud
date: 2014-09-01
href: http://www.ludovia.com/2014/09/numerique-educatif-la-politique-des-collectivites-locales-passee-au-crible
tags:
- Administration
- Économie
- Éducation
---

> Ludomag a présenté mardi dernier sur l'Université d'été LUDOVIA, les résultats en avant-première de l'enquête sur les politiques des collectivités locales et territoriales en matière de numérique éducatif. Si au final peu de collectivités ont répondu à la totalité des questions, cette étude témoigne néanmoins d'une évolution des pratiques et des collaborations.
