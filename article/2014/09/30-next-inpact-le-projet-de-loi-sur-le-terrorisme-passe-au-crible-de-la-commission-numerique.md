---
site: Next INpact
title: "Le projet de loi sur le terrorisme passé au crible de la Commission numérique"
author: Xavier Berne
date: 2014-09-30
href: http://www.nextinpact.com/news/90160-le-projet-loi-sur-terrorisme-passe-au-crible-commission-numerique.htm
tags:
- Internet
- Institutions
---

> Alors que son précédent avis sur le blocage administratif des sites terroristes n’a pas été suivi par les députés, la «commission numérique» de l’Assemblée nationale vient d’adresser de nouvelles critiques à l’égard du projet de loi sur le terrorisme de Bernard Cazeneuve (voir le texte tel que voté le 18 septembre, notre analyse).
