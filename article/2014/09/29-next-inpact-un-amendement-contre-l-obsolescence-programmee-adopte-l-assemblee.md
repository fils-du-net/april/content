---
site: Next INpact
title: "Un amendement contre l’obsolescence programmée adopté à l’Assemblée"
author: Xavier Berne
date: 2014-09-29
href: http://www.nextinpact.com/news/90140-un-amendement-contre-l-obsolescence-programmee-adopte-a-l-assemblee.htm
tags:
- Entreprise
- Institutions
---

> À l’Assemblée nationale, la commission parlementaire chargée d’examiner le récent projet de loi sur la transition énergétique a adopté vendredi un amendement visant à réprimer davantage les pratiques dites d’obsolescence programmée. Le fait de raccourcir intentionnellement la durée de vie d’un produit lors de sa conception pourrait sur cette base être considéré comme une tromperie, dès lors passible de sanctions.
