---
site: ZDNet
title: "La ville de Turin va passer de Windows XP à Ubuntu"
date: 2014-09-11
href: http://www.zdnet.fr/actualites/la-ville-de-turin-va-passer-de-windows-xp-a-ubuntu-39806171.htm
tags:
- Administration
- Économie
---

> Pour la municipalité de Turin, Windows, c’est terminé. Pour éviter de devoir renouveler son parc de 8300 PC, la ville va ainsi migrer de Windows XP vers Ubuntu. Economie attendue: 6 millions d’euros sur 5 ans.
