---
site: La gazette.fr
title: "L'Etat entrepreneur ouvert, nouvel avatar du numérique au service de la modernisation"
author: Sabine Blanc
date: 2014-09-18
href: http://www.lagazettedescommunes.com/270157/letat-entrepreneur-ouvert-nouvel-avatar-du-numerique-au-service-de-la-modernisation
tags:
- Internet
- Administration
- Associations
- Open Data
---

> Ce mercredi 17 septembre, le secrétaire d’Etat en charge de la Modernisation de l’Etat et de la Simplification a présenté une communication synthétisant sa stratégie en terme de numérique. Elle s’inspire du monde de l’entreprise et la donnée y joue un rôle pivot.
