---
site: Rue89
title: "Firefox OS veut bousculer Android et iOS? C’est tout ce qu’on souhaite"
author: Philippe Vion-Dury
date: 2014-09-02
href: http://rue89.nouvelobs.com/2014/09/02/firefox-os-veut-bousculer-android-ios-cest-tout-quon-souhaite-254578
tags:
- Entreprise
- Innovation
- Promotion
---

> Le navigateur Firefox sort cette année-là, et détonne: libre, gratuit, collaboratif, issu d’un travail bénévole, il convainc tout de suite. Il a surtout su faire ce que Linux n’a pas réussi (dans l’univers des systèmes d’exploitation): dépasser les cercles de développeurs et de geeks pour toucher le grand public.
