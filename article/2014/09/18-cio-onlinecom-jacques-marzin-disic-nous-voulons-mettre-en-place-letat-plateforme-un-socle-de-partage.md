---
site: "cio-online.com"
title: "Jacques Marzin (DISIC): «nous voulons mettre en place l'Etat Plateforme, un socle de partage interministériel»"
author: Bertrand Lemaire
date: 2014-09-18
href: http://www.cio-online.com/actualites/lire-disic-7068.html
tags:
- Internet
- Administration
- RGI
- Standards
- Open Data
---

> Après le décret du 1er Août 2014, le principe d'un SI unique de l'Etat est acté. Jacques Marzin, directeur de la DISIC (Direction interministérielle des systèmes d'information et de communication) nous en explique toutes les conséquences et les limites en complément des déclarations du secrétaire d'Etat Thierry Mandon.
