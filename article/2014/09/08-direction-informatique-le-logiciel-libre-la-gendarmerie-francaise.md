---
site: Direction Informatique
title: "Le logiciel libre à la Gendarmerie française"
author: Jean-François Ferland
date: 2014-09-08
href: http://www.directioninformatique.com/le-logiciel-libre-a-la-gendarmerie-francaise-1-alibi-pour-une-independance-technologique/30069
tags:
- Administration
- Économie
- Promotion
---

> La Gendarmerie nationale, qui est responsable de la sécurité dans les zones rurales et les banlieues dans la France et ses territoires – l’équivalent en quelque sorte de la Sureté du Québec – a réalisé au cours des années 2000 un projet de grande envergure: la migration de dizaines de milliers de postes de travail au moyen de logiciels libres.
