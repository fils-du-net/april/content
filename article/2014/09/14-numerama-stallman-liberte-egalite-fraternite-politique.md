---
site: Numerama
title: "Stallman Liberté Egalité Fraternité Politique"
author: Jim
date: 2014-09-14
href: http://www.numerama.com/f/133471-t-stallman-liberte-egalite-fraternite-politique.html
tags:
- Associations
- Philosophie GNU
- Promotion
---

> Stallman est intervenu à la fête de l'Humanité dans le cadre de l'espace dédié au libre, auquel ont contribué de nombreuses associations dont je ne donne pas la liste...
