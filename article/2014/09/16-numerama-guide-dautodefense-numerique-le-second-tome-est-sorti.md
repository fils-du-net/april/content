---
site: Numerama
title: "Guide d'autodéfense numérique: le second tome est sorti"
author: Julien L.
date: 2014-09-16
href: http://www.numerama.com/magazine/30584-guide-d-autodefense-numerique-le-second-tome-est-sorti.html
tags:
- Internet
- Innovation
- Vie privée
---

> Il y a quatre ans, un collectif s'est attaché à rédiger un guide d'autodéfense numérique afin de permettre à chacun d'utiliser les technologies numériques en ayant connaissance des bonnes pratiques en la matière. S'il ne prétend pas donner accès à une protection parfaite, ce guide offre toutefois des pistes pour appréhender correctement les outils technologiques et réduire la surveillance électronique.
