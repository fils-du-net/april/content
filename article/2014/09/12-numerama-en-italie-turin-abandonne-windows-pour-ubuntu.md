---
site: Numerama
title: "En Italie, Turin abandonne Windows pour Ubuntu"
author: Julien L.
date: 2014-09-12
href: http://www.numerama.com/magazine/30541-en-italie-turin-abandonne-windows-pour-ubuntu.html
tags:
- Logiciels privateurs
- Administration
- Économie
- International
---

> La ville de Turin a décidé de migrer son parc informatique vers Ubuntu. Outre des considérations économiques, la municipalité a pris en compte l'arrêt du support de Windows XP, l'expiration des licences et la vétusté des postes. Une économie de 300 euros par ordinateur est attendue.
