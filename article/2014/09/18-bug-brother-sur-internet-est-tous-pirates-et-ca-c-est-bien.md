---
site: Bug Brother
title: "«Sur Internet, on est tous pirates, et ça c’est bien»"
author: Jean-Marc Manach
date: 2014-09-18
href: http://bugbrother.blog.lemonde.fr/2014/09/19/sur-internet-on-est-tous-pirates-et-ca-cest-bien
tags:
- Internet
- Partage du savoir
- HADOPI
- DRM
- Informatique en nuage
- Vie privée
---

> traces26_revuesdotorg-small280Quand j'étais chargé de sensibiliser les étudiants d'e-juristes.org, le Master 2 Professionnel spécialité «droit des nouvelles technologies et société de l’information» de l’Université Paris Ouest Nanterre La Défense (Paris X), qui "vise à former des spécialistes sur les questions juridiques posées par les nouvelles technologies de l’information et de la communication (TIC)", je prenais un malin plaisir à commencer mes cours en leur demandant: «Qui n'a jamais "piraté" un logiciel, film, série ou fichier .mp3 -sans le payer?»
