---
site: Next INpact
title: "Valls donnera le coup d’envoi du projet de loi numérique d'ici la fin du mois"
author: Xavier Berne
date: 2014-09-04
href: http://www.nextinpact.com/news/89656-valls-donnera-coup-d-envoi-projet-loi-numerique-dici-fin-mois.htm
tags:
- Internet
- Économie
- Institutions
- Neutralité du Net
- Open Data
---

> Le gouvernement a annoncé hier à l’issue du Conseil des ministres que le Conseil national du numérique lancerait «avant la fin du mois de septembre» une concertation censée précéder le dépôt, au cours de l'année 2015, d’un grand projet de loi relatif au numérique. Ce coup d’envoi devrait être donné par le Premier ministre, Manuel Valls.
