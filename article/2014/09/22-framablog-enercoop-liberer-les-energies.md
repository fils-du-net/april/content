---
site: Framablog
title: "Enercoop: libérer les énergies"
author: Pyg
date: 2014-09-22
href: http://www.framablog.org/index.php/post/2014/09/22/Enercoop-liberer-les-energies
tags:
- Entreprise
- Économie
- Associations
---

> Le logiciel libre donne à tous salariés d’Enercoop et aux informaticiens en particulier, les moyens de domestiquer, de comprendre et d’enrichir les systèmes qu’ils utilisent.
> Le logiciel libre signifie le partage de la connaissance, c’est la base de l’enseignement, du développement d’une culture libre et de l’épanouissement intellectuel de chacun.
