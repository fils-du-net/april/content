---
site: ToulÉco
title: "La mairie de Toulouse économise 1 million d'euros en passant au logiciel libre"
date: 2014-09-02
href: http://www.touleco.fr/La-mairie-de-Toulouse-economise-1-million-d-euros-en-passant-au,14366
tags:
- Administration
- Économie
- Open Data
---

> Amorcé sous la précédente majorité, le basculement des services municipaux toulousains vers la suite bureautique LibreOffice a été confirmé par la nouvelle majorité. Il devrait même être achevé d’ici six mois. Résultat: le recours à ce logiciel libre va permettre une économie substantielle de 1 million d’euros pour la collectivité.
