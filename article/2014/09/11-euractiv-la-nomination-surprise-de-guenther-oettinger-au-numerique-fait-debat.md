---
site: EurActiv
title: "La nomination surprise de Günther Oettinger au numérique fait débat"
author: Dario Sarmadi
date: 2014-09-11
href: http://www.euractiv.fr/sections/societe-de-linformation/la-nomination-surprise-de-gunther-oettinger-au-numerique-fait-debat
tags:
- Institutions
- Europe
---

> En Allemagne, bon nombre de personnes sont surprises par la nomination de Günther Oettinger au poste de l'économie numérique dans la nouvelle Commission de Jean-Claude Juncker.
