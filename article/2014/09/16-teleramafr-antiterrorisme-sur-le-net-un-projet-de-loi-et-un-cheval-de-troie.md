---
site: Télérama.fr
title: "Antiterrorisme sur le Net: un projet de loi... et un cheval de Troie"
author: Olivier Tesquet
date: 2014-09-16
href: http://www.telerama.fr/medias/antiterrorisme-sur-le-net-un-projet-de-loi-et-un-cheval-de-troie,116819.php
tags:
- Internet
- Institutions
- International
---

> Le texte, discuté en procédure accélérée à l'Assemblée nationale, vise toutes les infractions informatiques, bien au-delà de projets terroristes. Et pourrait inquiéter hackers, activistes et journalistes.
