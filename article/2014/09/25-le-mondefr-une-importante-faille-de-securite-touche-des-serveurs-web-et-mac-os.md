---
site: Le Monde.fr
title: "Une importante faille de sécurité touche des serveurs Web et Mac OS"
date: 2014-09-25
href: http://www.lemonde.fr/pixels/article/2014/09/25/une-importante-faille-de-securite-touche-des-serveurs-web-et-mac-os_4493812_4408996.html
tags:
- Internet
---

> Une importante faille de sécurité touchant l'un des programmes-clés de nombreuses versions de Linux – ainsi que Mac OS – a été découverte par l'éditeur RedHat et concerne un très grand nombre de serveurs Web et d'autres machines connectées.
