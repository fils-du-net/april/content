---
site: ITespresso
title: "e-Gouvernement: l'ONU salue les efforts de la France"
date: 2014-09-02
href: http://www.itespresso.fr/e-gouvernement-france-distinguee-onu-78536.html
tags:
- Internet
- Administration
- Institutions
- International
- Open Data
---

> Le pays se hisse au 4e rang mondial dans le dernier rapport réalisé par l’Organisation des Nations Unies pour évaluer le développement de la gouvernance en ligne dans ses 193 Etats membres.
