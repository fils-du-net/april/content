---
site: "Radio-Canada.ca"
title: "Une nouvelle génération de droits de la personne?"
author: Danielle Beaudoin
date: 2014-09-19
href: http://ici.radio-canada.ca/nouvelles/societe/2014/09/19/005-droits-personne-nouveaux-environnement-sante-mentale.shtml
tags:
- Internet
- Institutions
- Sciences
- Vie privée
---

> L'environnement, la santé mentale, la vie privée, l'accès à Internet. Peut-on parler de nouveaux droits de la personne? Des experts croient que oui, d'autres disent que non. Voici quelques pistes de réflexion.
