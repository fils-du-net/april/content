---
site: Numerama
title: "Le Gouvernement \"fera du numérique un levier\", avec une loi en 2015"
author: Guillaume Champeau
date: 2014-09-03
href: http://www.numerama.com/magazine/30439-le-gouvernement-34fera-du-numerique-un-levier34-avec-une-loi-en-2015.html
tags:
- Entreprise
- Institutions
- Innovation
---

> Pas d'annonces, mais des engagements de principes. Pour le premier conseil des ministres de la rentrée, le Gouvernement a dévoilé une communication volontariste sur le numérique présentée par Axelle Lemaire, et confirmé qu'une loi serait examinée l'an prochain au Parlement.
