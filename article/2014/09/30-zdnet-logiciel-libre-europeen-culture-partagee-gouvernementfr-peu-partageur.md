---
site: ZDNet
title: "Logiciel libre européen, culture partagée, gouvernement.fr peu partageur..."
author: Thierry Noisette
date: 2014-09-30
href: http://www.zdnet.fr/actualites/logiciel-libre-europeen-culture-partagee-gouvernementfr-peu-partageur-39807107.htm
tags:
- Entreprise
- Internet
- Administration
- Licenses
- Europe
- Vie privée
---

> Bouquet de brèves: une appli open source pour sondages et inscriptions en ligne offerte par la Commission européenne, des documents partagés par l'université de Caen et l'agence de recherche australienne, un portail gouvernemental modérément amateur de partage, et trois mini-brèves IT.
