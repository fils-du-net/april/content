---
site: Rue89
title: "Surprise! La France dans le top 5 mondial des e-gouvernements"
author: Robin Prudent
date: 2014-09-03
href: http://rue89.nouvelobs.com/2014/09/03/suprise-france-top-5-mondial-e-gouvernements-254613
tags:
- Internet
- Administration
- Institutions
- International
- Open Data
---

> La France s’est aussi engagée [...] à accroître l’utilisation de logiciels open source [libres, ndlr]. La nouvelle politique, introduite en 2012, a pour objectif de réduire les dépenses en technologies de l’information et de la communication (TIC) et améliorer leurs souplesses tout en encourageant l’innovation et l’engagement d’autres acteurs, comme les services locaux et les communautés de développeurs.
