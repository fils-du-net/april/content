---
site: Next INpact
title: "Le projet de loi sur le terrorisme adopté par les députés: notre compte-rendu"
author: Marc Rees
date: 2014-09-18
href: http://www.nextinpact.com/news/89946-projet-loi-sur-terrorisme-en-temps-reel-articles-deja-votes.htm
tags:
- Internet
- Institutions
---

> Les députés viennent d'adopter le projet de loi sur le terrorisme après les débats que vous avez pu suivre ces derniers jours sur ce flux ou en suivant le hashtag #PJLterrorisme sur Twitter. Voici notre compte-rendu.
