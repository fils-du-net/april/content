---
site: Next INpact
title: "Lionel Tardy demande à Fleur Pellerin de se positionner sur Hadopi"
author: Xavier Berne
date: 2014-09-02
href: http://www.nextinpact.com/news/89548-lionel-tardy-demande-a-fleur-pellerin-se-positionner-sur-hadopi.htm
tags:
- Internet
- HADOPI
- Institutions
---

> Fleur Pellerin, qui ne s'est jamais positionnée de manière très claire sur Hadopi, est désormais priée de le faire. Le député (UMP) Lionel Tardy vient en effet d'interroger la nouvelle ministre de la Culture sur ce chantier laissé en plan par Aurélie Filippetti, de même que sur l'avenir du projet de loi sur la Création préparé par l'ex-locataire de la Rue de Valois.
