---
site: Next INpact
title: "Fleur Pellerin consacre la Hadopi en enterrant son transfert au CSA"
author: Marc Rees
date: 2014-09-23
href: http://www.nextinpact.com/news/90041-fleur-pellerin-consacre-hadopi-en-enterrant-son-transfert-au-csa.htm
tags:
- Internet
- HADOPI
- Institutions
---

> La fusion des compétences de la Hadopi entre les mains du CSA n’est vraiment plus la priorité du ministère de la Culture. Dans une interview au Monde, publiée cet après-midi, Fleur Pellerin confirme que désormais, la priorité est la lutte contre le streaming et le direct download.
