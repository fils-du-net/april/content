---
site: Marianne
title: "Traité transatlantique: le gouvernement demande enfin la transparence!"
author: Bruno Rieth
date: 2014-09-18
href: http://www.marianne.net/Traite-transatlantique-le-gouvernement-demande-enfin-la-transparence-_a241454.html
tags:
- TAFTA
- Institutions
- Europe
---

> Jusqu’à présent, le gouvernement se foutait bien de l’opacité entourant le mandat de négociation des émissaires européens sur le traité transatlantique. Pis, il paraissait l'approuver. Mais Matthias Fekl, le remplaçant du phobique Thomas Thévenoud au secrétariat au Commerce extérieur, vient d’exiger que le secret soit levé. Ni plus ni moins que ce que réclament depuis des mois les opposants au traité…
