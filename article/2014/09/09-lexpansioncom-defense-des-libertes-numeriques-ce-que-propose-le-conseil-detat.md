---
site: LExpansion.com
title: "Défense des libertés numériques: ce que propose le Conseil d'Etat"
author: Raphaële Karayan
date: 2014-09-09
href: http://lexpansion.lexpress.fr/high-tech/defense-des-libertes-numeriques-ce-que-propose-le-conseil-d-etat_1574241.html
tags:
- Internet
- Institutions
- Neutralité du Net
- Vie privée
---

> Le Conseil d'Etat publie ce mardi une étude sur les droits fondamentaux, qui aborde notamment la protection des données personnelles, la neutralité du Net et le droit des hébergeurs. Entretien avec les auteurs, Maryvonne de Saint-Pulgent et Laurent Cytermann.
