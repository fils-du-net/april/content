---
site: 20minutes.fr
title: "Linux et Mac OS X: Les Etats-Unis mettent en garde contre une faille de sécurité"
date: 2014-09-25
href: http://www.20minutes.fr/web/1449719-20140925-etats-unis-mettent-garde-contre-faille-securite-linux-mac-os-x
tags:
- Internet
- Institutions
- International
---

> Les Etats-Unis ont mis en garde ce jeudi contre une faille de sécurité concernant les systèmes d'exploitation Linux GNU et Mac OS X d'Apple.
