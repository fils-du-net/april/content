---
site: Le Monde.fr
title: "Importante manifestation en ligne pour défendre la neutralité du Net"
author: Grégor Brandy
date: 2014-09-10
href: http://www.lemonde.fr/pixels/article/2014/09/10/importante-manifestation-en-ligne-pour-defendre-la-neutralite-du-net_4484248_4408996.html
tags:
- Internet
- Institutions
- Associations
- Neutralité du Net
- International
---

> Plusieurs sites et services vont faire semblant de mettre longtemps à charger, mercredi, pour alerter leurs utilisateurs sur les menaces pesant sur la neutralité du Net.
