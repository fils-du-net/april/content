---
site: Next INpact
title: "La France, 1er pays au monde pour les services publics en ligne selon l’ONU"
author: Xavier Berne
date: 2014-09-03
href: http://www.nextinpact.com/news/89637-la-france-1er-pays-au-monde-pour-services-publics-en-ligne-selon-l-onu.htm
tags:
- Internet
- Administration
- Institutions
- International
- Open Data
---

> La France est le pays qui dispose des meilleurs services publics en ligne au monde, notamment grâce au site officiel de l'administration: «service-public.fr». C’est en tout cas ce qui ressort d’une récente étude menée par les Nations unies.
