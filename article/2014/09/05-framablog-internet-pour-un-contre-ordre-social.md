---
site: Framablog
title: "Internet. Pour un contre-ordre social"
author: Christophe Masutti
date: 2014-09-05
href: http://www.framablog.org/index.php/post/2014/09/05/internet-pour-un-contre-ordre-social-christophe-masutti
tags:
- Économie
- Sensibilisation
- Associations
- Philosophie GNU
---

> Depuis le milieu des années 1980, les méthodes de collaboration dans la création de logiciels libres montraient que l’innovation devait être collective pour être assimilée et partagée par le plus grand nombre. La philosophie du Libre s’opposait à la nucléarisation sociale et proposait un modèle où, par la mise en réseau, le bien-être social pouvait émerger de la contribution volontaire de tous adhérant à des objectifs communs d’améliorations logicielles, techniques, sociales.
