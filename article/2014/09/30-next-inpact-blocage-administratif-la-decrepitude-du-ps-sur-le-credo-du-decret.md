---
site: Next INpact
title: "Blocage administratif: la décrépitude du PS sur le crédo du décret"
author: Marc Rees
date: 2014-09-30
href: http://www.nextinpact.com/news/90172-blocage-administratif-decrepitude-ps-sur-credo-decret.htm
tags:
- Internet
- Institutions
---

> L'avenir du décret sur le blocage des sites pédopornographiques revient telle la marée sur les rivages de l’Assemblée nationale ou du Sénat. Dernière vague, une question posée par la députée Marie-Anne Chapdelaine qui vient de demander au ministère de l’Intérieur où il en était de la rédaction de ce texte d’application.
