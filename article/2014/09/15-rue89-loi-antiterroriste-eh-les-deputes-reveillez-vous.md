---
site: Rue89
title: "Loi antiterroriste: eh, les députés, réveillez-vous!"
author: Pascal Riché
date: 2014-09-15
href: http://rue89.nouvelobs.com/2014/09/15/loi-antiterroriste-eh-les-deputes-reveillez-254869
tags:
- Internet
- Institutions
---

> Quand Sécurité et Liberté sont dans un bateau, hélas, Liberté tombe à l’eau. C’est souvent le cas. Dans ce couple-là, quand il y a conflit, c’est la seconde la plus fragile.
