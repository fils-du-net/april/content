---
site: atlantico
title: "Docteurs follamour? Ce que à quoi pourrait aboutir les biologistes qui piratent l’ADN"
date: 2014-09-06
href: http://www.atlantico.fr/decryptage/docteurs-follamour-que-quoi-pourrait-aboutir-biologistes-qui-piratent-adn-thomas-landrain-1738646.html
tags:
- Partage du savoir
- Innovation
- Sciences
---

> Les médias s'intéressent depuis peu au mouvement du biohacking, qui consiste à mettre le maximum de moyen en oeuvre pour démocratiser les technologies qui sont le plus souvent bloquées et réservées à une élite. Se faisant, le mouvement permet à toute une communauté de penser de nouvelles façons d'utiliser des technologies pour des résultats parfois plus pratiques.
