---
site: Cowcotland
title: "Le site Open Source Exercism.io propose d'apprendre le Code pour trouver un emploi"
author: Nicolas BOUTET
date: 2014-09-23
href: http://www.cowcotland.com/news/43966/site-open-source-exercism-io-apprendre-code-trouver-emploi.html
tags:
- Internet
- Éducation
---

> Le code pourrait être l’avenir de l’homme, du moins une des solutions incontournables pour le développement de la planète. Si une des parties visibles de cette demande concerne les applications pour smartphones ou tablettes, qui oscillent entre gadgets et réels services. Le gros de la demande concerne les entreprises dont l’appétit en application dédiée est gargantuesque. Si l’éducation nationale n’a pas encore décidé de mettre le code comme apprentissage fondamental, les initiatives se développent un peu partout et notamment sur le Web.
