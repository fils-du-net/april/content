---
site: ZDNet
title: "Pratique: le guide du logiciel libre pour les TPE-PME"
author: Fabien Soyez
date: 2014-09-22
href: http://www.zdnet.fr/actualites/pratique-le-guide-du-logiciel-libre-pour-les-tpe-pme-39806659.htm
tags:
- Entreprise
- Promotion
---

> Sortis de leur carcan “geek”, les logiciels libres offrent aux entreprises une alternative aux produits propriétaires, couvrant la plupart de leurs tâches.
