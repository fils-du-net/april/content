---
site: ComputerworldUK
title: "How EU's Unified Patent Court May Repeat US's Past Mistakes"
author: Glyn Moody
date: 2014-09-22
href: http://www.computerworlduk.com/blogs/open-enterprise/upcs-problems-3573298
tags:
- Institutions
- Brevets logiciels
- English
---

> Back in May, I wrote about a very interesting paper discussing some potential pitfalls of the new Unified Patent Court. Given the magnitude of the change that it and the unitary patent system will bring, it is extraordinary that we still don't really know how things will work out in practice. That makes another paper called "The Unified Patent Court (UPC) in Action - How Will the Design of the UPC Affect Patent Law?" particularly welcome, since, as its title suggests, it explores how the new UPC is likely to shape the contours of patent law in Europe.
