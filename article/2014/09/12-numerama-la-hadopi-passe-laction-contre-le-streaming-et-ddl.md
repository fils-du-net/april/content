---
site: Numerama
title: "La Hadopi passe à l'action contre le streaming et DDL"
author: Guillaume Champeau
date: 2014-09-12
href: http://www.numerama.com/magazine/30551-la-hadopi-passe-a-l-action-contre-le-streaming-et-ddl.html
tags:
- Internet
- HADOPI
---

> Faute pour François Hollande d'avoir tenu sa promesse d'abroger la loi Hadopi, la Haute Autorité poursuit sa route. Et elle s'ouvre tout un nouveau chemin en dévoilant ce vendredi la mise en route d'un plan d'actions contre les sites "massivement contrefaisants". Dans le coeur de cible: les sites de streaming et les plateformes de téléchargement direct.
