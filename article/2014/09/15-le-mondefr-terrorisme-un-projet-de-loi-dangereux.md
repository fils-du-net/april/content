---
site: Le Monde.fr
title: "Terrorisme: un projet de loi dangereux"
date: 2014-09-15
href: http://www.lemonde.fr/proche-orient/article/2014/09/15/terrorisme-un-projet-de-loi-dangereux_4487639_3218.html
tags:
- Internet
- Institutions
---

> Le projet de loi examiné par le Parlement constitue une nouvelle entorse alarmante aux libertés publiques.
