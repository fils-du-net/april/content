---
site: toolinux
title: "Guide de l'open source, édition 2014"
date: 2014-09-19
href: http://www.toolinux.com/Guide-de-l-open-source-edition,21495
tags:
- Entreprise
- Sensibilisation
---

> Cette nouvelle édition du Guide recense plus de 350 solutions open source professionnelles, dans près de 50 domaines d’applications pour l’entreprise.
