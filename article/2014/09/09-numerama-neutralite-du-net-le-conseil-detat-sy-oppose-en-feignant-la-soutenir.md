---
site: Numerama
title: "Neutralité du net: le Conseil d'Etat s'y oppose en feignant la soutenir!"
author: Guillaume Champeau
date: 2014-09-09
href: http://www.numerama.com/magazine/30493-neutralite-du-net-le-conseil-d-etat-s-y-oppose-en-feignant-la-soutenir.html
tags:
- Internet
- Institutions
- Neutralité du Net
---

> Le Conseil d'Etat prétend vouloir consacrer dans la loi le principe de la neutralité du net, mais pas sans prévoir dans la loi toutes les exceptions que réclament les opérateurs télécoms.
