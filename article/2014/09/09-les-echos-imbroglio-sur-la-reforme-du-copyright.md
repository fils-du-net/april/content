---
site: Les Echos
title: "Imbroglio sur la réforme du copyright"
author: Charles Cuvelliez
date: 2014-09-09
href: http://www.lesechos.fr/idees-debats/cercle/cercle-108516-copyright-les-usagers-sont-sur-venus-tous-les-autres-sur-mars-1040594.php
tags:
- Internet
- Économie
- HADOPI
- Institutions
- Droit d'auteur
- Licenses
---

> La Commission européenne a lancé une vaste consultation sur la réforme du copyright qui a recueilli 9 500 contributions d'utilisateurs et de consommateurs... Et personne n'est d'accord.
