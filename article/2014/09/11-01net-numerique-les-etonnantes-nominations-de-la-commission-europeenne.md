---
site: 01net.
title: "Numérique: les étonnantes nominations de la Commission européenne"
author: Pascal Samama
date: 2014-09-11
href: http://www.01net.com/editorial/626674/numerique-les-etonnantes-nominations-de-la-commission-europeenne
tags:
- Internet
- Institutions
- Europe
---

> Jean-Claude Juncker, a présenté son équipe. Les nouveaux responsables du numérique sont loin d’être des spécialistes. Bonne nouvelle pour les géants du Net, moins bonne pour l’Europe numérique. Portraits.
