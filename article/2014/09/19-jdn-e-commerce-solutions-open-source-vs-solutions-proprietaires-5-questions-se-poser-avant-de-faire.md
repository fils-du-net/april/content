---
site: JDN
title: "E-commerce: solutions open source vs solutions propriétaires: 5 questions a se poser avant de faire son choix"
author: Yannick Maingot
date: 2014-09-19
href: http://www.journaldunet.com/ebusiness/expert/58489/e-commerce---solutions-open-source-vs-solutions-proprietaires---5-questions-a-se-poser-avant-de-faire-son-choix.shtml
tags:
- Entreprise
- Logiciels privateurs
- Économie
---

> Dans le monde «impitoyable» des technologies e-commerce, s’affrontent depuis toujours deux philosophies: celle des solutions technologiques reposant sur le modèle Open Source et celle des éditeurs de solutions propriétaires.
