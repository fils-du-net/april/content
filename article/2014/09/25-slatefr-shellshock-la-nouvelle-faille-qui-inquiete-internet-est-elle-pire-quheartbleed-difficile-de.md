---
site: Slate.fr
title: "Shellshock, la nouvelle faille qui inquiète Internet, est-elle pire qu'Heartbleed? Difficile de le savoir"
author: Andréa Fradin
date: 2014-09-25
href: http://www.slate.fr/story/92601/bash-nouveau-bug-pire-heartbleed
tags:
- Internet
---

> Le bug touche Bash, un programme que vous ne connaissez peut-être pas, mais qui permet à l'utilisateur d'accéder aux fonctions du système d'exploitation.
