---
site: Numerama
title: "Voici les deux commissaires européens chargés du numérique"
author: Guillaume Champeau
date: 2014-09-10
href: http://www.numerama.com/magazine/30519-voici-les-deux-commissaires-europeens-charges-du-numerique.html
tags:
- Institutions
- Europe
---

> Un Estonien et un Allemand. Jean-Claude Juncker a choisi deux hommes pour donner chair à la politique numérique de la Commission européenne: Andrus Ansip pour le marché unique numérique, et Günther Oettinger pour l'économie et la société numériques.
