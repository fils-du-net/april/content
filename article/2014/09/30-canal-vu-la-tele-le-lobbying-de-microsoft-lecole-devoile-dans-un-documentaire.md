---
site: Canal+
title: "Vu à la télé: le lobbying de Microsoft à l'école dévoilé dans un documentaire"
date: 2014-09-30
href: http://www.framablog.org/index.php/post/2014/09/30/microsoft-education-logiciel-libre-video
tags:
- Entreprise
- Logiciels privateurs
- April
- Éducation
---

> Le 8 septembre a été diffusé sur Canal+ le documentaire «École du futur: la fin des profs?» dans le cadre de l’émission Spécial Investigation. Il nous montre des expériences innovantes dans des classes en France et aux USA, s’intéresse aux marchés des manuels scolaires et accorde une large part à l’offre et aux stratégies commerciales de géants comme Apple ou Microsoft.
