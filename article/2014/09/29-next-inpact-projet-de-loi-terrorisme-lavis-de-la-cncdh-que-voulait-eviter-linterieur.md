---
site: Next INpact
title: "Projet de loi Terrorisme: l'avis de la CNCDH que voulait éviter l'Intérieur"
author: Marc Rees
date: 2014-09-29
href: http://www.nextinpact.com/news/90139-projet-loi-terrorisme-avis-cncdh-que-voulait-eviter-interieur.htm
tags:
- Internet
- Institutions
---

> Après la Commission des libertés numériques, le Conseil national du numérique, le Syndicat de la Magistrature, la Quadrature du Net, et d’autres, c’est autour de la Commission nationale consultative des droits de l’Homme de rendre un avis au vitriol du projet de loi sur le terrorisme porté par Bernard Cazeneuve. Pour l'occasion, la CNCDH s'est auto-saisie, le ministre de l'Intérieur ayant oublié son existence.
