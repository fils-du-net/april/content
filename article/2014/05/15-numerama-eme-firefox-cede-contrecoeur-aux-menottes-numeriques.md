---
site: Numerama
title: "EME: Firefox cède à contrecoeur aux menottes numériques"
author: Julien L.
date: 2014-05-15
href: http://www.numerama.com/magazine/29383-eme-firefox-cede-a-contrecoeur-aux-menottes-numeriques.html
tags:
- Internet
- Associations
- DRM
- Standards
---

> Mozilla a annoncé que Firefox prendra en compte les menottes numériques EME (extensions pour médias chiffrés), malgré les multiples difficultés qu'elles occasionnent. La fondation a expliqué ne pas avoir le choix et promis de faire son possible pour que ce support soit le moins problématique possible.
