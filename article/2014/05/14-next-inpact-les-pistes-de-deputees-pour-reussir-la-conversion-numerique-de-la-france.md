---
site: Next INpact
title: "Les pistes de députées pour réussir «la conversion numérique» de la France"
author: Xavier Berne
date: 2014-05-14
href: http://www.nextinpact.com/news/87518-les-pistes-deputees-pour-reussir-conversion-numerique-france.htm
tags:
- Entreprise
- Internet
- Administration
- Institutions
- Sensibilisation
- Éducation
- Innovation
- Neutralité du Net
- Europe
- International
- Open Data
---

> Éveil au codage dès l'école primaire, consécration du principe de neutralité du Net, modification de la législation européenne relative aux données personnelles, création d’un «Nasdaq européen», accélération de l’Open Data... Les pistes des députées Corinne Erhel et Laure de La Raudière ne manquent pas pour «enclencher la conversion numérique » de la France et favoriser ainsi le développement économique de notre pays. Petit tour d’horizon du rapport présenté aujourd'hui par les deux parlementaires.
