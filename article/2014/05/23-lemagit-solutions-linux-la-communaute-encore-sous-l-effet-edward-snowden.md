---
site: LeMagIT
title: "Solutions Linux: La communauté encore sous l’effet Edward Snowden"
author: Cyrille Chausson
date: 2014-05-23
href: http://www.lemagit.fr/actualites/2240221235/Solutions-Linux-La-communaute-encore-sous-leffet-Edward-Snowden
tags:
- Entreprise
- Internet
- Économie
- Associations
- Innovation
---

> Le logiciel libre peut-il être la pilule miracle contre les menaces sécuritaires révélées par les écoutes de la NSA? C’est l'une des questions à laquelle a tenté de répondre l’édition 2014 de Solutions Linux.
