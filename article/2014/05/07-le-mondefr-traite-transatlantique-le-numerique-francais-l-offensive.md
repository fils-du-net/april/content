---
site: Le Monde.fr
title: "Traité transatlantique: le numérique français à l’offensive?"
author: Maxime Vaudano
date: 2014-05-07
href: http://www.lemonde.fr/technologies/article/2014/05/07/traite-transatlantique-le-numerique-francais-a-l-offensive_4413077_651865.html
tags:
- Internet
- Économie
- Institutions
- International
- ACTA
---

> Faire en sorte que le numérique soit aussi bien défendu que la betterave, tel est, en substance, l’objectif du Conseil national du numérique (CNN). Dans un rapport remis, mercredi 7 mai, à la secrétaire d’Etat au commerce extérieur, Fleur Pellerin, l’instance consultative insiste sur la nécessité de placer d’urgence les enjeux numériques au cœur des négociations en cours sur le futur traité de libre-échange entre l’Europe et les Etats-Unis (Tafta, Transatlantic Free Trade Area).
