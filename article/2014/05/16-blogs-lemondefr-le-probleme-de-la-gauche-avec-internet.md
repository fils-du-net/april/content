---
site: Blogs LeMonde.fr
title: "Le problème de la gauche avec internet"
author: Hubert Guillaud
date: 2014-05-16
href: http://internetactu.blog.lemonde.fr/2014/05/16/le-probleme-de-la-gauche-avec-internet
tags:
- Internet
- Institutions
- Open Data
---

> David Golumbia (@dgolumbia), auteur de La logique culturelle de l'informatique, publie une intéressante tribune dans Jacobin, le magazine socialiste américain. Comment expliquer, questionne-t-il, que si la révolution numérique produit de la démocratie, déstabilise les hiérarchies, décentralise ce qui était centralisé... bref, favorise les valeurs de gauche, celle-ci semble alors plus dispersée que jamais, et même en voie de disparition dans les démocraties les plus avancées?
