---
site: Next INpact
title: "La justice condamne un logiciel d’enregistrement de flux en streaming"
author: Marc Rees
date: 2014-05-28
href: http://www.nextinpact.com/news/87781-la-justice-condamne-logiciel-d-enregistrement-flux-en-streaming.htm
tags:
- Entreprise
- Internet
- Interopérabilité
- Institutions
- DADVSI
- DRM
---

> Peut-on légalement éditer un logiciel qui permet d’enregistrer le flux streaming de Deezer? La plateforme estime que non. Elle avait poursuivi à cette fin, avec d’autres ayants droit, l’auteur du logiciel Tubemaster++. L’affaire avait été jugée l’an passé, mais nous venons d’avoir copie de ce jugement important du tribunal correctionnel de Nîmes.
