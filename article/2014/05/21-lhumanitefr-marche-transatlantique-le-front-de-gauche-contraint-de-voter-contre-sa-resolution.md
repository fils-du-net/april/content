---
site: l'Humanité.fr
title: "Marché transatlantique: le Front de gauche contraint de voter contre sa résolution"
author: Sarah Sudre
date: 2014-05-21
href: http://www.humanite.fr/marche-transatlantique-la-resolution-europeenne-du-front-de-gauche-emiettee-par-le-ps-533192
tags:
- Économie
- Institutions
- Europe
- ACTA
---

> Le Front de Gauche a été contraint de voter contre sa propre résolution européenne portant sur les négociations transatlantiques, "vidée de sa substance par le PS". Ecoeuré de "la dérive atlantiste du gouvernement", André Chassaigne, président du groupe, assument ses responsabilités d'avoir voter contre un texte qui demande seulement plus de transparence.
