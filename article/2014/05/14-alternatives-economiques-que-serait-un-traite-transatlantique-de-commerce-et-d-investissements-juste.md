---
site: "Alternatives-economiques"
title: "Que serait un traité transatlantique de commerce et d’investissements JUSTES?"
author: Jean Gadrey
date: 2014-05-14
href: http://alternatives-economiques.fr/blogs/gadrey/2014/05/14/que-serait-un-traite-transatlantique-de-commerce-et-d%E2%80%99investissements-justes
tags:
- Entreprise
- Internet
- Économie
- Associations
- Promotion
- Europe
- International
- ACTA
---

> Cette question est essentielle pour ne pas en rester à une simple opposition au projet en cours, bien que cette opposition soit indispensable et actuellement prioritaire pour écarter le danger. Mais pour proposer quoi à la place? Un projet alternatif a été présenté par une alliance d’associations et d’ONG (une soixantaine en Europe, une vingtaine en dehors) dans un document de 20 pages baptisé «un Mandat Commercial Alternatif» accessible en ligne via ce lien, et, pour un résumé (7 pages), via cet autre lien.
