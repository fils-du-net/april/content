---
site: ActuaLitté.com
title: "Encourager les DRM favorise le monopole: double peine pour Hachette"
author: Nicolas Gary
date: 2014-05-30
href: http://www.actualitte.com/tablettes/encourager-les-drm-favorise-le-monopole-double-peine-pour-hachette-50466.htm
tags:
- Entreprise
- Internet
- Économie
- DRM
- Neutralité du Net
- International
---

> Ayant officiellement reconnu que l'éditeur et lui étaient en conflit, Jeff Bezos a mandaté ses équipes pour qu'elles diffusent un message de paix et d'amour. On peut notamment y découvrir une invitation à aller acheter chez d'autres vendeurs de livres les titres du groupe Hachette US, puisqu'ils ne peuvent plus être proposés dans les meilleures conditions chez Amazon. Une clémence, une abnégation rarement constatées.
