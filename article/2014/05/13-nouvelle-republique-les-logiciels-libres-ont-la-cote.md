---
site: Nouvelle République
title: "Les logiciels libres ont la cote"
date: 2014-05-13
href: http://www.lanouvellerepublique.fr/Loir-et-Cher/Actualite/24-Heures/n/Contenus/Articles/2014/05/13/Les-logiciels-libres-ont-la-cote-1905407
tags:
- Sensibilisation
- Associations
---

> Beau succès, samedi après-midi à la médiathèque, pour «l'install party» organisée par les associations Blogul et Solix. Une cinquantaine de personnes se sont déplacées, ordinateur sous le bras, pour découvrir et installer des logiciels libres avec l'aide d'informaticiens spécialisés.
