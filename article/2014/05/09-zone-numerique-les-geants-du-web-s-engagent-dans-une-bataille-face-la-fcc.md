---
site: Zone Numerique
title: "Les géants du Web s’engagent dans une bataille face à la FCC"
date: 2014-05-09
href: http://www.zone-numerique.com/les-geants-du-web-sengagent-dans-une-bataille-face-a-la-fcc.html
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- International
---

> La FCC, régulateur américain pourrait modifier les règles de Neutralité du Net et mettrait en place un traitement préférentiel permettant aux FAI de monnayer l’accès à leurs services. Les géants du Web se mobilisent contre la FCC pour éviter un Internet à deux vitesses.
