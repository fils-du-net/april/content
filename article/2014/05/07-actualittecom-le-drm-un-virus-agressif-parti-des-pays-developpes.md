---
site: ActuaLitté.com
title: "Le DRM, un virus agressif parti des pays développés"
author: Antoine Oury
date: 2014-05-07
href: http://www.actualitte.com/usages/le-drm-un-virus-agressif-parti-des-pays-developpes-49985.htm
tags:
- Économie
- Institutions
- DRM
- Droit d'auteur
- International
---

> Journée internationale contre le DRM : de Hollywood à la Chine, tout est sous verrous
