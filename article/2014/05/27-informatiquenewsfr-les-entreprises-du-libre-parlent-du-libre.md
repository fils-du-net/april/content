---
site: InformatiqueNews.fr
title: "Les entreprises du Libre parlent du Libre"
author: La rédaction
date: 2014-05-27
href: http://www.informatiquenews.fr/les-entreprises-du-libre-parlent-du-libre-15391
tags:
- Entreprise
- Internet
- Économie
- Standards
- Informatique en nuage
---

> Évidemment, c’est un peu comme si EDF ou la RATP publiait une enquête sur la qualité de leur service mais cela n’empêche d’observer les résultats de l’enquête réalisée par le Conseil National du Logiciel Libre (CNLL) sur les technologies d’avenir, les freins à l’adoption du libre, l’affaire Snowden…
