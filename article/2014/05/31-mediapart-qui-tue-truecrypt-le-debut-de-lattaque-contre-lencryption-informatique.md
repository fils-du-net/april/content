---
site: Mediapart
title: "Qui a tué Truecrypt?: Le début de l'attaque contre l'encryption informatique?"
author: mfrick
date: 2014-05-31
href: http://blogs.mediapart.fr/blog/mfrick/310514/qui-tue-truecrypt-le-debut-de-lattaque-contre-lencryption-informatique
tags:
- Internet
- Institutions
- Informatique-deloyale
---

> Mercredi 28 Mai 2014, Truecrypt, probablement le logiciel d'encryption informatique le plus élaboré pour le grand public, a cessé son développement. Comme seul justification deux phrases figurent sur le site, informant que Windows Vista, 7 et 8 proposent une encryption intégrée au système et que le développement a cessé avec la fin du support de Windows XP. Bien qu'aucun détail ne soit connu, beaucoup mène à penser que Truecrypt a en fait été enterré par des services secrets.
