---
site: Libération.fr
title: "Libre-échange: qui pense quoi sur le Tafta?"
author: Laure EQUY
date: 2014-05-12
href: http://www.liberation.fr/politiques/2014/05/12/libre-echange-qui-pense-quoi-sur-le-tafta_1015704
tags:
- TAFTA
- Économie
- Institutions
- Europe
- ACTA
---

> Front de gauche et EE-LV profitent de la campagne pour mobiliser contre le grand marché transatlantique en cours de négociations. Le PS se dit méfiant mais paraît discret, l'UMP a priori favorable. Les positions de chacun.
