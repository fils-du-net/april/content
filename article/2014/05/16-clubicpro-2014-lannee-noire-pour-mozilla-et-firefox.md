---
site: ClubicPro
title: "2014: l'année noire pour Mozilla et Firefox?"
author: Guillaume Belfiore
date: 2014-05-16
href: http://pro.clubic.com/entreprises/fondation-mozilla/actualite-703229-mozilla-firefox.html
tags:
- Internet
- Associations
- DRM
- Standards
---

> Décidément, l'année 2014 ne semble pas réussir à la fondation Mozilla et cette dernière s'est retrouvée en quelques mois au centre de plusieurs polémiques.
