---
site: Next INpact
title: "Le CNNum préconise de «temporiser les négociations» du traité TAFTA"
author: Xavier Berne
date: 2014-05-09
href: http://www.nextinpact.com/news/87425-le-cnnum-preconise-temporiser-negociations-traite-tafta.htm
tags:
- Économie
- Institutions
- Associations
- Europe
- International
- ACTA
---

> Dans un avis rendu mercredi à Fleur Pellerin, Secrétaire d’État chargée du Commerce extérieur, le Conseil national du numérique recommande à l’Union européenne d’être particulièrement vigilante dans le cadre des négociations qui ont actuellement cours avec les États-Unis, à propos d’un accord de libre-échange qui impacterait de très nombreux secteur de l’économie, à commencer par le numérique.
