---
site: Sud Ouest
title: "La faille de sécurité d'Internet Explorer est corrigée, y compris pour Windows XP"
date: 2014-05-02
href: http://www.sudouest.fr/2014/05/02/la-faille-de-securite-d-internet-explorer-est-corrigee-y-compris-pour-windows-xp-1541941-4725.php
tags:
- Entreprise
- Internet
- Logiciels privateurs
---

> Microsoft s'est dépêché de corriger une faille de sécurité détectée le 28 avril, décrite comme une "menace importante" par les experts
