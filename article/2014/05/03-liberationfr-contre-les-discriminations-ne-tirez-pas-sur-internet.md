---
site: Libération.fr
title: "Contre les discriminations, ne tirez pas sur Internet"
author: Rokhaya Diallo et Félix Tréguer
date: 2014-05-03
href: http://www.liberation.fr/societe/2014/04/27/contre-les-discriminations-ne-tirez-pas-sur-internet_1005778
tags:
- Internet
- Institutions
- Associations
---

> Dans plusieurs textes en cours de discussion au Parlement, et notamment le projet de loi sur l’égalité femmes-hommes, le gouvernement compte œuvrer au renforcement de la répression sur Internet.
