---
site: Infobourg.com
title: "Une base de données recensant les logiciels libres et gratuits par discipline"
author: Dominic Leblanc
date: 2014-05-15
href: http://www.infobourg.com/2014/05/15/une-base-de-donnees-recensant-les-logiciels-libres-et-gratuits-par-discipline
tags:
- Administration
- Partage du savoir
- Éducation
- International
---

> eau des répondants TIC des cégeps a dévoilé le 5 mai dernier une base de données recensant des logiciels libres et/ou gratuits, fort utile pour tous les ordres d’enseignement.
