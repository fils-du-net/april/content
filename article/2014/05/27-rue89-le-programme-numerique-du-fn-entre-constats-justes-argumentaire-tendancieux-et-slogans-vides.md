---
site: Rue89
title: "Le programme numérique du FN: entre constats justes, argumentaire tendancieux et slogans vides"
author: Xavier de La Porte
date: 2014-05-27
href: http://rue89.nouvelobs.com/2014/05/27/programme-numerique-fn-entre-constats-justes-argumentaire-tendancieux-slogans-vides-252481
tags:
- Internet
- Institutions
- Europe
- ACTA
---

> Parmi les un peu plus de 4 millions de personnes qui ont voté pour le Front national aux élections européennes, très peu sans doute l’ont fait pour des raisons numériques. Je veux dire par là, très peu sans doute se sont décidées en fonction de ce que le Front national avance sur les questions numériques.
