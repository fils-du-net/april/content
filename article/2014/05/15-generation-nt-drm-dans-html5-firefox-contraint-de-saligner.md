---
site: "Génération-NT"
title: "DRM dans HTML5: Firefox contraint de s'aligner"
author: Jérôme G.
date: 2014-05-15
href: http://www.generation-nt.com/html5-video-drm-firefox-adobe-eme-cdm-netflix-actualite-1884312.html
tags:
- Internet
- Associations
- DRM
- Standards
---

> Afin de ne pas marginaliser Firefox, Mozilla accepte d'implémenter pour le navigateur une solution de gestion des droits numériques des vidéos au sein de HTML5.
