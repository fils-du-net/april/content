---
site: ActuaLitté.com
title: "#DRMDAY: 'Non, ce n'est pas obligatoire, il y a des solutions alternatives'"
author: David Queffélec
date: 2014-05-06
href: http://www.actualitte.com/usages/drmday-non-ce-n-est-pas-obligatoire-il-y-a-des-solutions-alternatives-49961.htm
tags:
- Entreprise
- Internet
- DRM
---

> Un frein au développement des usages numériques 
