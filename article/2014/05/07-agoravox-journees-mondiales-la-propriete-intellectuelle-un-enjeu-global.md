---
site: AgoraVox
title: "Journées mondiales: la propriété intellectuelle, un enjeu global"
author: Hubert Arvilles
date: 2014-05-07
href: http://www.agoravox.fr/culture-loisirs/culture/article/journees-mondiales-la-propriete-151607
tags:
- April
- Institutions
- Associations
- DRM
---

> Entre la question des brevets et des droits d’auteur, la propriété intellectuelle est sujette à de nombreuses interprétations et controverses. Pour preuve, le 26 avril dernier, une journée internationale lui était consacrée, alors qu'aujourd’hui, mardi 6 mai, c’est la journée mondiale contre les DRM. Au-delà de ces questions de point de vue, le financement de la création et de l’innovation reste une question centrale qui concerne de nombreux pays comme le montre entre autres un récent rapport du commerce extérieur des Etats-Unis…
