---
site: Next INpact
title: "Les candidats aux européennes invités à signer le «Pacte du logiciel libre»"
author: Xavier Berne
date: 2014-05-16
href: http://www.nextinpact.com/news/87593-les-candidats-aux-europeennes-invites-a-signer-pacte-logiciel-libre.htm
tags:
- April
- Institutions
- Promotion
- Europe
---

> Alors que les élections européennes avancent désormais à grands pas, plusieurs organisations de promotion du logiciel libre s’activent afin de faire signer le «Pacte du logiciel libre» à un maximum de candidats. Seuls 101 personnes se présentant afin de devenir eurodéputé s’y sont cependant ralliées à ce jour, dont 48 uniquement pour la France.
