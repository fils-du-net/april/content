---
site: Contrepoints
title: "CSA, neutralité et gouvernance du Net"
author: h16
date: 2014-05-04
href: http://www.contrepoints.org/2014/05/04/164997-csa-neutralite-et-gouvernance-du-net
tags:
- Internet
- Économie
- HADOPI
- Institutions
- Neutralité du Net
---

> Dans cette interview (que je fournis à la fin de cet article), Bayart qui se décrit lui-même comme un militant pour les libertés fondamentales dans la société de l’information, par la neutralité du net et le logiciel libre, est interrogé sur quelques domaines numériques connexes, et en profite pour passer en revue différents aspects de l’évolution de la société numérique, dans le monde en général et en France plus particulièrement.
