---
site: Le Figaro.fr
title: "Le traité de libre-échange transatlantique divise droite et gauche"
author: Anne Rovan et Judith Waintraub
date: 2014-05-05
href: http://www.lefigaro.fr/international/2014/05/05/01003-20140505ARTFIG00319-le-traite-de-libre-echange-divise-droite-et-gauche.php
tags:
- Entreprise
- Économie
- Institutions
- International
- ACTA
---

> L'accord commercial, encore en négociation, entre l'Europe et les États-Unis fait polémique. Et s'invite dans le scrutin européen.
