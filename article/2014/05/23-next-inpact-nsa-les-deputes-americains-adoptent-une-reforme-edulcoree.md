---
site: Next INpact
title: "NSA: les députés américains adoptent une réforme édulcorée"
author: Vincent Hermann
date: 2014-05-23
href: http://www.nextinpact.com/news/87708-nsa-deputes-americains-adoptent-reforme-edulcoree.htm
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> L’annonce en janvier d’une réforme de la NSA par Barack Obama avait laissé de l’espoir sur un possible resserrage de la politique de surveillance américaine. Mais le projet, adopté hier par la chambre des représentants, comporte des changements de dernière minute qui ont provoqué le retrait du support des grandes entreprises et des associations de défense des libertés civiles.
