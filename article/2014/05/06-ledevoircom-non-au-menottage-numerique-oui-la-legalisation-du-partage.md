---
site: LeDevoir.com
title: "Non au menottage numérique, oui à la légalisation du partage"
author: Fabián Rodríguez et le CA de FACIL
date: 2014-05-06
href: http://www.ledevoir.com/societe/science-et-technologie/407429/non-au-menottage-numerique-oui-a-la-legalisation-du-partage
tags:
- Internet
- Partage du savoir
- Associations
- DRM
- Droit d'auteur
- Promotion
- International
---

> En ce mardi 6 mai 2014, Journée internationale contre les DRM, FACIL se joint à de nombreux citoyens et organismes à travers le monde pour protester contre les technologies de menottage numérique imposées au public par les gros joueurs des industries culturelles et technologiques.
