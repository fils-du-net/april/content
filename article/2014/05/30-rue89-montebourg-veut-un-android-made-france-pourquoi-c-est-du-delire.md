---
site: Rue89
title: "Montebourg veut un Android «made in France»: pourquoi c’est du délire"
author: Philippe Vion-Dury
date: 2014-05-30
href: http://rue89.nouvelobs.com/2014/05/30/montebourg-veut-android-made-in-france-pourquoi-cest-delire-252580
tags:
- Entreprise
- Économie
- April
- Institutions
- Innovation
---

> Le ministre aimerait que la France développe son propre OS, cet ensemble de logiciels qui fait marcher votre smartphone ou votre ordinateur. Pour les professionnels du numérique, c’est une hérésie.
