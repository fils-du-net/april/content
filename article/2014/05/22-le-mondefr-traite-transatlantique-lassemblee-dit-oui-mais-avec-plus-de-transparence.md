---
site: Le Monde.fr
title: "Traité transatlantique: l'Assemblée dit oui, mais avec plus de transparence"
date: 2014-05-22
href: http://www.lemonde.fr/politique/article/2014/05/22/traite-tafta-l-assemblee-vote-la-suite-des-negociations-avec-plus-de-transparence_4424077_823448.html
tags:
- Économie
- Institutions
- Sciences
- Europe
- ACTA
- Vie privée
---

> Le Front de gauche, qui avait proposé un texte très réécrit en commission, dénonce un document «dénaturé» sur le futur traité transatlantique sur une zone libre-échange.
