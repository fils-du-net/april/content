---
site: 01netPro.
title: "Des élections européennes à forts enjeux numériques"
author: Xavier Biseul
date: 2014-05-12
href: http://pro.01net.com/editorial/619566/des-elections-europeennes-a-forts-enjeux-numeriques
tags:
- Économie
- Institutions
- Europe
- ACTA
---

> Protection des données personnelles, brevetabilité des logiciels, accès aux marchés publics… A moins de deux semaines du scrutin, le Conseil national du numérique rappelle aux candidats les dossiers sensibles qui les attendent, notamment dans le cadre des négociations en cours avec les Etats-Unis.
