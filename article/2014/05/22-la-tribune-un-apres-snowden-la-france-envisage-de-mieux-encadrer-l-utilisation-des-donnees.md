---
site: La Tribune
title: "Un an après Snowden, la France envisage de mieux encadrer l’utilisation des données"
author: Delphine Cuny
date: 2014-05-22
href: http://www.latribune.fr/technos-medias/20140522trib000831369/un-an-apres-snowden-la-france-envisage-de-mieux-encadrer-l-utilisation-des-donnees.html
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> Elus et professionnels du numérique s’interrogent au Sénat sur la nécessité de réformer le cadre juridique des interceptions de données, près d’un an après le scandale Prism. Le futur projet de loi numérique devrait permettre de répondre à certains enjeux.
