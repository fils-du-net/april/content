---
site: Le Courrier Parlementaire
title: "Logiciel libre: systématiquement écarté des choix"
date: 2014-05-09
href: http://www.courrierparlementaire.com/article/11898
tags:
- Logiciels privateurs
- Administration
- Économie
- Standards
- International
---

> Plusieurs freins à l’utilisation du logiciel libre au sein de l’appareil gouvernemental existent toujours, malgré des mesures prises pour promouvoir son utilisation. C’est ce qu’on fait valoir plusieurs interlocuteurs contactés par Le Courrier parlementaire©. Ils proposent aussi des solutions pour favoriser la percée du logiciel libre dans l’État.
