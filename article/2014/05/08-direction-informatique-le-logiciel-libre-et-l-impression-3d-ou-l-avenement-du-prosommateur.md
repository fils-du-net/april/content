---
site: Direction Informatique
title: "Le logiciel libre et l’impression 3D, ou l’avénement du «prosommateur»"
author: Laurent Bounin
date: 2014-05-08
href: http://www.directioninformatique.com/blogue/le-logiciel-libre-et-limpression-3d-ou-lavenement-du-prosommateur/27013
tags:
- Économie
- Partage du savoir
- Matériel libre
- Innovation
---

> Selon Jeremy Rifkin, la fin de la troisième révolution industrielle est à prévoir et le modèle du logiciel libre en serait l’un des artisans.
