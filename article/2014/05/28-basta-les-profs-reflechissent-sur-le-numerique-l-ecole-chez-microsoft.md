---
site: Basta!
title: "Les profs réfléchissent sur le numérique à l’école… chez Microsoft!"
author: Morgane Thimel
date: 2014-05-28
href: http://www.bastamag.net/Journee-sur-le-numerique-les-profs
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Éducation
- Innovation
- Europe
- International
---

> L’Académie de Paris organise sa «Journée de l’innovation et du numérique» aujourd’hui, ce 28 mai. L’occasion pour le corps enseignant de s’interroger sur la place de l’informatique dans le milieu scolaire. La manifestation sera ouverte à tous: les professeurs du premier et second degrés, les conseillers principaux d’éducation (CPE), les formateurs… et aussi les sponsors. Elle ne se déroulera pas dans une université ou dans une classique salle de conférence mais... au siège de Microsoft France à Issy-les-Moulineaux!
