---
site: Le Figaro.fr
title: "Bataille Oracle/Google sur les droits d'auteurs"
date: 2014-05-10
href: http://www.lefigaro.fr/flash-eco/2014/05/10/97002-20140510FILWWW00035-bataille-oraclegoogle-sur-les-droits-d-auteurs.php
tags:
- Entreprise
- Institutions
- Droit d'auteur
---

> Une cour d'appel américaine a relancé vendredi un bras de fer de longue haleine entre le spécialiste des logiciels Oracle et le géant internet Google sur d'éventuelles violations de droits d'auteurs sur des éléments du code de programmation Java. Oracle, propriétaire de Java depuis qu'il en a racheté le concepteur Sun Microsystems en 2010, a porté plainte il y a plusieurs années contre Google, qui utilise des éléments de Java (API, application programming interfaces) dans son système d'exploitation mobile Android.
