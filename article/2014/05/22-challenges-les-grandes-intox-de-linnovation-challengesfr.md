---
site: Challenges
title: "Les grandes \"intox\" de l'innovation Challenges.fr"
author: Olivier Ezratty
date: 2014-05-22
href: http://www.challenges.fr/high-tech/20140521.CHA4030/les-grandes-intox-de-l-innovation.html
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Innovation
- Promotion
- International
---

> Olivier Ezratty, blogueur et consultant high-tech, prend un malin plaisir à démonter et démasquer l'intox et la propagande en vigueur dans le monde du numérique. Extraits.
