---
site: Numerama
title: "TrueCrypt ferme pour insécurité, et conseille de faire confiance à... Microsoft"
author: Guillaume Champeau
date: 2014-05-29
href: http://www.numerama.com/magazine/29528-truecrypt-ferme-pour-insecurite-et-conseille-de-faire-confiance-a-microsoft.html
tags:
- Internet
- Vie privée
---

> L'affaire semble tellement improbable que beaucoup ont du mal à y croire. L'équipe anonyme de développement de TrueCrypt, un logiciel open-source permettant de créer facilement un espace de stockage chiffré, a publié sur le site officiel un message d'avertissement et une nouvelle version de TrueCrypt, qui assurent que TrueCrypt n'est pas sûr. Ils conseillent d'utiliser BitLocker, la solution propriétaire de Microsoft.
