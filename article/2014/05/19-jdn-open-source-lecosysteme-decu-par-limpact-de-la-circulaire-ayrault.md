---
site: JDN
title: "Open Source: l'écosystème déçu par l'impact de la circulaire Ayrault"
date: 2014-05-19
href: http://www.journaldunet.com/solutions/saas-logiciel/open-source-impact-de-la-circulaire-ayrault-0514.shtml
tags:
- Entreprise
- Administration
- Économie
- Institutions
---

> La dernière étude conduite par le Conseil national du Logiciel Libre au sein de la communauté Open Source révèle plusieurs déceptions de cette dernière sur le terrain politique.
