---
site: Next INpact
title: "Le CSA, le label et la bête de la riposte graduée"
author: Marc Rees
date: 2014-05-07
href: http://www.nextinpact.com/news/87418-le-csa-label-et-bete-riposte-graduee.htm
tags:
- Internet
- HADOPI
- Institutions
---

> Hier, à l'Assemblée nationale, Olivier Schrameck a présenté le rapport annuel du Conseil supérieur de l’audiovisuel à la Commission des affaires culturelles. Questionné par les députés, le président de l’autorité administrative indépendante est revenu brièvement sur la question de la fusion avec la Hadopi, un sujet évité dans ce dernier rapport.
