---
site: Le Monde.fr
title: "Monnaies électroniques, la mode du «Dark»"
author: Yves Eudes
date: 2014-05-28
href: http://www.lemonde.fr/pixels/article/2014/05/28/monnaies-electroniques-la-mode-du-dark_4424655_4408996.html
tags:
- Internet
- Économie
- Associations
- Innovation
- Vie privée
---

> Dark Market, darkcoins… des hacktivistes travaillent sur des systèmes de paiement totalement anonymes.
