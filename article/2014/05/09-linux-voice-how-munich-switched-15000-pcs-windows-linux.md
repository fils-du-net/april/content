---
site: Linux Voice
title: "How Munich switched 15,000 PCs from Windows to Linux"
author: Mike Saunders
date: 2014-05-09
href: http://www.linuxvoice.com/the-big-switch
tags:
- Entreprise
- Administration
- Économie
---

> (Le conseil municipal de Munich a migré 15 000 travailleurs de Windows à Linux. C'est une grande réussite pour le Logiciel Libre, qui a énormément bouleversé Microsoft. Nous avons rendu visite à la ville et nous avons parlé à Peter Hofmann, l'homme derrière la migration - lisez la suite pour tous les détails croustillants sur ce qui s'est bien passé, ce qui s'est mal passé, et ce qui a fait Steve Ballmer sueur...) Munich city council has migrated 15,000 workers from Windows to Linux. It’s a great success story for Free Software, and it upset Microsoft enormously.
