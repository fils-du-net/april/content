---
site: Mediapart
title: "La neutralité du net, ou les enjeux cachés de l’élection européenne…"
author: Cécile Monnier
date: 2014-05-03
href: http://blogs.mediapart.fr/blog/cecile-monnier/030514/la-neutralite-du-net-ou-les-enjeux-caches-de-l-election-europeenne
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- Europe
- International
---

> La rencontre internationale NetMundial des 23 et 24 avril derniers sur la gouvernance de l’Internet s’est achevée sur une véritable déception, avec la signature d’un document dans lequel n’apparait pas le principe de neutralité du net. Une absence qui inquiète, alors même que la Federal communications Commission (organisme chargé d’encadrer le net aux Etats-Unis) venait d’annoncer son intention d’avancer vers la création de débits différenciés pour les fournisseurs d’accès.
