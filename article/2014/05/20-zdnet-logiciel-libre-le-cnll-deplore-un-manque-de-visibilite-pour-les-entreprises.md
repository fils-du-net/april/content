---
site: ZDNet
title: "Logiciel libre: le CNLL déplore un manque de visibilité pour les entreprises"
author: Louis Adam
date: 2014-05-20
href: http://www.zdnet.fr/actualites/logiciel-libre-le-cnll-deplore-un-manque-de-visibilite-pour-les-entreprises-39801365.htm
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Institutions
- Brevets logiciels
- Promotion
- Informatique en nuage
---

> Le Conseil National du Logiciel Libre a réalisé une enquête auprès de ses adhérents autour des positions et demandes des acteurs du secteur. Plusieurs constats ont été tirés par l’étude, mais le manque de visibilité reste le problème central. Son porte-parole répond aux questions de ZDNet.fr.
