---
site: Bilan
title: "La biologie «Do It Yourself» s’installe en Suisse"
author: Fabrice Delaye
date: 2014-05-09
href: http://www.bilan.ch/techno-les-plus-de-la-redaction/la-biologie-do-it-yourself-sinstalle-en-suisse
tags:
- Matériel libre
- Innovation
- Licenses
- Sciences
---

> Inspiré par la culture du logiciel libre et les «makers», le biohacking ouvre une troisième voie entre l’académie et l’entreprise. Un espace romand vient d’être créé. La biotech pour tous?
