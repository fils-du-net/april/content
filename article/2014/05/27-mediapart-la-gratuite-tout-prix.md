---
site: Mediapart
title: "La gratuité à tout prix!"
author: Ilian Amar
date: 2014-05-27
href: http://blogs.mediapart.fr/blog/ilian-amar/270514/la-gratuite-tout-prix
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- April
- Éducation
---

> Autrefois on obtenait des "points" en faisant son plein d'essence chez Untel ou Trucmuche et on finissait par gagner des gadgets "gratuits". On les avait bien sûr payés mille fois sur ses factures d'essence... Personne n'était dupe du système ni du fait que les cadeaux étaient sans valeur. Tout cela n'allait pas bien loin...
