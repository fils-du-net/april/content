---
site: l'Humanité.fr
title: "Traité transatlantique: quels enjeux pour le numérique?"
author: Pierric Marissal
date: 2014-05-29
href: http://www.humanite.fr/traite-transatlantique-quels-enjeux-pour-le-numerique-538740
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Brevets logiciels
- Sciences
- ACTA
- Vie privée
---

> Deux questions fondamentales liées au numérique pourraient être négociées dans le traité transatlantique (Tafta): le libre commerce des données personnelles et un alignement de la propriété intellectuelle sur le copyright américain.
