---
site: Silicon.fr
title: "Solutions Linux 2014: L’Open Source raisonne Cloud et rêve des métiers"
author: Jacques Cheminat
date: 2014-05-21
href: http://www.silicon.fr/solutions-linux-2014-approche-en-metiers-lopen-source-94497.html
tags:
- Entreprise
- Innovation
- Informatique en nuage
---

> Le millésime 2014 du salon Solutions Linux est revenu sur la grande variété de l’univers de l’Open Source. BPM, moteur de recherche, base de données, méthode DevOps et cloud, sont au menu avec comme fil rouge la prise de conscience des besoins métiers.
