---
site: Numerama
title: "Hadopi: Comment le rapport MIQ propose de contourner la Justice"
author: Guillaume Champeau
date: 2014-05-12
href: http://www.numerama.com/magazine/29334-hadopi-comment-le-rapport-miq-propose-de-contourner-la-justice.html
tags:
- Internet
- Administration
- HADOPI
- Neutralité du Net
---

> Le rapport contre la contrefaçon commerciale remis ce lundi par Mireille Imbert-Quaretta propose de remplacer l'action judiciaire par l'action privée des intermédiaires financiers et des hébergeurs, appuyée ou provoquée par les services de l'Etat.
