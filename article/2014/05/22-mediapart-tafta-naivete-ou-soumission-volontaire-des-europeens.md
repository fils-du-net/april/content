---
site: Mediapart
title: "TAFTA: Naïveté ou soumission volontaire des Européens."
author: Jean-Paul Baquiast
date: 2014-05-22
href: http://blogs.mediapart.fr/blog/jean-paul-baquiast/220514/tafta-naivete-ou-soumission-volontaire-des-europeens
tags:
- TAFTA
- Entreprise
- Économie
- Institutions
- ACTA
---

> Le rouleau compresseur du TAFTA poursuit sa marche inexorable, malgré les nombreuses oppositions tant techniques que plus globalement politiques qu'il suscite dans différents pays européens.
