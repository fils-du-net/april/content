---
site: 01net.
title: "Isolé, Mozilla renonce à la bataille contre les DRM dans Firefox"
author: Pierre Fontaine
date: 2014-05-15
href: http://www.01net.com/editorial/619800/isole-mozilla-renonce-a-la-bataille-contre-les-drm-dans-firefox
tags:
- Internet
- Associations
- DRM
- Standards
---

> Désormais intégrés aux standards du Web, les DRM s’embarquent au cœur même des navigateurs. Dernier combattant, Mozilla a annoncé renoncer à la lutte et intégrera ces verrous numériques dans Firefox.
