---
site: 20minutes.fr
title: "Axelle Lemaire, secrétaire d'État au Numérique: «On apprend l’anglais, le chinois, il faut apprendre à coder!»"
date: 2014-05-14
href: http://www.20minutes.fr/high-tech/1375261-posez-vos-questions-a-axelle-lemaire-secretaire-d-etat-au-numerique
tags:
- Entreprise
- Internet
- Institutions
- Éducation
- Neutralité du Net
- Open Data
---

> Quels sont les projets d’Axelle Lemaire pour la France? Compte-t-elle développer l’open data? Comment se positionne-t-elle dans le débat sur la «neutralité du Net»? Quid de l’arrivée de Netflix? Comment réformer le financement pour faire de la France une «start-up République»?…
