---
site: Developpez.com
title: "Mozilla contraint d'intégrer le support des DRM dans Firefox"
author: Stéphane le calme
date: 2014-05-15
href: http://www.developpez.com/actu/71270/Mozilla-contraint-d-integrer-le-support-des-DRM-dans-Firefox-est-ce-la-meilleure-decision-pour-l-entreprise
tags:
- Internet
- Associations
- DRM
- Standards
---

> En mars de l’année dernière, le W3C avait publié un premier brouillon d’EME (Encrypted Media Extensions), un mécanisme qui ajoutait aux balises  ou  de HTMLMediaElement des fonctions de contrôle de lecture pour les contenus protégés. En clair, cette technologie devrait permettre à des acteurs comme Google, Netflix, Amazon et autres de diffuser leurs contenus avec DRM. Parmi les défenseurs de ce projet figurait à la première loge Tim Berners-Lee, le «père du web», qui estimait qu’intégrer ces technologies permettrait d’éviter un Web fractionné en multiples sous-standards privés.
