---
site: Zone Numerique
title: "TrueCrypt: pourquoi un arrêt soudain du logiciel open source de chiffrement?"
author: François Giraud
date: 2014-05-30
href: http://www.zone-numerique.com/truecrypt-pourquoi-un-arret-soudain-du-logiciel-open-source-de-chiffrement.html
tags:
- Internet
- Logiciels privateurs
- Institutions
- Informatique-deloyale
---

> Le logiciel open source de chiffrement TrueCrypt contiendrait des vulnérabilités. Les développeurs indiquent qu’il faut migrer vers le logiciel BitLocker de Microsoft.
