---
site: Silicon.fr
title: "La Chine boude Windows au profit de Linux"
author: David Feugey
date: 2014-05-28
href: http://www.silicon.fr/chine-boude-windows-au-profit-linux-94672.html
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- International
---

> Les différends entre les États-Unis et la Chine pourraient porter un coup dur à la pénétration de Microsoft sur le marché chinois. Linux est en embuscade…
