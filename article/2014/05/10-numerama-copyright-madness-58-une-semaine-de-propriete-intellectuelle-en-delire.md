---
site: Numerama
title: "Copyright Madness (#58): une semaine de propriété intellectuelle en délire!"
author: Julien L.
date: 2014-05-10
href: http://www.numerama.com/magazine/29325-copyright-madness-58-une-semaine-de-propriete-intellectuelle-en-delire.html
tags:
- Internet
- HADOPI
- Droit d'auteur
---

> Chaque semaine, Numerama publie une sélection de sujets composée par Lionel Maurel et Thomas Fourmeux présentant les écarts les plus marquants dans le domaine de la propriété intellectuelle. Et pour cette 58ème édition, la revue de presse est gratinée! À noter aussi, l'arrivée d'une nouvelle rubrique baptisée "Monument Madness" sur l'absence de liberté de panorama pour les photographies de monuments.
