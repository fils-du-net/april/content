---
site: Numerama
title: "FreeBSD a sa faille Heartbleed. Et beaucoup de systèmes utilisent FreeBSD"
author: Guillaume Champeau
date: 2014-05-01
href: http://www.numerama.com/magazine/29253-freebsd-a-sa-faille-heartbleed-et-beaucoup-de-systemes-utilisent-freebsd.html
tags:
- Internet
---

> FreeBSD a découvert qu'une faille dans sa gestion de flux TCP permettait, de façon "extrêmement difficile", d'obtenir à distance une copie d'informations stockées en mémoire dans le noyau. Mac OS X fait partie des systèmes qui utilisent le code réseau de FreeBSD.
