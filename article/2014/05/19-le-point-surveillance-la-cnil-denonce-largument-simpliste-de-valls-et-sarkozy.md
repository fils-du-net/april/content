---
site: Le Point
title: "Surveillance: la Cnil dénonce l'argument \"simpliste\" de Valls et Sarkozy "
date: 2014-05-19
href: http://www.lepoint.fr/chroniqueurs-du-point/guerric-poncet/surveillance-pour-la-cnil-valls-comme-sarkozy-ont-use-d-arguments-simplistes-19-05-2014-1825163_506.php
tags:
- Internet
- Institutions
- Vie privée
---

> Le gendarme de la vie privée sonne la charge contre "l'argument sans cesse ressassé du rien à se reprocher, rien à cacher" invoqué pour surveiller les citoyens.
