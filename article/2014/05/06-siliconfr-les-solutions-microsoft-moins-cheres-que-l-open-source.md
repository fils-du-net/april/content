---
site: Silicon.fr
title: "Les solutions Microsoft moins chères que l’Open Source"
author: Jacques Cheminat
date: 2014-05-06
href: http://www.silicon.fr/les-solutions-microsoft-moins-cher-lopen-source-94187.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- International
---

> En plein débat sur l’adoption de solutions Open Source en Angleterre, le DSI d’une collectivité locale britannique jette un pavé dans la mare en affirmant que Microsoft se révèle moins cher que les offres du libre.
