---
site: JDN
title: "Le DRM arrive dans Firefox: décision délicate pour Mozilla"
author: Antoine Crochet-Damais
date: 2014-05-15
href: http://www.journaldunet.com/solutions/dsi/un-drm-dans-firefox-0514.shtml
tags:
- Internet
- Associations
- DRM
- Standards
---

> La fondation Mozilla va prochainement intégrer le standard de DRM de HTML5. Il permettra aux sites d'autoriser ou non l'accès à leur contenu en fonction de droit d'auteur ou de licence.
