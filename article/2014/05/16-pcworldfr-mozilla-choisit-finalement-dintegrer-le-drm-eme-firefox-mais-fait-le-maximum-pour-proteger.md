---
site: PCWorld.fr
title: "Mozilla choisit finalement d'intégrer le DRM EME à Firefox, mais fait le maximum pour protéger les internautes"
author: Mathieu Chartier
date: 2014-05-16
href: http://www.pcworld.fr/logiciels/actualites,mozilla-firefox-drm-eme-cdm,548899,1.htm
tags:
- Internet
- Associations
- DRM
- Standards
---

> Mozilla n'aime pas les DRM, mais dit ne pas avoir vraiment le choix de supporter celui utilisé par de grands diffuseurs de vidéo sur Internet s'il ne veut pas voir fuir ses utilisateurs.
