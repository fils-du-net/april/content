---
site: lesoir.be
title: "Des résultats incomplets dus à un bug: «C’est la mort du vote électronique», dit Maingain"
author: Sandra Durieux, P. LA.
date: 2014-05-26
href: http://www.lesoir.be/554497/article/actualite/belgique/elections-2014/2014-05-26/des-resultats-incomplets-dus-un-bug-c-est-mort-du-vote-electronique-
tags:
- Administration
- Institutions
- Vote électronique
- International
---

> Un bug pertube fortement la diffusion des résultats électoraux. En cause, un gros problème informatique qui a touché les communes (wallonnes et bruxelloises) votant avec l’ancien système électronique.
