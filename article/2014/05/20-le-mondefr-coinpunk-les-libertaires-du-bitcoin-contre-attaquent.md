---
site: Le Monde.fr
title: "CoinPunk, les libertaires du bitcoin contre-attaquent"
author: Yves Eudes
date: 2014-05-20
href: http://www.lemonde.fr/pixels/article/2014/05/20/coinpunk-les-libertaires-du-bitcoin-contre-attaquent_4415623_4408996.html
tags:
- Entreprise
- Internet
- Économie
- Innovation
---

> Loin des scandales liés à la monnaie virtuelle, un petit groupe de militants continue de travailler à un projet de paiement en ligne grand public. Et sans banques.
