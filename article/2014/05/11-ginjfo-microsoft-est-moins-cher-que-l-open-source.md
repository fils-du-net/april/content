---
site: GinjFo
title: "Microsoft est moins cher que l’Open Source?"
author: Jérôme Gianoli 
date: 2014-05-11
href: http://www.ginjfo.com/actualites/politique-et-economie/microsoft-moins-cher-lopen-source-20140511
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- International
---

> La politique tarifaire de Microsoft est critiquable face aux solutions concurrentes sous Linux et il n’y a pas de doute que, pour certains, l’achat d’applications proposée par la société de Redmond est impossible sans avoir prévu un budget spécifique et conséquent. Mais Microsoft est-il vraiment plus cher que l’Open Source? La réponse n’est pas si évidente.
