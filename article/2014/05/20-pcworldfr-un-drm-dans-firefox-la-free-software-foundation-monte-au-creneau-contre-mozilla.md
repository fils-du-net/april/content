---
site: PCWorld.fr
title: "Un DRM dans Firefox: la Free Software Foundation monte au créneau contre Mozilla"
author: Mathieu Chartier
date: 2014-05-20
href: http://www.pcworld.fr/logiciels/actualites,mozilla-firefox-drm-eme-cdm-adobe-free-software-foundation-fsf,548979,1.htm
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Associations
- DRM
---

> Mozilla dit être contraint d'accepter d'intégrer les DRM EME dans son navigateur Internet. La FSF lui répond qu'elle n'avait pas à se forcer, et que même justifiée, sa décision n'est pas éthiquement acceptable.
