---
site: Computerworld UK
title: "Software Patents in Denmark: To Be or Not To Be?"
author: Glyn Moody
date: 2014-05-08
href: http://blogs.computerworlduk.com/open-enterprise/2014/05/software-patents-in-denmark-to-be-or-not-to-be/index.htm
tags:
- Économie
- Institutions
- Brevets logiciels
- Europe
- English
---

> (Il y a de très bonnes raisons de craindre que l'arrivée imminente du brevent unitaire va nous apporter exactement les mêmes problèmes [qu'aux US) en Europe, et pourtant il n'y a eu presqu'aucune discussion à leur propros, particulièrement ici au Royaume Uni...
