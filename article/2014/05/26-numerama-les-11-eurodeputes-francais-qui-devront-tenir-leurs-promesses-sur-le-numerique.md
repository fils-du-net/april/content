---
site: Numerama
title: "Les 11 eurodéputés français qui devront tenir leurs promesses sur le numérique"
author: Guillaume Champeau
date: 2014-05-26
href: http://www.numerama.com/magazine/29492-les-11-eurodeputes-francais-qui-devront-tenir-leurs-promesses-sur-le-numerique.html
tags:
- Internet
- Institutions
- Europe
---

> 11 députés européens élus dimanche en France se sont engagés à respecter une charte des droits numériques, en 10 points. Les voici.
