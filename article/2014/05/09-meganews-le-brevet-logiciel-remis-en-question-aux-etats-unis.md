---
site: meganews
title: "Le brevet logiciel remis en question aux Etats-Unis?"
author: itmag11
date: 2014-05-09
href: http://www.itmag-dz.com/2014/05/le-brevet-logiciel-remis-en-question-aux-etats-unis
tags:
- Entreprise
- Économie
- Institutions
- Brevets logiciels
- International
---

> Les neuf juges de la Cour suprême des États-Unis vont entamer aujourd’hui une série d’auditions au sujet du brevet logiciel. Les juges de la Cour suprême des États-Unis sont intervenus dans un débat qui fait rage sur le logiciel et le brevet. Le principe même d’un dépôt de brevet sur un algorithme ou un processus logiciel […]
