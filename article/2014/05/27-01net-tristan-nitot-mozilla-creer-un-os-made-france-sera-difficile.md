---
site: 01net.
title: "Tristan Nitot (Mozilla): «Créer un OS made in France sera difficile»"
author: Gilbert Kallenborn
date: 2014-05-27
href: http://www.01net.com/editorial/620564/tristan-nitot-mozilla-creer-un-os-made-in-france-sera-difficile
tags:
- Internet
- Institutions
- Innovation
- Standards
---

> Inutile de réinventer la roue pour assurer sa souveraineté numérique, estime le porte-parole de Mozilla. Il serait plus judicieux de s’appuyer sur les projets open source existants.
