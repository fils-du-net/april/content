---
site: Next INpact
title: "Au ministère de la Culture, craintes et revendications sur le chantier du droit d'auteur"
author: Marc Rees
date: 2014-11-20
href: http://www.nextinpact.com/news/91011-au-ministere-culture-craintes-et-revendications-sur-chantier-droit-d-auteur.htm
tags:
- Internet
- Partage du savoir
- Institutions
- Droit d'auteur
- Europe
---

> Mardi, Fleur Pellerin est venue au Conseil Supérieur de la Propriété Littéraire et Artistique, qui tenait là sa séance plénière. À cette occasion, elle a salué les travaux du professeur de droit Pierre Sirinelli qui présentait ce jour son rapport d'étape (notre actualité) planchant sur une éventuelle révision de la directive de 2001 sur le droit d’auteur et les droits voisins.
