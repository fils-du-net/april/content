---
site: Numerama
title: "L'Allemagne pourrait exiger le code source de ses fournisseurs"
author: Guillaume Champeau
date: 2014-11-04
href: http://www.numerama.com/magazine/31178-l-allemagne-pourrait-exiger-le-code-source-de-ses-fournisseurs.html
tags:
- Entreprise
- Internet
- Institutions
- International
- Vie privée
---

> Pour renforcer la sécurité et la confidentialité des communications, l'Allemagne préparerait un projet de loi pour obliger les prestataires de services à donner l'accès à leur code lorsqu'ils ont des contrats avec l'Etat ou des entreprises stratégiques.
