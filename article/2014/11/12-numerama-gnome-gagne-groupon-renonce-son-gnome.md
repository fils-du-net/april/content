---
site: Numerama
title: "GNOME a gagné, Groupon renonce à son Gnome"
author: Julien L.
date: 2014-11-12
href: http://www.numerama.com/magazine/31247-gnome-a-gagne-groupon-renonce-a-son-gnome.html
tags:
- Entreprise
- Économie
- HADOPI
---

> La société Groupon et la fondation Gnome sont parvenues à un terrain d'entente sur l'utilisation du nom Gnome. L'entreprise américaine a accepté de renoncer à cette marque pour désigner son futur système d'exploitation pour marchands.
