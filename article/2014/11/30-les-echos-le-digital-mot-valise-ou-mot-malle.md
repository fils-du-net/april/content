---
site: Les Echos
title: "Le Digital, mot valise ou mot malle?"
author: Dominique Turcq
date: 2014-11-30
href: http://www.lesechos.fr/idees-debats/cercle/cercle-119295-le-digital-mot-valise-ou-mot-malle-1069607.php
tags:
- Internet
- Innovation
---

> Le digital est l’un des mots-valises les plus luxueux du vocabulaire managérial récent. Ce n’est pas un mot valise, c’est un mot malle Louis Vuitton plein de tiroirs et de recoins.
