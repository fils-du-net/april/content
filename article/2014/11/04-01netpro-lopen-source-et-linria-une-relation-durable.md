---
site: 01netPro.
title: "L'Open source et l'Inria, une relation durable"
author: Marie Jung
date: 2014-11-04
href: http://pro.01net.com/editorial/630702/l-open-source-et-l-inria-une-relation-durable
tags:
- Administration
- Sensibilisation
- Innovation
- Sciences
---

> Depuis 30 ans, l'engagement de l'Inria en faveur des logiciels libres ne faiblit pas. Présent à l'Open World Forum, l’institut de recherche a fait le panorama de ses différentes initiatives dans le domaine.
