---
site: 20minutes.fr
title: "Le virage open-source et gratuit de Microsoft"
author: Philippe Berry
date: 2014-11-13
href: http://www.20minutes.fr/high-tech/1479639-20141113-virage-open-source-gratuit-microsoft
tags:
- Entreprise
- Logiciels privateurs
- Droit d'auteur
- Innovation
---

> Il est loin le temps où Steve Ballmer déclarait que Linux était «un cancer». Son successeur à la tête de Microsoft, Satya Nadella, est en train de transformer l'ADN de l'entreprise en misant, sur certains front, sur une approche multi-plateformes, open-source et gratuite.
