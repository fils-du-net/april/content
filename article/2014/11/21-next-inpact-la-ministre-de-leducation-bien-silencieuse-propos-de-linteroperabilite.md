---
site: Next INpact
title: "La ministre de l'Éducation bien silencieuse à propos de l'interopérabilité"
author: Xavier Berne
date: 2014-11-21
href: http://www.nextinpact.com/news/91013-la-ministre-l-education-bien-silencieuse-a-propos-l-interoperabilite.htm
tags:
- Internet
- Interopérabilité
- April
- HADOPI
- Institutions
- Éducation
- Promotion
---

> Pas un seul mot. Interrogées avant-hier par une députée qui souhaitait savoir comment le ministère de l’Enseignement supérieur entendait favoriser le logiciel libre et l'interopérabilité notamment au sein des universités, la ministre Najat Vallaud-Belkacem et sa secrétaire d’État Geneviève Fioraso sont restées bien muettes.
