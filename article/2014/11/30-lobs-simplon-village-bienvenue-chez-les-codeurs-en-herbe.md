---
site: L'OBS
title: "Simplon Village: bienvenue chez les codeurs en herbe"
author: Cyril Bonnet
date: 2014-11-30
href: http://tempsreel.nouvelobs.com/education/20141127.OBS6382/simplon-village-bienvenue-chez-les-codeurs-en-herbe.html
tags:
- Entreprise
- Éducation
---

> Au cœur du Perche, une école numérique accueille gratuitement des gens de tous horizons pour les former au code informatique. Et les remettre dans le circuit du travail.
