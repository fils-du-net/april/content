---
site: Challenges
title: "\"Open innovation\": quand les grands groupes lorgnent les start-up"
author: Olivier Ezratty
date: 2014-11-06
href: http://www.challenges.fr/tribunes/20141104.CHA9800/innovation-ouverte-quand-les-grands-groupes-lorgnent-les-start-up.html
tags:
- Entreprise
- Innovation
---

> Olivier Ezratty, consultant high-tech, explique comment les grands groupes lorgnent sur les start-up pour faire éclore l'innovation à bon compte. Opération gagnant-gagnant?
