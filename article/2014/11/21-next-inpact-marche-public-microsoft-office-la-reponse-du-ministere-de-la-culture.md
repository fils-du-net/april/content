---
site: Next INpact
title: "Marché public Microsoft Office: la réponse du ministère de la Culture"
author: Marc Rees
date: 2014-11-21
href: http://www.nextinpact.com/news/91036-marche-public-microsoft-office-reponse-ministere-culture.htm
tags:
- Logiciels privateurs
- Administration
- Interopérabilité
- Marchés publics
---

> Après publication de notre article ce matin, le ministère de la Culture a finalement trouvé le temps de nous expliquer ce marché public visant l’acquisition de 300 licences Microsoft Office, sans concurrence possible avec une autre suite.
