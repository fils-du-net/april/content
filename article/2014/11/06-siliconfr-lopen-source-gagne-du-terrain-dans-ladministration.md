---
site: Silicon.fr
title: "L'Open Source gagne du terrain dans l'administration"
author: Alain Merle
date: 2014-11-06
href: http://www.silicon.fr/open-source-administration-bases-donnees-cloud-tribune-101296.html
tags:
- Administration
- Sensibilisation
- Standards
- Informatique en nuage
---

> L’Open Source infuse de plus en plus de couches d’infrastructures au sein de l’administration. Ce mouvement est doublement bénéfique: il limite l’adhérence aux grandes technologies propriétaires et laisse libre court à l’expérimentation, explique la DSI de l’Etat dans cette tribune.
