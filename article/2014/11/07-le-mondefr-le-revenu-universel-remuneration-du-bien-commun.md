---
site: Le Monde.fr
title: "Le revenu universel, rémunération du bien commun"
author: Jean-Eric Hyafil
date: 2014-11-07
href: http://www.lemonde.fr/emploi/article/2014/11/07/le-revenu-universel-remuneration-du-bien-commun_4520403_1698637.html
tags:
- Économie
---

> La notion de revenu d’existence, objet de controverse entre économistes, n’est pas une nouveauté dans l’histoire de la pensée économique, rappelle Jean-Eric Hyafil, membre fondateur du Mouvement français pour un revenu de base
