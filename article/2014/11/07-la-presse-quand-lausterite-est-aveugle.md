---
site: La Presse
title: "Quand l'austérité est aveugle"
author: Mylène Moisan
date: 2014-11-07
href: http://www.lapresse.ca/le-soleil/opinions/chroniqueurs/201411/06/01-4816600-quand-lausterite-est-aveugle.php
tags:
- Administration
- Accessibilité
- International
---

> Miguel Ross était tout content, il avait décroché un super boulot. Il allait aider le gouvernement à économiser de l'argent en trouvant des logiciels libres, donc gratuits, pour remplacer ceux qui coûtent une petite fortune à l'État.
