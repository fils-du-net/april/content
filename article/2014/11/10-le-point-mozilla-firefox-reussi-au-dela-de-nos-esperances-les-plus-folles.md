---
site: Le Point
title: "Mozilla: \"Firefox a réussi au-delà de nos espérances les plus folles\""
author: Guerric Poncet
date: 2014-11-10
href: http://www.lepoint.fr/chroniqueurs-du-point/guerric-poncet/mozilla-firefox-a-reussi-au-dela-de-nos-esperances-les-plus-folles-10-11-2014-1879940_506.php
tags:
- Internet
- Innovation
- Standards
---

> Le navigateur libre a 10 ans! Rencontre avec Tristan Nitot, patron de Mozilla en Europe, pour évoquer la stratégie mobile et l'accord avec Google.
