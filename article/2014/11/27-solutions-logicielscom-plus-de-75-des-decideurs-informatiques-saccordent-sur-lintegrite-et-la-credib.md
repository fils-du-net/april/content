---
site: "Solutions-Logiciels.com"
title: "Plus de 75 % des décideurs informatiques s'accordent sur l'intégrité et la crédibilité des logiciels open source"
author: Juliette Paoli
date: 2014-11-27
href: http://www.solutions-logiciels.com/actualites.php?actu=15159
tags:
- Entreprise
- Sensibilisation
---

> Selon une enquête menée par l’Institut Ponemon et Zimbra, les spécialistes en informatique préfèrent les logiciels open source aux applications propriétaires pour leurs performances supérieures en termes de continuité de l'activité, de qualité et de contrôle.
