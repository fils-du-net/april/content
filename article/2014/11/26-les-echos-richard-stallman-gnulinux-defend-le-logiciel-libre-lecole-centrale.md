---
site: Les Echos
title: "Richard Stallman (GNU/Linux) défend le logiciel libre à l'Ecole Centrale"
author: Florian Debes
date: 2014-11-26
href: http://business.lesechos.fr/directions-numeriques/0203965181304-richard-stallman-deconseille-le-cloud-public-aux-entreprises-105681.php
tags:
- Entreprise
- Marchés publics
- Philosophie GNU
- Promotion
- Informatique en nuage
- Europe
- International
---

> Richard Stallman, grand défenseur du logiciel libre et inventeur du système d’exploitation GNU/Linux, tenait conférence, vendredi 21 novembre 2014, devant les anciens élèves des Ecoles Centrales et Supélec. Il critique notamment l'utilisation des logiciels Saas.
