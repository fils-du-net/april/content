---
site: La gazette.fr
title: "L’Etat start-up n’est-il qu’un slogan?"
author: Sabine Blanc
date: 2014-11-13
href: http://www.lagazettedescommunes.com/292977/letat-start-up-nest-il-quun-slogan
tags:
- Internet
- Administration
- Économie
- Institutions
- Innovation
---

> La start-up est un champ sémantique et symbolique mis en avant par Thierry Mandon, le secrétaire d’Etat en charge de la Réforme de l’Etat et de la Simplification, comme modèle de fonctionnement à suivre. Certains projets s’en revendiquent déjà. Cette inspiration dans l’air du temps a ses limites si l’on analyse ce que recouvre vraiment le terme.
