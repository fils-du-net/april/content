---
site: ZDNet France
title: "Formats ouverts à l’école: l’April lance un appel"
author: Louis Adam
date: 2014-11-18
href: http://www.zdnet.fr/actualites/formats-ouverts-a-l-ecole-l-april-lance-un-appel-39809713.htm
tags:
- Interopérabilité
- April
- Éducation
- Standards
---

> L’April a lancé un appel en faveur de l’utilisation de formats ouverts au sein de l’éducation nationale. Leur cheval de bataille: l’interopérabilité des documents, qui se doivent d’être lisibles par tous les utilisateurs. Et bien sûr, protéger les élèves des «stratégies d’enfermement» des gros éditeurs.
