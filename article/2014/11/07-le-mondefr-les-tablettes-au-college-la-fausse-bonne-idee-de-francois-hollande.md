---
site: Le Monde.fr
title: "Les tablettes au collège, la fausse bonne idée de François Hollande"
author: Damien Leloup
date: 2014-11-07
href: http://www.lemonde.fr/pixels/article/2014/11/07/les-tablettes-au-college-la-fausse-bonne-idee-de-francois-hollande_4520005_4408996.html
tags:
- Administration
- Institutions
- Associations
- Éducation
- Promotion
---

> «Des cours de codage» et «une tablette et une formation au numérique» pour tous les élèves de cinquième à partir de la rentrée 2016: voilà les deux principaux points du «plan numérique» pour l'école qu'a précisé François Hollande, jeudi 6 novembre sur TF1. Les tablettes au collège, c'est presque une spécialité du président de la République: la Corrèze, son fief, équipe depuis 2010 les collégiens en iPad, la tablette d'Apple, dans le cadre d'un programme pilote qui prévoyait initialement d'équiper les élèves d'ordinateurs portables.
