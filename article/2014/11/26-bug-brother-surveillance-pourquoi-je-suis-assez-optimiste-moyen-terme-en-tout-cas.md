---
site: Bug Brother
title: "Surveillance: pourquoi je suis assez optimiste (à moyen terme en tout cas)"
author: Jean-Marc Manach
date: 2014-11-26
href: http://bugbrother.blog.lemonde.fr/2014/11/26/surveillance-pourquoi-je-suis-assez-optimiste-a-moyen-terme-en-tout-cas
tags:
- Internet
- Institutions
- International
- Vie privée
---

> Dans le prolongement de l’affaire Snowden, la revue Risques de la Fédération française des sociétés d'assurance m'a invité, le 24 juin dernier, à participer à une table ronde réunissant également Philippe Aigrain (@balaitous sur Twitter), co-fondateur de La Quadrature du Net, chercheur et essayiste, Antoine Lefébure (@segalen), auteur de L'affaire Snowden (Ed. La Découverte), Pierre-Olivier Sur, bâtonnier de Paris et Thierry van Santen, directeur général d’Allianz Global Corporate &amp; Specialty SE France.
