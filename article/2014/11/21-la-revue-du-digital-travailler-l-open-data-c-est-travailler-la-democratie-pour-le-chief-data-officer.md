---
site: La Revue du Digital
title: "“Travailler l’Open Data, c’est travailler la démocratie” pour le Chief Data Officer de la France"
author: William El Kaim
date: 2014-11-21
href: http://www.larevuedudigital.com/2014/11/actu/travailler-lopen-data-cest-travailler-la-democratie-pour-le-chief-data-officer-de-la-france
tags:
- Internet
- Administration
- Innovation
- Open Data
---

> Les projets se multiplient au sein d’Etalab, le bras armé de l’état pour sa politique Open Data. Les premiers résultats sont désormais disponibles soit sous forme d’ouverture de données à destination de nouveaux écosystèmes soit sous la forme de nouvelles applications disponibles pour les citoyens.
