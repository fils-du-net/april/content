---
site: ZDNet
title: "Créatifs libres et open source: les lauréats étudiants de l'Open World Forum"
author: Thierry Noisette
date: 2014-11-09
href: http://www.zdnet.fr/actualites/creatifs-libres-et-open-source-les-laureats-etudiants-de-l-open-world-forum-39809259.htm
tags:
- Internet
- Accessibilité
- Associations
- Éducation
- Innovation
- Neutralité du Net
---

> Six projets, nominés à la Student DemoCup, ont été présentés à l'OWF. Les lauréats ont pour objet la mesure de la neutralité du Net, un éditeur visuel pour jeux vidéo 3D et une appli pour les personnes ayant des troubles d’élocution graves.
