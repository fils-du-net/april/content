---
site: Les Echos
title: "Comment le projet d’une poignée de chômeurs a transformé Internet"
author: Nicolas Rauline
date: 2014-11-07
href: http://www.lesechos.fr/journal20141107/lec2_high_tech_et_medias/0203918239996-comment-le-projet-dune-poignee-de-chomeurs-a-transforme-internet-1061959.php
tags:
- Entreprise
- Internet
- Innovation
---

> Né dans l’ombre de Netscape, repris par ses jeunes développeurs, Firefox a trouvé son public et réussi à imposer son modèle.
