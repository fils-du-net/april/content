---
site: Vox
title: "The courts set a new record for rejecting software patents in 2014"
author: Timothy B. Lee
date: 2014-11-17
href: http://www.vox.com/2014/11/17/7222807/software-patent-invalid-record
tags:
- Institutions
- Brevets logiciels
- International
- English
---

> (Les tribunaux ont récemment été de plus en plus hostiles aux brevets logiciels) Courts have recently grown increasingly hostile to software patents. A June Supreme Court ruling significantly limited the kinds of software inventions that are eligible for patent protection. And even before that ruling, there had been a dramatic increase in the number of legal decisions holding that software-related inventions were unpatentable.
