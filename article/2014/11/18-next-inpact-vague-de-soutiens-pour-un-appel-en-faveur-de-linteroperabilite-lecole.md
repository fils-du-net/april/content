---
site: Next INpact
title: "Vague de soutiens pour un appel en faveur de l'interopérabilité à l'école"
author: Xavier Berne
date: 2014-11-18
href: http://www.nextinpact.com/news/90962-vague-soutiens-pour-appel-en-faveur-l-interoperabilite-a-ecole.htm
tags:
- Administration
- Interopérabilité
- April
- Institutions
- Éducation
- Standards
---

> Depuis la semaine dernière, l’association de promotion du logiciel libre (April) invite les internautes à se rallier à un appel en faveur de l’interopérabilité au sein de l’Éducation nationale. L’objectif est d’arriver à imposer l’utilisation de formats ouverts «pour travailler mieux, plus efficacement [et] pour la qualité de notre enseignement». Cette question capitale fait néanmoins figure d’absente au grand «plan numérique» promis par François Hollande pour l'école.
