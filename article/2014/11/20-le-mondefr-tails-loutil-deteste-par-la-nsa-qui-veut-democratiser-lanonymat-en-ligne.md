---
site: Le Monde.fr
title: "Tails, l'outil détesté par la NSA, qui veut démocratiser l'anonymat en ligne"
author: Amaëlle Guiton
date: 2014-11-20
href: http://www.lemonde.fr/pixels/article/2014/11/20/tails-l-outil-deteste-par-la-nsa-qui-veut-democratiser-l-anonymat-en-ligne_4514650_4408996.html
tags:
- Internet
- Innovation
- Vie privée
---

> Nous avons rencontré les développeurs d'un outil craint par la NSA, utilisé par Edward Snowden et de plus en plus d'internautes désireux de se protéger sur Internet.
