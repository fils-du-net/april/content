---
site: "cio-online.com"
title: "Les logiciels libres populaires d'abord pour leur qualité avant leur coût"
author: Bertrand Lemaire
date: 2014-11-25
href: http://www.cio-online.com/actualites/lire-les-logiciels-libres-populaires-d-abord-pour-leur-qualite-avant-leur-cout-7231.html
tags:
- Entreprise
- Sensibilisation
- Innovation
---

> Une enquête menée par l'Institut Ponemon pour le compte de Zimbra confirme que les logiciels libres sont appréciés d'abord pour leur qualité avant de l'être pour leur coût.
