---
site: Next INpact
title: "Firefox se désintoxique de Google et s'associe à Yahoo outre-Atlantique"
author: David Legrand
date: 2014-11-20
href: http://www.nextinpact.com/news/91001-firefox-se-desintoxique-google-et-sassocie-a-yahoo-outre-atlantique.htm
tags:
- Entreprise
- Internet
- Économie
- HADOPI
---

> Mozilla vient de fêter ses dix ans et d'annoncer quelques nouveautés. Mais la fondation semble surtout décidée à revoir son modèle économique afin de réduire sa dépendance vis-à-vis de Google. Ainsi, après avoir introduit des tuiles publicitaires dans Firefox, elle s'est associée avec Yahoo et d'autres services.
