---
site: Next INpact
title: "Le ministère de la Culture veut 300 licences Microsoft Office"
author: Marc Rees
date: 2014-11-21
href: http://www.nextinpact.com/news/90971-le-ministere-culture-veut-300-licences-microsoft-office.htm
tags:
- Logiciels privateurs
- Administration
- Interopérabilité
- Marchés publics
---

> Le ministère de la Culture a lancé le 7 novembre dernier un appel d’offres pour acquérir jusqu’à 300 licences Microsoft Office. Passé par le secrétariat général, et spécialement à la sous-direction des systèmes d’information, ce marché est certes modeste, mais il soulève néanmoins quelques questions.
