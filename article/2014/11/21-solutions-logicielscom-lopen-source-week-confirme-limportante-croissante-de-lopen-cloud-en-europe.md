---
site: "Solutions-Logiciels.com"
title: "L'Open Source Week confirme l'importante croissante de l'open cloud en Europe"
author: Olivier Bouzereau
date: 2014-11-21
href: http://www.solutions-logiciels.com/actualites.php?actu=15150
tags:
- Entreprise
- Internet
- Innovation
- Promotion
- RGI
- Informatique en nuage
- Open Data
---

> Du 30 octobre au 7 novembre derniers, Paris a accueilli l’Open Source Week. L’occasion de montrer l’influence croissante des codes open source dans les applications mobiles et les infrastructures cloud en particulier.
