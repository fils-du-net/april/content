---
site: Numerama
title: "Mozilla choisit de défendre Mozilla, pas les valeurs du logiciel libre!"
author: Guillaume Champeau
date: 2014-11-20
href: http://www.numerama.com/magazine/31329-mozilla-choisit-de-defendre-mozilla-pas-les-valeurs-du-logiciel-libre.html
tags:
- Entreprise
- Internet
- Économie
- Associations
- International
---

> Mozilla a annoncé la fin du contrat mondial qui le liait à Google, mais en faisant des choix hautement critiquables. Ainsi, Google restera proposé en Europe où il est déjà ultra-dominant, et en Chine c'est le moteur de recherche Baidu soumis à la censure étatique qui sera proposé par défaut aux internautes chinois. Mozilla a-t-il perdu son idéalisme au profit du seul réalisme économique?
