---
site: ZDNet
title: "Gnome vs Groupon: face à la grogne libriste, le géant de l’ecommerce se couche"
author: Louis Adam
date: 2014-11-12
href: http://www.zdnet.fr/actualites/gnome-vs-groupon-face-a-la-grogne-libriste-le-geant-de-l-ecommerce-se-couche-39809363.htm
tags:
- Entreprise
- Économie
---

> Les contributeurs du projet Gnome ont eu fort à faire pour faire plier Groupon: la marque américaine avait en effet dévoilé un nouveau projet de point de vente sous forme de tablette nommé Gnome. Une initiative problématique pour l’environnement de bureau éponyme.
