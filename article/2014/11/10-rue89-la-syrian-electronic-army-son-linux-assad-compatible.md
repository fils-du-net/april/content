---
site: Rue89
title: "La Syrian Electronic Army a son Linux «Assad compatible»"
author: Gurvan Kristanadjaja
date: 2014-11-10
href: http://rue89.nouvelobs.com/2014/11/10/syrian-electronic-army-a-linux-assad-compatible-255967
tags:
- Internet
- Innovation
---

> La Syrian Electronic Army (SEA, l’armée syrienne électronique) prend du galon. Cantonnée jusque-là à des piratages de sites (CNN, BBC, Skype, Microsoft, ou encore Harvard), le groupe de cyberhackers pro Bachar el-Assad vient de lancer son propre système d’exploitation baptisé SEAnux.
