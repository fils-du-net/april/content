---
site: Obsession
title: "Firefox: \"Nous allons faire trembler les géants du net\""
author: Boris Manenti
date: 2014-11-06
href: http://obsession.nouvelobs.com/high-tech/20141106.OBS4333/firefox-nous-allons-faire-trembler-les-geants-du-net.html
tags:
- Internet
- Innovation
---

> Le navigateur web a 10 ans. Alors que sa part de marché décline, la fondation Mozilla regarde vers les smartphones.
