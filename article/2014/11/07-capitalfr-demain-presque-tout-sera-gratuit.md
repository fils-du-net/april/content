---
site: Capital.fr
title: "«Demain, presque tout sera gratuit»"
author: Gilles Tanguy
date: 2014-11-07
href: http://www.capital.fr/enquetes/economie/demain-presque-tout-sera-gratuit-978272
tags:
- Entreprise
- Économie
- Innovation
---

> Dans son dernier livre «La Nouvelle Société du coût marginal zéro» (Ed. Les Liens qui libèrent), le célèbre économiste américain Jeremy Rifkin, qui a conseillé Angela Merkel, ne pronostique rien de moins que la fin du capitalisme actuel. Grâce à l'explosion des plates-formes de partage.
