---
site: Numerama
title: "Face à l'omniprésent Wikipédia, Encyclopædia Universalis dépose le bilan"
author: Julien L.
date: 2014-11-22
href: http://www.numerama.com/magazine/31360-face-a-l-omnipresent-wikipedia-encyclopadia-universalis-depose-le-bilan.html
tags:
- Entreprise
- Internet
- Partage du savoir
- Neutralité du Net
---

> La société éditant l'Encyclopædia Universalis a déposé le bilan, incapable de tenir la distance face à Wikipédia. Elle a été placée en redressement judiciaire et ses perspectives futures sont particulièrement sombres.
