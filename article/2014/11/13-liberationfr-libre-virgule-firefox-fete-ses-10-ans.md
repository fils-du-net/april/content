---
site: Libération.fr
title: "Libre virgule, Firefox fête ses 10 ans"
author: Camille GÉVAUDAN
date: 2014-11-13
href: http://ecrans.liberation.fr/ecrans/2014/11/13/libre-virgule-firefox-fete-ses-dix-ans_1141614
tags:
- Internet
- Innovation
- Standards
- Vie privée
---

> Le célèbre navigateur a vu le jour le 9 novembre 2004. Tristan Nitot, le porte-parole de la fondation Mozilla qui l'a conçu, raconte son histoire, sa personnalité et ses nouveautés.
