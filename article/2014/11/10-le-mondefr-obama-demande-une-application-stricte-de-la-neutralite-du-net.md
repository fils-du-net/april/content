---
site: Le Monde.fr
title: "Obama demande une application stricte de la neutralité du Net"
date: 2014-11-10
href: http://www.lemonde.fr/ameriques/article/2014/11/10/obama-demande-une-application-stricte-de-la-neutralite-du-net_4521384_3222.html
tags:
- Internet
- Institutions
- Neutralité du Net
- International
---

> Un «Internet libre et ouvert». Voilà le vœu qu'a formulé Barack Obama, lundi 10 novembre, à la Federal Communications Commission (FCC), l'autorité du marché des télécommunications aux Etats-Unis. Dans un communiqué, le président américain a notamment demandé à l'institution d'appliquer les règles «les plus strictes possibles» afin de préserver la neutralité d'Internet, martelant sa ferme opposition à un Internet à deux vitesses.
