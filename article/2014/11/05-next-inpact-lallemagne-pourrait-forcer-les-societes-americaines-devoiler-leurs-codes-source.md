---
site: Next INpact
title: "L'Allemagne pourrait forcer les sociétés américaines à dévoiler leurs codes-source"
author: Vincent Hermann
date: 2014-11-05
href: http://www.nextinpact.com/news/90772-l-allemagne-pourrait-forcer-societes-americaines-a-devoiler-leurs-codes-source.htm
tags:
- Entreprise
- Institutions
- International
- Vie privée
---

> L’Allemagne travaille actuellement sur un projet de loi assez radical: l’obligation pour les entreprises américaines de montrer patte blanche si elles veulent fournir du matériel et des services à des secteurs jugés critiques de l’économie. Une décision qui pourrait largement favoriser les sociétés allemandes, en particulier Deutsche Telekom.
