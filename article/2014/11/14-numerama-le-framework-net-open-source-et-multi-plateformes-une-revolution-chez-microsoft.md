---
site: Numerama
title: "Le framework .NET open-source et multi-plateformes, une révolution chez Microsoft"
author: Julien L.
date: 2014-11-14
href: http://www.numerama.com/magazine/31278-le-framework-net-open-source-et-multi-plateformes-une-revolution-chez-microsoft.html
tags:
- Entreprise
- Logiciels privateurs
- Licenses
---

> Microsoft a décidé d'ouvrir le framework .NET et de le passer en open source afin que tout le monde puisse contribuer à son développement. Cette décision est une révolution pour l'entreprise américaine, qui a longtemps misé exclusivement sur une approche propriétaire des logiciels.
