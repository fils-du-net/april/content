---
site: Rue89
title: "En Corrèze, iPads pour tous à l’école: «Vous allez voir, ce sont des mages»"
author: Rémi Noyon
date: 2014-11-21
href: http://rue89.nouvelobs.com/2014/11/21/correze-ipads-tous-a-lecole-allez-voir-sont-mages-256094
tags:
- Internet
- Logiciels privateurs
- Associations
- Éducation
- Marchés publics
---

> François Hollande veut généraliser l'usage des tablettes à l'école, comme il l'a fait en Corrèze. «Apologie du consumérisme» ou avenir de l'enseignement? Reportage à Tulle.
