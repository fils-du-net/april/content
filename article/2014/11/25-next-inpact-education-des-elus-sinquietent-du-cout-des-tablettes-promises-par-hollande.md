---
site: Next INpact
title: "Éducation: des élus s'inquiètent du coût des tablettes promises par Hollande"
author: Xavier Berne
date: 2014-11-25
href: http://www.nextinpact.com/news/91022-education-elus-sinquietent-cout-tablettes-promises-par-hollande.htm
tags:
- Économie
- Interopérabilité
- April
- Institutions
- Éducation
---

> Le moins que l’on puisse dire, c’est que la promesse de François Hollande d’offrir des tablettes à tous les élèves de cinquième à partir de 2016 est loin de convaincre... Deux parlementaires de l’opposition ont récemment saisi la balle au bond pour pointer du doigt le coût et l’utilité d’un tel déploiement.
