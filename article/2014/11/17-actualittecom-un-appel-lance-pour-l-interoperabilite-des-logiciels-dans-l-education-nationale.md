---
site: ActuaLitté.com
title: "Un appel lancé pour l’interopérabilité des logiciels dans l’Education nationale"
author: Stephan Colin
date: 2014-11-17
href: https://savoir.actualitte.com/article/numerique/430/un-appel-lance-pour-l-interoperabilite-des-logiciels-dans-l-education-nationale
tags:
- Logiciels privateurs
- Interopérabilité
- April
- Éducation
- Standards
---

> L’association April, créée en 1996, défend la démocratisation et la diffusion du logiciel libre et des standards ouverts dans l’espace francophone. A ce titre, elle vient de soutenir l’initiative d’un appel pour l’interopérabilité dans l’Education nationale.
