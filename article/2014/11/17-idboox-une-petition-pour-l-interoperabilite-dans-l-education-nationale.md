---
site: IDBoox
title: "Une pétition pour l’interopérabilité dans l’Education nationale"
author: Elizabeth Sutton
date: 2014-11-17
href: http://www.idboox.com/actu-web/une-petition-pour-linteroperabilite-dans-leducation-nationale
tags:
- Interopérabilité
- April
- Éducation
---

> 100 personnes tous personnels de l’Éducation nationale confondus et soutenus par l’April ont lancé une pétition pour demander à ce que les documents utilisés à l’école soient tous au format ouvert, pour garantir l’interopérabilité des fichiers et des données (quel que soit le logiciel ou le système utilisés).
