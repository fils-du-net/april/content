---
site: La Tribune
title: "Démanteler Google: quand les politiques s’invitent dans un dossier de concurrence"
author: Delphine Cuny
date: 2014-11-25
href: http://www.latribune.fr/technos-medias/internet/20141125tribc0e2f8db3/demanteler-google-quand-les-politiques-s-invitent-dans-un-dossier-de-concurrence.html
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Europe
---

> Le Parlement européen devrait se prononcer jeudi sur une motion proposant une scission du géant américain de l’Internet. Si Google n’a pas souhaité réagir officiellement, cette intrusion du politique dans l’enquête antitrust de la Commission l’inquiète. Décryptage
