---
site: AgoraVox
title: "Cyberguerre en kit"
author: Cosmogonie
date: 2014-11-12
href: http://www.agoravox.fr/actualites/technologies/article/cyberguerre-en-kit-159265
tags:
- Internet
- Innovation
- International
---

> La Syrian Electronic Army (SEA) l'annonçait depuis quelques semaines sur twitter, c'est désormais chose faite: une petite distribution basée sur Linux estampillée SEA est disponible, librement téléchargeable par tout un chacun. Que ce groupe de hackers syriens, partisans du régime, passe un temps assez long à concocter un système d'exploitation plutôt simple, sorte de TAILS mâtiné de Kali Linux, visant donc de toute évidence à mettre à la portée du plus grand nombre des «outils de pénétration», semble surprenant au premier abord.
