---
site: Next INpact
title: "Droit d'auteur: critiquée, l'eurodéputée du Parti Pirate répond à Fleur Pellerin"
author: Marc Rees
date: 2014-11-24
href: http://www.nextinpact.com/news/91065-droit-dauteur-critiquee-eurodeputee-parti-pirate-repond-a-fleur-pellerin.htm
tags:
- Institutions
- Droit d'auteur
- Europe
---

> Le 18 novembre dernier, devant les ayants droit du Conseil supérieur de la propriété littéraire et artistique, Fleur Pellerin a écorné la décision du Parlement européen de confier à Julia Reda, eurodéputée du Parti Pirate, la rédaction d’un rapport sur l’adaptation de la directive sur le droit d’auteur. Celle-ci vient de répondre à ses remarques.
