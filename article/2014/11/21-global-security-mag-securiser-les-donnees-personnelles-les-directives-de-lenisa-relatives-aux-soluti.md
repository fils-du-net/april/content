---
site: Global Security Mag
title: "Sécuriser les données personnelles: Les directives de l'ENISA relatives aux solutions cryptographiques"
author: Marc Jacob
date: 2014-11-21
href: http://www.globalsecuritymag.fr/Securiser-les-donnees-personnelles,20141121,48955.html
tags:
- Innovation
- Informatique en nuage
- Vie privée
---

> L’ENISA lance aujourd’hui deux rapports. Le rapport de 2014 «Algorithmes, taille clé et paramètres» est le document de référence fournissant un ensemble de directives aux preneurs de décisions, en particulier, les spécialistes qui conçoivent et mettent en œuvre les solutions cryptographiques pour la protection des données personnelles au sein des organisations commerciales ou des services gouvernementaux pour les citoyens.
