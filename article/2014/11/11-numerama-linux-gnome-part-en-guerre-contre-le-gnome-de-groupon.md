---
site: Numerama
title: "Linux: GNOME part en guerre contre le Gnome de Groupon"
author: Guillaume Champeau
date: 2014-11-11
href: http://www.numerama.com/magazine/31241-linux-gnome-part-en-guerre-contre-le-gnome-de-groupon.html
tags:
- Entreprise
- Économie
---

> La Fondation GNOME juge "scandaleux" que Groupon ait choisi d'utiliser le même nom pour désigner son système d'exploitation pour marchands, annoncé en mai dernier. L'éditeur de l'environnement graphique pour Linux a décidé de passer à l'offensive, en demandant le soutien de la communauté du logiciel libre.
