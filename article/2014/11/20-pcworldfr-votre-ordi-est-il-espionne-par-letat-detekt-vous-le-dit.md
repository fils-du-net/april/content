---
site: PCWorld.fr
title: "Votre ordi est-il espionné par l'Etat? DETEKT vous le dit..."
author: Mathieu Chartier
date: 2014-11-20
href: http://www.pcworld.fr/logiciels/actualites,detekt-anti-spyware-agences-gouvernementales-espionnage,551833,1.htm?comments=1#comments
tags:
- Internet
- Innovation
- Vie privée
---

> Un programme capable de détecter les principaux mouchards des agences de renseignement internationales...
