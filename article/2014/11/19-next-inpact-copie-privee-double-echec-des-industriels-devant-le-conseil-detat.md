---
site: Next INpact
title: "Copie privée: double échec des industriels devant le Conseil d'État"
author: Marc Rees
date: 2014-11-19
href: http://www.nextinpact.com/news/90998-copie-privee-double-echec-industriels-devant-conseil-detat.htm
tags:
- Entreprise
- Internet
- Économie
- Institutions
- DRM
---

> Le Conseil d’État vient de rendre ses deux arrêts consécutifs à la demande d’annulation adressée par plusieurs syndicats et industriels contre deux barèmes de la commission copie privée. C’est un échec pour les requérants, et une victoire sur toute la ligne pour les ayants droit et le ministère de la Culture, qui a épaulé leurs revendications.
