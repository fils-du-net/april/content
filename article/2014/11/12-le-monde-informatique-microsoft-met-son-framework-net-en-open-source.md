---
site: Le Monde Informatique
title: "Microsoft met son framework .Net en Open Source"
author: Maryse Gros
date: 2014-11-12
href: http://www.lemondeinformatique.fr/actualites/lire-microsoft-met-son-framework-net-en-open-source-59256.html
tags:
- Entreprise
- Logiciels privateurs
- Informatique en nuage
---

> Avancée significative hors de l'univers Windows pour Microsoft avec la mise en Open Source de la partie serveur de son framework .Net. L'éditeur livre aussi une version communautaire de son IDE Visual Studio et annonce le portage du runtime de .Net sous Linux et Mac OS.
