---
site: ITespresso
title: "Detekt: un rempart open source à la cyber-surveillance"
author: Clément Bohic
date: 2014-11-21
href: http://www.itespresso.fr/detekt-rempart-open-source-cyber-surveillance-81367.html
tags:
- Internet
- Logiciels privateurs
- Innovation
- Vie privée
---

> Une coalition internationale se monte autour du logiciel open source Detekt, destiné à repérer les PC Windows potentiellement exposés à des écoutes électroniques.
