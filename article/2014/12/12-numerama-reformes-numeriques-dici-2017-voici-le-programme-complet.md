---
site: Numerama
title: "Réformes numériques d'ici 2017: voici le programme complet!"
author: Guillaume Champeau
date: 2014-12-12
href: http://www.numerama.com/magazine/31581-reformes-numeriques-d-ici-2017-voici-le-programme-complet.html
tags:
- Internet
- Institutions
- Innovation
- Informatique en nuage
- Vie privée
---

> Numerama dresse la liste des 13 réformes dans le domaine du numérique que le Gouvernement de Manuel Valls a programmé d'ici la fin du mandat de François Hollande en 2017. Outre la grande loi numérique prévue en 2015 et une nouvelle loi anti-piratage post-Hadopi prévue en 2016, de nombreux textes et mesures touchent directement ou indirectement au numérique.
