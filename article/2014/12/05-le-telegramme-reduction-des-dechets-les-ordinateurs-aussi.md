---
site: Le Telegramme
title: "Réduction des déchets. Les ordinateurs aussi"
date: 2014-12-05
href: http://www.letelegramme.fr/finistere/le-faou/reduction-des-dechets-les-ordinateurs-aussi-05-12-2014-10450827.php
tags:
- Sensibilisation
- Associations
---

> Une trentaine de personnes se sont succédé samedi après-midi à l'après-midi découverte dédiée à lutter contre l'obsolescence programmée des ordinateurs, organisée par Radio Evasion.
