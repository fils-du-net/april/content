---
site: Next INpact
title: "Enjeu de lourds intérêts, le blocage d'Allostreaming sera rejugé fin 2015"
author: Marc Rees
date: 2014-12-10
href: http://www.nextinpact.com/news/91329-enjeu-lourds-interets-blocage-d-allostreaming-sera-rejuge-fin-2015.htm
tags:
- Internet
- Économie
- HADOPI
- Europe
---

> Selon nos informations, le dossier Allostreaming ne sera pas réexaminé avant la fin octobre 2015 par la cour d’appel de Paris. Cette affaire, l’une des plus importantes en matière de propriété intellectuelle, concerne le blocage d’une galaxie de sites de streaming exigé par le monde de l’audiovisuel et du cinéma. Cependant, les ayants droit ne veulent pas débourser un centime pour la mise en œuvre de ce verrouillage.
