---
site: GoMet'
title: "Bernard Stiegler: haro sur l'algorithme"
author: Philippe Amsellem
date: 2014-12-26
href: http://www.go-met.com/philosophie-stiegler-crie-haro-lalgorithme
tags:
- Économie
- Innovation
---

> «La problème qui se pose est de faire en sorte que l’automatisation devienne une chance et non un handicap, avec de nouvelles industries qui se développent comme les logiciels libres ou encore l’économie contributive», énonce-t-il.
