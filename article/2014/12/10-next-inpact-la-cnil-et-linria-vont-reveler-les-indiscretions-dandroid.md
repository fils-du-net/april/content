---
site: Next INpact
title: "La CNIL et l'Inria vont révéler les indiscrétions d'Android"
author: Marc Rees
date: 2014-12-10
href: http://www.nextinpact.com/news/91332-la-cnil-et-l-inria-vont-reveler-indiscretions-d-android.htm
tags:
- Entreprise
- Institutions
- Vie privée
---

> La CNIL va diffuser lundi une étude intéressante montée avec l’Inria. Elle visera à informer les utilisateurs de la masse de données personnelles passant dans les mains de leur smartphone et des applications installées. Une première campagne visait l'iPhone en avril 2013. Cette fois Android sera sur le grill.
