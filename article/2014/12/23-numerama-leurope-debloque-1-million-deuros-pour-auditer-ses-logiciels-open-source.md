---
site: Numerama
title: "L'Europe débloque 1 million d'euros pour auditer ses logiciels open-source"
author: Julien L.
date: 2014-12-23
href: http://www.numerama.com/magazine/31689-l-europe-debloque-1-million-d-euros-pour-auditer-ses-logiciels-open-source.html
tags:
- Institutions
- Innovation
- Europe
- Vie privée
---

> La députée européenne Julia Reda, par ailleurs membre du parti pirate, annonce qu'elle a obtenu de la Commission européenne le déblocage d'une enveloppe d'un million d'euros dans le cadre d'un projet pilote visant à organiser l'audit des logiciels open source utilisés par les institutions européennes.
