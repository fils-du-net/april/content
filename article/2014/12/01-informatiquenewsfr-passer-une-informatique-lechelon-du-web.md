---
site: InformatiqueNews.fr
title: "Passer à une informatique à l'échelon du web?"
author: Thierry Outrebon
date: 2014-12-01
href: http://www.informatiquenews.fr/passer-informatique-lechelon-du-web-mike-joachamsen-emulex-26012
tags:
- Entreprise
- Internet
- Sensibilisation
- Innovation
- Informatique en nuage
---

> Briser les chaînes qui vous entravent – L’Entreprise est-elle prête à passer à une informatique à l’échelon du web?
