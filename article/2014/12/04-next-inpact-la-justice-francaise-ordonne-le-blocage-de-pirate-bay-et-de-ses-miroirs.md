---
site: Next INpact
title: "La justice française ordonne le blocage de The Pirate Bay et de ses miroirs"
author: Marc Rees
date: 2014-12-04
href: http://www.nextinpact.com/news/91250-la-justice-francaise-ordonne-blocage-the-pirate-bay-et-ses-miroirs.htm
tags:
- Entreprise
- Internet
- Institutions
---

> Les majors de la musique, représentées par la Société Civile des Producteurs de Phonogrammes, ont obtenu du tribunal de grande instance de Paris le blocage de The Pirate Bay et de ses miroirs. Une mesure qui devrait être ordonnée aux fournisseurs d’accès grâce à un des articles de la loi Hadopi, le 336-2 du Code de la propriété intellectuelle
