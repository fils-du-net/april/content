---
site: 01net.
title: "Falque-Pierottin (Cnil): «La protection des données est un droit fondamental»"
author: Pascal Samama
date: 2014-12-10
href: http://www.01net.com/editorial/636225/i-falque-pierottin-cnil-la-protection-des-donnees-est-un-droit-fondamental
tags:
- Entreprise
- Institutions
- Europe
- Vie privée
---

> Lors du forum européen sur la gouvernance des données, la présidente de la Cnil française a présenté une déclaration commune qui pourrait devenir les CGU des Européens sur les données personnelles.
