---
site: Maddyness
title: "#Bordeaux: Ouverture de La Banquiz, le premier accélérateur des startups du logiciel libre"
author: Jennifer Padjemi
date: 2014-12-08
href: http://www.maddyness.com/accompagnement/accelerateurs/2014/12/08/la-banquiz
tags:
- Entreprise
- Administration
- Économie
- Innovation
---

> Alain Rousset, président de Bordeaux Unitec et François Pellegrini, président d’Aquinetic vont inaugurer prochainement La Banquiz. Nouvel arrivant dans le paysage des accélérateurs français, celui-ci se destine à l’attention des startups du secteur des logiciels libres en France. L’inauguration de ce lieu est prévue vendredi 12 décembre prochain au Cinéma Jean Eustache à Pessac.
