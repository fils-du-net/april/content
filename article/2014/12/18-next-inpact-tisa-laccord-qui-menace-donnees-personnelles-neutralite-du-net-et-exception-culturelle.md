---
site: Next INpact
title: "TISA, l'accord qui menace données personnelles, neutralité du net et exception culturelle"
author: Marc Rees
date: 2014-12-18
href: http://www.nextinpact.com/news/91465-tisa-l-accord-qui-menace-donnees-personnelles-neutralite-net-et-exception-culturelle.htm
tags:
- Internet
- Économie
- HADOPI
- Institutions
- Neutralité du Net
- Europe
- International
- Vie privée
---

> Pourrait-on imaginer un jour Pascal Rogard manifester coude à coude avec la CNIL et les membres de la Quadrature du Net? Ce miracle pourrait avoir lieu au regard des positions américaines exprimées dans le cadre d'un accord de libre-échange nommé «Accord sur le Commerce des Services» (ACS) ou en anglais «Trade in Service Agreement» (TISA)
