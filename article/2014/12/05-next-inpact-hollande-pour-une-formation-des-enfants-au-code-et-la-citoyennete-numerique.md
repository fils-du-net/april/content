---
site: Next INpact
title: "Hollande pour une formation des enfants au code et à la «citoyenneté numérique»"
author: Xavier Berne
date: 2014-12-05
href: http://www.nextinpact.com/news/91252-hollande-pour-formation-enfants-au-code-et-a-citoyennete-numerique.htm
tags:
- Institutions
- Éducation
---

> Annoncé par le chef de l’État lors de son interview télévisée du 14 juillet, le futur «plan pour le numérique à l’école» vient à nouveau d’être évoqué par François Hollande. Le président de la République a notamment affirmé qu’il fallait «préparer nos enfants beaucoup plus tôt au codage», ainsi qu’à «la société numérique de demain».
