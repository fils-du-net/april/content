---
site: l'Humanité.fr
title: "Piratage de Sony: la leçon d’un vrai pro"
author: Benoit Delrue
date: 2014-12-19
href: http://www.humanite.fr/piratage-de-sony-la-lecon-dun-vrai-pro-560837
tags:
- Entreprise
- Internet
- Institutions
- International
- Vie privée
---

> L’échange ce matin sur RTL, entre Yves Calvi et le professionnel Benjamin Bayart, éclaire d’un jour nouveau la cyber-attaque de Sony Pictures. Loin des approches belliqueuses et des chagrins de stars, l’affaire révèle un danger pour nos données.
