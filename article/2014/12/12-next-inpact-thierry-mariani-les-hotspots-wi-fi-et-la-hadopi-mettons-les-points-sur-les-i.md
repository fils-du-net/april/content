---
site: Next INpact
title: "Thierry Mariani, les hotspots Wi-Fi et la Hadopi... mettons les points sur les i"
author: Marc Rees
date: 2014-12-12
href: http://www.nextinpact.com/news/91377-thierry-mariani-hotspots-wi-fi-et-hadopi-mettons-points-sur-i.htm
tags:
- Internet
- HADOPI
- Institutions
- Europe
- Vie privée
---

> Le député Thierry Mariani  s’est attiré hier une salve de critiques après qu’il a applaudi les facilités de connexion dans les hotspots accessibles en Corée. Nombreux sont ceux qui lui ont rappelé sur Twitter qu'il avait voté la loi Hadopi et ne devait finalement ne s’en prendre qu’à lui-même. Cependant, une petite remise en perspective s’impose.
