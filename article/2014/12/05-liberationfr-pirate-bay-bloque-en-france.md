---
site: Libération.fr
title: "The Pirate Bay bloqué en France"
author: Camille Gévaudan
date: 2014-12-05
href: http://ecrans.liberation.fr/ecrans/2014/12/05/the-pirate-bay-bloque-en-france_1157463
tags:
- Internet
- HADOPI
- Institutions
---

> D'ici quinze jours, les principaux fournisseurs d'accès à Internet devront empêcher l'accès des Français au moteur de recherche de fichiers pirates.
