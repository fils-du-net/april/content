---
site: internet ACTU.net
title: "Préserver les logiciels… et la philosophie qui va avec"
author: Rémi Sussan
date: 2014-12-04
href: http://www.internetactu.net/2014/12/04/preserver-les-logiciels-et-la-philosophie-qui-va-avec
tags:
- Partage du savoir
- Innovation
- Sciences
- Standards
---

> Une organisation futuriste tournée vers le passé. C’est bien ainsi qu’on pourrait définir la fondation du “Long Now”, fondée par Stewart Brand et Brian Eno, et dont nous avons déjà parlé dans nos colonnes.
