---
site: ludomag
title: "Un appel en faveur des formats ouverts dans l’Education Nationale"
author: Aurélie Julien
date: 2014-12-22
href: http://www.ludovia.com/2014/12/un-appel-en-faveur-des-formats-ouverts-dans-leducation-nationale
tags:
- Interopérabilité
- April
- Éducation
- Standards
---

> L'April et une centaine de professeurs tous acteurs du numériques éducatifs (formateurs, référents numériques, gestionnaires ENT...) ont lancé il y a une semaine un appel en faveur des formats ouverts dans l'Éducation nationale. En plus du soutien initial du SE-UNSA, du SGEN-CFDT, de la Fédération SUD Éducation, cet appel a été signé aussi par la CGT-Éduc'action ainsi que le SNES-FSU. Du côté des associations d'enseignants, en plus des Clionautes, de LinuxÉdu, Sésamath, PAGESTEC, la FADBEN et de nombreuses autres ont rejoint l'appel.
