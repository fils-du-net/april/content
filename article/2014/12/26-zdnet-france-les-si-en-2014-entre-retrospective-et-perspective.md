---
site: ZDNet France
title: "Les SI en 2014: entre rétrospective et perspective"
author: Frédéric Charles
date: 2014-12-26
href: http://www.zdnet.fr/actualites/les-si-en-2014-entre-retrospective-et-perspective-1-2-39811961.htm
tags:
- Entreprise
- Économie
- Innovation
- Informatique en nuage
- Europe
---

> La DSI est aussi sur le pont quand la faille Heartbleed est découverte dans OpenSSL, utilisé par des millions de sites et de serveurs, dont ceux des entreprises qui utilisent l'open source pour leurs sites Internet.
