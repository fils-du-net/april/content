---
site: Le Monde.fr
title: "«Il ne faut pas que les données personnelles soient échangées contre de la banane!»"
date: 2014-12-08
href: http://www.lemonde.fr/pixels/article/2014/12/08/il-ne-faut-pas-que-les-donnees-personnelles-soient-echangees-contre-de-la-banane_4536320_4408996.html
tags:
- Entreprise
- Internet
- Institutions
- Sciences
- ACTA
- Vie privée
---

> Les autorités européennes de protection des données personnelles – le «G29», dont le représentant français est la Commission nationale de l'informatique et des libertés (CNIL) – organisent lundi 8 décembre à Paris une conférence internationale consacrée à la protection des données personnelles: l'European Data Governance Forum, à suivre en direct vidéo et sur Twitter avec le mot-clé #EDGF14.
