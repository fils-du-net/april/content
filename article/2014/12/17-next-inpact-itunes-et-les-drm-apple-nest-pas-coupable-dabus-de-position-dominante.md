---
site: Next INpact
title: "iTunes et les DRM: Apple n'est pas coupable d'abus de position dominante"
author: Vincent Hermann
date: 2014-12-17
href: http://www.nextinpact.com/news/91454-itunes-et-drm-apple-nest-pas-coupable-dabus-position-dominante.htm
tags:
- Entreprise
- Institutions
- DRM
---

> Depuis une dizaine d’années, Apple fait face à une imposante action collective au sujet d’ITunes et des DRM. La firme était accusée d’avoir abusé de sa position dominante et l’enjeu était de taille, avec plus d’un milliard de dollars potentiels dans la balance. Un jury a cependant jeté la plainte hier aux oubliettes.
