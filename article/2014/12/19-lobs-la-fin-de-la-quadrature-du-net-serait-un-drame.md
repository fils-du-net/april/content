---
site: L'OBS
title: "La fin de La Quadrature du Net serait un drame"
author: Xavier de La Porte 
date: 2014-12-19
href: http://rue89.nouvelobs.com/2014/12/19/fin-quadrature-net-serait-drame-256649
tags:
- Internet
- Associations
- Neutralité du Net
---

> Fondée en 2008, La Quadrature du Net défend un Internet libre et ouvert. Elle s’est illustrée dans la lutte contre la loi Création et Internet qui avait abouti à la mise en place de l’Hadopi, faisant à la fois un travail de contre-expertise et de militantisme d’un nouveau genre. Utiliser le mail pour abreuver les députés d’informations et de questions a été une des armes de l’association.
