---
site: LeMonde.fr
title: "Comment la France a laissé la voie libre aux tribunaux privés internationaux"
author: Maxime Vaudano
date: 2014-12-19
href: http://transatlantique.blog.lemonde.fr/2014/12/19/comment-la-france-a-laisse-la-voie-libre-aux-tribunaux-prives-internationaux
tags:
- Entreprise
- Économie
- Institutions
- Europe
- International
---

> C'est l'histoire d'un coup de force franco-allemand avorté qui aurait pu abattre un totem du commerce mondial. L'histoire de quelques semaines décisives où la timidité du gouvernement français a conduit à laisser l'Union européenne avaliser un choix qu'elle regrettera peut-être dans quelques années: l'inclusion d'un mécanisme d'arbitrage privé dans l'accord commercial CETA conclu entre l'UE et le Canada le 26 septembre.
