---
site: Commentçamarche.net
title: "L'open source plébiscité en entreprise"
author: Sophie Garrigues
date: 2014-12-30
href: http://www.commentcamarche.net/news/5865914-l-open-source-plebiscite-en-entreprise
tags:
- Entreprise
---

> Selon une enquête de l'Institut Ponemon et de Zimbra, 3 décideurs informatique sur 4 s'accordent sur la crédibilité des logiciels libres.
