---
site: Next INpact
title: "Ne laissons pas Internet devenir une «zone de non-droit»"
author: Yoann Spicher
date: 2014-12-20
href: http://www.nextinpact.com/news/91498-tribune-ne-laissons-pas-internet-devenir-zone-non-droit.htm
tags:
- Internet
- April
- Institutions
- Associations
- Neutralité du Net
- Vie privée
---

> Alors que, par essence, Internet pourrait être le lieu privilégié et concret de l'application réelle de nos droits fondamentaux, nous assistons peu à peu à leur émiettement et à l'apparition de la «zone de non-droit». Ironiquement, alors que cette menace était agitée par une puissance publique prétendant défendre l'intérêt général pour justifier ses tentatives de reprise de contrôle sur un outil lui échappant, c'est finalement sous la forme d'un recul de la capacité des citoyens à faire valoir leurs droits face à l'exercice arbitraire du pouvoir qu'elle se réalise.
