---
site: Numerama
title: "Le Parti Pirate nargue la justice. Amusant mais à double tranchant."
author: Guillaume Champeau
date: 2014-12-08
href: http://www.numerama.com/magazine/31523-le-parti-pirate-nargue-la-justice-amusant-mais-a-double-tranchant.html
tags:
- Internet
- Institutions
- Droit d'auteur
---

> Le Parti Pirate a annoncé la création d'un nouveau site miroir pour donner l'accès à The Pirate Bay, démontrant l'inutilité pratique du jugement du 4 décembre dernier. Mais cette provocation pourrait se retourner contre lui, et donner du grain à moudre aux partisans d'une automatisation des procédures judiciaires en matière de blocage de sites internet.
