---
site: Le Monde Informatique
title: "L'UE met 1M€ dans un audit de sécurité Open Source"
author: Dominique Filippone
date: 2014-12-23
href: http://www.lemondeinformatique.fr/actualites/lire-l-ue-met-1meteuro-dans-un-audit-de-securite-open-source-59702.html
tags:
- Institutions
- Europe
- Vie privée
---

> Le Parlement Européen va mener un audit de sécurité afin de s'assurer que l'ensemble de ses solutions libres et Open Source ne posent pas de problème de sécurité. 500 000 euros seront aussi alloués pour développer l'outil web de création d'amendement, AT4AM.
