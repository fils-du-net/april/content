---
site: Next INpact
title: "Edward Snowden: «Veut-on sacrifier toute notre vie privée au profit de la sécurité?»"
author: Vincent Hermann
date: 2014-12-11
href: http://www.nextinpact.com/news/91358-edward-snowden-veut-on-sacrifier-toute-notre-vie-privee-au-profit-securite.htm
tags:
- Internet
- Institutions
- Vie privée
---

> Hier, Edward Snowden est intervenu pour la première fois en France dans le cadre d’une conférence organisée par Amnesty International. Il est notamment intervenu sur l’erreur de logique fondamentale qui alimente selon lui le monde du renseignement aujourd’hui. Il a rappelé également que la France n’était pas épargnée.
