---
site: Framablog
title: "Qui agit pour nos libertés numériques?"
author: Goofy
date: 2014-12-13
href: http://www.framablog.org/index.php/post/2014/12/12/qui-agit-pour-nos-libertes-numeriques
tags:
- Entreprise
- Internet
- Associations
- Droit d'auteur
- ACTA
- Vie privée
---

> Le site « The Pirate Bay » a été fermé[1]. Dans la tête des gens, ça a fait tilt de savoir que demain, ils devraient télécharger leur émission de télévision favorite autre part. En y réfléchissant, ils ont décidé que c’était le début d’une pente glissante. Ils comprennent que ça veut peut-être dire que ce contenu alternatif sera probablement d’accès difficile voire impossible. Que les langoliers[2] nous rattrapent plus vite que nous ne l’imaginions.
