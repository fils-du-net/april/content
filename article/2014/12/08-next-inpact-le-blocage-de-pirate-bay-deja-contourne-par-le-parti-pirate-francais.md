---
site: Next INpact
title: "Le blocage de The Pirate Bay déjà contourné par le Parti Pirate français"
author: Marc Rees
date: 2014-12-08
href: http://www.nextinpact.com/news/91287-le-blocage-the-pirate-bay-deja-contourne-par-parti-pirate-francais.htm
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> La branche française du Parti Pirate a mis en ligne un nouveau miroir du site de The Pirate Bay. Une réaction au blocage visant le site de lien Torrent, alors qu’une adresse du parti politique est touchée par le jugement du TGI de Paris révélé dans nos colonnes.
