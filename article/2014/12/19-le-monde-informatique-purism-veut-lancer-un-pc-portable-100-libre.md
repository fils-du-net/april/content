---
site: Le Monde Informatique
title: "Purism veut lancer un PC portable 100% libre"
author: Jean Elyan
date: 2014-12-19
href: http://www.lemondeinformatique.fr/actualites/lire-purism-veut-lancer-un-pc-portable-100-libre-59677.html
tags:
- Entreprise
- Associations
- DRM
---

> Le Librem 15, l'ordinateur portable conçu par Purism, doit permettre de contourner le verrouillage du firmware et de démarrer «librement» sa machine sans vérification de signature au moment du boot. La campagne de financement communautaire lancée par le concepteur sera prolongée jusqu'à fin janvier.
