---
site: 01net.
title: "Procès DRM d'iTunes: Apple est déclaré non coupable"
author: C.B.
date: 2014-12-17
href: http://www.01net.com/editorial/637501/proces-des-drm-ditunes-apple-est-declare-non-coupable
tags:
- Entreprise
- Économie
- Institutions
- DRM
- International
---

> Le juge d'Oakland a estimé que la firme à la pomme n'a pas employé de techniques anticoncurrentielles pour vendre ses iPod et de la musique en ligne.
