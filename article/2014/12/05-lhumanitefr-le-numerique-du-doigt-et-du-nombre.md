---
site: l'Humanité.fr
title: "Le numérique : du doigt et du nombre"
author: Cynthia Fleury
date: 2014-12-05
href: http://www.humanite.fr/le-numerique-du-doigt-et-du-nombre-559408
tags:
- Internet
- Innovation
- Open Data
---

> Faire baisser le prix du livre numérique pour, au passage, s’attaquer au livre et s’attaquer au format poche, ce serait un enjeu de taille pour l’économie numérique. Délinéarisation des contenus culturels («comme si on pouvait tuer le temps, sans blesser l’éternité», David Thoreau), métamorphose de l’archivage, livres augmentés, relance de l’approche culturelle universaliste et encyclopédiste, livre comme objet d’art, mobilité des œuvres artistiques, etc. Voilà quelques pistes de réflexion du livre blanc publié par l’Afdel et Renaissance numérique.
