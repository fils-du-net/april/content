---
site: Next INpact
title: "Le gouvernement promet une Agence du numérique pour début 2015"
author: Xavier Berne
date: 2014-12-19
href: http://www.nextinpact.com/news/91479-le-gouvernement-promet-agence-numerique-pour-debut-2015.htm
tags:
- Institutions
---

> Dans les cartons depuis plusieurs mois déjà, l’Agence française du numérique s’apprête à prendre son envol. Cette nouvelle institution, qui regroupera la French Tech, la mission Très Haut Débit et la Délégation aux usages de l’internet, sera en effet «créée au début de l'année 2015» selon Axelle Lemaire.
