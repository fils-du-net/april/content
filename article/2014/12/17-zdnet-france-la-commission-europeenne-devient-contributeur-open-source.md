---
site: ZDNet France
title: "La Commission européenne devient contributeur open source"
author: Guillaume Serries
date: 2014-12-17
href: http://www.zdnet.fr/actualites/la-commission-europeenne-devient-contributeur-open-source-39811539.htm
tags:
- Administration
- Institutions
- Marchés publics
- Europe
---

> Grosse consommatrice de solutions open source, Bruxelles estime que les patches et les nouvelles fonctionnalités conçues au sein de son SI doivent désormais bénéficier à la communauté. Une politique adéquate va être mise en œuvre en 2015.
