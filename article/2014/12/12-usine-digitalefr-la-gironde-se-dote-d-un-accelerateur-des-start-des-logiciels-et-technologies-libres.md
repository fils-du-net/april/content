---
site: "usine-digitale.fr"
title: "La Gironde se dote d’un accélérateur des start-up des logiciels et technologies libres"
author: Nicolas César
date: 2014-12-12
href: http://www.usine-digitale.fr/article/la-gironde-se-dote-d-un-accelerateur-des-start-up-des-logiciels-et-technologies-libres.N302964
tags:
- Entreprise
- Administration
- Économie
- Innovation
---

> Ce vendredi 12 décembre, Alain Rousset, président de la Région Aquitaine, et François Pellegrini, président d'Aquinetic, association qui porte le cluster aquitain de compétences en numérique, inaugurent "La Banquiz", une structure dédiée à l’accompagnement des jeunes pousses de l'économie du logiciel libre.
