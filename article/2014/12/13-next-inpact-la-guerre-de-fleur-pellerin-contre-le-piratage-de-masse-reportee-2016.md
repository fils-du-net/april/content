---
site: Next INpact
title: "La guerre de Fleur Pellerin contre le «piratage de masse» reportée à 2016?"
author: Marc Rees
date: 2014-12-13
href: http://www.nextinpact.com/news/91398-la-guerre-fleur-pellerin-contre-piratage-masse-reporte-a-2016.htm
tags:
- Internet
- Économie
- HADOPI
- Institutions
- Europe
---

> Le ministère de la Culture confirme sa volonté d’apporter un «soutien à la mutation numérique des industries culturelles». Dans l’agenda des réformes exposé sur le site du gouvernement, la Rue de Valois esquisse les trois principales mesures dont le chantier devrait débuter au premier trimestre de l'année prochaine. Mais curieusement, le document prévoit une mise en œuvre début 2016.
