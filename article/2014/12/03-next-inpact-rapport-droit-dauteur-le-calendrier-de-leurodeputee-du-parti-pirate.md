---
site: Next INpact
title: "Rapport droit d'auteur: le calendrier de l'eurodéputée du Parti pirate"
author: Marc Rees
date: 2014-12-03
href: http://www.nextinpact.com/news/91202-rapport-droit-dauteur-calendrier-l-eurodeputee-parti-pirate.htm
tags:
- Institutions
- Droit d'auteur
- Europe
---

> On connait désormais le calendrier du rapport de l’eurodéputée Julia Reda (Parti Pirate, apparentée Verts) sur l’adaptation de la directive sur le droit d’auteur. L’agenda a été fixé hier lors d’une réunion de la commission des affaires juridiques (JURI) au Parlement européen.
