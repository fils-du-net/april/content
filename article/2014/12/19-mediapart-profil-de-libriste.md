---
site: Mediapart
title: "Profil de libriste"
author: André Ani
date: 2014-12-19
href: http://blogs.mediapart.fr/blog/andre-ani/191214/profil-de-libriste
tags:
- Internet
- Sensibilisation
- Associations
- Innovation
- Vie privée
---

> Bonjour, c’est Pierre, co-fondateur de IndieHosters. Avec Michiel de Jong, nous avons créé indieHosters il y a 2 mois pour aider les logiciels libres dans le nuages à toucher le grand public.
