---
site: LeMonde.fr
title: "Ouvrir les algorithmes pour comprendre et améliorer les traitements dont nous sommes l’objet"
author: Hubert Guillaud
date: 2014-12-26
href: http://internetactu.blog.lemonde.fr/2014/12/26/ouvrir-les-algorithmes-pour-comprendre-et-ameliorer-les-traitements-dont-nous-sommes-lobjet
tags:
- Sciences
- Open Data
- Vie privée
---

> Ouvrir les données ne suffit pas. Permettre aux utilisateurs de récupérer leurs données non plus. "La restitution des données à l’utilisateur ne suffira pas à les armer, s’ils ne peuvent être conscients des traitements que leurs données subissent". Les utilisateurs, même s'ils récupéraient leurs données ne seraient pas à égalité avec les services qui les utilisent, "car ceux-ci savent les traiter". Mais pas les utilisateurs! Eux n'ont aucun moyen pour l'instant de connaître les traitements qui sont appliqués à leurs données! Ils n'en connaissent que les résultats.
