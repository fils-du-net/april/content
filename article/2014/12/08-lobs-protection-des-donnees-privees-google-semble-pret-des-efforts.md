---
site: L'OBS
title: "Protection des données privées: \"Google semble prêt à des efforts\""
author: Boris Manenti
date: 2014-12-08
href: http://obsession.nouvelobs.com/high-tech/20141205.OBS7146/protection-des-donnees-privees-google-semble-pret-a-des-efforts.html
tags:
- Entreprise
- Internet
- Institutions
- Europe
- Vie privée
---

> La Cnil organise une grande conférence sur la protection des données personnelles. Sa présidente Isabelle Falque-Pierrotin dessine les contours des discussions.
