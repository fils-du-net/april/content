---
site: Les Echos
title: "L'erreur fondamentale de Jeremy Rifkin"
author: Vincent Lorphelin
date: 2014-12-10
href: http://www.lesechos.fr/idees-debats/cercle/cercle-119788-lerreur-fondamentale-de-jeremy-rifkin-3-1073565.php
tags:
- Économie
- Licenses
---

> L’économie du partage des ressources doit aussi inclure un partage des profits, ce qui ouvre une voie intermédiaire entre les économies associative et capitaliste: l’économie de l'utilité.
