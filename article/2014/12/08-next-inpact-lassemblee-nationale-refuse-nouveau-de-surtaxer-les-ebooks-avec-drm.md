---
site: Next INpact
title: "L'Assemblée nationale refuse à nouveau de surtaxer les ebooks avec DRM"
author: Xavier Berne
date: 2014-12-08
href: http://www.nextinpact.com/news/91279-l-assemblee-nationale-refuse-a-nouveau-surtaxer-ebooks-avec-drm.htm
tags:
- Économie
- Interopérabilité
- April
- Institutions
- Associations
- DRM
- Europe
---

> Alors que la France pourrait bientôt être condamnée par la justice européenne à cause de la TVA réduite dont profitent tous les ebooks, les députés écologistes proposaient de trouver une sortie de secours en n’appliquant ce taux de 5,5 % qu’aux seuls livres numériques dépourvus de verrous (DRM). Mais sans grande surprise, le gouvernement et les parlementaires s’y sont opposés.
