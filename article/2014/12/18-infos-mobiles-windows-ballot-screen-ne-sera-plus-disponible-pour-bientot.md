---
site: "infos-mobiles"
title: "Windows Ballot Screen ne sera plus disponible pour bientôt"
author: Castella
date: 2014-12-18
href: http://www.infos-mobiles.com/windows-ballot-screen/windows-ballot-screen-sera-disponible-bientot/86073
tags:
- Entreprise
- Institutions
- Europe
---

> En 2009 Microsoft a commencé à proposer le choix entre différents navigateurs web pour ceux qui utilisent Windows dans l’Europe mais désormais il faudrait dire adieu à Windows Ballot Screen.
