---
site: Next INpact
title: "Le blocage administratif des sites programmé pour février 2015"
author: Marc Rees
date: 2014-12-12
href: http://www.nextinpact.com/news/91391-le-blocage-administratifs-sites-programme-pour-fevrier-2016.htm
tags:
- Internet
- Institutions
- Europe
---

> Selon nos informations, le gouvernement entend activer le blocage administratif à partir de février 2015 au plus tard. Le projet de décret est dans la boucle, mais il doit encore être notifié à Bruxelles, puisqu’il touche à la «société de l’information». Paris envisage maintenant une notification d’urgence afin de tenir ce calendrier.
