---
site: Datanews.be
title: "Certains outils de cryptage ont résisté à la NSA"
author: Pieterjan Van Leemputten
date: 2014-12-29
href: http://datanews.levif.be/ict/actualite/certains-outils-de-cryptage-ont-resiste-a-la-nsa/article-normal-359275.html
tags:
- Internet
- Institutions
---

> La NSA ne peut quand même pas contourner toute forme de sécurité. Le service de sécurité américain s'est en effet cassé les dents sur un certain nombre de méthodes de protection.
