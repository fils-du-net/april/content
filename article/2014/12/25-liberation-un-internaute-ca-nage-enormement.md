---
site: Libération
title: "Un internaute, ça nage énormément"
author: Véronique Bonnet
date: 2014-12-25
href: http://ecrans.liberation.fr/ecrans/2014/12/25/un-internaute-ca-nage-enormement_1170082
tags:
- Internet
- Sensibilisation
---

> Au dehors, ça n’arrête pas: à peine un rivage abordé, ça dérive déjà, et encore. «Où suis-je?» Au dedans, ça nage complètement. Ça part même en patauge. «Où en suis-je?»
