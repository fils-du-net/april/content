---
site: L'OBS
title: "Start-up: ces \"barbares\" qui veulent débloquer la France"
author: Sophie Fay, Dominique Nora, Nicole Pénicaut, Donald Hébert
date: 2014-12-20
href: http://tempsreel.nouvelobs.com/economie/20141219.OBS8339/start-up-ces-barbares-qui-veulent-debloquer-la-france.html
tags:
- Entreprise
- Administration
- Innovation
---

> Ils ont entre 30 et 45 ans. Ils se surnomment eux-mêmes les "barbares". Avec internet, les réseaux sociaux et l'industrie du logiciel, ils bousculent le système...
