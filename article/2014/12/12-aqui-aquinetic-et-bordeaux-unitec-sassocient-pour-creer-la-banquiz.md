---
site: Aqui!
title: "Aquinetic et Bordeaux Unitec s'associent pour créer la Banquiz"
author: Romain Béteille
date: 2014-12-12
href: http://www.aqui.fr/economies/aquinetic-et-bordeaux-unitec-s-associent-pour-creer-la-banquiz,11266.html
tags:
- Entreprise
- Administration
- Innovation
- Open Data
---

> Ce vendredi 12 décembre à Pessac est née la Banquizz, premier accélérateur des start-ups du libre en France. Il est né d'un partenariat entre Aquinetic (cluster aquitain des technologies libres) et Bordeaux Unitec (technopole qui accompagne depuis 25 ans les créateurs de jeunes sociétés de la métropole bordelaise). Banquiz vise ainsi à fédérer les quelques 300 entreprises et 20 000 emplois crées dans le numérique en Aquitaine, un incubateur qui accueille dès cette année sa première promo de trois sociétés.
