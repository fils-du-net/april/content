---
site: Côté Brest
title: "Une médiathèque de poche"
author: Pierre Gicquel
date: 2014-12-10
href: http://www.cotebrest.fr/2014/12/10/une-mediatheque-de-poche
tags:
- Partage du savoir
- Associations
- Innovation
---

> Pensée et développée par les Chats cosmiques, une jeune association brestoise au poil, la CosmicBox est un mini ordinateur qui a plus d’un tour dans son sac.
