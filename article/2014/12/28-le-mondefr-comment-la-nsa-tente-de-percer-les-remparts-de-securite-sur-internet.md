---
site: Le Monde.fr
title: "Comment la NSA tente de percer les remparts de sécurité sur Internet"
author: Martin Untersinger
date: 2014-12-28
href: http://www.lemonde.fr/pixels/article/2014/12/28/les-enormes-progres-de-la-nsa-pour-defaire-la-securite-sur-internet_4546843_4408996.html
tags:
- Internet
- Institutions
- Éducation
---

> De nouveaux documents dévoilés par Edward Snowden révèlent les capacités de l’agence américaine, et les outils qui continuent de lui résister.
