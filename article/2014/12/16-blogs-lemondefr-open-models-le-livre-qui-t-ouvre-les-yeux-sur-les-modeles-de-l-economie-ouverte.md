---
site: Blogs LeMonde.fr
title: "Open Models, le livre qui t’ouvre les yeux sur les modèles de l’économie ouverte"
author: Anne-Sophie Novel
date: 2014-12-16
href: http://alternatives.blog.lemonde.fr/2014/12/16/open-models-le-livre-qui-touvre-les-yeux-sur-les-modeles-de-leconomie-ouverte
tags:
- Économie
- Partage du savoir
- Innovation
- Open Data
---

> Pour qui souhaite comprendre le point commun entre le constructeur automobile Tesla, le navigateur internet Firefox, l'encyclopédie Wikipedia et la voiture de course de Joe Justice, l'ouvrage collectif Open Models est idéal. Fait de pixel et de papier, il vous plonge dans l'univers encore sous-jacent et pourtant fort puissant de l'économie ouverte. Visite guidée.
