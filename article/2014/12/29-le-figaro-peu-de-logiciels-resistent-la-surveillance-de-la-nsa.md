---
site: Le Figaro
title: "Peu de logiciels résistent à la surveillance de la NSA"
author: Jules Darmanin
date: 2014-12-29
href: http://www.lefigaro.fr/secteur/high-tech/2014/12/29/01007-20141229ARTFIG00256-peu-de-logiciels-resistent-a-la-surveillance-de-la-nsa.php
tags:
- Internet
- Institutions
- Innovation
---

> Depuis juin 2013, pas un mois ne passe sans que de nouvelles informations, extraites de la masse des révélations d'Edward Snowden, agitent la communauté des défenseurs des libertés publiques et de la vie privée. Le coup de projecteur porté sur les pratiques des agences de renseignement anglo-saxonnes est inédit. Le feuilleton semble sans fin, et la puissance de la NSA et de ses partenaires infinie. «L'ère post-Snowden» a vu de nombreux internautes utiliser des outils de cryptographie pour chiffrer leurs communications et passer sous le radar de la NSA.
