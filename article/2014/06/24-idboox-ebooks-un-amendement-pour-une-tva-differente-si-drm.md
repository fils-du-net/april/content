---
site: IDBoox
title: "Ebooks: Un amendement pour une TVA différente si DRM"
author: Elizabeth Sutton
date: 2014-06-24
href: http://www.idboox.com/economie-du-livre/drm-ebooks-un-nouvel-amendement-pour-une-tva-differente
tags:
- April
- Institutions
- DRM
---

> Dans le cadre du projet de loi de finances rectificative pour 2014, les députés associés au groupe écologiste ont déposé l’amendement N°139. Ils demandent que soit apposé un taux de TVA différent si un livre numérique est affublé de verrous numériques (DRM).
