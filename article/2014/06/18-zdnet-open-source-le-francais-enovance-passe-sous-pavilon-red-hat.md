---
site: ZDNet
title: "Open source: le français eNovance passe sous pavilon Red Hat"
date: 2014-06-18
href: http://www.zdnet.fr/actualites/open-source-le-francais-enovance-passe-sous-pavilon-red-hat-39802641.htm
tags:
- Entreprise
- Internet
- Économie
- Informatique en nuage
---

> L'éditeur français s'est taille une belle réputation avec ses logiciels développés sur la plate-forme cloud Openstack utilisés notamment par Cloudwatt. Montant de l'opération: 70 millions d'euros.
