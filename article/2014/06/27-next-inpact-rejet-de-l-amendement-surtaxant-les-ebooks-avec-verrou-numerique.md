---
site: Next INpact
title: "Rejet de l’amendement surtaxant les ebooks avec verrou numérique"
author: Xavier Berne
date: 2014-06-27
href: http://www.nextinpact.com/news/88388-rejet-l-amendement-surtaxant-ebooks-avec-verrou-numerique.htm
tags:
- Interopérabilité
- April
- Institutions
- DRM
---

> L’amendement écologiste visant à réserver le taux de TVA réduite de 5,5 % aux seuls ebooks sans DRM et de format ouvert a été rejeté hier par l’Assemblée nationale. Le gouvernement et le rapporteur du projet de loi de finances rectificative s’y sont - sans surprise - opposés.
