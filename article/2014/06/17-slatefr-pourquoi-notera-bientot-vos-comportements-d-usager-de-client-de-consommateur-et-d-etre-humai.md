---
site: Slate.fr
title: "Pourquoi on notera bientôt vos comportements d’usager, de client, de consommateur et d’être humain"
author: Jean-Laurent Cassely
date: 2014-06-17
href: http://www.slate.fr/story/88115/noter-comportements-usager-client-consommateur
tags:
- Entreprise
- Internet
- Économie
- Innovation
- Vie privée
---

> Dans le futur proche imaginé par l'économiste Tyler Cowen, les technologies permettront aux usagers de noter des services, mais l'inverse sera possible... Autant le dire tout de suite: tout le monde ne sera pas premier de la classe.
