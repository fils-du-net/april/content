---
site: Next INpact
title: "L’Assemblée nationale installera le 11 juin sa commission «numérique»"
author: Xavier Berne
date: 2014-06-05
href: http://www.nextinpact.com/news/87950-l-assemblee-nationale-installera-11-juin-sa-commission-numerique.htm
tags:
- Internet
- Institutions
- Neutralité du Net
- Open Data
- Vie privée
---

> Mercredi prochain, c'est-à-dire le 11 juin, l’Assemblée nationale installera sa commission «ad hoc» dédiée au numérique. L’institution, qui devrait être co-présidée par le député socialiste Christian Paul, comptera parmi ses rangs Philippe Aigrain, de l'association La Quadrature du Net, le fondateur de Mediapart Edwy Plenel, ou bien encore le «monsieur Open Data» de la France, Henri Verdier.
