---
site: Le Figaro.fr
title: "HP France gagne contre l'UFC-Que Choisir"
date: 2014-06-09
href: http://www.lefigaro.fr/flash-eco/2014/06/09/97002-20140609FILWWW00115-hp-france-gagne-contre-l-ufc-que-choisir.php
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
---

> La cour d'appel de Paris a donné raison à Hewlett Packard France, attaqué par l'association de consommateurs UFC-Que Choisir qui contestait la vente liée d'ordinateurs avec des logiciels pré-installés. Dans un arrêt en date du 5 juin, dont l'AFP a obtenu copie, la cour d'appel estime que "la pratique de vente déloyale vis-à-vis des consommateurs reprochée à la société HP France n'est pas constituée". Elle rejette donc la demande de l'UFC-Que Choisir et la condamne à verser 5000 euros à HP France.
