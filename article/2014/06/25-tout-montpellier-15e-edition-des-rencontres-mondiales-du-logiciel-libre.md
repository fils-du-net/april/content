---
site: Tout Montpellier
title: "15e édition des Rencontres Mondiales du Logiciel Libre"
author: Frédéric LEMONNIER
date: 2014-06-25
href: http://www.toutmontpellier.fr/montpellier-15e-edition-des-rencontres-mondiales-du-logiciel-libre--92335.html
tags:
- Associations
- Promotion
---

> Les Rencontres Mondiales du Logiciel Libre (RMLL) se tiendront du 5 au 11 juillet 2014 à Montpellier.
