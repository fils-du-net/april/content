---
site: Next INpact
title: "PNIJ: Mireille Imbert-Quaretta en coulisses du décret Big Brother"
author: Marc Rees
date: 2014-06-25
href: http://www.nextinpact.com/news/88278-pnij-mireille-imbert-quaretta-en-coulisses-decret-big-brother.htm
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Vie privée
---

> Le décret encadrant la plateforme nationale des interceptions judiciaires (PNIJ) sera examiné sous peu par la section du contentieux au Conseil d’État. Surprise, c’est Mireille Imbert-Quaretta qui a été nommée rapporteure du texte, la magistrate qui préside également la Commission de protection des droits au sein de la Hadopi.
