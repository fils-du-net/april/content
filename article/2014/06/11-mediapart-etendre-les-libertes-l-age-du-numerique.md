---
site: Mediapart
title: "Etendre les libertés à l’âge du numérique"
author: Edwy Plenel
date: 2014-06-11
href: http://blogs.mediapart.fr/edition/libres-enfants-du-numerique/article/110614/etendre-les-libertes-l-age-du-numerique
tags:
- Internet
- Institutions
---

> A l’initiative de députés de tous bords, réunis autour du socialiste Christian Paul, l’Assemblée nationale met en place, mercredi 11 juin, une «Commission de réflexion et de propositions sur le droit et les libertés à l’âge du numérique», composée à parité de parlementaires et de représentants de la société civile. Saluant une initiative bienvenue et pluraliste, Mediapart et La Quadrature du net ont accepté d’y participer dans le respect de leur liberté de parole et dans le souci de la publicité des travaux.
