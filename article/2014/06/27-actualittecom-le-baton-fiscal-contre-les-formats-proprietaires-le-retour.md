---
site: ActuaLitté.com
title: "Le bâton fiscal contre les formats propriétaires, le retour"
author: Clément Solym
date: 2014-06-27
href: http://www.actualitte.com/legislation/le-baton-fiscal-contre-les-formats-proprietaires-le-retour-51023.htm
tags:
- Interopérabilité
- Institutions
- DRM
- Europe
---

> Certains savaient qu'il reviendrait, et voilà que dans le cadre du Projet de loi de finance rectificative, l'amendement présenté par Isabelle Attard en novembre 2013 refait surface. Et il embarrasse toujours autant. Soutenu par une vingtaine de députés, il consiste à introduire une sanction fiscale pour tout livre numérique qui ne serait pas «dans un format de données ouvert».
