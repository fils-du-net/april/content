---
site: Le Monde.fr
title: "Vie privée et téléphones: décision cruciale de la Cour suprême américaine"
author: Martin Untersinger
date: 2014-06-25
href: http://www.lemonde.fr/pixels/article/2014/06/25/pas-de-fouille-de-telephone-sans-mandat-juge-la-cour-supreme-americaine_4445199_4408996.html
tags:
- Institutions
- International
- Vie privée
---

> Les téléphones d'aujourd'hui ressemblent davantage à des ordinateurs: la capacité de stockage et la connexion à Internet des smartphones en ont fait les dépositaires d'une grande partie de notre intimité.
