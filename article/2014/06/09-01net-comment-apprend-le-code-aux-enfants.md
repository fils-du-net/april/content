---
site: 01net.
title: "Comment apprend-on le code aux enfants?"
author: Amélie Charnay
date: 2014-06-09
href: http://www.01net.com/editorial/621432/comment-apprend-on-le-code-aux-enfants
tags:
- Internet
- Associations
- Éducation
- Innovation
---

> 87% des Français souhaitent que le code soit enseigné aux enfants. De nombreuses associations le font déjà justement. Alors, nous avons voulu savoir comment on enseigne la programmation à des élèves de primaire. Reportage sur une séance de «Kids coding».
