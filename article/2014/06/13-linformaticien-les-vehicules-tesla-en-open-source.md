---
site: L'Informaticien
title: "Les véhicules Tesla en open source"
author: Guillaume Perissat
date: 2014-06-13
href: http://www.linformaticien.com/actualites/id/33417/les-vehicules-tesla-en-open-source.aspx
tags:
- Entreprise
- Innovation
- Neutralité du Net
- Standards
---

> Tesla Motors a annoncé, par la voix de son fondateur Elon Musk, l’ouverture des brevets relatifs au véhicule électrique. Plus que de la philanthropie, c’est le moyen pour Tesla de standardiser à moindre coût les bornes de rechargement de ses voitures.
