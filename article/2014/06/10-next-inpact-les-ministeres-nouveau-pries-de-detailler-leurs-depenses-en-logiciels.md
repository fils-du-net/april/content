---
site: Next INpact
title: "Les ministères (à nouveau) priés de détailler leurs dépenses en logiciels"
author: Xavier Berne
date: 2014-06-10
href: http://www.nextinpact.com/news/87909-les-ministeres-a-nouveau-pries-detailler-leurs-depenses-en-logiciels.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
---

> Après avoir demandé il y a un an tout juste aux 37 ministres du gouvernement Ayrault de détailler leurs dépenses en logiciels (libres et propriétaires), la députée Isabelle Attard en remet aujourd’hui une couche. L’élue Nouvelle Donne vient en effet d’écrire à chacun des 16 ministres de plein exercice du gouvernement Valls, afin que ceux-ci lèvent un voile supplémentaire sur l’utilisation des logiciels libres au sein de l’administration. En outre, la parlementaire réclame une nouvelle fois des données chiffrées sur les dépenses logicielles de l’État.
