---
site: Internet Policy Review
title: "French parliament forms a committee on digital affairs"
author: Frédéric Dubois
date: 2014-06-10
href: http://policyreview.info/articles/news/french-parliament-forms-committee-digital-affairs/296
tags:
- Internet
- Institutions
- Innovation
- Standards
- Europe
- Open Data
- English
---

> (Le parlement Français vient juste de décider de mettre en place une commission du numérique) The French parliament has just decided to set up a Commission du numérique (Committee on digital affairs). This could change the course of European internet policy.
