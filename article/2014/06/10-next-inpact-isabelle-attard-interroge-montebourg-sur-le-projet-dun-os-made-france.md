---
site: Next INpact
title: "Isabelle Attard interroge Montebourg sur le projet d'un OS «Made in France»"
author: David Legrand
date: 2014-06-10
href: http://www.nextinpact.com/news/86938-isabelle-attard-interroge-montebourg-sur-projet-dun-os-made-in-france.htm
tags:
- Entreprise
- Administration
- Institutions
- Promotion
---

> Il y a quelques semaines, L'Opinion nous apprenait que les services de Bercy se penchaient sur la question de la souveraineté numérique et qu'Arnaud Montebourg avait en tête un projet de système d'exploitation français. Bien que reprenant principalement des propos de Pierre Bellanger, l'article n'a pas tardé à faire réagir à tous les niveaux. Aujourd'hui, c'est la députée Isabelle Attard qui pose officiellement une question au ministre.
