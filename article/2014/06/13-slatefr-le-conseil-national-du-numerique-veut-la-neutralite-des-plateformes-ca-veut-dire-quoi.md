---
site: Slate.fr
title: "Le Conseil national du numérique veut la «neutralité des plateformes». Ça veut dire quoi?"
author: Andréa Fradin
date: 2014-06-13
href: http://www.slate.fr/story/88461/rapport-conseil-national-numerique-neutralite-plateformes
tags:
- Internet
- Institutions
- Neutralité du Net
---

> Les «plateformes» du Net. Il faudra vous faire à cette expression, tant elle est aujourd'hui collée aux lèvres de toute personnalité, politique ou membre de comités de réflexion, en charge de réfléchir à la politique numérique en France et en Europe. Qui sont ces «plateformes»?...
