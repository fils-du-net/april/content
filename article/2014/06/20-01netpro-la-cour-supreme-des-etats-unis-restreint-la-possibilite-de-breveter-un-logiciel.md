---
site: 01netPro.
title: "La cour suprême des Etats-Unis restreint la possibilité de breveter un logiciel"
author: Marie Jung, avec l'AFP
date: 2014-06-20
href: http://pro.01net.com/editorial/622316/la-cour-supreme-des-etats-unis-restreint-la-possibilite-de-breveter-un-logiciel
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Innovation
- International
---

> Dans une décision guettée par de nombreuses firmes informatiques comme Google, IBM et Facebook, la Cour suprême des États-Unis a annulé un brevet car le logiciel se limitait à utiliser "une idée abstraite existante".
