---
site: 01net.
title: "NSA: un premier rapport de transparence... carrément opaque"
author: Gilbert Kallenborn
date: 2014-06-30
href: http://www.01net.com/editorial/622900/nsa-un-premier-rapport-de-transparence-carrement-opaque
tags:
- Internet
- Institutions
- International
- Vie privée
---

> Le gouvernement américain a publié des statistiques sur la surveillance réalisée par ses agences de renseignement. Mais les chiffres ne permettent de tirer aucune conclusion.
