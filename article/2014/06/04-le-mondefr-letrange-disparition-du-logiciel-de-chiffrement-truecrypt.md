---
site: Le Monde.fr
title: "L'étrange disparition du logiciel de chiffrement TrueCrypt"
author: Yves Eudes
date: 2014-06-04
href: http://www.lemonde.fr/pixels/article/2014/06/04/l-etrange-disparition-du-logiciel-truecrypt_4431134_4408996.html
tags:
- Internet
- Logiciels privateurs
- Vie privée
---

> Ce logiciel, recommandé par des ONG et utilisé par Edward Snowden et des journalistes ou militants anti-censure, a subitement été retiré du Web par ses créateurs. Sans explications.
