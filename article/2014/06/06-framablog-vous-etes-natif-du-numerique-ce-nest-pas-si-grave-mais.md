---
site: Framablog
title: "Vous êtes «natif du numérique»? — Ce n'est pas si grave, mais…"
author: Cory Doctorow (traduction Framalang=
date: 2014-06-06
href: http://www.framablog.org/index.php/post/2014/06/05/vous-etes-natifs-num%C3%A9riques-pas-grave-mais
tags:
- Entreprise
- Internet
- Vie privée
---

> Quiconque y prête attention s’apercevra qu’en réalité, les enfants se soucient énormément de leur vie privée. Ils ne veulent surtout pas que leurs parents sachent ce qu’ils disent à leurs amis. Ils ne veulent pas que leurs amis les voient dans leurs relations avec leurs parents. Ils ne veulent pas que leurs professeurs apprennent ce qu’ils pensent d’eux. Ils ne veulent pas que leurs ennemis connaissent leurs peurs et leurs angoisses.
