---
site: Next INpact
title: "Cryptocat: un projet Kickstarter pour chiffrer vos conversations audio et vidéo"
author: Sébastien Gavois
date: 2014-06-30
href: http://www.nextinpact.com/news/88433-cryptocat-projet-kickstarter-pour-chiffrer-vos-conversations-audio-et-video.htm
tags:
- Internet
- Innovation
- Vie privée
---

> Cryptocat vient d'annoncer le lancement d'un projet Kickstarter dont le but est de récolter 45 000 dollars afin de proposer des conversations audio et vidéo chiffrées. Il est également question de l'application Android attendue depuis un moment déjà et d'améliorations significatives sur la mouture iOS.
