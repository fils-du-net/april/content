---
site: Le Monde Informatique
title: "L'arrêt de TrueCrypt laisse de nombreux experts perplexes"
author: Dominique Filippone
date: 2014-06-02
href: http://www.lemondeinformatique.fr/actualites/lire-l-arret-de-truecrypt-laisse-perplexe-de-nombreux-experts-57657.html
tags:
- Internet
- Vie privée
---

> La fin brutale du freeware Open Source TrueCrypt spécialisé dans le chiffrement de disques et de fichiers a laissé place à l'incompréhension dans le milieu de la sécurité. Entre hack massif et arrêt forcé à la Lavabit, les suppositions vont bon train.
