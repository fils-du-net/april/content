---
site: Mediapart
title: "Profil de libriste, Philippe"
author: André Ani
date: 2014-06-23
href: http://blogs.mediapart.fr/blog/andre-ani/230614/profil-de-libriste-philippe
tags:
- Entreprise
- Économie
- Promotion
---

> Bonjour, côté état civil, j’ai 45 ans, marié, deux enfants et un lapin :-) J’ai une formation d’ingénieur en informatique. J’ai travaillé en société de services, dans l’édition de logiciel et j’ai également été responsable informatique. J’ai un peu fait tous les métiers ou presque de l’informatique. Je suis plutôt un généraliste, il n’y a pas vraiment de sujet que je ne puisse pas aborder.
