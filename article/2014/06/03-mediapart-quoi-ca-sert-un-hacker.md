---
site: Mediapart
title: "A quoi ça sert un hacker?"
author: Flo Laval
date: 2014-06-03
href: http://blogs.mediapart.fr/blog/films-dun-jour/030914/quoi-ca-sert-un-hacker
tags:
- Internet
- Partage du savoir
- Sensibilisation
- Vie privée
---

> Au centre du fonctionnement hacker, il y a le logiciel libre, la culture du partage, de la collaboration, la volonté de faire des choses et de rester dans le concret.
