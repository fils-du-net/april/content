---
site: Le Monde.fr
title: "Un an après les révélations Snowden, «relancer le Net»"
author: Yves Eudes
date: 2014-06-05
href: http://www.lemonde.fr/pixels/article/2014/05/19/un-an-apres-les-revelations-snowden-relancer-le-net_4415621_4408996.html
tags:
- Internet
- Institutions
- Associations
- Vie privée
---

> Pour célébrer le premier anniversaire du début des révélations d’Edward Snowden sur les programmes de surveillance de la NSA, l’association américaine de défense des libertés numériques Fight for the Future lance, à partir du 5 juin prochain, une opération baptisée «Reset The Net» - réinitaliser l’Internet.
