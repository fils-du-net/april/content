---
site: Next INpact
title: "Un an après Prism, Reset the Net veut mobiliser autour de la vie privée"
author: Vincent Hermann
date: 2014-06-05
href: http://www.nextinpact.com/news/87957-un-an-apres-prism-reset-the-net-veut-mobiliser-autour-vie-privee.htm
tags:
- Entreprise
- Internet
- Associations
- Vie privée
---

> Le collectif «Fight for the Future» lance aujourd’hui une vaste campagne de communication baptisée «Reset the Net». Sa mission est de sensibiliser les développeurs autant que les internautes aux questions de sécurité information et de respect de la vie privée. Recommandations, outils, support des protocoles de sécurité: la tâche est vaste.
