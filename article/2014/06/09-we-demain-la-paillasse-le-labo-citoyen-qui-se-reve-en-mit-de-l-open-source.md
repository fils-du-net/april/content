---
site: We Demain
title: "La Paillasse, le labo citoyen qui se rêve en «MIT de l’open-source»"
author: Côme Bastin
date: 2014-06-09
href: http://www.wedemain.fr/La-Paillasse-le-labo-citoyen-qui-se-reve-en-MIT-de-l-open-source_a547.html
tags:
- Associations
- Innovation
- Sciences
---

> Ce lieu d’innovation collaborative inaugurait samedi un nouvel espace parisien. Une étape clé pour ce «laboratoire de garage» né en 2011 dans un hackerspace de Vitry-sur-Seine.
