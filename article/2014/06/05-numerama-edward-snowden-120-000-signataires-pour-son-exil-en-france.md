---
site: Numerama
title: "Edward Snowden: 120 000 signataires pour son exil en France"
author: Julien L.
date: 2014-06-05
href: http://www.numerama.com/magazine/29582-edward-snowden-120-000-signataires-pour-son-exil-en-france.html
tags:
- Internet
- Institutions
- Vie privée
---

> Lancée ce mardi sur le site Change.org, la pétition appelant les autorités françaises à accorder l'asile à Edward Snowden pour ses révélations sur l'espionnage de masse a d'ores et déjà accueilli plus de 62 000 signatures. Proposée par l'hebdomadaire l'Express, elle a également été soutenue par 52 personnalités françaises, dont des politiques, des chercheurs, des journalistes et des écrivains.
