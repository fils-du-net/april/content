---
site: Les Numeriques
title: "Maker Faire: le festival du faire soi-même est à Paris ce week-end"
author: Romain Thuret
date: 2014-06-20
href: http://www.lesnumeriques.com/maker-faire-festival-faire-soi-meme-est-a-paris-week-end-n34844.html
tags:
- Partage du savoir
- Matériel libre
- Innovation
---

> Le festival Maker Faire se tient pour la première fois à Paris ce week-end, dans le 19e arrondissement. L'occasion de faire connaissance avec plusieurs acteurs français et internationaux du "Do It Yourself", à coup d'impression 3D, de fabrication et programmation de robots et autres FabLabs innovants, mais aussi de s'adonner à cette pratique.
