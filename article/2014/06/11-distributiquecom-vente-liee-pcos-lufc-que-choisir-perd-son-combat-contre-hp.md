---
site: Distributique.com
title: "Vente liée PC/OS: l'UFC-Que Choisir perd son combat contre HP"
author: Oscar Barthe
date: 2014-06-11
href: http://www.distributique.com/actualites/lire-vente-liee-pc-os-l-ufc-que-choisir-perd-son-combat-contre-hp-21892.html
tags:
- Entreprise
- April
- Institutions
- Vente liée
- Associations
---

> Dans le duel judiciaire qui l'oppose à Hewlett Packard, l'association de consommateurs UFC-Que Choisir vient de se voit infliger un revers par la Cour d'Appel de Paris. Cette dernière a jugé que la vente liée d'un PC et d'un système d'exploitation n'était pas une pratique commerciale déloyale, à l'instar de la Cour de Cassation en juillet 2012.
