---
site: Blogs LeMonde.fr
title: "L’Assemblée se dote d’une commission sur le numérique"
author: Hélène Bekmezian
date: 2014-06-04
href: http://parlement.blog.lemonde.fr/2014/06/04/lassemblee-se-dote-dune-commission-sur-le-numerique
tags:
- Internet
- Institutions
- Neutralité du Net
- Open Data
---

> Droit à l'oubli, cybercriminalité, économie numérique, ouverture des données publiques... Progressivement, de façon disparate mais certaine, Internet s'installe dans la législation et, récemment, le sujet a été abordé dans des textes aussi divers que la loi de programmation militaire, la proposition de loi de lutte contre la prostitution ou encore le projet de loi sur la géolocalisation.
