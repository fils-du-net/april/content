---
site: Silicon.fr
title: "Après le scandale «Made in NSA», la campagne Reset The Net mobilise"
author: Ariane Beky
date: 2014-06-05
href: http://www.silicon.fr/reset-the-net-campagne-ecosysteme-nsa-94861.html
tags:
- Internet
- Institutions
- Associations
- Vie privée
---

> Un an après l’affaire des écoutes américaines, la campagne «Reset The Net» rassemble des acteurs et organisations du numérique. Le but: sensibiliser les développeurs et les internautes aux problématiques de sécurité et de protection de la vie privée.
