---
site: Next INpact
title: "La maîtrise du numérique parmi les «savoirs fondamentaux» des élèves"
author: Xavier Berne
date: 2014-06-11
href: http://www.nextinpact.com/news/88059-la-maitrise-numerique-parmi-savoirs-fondamentaux-eleves.htm
tags:
- Internet
- Droit d'auteur
- Éducation
---

> Quelles sont les connaissances que chaque élève français devrait systématiquement avoir à la fin de sa scolarité obligatoire, c’est-à-dire à 16 ans? Telle est la question à laquelle devait répondre le Conseil supérieur des programmes (CSP) afin de guider le ministère de l’Éducation nationale dans la rédaction d’un «socle commun de connaissances, de compétences et de culture» qui devrait entrer en vigueur à la rentrée 2016. Le numérique est bien entendu au programme. Explications.
