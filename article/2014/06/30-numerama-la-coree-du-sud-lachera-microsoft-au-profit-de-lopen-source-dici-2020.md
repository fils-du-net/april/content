---
site: Numerama
title: "La Corée du Sud lâchera Microsoft au profit de l'Open Source d'ici 2020"
author: Guillaume Champeau
date: 2014-06-30
href: http://www.numerama.com/magazine/29858-la-coree-du-sud-lachera-microsoft-au-profit-de-l-open-source-d-ici-2020.html
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Standards
- International
---

> S'estimant trop dépendante de Microsoft, la Corée du Sud a donné le coup d'envoi d'un plan en faveur des solutions open-source, qu'elle espère achever d'ici 2020.
