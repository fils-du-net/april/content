---
site: Industrie et Technologies
title: "Des surplus à l’Open Source"
author: Jean-François Preveraud
date: 2014-06-24
href: http://www.industrie-techno.com/des-surplus-a-l-open-source.30735
tags:
- Matériel libre
- Innovation
---

> La tendance est à l’Open Source, notamment dans le mouvement Maker et autour des FabLab. Mais est-ce là un phénomène si nouveau? Pas si sûr car l’homme a toujours réutilisé et détourné les objets.
