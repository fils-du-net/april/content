---
site: Numerama
title: "Pourquoi les voitures Tesla passent à l'open-source"
author: Guillaume Champeau
date: 2014-06-13
href: http://www.numerama.com/magazine/29681-pourquoi-les-voitures-tesla-passent-a-l-open-source.html
tags:
- Entreprise
- Innovation
---

> Elon Musk a annoncé que le fabricant de voitures électriques Tesla Motors allait renoncer à l'exclusivité de ses droits sur son portefeuille de brevets. Un choix industriel qui vise à dynamiser le marché, pour bénéficier à Tesla.
