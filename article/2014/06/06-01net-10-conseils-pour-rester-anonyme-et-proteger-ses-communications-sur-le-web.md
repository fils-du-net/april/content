---
site: 01net.
title: "10 conseils pour rester anonyme et protéger ses communications sur le web"
author: Nathan Sommelier
date: 2014-06-06
href: http://www.01net.com/editorial/621326/comment-rester-anonyme-et-proteger-ses-communications-sur-le-web
tags:
- Entreprise
- Internet
- Vie privée
---

> Est-il possible de communiquer sans être espionné, sans que nos conversations, nos mails, notre historique de navigation tombent sur des oreilles indiscrètes? S'il est illusoire de penser que l'on peut être parfaitement anonyme sur le Web, on vous donne des pistes pour mieux vous protéger.
