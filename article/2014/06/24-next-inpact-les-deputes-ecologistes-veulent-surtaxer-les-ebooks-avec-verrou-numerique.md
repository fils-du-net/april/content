---
site: Next INpact
title: "Les députés écologistes veulent surtaxer les ebooks avec verrou numérique"
author: Xavier Berne
date: 2014-06-24
href: http://www.nextinpact.com/news/88308-les-deputes-ecologistes-veulent-surtaxer-ebooks-avec-verrou-numerique.htm
tags:
- April
- Institutions
- DRM
---

> Plus de sept mois après avoir réussi une percée remarquée sur la question des ebooks, les députés écologistes remontent au front. Les parlementaires viennent en effet de redéposer leur amendement visant à réserver la TVA à un taux réduit de 5,5 % aux seuls ebooks sans verrou numérique et/ou de format ouvert. Explications.
