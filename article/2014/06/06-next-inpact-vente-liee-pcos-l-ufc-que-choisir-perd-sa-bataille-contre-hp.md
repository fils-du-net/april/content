---
site: Next INpact
title: "Vente liée PC/OS: l’UFC-Que Choisir perd sa bataille contre HP"
author: Marc Rees
date: 2014-06-06
href: http://www.nextinpact.com/news/88000-vente-liee-pc-os-l-ufc-que-choisir-perd-sa-bataille-contre-hp.htm
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
---

> Ce 5 juin, statuant sur le renvoi de la Cour de cassation, la Cour d’appel de Paris a finalement donné raison à HP dans son litige qui l’opposait à l’UFC-Que Choisir. L’association de consommateur s’était lancée dans une longue bataille avec en ligne de mire, la problématique de la vente liée PC et OS.
