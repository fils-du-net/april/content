---
site: Mediapart
title: "Profil de libriste: Hervé"
author: André Ani
date: 2014-06-22
href: http://blogs.mediapart.fr/blog/andre-ani/220614/profil-de-libriste-herve
tags:
- Internet
- Partage du savoir
- Associations
---

> A force de répéter toujours la même chose, il devient intéressant de le formaliser. C’était donc au début quelques logiciels que je recommandais oralement, puis j’ai fais une liste. Naturellement j’ai ensuite eu envie de la partager en minimisant les efforts. Un site web est le plus approprié
