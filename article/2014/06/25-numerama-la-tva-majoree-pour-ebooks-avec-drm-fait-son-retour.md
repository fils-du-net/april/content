---
site: Numerama
title: "La TVA majorée pour ebooks avec DRM fait son retour"
author: Julien L.
date: 2014-06-25
href: http://www.numerama.com/magazine/29793-la-tva-majoree-pour-ebooks-avec-drm-fait-son-retour.html
tags:
- April
- Institutions
- DRM
- Europe
---

> À l'Assemblée nationale, un amendement propose d'ajuster le taux de TVA des livres électroniques en fonction de la présence ou non des mesures techniques de protection (DRM). Cet amendement, rejeté une première fois l'année dernière, fait son retour à l'occasion du projet de loi de finances rectificative. Les internautes sont invités à se mobiliser.
