---
site: ZDNet
title: "La Corée du Sud veut passer aux logiciels libres et open source d'ici 2020"
author: Thierry Noisette
date: 2014-06-28
href: http://www.zdnet.fr/actualites/la-coree-du-sud-veut-passer-aux-logiciels-libres-et-open-source-d-ici-2020-39803113.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Standards
- International
---

> Poussée par les arrêt passés ou futurs du support de systèmes d'exploitation propriétaires, la Corée du Sud a décidé de se libérer de sa dépendance en faisant passer le secteur public au logiciel libre.
