---
site: Next INpact
title: "Al Gore: Edward Snowden a «rendu un important service»"
author: Vincent Hermann
date: 2014-06-12
href: http://www.nextinpact.com/news/88074-al-gore-edward-snowden-a-rendu-important-service.htm
tags:
- Institutions
- Licenses
- Sciences
- International
- Vie privée
---

> L’ancien vice-président des États-Unis Al Gore a pris position en faveur du service rendu au pays par Edward Snowden. Un avis qui tranche de manière importante avec la position officielle ou encore certains grands patrons tels que Bill Gates.
