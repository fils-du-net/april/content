---
site: Numerama
title: "La TVA majorée pour ebooks avec DRM rejetée par l'Assembe"
author: Julien L.
date: 2014-06-27
href: http://www.numerama.com/magazine/29830-la-tva-majoree-pour-ebooks-avec-drm-rejetee-par-l-assemblee.html
tags:
- April
- Institutions
- DRM
---

> L'Assemblée nationale a rejeté l'amendement du groupe EELV proposant une TVA majorée pour les livres électroniques utilisant des verrous numériques (DRM). Un précédent amendement, déposé l'an dernier, avait connu le même sort.
