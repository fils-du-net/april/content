---
site: ZDNet
title: "Levées de fonds dans l'industrie des logiciels libres et open source"
author: Thierry Noisette
date: 2014-06-30
href: http://www.zdnet.fr/actualites/levees-de-fonds-dans-l-industrie-des-logiciels-libres-et-open-source-39803167.htm
tags:
- Entreprise
- Économie
---

> Ces dernières semaines, plusieurs entreprises du secteur de l'open source ont réalisé des tours de table de millions, voire dizaines de millions, de dollars ou d'euros. Récapitulatif pour Couchbase (USA), Commerce Guys (France) et Odoo (ex-OpenERP, Belgique).
