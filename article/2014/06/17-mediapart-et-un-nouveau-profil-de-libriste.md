---
site: Mediapart
title: "Et un nouveau profil de libriste"
author: André An
date: 2014-06-17
href: http://blogs.mediapart.fr/blog/andre-ani/170614/et-un-nouveau-profil-de-libriste
tags:
- April
- Partage du savoir
- Sensibilisation
- Vie privée
---

> Je m’appelle Olivier, j’ai une formation en sciences humaines, et je suis un ex cadre. J’anime chaque samedi le magazine #HotLine sur Radio Ici&amp;Maintenant! (FM 95.2), émission en direct qui traite d’informatique.
