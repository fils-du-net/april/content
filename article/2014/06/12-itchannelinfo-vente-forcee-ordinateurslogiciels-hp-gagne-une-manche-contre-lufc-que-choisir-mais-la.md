---
site: ITChannel.info
title: "Vente forcee ordinateurs/logiciels: HP gagne une manche contre l'UFC-Que choisir mais la partie n'est peut-être pas terminée"
date: 2014-06-12
href: http://www.itchannel.info/index.php/articles/148797/vente-forcee-ordinateurs-logiciels-hp-gagne-manche-contre-ufc-choisir-mais-partie-est-peut-etre-pas-terminee.html
tags:
- Entreprise
- April
- Institutions
- Vente liée
- Associations
---

> UFC-Que Choisir a perdu en appel son procès contre la vente forcée. Voici la réaction de l'April à laquelle il apparaît qu'une action politique est nécessaire pour faire respecter durablement le droit des consommateurs vis à vis de la vente forcée matériel/logiciel.
