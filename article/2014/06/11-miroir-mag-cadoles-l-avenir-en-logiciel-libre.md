---
site: Miroir Mag
title: "Cadoles: L’avenir en logiciel libre"
author: Marion Chevassus
date: 2014-06-11
href: http://www.miroir-mag.fr/61064-cadoles-lavenir-en-logiciel-libre
tags:
- Entreprise
- Internet
- Économie
- Éducation
---

> “J’ai attendu d’avoir Bac+2 avant de savoir me servir d’un traitement de texte”, Vincent Febvre pense qu’en France, on est très en retard, question informatique. Ce développeur trentenaire de Cadoles, une jeune entreprise dijonnaise, nous explique pourquoi et quelles conséquences pourraient bien avoir l’ignorance manifeste de la langue html sur notre destin. Un travail à reprendre depuis l’école.
