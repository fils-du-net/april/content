---
site: SciDev.Net
title: "Piraterie informatique: redéfinir le développement en 'bidouillant'"
author: Joshua Howgego
date: 2014-06-18
href: http://www.scidev.net/afrique-sub-saharienne/innovation/actualites/piraterie-informatique-red-finir-le-d-veloppement-en-bidouillant.html
tags:
- Partage du savoir
- Matériel libre
- Innovation
- International
---

> Il est ressorti d’une réunion que la diffusion d’une ‘mentalité de pirate’ pour une résolution pratique des problèmes avec les équipements disponibles peut aider à redémarrer le développement - mais seulement avec une gestion coordonnée des informations et des ateliers sur le piratage.
