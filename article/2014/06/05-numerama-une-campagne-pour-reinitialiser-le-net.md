---
site: Numerama
title: "Une campagne pour \"réinitialiser le net\""
author: Julien L.
date: 2014-06-05
href: http://www.numerama.com/magazine/29592-une-campagne-pour-reinitialiser-le-net.html
tags:
- Internet
- Institutions
- Associations
- Vie privée
---

> Un an après les premières publications dans la presse relatives aux activités de la NSA, une opération a été lancée aux États-Unis. Celle-ci appelle à "réinitialiser le net" en invitant les internautes à ne plus demander leur droit à la vie privée, mais à le reprendre directement.
