---
site: Linux Magazine
title: "34 Free Software Advocates Elected to European Parliament"
author: Bruce Byfield
date: 2014-06-18
href: http://www.linuxpromagazine.com/Online/Features/34-Free-Software-Advocates-Elected-to-European-Parliament
tags:
- April
- Institutions
- Sensibilisation
- Europe
- English
---

> (Des groupes de promotion des Logiciels Libres en Europe ont obtenu 162 candidats pro logiciels libres, dont 34 ont été élus au parlement Européen) Free software advocacy groups in Europe signed up 162 pro-free software candidates, 34 of whom were elected to the European Parliament.
