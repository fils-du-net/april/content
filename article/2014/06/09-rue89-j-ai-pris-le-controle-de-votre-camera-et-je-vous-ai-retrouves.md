---
site: Rue89
title: "J’ai pris le contrôle de votre caméra et je vous ai retrouvés"
author: Gurvan Kristanadjaja
date: 2014-06-09
href: http://rue89.nouvelobs.com/2014/06/09/jai-pris-les-commandes-camera-ai-retrouves-252793
tags:
- Internet
- Vie privée
---

> Webcams, imprimantes, portes de garage... Vous n’avez pas protégé vos objets connectés? Dommage. Le moteur de recherche Shodan nous a permis d’en prendre les commandes. Nous avons pu prévenir certains d’entre vous.
