---
site: Framablog
title: "Plus rien ne marche, qu'est-ce qu'on fait?"
author: Quinn Norton (traduction Framalang)
date: 2014-06-02
href: http://www.framablog.org/index.php/post/plus-rien-ne-marche-que-faire
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> Un beau jour un de mes amis a pris par hasard le contrôle de plusieurs milliers d’ordinateurs. Il avait trouvé une faille dans un bout de code et s’était mis à jouer avec. Ce faisant, il a trouvé comment obtenir les droits d’administration sur un réseau. Il a écrit un script, et l’a fait tourner pour voir ce que ça donnerait. Il est allé se coucher et il a dormi environ quatre heures. Le matin suivant, en allant au boulot, il a jeté un coup d’œil et s’est aperçu qu’il contrôlait désormais près de 50 000 ordinateurs.
