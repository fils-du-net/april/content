---
site: Numerama
title: "La France déclare officiellement la guerre à l'ICANN"
author: Guillaume Champeau
date: 2014-06-26
href: http://www.numerama.com/magazine/29816-la-france-declare-officiellement-la-guerre-a-l-icann.html
tags:
- Internet
- Institutions
- International
---

> Après son échec à faire obstacle à la délégation de nouveaux noms de domaine en .vin et .wine, la France a déclaré jeudi la guerre à l'ICANN, en jugeant qu'elle n'était "plus l'enceinte adéquate pour discuter de la gouvernance de l'Internet".
