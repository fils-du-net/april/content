---
site: Direction Informatique
title: "Comment choisir une société de services en logiciel libre?"
author: Laurent Bounin
date: 2014-06-04
href: http://www.directioninformatique.com/blogue/comment-choisir-une-societe-de-services-en-logiciel-libre/27767
tags:
- Entreprise
- Économie
---

> Alors que mon billet précédent présentait plusieurs critères pour vous aider dans la sélection d’un logiciel libre, voici  certaines pistes de réflexion en ce qui a trait au choix de la société qui vous accompagnera tout au long de votre projet.
