---
site: Numerama
title: "La France fait du vin un sujet de rupture avec l'ICANN"
author: Guillaume Champeau
date: 2014-06-23
href: http://www.numerama.com/magazine/29772-la-france-fait-du-vin-un-sujet-de-rupture-avec-l-icann.html
tags:
- Internet
- Institutions
- International
---

> La France peut accepter beaucoup de choses, mais pas que l'on menace son vin. Y compris sur Internet. La secrétaire d'état Axelle Lemaire a ainsi prévenu l'ICANN que la France pourrait précipiter une perte de ses pouvoirs de gestion si elle ne revenait pas sur sa volonté d'attribuer de nouveaux domaines en .vin et .wine sans garantir de protection efficace aux indications géographiques.
