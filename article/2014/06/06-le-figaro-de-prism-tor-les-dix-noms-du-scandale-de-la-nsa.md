---
site: Le Figaro
title: "De Prism à Tor, les dix noms du scandale de la NSA"
date: 2014-06-06
href: http://www.lefigaro.fr/secteur/high-tech/2014/06/06/01007-20140606ARTFIG00093-de-prism-a-tor-les-dix-noms-du-scandale-de-la-nsa.php
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> Le 6 juin 2013, la presse américaine révélait l'existence du programme de surveillance Prism, apportant un nouveau lexique de sigles et de noms devenus familiers. Retour sur une année d'espionnage et de sécurité en ligne.
