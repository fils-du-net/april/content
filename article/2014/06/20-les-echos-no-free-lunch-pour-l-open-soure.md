---
site: Les Echos
title: "\"No free lunch\" pour l’open soure"
author: Charles Cuvelliez et JJ Quisquater
date: 2014-06-20
href: http://www.lesechos.fr/idees-debats/cercle/cercle-101303-no-free-lunch-pour-lopen-source-1015417.php
tags:
- Internet
- Économie
---

> Le bug "OpenHeart" qui a mis à mal la plupart des sites internet sécurisés est une aubaine pour les détracteurs de l’open source et de son modèle d’affaire ni viable ni fiable, selon eux. C’est seulement l’angélisme qui règne autour de l’opensource qui est mis à mal et c’est tant mieux pour lui.
