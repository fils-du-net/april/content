---
site: Numerama
title: "Hadopi, \"non merci!\", dit Axelle Lemaire à Aurélie Filippetti"
author: Guillaume Champeau
date: 2014-06-30
href: http://www.numerama.com/magazine/29861-hadopi-non-merci-dit-axelle-lemaire-a-aurelie-filippetti.html
tags:
- Internet
- HADOPI
- Institutions
---

> Axelle Lemaire demande à Aurélie Filippetti de s'occuper elle-même d'un éventuel transfert de l'Hadopi vers le CSA. La ministre du numérique refuse que ce chapitre soit inscrit à sa loi sur le numérique.
