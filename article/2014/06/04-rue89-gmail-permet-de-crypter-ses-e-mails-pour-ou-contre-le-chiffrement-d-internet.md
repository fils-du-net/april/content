---
site: Rue89
title: "Gmail permet de crypter ses e-mails. Pour ou contre le chiffrement d’Internet?"
author: Philippe Vion-Dury
date: 2014-06-04
href: http://rue89.nouvelobs.com/2014/06/04/gmail-permet-crypter-e-mails-contre-chiffrement-dinternet-252697
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> Pour faire oublier le scandale suscité par les révélations sur le programme Prism, les géants du Net rivalisent d’annonces sur de nouveaux outils et moyens mis en œuvre pour protéger leurs clients.
