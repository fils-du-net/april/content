---
site: Next INpact
title: "Aurélie Filippetti confirme le report du projet de loi Création à début 2015"
author: Marc Rees
date: 2014-06-23
href: http://www.nextinpact.com/news/88279-aurelie-filippetti-confirme-report-projet-loi-creation-a-debut-2015.htm
tags:
- Internet
- HADOPI
- Institutions
---

> Aurélie Filippetti a confirmé que le projet de loi Création ne serait examiné au Parlement qu’au début de l’année prochaine. Pour la Hadopi, c’est sans nul doute un rayon de soleil, alors que les nuages du transfert de ses missions au CSA se dissipent.
