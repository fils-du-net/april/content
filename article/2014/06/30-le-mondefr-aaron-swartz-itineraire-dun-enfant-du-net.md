---
site: Le Monde.fr
title: "Aaron Swartz, itinéraire d'un enfant du Net"
author: Damien Leloup
date: 2014-06-30
href: http://www.lemonde.fr/pixels/article/2014/06/30/aaron-swartz-itineraire-d-un-enfant-du-net_4447830_4408996.html
tags:
- Internet
- Institutions
- Droit d'auteur
- Contenus libres
---

> Le documentaire «The Internet's Own Boy» dresse un long et beau portrait du militant des libertés numériques qui s'est suicidé l'an dernier en pleine procédure judiciaire.
