---
site: Computerworld UK
title: "US Limits Software Patents - As EU Boosts Them"
author: Glyn Moody
date: 2014-06-26
href: http://blogs.computerworlduk.com/open-enterprise/2014/06/us-moves-forward-on-software-patents---as-eu-moves-backwards/index.htm
tags:
- Institutions
- Brevets logiciels
- International
- English
---

> (En Europe la clause "en tant que tel" a permis aux brevets logiciels de prendre pied. Aux États-Unis l'attitude dominante est de pouvoir tout breveter s'il "utilise un ordinateur". Enfin, la cour suprême s'est intéressée au problème) I've written a number of times about the curse of the "as such" clause in Article 52 of the European Patent Convention, which has allowed software patents to creep in to Europe by the backdoor.
