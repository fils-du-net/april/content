---
site: 01net.
title: "Windows préinstallé sur PC: la Justice donne raison à HP face à l'UFC Que Choisir"
author: Pascal Samama
date: 2014-06-09
href: http://www.01net.com/editorial/621430/windows-preinstalle-sur-pc-la-justice-donne-raison-a-hp-face-a-lufc-que-choisir
tags:
- Entreprise
- Institutions
- Associations
---

> Depuis 2006, UFC Que Choisir et HP France s’opposent sur la préinstallation de Windows sur les PC vendus par le fabricant. La cour d’appel de Paris estime que «la pratique de vente déloyale n’est pas constituée».
