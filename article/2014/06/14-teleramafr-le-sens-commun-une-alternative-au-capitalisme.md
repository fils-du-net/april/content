---
site: Télérama.fr
title: "Le sens \"commun\", une alternative au capitalisme ?"
author: Weronika Zarachowicz
date: 2014-06-14
href: http://www.telerama.fr/monde/le-sens-commun-une-alternative-au-capitalisme,113475.php
tags:
- Économie
- Institutions
- Innovation
---

> Partager les idées, les voitures, les connaissances, les maisons. Le “commun” inspire citoyens, philosophes ou juristes… Le philosophe Pierre Dardot et le sociologue Christian Laval nous éclairent sur cette aspiration grandissante.
