---
site: Next INpact
title: "Apprentissage du code en primaire: une loi déposée à l’Assemblée nationale"
author: Xavier Berne
date: 2014-06-13
href: http://www.nextinpact.com/news/88088-apprentissage-code-en-primaire-loi-deposee-a-l-assemblee-nationale.htm
tags:
- Institutions
- Éducation
- Neutralité du Net
---

> Après avoir mis en avant l’idée au travers de son récent rapport co-écrit avec Corinne Erhel, la députée Laure de la Raudière (UMP) vient de déposer une proposition de loi visant à rendre obligatoire l’apprentissage du codage informatique dès l’école primaire.
