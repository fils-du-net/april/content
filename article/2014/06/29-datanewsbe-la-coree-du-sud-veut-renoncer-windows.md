---
site: Datanews.be
title: "La Corée du Sud veut renoncer à Windows"
author: Pieterjan  Vanleemputten
date: 2014-06-29
href: http://datanews.levif.be/ict/actualite/la-coree-du-sud-veut-renoncer-a-windows/article-4000673407536.htm
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Standards
- International
---

> Les autorités sud-coréennes envisagent de migrer vers des systèmes d’exploitation open source. D’ici 2020, elles entendent en effet renoncer à Microsoft.
