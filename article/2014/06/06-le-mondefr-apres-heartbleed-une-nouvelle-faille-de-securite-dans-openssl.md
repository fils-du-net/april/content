---
site: Le Monde.fr
title: "Après Heartbleed, une nouvelle faille de sécurité dans OpenSSL"
author: Audrey Fournier
date: 2014-06-06
href: http://www.lemonde.fr/economie/article/2014/06/06/apres-heartbleed-une-nouvelle-faille-de-securite-dans-openssl_4433841_3234.html
tags:
- Internet
- Économie
- Vie privée
---

> Deux mois après la découverte du bug Heartbleed dans la bibliothèque OpenSSL, qui propose des outils libres et gratuits de cryptage, la chasse aux failles de sécurité a mis au jour de nouvelles vulnérabilités.
