---
site: Next INpact
title: "Hadopi-CSA: le report à 2015 du projet de loi Création se précise"
author: Xavier Berne
date: 2014-06-04
href: http://www.nextinpact.com/news/87929-hadopi-csa-report-a-2015-projet-loi-creation-se-precise.htm
tags:
- Internet
- HADOPI
- Institutions
---

> Le projet de loi «Création», préparé par Aurélie Filippetti afin notamment de transférer les compétences de la Hadopi au CSA, est décidément en bien mauvaise posture. Le texte, qui a notamment souffert du dernier remaniement ministériel et de la pause des municipales, pourrait en effet n’être discuté devant le Parlement que l’année prochaine. Et encore, s’il arrive à sortir de derrière les murs de la Rue de Valois...
