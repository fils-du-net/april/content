---
site: InformatiqueNews.fr
title: "Maker Faire: L'internet des objets s'expose à paris"
author: la rédactio
date: 2014-06-21
href: http://www.informatiquenews.fr/linternet-objets-sexpose-paris-17326
tags:
- Partage du savoir
- Matériel libre
- Innovation
---

> Paris accueille ce week end la première édition de l’exposition «Maker Faire». On peut y découvrir le phénomène du Maker Mouvement au travers de démonstrations, ateliers et conférences. Le 104, un bâtiment d’exposition de la ville de Paris, accueille ce samedi 21 juin et le dimanche 22 juin 2014, une exposition d’un type bien particulier qui mérite le détour. Les exposants appelés «Makers» sont des inventeurs, des créateurs qui partagent la passion de la création, du «do it yourself» sous toutes ses formes (couture, menuiserie, cuisine, impression 3D, robotique, physique, chimie…).
