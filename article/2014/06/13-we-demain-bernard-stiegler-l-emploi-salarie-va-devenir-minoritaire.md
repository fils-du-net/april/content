---
site: We Demain
title: "Bernard Stiegler: «l’emploi salarié va devenir minoritaire»"
author: Côme Bastin
date: 2014-06-13
href: http://www.wedemain.fr/Bernard-Stiegler-l-emploi-salarie-va-devenir-minoritaire_a551.html
tags:
- Entreprise
- Internet
- Économie
- Innovation
- Europe
---

> Alors que les technologies numériques dynamitent les paradigmes du XXe siècle, le philosophe appelle à pousser le web vers un modèle économique basé sur la contribution volontaire et la co-création.
