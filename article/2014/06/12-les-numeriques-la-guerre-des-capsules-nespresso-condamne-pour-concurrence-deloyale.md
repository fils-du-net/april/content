---
site: Les Numeriques
title: "La guerre des capsules: Nespresso condamné pour concurrence déloyale"
author: Alexandra Bellamy
date: 2014-06-12
href: http://www.lesnumeriques.com/cafetiere/guerre-capsules-nespresso-condamne-pour-concurrence-deloyale-n34700.html
tags:
- Entreprise
- Institutions
---

> Nous n'avons de cesse de le souligner lors de nos tests: le modèle économique de Nespresso, en partie basé sur la vente de ses propres capsules, peut constituer une contrainte pour le consommateur. Peu de compatibles et la vente des capsules originales seulement accessible chez Nespresso... Le géant suisse vient d'être condamné par le Tribunal de Commerce de Paris à verser 500 000 € de dommages et intérêts pour concurrence déloyale.
