---
site: Next INpact
title: "Pour Ubisoft les DRM ne peuvent pas stopper le piratage"
author: Kevin Hottot
date: 2014-06-23
href: http://www.nextinpact.com/news/88282-pour-ubisoft-drm-ne-peuvent-pas-stopper-piratage.htm
tags:
- Entreprise
- DRM
---

> Le piratage des jeux sur PC est un sujet délicat pour la plupart des éditeurs. Généralement, des mesures de protection (ou DRM) sont mises en place sur les jeux afin d'en limiter les effets, mais cela reste loin d'être une solution miracle. Selon Ubisoft, les DRM seraient même incapables de stopper le piratage.
