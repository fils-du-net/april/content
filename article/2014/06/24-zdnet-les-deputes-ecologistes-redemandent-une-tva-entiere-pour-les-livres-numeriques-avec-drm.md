---
site: ZDNet
title: "Les députés écologistes redemandent une TVA entière pour les livres numériques avec DRM"
author: Thierry Noisette
date: 2014-06-24
href: http://www.zdnet.fr/actualites/les-deputes-ecologistes-redemandent-une-tva-entiere-pour-les-livres-numeriques-avec-drm-39802907.htm
tags:
- April
- Institutions
- DRM
---

> Réserver la TVA à taux réduit aux e-books en format ouvert et sans mesure technique de protection, c'est ce que propose un amendement EELV, similaire à celui refusé par le gouvernement fin 2013.
