---
site: clubic.com
title: "Tesla pratique l’\"open source\" avec ses brevets sur les véhicules électriques"
author: Olivier Robillart
date: 2014-06-13
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/brevet-logiciel/actualite-709415-tesla-brevets.html
tags:
- Entreprise
- Innovation
---

> Le constructeur d'automobiles électriques met à disposition de ses concurrents ses propres brevets. Tesla espère que la technologie puisse être davantage utilisée par d'autres groupes.
