---
site: Next INpact
title: "Plateformes: le CNNum accepte les atteintes légitimes à la neutralité"
author: Nil Sanyas
date: 2014-06-13
href: http://www.nextinpact.com/news/88120-le-ccnum-accepte-atteintes-legitimes-a-neutralite-net.htm
tags:
- Entreprise
- Internet
- Associations
- Neutralité du Net
---

> Le Conseil national du numérique (CNNum) vient de remettre son rapport sur la neutralité des plateformes en ligne à Arnaud Montebourg et Axelle Lemaire. Un rapport qui comprend quatorze recommandations réparties en quatre volets afin d'établir des règles claires.
