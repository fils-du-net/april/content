---
site: "ouest-france.fr"
title: "«Libre en fête» au lycée de l'Hyrôme samedi"
date: 2014-03-27
href: http://www.ouest-france.fr/libre-en-fete-au-lycee-de-lhyrome-samedi-2056365
tags:
- Administration
- Éducation
- Marchés publics
- Promotion
---

> Pour accompagner l'arrivée du printemps, des événements de découverte des logiciels libres et du «libre» en général sont proposés partout en France.
