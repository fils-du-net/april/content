---
site: Métro
title: "Les candidats interpellés au sujet des logiciels libres"
author: Marie-Eve Shaffer
date: 2014-03-10
href: http://journalmetro.com/dossiers/elections-quebec-2014/460426/les-candidats-interpelles-au-sujet-des-logiciels-libres
tags:
- Institutions
- Associations
- Marchés publics
- Promotion
- International
---

> L’organisme à but non lucratif FACIL enjoint les candidats aux élections provinciales à signer le Pacte du logiciel libre.
