---
site: Silicon.fr
title: "Les experts de l’open source s’affichent contre les brevets logiciels"
author: David Feugey
date: 2014-03-06
href: http://www.silicon.fr/les-experts-americains-de-lopen-source-montent-au-front-contre-les-brevets-logiciels-93100.html
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- International
---

> L’affaire qui oppose Alice Corp. à CLS Bank ouvre la voie à une possible réforme des brevets logiciels outre-Atlantique. Les acteurs de l’open source affichent leur soutien auprès de la Cour suprême des États-Unis.
