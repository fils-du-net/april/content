---
site: ZeVillage.net
title: "Tiers-Lieux open source: on échange en avril à St-Étienne"
author: Eddie Javelle
date: 2014-03-26
href: http://www.zevillage.net/2014/03/26/quinzaine-des-tiers-lieux-opensources-en-avril-st-etienne-en-rhone-alpes
tags:
- Administration
- Associations
- Promotion
---

> La communauté Francophone des Tiers-Lieux se retrouve à Saint-Etienne du 7 au 18 avril 2014. Un événement participatif pour faire découvrir les Tiers-Lieux au plus grand nombre. Des rencontres créatives entre experts et grand public. Biens communs, Open Source, innovation, bidouilles, économie collaborative, économie contributive, design… la Quinzaine des Tiers-Lieux Francophones Open Source couvrira tous ces domaines.
