---
site: Numerama
title: "Des DRM sur les capsules de café"
author: Guillaume Champeau
date: 2014-03-04
href: http://www.numerama.com/magazine/28647-des-drm-sur-les-capsules-de-cafe.html
tags:
- Entreprise
- DRM
---

> En Amérique du Nord, un concurrent de Nespresso a décidé d'ajouter un DRM à ses cafetières, pour qu'elles n'acceptent que les capsules officielles.
