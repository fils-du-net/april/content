---
site: PC INpact
title: "ACTA: deux hauts fonctionnaires de Bercy attaquent une internaute"
author: Marc Rees
date: 2014-03-21
href: http://www.pcinpact.com/news/86523-acta-deux-hauts-fonctionnaires-bercy-attaquent-internaute.htm
tags:
- Institutions
- ACTA
---

> Vendredi dernier, un procès s’est tenu à Paris opposant une internaute, Émilie Colin, à MM. Patrice Guyot et Jean-Philippe Muller, deux hauts fonctionnaires qu’elle avait présentés sous des termes acidulés comme négociateurs français du projet de l’accord anticontrefaçon, ACTA, désormais rejeté par le Parlement européen.
