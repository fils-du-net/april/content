---
site: PC INpact
title: "Neutralité du Net: la commission ITRE de retour demain pour un vote crucial"
author: Nil Sanyas
date: 2014-03-17
href: http://www.pcinpact.com/news/86524-neutralite-net-commission-itre-retour-demain-pour-vote-crucial.htm
tags:
- Internet
- Institutions
- Associations
- Neutralité du Net
- Europe
---

> Fin février, à Strasbourg, la commission industrie, transports, énergie (ITRE) du Parlement européen votait les dispositions du Paquet Télécom de Neelie Kroes portant sur l'itinérance et la neutralité du net. Un vote très attendu, mais qui fut finalement reporté, faute de traduction complète de tous les amendements. Demain, la commission se réunira à 10h pour un vote crucial.
