---
site: Numerama
title: "Neutralité du net: vote crucial en Europe ce mardi"
author: Julien L.
date: 2014-03-17
href: http://www.numerama.com/magazine/28769-neutralite-du-net-vote-crucial-en-europe-ce-mardi.html
tags:
- Internet
- Institutions
- Associations
- Neutralité du Net
- Europe
---

> La commission ITRE du parlement européen doit se prononcer ce mardi sur le règlement "pour un Continent Connecté" proposé par la Commission européenne. Or, le texte contient des dispositions qui remettent en cause le principe de la neutralité du net. La Quadrature du Net appelle les citoyens à réagir.
