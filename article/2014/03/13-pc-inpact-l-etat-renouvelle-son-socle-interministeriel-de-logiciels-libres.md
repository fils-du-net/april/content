---
site: PC INpact
title: "L’État renouvelle son socle interministériel de logiciels libres"
author: Xavier Berne
date: 2014-03-13
href: http://www.pcinpact.com/news/86462-l-etat-renouvelle-son-socle-interministeriel-logiciels-libres.htm
tags:
- Administration
---

> Alors que le ministère de la Défense s’illustre depuis plusieurs années par un manque de transparence quant à ses achats de licences Microsoft, la Direction interministérielle des systèmes d'information et de communication (DISIC) vient de mettre à jour le «socle interministériel de logiciels libres», une sorte de liste de logiciels libres recommandés par l’État à ses administrations.
