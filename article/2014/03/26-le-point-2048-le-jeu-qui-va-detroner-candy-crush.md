---
site: Le Point
title: "2048, le jeu qui va détrôner Candy Crush"
author: Mathilde Lizé
date: 2014-03-26
href: http://www.lepoint.fr/jeux-video/2048-le-jeu-qui-va-detroner-candy-crush-26-03-2014-1805657_485.php
tags:
- Internet
- Innovation
---

> Il rend folles des millions de personnes, et son créateur lui-même a avoué ne l'avoir jamais terminé. 2048 est le nouveau jeu vidéo dont tout le monde parle.
