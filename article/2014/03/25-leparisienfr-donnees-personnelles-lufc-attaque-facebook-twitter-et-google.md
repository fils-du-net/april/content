---
site: leParisien.fr
title: "Données personnelles: l'UFC attaque Facebook, Twitter et Google +"
date: 2014-03-25
href: http://www.leparisien.fr/high-tech/donnees-personnelles-l-ufc-attaque-facebook-twitter-et-google-25-03-2014-3707289.php
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> Après dix mois de négociations avec les grands réseaux sociaux Facebook, Twitter et Google +, l'association de consommateurs UFC Que-Choisir a décidé de saisir la justice devant le tribunal de grande instance de Paris en demandant à ces géants d'internet de clarifier les conditions d'utilisation des données personnelles.
