---
site: 01net.
title: "PopCorn Time, le Netflix pirate, fait son retour après avoir fermé ce week-end"
author: Eric LB 
date: 2014-03-17
href: http://www.01net.com/editorial/616110/popcorn-time-le-netflix-pirate-fait-son-retour-apres-avoir-ferme-ce-week-end
tags:
- Internet
- Droit d'auteur
- Innovation
---

> Les développeurs qui ont mis au point le logiciel de streaming ont préféré abandonner le projet. Mais il a immédiatement été repris en main par l’équipe de YTS-Torrent, qui fournit les films piratés.
