---
site: Numerama
title: "Municipales 2014: 260 candidats ont signé le pacte du logiciel libre"
author: Julien L.
date: 2014-03-21
href: http://www.numerama.com/magazine/28444-municipales-2014-260-candidats-ont-signe-le-pacte-du-logiciel-libre.html
tags:
- April
- Institutions
---

> À l'occasion des prochaines élections municipales, l'APRIL sollicite les candidats pour leur demander de s'engager en faveur du logiciel libre. 45 d'entre d'eux ont d'ores et déjà signé le pacte.
