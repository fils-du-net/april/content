---
site: Lyon Capitale.fr
title: "Lafond signe le pacte logiciel libre, Collomb ne répond pas"
author: Florent Deligia
date: 2014-03-12
href: http://www.lyoncapitale.fr/Journal/Lyon/Politique/Elections/Municipales-2014/Lafond-signe-le-pacte-logiciel-libre-Collomb-ne-repond-pas
tags:
- Administration
- Promotion
---

> Candidat centriste à la mairie de Lyon, Éric Lafond vient d'annoncer qu'il avait signé le pacte logiciel libre.
