---
site: PC INpact
title: "Mozilla enquête sur une installation de Firefox facturée 20 euros chez Dell"
author: Vincent Hermann
date: 2014-03-06
href: http://www.pcinpact.com/news/86326-mozilla-enquete-sur-installation-firefox-facturee-20-euros-chez-dell.htm
tags:
- Entreprise
- Associations
---

> Récemment, une option sur le site anglais de Dell permettait pour au moins une machine d’installer Firefox contre la modique somme de 16,25 livres, soit 19,65 euros environ. Or, il apparaît que Mozilla et Dell n’ont aucun contrat de ce type, ce qui a conduit l’éditeur à démarrer une enquête.
