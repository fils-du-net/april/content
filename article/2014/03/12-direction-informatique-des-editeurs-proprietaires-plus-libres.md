---
site: Direction Informatique
title: "Des éditeurs propriétaires plus libres?"
author: Laurent Bounin
date: 2014-03-12
href: http://www.directioninformatique.com/blogue/des-editeurs-proprietaires-plus-libres-12/25118
tags:
- Entreprise
- Logiciels privateurs
- Sensibilisation
---

> Depuis l’apparition des premiers brevets sur les logiciels, les adeptes du modèle du logiciel libre et ceux du modèle du logiciel propriétaire sont en guerre ouverte.
