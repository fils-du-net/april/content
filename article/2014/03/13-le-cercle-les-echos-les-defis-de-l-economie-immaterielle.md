---
site: Le Cercle Les Echos
title: "Les défis de l’économie immatérielle"
author: David Thesmar
date: 2014-03-13
href: http://lecercle.lesechos.fr/economie-societe/politique-eco-conjoncture/politique-economique/221193390/defis-economie-immateriell
tags:
- Entreprise
- Internet
- Économie
- Innovation
---

> Dans nos économies modernes, la production de choses ne coûte ni ne rapporte presque plus rien. Ce basculement vers l’immatériel rend anachronique toute nostalgie de la France industrielle. Mais il nécessite une profonde réforme de nos représentations de l’économie.
