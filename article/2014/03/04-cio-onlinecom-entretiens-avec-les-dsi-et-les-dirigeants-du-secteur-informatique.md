---
site: "cio-online.com"
title: "Entretiens avec les DSI et les dirigeants du secteur informatique"
author: Bertrand Lemaire
date: 2014-03-04
href: http://www.cio-online.com/entretiens/lire-le-sill-vise-a-harmoniser-le-recours-au-logiciel-libre-par-l-etat-521.html
tags:
- Administration
- Marchés publics
---

> Autant il est impossible d'imposer un logiciel propriétaire dans un appel d'offres, autant cela ne pose pas de problème pour un logiciel libre qui peut être intégré par qui le souhaite. Le Conseil d'Etat a donné raison à une structure publique qui avait fait le choix explicite a priori d'un logiciel libre.
