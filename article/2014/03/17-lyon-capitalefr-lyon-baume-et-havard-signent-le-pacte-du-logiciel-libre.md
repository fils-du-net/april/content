---
site: Lyon Capitale.fr
title: "Lyon: Baume et Havard signent le pacte du logiciel libre"
author: Florent Deligia
date: 2014-03-17
href: http://www.lyoncapitale.fr/Journal/Lyon/Politique/Elections/Municipales-2014/Lyon-Baume-et-Havard-signent-le-pacte-du-logiciel-libre
tags:
- Administration
- Standards
---

> Après Eric Lafond, la semaine dernière, c'est au tour d’Émeline Baume (EELV) et Michel Havard (UMP) d'annoncer qu'ils viennent de signer le pacte du logiciel libre.
