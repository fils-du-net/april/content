---
site: PC INpact
title: "Les députés brésiliens adoptent une loi en faveur de la neutralité du Net"
author: Xavier Berne
date: 2014-03-27
href: http://www.pcinpact.com/news/86722-les-deputes-bresiliens-adoptent-loi-en-faveur-neutralite-net.htm
tags:
- April
- Institutions
- Associations
- Neutralité du Net
- International
- Vie privée
---

> Discuté depuis de longs mois déjà, le «Marco Civil da Internet» a été adopté mardi soir par la Chambre des députés du Brésil. Ce projet de loi, relancé par l’exécutif suite aux révélations d’Edward Snowden, contient des dispositions visant notamment à garantir la neutralité du Net dans ce pays d'Amérique du Sud. Soutenu au niveau international par WikiLeaks, La Quadrature du Net ou Tim Berners-Lee, le texte est l'objet de nombreuses attentions. Désormais, il doit cependant obtenir l’approbation du Sénat.
