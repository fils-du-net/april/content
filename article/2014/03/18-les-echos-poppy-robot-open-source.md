---
site: Les Echos
title: "Poppy, robot «open source»"
author: Frank Niedercorn
date: 2014-03-18
href: http://www.lesechos.fr/entreprises-secteurs/tech-medias/actu/0203379958065-poppy-robot-open-source-657779.php
tags:
- Innovation
- Sciences
---

> Les chercheurs ont doté Poppy d’une morphologie proche de celle des humains, à partir de pièces imprimées en 3D.
