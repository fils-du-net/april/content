---
site: come4news
title: "Demain, la cyberguerre"
author: poissonrouge
date: 2014-03-21
href: http://www.come4news.com/demain,-la-cyberguerre-148363
tags:
- Internet
- Institutions
- International
---

> N'importe quel programme qui n'est pas libre et qui n'est pas développé par nous est susceptible de contenir une backdoor ouverte par l'entreprise qui l'a conçue et qui peut être exploitée. Il faut être conscient de cela et en tirer des leçons : soit n'utiliser que des logiciels libres, soit prendre des dispositions contre d'éventuelles failles ouvertes par les logiciels propriétaires.
