---
site: Numerama
title: "Popcorn Time rend les armes, mais la suite arrive"
author: Guillaume Champeau
date: 2014-03-15
href: http://www.numerama.com/magazine/28767-popcorn-time-rend-les-armes-mais-la-suite-arrive.html
tags:
- Entreprise
- Internet
- Innovation
---

> Considéré comme le "Netflix du piratage", ou le "pire cauchemar d'Hollywood", le logiciel de téléchargement illégal de films Popcorn Time n'existe plus. En tout cas plus dans sa version officielle.
