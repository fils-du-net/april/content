---
site: Le Monde.fr
title: "«2048», stop ou encore?"
author: Marlène Duretz
date: 2014-03-27
href: http://www.lemonde.fr/vous/article/2014/03/27/2048-stop-ou-encore_4390930_3238.html
tags:
- Internet
- Innovation
---

> Ce jeu de réflexion, addictif et chronophage, fait des petits en ligne.
