---
site: "usine-digitale.fr"
title: "Poppy, le robot open source fabriqué avec une imprimante 3D"
author: Lélia de Matharel
date: 2014-03-19
href: http://www.usine-digitale.fr/article/poppy-le-robot-open-source-fabrique-avec-une-imprimante-3d.N249523
tags:
- Matériel libre
- Innovation
- Sciences
---

> Poppy le robot bordelais est la vedette du salon Innorobo, qui se déroule à Lyon du 18 au 20 mars. Et pour cause: ses plan sont accessibles en open source sur Internet et toutes ses pièces rigides peuvent être fabriquées avec une simple imprimante 3D.
