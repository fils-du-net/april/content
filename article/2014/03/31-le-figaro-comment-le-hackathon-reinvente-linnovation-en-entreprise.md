---
site: Le Figaro
title: "Comment le «hackathon» réinvente l'innovation en entreprise"
author: Lucie Ronfaut
date: 2014-03-31
href: http://www.lefigaro.fr/secteur/high-tech/2014/03/31/01007-20140331ARTFIG00112-hackathons-les-marathons-de-l-innovation-font-courir-les-geants-de-l-industrie-et-des-services.php
tags:
- Entreprise
- Éducation
- Innovation
---

> Axa, Pernod Ricard et la SNCF ont adopté les méthodes de Facebook. Elles font plancher des week-ends entiers des génies de l'informatique sur des projets d'applications et de services en ligne. Enquête sur un phénomène.
