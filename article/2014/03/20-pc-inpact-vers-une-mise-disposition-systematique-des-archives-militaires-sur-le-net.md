---
site: PC INpact
title: "Vers une mise à disposition systématique des archives militaires sur le Net?"
author: Xavier Berne
date: 2014-03-20
href: http://www.pcinpact.com/news/86590-vers-mise-a-disposition-systematique-archives-militaires-sur-net.htm
tags:
- Administration
- Partage du savoir
- Institutions
---

> Au travers d’un rapport publié au nom de la Commission des finances du Sénat, deux parlementaires viennent d’en appeler le ministère de la Défense à une mise en ligne systématique de ses archives numérisées - à condition toutefois que celles-ci soient communicables au public. Le numérique est clairement perçu par les élus comme le vecteur d’un souffle nouveau, même s’il peut actuellement poser des difficultés à l’administration.
