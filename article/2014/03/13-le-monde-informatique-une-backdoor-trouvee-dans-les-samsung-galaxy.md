---
site: Le Monde Informatique
title: "Une backdoor trouvée dans les Samsung Galaxy"
author: Jacques Cheminat
date: 2014-03-13
href: http://www.lemondeinformatique.fr/actualites/lire-une-backdoor-trouvee-dans-les-samsung-galaxy-56852.html
tags:
- Entreprise
- Informatique-deloyale
---

> Des développeurs pensent avoir découvert une backdoor dans les terminaux Galaxy de Samsung qui pourrait avoir accès à distance au système de fichiers des smartphones.
