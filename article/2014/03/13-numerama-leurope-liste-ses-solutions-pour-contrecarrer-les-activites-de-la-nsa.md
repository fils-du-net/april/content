---
site: Numerama
title: "L'Europe liste ses solutions pour contrecarrer les activités de la NSA"
author: Julien L.
date: 2014-03-13
href: http://www.numerama.com/magazine/28731-l-europe-liste-ses-solutions-pour-contrecarrer-les-activites-de-la-nsa.html
tags:
- Institutions
- Informatique-deloyale
- Informatique en nuage
- Europe
- International
- Vie privée
---

> Les parlementaires européens ont repris les principales propositions de la commission des libertés civiles (LIBE), allant du chiffrement au logiciel libre en passant par le cloud européen. Ils soutiennent aussi la perspective d'une protection renforcée à l'égard des lanceurs d'alerte.
