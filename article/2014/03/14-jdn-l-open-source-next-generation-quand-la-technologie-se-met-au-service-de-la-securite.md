---
site: JDN
title: "L’open source next-generation: quand la technologie se met au service de la sécurité"
author: Cyrille Badeau
date: 2014-03-14
href: http://www.journaldunet.com/solutions/expert/56799/l-open-source-next-generation---quand-la-technologie-se-met-au-service-de-la-securite.shtml
tags:
- Entreprise
- Sensibilisation
---

> Le concept de logiciel open source est apparu au début des années 80 comme un moyen pour les universitaires spécialisés en informatique et les chercheurs de travailler en collaboration pour élaborer le meilleur logiciel possible et relever de nouveaux défis.
