---
site: l'Humanité.fr
title: "On n’a plus de maitrise sur les données que nous produisons nous même"
author: Pierric Marissal
date: 2014-03-21
href: http://www.humanite.fr/social-eco/google-et-facebook-sont-des-engins-dont-le-carbura-562261
tags:
- Entreprise
- Internet
- Économie
- Vie privée
---

> Entretien avec Antonio Casilli sur le Digital Labor, travail invisible que l’on produit à notre insu pour les géants du Web comme Google et Facebook. Une interview qui vient en complément des pages Travail publiées ce lundi dans l’Humanité.
