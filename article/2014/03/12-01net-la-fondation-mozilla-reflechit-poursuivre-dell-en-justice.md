---
site: 01net.
title: "La Fondation Mozilla réfléchit à poursuivre Dell en justice"
author: Cécile Bolesse
date: 2014-03-12
href: http://www.01net.com/editorial/615830/la-fondation-mozilla-reflechit-a-poursuivre-dell-en-justice
tags:
- Entreprise
- Associations
---

> L'éditeur de Firefox n'a pas apprécié d'apprendre que Dell faisait payer l'installation du navigateur. La fondation Mozilla conteste cette pratique et envisage des poursuites.
