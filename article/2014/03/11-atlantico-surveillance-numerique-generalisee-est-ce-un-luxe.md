---
site: atlantico
title: "Surveillance numérique généralisée: est-ce un luxe?"
author: Jérémie Zimmermann
date: 2014-03-11
href: http://www.atlantico.fr/decryptage/surveillance-numerique-generalisee-capacite-proteger-vie-privee-va-t-elle-devenir-comble-luxe-jeremie-zimmermann-1006880.html
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> La journaliste américaine Julia Angwin dit avoir dépensé 2 200 dollars et passé de nombreuses heures de travail pour protéger ses données numériques. De quoi s'interroger sur notre capacité à protéger notre vie virtuelle.
