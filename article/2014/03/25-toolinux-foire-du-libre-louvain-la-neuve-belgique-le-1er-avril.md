---
site: toolinux
title: "Foire du Libre à Louvain-La-Neuve (Belgique) le 1er avril"
date: 2014-03-25
href: http://www.toolinux.com/Foire-du-Libre-a-Louvain-La-Neuve,20829
tags:
- Éducation
- Promotion
- International
---

> L’édition 2014 de la "Foire du Libre" se prépare activement dans la ville universitaire belge. Le compte a rebours a démarré.
