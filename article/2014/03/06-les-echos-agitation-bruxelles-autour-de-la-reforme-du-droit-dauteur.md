---
site: Les Echos
title: "Agitation à Bruxelles autour de la réforme du droit d'auteur"
author: Renaud Honoré
date: 2014-03-06
href: http://www.lesechos.fr/entreprises-secteurs/tech-medias/actu/0203353817280-agitation-a-bruxelles-autour-de-la-reforme-du-droit-d-auteur-654919.php
tags:
- Entreprise
- Institutions
- Droit d'auteur
- Europe
---

> La Commission avait lancé une consultation auprès de toutes les parties intéressées par une réforme d'un régime du droit d'auteur que remet en question le développement de l'économie numérique. La procédure s'est achevée hier, avec plus de 8.000 réponses, ce qui fait partie des records en la matière
