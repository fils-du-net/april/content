---
site: Le Courrier picard
title: "Beauvais veut devenir la capitale du logiciel libre"
author: FANNY DOLLÉ
date: 2014-03-21
href: http://www.courrier-picard.fr/region/beauvais-veut-devenir-la-capitale-du-logiciel-libre-ia186b0n336537
tags:
- Partage du savoir
- Associations
- Promotion
---

> «Le logiciel libre, c’est comme si on partageait les recettes d’un bon plat qu’on a aimé». Marc Hépiègne, président de l’association Oisux, cherche à vulgariser au mieux l’esprit de sa communauté. Tous ont en commun d’utiliser des logiciels gratuits et participatifs, en opposition aux logiciels propriétaires (Windows, Mac…). Ses membres – une quarantaine d’actifs dans le département – proposent aujourd’hui des initiations dans le cadre de la journée de l’Internet.
