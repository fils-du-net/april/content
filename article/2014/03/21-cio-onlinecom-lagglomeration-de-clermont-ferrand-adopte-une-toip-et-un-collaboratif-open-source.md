---
site: "cio-online.com"
title: "L'Agglomération de Clermont-Ferrand adopte une ToIP et un collaboratif open-source"
author: Bertrand Lemaire
date: 2014-03-21
href: http://www.cio-online.com/actualites/lire-l-agglomeration-de-clermont-ferrand-adopte-une-toip-et-un-collaboratif-open-source-5709.html
tags:
- Logiciels privateurs
- Administration
- Économie
- Interopérabilité
---

> Les 21 communes de Clermont Communauté ont changé leur téléphonie sur IP pour développer les fonctionnalités et baisser les coûts, notamment en y joignant un collaboratif.
