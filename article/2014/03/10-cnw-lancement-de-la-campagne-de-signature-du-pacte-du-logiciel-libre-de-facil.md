---
site: CNW
title: "Lancement de la campagne de signature du Pacte du logiciel libre de FACIL"
date: 2014-03-10
href: http://www.newswire.ca/fr/story/1319595/lancement-de-la-campagne-de-signature-du-pacte-du-logiciel-libre-de-facil
tags:
- Institutions
- Associations
- International
---

> FACIL, pour l'appropriation collective de l'informatique libre (FACIL) procède aujourd'hui au lancement de sa campagne de signature du Pacte du logiciel libre par les candidat(e)s à l'élection québécoise. L'objectif de cette campagne est de recueillir le maximum de signatures du Pacte avant le jour du scrutin le 7 avril 2014.
