---
site: Miroir Social
title: "Comment une direction de Bercy a bataillé pour faire revenir... Microsoft"
author: Robin Carcan
date: 2014-03-11
href: http://www.miroirsocial.com/actualite/9860/comment-une-direction-de-bercy-a-bataille-pour-faire-revenir-microsoft
tags:
- Logiciels privateurs
- Administration
---

> La grande «méchante» suite bureautique de logiciels propriétaires de Microsoft a fait son retour tout récemment au sein de la DNVI (direction nationale des vérifications internationales). Un coup de volant surprenant au sein de l'administration, qui migre depuis des années vers le logiciel libre, pour des raisons à la fois politique et économique. Solidaires finances publiques raconte comment un drôle de grain de sable a grippé ce vaste mouvement migratoire.
