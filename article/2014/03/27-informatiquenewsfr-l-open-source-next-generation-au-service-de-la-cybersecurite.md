---
site: InformatiqueNews.fr
title: "L’open source next-generation au service de la cybersécurité"
author: Cyrille Badeau
date: 2014-03-27
href: http://www.informatiquenews.fr/lopen-source-next-generation-au-service-de-la-cybersecurite-cyrille-badeau-sourcefire-12143
tags:
- Entreprise
- Innovation
---

> Le concept de logiciel open source est apparu au début des années 80 comme un moyen pour les universitaires spécialisés en informatique et les chercheurs de travailler en collaboration pour élaborer le meilleur logiciel possible et relever de nouveaux défis. Alors que l’adoption des nouvelles technologies a trouvé un nouvel élan dans les années 90, l’intérêt pour une approche «ouverte» a continué de croître et les utilisateurs y ont trouvé un réel intérêt.
