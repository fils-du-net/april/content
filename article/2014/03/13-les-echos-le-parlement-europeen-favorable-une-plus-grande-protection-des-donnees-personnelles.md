---
site: Les Echos
title: "Le Parlement européen favorable à une plus grande protection des données personnelles"
author: Sandrine Cassini
date: 2014-03-13
href: http://www.lesechos.fr/entreprises-secteurs/tech-medias/actu/0203372527338-le-parlement-europeen-favorable-a-une-plus-grande-protection-des-donnees-personnelles-656759.php
tags:
- Entreprise
- Internet
- Institutions
- Europe
- Vie privée
---

> Les députés ont adopté deux textes visant à renforcer la protection des internautes, en leur octroyant par exemple la permission d’effacer leurs données. Mais les Etats ont du mal à accepter l’idée d’un cadre juridique unique pour les données personnelles, qui leur ferait perdre leurs prérogatives.
