---
site: ITRnews.com
title: "Red Hat demande à la Cour suprême d’examiner les obstacles à l’innovation que sont les brevets logiciels"
author: ITRnews.com
date: 2014-03-05
href: http://www.itrnews.com/articles/146950/red-hat-demande-cour-supreme-examiner-obstacles-innovation-sont-brevets-logiciels.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Innovation
- International
---

> La requête de Red Hat vise à considérer l’affaire Alice Corp. contre CLS Bank dans le contexte des logiciels Open Source. L'éditeur de logiciels Open Source a déposé cette requête (amicus brief) auprès de la Cour suprême des Etats-Unis à la laquelle elle demande d’examiner la possibilité d’étendre l’interdiction de breveter des idées abstraites aux brevets logiciels. Une telle décision marquerait un net changement de cap de la loi sur les brevets et se traduirait par l’invalidation de très nombreux brevets logiciels.
