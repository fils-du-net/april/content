---
site: PC INpact
title: "Une porte dérobée sur certains terminaux Galaxy de Samsung"
author: Damien Labourot
date: 2014-03-13
href: http://www.pcinpact.com/news/86468-une-porte-derobee-sur-certains-terminaux-galaxy-samsung.htm
tags:
- Entreprise
- Informatique-deloyale
---

> Un développeur travaillant sur Replicant, une version alternative d'Android, indique avoir trouvé des portes dérobées (backdoors) sur certains smartphones et tablettes Samsung. Le modem aurait des droits en lecture et surtout en écriture qui permettraient de manipuler des données à distance.
