---
site: LaDepeche.fr
title: "Maubourguet. Fête de l'internet et du logiciel libre"
author: Janine Noguez
date: 2014-03-24
href: http://www.ladepeche.fr/article/2014/03/24/1846552-maubourguet-fete-de-l-internet-et-du-logiciel-libre.html
tags:
- Administration
- Associations
- Promotion
---

> Du 24 au 29 mars, à la Cyber-Base du Val d'Adour, 2 événements sont fêtés avec des animations gratuites, la Fête de l'internet et Libre en fête.
