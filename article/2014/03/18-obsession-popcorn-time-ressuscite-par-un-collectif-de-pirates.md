---
site: Obsession
title: "Popcorn Time ressuscité par un collectif de pirates"
author: Amandine Schmit
date: 2014-03-18
href: http://obsession.nouvelobs.com/high-tech/20140318.OBS0239/popcorn-time-ressuscite-par-un-collectif-de-pirates.html
tags:
- Internet
- Économie
- Droit d'auteur
- Innovation
---

> Ce logiciel permettait de regarder des films et des séries TV en quelques clics. Après avoir été fermé vendredi par ses fondateurs, le site est de retour.
