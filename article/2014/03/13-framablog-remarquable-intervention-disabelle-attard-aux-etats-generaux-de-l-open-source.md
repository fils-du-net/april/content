---
site: Framablog
title: "Remarquable intervention d'Isabelle Attard aux États Généraux de l’Open Source"
author: Isabelle Attard
date: 2014-03-13
href: http://www.framablog.org/index.php/post/2014/03/13/isabelle-attard-etats-generaux-open-source
tags:
- Logiciels privateurs
- Administration
- Institutions
- Vente liée
- Licenses
- Marchés publics
- Standards
---

> Vous savez que le logiciel libre me tient à cœur. Pas pour des raisons idéologiques ou parce que «c’est à la mode», mais parce que c’est un vrai enjeu de société. Oui, je sais que je conclus les États Généraux de l’Open Source. Je sais aussi que la distinction entre logiciels Open Source et logiciels Libres est un débat virulent entre les partisans de chaque dénomination. Ces distinctions me paraissent trop peu importantes pour faire l’objet d’une argumentation. D’ailleurs, le deuxième groupe de travail aujourd’hui utilise «Open Source», et le quatrième «logiciel libre».
