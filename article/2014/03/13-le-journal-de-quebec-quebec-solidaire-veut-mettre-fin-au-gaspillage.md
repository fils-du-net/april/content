---
site: Le Journal de Québec
title: "Québec solidaire veut mettre fin au gaspillage"
author: Annabelle Caillou
date: 2014-03-13
href: http://www.journaldequebec.com/2014/03/13/quebec-solidaire-veut-mettre-fin-au-gaspillage
tags:
- Économie
- Institutions
- Éducation
- International
---

> Québec solidaire (QS) veut réduire les subventions octroyées aux entreprises multinationales pour «cesser le gaspillage des fonds publics par les vieux partis».
