---
site: clubic.com
title: "Les programmes numériques des candidats aux municipales à Nice"
author: Olivier Robillart
date: 2014-03-11
href: http://pro.clubic.com/technologie-et-politique/actualite-688788-programmes-numeriques-candidats-municipales-nice.html
tags:
- Entreprise
- Administration
- Marchés publics
- Open Data
---

> Jusqu'au premier tour des élections municipales, la rédaction publie les propositions concernant le numérique des principaux candidats des 6 villes les plus peuplées de France. Après Paris, Marseille, Lyon et Toulouse, cette semaine, la parole est donnée aux programmes des candidats à la mairie de Nice.
