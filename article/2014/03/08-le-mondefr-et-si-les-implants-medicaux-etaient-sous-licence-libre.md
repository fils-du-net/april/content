---
site: Le Monde.fr
title: "Et si les implants médicaux étaient sous licence libre?"
author: Alexandre Léchenet
date: 2014-03-08
href: http://www.lemonde.fr/technologies/article/2014/03/08/et-si-les-implants-medicaux-etaient-sous-licence-libre_4379783_651865.html
tags:
- Logiciels privateurs
- Innovation
- Sciences
---

> Lors d'une présentation à «South by Southwest», Karen Sandler a expliqué comment la pose d'un pacemaker l'avait poussé à s'interroger sur le logiciel le faisant fonctionner.
