---
site: L'Informaticien
title: "Popcorn Time ressuscité!"
author: Guillaume Perissat
date: 2014-03-18
href: http://www.linformaticien.com/actualites/id/32440/popcorn-time-ressuscite.aspx
tags:
- Internet
- Droit d'auteur
- Innovation
- Neutralité du Net
---

> Né en février, mort un vendredi (14 mars), ramené à la vie le lundi… Telle est l’extraordianire aventure de Popcorn Time, un logiciel libre de streaming vidéo.
