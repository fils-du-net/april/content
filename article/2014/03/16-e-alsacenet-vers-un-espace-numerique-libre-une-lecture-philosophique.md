---
site: "e-alsace.net"
title: "Vers un espace numérique libre, une lecture philosophique"
author: Véronique Bonnet
date: 2014-03-16
href: http://www.e-alsace.net/index.php/tribune/detail?newsId=192
tags:
- Sensibilisation
- Philosophie GNU
- Sciences
---

> La conférence, intitulée "Vers un espace numérique libre, une lecture philosophique", a le projet de mettre en perspective les idéaux des Lumières et, dans nos existences aussi bien publiques que privées, la place importante qu'a pris l'informatique. Celle-­ci modifie non seulement notre rapport au monde, mais aussi notre rapport aux autres et à nous­-mêmes.
