---
site: PC INpact
title: "Régulation: le président du CSA dans l'oreille de la Commission européenne"
author: Marc Rees
date: 2014-03-04
href: http://www.pcinpact.com/news/86290-regulation-president-csa-dans-oreille-commission-europeenne.htm
tags:
- Internet
- Institutions
- Europe
- International
---

> La nomination d’Olivier Schrameck à la présidence du groupe des régulateurs européens des services de médias audiovisuels (ERGA) n’est pas anodine. Cette élection annoncée aujourd’hui par le CSA, qu’il préside, permettra de mieux distiller encore les préconisations du rapport Lescure.
