---
site: ZDNet
title: "Distributeurs de billets: de Windows XP à Linux?"
author: Olivier Chicheportiche
date: 2014-03-21
href: http://www.zdnet.fr/actualites/distributeurs-de-billets-de-windows-xp-a-linux-39798851.htm
tags:
- Entreprise
- Logiciels privateurs
- Économie
---

> Avec la fin programmée du support de Windows XP et de Windows XP Embbeded, les banques gestionnaires de distributeurs se posent sérieusement la question de l'open source.
