---
site: Libération.fr
title: "La querelle des algorithmes scolaires"
author: Léa Lejeune
date: 2014-03-27
href: http://www.liberation.fr/economie/2014/03/24/la-querelle-des-algorithmes-scolaires_989290
tags:
- Institutions
- Éducation
---

> Les rudiments de la programmation et du code informatique enseignés dès le primaire? L’idée agite la sphère geek. Lubie ou projet visionnaire?
