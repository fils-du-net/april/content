---
site: Le Figaro
title: "Cinéma: l'application Popcorn Time relancée par un collectif de pirates"
author: Guillaume Millochau
date: 2014-03-17
href: http://www.lefigaro.fr/secteur/high-tech/2014/03/17/01007-20140317ARTFIG00239-cinema-l-application-popcorn-time-relancee-par-un-collectif-de-pirates.php
tags:
- Internet
- Droit d'auteur
- Innovation
---

> L'application pour regarder des films en haute définition gratuitement, abandonnée par ses concepteurs, a été ressuscitée.
