---
site: Numerama
title: "L'architecte de la loi Hadopi nommé par Aurélie Filippetti au CSPLA"
author: Guillaume Champeau
date: 2014-03-05
href: http://www.numerama.com/magazine/28662-l-architecte-de-la-loi-hadopi-nomme-par-aurelie-filippetti-au-cspla.html
tags:
- Entreprise
- HADOPI
- Institutions
- Droit d'auteur
---

> Ancien conseiller de l'ancienne ministre de culture Christine Albanel, recruté il y a un an par SFR, Olivier Henrard a été désigné par Aurélie Filippetti pour représenter les opérateurs télécoms au Conseil supérieur de la propriété littéraire et artistique (CSPLA), qui doit étudier plusieurs sujets en lien avec les droits d'auteur dans l'univers numérique.
