---
site: Le Monde Informatique
title: "Démission groupée chez Mozilla pour déstabiliser un CEO contesté"
author: Jacques Cheminat
date: 2014-03-31
href: http://www.lemondeinformatique.fr/actualites/lire-demission-groupee-chez-mozilla-pour-destabiliser-un-ceo-conteste-57030.html
tags:
- Internet
- Associations
---

> La nomination de Brendan Eich comme CEO de Mozilla ne fait pas que des heureux. Après une campagne sur les réseaux sociaux demandant la démission du dirigeant pour ses positions politiques, trois membres du conseil d'administration ont rendu leur mandat.
