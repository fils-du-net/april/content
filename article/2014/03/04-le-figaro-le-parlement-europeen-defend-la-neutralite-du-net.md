---
site: Le Figaro
title: "Le Parlement européen défend la neutralité du Net"
date: 2014-03-04
href: http://www.lefigaro.fr/secteur/high-tech/2014/04/03/01007-20140403ARTFIG00224-le-parlement-europeen-defend-la-neutralite-du-net.php
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- Europe
---

> Le parlement européen s'est prononcé jeudi en faveur de la «neutralité du Net», qui garantit l'accès légal et sans discrimination à InterNet pour tous les citoyens.
