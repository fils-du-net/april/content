---
site: PC INpact
title: "L'après-Hadopi: les (très) grandes lignes du projet de loi sur la Création"
author: Marc Rees
date: 2014-03-12
href: http://www.pcinpact.com/news/86434-lapres-hadopi-tres-grandes-lignes-projet-loi-sur-creation.htm
tags:
- Internet
- Économie
- HADOPI
- Institutions
- Droit d'auteur
---

> À l’occasion de la réponse de la France à Bruxelles sur le droit d’auteur, Aurélie Filippetti a esquissé des détails sur la future grande loi sur la création, celle qui devrait sauf surprise, faire passer la riposte graduée de la Hadopi au CSA.
