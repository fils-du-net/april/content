---
site: ZDNet
title: "Le moment parfait pour des compagnies open source milliardaires, selon un investisseur"
author: Thierry Noisette
date: 2014-03-28
href: http://www.zdnet.fr/actualites/le-moment-parfait-pour-des-compagnies-open-source-milliardaires-selon-un-investisseur-39799187.htm
tags:
- Entreprise
- Économie
- Innovation
---

> Pour le financier Mike Volpi, dont le fonds a investi dans une vingtaine de start-up, dont Hortonworks qui vient de lever 100 millions de dollars, «le moment est venu de faire de l'argent dans l'open source».
