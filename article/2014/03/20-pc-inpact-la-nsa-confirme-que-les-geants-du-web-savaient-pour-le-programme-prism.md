---
site: PC INpact
title: "La NSA confirme que les géants du web savaient pour le programme Prism"
author: Vincent Hermann
date: 2014-03-20
href: http://www.pcinpact.com/news/86591-la-nsa-confirme-que-geants-web-savaient-pour-programme-prism.htm
tags:
- Entreprise
- Internet
- Institutions
- International
- Vie privée
---

> Alors que les grandes firmes américaines du web s’acharnent depuis des mois à soigner leur communication autour de Prism, la NSA a clairement indiqué hier au Sénat américain qu’elles étaient parfaitement au courant de la collecte qui était pratiquée. Une double confirmation qui envoie une nouvelle onde de choc, bien plus forte, dans le monde de l’informatique et du respect de la vie privée.
