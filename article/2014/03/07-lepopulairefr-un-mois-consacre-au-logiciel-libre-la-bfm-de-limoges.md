---
site: lepopulaire.fr
title: "Un mois consacré au logiciel libre à la BFM de Limoges"
author: Marion Buzy
date: 2014-03-07
href: http://www.lepopulaire.fr/limousin/actualite/2014/03/07/un-mois-consacre-au-logiciel-libre-a-la-bfm-de-limoges_1903834.html
tags:
- Logiciels privateurs
- Associations
- Promotion
---

> Si vous souhaitez avoir un minimum de main mise sur ce que fait votre ordinateur, le mois du logiciel libre, qui commence le 8 mars à la BFM, est fait pour vous.
