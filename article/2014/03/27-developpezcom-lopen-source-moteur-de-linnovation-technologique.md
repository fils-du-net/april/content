---
site: Developpez.com
title: "L'open source, moteur de l'innovation technologique?"
author: Hinault Romaric
date: 2014-03-27
href: http://www.developpez.com/actu/69401/L-open-source-moteur-de-l-innovation-technologique-80pourcent-de-logiciels-commerciaux-seront-bases-sur-des-piles-open-source
tags:
- Entreprise
- Économie
- Innovation
- Informatique en nuage
---

> 80% de logiciels commerciaux seront basés sur des piles open source.
