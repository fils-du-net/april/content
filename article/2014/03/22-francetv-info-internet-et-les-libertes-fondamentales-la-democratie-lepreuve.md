---
site: francetv info
title: "Internet et les libertés fondamentales: la démocratie à l'épreuve"
author: Benjamin  Loveluck
date: 2014-03-22
href: http://www.francetvinfo.fr/internet/tribune-internet-et-les-libertes-fondamentales-la-democratie-a-l-epreuve_557781.html
tags:
- Internet
- Institutions
- International
- Vie privée
---

> Leurs visages ont été projetés sur écran géant lors de la récente conférence SXSW, grand-messe sur les médias tenue à Austin, au Texas. Ils ont pris la parole via une mauvaise connexion, hachée et entrecoupée, rappelant au public leur présence lointaine et leur statut de fugitifs. Et ils ont renouvelé leur appel à défendre internet contre les assauts de l’Etat américain. Juliian Assange a évoqué une "occupation militaire d’internet", la "militarisation d’un espace civil". Edward Snowden a expliqué que la NSA avait "mis le feu" au réseau.
