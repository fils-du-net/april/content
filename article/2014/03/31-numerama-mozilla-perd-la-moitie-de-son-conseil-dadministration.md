---
site: Numerama
title: "Mozilla perd la moitié de son conseil d'administration"
author: Guillaume Champeau
date: 2014-03-31
href: http://www.numerama.com/magazine/28922-mozilla-perd-la-moitie-de-son-conseil-d-administration.html
tags:
- Internet
- HADOPI
- Associations
---

> Sur fond de tensions stratégiques, trois des six membres du Conseil d'administration de Mozilla ont décidé de quitter le navire, pour marquer leur désaccord avec le choix du nouveau directeur Brendam Eich.
