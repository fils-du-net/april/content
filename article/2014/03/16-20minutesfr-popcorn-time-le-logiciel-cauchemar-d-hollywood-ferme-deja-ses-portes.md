---
site: 20minutes.fr
title: "Popcorn Time, le logiciel cauchemar d’Hollywood, ferme déjà ses portes"
date: 2014-03-16
href: http://www.20minutes.fr/high-tech/1324526-popcorn-time-le-logiciel-cauchemar-d-hollywood-ferme-deja-ses-portes
tags:
- Entreprise
- Internet
- Droit d'auteur
- Innovation
---

> Les développeurs de Popcorn Time expliquent dans une lettre être «extrêmement fiers» du logiciel surnommé ces derniers jours le «Netflix du piratage», qui n’a pas survécu mais aura permis de «reconnaître que l’industrie du cinéma a établi bien trop de restrictions sur bien trop de marchés»…
