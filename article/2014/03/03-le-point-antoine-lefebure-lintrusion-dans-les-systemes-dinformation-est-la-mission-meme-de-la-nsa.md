---
site: Le Point
title: "Antoine Lefébure: \"L'intrusion dans les systèmes d'information est la mission même de la NSA\""
author: Jean Guisnel
date: 2014-03-03
href: http://www.lepoint.fr/editos-du-point/jean-guisnel/antoine-lefebure-l-intrusion-dans-les-systemes-d-information-est-la-mission-meme-de-la-nsa-03-03-2014-1797268_53.php
tags:
- Internet
- Institutions
- Informatique-deloyale
- Vie privée
---

> Edward Snowden a dessillé les yeux de la planète! La tentaculaire surveillance mondiale de la NSA a rendu notre monde transparent pour l'État américain. Décryptage.
