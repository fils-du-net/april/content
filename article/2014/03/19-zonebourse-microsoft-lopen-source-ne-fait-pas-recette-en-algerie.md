---
site: zonebourse
title: "Microsoft: L'Open Source ne fait pas recette en Algérie"
date: 2014-03-19
href: http://www.zonebourse.com/MICROSOFT-CORPORATION-4835/actualite/Microsoft--LOpen-Source-ne-fait-pas-recette-en-Algerie-18119489
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Institutions
- International
---

> L'Administration algérienne ne s'intéresse pas aux logiciels gratuits Open Source. Plus de cinq ans après l'élaboration du projet e-Algérie, qui prévoyait l'introduction graduelle de l'Open Source, aucune démarche concrète n'a été menée. L'informatique dans des institutions stratégiques continue à fonctionner avec des logiciels propriétaires fermés.
