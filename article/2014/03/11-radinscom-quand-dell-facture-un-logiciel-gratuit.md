---
site: radins.com
title: "Quand Dell facture un logiciel gratuit..."
author: Sylvain Leleu
date: 2014-03-11
href: http://news.radins.com/actualites/quand-dell-facture-un-logiciel-gratuit,7572.html
tags:
- Internet
- Associations
---

> Le fabricant d'ordinateurs Dell, numéro 2 mondial, demande à ses clients de débourser 19 € pour l'installation du navigateur Firefox. Ce logiciel, qui permet de navigateur sur Internet, est pourtant disponible gratuitement au téléchargement.
