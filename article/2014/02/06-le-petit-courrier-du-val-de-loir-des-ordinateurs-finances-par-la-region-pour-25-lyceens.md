---
site: Le Petit Courrier du Val de Loir
title: "Des ordinateurs financés par la Région pour 25 lycéens"
author: Maxime Davoust
date: 2014-02-06
href: http://www.lecourrier-lecho.fr/2014/02/08/des-ordinateurs-finances-par-la-region-pour-25-lyceens
tags:
- Administration
- Économie
- Éducation
---

> L'opération Ordipass mise en oeuvre par la Région Pays de la Loire, permet à des lycéens de bénéficier d'un ordinateur à prix réduit.
