---
site: la ligue de l'enseignement
title: "Priorité au logiciel libre"
author: Denis Lebioda
date: 2014-02-11
href: http://www.laligue-alpesdusud.org/associatifs_leblog/?2014/02/11/4120-priorite-au-logiciel-libre
tags:
- April
- Promotion
---

> L'April lance une grande campagne d'adhésion sur le thème «donnons la priorité au logiciel libre».
