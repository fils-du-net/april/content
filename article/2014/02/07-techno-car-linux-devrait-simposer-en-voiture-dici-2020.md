---
site: "Techno-Car"
title: "Linux devrait s'imposer en voiture d'ici 2020"
author: Benoît Solivellas
date: 2014-02-07
href: http://techno-car.fr/linux-devrait-simposer-en-voiture-dici-2020
tags:
- Entreprise
- Innovation
---

> Une étude du cabinet IHS affirme que Linux sera le premier système d’exploitation utilisé par les les systèmes multimédia embarqués, à partir de 2020. Un domaine dominé aujourd’hui par QNX.
