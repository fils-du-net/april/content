---
site: ZDNet
title: "Grande cause nationale: c’est manqué pour le numérique"
author: Christophe Auffray
date: 2014-02-14
href: http://www.zdnet.fr/actualites/grande-cause-nationale-c-est-manque-pour-le-numerique-39797830.htm
tags:
- Institutions
- Éducation
- International
---

> Pédagogie et formation au numérique ne seront pas une cause nationale en 2014 pour le gouvernement qui a choisi pour ce label l’engagement associatif. Le numérique a néanmoins occupé une bonne place dans le déplacement du président aux US.
