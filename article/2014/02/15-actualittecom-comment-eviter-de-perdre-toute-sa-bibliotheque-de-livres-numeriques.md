---
site: ActuaLitté.com
title: "Comment éviter de perdre toute sa bibliothèque de livres numériques"
date: 2014-02-15
href: http://www.actualitte.com/usages/comment-eviter-de-perdre-toute-sa-bibliotheque-de-livres-numeriques-48214.htm
tags:
- Entreprise
- DRM
---

> Après les remous qu'a provoqués la société Adobe, annonçant que son DRM Musclor, plus résistant aux attaques de pirates, allait être installé prochainement, les utilisateurs sont en droit de se poser quelques questions. D'abord, pourquoi des DRM... Mais surtout, avec une pareille mise à jour, les anciens fichiers seront-ils lisibles de nouveau ? Excellentes questions, pas de doute. Dans tous les cas, il importe de préserver sa bibliothèque numérique, et pour ce faire, quelques solutions pratiques existent.
