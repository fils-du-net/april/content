---
site: PC INpact
title: "Jacques Sauret, de Terra Nova, nous parle d’e-administration"
author: Xavier Berne
date: 2014-02-03
href: http://www.pcinpact.com/news/85468-interview-jacques-sauret-terra-nova-nous-parle-d-e-administration.htm
tags:
- Internet
- Administration
- Institutions
- RGI
---

> Alors que Matignon a présenté il y a plusieurs semaines différentes mesures en faveur de la modernisation de l’action publique (sur l'Open Data, la mutualisation au sein des systèmes d’information de l’État, l'e-administration,...), PC INpact a pu interroger Jacques Sauret, l’ancien président de l'Agence pour le développement de l'administration électronique (ADAE). Et pour cause: l’intéressé a travaillé pendant plusieurs mois sur la rédaction d'un rapport à ce sujet pour la fondation Terra Nova, traditionnellement située sur la gauche de l’échiquier politique.
