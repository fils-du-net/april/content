---
site: Mediapart
title: "L'April lance une grande campagne d'adhésion «Donnons la priorité au logiciel libre»"
author: Frédéric Couchet
date: 2014-02-14
href: http://blogs.mediapart.fr/blog/frederic-couchet/140214/lapril-lance-une-grande-campagne-dadhesion-donnons-la-priorite-au-logiciel-libre
tags:
- April
- Promotion
---

> L'April vient de lancer, avec le soutien de Richard Stallman (président de la Fondation pour le Logiciel Libre), une campagne d'adhésion sur le thème «donnons la priorité au logiciel libre».
