---
site: atlantico
title: "L'impression 3D met-elle en péril la propriété intellectuelle?"
author: Erwan Le Noan
date: 2014-02-06
href: http://www.atlantico.fr/decryptage/impression-3d-met-elle-en-peril-propriete-intellectuelle-erwan-noan-974415.html
tags:
- Entreprise
- Partage du savoir
- DRM
- Innovation
- Neutralité du Net
---

> Si l'impression des objets en 3D devient accessible au grand public, désormais habitué à ce que beaucoup de données soient gratuites et se partagent entre les utilisateurs sur Internet, les producteurs verront la protection des droits de leur création menacée. Décryptage comme chaque semaine dans la rubrique du "buz du bizz".
