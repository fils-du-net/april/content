---
site: PC INpact
title: "L’Assemblée nationale se dote d’une commission dédiée au numérique"
author: Xavier Berne
date: 2014-02-26
href: http://www.pcinpact.com/news/86171-l-assemblee-nationale-se-dote-d-une-commission-dediee-au-numerique.htm
tags:
- Internet
- Institutions
- Vie privée
---

> L’Assemblée nationale devrait installer d’ici quelques semaines une toute nouvelle commission chargée de mener des réflexions et de formuler des recommandations sur le thème du droit et des libertés «à l’âge du numérique». L'initiative a été lancée par les élus de la majorité, et ce après les épisodes difficiles de ces derniers mois, à propos notamment la loi de programmation militaire.
