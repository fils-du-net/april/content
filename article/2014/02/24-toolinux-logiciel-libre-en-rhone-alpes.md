---
site: toolinux
title: "Logiciel libre en Rhône-Alpes"
date: 2014-02-24
href: http://toolinux.com/Logiciel-libre-en-Rhone-Alpes
tags:
- Administration
- Économie
- Interopérabilité
- April
- Open Data
---

> Peu d’élus défendent avec conviction et constance ce modèle de développement de l’informatique. Pourtant le logiciel libre est indéniablement un moteur pour l’économie locale.
