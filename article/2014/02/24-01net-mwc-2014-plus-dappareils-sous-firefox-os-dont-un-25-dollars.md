---
site: 01net.
title: "MWC 2014: plus d'appareils sous Firefox OS, dont un à 25 dollars"
author: Cécile Bolesse
date: 2014-02-24
href: http://www.01net.com/editorial/614724/mwc-2014-plus-d-appareils-sous-firefox-os-dont-un-a-25-dollars
tags:
- Associations
- Innovation
---

> La Fondation Mozilla a profité du salon de Barcelone pour faire plusieurs annonces dont celle d'un smartphone d'entrée de gamme à 25 dollars.
