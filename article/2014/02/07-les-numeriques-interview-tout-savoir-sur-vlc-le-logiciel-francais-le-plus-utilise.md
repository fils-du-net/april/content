---
site: Les Numeriques
title: "Interview: tout savoir sur VLC, le logiciel français le plus utilisé"
author:  Vincent Alzieu
date: 2014-02-07
href: http://www.lesnumeriques.com/appli-logiciel/interview-tout-savoir-sur-vlc-logiciel-francais-plus-utilise-a1777.html
tags:
- Associations
- Innovation
---

> VLC est le logiciel français le plus utilisé au monde. Il a été téléchargé 1,6 milliards de fois. Interview en 10 questions d'un des responsables du projet: Jean-Baptiste Kempf.
