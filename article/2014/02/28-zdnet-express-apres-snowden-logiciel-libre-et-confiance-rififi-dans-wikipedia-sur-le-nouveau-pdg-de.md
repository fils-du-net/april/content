---
site: ZDNet
title: "Express: après Snowden, logiciel libre et confiance, rififi dans Wikipédia sur le nouveau PDG de Radio France, cours en ligne"
author: Thierry Noisette
date: 2014-02-28
href: http://www.zdnet.fr/actualites/express-apres-snowden-logiciel-libre-et-confiance-rififi-dans-wikipedia-sur-le-nouveau-pdg-de-radio-france-cours-en-ligne-39798195.htm
tags:
- Administration
- April
- Institutions
- Promotion
---

> Bouquet de brèves: une revue de web avec les logiciels libres, brique essentielle pour lutter contre la surveillance orwellienne des NSA du monde, curieuses tentatives de censure dans l'article de Wikipédia sur Mathieu Gallet, et leçons d'informatique libre.
