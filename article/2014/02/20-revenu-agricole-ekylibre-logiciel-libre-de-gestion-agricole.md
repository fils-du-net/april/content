---
site: Revenu Agricole
title: "Ekylibre: logiciel libre de gestion agricole"
author: Virginie Parmentier
date: 2014-02-20
href: http://www.revenuagricole.fr/focus-gestion/gestion-fiscalite-epargne/gestion-en-bref/11217-ekylibre-logiciel-libre-de-gestion-agricole
tags:
- Entreprise
- Associations
---

> L'originalité de ce logiciel: il vient du monde «libre». Il peut s'adapter aux besoins de chaque exploitation. L'agriculteur peut y entrer sa comptabilité, mais aussi gérer en temps réel son stock, ses ventes, ses achats ou encore la traçabilité de sa production. Ekylibre est une application web, donc naturellement multi-postes. Il est accessible depuis un ordinateur, un smartphone ou une tablette et peut donc suivre en temps réel l'agriculteur sur son exploitation.
