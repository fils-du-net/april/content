---
site: Numerama
title: "OpenKnit: une imprimante de vêtements open-source"
author: Guillaume Champeau
date: 2014-02-21
href: http://www.numerama.com/magazine/28513-openknit-une-imprimante-de-vetements-open-source.html
tags:
- Partage du savoir
- Matériel libre
- Innovation
---

> OpenKnit est un métier à tisser open-source, qui permet d'imprimer des vêtements à la demander, et de partager ses créations grâce à un format standard.
