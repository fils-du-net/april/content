---
site: Numerama
title: "\"Récupérez votre vie privée!\", un clip soutenu par la Quadrature du Net"
author: Julien L.
date: 2014-02-12
href: http://www.numerama.com/magazine/28379-recuperez-votre-vie-privee-un-clip-soutenu-par-la-quadrature-du-net.html
tags:
- Associations
- Vie privée
---

> Soutenu par la Quadrature du Net et financé par les internautes, le clip d'animation sur la vie privée réalisé par Benoît Musereau a été achevé ces jours-ci
