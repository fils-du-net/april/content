---
site: Les Echos
title: "VLC le n° 1 des logiciels français"
author: Erick Hess
date: 2014-02-12
href: http://www.lesechos.fr/entreprises-secteurs/tech-medias/actu/0203299295042-vlc-le-n-1-des-logiciels-francais-649810.php
tags:
- Associations
- Innovation
- International
---

> Avec plus de 100 millions d'utilisateurs dans le monde, le lecteur vidéo VLC, célèbre pour son cône de chantier, est le logiciel français le plus populaire, mais aussi le moins rentable…
