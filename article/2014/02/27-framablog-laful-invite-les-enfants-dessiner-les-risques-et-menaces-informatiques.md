---
site: Framablog
title: "L'AFUL invite les enfants à dessiner les risques et menaces informatiques"
author: aKa
date: 2014-02-27
href: http://www.framablog.org/index.php/post/2014/02/27/aful-dessine-moi-menaces-informatiques
tags:
- Associations
- Éducation
- Informatique-deloyale
- Licenses
---

> À l’occasion de la publication de son document «Comment se protéger de l’informatique, ou l’informatique à l’ère post-Snowden» l’AFUL organise une grande activité de dessin pour les enfants en leur demandant «Dessine-moi les menaces informatiques!».
