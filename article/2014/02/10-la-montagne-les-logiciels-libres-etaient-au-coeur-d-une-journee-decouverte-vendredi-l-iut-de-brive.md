---
site: la montagne
title: "Les logiciels libres étaient au cœur d’une journée découverte, vendredi, à l’IUT de Brive"
author: Ophélie Crémillieux
date: 2014-02-10
href: http://www.lamontagne.fr/limousin/actualite/departement/correze/brive/2014/02/10/les-logiciels-libres-etaient-au-cur-dune-journee-decouverte-vendredi-a-liut-de-brive_1866793.html
tags:
- Associations
- Promotion
---

> L’association Pullco a organisé, vendredi, une journée découverte des logiciels libres; des logiciels aux possibilités illimitées.
