---
site: Le Monde Informatique
title: "Mozilla va intégrer de la publicité dans Firefox"
author: Jacques Cheminat
date: 2014-02-12
href: http://www.lemondeinformatique.fr/actualites/lire-mozilla-va-integrer-de-la-publicite-dans-firefox-56552.html
tags:
- Internet
- Associations
---

> L'éditeur Open Source va intégrer des publicités dans son navigateur avec des liens sponsorisés. L'objectif est d'augmenter les revenus de la Fondation, très dépendante actuellement des reversements de Google.
