---
site: Mediapart
title: "Lutter pour nos données."
author: André Ani
date: 2014-02-15
href: http://blogs.mediapart.fr/blog/andre-ani/150214/lutter-pour-nos-donnees
tags:
- Internet
- Associations
- International
- Vie privée
---

> L’affaire Snowden a révélée au grand public la surveillance de masse, généralisée. Mais nous le savions déjà.
