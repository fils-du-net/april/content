---
site: "Solutions-Logiciels.com"
title: "La région Rhône-Alpes passe au logiciel libre"
author: Juliette Paoli
date: 2014-02-24
href: http://www.solutions-logiciels.com/actualites.php?actu=14440
tags:
- Administration
- April
- Associations
- Promotion
---

> Dans une délibération du 20 février 2014, la région Rhône-Alpes a décidé de s'engager en faveur du logiciel libre.
