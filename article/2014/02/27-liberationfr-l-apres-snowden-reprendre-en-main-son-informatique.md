---
site: Libération.fr
title: "L’après-Snowden: reprendre en main son informatique"
author: Frédéric Couchet et Lionel Allorge
date: 2014-02-27
href: http://ecrans.liberation.fr/ecrans/2014/02/24/l-apres-snowden-reprendre-en-main-son-informatique_982609
tags:
- Internet
- Logiciels privateurs
- April
- Institutions
- Informatique-deloyale
- Promotion
- Vie privée
---

> Que nous révèle Edward Snowden? Tout d’abord que, contrairement à ce que tendraient à faire croire les glapissements indignés de l’administration américaine, ce n’est pas une affaire d’Etat, mais l’affaire de tous les Etats. La faiblesse, voire l’absence, des réactions diplomatiques montre assez que Prism et sa galaxie de programmes de surveillance, si elles se sont construites sous l’égide de la NSA, constituent aujourd’hui en réalité une Bourse mondiale d’échanges de données personnelles à laquelle tous les alliés des Etats-Unis participent de très près ou d’à peine plus loin.
