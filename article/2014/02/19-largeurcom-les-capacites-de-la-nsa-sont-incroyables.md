---
site: Largeur.com
title: "«Les capacités de la NSA sont incroyables»"
author: Daniel Saraga
date: 2014-02-19
href: http://www.largeur.com/?p=4106
tags:
- Entreprise
- Internet
- Institutions
- Standards
- Vie privée
---

> La surveillance étatique d'internet inquiète les professionnels du secteur. Pour l'expert américain en sécurité Bruce Schneier, la solution devra passer par la politique. Interview.
