---
site: PC INpact
title: "Pacte Défense Cyber: les axes majeurs et les actions concrètes"
author: Vincent Hermann
date: 2014-02-10
href: http://www.pcinpact.com/news/85829-pacte-defense-cyber-axes-majeurs-et-actions-concretes.htm
tags:
- Internet
- Logiciels privateurs
- HADOPI
- Institutions
- Sciences
---

> Comme prévu, Jean-Yves Le Drian, ministre de la Défense, a présenté vendredi les grandes lignes du « Pacte Défense Cyber », un grand plan de cyberdéfense venant se greffer à la loi de programmation militaire. Évalué à un coût d’un milliard d’euros, il sera développé sur six axes majeurs.
