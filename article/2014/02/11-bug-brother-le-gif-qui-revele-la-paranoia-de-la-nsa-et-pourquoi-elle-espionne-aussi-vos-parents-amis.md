---
site: Bug Brother
title: "Le .gif qui révèle la paranoïa de la NSA, et pourquoi elle espionne aussi vos parents &amp; amis"
author: Jean-Marc Manach
date: 2014-02-11
href: http://bugbrother.blog.lemonde.fr/2014/02/11/le-gif-qui-revele-la-paranoia-de-la-nsaqui-espionne-donc-aussi-vos-enfants-parents-amis
tags:
- April
- Institutions
- Vie privée
---

> Plus de 5000 sites web ont décidé de se mettre en berne, ce mardi 11 février 2014, afin de dénoncer la "surveillance de masse" mise en place par la NSA, les "grandes oreilles" américaines (&amp; britanniques, canadiennes, australiennes, néo-zélandaises, associées à de nombreux autres pays -dont la France), et d'appeler à l'adoption des 13 principes internationaux sur l’application des droits de l’Homme à la surveillance des communications rédigés par plus de 360 ONG et juristes du monde entier.
