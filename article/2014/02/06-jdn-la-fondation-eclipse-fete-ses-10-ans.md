---
site: JDN
title: "La fondation Eclipse fête ses 10 ans"
author: Antoine Crochet-Damais
date: 2014-02-06
href: http://www.journaldunet.com/developpeur/java-j2ee/eclipse-10e-anniversaire-0214.shtml
tags:
- Entreprise
- Associations
- Innovation
- Licenses
---

> Lancée par IBM pour développer une infrastructure Java Open Source, la fondation Eclipse orchestre 10 ans après sa création près de 250 projets et compte plus de 200 membres.
