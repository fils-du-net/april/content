---
site: PC INpact
title: "Le contrat open bar entre Microsoft et la Défense sous le prisme du Sénat"
author: Marc Rees
date: 2014-02-27
href: http://www.pcinpact.com/news/86200-le-contrat-open-bar-entre-microsoft-et-defense-sous-prisme-senat.htm
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- April
- Marchés publics
---

> Le contrat open bar signé entre Microsoft et la Défense continue à questionner les parlementaires. Ceux-ci s’interrogent tant sur son opportunité que sur les conditions de passation de ce marché.
