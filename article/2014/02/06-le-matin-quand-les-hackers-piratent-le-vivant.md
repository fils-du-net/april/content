---
site: Le Matin
title: "Quand les hackers piratent le vivant"
author: Simon Koch
date: 2014-02-06
href: http://www.lematin.ch/high-tech/hackers-piratent-vivant/story/30454173
tags:
- Partage du savoir
- Matériel libre
- Innovation
- Sciences
---

> Le génie génétique se pratique aussi en hobby, dans des laboratoires communautaires et indépendants. Thomas Landrain, figure du mouvement «DIY bio» en France, a plaidé sa cause lors de la conférence Lift à Genève.
