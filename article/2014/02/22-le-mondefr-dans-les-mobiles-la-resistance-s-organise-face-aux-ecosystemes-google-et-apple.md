---
site: Le Monde.fr
title: "Dans les mobiles, la résistance s’organise face aux écosystèmes Google et Apple"
author: Sarah Belouezzane
date: 2014-02-22
href: http://www.lemonde.fr/economie/article/2014/02/22/dans-les-mobiles-la-resistance-s-organise-face-aux-ecosystemes-google-et-apple_4371628_3234.html
tags:
- Économie
- Associations
- Innovation
---

> Les systèmes d'exploitation Tizen OS et Firefox OS seront présentés à l’occasion du «Mobile World Congress» cette semaine à Barcelone.
