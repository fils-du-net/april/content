---
site: Silicon.fr
title: "La DARPA publie un catalogue de projets open source"
author: David Feugey
date: 2014-02-06
href: http://www.silicon.fr/darpa-publie-catalogue-projets-open-source-92540.html
tags:
- Administration
- Partage du savoir
- Informatique en nuage
- Europe
---

> Le DARPA Open Catalog regroupe les logiciels que cette agence de la défense américaine est en droit de publier. Un vaste ensemble d’outils open source qui devrait vivement intéresser la communauté.
