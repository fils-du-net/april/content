---
site: ZDNet
title: "Cnil: réélection serrée de sa présidente Isabelle Falque-Pierrotin"
author: Thierry Noisette
date: 2014-02-05
href: http://www.zdnet.fr/actualites/cnil-reelection-serree-de-sa-presidente-isabelle-falque-pierrotin-39797633.htm
tags:
- Internet
- Institutions
- Sciences
- Vie privée
---

> La Commission Informatique et libertés, dont 11 des 17 membres viennent d'être nommés, parmi lesquels 8 nouveaux entrants, a réélu la présidente sortante, d'une voix. Les attributions internes seront fixées dans deux semaines.
