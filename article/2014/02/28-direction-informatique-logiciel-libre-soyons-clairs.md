---
site: Direction Informatique
title: "Logiciel libre: soyons clairs!"
author: Laurent Bounin
date: 2014-02-28
href: http://www.directioninformatique.com/blogue/logiciel-libre-soyons-clairs/24874
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Associations
- Marchés publics
- Philosophie GNU
- International
---

> Depuis quelques années, la présence du logiciel libre s’impose dans le paysage médiatique québécois. En effet, plusieurs événements consécutifs lui ont progressivement conféré une place de choix dans nos quotidiens.
