---
site: La Semaine
title: "Une imprimante 3D pliable unique au monde A Folschviller, les pionniers d'un monde libre"
author: Arnaud STOERKLER
date: 2014-02-23
href: http://www.lasemaine.fr/2014/02/13/a-folschviller-les-pionniers-d-un-monde-libre
tags:
- Matériel libre
- Innovation
---

> Concepteur et vendeur de la première imprimante 3D pliable, ultra-légère et ‘‘RepRap’’ (qui peut créer certaines pièces qui la composent) au monde, Emmanuel Gilloz vient d'ouvrir à Folschviller un FabLab, atelier participatif dédié aux technologies de demain. Il continue d’y développer sa machine sous licence libre, associé à l'entrepreneur forbachois Alain Skiba.
