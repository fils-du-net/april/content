---
site: InformatiqueNews.fr
title: "Qui fabrique Linux?"
author: Guy Hervier
date: 2014-02-03
href: http://www.informatiquenews.fr/qui-fabrique-linux-9717
tags:
- Entreprise
- Associations
- Innovation
---

> Depuis 1991 suite à l’appel de Linus Torvald, Linux a fait bien du chemin. Il est au cœur de presque tous les systèmes informatiques, à l’exception des PC sur lesquels Windows règne encore en maître.
