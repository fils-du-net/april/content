---
site: Agoravox TV
title: "ThinkerView: \"Allons-nous vers une Cyber Dictature?\" avec Jérémie Zimmermann et Éric Filiol"
author: BlueMan
date: 2014-02-12
href: http://www.agoravox.tv/tribune-libre/article/thinkerview-allons-nous-vers-une-43445
tags:
- Internet
- Institutions
- Associations
- International
- Vie privée
---

> Cette vidéo est un entretien organisé par ThinkerView avec Jérémie Zimmermann (La Quadrature du Net) et Éric Filiol (ex DGSE, hacker) qui donnent leur point de vue sur la surveillance abusive à travers internet. Allons-nous vers une cyber dictature?
