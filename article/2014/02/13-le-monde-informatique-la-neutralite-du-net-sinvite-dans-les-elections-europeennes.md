---
site: Le Monde Informatique
title: "La neutralité du Net s'invite dans les élections européennes"
author: Jean Elyan avec IDG News Service
date: 2014-02-13
href: http://www.lemondeinformatique.fr/actualites/lire-la-neutralite-du-net-s-invite-dans-les-elections-europeennes-56561.html
tags:
- April
- Institutions
- Associations
- Neutralité du Net
- Europe
- Vie privée
---

> Des responsables politiques européens misent sur la neutralité du net et la confidentialité des données pour gagner des suffrages. À l'approche des élections au Parlement européen, qui auront lieu en mai, certains parlementaires de l'UE ont signé la «charte des droits numériques».
