---
site: "cio-online.com"
title: "Jacques Attali: «Les logiciels propriétaires sont un obstacle à l'innovation»"
author: Bertrand Lemaire
date: 2014-02-11
href: http://www.cio-online.com/actualites/lire-%a0les-logiciels-proprietaires-sont-un-obstacle-a-l-innovation%a0-5627.html
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Éducation
---

> Lors du Gala des DSI, Jacques Attali participait à une table ronde. L'économiste s'est lancé dans une diatribe contre les rentes de situation.
