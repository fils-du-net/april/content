---
site: PC INpact
title: "Les échanges non marchands et la mort des DRM expurgés du rapport Castex"
author: Marc Rees
date: 2014-02-17
href: http://www.pcinpact.com/news/85987-les-echanges-non-marchands-et-mort-drm-expurges-rapport-castex.htm
tags:
- Économie
- Partage du savoir
- Institutions
- DRM
- Droit d'auteur
- Europe
---

> C’est fait! Les eurodéputés, membres de la commission des affaires juridiques, ont adopté la semaine dernière le rapport de Françoise Castex sur la copie privée. Ce texte, quoique dénué de force juridique, entend apporter une réponse parlementaire à un rapport dit Vitorino, à la commission européenne, document qui avait largement déplu aux sociétés de gestion collective.
