---
site: Le Monde Numerique
title: "Google.fr a publié l'annonce de la sanction de la CNIL en Home Internet amende cnil google gouvernement"
author: Nourdine
date: 2014-02-08
href: http://www.lemondenumerique.com/article-30724-google-fr-a-publie-l-annonce-de-la-sanction-de-la-cnil-en-home.html
tags:
- Entreprise
- Internet
- Institutions
- Sciences
- Vie privée
---

> C’est aujourd’hui samedi 8 février que le géant américain de l'internet Google a mis en ligne sur sa page d'accueil française un encart mentionnant sa condamnation à une amende de 150.000 euros par la Commission nationale de l'informatique et des libertés.
