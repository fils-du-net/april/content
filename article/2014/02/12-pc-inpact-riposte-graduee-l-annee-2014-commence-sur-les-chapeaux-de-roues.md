---
site: PC INpact
title: "Riposte graduée: l’année 2014 commence sur les chapeaux de roues"
author: Xavier Berne
date: 2014-02-12
href: http://www.pcinpact.com/news/85869-riposte-graduee-l-annee-2014-commence-sur-chapeaux-roues.htm
tags:
- Internet
- HADOPI
---

> La Hadopi vient de commencer l’année 2014 sur les chapeaux de roues. En effet, selon la Haute autorité (PDF), plus de 130 000 courriels d’avertissement ont été adressés le mois dernier aux abonnés français flashés sur les réseaux peer-to-peer. Un niveau historiquement élevé qui souligne bien la montée de régime du dispositif.
