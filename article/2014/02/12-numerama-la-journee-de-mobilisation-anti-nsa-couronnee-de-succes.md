---
site: Numerama
title: "La journée de mobilisation anti-NSA couronnée de succès"
author: Julien L.
date: 2014-02-12
href: http://www.numerama.com/magazine/28382-la-journee-de-mobilisation-anti-nsa-couronnee-de-succes.html
tags:
- Internet
- Institutions
- International
- Vie privée
---

> La journée de protestation organisée le 11 février contre la NSA a été un vrai succès. Des milliers de sites ont participé au mouvement, tandis que les Américains ont contacté massivement leurs élus. À l'étranger, une pétition a également recueilli un soutien très important.
