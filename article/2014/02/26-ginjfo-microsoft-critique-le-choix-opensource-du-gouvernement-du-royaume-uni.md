---
site: GinjFo
title: "Microsoft critique le choix «OpenSource» du gouvernement du Royaume-Uni"
author: Jérôme Gianoli
date: 2014-02-26
href: http://www.ginjfo.com/actualites/logiciels/microsoft-critique-le-choix-opensource-du-gouvernement-du-royaume-uni-20140226
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Institutions
- Associations
- International
---

> Microsoft a du mal à accepter que le gouvernement du Royaume-Uni lui face faux bond en abandonnant ses outils bureautiques pour passer dans le camp de l’ennemi, l’open-source. Pour LibreOffice, Microsoft ne cherche qu’une seule chose, défendre ses intérêts financiers.
