---
site: Framablog
title: "L'April en campagne - Entretien avec Lionel Allorge et Frédéric Couchet"
author: aKa
date: 2014-02-12
href: http://www.framablog.org/index.php/post/2014/02/12/april-campagne
tags:
- April
- Sensibilisation
---

> L’April repart en campagne «afin d’augmenter sa capacité d’action, de donner la priorité au logiciel libre et de contribuer à construire une société plus libre, plus égalitaire et plus fraternelle».
