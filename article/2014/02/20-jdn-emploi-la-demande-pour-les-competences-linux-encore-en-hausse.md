---
site: JDN
title: "Emploi: la demande pour les compétences Linux encore en hausse"
author: Virgile Juhan
date: 2014-02-20
href: http://www.journaldunet.com/solutions/emploi-rh/linux-emploi-et-salaire-0214.shtml
tags:
- Entreprise
- Économie
- Associations
- International
---

> Une étude réalisée par Dice montre que l'intérêt des recruteurs pour les spécialistes de Linux ne cesse de croître. Les salaires suivent évidemment cette hausse.
