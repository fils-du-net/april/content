---
site: ActuaLitté.com
title: "ReLIRE et Google Books: 'selon que vous serez puissant ou misérable'"
author: Nicolas Gary
date: 2014-02-28
href: http://www.actualitte.com/legislation/relire-et-google-books-selon-que-vous-serez-puissant-ou-miserable-48495.htm
tags:
- Institutions
- Associations
- Droit d'auteur
---

> Le Conseil Constitutionnel a validé la Constitutionnalité de la loi portant sur la numérisation des oeuvres indisponibles du XXe siècle. Mais dans leurs commentaires, les Sages laissent passer un point particulièrement épineux, concernant le défaut d'exploitation des oeuvres imprimées. La décision semble en effet accepter, de bonne grâce, que l'éditeur soit pris en flagrant délit de non-commercialisation d'une oeuvre. Et ce, alors qu'il est censé, contractuellement, en assurer l'exploitation. Ce n'est pourtant pas le seul point qui pose problème…
