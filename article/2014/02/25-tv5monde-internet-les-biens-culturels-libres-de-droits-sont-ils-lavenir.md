---
site: TV5Monde
title: "Internet: les biens culturels “libres de droits“ sont-ils l'avenir?"
date: 2014-02-25
href: http://www.tv5.org/cms/chaine-francophone/info/Les-dossiers-de-la-redaction/ACTA/p-27560-Internet-les-biens-culturels-libres-de-droits-sont-ils-l-avenir-.htm
tags:
- Entreprise
- Internet
- HADOPI
- Associations
- DRM
- Droit d'auteur
- Licenses
- Contenus libres
---

> La Commission européenne a lancé une nouvelle consultation sur le droit d'auteur en décembre dernier: Internet reste, en 2014, un enjeu central pour la diffusion des œuvres culturelles. La suprématie commerciale des “firmes culturelles“ n'est pourtant pas totalement acquise en termes de propriété intellectuelle. Pour preuve, les différents modèles de droits de propriété intellectuelle “libres“ qui prospèrent sur la toile. Quel futur modèle pour la culture européenne sur Internet?
