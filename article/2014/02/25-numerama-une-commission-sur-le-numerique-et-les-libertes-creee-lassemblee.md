---
site: Numerama
title: "Une commission sur le numérique et les libertés créée à l'Assemblée"
author: Guillaume Champeau
date: 2014-02-25
href: http://www.numerama.com/magazine/28569-une-commission-sur-le-numerique-et-les-libertes-creee-a-l-assemblee.html
tags:
- Internet
- Institutions
- Vie privée
---

> L'Assemblée Nationale crée une commission temporaire "de réflexion et de propositions sur le droit et les libertés à l'âge du numérique", qui devra rendre ses propositions d'ici 6 mois à 1 an, avec une composition mixte entre députés et personnalités extérieures. Bonne, ou inquiétante nouvelle?
