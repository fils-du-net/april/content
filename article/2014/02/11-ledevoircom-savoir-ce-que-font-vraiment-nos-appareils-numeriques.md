---
site: LeDevoir.com
title: "Savoir ce que font vraiment nos appareils numériques"
author: Fabien Deglise
date: 2014-02-11
href: http://www.ledevoir.com/societe/actualites-en-societe/399612/savoir-ce-que-font-vraiment-nos-appareils-numeriques
tags:
- Entreprise
- Internet
- Institutions
- International
- Vie privée
---

> Une vaste coalition d'organismes et d'individus de tous les horizons, dont FACIL a l'honneur de faire partie, a décrété que le 11 février serait une journée internationale de protestation contre l'espionnage de masse de la National Security Agency (NSA) et autres agences de renseignement comparables. Le choix du 11 février permet de commémorer à la fois l'anniversaire du décès tragique du militant Aaron Swartz et celui de l'importante victoire contre le liberticide projet de loi américain SOPA, il y a deux ans, en février 2012. (Voir https://thedaywefightback.org.)
