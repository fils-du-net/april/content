---
site: GreenIT.fr
title: "Sobriété fonctionnelle: la clé de l’écoconception des logiciels?"
author: Frédéric Bordage
date: 2014-02-21
href: http://www.greenit.fr/article/logiciels/sobriete-fonctionnelle-la-cle-de-l-ecoconception-des-logiciels-5101
tags:
- Économie
- Innovation
---

> Plus je travaille sur des projets concrets d’écoconception de logiciel et plus il m’apparaît évident que le principal levier, pour réduire leurs impacts environnementaux, réside dans la sobriété fonctionnelle.
