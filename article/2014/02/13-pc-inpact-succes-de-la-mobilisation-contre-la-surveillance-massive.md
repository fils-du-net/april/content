---
site: PC INpact
title: "Succès de la mobilisation contre la surveillance massive"
author: Nil Sanyas
date: 2014-02-13
href: http://www.pcinpact.com/news/85909-succes-mobilisation-contre-surveillance-massive.htm
tags:
- Entreprise
- Internet
- April
- Associations
- Vie privée
---

> Afin de protester contre la surveillance de masse, en particulier celle opérée par la NSA, de nombreuses associations ont organisé ce mardi 11 février une opération «The Day We Fight Back». Cette journée de riposte a été appuyée par de nombreuses associations du globe, et près de 245 000 signatures d'internautes ont été collectées. Certains grands noms du Web, dont Google, Microsoft et Facebook, ont apporté leur soutien.
