---
site: 20minutes.fr
title: "Espionnage: Des technologies pour échapper à la NSA?"
author: Anaëlle Grondin
date: 2014-02-11
href: http://www.20minutes.fr/high-tech/1296002-espionnage-des-technologies-pour-echapper-a-la-nsa
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> Applications, logiciels, smartphones «inviolables»… Des entreprises surfent sur la vague de paranoïa engendrée par les révélations sur la surveillance de l'Agence nationale de sécurité américaine…
