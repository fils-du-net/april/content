---
site: Libération.fr
title: "La NSA a les grandes oreilles qui sifflent"
author: Erwan CARIO
date: 2014-02-17
href: http://ecrans.liberation.fr/ecrans/2014/02/13/la-nsa-a-les-grandes-oreilles-qui-sifflent_980117
tags:
- Internet
- Institutions
- Associations
- International
- Vie privée
---

> Mardi, des milliers d’internautes ont voulu reprendre la main. De nombreuses organisations à travers le monde ont annoncé «The day we fight back», «le jour où nous contre-attaquons». L’adversaire, c’est la surveillance globale orchestrée (principalement) par les Etats-Unis et leur National Security Agency, dont le sigle NSA a connu un regain de notoriété depuis juin et les révélations d’Edward Snowden.
