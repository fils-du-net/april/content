---
site: Numerama
title: "La loi Numérique mettra l'économie d'un côté, la liberté de l'autre"
author: Guillaume Champeau
date: 2014-02-24
href: http://www.numerama.com/magazine/28537-la-loi-numerique-mettra-l-economie-d-un-cote-la-liberte-de-l-autre.html
tags:
- Économie
- Institutions
---

> Le grand projet de loi sur le numérique sera envoyé dans les prochains jours au Conseil National du Numérique, avant une présentation en conseil des ministres au mois de juin. Le texte sera composé en deux volets, l'un économique, l'autre sur les libertés. Mais les deux peuvent-ils vraiment être séparés?
