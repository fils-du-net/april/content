---
site: Libération.fr
title: "L’hérésie joyeuse de la gratuité"
author: Jean-Louis Sagot-Duvauroux
date: 2014-02-26
href: http://www.liberation.fr/politiques/2014/02/26/l-heresie-joyeuse-de-la-gratuite_983111
tags:
- Entreprise
- Administration
- Économie
---

> Elle vient par d’autres voies, car les transports publics ne sont pas le seul champ où la gratuité est efficace et désirable. Plusieurs collectivités pratiquent la gratuité d’un quota d’eau considéré comme vital. D’autres ont institué la gratuité des obsèques, entourant ainsi le chagrin d’une solidarité concrète et libérant les endeuillés de la pénible négociation avec les entreprises de pompes funèbres. D’autres encore s’inscrivent dans le mouvement du logiciel libre qui porte des formes très novatrices d’appropriation et d’élaboration collectives.
