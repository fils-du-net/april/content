---
site: lemondeinformatique.fr
title: "L'April partenaire du premier Master consacré au Libre"
author: Claire Heitz 
date: 2006-09-08
href: http://www.lemondeinformatique.fr/actualites/lire-l-april-partenaire-du-premier-master-consacre-au-libre-20437.html
tags:
- Le Logiciel Libre
- April
- Éducation
---

> L'April (l'Association pour la promotion et la recherche en informatique libre) s'implique dans le premier  Master en ingénierie du logiciel libre  (I2L) lancée par L'Université du littoral-Côte d'Opale (Calais) pour cette rentrée scolaire. Plusieurs membres de l'association ont contribué à élaborer le contenu du module d'enseignement « environnement libre » et vont y intervenir, dont Frédéric Couchet, délégué général et fondateur de l'April, Jérémie Zimmermann et Loïc Dayot, membres du conseil d'administration.
