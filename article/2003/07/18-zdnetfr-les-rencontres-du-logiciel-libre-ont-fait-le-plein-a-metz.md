---
site: zdnet.fr
title: "Les Rencontres du logiciel libre ont fait le plein à Metz"
author: Thierry Noisette
date: 2003-07-18
href: http://www.zdnet.fr/actualites/informatique/0,39040745,2137593,00.htm
tags:
- Le Logiciel Libre
- April
---

> La 4e édition des "Rencontres mondiales du logiciel libre", organisée par des passionnés et des professionnels, a permis de prendre le pouls d'un secteur de plus en plus influent en termes économiques. Sans perdre sa fibre politique et sociale.
> METZ - «Nous sommes dans une période prérévolutionnaire, où les puissances s'arc-boutent sur leurs privilèges, à la fin on va gagner, mais pour ça il faut se battre!», lance François Pellegrini, informaticien à l'Inria et militant du collectif Eurolinux. Il était l'un des nombreux organisateurs et intervenants des "Rencontres mondiales du logiciel libre" (RMLL), qui ont illustré, pour leur quatrième édition, cet optimisme grandissant.
