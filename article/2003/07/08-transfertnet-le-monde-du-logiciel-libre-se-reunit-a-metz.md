---
site: transfert.net
title: "Le monde du logiciel libre se réunit à Metz"
author: Anne Lindivat
date: 2003-07-08
href: http://www.transfert.net/Le-monde-du-logiciel-libre-se
tags:
- Le Logiciel Libre
- April
---

> La quatrième édition des Rencontres mondiales du logiciel libre (RMLL) se tiendra à Metz du 9 au 12 juillet 2003. Cette manifestation au succès croissant réunira grand public, experts et entreprises autour de conférences et d’ateliers thématiques destinés tant aux techniciens spécialistes qu’aux curieux néophytes. Aux côtés des associations du milieu logiciel libre, 800 sociétés sont inscrites.
