---
site: lemondeinformatique.fr
title: "4 000 personnes aux Rencontres mondiales du logiciel libre"
author: Bertrand Lemaire 
date: 2008-07-08
href: http://www.lemondeinformatique.fr/actualites/lire-4-000-personnes-aux-rencontres-mondiales-du-logiciel-libre-26530.html
tags:
- Le Logiciel Libre
- April
- RGI
---

> Du 1er au 5 juillet 2008 à Mont-de-Marsan (Landes), les Rencontres Mondiales du Logiciel Libre (RMLL) ont réuni plus de 4000 visiteurs badgés ou identifiés, soit le double de ce qui était prévu. « Le choix de Mont-de-Marsan avait laissé perplexe une partie de la Communauté mais le bilan est des plus positifs, notamment par ce record de fréquentation » se réjouit Jean-Christophe Elineau, président du comité d'organisation.
> [...] Et rien ne vaut une rencontre mondiale pour faire avancer des dossiers locaux. Henri Emmanuelli, président du Conseil Général des Landes, avait prévu de rester sur site environ une heure et demie mais y a finalement passé cinq heures, dont une longue conversation avec Richard Stallman. La ville de Mont-de-Marsan a annoncé sa bascule sous logiciels libres. Le préfet du département des Landes, de son côté, a promis que le RGI (Référentiel Général d'Interopérabilité) sortirait un jour...
