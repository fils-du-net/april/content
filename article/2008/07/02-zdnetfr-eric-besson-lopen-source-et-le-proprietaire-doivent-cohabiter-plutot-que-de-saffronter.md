---
site: zdnet.fr
title: "Éric Besson : « L'open source et le propriétaire doivent cohabiter plutôt que de s'affronter »"
author: Christophe Guillemin
date: 2008-07-02
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39382112,00.htm
tags:
- Le Logiciel Libre
- Logiciels privateurs
- RGI
---

> Technologie - Lors de leurs Assises, les éditeurs français ont expérimé leur irritation de voir les services publics adopter de plus en plus souvent les logiciels libres. Eric Besson a pour sa part considéré que ces deux mondes doivent coexister.
