---
site: journaldunet.com
title: "Logiciels libres et standards ouverts pour une administration électronique efficace et durable"
author: Benoît Sibaud
date: 2008-07-04
href: http://www.journaldunet.com/solutions/expert/29115/logiciels-libres-et-standards-ouverts-pour-une-administration-electronique-efficace-et-durable.shtml
tags:
- Administration
- April
- RGI
- Standards
---

> Logiciels libres et standards ouverts pour une administration électronique efficace et durable
