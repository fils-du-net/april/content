---
site: ecrans.fr
title: "Windows, le prix mais pas le choix"
author: Camille Gévaudan 
date: 2008-06-25
href: http://www.ecrans.fr/Windows-le-prix-mais-pas-le-choix,4480.html
tags:
- Logiciels privateurs
- Vente liée
---

> La vente liée est interdite, et Darty le sait très bien. Mais l’enseigne continuera d’imposer Windows à tout acheteur d’un ordinateur neuf, puisque la justice vient de l’y autoriser.
> [...]
> Jérémy Monnet, administrateur de l’April, se félicite de ce rappel à l’ordre et espère qu’elle amènera le secrétaire d’État à la Consommation à reconsidérer ses positions.
> Mais qui dit demi-victoire dit demi-défaite. Les magistrats ont bien reconnu que Darty pratiquait des « ventes subordonnées », rejetant l’argument du distributeur selon lequel il ne s’agit pas de vente liée lorsqu’on n’acquiert qu’un droit d’usage du logiciel et non sa propriété. Mais ils ont choisi d’excuser la pratique « dans l’intérêt des consommateurs », considérant que l’installation d’un système d’exploitation est un exercice trop difficile pour l’acheteur lambda : « La substitution d’un logiciel par un autre est une tâche particulièrement délicate (…) hors de portée du consommateur moyen. (…) ». Ils ont pour cela pris en compte un rapport de Darty, qui affirme que la désinstallation de logiciels peut compromettre la stabilité d’un ordinateur et que l’installation de Linux — effectuée pourtant par des experts — prend trois heures.
