---
site: pcinpact.com
title: "Interopérabilité : Microsoft supprime en douce des informations"
author: Vincent Hermann
date: 2008-06-24
href: http://www.pcinpact.com/actu/news/44379-microsoft-doj-interoperabilite-documentation.htm
tags:
- Interopérabilité
- RGI
---

> [...] La première concerne la fameuse documentation fournie par la firme sur le délicat sujet de l’interopérabilité. Le Comité a en effet remarqué que des informations avaient tout simplement disparu de la documentation. Après examen, il se trouve que ces informations n’auront en fait pas d’impact sur les éditeurs tiers, et le Comité précise qu’elles étaient relatives aux opérations s’exécutant en interne au sein de l’environnement Windows Server.
