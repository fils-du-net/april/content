---
site: toolinux.com
title: "L'Education Nationale renouvelle sa confiance à Mandriva"
author: La rédaction
date: 2008-05-11
href: http://www.toolinux.com/news/services/l_education_nationale_renouvelle_sa_confiance_a_mandriva_ar10528.html
tags:
- Le Logiciel Libre
- Administration
---

> En France, l’éditeur Mandriva et le Ministère de l’Éducation Nationale ont décidé, pour la quatrième année consécutive, de renouveler leur contrat de partenariat.
