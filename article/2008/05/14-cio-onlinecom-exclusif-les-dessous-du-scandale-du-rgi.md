---
site: "cio-online.com"
title: "Exclusif : les dessous du scandale du RGI"
author: Bertrand Lemaire
date: 2008-05-14
href: http://www.cio-online.com/actualites/lire-exclusif-les-dessous-du-scandale-du-rgi-1342.html
tags:
- RGI
- Standards
---

> La rédaction de CIO a reçu une copie de la note qui aurait été remise par la direction de Microsoft au gouvernement français à l'automne dernier et qui serait à l'origine du recul de la France face à l'éditeur américain.
> Le texte qui nous est parvenu est, pour nous, crédible, notamment à cause des voies empruntées, raison pour laquelle la Rédaction de CIO publie cet article. Mais il convient néanmoins de rester prudent, la manipulation semblant être une chose courante dans ce dossier... Le fait que ce texte sorte à quelques jours d'une grande opération de Microsoft dans l'Hexagone, l'Imagine Cup, est probablement un hasard...
