---
site: linformaticien.com
title: "Microsoft : nouvelle prise en charge de fichiers dans Office en 2009"
author: Emilien Ercolani  
date: 2008-05-22
href: http://www.linformaticien.com/Actualit%E9s/tabid/58/newsid496/4424/microsoft-nouvelle-prise-en-charge-de-fichiers-dans-office-en-2009/Default.aspx
tags:
- April
- RGI
- Standards
---

> Réaction de l’April
> Selon le délégué général de l’April, Frédéric Couchet, « le support natif d'ODF par Microsoft Office rend caduque la seule raison qui était finalement invoquée pour justifier la double présence d'ODF et OOXML », concernant le RGI (Référentiel Général d’Interopérabilité).
> En effet, le RGI prévoyait un format unique pour les documents révisables, et un autre pour les documents figés (PDF). « Microsoft ne pourra plus prétendre être désavantagé, à moins d'avouer que son support natif d'ODF ne fonctionnera pas ou que son annonce n'est que du vent ».
