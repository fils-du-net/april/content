---
site: vnunet.fr
title: "Comment Microsoft impose son format OpenXML à l'administration française"
author: Christophe Lagane
date: 2008-05-15
href: http://www.vnunet.fr/fr/news/2008/05/15/comment_microsoft_impose_son_format_openxml_a_l_administration_francaise
tags:
- Administration
- April
- RGI
- Standards
---

> [...] Rappelons d’abord que le RGI, publié en mars 2007, confirmait le format normalisé ISO ODF comme choix de prédilection des services informatiques de l’administration française. Or, bloqué en octobre 2007, le RGI est devenu prioritaire en avril 2008, depuis qu’il intègre le format concurrent Office OpenXML (OOXML) de Microsoft quelques jours après sa normalisation ISO.
