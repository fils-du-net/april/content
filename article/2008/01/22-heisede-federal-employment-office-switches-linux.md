---
site: heise online
title: "Federal Employment Office switches to Linux"
author: 
date: 2008-01-22
href: http://www.heise.de/english/newsticker/news/102218
tags:
- Le Logiciel Libre
- Administration
---

> Shortly after switching all of its 13,000 public internet information workstations from Windows NT to Linux, the Federal Employment Office (BA) issued an interim report. According to an announcement by Klaus Vitt, CIO for the BA, the switch to open source software will, 'allow the BA to react with flexibility to new technological developments.
