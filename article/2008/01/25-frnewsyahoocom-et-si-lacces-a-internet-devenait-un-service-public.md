---
site: yahoo
title: "Et si l'accès à Internet devenait un service public ?"
author: 
date: 2008-01-25
href: http://fr.news.yahoo.com/grp_test/20080125/ttc-et-si-l-acces-internet-devenait-un-s-549fc7d_1.html
tags:
- Internet
---

> Bibliothèques, Espaces Culture multimédia du ministère de la Culture, Points Cyb du ministère de la Jeunesse et des Sports, associations, Espaces du réseau Cyber-base de la Caisse des Dépôts… Les accès publics à Internet se multiplient, mais pas dans une grande homogénéité technique, juridique, ni de gestion.
