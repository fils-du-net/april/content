---
site: ZDNet
title: "La France est devenue « un pays phare pour le logiciel libre »"
author: Christophe Guillemin
date: 2008-01-18
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39377576,00.htm
tags:
- Le Logiciel Libre
---

> Business - Selon le cabinet Pierre Audoin Consultants, le marché français du logiciel libre génère le plus de revenus en Europe. Sur un an, le secteur a progressé de 66 %, tiré notamment par les programmes d’équipements des services publics.Avec un chiffre d'affaires de 730 millions d'euros, le marché français des logiciels libres et des services associés est le plus important sur le Vieux Continent, esime le cabinet Pierre Audoin Consultant (PAC).
