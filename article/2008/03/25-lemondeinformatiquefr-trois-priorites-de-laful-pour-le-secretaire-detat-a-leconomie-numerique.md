---
site: lemondeinformatique.fr
title: "Trois priorités de l'Aful pour le secrétaire d'Etat à l'Economie Numérique"
author:  Maryse Gros 
date: 2008-03-25
href: http://solutionspme.lemondeinformatique.fr/articles/lire-trois-priorites-de-l-aful-pour-le-secretaire-d-etat-a-l-economie-numerique-1833.html
tags:
- April
- RGI
- Standards
---

> A son tour, l'Aful a réagi, en fin de semaine, à la nomination d'Eric Besson au poste de secrétaire d'Etat à l'Economie Numérique le 18 mars dernier. L'association francophone des utilisateurs de Linux et des logiciels libres souligne d'abord la nécessité de favoriser l'interopérabilité et les standards « réellement » ouverts, « éléments indispensables à une concurrence libre et non faussée ».
