---
site: zdnet.fr
title: "Interopérabilité : l'ouverture de Microsoft ne convainc pas l'April"
author: La rédaction
date: 2008-02-22
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39378916,00.htm
tags:
- April
- RGI
- Standards
---

> [...] Pour l'April, ce n'est pas suffisant. « Cette nouvelle stratégie ne va pas au bout de la démarche », estime l'association, car le fait de réclamer tout de même une licence de brevet pour l'exploitation commerciale de ses protocoles est contraire au fonctionnement du libre. « Dans le monde du logiciel libre, il n'y a pas de distinction entre distribution non-commerciale et distribution commerciale », rappelle l'organisme « Les développements réalisés par des bénévoles peuvent être vendus par des entreprises, et inversement les développements menés par des entreprises peuvent ensuite être distribués de façon non-commerciale ».
