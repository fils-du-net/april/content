---
site: Numerama
title: "Axelle Lemaire défend le droit au chiffrement des données"
author: Guillaume Champeau
date: 2015-09-02
href: http://www.numerama.com/magazine/34081-axelle-lemaire-defend-le-droit-au-chiffrement-des-donnees.html
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> Alors que le procureur François Molins s'y oppose, la ministre du numérique défend le droit des utilisateurs de bénéficier d'un téléphone mobile sur lequel les données sont chiffrées et inaccessibles sans mot de passe. Mais la loi française jugée trop inefficace par le ministère de l'Intérieur impose que les autorités puissent y accéder sur demande.
