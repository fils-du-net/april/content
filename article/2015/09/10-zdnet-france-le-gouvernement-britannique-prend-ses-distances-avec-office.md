---
site: ZDNet France
title: "Le gouvernement britannique prend ses distances avec Office"
date: 2015-09-10
href: http://www.zdnet.fr/actualites/le-gouvernement-britannique-prend-ses-distances-avec-office-39824702.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Standards
---

> Le gouvernement britannique poursuit sa transition vers des formats libres pour ses documents initiée en 2014 et publie plusieurs guides détaillant les spécificités du format ODF, un format de document ouvert pour les applications bureautiques.
