---
site: Silicon.fr
title: "Bureautique: la France marque sa préférence pour ODF"
author: Reynald Fléchaux
date: 2015-09-23
href: http://www.silicon.fr/bureautique-france-marque-preference-format-libre-odf-127067.html
tags:
- Logiciels privateurs
- Administration
- April
- Promotion
- RGI
- Standards
---

> Le référentiel général d’interopérabilité (RGI), que le gouvernement s’apprête à publier, va donner la préférence au format bureautique ODF, issu d’OpenOffice. OpenXML de Microsoft est placé «en observation».
