---
site: Telos
title: "Larry Lessig, de la Silicon Valley à la Maison Blanche"
author: Monique Dagnaud
date: 2015-09-08
href: http://www.telos-eu.com/fr/vie-politique/larry-lessig-de-la-silicon-valley-a-la-maison-blan.html
tags:
- Internet
- Économie
- Institutions
- International
---

> Fred Turner, l’historien de l’Internet, exhorte les icônes de la Silicon Valley à cesser de proclamer que l’on peut changer le monde grâce aux outils numériques, et les supplie de s’engager en politique. Et bien, Larry Lessig, le défricheur du droit de l’Internet, s’est décidé à sauter le pas. Il se présente aux primaires du parti démocrate pour la présidentielle de 2016. Mais qui a tué le Larry d’avant?
