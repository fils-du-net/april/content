---
site: L'OBS
title: "Pourra-t-on troller le projet de loi numérique?"
author: Andréa Fradin
date: 2015-09-25
href: http://rue89.nouvelobs.com/2015/09/25/pourra-t-troller-projet-loi-numerique-261368
tags:
- Internet
- Institutions
---

> Construire l’Etoile noire de «Star Wars», expulser Justin Bieber ou dire la vérité sur les extraterrestres: quand les Etats donnent la parole aux citoyens, ces derniers ne se privent pas pour la prendre de manière absurde et souvent très drôle. La preuve avec ces demandes auxquelles la Maison Blanche, qui s’est engagée à répondre à toute pétition recueillant 100 000 signatures en un mois, a dû faire face.
