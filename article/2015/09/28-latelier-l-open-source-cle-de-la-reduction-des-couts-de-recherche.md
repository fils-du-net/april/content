---
site: L'Atelier
title: "L’open source, clé de la réduction des coûts de recherche"
author: Pauline Canteneur
date: 2015-09-28
href: http://www.atelier.net/trends/articles/open-source-cle-de-reduction-couts-de-recherche_437680
tags:
- Partage du savoir
- Matériel libre
- Sciences
---

> Disposer d’équipement scientifique open source pourrait permettre aux équipes de recherche de faire baisser leurs budgets. Projet proposé par des chercheurs de la Michigan Tech.
