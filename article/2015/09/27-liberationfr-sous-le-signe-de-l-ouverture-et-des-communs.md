---
site: Libération.fr
title: "Sous le signe  de l’ouverture et des communs"
author: Lionel Maurel
date: 2015-09-27
href: http://www.liberation.fr/debats/2015/09/27/sous-le-signe-de-l-ouverture-et-des-communs_1391920
tags:
- Internet
- Administration
- Institutions
- Open Data
---

> Véritable renversement de perspective, le projet d’Axelle Lemaire favorise un retour aux «promesses originelles» du numérique. Il donne à la société civile des moyens pour mieux évaluer et contrôler l’action des pouvoirs publics.
