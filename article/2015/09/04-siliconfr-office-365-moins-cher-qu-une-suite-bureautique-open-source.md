---
site: Silicon.fr
title: "Office 365 moins cher qu’une suite bureautique Open Source?"
author: Jacques Cheminat
date: 2015-09-04
href: http://www.silicon.fr/office-365-cher-quune-suite-bureautique-open-source-125599.html
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Interopérabilité
- RGI
---

> A l’occasion de la bascule d’une mairie italienne d’OpenOffice à Office 365, Microsoft s’est targué d’être 80% moins cher que son concurrent Open Source. Coût de déploiement, formation, problèmes d’interopérabilité, l’éditeur américain déroule ses arguments.
