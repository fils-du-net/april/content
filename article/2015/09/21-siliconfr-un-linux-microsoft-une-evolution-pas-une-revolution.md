---
site: Silicon.fr
title: "Un Linux Microsoft? Une évolution pas une révolution"
author: Jacques Cheminat
date: 2015-09-21
href: http://www.silicon.fr/distribution-linux-de-microsoft-evolution-revolution-126965.html
tags:
- Entreprise
- Innovation
---

> Microsoft aurait-il viré sa cuti en créant sa propre distribution Linux? C’est aller un peu vite en besogne, cette initiative est circonscrite au réseau (SDN) dans le cadre du projet OCP.
