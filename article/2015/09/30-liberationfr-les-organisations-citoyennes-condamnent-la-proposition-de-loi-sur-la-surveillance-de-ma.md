---
site: Libération.fr
title: "Les organisations citoyennes condamnent la proposition de loi sur la surveillance de masse"
author: Un collectif d'associations de défense des libertés numériques
date: 2015-09-30
href: http://www.liberation.fr/debats/2015/09/30/les-organisations-citoyennes-condamnent-la-proposition-de-loi-sur-la-surveillance-de-masse_1393325
tags:
- Internet
- Institutions
- Associations
- Sciences
- Vie privée
---

> Un collectif d'associations appelle les parlementaires français à rejeter la proposition de loi relative aux mesures de surveillance des communications électroniques internationales.
