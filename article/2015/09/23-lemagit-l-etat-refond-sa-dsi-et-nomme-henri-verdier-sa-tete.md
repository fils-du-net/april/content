---
site: LeMagIT
title: "L’état refond sa DSI et nomme Henri Verdier à sa tête"
author: Cyrille Chausson
date: 2015-09-23
href: http://www.lemagit.fr/actualites/4500254047/Letat-refond-sa-DSI-et-nomme-Henri-Verdier-a-sa-tete
tags:
- Administration
- RGI
- Open Data
---

> La DISIC fusionne officiellement avec Etalab et devient le DINSIC. Henri Verdier en assure la direction.
