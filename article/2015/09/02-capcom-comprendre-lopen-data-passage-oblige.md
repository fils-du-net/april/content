---
site: Cap'Com
title: "Comprendre l'open data, passage obligé!"
author: Anne Revol
date: 2015-09-02
href: http://www.cap-com.org/content/comprendre-lopen-data-passage-oblige
tags:
- Internet
- Administration
- Open Data
---

> Les collectivités locales de plus de 3 500 habitants et leurs EPCI vont devoir publier sur Internet certaines de leurs données. Introduite par la loi NOTRe votée cet été, cette obligation remet le sujet de l'open data sur le haut de la pile dans les collectivités. Et les communicants sont en première ligne pour expliquer et accompagner l'ouverture des données publiques. Évolution, expériences, cadre légal, processus, moyens: défrichons ce sujet complexe avec des acteurs de l'open data pour mieux en cerner les enjeux de communication.
