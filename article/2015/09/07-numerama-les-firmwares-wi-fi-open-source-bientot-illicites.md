---
site: Numerama
title: "Les firmwares Wi-Fi open-source bientôt illicites?"
author: Guillaume Champeau
date: 2015-09-07
href: http://www.numerama.com/magazine/34128-les-firmwares-wi-fi-open-source-bientot-illicites.html
tags:
- Internet
- Institutions
- Standards
- Europe
- International
---

> Le régulateur américain des communications propose de modifier la législation sur les appareils utilisant des radiofréquences, pour faciliter le processus de certification mais imposer que le logiciel qui pilote l'appareil soit validé par le constructeur autorisé, et qu'aucun autre ne puisse lui être substitué. Une menace pour l'open-source et l'open-hardware?
