---
site: Next INpact
title: "«La gratuité, c'est le vol» à réactions"
author: Marc Rees
date: 2015-09-11
href: http://www.nextinpact.com/news/96478-la-gratuite-cest-vol-a-reactions.htm
tags:
- Entreprise
- Institutions
- Droit d'auteur
- Europe
---

> Richard Malka, avocat en droit de la presse, qui défend notamment Charlie Hebdo, distribue gratuitement un livre intitulé «La gratuité c’est le vol, 2015: la fin du droit d’auteur?». Au-delà du joli paradoxe, l’ouvrage s’oppose à la réforme du secteur envisagée à Bruxelles. Un positionnement qui ne laisse pas insensible.
