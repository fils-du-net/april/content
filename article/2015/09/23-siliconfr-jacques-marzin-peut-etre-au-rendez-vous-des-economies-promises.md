---
site: Silicon.fr
title: "Jacques Marzin: «On peut être au rendez-vous des économies promises»"
author: Reynald Fléchaux
date: 2015-09-23
href: http://www.silicon.fr/jacques-marzin-dsi-etat-etre-rendez-vous-economies-promises-127138.html
tags:
- Logiciels privateurs
- Administration
- RGI
---

> Donc, en juin, en accord avec les secrétaires généraux des ministères, nous avons décidé de laisser les ministères sous Microsoft Outlook continuer à l’utiliser, tout en fédérant dans les six mois qui viennent l’ensemble de l’offre libre des ministères ayant choisi des voies alternatives diverses et variées. Car on ne peut pas utiliser le libre sans utiliser ce qui fait la force de ce système ; autrement dit, on ne peut pas opter pour une stratégie Open Source sans participer aux communautés et sans s’accorder sur des choix partagés.
