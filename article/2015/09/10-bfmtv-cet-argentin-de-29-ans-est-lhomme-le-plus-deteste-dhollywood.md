---
site: BFMtv
title: "Cet Argentin de 29 ans est l'homme le plus détesté d'Hollywood"
author: Olivier Laffargue
date: 2015-09-10
href: http://bfmbusiness.bfmtv.com/entreprise/cet-argentin-de-29-ans-est-l-homme-le-plus-deteste-d-hollywood-913509.html
tags:
- Entreprise
- Internet
- Économie
- Droit d'auteur
- Innovation
---

> Le créateur de PopCorn Time, le "Netflix" pirate utilisé par des millions de personnes à travers le monde, vient de révéler son identité. Il s'agit de Federico Abad, un Argentin de 29 ans. 
