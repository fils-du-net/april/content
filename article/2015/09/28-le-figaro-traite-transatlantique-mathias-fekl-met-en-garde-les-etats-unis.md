---
site: Le Figaro
title: "Traité transatlantique: Mathias Fekl met en garde les États-Unis"
author: Patrick Bèle
date: 2015-09-28
href: http://www.lefigaro.fr/conjoncture/2015/09/28/20002-20150928ARTFIG00001-tafta-mathias-fekl-met-en-garde-les-etats-unis.php
tags:
- Économie
- Institutions
- ACTA
---

> Le secrétaire d'État français au Commerce extérieur explique dans un entretien dans les colonnes de Sud-Ouest à paraître ce lundi, que les États-Unis doivent faire preuve de plus de réciprocité dans les négociations sur le traité de libre-échange UE-États-Unis.
