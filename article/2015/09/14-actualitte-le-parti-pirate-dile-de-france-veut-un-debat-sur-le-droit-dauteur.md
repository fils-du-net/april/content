---
site: ActuaLitté
title: "Le Parti Pirate d'Île de France veut un débat sur le droit d'auteur"
author: François Vermorel
date: 2015-09-14
href: https://www.actualitte.com/article/tribunes/la-gratuite-c-est-le-vol-pour-un-vrai-debat-sur-le-droit-d-auteur/60517
tags:
- Économie
- Droit d'auteur
---

> L'opération de communication organisée par le Syndicat National de l'Édition, #AuteursEnDanger, n'a pas manqué de faire réagir. Pas forcément pour les bonnes raisons: le discours de l'avocat de Charlie Hebdo, Richard Malka, a été vivement critiqué. La vision biaisée du droit d'auteur qu'il présente, et celle de la réforme européenne du droit semblent plus que partiales. François Vermorel, auteur, éditeur et animateur de la section Île de France du Parti Pirate a souhaité lui répondre par de réels arguments, dans une tribune que nous reproduisons dans son intégralité ci-dessous.
