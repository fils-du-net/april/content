---
site: Le Monde.fr
title: "Amputé, Nicolas Huchet a fabriqué sa propre main bionique"
author: Claire Legros
date: 2015-09-15
href: http://www.lemonde.fr/festival/article/2015/09/15/nicolas-huchet-bionico-man-solidaire_4757752_4415198.html
tags:
- Économie
- Partage du savoir
- Innovation
---

> Ce Rennais de 35 ans veut généraliser l’accès aux prothèses innovantes grâce aux logiciels libres et à l’impression 3D.
