---
site: France Info
title: "Ce que prévoit la loi - collaborative - sur le numérique"
author: Jérôme Colombain
date: 2015-09-28
href: http://www.franceinfo.fr/emission/nouveau-monde/2015-2016/ce-que-prevoit-la-loi-collaborative-sur-le-numerique-28-09-2015-06-50
tags:
- Internet
- Institutions
- Neutralité du Net
- Vie privée
---

> Récupérer ses données sur Facebook, se faire oublier sur Google… Voilà ce que devrait permettre la future loi sur le numérique. En attendant, le texte fait l'objet une grande consultation sur Internet.
