---
site: Libération.fr
title: "«Droit à l’oubli»: vers une obligation mondiale?"
author: Amaelle Guiton
date: 2015-09-21
href: http://www.liberation.fr/economie/2015/09/21/droit-a-l-oubli-vers-une-obligation-mondiale_1387600
tags:
- Entreprise
- Internet
- Institutions
---

> La Commission informatique et liberté a rejeté le recours de Google et lui redemande d'étendre à tout son moteur de recherche le droit au déréférencement reconnu en Europe.
