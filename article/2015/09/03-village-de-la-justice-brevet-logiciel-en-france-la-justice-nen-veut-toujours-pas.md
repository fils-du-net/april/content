---
site: Village de la Justice
title: "Brevet logiciel: en France, la justice n'en veut (toujours) pas."
author: Bernard Lamon
date: 2015-09-03
href: http://www.village-justice.com/articles/Brevet-logiciel-France-justice,20334.html
tags:
- Entreprise
- Internet
- Institutions
- Brevets logiciels
- Europe
---

> Le brevet-logiciel ne passe pas le test judiciaire... en France, dit le TGI de Paris. Un jugement du tribunal de grande instance de Paris du 18 juin 2015 permet de rappeler que le juge français n’aime pas du tout le brevet portant sur un logiciel.
