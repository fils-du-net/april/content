---
site: Le Monde.fr
title: "La bataille contre la surveillance s’intensifie devant le Conseil d’Etat"
author: Martin Untersinger
date: 2015-09-03
href: http://www.lemonde.fr/pixels/article/2015/09/03/la-bataille-contre-la-surveillance-s-intensifie-devant-le-conseil-d-etat_4745114_4408996.html
tags:
- Internet
- Institutions
- Associations
- Europe
- Vie privée
---

> C’est un tir de barrage contre la surveillance des communications en France. Des associations – La Quadrature du Net, French Data Network et la Fédération FDN – ont déposé trois nouveaux recours devant le Conseil d’Etat pour annuler certaines dispositions utilisées par les services français de police et de renseignement.
