---
site: Next INpact
title: "Henri Verdier à la tête de la nouvelle Direction interministérielle du numérique (DINSIC)"
author: Xavier Berne
date: 2015-09-24
href: http://www.nextinpact.com/news/96633-henri-verdier-prend-tete-nouvelle-direction-interministerielle-numerique-dinsic.htm
tags:
- Administration
- RGI
- Open Data
---

> Jusqu’ici directeur de la mission Etalab et Administrateur général des données, Henri Verdier a été nommé hier à la tête de la nouvelle Direction interministérielle du numérique (DINSIC). Il remplace ainsi Jacques Marzin, habituellement qualifié de «DSI de l’État».
