---
site: Le Monde.fr
title: "Favorisons la libre diffusion de la culture et des savoirs"
author: collectif
date: 2015-09-10
href: http://www.lemonde.fr/idees/article/2015/09/10/favorisons-la-libre-diffusion-de-la-culture-et-des-savoirs_4751847_3232.html
tags:
- Partage du savoir
- Institutions
- Droit d'auteur
- Contenus libres
---

> Le projet de loi numérique donne enfin un fondement juridique aux biens communs de la connaissance. Mais il est urgent d’endiguer des pratiques qui limitent l’accès du public à des oeuvres qui se trouvent pourtant dans le domaine public, au profit d’intérêts commerciaux, alerte un collectif où figurent Pierre Lescure, Bruno Latour, Denis Podalydès et Benoît Thieulin.
