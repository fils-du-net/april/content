---
site: La Vie des idées
title: "Communs de la connaissance et enclosures"
author: Lionel Maurel
date: 2015-09-29
href: http://www.laviedesidees.fr/Defense-de-l-enclosure.html
tags:
- Internet
- Économie
- Partage du savoir
- Droit d'auteur
- Neutralité du Net
---

> En réponse à l’article d’Allan Greer paru sur la Vie des idées, contestant l’usage, dans le domaine informationnel, du concept d’origine anglaise d’«enclosure», Lionel Maurel défend l’application de cette notion au champ des biens communs numériques.
