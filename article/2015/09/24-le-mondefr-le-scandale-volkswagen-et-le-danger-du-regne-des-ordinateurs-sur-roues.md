---
site: Le Monde.fr
title: "Le scandale Volkswagen et le danger du règne des ordinateurs sur roues"
author: Martin Untersinger
date: 2015-09-24
href: http://www.lemonde.fr/pixels/article/2015/09/24/volkswagen-les-defis-des-pouvoirs-publics-a-l-ere-des-algorithmes_4770138_4408996.html
tags:
- Entreprise
- Logiciels privateurs
- Sensibilisation
- International
---

> Le scandale qui touche Volkswagen rappelle la place essentielle prise par les logiciels dans l’automobile et la difficulté à en contrôler la sûreté.
