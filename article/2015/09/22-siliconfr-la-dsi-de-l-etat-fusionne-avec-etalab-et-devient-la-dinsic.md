---
site: Silicon.fr
title: "La DSI de l’Etat fusionne avec Etalab et devient la Dinsic"
author: Reynald Fléchaux
date: 2015-09-22
href: http://www.silicon.fr/dsi-etat-fusionne-etalab-devient-dinsic-127020.html
tags:
- Administration
- RGI
- Open Data
---

> Un décret officialise la fusion de la Disic, la DSI de l’Etat, avec Etalab et le pôle innovation du SGMAP. Cette direction du numérique constituée n’attend plus que la nomination d’Henri Verdier à sa tête.
