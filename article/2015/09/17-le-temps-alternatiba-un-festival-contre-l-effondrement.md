---
site: Le Temps
title: "Alternatiba, un festival contre l’effondrement"
author: Nic Ulmi
date: 2015-09-17
href: http://www.letemps.ch/Page/Uuid/9d12d91c-5ca3-11e5-af59-94bd5b6861b3/Alternatiba_un_festival_contre_leffondrement
tags:
- Économie
- Partage du savoir
- Innovation
- International
---

> Un «village de transition vers le monde de demain» surgit à Genève, comme dans une centaine d’autres localités depuis 2013. 20 000 personnes y sont attendues
