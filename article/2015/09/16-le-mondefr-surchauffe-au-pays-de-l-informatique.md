---
site: Le Monde.fr
title: "Surchauffe au pays de l’informatique"
author: Macha Séry
date: 2015-09-16
href: http://www.lemonde.fr/televisions-radio/article/2015/09/16/tv-quo-papier-unique-du-date-mercredi-16-sous-les-feux-du-business-informatique-suis-pas-convaincue-de-ce-titre_4758811_1655027.html
tags:
- Entreprise
- Innovation
---

> Autre découverte passionnante à la lumière des débats actuels: le flou nimbant, à l’époque, la notion de propriété intellectuelle. «Dans le secteur des nouvelles technologies, celui qui avait eu une idée était rarement la même personne qui franchissait la ligne d’arrivée avec le produit, rappelle Christopher C. Rogers. Vu sous l’angle positif, on peut dire qu’il y avait un désir de l’ensemble de l’industrie d’innover et de partager des idées en open source. Dans la réalité, la ligne de partage entre l’innovation et le piratage était mince.
