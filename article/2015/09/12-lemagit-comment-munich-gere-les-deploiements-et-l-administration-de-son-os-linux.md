---
site: LeMagIT
title: "Comment Munich gère les déploiements et l’administration de son OS Linux"
author: Cyrille Chausson
date: 2015-09-12
href: http://www.lemagit.fr/etude/Comment-Munich-gere-les-deploiements-et-ladministration-de-son-OS-Linux
tags:
- Administration
- Promotion
---

> Lors de DebConf 2015, qui s’est tenue en août dernier en Allemagne, la ville de Munich a donné quelques détails sur la façon dont elle gère les déploiements de Linux sur les postes de travail de la ville.
