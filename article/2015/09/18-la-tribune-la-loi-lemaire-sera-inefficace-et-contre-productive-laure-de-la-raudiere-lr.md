---
site: La Tribune
title: "\"La loi Lemaire sera inefficace et contre-productive\" Laure de La Raudière, LR"
author: Sylvain Rolland
date: 2015-09-18
href: http://www.latribune.fr/technos-medias/la-loi-numerique-lemaire-sera-inefficace-et-contre-productive-laure-de-la-raudiere-lr-506564.html
tags:
- Entreprise
- Internet
- Institutions
- Innovation
- Europe
- Open Data
---

> La députée Les Républicains estime que l’échelon national est inadapté et critique la passivité du couple franco-allemand sur les dossiers numériques.
