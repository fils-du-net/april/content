---
site: Mediapart
title: "Informatique et le scandale Volkswagen"
author: Chatterley Epaminondas
date: 2015-09-26
href: http://blogs.mediapart.fr/blog/chatterley-epaminondas/260915/informatique-et-le-scandale-volkswagen
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Droit d'auteur
---

> Le scandale Volkswagen a déclenché une réaction en chaîne aux USA et en Europe.
