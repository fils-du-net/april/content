---
site: L'OBS
title: "Comment les fabricants de tablettes s’incrustent dans les classes"
author: Sonia Barge et Robin Prudent
date: 2015-09-17
href: http://rue89.nouvelobs.com/2015/09/17/comment-les-fabricants-tablettes-sincrustent-les-classes-261232
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Éducation
- Sciences
---

> Pour leur rentrée, les élèves de sixième du collège privé Charlemagne, à Lesquin (Nord), ont eu une belle surprise. Sur leurs tables d’écoliers, 200 iPad flambant neufs. Plus excitant qu’une copie double, et plus léger qu’une pile de livres.
