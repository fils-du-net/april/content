---
site: Next INpact
title: "L'accès à Internet bientôt autorisé lors des examens universitaires?"
author: Xavier Berne
date: 2015-09-10
href: http://www.nextinpact.com/news/96368-l-acces-a-internet-bientot-autorise-lors-examens-universitaires.htm
tags:
- Internet
- Partage du savoir
- Institutions
- Éducation
- Contenus libres
---

> Les étudiants français auront-ils un jour le droit de se rendre sur Internet lors de leurs partiels? Selon un rapport remis le 8 septembre à François Hollande, ce mouvement est «inéluctable». De nombreuses autres préconisations concernant le numérique ont d’ailleurs été faites à l’exécutif, par exemple s’agissant de l’usage de licences libres pour les ressources pédagogiques ou de l’introduction d’une licence «Humanités numériques».
