---
site: LeMagIT
title: "L’affaire Volkswagen? Un plaidoyer pour le logiciel libre"
author: Valéry Marchive
date: 2015-09-24
href: http://www.lemagit.fr/tribune/Laffaire-Volkswagen-Un-plaidoyer-pour-le-logiciel-libre
tags:
- Entreprise
- Logiciels privateurs
- Sensibilisation
---

> Au-delà des considérations légales voire éthiques, c’est la question de l’accès au code source qui est posée, un accès de plus en plus essentiel pour les libertés alors que les boîtes noires aux algorithmes obscurs se multiplient.
