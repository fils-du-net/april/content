---
site: Libération.fr
title: "Le numérique au pot commun"
author: Amaelle Guiton
date: 2015-09-27
href: http://www.liberation.fr/economie/2015/09/27/le-numerique-au-pot-commun_1392077
tags:
- Internet
- Partage du savoir
- Institutions
- Droit d'auteur
- Sciences
- Vie privée
---

> Comment encourager la diffusion des savoirs et de la culture? La question fait débat alors que le projet de loi sur «la République numérique» est soumis à consultation.
