---
site: atlantico
title: "Loi sur le numérique : une bonne première mouture sur les données personnelles… Mais qui laisse un goût amer sur le financement des entreprises "
author: Christophe Benavent
date: 2015-09-28
href: http://www.atlantico.fr/decryptage/loi-numerique-bonne-premiere-mouture-donnees-personnelles-mais-qui-laisse-gout-amer-financement-entreprises-christophe-benavent-2354397.html
tags:
- Internet
- Institutions
---

> Axelle Lemaire et Manuel Valls ont mis en ligne samedi 26 septembre leur projet de loi pour une "République numérique". Si les dispositions sur la gestion des données personnelles paraissent pour le moment pertinentes, la partie sur l'économie a été écartée par rapport aux annonces faites en février.
