---
site: Developpez.com
title: "Microsoft Office vs LibreOffice"
author: Michael Guilloux
date: 2015-09-16
href: http://www.developpez.com/actu/89924/Microsoft-Office-vs-LibreOffice-le-ministere-italien-de-la-defense-revise-son-choix-et-abandonne-la-suite-proprietaire-pour-la-suite-libre
tags:
- Logiciels privateurs
- Administration
- International
---

> Le libre et l’open source sont plus que jamais une menace pour les solutions propriétaires. Si au départ, ils semblaient plus être des mouvements de protestation contre les solutions propriétaires, ils ont progressivement embarqué une forte communauté qui travaille pour donner aux utilisateurs des arguments plus ou moins convaincants pour adopter les logiciels libres et open source.
