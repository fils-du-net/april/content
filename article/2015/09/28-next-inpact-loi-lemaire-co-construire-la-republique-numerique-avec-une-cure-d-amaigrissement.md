---
site: Next INpact
title: "Loi Lemaire: co-construire la République numérique avec une cure d’amaigrissement"
author: Marc Rees
date: 2015-09-28
href: http://www.nextinpact.com/news/96601-loi-lemaire-co-construire-republique-numerique-avec-cure-d-amaigrissement.htm
tags:
- Internet
- Institutions
---

> Co-construire la loi. Voilà le chantier qu’a initié ce week-end Axelle Lemaire en présentation de son projet de loi aux côtés de Manuel Valls. Désormais, le texte sur la «République numérique», après deux ans de retard, est en effet ouvert à contribution.
