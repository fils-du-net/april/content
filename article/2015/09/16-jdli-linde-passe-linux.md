---
site: JDLI
title: "L'Inde passe à Linux"
author: Nicolas Boudier-Ducloy
date: 2015-09-16
href: http://www.jdli.com/l-inde-passe-a-linux-art-1705-1.html
tags:
- Administration
- Institutions
- Éducation
- International
---

> BOSS: tel est le nom du système d'exploitation qui devrait remplacer tous les autres dans les administrations d'Inde.
