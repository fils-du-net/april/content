---
site: ZDNet
title: "LibreOffice entre au ministère de la Défense, mais en Italie"
date: 2015-09-17
href: http://www.zdnet.fr/actualites/libreoffice-entre-au-ministere-de-la-defense-mais-en-italie-39825032.htm
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Interopérabilité
- April
- Marchés publics
- International
---

> Une autre administration de premier plan fait un pas vers le logiciel libre : le ministère de la Défense italien. Ainsi, ce sont 150.000 postes équipés de Microsoft Office qui basculeront sur LibreOffice. En France, c'est "Open Bar" Microsoft.
