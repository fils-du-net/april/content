---
site: Silicon.fr
title: "Suite bureautique: la Défense italienne se choisit LibreOffice"
author: Jacques Cheminat
date: 2015-09-16
href: http://www.silicon.fr/ministere-italien-de-defense-se-convertit-a-libreoffice-odf-126529.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Interopérabilité
- International
---

> En Italie, le ministère de la Défense bascule sur la suite Open Source LibreOffice au détriment de Microsoft Office. Et le format ODF.
