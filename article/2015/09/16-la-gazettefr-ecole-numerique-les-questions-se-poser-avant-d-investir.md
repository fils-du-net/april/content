---
site: La gazette.fr
title: "Ecole numérique: les questions à se poser avant d’investir"
author: Sophie Maréchal
date: 2015-09-16
href: http://www.lagazettedescommunes.com/394626/ecole-numerique-les-questions-a-se-poser-avant-dinvestir
tags:
- Internet
- Administration
- Éducation
- Innovation
- Sciences
---

> Les différentes vagues d’équipement numérique des écoles permettent de dégager des principes afin de bien choisir le matériel et les logiciels, en lien avec les académies. 5 questions à se poser, et témoignages de communes.
