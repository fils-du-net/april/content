---
site: L'OBS
title: "République numérique: ce projet est une «loi miettes»"
author: Stéphane Bortzmeyer
date: 2015-09-28
href: http://rue89.nouvelobs.com/2015/09/28/republique-numerique-projet-est-loi-miettes-261410
tags:
- Institutions
- Droit d'auteur
---

> La consultation gouvernementale sur le «Projet de loi pour une République numérique» est désormais en ligne. Quelques observations personnelles.
