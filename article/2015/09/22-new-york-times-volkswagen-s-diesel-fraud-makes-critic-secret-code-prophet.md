---
site: The New York Times
title: "Volkswagen’s Diesel Fraud Makes Critic of Secret Code a Prophet"
author: Jim Dwyer
date: 2015-09-22
href: http://www.nytimes.com/2015/09/23/nyregion/volkswagens-diesel-fraud-makes-critic-of-secret-code-a-prophet.html
tags:
- Entreprise
- Logiciels privateurs
- Sensibilisation
- Droit d'auteur
---

> A Columbia University law professor stood in a hotel lobby one morning and noticed a sign apologizing for an elevator that was out of order. It had dropped unexpectedly three stories a few days earlier. The professor, Eben Moglen, tried to imagine what the world would be like if elevators were not built so that people could inspect them.
