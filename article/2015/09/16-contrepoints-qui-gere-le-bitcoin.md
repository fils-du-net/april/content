---
site: Contrepoints
title: "Qui gère le bitcoin?"
author: Gérard Dréan
date: 2015-09-16
href: http://www.contrepoints.org/2015/09/16/221958-qui-gere-le-bitcoin
tags:
- Internet
- Économie
- Innovation
---

> Or tout le monde peut définir et modifier le code, et chaque utilisateur décide par lui-même quel code il utilise sur ses propres matériels. En effet, tous les logiciels du système Bitcoin sont des logiciels libres, c’est-à-dire gratuits et librement utilisables, et surtout dont le code source est lui-même public et où chacun a droit de l’examiner, de le modifier et de distribuer ces versions modifiées.
