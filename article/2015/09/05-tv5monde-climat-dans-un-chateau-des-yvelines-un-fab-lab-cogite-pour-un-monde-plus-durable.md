---
site: TV5MONDE
title: "Climat: dans un château des Yvelines, un \"Fab Lab\" cogite pour un monde plus durable"
author: Nathalie Alonso
date: 2015-09-05
href: http://information.tv5monde.com/en-continu/climat-dans-un-chateau-des-yvelines-un-fab-lab-cogite-pour-un-monde-plus-durable-52010
tags:
- Partage du savoir
- Innovation
- Sciences
---

> Inventer des objets "sexy comme Apple mais ouverts comme Wikipedia"? Tel est le pari d'une centaine d'innovateurs, experts et bénévoles du monde entier lâchés pendant cinq semaines dans un château en rase campagne près de Paris pour finaliser douze projets écologiques et accessibles à tous.
