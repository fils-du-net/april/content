---
site: "usine-digitale.fr"
title: "L’affaire Volkswagen relance le débat sur l’open source dans l’automobile"
author: Nathalie Steiwer
date: 2015-09-23
href: http://www.usine-digitale.fr/article/l-affaire-volkswagen-relance-le-debat-sur-l-open-source-dans-l-automobile.N352321
tags:
- Entreprise
- Logiciels privateurs
- Associations
---

> Avec le “scandale” Volkswagen, s’ouvre la question de l’accès aux codes des logiciels embarqués. Le constructeur allemand a été pris la main dans le sac pour avoir truqué les résultats aux tests d’émissions en Californie grâce à un logiciel tenant compte des mouvements du volant.
