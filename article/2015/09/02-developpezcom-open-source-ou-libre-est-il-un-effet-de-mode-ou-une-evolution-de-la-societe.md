---
site: Developpez.com
title: "Open Source ou Libre: est-il un effet de mode ou une évolution de la société?"
author: zoom61
date: 2015-09-02
href: http://open-source.developpez.com/actu/89410/Open-Source-ou-Libre-est-il-un-effet-de-mode-ou-une-evolution-de-la-societe/
tags:
- Partage du savoir
- Innovation
- Promotion
- Informatique en nuage
---

> En prenant comme repère l'année écoulée, vous pouvez constater que la fréquence des projets et des initiatives Open Source ou libres a augmenté significativement en mise à jour, mais aussi en nouveautés. Vous pouvez le voir à travers les sujets suivants : le Vatican, Microsoft, Netflix, la Commission européenne, Document Freedom Day, Offre Libre, Swift, etc.
