---
site: Rue89
title: "Au Ceaac, on puise des idées dans l'open source"
author: Catherine Merckling
date: 2015-09-19
href: http://www.rue89strasbourg.com/index.php/2015/09/19/blogs/au-ceaac-on-puise-des-idees-dans-lopen-source
tags:
- Économie
- Partage du savoir
- Innovation
---

> Pour tous ceux qui se demandent à quoi sert l’art contemporain, Open Source interpelle avec sept positions critiques sur le monde actuel. Que leurs auteurs jouent sur l’ironie, la contestation ou la proposition de solutions concrètes, les œuvres apportent chacune leur pierre à l’édifice de la pensée d’un possible futur.
