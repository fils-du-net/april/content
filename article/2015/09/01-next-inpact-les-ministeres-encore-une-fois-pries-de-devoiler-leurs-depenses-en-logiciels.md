---
site: Next INpact
title: "Les ministères (encore une fois) priés de dévoiler leurs dépenses en logiciels"
author: Xavier Berne
date: 2015-09-01
href: http://www.nextinpact.com/news/96338-les-ministeres-encore-fois-pries-devoiler-leurs-depenses-en-logiciels.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
---

> Isabelle Attard ne lâche pas le morceau. La députée (ex-EELV) vient de demander pour la troisième fois aux différents ministères de dévoiler leurs dépenses en logiciels, alors que l’exécutif s'était montré jusqu'ici relativement discret sur ce dossier...
