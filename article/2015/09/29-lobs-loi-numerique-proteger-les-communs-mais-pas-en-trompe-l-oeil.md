---
site: L'OBS
title: "Loi numérique: protéger les communs, mais pas en trompe-l’oeil"
author: Lionel Maurel
date: 2015-09-29
href: http://rue89.nouvelobs.com/rue89-culture/2015/09/29/loi-numerique-proteger-les-communs-trompe-loeil-261424
tags:
- Internet
- Partage du savoir
- Institutions
- Droit d'auteur
---

> La semaine dernière, on a appris que la justice américaine, au terme d’une longue procédure, avait finalement donné tort à la société Warner qui revendiquait un droit d’auteur sur la chanson «Happy Birthday». Son titre de propriété était en réalité sans valeur, alors qu’il fut utilisé depuis les années 80 pour empocher chaque année plusieurs millions de dollars en royalties et entraver la libre réutilisation de cet air datant de la fin du XIXe siècle!
