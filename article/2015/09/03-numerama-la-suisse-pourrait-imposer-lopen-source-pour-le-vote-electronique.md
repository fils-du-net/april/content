---
site: Numerama
title: "La Suisse pourrait imposer l'open-source pour le vote électronique"
author: Guillaume Champeau
date: 2015-09-03
href: http://www.numerama.com/magazine/34099-la-suisse-pourrait-imposer-l-open-source-pour-le-vote-electronique.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Institutions
- Vote électronique
- International
---

> Le Conseil fédéral suisse a rejeté jeudi l'idée d'obliger les cantons à n'avoir recours qu'à des solutions publiques pour organiser des votes électroniques, mais il a prévenu qu'une loi pourrait être proposée pour obliger les prestataires privés à publier leur code source.
