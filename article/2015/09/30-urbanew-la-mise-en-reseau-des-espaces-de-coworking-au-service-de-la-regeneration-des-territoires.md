---
site: urbanew
title: "La mise en réseau des espaces de coworking au service de la régénération des territoires"
author: Raphaël Besson
date: 2015-09-30
href: https://www.urbanews.fr/2015/09/30/49648-la-mise-en-reseau-des-espaces-de-coworking-au-service-de-la-regeneration-des-territoires
tags:
- Entreprise
- Administration
---

> Depuis l’ouverture de Citizen Space en 2006 à San Francisco, le nombre d’espaces de coworking double chaque année. Et c’est en Europe, selon une étude de Deskwanted, que l’on compte le plus de lieux de travail partagés au monde, avec en 2013, 1 160 lieux sur 2 500 au total. Le phénomènene des coworking spaces a émergé au cœur des mouvements dits du «libre» (open innovation, hacking, open source, open data, etc.) et de la culture urbaine et numérique (Douheihi, 2011; Anderson, 2012). Il est aussi étroitement lié à l’augmentation continue du nombre d’indépendants.
