---
site: vousnousils
title: "Enseignement supérieur: Open source et Creative Commons préconisés pour les ressources pédagogiques"
author: Elsa Doladille
date: 2015-09-14
href: http://www.vousnousils.fr/2015/09/14/enseignement-superieur-open-source-et-creative-commons-preconises-pour-les-ressources-pedagogiques-575072
tags:
- Internet
- Institutions
- Éducation
- Contenus libres
---

> Le rapport StraNES, remis la semaine dernière à François Hollande, encourage l'usage de l'Open source et des Creative Commons pour les contenus pédagogiques produits dans l'enseignement supérieur.
