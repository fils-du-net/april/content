---
site: Next INpact
title: "Données perso: l'avocat général de la CJUE juge «invalide» le Safe Harbor avec les USA"
author: Xavier Berne
date: 2015-09-23
href: http://www.nextinpact.com/news/96618-donnees-perso-l-avocat-general-cjue-juge-invalide-safe-harbor-avec-usa.htm
tags:
- Internet
- Logiciels privateurs
- Institutions
- Europe
- International
- Vie privée
---

> Coup de tonnerre! L’avocat général de la Cour de justice de l’Union européenne (CJUE) estime que le «Safe Harbor» négocié à la fin des années 90 entre la Commission européenne et les États-Unis est aujourd’hui «invalide», du fait notamment des révélations d’Edward Snowden. Son avis ne lie cependant pas les juges, dont la décision n'interviendra probablement pas avant plusieurs semaines.
