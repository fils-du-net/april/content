---
site: Numerama
title: "PJL numérique: 75 personnalités pour la consécration des biens communs"
author: Julien L.
date: 2015-09-11
href: http://www.numerama.com/magazine/34175-pjl-numerique-75-personnalites-pour-la-consecration-des-biens-communs.html
tags:
- Partage du savoir
- Institutions
- Contenus libres
---

> Le journal Le Monde publie une tribune de 75 personnalités issues de la science, du numérique et de la culture soutenant la libre diffusion de la culture et des savoirs. Ils plaident pour une définition positive du domaine public dans le projet de loi sur le numérique et souhaitent sécuriser les nouvelles pratiques en matière de recherche.
