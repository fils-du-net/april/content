---
site: Basta !
title: "Plus de 40 000 personnes à la découverte du village des alternatives à Paris"
author: Lionel Maurel
date: 2015-09-29
href: http://www.bastamag.net/Plus-de-40-000-personnes-a-la-decouverte-du-village-des-alternatives-a-Paris
tags:
- Économie
- Partage du savoir
- Associations
- Innovation
- Promotion
---

> Les internautes fatigués d’enrichir les propriétaires de Microsoft ou d’Apple se familiarisent avec les logiciels libres. Ceux qui ne veulent plus sponsoriser l’électricité d’origine nucléaire et fossile se renseignent auprès de la coopérative Enercoop, admirent le concentrateur solaire mis au point par l’association Open source écologie, qui produit directement de la chaleur, ou questionnent «Approche paille» qui promeut la paille comme isolant. Les élus qui souhaitent repenser la gestion des déchets de leurs collectivités jettent un œil à «Zero Waste» (zéro déchet).
