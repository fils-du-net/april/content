---
site: Developpez.com
title: "Quels sont les modèles économiques open source qui fonctionnent le mieux?"
author: Stéphane le calme
date: 2015-09-29
href: http://www.developpez.com/actu/90549/Quels-sont-les-modeles-economiques-open-source-qui-fonctionnent-le-mieux-Un-blogueur-en-propose-quelques-uns
tags:
- Entreprise
- Économie
---

> Si la désignation open source s’applique aux logiciels dont la licence respecte les critères tels que définis par l’Open Source Initiative (par exemple la possibilité de libre redistribution, d’accès au code source voire même de création de travaux dérivés), nombreux sont ceux pour qui open source rime avec gratuité. Il n’en est rien. En réalité, il est possible de développer un modèle économique autour de l’open source.
