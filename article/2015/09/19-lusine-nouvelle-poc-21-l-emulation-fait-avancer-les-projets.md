---
site: L'usine Nouvelle
title: "POC 21, l’émulation fait avancer les projets"
author: Olivier Cognasse
date: 2015-09-19
href: http://www.usinenouvelle.com/article/poc-21-l-emulation-fait-avancer-les-projets.N351289
tags:
- Partage du savoir
- Associations
- Innovation
---

> Après cinq semaines de résidence au château de Millemont, POC 21 a rendu son verdict avec 20 projets aboutis, mais encore évolutifs.
