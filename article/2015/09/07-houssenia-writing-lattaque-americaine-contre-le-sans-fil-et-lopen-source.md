---
site: Houssenia Writing
title: "L'attaque américaine contre le sans-fil et l'Open Source"
author: Houssen Moshinaly
date: 2015-09-07
href: http://actualite.housseniawriting.com/technologie/2015/09/07/lattaque-americaine-contre-le-sans-fil-et-lopen-source/8097
tags:
- Institutions
- Innovation
- International
---

> La FCC veut moderniser ses règles sur les équipements à radio fréquence, mais ces règles impliquent un verrouillage des routeurs Wifi et d’autres appareils qui émettent des fréquences radios. Les critiques extrêmes estiment que dans un futur proche, on ne pourra même plus installer des systèmes tels que Linux sur des Laptops.
