---
site: Next INpact
title: "Loi Création: adoption des amendements sur la copie privée"
author: Marc Rees
date: 2015-09-15
href: http://www.nextinpact.com/news/96508-loi-creation-analyse-amendements-sur-copie-privee.htm
tags:
- Entreprise
- Économie
- Institutions
- Droit d'auteur
- Open Data
---

> Le projet de loi Création sera débattu en séance à à l'Assemblée nationale à partir du 28 septembre. Plusieurs amendements ont été déposés en commission pour revoir le régime de la redevance copie privée. Tour d'horizon de ces sept premières rustines, en attendant celles notamment du rapporteur Patrick Bloche.
