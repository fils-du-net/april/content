---
site: RTL.fr
title: "Le projet de loi numérique sera mis en ligne le 26 septembre"
author: Benjamin Hue
date: 2015-09-21
href: http://www.rtl.fr/culture/web-high-tech/loi-sur-le-numerique-ce-que-prevoit-le-texte-mis-en-consultation-publique-7779758691
tags:
- Internet
- Institutions
- Open Data
---

> Après trois années de reports, les contours du projet de loi sur le numérique se précisent. Le texte devait être mis en ligne lundi 21 septembre pour une discussion publique. Après arbitrage de Matignon, il le sera finalement samedi 26 septembre, précise-t-on au cabinet de la Secrétaire d'État. "La consultation, d'une durée de trois semaines, permettra à chacun de contribuer au texte législatif pour l'enrichir et le perfectionner", peut-on lire sur le site du ministère de l'Économie.
