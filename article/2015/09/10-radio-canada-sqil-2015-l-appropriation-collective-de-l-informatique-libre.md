---
site: "Radio-Canada"
title: "SQIL 2015: l’appropriation collective de l’informatique libre"
author: Martin Lessard
date: 2015-09-10
href: http://blogues.radio-canada.ca/triplex/2015/09/10/sqil-2015-lappropriation-collective-de-linformatique-libre
tags:
- Entreprise
- Promotion
- International
---

> Aperçu du programme de la Semaine de l'informatique libre du Québec (SQIL)
