---
site: Ere numerique
title: "Le gouvernement britannique ne veut plus de Microsoft Office"
author: Nicolas Boudier-Ducloy
date: 2015-09-12
href: http://www.erenumerique.fr/le_gouvernement_britannique_ne_veut_plus_de_microsoft_office-article-12928-1.html
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Standards
- International
---

> Alors que la prochaine version de Microsoft Office arrive dans moins de deux semaines, le gouvernement britannique appelle à nouveau ses administrations à s'en débarrasser.
