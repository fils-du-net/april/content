---
site: internet ACTU.net
title: "Qui nous protégera des logiciels tricheurs?"
author: Hubert Guillaud
date: 2015-09-29
href: http://www.internetactu.net/2015/09/29/qui-nous-protegera-des-logiciels-tricheurs
tags:
- Entreprise
- Institutions
- Sensibilisation
- Standards
---

> Le volkswagengate a bien sûr reposé la double question de l’indépendance des contrôles et de la transparence du code. Si la question de l’indépendance des contrôles est en passe d’être résolue par des mesures d’homologation aléatoires en conditions de conduites réelles, la question de la transparence, pour des questions de propriété intellectuelle, elle, demeure complète.
