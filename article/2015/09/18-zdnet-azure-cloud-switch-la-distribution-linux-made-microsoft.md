---
site: ZDNet
title: "Azure Cloud Switch: la distribution Linux made in Microsoft"
date: 2015-09-18
href: http://www.zdnet.fr/actualites/azure-cloud-switch-la-distribution-linux-de-microsoft-39825136.htm
tags:
- Entreprise
- Innovation
- Informatique en nuage
---

> Microsoft a officialisé l’existence d’une distribution Linux concoctée par ses soins, Azure Cloud Switch (ACS). Cette distribution est pensée pour Azure et tout particulièrement pour faciliter la gestion des équipements réseaux de l’infrastructure Azure.
