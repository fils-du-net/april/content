---
site: 01net.
title: "Guerre des brevets: Apple pourrait exiger de Samsung qu'il retire trois fonctions de ses smartphones"
author: Cécile Bolesse
date: 2015-09-18
href: http://www.01net.com/actualites/guerre-des-brevets-apple-pourrait-faire-retirer-trois-fonctions-des-smartphones-samsung-915942.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> Le conflit entre Apple et Samsung rebondit. Une cour d’appel estime que le fabricant sud-coréen aurait dû se voir interdire des fonctions comme la correction automatique et le «glisser pour déverrouiller», brevetées par Apple.
