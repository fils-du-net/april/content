---
site: Le Monde.fr
title: "Dans les Yvelines, des éco-hackeurs au service du climat"
author: Audrey Garric
date: 2015-09-16
href: http://www.lemonde.fr/planete/visuel/2015/09/16/dans-les-yvelines-des-eco-hackeurs-au-service-du-climat_4759698_3244.html
tags:
- Économie
- Partage du savoir
- Innovation
---

> Dans un château à une heure de Paris, une centaine d’ingénieurs et de designers inventent des solutions en open source pour la transition énergétique, à 80 jours de la COP21.
