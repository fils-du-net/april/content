---
site: Numerama
title: "Comment le scandale Volkswagen rejaillit sur le droit d'auteur"
author: Guillaume Champeau
date: 2015-09-22
href: http://www.numerama.com/magazine/34260-comment-le-scandale-volkswagen-rejaillit-sur-le-droit-d-auteur.html
tags:
- Entreprise
- Logiciels privateurs
- Droit d'auteur
- International
---

> Selon l'Electronic Frontier Foundation (EFF), Volkswagen n'aurait pas pu dissimuler le logiciel truquant les résultats des tests anti-pollution de ses voitures si la loi n'interdisait pas aux chercheurs de travailler sur le firmware des véhicules.
