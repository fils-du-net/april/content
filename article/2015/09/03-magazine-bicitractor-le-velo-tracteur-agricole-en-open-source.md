---
site: UP Magazine
title: "Bicitractor: le vélo-tracteur agricole en open source"
author: Maryline Passini
date: 2015-09-03
href: http://up-magazine.info/index.php/planete/securite-alimentaire/4944-bicitractor-le-velo-tracteur-agricole-a-construire-soi-meme
tags:
- Économie
- Matériel libre
- Innovation
- Sciences
---

> Voici un beau projet "BICITRACTOR" développé par Farmingsoul qui tracte l’avenir de l’agriculture "maraîchère" dans un sens durable au coeur des nouveaux open models (business models de l’économie ouverte qui ouvre aux Biens Communs) et d’une vraie économie de partage qui, je l’espère, créera une autre valeur loin de l’économie collaborative type Uber!
