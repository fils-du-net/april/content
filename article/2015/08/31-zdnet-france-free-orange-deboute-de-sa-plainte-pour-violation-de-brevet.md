---
site: ZDNet France
title: "Free: Orange débouté de sa plainte pour violation de brevet"
date: 2015-08-31
href: http://www.zdnet.fr/actualites/free-orange-deboute-de-sa-plainte-pour-violation-de-brevet-39824126.htm
tags:
- Entreprise
- Internet
- Institutions
- Brevets logiciels
---

> L'opérateur historique estimait que son concurrent viole ses brevets dans le domaine de la télévision de rattrapage et lui réclamait 250 millions d'euros de dommages et intérêts.
