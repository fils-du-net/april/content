---
site: Silicon.fr
title: "La Commission européenne renforce son action Open Source"
author: David Feugey
date: 2015-03-30
href: http://www.silicon.fr/la-commission-europeenne-renforce-sa-participation-dans-lopen-source-112455.html
tags:
- Interopérabilité
- Institutions
- Marchés publics
- Europe
---

> La stratégie Open Source de la Commission européenne est révisée et renforcée. Revue de détail des nouvelles initiatives.
