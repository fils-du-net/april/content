---
site: Developpez.com
title: "Le Manifeste GNU souffle sa 30e bougie"
author: Amine Horseman
date: 2015-03-20
href: http://www.developpez.com/actu/82813/Le-Manifeste-GNU-souffle-sa-30e-bougie-retour-sur-ce-celebre-document-initie-par-Richard-Stallman-qui-avait-bouleverse-le-monde-de-l-informatique
tags:
- Partage du savoir
- Philosophie GNU
---

> Retour sur ce célèbre document initié par Richard Stallman, qui avait bouleversé le monde de l'informatique
