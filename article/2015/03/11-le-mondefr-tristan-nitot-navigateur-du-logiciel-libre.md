---
site: Le Monde.fr
title: "Tristan Nitot, navigateur du logiciel libre"
date: 2015-03-11
href: http://www.lemonde.fr/pixels/article/2015/03/11/tristan-nitot-navigateur-du-logiciel-libre_4591132_4408996.html
tags:
- Internet
- April
- Promotion
- Informatique en nuage
- Vie privée
---

> Le visage de la fondation Mozilla, l'éditeur du navigateur Firefox, change de travail, mais pas d'idéaux.
