---
site: Next INpact
title: "Tristan Nitot devient directeur produit chez Cozy Cloud"
author: Sébastien Gavois
date: 2015-03-11
href: http://www.nextinpact.com/news/93410-tristan-nitot-devient-directeur-produit-chez-cozy-cloud.htm
tags:
- Internet
- Informatique en nuage
- Vie privée
---

> Un peu plus d'un mois après avoir quitté son poste chez Mozilla, Tristan Nitot annonce qu'il rejoint finalement les équipes de Cozy Cloud en tant que directeur produit (Chief Product Officer dans la langue de Shakespeare).
