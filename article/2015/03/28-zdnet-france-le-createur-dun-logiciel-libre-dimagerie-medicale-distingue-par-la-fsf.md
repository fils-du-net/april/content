---
site: ZDNet France
title: "Le créateur d'un logiciel libre d'imagerie médicale distingué par la FSF"
author: Thierry Noisette
date: 2015-03-28
href: http://www.zdnet.fr/actualites/le-createur-d-un-logiciel-libre-d-imagerie-medicale-distingue-par-la-fsf-39817098.htm
tags:
- Administration
- Interopérabilité
- Innovation
- Promotion
---

> La Free Software Foundation a décerné son prix annuel à Sébastien Jodogne, ingénieur en imagerie médicale du CHU de Liège, pour le logiciel Orthanc. Il est employé dans des hôpitaux, en Belgique et dans le monde, pour  améliorer et automatiser le flux d’imageries médicales
