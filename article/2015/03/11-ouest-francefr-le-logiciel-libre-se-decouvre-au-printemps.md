---
site: "ouest-france.fr"
title: "Le logiciel libre se découvre au printemps"
author: Léa Dall'Aglio
date: 2015-03-11
href: http://www.ouest-france.fr/le-logiciel-libre-se-decouvre-au-printemps-3248922
tags:
- April
- Partage du savoir
- Sensibilisation
- Associations
---

> Une semaine d'ateliers autour du logiciel libre, tel est le programme de «Libre en fête en Trégor», dès le 21 mars.
