---
site: Centre Presse
title: "Pour vivre le Web en liberté"
author: Laurent Gaudens
date: 2015-03-17
href: http://www.centre-presse.fr/article-375839-pour-vivre-le-web-en-liberte.html
tags:
- Associations
- Promotion
---

> Le logiciel libre, c'est une philosophie. Et l'APP3L est là pour diffuser la bonne parole et aider ceux qui veulent franchir le pas.
