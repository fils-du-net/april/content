---
site: ZDNet France
title: "Le code source développé par l’Etat est théoriquement accessible"
author: Louis Adam
date: 2015-03-09
href: http://www.zdnet.fr/actualites/le-code-source-developpe-par-l-etat-est-theoriquement-accessible-39816034.htm
tags:
- Administration
- Institutions
---

> Saisie par un chercheur, la Cada a rendu un avis concernant la question du code source des logiciels développés par les services de l’Etat: celui-ci est communicable aux citoyens qui en font la demande, à quelques exceptions près.
