---
site: LeMagIT
title: "La Commission Européenne s'engage pour les logiciels libres"
author: Christophe Bardy
date: 2015-03-30
href: http://www.lemagit.fr/actualites/4500243429/La-Commission-Europeenne-sengage-pour-les-logiciels-libres
tags:
- Administration
- Interopérabilité
- Institutions
- Marchés publics
- Europe
---

> Pour sa stratégie IT 2014-2017, la Commission Européenne a choisi de mettre en avant les solutions et les standards open source. Le MagIT revient sur les 10 points mis en avant par la CE.
