---
site: ActuaLitté.com
title: "Comparer un livre imprimé à un ebook verrouillé, une incohérence"
author: Nicolas Gary
date: 2015-03-05
href: https://www.actualitte.com/legislation/comparer-un-livre-imprime-a-un-ebook-verrouille-une-incoherence-55611.htm
tags:
- Économie
- April
- DRM
- Promotion
- Sciences
- Europe
---

> Au commencement était la plaisanterie potache. La Team Alexandriz, depuis son fil Twitter, lance un détournement de la campagne du Syndicat national de l'édition, ThatIsNotABook. Le SNE souhaite sensibiliser la Commission européenne à la neutralité fiscale, autrement dit, une TVA égale pour livres papier et numériques. Et puis, intervient l'APRIL, avec un projet on ne peut plus sérieux...
