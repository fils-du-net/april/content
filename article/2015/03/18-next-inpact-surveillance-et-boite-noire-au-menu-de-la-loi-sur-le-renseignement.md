---
site: Next INpact
title: "Surveillance et boîte noire au menu de la loi sur le renseignement"
author: Marc Rees
date: 2015-03-18
href: http://www.nextinpact.com/news/93484-surveillance-et-boite-noire-au-menu-loi-sur-renseignement.htm
tags:
- Internet
- Institutions
- Vie privée
---

> Jour après jour, le projet de loi sur le renseignement dévoile ses ombres. On a appris ainsi hier que le gouvernement entendait installer des boîtes noires sur les infrastructures réseau des opérateurs télécoms. Mieux, ces mesures pourront être étendues aux acteurs de l’internet. Tour d’horizon grâce aux explications apportées par l'exécutif.
