---
site: Rue89Lyon
title: "Connexions pour un numérique éthique, aux journées du logiciel libre à Lyon"
author: Dalya Daoud
date: 2015-03-31
href: http://www.rue89lyon.fr/2015/03/31/connexions-numerique-ethique-journees-logiciel-libre-lyon
tags:
- Sensibilisation
- Associations
- Promotion
---

> Entre geeks barbus, gosses passionnés et têtes blanches, retour sur l’un des plus importants rendez-vous des libristes -les usagers des logiciels libres- en France. Ce week-end se déroulaient à Lyon les fameuses Journées du logiciel libre (JDLL), un événement qui fait clignoter les neurones.
