---
site: Le Telegramme
title: "Logiciels libres. La Ville donne l'exemple"
date: 2015-03-21
href: http://www.letelegramme.fr/cotes-darmor/lannion/logiciels-libres-la-ville-donne-l-exemple-21-03-2015-10565762.php
tags:
- Administration
- Matériel libre
- Open Data
---

> C'est une réalité sur les ordinateurs de la Ville. Les élus ont décidé de privilégier les logiciels libres depuis le début des années 2000. Dans ce monde du gratuit et du collaboratif, ils ont aussi opté pour le partage de données.
