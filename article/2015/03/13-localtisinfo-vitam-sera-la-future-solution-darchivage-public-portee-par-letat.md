---
site: Localtis.info
title: "Vitam sera la future solution d'archivage public portée par l'Etat"
author: Philippe Parmantier
date: 2015-03-13
href: http://www.localtis.info/cs/ContentServer?pagename=Localtis/LOCActu/ArticleActualite$urljid=1250268600977$urlcid=1250268598094
tags:
- Administration
- Partage du savoir
---

> Lancé le 9 mars en présence de la ministre de la Culture, Fleur Pellerin, et de Thierry Mandon, secrétaire d'Etat chargé de la réforme de l'Etat et de la simplification, le programme Vitam vise à créer une solution d'archivage unifiée pour l'administration. Sur le plan des grands principes, elle sera conforme au concept d'Etat plateforme, adaptée à l'explosion des données, déjà amorcée, et largement réutilisable, y compris par les collectivités territoriales... sur une base volontaire.
