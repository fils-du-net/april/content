---
site: "usine-digitale.fr"
title: "L’Etat s’ouvre davantage aux logiciels open source"
author: Ridha Loukil
date: 2015-03-16
href: http://www.usine-digitale.fr/article/l-etat-s-ouvre-davantage-aux-logiciels-open-source.N319400
tags:
- Économie
- Institutions
- Marchés publics
- RGI
---

> Avec le nouveau socle interministériel de logiciels libres, l’Etat confirme sa volonté d’accroiîre l’utilisation de l’open source dans ses services. Au menu : non seulement les postes de travail, mais aussi les serveurs, les bases de données et les outils de développement.
