---
site: Next INpact
title: "Les enfants devront savoir utiliser une tablette avant leur entrée en CP"
author: Xavier Berne
date: 2015-03-26
href: http://www.nextinpact.com/news/93593-les-enfants-devront-savoir-utiliser-tablette-avant-leur-entree-en-cp.htm
tags:
- Administration
- Éducation
---

> Le ministère de l’Éducation nationale a publié aujourd’hui le nouveau programme d’enseignement qui s’appliquera dès la rentrée prochaine dans toutes les écoles maternelles. L’apprentissage des outils numériques, et plus particulièrement des tablettes, revient à plusieurs reprises.
