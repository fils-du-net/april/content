---
site: Numerama
title: "10 problèmes posés par la censure d'Islamic-News.info"
author: Guillaume Champeau
date: 2015-03-16
href: http://www.numerama.com/magazine/32494-10-problemes-poses-par-la-censure-d-islamic-newsinfo.html
tags:
- Internet
- Institutions
---

> En ordonnant la censure d'Islam-News.info sur simple décision administrative, le Gouvernement a donné raison à ceux qui craignaient que le dispositif introduit par la loi anti-terrorisme de 2014 soit utilisée abusivement.
