---
site: ZDNet France
title: "Tristan Nitot rejoint le cloud open-source de Cozy"
author: Guillaume Serries
date: 2015-03-11
href: http://www.zdnet.fr/actualites/tristan-nitot-rejoint-le-cloud-open-source-de-cozy-39816192.htm
tags:
- Entreprise
- Internet
- Innovation
- Informatique en nuage
- Vie privée
---

> Le français Cozy Cloud recrute l'ex évangéliste Europe de Mozilla. De quoi se faire connaître pour cette startup, et continuer l'aventure de l'open source pour Tristan Nitot. Cette fois côté cloud computing.
