---
site: "ouest-france.fr"
title: "Tux, la mascotte de Linux, représente la fête du Libre"
date: 2015-03-12
href: http://www.ouest-france.fr/tux-la-mascotte-de-linux-represente-la-fete-du-libre-3252574
tags:
- Partage du savoir
- Associations
- Promotion
---

> Conférences, entraide... Depuis 15 ans, au printemps, partout en France sont organisés des événements de découverte des logiciels libres et du «Libre» en général. Rendez-vous les 20 et 21 mars au centre social.
