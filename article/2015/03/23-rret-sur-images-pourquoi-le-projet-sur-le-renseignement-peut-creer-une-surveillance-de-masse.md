---
site: "@rrêt sur images"
title: "Pourquoi le projet sur le renseignement peut créer une \"surveillance de masse\""
author: Jean-Marc Manach
date: 2015-03-23
href: http://www.arretsurimages.net/chroniques/2015-03-23/Pourquoi-le-projet-sur-le-renseignement-peut-creer-une-surveillance-de-masse-id7589
tags:
- Internet
- Institutions
- Associations
- Vie privée
---

> Les défenseurs des libertés sur Internet ont peur. Depuis la révélation, dans Le Figaro, que le gouvernement voulait installer des "boîtes noires" chez les opérateurs télécom afin de pouvoir surveiller, "en temps réel" et "de manière totale et systématique" les personnes représentant une "menace", ainsi que les "comportements suspects", on ne compte plus le nombre d'articles criant au scandale et de réactions anxiogènes. Etrangement, et à l'exception de NextInpact, aucun n'explique ce que recouvrent précisément les nouveaux articles 851-3 et 851-4 dont il est question.
