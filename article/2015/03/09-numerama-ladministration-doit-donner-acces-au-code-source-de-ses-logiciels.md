---
site: Numerama
title: "L'administration doit donner accès au code source de ses logiciels!"
author: Guillaume Champeau
date: 2015-03-09
href: http://www.numerama.com/magazine/32423-l-administration-doit-donner-acces-au-code-source-de-ses-logiciels.html
tags:
- Administration
- Institutions
- Licenses
---

> Dans un avis rendu en début d'année, la Commission d'accès aux documents administratifs (CADA) estime que le code source des logiciels produits par les services centraux ou décentralisés de l'Etat sont des documents administratifs comme les autres, qui doivent être communiqués à qui en fait la demande.
