---
site: Le Monde.fr
title: "Laura Poitras: «Les gouvernements veulent militariser Internet»"
date: 2015-03-04
href: http://www.lemonde.fr/pixels/article/2015/03/04/laura-poitras-les-gouvernements-veulent-militariser-internet_4586981_4408996.html
tags:
- Internet
- Institutions
- Europe
- Vie privée
---

> La réalisatrice de Citizenfour, le documentaire consacré à Edward Snowden et à la surveillance de masse qui sort en salles ce mercredi, participait ce mardi à un débat organisé par Le Monde et Haut et Court. En voici les principaux moments.
