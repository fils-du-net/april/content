---
site: GreenIT.fr
title: "Libre et ouvert, oui, mais pour qui?"
author: David Bourguignon
date: 2015-03-04
href: http://www.greenit.fr/article/logiciels/libre-et-ouvert-oui-mais-pour-qui-5436
tags:
- Entreprise
- Économie
---

> Le logiciel “libre et ouvert” fut une belle utopie, née au début de l’épopée de l’ordinateur personnel, et qui mourra probablement avec la récupération quasi-totale de ce mouvement par quelques firmes oligopolistiques, dans le courant de cette décennie. Pourquoi une vie si brève? Probablement parce que, dès le début, un non-dit s’est glissé dans la définition de l’enjeu du mouvement par ses propres acteurs: libre et ouvert, oui, mais pour qui?
