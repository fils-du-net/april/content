---
site: Next INpact
title: "Copie privée: jeudi, un nouvel arrêt important à la CJUE"
author: Marc Rees
date: 2015-03-02
href: http://www.nextinpact.com/news/93243-copie-privee-jeudi-nouvel-arret-important-a-cjue.htm
tags:
- Économie
- Institutions
- Europe
---

> Jeudi 5 mars, la Cour de justice de l'Union européenne rendra un nouvel arrêt important en matière de redevance pour copie privée. L’affaire est née au Danemark, mais ses résultats pourraient irradier l’ensemble des États membres qui ont opté pour ce prélèvement, dont la France. Explications.
