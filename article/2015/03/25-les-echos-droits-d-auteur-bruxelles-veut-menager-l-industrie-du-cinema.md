---
site: Les Echos
title: "Droits d’auteur: Bruxelles veut ménager l’industrie du cinéma"
author: Renaud Honoré
date: 2015-03-25
href: http://www.lesechos.fr/tech-medias/medias/0204255022191-droits-dauteur-bruxelles-veut-menager-lindustrie-du-cinema-1105413.php
tags:
- Internet
- Institutions
- Droit d'auteur
- Promotion
- Europe
---

> La Commission a dévoilé les premières pistes de sa stratégie numérique, avec notamment la réforme attendue des droits d’auteur.Bruxelles semble renoncer au big-bang.
