---
site: Libération.fr
title: "La France sommée de relever la TVA pour les livres numériques"
author: Camille Gévaudan
date: 2015-03-05
href: http://www.liberation.fr/culture/2015/03/05/la-france-sommee-de-relever-la-tva-pour-les-livres-numeriques_1214767
tags:
- Économie
- April
- Institutions
- DRM
- Europe
---

> La Cour de justice de l’Union européenne a jugé que le taux réduit n'est pas conforme aux directives européennes. Le Syndicat national du livre plaide pour une harmonisation avec les livres papier.
