---
site: Slate.fr
title: "Comment sauver l'open-source?"
author: David Auerbach
date: 2015-03-05
href: http://www.slate.fr/story/98575/open-source
tags:
- Entreprise
- Économie
- Innovation
---

> Une des principales infrastructures d'Internet vit presque sans financement.
