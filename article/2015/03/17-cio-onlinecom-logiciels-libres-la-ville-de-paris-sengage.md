---
site: "cio-online.com"
title: "Logiciels libres : la ville de Paris s'engage"
author: Bertrand Lemaire
date: 2015-03-17
href: http://www.cio-online.com/actualites/lire-logiciels-libres%A0-la-ville-de-paris-s-engage-7464.html
tags:
- Administration
- April
---

> Comme d'autres collectivités avant elle, la ville de Paris vient d'adhérer à l'APRIL, association qui oeuvre pour promouvoir le logiciel libre.
