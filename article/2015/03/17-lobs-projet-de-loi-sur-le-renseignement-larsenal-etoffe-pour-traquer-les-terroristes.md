---
site: L'OBS
title: "Projet de loi sur le renseignement : l'arsenal étoffé pour traquer les terroristes"
author: Boris Manenti
date: 2015-03-17
href: http://tempsreel.nouvelobs.com/tech/20150317.OBS4798/projet-de-loi-sur-le-renseignement-l-arsenal-qui-fait-du-citoyen-un-suspect.html
tags:
- Internet
- Institutions
- Vie privée
---

> Le projet de loi sur le renseignement qui sera présenté jeudi en Conseil des ministres multiplie les possibilités de surveillance, notamment en contrôlant internet.
