---
site: Numerama
title: "Les FAI devront livrer à l'Etat toutes les infos sur leurs réseaux"
author: Guillaume Champeau
date: 2015-03-30
href: http://www.numerama.com/magazine/32633-les-fai-devront-livrer-a-l-etat-toutes-les-infos-sur-leurs-reseaux.html
tags:
- Entreprise
- Internet
- Institutions
---

> En vertu d'un décret du 27 mars 2015, les fournisseurs d'accès à internet et hébergeurs déclarés "d'importance vitale" auront l'obligation de fournir à l'Etat toutes les documentations techniques sur les matériels et logiciels utilisés dans leurs réseaux, ainsi que les codes sources.
