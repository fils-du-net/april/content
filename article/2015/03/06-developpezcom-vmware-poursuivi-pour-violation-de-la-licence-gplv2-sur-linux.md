---
site: Developpez.com
title: "VMware poursuivi pour violation de la licence GPLv2 sur Linux"
author: Michael Guilloux
date: 2015-03-06
href: http://www.developpez.com/actu/82199/VMware-poursuivi-pour-violation-de-la-licence-GPLv2-sur-Linux-ses-produits-utilisent-des-codes-soumis-au-droit-d-auteur-sans-autorisation
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Licenses
- Informatique en nuage
---

> Christoph Hellwig, développeur clé et l'un des meilleurs contributeurs du noyau Linux a porté plainte contre VMware à la cour de district de Hambourg en Allemagne. Le développeur connu comme le responsable du sous-système de stockage SCSI du noyau Linux accuse la société informatique US de violations de la GNU General Public License (GPL). La société spécialisée dans les produits liés à la virtualisation aurait en effet omis de se conformer aux conditions de droit d'auteur pour l'utilisation de logiciels open source.
