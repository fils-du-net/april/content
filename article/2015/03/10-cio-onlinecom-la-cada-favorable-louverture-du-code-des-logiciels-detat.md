---
site: "cio-online.com"
title: "La CADA favorable à l'ouverture du code des logiciels d'Etat"
author: Bertrand Lemaire
date: 2015-03-10
href: http://www.cio-online.com/actualites/lire-la-cada-favorable-a-l-ouverture-du-code-des-logiciels-d-etat-7450.html
tags:
- Administration
- Institutions
- Informatique en nuage
---

> Un logiciel créé par des fonctionnaires doit pouvoir être délivré à un demandeur comme n'importe quel document selon la Commission d'accès aux documents administratifs (CADA) qui s'est prononcé suite à un litige relatif à un logiciel de calcul fiscal.
