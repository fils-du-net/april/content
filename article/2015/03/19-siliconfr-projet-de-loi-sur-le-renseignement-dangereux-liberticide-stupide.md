---
site: Silicon.fr
title: "Projet de loi sur le renseignement: «dangereux», «liberticide», «stupide»"
author: Reynald Fléchaux
date: 2015-03-19
href: http://www.silicon.fr/projet-loi-renseignement-dangereux-liberticide-stupide-111470.html
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Vie privée
---

> Volée de bois vert pour le projet de loi sur le renseignement. Les associations représentant l’industrie et les défenseurs des libertés numériques pointent notamment deux mesures: l’installation de boîtes noires d’analyse des comportements sur les réseaux des opérateurs ou FAI et la remise aux autorités de moyens de déchiffrement.
