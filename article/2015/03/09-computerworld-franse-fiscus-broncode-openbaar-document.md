---
site: Computerworld
title: "Franse fiscus-broncode is 'openbaar document'"
author: Jasper Bakker
date: 2015-03-09
href: http://computerworld.nl/open-source/85595-franse-fiscus-broncode-is--openbaar-document
tags:
- Administration
- Institutions
- Open Data
---

> Het Franse ministerie van EZ is op de vingers getikt voor het achterhouden van de broncode voor belastingsoftware. Burgers hebben recht op dat 'openbare document'.
