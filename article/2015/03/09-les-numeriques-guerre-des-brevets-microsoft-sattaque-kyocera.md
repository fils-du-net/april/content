---
site: Les Numeriques
title: "Guerre des brevets: Microsoft s'attaque à Kyocera"
author: Johann Breton
date: 2015-03-09
href: http://www.lesnumeriques.com/guerre-brevets-microsoft-attaque-a-kyocera-n40297.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> Aux États-Unis, Microsoft vient de déposer une plainte contre le Japonais Kyocera. Il reproche au fabricant de smartphones durcis d'avoir enfreint 7 brevets au travers de fonctions relativement basiques d'Android.
