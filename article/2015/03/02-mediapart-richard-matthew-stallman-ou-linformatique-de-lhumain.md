---
site: Mediapart
title: "Richard Matthew Stallman ou l'informatique de l'humain"
author: Véronique Bonnet
date: 2015-03-02
href: http://blogs.mediapart.fr/edition/la-revue-du-projet/article/020315/richard-matthew-stallman-ou-linformatique-de-lhumain
tags:
- April
- Philosophie GNU
- Promotion
---

> L’Humanité.fr, dans un article du 13 septembre 2014, proposait des morceaux choisis du discours sur les droits de l’homme et l’informatique que Richard Matthew Stallman avait tenu la veille à la Fête de l’Humanité.
