---
site: Contrepoints
title: "Rapport Reda: vers l’abolition de la propriété intellectuelle en Europe?"
author: Ferghane Azihari
date: 2015-03-23
href: http://www.contrepoints.org/2015/03/23/201894-rapport-reda-vers-labolition-de-la-propriete-intellectuelle-en-europe
tags:
- Institutions
- Droit d'auteur
- Europe
---

> L’eurodéputée pirate Julia Reda est la cible de l’industrie culturelle qui, épaulée par les États européens, veut continuer à abuser les citoyens.
