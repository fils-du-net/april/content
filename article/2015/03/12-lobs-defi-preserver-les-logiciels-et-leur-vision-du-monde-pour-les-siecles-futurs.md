---
site: L'OBS
title: "Défi: préserver les logiciels et leur vision du monde pour les siècles futurs"
author: Rémi Sussan
date: 2015-03-12
href: http://rue89.nouvelobs.com/2015/03/12/defi-preserver-les-logiciels-vision-monde-les-siecles-futurs-258027
tags:
- Partage du savoir
- Innovation
---

> A côté du «manuel de la civilisation», une encyclopédie de la culture qui pourrait servir de réceptacle à la culture humaine en cas de catastrophe naturelle ou artificielle, la fondation du Long Now s’intéresse à d’autres formes de préservation.
