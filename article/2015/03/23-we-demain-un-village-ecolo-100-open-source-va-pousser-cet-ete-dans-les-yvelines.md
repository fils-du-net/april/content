---
site: We Demain
title: "Un village écolo 100 % open source va pousser cet été dans les Yvelines"
author: Côme Bastin
date: 2015-03-23
href: http://www.wedemain.fr/Un-village-ecolo-100-open-source-va-pousser-cet-ete-dans-les-Yvelines_a918.html
tags:
- Partage du savoir
- Matériel libre
- Associations
- Innovation
- Promotion
- Sciences
---

> Du 15 août au au 20 septembre, une centaine de makers va bâtir un village de la transition énergétique au château de Millemont. Co-fondateur du collectif OuiShare, dédié à l'économie collaborative, Benjamin Tincq est l'un des initiateurs de ce projet baptisé POC 21. Interview.
