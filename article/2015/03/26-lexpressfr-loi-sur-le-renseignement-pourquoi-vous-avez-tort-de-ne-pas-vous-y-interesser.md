---
site: L'Express.fr
title: "Loi sur le Renseignement: pourquoi vous avez tort de ne pas vous y intéresser"
author: Raphaele Karayan
date: 2015-03-26
href: http://www.lexpress.fr/actualite/politique/loi-sur-le-renseignement-pourquoi-vous-avez-tort-de-ne-pas-vous-y-interesser_1665376.html
tags:
- Internet
- Institutions
- Associations
- Vie privée
---

> Vous n'êtes pas juriste, vous n'êtes pas geek et vous ne comprenez pas pourquoi le débat sur les moyens de lutter contre les terroristes vous concerne? Cet article est pour vous. Promis, vous ne verrez plus le projet de loi de la même façon.
