---
site: Next INpact
title: "L'État actualise son socle interministériel de logiciels libres"
author: Xavier Berne
date: 2015-03-10
href: http://www.nextinpact.com/news/93390-l-etat-actualise-son-socle-interministeriel-logiciels-libres.htm
tags:
- Administration
- HADOPI
- RGI
---

> La France vient de renouveler son «socle interministériel de logiciels libres», cet espèce de guide des solutions libres recommandées pour les ordinateurs de l’administration. On retrouve dans le «cru 2015» des programmes bien connus tels que VLC, Firefox, LibreOffice ou bien encore Adblock Plus.
