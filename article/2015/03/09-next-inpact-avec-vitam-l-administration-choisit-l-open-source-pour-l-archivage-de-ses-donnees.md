---
site: Next INpact
title: "Avec VITAM, l’administration choisit l’open source pour l’archivage de ses données"
author: Xavier Berne
date: 2015-03-09
href: http://www.nextinpact.com/news/93376-avec-vitam-l-administration-choisit-l-open-source-pour-l-archivage-ses-donnees.htm
tags:
- Administration
- Partage du savoir
---

> Le gouvernement donne aujourd’hui le coup d’envoi des travaux du projet VITAM, du nom de ce socle interministériel destiné à l’archivage des documents électroniques détenus par l’administration. Développé en open source, ce programme va tout d’abord concerner trois ministères particulièrement concernés par les problématiques d’archivage: la Défense, les Affaires étrangères et la Culture.
