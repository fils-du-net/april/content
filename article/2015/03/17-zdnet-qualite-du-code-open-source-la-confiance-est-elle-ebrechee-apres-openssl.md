---
site: ZDNet
title: "Qualité du code Open source: la confiance est-elle ébréchée après OpenSSL?"
date: 2015-03-17
href: http://www.zdnet.fr/partenaires/ibm/qualite-du-code-open-source-la-confiance-est-elle-ebrechee-apres-openssl-39816458.htm
tags:
- Entreprise
- Innovation
---

> La communauté des développeurs a-t-elle été ébranlée par une succession de failles du protocole de chiffrement SSL? Ebranlée, non, mais bien secouée. Certains éditeurs en profitent pour parler de «réinvestir». Pourquoi?
