---
site: ZDNet France
title: "Inde: l'administration amorce un virage open source"
author: Louis Adam
date: 2015-03-31
href: http://www.zdnet.fr/actualites/inde-l-administration-amorce-un-virage-open-source-39817228.htm
tags:
- Administration
- Institutions
- International
---

> Dans une circulaire publiée dimanche, le gouvernement indien a annoncé sa volonté de redoubler d’effort pour généraliser l’utilisation d’outils open source au sein des différents ministères et administrations du pays.
