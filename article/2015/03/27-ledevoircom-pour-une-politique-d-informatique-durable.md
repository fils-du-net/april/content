---
site: LeDevoir.com
title: "Pour une politique d’informatique durable"
author: Yves Moisan
date: 2015-03-27
href: http://www.ledevoir.com/societe/science-et-technologie/435622/pour-une-politique-d-informatique-durable
tags:
- Économie
- Interopérabilité
- Institutions
- Marchés publics
- International
---

> La sphère médiatique ne manque pas de déclarations concernant le «bordel informatique» au gouvernement du Québec, certains — comme la Coalition avenir Québec et le Parti québécois — appelant même une commission d’enquête. À défaut d’avoir la légitimité qui m’autoriserait à prodiguer des conseils, je me permets de formuler ce que je crois être une opinion citoyenne forte: dans ce contexte d’austérité et de précarité économique généralisée, il est grand temps que le Québec se dote d’une politique d’informatique durable.
