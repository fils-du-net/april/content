---
site: Numerama
title: "Bruxelles impose la TVA à 20 % sur les livres électroniques"
author: Guillaume Champeau
date: 2015-03-05
href: http://www.numerama.com/magazine/32393-bruxelles-impose-la-tva-a-20-sur-les-livres-electroniques.html
tags:
- Économie
- Institutions
- DRM
- Europe
---

> Les livres numériques devront être vendus en France avec un taux de TVA de 20 %, alors que les mêmes livres au format papier pourront être vendus au taux de 5,5 %. Ainsi en a jugé la justice européenne, dans une application rigoriste de la directive TVA, devenue anachronique.
