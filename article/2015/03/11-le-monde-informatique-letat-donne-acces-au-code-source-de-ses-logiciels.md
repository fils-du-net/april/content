---
site: Le Monde Informatique
title: "L'Etat donne accès au code source de ses logiciels"
author: Bertand Lemaire
date: 2015-03-11
href: http://www.lemondeinformatique.fr/actualites/lire-l-etat-donne-acces-au-code-source-de-ses-logiciels-60498.html
tags:
- Administration
- Institutions
- Open Data
---

> Dans un litige relatif à un logiciel de calcul fiscal, la Commission d'accès aux documents administratifs a fait savoir que, sous certaines précautions, tout demandeur pouvait avoir accès au code source des logiciels créés par l'administration.
