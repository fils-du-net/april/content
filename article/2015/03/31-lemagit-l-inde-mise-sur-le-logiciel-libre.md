---
site: LeMagIT
title: "L’Inde mise sur le logiciel libre"
author: Valéry Marchive
date: 2015-03-31
href: http://www.lemagit.fr/actualites/4500243459/LInde-mise-sur-le-logiciel-libre
tags:
- Institutions
- Marchés publics
- International
---

> Le gouvernement indien vient de publier sa politique relative à l’Open source. Celle-ci affiche une ambition: «encourager l’adoption formelle et l’utilisation de logiciels libres dans les organisations gouvernementales».
