---
site: mensquare
title: "Le projet de loi sur le renseignement ne convainc pas"
date: 2015-03-27
href: http://www.mensquare.com/actu/politique/1000859-loi-renseignement-mobilise-defenseurs-libertes-individuelles
tags:
- Internet
- Institutions
- Associations
- Vie privée
---

> Pour les opposants à la loi sur le renseignement, le gouvernement veut instaurer un système proche de celui de la NSA en France, au détriment des libertés.
