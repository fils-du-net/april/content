---
site: Next INpact
title: "Comment la France veut décupler les pouvoirs du renseignement"
author: Marc Rees
date: 2015-03-17
href: http://www.nextinpact.com/news/93476-comment-france-veut-decupler-pouvoirs-renseignement.htm
tags:
- Internet
- Institutions
---

> «Il ne s’agit pas d’adopter un Patriot Act à la française» promettait Axelle Lemaire, le 14 janvier dernier. Le projet de loi sur le renseignement sera présenté demain en conseil des ministres. L'objectif? Muscler les moyens d’anticipation de la police administrative en France. Le texte devrait cependant susciter de vives critiques compte tenu de son ampleur.
