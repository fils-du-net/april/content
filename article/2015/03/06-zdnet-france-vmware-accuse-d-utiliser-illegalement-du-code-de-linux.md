---
site: ZDNet France
title: "VMware: accusé d’utiliser illégalement du code de Linux"
date: 2015-03-06
href: http://www.zdnet.fr/actualites/vmware-accuse-d-utiliser-illegalement-du-code-de-linux-39815922.htm
tags:
- Entreprise
- Logiciels privateurs
- Licenses
---

> Un important contributeur du kernel Linux, Christoph Hellwig, porte plainte en Allemagne contre VMWare, accusé d’exploiter dans ses produits propriétaires ESXi du code sous GPL v2 en violation de la licence.
