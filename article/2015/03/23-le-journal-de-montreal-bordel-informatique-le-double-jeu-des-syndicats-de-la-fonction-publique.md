---
site: Le Journal de Montréal
title: "«Bordel informatique»: le double jeu des syndicats de la fonction publique."
author: Cyrille Béraud
date: 2015-03-23
href: http://www.journaldemontreal.com/2015/03/23/bordel-informatique---le-double-jeu-des-syndicats-de-la-fonction-publique
tags:
- Logiciels privateurs
- Administration
- Économie
- Associations
- Marchés publics
- International
---

> Depuis quelques semaines, un groupe hétéroclite d’associations rassemblées autour du Syndicat des professionnels du gouvernement du Québec (SPGQ) exige la mise en place d’une commission d’enquête sur les marchés publics dans le secteur des technologies de l’information.
