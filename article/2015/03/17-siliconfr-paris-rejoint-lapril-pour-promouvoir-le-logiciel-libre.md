---
site: Silicon.fr
title: "Paris rejoint l'April pour promouvoir le logiciel libre"
author: Ariane Beky
date: 2015-03-17
href: http://www.silicon.fr/paris-rejoint-april-logiciel-libre-111190.html
tags:
- Administration
- April
---

> Déjà membre de l’Adullact et de OW2, la Ville de Paris rejoint l’April, association de promotion et défense du logiciel libre. La capitale française confirme ainsi son engagement à la communauté.
