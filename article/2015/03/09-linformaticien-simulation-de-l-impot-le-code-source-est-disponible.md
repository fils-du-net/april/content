---
site: L'Informaticien
title: "Simulation de l’impôt: le code source est disponible!"
author: Emilien Ercolani
date: 2015-03-09
href: http://www.linformaticien.com/actualites/id/35982/simulation-de-l-impot-le-code-source-est-disponible.aspx
tags:
- Administration
- April
- Institutions
- Licenses
- Open Data
---

> Dans un avis, la Commission d’accès aux documents administratifs (CADA) a estimé qu’il est possible de communiquer le code source d’un logiciel conçu par l’Etat. En l’occurrence, cela concerne le simulateur d’impôts en ligne.
