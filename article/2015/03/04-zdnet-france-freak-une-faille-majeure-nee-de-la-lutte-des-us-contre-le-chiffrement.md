---
site: ZDNet France
title: "\"FREAK\" – une faille majeure née de la lutte des US contre le chiffrement"
date: 2015-03-04
href: http://www.zdnet.fr/actualites/freak-une-faille-majeure-nee-de-la-lutte-des-us-contre-le-chiffrement-39815774.htm
tags:
- Internet
- Institutions
- Vie privée
---

> Les Etats-Unis ne voulaient pas d’un chiffrement fort, et la faille FREAK affectant le TLS/SSL est un héritage de cette politique. Une attaque FREAK permet ainsi de forcer l’utilisation d’un chiffrement ancien et vulnérable et donc d’intercepter des données en principe protégées. Mise à jour: l'interview d'Antoine Delignat-Lavaud, chercheur au sein de l’équipe Prosecco qui a dévoilé ces failles.
