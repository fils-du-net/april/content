---
site: Developpez.com
title: "Microsoft poursuit un équipementier partenaire potentiel"
author: Stéphane le calme
date: 2015-03-07
href: http://www.developpez.com/actu/82260/Microsoft-poursuit-un-equipementier-partenaire-potentiel-pour-violation-de-ses-brevets-Android
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> Microsoft a déposé une plainte ce vendredi contre le fabricant japonais de smartphones Kyocera suite à la découverte d’une utilisation de sept de ses brevets Android sans autorisation par ses lignes de téléphone Hydro et Brigadier. Parmi ces brevets, on note celui qui protège l’utilisation des services de localisation et la messagerie par texte.
