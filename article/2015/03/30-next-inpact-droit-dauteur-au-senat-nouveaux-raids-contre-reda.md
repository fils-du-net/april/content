---
site: Next INpact
title: "Droit d'auteur: au Sénat, nouveaux raids contre Reda"
author: Marc Rees
date: 2015-03-30
href: http://www.nextinpact.com/news/93627-droit-dauteur-au-senat-nouveaux-raids-contre-reda.htm
tags:
- Institutions
- Droit d'auteur
- Europe
---

> Mercredi dernier, la Commission de la Culture du Sénat a réuni plusieurs sociétés de gestion collective autour d’une table ronde. Enjeu ? Discuter de la possible refonte de la directive sur le droit d’auteur. À cette occasion, les ayants droit français ont une nouvelle fois démultiplié les critiques contre le rapport de l’eurodéputée du Parti Pirate, Julia Reda, portant sur le sujet.
