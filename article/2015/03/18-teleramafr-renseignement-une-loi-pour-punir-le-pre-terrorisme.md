---
site: Télérama.fr
title: "Renseignement: une loi pour punir le «pré-terrorisme»?"
author: Olivier Tesquet
date: 2015-03-18
href: http://www.telerama.fr/medias/renseignement-une-loi-pour-punir-le-pre-terrorisme,124333.php
tags:
- Internet
- Institutions
- Vie privée
---

> Manuel Valls présente en conseil des ministres ce jeudi 19 mars un projet de loi renseignement qui prévoit la création d'un algorithme de détection des comportements suspects sur Internet. Faut-il déjà crier au loup?
