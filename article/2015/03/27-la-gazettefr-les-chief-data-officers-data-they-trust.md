---
site: La gazette.fr
title: "Les chief data officers: in data, they trust"
author: Sabine Blanc
date: 2015-03-27
href: http://www.lagazettedescommunes.com/341918/les-chief-data-officers-in-data-they-trust
tags:
- Entreprise
- Administration
- Associations
- Innovation
- Open Data
- Vie privée
---

> Le think tank de Vinci La fabrique de la cité a organisé le 23 mars une riche matinée d’échanges avec des CDO américains, ces nouvelles fonctions chargées d’utiliser les données pour optimiser le fonctionnement des villes. Un discours très enthousiaste, trop peut-être.
