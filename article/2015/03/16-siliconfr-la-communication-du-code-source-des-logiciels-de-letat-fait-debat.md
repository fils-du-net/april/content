---
site: Silicon.fr
title: "La communication du code source des logiciels de l'Etat fait débat"
author: Ariane Beky
date: 2015-03-16
href: http://www.silicon.fr/cada-administration-communiquer-code-source-logiciel-avocat-110885.html
tags:
- Administration
- Institutions
- Droit d'auteur
---

> D’après la Commission d’accès aux documents administratifs (Cada), le code source d’un logiciel produit par l’administration est un document communicable à toute personne qui le demande. Un avis basé sur une interprétation «audacieuse» de la loi, selon l’avocat François Coupez.
