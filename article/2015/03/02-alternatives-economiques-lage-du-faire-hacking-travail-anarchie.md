---
site: "Alternatives-economiques"
title: "L'âge du faire. Hacking, travail, anarchie"
author: Denis Clerc
date: 2015-03-02
href: http://www.alternatives-economiques.fr/l-age-du-faire--hacking--travail--a_fr_art_1351_71937.html
tags:
- Entreprise
- Partage du savoir
- Innovation
- International
---

> C'est un travail passionnant que nous livre l'auteur, professeur de sociologie au Cnam, après un séjour d'un an dans la baie de San Francisco, entre la ville elle-même et la célèbre Silicon Valley. Pour y observer non pas les réussites célèbres de start-up ou de personnes aux noms connus de tous. Mais plutôt le chaudron qui, souvent, leur a donné naissance et qui en est, d'une certaine manière, à la fois la condition et l'antithèse.
