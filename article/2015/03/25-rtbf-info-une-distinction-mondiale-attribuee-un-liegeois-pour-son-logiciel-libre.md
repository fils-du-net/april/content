---
site: RTBF Info
title: "Une distinction mondiale attribuée à un Liégeois pour son logiciel libre"
author: Perrine Willamme
date: 2015-03-25
href: http://www.rtbf.be/info/societe/detail_la-plus-prestigieuse-distinction-mondiale-attribuee-a-un-liegeois-pour-son-logiciel-libre?id=8940485
tags:
- Administration
- Interopérabilité
- Innovation
- Promotion
---

> Un Belge vient de recevoir la plus prestigieuse distinction en matière de logiciels libres. Sébastien Jodogne, ingénieur en informatique au Centre hospitalier universitaire (CHU) de Liège, a conçu un logiciel libre d'échange d'imageries médicales, nommé Orthanc. C'est un logiciel libre et open-source, c'est-à-dire accessible librement par téléchargement. En service au CHU depuis 3 ans, il est également déjà utilisé par de nombreux hôpitaux dans le monde.
