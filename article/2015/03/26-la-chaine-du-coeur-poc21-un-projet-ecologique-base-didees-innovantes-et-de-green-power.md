---
site: La Chaîne du Cœur
title: "POC21: un projet écologique à base d'idées innovantes et de green power!"
date: 2015-03-26
href: http://www.lachaineducoeur.fr/edition/actu/voir/poc21-un-projet-ecologique-a-base-d-idees-innovantes-et-de-green-power-2572
tags:
- Économie
- Sensibilisation
- Associations
- Innovation
- Sciences
---

> Une centaines d'experts dans de multiples domaines tels que l'énergie,a construction, l'eau, l'agriculture, les déchets ou encore la fabrication numérique, vont accompagner les 50 participants au projet pour élaborer le prototype d'une expérience unique et innovante: créer un village écologique 100% open source dans les Yvelines!
