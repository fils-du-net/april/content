---
site: Numerama
title: "Droit d'auteur: la députée pirate organise une soirée questions-réponses"
author: Julien L.
date: 2015-03-14
href: http://www.numerama.com/magazine/32488-droit-d-auteur-la-deputee-pirate-organise-une-soiree-questions-reponses.html
tags:
- Institutions
- Droit d'auteur
- Europe
---

> Julia Reda organisera une session de questions / réponses lundi 16 mars sur Mumble, afin de parler de son évaluation de la directive sur le droit d’auteur et les droits voisins de 2001.
