---
site: Les Echos
title: "Julia Reda, une «pirate» à Bruxelles, Enquête"
author: Julien Dupont-Calbo
date: 2015-03-27
href: http://www.lesechos.fr/journal20150327/lec1_enquete/0204242116237-julia-reda-une-pirate-a-bruxelles-1105848.php
tags:
- Entreprise
- Internet
- Institutions
- DRM
- Droit d'auteur
- Europe
---

> La jeune Allemande de vingt-huit ans, seule élue du Parti pirate au Parlement européen, est chargée d’un rapport d’évaluation sur le droit d’auteur en Europe. Réformateur, son texte a mis vent debout le milieu culturel parisien, qui s’active en coulisses pour faire valoir ses vues. Mais la «corsaire» est coriace…
