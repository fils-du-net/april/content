---
site: Next INpact
title: "Un ebook n'est pas toujours un vrai livre, rappelle l'April"
author: Marc Rees
date: 2015-03-06
href: http://www.nextinpact.com/news/93365-un-ebook-nest-pas-toujours-vrai-livre-rappelle-april.htm
tags:
- Économie
- April
- Institutions
- DRM
- Europe
---

> La Cour de justice de l’Union européenne a estimé hier que la France ne pouvait pas appliquer aux ebooks le même taux de TVA que les livres papier (5,5 %). Le premier est juridiquement considéré comme une prestation de service, le second, un bien physique. La décision a provoqué un concert de réprobations, tous estimant qu'il s'agit d'une atteinte à la neutralité technologique. Tous... sauf d'irréductibles libristes qui tiennent à rappeler quelques fondamentaux.
