---
site: LaLibre.be
title: "Sébastien Jodogne: \"Le logiciel libre est au cœur de notre économie numérique\""
author: Marc Bechet et P.-F.L.
date: 2015-03-23
href: http://www.lalibre.be/actu/sciences-sante/sebastien-jodogne-le-logiciel-libre-est-au-cur-de-notre-economie-numerique-550fea8a3570c8b952c70d93
tags:
- Administration
- Interopérabilité
- Innovation
- Promotion
- Sciences
---

> Orthanc? Les fans du "Seigneur des anneaux" se souviendront peut-être que c’est le nom donné à la tour du sorcier Saroumane… Alors, certes, Sébastien Jodogne, passionné par le récit et l’univers de Tolkien, s’en est inspiré quand il a fallu donner un nom à son… logiciel d’imagerie médicale (IRM, PET-scans, etc.). Orthanc est en est effet le nom d’un logiciel informatique destiné au milieu hospitalier. Un logiciel libre, "open source", pour être précis.
