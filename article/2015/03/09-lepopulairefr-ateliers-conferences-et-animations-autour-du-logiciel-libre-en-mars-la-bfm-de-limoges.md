---
site: lepopulaire.fr
title: "Ateliers, conférences et animations autour du logiciel libre, en mars, à la BFM de Limoges"
author: Gilles Deville
date: 2015-03-09
href: http://www.lepopulaire.fr/limousin/actualite/2015/03/09/ateliers-conferences-et-animations-autour-du-logiciel-libre-en-mars-a-la-bfm-de-limoges_11356436.html
tags:
- Partage du savoir
- Promotion
---

> À partir de mardi et jusqu'au samedi 28 mars, l'atelier multimédia de la BFM centre-ville organise la 10e édition du mois du logiciel libre.
