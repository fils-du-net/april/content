---
site: Next INpact
title: "Islamic-news.info bloqué sans juge, pour apologie ou provocation au terrorisme"
author: Marc Rees
date: 2015-03-16
href: http://www.nextinpact.com/news/93457-islamic-news-info-bloque-sans-juge-pour-apologie-ou-provocation-au-terrorisme.htm
tags:
- Internet
- Institutions
---

> Le site islamic-news.info est parmi les premiers bloqués par le ministère de l’Intérieur, comme l’autorise désormais la loi sur le terrorisme. Une tentative de visite se solde désormais par un message de la Place Beauvau arborant une grande main rouge pour imager ce blocage administratif, sans juge.
