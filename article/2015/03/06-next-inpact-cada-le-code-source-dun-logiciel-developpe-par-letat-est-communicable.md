---
site: Next INpact
title: "CADA: le code source d'un logiciel développé par l'État est communicable!"
author: Marc Rees
date: 2015-03-06
href: http://www.nextinpact.com/news/93369-cada-code-source-d-un-logiciel-developpe-par-l-etat-est-communicable.htm
tags:
- Administration
- April
- Partage du savoir
- Institutions
---

> La commission d’accès aux documents administratifs est une autorité bien pratique pour les citoyens, et notamment les journalistes. Elle permet d’avoir connaissance des documents qui tapissent les tiroirs des administrations, mais qui intéressent pourtant bon nombre d’administrés. L’un d’eux a tenté l’expérience avec une cible bien originale: le code source des logiciels utilisés par les autorités publiques.
