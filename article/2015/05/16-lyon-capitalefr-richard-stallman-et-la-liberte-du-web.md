---
site: Lyon Capitale.fr
title: "Richard Stallman et la liberté du web"
author: Florent Deligia
date: 2015-05-16
href: http://www.lyoncapitale.fr/Journal/Communs/Univers/Technologies/High-tech/Richard-Stallman-et-la-liberte-du-web
tags:
- Entreprise
- Philosophie GNU
- Promotion
- Vie privée
---

> On l'appelle aussi rms. Richard Stallman, créateur du système d'exploitation GNU/Linux et défenseur de la protection des données numériques, était l'invité du "Gourou Talk" jeudi 14 mai, à l'European Lab. Retour sur une conférence menée par un programmeur libre.
