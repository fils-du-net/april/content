---
site: Les Echos
title: "La Russie veut s’affranchir d’Apple et Google en lançant son propre OS"
author: Nicolas Richaud
date: 2015-05-19
href: http://www.lesechos.fr/tech-medias/hightech/02174881979-la-russie-veut-saffranchir-de-google-et-apple-en-lancant-son-propre-os-1120622.php
tags:
- Entreprise
- Institutions
- International
---

> Le ministre de la communication a annoncé que son pays s’attelait à la création de son propre système d’exploitation mobile en vue de s’émanciper des technologies occidentales. Et aimerait y associer des groupes installés dans les autres pays des BRICS.
