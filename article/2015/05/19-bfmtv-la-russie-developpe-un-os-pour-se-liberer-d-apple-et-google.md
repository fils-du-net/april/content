---
site: BFMtv
title: "La Russie développe un OS pour se libérer d’Apple et Google"
author: Pascal Samama
date: 2015-05-19
href: http://bfmbusiness.bfmtv.com/entreprise/la-russie-developpe-un-os-pour-se-liberer-d-apple-et-google-888144.html
tags:
- Entreprise
- Institutions
- DRM
- International
---

> Depuis l'affaire Snowden, Vladimir Poutine cherche à réduire la présence technologique américaine en Russie. Après avoir créé un navigateur et un réseau social, la Russie prépare un OS mobile avec des anciens de Nokia.
