---
site: Libération.fr
title: "Miracles et mirages du «crowdsourcing»"
author: Karën Fort
date: 2015-05-07
href: http://www.liberation.fr/societe/2015/05/07/miracles-et-mirages-du-crowdsourcing_1297262
tags:
- Entreprise
- Internet
- Économie
---

> La plateforme d’Amazon permettrait de faire réaliser des tâches pour très peu cher, avec une bonne qualité de résultat, par des gens pour lesquels il s’agit d’un hobby. La réalité est plus proche de la mine de charbon que de la mine d’or.
