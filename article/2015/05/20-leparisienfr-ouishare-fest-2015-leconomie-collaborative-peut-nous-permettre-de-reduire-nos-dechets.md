---
site: leParisien.fr
title: "Ouishare Fest 2015: «L'économie collaborative peut nous permettre de réduire nos déchets»"
author: Émilie Laystary
date: 2015-05-20
href: http://www.leparisien.fr/environnement/conso/ouishare-fest-2015-l-economie-collaborative-peut-nous-permettre-de-reduire-nos-dechets-20-05-2015-4787249.php
tags:
- Économie
- Innovation
---

> En ce moment à Paris se tient le festival de l'économie collaborative. Pour Diana Filippova, connector à Ouishare, il faut à tout prix remettre l'humain au cœur de nos façons de consommer.
