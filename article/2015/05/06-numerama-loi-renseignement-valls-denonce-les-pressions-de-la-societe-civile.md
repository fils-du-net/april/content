---
site: Numerama
title: "Loi Renseignement: Valls dénonce les \"pressions\" de la société civile"
author: Guillaume Champeau
date: 2015-05-06
href: http://www.numerama.com/magazine/33012-loi-renseignement-valls-denonce-les-pressions-de-la-societe-civile.html
tags:
- Internet
- HADOPI
- Institutions
- Associations
---

> Pour Manuel Valls, les nombreuses associations, personnalités et simples citoyens qui ont appelé les députés à voter contre le projet de loi Renseignement ont mis une "pression" à laquelle "heureusement peu de députés ont été sensibles". Ils ont succombé à une autre pression: celle du Premier ministre.
