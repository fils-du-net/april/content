---
site: Le Huffington Post
title: "Pour un nouveau manuel scolaire"
author: Émilie Blanchard
date: 2015-05-11
href: http://www.huffingtonpost.fr/emilie-blanchard/manuel-scolaire-numerique-ecole_b_7254378.html
tags:
- Partage du savoir
- Éducation
- Contenus libres
---

> Notre objet n'est pas ici de nous positionner pour ou contre cette réforme, mais d'apporter un éclairage supplémentaire en faisant entendre notre voix sur la nécessaire (r)évolution du manuel scolaire.
