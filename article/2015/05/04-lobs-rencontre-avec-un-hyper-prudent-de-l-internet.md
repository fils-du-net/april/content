---
site: L'OBS
title: "Rencontre avec un «hyper prudent» de l’Internet"
author: Rémi Noyon
date: 2015-05-04
href: http://rue89.nouvelobs.com/2015/06/04/rencontre-hyper-prudent-linternet-259548
tags:
- Internet
- Vie privée
---

> Slate a rencontré un flippé du Net avec de bonnes raisons de l’être. Aeris – c’est un pseudo – est ingénieur en développement logiciel. Il participe au projet NSA Observer, qui trie les documents dévoilés par Edward Snowden. A ce titre, il tente de déjouer la surveillance, commerciale ou étatique.
