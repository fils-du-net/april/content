---
site: L'OBS
title: "Loi renseignement: et si le Sénat nous sauvait..."
author: Andréa Fradin
date: 2015-05-20
href: http://rue89.nouvelobs.com/2015/05/20/loi-renseignement-si-senat-sauvait-259286
tags:
- Internet
- Institutions
- Vie privée
---

> On le dit dépassé, désuet, voire bon à jeter. «Un bel objet inutile.» Et pourtant, sur certains textes, le Sénat semble parfois plus éveillé que l’Assemblée. A demander des précisions au gouvernement. A avouer sans fard sa compétence limitée sur quelques points plus précis.
