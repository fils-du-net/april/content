---
site: Numerama
title: "Jean Zay, un défenseur du domaine public au Panthéon"
author: Guillaume Champeau
date: 2015-05-29
href: http://www.numerama.com/magazine/33228-jean-zay-un-defenseur-du-domaine-public-au-pantheon.html
tags:
- Partage du savoir
- Institutions
- Droit d'auteur
---

> Entré au Panthéon ce mercredi pour ses faits de résistance pendant la seconde guerre mondiale, Jean Zay avait proposé, lorsqu'il était ministre de l'éducation et des beaux arts, une loi sur le droit d'auteur qui aurait permis d'enrichir plus rapidement le domaine public et les auteurs, au seul détriment des éditeurs. La guerre aura enterré son idée.
