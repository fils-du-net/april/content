---
site: Le Monde.fr
title: "Adblock Plus remporte une victoire judiciaire en Allemagne"
author: Cécile Boutelet
date: 2015-05-28
href: http://www.lemonde.fr/pixels/article/2015/05/28/adblock-plus-remporte-une-victoire-judiciaire-en-allemagne_4642162_4408996.html
tags:
- Entreprise
- Internet
- Économie
- Institutions
- International
---

> Un tribunal de Munich a jugé que le bloqueur de publicité n’était pas une entrave à la concurrence.
