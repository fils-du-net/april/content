---
site: LesAffaires.com
title: "Logiciel libre (dossier de six articles)"
author: Jean-François Venne
date: 2015-05-16
href: http://www.lesaffaires.com/dossier/logiciel-libre
tags:
- Entreprise
- Économie
- Sensibilisation
- Marchés publics
- Promotion
- International
---

> L'industrie du logiciel libre (ou open source) connaît une forte croissance dans le monde. «Ces logiciels sont omniprésents dans les infrastructures Web et la mobilité, et dans les logiciels embarqués, c'est-à-dire des logiciels présents dans les voitures, avions ou électroménagers», explique Stéphane Couture, chercheur en sociologie et communication numérique à l'Université McGill.
