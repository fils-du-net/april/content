---
site: Slate.fr
title: "Largement adoptée par l'Assemblée, la loi sur le renseignement a divisé le PS comme l'UMP"
author: Grégor Brandy
date: 2015-05-05
href: http://www.slate.fr/story/101233/projet-loi-renseignement-vote
tags:
- Internet
- HADOPI
- Institutions
- DADVSI
- DRM
---

> Si une majorité des députés, dans la majorité comme dans l'opposition, ont voté le texte du gouvernement, 86 s'y sont opposés et 42 se sont abstenus.
