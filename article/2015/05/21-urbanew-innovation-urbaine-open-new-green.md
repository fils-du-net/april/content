---
site: urbanew
title: "Innovation urbaine: Open is the new Green!"
author: Bruno Morleo 
date: 2015-05-21
href: http://www.urbanews.fr/2015/05/21/48379-innovation-urbaine-open-is-the-new-green
tags:
- Entreprise
- Économie
- Innovation
- Open Data
---

> La culture collaborative et le jeu d’acteurs prennent de plus en plus de place dans les processus de conception urbaine. Une approche plus ouverte et dynamique qui permet de mieux prendre en compte l’évolution des modes de vie, des usages, de l’histoire, de l’héritage et de tout patrimoine immatériel.
