---
site: 01net.
title: "La Russie veut bouter Android et iOS hors de ses frontières"
author: Gilbert Kallenborn
date: 2015-05-19
href: http://www.01net.com/editorial/654925/la-russie-veut-bouter-android-et-ios-hors-de-ses-frontieres
tags:
- Entreprise
- Institutions
- International
---

> Le gouvernement Poutine veut créer un système d’exploitation mobile 100 % russe, en s’appuyant sur le logiciel open source Sailfish OS, créé par l’éditeur finlandais Jolla. Objectif: réduire la dépendance aux technologies américaines.
