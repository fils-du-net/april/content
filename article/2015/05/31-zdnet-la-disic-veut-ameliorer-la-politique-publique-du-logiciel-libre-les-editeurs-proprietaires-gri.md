---
site: ZDNet
title: "La Disic veut améliorer la politique publique du logiciel libre, les éditeurs propriétaires grincent"
author: Thierry Noisette
date: 2015-05-31
href: http://www.zdnet.fr/actualites/la-disic-veut-ameliorer-la-politique-publique-du-logiciel-libre-les-editeurs-proprietaires-grincent-39820036.htm
tags:
- Entreprise
- Administration
- Économie
- Interopérabilité
- April
- Marchés publics
- RGI
---

> La Direction interministérielle des SI de l’État souhaite avancer dans la voie tracée depuis 2012, entre autres en permettant à des agents de contribuer à du logiciel libre sur leur temps de travail. Chez les éditeurs de logiciels propriétaires, on "s'insurge".
