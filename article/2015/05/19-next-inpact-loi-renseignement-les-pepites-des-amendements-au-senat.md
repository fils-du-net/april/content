---
site: Next INpact
title: "Loi Renseignement: les pépites des amendements au Sénat"
author: Marc Rees
date: 2015-05-19
href: http://www.nextinpact.com/news/94123-loi-renseignement-pepites-amendements-au-senat.htm
tags:
- Internet
- Institutions
- Vie privée
---

> C’est demain que la Commission des lois du Sénat arbitrera l’ensemble des amendements déposés sur le projet de loi Renseignement. Petit tour d’horizon de quelques-unes des propositions trouvées dans la liasse des 257 pages que nous avons décortiquées.
