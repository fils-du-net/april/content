---
site: Next INpact
title: "Le discret contrat de 11 millions d'euros entre l'État et Microsoft Irlande"
author: Xavier Berne
date: 2015-05-22
href: http://www.nextinpact.com/news/95175-le-discret-contrat-11-millions-d-euros-entre-l-etat-et-microsoft-irlande.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- April
- Marchés publics
---

> Les ministères du Travail, de la Santé et de la Jeunesse et des sports ont conclu l’année dernière avec Microsoft Irlande – et dans la plus grande discrétion – un important marché de plus de 11 millions d’euros. Si son contenu exact demeure encore extrêmement flou, il ressemble de très près au tristement célèbre contrat «Open Bar» signé par le ministère de la Défense, et tant décrié par les militants du logiciel libre.
