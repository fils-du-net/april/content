---
site: AlterInfos
title: "«La technologie ne peut se concevoir isolée de la politique»"
author: Rafael Rico Ríos
date: 2015-05-20
href: http://www.alterinfos.org/spip.php?article6841
tags:
- Institutions
- Licenses
- International
---

> Carlos Eduardo Parra Falcón est chef des opérations du projet Canaima au Centre national des technologies de l’information (CNTI), entité dépendant du ministère du pouvoir populaire pour la science, la technologie et l’innovation de la République bolivarienne du Venezuela.
