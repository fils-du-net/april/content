---
site: Developpez.com
title: "Open source: la société française Mandriva ferme ses portes, après 16 ans consacrés à la promotion de Linux sur le Desktop"
author: Hinault Romaric
date: 2015-05-27
href: http://www.developpez.com/actu/85789/Open-source-la-societe-francaise-Mandriva-ferme-ses-portes-apres-16-ans-consacres-a-la-promotion-de-Linux-sur-le-Desktop
tags:
- Entreprise
---

> Mandriva, société française qui a participé à l’écriture des belles pages de l’open source, notamment avec sa distribution Linux, vient de mettre la clé sous la porte. Sur la fiche de l’entreprise sur le site societe.com, on peut lire que Mandriva est en cours de liquidation.
