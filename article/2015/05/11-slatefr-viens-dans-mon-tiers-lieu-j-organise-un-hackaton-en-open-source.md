---
site: Slate.fr
title: "Viens dans mon tiers-lieu, j’organise un hackaton en open source"
author: Jean-Laurent Cassely
date: 2015-05-11
href: http://www.slate.fr/story/100525/paye-ton-fablab
tags:
- Économie
- Matériel libre
- Innovation
---

> L’Open model, le peer to peer et les fablabs expliqués comme au bistrot. Ou les questions les plus bêtes que vous n’osez pas poser à un «maker» qui bosse dans un «fablab collaboratif» en «open source».
