---
site: Les Echos
title: "Informatique: les éditeurs américains mis à l’écart par l’administration"
author: Sandrine Cassini
date: 2015-05-28
href: http://www.lesechos.fr/tech-medias/hightech/02197291550-les-editeurs-americains-mis-a-lecart-par-ladministration-1123244.php
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- April
- Marchés publics
- RGI
---

> Plusieurs démarches de l’administration excluent les éditeurs étrangers de logiciels, en particulier Microsoft.
