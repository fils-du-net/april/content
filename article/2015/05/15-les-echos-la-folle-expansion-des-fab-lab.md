---
site: Les Echos
title: "La folle expansion des Fab Lab"
author: Laurent Marcaillou
date: 2015-05-15
href: http://www.lesechos.fr/idees-debats/sciences-prospective/02167411389-la-folle-expansion-des-fab-lab-1119454.php
tags:
- Économie
- Partage du savoir
- Matériel libre
- Innovation
---

> Les ateliers de fabrication numérique se multiplient sur tout le territoire sous l'impulsion des élus et des passionnés. 37 Fab Lab français ont décidé de se fédérer pour valoriser leur action et créer une labellisation.
