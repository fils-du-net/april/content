---
site: ZDNet France
title: "\"Alerte enlèvement\" - Mandriva a disparu..."
author: Louis Adam
date: 2015-05-27
href: http://www.zdnet.fr/actualites/alerte-enlevement-mandriva-a-disparu-39819846.htm
tags:
- Entreprise
- Économie
---

> La société française Mandriva, qui éditait la distribution Linux éponyme, serait en train de fermer ses portes. Le site est actuellement hors ligne et selon societe.com, l’entreprise est en liquidation.
