---
site: L'Informaticien
title: "Loi Renseignement: dystopie gouvernementale"
author: Bertrand Garé
date: 2015-05-04
href: http://www.linformaticien.com/actualites/id/36542/loi-renseignement-dystopie-gouvernementale.aspx
tags:
- Internet
- Institutions
---

> Le projet de loi sur le renseignement alimente un débat autour de l’utilisation d’Internet et voit se confronter deux visions antinomiques du réseau des réseaux. L’une, quasi angélique, parfois déconnectée de la réalité. L’autre, vision de contrôle chère au jacobinisme francais de nos gouvernements, tout aussi irréaliste comme l’a démontré le pseudo blocage de T411.
