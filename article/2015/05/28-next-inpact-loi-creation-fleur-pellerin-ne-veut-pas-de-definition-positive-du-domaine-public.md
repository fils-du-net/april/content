---
site: Next INpact
title: "Loi Création: Fleur Pellerin ne veut pas de définition positive du domaine public"
author: Marc Rees
date: 2015-05-28
href: http://www.nextinpact.com/news/95230-loi-creation-fleur-pellerin-ne-veut-pas-definition-positive-domaine-public.htm
tags:
- Institutions
- Droit d'auteur
---

> À deux doigts près ! Aurélie Filippetti était bien prête à clarifier et mieux protéger les œuvres du domaine public. Cependant, Fleur Pellerin a décidé de gommer les dispositions prévues par sa prédécesseure, dans le projet de loi Création qu’elle porte désormais.
