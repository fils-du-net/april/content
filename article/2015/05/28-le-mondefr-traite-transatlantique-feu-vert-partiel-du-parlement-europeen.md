---
site: Le Monde.fr
title: "Traité transatlantique: feu vert partiel du Parlement européen"
author: Cécile Ducourtieux
date: 2015-05-28
href: http://www.lemonde.fr/economie/article/2015/05/28/traite-transatlantique-feu-vert-partiel-du-parlement-europeen_4642813_3234.html
tags:
- Entreprise
- Économie
- Institutions
- Europe
- International
- ACTA
---

> L'accord de libre-échange en cours de négociation entre Européens et Américains a été approuvé en commission. Un dixième round est prévu dans le courant du mois de juillet.
