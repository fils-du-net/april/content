---
site: Next INpact
title: "Le fisc rechigne à révéler le code source de ses logiciels"
author: Marc Rees
date: 2015-05-25
href: http://www.nextinpact.com/news/95159-le-fisc-rechigne-a-reveler-code-source-ses-logiciels.htm
tags:
- Administration
- Économie
- April
- Open Data
---

> En mars dernier, la Commission d’accès aux documents administratifs (CADA) estimait que le code source du logiciel servant au calcul de l’impôt sur le revenu était communicable aux citoyens. Sacrée nouvelle! Cependant, trois mois plus tard, cette libération a visiblement beaucoup de mal à percer les murs épais de Bercy.
