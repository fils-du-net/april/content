---
site: Le Point
title: "\"Les citoyens vont révolutionner le système économique\""
author: Yoann Duval
date: 2015-05-13
href: http://www.lepoint.fr/high-tech-internet/les-citoyens-vont-revolutionner-le-systeme-economique-13-05-2015-1928132_47.php
tags:
- Innovation
---

> Il n'a pas l'air à le voir, mais Neil Gershenfeld est le porte-voix d'un mouvement planétaire en pleine ébullition. Pour preuve, le week-end dernier, à Toulouse, se sont réunis plus de 40 FabLabs européens à l'invitation d'Artilect, le plus grand FabLab de France. Pour son fondateur, Nicolas Lassabe, "Neil est celui qui a vu juste et qui a permis au FabLab Festival d'exister avec peu de moyens. Grâce aux équipes et aux bénévoles, plus de 5 000 spectateurs se sont déplacés pour découvrir nos 50 makers (les exposants, NDLR)».
