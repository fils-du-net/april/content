---
site: Le Monde.fr
title: "Réforme du droit d'auteur, piratage...: Bruxelles dévoile son «agenda numérique»"
author: Cécile Ducourtieux
date: 2015-05-06
href: http://www.lemonde.fr/economie/article/2015/05/06/bruxelles-devoile-son-agenda-numerique_4628649_3234.html
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> La Commission européenne envisage de réformer le droit d’auteur et de renforcer la lutte contre le piratage. Elle pose la question de la régulation, ou non des plates-formes Internet (Amazon, Apple, Facebook ou Google).
