---
site: "usine-digitale.fr"
title: "OuiShare Fest: Le peer-to-peer peut-il vraiment sauver le monde?"
author: Aurélie Barbaux
date: 2015-05-20
href: http://www.usine-digitale.fr/editorial/ouishare-fest-le-peer-to-peer-peut-il-vraiment-sauver-le-monde.N330590
tags:
- Internet
- Économie
- Innovation
---

> Le peer-to-peer ne se limite pas à l’échange de fichiers entre ordinateurs. Il permet à des individus de créer, entre égaux, de la valeur, c’est-à-dire des biens communs, immatériels mais aussi matériels. Un modèle vertueux, qui pourrait effectivement s’imposer dans une économie post-capitaliste, à condition néanmoins de régler la question de la rémunération des contributions. Mais de là à "Sauver le monde", comme le promet le titre du livre de Michel Bauwens, fondateur de la Fondation P2P… À voir.
