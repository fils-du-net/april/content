---
site: Le Figaro
title: "La Cada est favorable à l'accès aux codes sources d'un logiciel de l'Etat"
author: Alain Bensoussan
date: 2015-05-13
href: http://blog.lefigaro.fr/bensoussan/2015/05/la-cada-est-favorable-a-lacces-aux-codes-sources-dun-logiciel-de-letat.html
tags:
- Administration
- April
- Contenus libres
---

> La loi du 17 juillet 1978 reconnaît à toute personne le droit d'obtenir communication des documents détenus dans le cadre de sa mission de service public par une administration, quel que soit leur forme ou leur support.
