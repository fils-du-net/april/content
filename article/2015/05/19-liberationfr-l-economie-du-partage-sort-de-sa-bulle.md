---
site: Libération.fr
title: "L’économie du partage sort de sa bulle"
author: Christophe Alix
date: 2015-05-19
href: http://www.liberation.fr/economie/2015/05/19/l-economie-du-partage-sort-de-sa-bulle_1312709
tags:
- Entreprise
- Administration
- Économie
- Innovation
- Sciences
---

> Au-delà de Uber et Airbnb, le mouvement collaboratif consolide son modèle et commence à irriguer de nouveaux secteurs. Revue de tendances.
