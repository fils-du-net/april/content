---
site: L'Opinion
title: "Le brevet unitaire européen est conforté"
author: Hugo Sedouramane
date: 2015-05-04
href: http://www.lopinion.fr/4-mai-2015/brevet-unitaire-europeen-est-conforte-mise-a-jour-23923
tags:
- Institutions
- Brevets logiciels
- Europe
---

> La Cour de justice de l'Union européenne a débouté l'Espagne qui s'opposait aux processus juridiques définissant le déploiement du brevet à effet unitaire
