---
site: Les Echos
title: "L’économie du partage peut-elle séduire l’entreprise?"
author: Benoit Georges
date: 2015-05-18
href: http://www.lesechos.fr/idees-debats/editos-analyses/02175118024-leconomie-du-partage-peut-elle-seduire-lentreprise-1120399.php
tags:
- Entreprise
- Économie
- Innovation
---

> Après avoir conquis les particuliers, les plates-formes de partage commencent à cibler le monde de l’entreprise. Mais, pour trouver le succès, elles devront vaincre de nombreuses réticences.
