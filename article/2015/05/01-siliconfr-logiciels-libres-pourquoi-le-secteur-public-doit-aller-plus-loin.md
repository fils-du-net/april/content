---
site: Silicon.fr
title: "Logiciels libres: pourquoi le secteur public doit aller plus loin"
author: Jacques Marzin
date: 2015-05-01
href: http://www.silicon.fr/logiciels-libres-secteur-public-aller-plus-loin-115810.html
tags:
- Administration
- Interopérabilité
- RGI
---

> Le Socle Interministériel des Logiciels Libres (SILL), dont la v2 a été publiée en février, contribue à une meilleure rationalisation du SI de l’Etat. Sa diffusion doit toutefois être étendue. D’où les réflexions en cours concernant une gouvernance interministérielle renforcée autour du libre, explique le DSI de l’Etat, Jacques Marzin.
