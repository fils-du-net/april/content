---
site: Next INpact
title: "Loi Création: la transparence des flux Copie Privée a sauté"
author: Marc Rees
date: 2015-05-28
href: http://www.nextinpact.com/news/95229-loi-creation-transparence-flux-copie-privee-a-saute.htm
tags:
- Internet
- Institutions
- Droit d'auteur
- Open Data
---

> i relative à la liberté de la création, à l'architecture et au patrimoine (PDF) a été révélé le 18 mai dernier par le Conseil économique et social. Nous reviendrons en détail sur certaines de ses dispositions, mais l’une manque déjà à l’appel: celle relative à la transparence de l’utilisation de la redevance pour copie privée. Explications.
