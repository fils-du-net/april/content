---
site: ActuaLitté.com
title: "Le DRM, la mauvaise réponse au partage simplifié d'un ebook"
author: Nicolas Gary
date: 2015-05-06
href: https://www.actualitte.com/usages/le-drm-la-mauvaise-reponse-au-partage-simplifie-d-un-ebook-56625.htm
tags:
- Entreprise
- April
- DRM
- Droit d'auteur
---

> Aujourd'hui, mercredi 6 mai, est une journée consacrée, à travers le monde, à la lutte contre les DRM. Cette mesure technique de protection est «une mauvaise réponse apportée» à «la facilité de partager les ebooks». Plutôt que de se lancer dans une explication du comment neutraliser ces MTP, l'April vient de diffuser une vidéo éloquente, sur les ravages qu'un DRM occasionne dans la pratique pourtant innocente de la lecture.
