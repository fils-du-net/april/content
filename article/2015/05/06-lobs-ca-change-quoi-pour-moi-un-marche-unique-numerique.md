---
site: L'OBS
title: "Ça change quoi, pour moi, un marché unique numérique?"
author: Delphine Cuny
date: 2015-05-06
href: http://rue89.nouvelobs.com/2015/05/06/ca-change-quoi-marche-unique-numerique-259050
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Droit d'auteur
- Europe
---

> Bruxelles dit explicitement vouloir «en finir avec le blocage géographique», une pratique discriminatoire «injustifiée» qui permet «à des vendeurs en ligne d’empêcher les consommateurs d’accéder à un site internet sur la base de leur localisation» ou de simplement les rediriger vers le site de leur pays aux prix, comme par hasard, différents.
