---
site: Next INpact
title: "Le marché unique numérique européen prévu d'ici fin 2016"
author: Guénaël Pépin
date: 2015-05-06
href: http://www.nextinpact.com/news/94022-le-marche-unique-numerique-europeen-prevu-d-ici-fin-2016.htm
tags:
- Internet
- Économie
- Interopérabilité
- Institutions
- Droit d'auteur
- Neutralité du Net
- Europe
---

> La Commission européenne vient de présenter sa feuille de route pour un marché unique numérique, prévu pour la fin 2016. Le projet comprend de nombreuses réformes, notamment celle des télécoms, du droit d’auteur et des données personnelles, mais aussi des mesures visant à favoriser les entreprises européennes. Un objectif ambitieux, qui reste encore soumis aux débats et lobbyings.
