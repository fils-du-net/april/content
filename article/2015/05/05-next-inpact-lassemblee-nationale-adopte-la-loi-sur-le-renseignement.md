---
site: Next INpact
title: "L'Assemblée nationale adopte la loi sur le renseignement"
author: Xavier Berne
date: 2015-05-05
href: http://www.nextinpact.com/news/93997-la-loi-sur-renseignement-soumise-cet-apres-midi-au-vote-deputes.htm
tags:
- Internet
- Institutions
- Vie privée
---

> Mise à jour : Sans grande surprise, le projet de loi sur le renseignement a été adopté par l'Assemblée nationale, par 438 voix «pour», 86 «contre» et 42 abstentions. Le texte sera donc transmis au Sénat.
