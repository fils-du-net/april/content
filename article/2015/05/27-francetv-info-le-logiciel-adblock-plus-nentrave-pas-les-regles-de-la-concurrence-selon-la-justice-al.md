---
site: francetv info
title: "Le logiciel Adblock Plus n'entrave pas les règles de la concurrence, selon la justice allemande"
date: 2015-05-27
href: http://www.francetvinfo.fr/internet/le-logiciel-adblock-plus-n-entrave-pas-les-regles-de-la-concurrence-selon-la-justice-allemande_923945.html
tags:
- Entreprise
- Internet
- Institutions
- International
---

> Adblock n'enfreint pas la loi, selon la justice allemande. L'entreprise allemande éditrice du logiciel de blocage de publicités Adblock Plus a remporté, mercredi 27 mai, le procès que lui intentaient deux médias allemands, a indiqué le tribunal de Munich.
