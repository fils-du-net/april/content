---
site: L'avenir
title: "Windows, Mac OS, Android: des logiciels malveillants, tranche le pape du logiciel libre"
author: Georges Lekeu
date: 2015-05-26
href: http://www.lavenir.net/cnt/DMF20150526_00655078
tags:
- Entreprise
- Logiciels privateurs
- DRM
- Informatique-deloyale
- Vie privée
---

> Institutionnalisés par les plus grands, les «malwares» gouvernent notre vie numérique et bafouent nos droits, estime Richard Stallman, le père du système d’exploitation libre GNU.
