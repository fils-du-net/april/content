---
site: Le Monde.fr
title: "L’économie collaborative et le peer-to-peer vont-ils bouleverser le monde?"
author: Margherita Nasi
date: 2015-05-22
href: http://www.lemonde.fr/emploi/article/2015/05/22/l-economie-collaborative-et-le-peer-to-peer-vont-ils-bouleverser-le-monde_4638598_1698637.html
tags:
- Économie
- Innovation
---

> C'est un titre qui peut paraître ambitieux voire exagéré : dans son nouvel ouvrage, Michel Bauwens propose ni plus ni moins que de « Sauver le monde ». De quoi faire « ricaner les sceptiques de tout genre », reconnaît dans la préface le philosophe Bernard Stiegler. Pourtant, les thèses avancées par Michel Bauwens, théoricien de l’économie collaborative, s'inscrivent dans un horizon sombre : au cours des vingt prochaines années, l'automatisation pourrait provoquer le déclin de la société fondée sur le salariat, et nombre d'emplois risquent de disparaître.
