---
site: JDN
title: "Open Source ou Open Core: pourquoi s'en soucier?"
author: Ashesh Badani
date: 2015-05-20
href: http://www.journaldunet.com/solutions/expert/60997/open-source-ou-open-core---pourquoi-s-en-soucier.shtml
tags:
- Entreprise
- Économie
- Licenses
- Promotion
---

> Certains essayent d’appliquer un semblant d’Open Source à des business models et des modes de facturation d’un autre temps. Ils sont devenus des fournisseurs de logiciels «open core».
