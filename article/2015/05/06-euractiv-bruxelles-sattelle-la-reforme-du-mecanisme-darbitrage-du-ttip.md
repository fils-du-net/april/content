---
site: EurActiv
title: "Bruxelles s'attelle à la réforme du mécanisme d'arbitrage du TTIP"
date: 2015-05-06
href: http://www.euractiv.fr/sections/commerce-industrie/bruxelles-sattelle-la-reforme-du-mecanisme-darbitrage-du-ttip-314364
tags:
- Entreprise
- Institutions
- Europe
- International
- ACTA
---

> La commissaire européenne en charge du commerce, Cecilia Malmström, a rencontré son homologue américain aux États-Unis le 4 mai, afin de proposer une réforme du mécanisme de règlement des différends entre investisseurs et États (RDIE) dans le cadre du TTIP.
