---
site: La Tribune
title: "Cecilia Malmström: une \"infirmière\" au chevet du TTIP"
author: Florence Autret
date: 2015-05-12
href: http://www.latribune.fr/opinions/blogs/vu-de-bruxelles/cecilia-malmstrom-une-infirmiere-au-chevet-du-ttip-475734.html
tags:
- Économie
- Institutions
- Standards
- International
- ACTA
---

> «On peut dire merci à nos amis russes», expliquait la semaine dernière le lobbyiste d'un groupe industriel américain à Bruxelles au sujet du TTIP.
