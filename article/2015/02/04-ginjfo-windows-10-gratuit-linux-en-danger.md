---
site: GinjFo
title: "Windows 10 gratuit, Linux en danger?"
author: Jérôme Gianoli 
date: 2015-02-04
href: http://www.ginjfo.com/actualites/logiciels/windows-10-gratuit-linux-en-danger-20150203
tags:
- Logiciels privateurs
---

> L’annonce de Microsoft d’offrir Windows 10 peut-elle mettre en danger les distributions Linux pour PC?
