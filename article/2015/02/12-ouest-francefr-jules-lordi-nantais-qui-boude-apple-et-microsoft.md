---
site: "ouest-france.fr"
title: "Jules, l'ordi nantais qui boude Apple et Microsoft"
date: 2015-02-12
href: http://www.entreprises.ouest-france.fr/article/innovation-jules-lordi-nantais-qui-boude-apple-microsoft-12-02-2015-193162
tags:
- Logiciels privateurs
- Innovation
---

> Daniel Macaud aime l'informatique, mais n'aime pas les logiques commerciales qui s'y déploient: «On ne peut pas continuer à dire aux clients que leur machine, qui date à peine de quatre ou cinq ans, est trop vieille et qu'il faut tout changer pour racheter du neuf.»
