---
site: Le Journal de Québec
title: "Péquistes et caquistes veulent embaucher davantage d’informaticiens à meilleur salaire"
author: Charles Lecavalier
date: 2015-02-09
href: http://www.journaldequebec.com/2015/02/09/pequistes-et-caquistes-veulent-embaucher-davantage-dinformaticiens
tags:
- Économie
- Institutions
- International
---

> Le gouvernement du Québec doit embaucher davantage d’informaticiens et doit leur donner un meilleur salaire s’il veut éviter de payer une fortune aux consultants du secteur privé, estiment les partis d’oppositions.
