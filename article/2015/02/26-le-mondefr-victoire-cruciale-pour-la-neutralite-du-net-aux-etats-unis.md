---
site: Le Monde.fr
title: "Victoire cruciale pour la neutralité du Net aux Etats-Unis"
date: 2015-02-26
href: http://www.lemonde.fr/pixels/article/2015/02/26/etape-decisive-pour-la-neutralite-du-net-aux-etats-unis_4583490_4408996.html
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- International
---

> Le régulateur des télécommunications a annoncé après des années de débats de nouvelles règles concernant le traitement des données sur le Web.
