---
site: EurActiv
title: "La France fait marche arrière sur l'arbitrage dans le TTIP"
author: Aline Robert
date: 2015-02-26
href: http://www.euractiv.fr/sections/commerce-industrie/la-france-fait-marche-arriere-sur-larbitrage-dans-le-ttip-312427
tags:
- Économie
- Institutions
- International
- ACTA
---

> Dans une note transmise aux élus français, le SGAE appelle à ne pas fermer totalement la porte aux mécanismes de règlements des différends investisseurs-Etat.
