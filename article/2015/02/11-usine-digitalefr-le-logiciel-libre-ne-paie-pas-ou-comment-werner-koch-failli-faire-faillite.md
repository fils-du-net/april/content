---
site: "usine-digitale.fr"
title: "Le logiciel libre ne paie pas, ou comment Werner Koch a failli faire faillite"
author: Julien Bergounhoux
date: 2015-02-11
href: http://www.usine-digitale.fr/article/le-logiciel-libre-ne-paie-pas-ou-comment-werner-koch-a-failli-faire-faillite.N312344
tags:
- Vie privée
---

> En 1999 l'allemand Werner Koch sortait la première version de GnuPG pour permettre à tout un chacun de protéger ses données. Quinze ans plus tard, son logiciel de chiffrement est devenu une référence utilisée partout... mais lui a presque jeté l'éponge. La raison ? Un cruel manque de financement.
