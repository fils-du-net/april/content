---
site: L'Expansion
title: "Louis Pouzin: \"Internet est bâti sur un marécage\""
author: Emmanuel Paquette
date: 2015-02-15
href: http://lexpansion.lexpress.fr/high-tech/louis-pouzin-internet-est-bati-sur-un-marecage_1650342.html
tags:
- Internet
- Institutions
- Innovation
---

> Pionnier des réseaux, le président d'honneur de la Société française de l'Internet Louis Pouzin s'inquiète aujourd'hui de la volonté de contrôle des Etats à l'aune des derniers attentats et des poussées sécuritaires des gouvernants.
