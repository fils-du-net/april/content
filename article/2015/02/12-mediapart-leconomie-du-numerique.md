---
site: Mediapart
title: "L'Economie du Numérique"
author: Mike La Menace
date: 2015-02-12
href: http://blogs.mediapart.fr/blog/mike-la-menace/120215/leconomie-du-numerique
tags:
- Logiciels privateurs
- Économie
- Sensibilisation
- Vie privée
---

> Syriza a gagné, syriza a raison. Nous sommes pleins à avoir raison. Mais la force est supérieur. Elle imprègne le tissu social, les médias (pas tous ;-). Aussi, nous devons faire des "économies". Dans ma commune c'est une baisse programmée d'une dotation (la dsc) qui va décroître de 130.000€ pour arriver à 0 en 2017. Ça tombe bien en 2017 y a des élections je crois. Mais je m'égare.
