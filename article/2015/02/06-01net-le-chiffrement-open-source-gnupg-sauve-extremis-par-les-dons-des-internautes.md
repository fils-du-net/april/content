---
site: 01net.
title: "Le chiffrement open source GnuPG, sauvé in extremis par les dons des internautes"
author: Gilbert Kallenborn
date: 2015-02-06
href: http://www.01net.com/editorial/644468/le-chiffrement-open-source-gnupg-sauve-in-extremis-par-les-dons-des-internautes
tags:
- Internet
- Économie
- Innovation
- Vie privée
---

> Tenue à bout de bras par un seul développeur, cette technologie essentielle pour la protection des communications sur Internet risquait de tomber aux oubliettes.
