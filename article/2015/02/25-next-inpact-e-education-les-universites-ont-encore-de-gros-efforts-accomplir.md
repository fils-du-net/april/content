---
site: Next INpact
title: "E-éducation: les universités ont encore «de gros efforts à accomplir»"
author: Xavier Berne
date: 2015-02-25
href: http://www.nextinpact.com/news/93177-e-education-universites-ont-encore-gros-efforts-a-accomplir.htm
tags:
- Partage du savoir
- Institutions
- Éducation
- Contenus libres
- Europe
---

> Le Conseil économique, social et environnement (CESE) a adopté hier à l’unanimité un rapport sur la «pédagogie numérique» au sein des établissements de l’enseignement supérieur. Les auteurs de ces travaux estiment que la France a encore de gros progrès à effectuer.
