---
site: Le Point
title: "Tristan Nitot: \"Si on renonce à nos libertés, les terroristes ont gagné\""
date: 2015-02-03
href: http://www.lepoint.fr/chroniqueurs-du-point/guerric-poncet/tristan-nitot-quitte-mozilla-et-denonce-le-flicage-des-citoyens-03-02-2015-1901955_506.php
tags:
- Internet
- Vie privée
---

> Le patron de Mozilla en Europe va quitter son poste à la mi-février. Tristan Nitot, qui avait fondé la branche européenne de l'éditeur du navigateur libre Firefox avant d'en devenir un cadre mondial, souhaite se consacrer à l'écriture d'un livre sur la surveillance de masse dont sont victimes les citoyens, notamment en France. "Aujourd'hui, il y a une certaine résignation de la part des citoyens, qui pensent qu'ils ne peuvent rien faire, mais c'est faux, on peut commencer à faire quelque chose", nous a-t-il confié mardi matin par téléphone.
