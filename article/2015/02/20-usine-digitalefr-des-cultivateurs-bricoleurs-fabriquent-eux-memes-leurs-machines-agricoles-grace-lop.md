---
site: "usine-digitale.fr"
title: "Des cultivateurs-bricoleurs fabriquent eux-mêmes leurs machines agricoles grâce à l'open hardware"
author: Lélia de Matharel
date: 2015-02-20
href: http://www.usine-digitale.fr/article/des-cultivateurs-bricoleurs-fabriquent-eux-memes-leurs-machines-agricoles-grace-a-l-open-hardware.N314684
tags:
- Internet
- Économie
- Matériel libre
- Innovation
---

> L’open hardware, qui permet aux passionnés de technologie de construire eux-mêmes leurs robots, leurs smartphones ou leurs voitures, intéresse un nouveau public: les agriculteurs. Grâce à des plans accessibles librement sur Internet, ils peuvent fabriquer des machines agricoles moins chères que celles qui sont disponibles dans le commerce.
