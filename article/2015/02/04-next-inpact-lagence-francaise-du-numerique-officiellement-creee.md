---
site: Next INpact
title: "L'Agence française du numérique officiellement créée"
author: Xavier Berne
date: 2015-02-04
href: http://www.nextinpact.com/news/92963-l-agence-francaise-numerique-officiellement-creee.htm
tags:
- Internet
- Économie
- Institutions
- Open Data
---

> Le décret actant la création de l’Agence française du numérique a été publié ce matin au Journal officiel. Placée sous l’autorité de Bercy, cette nouvelle institution regroupera en fait trois structures existantes: la mission Très Haut Débit, la French Tech et la Délégation aux usages de l’internet.
