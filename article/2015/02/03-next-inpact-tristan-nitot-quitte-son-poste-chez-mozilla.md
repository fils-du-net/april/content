---
site: Next INpact
title: "Tristan Nitot quitte son poste chez Mozilla"
author: Bastien Gavois
date: 2015-02-03
href: http://www.nextinpact.com/news/92958-tristan-nitot-quitte-son-poste-chez-mozilla.htm
tags:
- Internet
- Vie privée
---

> Les défenseurs français de Firefox vont sans doute ressentir comme un petit pincement au cœur: Tristan Nitot vient d'annoncer son départ de Mozilla. Il souhaite se consacrer à l'écriture d'un livre, mais précise qu'il reste néanmoins bénévole dans la fondation au sein de laquelle il a déjà passé 17 ans.
