---
site: Silicon.fr
title: "Android: Microsoft et Samsung mettent fin à leur dispute"
author: David Feugey
date: 2015-02-10
href: http://www.silicon.fr/brevets-android-microsoft-samsung-fin-dispute-secret-107970.html
tags:
- Entreprise
- Brevets logiciels
---

> Un accord confidentiel a été signé entre Microsoft et Samsung au sujet d’Android. Encore une fois, Microsoft a évité la case justice.
