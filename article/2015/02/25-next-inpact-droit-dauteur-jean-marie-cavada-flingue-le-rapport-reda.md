---
site: Next INpact
title: "Droit d'auteur: Jean-Marie Cavada flingue le rapport Reda"
author: Marc Rees
date: 2015-02-25
href: http://www.nextinpact.com/news/93201-droit-d-auteur-jean-marie-cavada-flingue-rapport-reda.htm
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Au Parlement européen, en commission des affaires juridiques («JURI», dans le jargon), Julia Reda a présenté lundi son projet de rapport sur la réforme du droit d’auteur. Au terme des discussions, Jean-Marie Cavada, qui présidait la séance, n’a pas été d’une tendresse particulière avec l’eurodéputée du Parti pirate.
