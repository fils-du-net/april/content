---
site: l'Humanité.fr
title: "Du logiciel libre au bien commun"
date: 2015-02-03
href: http://www.humanite.fr/du-logiciel-libre-au-bien-commun-564444
tags:
- Économie
- International
---

> Valérie Peugeot Membre de l’association Vecam Comment le partage peut-il faire la nouvelle économie?
