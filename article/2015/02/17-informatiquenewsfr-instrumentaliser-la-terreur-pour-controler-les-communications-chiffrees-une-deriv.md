---
site: InformatiqueNews.fr
title: "Instrumentaliser la terreur pour contrôler les communications chiffrées: une dérive dangereuse"
author: Guy Hervier
date: 2015-02-17
href: http://www.informatiquenews.fr/instrumentaliser-la-terreur-pour-controler-les-communications-chiffrees-une-derive-dangereuse-jeremie-zimmermann-quadrature-du-net-30248
tags:
- Internet
- Institutions
- Promotion
- Vie privée
---

> Les attentats de janvier à Paris ont déclenché une vague de discours sécuritaires et de dangereux projets législatifs s’annoncent bien au-delà des frontières françaises. Un contrôle des communications en ligne, de la surveillance, des attaques contre l’expression anonyme et le chiffrement sont déjà à l’ordre du jour, sous prétexte de combattre un ennemi invisible dans une guerre perpétuelle.
