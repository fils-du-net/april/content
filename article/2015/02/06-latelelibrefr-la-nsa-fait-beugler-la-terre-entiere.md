---
site: LaTeleLibre.fr
title: "La NSA fait Beugler la Terre entière"
author: Lurinas
date: 2015-02-06
href: http://latelelibre.fr/2015/02/06/nsa-fait-beugler-terre-entiere
tags:
- Internet
- Institutions
- Europe
- Vie privée
---

> La masse de documents exfiltrés par Snowden est telle que les révélations se succèdent ad nauseam. Le Conseil de l’Europe lui-même vient de livrer un rapport très critique sur les pratiques de la NSA. Si même lui le dit, c’est dire l’ampleur du scandale!
