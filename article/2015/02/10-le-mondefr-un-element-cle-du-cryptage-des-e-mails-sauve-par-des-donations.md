---
site: Le Monde.fr
title: "Un élément-clé du cryptage des e-mails sauvé par des donations"
date: 2015-02-10
href: http://www.lemonde.fr/pixels/article/2015/02/10/un-projet-crucial-pour-la-securite-des-emails-sauve-de-la-banqueroute_4573019_4408996.html
tags:
- Internet
- Économie
- Innovation
- Sciences
- Vie privée
---

> Après un article dans la presse, l'Allemand Werner Koch a reçu suffisemment d'argent pour relancer son projet GPG de cryptage des communications.
