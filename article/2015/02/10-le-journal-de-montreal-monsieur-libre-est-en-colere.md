---
site: Le Journal de Montréal
title: "Monsieur Libre est en colère"
author: Jean-Nicolas Blanchet
date: 2015-02-10
href: http://www.journaldemontreal.com/2015/02/10/monsieur-libre-est-en-colere
tags:
- Logiciels privateurs
- Administration
- Institutions
- Marchés publics
- International
---

> L’important défenseur de l’industrie du logiciel libre est en beau fusil contre le gouvernement, mais pas à peu près.
