---
site: Next INpact
title: "Décret sur le blocage administratif des sites: l'avis critique de l'ARCEP"
author: Marc Rees
date: 2015-02-06
href: http://www.nextinpact.com/news/92995-decret-sur-blocage-administratif-sites-l-avis-critique-l-arcep.htm
tags:
- Internet
- Économie
- Institutions
- Neutralité du Net
---

> Contactée, l'Autorité de régulation des communications électroniques et des postes (ARCEP) a bien voulu nous transmettre son avis officiel sur le projet de décret imposant le blocage des sites pédopornographiques ou ceux incitant ou provocant au terrorisme. Un texte devenu aujourd’hui définitif suite à sa publication au Journal officiel.
