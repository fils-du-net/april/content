---
site: Le Temps
title: "Snowden &amp; Co oscarisés. Et maintenant?"
author: Mehdi Atmani
date: 2015-02-25
href: http://www.letemps.ch/Page/Uuid/8d4b56fa-bc47-11e4-b1aa-59105399a835/Snowden__Co_oscaris%C3%A9s._Et_maintenant
tags:
- Internet
- Institutions
- International
- Vie privée
---

> Le documentaire «Citizenfour», sur Edward Snowden, remporte la statuette. En guest star sur la Toile, le «whistleblower» se livre en interview
