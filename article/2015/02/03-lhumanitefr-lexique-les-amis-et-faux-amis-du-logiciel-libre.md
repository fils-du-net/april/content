---
site: l'Humanité.fr
title: "Lexique: les amis et faux amis du logiciel libre"
date: 2015-02-03
href: http://www.humanite.fr/lexique-les-amis-et-faux-amis-du-logiciel-libre-564378
tags:
- Philosophie GNU
- International
---

> Logiciel libre. Est libre un programme qui rassemble les quatre libertés fondamentales: liberté d’utilisation, d’accès  au code source et de  sa modification, liberté de distribution. Pour le fondateur du mouvement, Richard Stallman, «le logiciel libre tient en trois mots. Liberté, parce qu’il respecte celle de ses utilisateurs. Égalité, car, en l’utilisant, personne n’a de pouvoir sur personne. Fraternité, parce que nous encourageons la coopération entre les utilisateurs.»
