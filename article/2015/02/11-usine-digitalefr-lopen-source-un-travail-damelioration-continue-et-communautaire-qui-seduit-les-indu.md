---
site: "usine-digitale.fr"
title: "L'open source, un travail d'amélioration continue et communautaire qui séduit les industriels"
author: Morgane Remy
date: 2015-02-11
href: http://www.usine-digitale.fr/editorial/l-open-source-un-travail-d-amelioration-continue-et-communautaire-qui-seduit-les-industriels.N307172
tags:
- Entreprise
- Économie
- Innovation
---

> L’open source fait des petits. Né dans le logiciel, avec du code source "ouvert" et accessible à tous si l’auteur n’oppose aucune restriction, le concept "open" touche désormais le hardware. Il permet désormais à des communauté de "makers" de fabriquer des objets dont les conceptions sont ouvertes et de les améliorer. Un bon moyen pour une start-up de se lancer dans un projet, à moindre coût. D'abord réticentes, les grandes entreprises se mettent à leur tour à regarder avec intérêt ces nouvelles pratiques; Y compris dans des secteurs longtemps réticents, comme l’automobile.
