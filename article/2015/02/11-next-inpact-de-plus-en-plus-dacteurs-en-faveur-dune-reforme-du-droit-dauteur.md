---
site: Next INpact
title: "De plus en plus d'acteurs en faveur d'une réforme du droit d'auteur"
author: Marc Rees
date: 2015-02-11
href: http://www.nextinpact.com/news/93039-de-plus-en-plus-dacteurs-en-faveur-dune-reforme-droit-dauteur.htm
tags:
- Interopérabilité
- April
- Institutions
- Associations
- DRM
- Droit d'auteur
- Europe
---

> Le récent projet de rapport de Julia Reda, eurodéputée du Parti Pirate, n’a pas reçu qu’une pluie d’orties des ayants droit et du ministère de la Culture. Il a été aussi très favorablement accueilli par des acteurs d’horizons très divers.
