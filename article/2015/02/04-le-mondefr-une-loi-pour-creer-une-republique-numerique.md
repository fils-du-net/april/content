---
site: Le Monde.fr
title: "Une loi pour créer une «république numérique»"
author: Damien Leloup et Sarah Belouezzane
date: 2015-02-04
href: http://www.lemonde.fr/pixels/article/2015/02/04/une-loi-pour-creer-une-republique-du-numerique_4569510_4408996.html
tags:
- Internet
- Institutions
- Innovation
- Sciences
- Vie privée
---

> Données personnelles, cybersécurité, start-up... Axelle Lemaire, secrétaire d’Etat au numérique, dévoile les premières pistes de travail du gouvernement.
