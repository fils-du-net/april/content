---
site: Direction Informatique
title: "Mise au point sur le logiciel libre et sur sa sécurité"
author: Laurent Bounin
date: 2015-02-26
href: http://www.directioninformatique.com/blogue/mise-au-point-sur-le-logiciel-libre-et-sa-securite/33013
tags:
- Entreprise
- Économie
---

> Au cours des trente dernières années, le thème de la sécurité a été largement exploité par les équipes de marketing et de vente des éditeurs propriétaires pour mettre à mal les logiciels libres.
