---
site: Next INpact
title: "La concertation du CNNum s'achève, ses propositions prévues pour le mois de mars"
author: Xavier Berne
date: 2015-02-05
href: http://www.nextinpact.com/news/92982-la-concertation-cnnum-s-acheve-ses-propositions-prevues-pour-mois-mars.htm
tags:
- Institutions
- Innovation
- Neutralité du Net
- Europe
- Open Data
- Vie privée
---

> Officiellement lancée en octobre dernier, la grande concertation du Conseil national du numérique (CNNum) s’est achevée hier. L’institution doit désormais finaliser ses recommandations et les présenter au gouvernement d’ici la mi-mars, en vue notamment de l’élaboration du futur projet de loi numérique.
