---
site: Mediapart
title: "Toyota propose une \"Open Source Car\" à l’hydrogène"
author: Jean-Lucien Hardy
date: 2015-02-12
href: http://blogs.mediapart.fr/blog/jean-lucien-hardy/120215/toyota-propose-une-open-source-car-l-hydrogene
tags:
- Entreprise
- Économie
- Brevets logiciels
- Innovation
---

> Manifestement, Toyota veut que la voiture à hydrogène soit une voiture libre de toutes contraintes procédurières, une Open Source Car en quelque sorte, à l’instar des Open Source Software que sont Linux, Androïd et quantités d’autres logiciels solides tels que Firefox ou LibreOffice.
