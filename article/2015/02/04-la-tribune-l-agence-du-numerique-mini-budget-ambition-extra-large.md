---
site: La Tribune
title: "L’Agence du numérique, mini budget, ambition extra large"
author: Delphine Cuny
date: 2015-02-04
href: http://www.latribune.fr/technos-medias/20150204triba4f3e7151/l-agence-du-numerique-mini-budget-ambition-extra-large.html
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Innovation
---

> Cette nouvelle agence, dont le décret de création vient d'être publié, regroupera la mission Très haut débit, la délégation aux usages de l’Internet et la mission French Tech de soutien aux startups. Un assemblage disparate et un budget réduit au service d’une vision inclusive et très large du «numérique pour tous.»
