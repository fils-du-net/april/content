---
site: "usine-digitale.fr"
title: "Le logiciel libre, un tremplin pour les start-up du web"
author: Morgane Remy
date: 2015-02-12
href: http://www.usine-digitale.fr/article/le-logiciel-libre-un-tremplin-pour-les-start-up-du-web.N307181
tags:
- Entreprise
- Innovation
---

> Le deuxième épisode de notre série consacrée à l'open source se concentre sur le logiciel libre, qui permet souvent à des start-up de se lancer rapidement et à moindre coût dans un projet. Dans certains cas, l’open source représente même la seule solution techniquement viable.
