---
site: l'Humanité.fr
title: "«Des alternatives aux outils privés existent»"
author: Pierric Marissal
date: 2015-02-03
href: http://www.humanite.fr/des-alternatives-aux-outils-prives-existent-564377
tags:
- Internet
- Sensibilisation
- Associations
---

> Notre régime de propriété intellectuelle est-il encore adapté et juste? délégué général de Framasoft et coordinateur  de Dégooglisons Internet, explique comment se passer des mastodontes du Net.
