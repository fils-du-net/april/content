---
site: Mediapart
title: "Un \"Temps des communs\" à Toulouse à l’automne"
author: Bernard Brunet
date: 2015-02-24
href: http://blogs.mediapart.fr/blog/friture-mag/240215/un-temps-des-communs-toulouse-l-automne
tags:
- April
- Associations
- Promotion
---

> Après une première édition en octobre 2013 (200 événements dans 5 pays) intitulée Villes en Biens communs, nous rééditons le festival des communs, appelé cette fois-ci «Le temps des Communs»: rencontres-débats, ateliers, colloques bien entendu, mais aussi initiation aux licences libres ou à des logiciels libres, cartoparties, écriture collaborative d’un manuel scolaire ouvert, troc de semences, initiation à Arduino ou à une imprimante 3D, découverte de l’open data culture, film sur l’autogestion de l’eau, décryptage de l’habitat groupé… A Toulouse, un groupe s’est constitué pour organiser une
