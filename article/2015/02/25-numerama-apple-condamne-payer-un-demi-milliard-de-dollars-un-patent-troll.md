---
site: Numerama
title: "Apple condamné à payer un demi-milliard de dollars à un \"patent troll\""
author: Julien L.
date: 2015-02-25
href: http://www.numerama.com/magazine/32320-apple-condamne-a-payer-un-demi-milliard-de-dollars-a-un-34patent-troll34.html
tags:
- Entreprise
- HADOPI
- Institutions
- Brevets logiciels
- International
---

> Apple vient d'être condamné aux USA à verser 532,9 millions de dollars de dommages et intérêts à une société américaine qui l'accuse d'avoir enfreint trois de ses brevets. L'entreprise s'avère être un patent troll, c'est-à-dire une firme qui ne fait que collecter des brevets pour pouvoir toucher ensuite des redevances. Apple prévoit de faire appel.
