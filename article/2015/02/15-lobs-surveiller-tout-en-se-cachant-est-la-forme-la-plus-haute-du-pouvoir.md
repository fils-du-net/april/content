---
site: L'OBS
title: "«Surveiller, tout en se cachant, est la forme la plus haute du pouvoir»"
author: Claire Richard
date: 2015-02-15
href: http://rue89.nouvelobs.com/2015/02/15/surveiller-les-autres-tout-cachant-est-forme-plus-haute-pouvoir-257575
tags:
- Entreprise
- Internet
- Institutions
- Innovation
- Open Data
- Vie privée
---

> Frank Pasquale, professeur à l’Université du Maryland, vient de publier «The Black box society». Il décrit comment les algorithmes, protégés par le secret commercial, créent de nouveaux rapports de pouvoir.
