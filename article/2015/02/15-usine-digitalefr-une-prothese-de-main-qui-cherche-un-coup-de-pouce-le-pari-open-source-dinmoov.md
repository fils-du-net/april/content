---
site: "usine-digitale.fr"
title: "Une prothèse de main qui cherche un coup de pouce, le pari open source d'InMoov"
author: Morgane Remy
date: 2015-02-15
href: http://www.usine-digitale.fr/article/une-prothese-de-main-qui-cherche-un-coup-de-pouce-le-pari-open-source-d-inmoov.N307217
tags:
- Entreprise
- Matériel libre
- Innovation
- Sciences
---

> Cinquième et dernier épisode de notre série consacrée à l'open source avec une histoire d’innovation. Mais c’est surtout le récit d’une série de rencontres autour d’un objectif commun, celui d'InMoov qui a conçu une prothèse de main fonctionnelle à moindre coût avant de la transformer en robot.
