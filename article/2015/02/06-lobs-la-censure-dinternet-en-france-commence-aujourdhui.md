---
site: L'OBS
title: "La censure d'internet en France commence aujourd'hui"
author: Boris Manenti
date: 2015-02-06
href: http://tempsreel.nouvelobs.com/attentats-charlie-hebdo-et-maintenant/20150206.OBS1866/la-censure-d-internet-en-france-commence-aujourd-hui.html
tags:
- Internet
- Institutions
---

> Le gouvernement a adopté un décret prévoyant de bloquer les sites djihadistes et pédopornographiques, sans passer par la case justice.
