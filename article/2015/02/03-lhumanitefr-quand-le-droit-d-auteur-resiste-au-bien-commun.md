---
site: l'Humanité.fr
title: "Quand le droit d’auteur résiste au bien commun"
date: 2015-02-03
href: http://www.humanite.fr/quand-le-droit-dauteur-resiste-au-bien-commun-564373
tags:
- Internet
- Économie
- Partage du savoir
- HADOPI
- Contenus libres
---

> Par Danièle Bourcier Responsable scientifique de Creative Commons. Notre régime de propriété intellectuelle est-il encore adapté et juste?
