---
site: Next INpact
title: "Le Sénat adopte le nouveau délit d'obsolescence programmée"
author: Xavier Berne
date: 2015-02-17
href: http://www.nextinpact.com/news/93090-le-senat-adopte-nouveau-delit-d-obsolescence-programmee.htm
tags:
- Institutions
---

> Le Sénat a adopté tard dans la nuit l’article de la loi sur la croissance énergétique punissant les pratiques dites d’obsolescence programmée de deux ans de prison et de 300 000 euros d’amende. L'objectif? Sanctionner les fabricants réduisant sciemment la durée de vie de leurs produits (imprimantes, smartphones...) afin que les consommateurs les remplacent plus souvent.
