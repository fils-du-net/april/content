---
site: infoDSI
title: "L'April dévoile son nouveau conseil d'administration"
date: 2015-02-11
href: http://www.infodsi.com/articles/153992/april-devoile-son-nouveau-conseil-administration.html
tags:
- April
- Institutions
---

> Le nouveau conseil de l'April sera présidé par Jean-Christophe Becquet. «Après trois ans comme président d'une association devenue incontournable pour la protection de la liberté des citoyens, j'ai le plaisir de passer la main à Jean-Christophe qui saura mener l'April encore plus loin dans son combat pour le logiciel libre et pour la défense des libertés», indique en effet Lionel Allorge, le président sortant de l'association.
