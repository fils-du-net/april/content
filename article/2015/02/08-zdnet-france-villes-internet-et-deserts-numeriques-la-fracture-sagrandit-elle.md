---
site: ZDNet France
title: "Villes internet et déserts numériques, la fracture s'agrandit-elle?"
author: Frédéric Charles
date: 2015-02-08
href: http://www.zdnet.fr/actualites/villes-internet-et-deserts-numeriques-la-fracture-s-agrandit-elle-39814346.htm
tags:
- Internet
- Administration
- Sensibilisation
- Informatique en nuage
- Open Data
---

> Les labels 2015 $abstractquot;territoires, villes et villages internet$abstractquot; sont arrivés, les Départements réclament leur label. La folie du label contamine t-elle la république numérique?
