---
site: Mediapart
title: "Profil de Libriste: Frank"
author: André Ani
date: 2015-02-18
href: http://blogs.mediapart.fr/blog/andre-ani/180215/profil-de-libriste-frank
tags:
- Entreprise
- Internet
- Informatique en nuage
- Vie privée
---

> Bonjour Prof Tux, je m’appelle Frank Rousseau, j’ai 32 ans et je suis libre comme l’air! Bon en fait j’ai pas grand chose en commun avec GTO. J’aime juste passer beaucoup de temps devant mon ordi pour faire des sites et applications web et ce depuis plus de 15 ans. Et ce en tant que hobby ou activité professionnelle. Aujourd’hui je suis Co-Fondateur et Directeur Technique de Cozy Cloud, une startup qui réalise des clouds personnels libres.
