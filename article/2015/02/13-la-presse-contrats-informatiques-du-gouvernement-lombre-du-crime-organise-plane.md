---
site: La Presse
title: "Contrats informatiques du gouvernement: l'ombre du crime organisé plane"
author: Patrice Bergeron
date: 2015-02-13
href: http://www.lapresse.ca/actualites/politique/politique-quebecoise/201502/13/01-4843943-contrats-informatiques-du-gouvernement-lombre-du-crime-organise-plane.php
tags:
- Administration
- Économie
- Institutions
- Sciences
- International
---

> Le crime organisé pourrait très bien être impliqué dans l'attribution des contrats informatiques au gouvernement, soupçonne un collectif de 16 organismes regroupant notamment des syndicats, qui dénonce le gaspillage dans les projets informatiques gérés par l'État.
