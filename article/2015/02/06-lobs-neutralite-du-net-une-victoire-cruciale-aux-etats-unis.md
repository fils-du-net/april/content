---
site: L'OBS
title: "Neutralité du net: une victoire cruciale aux Etats-Unis"
author: Amandine Schmitt (avec Boris Manenti)
date: 2015-02-06
href: http://obsession.nouvelobs.com/high-tech/20150205.OBS1795/neutralite-du-net-une-victoire-cruciale-aux-etats-unis.html
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- International
---

> Le chef du régulateur des télécoms américain veut faire d'internet un "service d'utilité publique". Cette mesure empêcherait l'émergence d'un internet à deux vitesses.
