---
site: l'Humanité.fr
title: "Magnetic, l’informatique en un clic éthique"
author: Stéphane Guérard
date: 2015-02-03
href: http://www.humanite.fr/magnetic-linformatique-en-un-clic-ethique-564371
tags:
- Entreprise
- Économie
- Éducation
---

> Depuis quatre ans, une quinzaine de développeurs et de chefs de projet numérique misent sur le fonctionnement en coopérative pour élargir leurs activités d’écoconception durable d’outils numériques et de sites Web. Quand la culture geek se marie parfaitement avec l’ESS.
