---
site: "Branchez-Vous!"
title: "Un collectif réclame une enquête publique sur l'informatique au gouvernement du Québec"
author: Laurent LaSalle
date: 2015-02-16
href: http://branchez-vous.com/2015/02/16/collectif-reclame-enquete-publique-sur-linformatique-au-gouvernement-du-quebec
tags:
- Économie
- Institutions
- Accessibilité
- Associations
- Marchés publics
- International
- Open Data
---

> Devant la place prépondérante qu’occupe désormais la technologie, les signataires d’une déclaration commune demandent une enquête publique sur l’octroi des contrats gouvernementaux liés aux technologies de l’information.
