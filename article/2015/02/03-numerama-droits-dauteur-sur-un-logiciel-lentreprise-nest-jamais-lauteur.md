---
site: Numerama
title: "Droits d'auteur sur un logiciel: l'entreprise n'est jamais l'auteur"
author: Guillaume Champeau
date: 2015-02-03
href: http://www.numerama.com/magazine/32102-droits-d-auteur-sur-un-logiciel-l-entreprise-n-est-jamais-l-auteur.html
tags:
- Entreprise
- HADOPI
- Droit d'auteur
---

> Dans un arrêt du 15 janvier 2015, la Cour de cassation a marqué le principe selon lequel un auteur d'un logiciel ne peut pas être une personne morale, mais nécessairement une ou plusieurs personnes physiques. L'entreprise n'est que propriétaire du logiciel et investie des droits.
