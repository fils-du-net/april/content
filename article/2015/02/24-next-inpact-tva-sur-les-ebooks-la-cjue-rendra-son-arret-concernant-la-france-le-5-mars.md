---
site: Next INpact
title: "TVA sur les ebooks: la CJUE rendra son arrêt concernant la France le 5 mars"
author: Xavier Berne
date: 2015-02-24
href: http://www.nextinpact.com/news/93179-tva-sur-ebooks-cjue-rendra-son-arret-concernant-france-5-mars.htm
tags:
- Économie
- Institutions
- Europe
---

> Paris sera fixé sur son sort le 5 mars prochain. La Cour de justice de l’Union européenne (CJUE) a en effet prévu de dire si oui ou non la France et le Luxembourg ont violé les règles de l’UE en appliquant un taux réduit de TVA sur les livres numériques.
