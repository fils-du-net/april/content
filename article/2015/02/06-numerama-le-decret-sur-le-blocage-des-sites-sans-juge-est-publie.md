---
site: Numerama
title: "Le décret sur le blocage des sites sans juge est publié"
author: Julien L.
date: 2015-02-06
href: http://www.numerama.com/magazine/32134-le-decret-sur-le-blocage-des-sites-sans-juge-est-publie.html
tags:
- Internet
- Institutions
- Sciences
---

> Le gouvernement a fait publier au Journal officiel le décret précisant les modalités de blocage des sites diffusant des contenus pédopornographiques ou à caractère terroriste.
