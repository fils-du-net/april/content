---
site: La Vie
title: "Emmabuntüs, des ordinateurs pour tous"
author: Claire Legros
date: 2015-02-19
href: http://www.lavie.fr/hebdo/2015/3625/emmabuntus-des-ordinateurs-pour-tous-17-02-2015-60620_676.php
tags:
- Économie
- Associations
---

> En France, environ 20 kg de déchets électroniques par personne, dont la majorité n’est pas recyclée, sont produits chaque année. Pour éviter ce gaspillage, une vingtaine de bénévoles se réunissent chaque semaine dans les communautés Emmaüs de Perpignan, Dijon et Paris pour reconditionner le matériel informatique collecté par les compagnons. L’équation est simple.
