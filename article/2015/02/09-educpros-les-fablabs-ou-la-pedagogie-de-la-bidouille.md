---
site: EducPros
title: "Les fablabs ou la pédagogie de la bidouille"
author: Céline Authemayou
date: 2015-02-09
href: http://www.letudiant.fr/educpros/enquetes/fablabs-la-pedagogie-de-la-bidouille.html
tags:
- Partage du savoir
- Éducation
- Innovation
- Sciences
---

> Les fablabs – pour "ateliers de fabrication" – se multiplient sur tout le territoire français. L’enseignement supérieur n’échappe pas au phénomène: universités et écoles créent leur propre structure. Plus que de simples ateliers de bidouille réservés aux fous de technologies numériques, ces lieux permettent aux établissements de décloisonner leurs enseignements tout en valorisant l’apprentissage par l’action.
