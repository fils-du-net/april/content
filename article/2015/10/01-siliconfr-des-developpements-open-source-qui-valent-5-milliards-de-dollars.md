---
site: Silicon.fr
title: "Des développements Open Source qui valent 5 milliards de dollars"
author: Ariane Beky
date: 2015-10-01
href: http://www.silicon.fr/fondation-linux-developpements-open-source-5-milliards-dollars-127880.html
tags:
- Entreprise
- Économie
---

> 5 milliards de dollars, c’est la valeur estimée du développement de projets collaboratifs soutenus par la Fondation Linux. Les lignes de code source et les années-personnes sont prises en compte.
