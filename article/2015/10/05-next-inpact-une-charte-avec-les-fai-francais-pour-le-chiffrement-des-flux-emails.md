---
site: Next INpact
title: "Une charte avec les FAI français pour le chiffrement des flux emails"
author: Marc Rees
date: 2015-10-05
href: http://www.nextinpact.com/news/96749-une-charte-avec-fai-francais-pour-chiffrement-emails.htm
tags:
- Entreprise
- Internet
- Institutions
- Europe
- Vie privée
---

> Axelle Lemaire vient d‘annoncer en milieu de matinée «la signature d’une charte entre les opérateurs français pour le cryptage des boites email». Cependant, l’engagement qui concerne le transport des messages, ne devrait pas contrarier les interceptions légales, selon les vœux de l’Agence nationale de la sécurité des systèmes d'information.
