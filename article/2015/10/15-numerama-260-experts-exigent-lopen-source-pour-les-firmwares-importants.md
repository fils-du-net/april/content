---
site: Numerama
title: "260 experts exigent l'Open Source pour les firmwares importants"
author: Guillaume Champeau
date: 2015-10-15
href: http://www.numerama.com/tech/126796-260-experts-exigent-lopen-source-pour-les-firmwares-importants.html
tags:
- Internet
- Institutions
---

> Un groupe d'experts de l'industrie informatique, dont le «père de l'internet» Vint Cerf, ont demandé au régulateur américain d'obliger les constructeurs à rendre leurs firmwares open-source. S'ils parlent en priorité des routeurs Wi-Fi, le sujet pourrait s'étendre jusqu'aux firmwares des moteurs de voiture...
