---
site: leJDD.fr
title: "Loi sur le numérique: les internautes soumettent leurs propositions"
author: Patxi Berhouet
date: 2015-10-06
href: http://www.lejdd.fr/Societe/Loi-sur-le-numerique-les-internautes-soumettent-leurs-propositions-754341
tags:
- Internet
- Administration
- Institutions
- Vente liée
- Vote électronique
- Open Data
---

> Douze jours avant la clôture de la consultation sur le projet de loi pour une République numérique, plus de 3500 contributions ont été reçues.
