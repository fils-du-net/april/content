---
site: Voir.ca
title: "Circulation automobile: la puissance des données ouvertes et des logiciels libres"
author: Yvan Dutil
date: 2015-10-05
href: http://voir.ca/yvan-dutil/2015/10/05/circulation-automobile-la-puissance-des-donnees-ouvertes-et-des-logiciels-libres
tags:
- Administration
- Institutions
- Innovation
---

> L’annonce récente de l’installation de nouveaux radars photographique dans la région de Québec a soulevé ma curiosité. En effet, la très grande majorité de ces radars seront mobiles, mais dans un souci de transparence, la ville a déjà annoncé où ils seront situés. La question est de savoir si les emplacements sont choisis de façon rationnelle.
