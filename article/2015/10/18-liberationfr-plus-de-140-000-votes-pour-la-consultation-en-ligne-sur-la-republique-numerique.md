---
site: Libération.fr
title: "Plus de 140 000 votes pour la consultation en ligne sur la «République numérique»"
author: Amaelle Guiton
date: 2015-10-18
href: http://www.liberation.fr/futurs/2015/10/18/pres-de-140-000-votes-pour-la-consultation-en-ligne-sur-la-republique-numerique_1406724
tags:
- Entreprise
- April
- Institutions
- Associations
- Droit d'auteur
---

> La consultation en ligne sur le projet de loi «pour la République numérique», qui a mobilisé plus de 20 000 contributeurs, se termine ce dimanche à minuit. Les propositions des internautes seront arbitrées cette semaine.
