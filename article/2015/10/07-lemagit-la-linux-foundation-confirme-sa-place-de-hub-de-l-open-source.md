---
site: LeMagIT
title: "La Linux Foundation confirme sa place de hub de l’Open Source"
author: Cyrille Chausson
date: 2015-10-07
href: http://www.lemagit.fr/actualites/4500255025/La-Linux-Foundation-confirme-sa-place-de-hub-de-lOpen-Source
tags:
- Associations
- Innovation
- Licenses
---

> A l’occasion de la LinuxCon qui se tient actuellement à Dublin, la Linux Foundation a présenté un projet collaboratif pour rapprocher la branche temps réel du noyau Linux principal et montrer que la conformité du code et des licences étaient deux éléments clés à prendre en compte.
