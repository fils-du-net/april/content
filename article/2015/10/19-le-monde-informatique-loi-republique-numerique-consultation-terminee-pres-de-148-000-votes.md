---
site: Le Monde Informatique
title: "Loi République numérique: Consultation terminée, près de 148 000 votes"
author: Maryse Gros
date: 2015-10-19
href: http://www.lemondeinformatique.fr/actualites/lire-loi-republique-numerique-consultation-terminee-pres-de-148-000-votes-62704.html
tags:
- April
- Institutions
- Associations
- Open Data
---

> La concertation publique autour du projet de loi pour une République numérique vient de s'achever. Plus de 8 500 contributions et 147 710 votes ont été effectués autour de ses 30 articles. La secrétaire d'Etat au numérique, Axelle Lemaire, recevra cette semaine quatre auteurs des contributions ayant suscité le plus d'intérêt.
