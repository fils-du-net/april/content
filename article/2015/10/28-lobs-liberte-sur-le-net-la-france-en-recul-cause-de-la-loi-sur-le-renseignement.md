---
site: L'OBS
title: "Liberté sur le Net: la France en recul à cause de la loi sur le renseignement"
author: Emilie Brouze
date: 2015-10-28
href: http://rue89.nouvelobs.com/2015/10/28/liberte-net-france-recul-a-cause-loi-renseignement-261856
tags:
- Internet
- Institutions
- Associations
- International
---

> L’ONG américaine Freedom House a publié ce mercredi un rapport de 40 pages sur la liberté sur Internet [PDF]. Par rapport à l’année passée, la liberté d’accès et d’expression sur le Net a reculé dans 32 pays sur les 65 étudiés. La France en fait partie, tout comme la Libye, l’Ukraine, la Birmanie ou encore la Turquie.
