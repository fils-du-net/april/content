---
site: ZDNet France
title: "Les services libres Framasoft pour dégoogliser Internet, en chiffres"
author: Thierry Noisette
date: 2015-10-07
href: http://www.zdnet.fr/actualites/les-services-libres-framasoft-pour-degoogliser-internet-en-chiffres-39826064.htm
tags:
- Internet
- Associations
---

> Un an après son lancement, la campagne de l'association libriste pour créer des alternatives aux services stars américains (et à la curiosité insatiable des services de renseignement US) donne des statistiques. Plus de 500 sondages par jour via Framadate, par exemple.
