---
site: InformatiqueNews.fr
title: "L'open source: L'innovation et la sécurité par la transparence"
author: La rédaction
date: 2015-10-23
href: http://www.informatiquenews.fr/lopen-source-linnovation-et-la-securite-par-la-transparence-ismet-geri-forgerock-40854
tags:
- Entreprise
- Innovation
- Promotion
---

> Le contraste entre les logiciels propriétaires et open source est aussi vieux que l’industrie de l’informatique elle-même. Dans presque toutes les catégories, des logiciels sont disponibles soit auprès de fournisseurs qui développent et commercialisent eux-mêmes leur code, soit auprès de communautés de développeurs travaillant avec du code ouvert. Au cours de la dernière décennie, l’aversion envers l’utilisation des logiciels libres, particulièrement dans les entreprises, a pris un tournant majeur.
