---
site: AgoraVox
title: "Réseaux sociaux alternatifs"
author: Bonnes Nouvelles
date: 2015-10-28
href: http://www.agoravox.fr/tribune-libre/article/reseaux-sociaux-alternatifs-173417
tags:
- Internet
- Sensibilisation
- Innovation
- Vie privée
---

> Rejoignez un réseau social plus éthique.
