---
site: Libération.fr
title: "Sur le Net, des «communs» en devenir"
author: Amaelle Guiton
date: 2015-10-18
href: http://www.liberation.fr/futurs/2015/10/18/sur-le-net-des-communs-en-devenir_1406753
tags:
- Entreprise
- Internet
- Économie
- Partage du savoir
- Institutions
- Droit d'auteur
---

> Régulation. Les pratiques numériques ont fait émerger de nouvelles ressources partagées. Leur reconnaissance est l’un des enjeux du projet de loi Lemaire.
