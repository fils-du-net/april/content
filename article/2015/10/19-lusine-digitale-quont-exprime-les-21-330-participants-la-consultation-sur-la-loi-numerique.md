---
site: L'Usine Digitale
title: "Qu'ont exprimé les 21 330 participants à la consultation sur la loi numérique?"
author: Aurélie Barbaux
date: 2015-10-19
href: http://www.usine-digitale.fr/editorial/qu-ont-exprime-les-21-330-participants-a-la-consultation-sur-la-loi-numerique.N357473
tags:
- Internet
- April
- Institutions
- Associations
- Open Data
---

> La consultation publique pour le projet de loi pour une République numérique d'Axelle Lemaire a recueilli 147710 votes et 8501 contributions argumentées de la part de 21 330 personnes. Cette participation démontre une plus grande maturité qu’attendue des citoyens sur des sujets souvent avancés comme trop techniques. Une leçon pour Emmanuel Macron - qui prépare sa propre loi numérique - et l’Europe, qui affinent directives et réglements?
