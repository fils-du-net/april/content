---
site: France 24
title: "Le Parlement européen déçoit le défenseurs de la neutralité du Net"
author: Sébastian Seibt
date: 2015-10-27
href: http://www.france24.com/fr/20151027-neutralite-net-paquet-numerique-parlement-europe-ue-internet-reglementation-critique
tags:
- Internet
- Institutions
- Neutralité du Net
- Europe
---

> Le Parlement européen a validé, mardi, les mesures très controversées à l'encontre de la neutralité du Net. À tel point que certains défenseurs de ce principe parlent d’un jour noir pour les internautes européens.
