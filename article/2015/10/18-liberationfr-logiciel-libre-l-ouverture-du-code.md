---
site: Libération.fr
title: "Logiciel libre L’ouverture du code"
author: Amaelle Guiton
date: 2015-10-18
href: http://www.liberation.fr/futurs/2015/10/18/logiciel-libre-l-ouverture-du-code_1406754
tags:
- Innovation
- Promotion
---

> La légende veut que ce soit en souhaitant modifier le pilote d’une imprimante Xerox récalcitrante et en découvrant qu’il n’avait pas accès au code source que l’informaticien américain Richard Stallman ait eu, en 1980, le déclic qui le pousserait à initier le mouvement du «logiciel libre»: celui que chacun peut librement utiliser, étudier, modifier et partager - ce qui implique l’ouverture du code.
