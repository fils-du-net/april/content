---
site: Les Echos
title: "Pour en finir avec la vieille politique!"
author: Gaspard Koenig
date: 2015-10-06
href: http://www.lesechos.fr/idees-debats/editos-analyses/021381960462-pour-en-finir-avec-la-vieille-politique-1162741.php
tags:
- Internet
- Institutions
- Innovation
---

> L’oligopole des partis réduit les électeurs à choisir un lider maximo vite honni de tous. Internet permet désormais la participation directe des citoyens qui pourrait devenir la norme plutôt que l’exception.
