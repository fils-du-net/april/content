---
site: Libération.fr
title: "Internet, portail des lieux du commun"
author: Amaelle Guiton
date: 2015-10-17
href: http://www.liberation.fr/futurs/2015/10/17/internet-portail-des-lieux-du-commun_1405390
tags:
- Internet
- Économie
- Partage du savoir
- Institutions
- Droit d'auteur
---

> De Wikipédia aux logiciels libres, des Creative Commons à l'Open Access, l'informatique et Internet ont fait émerger de nouveaux «biens communs». Quelle place leur donner? La question est au cœur du débat sur le projet de loi numérique.
