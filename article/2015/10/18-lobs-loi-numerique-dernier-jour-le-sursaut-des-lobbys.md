---
site: L'OBS
title: "Loi numérique, dernier jour: le sursaut des lobbys"
author: Andréa Fradin
date: 2015-10-18
href: http://rue89.nouvelobs.com/2015/10/18/loi-numerique-dernier-jour-sursaut-lobbys-261722
tags:
- Institutions
- Open Data
---

> C’est fini! Après une vingtaine de jours de débat, le projet de loi numérique ferme ses portes au public ce dimanche, à minuit!
