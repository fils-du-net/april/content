---
site: Next INpact
title: "Loi sur la surveillance internationale: au Sénat, les écologistes voteront contre, des UDI aussi"
author: Marc Rees
date: 2015-10-27
href: http://www.nextinpact.com/news/97059-loi-sur-surveillance-internationale-au-senat-ecologistes-voteront-contre-udi-aussi.htm
tags:
- Internet
- Institutions
- Vie privée
---

> C’est ce soir que la proposition de loi sur la surveillance internationale sera examinée par les sénateurs. Ce texte d’origine parlementaire (en façade) a été justifié par la censure constitutionnelle de l’article éponyme de la loi sur le renseignement.
