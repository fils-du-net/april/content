---
site: L'OBS
title: "Loi numérique: les trolls et WTF du 7e jour"
author: Andréa Fradin
date: 2015-10-02
href: http://rue89.nouvelobs.com/2015/10/02/loi-numerique-les-trolls-wtf-7e-jour-261473
tags:
- Internet
- Institutions
- Vote électronique
- Open Data
---

> Et c’est reparti! Une semaine après le lancement de la consultation sur le projet de loi numérique, on y replonge, afin de voir si les internautes se mobilisent et sur quels sujets (conneries et trolleries incluses).
