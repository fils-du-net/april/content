---
site: Slate.fr
title: "Bassel Khartabil, prisonnier syrien qui vit et risque de mourir pour Internet"
author: Stéphanie Vidal
date: 2015-10-09
href: http://www.slate.fr/story/107927/bassel-khartabil-prisonnier-syrien-internet
tags:
- Internet
- Institutions
- Promotion
- International
---

> Défenseur mondialement connu de l'Internet libre et de la culture open source, il est emprisonné depuis trois ans et demi par le régime de Bachar el-Assad et vient d'être transféré de la prison d'Adra vers une destination inconnue.
