---
site: ZDNet France
title: "Projet de loi République Numérique: forte participation citoyenne et transparence du lobbying"
author: Pierre Col
date: 2015-10-22
href: http://www.zdnet.fr/actualites/projet-de-loi-republique-numerique-forte-participation-citoyenne-et-transparence-du-lobbying-39826810.htm
tags:
- Entreprise
- April
- Institutions
- Associations
- Open Data
---

> La consultation publique sur le projet de loi pour une République Numérique a permis aux citoyens de s’exprimer, et ils ne s’en sont pas privés. Au-delà, cette opération a aussi permis de recenser les positions et arguments de nombreux groupes de pression, exposés en toute transparence.
