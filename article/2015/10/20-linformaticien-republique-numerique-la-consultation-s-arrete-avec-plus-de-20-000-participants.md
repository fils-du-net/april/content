---
site: L'Informaticien
title: "République Numérique: la consultation s’arrête avec plus de 20 000 participants"
date: 2015-10-20
href: http://www.linformaticien.com/actualites/id/38240/republique-numerique-la-consultation-s-arrete-avec-plus-de-20-000-participants.aspx
tags:
- April
- Institutions
- Associations
- Open Data
---

> Le projet de consultation à vocation collaborative sur la future loi Numérique, porté par Axelle Lemaire, a attiré plus de 20 000 participants. Un succès, mais qui doit être confirmé par la prise en compte, ou non, de plus de 8 000 contributions écrites.
