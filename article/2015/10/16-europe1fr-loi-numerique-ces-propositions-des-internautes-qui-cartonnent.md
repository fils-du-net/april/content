---
site: Europe1.fr
title: "Loi numérique: ces propositions des internautes qui cartonnent"
author: Noémi Marois
date: 2015-10-16
href: http://www.europe1.fr/economie/loi-numerique-ces-propositions-des-internautes-qui-cartonnent-2531025
tags:
- Internet
- Institutions
- Neutralité du Net
---

> Utilisation de systèmes d'exploitation libre, actions de groupe dans le domaine du numérique… certaines propositions ont récolté de nombreux votes.
