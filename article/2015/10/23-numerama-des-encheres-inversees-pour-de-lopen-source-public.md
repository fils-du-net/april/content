---
site: Numerama
title: "Des enchères inversées pour de l'open-source public"
author: Guillaume Champeau
date: 2015-10-23
href: http://www.numerama.com/tech/128025-des-encheres-inversees-pour-de-lopen-source-public.html
tags:
- Administration
- Économie
- Accessibilité
---

> L'administration américaine testera à partir de la semaine prochaine un nouveau type de marché public pour les petits projets open-source, basés sur des enchères inversées.
