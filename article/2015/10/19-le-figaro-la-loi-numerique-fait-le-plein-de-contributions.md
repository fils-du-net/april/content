---
site: Le Figaro
title: "La loi Numérique fait le plein de contributions"
author: Jules Darmanin
date: 2015-10-19
href: http://www.lefigaro.fr/secteur/high-tech/2015/10/19/32001-20151019ARTFIG00261-21000-francais-ont-participe-a-la-consultation-sur-la-loi-numerique.php
tags:
- Administration
- April
- Institutions
- Neutralité du Net
---

> Le portail du projet de loi Numérique a enregistré 21.000 participations. Le gouvernement doit répondre aux contributions par une synthèse le 26 octobre.
