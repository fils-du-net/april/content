---
site: ChannelNews
title: "Création de l’Association Francophone de Prévention des Risques Numériques sous l’égide de DS Avocats"
author: Dirk Basyn
date: 2015-10-29
href: http://www.channelnews.fr/creation-de-lassociation-francophone-de-prevention-des-risques-numeriques-sous-legide-de-ds-avocats-57222
tags:
- Associations
---

> Première association loi 1901 de ce type, l’Association Francophone de Prévention des Risques Numériques vient d’être créée sous sous l’égide du cabinet DS Avocats. Elle a pour originalité de rassembler en son sein trois domaines de compétences transversales et indispensables à la prévention des risques numériques: le juridique, le technique et les assurances. Les objectifs de l’AFPRN sont la sensibilisation et la prévention, voire l’identification des bons comportements à adopter face aux risques numériques.
