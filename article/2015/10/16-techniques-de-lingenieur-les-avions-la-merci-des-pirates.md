---
site: Techniques de l'Ingénieur
title: "Les avions à la merci des pirates?"
author: Philippe Richard
date: 2015-10-16
href: http://www.techniques-ingenieur.fr/actualite/web-thematique_89430/les-avions-a-la-merci-des-pirates-article_297547
tags:
- Sensibilisation
---

> La prise de contrôle d’un appareil n’appartient pas à la science-fiction. Différents experts en sécurité estiment que de nombreuses failles permettent d’imaginer le pire.
