---
site: L'OBS
title: "«Cryptologie légale»: mais qu’a voulu dire Manuel Valls?"
author: Andréa Fradin
date: 2015-10-16
href: http://rue89.nouvelobs.com/2015/10/16/cryptologie-legale-qua-voulu-dire-manuel-valls-261691
tags:
- Internet
- Institutions
- Vie privée
---

> Le Premier ministre a présenté ce vendredi, à la Maison de la Chimie à Paris, «la stratégie nationale pour la sécurité numérique». Une phrase du discours qu’il a prononcé nous a interpellés.
