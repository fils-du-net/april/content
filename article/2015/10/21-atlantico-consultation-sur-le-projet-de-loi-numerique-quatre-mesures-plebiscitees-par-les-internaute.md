---
site: atlantico
title: "Consultation sur le projet de loi numérique: quatre mesures plébiscitées par les internautes et qui peuvent mettre le gouvernement dans une position délicate"
author: Claude-Etienne Armingaud
date: 2015-10-21
href: http://www.atlantico.fr/decryptage/consultation-projet-loi-numerique-quatre-mesures-plebiscitees-internautes-et-qui-peuvent-mettre-gouvernement-dans-position-2398302.html
tags:
- Institutions
---

> Le gouvernement vient de clore la consultation publique ouverte à l’occasion du projet de loi numérique. Retour sur les propositions les plus populaires.
