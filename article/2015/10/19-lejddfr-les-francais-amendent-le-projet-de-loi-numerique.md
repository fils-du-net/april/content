---
site: leJDD.fr
title: "Les Français amendent le projet de loi numérique"
author: Anne-Charlotte Dusseaulx
date: 2015-10-19
href: http://www.lejdd.fr/Societe/Les-Francais-amendent-le-projet-de-loi-numerique-756095
tags:
- Administration
- Institutions
- Open Data
---

> Jusqu'à dimanche soir, le projet de loi sur le numérique était ouvert à la participation. Plus de 20.000 Français ont fait part de leurs suggestions pour amender le texte législatif. La synthèse de cette consultation sera présentée fin octobre par le gouvernement.
