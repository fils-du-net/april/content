---
site: Le Journal de Montréal
title: "Le message de Sylvain Carle pour le libre"
author: Jean-Nicolas Blanchet
date: 2015-10-14
href: http://www.journaldemontreal.com/2015/10/14/le-message-de-sylvain-carle-pour-le-libre
tags:
- Entreprise
- Institutions
- Associations
- Promotion
- International
---

> L’ancien employé de Twitter et investisseur Sylvain Carle évalue aussi que le gouvernement québécois est timide concernant l’implantation et le développement des logiciels libres.
