---
site: Next INpact
title: "On vous explique la proposition de loi sur la surveillance internationale"
author: Marc Rees
date: 2015-10-26
href: http://www.nextinpact.com/news/97032-on-vous-explique-proposition-loi-sur-surveillance-internationale.htm
tags:
- Internet
- Institutions
- Vie privée
---

> C’est demain en fin de journée que la proposition de loi sur la surveillance internationale entrera en discussion en séance publique au Sénat. Une douzaine d'amendements ont été déposés pour l’heure, traduisant une perfection absolue du texte… ou une faible mobilisation des parlementaires.
