---
site: ITespresso
title: "Mozilla casse sa tirelire pour l'open source"
date: 2015-10-27
href: http://www.itespresso.fr/mozilla-casse-tirelire-open-source-111721.html
tags:
- Internet
- Économie
---

> Mozilla formalise son programme de soutien à l’open source en débloquant 1 million de dollars pour financer une première sélection de 10 projets.
