---
site: Next INpact
title: "Europe: neutralité du Net et fin de l'itinérance adoptées, malgré des réticences"
author: Guénaël Pépin
date: 2015-10-27
href: http://www.nextinpact.com/news/97046-europe-neutralite-net-et-fin-itinerance-adoptees-malgre-reticences.htm
tags:
- Internet
- Institutions
- Neutralité du Net
- Europe
---

> Le Parlement européen vient d'adopter le texte sur l'«Internet ouvert», censé entériner la fin des frais d'itinérance en Europe et la neutralité du Net. Tous les amendements proposés ont été rejetés, alors que des questions restent en suspens.
