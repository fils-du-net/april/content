---
site: Slate.fr
title: "Renseignement: le PS français partiellement désavoué par ses collègues européens"
author: Grégor Brandy
date: 2015-10-29
href: http://www.slate.fr/story/109155/surveillance-socialistes-parlement-europeen
tags:
- Institutions
- Associations
- Europe
- Vie privée
---

> Le Parlement européen a adopté, grâce aux voix de la plupart des élus sociaux-démocrates, un texte proclamant son inquiétude face à la loi renseignement. Les élus du PS français ont cependant réussi à bloquer leur demande d'une évaluation du dispositif par la Commission.
