---
site: Rue89Lyon
title: "Les défenseurs du bio doivent-ils être technophobes?"
author: Roma Cotek
date: 2015-10-01
href: http://www.rue89lyon.fr/2015/10/01/defenseurs-bio-doivent-ils-etre-technophobes
tags:
- Partage du savoir
- Matériel libre
- Innovation
---

> J’étais sur ce salon. A côté des robots il y avait une démonstration de désherbage mécanique avec des engins tout simples en métal, fabriqués directement par des maraichers bio (donc parfaitement adaptés à leurs besoins) membres de l’Atelier paysan. Cette association met ensuite les plans de ces engins et outils en open source sur le net. On peut se construire sa machine à désherber pour moins de 2000 euros et faire évoluer les plans, car ils sont sous licence creative commons. La même logique que les logiciels libres. La technologie peut avoir du bon.
