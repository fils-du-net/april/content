---
site: Basta !
title: "Un village d'éco-inventeurs, pour développer des outils libres au service de l’écologie et de l’intérêt général"
author: Sophie Chapelle
date: 2015-10-09
href: http://www.bastamag.net/Un-village-d-eco-inventeurs-pour-developper-des-outils-libres-au-service-de-l
tags:
- Économie
- Matériel libre
- Associations
- Innovation
---

> Une éolienne en kit à moins de vingt euros, une douche qui recycle l’eau en boucle, un tracteur à pédales, un filtre antibactérien qui rend l’eau potable… Le point commun entre ces projets sociaux et écologiques? L’open source. Des outils sans brevets, donc librement diffusables et appropriables par tous, à moindre coûts. Dans les Yvelines, des jeunes ont passé cinq semaines à développer des projets innovants, qui pourraient révolutionner nos manières de travailler, produire ou consommer. Rencontre avec ces makers qui veulent changer le monde.
