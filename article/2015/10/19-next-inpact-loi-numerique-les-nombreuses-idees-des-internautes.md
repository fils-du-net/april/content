---
site: Next INpact
title: "Loi Numérique: les (nombreuses) idées des internautes"
author: Xavier Berne
date: 2015-10-19
href: http://www.nextinpact.com/news/96945-loi-numerique-nombreuses-idees-internautes.htm
tags:
- Administration
- April
- Institutions
- Associations
- Open Data
---

> Après trois semaines de débats, la consultation publique relative à l’avant-projet de loi numérique s’est achevée dimanche 18 octobre. Les internautes se sont plutôt bien prêtés au jeu, puisqu’ils ont soumis plusieurs centaines de propositions (plus ou moins pertinentes...) au gouvernement – qui aura bien entendu le dernier mot. Next INpact vous propose un petit passage en revue.
