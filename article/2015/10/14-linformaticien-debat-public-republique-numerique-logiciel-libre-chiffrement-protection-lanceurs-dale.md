---
site: L'Informaticien
title: "Débat public République Numérique: logiciel libre, chiffrement, protection lanceurs d'alertes..."
author: Charlie Braume
date: 2015-10-14
href: http://www.linformaticien.com/actualites/id/38186/debat-public-republique-numerique-logiciel-libre-chiffrement-protection-lanceurs-d-alertes.aspx
tags:
- Internet
- Administration
- Institutions
- Neutralité du Net
- Open Data
---

> Il ne reste plus que 4 jours pour participer à la première grande consultation publique participative sur un projet de loi. Et ensuite vérifier vite la réalité de la prise en compte des votes des internautes.
