---
site: Les Echos
title: "DevOps consacre les bénéfices de l’Open Source"
author: Carine Braun-Heneault
date: 2015-10-21
href: http://www.lesechos.fr/idees-debats/cercle/cercle-142003-devops-consacre-les-benefices-de-lopen-source-1167768.php
tags:
- Entreprise
- Innovation
---

> Le développement logiciel est devenu en quelques années un secteur éminemment stratégique de l’industrie informatique. Et de l’industrie dans son ensemble, notamment en France, si l’on en croit les récentes annonces relatives aux plans de transformation du tissu industriel vers le numérique.
