---
site: Numerama
title: "Le Parlement européen adopte un cadre flou sur la neutralité du net"
author: Guillaume Champeau
date: 2015-10-27
href: http://www.numerama.com/tech/128331-le-parlement-europeen-adopte-un-cadre-flou-sur-la-neutralite-du-net.html
tags:
- Internet
- Institutions
- Neutralité du Net
- Europe
---

> Le Parlement européen a adopté mardi en seconde lecture la recommandation sur le Marché unique européen des communications électroniques, qui fixe un cadre sur la neutralité du net dont la portée réelle reste très douteuse.
