---
site: LeMagIT
title: "PaaS Open Source: les principales difficultés rencontrées"
author: Stephen J. Bigelow
date: 2015-10-16
href: http://www.lemagit.fr/conseil/Paas-Open-Source-quelles-sont-les-difficultes
tags:
- Entreprise
- Informatique en nuage
---

> Le Paas Open Source peut certes faciliter le développement et le déploiement d’applications Cloud. Mais pose aussi de problèmes aux développeurs et aux entreprises. Six sont détaillés dans cet article.
