---
site: Maddyness
title: "#Juridique: Projet de loi pour une République numérique: quelles opportunités pour les startups?"
author: Morgane Remy
date: 2015-10-12
href: http://www.maddyness.com/dossiers/2015/10/12/republique-numerique
tags:
- Entreprise
- Internet
- Institutions
- Innovation
- Neutralité du Net
- Open Data
---

> Le projet de loi pour la République numérique d’Axelle Lemaire est soumis à une consultation en ligne jusqu’au 18 octobre. Revue de détails des opportunités qui s’ouvrent aux startups.
