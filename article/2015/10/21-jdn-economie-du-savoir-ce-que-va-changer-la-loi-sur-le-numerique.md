---
site: JDN
title: "Economie du savoir: ce que va changer la loi sur le numérique"
author: Aude Fredouelle
date: 2015-10-21
href: http://www.journaldunet.com/ebusiness/le-net/1164819-economie-du-savoir-ce-que-va-changer-le-projet-de-loi-numerique
tags:
- Économie
- Institutions
- Sciences
- Open Data
---

> Le texte qui prévoit la création d'un domaine commun informationnel et cadre la diffusion des travaux de recherche est loin de faire l'unanimité.
