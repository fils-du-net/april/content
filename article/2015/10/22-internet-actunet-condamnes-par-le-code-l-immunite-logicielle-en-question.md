---
site: internet ACTU.net
title: "Condamnés par le code: l’immunité logicielle en question"
author: Hubert Guillaud
date: 2015-10-22
href: http://www.internetactu.net/2015/10/22/condamnes-par-le-code-limmunite-logicielle-en-question
tags:
- Partage du savoir
- Institutions
- Innovation
- Sciences
- Vie privée
---

> Sur Slate.com, la réalistrice et juriste, diplômée de l’école de droit de Yale, Rebecca Wexler nous rappelle que les programmes dont on ne peut accéder au code sont partout: dans les ascenseurs, dans les avions, dans les appareils médicaux, etc. L’impossibilité à y accéder à des conséquences directes sur la société et la politique… Ils peuvent menacer notre vie privée en recueillant des informations sur chacun d’entre nous sans notre consentement.
