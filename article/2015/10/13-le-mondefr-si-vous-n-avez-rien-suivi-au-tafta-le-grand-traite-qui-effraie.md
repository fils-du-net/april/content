---
site: Le Monde.fr
title: "Si vous n’avez rien suivi au Tafta, le grand traité qui effraie"
author: Maxime Vaudano
date: 2015-10-13
href: http://www.lemonde.fr/les-decodeurs/article/2015/10/13/si-vous-n-avez-rien-suivi-au-tafta-le-grand-traite-qui-effraie_4788413_4355770.html
tags:
- Économie
- Institutions
- Standards
- ACTA
---

> Le traité entre les Etats-Unis et l’Union européenne, dont les négociations patinent, a mis plus de 150 000 Allemands dans la rue et fait face à une opposition grandissante.
