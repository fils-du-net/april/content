---
site: JDN
title: "Paris Open Source Summit: un événement IT d'envergure mondiale"
author: Antoine Crochet- Damais
date: 2015-10-05
href: http://www.journaldunet.com/solutions/dsi/1162990-paris-open-source-summit-veut-devenir-l-evenement-europen-de-l-open-source
tags:
- Entreprise
- Économie
- April
- Institutions
- Associations
- Promotion
---

> Fusion du salon Solutions Linux et de l'Open World Forum, le Paris Open Source Summit se tiendra les 18 et 19 novembre aux Docks de Paris. Le JDN en est partenaire.
