---
site: ZDNet
title: "Projets open source: c'est 5 milliards de dollars pour la Linux Fondation"
date: 2015-10-01
href: http://www.zdnet.fr/actualites/projets-open-source-c-est-5-milliards-de-dollars-pour-la-linux-fondation-39825834.htm
tags:
- Entreprise
- Économie
---

> Dans un nouveau rapport, la Linux Foundation tente d’estimer la valeur totale des différents projets portés par la fondation. Selon son calcul, ceux-ci vaudraient au total 5 milliards, un chiffre calculé à partir du nombre de lignes de codes nécessaires à leur développement.
