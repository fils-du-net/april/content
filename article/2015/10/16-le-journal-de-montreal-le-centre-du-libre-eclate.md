---
site: Le Journal de Montréal
title: "Le centre du libre éclate"
author: Jean-Nicolas Blanchet
date: 2015-10-16
href: http://www.journaldemontreal.com/2015/10/16/le-centre-du-libre-eclate
tags:
- Administration
- Économie
- Institutions
- Marchés publics
- International
---

> Ça ne va pas bien pour le Centre d’expertise en logiciel libre du gouvernement (CELL).
