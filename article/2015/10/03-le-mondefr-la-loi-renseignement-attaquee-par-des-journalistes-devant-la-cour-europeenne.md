---
site: Le Monde.fr
title: "La loi renseignement attaquée par des journalistes devant la Cour européenne"
author: Franck Johannès
date: 2015-10-03
href: http://www.lemonde.fr/pixels/article/2015/10/03/des-journalistes-attaquent-la-loi-renseignement_4781885_4408996.html
tags:
- Internet
- Institutions
- Associations
- Promotion
- Europe
---

> Cent quatre-vingts journalistes de la presse judiciaire attaquent la loi renseignement devant la Cour européenne des droits de l’homme et protestent contre la surveillance de masse.
