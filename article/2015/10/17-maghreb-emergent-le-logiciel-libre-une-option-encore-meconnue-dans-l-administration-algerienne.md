---
site: Maghreb Emergent
title: "Le logiciel libre une option encore méconnue dans l’administration algérienne"
author: Abdelkader Zahar
date: 2015-10-17
href: http://www.maghrebemergent.info/high-tech/information-technology/52103-le-logiciel-libre-une-option-encore-meconnue-dans-l-administration-algerienne.html
tags:
- Logiciels privateurs
- Administration
- Institutions
- Promotion
- International
---

> L’instruction du Premier ministre aux administrations de ne pas migrer vers Windows 10 est louable mais insuffisante. Elle devrait être suivie d’actions plus concrètes en faveur de l’usage du logiciel libre en Algérie.
