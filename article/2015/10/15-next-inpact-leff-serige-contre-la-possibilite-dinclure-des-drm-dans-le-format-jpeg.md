---
site: Next INpact
title: "L'EFF s'érige contre la possibilité d'inclure des DRM dans le format JPEG"
author: Vincent Hermann
date: 2015-10-15
href: http://www.nextinpact.com/news/96907-leff-serige-contre-possibilite-dinclure-drm-dans-format-jpeg.htm
tags:
- Entreprise
- Internet
- Associations
- DRM
---

> Le comité Joint Photographic Experts Group (JPEG) s’est réuni pour aborder la possibilité d’inclure des verrous numériques dans les images JPG. La question se pose devant les problématiques de respect de droits d’auteur. L’EFF, qui y est farouchement opposée, était sur place pour expliquer aux ayants droit les dangers d’une telle démarche.
