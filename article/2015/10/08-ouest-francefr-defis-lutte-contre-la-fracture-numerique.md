---
site: "ouest-france.fr"
title: "Défis lutte contre la fracture numérique"
date: 2015-10-08
href: http://www.ouest-france.fr/defis-lutte-contre-la-fracture-numerique-3754153
tags:
- Sensibilisation
- Associations
- Promotion
---

> L'association lanestérienne propose une solution complète pour permettre aux personnes qui le souhaitent, de s'initier aux usages du numérique et d'acquérir du matériel à moindre coût.
