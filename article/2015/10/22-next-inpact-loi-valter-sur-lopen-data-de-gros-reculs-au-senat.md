---
site: Next INpact
title: "Loi Valter sur l'Open Data: de «gros reculs» au Sénat"
author: Xavier Berne
date: 2015-10-22
href: http://www.nextinpact.com/news/97005-loi-valter-sur-l-open-data-gros-reculs-au-senat.htm
tags:
- Administration
- Institutions
- Open Data
---

> Après avoir été adopté par une Assemblée nationale quasi déserte le 6 octobre dernier, le projet de loi Valter sur la gratuité des informations du secteur public arrive au Sénat. Le texte a été examiné hier en commission, où plusieurs amendements perçus comme de «gros reculs» par l’association Regards Citoyens ont été votés.
