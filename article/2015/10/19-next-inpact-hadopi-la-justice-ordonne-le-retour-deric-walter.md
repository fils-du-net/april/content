---
site: Next INpact
title: "Hadopi: la justice ordonne le retour d'Éric Walter!"
author: Marc Rees
date: 2015-10-19
href: http://www.nextinpact.com/news/96943-hadopi-justice-ordonne-retour-deric-walter.htm
tags:
- HADOPI
---

> Licencié de la Hadopi depuis le 1er août dernier, Éric Walter avait contesté cette décision. Surprise ! Vendredi, le tribunal administratif de Paris a ordonné sa réintégration. Il va du coup redevenir secrétaire général de l’autorité publique indépendante.
