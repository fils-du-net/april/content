---
site: 20minutes.fr
title: "Axelle Lemaire: «Je m’engage à intégrer au projet de loi numérique les idées citoyennes convaincantes»"
author: Nicolas Beunaiche
date: 2015-10-19
href: http://www.20minutes.fr/societe/1711899-20151019-axelle-lemaire-engage-integrer-projet-loi-numerique-idees-citoyennes-convaincantes
tags:
- Administration
- Institutions
- Promotion
---

> Pour la première fois, les Français étaient appelés à voter, commenter et amender un projet de loi avant son arrivée au Parlement. Durant trois semaines, sur une plateforme en ligne dédiée, plus de 21.000 internautes ont ainsi pu donner leur avis sur la «République numérique» que promeut le gouvernement, et même soumettre leurs propres idées. Neutralité du Net, paiement par SMS, vote en ligne… Ils ont tranché. Que va en retenir le gouvernement ? Axelle Lemaire, la secrétaire d’Etat chargée du Numérique, a répondu à 20 Minutes.
