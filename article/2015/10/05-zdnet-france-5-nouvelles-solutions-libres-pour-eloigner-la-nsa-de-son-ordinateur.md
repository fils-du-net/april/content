---
site: ZDNet France
title: "5 nouvelles solutions libres pour éloigner la NSA de son ordinateur"
author: Guillaume Serries
date: 2015-10-05
href: http://www.zdnet.fr/actualites/5-nouvelles-solutions-libres-pour-eloigner-la-nsa-de-son-ordinateur-39825984.htm
tags:
- Internet
- Associations
- Innovation
- Promotion
- Vie privée
---

> Framasoft accuse Google Docs, Dropbox ou encore WeTransfer de piocher allègrement dans les données de leurs utilisateurs pour contenter les services de sécurité américains. L'association française propose de nouvelles solutions "libres" en alternative.
