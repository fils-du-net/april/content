---
site: ZDNet France
title: "République Numérique: clap de fin pour l’expérience collaborative"
author: Louis Adam
date: 2015-10-19
href: http://www.zdnet.fr/actualites/republique-numerique-clap-de-fin-pour-l-experience-collaborative-39826738.htm
tags:
- Institutions
- Associations
---

> Le projet de consultation collaborative autour de la loi Numérique prend fin: après un mois d’activité sur la plateforme République Numérique, le projet de loi va maintenant reprendre le chemin du parlement où le texte doit être examiné début 2016. Tour d'horizon des contributions et quid de la suite?
