---
site: Le Telegramme
title: "Centre des Abeilles. Mille ordinateurs redistribués"
date: 2015-06-08
href: http://www.letelegramme.fr/finistere/quimper/penhars/centre-des-abeilles-mille-ordinateurs-redistribues-08-06-2015-10657800.php
tags:
- Économie
- Associations
---

> Et de 1.000! C'est le nombre total d'ordinateurs redistribués par le Centre des Abeilles et Linux Quimper depuis le début de cette opération, fin 2009. Samedi matin, au Centre des Abeilles, cette barre symbolique a été franchie avec la redistribution gratuite d'une quinzaine d'ordinateurs déclassés et reconfigurés.
