---
site: Archimag
title: "Wikibuilding: et si Paris se réinventait en mode wiki et open source?"
author: Clémence Jost
date: 2015-06-03
href: http://www.archimag.com/vie-numerique/2015/06/03/wikibuilding-paris-wiki-open-source
tags:
- Administration
- Économie
- Partage du savoir
- Innovation
- Vie privée
---

> Wikibuilding a été conçu dans le cadre de l'appel à projets "Réinventer Paris", lancé par la Ville de Paris en 2014. Encré dans le numérique, il imagine une urbanité augmentée, au croisement de l'open source et du wiki.
