---
site: Silicon.fr
title: "Logiciels préinstallés: la Cour de cassation demande à l’Europe de trancher"
author: Reynald Fléchaux
date: 2015-06-26
href: http://www.silicon.fr/logiciels-preinstalles-cour-de-cassation-demande-a-leurope-de-trancher-120333.html
tags:
- Entreprise
- Institutions
- Vente liée
- Promotion
- Europe
---

> Saisie de la question de la vente liée d’ordinateurs et de logiciels, la Cour de cassation saisit la Cour de justice de l’Union européenne.
