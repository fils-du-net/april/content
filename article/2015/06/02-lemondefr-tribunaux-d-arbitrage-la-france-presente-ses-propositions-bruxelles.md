---
site: LeMonde.fr
title: "Tribunaux d’arbitrage: la France présente ses propositions à Bruxelles"
author: Maxime Vaudano
date: 2015-06-02
href: http://transatlantique.blog.lemonde.fr/2015/06/02/tribunaux-darbitrage-la-france-presente-ses-propositions-a-bruxelles
tags:
- Entreprise
- Économie
- Institutions
- ACTA
---

> La France parviendra-t-elle à ménager la chèvre et le chou? Après plusieurs mois de réflexions et de consultation, le gouvernement a transmis mardi 2 juin à la Commission européenne ses propositions de réforme du mécanisme d'arbitrage privé controversé ISDS (acronyme de Investor-State Dispute Settlement) qui plombe les négociations du traité transatlantique entre l'Europe et les Etats-Unis.
