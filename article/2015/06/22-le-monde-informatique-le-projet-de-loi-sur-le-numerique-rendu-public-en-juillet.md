---
site: Le Monde Informatique
title: "Le projet de loi sur le numérique rendu public en juillet"
author: Maryse Gros
date: 2015-06-22
href: http://www.lemondeinformatique.fr/actualites/lire-le-projet-de-loi-strategie-numerique-rendu-public-en-juillet-61546.html
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Innovation
- Marchés publics
- Neutralité du Net
---

> En attendant la présentation du projet de loi sur le numérique, Manuel Valls a tracé les grands traits de la stratégie du Gouvernement en la matière qui prend notamment appui sur le rapport «Ambition numérique» du CNNum, synthèse de la concertation organisée depuis octobre 2014. Des organisations IT dont Syntec Numérique et l'Afdel soulignent leurs points de désaccords avec ce rapport.
