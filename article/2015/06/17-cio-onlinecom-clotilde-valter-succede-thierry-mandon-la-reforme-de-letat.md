---
site: "cio-online.com"
title: "Clotilde Valter succède à Thierry Mandon à la Réforme de l'Etat"
author: Bertrand Lemaire
date: 2015-06-17
href: http://www.cio-online.com/actualites/lire-clotilde-valter-succede-a-thierry-mandon-a-la-reforme-de-l-etat-7698.html
tags:
- Administration
- Institutions
---

> La députée Clotilde Valter devient secrétaire d'Etat chargée de la Réforme de l'Etat et de la simplification auprès du Premier ministre. Elle aura ainsi la tutelle du SGMAP et donc de la DISIC.
