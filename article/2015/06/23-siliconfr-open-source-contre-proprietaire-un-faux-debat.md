---
site: Silicon.fr
title: "«Open Source contre propriétaire, un faux débat»"
author: Reynald Fléchaux
date: 2015-06-23
href: http://www.silicon.fr/garnier-efel-open-source-proprietaire-faux-debat-119902.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Marchés publics
- Promotion
---

> Efel Power, association regroupant des éditeurs de logiciels hexagonaux, pointe les limites du débat sur la priorité accordée à l’Open Source dans l’administration.
