---
site: L'OBS
title: "«La simplification, la simplification», hurlaient les députés"
author: Numeratresse
date: 2015-06-18
href: http://rue89.nouvelobs.com/2015/06/18/simplification-simplification-hurlaient-les-deputes-259777
tags:
- Internet
- Administration
- Économie
- Institutions
- Innovation
- Open Data
- Vie privée
---

> Une Apple Watch sur le mixeur, des militants anti-écran et des algorithmistes: nous avons tenté d’imaginer à quoi ressemblerait la France esquissée par le rapport du Conseil national du numérique publié ce jeudi.
