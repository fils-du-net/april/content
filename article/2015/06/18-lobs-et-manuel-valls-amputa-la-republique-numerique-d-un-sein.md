---
site: L'OBS
title: "Et Manuel Valls amputa la «République numérique» d’un sein"
author: Xavier de La Porte
date: 2015-06-18
href: http://rue89.nouvelobs.com/2015/06/18/manuel-valls-amputa-republique-numerique-dun-sein-259836
tags:
- Internet
- Économie
- Institutions
- Vie privée
---

> L’intervention de Manuel Valls en fin de matinée ce jeudi à la Gaîté lyrique à Paris, pour présenter les grandes lignes de la loi numérique à venir, est étonnante.
