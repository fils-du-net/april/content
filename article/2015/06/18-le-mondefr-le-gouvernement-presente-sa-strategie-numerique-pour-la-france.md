---
site: Le Monde.fr
title: "Le gouvernement présente sa stratégie numérique pour la France"
author: Sarah Belouezzane et Martin Untersinger
date: 2015-06-18
href: http://www.lemonde.fr/pixels/article/2015/06/18/le-gouvernement-presente-sa-strategie-numerique-pour-la-france_4657207_4408996.html
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Innovation
- Neutralité du Net
- Promotion
- Open Data
- Vie privée
---

> Elle était attendue depuis des mois. Jeudi 18 juin, le premier ministre Manuel Valls a présenté lors d’une conférence de presse rien de moins que la stratégie numérique de la France: une série de mesures visant à exposer les ambitions du gouvernement en la matière. La journée a été riche puisque M. Valls s’est aussi vu remettre un rapport du Conseil national du numérique, issu d’une consultation publique et visant à donner des recommandations pour la prochaine grande loi sur le numérique prévue pour l’automne.
