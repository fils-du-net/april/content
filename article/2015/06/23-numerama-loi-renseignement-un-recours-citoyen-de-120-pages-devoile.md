---
site: Numerama
title: "Loi Renseignement: un recours citoyen de 120 pages dévoilé!"
author: Guillaume Champeau
date: 2015-06-23
href: http://www.numerama.com/magazine/33489-loi-renseignement-un-recours-citoyen-de-120-pages-devoile.html
tags:
- Internet
- Institutions
- Associations
- Vie privée
---

> La Quadrature du Net, l'association French Data Network (FDN) et la fédération FDN ont rendu public mardi un projet d'argumentaire de 120 pages qui sera envoyé au Conseil constitutionnel pour lui permettre de limiter la portée du projet de loi Renseignement.
