---
site: Next INpact
title: "Loi Renseignement: la lutte contre la contrefaçon pourrait justifier la surveillance"
author: Marc Rees
date: 2015-06-03
href: http://www.nextinpact.com/news/95309-loi-renseignement-lutte-contre-contrefacon-pourrait-justifier-surveillance.htm
tags:
- Économie
- Institutions
- Vie privée
---

> Lors des débats autour du projet de loi sur le renseignement, le sénateur Jean-Pierre Sueur a expliqué en creux que la lutte contre la contrefaçon pourrait autoriser le déploiement des outils de surveillance programmé par ce texte. Les débats se poursuivent actuellement jusqu'au 9 juin (on pourra suivre ici notre live tweet).
