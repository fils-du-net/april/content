---
site: FutureMag
title: "Richard Stallman: les logiciels libres au secours de la vie privée"
author: Camille Gicquel
date: 2015-06-05
href: http://www.futuremag.fr/animation/richard-stallman-les-logiciels-libres-au-secours-de-la-vie-privee-free-software-NSA-cybers%C3%A9curit%C3%A9-data
tags:
- Internet
- Logiciels privateurs
- Sensibilisation
- Vie privée
---

> Géo-localisation, recherches sur le Web… Les informations personnelles que nous laissons sur Internet font l’objet d’une réappropriation massive par de nombreuses entreprises. Si certaines fondent leur modèle économique sur ces données, d’autres organisations y accèdent dans le but de nous espionner et de nous surveiller. Alors, comment se protéger tout en tirant profit des services innovants proposés en ligne? Pour répondre à cette question, FUTUREMAG est parti à la rencontre de Richard Stallman, leader du mouvement des logiciels libres, lors de son récent passage en France.
