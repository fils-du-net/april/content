---
site: Le Monde.fr
title: "Traité transatlantique: le Parlement européen se divise et reporte son vote"
author: Claire Gatinois
date: 2015-06-10
href: http://www.lemonde.fr/economie/article/2015/06/10/traite-transatlantique-le-parlement-europeen-se-divise-et-reporte-son-vote_4650809_3234.html
tags:
- Économie
- Institutions
- Europe
- International
- ACTA
---

> Machine à fantasmes et source d'inquiétudes, le projet de traité de libre-échange entre l'Europe et les Etats-Unis sème la pagaille à Strasbourg.
