---
site: L'OBS
title: "Loi renseignement avant/après: petits mieux et grandes vacheries"
author: Andréa Fradin
date: 2015-06-25
href: http://rue89.nouvelobs.com/2015/06/25/loi-renseignement-avantapres-petits-mieux-grandes-vacheries-259950
tags:
- Internet
- Institutions
- Vie privée
---

> De sa fuite dans Le Figaro le 17 mars dernier à son adoption solennelle ce 24 juin, le projet de loi sur le renseignement a connu de nombreux rebondissements.
