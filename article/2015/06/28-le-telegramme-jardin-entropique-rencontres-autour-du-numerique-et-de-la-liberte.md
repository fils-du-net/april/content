---
site: Le Telegramme
title: "Jardin Entropique: rencontres autour du numérique et de la liberté"
date: 2015-06-28
href: https://www.letelegramme.fr/ille-et-vilaine/rennes/rennes-jardin-entropique-rencontres-autour-du-numerique-et-de-la-liberte-28-06-2015-10684733.php
tags:
- Philosophie GNU
- Promotion
- Vie privée
---

> Ce week-end s'est déroulé à Rennes le Jardin Entropique, un rendez-vous pour échanger, débattre et permettre de découvrir certains aspects du monde de l'informatique autour du thème du numérique et de la liberté.
