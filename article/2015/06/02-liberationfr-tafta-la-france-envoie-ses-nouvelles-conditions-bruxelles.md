---
site: Libération.fr
title: "Tafta: la France envoie ses (nouvelles) conditions à Bruxelles"
author: Lilian Alemagna
date: 2015-06-02
href: http://www.liberation.fr/politiques/2015/06/02/tafta-la-france-envoie-ses-nouvelles-conditions-a-bruxelles_1321001
tags:
- Entreprise
- Économie
- Institutions
- ACTA
---

> Dans un document d'une quinzaine de pages, Paris demande à encadrer très fortement les tribunaux d'arbitrage censés régler les futurs différends commerciaux.
