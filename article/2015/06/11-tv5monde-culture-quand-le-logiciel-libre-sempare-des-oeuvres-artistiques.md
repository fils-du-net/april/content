---
site: TV5Monde
title: "Culture: quand le logiciel libre s'empare des œuvres artistiques"
author: Pascal Hérard
date: 2015-06-11
href: http://information.tv5monde.com/info/culture-quand-le-logiciel-libre-s-empare-des-oeuvres-artistiques-37998
tags:
- Économie
- Sensibilisation
- Droit d'auteur
- Licenses
- Contenus libres
---

> Un film d'animation produit à l'aide de logiciels libres et offert à la planète entière, des musiciens qui livrent leurs morceaux gratuitement sur Internet... Qui sont ces nouveaux artistes qui promeuvent de nouveaux modèles de partage de biens culturels, à l'opposé du droit d'auteur?
