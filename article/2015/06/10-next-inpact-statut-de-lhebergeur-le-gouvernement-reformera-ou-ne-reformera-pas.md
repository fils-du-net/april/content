---
site: Next INpact
title: "Statut de l'hébergeur: le gouvernement réformera ou ne réformera pas?"
author: Marc Rees
date: 2015-06-10
href: http://www.nextinpact.com/news/95367-statut-hebergeur-gouvernement-reformera-ou-ne-reformera-pas.htm
tags:
- Entreprise
- Internet
- Économie
- Institutions
---

> Réformera ou réformera pas? Pour faire court, en avril dernier, le gouvernement a adressé une note à Bruxelles pour solliciter la réforme du statut (européen) des hébergeurs. Hier, à l’Assemblée nationale, Axelle Lemaire révèle que cette réforme n’est pas soutenue par le gouvernement.
