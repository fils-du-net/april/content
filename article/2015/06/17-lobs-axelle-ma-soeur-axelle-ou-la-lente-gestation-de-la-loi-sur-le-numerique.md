---
site: L'OBS
title: "Axelle ma sœur Axelle, ou la lente gestation de la loi sur le numérique"
author: Andréa Fradin et Rémi Noyon
date: 2015-06-17
href: http://rue89.nouvelobs.com/2015/06/17/axelle-soeur-axelle-lente-gestation-loi-numerique-259787
tags:
- Internet
- Économie
- Institutions
- Neutralité du Net
---

> Manuel Valls présente ce jeudi la «stratégie numérique» de la France. L’un des aspects devrait être la loi Lemaire. Cela fait des mois qu’on l’attend. Why darling?
