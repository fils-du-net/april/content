---
site: "e-RSE.net"
title: "Exposition artistique sur l’Open Source: les logiciels libres facteurs de développement durable"
author: Marie Gerault
date: 2015-06-12
href: http://e-rse.net/exposition-art-open-source-logiciels-libres-developpement-durable-12494
tags:
- Économie
- Sensibilisation
- Associations
- Innovation
---

> Du 20 juin au 18 octobre 2015, l’association COAL présente Open Source, deuxième exposition de sa trilogie Think global, Act local au CEAAC de Strasbourg. L’exposition explore un changement majeur des valeurs occidentales contemporaines et présente un ensemble de pratiques créatives, entre ironie et activisme. L’écologie derrière nos écrans.
