---
site: ITespresso
title: "Vente liée ordinateur-logiciel: la Cour de cassation demande l'arbitrage de l'Europe"
date: 2015-06-26
href: http://www.itespresso.fr/vente-liee-ordinateur-logiciel-cour-cassation-demande-arbitrage-europe-99893.html
tags:
- Institutions
- Vente liée
- Europe
---

> Sur demande de la Cour de cassation française, la Cour de justice de l’Union européenne devra se prononcer que la réalité de pratiques commerciales déloyales dans le cadre de la vente liée d’ordinateur et de logiciels.
