---
site: Contrepoints
title: "Le mouvement du logiciel libre: sentinelle des libertés en ligne"
author: Farid Gueham
date: 2015-06-17
href: http://www.contrepoints.org/2015/06/17/211181-le-mouvement-du-logiciel-libre-sentinelle-des-libertes-en-ligne
tags:
- Internet
- Institutions
- Sensibilisation
- Associations
- Neutralité du Net
---

> «Il faut alerter vos sénateurs contre le flicage généralisé!» Richard Stallman
