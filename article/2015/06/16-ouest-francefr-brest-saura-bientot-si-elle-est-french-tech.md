---
site: "ouest-france.fr"
title: "Brest saura bientôt si elle est French tech..."
date: 2015-06-16
href: http://www.ouest-france.fr/brest-saura-bientot-si-elle-est-french-tech-3489481
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Innovation
- Promotion
---

> Quatorze métropoles sont en lice pour décrocher le label. Il concerne les plus actives et innovantes sur le numérique.
