---
site: lesoir.be
title: "Internet: grande victoire pour les défenseurs de la vie privée"
author: Alain Jennotte 
date: 2015-06-11
href: http://www.lesoir.be/904798/article/economie/2015-06-11/internet-grande-victoire-pour-defenseurs-vie-privee
tags:
- Internet
- Institutions
- International
- Vie privée
---

> La Cour constitutionnelle belge a annulé la loi sur la conservation des données internet qui imposait la conservation massive des activités en ligne.
