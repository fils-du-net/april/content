---
site: madmoiZelle
title: "«Faut-il apprendre à coder aux enfants?», une conférence pertinente sur une problématique d’actualité"
author: Sarah Bocelli
date: 2015-06-18
href: http://www.madmoizelle.com/apprendre-coder-enfants-conference-382905
tags:
- Partage du savoir
- Éducation
- Innovation
- Promotion
- Sciences
---

> À l’occasion de la sortie de Scratch pour les Kids, un manuel d’initiation informatique, les éditions Eyrolles et Mozilla ont organisé une conférence autour d’un thème d’actualité: faut-il apprendre à coder aux enfants?
