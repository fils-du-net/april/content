---
site: Silicon.fr
title: "RGI v2: la guerre de religion Microsoft – Open Source ravivée"
author: Reynald Fléchaux
date: 2015-06-01
href: http://www.silicon.fr/rgi-v2-retour-de-guerre-de-religion-microsoft-open-source-117711.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Interopérabilité
- Marchés publics
- RGI
- Standards
---

> La DSI de l’Etat élabore un nouveau référentiel de formats informatiques, faisant l’impasse sur OpenXML de Microsoft Office. L’Afdel monte au créneau pour dénoncer une position «dogmatique».
