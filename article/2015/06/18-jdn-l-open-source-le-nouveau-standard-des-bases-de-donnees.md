---
site: JDN
title: "L’Open Source, LE nouveau standard des bases de données?"
author: Mathieu Le Faucheur
date: 2015-06-18
href: http://www.journaldunet.com/solutions/expert/61387/l-open-source--le-nouveau-standard-des-bases-de-donnees--8239.shtml
tags:
- Administration
- Économie
- RGI
---

> Le monde informatique a ses révolutions: cloud, Big Data, Internet des objets, etc. Le logiciel libre en est une. Véritable vague de fond, il constitue un atout non négligeable pour répondre aux besoins des entreprises et lutter contre les dérives de certains acteurs en position de monopole.
