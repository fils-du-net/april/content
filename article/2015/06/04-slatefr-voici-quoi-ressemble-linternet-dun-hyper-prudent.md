---
site: Slate.fr
title: "Voici à quoi ressemble l'Internet d'un hyper prudent"
author: Juliette Harau
date: 2015-06-04
href: http://www.slate.fr/story/101631/internet-hyper-prudent
tags:
- Internet
- Vie privée
---

> La loi sur le renseignement fait craindre pour la protection de la vie privée. Mais les risques existent déjà, et certains s'en protègent mieux que vous.
