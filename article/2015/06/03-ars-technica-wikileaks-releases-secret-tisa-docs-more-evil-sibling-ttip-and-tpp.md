---
site: Ars Technica
title: "WikiLeaks releases secret TISA docs: The more evil sibling of TTIP and TPP"
author: Glyn Moody
date: 2015-06-03
href: http://arstechnica.co.uk/tech-policy/2015/06/wikileaks-releases-secret-tisa-docs-the-more-evil-sibling-of-ttip-and-tpp
tags:
- Économie
- Institutions
- ACTA
- Vie privée
---

> (le nouvel accord qui paralyse encore plus les gouvernements et citoyens) The new agreement that would hamstring governments and citizens even further.
