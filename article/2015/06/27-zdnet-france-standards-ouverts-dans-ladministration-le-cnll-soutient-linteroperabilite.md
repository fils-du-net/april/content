---
site: ZDNet France
title: "Standards ouverts dans l'administration: le CNLL soutient l'interopérabilité"
author: Thierry Noisette
date: 2015-06-27
href: http://www.zdnet.fr/actualites/standards-ouverts-dans-l-administration-le-cnll-soutient-l-interoperabilite-39821572.htm
tags:
- Entreprise
- Économie
- Interopérabilité
- Associations
- Marchés publics
- Promotion
- RGI
- Standards
---

> Le Conseil national du logiciel libre (représentant via ses adhérents 300 entreprises) soutient la définition de l'interopérabilité de la Disic. Selon un sondage, elle permettrait 3 à 5 milliards d'économies aux SI de l'administration et des entreprises françaises.
