---
site: Next INpact
title: "Le gouvernement prêt à saper l'Open Data sur les données de transport"
author: Xavier Berne
date: 2015-06-22
href: http://www.nextinpact.com/news/95504-le-gouvernement-pret-a-saper-open-data-sur-donnees-transport.htm
tags:
- Entreprise
- Internet
- Institutions
- Innovation
- Open Data
---

> Alors que le Premier ministre promettait jeudi d’inscrire les principes de l’Open Data dans la loi, son gouvernement a soutenu dans le même temps un amendement à la loi Macron qui permettra aux sociétés de transport de s’exonérer des obligations de diffusion initialement voulues par le législateur. La SNCF, Air France ou la RATP pourront en effet signer des codes de bonne conduite prévoyant entre autre le paiement de redevances.
