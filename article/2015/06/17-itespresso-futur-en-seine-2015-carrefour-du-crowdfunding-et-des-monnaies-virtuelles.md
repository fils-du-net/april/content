---
site: ITespresso
title: "Futur en Seine 2015: carrefour du crowdfunding et des monnaies virtuelles"
author: Clément Bohic
date: 2015-06-17
href: http://www.itespresso.fr/futur-en-seine-2015-carrefour-crowdfunding-monnaies-virtuelles-98958.html
tags:
- Entreprise
- Internet
- Économie
- Innovation
---

> Quels sont les marqueurs de Futur en Seine 2015? Finance 2.0, robotique, impression 3D, crypto-monnaies, objets connectés…Petite visite sur place.
