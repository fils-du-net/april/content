---
site: Libération.fr
title: "Loi renseignement: ils ont dit non"
author: Amaelle Guiton
date: 2015-06-24
href: http://www.liberation.fr/societe/2015/06/24/catherine-morin-desailly_1336483
tags:
- Internet
- Institutions
- Vie privée
---

> Ces huit élus se sont opposés au texte sur la surveillance au nom du «refus des abus de pouvoir». De gauche à droite sur la photo, extraits de leurs discours dans l’hémicycle.
