---
site: L'OBS
title: "Je suis tombé dans l’univers parallèle des «crypto-parties»"
author: Marc Meillassoux
date: 2015-06-28
href: http://rue89.nouvelobs.com/2015/06/28/suis-tombe-lunivers-parallele-crypto-parties-259995
tags:
- Internet
- Sensibilisation
- Vie privée
---

> Ces rencontres hackers-débutants peuvent rebuter par leur langage ésotérique: mais elles sont précieuses pour partager des outils de cryptographie et sécuriser les connexions internet, mails, docs et périphériques.
