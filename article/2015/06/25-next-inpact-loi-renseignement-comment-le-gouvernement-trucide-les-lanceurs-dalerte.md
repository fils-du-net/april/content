---
site: Next INpact
title: "Loi Renseignement: comment le gouvernement a trucidé les lanceurs d'alerte"
author: Marc Rees
date: 2015-06-25
href: http://www.nextinpact.com/news/95554-loi-renseignement-comment-gouvernement-a-trucide-lanceurs-d-alerte.htm
tags:
- Institutions
- Vie privée
---

> «Nous regrettons l’amendement de dernière minute du gouvernement concernant les lanceurs d’alerte, qui est tout sauf un amendement de précision. La protection des lanceurs d’alerte était pourtant une des avancées de nos travaux dans l’hémicycle!». Voilà comment Isabelle Attard, députée écologiste, a regretté le coup en douce du gouvernement à l’encontre des futurs Snowden Français.
