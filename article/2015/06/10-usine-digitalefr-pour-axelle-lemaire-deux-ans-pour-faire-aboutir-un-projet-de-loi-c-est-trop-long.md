---
site: "usine-digitale.fr"
title: "Pour Axelle Lemaire, \"deux ans pour faire aboutir un projet de loi, c’est trop long\""
author: Emmanuelle Delsol
date: 2015-06-10
href: http://www.usine-digitale.fr/article/pour-axelle-lemaire-deux-ans-pour-faire-aboutir-un-projet-de-loi-c-est-trop-long.N334854
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Innovation
---

> Les 6e rencontres parlementaires pour l’économie numérique se tenaient le 10 juin à Paris. Outre le défilé un peu trop long d’expertises non contestables dans le domaine, la ministre Axelle Lemaire, secrétaire d’Etat chargée du numérique, a rappelé sa feuille de route. Non sans que Benoît Thieulin, président du CNNum ne critique la façon dont les politiques continuent de traiter le numérique.
