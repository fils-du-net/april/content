---
site: Next INpact
title: "Loi Renseignement: la saisine constitutionnelle du président de la République"
author: Marc Rees
date: 2015-06-26
href: http://www.nextinpact.com/news/95567-loi-renseignement-saisine-constitutionnelle-president-republique.htm
tags:
- Internet
- HADOPI
- Institutions
- Associations
- Vie privée
---

> Quels sont les reproches adressés par François Hollande à sa loi sur le renseignement? Nous publions la saisine de trois pages qu’a adressée hier l’Élysée au Conseil constitutionnel. Une saisine à la fois très vaste mais qui laisse de côté plusieurs points noirs, notamment celui de la surveillance internationale.
