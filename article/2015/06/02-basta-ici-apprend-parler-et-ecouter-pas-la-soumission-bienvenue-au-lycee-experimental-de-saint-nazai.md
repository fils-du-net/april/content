---
site: Basta !
title: "«Ici, on apprend à parler et à écouter, pas la soumission»: bienvenue au lycée expérimental de Saint-Nazaire"
author: Nolwenn Weiler
date: 2015-06-02
href: http://www.bastamag.net/A-Saint-Nazaire-la-vie-revee-des-lyceens
tags:
- Éducation
---

> Imaginez une école où les cours ne sont pas obligatoires, où il n’y a ni notes ni contrôles, où les élèves participent à la gestion de leur établissement, et où il n’y a pas de violences. C’est le quotidien du lycée expérimental de Saint-Nazaire qui a ouvert ses classes il y a trente ans. Mis en place pour répondre aux manques de motivation et à l’échec scolaire, cet établissement alternatif accueille chaque année 150 élèves, dont de nombreuses «gueules cassées» de l’Education nationale.
