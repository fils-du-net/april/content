---
site: Silicon.fr
title: "Open Source et administration: le CCNum rallume l’incendie"
author: Reynald Fléchaux
date: 2015-06-19
href: http://www.silicon.fr/open-source-administration-ccnum-rallume-incendie-119578.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Interopérabilité
- Institutions
- Marchés publics
- RGI
- Standards
---

> Dans son rapport Ambition Numérique, le Conseil national du numérique préconise de donner la priorité au logiciel libre dans l’administration. Syntec Numérique et Afdel sont vent debout contre une proposition qui, à leurs yeux, sent un peu la naphtaline.
