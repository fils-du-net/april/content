---
site: Next INpact
title: "Loi Renseignement: les non-résidents mieux protégés, les lanceurs d'alerte beaucoup moins"
author: Marc Rees
date: 2015-06-23
href: http://www.nextinpact.com/news/95518-loi-renseignement-non-residents-mieux-proteges-lanceurs-d-alerte-beaucoup-moins.htm
tags:
- HADOPI
- Institutions
- Vie privée
---

> Le projet de loi sur le renseignement sera débattu puis voté par les sénateurs à partir de 16 heures, aujourd’hui. Viendra demain le tour des députés. En toute dernière ligne droite, des amendements ont été déposés pour corriger les points jugés perfectibles par le gouvernement. L'un concerne les non résidents français, l'autre les lanceurs d'alerte.
