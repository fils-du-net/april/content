---
site: Silicon.fr
title: "ODF seul format bureautique: la France tiendra-t-elle bon?"
author: Reynald Fléchaux
date: 2015-06-26
href: http://www.silicon.fr/odf-seul-format-bureautique-france-tiendra-bon-120263.html
tags:
- Entreprise
- Interopérabilité
- Marchés publics
- Promotion
- RGI
- Standards
---

> Le Conseil national du logiciel libre affiche son soutien à la DSI de l’Etat, critiquée pour sa volonté d’imposer le seul ODF comme format bureautique. Faisant l’impasse sur OpenXML de Microsoft. Un choix que la Grande-Bretagne a déjà assumé.
