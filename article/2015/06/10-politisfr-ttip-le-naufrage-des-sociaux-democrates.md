---
site: Politis.fr
title: "TTIP: le naufrage des sociaux-démocrates"
author: Pierre-Yves Baillet
date: 2015-06-10
href: http://www.politis.fr/TTIP-le-naufrage-des-sociaux,31512.html
tags:
- Économie
- Institutions
- Europe
- International
- ACTA
---

> Deux sociaux-démocrates, le président du Parlement européen, Martin Schultz, ainsi que Bernd Lange, qui préside la commission du commerce international, ont exceptionnellement ajourné le vote d’une résolution sur le traité transatlantique entre l’UE et les Etats-Unis.
