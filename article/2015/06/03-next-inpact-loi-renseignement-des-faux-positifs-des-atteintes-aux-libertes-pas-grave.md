---
site: Next INpact
title: "Loi Renseignement: des faux positifs, des atteintes aux libertés? Pas grave!"
author: Marc Rees
date: 2015-06-03
href: http://www.nextinpact.com/news/95299-loi-renseignement-faux-positifs-atteintes-aux-libertes-pas-grave.htm
tags:
- Internet
- Institutions
- ACTA
- Vie privée
---

> Hier, le Sénat a commencé l’examen du projet de loi sur le renseignement par l’inévitable discussion générale. Chacun des groupes et sénateurs a pu ainsi donner « sa » religion sur ce texte, contesté par bon nombre d’organisations de la société civile, tout comme la CNIL ou le défenseur des droits. Compte rendu.
