---
site: Next INpact
title: "Le CNNum invite le gouvernement à donner la priorité aux logiciels libres"
author: Xavier Berne
date: 2015-06-23
href: http://www.nextinpact.com/news/95515-le-cnnum-invite-gouvernement-a-donner-priorite-aux-logiciels-libres.htm
tags:
- Entreprise
- Administration
- Économie
- Interopérabilité
- April
- HADOPI
- Institutions
- Marchés publics
- Promotion
- RGI
- Standards
---

> Le moins que l’on puisse dire, c’est que le rapport remis jeudi à Manuel Valls, et qui se présente comme le fruit de plus de cinq mois de concertation citoyenne, prend clairement position en faveur du logiciel libre. Dans le sillon d'un précédent rapport sénatorial, le Conseil national du numérique préconise en effet de «mobiliser le levier de la commande publique pour mettre en avant des exigences d’interopérabilité, de standards ouverts et d’accès au code source».
