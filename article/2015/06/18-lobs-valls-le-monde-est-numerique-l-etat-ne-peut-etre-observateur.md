---
site: L'OBS
title: "Valls: «Le monde est numérique, l’Etat ne peut être observateur»"
author: Rémi Noyon
date: 2015-06-18
href: http://rue89.nouvelobs.com/2015/06/18/valls-monde-est-numerique-letat-peut-etre-observateur-259829
tags:
- Internet
- Économie
- Institutions
- Neutralité du Net
- Open Data
---

> Ce devait être l’annonce de la «stratégie numérique» de la France. Manuel Valls a détaillé jeudi midi les grandes lignes de la loi numérique, la future «loi Lemaire», qui n’en finit plus de prendre forme. Voilà des mois qu’elle est annoncée.
