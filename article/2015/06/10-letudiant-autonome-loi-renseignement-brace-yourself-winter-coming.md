---
site: L'Etudiant Autonome
title: "Loi Renseignement: Brace yourself, Winter is coming..."
author: François-Xavier Cornillet
date: 2015-06-10
href: https://letudiantautonome.fr/loi-renseignement-1006
tags:
- Internet
- Institutions
- Vie privée
---

> Hier, le Sénat a suivi l’avis de l’Assemblée Nationale, en votant la Loi Renseignement à 252 pour et 67 contre. Pour référence, l’Assemblée Nationale avait voté à 438 pour et 86 contre.
