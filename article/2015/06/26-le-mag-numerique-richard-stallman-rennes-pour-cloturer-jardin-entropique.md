---
site: Le Mag numérique
title: "Richard Stallman à Rennes pour clôturer Jardin Entropique"
author: Anthony Chénais
date: 2015-06-26
href: http://www.lemag-numerique.com/2015/06/richard-stallman-a-rennes-pour-cloturer-jardin-entropique-7601
tags:
- Associations
- Philosophie GNU
- Promotion
- Vie privée
---

> Breizh Entropy Congress et Jardin Numérique fusionnent pour créer un événement sur le numérique et la liberté, Jardin Entropique.
