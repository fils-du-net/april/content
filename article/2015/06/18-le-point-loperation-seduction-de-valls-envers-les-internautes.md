---
site: Le Point
title: "L'opération séduction de Valls envers les internautes"
date: 2015-06-18
href: http://www.lepoint.fr/high-tech-internet/projet-de-loi-sur-le-numerique-toutes-les-annonces-de-manuel-valls-18-06-2015-1937869_47.php
tags:
- Internet
- Institutions
- Neutralité du Net
---

> Le Premier ministre a annoncé vouloir affirmer le principe de la neutralité du Net et garantir un droit à la connexion pour tous.
