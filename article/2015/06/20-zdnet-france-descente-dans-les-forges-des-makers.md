---
site: ZDNet France
title: "Descente dans les forges des \"makers\""
author: Frédéric Charles
date: 2015-06-20
href: http://www.zdnet.fr/actualites/descente-dans-les-forges-des-makers-39821154.htm
tags:
- Entreprise
- Économie
- Matériel libre
- Innovation
---

> Cette semaine GreenSI a visité un des lieux des "makers", Usine IO, une forge de l'ère numérique, où on réinvente la fabrication.
