---
site: UP
title: "Ordis partagés, ordis libérés!"
author: Jeanne La Prairie
date: 2015-06-18
href: http://www.up-inspirer.fr/magazine/150618401-ordis-partages-ordis-liberes
tags:
- Économie
- Sensibilisation
- Associations
- Innovation
---

> Les logiciels libres, gratuits, créés par des communautés de chercheurs et de passionnés, permettent à certains d'accéder à l'informatique. Rencontre avec les geeks activistes qui luttent contre le gâchis et la précarité numérique en redonnant vie aux vieilles machines.
