---
site: www.francesoir.fr
title: "Axelle Lemaire: \"la confiance, c'est le socle incontournable de la construction d'un nouveau web\""
author: Maxime Macé
date: 2015-11-30
href: http://www.francesoir.fr/politique-france/axelle-lemaire-la-confiance-cest-le-socle-incontournable-de-la-construction-dun
tags:
- Administration
- Institutions
- Sciences
---

> Attendu depuis le début du quinquennat de François Hollande, le projet visant à moderniser les lois encadrant la société numérique est arrivé à son terme grâce au travail et à la ténacité de la secrétaire d'Etat au Numérique, Axelle Lemaire. Pour "FranceSoir", elle explique le cheminement de ce projet de loi et certaines de ses nombreuses dispositions.
