---
site: La Revue du Digital
title: "Quand Microsoft sauvait sa présence chez PSA à cause du coût de migration d’Excel"
date: 2015-11-20
href: http://www.larevuedudigital.com/2015/11/20/quand-microsoft-sauvait-sa-presence-chez-psa-a-cause-du-cout-de-migration-dexcel
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Interopérabilité
---

> Dans sa course à la réduction des coûts, Peugeot aurait bien voulu trancher dans le vif et se débarrasser de Microsoft sur le poste de travail de ses milliers de collaborateurs, soit entre 70 000 et 80 000 PC dans le monde. C’était entre 2008 et 2010.
