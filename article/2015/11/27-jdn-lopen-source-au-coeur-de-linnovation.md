---
site: JDN
title: "L'open source au cœur de l'innovation"
author: François Mero
date: 2015-11-27
href: http://www.journaldunet.com/solutions/expert/62879/l-open-source-au-c-ur-de-l-innovation.shtml
tags:
- Entreprise
- Économie
- Innovation
---

> Les quatre domaines suscitant le plus d’innovations aujourd’hui, le fameux «SMAC» pour Social, Mobile, Analytique et Cloud, reposent tous majoritairement sur des technologies open source.
