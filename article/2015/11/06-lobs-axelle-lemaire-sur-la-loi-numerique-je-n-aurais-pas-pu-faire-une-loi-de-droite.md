---
site: L'OBS
title: "Axelle Lemaire sur la loi numérique: «Je n’aurais pas pu faire une loi de droite»"
date: 2015-11-06
href: http://rue89.nouvelobs.com/2015/11/06/axelle-lemaire-loi-numerique-naurais-pu-faire-loi-droite-261997
tags:
- Internet
- Institutions
---

> Mort numérique, données, propriété intellectuelle, vie privée... Nous avons parlé avec Axelle Lemaire de son projet de loi numérique. Qui mêle consultation citoyenne inédite, lobbys traditionnels et sempiternelles guéguerres ministérielles.
