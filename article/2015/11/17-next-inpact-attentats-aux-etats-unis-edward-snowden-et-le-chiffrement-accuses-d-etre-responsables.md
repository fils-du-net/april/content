---
site: Next INpact
title: "Attentats: aux États-Unis, Edward Snowden et le chiffrement accusés d’être responsables"
author: Vincent Hermann
date: 2015-11-17
href: http://www.nextinpact.com/news/97357-attentats-aux-etats-unis-edward-snowden-et-chiffrement-accuses-d-etre-responsables.htm
tags:
- Internet
- Institutions
- International
- Vie privée
---

> Alors que la France se prépare à faire évoluer sa constitution pour l’adapter au contexte douloureux des évènements du 13 novembre, certains officiels américains n’ont pas tardé à réagir sur la tragédie en désignant une cible toute trouvée: Edward Snowden. Le lanceur d’alerte est accusé d’avoir permis aux terroristes de passer sous les radars du renseignement.
