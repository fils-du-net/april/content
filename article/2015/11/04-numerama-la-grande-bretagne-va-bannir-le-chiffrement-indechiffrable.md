---
site: Numerama
title: "La Grande-Bretagne va bannir le chiffrement indéchiffrable"
author: Guillaume Champeau
date: 2015-11-04
href: http://www.numerama.com/politique/129414-la-grande-bretagne-va-bannir-le-chiffrement-indechiffrable.html
tags:
- Internet
- Institutions
- International
- Vie privée
---

> Présentée ce mercredi par le gouvernement britannique, l'Investigatory Powers Bill obligerait les éditeurs d'applications à conserver les moyens de déchiffrer les communications qu'ils facilitent, pour coopérer efficacement avec la police ou les services de renseignement en cas de besoin.
