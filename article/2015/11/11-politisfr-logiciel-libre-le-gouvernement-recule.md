---
site: Politis.fr
title: "Logiciel libre: le gouvernement recule"
author: Christine Tréguier
date: 2015-11-11
href: http://www.politis.fr/Logiciel-libre-le-gouvernement,33000.html
tags:
- Entreprise
- Économie
- April
- Institutions
- Marchés publics
- Open Data
---

> Présenté fin septembre, le projet de loi pour la République numérique d’Axelle Lemaire a fait l’objet d’une consultation publique durant trois semaines. La procédure est inédite et elle a connu un franc succès en terme de participation: 8500 commentaires et contributions ont été déposés sur le site par quelque 21 000 personnes. L’ambition affichée de cette consultation était d’améliorer la loi grâce aux propositions des internautes. Mais la promesse de «co-rédaction» de la loi lancée par Axelle Lemaire a vite trouvé ses limites.
