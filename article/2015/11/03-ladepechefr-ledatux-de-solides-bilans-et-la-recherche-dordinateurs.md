---
site: LaDepeche.fr
title: "Lédatux: de solides bilans et la recherche d'ordinateurs"
date: 2015-11-03
href: http://www.ladepeche.fr/article/2015/11/03/2209498-ledatux-de-solides-bilans-et-la-recherche-d-ordinateurs.html
tags:
- Sensibilisation
- Associations
---

> On ressent un indéniable engouement pour la découverte de l'informatique, le besoin d'apprendre et de se perfectionner. La méthode d'enseignement personnalisé convient parfaitement et fait la renommée de ce club accueillant débutants et personnes confirmées.
