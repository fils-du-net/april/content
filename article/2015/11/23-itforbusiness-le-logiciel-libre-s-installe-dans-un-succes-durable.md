---
site: ITforBusiness
title: "Le logiciel libre s’installe dans un succès durable"
author: La rédaction
date: 2015-11-23
href: http://www.itforbusiness.fr/thematiques/logiciels/item/7260-le-logiciel-libre-s-installe-dans-un-succes-durable
tags:
- Entreprise
- Économie
- Innovation
---

> Le logiciel libre continue de s’imposer dans l’Hexagone. Les entreprises du secteur génèrent désormais près de 4,5 Md€, soit une augmentation de 33% par rapport à 2012. C’est ce que révèle une étude du cabinet Pierre Audoin Conseil (PAC) réalisée pour le Conseil national du logiciel libre (CNLL) et le syndicat patronal du numérique, Syntec numérique.
