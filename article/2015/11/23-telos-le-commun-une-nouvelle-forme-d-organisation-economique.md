---
site: Telos
title: "Le commun, une nouvelle forme d’organisation économique?"
author: François  Meunier
date: 2015-11-23
href: http://www.telos-eu.com/fr/politique-economique/le-commun-une-nouvelle-forme-dorganisation-economi.html
tags:
- Entreprise
- Économie
- Institutions
- Innovation
- Sciences
---

> Si Ostrom s’est occupée initialement des communs «physiques», comme par exemple les ressources en poissons, la réflexion s’est rapidement étendue à ce qu’on appelle les «communs de la connaissance ou informationnels», dont le meilleur exemple est celui des logiciels libres du type Linux ou Firefox, une nouvelle forme d’organisation de la propriété. Cela donne lieu à l’important chapitre 5, par Pierre-André Mangolte, suivi de deux autres, sur la musique à l’heure d’internet et sur InnoCentive, cette initiative de R&amp;D collaborative sans but lucratif.
