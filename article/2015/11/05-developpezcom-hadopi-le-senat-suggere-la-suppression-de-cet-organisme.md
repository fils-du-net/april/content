---
site: Developpez.com
title: "HADOPI: le Sénat suggère la suppression de cet organisme"
author: Michael Guilloux
date: 2015-11-05
href: http://www.developpez.com/actu/92122/HADOPI-le-Senat-suggere-la-suppression-de-cet-organisme
tags:
- Internet
- HADOPI
- Institutions
---

> La lutte contre la piraterie des œuvres culturelles demeure un casse-tête pour les autorités françaises qui n’ont pas encore trouvé la bonne formule pour dissuader les fraudeurs et combattre efficacement le phénomène. À la quête de solutions efficaces, le Sénat a émis des propositions en août dernier pour réformer la HADOPI (Haute Autorité pour la diffusion des œuvres et la protection des droits sur Internet).
