---
site: ZDNet France
title: "Les bons plans sécurité et vie privée d'Edward Snowden"
date: 2015-11-13
href: http://www.zdnet.fr/actualites/les-bons-plans-securite-et-vie-privee-d-edward-snowden-39828134.htm
tags:
- Vie privée
---

> La sécurité et la protection de la vie privée commencent déjà par des gestes simples. Edward Snowden et The Intercept livrent leurs astuces pour utiliser Internet et smartphone de façon plus sûre.
