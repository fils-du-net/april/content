---
site: Numerama
title: "Le CEO de Microsoft en France pour rencontrer François Hollande"
author: Julien Lausson
date: 2015-11-09
href: http://www.numerama.com/business/130083-le-pdg-de-microsoft-en-france-pour-rencontrer-francois-hollande.html
tags:
- Entreprise
- Économie
- Institutions
---

> Le CEO de Microsoft, Satya Nadella, est en France ce lundi. Il doit rencontrer le président de la République et annoncer des investissements dans les startups et l'éducation nationale.
