---
site: Next INpact
title: "Hadopi: le cinéma va injecter «l'amende automatique» via la loi Création"
author: Marc Rees
date: 2015-11-09
href: http://www.nextinpact.com/news/97243-hadopi-cinema-va-injecter-l-amende-automatique-via-loi-creation.htm
tags:
- Internet
- HADOPI
- Institutions
- Droit d'auteur
---

> Après adoption à l’Assemblée nationale, le projet de loi Création doit maintenant être examiné puis voté par les sénateurs. Si l’agenda est encore incertain, les vœux des ayants droit se précisent.
