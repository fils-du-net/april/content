---
site: Silicon.fr
title: "Loi République numérique, une V2 teintée de collaboratif citoyen"
author: Ariane Beky
date: 2015-11-06
href: http://www.silicon.fr/loi-republique-numerique-lemaire-gouvernement-130940.html
tags:
- Institutions
- Droit d'auteur
- Open Data
---

> Le projet de loi Lemaire compte une quarantaine d’articles, contre 30 auparavant. Certaines contributions citoyennes retenues sont moins neutres que d’autres pour l’industrie.
