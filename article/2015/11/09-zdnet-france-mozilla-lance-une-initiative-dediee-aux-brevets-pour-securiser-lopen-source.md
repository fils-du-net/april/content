---
site: ZDNet France
title: "Mozilla lance une initiative dédiée aux brevets pour sécuriser l'Open Source"
date: 2015-11-09
href: http://www.zdnet.fr/actualites/mozilla-lance-une-initiative-dediee-aux-brevets-pour-securiser-l-open-source-39827874.htm
tags:
- Associations
- Brevets logiciels
---

> La fondation Mozilla a inauguré un nouveau programme destiné au rachat et au dépôt de brevet: la société veut pouvoir proposer sa licence afin d’assurer une plus grande protection des projets open source à l’égard de la loi du droit de la propriété intellectuelle.
