---
site: Les Echos
title: "Le «hackerspace», une bulle d’expérimentation managériale, Conduite du changement"
author: Marie-Sophie Ramspacher
date: 2015-11-25
href: http://business.lesechos.fr/directions-ressources-humaines/management/conduite-du-changement/021504928919-le-hackerspace-une-bulle-d-experimentation-manageriale-204935.php
tags:
- Entreprise
- Économie
- Innovation
---

> Le Stylo d’Or, remis chaque année par la revue «Personnel» à un auteur inspirant les DRH, a été attribué hier à Michel Lallement pour son dernier ouvrage «L’âge du faire» qui soumet le modèle du «hackerspace» aux managers en quête...
