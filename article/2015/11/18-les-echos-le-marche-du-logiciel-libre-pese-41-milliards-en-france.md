---
site: Les Echos
title: "Le marché du logiciel libre pèse 4,1 milliards en France"
author: Fabienne Schmitt
date: 2015-11-18
href: http://www.lesechos.fr/tech-medias/hightech/021490018237-le-marche-du-logiciel-libre-pese-41-milliards-en-france-1176333.php
tags:
- Entreprise
- Économie
- Interopérabilité
- Innovation
---

> L’open source s’impose de plus en plus en France. Alors qu’il était à peine émergent il y a quinze ans, ce marché pèse aujourd’hui 4,1 milliards d’euros, soit une augmentation de 33% par rapport à 2012, d’après une étude réalisée par Pierre Audoin Consultants pour le Conseil national du logiciel libre (CNLL) et le Syntec numérique, à l’occasion de la première édition du Paris Open Source Summit qui se tient jusqu’à jeudi.
