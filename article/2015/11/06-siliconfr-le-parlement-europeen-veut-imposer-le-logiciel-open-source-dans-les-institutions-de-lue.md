---
site: Silicon.fr
title: "Le Parlement européen veut imposer le logiciel Open Source dans les institutions de l'UE"
author: Ariane Beky
date: 2015-11-06
href: http://www.silicon.fr/parlement-europeen-logiciel-open-source-surveillance-130838.html
tags:
- April
- Institutions
- Marchés publics
- Promotion
- RGI
- Europe
---

> Dans une résolution, le Parlement européen condamne la surveillance de masse et prône le remplacement systématique des logiciels propriétaires par des logiciels ouverts dans toutes les institutions de l’Union européenne. Les partisans du logiciel libre saluent un «signal fort».
