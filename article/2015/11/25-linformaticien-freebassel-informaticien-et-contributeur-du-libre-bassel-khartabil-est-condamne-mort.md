---
site: L'Informaticien
title: "#FreeBassel: informaticien et contributeur du libre, Bassel Khartabil est condamné à mort en Syrie"
author: Emilien Ercolani
date: 2015-11-25
href: http://www.linformaticien.com/actualites/id/38651/freebassel-informaticien-et-contributeur-du-libre-bassel-khartabil-est-condamne-a-mort-en-syrie.aspx
tags:
- Institutions
- International
---

> Emprisonné depuis 3 ans en Syrie, l’informaticien Bassel Khartabil est condamné à mort en Syrie. Il est à l’origine de nombreux projets, comme la reconstitution en 3D de la cité antique de Palmyre, mais aussi les Creative Commons en Syrie et un contributeur à Wikipedia, Ubuntu ou encore Mozilla.
