---
site: L'Opinion
title: "Tristan Nitot: «Combattre la cryptographie n'affaiblira pas les djihadistes»"
author: Hugo Sedouramane
date: 2015-11-17
href: http://www.lopinion.fr/edition/economie/tristan-nitot-combattre-cryptographie-n-affaiblira-pas-djihadistes-90772
tags:
- Internet
- Institutions
- Vie privée
---

> Les États-Unis et la Russie s'inquiètent de l'utilisation d’applications comme Telegram ou Signal qui permettent aux utilisateurs - donc potentiellement aux terroristes - d'échanger des messages cryptés. En France, on semble naviguer dans un flou juridique...
