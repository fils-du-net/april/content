---
site: ZDNet France
title: "Aucun logiciel n'est à l'abri de l'espionnage, dixit un ex-espion"
date: 2015-11-13
href: http://www.zdnet.fr/actualites/aucun-logiciel-n-est-a-l-abri-de-l-espionnage-dixit-un-ex-espion-39828120.htm
tags:
- Institutions
- Vie privée
---

> Selon un ancien de la NSA, William Binney, l'agence de renseignement dispose de tellement de ressources qu'il est pratiquement impossible de se protéger contre la surveillance par les Etats-Unis, que le logiciel soit Open Source ou propriétaire.
