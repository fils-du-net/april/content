---
site: "cio-online.com"
title: "Vive l'Europe Libre!"
author: Bertrand Lemaire
date: 2015-11-04
href: http://www.cio-online.com/actualites/lire-vive-l-europe-libre%A0-8000.html
tags:
- April
- Institutions
- Marchés publics
- RGI
- Europe
---

> Le Parlement Européen a adopté une longue motion condamnant la cybersurveillance généralisée et encourageant le logiciel libre et souverain.
