---
site: L'OBS
title: "Journée d’hommage au militant de l’Internet Aaron Swartz"
author: Tom Wersinger
date: 2015-11-07
href: http://rue89.nouvelobs.com/2015/11/07/journee-dhommage-militant-linternet-aaron-swartz-261998
tags:
- Internet
- Partage du savoir
- Institutions
---

> Ce week-end est organisée la troisième journée internationale d’hommage à Aaron Swartz, développeur surdoué, blogueur et activiste américain qui s’est suicidé en 2013, un mois avant son procès pour fraude électronique.
