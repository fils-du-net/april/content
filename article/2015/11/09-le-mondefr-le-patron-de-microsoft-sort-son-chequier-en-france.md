---
site: Le Monde.fr
title: "Le patron de Microsoft sort son chéquier en France"
author: Sandrine Cassini 
date: 2015-11-09
href: http://www.lemonde.fr/economie/article/2015/11/09/microsoft-sort-son-chequier-et-investit-a-son-tour-en-france_4805509_3234.html
tags:
- Entreprise
- Économie
- April
- Institutions
- Innovation
---

> Satya Nadella rencontre François Hollande lundi pour lui annoncer un investissement de 83 millions d’euros dans des start-up et un partenariat avec l’éducation nationale.
