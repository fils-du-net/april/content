---
site: Silicon.fr
title: "Microsoft: le nouveau meilleur ami de l’Open Source?"
author: Reynald Fléchaux
date: 2015-11-20
href: http://www.silicon.fr/microsoft-nouveau-meilleur-ami-open-source-132011.html
tags:
- Entreprise
- Informatique en nuage
---

> Présent sur le Paris Open Source Summit, Microsoft est-il devenu un réel allié des communautés Open Source? L’éditeur, jadis meilleur ennemi du libre, fait étalage de sa bonne volonté au travers de ses liens avec Docker et Red Hat.
