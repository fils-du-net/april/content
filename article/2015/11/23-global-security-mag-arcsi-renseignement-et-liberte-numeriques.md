---
site: Global Security Mag
title: "ARCSI: renseignement et liberté numériques"
author: Emmanuelle Lamandé
date: 2015-11-23
href: http://www.globalsecuritymag.fr/ARCSI-renseignement-et-liberte,20151127,57905.html
tags:
- Internet
- Institutions
- Vie privée
---

> A l’occasion de ses 9èmes rencontres, l’ARCSI (Association des Réservistes du Chiffre et de la Sécurité de l’Information) a souhaité mettre en avant un thème souvent controversé, mais ô combien fondamental: la protection des libertés individuelles et de la vie privée en regard de la sécurité et du renseignement. Pour ce faire, l’association a fait appel à d’éminents experts œuvrant chaque jour pour le respect de nos libertés et de notre sécurité sur le territoire.
