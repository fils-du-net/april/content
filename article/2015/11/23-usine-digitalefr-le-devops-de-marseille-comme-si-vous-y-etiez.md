---
site: "usine-digitale.fr"
title: "Le DevOps de Marseille comme si vous y étiez..."
author: Jean-Christophe Barla
date: 2015-11-23
href: http://www.usine-digitale.fr/article/le-devops-de-marseille-comme-si-vous-y-etiez.N364295
tags:
- Entreprise
- Innovation
---

> Organisé à Marseille par la société Treeptik, le salon DevOps D-DAY 2015 a attiré plus de 250 professionnels du développement ("Dev") et de l'exploitation ("Ops") d'applications et du cloud. Entre conférences et nouvelles solutions, l'événement s'est efforcé de démontrer l'intérêt d'une évolution des pratiques pour accélérer la transition numérique des entreprises.
