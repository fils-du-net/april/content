---
site: internet ACTU.net
title: "Brève histoire de l’empowerment: à la reconquête du sens politique"
author: Valérie Peugeot
date: 2015-11-13
href: http://www.internetactu.net/2015/11/13/breve-histoire-de-lempowerment-a-la-reconquete-du-sens-politique
tags:
- Internet
- Économie
- Institutions
- Associations
---

> Le terme d’empowerment fait partie de ces mots qui font irruption dans notre langue depuis une demi-décennie et se disséminent à grande vitesse. Sans doute peut-on en partie expliquer ce succès par son caractère polysémique, une polysémie qui facilite son appropriation dans des contextes et par des acteurs différents, qui explique la difficulté à le traduire et justifie la longévité du terme anglais. L’analyse sémiologique du terme reste à faire.
