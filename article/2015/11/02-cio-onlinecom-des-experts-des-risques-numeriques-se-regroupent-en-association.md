---
site: "cio-online.com"
title: "Des experts des risques numériques se regroupent en association"
author: Bertrand Lemaire
date: 2015-11-02
href: http://www.cio-online.com/actualites/lire-des-experts-des-risques-numeriques-se-regroupent-en-association-7987.html
tags:
- Entreprise
- Associations
---

> Des avocats, des assureurs et des prestataires en informatique se sont regroupés en association pour promouvoir la gestion des risques numériques. Les entreprises utilisatrices et les individus sont invités à les rejoindre.
