---
site: Silicon.fr
title: "Innovation: la leçon donnée par le monde du libre"
author: Jean-Luc Beylat
date: 2015-11-05
href: http://www.silicon.fr/ouverture-lecon-innovation-monde-libre-130665.html
tags:
- Entreprise
- Promotion
---

> A quelques jours de l’ouverture du premier Paris Open Source Summit, Jean-Luc Beylat, président du pôle de compétitivité Systematic, se penche sur les enseignements du logiciel libre en matière d’innovation ouverte. Pour lui, une culture nouvelle qui colle bien à la réalité du monde d’aujourd’hui, où la vitesse prime sur tout le reste.
