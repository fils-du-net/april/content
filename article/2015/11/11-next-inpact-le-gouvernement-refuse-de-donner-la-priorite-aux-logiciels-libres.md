---
site: Next INpact
title: "Le gouvernement refuse de donner la priorité aux logiciels libres"
author: Xavier Berne
date: 2015-11-11
href: http://www.nextinpact.com/news/97257-le-gouvernement-refuse-donner-priorite-aux-logiciels-libres.htm#/page/1
tags:
- Entreprise
- Économie
- April
- Institutions
- Promotion
---

> Alors que les participants à la consultation relative au projet de loi numérique ont massivement demandé à ce que l’État privilégie l’utilisation de logiciels libres au sein de l’administration, le gouvernement n’a – sans grande surprise – pas exaucé ce souhait partagé par plusieurs milliers d’internautes. Une «reculade» pour certains, le choix d'actionner d’autres leviers selon l’exécutif.
