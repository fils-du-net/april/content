---
site: Next INpact
title: "Cinq questions sur le projet de loi numérique"
author: Xavier Berne
date: 2015-11-11
href: http://www.nextinpact.com/news/97227-projet-loi-numerique-on-fait-point.htm
tags:
- Internet
- Institutions
---

> Voilà plusieurs mois que le projet de loi numérique d'Axelle Lemaire fait parler de lui. L’occasion pour nous de vous proposer un point sur la situation, sous forme de questions-réponses.
