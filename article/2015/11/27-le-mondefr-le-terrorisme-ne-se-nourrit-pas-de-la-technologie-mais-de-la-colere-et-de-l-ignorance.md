---
site: Le Monde.fr
title: "«Le terrorisme ne se nourrit pas de la technologie, mais de la colère et de l’ignorance»"
date: 2015-11-27
href: http://www.lemonde.fr/pixels/article/2015/11/27/le-terrorisme-ne-se-nourrit-pas-de-la-technologie-mais-de-la-colere-et-de-l-ignorance_4818981_4408996.html
tags:
- Internet
- Institutions
- Innovation
- Promotion
- Sciences
- Vie privée
---

> Nadim Kobeissi est doctorant en cryptographie appliquée à l’Inria, spécialisé dans les messageries sécurisées. Il démonte le lien entre terrorisme et chiffrement des données.
