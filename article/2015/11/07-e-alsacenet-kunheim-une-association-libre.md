---
site: "e-alsace.net"
title: "Kunheim: une association libre"
date: 2015-11-07
href: http://www.e-alsace.net/index.php/smallnews/detail?newsId=19791
tags:
- Associations
- Promotion
---

> Une association baptisée Linux-Kunheim est née dans le Haut-Rhin. Elle propose divers services liés aux logiciels libres.
