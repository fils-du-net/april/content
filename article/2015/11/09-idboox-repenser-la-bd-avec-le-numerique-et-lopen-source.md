---
site: IDBoox
title: "Repenser la BD avec le numérique et l'Open Source"
author: Elizabeth Sutton
date: 2015-11-09
href: http://www.idboox.com/interviews/interview-repenser-la-bd-avec-le-numerique-et-lopen-source
tags:
- Partage du savoir
---

> La bande dessinée est un segment éditorial très apprécié par les petits et les grands. Avec le numérique, plusieurs expérimentations sont faites pour porter la BD sur écran.
