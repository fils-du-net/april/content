---
site: JDN
title: "Le logiciel libre représente 50 000 emplois et 4 milliards d'euros en France"
author: Antoine Crochet
date: 2015-11-18
href: http://www.journaldunet.com/solutions/dsi/1166651-le-secteur-du-logiciel-libre-represente-50-000-emplois-en-france
tags:
- Entreprise
- Économie
---

> Les chiffres sont révélés par une étude de PAC réalisée pour le compte du Conseil National du Logiciel Libre et du Syntec Numérique. Elle est publiée lors du Paris Open Source Summit.
