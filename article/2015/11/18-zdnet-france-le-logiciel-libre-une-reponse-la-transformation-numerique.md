---
site: ZDNet France
title: "Le logiciel libre, une réponse à la transformation numérique?"
author: Christophe Auffray
date: 2015-11-18
href: http://www.zdnet.fr/actualites/le-logiciel-libre-une-reponse-a-la-transformation-numerique-39828326.htm
tags:
- Entreprise
- Économie
- Innovation
---

> Le logiciel Libre et Open Source, c'est en France un marché de 4,1 milliards d'euros selon une étude PAC. Et sa croissance est forte et rapide grâce à son utilisation dans le cadre de la transformation numérique des entreprises et du développement des projets dans les domaines des SMACS.
