---
site: L'Express.fr
title: "Réinventer les biens communs"
author: Christophe Dutheil
date: 2015-11-26
href: http://www.lexpress.fr/emploi/business-et-sens/reinventer-les-biens-communs_1739819.html
tags:
- Économie
- Partage du savoir
- Associations
---

> Avec l'essor de l'économie collaborative, la notion de biens communs (également appelés communaux ou "?commons?" en anglais?") est plus que jamais sous le feu des projecteurs. Il s'agit, dixit Wikipedia, de "?l'ensemble des ressources, matérielles ou non, relevant d'une appropriation, d'un usage et d'une exploitation collectifs?".
