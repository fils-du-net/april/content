---
site: ZDNet France
title: "Logiciel libre: à Toulouse, le Capitole et le Bazar"
author: Louis Adam
date: 2015-11-24
href: http://www.zdnet.fr/actualites/logiciel-libre-a-toulouse-le-capitole-et-le-bazar-39828558.htm
tags:
- Associations
- Promotion
---

> Le Capitole du libre aurait pu faire partie de la longue liste d’événements annulés suite aux attentats de Paris, mais les militants toulousains ont décidé de maintenir tant bien que mal à la manifestation, renommée pour l’occasion Bazar du Libre. ZDNet est passé faire un tour.
