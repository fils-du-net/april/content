---
site: Numerama
title: "Pourquoi Mozilla va acheter et déposer des brevets"
author: Guillaume Champeau
date: 2015-11-07
href: http://www.numerama.com/business/129980-pourquoi-mozilla-va-acheter-et-deposer-des-brevets.html
tags:
- Entreprise
- Associations
- Brevets logiciels
---

> Mozilla a annoncé vendredi un programme d'acquisition et de dépôt de brevets, avec l'intention de les placer immédiatement sous des licences libres virales, qui inciteraient les éditeurs de logiciels propriétaires à rejoindre le mouvement du libre.
