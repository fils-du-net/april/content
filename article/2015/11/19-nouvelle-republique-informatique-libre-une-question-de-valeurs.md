---
site: Nouvelle République
title: "Informatique libre: Une question de valeurs"
date: 2015-11-19
href: http://www.lanouvellerepublique.fr/Loir-et-Cher/Actualite/24-Heures/n/Contenus/Articles/2015/11/19/Informatique-libre-Une-question-de-valeurs-2537478
tags:
- April
- Sensibilisation
- Associations
---

> La Journée régionale du libre investit Romorantin ce samedi. Au-delà de la technique informatique, Solix espère promouvoir la notion de liberté.
