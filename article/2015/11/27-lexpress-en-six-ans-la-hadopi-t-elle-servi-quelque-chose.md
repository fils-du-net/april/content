---
site: L'Express
title: "En six ans, la Hadopi a-t-elle servi à quelque chose?"
author: Raphaële Karayan
date: 2015-11-27
href: http://lexpansion.lexpress.fr/high-tech/en-six-ans-la-hadopi-a-t-elle-servi-a-quelque-chose_1739525.html
tags:
- Internet
- HADOPI
- Institutions
---

> Comment la Haute autorité a-t-elle modifié le paysage culturel numérique français? Les internautes piratent-ils moins de films et de musique? Bilan, cinq ans après l'envoi des premiers avertissements.
