---
site: Le Penthièvre
title: "Quentin redonne du souffle à vos ordis"
author: yann.andre
date: 2015-11-03
href: http://www.lepenthievre.fr/2015/11/12/le-bon-tuyau-quentin-redonnedu-souffle-a-vos-ordis
tags:
- Économie
- Associations
- Promotion
---

> Votre ordinateur est à bout de souffle? C'est peut-être le système d'exploitation qui est trop gourmand. À la MJC de jeunes Briochins savent comment faire. Et c'est gratuit.
