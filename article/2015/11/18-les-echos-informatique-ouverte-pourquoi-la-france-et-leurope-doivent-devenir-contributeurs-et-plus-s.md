---
site: Les Echos
title: "Informatique ouverte: pourquoi la France et l'Europe doivent devenir contributeurs et plus seulement consommateurs?"
author: Cédric Thomas
date: 2015-11-18
href: http://www.lesechos.fr/idees-debats/cercle/cercle-144214-informatique-ouverte-pourquoi-la-france-et-leurope-doivent-devenir-contributeurs-et-plus-seulement-consommateurs-1176308.php
tags:
- Entreprise
- Économie
- Innovation
- Informatique en nuage
---

> En France et dans de nombreux autres pays, les gouvernements ont compris que l'open source facilite la modernisation des systèmes d'information et favorise une indépendance informatique maîtrisée. En Asie, les nouveaux géants de l'e-commerce deviennent des champions de la collaboration logicielle. Consommer de l'open source ne suffit pas et sans réaction et volonté politique avérée et fédératrice, l’Europe et la France seront vite dépassées…
