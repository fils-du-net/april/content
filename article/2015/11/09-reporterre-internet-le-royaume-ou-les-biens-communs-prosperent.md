---
site: Reporterre
title: "Internet, le royaume où les biens communs prospèrent"
author: Émilie Massemin
date: 2015-11-09
href: http://www.reporterre.net/Internet-le-royaume-ou-les-biens-communs-prosperent
tags:
- April
- Sensibilisation
- Associations
- ACTA
---

> Aux antipodes des logiciels dits «propriétaires», les «libristes» défendent des ressources informatiques développées collectivement et utilisables par tous. Loin de se contenter d’enchaîner les lignes de codes, ces passionnés sont devenus des acteurs incontournables des combats citoyens.
