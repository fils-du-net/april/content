---
site: Techniques de l'Ingénieur
title: "Le B-A BA de la sécurité informatique selon Edward Snowden"
author: Philippe Richard
date: 2015-11-19
href: http://www.techniques-ingenieur.fr/actualite/articles/le-b-a-ba-de-la-securite-informatique-selon-edward-snowden-29997
tags:
- Internet
- Institutions
- Vie privée
---

> Depuis les révélations d’Edward Snowden, le grand public, les hommes politiques et les entreprises découvrent que toutes les communications sont sur écoute.
