---
site: 20minutes.fr
title: "Loi numérique: Ce que le gouvernement a retenu, ce qu'il a rejeté"
author: Nicolas Beunaiche
date: 2015-11-06
href: http://www.20minutes.fr/societe/1725315-20151106-projet-loi-numerique-gouvernement-suivi-avis-francais
tags:
- Internet
- Institutions
- Associations
- Promotion
---

> Pour la première fois, le gouvernement a fait appel aux citoyens français pour co-écrire le projet de loi sur le numérique. Une iniative portée par Axelle Lemaire...
