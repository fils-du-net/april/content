---
site: Le Monde Informatique
title: "L'Europe condamne la cybersurveillance et encourage le logiciel libre"
author: Bertrand Lemaire
date: 2015-11-05
href: http://www.lemondeinformatique.fr/actualites/lire-l-europe-condamne-la-cybersurveillance-et-encourage-le-logiciel-libre-62888.html
tags:
- Administration
- April
- Institutions
- RGI
- Europe
- Vie privée
---

> Données personnelles: Le Parlement Européen a adopté une longue motion condamnant la cybersurveillance généralisée et encourageant le logiciel libre et souverain.
