---
site: Next INpact
title: "Numérique: le projet de loi Lemaire 2.0 dévoilé en fin de semaine"
author: Marc Rees
date: 2015-11-02
href: http://www.nextinpact.com/news/97132-numerique-projet-loi-lemaire-2-0-devoile-en-fin-semaine.htm
tags:
- Internet
- April
- Institutions
- Associations
---

> Sur France Inter, ce matin, Axelle Lemaire a annoncé que son projet de loi sur le numérique sera présenté en fin de semaine. Avant cela, mercredi, un dernier arbitrage ministériel viendra trancher les éventuels points de désaccord.
