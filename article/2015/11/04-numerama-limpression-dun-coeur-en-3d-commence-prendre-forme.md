---
site: Numerama
title: "L'impression d'un cœur en 3D commence à prendre forme"
author: Julien Lausson
date: 2015-11-04
href: http://www.numerama.com/sciences/128942-limpression-dun-coeur-3d-commence-a-prendre-forme.html
tags:
- Matériel libre
- Innovation
- Sciences
---

> Enfin, cerise sur le gâteau, l’outil s’appuie sur des logiciels libres. «Non seulement le coût est faible, mais en utilisant un logiciel open-source, nous avons accès aux paramètres d’impression pour les affiner, nous pouvons optimiser ce que nous faisons et maximiser la qualité de ce que nous imprimons», détaille Adam Feinberg
