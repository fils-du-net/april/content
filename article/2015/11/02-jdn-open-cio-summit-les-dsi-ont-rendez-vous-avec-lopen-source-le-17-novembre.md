---
site: JDN
title: "Open CIO Summit: les DSI ont rendez-vous avec l'Open Source le 17 novembre"
author: Antoine Crochet-Damais
date: 2015-11-02
href: http://www.journaldunet.com/solutions/dsi/1165781-open-cio-summit-2015-les-dsi-ont-rendez-vous-avec-l-open-source-le
tags:
- Entreprise
- Économie
---

> Proposant un espace d'échange sur l'open source, l'événement est organisé dans le cadre du Paris Open Source Summit. Il se tiendra cette année au ministère de l’Économie.
