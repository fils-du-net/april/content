---
site: Next INpact
title: "Domaine commun informationnel: les ayants droit marquent des points à Matignon"
author: Marc Rees
date: 2015-11-05
href: http://www.nextinpact.com/news/97205-domaine-commun-informationnel-ayants-droit-marquent-points-a-matignon.htm
tags:
- Institutions
- Associations
- Droit d'auteur
---

> Ce matin à Matignon, les ayants droit ont finalement eu (presque) gain de cause sur l’article 8 du projet de loi Lemaire, celui sur la définition positive du domaine commun informationnel. Le texte va être retiré, pour l'instant.
