---
site: BFMtv
title: "L'open source accompagne l'innovation collaborative"
author: Olivier Laffargue
date: 2015-11-27
href: http://bfmbusiness.bfmtv.com/01-business-forum/l-open-source-accompagne-l-innovation-collaborative-933499.html
tags:
- Entreprise
- Économie
- Innovation
- Informatique en nuage
---

> Apparu il y a une trentaine d'années, comme une pratique alternative propre au monde spécialisé des programmeurs, le logiciel libre a évolué pour devenir aujourd'hui un des principaux véhicules de l'innovation collaborative dans l'industrie des technologies numériques.
