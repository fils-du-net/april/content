---
site: Libération.fr
title: "La prothèse bionique imprimée à la maison"
author: Amandine Cailhol
date: 2015-12-27
href: http://www.liberation.fr/futurs/2015/12/27/la-prothese-bionique-imprimee-a-la-maison_1423138
tags:
- Économie
- Partage du savoir
- Matériel libre
- Innovation
---

> Nicolas Huchet, alias Bionicohand, ne vise pas le gratuit, mais le bas coût. Et surtout le «faire au lieu d’acheter» grâce à l’open source, à l’esprit collaboratif et au «do it yourself», la philosophie du «faire soi-même».
