---
site: Next INpact
title: "Modernisation du droit d'auteur en Europe, levée de boucliers en France"
author: Marc Rees
date: 2015-12-15
href: http://www.nextinpact.com/news/96773-modernisation-droit-d-auteur-en-europe-levee-boucliers-en-france.htm
tags:
- Entreprise
- Économie
- Institutions
- Droit d'auteur
- Europe
---

> Le plan d’action de modernisation de la Commission européenne a suscité de fraiches réactions de la part du ministère de la Culture et des ayants droit français. Tour d’horizon.
