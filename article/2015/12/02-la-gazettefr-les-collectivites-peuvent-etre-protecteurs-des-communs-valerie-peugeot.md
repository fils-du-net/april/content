---
site: La gazette.fr
title: "«Les collectivités peuvent être protecteurs des communs» – Valérie Peugeot"
author: Romain Mazon
date: 2015-12-02
href: http://www.lagazettedescommunes.com/418463/les-collectivites-peuvent-etre-protecteurs-des-communs-valerie-peugeot
tags:
- Économie
- Institutions
---

> Utilisée au Moyen-Age, la notion de biens communs revient en force, en ce début de XXIè siècle, portée par le secteur du numérique et ses usages de partage. La dynamique et l'agilité des communautés du web qui co-construisent et développent de nouveaux services secoue sérieusement les tenants des «vieux» concepts de propriété et de droits d'auteur. Mais la notion de «biens communs», ou «communs» bouscule aussi celle de services publics, et ceux qui les rendent, élus et fonctionnaires. Pour autant, ces notions s'affrontent-elles, ou, au contraire, se complètent-elles?
