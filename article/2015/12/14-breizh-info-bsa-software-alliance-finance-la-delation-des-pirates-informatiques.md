---
site: Breizh Info
title: "BSA, the Software alliance finance la délation des pirates informatiques"
date: 2015-12-14
href: http://www.breizh-info.com/36190/actualite-economique/the-softwara-alliance-finance-la-delation-des-pirates-informatiques
tags:
- Entreprise
- Économie
- Sensibilisation
- Licenses
---

> Les multinationales américaines partent en guerre contre le piratage informatique en France. Après la loi Hadopi contre le piratage informatique (de la musique et du cinéma essentiellement) adoptée il y a plusieurs années par le gouvernement, c’est la société américaine BSA qui mène campagne notamment via les réseaux sociaux pour appeler à la dénonciation (avec une rémunération à la clé) de ceux qui possèdent des logiciels pirates.
