---
site: ZDNet France
title: "Education nationale: le partenariat avec Microsoft scandalise les libristes"
author: Thierry Noisette
date: 2015-12-03
href: http://www.zdnet.fr/actualites/education-nationale-le-partenariat-avec-microsoft-scandalise-les-libristes-39829272.htm
tags:
- Entreprise
- Logiciels privateurs
- April
- Institutions
- Associations
- Éducation
---

> L'accord du ministère de Najat Vallaud-Belkacem avec Microsoft, qui formera les enseignants et à travers eux leurs élèves aux logiciels et solutions de l'éditeur de Redmond, provoque la colère.
