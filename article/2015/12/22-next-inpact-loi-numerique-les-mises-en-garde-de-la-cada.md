---
site: Next INpact
title: "Loi Numérique: les mises en garde de la CADA"
author: Xavier Berne
date: 2015-12-22
href: http://www.nextinpact.com/news/97805-loi-numerique-mises-en-garde-cada.htm
tags:
- Administration
- Institutions
- Open Data
---

> Restée jusqu’ici très discrète (refusant notamment nos demandes d’interview), la Commission d’accès aux documents administratifs est finalement sortie de son silence vendredi 18 décembre, en publiant – tout comme la CNIL ou l’ARCEP – son avis sur l’avant-projet de loi numérique. Tout en saluant plusieurs avancées, l’institution a néanmoins appelé le gouvernement à revoir sa copie sur certains points.
