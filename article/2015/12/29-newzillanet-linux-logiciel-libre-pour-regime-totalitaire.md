---
site: NewZilla.net
title: "Linux, logiciel libre pour régime totalitaire"
author: Philippe Crouzillacq
date: 2015-12-29
href: http://www.newzilla.net/2015/12/29/linux-un-logiciel-libre-au-service-dun-regime-totalitaire
tags:
- Institutions
- Informatique-deloyale
- International
- Vie privée
---

> En Corée du Nord, le régime de Pyongyang dirigé par le sémillant Kim Jong-un, a développé Red Star OS son propre système d’exploitation pour ordinateur, basé sur une distribution Linux. Au programme, 1984… comme si vous y étiez!
