---
site: clubic
title: "Debian: la communauté en deuil après le décès de Ian Murdock"
author: Guillaume Belfiore
date: 2015-12-31
href: http://www.clubic.com/linux-os/actualite-790870-debian-communaute-deuil-deces-ian-murdock.html
tags:
- Philosophie GNU
---

> En cette période de fête de fin d'année, Ian Murdock, le fondateur du projet Debian, est décédé. La communauté lui rend hommage.
