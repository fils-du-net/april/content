---
site: infoDSI
title: "Quinze organisations dénoncent l'accord entre Microsoft et le ministère de l'Education nationale"
date: 2015-12-03
href: http://www.infodsi.com/articles/159878/quinze-organisations-denoncent-accord-entre-microsoft-ministere-education-nationale.html
tags:
- Entreprise
- April
- Institutions
- Associations
- Éducation
---

> Lundi 30 novembre 2015, la ministre de l'Éducation nationale, de l’Enseignement supérieur et de la recherche Najat Vallaud-Belkacem a annoncé la signature d'un accord de partenariat entre Microsoft et son ministère. Microsoft s'engage à investir sur 18 mois quelque 13 millions d'euros dédiés à l'accompagnement des enseignants, à «la mise à disposition de plateformes collaboratives», ainsi qu'à l'apprentissage du code informatique.
