---
site: l'Humanité.fr
title: "Petits-fours, hôtesses et logiciels… le lobbying très bien rodé de Microsoft"
author: Rémi Boulle
date: 2015-12-18
href: http://www.humanite.fr/petits-fours-hotesses-et-logiciels-un-lobbying-tres-bien-rode-593380
tags:
- Entreprise
- Logiciels privateurs
- April
- Institutions
- Éducation
- Promotion
---

> Professeur de mathématiques et vice-président de l’Association pour la promotion et la recherche en informatique libre (April), Rémi Boulle milite depuis des années pour le logiciel libre à l’école. Il témoigne de l’intense lobbying du géant américain à toutes les échelles du ministère.
