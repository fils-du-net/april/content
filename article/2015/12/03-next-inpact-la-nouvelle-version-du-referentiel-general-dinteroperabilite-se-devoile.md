---
site: Next INpact
title: "La nouvelle version du «référentiel général d'interopérabilité» se dévoile"
author: Xavier Berne
date: 2015-12-03
href: http://www.nextinpact.com/news/97563-la-nouvelle-version-referentiel-general-d-interoperabilite-se-devoile.htm
tags:
- Administration
- Interopérabilité
- April
- Institutions
- RGI
- Standards
---

> La publication du nouveau «référentiel général d’interopérabilité» applicable aux administrations françaises semble plus que jamais imminente. La France vient en effet de notifier son texte à Bruxelles (PDF), dévoilant pour l’occasion les pistes retenues par l’exécutif.
