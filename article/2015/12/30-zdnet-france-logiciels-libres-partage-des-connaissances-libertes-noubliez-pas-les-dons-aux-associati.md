---
site: ZDNet France
title: "Logiciels libres, partage des connaissances, libertés: n'oubliez pas les dons aux associations"
author: Thierry Noisette
date: 2015-12-30
href: http://www.zdnet.fr/actualites/logiciels-libres-partage-des-connaissances-libertes-n-oubliez-pas-les-dons-aux-associations-39830416.htm
tags:
- Économie
- April
- Sensibilisation
- Associations
- Promotion
- Europe
---

> Plus que 24 heures pour les dons déductibles des impôts cette année, l'occasion d'un rappel: pour Wikimédia France Framasoft et bien d'autres associations, le soutien passe (aussi) par une contribution financière.
