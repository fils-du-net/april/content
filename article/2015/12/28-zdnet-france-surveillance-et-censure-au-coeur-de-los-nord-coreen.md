---
site: ZDNet France
title: "Surveillance et censure au coeur de l'OS nord-coréen"
author: Guillaume Serries
date: 2015-12-28
href: http://www.zdnet.fr/actualites/surveillance-et-censure-au-coeur-de-l-os-nord-coreen-39830306.htm
tags:
- Internet
- Institutions
- International
- Vie privée
---

> L'OS développé en Corée du Nord ressemble à son système politique, affirment deux chercheurs allemands, pointant des outils de surveillance et de censure embarqués.
