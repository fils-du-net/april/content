---
site: LeMagIT
title: "Logiciels libres et secteur public: le SILL 2016 est validé"
author: Cyrille Chausson
date: 2015-12-21
href: http://www.lemagit.fr/actualites/4500267851/Logiciels-libres-et-secteur-public-le-SILL-2016-est-valide
tags:
- Administration
- Institutions
- Sensibilisation
- RGI
- Open Data
---

> Le socle interministériel de logiciels libres a été validé dans sa version 2016 et intègre désormais l’écosystème Android.
