---
site: Libération.fr
title: "La loi numérique décryptée en 8 points"
author: Amaelle Guiton
date: 2015-12-11
href: http://www.liberation.fr/futurs/2015/12/11/la-loi-numerique-decryptee-en-8-points_1419942
tags:
- Internet
- Institutions
- Neutralité du Net
- Open Data
- Vie privée
---

> Adopté en Conseil des ministres, le projet de loi numérique renforce l'ouverture des données publiques, les droits des internautes sur leurs données et l'accès au réseau pour les plus fragiles. Mais la «neutralité du Net» a été limitée, et les «biens communs numériques» abandonnés.
