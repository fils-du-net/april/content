---
site: LeDevoir.com
title: "Les écobricoleurs du climat s’activent  en marge de la COP21"
author: Isabelle Paré
date: 2015-12-01
href: http://www.ledevoir.com/environnement/actualites-sur-l-environnement/456707/les-ecobricoleurs-du-climat-s-activent-en-marge-de-la-cop21
tags:
- Économie
- Innovation
---

> Ils sont jeunes, ingénieurs, scientifiques, designers ou militants, et ne veulent pas attendre que les grands de ce monde se mettent à table pour sauver la Terre. On les surnomme «hackers» ou «makers», mais ils sont en fait les écobricoleurs d’une génération qui veut donner au plus grand nombre les clés pour changer la planète de façon durable.
