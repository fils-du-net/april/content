---
site: Libération.fr
title: "Qui a participé à la consultation en ligne sur la loi numérique?"
author: Amaelle Guiton
date: 2015-12-18
href: http://www.liberation.fr/futurs/2015/12/18/qui-a-participe-a-la-consultation-en-ligne-sur-la-loi-numerique_1421667
tags:
- Institutions
- Associations
---

> Le secrétariat d’Etat au Numérique doit publier ce vendredi les résultats du questionnaire envoyé aux inscrits sur la plateforme «République numérique». «Libération» a pu les consulter en amont.
