---
site: L'OBS
title: "Aux électeurs du FN: «Ton logiciel est écrit par un immigré et il t’emmerde»"
author: Benoît Le Corre
date: 2015-12-08
href: http://rue89.nouvelobs.com/2015/12/08/electeurs-fn-logiciel-est-ecrit-immigre-temmerde-262405
tags:
- Institutions
- Sensibilisation
---

> Don Ho, créateur d’un éditeur de texte, a demandé à ses utilisateurs ayant voté FN d’arrêter d’utiliser son logiciel. Dans la communauté geek, le message est mal passé.
