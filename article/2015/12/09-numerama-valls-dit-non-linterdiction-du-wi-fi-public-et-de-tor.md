---
site: Numerama
title: "Valls dit non à l'interdiction du Wi-Fi public et de TOR"
author: Guillaume Champeau
date: 2015-12-09
href: http://www.numerama.com/politique/134244-valls-dit-non-a-linterdiction-du-wi-fi-public-et-de-tor.html
tags:
- Internet
- Institutions
- Vie privée
---

> Le Premier ministre Manuel Valls a affirmé que le gouvernement n'envisageait pas d'interdire les réseaux Wi-Fi public ou le réseau Tor, comme une note interne du ministère de l'intérieur l'avait évoqué.
