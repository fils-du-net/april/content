---
site: Mediapart
title: "L'Éducation Nationale vend nos enfants à Microsoft pour 13 millions"
author: Ilian Amar
date: 2015-12-08
href: https://blogs.mediapart.fr/ilian-amar/blog/081215/leducation-nationale-vend-nos-enfants-microsoft-pour-13-millions
tags:
- Entreprise
- Logiciels privateurs
- April
- Institutions
- Vente liée
- Éducation
---

> Lequel des deux affiche le plus grand sourire? Sur la photo, la Ministre de l'EN et le PDG de Microsoft France sont rayonnants. C'est pourtant une nouvelle calamité qu'ils annoncent aux élèves, aux enseignants et aux parents...
