---
site: Numerama
title: "Pourquoi Don Ho ne veut pas que les électeurs FN utilisent NotePad++"
author: Julien Cadot
date: 2015-12-08
href: http://www.numerama.com/tech/133905-le-createur-de-notepad-invite-les-electeurs-fn-a-desinstaller-son-logiciel.html
tags:
- Internet
- Institutions
- Sensibilisation
---

> Don Ho ne souhaite pas que son logiciel Notepad++ soit utilisé par des électeurs du Front National. Il l'a fait savoir sur Twitter; nous l'avons rencontré pour en discuter.
