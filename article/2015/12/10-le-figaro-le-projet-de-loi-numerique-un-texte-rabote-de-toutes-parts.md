---
site: Le Figaro
title: "Le projet de loi Numérique, un texte raboté de toutes parts"
author: Lucie Ronfaut
date: 2015-12-10
href: http://www.lefigaro.fr/secteur/high-tech/2015/12/10/32001-20151210ARTFIG00280-le-projet-de-loi-numerique-une-loi-a-basses-revolutions.php
tags:
- Internet
- Institutions
- Open Data
---

> La grande loi un temps espérée par la profession a été réduite au fil des mois à sa plus simple expression. Le texte doit être examiné en procédure d'urgence au Parlement.
