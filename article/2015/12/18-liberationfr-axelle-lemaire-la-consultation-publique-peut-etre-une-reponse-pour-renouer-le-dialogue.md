---
site: Libération.fr
title: "Axelle Lemaire: «La consultation publique peut être une réponse pour renouer le dialogue démocratique»"
author: Amaelle Guiton
date: 2015-12-18
href: http://www.liberation.fr/futurs/2015/12/18/axelle-lemaire-la-consultation-publique-peut-etre-une-reponse-pour-renouer-le-dialogue-democratique_1421480
tags:
- Institutions
- Sensibilisation
- Neutralité du Net
- Vie privée
---

> Pour la secrétaire d'Etat chargée du Numérique, la «coconstruction» en ligne expérimentée pour le projet de loi numérique est une façon de «renouveler le débat démocratique». Quitte à «nourrir des déceptions».
