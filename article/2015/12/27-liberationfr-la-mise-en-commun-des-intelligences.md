---
site: Libération.fr
title: "La mise en commun des intelligences"
author: Amaelle Guiton
date: 2015-12-27
href: http://www.liberation.fr/futurs/2015/12/27/la-mise-en-commun-des-intelligences_1423137
tags:
- Internet
- Économie
- Innovation
- Promotion
---

> «Une intelligence partout distribuée, sans cesse valorisée, coordonnée en temps réel»: ainsi le philosophe français Pierre Lévy définissait-il, en 1994, «l’intelligence collective», vue comme finalité sociale idéale de l’informatique communiquante… Vieux rêve d’un «hypercortex numérique» porté par bien des pionniers de l’Internet et du Web, qui combine le «pouvoir d’agir», offert aux individus par l’ordinateur personnel, et la construction collaborative des savoirs par la libre circulation des connaissances sur le réseau.
