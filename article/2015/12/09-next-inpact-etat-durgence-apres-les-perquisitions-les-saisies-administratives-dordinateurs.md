---
site: Next INpact
title: "État d'urgence: après les perquisitions, les saisies administratives d'ordinateurs"
author: Marc Rees
date: 2015-12-09
href: http://www.nextinpact.com/news/97615-etat-d-urgence-apres-perquisitions-saisies-administratives-dordinateurs.htm
tags:
- Institutions
---

> Dans le projet de loi constitutionnel sur l’état d’urgence, le gouvernement annonce la possibilité future pour les autorités de saisir le matériel informatique lors d’une perquisition administrative.
