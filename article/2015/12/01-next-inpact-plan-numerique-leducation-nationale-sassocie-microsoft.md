---
site: Next INpact
title: "Plan numérique: l'Éducation nationale s'associe à Microsoft"
author: Xavier Berne
date: 2015-12-01
href: http://www.nextinpact.com/news/97521-plan-numerique-l-education-nationale-s-associe-a-microsoft.htm
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Éducation
---

> Le ministère de l’Éducation nationale a décidé de s’associer pendant un an et demi avec le géant américain Microsoft afin d’accompagner la mise en œuvre du plan pour le numérique à l’école. La firme de Redmond propose de nombreuses formations et solutions, et peut espérer d’obtenir en retour une grande visibilité auprès des élèves français.
