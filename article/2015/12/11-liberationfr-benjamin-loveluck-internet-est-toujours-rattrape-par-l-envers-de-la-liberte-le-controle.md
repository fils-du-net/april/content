---
site: Libération.fr
title: "Benjamin Loveluck: «Internet est toujours rattrapé par l’envers de la liberté: le contrôle»"
author: Amaelle Guiton
date: 2015-12-11
href: http://www.liberation.fr/debats/2015/12/11/benjamin-loveluck-internet-est-toujours-rattrape-par-l-envers-de-la-liberte-le-controle_1420163
tags:
- Internet
- Économie
- Institutions
- Droit d'auteur
---

> Pour ce spécialiste des usages numériques, le Réseau, produit du complexe militaro-scientifique, vise une forme d’auto-organisation qui s’affranchirait de toute autorité politique.
