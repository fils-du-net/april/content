---
site: ZDNet France
title: "Décès de Ian Murdock, le fondateur du projet Debian"
author: Louis Adam
date: 2015-12-31
href: http://www.zdnet.fr/actualites/deces-de-ian-murdock-le-fondateur-du-projet-debian-39830444.htm
tags:
- Philosophie GNU
---

> Docker a annoncé la mort de Ian Murdock, employé de la société et également fondateur du projet Debian. Il avait 42 ans. Les circonstances exactes de sa mort sont inconnues.
