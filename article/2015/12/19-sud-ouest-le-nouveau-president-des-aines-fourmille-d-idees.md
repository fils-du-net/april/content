---
site: Sud Ouest
title: "Le nouveau président des aînés fourmille d’idées"
author: Alain Dulucq
date: 2015-12-19
href: http://www.sudouest.fr/2015/12/19/le-nouveau-president-des-aines-fourmille-d-idees-2222464-2485.php
tags:
- Sensibilisation
- Associations
---

> Le Club des aînés du village possède désormais un nouveau président. Francis Dufeil succède à Pierre Dutoya. Ce dernier reste secrétaire. Le poste de vice-président revient à Paul Lacazedieu et celui de trésorier à Jean-Claude Depomps.
