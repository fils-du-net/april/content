---
site: L'OBS
title: "Coder? Un jeu d'enfants!"
author: Louis Morice
date: 2015-12-05
href: http://tempsreel.nouvelobs.com/bien-bien/20151201.OBS0529/coder-un-jeu-d-enfants.html
tags:
- Entreprise
- Internet
- Associations
- Éducation
- Promotion
- International
---

> Les fabricants de jouets ne s'intéressent pas encore vraiment au codage. Profitez-en pour vous penchez sur les logiciels libres qui permettent de vous initier avec vos enfants.
