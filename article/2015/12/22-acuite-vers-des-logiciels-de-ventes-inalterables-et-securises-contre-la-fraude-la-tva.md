---
site: Acuité
title: "Vers des logiciels de ventes inaltérables et sécurisés contre la fraude à la TVA"
date: 2015-12-22
href: http://www.acuite.fr/actualite/legislation/84152/vers-des-logiciels-de-ventes-inalterables-et-securises-contre-la-fraude
tags:
- Entreprise
- Économie
- Institutions
- International
---

> Dans l’objectif de lutter contre la fraude et l’optimisation fiscales, les parlementaires ont adopté une mesure visant à interdire la détention de logiciels libres de comptabilité, de gestion ou d’encaissement. Aussi l’article 88 du Projet de loi de finances (PLF) pour 2016 fraîchement voté, renforce un dispositif mis en place par la loi relative à la lutte contre la fraude fiscale et la grande délinquance économique et financières du 6 décembre 2013.
