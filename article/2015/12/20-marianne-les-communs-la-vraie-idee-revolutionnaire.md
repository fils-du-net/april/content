---
site: Marianne
title: "Les communs, la vraie idée révolutionnaire"
author: Bertrand Rothé
date: 2015-12-20
href: http://www.marianne.net/les-communs-vraie-idee-revolutionnaire-100238861.html
tags:
- Économie
- Partage du savoir
- Associations
- Sciences
---

> Pourtant mentionnés dans le code civil, ces biens dont "l'usage est commun à tous" auraient pu disparaître si la crise du système néo-libéral ne les avait remises au goût du jour.
