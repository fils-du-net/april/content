---
site: JDN
title: "PostgreSQL: pourquoi Météo-France a parié sur l’open source?"
author: Michel Edwell
date: 2015-12-07
href: http://www.journaldunet.com/solutions/expert/62987/postgresql---pourquoi-meteo-france-a-parie-sur-l-open-source.shtml
tags:
- Administration
- Sensibilisation
- Sciences
- Standards
- Open Data
---

> Face au défi du développement durable, météorologie et climat sont l’objet d’une attention croissante. Une prévision au meilleur de l’état de l’art est une nécessité. Les choix logiciels et matériels effectués par l’organisme français de météorologie ont été déterminants pour atteindre ce niveau de qualité.
