---
site: BFMtv
title: "Le ministère de l’Intérieur veut bloquer Tor et interdire le Wifi public"
author: Olivier Laffargue
date: 2015-12-07
href: http://hightech.bfmtv.com/internet/le-gouvernement-francais-veut-bloquer-tor-et-interdire-le-wifi-public-934835.html
tags:
- Internet
- Institutions
- Vie privée
---

> Pour améliorer la lutte contre le terrorisme, le ministère de l'Intérieur propose toute une série de mesures permettant de mieux identifier les internautes et de capter leurs échanges.
