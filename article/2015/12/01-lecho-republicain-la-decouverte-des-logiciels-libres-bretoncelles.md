---
site: L'Echo Républicain
title: "A la découverte des logiciels libres à Bretoncelles"
author: Muriel Bansard
date: 2015-12-01
href: http://www.lechorepublicain.fr/eure-et-loir/actualite/pays/le-perche/2015/12/01/a-la-decouverte-des-logiciels-libres-a-bretoncelles_11685840-7477.html
tags:
- April
- Promotion
---

> Bonne journée pour les organisateurs de l'atelier dédié aux logiciels libres, samedi, à l'Espace public numérique (EPN) de l'Abbé-Fret, à Bretoncelles.
