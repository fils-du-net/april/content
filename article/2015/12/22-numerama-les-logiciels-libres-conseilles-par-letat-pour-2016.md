---
site: Numerama
title: "Les logiciels libres conseillés par l'État pour 2016"
author: Julien Lausson
date: 2015-12-22
href: http://www.numerama.com/tech/135953-les-logiciels-libres-conseilles-par-letat-pour-2016.html
tags:
- Administration
- April
- Institutions
- Sensibilisation
- RGI
- Sciences
---

> Depuis 2012, l’État publie une liste de logiciels libres recommandés afin qu'elle puisse servir aux administrations. Celle-ci comprend des programmes très connus du grand public, comme Firefox et VLC, mais aussi des outils bien plus pointus.
