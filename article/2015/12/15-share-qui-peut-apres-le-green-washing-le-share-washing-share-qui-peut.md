---
site: Share qui peut !
title: "Après le «green washing», le «share washing» — Share qui peut!"
author: Caroline de Malet
date: 2015-12-15
href: http://blog.lefigaro.fr/share-qui-peut/2015/12/apres-le-green-washing-voici-venu-le-share-washing.html
tags:
- Entreprise
- Économie
- Partage du savoir
- Innovation
---

> On avait compris que l’économie lavait plus vert. Voici désormais qu’elle lave plus collaboratif. Après le «green washing», voici donc venu le temps du «share washing» ou «co-washing».
