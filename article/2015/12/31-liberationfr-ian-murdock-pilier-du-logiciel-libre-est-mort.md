---
site: Libération.fr
title: "Ian Murdock, pilier du logiciel libre, est mort"
author: Alexandre Hervaud
date: 2015-12-31
href: http://www.liberation.fr/futurs/2015/12/31/ian-murdock-pilier-du-logiciel-libre-est-mort_1423760
tags:
- Philosophie GNU
---

> Le fondateur de Debian, un système d'exploitation libre pour ordinateur, avait 42 ans. Il avait annoncé son suicide sur Twitter et se disait victime de violences policières.
