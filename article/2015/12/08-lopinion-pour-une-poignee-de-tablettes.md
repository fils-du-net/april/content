---
site: L'Opinion
title: "Pour une poignée de tablettes..."
author: Sophie Roquette
date: 2015-12-08
href: http://www.lopinion.fr/blog/carnet-liaison/poignee-tablettes-92458
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Institutions
- Éducation
---

> Bizarre, bizarre, ce mariage entre le géant californien des logiciels Microsoft et notre bon vieux ministère de l’Education nationale. Najat Vallaud-Belkacem vient de signer un partenariat avec la firme fondée par Bill Gates pour mettre en place la «révolution numérique» à l’école. Microsoft va investir 13 millions d’euros sur les prochains 18 mois pour introduire sa technologie dans nos écoles.
