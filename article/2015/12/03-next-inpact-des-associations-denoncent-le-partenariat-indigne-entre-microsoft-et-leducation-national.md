---
site: Next INpact
title: "Des associations dénoncent le «partenariat indigne» entre Microsoft et l'Éducation nationale"
author: Xavier Berne
date: 2015-12-03
href: http://www.nextinpact.com/news/97570-des-associations-denoncent-partenariat-indigne-entre-microsoft-et-l-education-nationale.htm
tags:
- Entreprise
- Logiciels privateurs
- April
- Institutions
- Associations
- Éducation
- Promotion
---

> Comme on pouvait s’y attendre, le récent partenariat conclu entre Microsoft et l’Éducation nationale (en vue du grand plan pour le numérique à l’école) est sous le feu des critiques. Des associations pointent aujourd’hui du doigt une dangereuse «collusion d’intérêts».
