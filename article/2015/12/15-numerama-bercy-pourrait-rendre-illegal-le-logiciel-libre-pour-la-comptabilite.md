---
site: Numerama
title: "Bercy pourrait rendre illégal le logiciel libre pour la comptabilité"
author: Julien Lausson
date: 2015-12-15
href: http://www.numerama.com/business/135137-bercy-pourrait-rendre-illegal-le-logiciel-libre-pour-la-comptabilite.html
tags:
- Entreprise
- Économie
- Institutions
---

> Dans le projet de loi de finances pour 2016, l'article 38 exige l'usage d'un logiciel inaltérable pour permettre le contrôle du fisc. Or, cette obligation fait courir un risque évident pour le logiciel libre.
