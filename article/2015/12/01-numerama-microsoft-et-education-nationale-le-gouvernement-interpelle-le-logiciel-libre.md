---
site: Numerama
title: "Microsoft et Éducation nationale: le gouvernement interpelle le logiciel libre"
author: Guillaume Champeau
date: 2015-12-01
href: http://www.numerama.com/business/133166-logiciels-libres-le-ministere-de-leducation-nationale-invite-a-proposer.html
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Éducation
---

> «N'hésitez pas à proposer», répond le cabinet de la ministre de l'Éducation nationale Najat Vallaud-Belkacem, aux défenseurs du logiciel libre qui s'insurgent de l'accord signé lundi avec Microsoft.
