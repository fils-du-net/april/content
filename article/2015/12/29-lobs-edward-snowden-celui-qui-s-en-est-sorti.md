---
site: L'OBS
title: "Edward Snowden, \"celui qui s’en est sorti\""
author: Jérémie Zimmermann
date: 2015-12-29
href: http://tempsreel.nouvelobs.com/monde/20151228.OBS1998/edward-snowden-celui-qui-s-en-est-sorti.html
tags:
- Internet
- Institutions
- Promotion
- Sciences
- Vie privée
---

> IL VA FAIRE 2016. Pour "l'Obs", une sélection de personnalités racontent ceux et celles qui marqueront la nouvelle année. Ici, Jérémie Zimmermann dresse le portrait du lanceur d'alerte Edward Snowden.
