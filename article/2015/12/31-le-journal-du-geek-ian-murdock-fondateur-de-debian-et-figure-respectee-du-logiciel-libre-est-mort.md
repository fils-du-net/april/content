---
site: Le Journal du Geek
title: "Ian Murdock, fondateur de Debian et figure respectée du logiciel libre, est mort"
author: Antoine
date: 2015-12-31
href: http://www.journaldugeek.com/2015/12/31/ian-murdock-fondateur-de-debian-et-figure-respectee-du-logiciel-libre-est-mort
tags:
- Philosophie GNU
---

> Une bien triste nouvelle pour en cette fin d’année. Ian Murdock, le père de la distribution Linux Debian et figure respectée dans le milieu du logiciel libre de droit s’est suicidé lundi dernier à San Francisco en Californie.
