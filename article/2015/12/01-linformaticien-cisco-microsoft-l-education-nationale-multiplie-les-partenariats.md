---
site: L'Informaticien
title: "Cisco, Microsoft: l’Éducation nationale multiplie les partenariats"
author: Guillaume Périssat
date: 2015-12-01
href: http://www.linformaticien.com/actualites/id/38719/cisco-microsoft-l-education-nationale-multiplie-les-partenariats.aspx
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Éducation
---

> Le 9 novembre, Satya Nadella allouait une jolie enveloppe de 13 millions d’euros à notre Éducation nationale. Le 27 novembre, c’est avec Cisco que Najat Vallaud Belkacem signait un accord cadre. Les géants de l’IT s’intéressent de près à l’enseignement en France et le ministère s’en réjouit, quitte à tourner le dos aux logiciels libres.
