---
site: CentPapiers
title: "Numérique: un partenariat indigne des valeurs de l’Education nationale!"
author: Albert Ricchi
date: 2015-12-22
href: http://www.centpapiers.com/numerique-un-partenariat-indigne-des-valeurs-de-leducation-nationale
tags:
- Entreprise
- Institutions
- Associations
- Éducation
---

> Dans son discours de Strasbourg, le 22 novembre 2011, François Hollande avait fait de la jeunesse «la grande cause de l’élection présidentielle».
