---
site: Libération.fr
title: "Benjamin Coriat: «L’idéologie propriétaire a atteint ses limites»"
author: Vittorio De Filippis
date: 2015-12-27
href: http://www.liberation.fr/futurs/2015/12/27/benjamin-coriat-l-ideologie-proprietaire-a-atteint-ses-limites_1423143
tags:
- Entreprise
- Internet
- Économie
- Innovation
---

> Pour l’économiste, les nouveaux communs, loin des plateformes prédatrices à la Airbnb ou Uber, sont une forme de résistance face au néolibéralisme.
