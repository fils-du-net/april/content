---
site: Silicon.fr
title: "Open Source: l’Etat publie son best of 2016 des logiciels libres"
author: Reynald Fléchaux
date: 2015-12-21
href: http://www.silicon.fr/open-source-etat-publie-best-of-2016-logiciels-libres-134354.html
tags:
- Administration
- Institutions
- Sensibilisation
- RGI
---

> L’Etat publie son socle interministériel des logiciels libres, ou SILL, référençant plus d’une centaine de solutions Open Source recommandées dans l’administration.
