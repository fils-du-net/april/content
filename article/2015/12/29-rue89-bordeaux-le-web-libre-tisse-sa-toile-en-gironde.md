---
site: Rue89 Bordeaux
title: "Le web libre tisse sa toile en Gironde"
author: Xavier Ridon
date: 2015-12-29
href: http://rue89bordeaux.com/2015/12/le-web-libre-tisse-sa-toile-en-gironde
tags:
- Internet
- Partage du savoir
- Sensibilisation
- Associations
- Neutralité du Net
---

> Les hackers du monde entier sont réunis depuis ce dimanche à Hambourg pour le 32e congrès du Chaos Computer Club. Au milieu des 12000 personnes présentes se trouvent Aquilenet et LaBx, des militants girondins pour un monde numérique libre.
