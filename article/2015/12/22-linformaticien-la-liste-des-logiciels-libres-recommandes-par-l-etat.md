---
site: L'Informaticien
title: "La liste des logiciels libres recommandés par l’Etat"
date: 2015-12-22
href: http://www.linformaticien.com/actualites/id/38927/la-liste-des-logiciels-libres-recommandes-par-l-etat.aspx
tags:
- Administration
- Institutions
- Sensibilisation
- Neutralité du Net
- RGI
---

> Mis sur pied en 2013, le Socle interministériel des logiciels libres (SILL) a publié une liste d’une centaine de logiciels open source approuvés par ses soins.
