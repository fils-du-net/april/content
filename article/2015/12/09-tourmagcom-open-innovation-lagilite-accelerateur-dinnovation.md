---
site: TourMaG.com
title: "Open Innovation: l'agilité, accélérateur d'innovation"
author: Rémi Bain-Thouverez
date: 2015-12-09
href: http://www.tourmag.com/Open-Innovation-l-agilite-accelerateur-d-innovation_a77602.html
tags:
- Entreprise
- Innovation
- Promotion
---

> Le tourisme est un des tout premiers secteurs concernés par cette approche stratégique de l'Open Innovation. Fidèle à notre positionnement, nous nous efforçons d’interviewer les protagonistes de notre secteur afin de vous donner des perspectives concrètes. L’idée est bien de vous apporter davantage de réponses que d’interrogation. La société Viaxoft nous livre son analyse.
