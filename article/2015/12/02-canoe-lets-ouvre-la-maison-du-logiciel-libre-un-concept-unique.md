---
site: canoe
title: "L'ÉTS ouvre la Maison du logiciel libre, un concept unique"
date: 2015-12-02
href: http://fr.canoe.ca/techno/materiel/archives/2015/12/20151202-171132.html
tags:
- Sensibilisation
- Associations
- Éducation
---

> Mardi, l'École de technologie supérieure de Montréal inaugurait officiellement sa Maison du logiciel libre.
