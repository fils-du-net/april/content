---
site: infoDSI
title: "La Ville de Grenoble confirme son engagement pour le logiciel libre"
date: 2015-12-21
href: http://www.infodsi.com/articles/160310/ville-grenoble-confirme-son-engagement-logiciel-libre.html
tags:
- Administration
- April
---

> Ainsi, la Ville de Grenoble annonce son adhésion à April, association constituée de plus de 4 000 membres utilisateurs et producteurs de logiciels libres.
