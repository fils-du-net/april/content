---
site: InformatiqueNews.fr
title: "L’avenir du stockage passe par l’Open Source et le cloud"
author: La rédaction
date: 2015-12-14
href: http://www.informatiquenews.fr/lavenir-du-stockage-passe-par-lopen-source-et-le-cloud-42499
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Innovation
- Informatique en nuage
---

> Si l’essor du Software-Defined Storage n’est pas vraiment une bonne nouvelle pour les fournisseurs traditionnels, les équipes informatiques au contraire y ont tout à gagner, que ce soit en matière d’évolutivité illimitée, de baisse des coûts ou d’administration des solutions non-propriétaires. Le concept open source signe l’arrêt de mort des monopoles détenus par les fournisseurs (le «vendor lock-in») et l’avènement d’une réelle portabilité sur le cloud.
