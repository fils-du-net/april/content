---
site: ZDNet France
title: "Apple, un champion de l'Open Source? Non, pas encore"
author: Steven J. Vaughan-Nichols
date: 2015-12-09
href: http://www.zdnet.fr/actualites/apple-un-champion-de-l-open-source-non-pas-encore-39829550.htm
tags:
- Entreprise
- Sensibilisation
---

> Depuis une semaine, le langage Swift d'Apple est en Open Source. Une occasion pour le marketing d'Apple de se revendiquer comme le premier acteur majeur de l'informatique à mettre l'Open Source au cœur de sa stratégie. L'affirmer ne suffit pas cependant pour en faire une réalité.
