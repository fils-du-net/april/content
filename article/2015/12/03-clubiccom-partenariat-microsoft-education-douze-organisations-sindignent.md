---
site: clubic.com
title: "Partenariat Microsoft - Education: douze organisations s'indignent"
author: Alexandre Laurent
date: 2015-12-03
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/logiciel-libre-open-source/actualite-788374-partenariat-microsoft-education-nationale-indignation-april-aful.html
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Associations
- Éducation
---

> Douze organisations réagissent vertement à l'accord passé début novembre entre Microsoft et l'Education nationale. Elles dénoncent un «partenariat indigne» et une collusion d'intérêts, au détriment de la concurrence et du logiciel libre.
