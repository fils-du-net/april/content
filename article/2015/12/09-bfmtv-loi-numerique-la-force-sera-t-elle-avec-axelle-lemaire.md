---
site: BFMtv
title: "Loi numérique: la «force» sera-t-elle avec Axelle Lemaire?"
author: Pascal Samama
date: 2015-12-09
href: http://bfmbusiness.bfmtv.com/entreprise/loi-numerique-la-force-sera-t-elle-avec-axelle-lemaire-935573.html
tags:
- Internet
- Institutions
- Sensibilisation
- Neutralité du Net
---

> Axelle Lemaire a présenté son projet de loi numérique en conseil des ministres. Un texte inspiré, selon la secrétaire d'État, par les Jedi de Star Wars auxquels elle fait référence dans un tweet.
