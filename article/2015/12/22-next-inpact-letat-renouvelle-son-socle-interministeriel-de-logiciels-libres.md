---
site: Next INpact
title: "L'État renouvelle son socle interministériel de logiciels libres"
author: Xavier Berne
date: 2015-12-22
href: http://www.nextinpact.com/news/97812-l-etat-renouvelle-son-socle-interministeriel-logiciels-libres.htm
tags:
- Administration
- Institutions
- Sensibilisation
- RGI
---

> L’État vient de procéder à l’actualisation annuelle de son «socle interministériel de logiciels libres», cette sorte de guide des solutions libres recommandées pour les ordinateurs de l’administration. On retrouve dans le «cru 2016» des programmes bien connus tels que VLC, Firefox, LibreOffice ou Gimp.
