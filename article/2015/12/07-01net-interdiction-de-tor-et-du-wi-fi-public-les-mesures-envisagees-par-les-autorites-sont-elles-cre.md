---
site: 01net.
title: "Interdiction de Tor et du Wi-Fi public: les mesures envisagées par les autorités sont-elles crédibles?"
author: Gilbert Kallenborn
date: 2015-12-07
href: http://www.01net.com/actualites/tor-wifi-public-chiffrement-que-pourrait-reellement-bloquer-le-gouvernement-934959.html
tags:
- Internet
- Institutions
- Vie privée
---

> Pour avoir les coudées franches, les forces de l’ordre françaises veulent interdire ou bloquer toute une série d’outils. Mais c’est beaucoup plus compliqué qu’il n’y parait.
