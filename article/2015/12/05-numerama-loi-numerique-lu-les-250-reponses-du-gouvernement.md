---
site: Numerama
title: "Loi numérique: on a lu les 250 réponses du gouvernement"
author: Guillaume Champeau
date: 2015-12-05
href: http://www.numerama.com/politique/133686-la-loi-numerique-ou-quand-le-gouvernement-repond-a-obi-wan-kenobi.html
tags:
- Internet
- Administration
- April
- HADOPI
- Institutions
- Sensibilisation
- Vente liée
- Associations
- Droit d'auteur
- Neutralité du Net
- Promotion
- Sciences
- Open Data
- Vie privée
---

> Dans le cadre de la consultation sur la loi pour une République numérique, le gouvernement a répondu à plus de 250 propositions faites par des internautes ou des organisations. Nous les avons triées et synthétisées pour voir ce que donnait ce premier effort de démocratie participative.
