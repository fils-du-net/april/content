---
site: france3
title: "COP21: la bidouille, amie du climat"
author: Sophie Tallois
date: 2015-12-07
href: http://france3-regions.francetvinfo.fr/cop21-la-bidouille-amie-du-climat-875689.html
tags:
- Partage du savoir
- Sensibilisation
- Associations
- Innovation
---

> Faire soi-même, se débrouiller pour réparer plutôt que jeter systématiquement, voilà un moyen d'allier deux bienfaits: mettre un frein à une consommation débridée et retrouver le plaisir de faire avec ses mains
