---
site: Le Journal de Montréal
title: "DCI unique: Québec admet les gaspillages"
author: Jean-Nicolas Blanchet
date: 2015-12-18
href: http://www.journaldemontreal.com/2015/12/18/dci-uniquequebec-admet-les-gaspillages
tags:
- Administration
- Économie
- Marchés publics
- International
---

> Malgré les critiques, le gouvernement s’est entêté depuis près d’une décennie avec un plan informatique qui nous a coûté des centaines de millions de dollars, mais hier, Québec a soudainement changé d’idée.
