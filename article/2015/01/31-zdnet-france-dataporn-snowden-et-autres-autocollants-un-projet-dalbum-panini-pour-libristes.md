---
site: ZDNet France
title: "Dataporn, Snowden et autres autocollants: un projet d'\"album Panini\" pour libristes"
author: Thierry Noisette
date: 2015-01-31
href: http://www.zdnet.fr/actualites/dataporn-snowden-et-autres-autocollants-un-projet-d-album-panini-pour-libristes-39813952.htm
tags:
- April
- Associations
- Promotion
---

> Les geeks libristes sont nombreux à décorer leurs ordis et leur environnement de stickers militants ou clin d'oeil. L'association Lorraine Data Network (LDN) lance un projet d'album pour les collectionner.
