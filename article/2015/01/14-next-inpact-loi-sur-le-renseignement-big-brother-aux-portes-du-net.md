---
site: Next INpact
title: "Loi sur le renseignement: Big Brother aux portes du Net?"
author: Marc Rees
date: 2015-01-14
href: http://www.nextinpact.com/news/91741-loi-sur-renseignement-big-brother-aux-portes-du-net.htm
tags:
- Internet
- Institutions
- Open Data
- Vie privée
---

> L’actuelle majorité compte bien accélérer la présentation d’un texte sur le renseignement. Il est quasiment prêt, a expliqué Jean Jacques Urvoas ce matin sur Europe 1. L’arsenal des mesures veut frapper de plein fouet les nouvelles technologies et la vie privée, sous couvert de sécurité et de lutte anti-terroriste. Un équilibre pour le moins subtil.
