---
site: Industrie et Technologies
title: "En 2015, pas de répit sur le front de la cyberguerre"
date: 2015-01-04
href: http://www.industrie-techno.com/en-2015-pas-de-repit-sur-le-front-de-la-cyberguerre.35237
tags:
- Internet
- Innovation
---

> En ce début d'année, Industrie &amp; Technologies a repéré pour vous les 15 leviers qui vont booster l'innovation en 2015. Ils ne sont pas tous au même degré de maturité mais tous tireront la créativité et l'inventivité des centres de R&amp;D. Aujourd'hui, la cybersécurité. Un sujet qui sera une préoccupation pour tous les industriels.
