---
site: Next INpact
title: "Le projet de loi sur le renseignement promis pour début mars par Manuel Valls"
author: Xavier Berne
date: 2015-01-21
href: http://www.nextinpact.com/news/92813-le-projet-loi-sur-renseignement-promis-pour-debut-mars-par-manuel-valls.htm
tags:
- Internet
- Institutions
- Open Data
- Vie privée
---

> Manuel Valls a présenté ce matin à l’issue du Conseil des ministres un train de « mesures de court terme » censées lutter contre le terrorisme, tout juste deux semaines après le sanglant attentat ayant frappé Charlie Hebdo. Le locataire de Matignon a surtout annoncé que le projet de loi sur le renseignement serait dévoilé début mars, et qu'il se pencherait en particulier sur les interceptions de communications électroniques.
