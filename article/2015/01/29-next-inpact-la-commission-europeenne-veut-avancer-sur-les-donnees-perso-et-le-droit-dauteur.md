---
site: Next INpact
title: "La Commission européenne veut avancer sur les données perso et le droit d'auteur"
author: Xavier Berne
date: 2015-01-29
href: http://www.nextinpact.com/news/92907-la-commission-europeenne-veut-avancer-sur-donnees-perso-et-droit-d-auteur.htm
tags:
- Internet
- HADOPI
- Institutions
- Droit d'auteur
- Neutralité du Net
- Europe
- Vie privée
---

> Alors que de nombreux textes discutés au niveau de l’Union européenne s’enlisent, la nouvelle Commission semble vouloir s’activer pour que le règlement sur les données personnelles soit définitivement adopté avant l’année prochaine. Un projet de directive sur le droit d’auteur pourrait également être présenté d’ici cet été.
