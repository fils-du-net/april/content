---
site: l'Humanité.fr
title: "Microsoft, César et les grammairiens"
author: Véronique Bonnet
date: 2015-01-16
href: http://www.humanite.fr/microsoft-cesar-et-les-grammairiens-595705
tags:
- Entreprise
- April
- Institutions
- Éducation
- Promotion
---

> La consultation de l’automne dernier sur le projet de loi numérique, via une plate-forme en ligne qui revendiquait une exemplarité citoyenne, s’était présentée comme ouverte à d’éventuels amendements. L’association informée et réactive qu’est l’April (april.org), par exemple, avait honoré cette invitation et en avait éclairé les enjeux.
