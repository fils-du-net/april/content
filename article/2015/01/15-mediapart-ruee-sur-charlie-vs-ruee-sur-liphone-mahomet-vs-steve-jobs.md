---
site: Mediapart
title: "Ruée sur Charlie vs. Ruée sur l'iPhone -- Mahomet vs. Steve Jobs"
author: Jean-Lucien HARDY
date: 2015-01-15
href: http://blogs.mediapart.fr/blog/jean-lucien-hardy/150115/ruee-sur-charlie-vs-ruee-sur-liphone-mahomet-vs-steve-jobs
tags:
- Entreprise
- Économie
- Partage du savoir
- Promotion
---

> Rien que du bonheur de voir les gens s'intéresser fébrilement aux valeurs de liberté plutôt qu'aux veaux d'or et aux gadgets ostentatoires!
