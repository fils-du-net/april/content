---
site: ZDNet France
title: "Librem: un laptop open source pour les puristes"
author: Louis Adam
date: 2015-01-26
href: http://www.zdnet.fr/actualites/librem-un-laptop-open-source-pour-les-puristes-39813603.htm
tags:
- Économie
- Innovation
---

> Annoncé en fin d’année dernière, le PC portable Librem devrait voir le jour à la suite d’une campagne de crowdfunding réussie. Cet ordinateur portable développé par la société Purism promet d’être basé au maximum sur des logiciels libres.
