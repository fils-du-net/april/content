---
site: Echos judiciaires Girondins
title: "«Les geeks sur La Banquiz»"
author: Eric Moreau
date: 2015-01-18
href: http://www.echos-judiciaires.com/high-tech/-les-geeks-sur-la-banquiz-a11136.html
tags:
- Entreprise
- Administration
- Innovation
- Marchés publics
- Promotion
---

> A l’heure où la France souhaite mettre en avant ses talents de la nouvelle économie du numérique, une pépinière d’un genre nouveau émerge dans la métropole bordelaise.
