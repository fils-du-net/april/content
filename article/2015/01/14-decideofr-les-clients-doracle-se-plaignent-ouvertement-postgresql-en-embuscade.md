---
site: Decideo.fr
title: "Les clients d'Oracle se plaignent ouvertement, PostgreSQL en embuscade"
date: 2015-01-14
href: http://www.decideo.fr/Les-clients-d-Oracle-se-plaignent-ouvertement-PostgreSQL-en-embuscade_a7619.html
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Promotion
---

> Campaign for clear Licensing, une organisation à but non-lucratif basée au Royaume-Uni défendant les droits des acheteurs de logiciels d'entreprise, vient d’exhorter Oracle, dans une lettre ouverte adressée à Larry Ellison et ses collègues, «à prendre des mesures pour améliorer la confiance de ses clients et répondre à leurs préoccupations concernant la stratégie de vendor lock-in de l’éditeur s’il veut que sa stratégie actuelle de faire migrer ses clients vers ses services de cloud computing soit couronnée de succès.»
