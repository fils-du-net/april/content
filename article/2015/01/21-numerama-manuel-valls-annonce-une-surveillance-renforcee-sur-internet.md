---
site: Numerama
title: "Manuel Valls annonce une surveillance renforcée sur Internet"
author: Guillaume Champeau
date: 2015-01-21
href: http://www.numerama.com/magazine/31947-manuel-valls-annonce-une-surveillance-renforcee-sur-internet.html
tags:
- Internet
- HADOPI
- Institutions
- Éducation
- Vie privée
---

> A la sortie du conseil des ministres, le Premier ministre Manuel Valls a détaillé le plan de renforcement de lutte contre le terrorisme élaboré par le gouvernement suite aux attentats commis en janvier 2015, qui passera en particulier par des moyens renforcés pour la surveillance.
