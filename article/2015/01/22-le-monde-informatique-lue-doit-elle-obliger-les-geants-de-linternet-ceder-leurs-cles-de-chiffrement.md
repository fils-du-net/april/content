---
site: Le Monde Informatique
title: "L'UE doit-elle obliger les géants de l'Internet à céder leurs clés de chiffrement"
author: Serge Leblal
date: 2015-01-22
href: http://www.lemondeinformatique.fr/actualites/lire-l-ue-doit-elle-obliger-les-geants-de-l-internet-a-ceder-leurs-cles-de-chiffrement-59993.html
tags:
- Entreprise
- Internet
- Institutions
- Europe
- Vie privée
---

> La montée en puissance du terrorisme en Europe relance le débat sur le chiffrement des communications et la création de backdoors réservés aux forces de l'ordre européenne. Le coordinateur antiterrorisme de l'UE, Gilles de Kerchove, demande sans détour un accès aux clefs de chiffrement des géants de l'Internet.
