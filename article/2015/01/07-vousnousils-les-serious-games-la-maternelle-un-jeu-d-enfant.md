---
site: vousnousils
title: "Les serious games à la maternelle: un jeu d’enfant!"
author: Fabien Soyez
date: 2015-01-07
href: http://www.vousnousils.fr/2015/01/07/les-serious-game-a-la-maternelle-un-jeu-denfant-559573
tags:
- Éducation
- Innovation
---

> Apprendre en s'amusant, avec les jeux sérieux? Cela commence dès la maternelle. Témoignage d'un professeur des écoles qui utilise, avec succès et lucidité, une série de jeux ludo-éducatifs avec sa classe.
