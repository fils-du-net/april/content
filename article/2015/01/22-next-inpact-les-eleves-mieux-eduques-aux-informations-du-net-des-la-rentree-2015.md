---
site: Next INpact
title: "Les élèves mieux éduqués aux informations du Net dès la rentrée 2015"
author: Xavier Berne
date: 2015-01-22
href: http://www.nextinpact.com/news/92826-les-eleves-mieux-eduques-aux-informations-net-des-rentree-2015.htm
tags:
- Internet
- Institutions
- Sensibilisation
- Droit d'auteur
- Éducation
---

> Alors que de nombreuses rumeurs se sont répandues sur la Toile suite aux attentats ayant frappé Charlie Hebdo, François Hollande a annoncé hier que tous les élèves, de la primaire à la terminale, bénéficieraient à partir de la rentrée prochaine d’un enseignement civique et moral qui se traitera en particulier de l’éducation aux médias. Il devrait donc y être tout particulièrement question d’Internet.
