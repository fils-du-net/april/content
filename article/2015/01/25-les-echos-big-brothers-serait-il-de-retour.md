---
site: Les Echos
title: "Big Brothers serait-il de retour?"
author:  Jean-Pierre Roy
date: 2015-01-25
href: http://www.lesechos.fr/idees-debats/cercle/cercle-121525-big-brothers-serait-il-de-retour-1086547.php
tags:
- Entreprise
- Institutions
- Informatique-deloyale
- Sciences
- ACTA
- Vie privée
---

> Coïncidence ou hasard, les grandes manœuvres entreprises par les sociétés américaines, dans le domaine de l’espace, des médias, de l’internet et de l’informatique convergent toutes vers un contrôle de presque tout, si nous ne réagissons pas rapidement.
