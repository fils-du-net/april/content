---
site: Next INpact
title: "De plus en plus d'ordinateurs dans les collèges publics"
author: Xavier Berne
date: 2015-01-07
href: http://www.nextinpact.com/news/91648-de-plus-en-plus-d-ordinateurs-dans-colleges-publics.htm
tags:
- Administration
- Interopérabilité
- April
- Éducation
- Open Data
---

> Alors que le président de la République a promis que tous les élèves de cinquième auraient des tablettes numériques à partir de la rentrée 2016, le ministère de l’Éducation nationale vient de dévoiler les résultats d’une enquête interne montrant que le niveau d’équipement des collèges en termes de matériel informatique a progressé ces dernières années - tout du moins en volume.
