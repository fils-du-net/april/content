---
site: "paris-normandie.fr"
title: "Une journée pour le web libre"
author: Lucie Aubourg
date: 2015-01-09
href: http://www.paris-normandie.fr/detail_communes/articles/2273354/une-journee-pour-le-web-libre#.VLLv5P3JOO4
tags:
- April
- Éducation
- Promotion
---

> Bienvenue dans «l’univers du libre». Cette dénomination englobe simplement «l’ensemble des démarches liées aux initiatives non propriétaires et non lucratives du web.» Tony Gheeraert, vice-président chargé du numérique à l’université de Rouen, s’est donné pour mission de sensibiliser le public à cette question qui lui «tient à cœur».
