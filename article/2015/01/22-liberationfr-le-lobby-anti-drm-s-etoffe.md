---
site: Libération.fr
title: "Le lobby anti-DRM s’étoffe"
author: Camille Gévaudan
date: 2015-01-22
href: http://ecrans.liberation.fr/ecrans/2015/01/22/le-lobby-anti-drm-s-etoffe_1186627
tags:
- Associations
- DRM
---

> Le journaliste et auteur de science-fiction Cory Doctorow est un fervent défenseur du partage en ligne et un grand penseur des technologies, qui ne doivent pas selon lui brider l’accès du public à la culture. On ne s’étonne donc pas de le voir rejoindre l’Electronic Frontier Foundation (EFF) en tant que consultant spécial chargé du lobby anti-DRM.
