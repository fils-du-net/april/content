---
site: Nouvelle République
title: "Le logiciel libre pour tous, ça existe"
date: 2015-01-29
href: http://www.lanouvellerepublique.fr/Loir-et-Cher/Communes/Blois/n/Contenus/Articles/2015/01/29/Le-logiciel-libre-pour-tous-ca-existe-2202743
tags:
- Associations
---

> Créée en 2013, l'association Blois Gul (Groupe d'utilisateurs du logiciel libre) a tenu sa première assemblée générale. Son but repose sur l'utilisation des logiciels informatiques dits alternatifs. De fait, la plupart des outils soumis à licence payante, ont un équivalent libre et gratuit. Encore faut-il le savoir! C'est précisément la réponse que le président fondateur, Patrice Lépissier, ancien informaticien, veut apporter au public.
