---
site: Le Monde.fr
title: "Le Chaos Communication Congress, place forte de la contre-culture numérique"
author: Martin Untersinger
date: 2015-01-03
href: http://www.lemonde.fr/pixels/article/2015/01/03/le-chaos-communication-congress-place-forte-de-la-contre-culture-numerique_4548903_4408996.html
tags:
- Internet
- Associations
- Innovation
- Neutralité du Net
- Vote électronique
- International
- Vie privée
---

> Le grand rassemblement de hackeurs et d’experts en sécurité informatique fête cette année ses trente ans.
