---
site: Rue89Lyon
title: "Comment et avec qui utiliser les logiciels libres à Lyon"
author: Eva Thiébaud
date: 2015-01-30
href: http://www.rue89lyon.fr/2015/01/30/comment-avec-qui-utiliser-logiciels-libres-lyon
tags:
- Sensibilisation
- Associations
- Éducation
---

> Une asso de Lyon a fait 5000 euros d’économie; des étudiants manipulent des machines complètement propres. Les logiciels libres, c’est fantastique, même s’ils effraient encore certains (complexité, changement d’habitude…). Entre la Maison des Rançy et l’université Lyon 2, on a rencontré à Lyon celles et ceux qui se frottent au libre et l’enseigne.
