---
site: Silicon.fr
title: "Linux: pour Torvalds la diversité des contributeurs est un détail"
author: Ariane Beky
date: 2015-01-26
href: http://www.silicon.fr/linux-linus-torvalds-diversite-contributeurs-detail-106673.html
tags:
- Innovation
---

> Le créateur du noyau Linux, Torvalds, a déclaré lors de la Linux.conf.au que la diversité et la mixité sont «des détails sans importance».
