---
site: Le Monde Informatique
title: "Dépôt de brevets: IBM toujours indétrônable grâce à la diversité"
author: Jean Elyan avec IDG NS
date: 2015-01-13
href: http://www.lemondeinformatique.fr/actualites/lire-depot-de-brevets-ibm-toujours-indetronable-grace-a-la-diversite-59881.html
tags:
- Entreprise
- Brevets logiciels
---

> En 2014, IBM est, pour la 22e année consécutive, arrivé en tête des détenteurs de brevets aux États-Unis. Selon le constructeur, ce succès est lié au rayonnement mondial de ses laboratoires de recherche et à sa politique liée à la diversité.
