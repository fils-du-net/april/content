---
site: l'Humanité.fr
title: "Logiciel libre et ESS, une économie à l’intention de tous"
author: Pierric Marissal
date: 2015-01-03
href: http://www.humanite.fr/logiciel-libre-et-ess-une-economie-lintention-de-tous-564379
tags:
- Économie
- April
- Sensibilisation
- Associations
---

> Le logiciel libre propose des outils de travail en adéquation avec les valeurs et convictions de l’économie sociale et solidaire. Mais les deux mouvements ont encore beaucoup à s’apporter et à apprendre l’un de l’autre.
