---
site: Framablog
title: "2015: l’Open Source a gagné, mais ce n’est qu’un début"
author: Glyn Moody (tradution Framalang)
date: 2015-01-03
href: http://www.framablog.org/index.php/post/2015/01/03/pendant-ce-temps-open-source-a-gagne
tags:
- Internet
- Innovation
- Promotion
---

> À l’aube d’une nouvelle année, la tradition veut que l’on fasse une rétrospective des 12 mois précédents. Mais en ce qui concerne cette rubrique, il est facile de résumer ce qui s’est passé: l‘open source a gagné. Reprenons depuis le début
