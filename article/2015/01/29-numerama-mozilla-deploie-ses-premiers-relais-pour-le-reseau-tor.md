---
site: Numerama
title: "Mozilla déploie ses premiers relais pour le réseau Tor"
author: Julien L.
date: 2015-01-29
href: http://www.numerama.com/magazine/32045-mozilla-deploie-ses-premiers-relais-pour-le-reseau-tor.html
tags:
- Internet
- Associations
- Vie privée
---

> Mozilla a activé ses premiers relais pour le réseau Tor. Douze nœuds sont pour le moment actifs. D'autres devraient suivre prochainement, à mesure que l'initiative lancée par la fondation l'an dernier prendra de l'ampleur.
