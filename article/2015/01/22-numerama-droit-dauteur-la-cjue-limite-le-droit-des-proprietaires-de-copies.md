---
site: Numerama
title: "Droit d'auteur: la CJUE limite le droit des propriétaires de copies"
author: Guillaume Champeau
date: 2015-01-22
href: http://www.numerama.com/magazine/31966-droit-d-auteur-la-cjue-limite-le-droit-des-proprietaires-de-copies.html
tags:
- Entreprise
- Institutions
- Droit d'auteur
- Europe
---

> La CJUE a jugé jeudi qu'un vendeur de posters ne pouvait pas proposer à ses clients de transférer l'encre de l'image sur une toile de tableau, sans payer des droits
