---
site: Le Journal de Montréal
title: "Blackberry, Linux et la sécurité"
author: Michel Dumais
date: 2015-01-16
href: http://www.journaldemontreal.com/2015/01/16/blackberry-linux-et-la-securite
tags:
- Entreprise
- Brevets logiciels
- Promotion
---

> En effet, BB détient d’importants brevets dans le domaine de la mobilité qui fait l’envie de nombreux compétiteurs. C’est d’ailleurs pour cette raison (entre autre) que Google a acheté la division mobile de Motorola.
