---
site: Numerama
title: "La CNIL dit \"oui mais\" à la surveillance accrue sur Internet"
author: Guillaume Champeau
date: 2015-01-26
href: http://www.numerama.com/magazine/32012-la-cnil-dit-34oui-mais34-a-la-surveillance-accrue-sur-internet.html
tags:
- Internet
- Institutions
- Vie privée
---

> Alors que la France se prépare à muscler une nouvelle fois ses lois sur le renseignement pour étendre les possibilités de collecte de données et de mises sur écoutes, la présidente de la CNIL milite pour que cette extension s'accompagne de garanties nouvelles.
