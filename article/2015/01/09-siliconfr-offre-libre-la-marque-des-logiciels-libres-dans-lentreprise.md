---
site: Silicon.fr
title: "Offre Libre: la marque des logiciels libres dans l'entreprise"
author: Ariane Beky
date: 2015-01-09
href: http://www.silicon.fr/offre-libre-marque-aful-logiciel-open-source-105409.html
tags:
- Entreprise
- Associations
---

> Offre Libre de l'Aful veut distinguer les offres commerciales Open Source respectant les libertÃ©s logicielles dans un cadre professionnel.
