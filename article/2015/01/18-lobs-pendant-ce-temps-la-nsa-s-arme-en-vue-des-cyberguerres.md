---
site: L'OBS
title: "Pendant ce temps, la NSA s’arme en vue des cyberguerres"
author: Benoît Le Corre
date: 2015-01-18
href: http://rue89.nouvelobs.com/2015/01/18/pendant-temps-nsa-sarme-vue-cyberguerres-257177
tags:
- Internet
- Institutions
- International
---

> Sur l’offre de stage, il est précisé qu’ils «recherchent des stagiaires qui veulent casser des choses». Ils, c’est la NSA, l’Agence américaine de la sécurité, du renseignement, à travers une cellule de formation appelée Politerain.
