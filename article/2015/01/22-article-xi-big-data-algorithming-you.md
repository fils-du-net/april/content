---
site: Article XI
title: "Big Data is algorithming you"
author: Pierre Alonso
date: 2015-01-22
href: http://www.article11.info/?Big-Data-is-algorithming-you
tags:
- Internet
- Institutions
- Open Data
- Vie privée
---

> Nouveau champ de domination et de commercialisation du monde, la collecte d’informations numériques via des algorithmes reste largement impensée. Sans doute en raison de la complexité du sujet. Tentative d’éclaircissement, avec Antoinette Rouvroy, chercheuse et spécialiste de la «gouvernementalité algorithmique».
