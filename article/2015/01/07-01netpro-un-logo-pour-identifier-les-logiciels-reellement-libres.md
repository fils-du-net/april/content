---
site: 01netPro.
title: "Un logo pour identifier les logiciels réellement libres"
author: Marie Jung
date: 2015-01-07
href: http://pro.01net.com/editorial/639765/un-logo-pour-identifier-les-logiciels-reellement-libres
tags:
- Entreprise
- Associations
---

> L'Association Francophone des utilisateurs de logiciels libres (Aful) propose aux fournisseurs de logiciels open source opérant en France d'apposer le logo "Offre Libre" à côté de leurs offres lorsqu'elles respectent les principes du libre.
