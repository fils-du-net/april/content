---
site: L'Express.fr
title: "Bernard Cazeneuve annonce de nouvelles mesures anti-terroristes"
date: 2015-01-11
href: http://www.lexpress.fr/actualite/politique/bernard-cazeneuve-annonce-de-nouvelles-mesures-anti-terroristes_1639533.html
tags:
- Internet
- Institutions
- Europe
- International
---

> Le ministre de l'Intérieur et onze de ses homologues européens ont convenu de prendre des mesures pour lutter contre le terrorisme. Bernard Cazeneuve a annoncé son intention de filtrer certains contenus sur Internet et de renforcer les contrôles aux frontières européennes.
