---
site: Techniques de l'Ingénieur
title: "Coup de pouce de l’État en faveur de l’Open Source"
date: 2015-01-08
href: http://www.techniques-ingenieur.fr/actualite/articles/coup-de-pouce-de-letat-en-faveur-de-lopen-source-31088
tags:
- Entreprise
- Administration
- Institutions
---

> Quelques semaines après sa validation, les administrations peuvent consulter la version 2016 du socle interministériel des logiciels libres (SILL) afin de découvrir et utiliser les quelque 100 logiciels Open Source recommandés par l’État.
