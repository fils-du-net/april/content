---
site: L'Atelier
title: "«Les villes ne se rendent pas compte du potentiel du citoyen-entrepreneur»"
author: Aurore Geraud
date: 2015-01-06
href: http://www.atelier.net/trends/articles/villes-ne-se-rendent-compte-potentiel-citoyen-entrepreneur_439456
tags:
- Entreprise
- Économie
- Innovation
---

> Rencontre avec le chercheur américain Boyd Cohen, sur sa vision de l'évolution du profil de l'entrepreneur à l'origine de la smart city de demain.
