---
site: Developpez.com
title: "Devrions-nous écrire moins de code et plus de blogs ? Selon plusieurs développeurs, le code n'est pas notre principale fonction"
author: Amine Horseman
date: 2015-01-30
href: http://www.developpez.com/actu/80813/Devrions-nous-ecrire-moins-de-code-et-plus-de-blogs-Selon-plusieurs-developpeurs-le-code-n-est-pas-notre-principale-fonction
tags:
- Entreprise
---

> Laura Klein, auteur du livre UX for Lean Startups avait écrit une lettre sur Medium destinée «aux Ingénieurs qui ne se soucient pas réellement de leurs clients» dans laquelle elle explique son point de vue concernant l'écriture du code dans le métier de développeur.
