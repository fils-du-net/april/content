---
site: L'Expansion
title: "A Davos, les chercheurs enterrent la vie privée"
date: 2015-01-23
href: http://lexpansion.lexpress.fr/high-tech/a-davos-les-chercheurs-enterrent-la-vie-privee_1644030.html
tags:
- Économie
- Institutions
- Vie privée
---

> Collecte de données personnelles, informations génétiques, drones... Plusieurs chercheurs ont annoncé mercredi au forum économique de Davos la fin de la privacy.
