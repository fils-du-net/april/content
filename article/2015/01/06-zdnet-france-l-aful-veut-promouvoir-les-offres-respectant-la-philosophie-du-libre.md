---
site: ZDNet France
title: "L’Aful veut promouvoir les offres respectant la philosophie du libre"
author: Louis Adam
date: 2015-01-06
href: http://www.zdnet.fr/actualites/l-aful-veut-promouvoir-les-offres-respectant-la-philosophie-du-libre-39812369.htm
tags:
- Entreprise
- Économie
- Associations
---

> L’Aful inaugure aujourd’hui une nouvelle initiative visant à promouvoir les éditeurs proposant des offres respectant l’esprit du libre. Une marque ouverte à ceux qui souhaitent valoriser et démarquer leurs offres libres aux yeux de leurs clients. Explications avec Laurent Séguin, président de l'association.
