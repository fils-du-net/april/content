---
site: L'OBS
title: "La Chine veut fouiner dans les ordis américains. Comme ses petits copains"
author: Andréa Fradin
date: 2015-01-31
href: http://rue89.nouvelobs.com/2015/01/31/chine-veut-fouiner-les-ordis-americains-comme-petits-copains-257421
tags:
- Internet
- Institutions
- Informatique-deloyale
- International
- Vie privée
---

> La Chine, pas franchement connue pour son amour d’un Internet libre et sans entrave, en remet une couche. Et vise cette fois-ci les équipementiers américains.
