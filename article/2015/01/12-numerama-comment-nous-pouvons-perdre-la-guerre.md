---
site: Numerama
title: "Comment nous pouvons perdre la guerre"
author: Guillaume Champeau
date: 2015-01-12
href: http://www.numerama.com/magazine/31837-comment-nous-pouvons-perdre-la-guerre.html
tags:
- Internet
- Institutions
- Europe
- International
- Vie privée
---

> Il n'y a qu'une seule manière de perdre la guerre contre le terrorisme. Et c'est celle que nous choisissons de plus en plus.
