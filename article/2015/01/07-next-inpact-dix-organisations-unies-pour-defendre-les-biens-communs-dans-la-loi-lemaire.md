---
site: Next INpact
title: "Dix organisations unies pour défendre les biens communs dans la loi Lemaire"
author: Marc Rees
date: 2015-01-07
href: http://www.nextinpact.com/news/97980-dix-organisations-veulent-defendre-biens-communs-dans-loi-lemaire.htm
tags:
- Institutions
- Associations
- Droit d'auteur
- Open Data
---

> Une dizaine d’organisations insistent pour rectifier la loi Lemaire afin d’y intégrer notamment la protection des biens communs ou encore insérer dans notre droit, l’exception de panorama.
