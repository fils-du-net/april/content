---
site: Numerama
title: "Droit d'auteur: les propositions du rapport de l'eurodéputée pirate Julia Reda"
author: Guillaume Champeau
date: 2015-01-19
href: http://www.numerama.com/magazine/31920-droit-d-auteur-les-propositions-du-rapport-de-l-eurodeputee-pirate-julia-reda.html
tags:
- Internet
- Interopérabilité
- Institutions
- DRM
- Droit d'auteur
- Europe
---

> L'eurodéputée pirate Julia Reda présentera mardi en commission son rapport sur la mise en oeuvre de la directive de 2001 sur le droit d'auteur dans la société de l'information, dans lequel elle plaide pour un large assouplissement des conditions de réutilisation des oeuvres. Mais sans remettre en cause les fondements du droit d'auteur ni exiger de révolution.
