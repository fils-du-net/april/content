---
site: Next INpact
title: "Plan e-éducation: davantage de numérique dans les enseignements à partir de 2016"
author: Xavier Berne
date: 2015-01-19
href: http://www.nextinpact.com/news/91755-plan-e-education-davantage-numerique-dans-enseignements-a-partir-2016.htm
tags:
- Internet
- Interopérabilité
- April
- Institutions
- Éducation
---

> À l’occasion du débat organisé mercredi dernier à l’Assemblée nationale à propos de la stratégie numérique de la France, la secrétaire d’État au Numérique Axelle Lemaire est revenue sur le grand «plan pour le numérique à l’école» promis l’été dernier par François Hollande. Non sans une certaine impression de réchauffé...
