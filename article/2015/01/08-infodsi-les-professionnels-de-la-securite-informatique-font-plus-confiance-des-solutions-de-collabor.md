---
site: infoDSI
title: "Les professionnels de la sécurité informatique font plus confiance à des solutions de collaboration open source qu’à des solutions propriétaires"
author: Olivier Thierry
date: 2015-01-08
href: http://www.infodsi.com/articles/153291/professionnels-securite-informatique-font-plus-confiance-solutions-collaboration-open-source-solutions-proprietaires-olivier-thierry-chief-marketing-officer-zimbra-specialiste-open-source.html
tags:
- Entreprise
---

> Selon un mythe ancien, les logiciels open source seraient fondamentalement risqués car ils déchargent les fournisseurs de toute responsabilité. On croit de façon générale que l’open source est synonyme d’une «bande de joyeux développeurs». Une autre perception, un peu plus positive, est que l’on peut faire confiance à la communauté des développeurs jusqu’à un certain point, et que l’open source est destinée aux personnes ayant un souci financier et disposées à échanger l’aspect sécuritaire contre l’aspect économique.
