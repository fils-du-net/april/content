---
site: Numerama
title: "L'UE veut les clés pour déchiffrer toutes les communications en ligne"
author: Guillaume Champeau
date: 2015-01-23
href: http://www.numerama.com/magazine/31982-l-ue-veut-les-cles-pour-dechiffrer-toutes-les-communications-en-ligne.html
tags:
- Entreprise
- Internet
- Institutions
- Europe
- Vie privée
---

> Le coordinateur de la lutte anti-terroriste de l'Union européenne, qui prépare les propositions finales qui seront adoptées début février par la Commission, demande que l'Europe se dote d'une législation qui rende illégale l'utilisation de moyens de communication chiffrés, sauf à ce que les prestataires fournissent les clés de chiffrement aux autorités compétentes.
