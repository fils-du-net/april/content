---
site: Le Courrier picard
title: "BEAUVAIS La capitale du logiciel libre se prépare"
author: Fanny Dolle
date: 2015-01-15
href: http://www.courrier-picard.fr/region/beauvais-la-capitale-du-logiciel-libre-se-prepare-ia186b0n502061
tags:
- Associations
- Promotion
---

> Les Rencontres mondiales du logiciel libre (RMLL), philosophie selon laquelle les logiciels peuvent être utilisés, partagés et améliorés, se préparent. La prochaine édition aura lieu du 6 au 10 juillet, à l’antenne universitaire de Beauvais.
