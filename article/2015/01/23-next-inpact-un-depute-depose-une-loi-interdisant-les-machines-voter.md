---
site: Next INpact
title: "Un député dépose une loi interdisant les machines à voter"
author: Xavier Berne
date: 2015-01-23
href: http://www.nextinpact.com/news/92848-un-depute-depose-loi-interdisant-machines-a-voter.htm
tags:
- Institutions
- Vote électronique
---

> Après le Sénat, c’est au tour de l’Assemblée nationale de recevoir une proposition de loi visant à interdire les machines à voter en France. S’il est aujourd'hui impossible pour les communes de passer des urnes traditionnelles à de tels équipements, quelques villes font encore de la résistance.
