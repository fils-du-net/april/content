---
site: Nouvelles de France
title: "Libertés numériques: sortir de l’idéologie totalitaire dominante et avoir une vrai volonté politique"
author: Barbara Mazières
date: 2015-01-07
href: http://www.ndf.fr/poing-de-vue/07-01-2016/78030
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> Les mesures que souhaite prendre le ministère de l’Intérieur soi-disant pour la sécurité sur internet sont, une nouvelle fois, des mesures contraires à la défense de nos liberté : coupure obligatoire de tout réseau Wi-Fi ouvert (mesure abandonnée pour l’instant? mais il faut rester vigilant), menace et censure des sites, blocage des réseaux d’anonymisation et fourniture des clés de chiffrement des messageries font partie des pistes soumises à arbitrage.
