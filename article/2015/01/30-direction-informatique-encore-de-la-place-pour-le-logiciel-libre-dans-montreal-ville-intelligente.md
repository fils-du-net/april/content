---
site: Direction Informatique
title: "Encore de la place pour le logiciel libre dans Montréal ville «intelligente»?"
author: Dominique Lemoine
date: 2015-01-30
href: http://www.directioninformatique.com/encore-de-la-place-pour-le-logiciel-libre-dans-montreal-ville-intelligente/33184
tags:
- Administration
- Économie
- Interopérabilité
- International
- Open Data
---

> La stratégie 2014-2017 de la Ville de Montréal pour devenir une ville dite «intelligente et numérique» s’appuierait notamment sur la mise en place d’une architecture technologique ouverte et interopérable.
