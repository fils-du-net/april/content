---
site: Next INpact
title: "Les «priorités législatives» de la commission numérique de l'Assemblée nationale"
author: Xavier Berne
date: 2015-01-27
href: http://www.nextinpact.com/news/92815-les-priorites-legislatives-commission-numerique-l-assemblee-nationale.htm
tags:
- Internet
- Économie
- Institutions
- Neutralité du Net
- Open Data
- Vie privée
---

> La commission «numérique» de l’Assemblée nationale vient de dresser un inventaire des sujets «prioritaires» sur lesquels elle compte prochainement aiguiller le législateur: protection des données personnelles, surveillance du Net, ouverture des informations publiques, neutralité des réseaux... Ces propositions devraient ensuite s’inviter dans les débats à venir sur les projets de loi du gouvernement en matière de numérique et de renseignement.
