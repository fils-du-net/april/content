---
site: Le Monde.fr
title: "Numérique: des images deux fois moins lourdes"
author: David Larousserie
date: 2015-01-19
href: http://www.lemonde.fr/sciences/article/2015/01/19/des-images-deux-fois-moins-lourdes_4559136_1650684.html
tags:
- Brevets logiciels
- Innovation
---

> Un ingénieur français a développé un nouveau format de compression plus efficace que l’incontournable JPEG.
