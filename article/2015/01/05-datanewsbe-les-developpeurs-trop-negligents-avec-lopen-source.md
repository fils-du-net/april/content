---
site: Datanews.be
title: "Les développeurs trop négligents avec l'open source"
date: 2015-01-05
href: http://datanews.levif.be/ict/actualite/les-developpeurs-trop-negligents-avec-l-open-source/article-normal-359933.html
tags:
- Entreprise
---

> Pas mal de code présent dans les librairies open source déborde d'erreurs. La plupart des développeurs estiment en effet à tort que ce code est sûr, et n'accordent donc guère d'attention à le tester et à le passer en revue. La... règle est dès lors d'attendre le prochain abus (exploit) qui peut avoir de graves conséquences.
