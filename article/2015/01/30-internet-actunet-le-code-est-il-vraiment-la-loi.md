---
site: internet ACTU.net
title: "Le code est-il vraiment la loi?"
author: Hubert Guillaud
date: 2015-01-30
href: http://www.internetactu.net/2015/01/30/le-code-est-il-vraiment-la-loi
tags:
- Innovation
---

> Le chercheur de l’université de Toronto, Quinn DuPont (@quinndupont), qui s’apprête à publier Cryptographie, mot-clé critique des humanités numériques, revenait récemment dans son blog sur la célèbre phrase de Lawrence Lessig, “le code est la loi”. Il rappelle que Lessig a inventé cette formule “en réagissant au déploiement des systèmes de gestion de droits numériques (DRM) pour contrer le partage illégal de fichiers. La leçon que nous devons tirer de cette phrase est que ontologiquement (et légalement) le code est préalable.
