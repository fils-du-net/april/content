---
site: NetPublic
title: "Informatique libre et éco-gestes: Guide pratique par l'EPN de Bédoin"
author: Jean-Luc Raymond
date: 2015-01-05
href: http://www.netpublic.fr/2015/01/informatique-libre-et-eco-gestes-guide-pratique
tags:
- April
- Associations
- Promotion
- RGI
---

> L’EPN ERIC (Espace Régional Internet Citoyen) ECG de la MJC de Bédoin (Vaucluse) outre son offre d’accompagnement au numérique: formations informatiques tous publics et aide à la démocratisation des logiciels libres, s’est également une spécialité de la promotion pour une informatique éco-responsable.
