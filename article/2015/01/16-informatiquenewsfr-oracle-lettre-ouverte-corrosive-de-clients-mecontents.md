---
site: InformatiqueNews.fr
title: "Oracle: lettre ouverte corrosive de clients mécontents"
author: Channelnews.fr
date: 2015-01-16
href: http://www.informatiquenews.fr/oracle-lettre-ouverte-corrosive-clients-mecontents-28244
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Promotion
---

> Campaign for clear Licensing, une organisation à but non-lucratif basée au Royaume-Uni qui se définit comme un régulateur souhaitant «secouer les choses dans dans le domaine des licences IT» , vient d’adresser à Larry Ellison et ses collègues d’Oracle une lettre ouverte plutôt corrosive.
