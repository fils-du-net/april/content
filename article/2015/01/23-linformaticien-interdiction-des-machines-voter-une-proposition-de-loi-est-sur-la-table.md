---
site: L'Informaticien
title: "Interdiction des machines à voter? Une proposition de loi est sur la table"
author: Emilien Ercolani
date: 2015-01-23
href: http://www.linformaticien.com/actualites/id/35507/interdiction-des-machines-a-voter-une-proposition-de-loi-est-sur-la-table.aspx
tags:
- HADOPI
- Institutions
---

> «Marginal», «en sursis», «dysfonctionnements»… Le député centriste de la Loire François Rochebloine n'a pas de mots assez durs pour décrire les machines à voter, qu’il souhaite définitivement interdire dans une proposition de loi déposée le 21 janvier.
