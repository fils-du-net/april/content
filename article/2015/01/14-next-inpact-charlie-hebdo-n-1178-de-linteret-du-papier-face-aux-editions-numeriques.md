---
site: Next INpact
title: "Charlie Hebdo n° 1178: de l'intérêt du papier face aux éditions numériques"
author: David Legrand
date: 2015-01-14
href: http://www.nextinpact.com/news/91736-charlie-hebdo-n-1178-interet-papier-face-aux-editions-numeriques.htm
tags:
- Internet
- Économie
- Partage du savoir
- DRM
---

> Aujourd'hui, tout le monde cherche à s'acheter le dernier Charlie Hebdo. Un numéro 1178 qui restera sans doute dans l'histoire, tiré à plusieurs millions d'exemplaires, qui pousse à réfléchir sur l'intérêt du papier et de la presse au format numérique. Mais sur la capacité des lecteurs à soutenir et à financer l'information.
