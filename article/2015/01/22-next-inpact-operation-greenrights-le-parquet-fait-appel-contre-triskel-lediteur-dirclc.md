---
site: Next INpact
title: "Opération GreenRights: le Parquet fait appel contre Triskel, l'éditeur d'irc.lc"
author: Marc Rees
date: 2015-01-22
href: http://www.nextinpact.com/news/92839-operation-greenrights-parquet-fait-appel-contre-triskel-l-editeur-dirc-lc.htm
tags:
- Internet
- Institutions
- Vie privée
---

> En marge du FIC 2015, nous avons appris que le Parquet avait fait appel du dossier Pierrick Goujon. Son service en ligne de raccourcisseur d’URL, irc.lc dédié à IRC, avait été utilisé par des anonymous lors d’une attaque par déni de service visant EDF.
