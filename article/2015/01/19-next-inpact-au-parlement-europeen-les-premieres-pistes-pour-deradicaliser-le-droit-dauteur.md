---
site: Next INpact
title: "Au Parlement européen, les premières pistes pour déradicaliser le droit d'auteur"
author: Marc Rees
date: 2015-01-19
href: http://www.nextinpact.com/news/92784-au-parlement-europeen-premieres-pistes-pour-deradicaliser-droit-d-auteur.htm
tags:
- Internet
- Interopérabilité
- Institutions
- DRM
- Droit d'auteur
- Europe
---

> On connait désormais la V.1 du rapport de l’eurodéputée Julia Reda (Parti Pirate, apparentée Verts) sur l’adaptation de la directive sur le droit d’auteur. Cette résolution, d’essence politique uniquement, a pour ambition de donner l’analyse du Parlement européen sur le sens des réformes voulues par la Commission européenne en ce secteur.
