---
site: GinjFo
title: "Windows en Chine, la variable prix de Linux n'a aucun poids!"
author: Jérôme Gianoli 
date: 2015-01-14
href: http://www.ginjfo.com/actualites/politique-et-economie/windows-en-chine-variable-prix-linux-na-poids-20150114
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Institutions
- International
---

> Le gouvernement Chinois a ouvertement exprimé sa méfiance face au produit Microsoft allant jusqu’à interdire l’installation de Windows 8 sur ses ordinateurs. En parallèle une enquête  anti-trust a été lancée contre le géant tandis que les recommandations au pays sont de passer à un système d’exploitation Open Source (Linux) jugé plus sûr
