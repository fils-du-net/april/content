---
site: Numerama
title: "Offre Libre: un label pour les vrais professionnels du logiciel libre"
author: Guillaume Champeau
date: 2015-01-07
href: http://www.numerama.com/magazine/31792-offre-libre-un-label-pour-les-vrais-professionnels-du-logiciel-libre.html
tags:
- Entreprise
- Associations
---

> L'Association Francophone des Utilisateurs de Logiciels Libres (AFUL) lance un label "Offre libre" pour distinguer les offres commerciales qui respectent parfaitement l'esprit du logiciel libre, qui ne se résume pas à l'utilisation de technologies open-source.
