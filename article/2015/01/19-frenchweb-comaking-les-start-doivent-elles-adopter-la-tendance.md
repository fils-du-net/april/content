---
site: FrenchWeb
title: "Comaking: les start-up doivent-elles adopter la tendance?"
author: Camille Adaoust
date: 2015-01-19
href: http://frenchweb.fr/comaking-les-start-up-doivent-elles-adopter-la-tendance/179886
tags:
- Entreprise
- Économie
- Innovation
---

> Le comaking, un phénomène qui nous vient des Etats-Unis. Il s’installe petit à petit en France. Paris, Lyon, Marseille, Bordeaux, dans les grandes villes, ces espaces de production partagés prennent leurs marques.
