---
site: Next INpact
title: "WikiLeaks: l'intervention des géants du Blu-ray dans le dossier Hadopi VLC"
author: Marc Rees
date: 2015-04-17
href: http://www.nextinpact.com/news/93861-wikileaks-l-intervention-geants-blu-ray-dans-dossier-hadopi-vlc.htm
tags:
- Entreprise
- Interopérabilité
- HADOPI
- Associations
- DRM
---

> WikiLeaks a diffusé ce matin 30 000 documents et 170 000 emails dérobés à Sony Pictures Entertainment par des pirates informatiques. Dans le lot, une cinquantaine de documents concerne la demande VideoLan adressée en 2012 à la Hadopi pour permettre l’interopérabilité du Blu-ray.
