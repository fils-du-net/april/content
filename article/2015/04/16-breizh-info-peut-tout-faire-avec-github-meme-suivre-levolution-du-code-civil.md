---
site: Breizh Info
title: "On peut tout faire avec GitHub, même suivre l'évolution du Code Civil"
date: 2015-04-16
href: http://www.breizh-info.com/25322/actualite-societale/on-peut-tout-faire-avec-github-meme-suivre-levolution-du-code-civil
tags:
- Internet
- Partage du savoir
- Institutions
- Innovation
---

> Suivre les changements multiples du Code Civil, ce n’est pas très simple. Même quand on connaît Legifrance qui présentent les versions successives d’une même loi, ou qu’on suit assidument les travaux des parlementaires. Ne serait-ce parce qu’un amendement ou un paragraphe de loi, cela ressemble souvent à ça: À l’article 165 du même code, le mot: «devant» est remplacé par les mots: «lors d’une cérémonie républicaine par». On peut trouver plus ergonomique, n’est-ce pas?
