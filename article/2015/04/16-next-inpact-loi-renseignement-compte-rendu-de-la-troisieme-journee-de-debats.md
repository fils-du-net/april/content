---
site: Next INpact
title: "Loi Renseignement: compte rendu de la troisième journée de débats"
author: Marc Rees
date: 2015-04-16
href: http://www.nextinpact.com/news/93837-loi-renseignement-compte-rendu-troisieme-journee-debats.htm
tags:
- Internet
- Administration
- Institutions
- Vie privée
---

> Les débats autour du projet de loi sur le renseignement ont débuté lundi à 16h00. Voici notre compte rendu de ceux de la troisième journée dans leur intégralité (voir ceux de la première journée, et celui de la deuxième journée).
