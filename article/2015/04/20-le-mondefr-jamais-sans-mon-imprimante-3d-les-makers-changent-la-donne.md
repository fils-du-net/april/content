---
site: Le Monde.fr
title: "Jamais sans mon imprimante 3D, les «makers» changent la donne"
author: Léonor Lumineau
date: 2015-04-20
href: http://www.lemonde.fr/emploi/article/2015/04/20/jamais-sans-mon-imprimante-3d-les-makers-changent-la-donne_4619304_1698637.html
tags:
- Matériel libre
- Associations
- Innovation
- Promotion
---

> Grâce aux outils numériques et à l'«open source» appliquée aux objets, le mouvement du «faire» prend de l'ampleur. Ses acteurs: des techniciens passionnés d'innovation, des entrepreneurs, des bidouilleurs du dimanche. En témoigne la «Maker Faire» dont l’édition 2015 se déroulera à Paris les 2 et 3 mai.
