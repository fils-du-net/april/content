---
site: Next INpact
title: "Loi Renseignement: le gouvernement feint de corriger les «boites noires»"
author: Marc Rees
date: 2015-04-15
href: http://www.nextinpact.com/news/93839-loi-renseignement-gouvernement-feint-corriger-boites-noires.htm
tags:
- Internet
- Institutions
- Vie privée
---

> Dans un amendement de dernière minute, face à la gronde suscité par les fameuses boites noires, le gouvernement tente d’amender le projet de loi sur le renseignement. Problème, les mesures proposées sont bien en retrait.
