---
site: www.itele.fr
title: "POC 21: Le village au service de l'écologie et de l'open source"
author: Maïna Fauliot-Marjany
date: 2015-04-22
href: http://www.itele.fr/france/video/poc-21-le-village-au-service-de-lecologie-et-de-lopen-source-120892
tags:
- Économie
- Partage du savoir
- Matériel libre
- Innovation
---

> Pendant cinq semaines, un grand atelier de la transition énergétique va voir le jour dans le jardin d’un château des Yvelines. L’ambition de ce projet baptisé 'POC21' est de proposer des solutions concrètes et fonctionnelles à l’épuisement des ressources et au réchauffement climatique.
