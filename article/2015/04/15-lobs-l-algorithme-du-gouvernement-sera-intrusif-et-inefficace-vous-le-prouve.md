---
site: L'OBS
title: "L’algorithme du gouvernement sera intrusif et inefficace. On vous le prouve"
author: Andréa Fradin
date: 2015-04-15
href: http://rue89.nouvelobs.com/2015/04/15/lalgorithme-gouvernement-sera-intrusif-inefficace-prouve-258672
tags:
- Internet
- Institutions
- Vie privée
---

> On a demandé à des spécialistes en informatique s’il était possible de concevoir un programme répondant aux attentes du gouvernement en matière de renseignement. Résultat: techniquement, c’est très foireux.
