---
site: Le Huffington Post
title: "Demain, serons-nous très humains plutôt que transhumains?"
author: Alain Damasio et Lévy-Provençal
date: 2015-04-05
href: http://www.huffingtonpost.fr/alain-damasio/humanisme-transhumanisme_b_6998646.html
tags:
- Internet
- Partage du savoir
- Innovation
- Sciences
---

> L'évolution des nouvelles technologies, poussant l'homme à intégrer l'existence et l'assistance des robots, des machines à son quotidien, pour améliorer sa qualité de vie pose non seulement une question éthique, mais encore met en opposition l'humanisme et le transhumanisme. Cette coexistence a-t-elle ou doit-elle avoir ses limites? Notre avenir doit-il faut de nous des "très humains" ou des transhumains? Alain Damasio, auteur de science-fiction et Michel Lévy-Provençal, ingénieur, tentent de les définir, au travers d'un échange.
