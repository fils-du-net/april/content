---
site: JDN
title: "Pourquoi rejoindre l'Open Invention Network"
author: Benjamin Jean
date: 2015-04-23
href: http://www.journaldunet.com/developpeur/expert/60705/pourquoi-rejoindre-l-open-invention-network.shtml
tags:
- Entreprise
- Brevets logiciels
---

> Créé par IBM, Novell, Philips, Red Hat et Sony, l’Open Invention Network a pour but de protéger les logiciels libres et open source de la menace que font planer les brevets.
