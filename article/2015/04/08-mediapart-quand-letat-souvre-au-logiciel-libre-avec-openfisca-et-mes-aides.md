---
site: Mediapart
title: "Quand l'État s'ouvre au logiciel libre avec OpenFisca et Mes-aides"
author: Frédéric Couchet
date: 2015-04-08
href: http://blogs.mediapart.fr/blog/frederic-couchet/080415/quand-letat-souvre-au-logiciel-libre-avec-openfisca-et-mes-aides-0
tags:
- Administration
- April
- Institutions
---

> Le journaliste Xavier Berne a publié début avril 2015 un article dans lequel il revient en détail sur la genèse et les objectifs d'OpenFisca et Mes-aides, qui permettent des simulations du système socio-fiscal français. Lors de la rédaction de son article, Xavier Berne m'avait contacté pour solliciter mon avis sur ces outils et cette initiative du gouvernement. Certains de mes commentaires ayant été repris en citations dans l'article, je vais développer ici un peu plus mon avis.
