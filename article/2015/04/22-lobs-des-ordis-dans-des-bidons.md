---
site: L'OBS
title: "Des ordis dans des bidons"
author: Claire Richard
date: 2015-04-22
href: http://rue89.nouvelobs.com/rue89-culture/2015/04/22/ordis-bidons-258696
tags:
- Internet
- Partage du savoir
- Matériel libre
- Associations
- Innovation
- International
---

> Récupération et coûts minimum: le mouvement des Jerrys diffuse un accès facilité à l’informatique avec des composants d’ordinateur et des logiciels libres dans des jerrycans et autres matériaux simples.
