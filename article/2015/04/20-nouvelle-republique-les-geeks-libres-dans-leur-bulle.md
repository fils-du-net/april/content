---
site: Nouvelle République
title: "Les geeks libres dans leur bulle"
date: 2015-04-20
href: http://www.lanouvellerepublique.fr/Deux-Sevres/Communes/Bressuire/n/Contenus/Articles/2015/04/20/Les-geeks-libres-dans-leur-bulle-2301153
tags:
- Associations
- Promotion
---

> L'association Gebull (Gâtine et Bocage utilisateurs de logiciels libres) qui organisait ses portes ouvertes samedi à la salle de la médiathèque pourrait passer pour un repaire de geeks (fous d'informatique ou de technologies) bien perchés dans leur monde. D'ailleurs, la table basse qu'ils ont fabriquée et programmée avec un jeu d'arcades pour quadras nostalgiques (Tron) pourrait confirmer cet a priori.
