---
site: Nouvelle République
title: "Pour vivre le Web en liberté"
author: Laurent Gaudens
date: 2015-04-14
href: http://www.lanouvellerepublique.fr/Vienne/Communautes-NR/n/Contenus/Articles/2015/04/14/Pour-vivre-le-Web-en-liberte-2294174
tags:
- Associations
- Promotion
---

> Le logiciel libre, c’est une philosophie. Et l’APP3L est là pour diffuser la bonne parole et aider ceux qui veulent franchir le pas.
