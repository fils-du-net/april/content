---
site: Developpez.com
title: "L'Europe veut lutter contre la surveillance de masse"
author: Michael Guilloux
date: 2015-04-28
href: http://www.developpez.com/actu/84501/L-Europe-veut-lutter-contre-la-surveillance-de-masse-par-le-financement-des-logiciels-open-source-cles-et-la-chasse-aux-bugs-dans-ces-outils
tags:
- Institutions
- Promotion
- Europe
- Vie privée
---

> Les divulgations d’Edward Snowden, l’ex-employé de la NSA, ont alerté le monde informatique sur la nécessité de renforcer la sécurité des différentes plateformes, dont une bonne partie n’a pas résisté aux outils sophistiqués d’espionnage des agences de renseignements US et partenaires.
