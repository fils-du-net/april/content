---
site: "usine-digitale.fr"
title: "POC 21: 5 semaines dans un château pour élaborer 12 produits open et écolo"
author: Séverine Fontaine
date: 2015-04-08
href: http://www.usine-digitale.fr/article/poc-21-5-semaines-dans-un-chateau-pour-elaborer-12-produits-open-et-ecolo.N321485
tags:
- Partage du savoir
- Matériel libre
- Innovation
- International
---

> Pendant 5 semaines non-stop, au château de Millemont près de Paris, une équipe de 50 personnes va co-concevoir cet été 12 produits open source et respectueux de l’environnement. L’initiative, lancée par l’accélérateur POC 21, se veut une réponse concrète au problème de la transition énergétique et de la préservation du climat.
