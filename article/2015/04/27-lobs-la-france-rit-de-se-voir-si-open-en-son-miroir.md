---
site: L'OBS
title: "La France rit de se voir si open en son miroir"
author: Robin Prudent et Rémi Noyon
date: 2015-04-27
href: http://rue89.nouvelobs.com/2015/04/29/france-rit-voir-si-open-miroir-258878
tags:
- Administration
- Institutions
- International
- Open Data
---

> Notre beau pays va présider le Partenariat pour un gouvernement ouvert, noué entre 65 pays, qui promeut l’ouverture des données et des gouvernements. Mais l’a-t-il vraiment mérité?
