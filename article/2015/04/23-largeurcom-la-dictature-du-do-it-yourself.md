---
site: Largeur.com
title: "La dictature du «do it yourself»"
date: 2015-04-23
href: http://www.largeur.com/?p=4405
tags:
- Économie
- Matériel libre
---

> D’alternative à la consommation, le «do it yourself» s’est mué en une injonction qui gagne tous les secteurs de la société.
