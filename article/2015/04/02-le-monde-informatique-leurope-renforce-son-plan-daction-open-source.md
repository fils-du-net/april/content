---
site: Le Monde Informatique
title: "L'Europe renforce son plan d'action Open Source"
author: Dominique Filippone
date: 2015-04-02
href: http://www.lemondeinformatique.fr/actualites/lire-l-europe-renforce-son-plan-d-action-open-source-60739.html
tags:
- Administration
- Interopérabilité
- Institutions
- Marchés publics
- Europe
---

> La Commission Européenne a mis à jour sa stratégie d'utilisation des logiciels Open Source prévue jusqu'en 2017. L'accent est mis sur l'implication des développeurs de la Commission aux projets de logiciels Open Source et à la généralisation de la European Union Public Licence.
