---
site: Numerama
title: "Cours de programmation à l'école: voici le programme provisoire"
author: Julien L.
date: 2015-04-15
href: http://www.numerama.com/magazine/32803-cours-de-programmation-a-l-ecole-voici-le-programme-provisoire.html
tags:
- Institutions
- Éducation
- Sciences
---

> La programmation informatique doit-elle être enseignée dès l'école primaire, afin de préparer les élèves à vivre dans un monde où les technologies de l'information et de la communication sont omniprésentes? Et si oui, comment doit-elle être prodiguée? Faut-il créer une matière dédiée, comme le français ou les mathématiques, ou doit-on l'inclure dans un cours existant?
