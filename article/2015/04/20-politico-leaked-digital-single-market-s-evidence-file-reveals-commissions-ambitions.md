---
site: Politico
title: "Leaked digital single market’s 'evidence file' reveals Commission's ambitions"
author: Zoya Sheftalovich
date: 2015-04-20
href: http://www.politico.eu/article/leaked-digital-single-market-strategy-evidence
tags:
- Internet
- Institutions
- Europe
---

> (des fuites de la prochaine stratégie du marché commun digital montrent que la commission s'apprête à proposer de vastes réformes qui pourraient tout affecter) Leaked copies of the upcoming Digital Single Market Strategy and its supporting Evidence file show the European Commission is ready to propose vast regulatory reforms that could affect everything from sales taxes and e-privacy to Internet searches and big data.
