---
site: Numerama
title: "L'Assemblée adopte les boîtes noires qui surveilleront votre comportement"
author: Guillaume Champeau
date: 2015-04-16
href: http://www.numerama.com/magazine/32809-l-assemblee-adopte-les-boites-noires-qui-surveilleront-votre-comportement.html
tags:
- Internet
- Économie
- HADOPI
- Institutions
- Vie privée
---

> Animée par la colère de Bernard Cazeneuve qui ne comprend pas pourquoi l'Etat ne pourrait pas user d'outils de collecte et de traitement de données mis en place par Facebook ou Google, l'Assemblée nationale a adopté mercredi soir l'article 2 qui permettra d'imposer des "boîtes noires" aux FAI, hébergeurs et autres réseaux sociaux, pour détecter les comportements suspects de terrorisme en puissance.
