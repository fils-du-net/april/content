---
site: L'OBS
title: "Fablabs: «Le pire mot que je connaisse en français, c’est “bof”»"
author: Gael Cerez
date: 2015-04-19
href: http://rue89.nouvelobs.com/2015/04/19/fablabs-pire-mot-connaisse-francais-cest-bof-258725
tags:
- Économie
- Partage du savoir
- Matériel libre
---

> Selon Neil Gershenfeld, professeur au MIT et créateur du premier «fablab», les laboratoires de fabrication, en faisant des citoyens les producteurs de ce qu’ils consomment, vont révolutionner le système économique.
