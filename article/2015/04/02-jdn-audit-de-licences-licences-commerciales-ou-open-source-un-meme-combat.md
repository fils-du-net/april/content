---
site: JDN
title: "Audit de licences: licences commerciales ou Open Source, un même combat?"
author: Benjamin Jean
date: 2015-04-02
href: http://www.journaldunet.com/solutions/expert/60469/audit-de-licences---licences-commerciales-ou-open-source--un-meme-combat.shtml
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Licenses
---

> La gestion et l'audit de licences sont indissociables de l'usage croissant de logiciels édités par des tiers, qu'ils soient commerciaux ou open source. Le point sur les finalités différentes de ces audits et la manière de les gérer.
