---
site: Next INpact
title: "Les sénateurs PS veulent «taxer» l'impression 3D avec la redevance copie privée"
author: Marc Rees
date: 2015-04-04
href: http://www.nextinpact.com/news/93720-les-senateurs-ps-veulent-taxer-impression-3d-avec-redevance-copie-privee.htm
tags:
- Économie
- Matériel libre
- Institutions
---

> Au Sénat, le groupe socialiste a déposé un amendement visant à étendre la redevance pour copie privée à l’impression 3D. Un texte qui sera débattu à l'occasion du projet de loi Macron sur la croissance, l'activité et l'égalité des chances.
