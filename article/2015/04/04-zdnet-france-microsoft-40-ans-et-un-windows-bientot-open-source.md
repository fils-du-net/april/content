---
site: ZDNet France
title: "Microsoft: 40 ans et un Windows bientôt open source?"
date: 2015-04-04
href: http://www.zdnet.fr/actualites/microsoft-40-ans-et-un-windows-bientot-open-source-39817482.htm
tags:
- Entreprise
- Logiciels privateurs
- Informatique en nuage
---

> Alors que le géant de Redmond prépare activement le lancement de Windows 10, une déclaration du directeur technique de Microsoft Azure jette le trouble.
