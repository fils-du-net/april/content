---
site: Silicon.fr
title: "RGI v2: après les polémiques, un outil enfin utile?"
author: Reynald Fléchaux
date: 2015-04-15
href: http://www.silicon.fr/rgi-v2-apres-polemiques-outil-enfin-utile-114015.html
tags:
- Administration
- Interopérabilité
- Institutions
- Innovation
- RGI
- Standards
---

> Après une première mouture marquée par une guéguerre opposant Microsoft au logiciel libre, le Référentiel Général d’Interopérabilité revient dans une seconde version. La Disic, la DSI de l’Etat, entend cette fois en faire un vrai levier d’optimisation des SI dans le secteur public.
