---
site: Next INpact
title: "Sous-Surveillance.fr, le site de la Quadrature du Net contre la loi Renseignement"
author: Marc Rees
date: 2015-04-01
href: http://www.nextinpact.com/news/93679-sous-surveillance-fr-site-quadrature-net-contre-loi-renseignement.htm
tags:
- Internet
- Institutions
- Associations
- Vie privée
---

> La Quadrature du Net vient de lancer Sous-Surveillance.fr, un site destiné informer le plus grand nombre sur les ombres du projet de loi sur le renseignement, et surtout à permettre à quiconque de sensibiliser les parlementaires encore indécis.
