---
site: Le Monde Informatique
title: "Windows bientôt en Open Source"
author: Jean Elyan
date: 2015-04-07
href: http://www.lemondeinformatique.fr/actualites/lire-windows-bientot-en-open-source-60767.html
tags:
- Entreprise
- Logiciels privateurs
- Informatique en nuage
---

> Un ingénieur de Microsoft a laissé entendre qu'une telle option n'était pas exclue: «C'est du domaine du possible». Une annonce choc à l'occasion des 40 ans de l'éditeur de Redmond.
