---
site: ITespresso
title: "Brevets: Microsoft fait fructifier le trésor Android"
author: Clément Bohic
date: 2015-04-30
href: http://www.itespresso.fr/brevets-microsoft-fructifier-tresor-android-94941.html
tags:
- Entreprise
- Économie
- Brevets logiciels
---

> Qisda (marque BenQ) rejoint la liste des fabricants qui versent des royalties à Microsoft pour vendre des terminaux sous Android ou Chrome OS.
