---
site: 01net.
title: "Et si Windows devenait open source?"
author: Gilbert Kallenborn
date: 2015-04-03
href: http://www.01net.com/editorial/651281/et-si-windows-devenait-open-source
tags:
- Entreprise
- Logiciels privateurs
- Informatique en nuage
---

> A l’occasion d’une conférence professionnelle, un directeur de Microsoft explique qu’une ouverture du code de Windows n’est pas impossible à l’avenir.
