---
site: Mediapart
title: "Loi renseignement: peut-on encore croire à la démocratie parlementaire?"
author: Philippe Aigrain
date: 2015-04-11
href: http://blogs.mediapart.fr/edition/libres-enfants-du-numerique/article/110415/loi-renseignement-peut-encore-croire-la-democratie-parlementaire
tags:
- Internet
- Institutions
- Associations
- Vie privée
---

> Les dispositions de surveillance généralisée et intrusive de la loi Renseignement sont dénoncées avec force par une très grande majorité de ceux qui se sont exprimés publiquement. En dehors de certains services de renseignement et de leurs hommes liges, elle n'est portée que par des politiques qui voient dans l'invocation des risques sécuritaires et la création d'une société de contrôle et de suspicion le seul moyen de préserver un pouvoir de plus en plus incapable de susciter une adhésion positive.
