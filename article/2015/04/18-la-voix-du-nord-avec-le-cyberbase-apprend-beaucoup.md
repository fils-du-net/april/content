---
site: La Voix du Nord
title: "Avec le cyberbase, on apprend beaucoup"
date: 2015-04-18
href: http://www.lavoixdunord.fr/region/avec-le-cyberbase-on-apprend-beaucoup-ia15b36963n2780323
tags:
- Matériel libre
- Sensibilisation
- Associations
- Marchés publics
---

> Dans le cadre des Journées communautaires de l’eau de la CAPH, les internautes fréquentant les espaces numériques Albert-Douay de Bouchain et Beauvillain d’Abscon ont découvert de nouveaux ateliers novateurs accessibles à tout public.
