---
site: L'Informaticien
title: "L’Etat va rafraîchir le Référentiel Général d’Interopérabilité"
author: Emilien Ercolani
date: 2015-04-14
href: http://www.linformaticien.com/actualites/id/36347/l-etat-va-rafraichir-le-referentiel-general-d-interoperabilite.aspx
tags:
- Administration
- Interopérabilité
- April
- Institutions
- Neutralité du Net
- RGI
- Standards
- Open Data
---

> Le RGI, publié pour la première fois en 2009, devrait évoluer. Une version «en mode provisoire» est disponible en ligne et ouverte aux commentaires publics.
