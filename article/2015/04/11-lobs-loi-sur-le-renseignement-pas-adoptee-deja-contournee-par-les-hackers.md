---
site: L'OBS
title: "Loi sur le renseignement: pas adoptée, déjà contournée par les hackers"
author: Jean-Rémy Dubois
date: 2015-04-11
href: http://tempsreel.nouvelobs.com/tech/20150410.OBS6931/loi-sur-le-renseignement-pas-adoptee-deja-contournee-par-les-hackers.html
tags:
- Internet
- Institutions
- Associations
- Vie privée
---

> Le projet de loi sur le renseignement prévoit une surveillance accrue du web. Pas de quoi inquiéter les spécialistes des réseaux qui savent déjà surfer sous le radar des autorités.
