---
site: Developpez.com
title: "Windows pourrait-il devenir Open Source?"
author: Amine Horseman
date: 2015-04-04
href: http://www.developpez.com/actu/83571/Windows-pourrait-il-devenir-Open-Source-C-est-certainement-possible-declare-Mark-Russinovich-directeur-technique-de-Microsoft-Azure
tags:
- Entreprise
- Logiciels privateurs
- Informatique en nuage
---

> «C'est certainement possible», déclare Mark Russinovich, directeur technique de Microsoft Azure
