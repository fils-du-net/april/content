---
site: Next INpact
title: "Loi Renseignement: Bernard Cazeneuve répond à la CNCDH, répondons à Bernard Cazeneuve"
author: Marc Rees
date: 2015-04-29
href: http://www.nextinpact.com/news/93947-loi-renseignement-bernard-cazeneuve-repond-a-cncdh-repondons-a-bernard-cazeneuve.htm
tags:
- Internet
- Institutions
- Vie privée
---

> Le 20 avril dernier, la Commission Nationale Consultative des Droits de l'Homme a publié un avis au vitriol (PDF) concernant le projet de loi sur le renseignement. La missive a peu été appréciée par le ministre de l’Intérieur, qui s’est fendu d’une note en réponse, histoire de contrecarrer ces remarques. Après avoir longuement analysé le projet de loi, nous avons décidé de reprendre les 20 remarques de la CNCDH, mises en avant par le ministre avec ses réponses, afin d’y apporter à notre tour nos commentaires.
