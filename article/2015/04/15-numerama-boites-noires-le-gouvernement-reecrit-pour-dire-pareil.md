---
site: Numerama
title: "Boîtes noires: le Gouvernement réécrit... pour dire pareil"
author: Guillaume Champeau
date: 2015-04-15
href: http://www.numerama.com/magazine/32806-boites-noires-le-gouvernement-reecrit-pour-dire-pareil.html
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> Le Gouvernement a réécrit son article sur les boîtes noires du projet de loi Renseignement, mais en ne le retouchant qu'à la marge. Et encore.
