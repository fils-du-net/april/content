---
site: L'OBS
title: "Michel Bauwens: «Quand survient le chaos, il faut déjà avoir les solutions»"
author: Xavier de La Porte
date: 2015-04-06
href: http://rue89.nouvelobs.com/2015/04/06/quand-survient-chaos-faut-deja-avoir-les-solutions-258517
tags:
- Économie
- Partage du savoir
- Innovation
---

> Pour Michel Bauwens, l’avenir est dans le pair-à-pair, qui permet aux gens de s’organiser en réseau pour créer des communs. Il y voit le germe d’une réforme de l’économie, de la société et même de la spiritualité.
