---
site: InformatiqueNews.fr
title: "Windows bientôt open source?"
author: Channelnews.fr
date: 2015-04-09
href: http://www.informatiquenews.fr/windows-bientot-open-source-34076
tags:
- Entreprise
- Logiciels privateurs
- Informatique en nuage
---

> Le futur de Windows passera-t-il par l’open source ? Ce n’est pas exclu si l’on en croit Mark Russinovich, le directeur technique de Microsoft Azure, dont les propos (exprimés au cours d’une discussion en marge d’une conférence technologique à Santa Clara en Californie) ont été repris par le site Redmond ChannelPartner.
