---
site: Next INpact
title: "Loi numérique: le rapport du CNNum se fait encore attendre"
author: Xavier Berne
date: 2015-04-30
href: http://www.nextinpact.com/news/93969-loi-numerique-rapport-cnnum-se-fait-encore-attendre.htm
tags:
- Internet
- Institutions
---

> Alors que le gouvernement n’a de cesse de promettre que le futur projet de loi numérique d’Axelle Lemaire sera présenté en Conseil des ministres au mois de juin, le Conseil national du numérique (CNNum) ne lui a toujours pas remis son rapport, censé être le préalable à ce texte d’envergure. On fait le point.
