---
site: Next INpact
title: "La programmation informatique pourrait faire son entrée dans le programme de CE1"
author: Xavier Berne
date: 2015-04-14
href: http://www.nextinpact.com/news/93814-la-programmation-informatique-pourrait-faire-son-entree-dans-programme-ce1.htm
tags:
- HADOPI
- Institutions
- Éducation
---

> L’apprentissage de la programmation informatique sera-t-il bientôt obligatoire dès l’école primaire? C’est effectivement ce qui se dessine au travers des projets de programme dévoilés hier par l’Éducation nationale. Des enseignements plus poussés en matière d’algorithmique auraient ensuite lieu au collège, à partir de la cinquième.
