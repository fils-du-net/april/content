---
site: Les Echos
title: "Après le logiciel libre, voici le matériel libre"
author: Jacques Henno
date: 2015-04-14
href: http://www.lesechos.fr/idees-debats/sciences-prospective/0204283695263-apres-le-logiciel-libre-voici-le-materiel-libre-1110940.php
tags:
- Internet
- Partage du savoir
- Matériel libre
- Brevets logiciels
- Innovation
---

> L'«open source» arrive dans l'univers des objets: de plus en plus de designers et d'industriels renoncent à une partie de leurs droits sur leurs créations. Un mouvement pas toujours désintéressé…
