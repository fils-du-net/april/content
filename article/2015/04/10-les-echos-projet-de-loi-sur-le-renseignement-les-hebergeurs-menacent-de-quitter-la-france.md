---
site: Les Echos
title: "Projet de loi sur le renseignement: les hébergeurs menacent de quitter la France"
author: Sandrine Cassini
date: 2015-04-10
href: http://www.lesechos.fr/tech-medias/hightech/0204293137141-projet-de-loi-sur-le-renseignement-les-hebergeurs-menacent-de-quitter-la-france-1110178.php
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Vie privée
---

> Le projet est débattu ce lundi à l’Assemblée nationale. Opérateurs télécoms et hébergeurs critiquent la mise en place de «boîtes noires» sur leurs infrastructures.
