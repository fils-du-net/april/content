---
site: L'OBS
title: "Réparer un tracteur, une voiture? Illégal selon les constructeurs"
author: Andréa Fradin
date: 2015-04-27
href: http://rue89.nouvelobs.com/2015/04/27/reparer-tracteur-voiture-les-constructeurs-estiment-cest-illegal-258871
tags:
- Entreprise
- Institutions
- Droit d'auteur
---

> Effet direct de l’évolution de la propriété intellectuelle aux Etats-Unis: les agriculteurs n’auraient pas le droit de réparer leurs tracteurs... sous peine d’être poursuivis pour violation du copyright.
