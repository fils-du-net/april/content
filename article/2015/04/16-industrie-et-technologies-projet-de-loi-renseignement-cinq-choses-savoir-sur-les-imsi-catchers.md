---
site: Industrie et Technologies
title: "Projet de loi Renseignement: cinq choses à savoir sur les Imsi-catchers"
author: Juliette Raynal
date: 2015-04-16
href: http://www.industrie-techno.com/projet-de-loi-renseignement-cinq-choses-a-savoir-sur-les-imsi-catchers.37645
tags:
- Institutions
- Innovation
- Vie privée
---

> Actuellement examiné au Parlement, le projet de loi Renseignement entend notamment permettre aux services de renseignement d'utiliser légalement les Imsi-catchers afin d'intercepter et de surveiller certaines conversations téléphoniques. Le point sur le fonctionnement de ces valises-espionnes.
