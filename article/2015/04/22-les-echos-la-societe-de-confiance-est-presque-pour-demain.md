---
site: Les Echos
title: "La société de confiance est (presque) pour demain"
author: Jean-Christophe Despres, Mehdi Benchoufi
date: 2015-04-22
href: http://www.lesechos.fr/idees-debats/cercle/cercle-132262-la-societe-de-confiance-est-presque-pour-demain-1113554.php
tags:
- Internet
- Administration
- Économie
- Institutions
- Innovation
- Sciences
---

> La Blockchain favorise l'émergence d'une société peer to peer. Notre système représentatif est-il à bout de souffle? Sommes-nous malades de la défiance? Comment recréer du commun dans des sociétés atomisées?
