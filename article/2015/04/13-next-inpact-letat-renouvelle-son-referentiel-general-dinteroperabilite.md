---
site: Next INpact
title: "L'État renouvelle son «référentiel général d'interopérabilité»"
author: Xavier Berne
date: 2015-04-13
href: http://www.nextinpact.com/news/93707-l-etat-renouvelle-son-referentiel-general-d-interoperabilite.htm
tags:
- Administration
- Interopérabilité
- April
- Institutions
- RGI
- Standards
---

> Le «référentiel général d’interopérabilité», qui s’applique depuis 2009 à toutes les administrations françaises, s’apprête à subir une refonte en bonne et due forme. La Direction interministérielle des systèmes d'information et de Communication (DISIC) a en effet lancé voilà plusieurs jours un appel à commentaires sur ce qui pourrait devenir la «V2» de ce document normatif.
