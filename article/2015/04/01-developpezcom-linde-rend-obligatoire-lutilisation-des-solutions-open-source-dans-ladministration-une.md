---
site: Developpez.com
title: "L'Inde rend obligatoire l'utilisation des solutions open source dans l'administration, une grande victoire pour les logiciels libres?"
author: zoom61
date: 2015-04-01
href: http://www.developpez.com/actu/83364/L-Inde-rend-obligatoire-l-utilisation-des-solutions-open-source-dans-l-administration-une-grande-victoire-pour-les-logiciels-libres
tags:
- Administration
- Institutions
- International
---

> Le gouvernement a annoncé dimanche une politique sur l'adoption des logiciels Open source, qui rend obligatoire pour toutes les applications et les services du gouvernement d'être développés en utilisant des logiciels Open source, de sorte que les projets puissent «assurer l'efficacité, la transparence et la fiabilité de ces services à un coût abordable».
