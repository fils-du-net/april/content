---
site: Developpez.com
title: "Le serveur Web Apache souffle sa 20e bougie"
author: Amine Horseman
date: 2015-04-04
href: http://www.developpez.com/actu/83561/Le-serveur-Web-Apache-souffle-sa-20e-bougie-que-pensez-vous-de-l-evolution-du-serveur-le-plus-utilise-sur-le-Web
tags:
- Internet
- Associations
---

> L’Apache Software Foundation (ASF) célèbrera la 15ème année de sa création lors de l’ApacheCon qui se déroulera du 13 au 17 avril 2015 à Austin au Texas. Durant cet évènement, l’ASF célèbrera également le 20ème anniversaire du célèbre Apache HTTP Web Server avec la présence des membres fondateurs du Groupe Apache.
