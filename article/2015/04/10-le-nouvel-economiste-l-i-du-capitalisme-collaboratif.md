---
site: Le nouvel Economiste
title: "L’an I du capitalisme collaboratif"
author: Edouard Laugier
date: 2015-04-10
href: http://www.lenouveleconomiste.fr/lan-i-du-capitalisme-collaboratif-26762
tags:
- Économie
- Matériel libre
- Innovation
- Sciences
---

> Les incroyables possibilités de l’économie collaborative, - le peer-to-peer-, vues par son théoricien belge Michel Bauwens
