---
site: PC INpact
title: "Affaire DCRI contre Wikipedia: interview du juriste Cédric Manara"
author: Marc Rees
date: 2013-04-08
href: http://www.pcinpact.com/news/78881-affaire-dcri-contre-wikipedia-interview-juriste-cedric-manara.htm
tags:
- Internet
- Partage du savoir
- Institutions
- Associations
---

> La Direction centrale du renseignement intérieur (ou DCRI) a réclamé d’urgence de la Wikimedia Foundation la suppression d’un article de la version française de l’encyclopédie libre. L’hébergeur américain n’a pas compris en quoi cette fiche sur une installation militaire française était couverte par le secret. La DCRI a obtenu gain de cause en menaçant Rémi Mathis, président de l’association Wikimedia France, d’une garde à vue et d’une mise en examen. Cédric Manara, professeur de droit à l'EDHEC Business School (LegalEDHEC Research Center), nous livre son analyse.
