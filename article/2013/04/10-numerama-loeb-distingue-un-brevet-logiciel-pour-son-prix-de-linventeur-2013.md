---
site: Numerama
title: "L'OEB distingue un brevet logiciel pour son \"prix de l'inventeur 2013\""
author: Guillaume Champeau
date: 2013-04-10
href: http://www.numerama.com/magazine/25646-l-oeb-distingue-un-brevet-logiciel-pour-son-34prix-de-l-inventeur-201334.html
tags:
- April
- Institutions
- Brevets logiciels
- Innovation
- Europe
- International
---

> En principe, les brevets logiciels sont interdits en Europe. En pratique, l'Office Européen des Brevets (OEB) en accorde régulièrement. Mercredi, l'Office a même été jusqu'à distinguer l'un d'entre eux dans sa sélection d'innovations méritant le "prix de l'inventeur européen 2013".
