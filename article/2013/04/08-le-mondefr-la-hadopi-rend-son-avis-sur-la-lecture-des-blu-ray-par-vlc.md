---
site: Le Monde.fr
title: "La Hadopi rend son avis sur la lecture des Blu-ray par VLC"
author: Guénaël Pépin
date: 2013-04-08
href: http://www.lemonde.fr/technologies/article/2013/04/08/la-hadopi-rend-son-avis-sur-la-lecture-des-blu-ray-par-vlc_3156110_651865.html
tags:
- Entreprise
- Interopérabilité
- HADOPI
- Éducation
- Video
---

> L'autorité n'impose pas à Sony de fournir les clés nécessaires à la lecture aux développeurs du lecteur multimédia, mais ouvre la porte à un règlement de différend.
