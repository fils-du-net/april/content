---
site: Joinup
title: "Spain's Extremadura starts switch of 40,000 government PCs to open source"
author: Gijs Hillenius
date: 2013-04-29
href: https://joinup.ec.europa.eu/community/osor/news/spains-extremadura-starts-switch-40000-government-pcs-open-source
tags:
- Administration
- Économie
- International
- English
---

> (Le gouvernement de la région Espagnole "Estrémadure" a démarré le passage en open source de ses 40 000 PC) The government of Spain's autonomous region of Extremadura has begun the switch to open source of it desktop PCs. The government expects the majority of its 40,000 PCs to be migrated this year, the region's CIO Theodomir Cayetano announced on 18 April. Extremadura estimates that the move to open source will help save 30 million euro per year.
