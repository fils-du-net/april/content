---
site: Developpez.com
title: "États-Unis: quand la guerre de brevets devient un troll, des poids lourds de l'industrie de la technologie veulent y mettre un terme"
author: Stéphane le calme
date: 2013-04-08
href: http://www.developpez.com/actu/53765/-tats-Unis-quand-la-guerre-de-brevets-devient-un-troll-des-poids-lourds-de-l-industrie-de-la-technologie-veulent-y-mettre-un-terme
tags:
- Entreprise
- Brevets logiciels
- International
---

> Depuis quelques années déjà, les procès autour des brevets s’enchaînent aux États-Unis.
