---
site: La Presse
title: "Comment internet nous espionne: notre cyberprison"
author: Rudy Le Cours
date: 2013-04-29
href: http://techno.lapresse.ca/nouvelles/internet/201304/29/01-4645511-comment-internet-nous-espionne-notre-cyberprison.php
tags:
- Internet
- Institutions
- Sensibilisation
- Informatique-deloyale
- International
---

> L'attaque terroriste au marathon de Boston et le complot déjoué pour faire sauter un train au-dessus de la rivière Niagara ont fait ressortir à quel point la surveillance policière dispose désormais de moyens très puissants.
