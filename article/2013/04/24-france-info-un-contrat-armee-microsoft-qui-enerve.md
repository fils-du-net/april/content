---
site: France Info
title: "Un contrat armée-Microsoft qui énerve"
date: 2013-04-24
href: http://www.franceinfo.fr/high-tech/nouveau-monde/un-contrat-armee-microsoft-qui-enerve-962963-2013-04-24
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- April
- Standards
---

> Microsoft et l'armée seraient-ils trop liés? C'est l'avis des défenseurs du logiciel libre et de quelques responsables politiques.
