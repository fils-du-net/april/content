---
site: L'Expansion.com
title: "Hackers: pour en finir avec les idées reçues"
author: Raphaële Karayan
date: 2013-04-03
href: http://lexpansion.lexpress.fr/high-tech/hackers-pour-en-finir-avec-les-idees-recues_378670.html?xtor=EPR-176-[xpn_hightech]-20130408--25453562@238986175-20130408123001slts
tags:
- Internet
- Partage du savoir
- Associations
---

> Qui sont vraiment les hackers, ces férus d'informatique épris de libertés numériques que l'on qualifie de "pirates"? La journaliste Amaelle Guiton a enquêté et livre des témoignages de ces individus trop souvent caricaturés. Entretien.
