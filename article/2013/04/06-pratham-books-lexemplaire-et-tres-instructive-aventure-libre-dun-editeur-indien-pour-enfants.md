---
site: Framablog
title: "L'exemplaire et très instructive aventure libre d'un éditeur indien pour enfants"
author: John Gautam (Traduction Framalang)
date: 2013-04-06
href: http://www.framablog.org/index.php/post/2013/04/06/pratham-books-creative-commons
tags:
- Entreprise
- Partage du savoir
- Éducation
- Licenses
- Contenus libres
- International
---

> Il y avait de multiples possibilités très intéressantes. Mais rapidement nous avons pris conscience des limitations des lois traditionnelles du « copyright » et les complexités administratives nécessaires aux négociations bi/multilatérales pour utiliser ou réutiliser du contenu sous licence. Nous n’avions ni l’envergure ni les ressources nécessaires pour nous engager dans ce type de négociations.
