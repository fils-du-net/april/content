---
site: Numerama
title: "La journée mondiale 2013 contre les DRM, c'est aujourd'hui"
author: Julien L.
date: 2013-04-03
href: http://www.numerama.com/magazine/25870-la-journee-mondiale-2013-contre-les-drm-c-est-aujourd-hui.html
tags:
- Internet
- April
- DRM
- Promotion
- Standards
---

> Comme chaque année, la FSF et l'APRIL se mobilisent contre les menottes numériques. Les deux organisations animent une journée mondiale contre les DRM, afin de sensibiliser le public sur leurs effets. L'édition 2013 se déroule toutefois dans une cadre spécifique, car le W3C envisage d'introduire ces verrous numériques dans la charpente même du web, en les autorisant dans le HTML5.
