---
site: MacGeneration
title: "Brevet: le \"glisser pour déverrouiller\" invalidé et un possible nouveau jugement"
author: Stéphane Moussie
date: 2013-04-05
href: http://www.macg.co/news/voir/259446/brevet-le-glisser-pour-deverrouiller-invalide-et-un-possible-nouveau-jugement
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Innovation
---

> Le Bundespatentgericht, tribunal allemand qui s'occupe des affaires de brevets, a invalidé le brevet EP1964022 d'Apple qui porte sur le geste "glisser pour déverrouiller".
