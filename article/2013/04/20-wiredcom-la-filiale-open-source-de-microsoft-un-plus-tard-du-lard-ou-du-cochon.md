---
site: Framablog
title: "La filiale open source de Microsoft un an plus tard: du lard ou du cochon?"
author: Robert McMillan (traduction framablog)
date: 2013-04-20
href: http://www.framablog.org/index.php/post/2013/04/20/microsoft-open-source
tags:
- Entreprise
- Logiciels privateurs
---

> La semaine derniere, Microsoft Open Technologies S.A. a fêté son premier anniversaire, tranquillement, sans fanfare, mais la semaine prochaine, Microsoft prévoit d’organiser une réception sur son campus de la Silicon Valley.
