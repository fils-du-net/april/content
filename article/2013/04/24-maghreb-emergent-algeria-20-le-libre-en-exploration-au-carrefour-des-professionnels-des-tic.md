---
site: Maghreb Emergent
title: "Algeria 2.0: Le \"libre\" en exploration au carrefour des professionnels des TIC"
date: 2013-04-24
href: http://www.maghrebemergent.info/high-tech/information-technology/item/23302-algeria-2-0-le-libre-en-exploration-au-carrefour-des-professionnels-des-tic.html
tags:
- Internet
- Partage du savoir
- Sensibilisation
- Associations
- Promotion
- Contenus libres
- International
---

> Opendata, openstreetmap, opensource, opensoftware, le concept du "libre" fait de plus en plus d'émules sur Internet DZ où les initiatives fleurissent sous l'impulsion d'une communauté engagée et active. Portrait des acteurs de ce mouvement rencontrés lors de la "Journée libre", au cyberparc de Sidi Abdallah, dans le cadre du Carrefour international des professionnels des TIC et d’Algeria 2.0, du 15 au 20 avril 2013.
