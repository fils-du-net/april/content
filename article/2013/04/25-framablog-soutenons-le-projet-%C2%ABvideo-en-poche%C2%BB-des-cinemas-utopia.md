---
site: Framablog
title: "Soutenons le projet «Vidéo en Poche» des cinémas Utopia"
author: aKa
date: 2013-04-25
href: http://www.framablog.org/index.php/post/2013/04/25/video-en-poche-utopia-rodolphe-village
tags:
- Entreprise
- Internet
- Partage du savoir
- HADOPI
- Associations
- DRM
- Licenses
- Video
- Contenus libres
---

> Venir avec sa clé USB pour la faire remplir au cinéma, ça me rappelle quand ma maman allait chercher du lait dans sa bouteille alu.
