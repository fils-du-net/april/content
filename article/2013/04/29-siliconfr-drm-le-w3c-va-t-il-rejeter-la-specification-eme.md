---
site: Silicon.fr
title: "DRM: le W3C va-t-il rejeter la spécification EME?"
author: Ariane Beky
date: 2013-04-29
href: http://www.silicon.fr/drm-w3c-eme-85727.html
tags:
- Entreprise
- Internet
- Interopérabilité
- April
- DRM
- Standards
---

> Pour empêcher l'introduction de DRM dans HTML5, vingt-sept organisations appellent le W3C à désapprouver les extensions pour médias chiffrés.
