---
site: LeMagIT
title: "Le ministère de la Défense poursuit sa lune de miel avec Microsoft"
author: Reynald Fléchaux
date: 2013-04-18
href: http://www.lemagit.fr/divers/2013/04/18/le-ministere-de-la-defense-poursuit-sa-lune-de-miel-avec-microsoft
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Interopérabilité
- April
- Institutions
---

> Malgré la circulaire Ayrault sur le logiciel libre, malgré la lutte menée par Bercy contre les mécanismes d’optimisation fiscale des grands noms américains du logiciel, la Défense envisage de reconduire son contrat cadre géant avec Microsoft. Qui facturera depuis l’Irlande.
