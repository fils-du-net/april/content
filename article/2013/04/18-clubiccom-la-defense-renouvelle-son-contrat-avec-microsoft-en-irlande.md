---
site: Clubic.com
title: "La Défense renouvelle son contrat avec Microsoft, en Irlande"
author: Thomas Pontiroli
date: 2013-04-18
href: http://pro.clubic.com/entreprises/microsoft/actualite-554582-microsoft-defense.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- April
- HADOPI
- Institutions
---

> L'armée française est sur le point de reconduire son contrat avec Microsoft. Celui-ci fait grand débat car il exclut tout appel d'offre, menace la souveraineté nationale et sera facturé dans un paradis fiscal.
