---
site: Numerama
title: "Hadopi/VLC: publication de l'avis dans les jours à venir"
author: Julien L.
date: 2013-04-03
href: http://www.numerama.com/magazine/25570-hadopi-vlc-publication-de-l-avis-dans-les-jours-a-venir.html
tags:
- Internet
- Interopérabilité
- HADOPI
- Associations
- DRM
- Video
---

> L'attente touche à sa fin. Dans les prochains jours, la Hadopi va publier son avis sur l'éventualité d'un feu vert accordé à VLC pour contourner les DRM des disques Blu-ray afin que ces derniers puissent être lus depuis une distribution Linux.
