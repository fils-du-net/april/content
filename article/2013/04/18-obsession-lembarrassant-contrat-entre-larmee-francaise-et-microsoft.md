---
site: Obsession
title: "L'embarrassant contrat entre l'armée française et Microsoft"
author: Paul Laubacher
date: 2013-04-18
href: http://obsession.nouvelobs.com/high-tech/20130417.OBS8191/l-embarrasant-contrat-entre-l-armee-francaise-et-microsoft.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- April
- Institutions
- Marchés publics
---

> Un risque d'espionnage informatique par les renseignements américains, un coût supplémentaire de 3 millions d'euros, aucun appel d'offres...
