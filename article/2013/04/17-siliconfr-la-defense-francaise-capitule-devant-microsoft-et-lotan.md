---
site: Silicon.fr
title: "La Défense française capitule devant Microsoft et l'OTAN?"
author: Ariane Beky
date: 2013-04-17
href: http://www.silicon.fr/defense-francaise-microsoft-otan-85386.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Interopérabilité
- April
- Institutions
- Informatique-deloyale
---

> Actuellement renégocié, le contrat-cadre signé en 2009 entre Microsoft et le ministère de la Défense aurait été influencé par les choix de l’Organisation du traité de l’Atlantique Nord.
