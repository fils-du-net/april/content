---
site: Framablog
title: "Du projet étudiant au navigateur web, la trajectoire d'un développeur open source"
author: Goofy
date: 2013-04-08
href: http://www.framablog.org/index.php/post/2013/04/08/Du-projet-etudiant-au-navigateur-web-la-trajectoire-developpeur-open-source
tags:
- Sensibilisation
- Éducation
- Innovation
- Licenses
---

> Aujourd’hui nous choisissons de mettre en lumière un jeune développeur qui devrait donner des idées à tous les étudiants en informatique qui nous lisent. Comme vous allez le voir, en choisissant la voie de l’open source, les projets qui paraissaient hors d’atteinte sont finalement accessibles. Rien n’est gagné d’avance mais la voie est libre!
