---
site: PC INpact
title: "VLC et Blu-Ray: «Voilà à quoi devait répondre l'Hadopi»"
author: Marc Rees
date: 2013-04-10
href: http://www.pcinpact.com/news/78939-vlc-et-blu-ray-voila-a-quoi-devait-repondre-hadopi.htm
tags:
- Interopérabilité
- HADOPI
- DRM
- Standards
- Video
---

> Marie Duponchelle, qui prépare une thèse sur les «mesures techniques de protection et [les] droits du consommateur» , est avocate en droit des TIC et des logiciels libres. Au sein de l'association VideoLan, éditrice de VLC, elle a participé à la saisine devant la Hadopi concernant le Blu-Ray. Dans un entretien, elle revient avec nous sur l’avis rendu hier par la Haute autorité.
