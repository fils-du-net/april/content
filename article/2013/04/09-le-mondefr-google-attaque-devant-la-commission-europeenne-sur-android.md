---
site: Le Monde.fr
title: "Google attaqué devant la Commission européenne sur Android"
date: 2013-04-09
href: http://www.lemonde.fr/technologies/article/2013/04/09/google-attaque-devant-la-commission-europeenne-sur-android_3156766_651865.html
tags:
- Entreprise
- Institutions
- Europe
---

> Des concurrents de Google estiment que l'entreprise exploite son système pour téléphones pour imposer ses services aux constructeurs et utilisateurs.
