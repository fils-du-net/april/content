---
site: Le Monde.fr
title: "Intellectual Ventures: l'épouvantail de la Silicon Valley"
author: Jérôme Marin
date: 2013-04-02
href: http://www.lemonde.fr/economie/article/2013/04/02/intellectual-ventures-l-epouvantail-de-la-silicon-valley_3151924_3234.html
tags:
- Entreprise
- Économie
- Institutions
- Brevets logiciels
- Éducation
- Innovation
---

> Avec 40 000 brevets en portefeuille, Intellectual Ventures effraie de nombreuses start-up américaines. Son activité: racheter des licences à tour de bras et menacer de procès les sociétés qui les utilisent.
