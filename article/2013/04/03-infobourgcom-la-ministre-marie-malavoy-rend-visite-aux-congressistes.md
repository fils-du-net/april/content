---
site: Infobourg.com
title: "La ministre Marie Malavoy rend visite aux congressistes"
author: Audrey Miller
date: 2013-04-03
href: http://www.infobourg.com/2013/04/03/aquops-2013-la-ministre-marie-malavoy-rend-visite-aux-congressistes
tags:
- Institutions
- Éducation
- International
---

> La visite de la ministre de l’Éducation, du Loisir et du Sport du Québec, Mme Marie Malavoy, a marqué la conclusion de la 31e édition du colloque annuel de l’AQUOPS, qui s’est tenu du 26 au 28 mars 2013 à Québec.
