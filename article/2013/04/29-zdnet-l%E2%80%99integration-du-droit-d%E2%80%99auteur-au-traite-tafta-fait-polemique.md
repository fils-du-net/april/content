---
site: ZDNet
title: "L’intégration du droit d’auteur au traité TAFTA fait polémique"
author: Thierry Noisette
date: 2013-04-29
href: http://www.zdnet.fr/actualites/l-integration-du-droit-d-auteur-au-traite-tafta-fait-polemique-39789909.htm
tags:
- Internet
- Institutions
- Associations
- Brevets logiciels
- Droit d'auteur
- Neutralité du Net
- Europe
- ACTA
---

> La question de la protection des droits d’auteur, des brevets et des marques va être discutée dans le cadre des négociations du TAFTA (Transatlantic Free Trade Agreement).
