---
site: Nouvelle République
title: "Acteurs du futur numérique"
date: 2013-04-11
href: http://www.lanouvellerepublique.fr/Loir-et-Cher/Communautes-NR/n/Contenus/Articles/2013/04/11/Acteurs-du-futur-numerique-1406293
tags:
- Internet
- Administration
- Open Data
---

> Dans le cadre de l’opération “Loir-et-Cher 2020” organisée par le conseil général, plusieurs “Labs41” débarquent. Le point sur ces expérimentations.
