---
site: Trading Sat
title: "Marché: Microsoft a-t-il contribué à la chute des ventes de PC?"
author: J. M.
date: 2013-04-11
href: http://www.tradingsat.com/actualites/marches-financiers/microsoft-a-t-il-contribue-a-la-chute-des-ventes-de-pc-439178.html
tags:
- Entreprise
- Logiciels privateurs
- Économie
- International
---

> Le marché mondial des ordinateurs a signé le pire trimestre de son histoire, chutant de 13,9% sur les trois premiers mois de l'année 2013 comparativement au T1 2012, a annoncé mercredi le cabinet International Data Corporation (IDC).
