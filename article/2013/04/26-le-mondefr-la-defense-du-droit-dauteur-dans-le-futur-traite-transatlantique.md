---
site: Le Monde.fr
title: "La défense du droit d'auteur dans le futur traité transatlantique"
date: 2013-04-26
href: http://www.lemonde.fr/technologies/article/2013/04/26/la-defense-du-droit-d-auteur-dans-le-futur-traite-transatlantique_3167444_651865.html
tags:
- Entreprise
- Internet
- Institutions
- Brevets logiciels
- Droit d'auteur
- Europe
- International
- ACTA
---

> Le futur traité commercial entre Etats-Unis et Europe pourrait contenir des mesures renforçant la défense du droit d'auteur, au détriment des libertés personnelles, s'inquiètent certains de leurs défenseurs.
