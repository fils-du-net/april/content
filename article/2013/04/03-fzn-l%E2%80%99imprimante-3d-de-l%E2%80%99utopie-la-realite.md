---
site: FZN
title: "L’imprimante 3D: de l’utopie à la réalité"
author: Fanny Corona
date: 2013-04-03
href: http://www.fredzone.org/limprimante-3d-de-lutopie-a-la-realite-392
tags:
- Entreprise
- Économie
- Matériel libre
- Innovation
---

> L’imprimante 3D semble être l’une des plus grande innovation technologique de notre époque. Pourtant elle existe bel et bien depuis les années 90. Sa performance et sa puissance de production se sont fortement améliorées depuis lors. Conçue pour les industriels, aujourd’hui elle s’est démocratisée et est accessible pour les particuliers, pour un prix d’entrée de 375 euros environ.
