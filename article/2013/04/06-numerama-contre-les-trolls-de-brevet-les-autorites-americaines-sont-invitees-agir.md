---
site: Numerama
title: "Contre les trolls de brevet, les autorités américaines sont invitées à agir"
author: Julien L.
date: 2013-04-06
href: http://www.numerama.com/magazine/25609-contre-les-trolls-de-brevet-les-autorites-americaines-sont-invitees-a-agir.html
tags:
- Entreprise
- Économie
- Institutions
- Brevets logiciels
- Innovation
- International
---

> L'EFF veut mettre les trolls en cage. Mais pas n'importe lesquels. L'organisation cible tout particulièrement les trolls de brevet (patent trolls), qui s'attaquent aux startups et aux PME pour leur extorquer de l'argent en les menaçant de les poursuivre en justice au nom de leur supposée propriété industrielle bafouée. L'EFF réclame l'intervention de la FTC.
