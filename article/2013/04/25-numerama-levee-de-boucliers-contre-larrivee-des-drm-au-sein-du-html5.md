---
site: Numerama
title: "Levée de boucliers contre l'arrivée des DRM au sein du HTML5"
author: Julien L.
date: 2013-04-25
href: http://www.numerama.com/magazine/25802-levee-de-boucliers-contre-l-arrivee-des-drm-au-sein-du-html5.html
tags:
- Entreprise
- Internet
- April
- DRM
- Droit d'auteur
- Standards
---

> En marge du développement du HTML5, le W3C se penche sur la conception d'extensions pour médias chiffrés. Autrement dit, l'organisme de normalisation travaille à introduire des DRM au sein du HTML5. Une information qui alarme de nombreuses organisations, qui se sont regroupées en coalition pour s'opposer à ce projet. Elles ont écrit une lettre au président du W3C pour l'inviter à rejeter l'arrivée des verrous numériques au sein des standards du web.
