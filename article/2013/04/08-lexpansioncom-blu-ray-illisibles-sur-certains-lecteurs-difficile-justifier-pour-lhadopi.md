---
site: L'Expansion.com
title: "Blu-Ray illisibles sur certains lecteurs: difficile à justifier, pour l'Hadopi"
author: Raphaële Karayan
date: 2013-04-08
href: http://lexpansion.lexpress.fr/high-tech/blu-ray-illisibles-sur-certains-lecteurs-difficile-a-justifier-pour-l-hadopi_379244.html
tags:
- Interopérabilité
- HADOPI
- DRM
- Droit d'auteur
- Video
---

> En réponse à la saisine de l'association Videolan, qui édite le lecteur multimédia open source VLC, l'Hadopi a estimé que le contournement des DRM était impossible sans demande préalable de licence auprès des ayants droit. Mais que dans l'éventualité d'un litige, leur position serait difficile à tenir.
