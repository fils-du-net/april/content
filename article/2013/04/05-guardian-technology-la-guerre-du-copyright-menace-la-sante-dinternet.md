---
site: Framablog
title: "La guerre du copyright menace la santé d'Internet"
author: Cory Doctorow (Traduction Framalang)
date: 2013-04-05
href: http://www.framablog.org/index.php/post/2013/04/05/guerre-copyright-menace-internet-doctorow
tags:
- Entreprise
- Internet
- Institutions
- DRM
- Droit d'auteur
- Contenus libres
---

> Ceux qui veulent trouver à tout prix des «solutions» contre le piratage risquent d’altérer l’intégrité et la liberté du réseau par la surveillance, la censure et le contrôle.
