---
site: Framablog
title: "Pourquoi nous avons besoin de jouer, partager et bidouiller la science"
author: Rayna Stamboliyska (Traduction Framalang)
date: 2013-04-03
href: http://www.framablog.org/index.php/post/2013/04/03/science-jouer-partager-hacker
tags:
- Éducation
- Sciences
---

> Les citoyens activement engagés dans la production scientifique fournissent le meilleur effort de compréhension de la science, et ce à tout âge.
