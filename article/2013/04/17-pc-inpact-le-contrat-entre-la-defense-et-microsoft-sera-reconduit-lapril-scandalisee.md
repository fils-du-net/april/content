---
site: PC INpact
title: "Le contrat entre la Défense et Microsoft sera reconduit, l'April scandalisée"
author: Nil Sanyas
date: 2013-04-17
href: http://www.pcinpact.com/news/79148-le-contrat-entre-defense-et-microsoft-sera-reconduit-april-scandalisee.htm
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Interopérabilité
- April
- Institutions
- Promotion
---

> Le Canard Enchainé publié ce mercredi 17 avril a confirmé que le ministère de la Défense allait bien reconduire son fameux contrat avec Microsoft, confortant notre actualité du mois de février dernier. Selon l'hebdomadaire, le choix de la Défense «coûte cher, augmente les risques d'espionnage et se négocie... dans un paradis fiscal».
