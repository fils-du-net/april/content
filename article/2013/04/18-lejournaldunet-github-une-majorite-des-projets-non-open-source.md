---
site: LeJournalduNet
title: "GitHub: une majorité des projets non-Open Source"
author: Antoine Crochet-Damais
date: 2013-04-18
href: http://www.journaldunet.com/developpeur/outils/github-des-sources-non-open-source-0413.shtml
tags:
- Internet
- Droit d'auteur
- Licenses
---

> Se présentant désormais comme un réseau social de développeurs, la plate-forme de partage de code source serait-elle prise d'assaut par des éditeurs propriétaires ? Une étude sème le doute.
