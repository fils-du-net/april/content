---
site: Numerama
title: "La DCRI intimide un administrateur de Wikipédia à cause d'un article"
author: Julien L.
date: 2013-04-06
href: http://www.numerama.com/magazine/25610-la-dcri-intimide-un-administrateur-de-wikipedia-a-cause-d-un-article.html
tags:
- Internet
- Partage du savoir
- Institutions
- Associations
---

> C'est une guerre d'édition d'un genre nouveau qu'a vécu la version francophone de Wikipédia. La DCRI a cherché à obtenir la suppression d'une page début mars. N'obtenant pas satisfaction mais n'ayant pas non plus motivé sa démarche, la DCRI a alors décidé de faire pression sur un administrateur bénévole résidant en France pour l'obliger à supprimer le contenu. Et alors, l'effet Streisand se déclencha.
