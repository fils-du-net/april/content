---
site: opensource.com
title: "Interview with Loïc Dachary of Upstream University"
author: Jen Wike
date: 2013-04-08
href: http://opensource.com/business/13/4/upstream-university
tags:
- Entreprise
- Sensibilisation
- English
---

> (Loïc Dachary affirme que l'Upstream University est née de l'idée inhabituelle que la formation à devenir un meilleur contributeur vaut la peine d'être considérée) Loïc Dachary says that the training program called Upstream University was born from the unusual idea that training to become a better contributor is worth considering.
