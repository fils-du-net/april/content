---
site: PC INpact
title: "La Hadopi va bientôt dire si VLC a le droit de lire le Blu-ray"
author: Marc Rees
date: 2013-04-03
href: http://www.pcinpact.com/news/78791-la-hadopi-va-bientot-dire-si-vlc-a-droit-lire-blu-ray.htm
tags:
- Interopérabilité
- HADOPI
- DRM
- Video
---

> Le secrétaire général de la Hadopi, Éric Walter, l’a annoncé sur son fil Twitter: «le collège de la Hadopi a adopté l'avis en réponse à la demande #VLC. [Sa] publication [est prévue] dans les jours à venir» . Il aura fallu plus d’un an à la Hadopi pour répondre à la problématique soulevée par l’association VidéoLAN. On ne connaît cependant pas encore le sens de la réponse apportée par la Haute autorité.
