---
site: "Futura-Sciences"
title: "Piratage, DRM: l'impression 3D s'invite dans les débats sur le \"libre\""
date: 2013-04-28
href: http://www.futura-sciences.com/fr/news/t/internet/d/piratage-drm-limpression-3d-sinvite-dans-les-debats-sur-le-libre_46111/
tags:
- Entreprise
- Internet
- Partage du savoir
- Matériel libre
- Droit d'auteur
- Innovation
---

> Droit d’auteur, propriété intellectuelle, piratage, DRM, mouvement «open source»: ces thèmes bien connus traversent des débats sur les logiciels, la musique ou les films, et atteignent aujourd’hui l’impression 3D. Pour la Journée mondiale de la propriété intellectuelle, dont le thème 2013 était «Créativité: la prochaine génération», une exposition y est consacrée à Genève. Découvrez ce nouveau débat.
