---
site: ZDNet
title: "DRM: VideoLAN \"atterré\" par l'avis rendu par l'Hadopi"
date: 2013-04-08
href: http://www.zdnet.fr/actualites/drm-videolan-atterre-par-l-avis-rendu-par-l-hadopi-39789125.htm
tags:
- Interopérabilité
- HADOPI
- DRM
- Video
---

> La Hadopi vient de rendre son avis suite à la question posée par l'éditeur sur les DRM des Blu-ray. Avis insuffisant selon ce dernier, mais qui lui laisse pourtant une porte de sortie entrouverte.
