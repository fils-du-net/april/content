---
site: Sud Ouest
title: "Opération libre a tenu ses promesses"
author: Jean-Marie Tinarrage
date: 2013-04-12
href: http://www.sudouest.fr/2013/04/12/operation-libre-a-tenu-ses-promesses-1022849-3318.php
tags:
- Internet
- Administration
- Partage du savoir
- Associations
- Open Data
---

> Samedi et dimanche derniers, Brocas a fait le buzz auprès des internautes du libre, entendez ceux qui n’utilisent que des logiciels libres auxquels ils apportent leur contribution.
