---
site: 01netPro.
title: "Pour l'April, «le tour de France du numérique pour l'éducation» sert de show-room à Microsoft"
author: Xavier Biseul
date: 2013-04-03
href: http://pro.01net.com/editorial/592049/pour-l-april-le-tour-de-france-du-numerique-pour-l-education-sert-de-show-room-a-microsoft
tags:
- Entreprise
- Administration
- April
- Éducation
---

> Depuis fin mars et jusqu’au 16 octobre, le Centre National de Documentation Pédagogique (CNDP) et le Café pédagogique organisent un «Tour de France du Numérique pour l'éducation» dans vingt villes-étapes.
