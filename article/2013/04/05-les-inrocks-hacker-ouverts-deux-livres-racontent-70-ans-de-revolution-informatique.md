---
site: Les Inrocks
title: "Hacker ouverts, deux livres racontent 70 ans de révolution informatique"
author: Diane Lisarelli
date: 2013-04-05
href: http://www.lesinrocks.com/2013/04/05/livres/hacker-ouverts-deux-livres-racontent-soixantedix-ans-revolution-informatique-11378503
tags:
- Internet
- Partage du savoir
---

> Alors que la bible de la culture hacker de Steven Levy sort enfin en France, Amaelle Guiton explore les résistances numériques actuelles. Sept décennies de révolution informatique en deux livres.
