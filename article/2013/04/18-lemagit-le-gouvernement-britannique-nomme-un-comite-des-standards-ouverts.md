---
site: LeMagIT
title: "Le gouvernement britannique nomme un comité des standards ouverts"
author: Cyrille Chausson
date: 2013-04-18
href: http://www.lemagit.fr/technologie/applications/open-source/2013/04/18/le-gouvernement-britannique-cree-un-comite-des-standards-ouverts
tags:
- Administration
- Interopérabilité
- Institutions
- RGI
- Standards
- International
- Open Data
---

> Après avoir énoncé sa préférence pour les logiciels Open Source, le gouvernement britannique a officiellement concrétisé la création de son «Open Standards Board», un comité des standards ouverts qui devra définir quels standards privilégier ou imposer dans les achats IT du gouvernement.
