---
site: Egalite et Réconciliation
title: "Sécurité nationale: un contrat entre Microsoft et le ministère de la Défense fait jaser"
date: 2013-04-21
href: http://www.egaliteetreconciliation.fr/Securite-nationale-un-contrat-entre-Microsoft-et-le-ministere-de-la-Defense-fait-jaser-17644.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- April
- Vente liée
- Marchés publics
- Europe
---

> En mai 2009, la Direction Interarmées des Réseaux d’Infrastructures et des Systèmes d’Information (DIRISI) et Microsoft signaient un accord cadre d’une durée de 4 ans portant sur le maintien en condition opérationnelle des systèmes informatiques du ministère de la Défense. Arrivant bien à terme, il est sur le point d’être reconduit pour une durée identique. Ce qui n’est pas sans susciter de polémiques.
