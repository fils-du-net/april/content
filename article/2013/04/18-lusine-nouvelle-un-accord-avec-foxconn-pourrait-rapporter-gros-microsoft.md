---
site: L'usine Nouvelle
title: "Un accord avec Foxconn pourrait rapporter gros à Microsoft"
author: Elodie Vallerey
date: 2013-04-18
href: http://www.usinenouvelle.com/article/un-accord-avec-foxconn-pourrait-rapporter-gros-a-microsoft.N195465
tags:
- Entreprise
- Brevets logiciels
---

> Le sous-traitant taïwanais Hon Hai, plus connu sous le nom de Foxconn, va verser des royalties à Microsoft pour l'utilisation de ses brevets pour la fabrication d'appareils fonctionnant avec les programmes de son concurrent Google.
