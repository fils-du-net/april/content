---
site: Le Cercle Les Echos
title: "M. Ayrault, ne tuez pas l’Open Source!"
author: Miguel Valdes Faura
date: 2013-04-02
href: http://lecercle.lesechos.fr/entreprises-marches/high-tech-medias/informatiques/221169098/m-ayrault-tuez-open-source
tags:
- Entreprise
- Administration
- Économie
- Institutions
---

> La directive Ayrault, publiée en septembre 2012, constitue une avancée majeure pour le logiciel libre dans les systèmes d’information de l’État, mais ne mettrait-elle pas en danger les acteurs du monde l'Open Source?
