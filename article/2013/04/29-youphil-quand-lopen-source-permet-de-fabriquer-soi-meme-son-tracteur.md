---
site: Youphil
title: "Quand l'open source permet de fabriquer soi-même son tracteur"
date: 2013-04-29
href: http://www.youphil.com/fr/article/06435-agriculture-open-source-internet-marcin-jakubowski
tags:
- Partage du savoir
- Matériel libre
- Innovation
- English
---

> Un agriculteur américain crée ses propres machines agricoles et partage ses plans librement sur internet.
