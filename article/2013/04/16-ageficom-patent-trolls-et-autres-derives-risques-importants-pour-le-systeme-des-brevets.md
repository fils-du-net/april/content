---
site: Agefi.com
title: "Patent Trolls et autres dérives: risques importants pour le système des brevets"
author: Michel Jaccard et Juliette Ancelle
date: 2013-04-16
href: http://www.agefi.com/une/detail/archive/2013/april/artikel/un-controle-plus-strict-des-demandes-deposees-et-une-meilleure-connaissance-des-solutions-existantes-permettent-de-reduire-les-effets-nefastes-de-certains-brevets-qui-nont-pas-lieu-detre.html
tags:
- Entreprise
- April
- Institutions
- Brevets logiciels
- Innovation
---

> Un contrôle plus strict des demandes déposées et une meilleure connaissance des solutions existantes permettent de réduire les effets néfastes de certains brevets qui n’ont pas lieu d’être.
