---
site: Nouvelle République
title: "Cllic 36: au nom des logiciels libres"
author: Christophe Gervais
date: 2013-04-03
href: http://www.lanouvellerepublique.fr/Indre/Communautes-NR/n/Contenus/Articles/2013/04/03/Cllic-36-au-nom-des-logiciels-libres-1395689
tags:
- Logiciels privateurs
- Sensibilisation
- Associations
---

> Les membres de l’association Cllic 36 militent “pour la fin du diktat” des sociétés informatiques. Ils proposent des solutions gratuites.
