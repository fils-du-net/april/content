---
site: Le Vinvinteur
title: "L’armée “accro” à Microsoft?"
date: 2013-04-20
href: http://levinvinteur.com/larmee-accro-a-microsoft
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Interopérabilité
- April
- Institutions
- Informatique-deloyale
- Marchés publics
---

> «L’armée capitule face à Microsoft»: confirmant une information du site PCInpact -qui avait levé le lièvre dès 2008-, le Canard Enchaîné a révélé mercredi dernier que l’armée française était sur le point de reconduire un contrat, sans appel d’offres, avec Microsoft. Problèmes: il «coûte cher, augmente les risques d’espionnage et se négocie… dans un paradis fiscal».
