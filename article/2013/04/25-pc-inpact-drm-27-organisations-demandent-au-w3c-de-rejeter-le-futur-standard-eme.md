---
site: PC INpact
title: "DRM: 27 organisations demandent au W3C de rejeter le futur standard EME"
author: Vincent Hermann
date: 2013-04-25
href: http://www.pcinpact.com/news/79364-drm-27-organisations-demandent-au-w3c-rejeter-futur-standard-eme.htm
tags:
- Entreprise
- Internet
- April
- Associations
- DRM
- Standards
---

> Il y a un peu plus de deux mois, le W3C publiait un premier brouillon pour un standard particulier. Les Encrypted Media Extensions ont pour objectif de permettre aux entreprises d’utiliser des solutions DRM si elles en ont besoin. Un ajout nettement décrié par une coalition de 27 organisations qui fustigent l’intrusion des DRM dans un standard (le HTML5) dont la mission est avant tout de fournir le même web à tous les internautes.
