---
site: ZDNet
title: "Affaire DCRI-Wikipédia: le ministère de l'Intérieur dément toute menace"
date: 2013-04-08
href: http://www.zdnet.fr/actualites/affaire-dcri-wikipedia-le-ministere-de-l-interieur-dement-toute-menace-39789107.htm
tags:
- Internet
- Partage du savoir
- Institutions
- Associations
---

> Face au scandale suscité par les manoeuvres de la Direction centrale du renseignement intérieur contre un bénévole de Wikipédia, le ministère de l'Intérieur réfute toute accusation de menaces.
