---
site: ITespresso
title: "DRM: vers un veto du W3C sur la spécification EME?"
author: La Rédaction
date: 2013-04-29
href: http://www.itespresso.fr/drm-veto-w3c-specification-eme-64280.html
tags:
- Entreprise
- Internet
- April
- DRM
- Standards
---

> Vingt-sept organisations en appellent au W3C pour faire barrage à l’introduction, au sein du HTML5, d’extensions liées à la gestion des droits numériques.
