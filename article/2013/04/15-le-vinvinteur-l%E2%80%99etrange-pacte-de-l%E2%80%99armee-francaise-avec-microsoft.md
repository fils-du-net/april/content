---
site: Le Vinvinteur
title: "L’étrange pacte de l’armée française avec Microsoft"
date: 2013-04-15
href: http://levinvinteur.com/letrange-pacte-de-larmee-francaise-avec-microsoft
tags:
- Entreprise
- Logiciels privateurs
- Administration
- April
- Institutions
- Marchés publics
- Standards
- International
---

> Cette semaine, le Vinvinteur enquête sur les liens légèrement malsains entre l’armée française et le géant du logiciel Microsoft. Nous avons même un document estampillé “diffusion restreinte”  à sortir… Explications.
