---
site: Programmez.com
title: "Solutions Linux, Libre et Open Source 2013: que retenir?"
author: François Tonic
date: 2013-05-31
href: http://www.programmez.com/actualites.php?id_actu=13464
tags:
- Entreprise
- Innovation
- Informatique en nuage
---

> Les 27 et 28 mai derniers, c’est déroulé le salon Linux, Libre et Open Source, traditionnel rendez-vous du monde ouvert à Paris depuis 16 ans. Cette année, le salon s’est déroulé sur 2 jours, au lieu de 3 et avec un nouveau nom. Ce nouveau format se veut plus dynamique, notamment pour les conférences.
