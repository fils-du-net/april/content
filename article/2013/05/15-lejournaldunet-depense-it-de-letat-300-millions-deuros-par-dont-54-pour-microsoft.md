---
site: LeJournalduNet
title: "Dépense IT de l'Etat: 300 millions d'euros par an, dont 54 pour Microsoft"
author: Virgile Juhan
date: 2013-05-15
href: http://www.journaldunet.com/solutions/dsi/depense-de-l-etat-en-logiciel-microsoft-0513.shtml
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
---

> Ces cinq dernières années, l'Etat a dépensé au moins 1,5 milliard d'euros dans l'acquisition et la maintenance de logiciels propriétaires. Sur cette somme, chaque année, plus de 54 millions vont chez Microsoft.
