---
site: LeMagIT
title: "Colin de la Higuera, président du SIF: Logiciels libres et cursus informatique au lycée et au collège, pas sans former les enseignants"
author: Anne-Marie Rouzeré
date: 2013-05-31
href: http://www.lemagit.fr/economie/carriere/2013/05/31/logiciels-libres-et-cursus-informatique-au-lycee-et-au-college-pas-sans-former-les-enseignants
tags:
- Internet
- Administration
- Institutions
- Éducation
---

> Pour les enseignants-chercheurs en informatique, ce n’est pas la priorité accordée aux logiciels libres par un projet de loi qui devrait faire débat, mais l’absence de moyens pour former leurs collègues des lycées et collèges. Faute – notamment – de reconnaître l’informatique comme une discipline à part entière. Sur ce point, «il est urgent de ne plus attendre», considère un rapport récent de l’Académie des sciences. Commentaires de Colin de la Higuera, président de la Société informatique de France.
