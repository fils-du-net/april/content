---
site: PC INpact
title: "DRM: le curieux oubli du projet de loi Hamon sur la consommation"
author: Marc Rees
date: 2013-05-07
href: http://www.pcinpact.com/news/79610-drm-curieux-oubli-projet-loi-hamon-sur-consommation.htm
tags:
- Interopérabilité
- April
- Institutions
- DRM
- Europe
---

> Le projet de loi sur la consommation présenté par Benoit Hamon vient transposer la directive sur les droits des consommateurs du 25 octobre 2011. Une directive qui veut notamment renforcer l’information du consommateur en augmentant le nombre des mentions obligatoires préalables à la conclusion d’un contrat de vente. Mais si dans la directive, les verrous numériques font partie du lot, le projet français oublie curieusement leur sort.
