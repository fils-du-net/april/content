---
site: Numerama
title: "HTML5: l'EFF se dresse contre les DRM en déposant une objection formelle"
author: Julien L.
date: 2013-05-30
href: http://www.numerama.com/magazine/26089-html5-l-eff-se-dresse-contre-les-drm-en-deposant-une-objection-formelle.html
tags:
- Entreprise
- Internet
- Associations
- DRM
- Informatique-deloyale
- Standards
---

> La bataille du web ouvert se joue aussi au niveau du W3C. Alors qu'est examinée la proposition d'intégrer des extensions pour médias chiffrés dans le HTML5, l'EFF a annoncé le dépôt d'une objection formelle contre cette perspective. L'ONG s'efforce de repousser l'introduction des verrous numériques (DRM) dans le HTML5, qui nuirait au web ouvert, à l'innovation et aux usagers.
