---
site: ZDNet
title: "Priorité au logiciel libre à l’école: un projet de loi très débattu"
author: Thierry Noisette
date: 2013-05-27
href: http://www.zdnet.fr/actualites/priorite-au-logiciel-libre-a-l-ecole-un-projet-de-loi-tres-debattu-39790743.htm
tags:
- Entreprise
- Administration
- April
- Institutions
- Éducation
- Marchés publics
---

> Le projet de loi sur l’école, qui vient d’être adopté au Sénat, donne la priorité aux logiciels libres et aux formats ouverts. Ce qui déchaîne la colère du Syntec et de l’Afdel, et les applaudissements des organisations libristes.
