---
site: Developpez.com
title: "Le code Linux «référence de qualité» selon Coverity"
author: Stéphane le calme
date: 2013-05-10
href: http://www.developpez.com/actu/55204/Le-code-Linux-reference-de-qualite-selon-Coverity-la-qualite-du-code-open-source-presque-equivalente-a-celle-du-code-proprietaire
tags:
- Entreprise
- Logiciels privateurs
---

> La qualité du code open source presque équivalente à celle du code propriétaire
