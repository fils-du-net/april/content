---
site: Les Echos
title: "Le biais de l’internet-centrisme"
author: Hubert Guillaud
date: 2013-05-25
href: http://blogs.lesechos.fr/internetactu-net/le-biais-de-l-internet-centrisme-a12986.html
tags:
- Internet
- Innovation
- Sciences
---

> De 2005 à 2007, le chercheur et éditorialiste, spécialiste de politique étrangère d’origine Bielarusse, Evgeny Morozov (@evgenymorozov), pensait que la technologie numérique était peut-être un moyen pour nous débarrasser des régimes autocratiques. Sa déception a été racontée dans un livre, The Net Delusion, où il s’en prenait à l’utopie du projet internet. Dans son nouveau livre Pour tout sauver, cliquez-là, le chercheur iconoclaste élargit sa critique pour comprendre les schémas de pensée à l’oeuvre derrière la révolution numérique.
