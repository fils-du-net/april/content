---
site: LeMagIT
title: "L’administration Obama structure officiellement sa politique publique liée à l’Open Data"
author: Cyrille Chausson
date: 2013-05-14
href: http://www.lemagit.fr/technologie/gestion-des-donnees/2013/05/14/ladministration-obama-structure-officiellement-sa-politique-publique-liee-a-lopen-data
tags:
- Internet
- Administration
- Interopérabilité
- Institutions
- Licenses
- International
- Open Data
---

> Décret présidentiel, cadre d’implémentation, rénovation de Data.gov, outils Open Source… le gouvernement américain accélère sa politique Open Data. Barack Obama pose officiellement les grands principes concrets de sa politique.
