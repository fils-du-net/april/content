---
site: AFJV
title: "L'influence du \"Logiciel Libre\" dans l'histoire du jeu vidéo"
author: Damien Djaouti
date: 2013-05-13
href: http://www.afjv.com/news/2468_l-influence-du-logiciel-libre-dans-l-histoire-du-jeu-video.htm
tags:
- Économie
- Sensibilisation
- Associations
- Licenses
---

> Etant donné sa richesse, l'histoire du jeu vidéo est difficile à appréhender comme un "tout", et est généralement abordée selon un angle précis. Qu'il s'agisse d'étudier l'évolution d'un genre de jeu, l'historique d'une société ou encore la vie d'une console, ce sont finalement ces nombreuses "petites histoires" qui font la grande. Aujourd'hui, nous vous proposons de nous intéresser à une de ces "petites histoires", qui est relativement méconnue: les influences de la culture du "logiciel Libre" au sein de l'histoire du jeu vidéo.
