---
site: clubic.com
title: "Mission Lescure: après les propositions, les réactions"
author: Olivier Robillart
date: 2013-05-13
href: http://pro.clubic.com/legislation-loi-internet/hadopi/actualite-558700-reactions-hadopi-lescure.html
tags:
- Internet
- Interopérabilité
- April
- HADOPI
- Institutions
- DRM
- Droit d'auteur
- Standards
---

> La mission Lescure vient, parmi ses 80 mesures, de proposer la fin de la Hadopi en tant qu'autorité administrative. Principale intéressée, la haute autorité dit se réjouir du maintien du dispositif de réponse graduée.
