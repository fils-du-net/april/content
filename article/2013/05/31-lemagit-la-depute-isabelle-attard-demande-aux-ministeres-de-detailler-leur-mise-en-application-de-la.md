---
site: LeMagIT
title: "La député Isabelle Attard demande aux ministères de détailler leur mise en application de la circulaire Ayrault"
author: Cyrille Chausson
date: 2013-05-31
href: http://www.lemagit.fr/divers/2013/05/31/la-depute-isabelle-attard-demande-aux-ministeres-de-detailler-leur-mise-en-application-de-la-circulaire-ayrault
tags:
- Logiciels privateurs
- Administration
- Institutions
---

> Le 28 mai dernier, la député écologiste Isabelle Attard a interpellé les 37 ministres du gouvernement sur l’état d’avancement de la mise en application de la circulaire Ayrault dans chacun des ministères.
