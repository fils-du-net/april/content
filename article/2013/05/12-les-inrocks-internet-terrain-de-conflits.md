---
site: les inrocks
title: "Internet: terrain de conflits"
author: Vincent Ostria
date: 2013-05-12
href: http://www.lesinrocks.com/2013/05/12/actualite/les-guerres-de-la-toile-11394233
tags:
- Internet
- Institutions
- Innovation
---

> Un documentaire explique comment internet a toujours été le terrain de conflits entre la réalité de la culture web et les pouvoirs publics.
