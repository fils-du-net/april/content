---
site: LeMagIT
title: "Le Sénat fait du libre une priorité dans l’éducatif numérique. Syntec et l’Afdel s’insurgent"
author: Cyrille Chausson
date: 2013-05-24
href: http://www.lemagit.fr/technologie/applications/open-source/2013/05/24/le-senat-fait-du-libre-une-priorite-dans-leducatif-numerique-syntec-et-lafdel-sinsurgent
tags:
- Entreprise
- Administration
- Interopérabilité
- April
- Institutions
- Éducation
- Marchés publics
- RGI
- Standards
---

> Alors le projet de loi portant sur la refondation de l’école de la République est actuellement en première lecture au Sénat, la commission de la culture, de l’éducation et de la communication a décidé de modifier le texte original en y ajoutant une disposition faisant du libre et des formats standards une priorité dans l’e-éducation. Syntec Numérique et l’Afdel crient à la discrimination.
