---
site: l'Humanité.fr
title: "Rapport Lescure: quelques bonnes intentions mais davantage de déceptions"
author: Pi. M.
date: 2013-05-13
href: http://www.humanite.fr/culture/rapport-lescure-quelques-bonnes-intentions-mais-da-541392
tags:
- Entreprise
- Internet
- Interopérabilité
- HADOPI
- Institutions
- Associations
- DRM
- Droit d'auteur
---

> Après neuf mois de consultations le rapport Lescure propose plusieurs dizaines de propositions. «Rien de révolutionnaire» avait prévenu son auteur. Le rapport commence en effet sur de belles intentions avant de céder définitivement à la pression des industriels et ne proposer qu’une Hadopi rhabillée aux couleurs d’un CSA tout puissant.
