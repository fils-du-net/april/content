---
site: La Vie éco
title: "Logiciel libre: le grand oublié du Plan Maroc Numeric 2013"
author: Zakaria Lahrach
date: 2013-05-29
href: http://www.lavieeco.com/news/economie/logiciel-libre-le-grand-oublie-du-plan-maroc-numeric-2013-25603.html
tags:
- Entreprise
- Économie
- Institutions
- Sensibilisation
- International
---

> Moindre coût, diversité des solutions et levier de discussion avec les éditeurs sont autant d'avantages dont devrait profiter l'administration marocaine. Le logiciel libre donne à l'utilisateur une grande liberté d'utilisation, de modification et de diffusion.
