---
site: Silicon.fr
title: "Faut-il donner la priorité aux logiciels libres à l'école?"
author: Ariane Beky
date: 2013-05-27
href: http://www.silicon.fr/logiciels-libres-projet-loi-ecole-86488.html
tags:
- Entreprise
- Administration
- Interopérabilité
- Institutions
- Associations
- Éducation
---

> Adopté en première lecture par le Sénat dans le cadre du projet de loi sur la refondation de l’école, la disposition relative à l’utilisation prioritaire des logiciels libres divise l’industrie.
