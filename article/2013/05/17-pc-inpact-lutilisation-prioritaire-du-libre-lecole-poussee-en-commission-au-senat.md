---
site: PC INpact
title: "L'utilisation prioritaire du libre à l'école poussée en commission au Sénat"
author: Xavier Berne
date: 2013-05-17
href: http://www.pcinpact.com/news/79791-lutilisation-prioritaire-libre-a-ecole-poussee-en-commission-au-senat.htm
tags:
- Internet
- Administration
- Institutions
- Sensibilisation
- Droit d'auteur
- Éducation
- Promotion
---

> Après avoir été adopté fin mars par l’Assemblée nationale, le projet de loi pour la refondation de l’école de la République est arrivé sur les bancs du Sénat. La Commission de la culture, de l'éducation et de la communication vient d’ailleurs de publier un rapport concernant le texte de Vincent Peillon, qui a trait à plusieurs reprises au numérique et à l’internet.
