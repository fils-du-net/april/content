---
site: LBR.ca
title: "Les logiciels libres et ceux sous licence"
author: Sylvain Gagnon
date: 2013-05-07
href: http://www.lbr.ca/index.php?pageID=10$urlidA=390
tags:
- Entreprise
- Logiciels privateurs
- Sensibilisation
- Licenses
- Philosophie GNU
---

> Nous vivons dans un monde où la consommation des technologies est de plus en plus contrôlée par les gros propriétaires informatiques que sont Apple, Adobe, Google, Microsoft, etc. La chronique d’aujourd’hui vise à informer les utilisateurs des autres alternatives qui s’offrent à eux avec le monde du logiciel libre.
