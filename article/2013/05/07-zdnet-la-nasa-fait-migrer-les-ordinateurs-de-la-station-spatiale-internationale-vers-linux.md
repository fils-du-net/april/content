---
site: ZDNet
title: "La Nasa fait migrer les ordinateurs de la Station spatiale internationale vers Linux"
date: 2013-05-07
href: http://www.zdnet.fr/actualites/la-nasa-fait-migrer-les-ordinateurs-de-la-station-spatiale-internationale-vers-linux-39790175.htm
tags:
- Administration
- Innovation
- International
---

> Les astronautes à bord de la SSI utiliseront des ordinateurs sous Debian, a décidé l’agence spatiale américaine, qui expérimente aussi un robot sous Linux pour des tâches fastidieuses ou dangereuses.
