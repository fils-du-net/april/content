---
site: LeMagIT
title: "Développement Open Source: les dépendances indirectes, un point noir dans la conformité"
author: Cyrille Chausson
date: 2013-05-06
href: http://www.lemagit.fr/technologie/applications/langages/2013/05/06/open-source-les-dependances-indirectes-un-point-noir-dans-la-conformite
tags:
- Entreprise
- Licenses
---

> Une étude de l’éditeur White Source indique que les projets de développements Open Source peinent à se mettre à conformité avec les trop nombreuses licences issues de dépendances associées.
