---
site: ZDNet
title: "Logiciel libre: des PME en croissance continue, représentées aux niveaux régional et national"
author: Thierry Noisette
date: 2013-05-29
href: http://www.zdnet.fr/actualites/logiciel-libre-des-pme-en-croissance-continue-representees-aux-niveaux-regional-et-national-39790787.htm
tags:
- Entreprise
- Économie
- Institutions
- Associations
---

> Les SSLL connaissent depuis plusieurs années une sensible croissance de leur CA et de leurs effectifs. Elles se réunissent au sein d'associations régionales, comme Alliance libre dans l'ouest, elles-mêmes membres du Conseil national du logiciel libre.
