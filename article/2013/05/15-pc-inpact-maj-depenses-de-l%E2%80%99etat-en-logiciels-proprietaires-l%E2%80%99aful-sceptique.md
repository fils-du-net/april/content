---
site: PC INpact
title: "Dépenses de l’État en logiciels propriétaires: l’AFUL sceptique"
author: Xavier Berne
date: 2013-05-15
href: http://www.pcinpact.com/news/79719-l-etat-a-depense-15-milliard-en-logiciels-sur-cinq-dernieres-annees.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- April
- Institutions
- Associations
---

> 300 millions d’euros par an. C’est à peu près ce qu’a dépensé en moyenne la France au cours des cinq dernières années pour l’acquisition et la maintenance de logiciels propriétaires selon le ministère du Budget. Sur l’ensemble de cette enveloppe annuelle, 53,9 millions d’euros sont d'ailleurs allés à Microsoft en 2011.
