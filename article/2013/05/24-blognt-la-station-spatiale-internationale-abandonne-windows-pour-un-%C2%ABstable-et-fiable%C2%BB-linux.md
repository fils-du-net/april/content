---
site: BlogNT
title: "La Station spatiale internationale abandonne Windows pour un «stable et fiable» Linux"
author: Yohann Poiron
date: 2013-05-24
href: http://www.blog-nouvelles-technologies.fr/archives/29091/la-station-spatiale-internationale-abandonne-windows-pour-un-stable-et-fiable-linux
tags:
- Entreprise
- Innovation
---

> La United Space Alliance, qui gère tous les ordinateurs de la Station spatiale internationale (ISS), en association avec la NASA, ont pris la décision que tous les dispositifs embarqués disposant de Windows XP migrent vers Debian 6, «parce que nous avions besoin d’un système d’exploitation qui [est] stable et fiable», a déclaré dans un communiqué de presse Keith Chuvala de la United Space Alliance, de la Fondation Linux.
