---
site: Le Cercle Les Echos
title: "L’innovation ouverte est-elle vraiment ouverte?"
author: Pénin Julien
date: 2013-05-30
href: http://lecercle.lesechos.fr/economie-societe/recherche-innovation/innovation/221173431/innovation-ouverte-est-elle-vraiment-ouve
tags:
- Entreprise
- Économie
- Partage du savoir
- Innovation
- Sciences
---

> Avec l’essor de l’informatique et la complexité croissante du processus innovant, l’innovation ouverte s’est imposée comme modèle stratégique. Cette expression désigne la dimension collaborative de certains projets innovants, mais elle n’implique pas nécessairement un affaiblissement du contrôle des entreprises sur leurs innovations comme c’est le cas dans l’open source.
