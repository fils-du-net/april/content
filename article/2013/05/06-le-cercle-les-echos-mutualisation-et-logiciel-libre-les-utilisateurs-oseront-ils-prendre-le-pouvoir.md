---
site: Le Cercle Les Echos
title: "Mutualisation et logiciel libre, les utilisateurs oseront-ils prendre le pouvoir?"
author: Philippe Scoffoni
date: 2013-05-06
href: http://lecercle.lesechos.fr/entreprises-marches/high-tech-medias/informatiques/221171970/mutualisation-et-logiciel-libre-utilisa
tags:
- Entreprise
- Administration
- Économie
- Associations
---

> Et si les utilisateurs reprenaient le contrôle de leurs logiciels, quel impact cela aurait-il sur les acteurs professionnels que sont les éditeurs et les prestataires de services? Quel serait le bilan au final?
