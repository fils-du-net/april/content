---
site: PC INpact
title: "Mise en demeure de LinuxFr: un responsable de Linkeo s’explique"
author: Marc Rees
date: 2013-05-31
href: http://www.pcinpact.com/news/80152-mise-en-demeure-linuxfr-responsable-linkeo-s-explique.htm
tags:
- Entreprise
- Internet
- Associations
---

> Linkeo, société de création et d’hébergement de site, n’a visiblement pas apprécié les critiques un peu trop acidulées. Elle avait posté une annonce d’emploi sur LinuxFr. Annonce à laquelle un contributeur a apporté un commentaire fleuri. Elle réclame la suppression de ces propos ainsi que 1500 euros à LinuxFr, au prétexte que le site serait dénué de mentions légales. Le responsable marketing s’en explique.
