---
site: iTech Post
title: "Why Is The International Space Station Switching From Windows To GNU/Linux?"
date: 2013-05-12
href: http://www.itechpost.com/articles/9064/20130512/why-international-space-station-switching-windows-gnu-linux.htm
tags:
- Logiciels privateurs
- Administration
- Innovation
- International
- English
---

> (Les ordinateurs sur la station spatiale internationale sont en train de psser à Windows XP à GNU/Linux) The computers aboard the International Space Station are being switched from Windows XP to GNU/Linux, according to an announcement from the United Space Alliance, the organization that manages the computers in association with NASA.
