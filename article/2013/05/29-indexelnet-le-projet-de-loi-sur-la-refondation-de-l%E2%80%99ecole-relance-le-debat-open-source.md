---
site: indexel.net
title: "Le projet de loi sur la refondation de l’école relance le débat open source"
author: Marie Varandat
date: 2013-05-29
href: http://www.indexel.net/actualites/le-projet-de-loi-sur-la-refondation-de-l-ecole-relance-le-debat-open-source-3832.html
tags:
- Entreprise
- Logiciels privateurs
- April
- Institutions
- Éducation
- Marchés publics
---

> En donnant la priorité aux logiciels libres, le projet de loi sur la refondation de l'école de la République a provoqué une nouvelle vague d’hostilités entre le camp open source et celui des logiciels dits propriétaires.
