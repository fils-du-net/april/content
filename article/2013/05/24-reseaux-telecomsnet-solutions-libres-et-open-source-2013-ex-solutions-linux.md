---
site: "Reseaux-Telecoms.net"
title: "Solutions Libres et Open-Source 2013 (ex-Solutions Linux)"
date: 2013-05-24
href: http://www.reseaux-telecoms.net/agenda/lire-solutions-libres-et-open-source-2013-ex-solutions-linux-6399.html
tags:
- Entreprise
- Administration
---

> La 15ème édition du salon Solutions Linux fait peau neuve. Renommé «Solutions Linux, Libres &amp; Open Source», l'évènement s'inscrira cette année dans la droite ligne fédératrice voulue par la circulaire Ayrault et la récente volonté du gouvernement à encourager un cadre propice à la structuration de la filière. Avec près de 6 000 visiteurs, la dynamique du salon fait figure de référence pour rassembler l'écosystème et les acteurs des secteurs publics, privés et associatifs.
