---
site: Le Point
title: "Documentaire: l'immanquable contre-histoire du Net"
author: Guerric Poncet
date: 2013-05-14
href: http://www.lepoint.fr/chroniqueurs-du-point/guerric-poncet/documentaire-l-immanquable-contre-histoire-du-net-14-05-2013-1666270_506.php
tags:
- Internet
- Institutions
- Innovation
- Sciences
---

> Arte diffuse mardi soir un documentaire qui démonte les croyances populaires sur la création d'Internet. Vous croyiez que c'était l'armée américaine?
