---
site: PC INpact
title: "Projet de loi sur l'école: l'usage du libre devient une priorité"
author: Marc Rees
date: 2013-05-27
href: http://www.pcinpact.com/news/79993-projet-loi-sur-ecole-senateurs-font-usage-libre-priorite.htm
tags:
- Entreprise
- April
- Institutions
- Associations
- Droit d'auteur
- Éducation
- Marchés publics
---

> Après examen du projet de loi d’orientation et de programmation pour la refondation de l’école de la République, les sénateurs ont voté un coup de pouce appuyé aux logiciels libres. Le service public de l’enseignement numérique et de l’enseignement à distance devra en effet utiliser «en priorité des logiciels libres et des formats ouverts de documents». Une première dans l'histoire parlementaire applaudie par les promoteurs du libre. Le texte doit encore être validé par les députés.
