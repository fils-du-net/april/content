---
site: LeMagIT
title: "Une étude d'Opsview relativise le poids de l’Open Source dans le Cloud"
date: 2013-05-14
href: http://www.lemagit.fr/technologie/datacenter-technologie/cloud-grid-computing/2013/05/14/une-etude-dopsview-relativise-le-poids-de-lopen-source-dans-le-cloud
tags:
- Entreprise
- Informatique en nuage
---

> Les technologies d’infrastructures Cloud Open source ne seraient pas aussi recherchées qu’on le dit par les personnes en charge de la sélection et de l’implémentation des projets Cloud au sein de leur entreprise.
