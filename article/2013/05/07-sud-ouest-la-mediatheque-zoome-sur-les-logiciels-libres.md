---
site: Sud Ouest
title: "La médiathèque zoome sur les logiciels libres"
author: Éric Coëne 
date: 2013-05-07
href: http://www.sudouest.fr/2013/05/07/la-mediatheque-zoome-sur-les-logiciels-libres-1046253-3166.php
tags:
- Logiciels privateurs
- Administration
- Sensibilisation
- Licenses
---

> La médiathèque Jean-Cocteau propose actuellement une exposition pour tout savoir sur les logiciels libres. Dix panneaux font découvrir au public les principes, les enjeux, les usages et l’économie des logiciels libres.
