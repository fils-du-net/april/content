---
site: Framablog
title: "Si on arrêtait d'utiliser les licences libres? (au profit du domaine public)"
author: Glyn Moody (Traduction Framablog)
date: 2013-05-18
href: http://www.framablog.org/index.php/post/2013/05/18/licence-libre-domaine-public-moody
tags:
- Droit d'auteur
- Innovation
- Licenses
- Philosophie GNU
- Informatique en nuage
---

> Les logiciels libres reposent sur un paradoxe. Afin que les utilisateurs puissent être libres, les licences libres utilisent quelque chose remettant en cause la liberté: le copyright. Ce dernier est un monopole intellectuel se basant sur la restriction de la liberté des gens à partager, la liberté est donc restreinte et non pas étendue. Quand Richard Stallman, en 1985, a créé la GNU Emacs General Public License, cela représentait un bidouillage brillant, maintenant il est peut-être temps de passer à autre chose.
