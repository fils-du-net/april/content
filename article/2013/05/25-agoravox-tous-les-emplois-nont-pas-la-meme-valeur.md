---
site: AgoraVox
title: "Tous les emplois n'ont pas la même valeur"
author: matthius
date: 2013-05-25
href: http://www.agoravox.fr/actualites/economie/article/tous-les-emplois-n-ont-pas-la-meme-136352
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Marchés publics
---

> Il est très important de dire que c'est l'économie de travail et une monnaie égalitaire qui permettraient la suppression de l'esclavagisme. Créer des emplois inutiles en aidant les entreprises de logiciels propriétaires est une hérésie, car il y a trop de services et pas assez d'industries.
