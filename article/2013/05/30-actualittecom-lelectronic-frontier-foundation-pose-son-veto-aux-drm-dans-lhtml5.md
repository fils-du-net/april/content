---
site: ActuaLitté.com
title: "L'Electronic Frontier Foundation pose son véto aux DRM dans l'HTML5"
author: Antoine Oury
date: 2013-05-30
href: http://www.actualitte.com/legislation/l-electronic-frontier-foundation-pose-son-veto-aux-drm-dans-l-html5-42709.htm
tags:
- Entreprise
- Internet
- Interopérabilité
- DRM
- Standards
---

> Pas de verrous à l'information
