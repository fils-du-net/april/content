---
site: AgoraVox
title: "Who owns the Future, par Jaron Lanier"
author: Jean-Paul Baquiast
date: 2013-05-24
href: http://www.agoravox.fr/culture-loisirs/culture/article/who-owns-the-future-par-jaron-136340
tags:
- Internet
- Partage du savoir
- Innovation
---

> Il s'était fait connaître dès les années 2005 par sa dénonciation de Wikipedia et plus généralement des logiciels libres, dans lesquels il voit la encore, sous le mythe de la gratuité, l'emprise de quelques «connaisseurs» favorisés n'hésitant pas à priver les professionnels de leurs compétences anciennes.
