---
site: ZDNet
title: "No piracy watchdog, a tablet tax and free software: France's vision of culture in the digital age"
author: Valéry Marchive
date: 2013-05-15
href: http://www.zdnet.com/no-piracy-watchdog-a-tablet-tax-and-free-software-frances-vision-of-culture-in-the-digital-age-7000015364
tags:
- Internet
- April
- HADOPI
- Institutions
- Associations
- DRM
- English
---

> (Une étude commissionnée l'été dernier pour découvrir comment protéger l'exception culturelle Française dans l'ère en-ligne a rendu son verdict - et cela génère déjà une bonne dose de critiques) A study commissioned last summer to find how to protect France's "exception culturelle" in the online era has delivered its verdict - and it's generating its fair share of criticism already.
