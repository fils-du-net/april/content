---
site: 01netPro.
title: "Un décret rend obligatoire l'open data au sein de l'Administration américaine"
author: Vincent Berdot
date: 2013-05-13
href: http://pro.01net.com/editorial/594897/un-decret-rend-obligatoire-l-open-data-au-sein-de-l-administration-americaine
tags:
- Internet
- Administration
- Institutions
- International
- Open Data
---

> Pour une fois, les Français montrent l’exemple dans le numérique. Si le concept de l’open data est bien américain, la France avait, dès mai 2011, publié un décret enjoignant les ministères et l’Administration centrale à libérer leurs données de fonctionnement. Deux ans plus tard, c’est au tour de l’Administration Obama de signer un tel décret.
