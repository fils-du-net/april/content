---
site: ZDNet
title: "Éducation nationale: la priorité au logiciel libre confirmée en commission à l'Assemblée nationale"
author: Thierry Noisette
date: 2013-05-30
href: http://www.zdnet.fr/actualites/education-nationale-la-priorite-au-logiciel-libre-confirmee-en-commission-a-l-assemblee-nationale-39790866.htm
tags:
- Administration
- April
- Institutions
- Éducation
---

> Adoptée par la commission des affaires culturelles, la priorité aux logiciels libres et aux formats ouverts arrivera en séance publique la semaine prochaine.
