---
site: infoDSI
title: "La qualité des logiciels open source est au moins équivalente à celle des logiciels propriétaires"
date: 2013-05-14
href: http://www.infodsi.com/articles/140717/qualite-logiciels-open-source-est-moins-equivalente-logiciels-proprietaires.html
tags:
- Logiciels privateurs
---

> La qualité des logiciels open source et propriétaires est meilleure que la moyenne de l’industrie logicielle. Telle est la principale conclusion du 5ème rapport annuel Coverity qui détaille l’analyse de plus de 450 millions de lignes de code open source et propriétaire. Ce qui représente le plus grand échantillon jamais étudié par le rapport. Ce service a été lancé en 2006 par Coverity et le U.S. Department of Homeland Security. Il s’agissait alors du plus grand projet d’étude privé-public sur l’intégrité des logiciels open source. Il est maintenant géré par Coverity.
