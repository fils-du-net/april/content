---
site: écrans
title: "Rapport Lescure: pour, contre, pour, contre, contre"
author: Camille Gévaudan, 
date: 2013-05-13
href: http://www.ecrans.fr/Rapport-Lescure-pour-contre-pour,16357.html
tags:
- Internet
- Économie
- Interopérabilité
- April
- HADOPI
- Institutions
- Associations
- DADVSI
- DRM
- Licenses
- Standards
---

> Quelques minutes après la présentation du rapport ce matin, les réactions ont commencé à pleuvoir. Et on est aussi allé en chercher d’autres nous-même. Compil.
