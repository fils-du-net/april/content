---
site: Localtis.info
title: "Projet de loi Peillon: la commission du Sénat modifie le texte de l'Assemblée... et la carte scolaire"
author: Valérie Liquet
date: 2013-05-16
href: http://www.localtis.info/cs/ContentServer?pagename=Localtis/LOCActu/ArticleActualite$urlcid=1250265212646
tags:
- Institutions
- Éducation
- Marchés publics
---

> Le projet de loi pour la refondation de l'école, qui sera discuté au Sénat à partir du 21 mai, a été largement amendé en commission. Pour les modifications touchant de près les collectivités locales, on retiendra principalement celles concernant la carte scolaire. Mais une dizaine d'autres étaient également au menu.
