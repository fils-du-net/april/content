---
site: Developpez.com
title: "\"Encrypted Media Extensions\" passe au statut d'ébauche publique"
author: Hinault Romaric
date: 2013-05-10
href: http://www.developpez.com/actu/55217/-Encrypted-Media-Extensions-passe-au-statut-d-ebauche-publique-le-W3C-determine-a-supporter-les-DRM-dans-HTML5-la-FSF-consternee
tags:
- Entreprise
- Internet
- Associations
- DRM
- Standards
- Video
---

> Le W3C déterminé à supporter les DRM dans HTML5, la FSF consternée
