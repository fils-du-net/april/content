---
site: Joinup
title: "French Senate recommends the use of free software for schools services"
author: Gijs Hillenius
date: 2013-05-27
href: http://joinup.ec.europa.eu/community/osor/news/french-senate-recommends-use-free-software-schools-services
tags:
- Entreprise
- April
- Institutions
- Associations
- Éducation
- Standards
- English
---

> (Le Sénat Français veut que ses écoles primaires et secondaires utilisent des solutions logiciel libre pour ses services publics et l'éducation à distance) France's Senate wants its primary and secondary schools to use free software solutions for its public services and distance education. The Parliament's upper house is about to accept a policy recommending that "this public service primarily uses free software and open document formats".
