---
site: Developpez.com
title: "France: l'usage du libre pourrait devenir une priorité dans l'enseignement à distance, le Sénat le préconise après examen d'un projet de loi"
author: Stéphane le calme
date: 2013-05-27
href: http://www.developpez.com/actu/56009/France-l-usage-du-libre-pourrait-devenir-une-priorite-dans-l-enseignement-a-distance-le-Senat-le-preconise-apres-examen-d-un-projet-de-loi
tags:
- Entreprise
- April
- Institutions
- Éducation
- Marchés publics
- Informatique en nuage
- Video
---

> Le 25 mai dernier, le Sénat s'est prononcé en faveur du projet de loi d'orientation et de programmation pour la refondation de l'école de la République à 176 voix contre 171.
