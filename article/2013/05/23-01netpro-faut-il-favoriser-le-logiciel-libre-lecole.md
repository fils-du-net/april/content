---
site: 01netPro.
title: "Faut-il favoriser le logiciel libre à l'école?"
author: Xavier Biseul
date: 2013-05-23
href: http://pro.01net.com/editorial/595799/pour-l-industrie-du-numerique-pas-question-de-favoriser-le-logiciel-libre-a-l-ecole
tags:
- Entreprise
- Administration
- April
- Institutions
- Éducation
- Marchés publics
---

> Des amendements apportés à deux projets de lois tentent de privilégier le recours aux logiciels libres au détriment des solutions du marché. Favoritisme! s’écrient l’Afdel et Syntec numérique. Bravo! saluent l'April et Linagora.
