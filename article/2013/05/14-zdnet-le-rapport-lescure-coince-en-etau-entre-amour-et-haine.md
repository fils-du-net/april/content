---
site: ZDNet
title: "Le rapport Lescure, coincé en étau entre amour et haine"
author: Clara Leonard
date: 2013-05-14
href: http://www.zdnet.fr/actualites/le-rapport-lescure-coince-en-etau-entre-amour-et-haine-39790267.htm
tags:
- Entreprise
- Internet
- April
- HADOPI
- Institutions
- Associations
- DRM
---

> Si le gouvernement a promis une mise en application rapide, certains dénoncent un rapport entre deux eaux: trop radical pour certains et trop mou pour d’autres.
