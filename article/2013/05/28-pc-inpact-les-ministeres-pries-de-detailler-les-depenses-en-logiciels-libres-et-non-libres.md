---
site: PC INpact
title: "Les ministères priés de détailler les dépenses en logiciels libres et non-libres"
author: Xavier Berne
date: 2013-05-28
href: http://www.pcinpact.com/news/80033-les-ministeres-pries-detailler-leurs-depenses-en-logiciels-libres-et-non-libres.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Associations
---

> La députée écologiste Isabelle Attard vient de transmettre une question écrite à chaque membre du gouvernement. L’objectif: connaître les suites qui ont été données à la circulaire Ayrault sur l’usage du libre dans l’administration. L’élue souhaite également que tous les ministres dévoilent «le montant des dépenses en logiciel, en distinguant les logiciels propriétaires des libres, au sein [de leur] ministère et des administrations qui en dépendent, pour chaque année de 2008 à 2012».
