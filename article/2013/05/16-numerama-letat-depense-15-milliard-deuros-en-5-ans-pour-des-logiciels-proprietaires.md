---
site: Numerama
title: "L'Etat a dépensé 1,5 milliard d'euros en 5 ans pour des logiciels propriétaires"
author: Julien L.
date: 2013-05-16
href: http://www.numerama.com/magazine/25971-l-etat-a-depense-15-milliard-d-euros-en-5-ans-pour-des-logiciels-proprietaires.html
tags:
- Logiciels privateurs
- Administration
- April
- Institutions
---

> Au cours des cinq dernières années, l'État a dépensé au moins 1,5 milliard d'euros dans des logiciels non libres. Il s'agit d'une estimation basse, car les données communiquées par le ministère du budget n'incluent pas certaines administrations publiques. Cela représente malgré tout une dépense moyenne de 300 millions d'euros par an.
