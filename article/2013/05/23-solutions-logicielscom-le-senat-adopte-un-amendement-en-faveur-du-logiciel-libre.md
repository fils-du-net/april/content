---
site: "Solutions-Logiciels.com"
title: "Le Sénat adopte un amendement en faveur du logiciel libre"
author: Frédéric Mazué
date: 2013-05-23
href: http://www.solutions-logiciels.com/actualites.php?actu=13417
tags:
- Entreprise
- Administration
- Institutions
- Éducation
---

> La Commission de la Culture, de l’Éducation et de la Communication du Sénat a adopté hier un amendement à l'article 10 du projet de loi de Refondation de l’École de la République prévoyant que le service public du numérique éducatif devra «utiliser en priorité des logiciels libres et des formats ouverts de documents».
