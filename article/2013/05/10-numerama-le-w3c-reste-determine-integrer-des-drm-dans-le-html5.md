---
site: Numerama
title: "Le W3C reste déterminé à intégrer des DRM dans le HTML5"
author: Julien L.
date: 2013-05-10
href: http://www.numerama.com/magazine/25921-le-w3c-reste-determine-a-integrer-des-drm-dans-le-html5.html
tags:
- Internet
- Associations
- DRM
- Standards
- Video
---

> Le W3C poursuit l'intégration des mesures techniques de protection (DRM) au sein du HTML 5. Baptisées extensions pour médias chiffrés, elles disposent désormais d'un nouveau statut au sein de l'organisme de normalisation. Une évolution qui a été condamnée par la FSF, qui s'oppose fermement à ce projet.
