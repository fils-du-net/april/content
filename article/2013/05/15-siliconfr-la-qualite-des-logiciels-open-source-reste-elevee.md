---
site: Silicon.fr
title: "La qualité des logiciels open source reste élevée"
author: Ariane Beky
date: 2013-05-15
href: http://www.silicon.fr/coverity-logiciels-open-source-proprietaires-86129.html
tags:
- Entreprise
- Institutions
---

> Pour la deuxième année consécutive, le code de logiciels open source et propriétaires analysé par Coverity affiche une densité de défauts inférieure à 1.
