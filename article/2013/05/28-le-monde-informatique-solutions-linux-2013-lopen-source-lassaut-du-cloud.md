---
site: Le Monde Informatique
title: "Solutions Linux 2013: l'Open Source à l'assaut du cloud"
author: Jacques Cheminat
date: 2013-05-28
href: http://www.lemondeinformatique.fr/actualites/lire-solutions-linux-2013-l-open-source-a-l-assaut-du-cloud-53746.html
tags:
- Entreprise
- Internet
- Innovation
---

> Le millésime 2013 de Solutions Linux a été fortement marqué par le développement de l'Open Source dans le cloud. OpenStack, CloudStack, Eucalyptus sont des termes souvent entendus dans les allées du salon.
