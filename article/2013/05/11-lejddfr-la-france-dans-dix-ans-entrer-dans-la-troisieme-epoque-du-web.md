---
site: leJDD.fr
title: "La France dans dix ans - \"Entrer dans la troisième époque du Web\""
author: Camille Neveux
date: 2013-05-11
href: http://www.lejdd.fr/Economie/Actualite/La-France-dans-dix-ans-Entrer-dans-la-troisieme-epoque-du-Web-606814
tags:
- Internet
- Économie
- Innovation
---

> INTERVIEW - Le philosophe Bernard Stiegler analyse pour le JDD le plan du gouvernement pour le numérique de demain.
