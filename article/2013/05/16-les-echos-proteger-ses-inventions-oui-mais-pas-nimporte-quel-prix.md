---
site: Les Echos
title: "Protéger ses inventions oui, mais pas à n'importe quel prix"
author: Joseph E. Stiglitz
date: 2013-05-16
href: http://business.lesechos.fr/directions-generales/strategie/0202765084417-proteger-ses-inventions-oui-mais-pas-a-n-importe-quel-prix-6783.php
tags:
- Entreprise
- Économie
- Partage du savoir
- Institutions
- Brevets logiciels
- Innovation
- Sciences
- International
---

> L'extension constante du champ de la propriété intellectuelle est souvent motivée par la seule recherche du profit. Avec un double risque: freiner l'innovation et interdire aux pays pauvres l'accès à la connaissance.
