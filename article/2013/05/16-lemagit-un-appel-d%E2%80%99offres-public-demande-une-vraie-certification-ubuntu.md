---
site: LeMagIT
title: "Un appel d’offres public demande une vraie certification Ubuntu"
author: Cyrille Chausson
date: 2013-05-16
href: http://www.lemagit.fr/technologie/applications/open-source/2013/05/16/un-appel-doffres-public-demande-une-vraie-certification-ubuntu
tags:
- Logiciels privateurs
- Administration
- Économie
- Sensibilisation
- Marchés publics
---

> Un appel d’offres portant sur la fourniture de matériels informatiques à des ministères demande clairement une certification des ordinateurs pour Ubuntu. Une première qui va au delà de la simple compatibilité. Cet accord-cadre interministériel demande également que les machines ne soient pas verrouillées sur un OS particulier.
