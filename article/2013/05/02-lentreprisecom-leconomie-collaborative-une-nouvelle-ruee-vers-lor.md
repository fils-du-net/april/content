---
site: L'Entreprise.com
title: "L'économie collaborative, une nouvelle ruée vers l'or"
author: Etienne Gless
date: 2013-05-02
href: http://lentreprise.lexpress.fr/developpement-et-innover/oui-share-premier-festival-de-l-economie-collaborative-les-2-et-3-mai-a-paris_40616.html
tags:
- Entreprise
- Économie
- Partage du savoir
- Matériel libre
- Innovation
---

> Je partage, tu consommes, nous produisons... Bienvenue dans un monde "open" où partage et contribution sont la règle. L'économie contributive mise sur la force des communautés pour produire, consommer et travailler autrement. Bienvenue dans un monde nouveau qui selon certains préfigure une nouvelle révolution industrielle!
