---
site: ZDNet
title: "Logiciels libres et open source en entreprise: la nécessité d'une politique précise"
author: Thierry Noisette
date: 2013-05-29
href: http://www.zdnet.fr/actualites/logiciels-libres-et-open-source-en-entreprise-la-necessite-d-une-politique-precise-39790827.htm
tags:
- Entreprise
- Associations
- Innovation
- Licenses
- Video
---

> Indispensable, en particulier pour les entreprises qui développent elles-mêmes du code, la politique d’usage des logiciels libres doit être cadrée, en particulier pour les licences, a rappelé une table ronde ce matin à Solutions Linux.
