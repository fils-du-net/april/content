---
site: Le nouvel Observateur
title: "\"Une contre-histoire de l'Internet\" sur Arte, le docu qui réhabilite enfin les hackers"
author: Jérémie Zimmermann
date: 2013-05-15
href: http://leplus.nouvelobs.com/contribution/861359-une-contre-histoire-de-l-internet-sur-arte-le-docu-qui-rehabilite-enfin-les-hackers.html
tags:
- Internet
- Partage du savoir
- Institutions
- Innovation
- ACTA
---

> Arte a diffusé mardi soir "Une contre-histoire de l'Internet", un documentaire relatant la construction et l'extension du réseau par ses acteurs principaux (à revoir ici). Jérémie Zimmermann, co-fondateur et porte-parole de la Quadrature du Net, y a participé. Il explique ici pourquoi une remise à plat était nécessaire.
