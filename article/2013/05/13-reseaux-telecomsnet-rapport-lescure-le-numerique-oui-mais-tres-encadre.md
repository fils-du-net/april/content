---
site: "Reseaux-Telecoms.net"
title: "Rapport Lescure: le numérique, oui, mais très encadré"
author: Bertrand Lemaire
date: 2013-05-13
href: http://www.reseaux-telecoms.net/actualites/lire-rapport-lescure-le-numerique-oui-mais-tres-encadre-26023.html
tags:
- Entreprise
- Internet
- Interopérabilité
- HADOPI
- Institutions
- Associations
- DRM
- Droit d'auteur
---

> Le Rapport de Pierre Lescure a été remis au Président de la république. Ses propositions visent certes au développement du numérique mais avec un encadrement renouvelé.
