---
site: PC INpact
title: "Open Bar Microsoft: une députée demande des comptes à la Défense"
author: Marc Rees
date: 2013-05-28
href: http://www.pcinpact.com/news/80047-open-bar-microsoft-deputee-demande-comptes-a-defense.htm
tags:
- Internet
- Logiciels privateurs
- Administration
- April
- Institutions
- Marchés publics
---

> L’offre Open Bar de Microsoft est ce contrat hors du commun signé entre l’éditeur américain et le ministère de la Défense. Une députée du groupe écologiste a cependant un peu de mal à comprendre comment un tel contrat sans appel d’offres a pu être passé malgré les avis défavorables à sa conclusion. Elle sollicite du coup quelques explications à l'exécutif.
