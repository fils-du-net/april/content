---
site: Ere numerique
title: "Les DRM ont la vie dure"
author: Léo de Urlevan
date: 2013-05-07
href: http://www.erenumerique.fr/les_drm_ont_la_vie_dure-article-5964-1.html
tags:
- Entreprise
- Économie
- Institutions
- DRM
- Europe
---

> Le changement, c’est maintenant. Oui, des choses changent mais pas toujours forcément dans le bon sens. Le gouvernement vient en effet de rendre public un avant-projet de loi sur la consommation. C’est un texte qui complète la directive sur les droits des consommateurs (2011).
