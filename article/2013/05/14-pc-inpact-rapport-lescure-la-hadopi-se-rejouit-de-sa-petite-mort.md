---
site: PC INpact
title: "Rapport Lescure: la Hadopi se réjouit de sa petite mort"
author: Nil Sanyas
date: 2013-05-14
href: http://www.pcinpact.com/news/79680-rapport-lescure-hadopi-se-rejouit-sa-petite-mort.htm
tags:
- Entreprise
- Internet
- Économie
- Interopérabilité
- April
- HADOPI
- Institutions
- Associations
- DADVSI
- DRM
---

> Suite à la publication officielle du rapport Lescure, les réactions ont été nombreuses. De l'Hadopi en passant par les artistes, les cinéastes, les opérateurs télécoms, les producteurs, les personnalités politiques et diverses associations, beaucoup ont eu leur mot à dire sur ces fameuses préconisations. Voici un condensé de ces réactions.
