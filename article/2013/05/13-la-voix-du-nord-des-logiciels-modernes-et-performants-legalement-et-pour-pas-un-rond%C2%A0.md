---
site: La Voix du Nord
title: "Des logiciels modernes et performants, légalement et pour pas un rond !"
author: Franck Bazin
date: 2013-05-13
href: http://www.lavoixdunord.fr/culture-loisirs/des-logiciels-modernes-et-performants-legalement-et-pour-ia0b0n1245401
tags:
- Économie
- Sensibilisation
- Associations
---

> Si vous avez besoin de logiciels efficaces mais pas forcément l'envie ou les moyens d'acheter des solutions souvent onéreuses (surtout pour un usage modéré), vous trouverez votre bonheur dans la pléthorique offre de logiciels libres et gratuits.
