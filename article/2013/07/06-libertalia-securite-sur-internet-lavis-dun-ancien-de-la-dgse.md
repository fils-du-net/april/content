---
site: Libertalia
title: "Sécurité sur Internet: l'avis d'un ancien de la DGSE"
author: Julien
date: 2013-07-06
href: http://www.libertalia.org/informatique/reseau/securite-sur-internet-l-avis-de-l-expert-en.xhtml
tags:
- Internet
- Administration
- Institutions
- Informatique-deloyale
- Marchés publics
- Europe
---

> Thinkerview (T): M. Eric Filiol, bonjour, je vous interviewe aujourd'hui pour un site internet qui s'appelle Thinkerview. J'aurais aimé avoir votre qualité d'expertise sur les récentes fuites de Prism, sur les fuites de différentes entreprises, de différents organismes qui laissent faire. Qu'est-ce que vous en pensez?
