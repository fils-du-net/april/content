---
site: Les Numeriques
title: "Enseignement: les députés donnent la priorité aux logiciels libres"
author: Florence Legrand
date: 2013-07-10
href: http://www.lesnumeriques.com/ordinateur/enseignement-deputes-donnent-priorite-logiciels-libres-n30314.html
tags:
- April
- Institutions
- Éducation
---

> Le lobby des éditeurs de logiciels n'a pas gagné.
