---
site: Mediapart
title: "Priorité au libre, dans la loi!"
author: André Ani
date: 2013-07-10
href: http://blogs.mediapart.fr/blog/andre-ani/100713/priorite-au-libre-dans-la-loi
tags:
- April
- Institutions
- Éducation
---

> Avec l’adoption du projet de loi pour l’enseignement supérieur et la recherche, la priorité au logiciel libre est inscrite dans la loi! C’est un pas important qui vient d’être franchi. Un petit pas, mais quand même. Cela prouve que les choses peuvent avancer dans le bon sens si on s’en donne les moyens!
