---
site: Mediapart
title: "42, l'école en carton de Xavier Niel et Nicolas Sadirac"
author: BastienLQ
date: 2013-07-22
href: http://blogs.mediapart.fr/blog/bastienlq/220713/42-lecole-en-carton-de-xavier-niel-et-nicolas-sadirac
tags:
- Entreprise
- Éducation
---

> Fin mars, Xavier Niel et Nicolas Sadirac annonçaient la création de 42, une école informatique se voulant différente du système scolaire traditionnel. Partant du constat que le secteur de l'informatique manque de main d'œuvre qualifiée, Xavier Niel, patron de Free, fondait à coup de millions d'euros l'école 42 en s'associant avec le créateur de l'Épitech, Nicolas Sadirac. Le 15 juillet commençait la première "piscine", une épreuve se déroulant sur un mois pour sélectionner les meilleurs candidats.
