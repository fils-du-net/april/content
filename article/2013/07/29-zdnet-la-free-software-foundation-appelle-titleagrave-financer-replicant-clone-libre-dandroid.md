---
site: ZDNet
title: "La Free Software Foundation appelle $titleagrave; financer Replicant, clone libre d'Android"
author: Thierry Noisette
date: 2013-07-29
href: http://www.zdnet.fr/actualites/la-free-software-foundation-appelle-a-financer-replicant-clone-libre-d-android-39792795.htm
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Associations
- Innovation
---

> La FSF a lancé un appel aux dons pour soutenir le développement de ce dérivé 100% libre d'Android, notamment pour acheter des appareils en vue de porter cet OS mobile sur de nouveaux smartphones et tablettes.
