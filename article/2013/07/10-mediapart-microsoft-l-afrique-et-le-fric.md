---
site: Mediapart
title: "Microsoft, l’Afrique et le fric"
author: Mohamed SANGARE
date: 2013-07-10
href: http://blogs.mediapart.fr/blog/mohamed-sangare/100713/microsoft-l-afrique-et-le-fric
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Sensibilisation
- International
---

> Depuis mon poste sous Windows 7 Ultimate Edition* (je vous expliquerai plus loin cette précision), j’écris cet article pour vous faire partager mon point de vue sur la stratégie que devraient adopter les dirigeants africains en charge des politiques sur les Nouvelles Technologies de l’information et la Communication communément appelées NTIC.
