---
site: LeMagIT
title: "Le logiciel libre tente une percée dans le projet de loi pour l’enseignement supérieur et la recherche"
author: Cyrille Chausson
date: 2013-07-02
href: http://www.lemagit.fr/technologie/applications/open-source/2013/07/02/le-logiciel-libre-tente-une-percee-dans-le-projet-de-loi-pour-lenseignement-superieur-et-la-recherche
tags:
- Entreprise
- Administration
- April
- Institutions
- Éducation
- Marchés publics
---

> Le projet de loi portant sur l’enseignement supérieur et la recherche contient un article amendé qui préconise la priorité aux logiciels libres dans l’enseignement. Une priorité retenue par la commission mixte paritaire entre les deux assemblées. Une seconde tentative, après l’anesthésie du Libre dans le projet de loi Peillon portant sur la refondation de l’école.
