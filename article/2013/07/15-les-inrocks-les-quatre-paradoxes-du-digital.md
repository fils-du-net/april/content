---
site: les inrocks
title: "Les quatre paradoxes du digital"
author: Nicolas Diaz
date: 2013-07-15
href: http://www.lesinrocks.com/2013/07/15/actualite/les-quatre-paradoxes-du-digital-11409245
tags:
- Internet
- Sensibilisation
- Associations
---

> Nicolas Diaz, webmaster à la FIDH* et hacktiviste, expose en quoi le progrès du digital, entre vitesse, précipitation et pertinence, nous impose de veiller à la préservation des libertés numériques.
