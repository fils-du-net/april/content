---
site: 01netPro.
title: "Priorité au logiciel libre dans l'enseignement supérieur"
author: Xavier Biseul
date: 2013-07-11
href: http://pro.01net.com/editorial/599679/priorite-au-logiciel-libre-dans-l-enseignement-superieur
tags:
- Entreprise
- Institutions
- Éducation
---

> Les tenants du logiciel libre tiennent leur revanche. Alors que le 4 juin les députés avaient refusé de favoriser le recours au logiciel libre à l’école dans le cadre du projet de loi de Refondation de l'Ecole de la République, ils viennent de prendre une position inverse, un mois plus tard. Le vote en dernière lecture par l'Assemblée nationale ce 9 juillet de la loi sur l'Enseignement Supérieur et de la Recherche (ESR) donne cette fois la priorité au logiciel libre.
