---
site: Tom's Guide
title: "La parlement a tranché: priorité au logiciel libre dans l'enseignement supérieur"
author: Geoffroy Husson
date: 2013-07-10
href: http://www.tomsguide.fr/actualite/loi-enseignement-superieur-logiciel-libre,21859.html
tags:
- Administration
- April
- Institutions
- Associations
- Éducation
- Neutralité du Net
---

> A l’occasion de la loi sur l’enseignement supérieur et la recherche votée, les parlementaires ont voté en faveur de l’utilisation «en priorité» du logiciel libre dans l’enseignement supérieur.
