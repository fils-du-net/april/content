---
site: PC INpact
title: "Le Parlement veut donner la priorité au libre dans l’enseignement supérieur"
author: Xavier Berne
date: 2013-07-01
href: http://www.pcinpact.com/news/80923-le-parlement-veut-donner-priorite-au-libre-dans-l-enseignement-superieur.htm
tags:
- Entreprise
- April
- Institutions
- Éducation
- Marchés publics
---

> Les établissements de l’enseignement supérieur français devront-ils bientôt utiliser prioritairement des logiciels libres ? C’est effectivement ce qui pourrait se passer si les parlementaires adoptaient définitivement le texte du projet de loi sur l'enseignement supérieur tel que retenu la semaine dernière en commission mixte paritaire. Sauf si le gouvernement ou certains élus décidaient toutefois de faire adopter un amendement allant dans un sens contraire, notamment sous les sirènes des puissants lobbys du secteur.
