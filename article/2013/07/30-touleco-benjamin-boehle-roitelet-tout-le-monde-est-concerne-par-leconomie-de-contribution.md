---
site: ToulÉco
title: "Benjamin Böhle-Roitelet: «Tout le monde est concerné par l'économie de contribution»"
author: A.F.
date: 2013-07-30
href: http://www.touleco.fr/Benjamin-Bohle-Roitelet-Tout-le-7315.html
tags:
- Entreprise
- Économie
- Innovation
- International
---

> Les technologies numériques ont engendré un nouveau modèle d’économie de type collaboratif: l’open source, Linux ou Wikipédia fonctionnent selon un process de coopération. Cette tendance se propage à tous les secteurs de l’économie offrant de vastes opportunités. Explication avec Benjamin Böhle-Roitelet, fondateur d’Ekito, une société toulousaine d’expertise des technologies avancées.
