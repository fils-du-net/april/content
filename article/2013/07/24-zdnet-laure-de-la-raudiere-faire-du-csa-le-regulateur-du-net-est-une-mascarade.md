---
site: ZDNet
title: "Laure de La Raudière: faire du CSA le régulateur du Net est \"une mascarade\""
date: 2013-07-24
href: http://www.zdnet.fr/actualites/laure-de-la-raudiere-faire-du-csa-le-regulateur-du-net-est-une-mascarade-39792677.htm
tags:
- Internet
- HADOPI
- Institutions
---

> Pour la députée, ce transfert de compétences risque de voir naître une régulation franco-française et technocratique de l’internet.
