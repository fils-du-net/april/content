---
site: Geeko
title: "Le mouvement du «logiciel libre» pose ses valises à Bruxelles"
author: Louis Colart
date: 2013-07-01
href: http://geeko.lesoir.be/2013/07/01/le-mouvement-du-logiciel-libre-pose-ses-valises-a-bruxelles
tags:
- Entreprise
- Philosophie GNU
- Promotion
---

> Les 14e rencontres du logiciel libre se tiendront à Bruxelles du 6 au 11 juillet. Un mouvement qui se sent renforcé par les récentes révélations sur l’espionnage de l’Internet.
