---
site: Silicon.fr
title: "La Free Software Foundation veut favoriser une offre Android 100% open source"
author: David Feugey
date: 2013-07-26
href: http://www.silicon.fr/la-free-software-foundation-veut-favoriser-une-offre-android-100-open-source-88061.html
tags:
- Associations
---

> La FSF aimerait que le projet de clone open source d’Android Replicant prenne de l’ampleur. À cet effet, elle vient de mettre une campagne de dons en ligne.
