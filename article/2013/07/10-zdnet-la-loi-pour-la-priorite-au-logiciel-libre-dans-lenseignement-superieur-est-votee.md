---
site: ZDNet
title: "La loi pour la priorité au logiciel libre dans l'enseignement supérieur est votée"
date: 2013-07-10
href: http://www.zdnet.fr/actualites/la-loi-pour-la-priorite-au-logiciel-libre-dans-l-enseignement-superieur-est-votee-39792273.htm
tags:
- April
- Institutions
- Associations
- Éducation
---

> Le lobbying des éditeurs de logiciels a cette fois échoué et le projet de loi sur l’enseignement supérieur et la recherche conserve sa disposition contestée accordant la priorité au logiciel libre. L'April et l'Aful se réjouissent de ce vote, tout comme Linagora.
