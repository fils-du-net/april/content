---
site: Le Monde Informatique
title: "Les logiciels libres sont une priorité pour l'enseignement supérieur"
author: Jacques Cheminat
date: 2013-07-10
href: http://www.lemondeinformatique.fr/actualites/lire-les-logiciels-libres-sont-une-priorite-pour-l-enseignement-superieur-54326.html
tags:
- Entreprise
- April
- Institutions
- Associations
- Éducation
- Marchés publics
---

> Après avoir été gommée dans la loi sur l'école, la disposition prévoyant d'accorder la priorité des logiciels libres dans l'Education Nationale a été adoptée par l'Assemblée Nationale pour les établissements de l'enseignement supérieur.
