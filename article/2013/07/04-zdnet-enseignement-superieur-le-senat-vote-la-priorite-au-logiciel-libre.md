---
site: ZDNet
title: "Enseignement supérieur: le Sénat a voté la priorité au logiciel libre"
author: Thierry Noisette
date: 2013-07-04
href: http://www.zdnet.fr/actualites/enseignement-superieur-le-senat-a-vote-la-priorite-au-logiciel-libre-39792101.htm
tags:
- Entreprise
- Institutions
- Éducation
- Marchés publics
---

> Les sénateurs ont voté hier le projet de loi relatif à l'enseignement supérieur et à la recherche, qui comprend la priorité aux logiciels libres pour les services et ressources pédagogiques numériques. Ultime obstacle mardi à l'Assemblée.
