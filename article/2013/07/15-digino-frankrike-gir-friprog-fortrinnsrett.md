---
site: digi.no
title: "Frankrike gir friprog fortrinnsrett"
author: Erik Rossen
date: 2013-07-15
href: http://www.digi.no/919665/frankrike-gir-friprog-fortrinnsrett
tags:
- April
- Institutions
- Éducation
---

> Betyr frihet, likhet og brorskap, jubler entusiastene.
