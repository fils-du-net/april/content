---
site: Archimag
title: "Le logiciel libre passe en fac"
author: Bruno Texier
date: 2013-07-10
href: http://www.archimag.com/article/le-logiciel-libre-passe-en-fac
tags:
- April
- Institutions
- Éducation
- Philosophie GNU
---

> Pour la première fois, une loi donne la priorité au logiciel libre.
