---
site: "Pro-Linux"
title: "Frankreich setzt bevorzugt auf freie Software an den Hochschulen"
author: Ferdinand Thommes
date: 2013-07-11
href: http://www.pro-linux.de/news/1/19997/frankreich-setzt-bevorzugt-auf-freie-software-an-den-hochschulen.html
tags:
- April
- Institutions
- Éducation
---

> In Frankreich liegt dem Präsidenten ein Gesetzentwurf vor, der erstmals freie Software bevorzugt.
