---
site: PCWorld.fr
title: "Le parlement donne la priorité au logiciel libre, mais le problème reste entier"
author: Michel Beck
date: 2013-07-10
href: http://www.pcworld.fr/business/actualites,parlement-donne-priorite-logiciel-libre,540209,1.htm
tags:
- April
- Institutions
- Éducation
---

> C'est un peu l'histoire de la bonne et de la mauvaise conscience : d'un côté l'une indique ce qu'il convient de faire quand l'autre nous incite à faire exactement le contraire. Et il n'est jamais facile de savoir à laquelle des deux se fier.
