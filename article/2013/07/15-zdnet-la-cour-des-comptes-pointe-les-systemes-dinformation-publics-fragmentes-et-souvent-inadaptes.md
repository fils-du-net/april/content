---
site: ZDNet
title: "La Cour des comptes pointe les systèmes d'information publics, \"fragmentés et souvent inadaptés\""
date: 2013-07-15
href: http://www.zdnet.fr/actualites/la-cour-des-comptes-pointe-les-systemes-d-information-publics-fragmentes-et-souvent-inadaptes-39792427.htm
tags:
- Internet
- Administration
- Institutions
- RGI
---

> Maillage territorial incohérent, gouvernance à revoir... La cour des Comptes pointe les lacunes de l'Etat en manière de systèmes informatiques.
