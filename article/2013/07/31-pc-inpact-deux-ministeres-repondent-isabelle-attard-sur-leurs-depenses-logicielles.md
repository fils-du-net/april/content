---
site: PC INpact
title: "Deux ministères répondent à Isabelle Attard sur leurs dépenses logicielles"
author: Xavier Berne
date: 2013-07-31
href: http://www.pcinpact.com/news/81501-deux-ministeres-repondent-a-isabelle-attard-sur-leurs-depenses-logicielles.htm
tags:
- Administration
- Économie
- Institutions
---

> Depuis le 27 mai dernier, chacun des 37 ministres du gouvernement Ayrault a été prié par la députée Isabelle Attard de fournir le détail de ses dépenses en logiciels pour la période 2008-2012. Le tout, si possible en opérant une distinction entre logiciels libres et propriétaires. Si 10 ministères s’étaient - plus ou moins bien - pliés jusqu'ici à l’exercice, deux nouvelles réponses viennent d’être publiées ce matin au Journal Officiel.
