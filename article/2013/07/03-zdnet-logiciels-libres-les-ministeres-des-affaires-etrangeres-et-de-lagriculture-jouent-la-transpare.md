---
site: ZDNet
title: "Logiciels libres: les ministères des Affaires étrangères et de l'Agriculture jouent la transparence"
author: Thierry Noisette
date: 2013-07-03
href: http://www.zdnet.fr/actualites/logiciels-libres-les-ministeres-des-affaires-etrangeres-et-de-l-agriculture-jouent-la-transparence-39792055.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- April
- Institutions
- Marchés publics
- RGI
---

> Le Quai d'Orsay développe ses sites web sous Spip et installe LibreOffice en parallèle à la suite de Microsoft. L'Agriculture a dépensé 2,1 millions d'euros en logiciels l'an dernier. Le ministère a généralisé l'usage des formats OpenDocument.
