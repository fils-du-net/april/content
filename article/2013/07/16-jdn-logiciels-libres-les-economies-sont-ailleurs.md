---
site: JDN
title: "Logiciels libres: les économies sont ailleurs"
author: Loann Fraillon
date: 2013-07-16
href: http://www.journaldunet.com/web-tech/expert/54832/logiciels-libres---les-economies-sont-ailleurs.shtml
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Éducation
---

> La loi Fioraso incite l'usage des logiciels libres dans l'enseignement supérieur et la recherche. Présenté comme un levier majeur d'économies budgétaires, le logiciel libre se soumet alors au jugement d'un plus grand public. Entre amour et désamour, zoom sur la face cachée du libre.
