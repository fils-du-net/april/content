---
site: Web & Tech
title: "Priorité au logiciel libre dans l'enseignement supérieur"
author: Julien Loubiere
date: 2013-07-10
href: http://web-tech.fr/priorite-au-logiciel-libre-dans-lenseignement-superieur
tags:
- April
- Institutions
- Associations
- Éducation
---

> Le texte de loi concernant l’enseignement supérieur et voté à l’Assemblée Nationale donne la priorité au logiciel libre.
