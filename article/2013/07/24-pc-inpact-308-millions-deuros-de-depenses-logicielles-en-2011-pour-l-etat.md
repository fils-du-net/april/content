---
site: PC INpact
title: "308 millions d'euros de dépenses logicielles en 2011 pour l’État"
author: Xavier Berne
date: 2013-07-24
href: http://www.pcinpact.com/news/81372-308-millions-depenses-logicielles-en-2011-pour-reforme-l-etat.htm
tags:
- Logiciels privateurs
- Administration
- Institutions
---

> Un nouveau ministère vient de lever une partie du voile sur ses dépenses logicielles, suite à la demande effectuée fin mai par la députée écologiste Isabelle Attard. Cette semaine, c’est le ministère de la Réforme de l'État, de la décentralisation et de la fonction publique qui s’est plié à l’exercice.
