---
site: Tela Botanica
title: "Le parlement français adopte pour la première fois une disposition législative donnant la priorité au logiciel libre"
author: Marie PICARD &amp; Grégoire DUCHÉ
date: 2013-07-11
href: http://www.tela-botanica.org/actu/article5794.html
tags:
- April
- Institutions
- Éducation
---

> Le parlement français vient d’inscrire pour la première fois dans la loi la priorité au logiciel libre pour un service public, avec l’adoption du projet de loi enseignement supérieur et recherche.
