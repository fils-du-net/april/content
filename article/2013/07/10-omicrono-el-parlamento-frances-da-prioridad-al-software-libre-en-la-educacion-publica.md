---
site: Omicrono
title: "El Parlamento francés da prioridad al software libre en la educación pública"
author: Javier Romero
date: 2013-07-10
href: http://www.omicrono.com/2013/07/el-parlamento-frances-da-prioridad-al-software-libre-en-la-educacion-publica
tags:
- April
- Institutions
- Éducation
---

> El Parlamento francés confirma que el software libre tendrá prioridad en la educación. Esta nueva ley parte del éxito en la votación del pasado día 3 de julio por el que se cuestionaba el asunto de la utilización de software libre en los sistemas informáticos de orden público del sector educativo.
