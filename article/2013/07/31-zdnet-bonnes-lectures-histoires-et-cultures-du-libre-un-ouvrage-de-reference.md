---
site: ZDNet
title: "Bonnes lectures: \"Histoires et cultures du Libre\", un ouvrage de référence"
author: Thierry Noisette
date: 2013-07-31
href: http://www.zdnet.fr/actualites/bonnes-lectures-histoires-et-cultures-du-libre-un-ouvrage-de-reference-39792903.htm
tags:
- Partage du savoir
- Sensibilisation
- Associations
- Licenses
---

> Pas seulement pour ne pas bronzer idiot, selon la formule rituelle, mais aussi comme livre de référence, cet ouvrage publié par Framabook aborde au fil de son bon demi-millier de pages un vaste éventail de sujets relatifs à la culture libriste, autour des logiciels libres et de leurs univers.
