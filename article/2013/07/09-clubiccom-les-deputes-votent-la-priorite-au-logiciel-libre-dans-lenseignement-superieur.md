---
site: clubic.com
title: "Les députés votent la priorité au logiciel libre dans l'Enseignement supérieur"
author: Ludwig Gallet
date: 2013-07-09
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/logiciel-libre-open-source/actualite-571032-enseignement-superieur-assemblee-nationale-vote-priorite-logiciel-libre.html
tags:
- Entreprise
- Administration
- April
- Institutions
- Éducation
---

> Après le Sénat, l'Assemblée nationale vient à son tour d'adopter le projet de loi sur l'enseignement supérieur. Pour la première fois, un texte législatif accorde une priorité au logiciel libre.
