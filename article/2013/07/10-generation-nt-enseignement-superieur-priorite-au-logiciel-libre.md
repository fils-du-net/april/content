---
site: "Génération-NT"
title: "Enseignement supérieur: priorité au logiciel libre"
author: Jérôme G.
date: 2013-07-10
href: http://www.generation-nt.com/enseignement-superieur-logiciel-libre-ecole-actualite-1759712.html
tags:
- Administration
- April
- Institutions
- Éducation
- Marchés publics
---

> Le Parlement français a voté l'utilisation en priorité des logiciels libres dans l'enseignement supérieur. Ce n'est pas le cas pour l'école.
