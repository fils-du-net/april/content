---
site: 01netPro.
title: "Légiférer sur la neutralité d'Internet pour en finir avec l'utopie numérique"
author: Michel Cosnard
date: 2013-07-11
href: http://pro.01net.com/editorial/599159/legiferer-sur-la-neutralite-dinternet-pour-en-finir-avec-lutopie-numerique
tags:
- Internet
- Institutions
- Neutralité du Net
---

> L'impact des technologies de l'information sur l'ensemble des activités humaines est tel que désormais, on peut raisonnablement prétendre que la société – et même le monde – est devenue numérique. Fruit de l'utopie des inventeurs d'Internet, du Web, des logiciels libres et open source, des modèles économiques alternatifs et autres avatars en ce domaine, cette évolution de la société s'est produite essentiellement à côté des structures classiques de gouvernance. Des réseaux de savoir, d'expression et d'information se sont constitués parallèlement aux hiérarchies de pouvoir.
