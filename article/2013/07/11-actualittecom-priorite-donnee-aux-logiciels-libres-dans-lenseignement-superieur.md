---
site: ActuaLitté.com
title: "Priorité donnée aux logiciels libres dans l'enseignement supérieur"
author: Victor De Sepausy
date: 2013-07-11
href: http://www.actualitte.com/scolarite/priorite-donnee-aux-logiciels-libres-dans-l-enseignement-superieur-43764.htm
tags:
- Entreprise
- Institutions
- Éducation
---

> Le résultat du projet de loi de Geneviève Fioraso.
