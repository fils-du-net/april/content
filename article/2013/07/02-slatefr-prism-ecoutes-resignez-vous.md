---
site: Slate.fr
title: "Prism, écoutes: résignez-vous!"
author: Andréa Fradin
date: 2013-07-02
href: http://www.slate.fr/monde/74668/prism-resignation
tags:
- Internet
- Institutions
---

> Nos libertés fondamentales, notre droit élémentaire au secret de nos échanges ont été bafoués et tout le monde s'en moque. Et si nous avions entériné que la vie privée était définitivement morte?
