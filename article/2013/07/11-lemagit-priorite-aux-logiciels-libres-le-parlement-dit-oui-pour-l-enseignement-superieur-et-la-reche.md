---
site: LeMagIT
title: "Priorité aux logiciels libres: le parlement dit oui pour l’enseignement supérieur et la recherche"
author: Cyrille Chausson
date: 2013-07-11
href: http://www.lemagit.fr/economie/business/2013/07/11/priorite-aux-logiciels-libres-le-parlement-dit-oui-pour-lenseignement-superieur-et-la-recherche
tags:
- Entreprise
- April
- Institutions
- Éducation
---

> Ce 9 juillet, le parlement français a adopté le texte de loi portant sur l’enseignement supérieur et la recherche et avec un amendement donnant la priorité aux logiciels libres dans le secteur. Une première.
