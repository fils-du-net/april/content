---
site: PC INpact
title: "Dépenses logicielles: Bercy et l’Écologie se découvrent d'un fil"
author: Xavier Berne
date: 2013-07-17
href: http://www.pcinpact.com/news/81240-depenses-logicielles-bercy-et-l-ecologie-se-decouvrent-dun-fi.htm
tags:
- Logiciels privateurs
- Administration
- Institutions
- Europe
---

> Comme l'avait réclamé la députée écologiste Isabelle Attard, plusieurs ministères viennent de lever (partiellement) le voile sur leurs dépenses en logiciels libres et propriétaires. Le ministère du Redressement productif vient en effet de se plier à l'exercice pour le compte de Bercy, de même que le ministre de la Ville pour l'Écologie.
