---
site: Apogeonline
title: "La Francia ha compreso la lezione italiana"
author: Simone Aliprandi
date: 2013-07-22
href: http://www.apogeonline.com/webzine/2013/07/22/la-francia-ha-compreso-la-lezione-italiana
tags:
- Administration
- April
- Institutions
- Éducation
---

> Qualche volta è l'Italia che ha da insegnare agli altri Paesi, stavolta con un esempio di disposizione seguito anche oltre le Alpi.
