---
site: ZDNet
title: "Projet de loi consommation: Benoît Hamon fait retirer un amendement anti-vente liée"
author: Thierry Noisette
date: 2013-07-30
href: http://www.zdnet.fr/actualites/projet-de-loi-consommation-beno-t-hamon-fait-retirer-un-amendement-anti-vente-liee-39792856.htm
tags:
- Institutions
- Vente liée
---

> Lors de l'examen en commission au Sénat, le ministre de la Consommation a fait retirer un amendement obligeant à indiquer les prix distincts des logiciels, "pour le retravailler".
