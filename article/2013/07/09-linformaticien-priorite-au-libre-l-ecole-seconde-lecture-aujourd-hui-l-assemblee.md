---
site: L'Informaticien
title: "Priorité au libre à l’école: seconde lecture aujourd’hui à l’Assemblée"
author: Louis Adam
date: 2013-07-09
href: http://www.linformaticien.com/actualites/id/29564/priorite-au-libre-a-l-ecole-seconde-lecture-aujourd-hui-a-l-assemblee.aspx
tags:
- Entreprise
- Institutions
- Associations
- Éducation
- Marchés publics
---

> C’est aujourd’hui que l’Assemblée nationale doit se pencher en seconde lecture sur le projet de loi relatif à l’enseignement supérieur et à la recherche. L’article 6 de ce projet fait polémique, un amendement mentionne en effet que les logiciels libres seront «utilisés en priorité»
