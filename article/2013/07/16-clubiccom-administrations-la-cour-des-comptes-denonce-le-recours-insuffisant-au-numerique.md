---
site: clubic.com
title: "Administrations: la Cour des comptes dénonce le recours insuffisant au numérique"
author: Ludwig Gallet
date: 2013-07-16
href: http://www.clubic.com/internet/actualite-572646-cour-comptes-rapport-organisation-territoriale.html
tags:
- Entreprise
- Internet
- Administration
- Économie
- Institutions
- RGI
---

> La Cour des comptes a publié le 11 juillet son rapport sur l'organisation territoriale de l'État. Elle n'est pas tendre avec l'organisation des systèmes d'information publics, qu'elle estime «défaillante».
