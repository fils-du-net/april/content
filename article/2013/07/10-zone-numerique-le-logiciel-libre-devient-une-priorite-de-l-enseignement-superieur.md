---
site: Zone Numerique
title: "Le logiciel libre devient une priorité de l' enseignement supérieur"
date: 2013-07-10
href: http://www.zone-numerique.com/le-logiciel-libre-devient-une-priorite-de-l-enseignement-superieur.html
tags:
- Entreprise
- Administration
- April
- Institutions
- Éducation
---

> La priorité vient d’être donnée au logiciel libre dans l’enseignement supérieur.Les députés et sénateurs viennent de voter un texte dans le cadre du projet de loi sur l’enseignement supérieur et la recherche. Il a été adopté hier par l’Assemblée nationale et devra paraître prochainement au Journal Officiel.
