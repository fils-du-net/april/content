---
site: Be Geek
title: "L'utilisation de logiciels libres est désormais prioritaire dans l'enseignement supérieur"
author: Antoine Roche
date: 2013-07-10
href: http://www.begeek.fr/lutilisation-de-logiciels-libres-est-desormais-prioritaire-dans-lenseignement-superieur-97501
tags:
- April
- Institutions
- Éducation
---

> En adoptant le projet de loi Fioraso, l’Assemblée Nationale donne la priorité aux logiciels libres dans l'enseignement supérieur.
