---
site: écrans
title: "Dans la loi, le libre arbitre"
author: Michael Ducousso
date: 2013-07-10
href: http://www.ecrans.fr/Dans-la-loi-le-libre-arbitre,16689.html
tags:
- Administration
- April
- Institutions
- Éducation
---

> Avec la loi Fioraso, le Parlement a validé une loi historique pour toute la communauté du Libre française. Pour la première fois, un texte légal donne la priorité à l’usage des logiciels libres.
