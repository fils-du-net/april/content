---
site: ActuaLitté
title: "Les éditeurs devant l'offre de manuels scolaires numériques"
author: Nicolas Gary
date: 2013-07-23
href: http://www.actualitte.com/education-international/les-editeurs-devant-l-offre-de-manuels-scolaires-numeriques-44023.htm
tags:
- Internet
- Économie
- Partage du savoir
- Éducation
- International
---

> Structurer le marché du livre scolaire, dans son format numérique, devient le combat premier des éditeurs américains. Ce commerce, qui connaît une certaine embellie depuis quelques années, permet aux étudiants de réaliser de réelles économies, surtout en regard du prix des ouvrages en version imprimée. Mais pour les éditeurs, la question économique se pose: comment gagner de l'argent malgré tout?
