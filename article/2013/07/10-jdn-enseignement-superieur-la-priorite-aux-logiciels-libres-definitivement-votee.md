---
site: JDN
title: "Enseignement supérieur: la \"priorité\" aux logiciels libres définitivement votée"
author: Virgile Juhan
date: 2013-07-10
href: http://www.journaldunet.com/solutions/saas-logiciel/logiciel-libre-dans-l-education-loi-fioraso-0713.shtml
tags:
- Entreprise
- April
- Institutions
- Associations
- Éducation
---

> La loi Fioraso sur l'enseignement supérieur et la recherche a été définitivement adoptée. Elle indique que les logiciels libres doivent être utilisés "en priorité". L'écosystème de l'Open Source jubile.
