---
site: RTBF Info
title: "Les logiciels libres à l'honneur cette semaine à l'ULB"
author: Geoffroy Fabré
date: 2013-07-08
href: http://www.rtbf.be/info/regions/detail_les-logiciels-libres-a-l-honneur-cette-semaine-a-l-ulb?id=8044050
tags:
- Administration
- Économie
- April
- Promotion
---

> Cette semaine, les rencontres mondiales du logiciel libre se tiennent à l'Université Libre de Bruxelles. Traitements de texte, systèmes d'exploitation, feuilles de calcul. Les logiciels libres sont gratuits et libres de droits. A la base, mis en ligne par des passionnés qui les développent, ils sont une solution de remplacement des logiciels payants, développés par Microsoft ou Apple. D'ailleurs les administrations bruxelloises elles-mêmes en utilisent.
