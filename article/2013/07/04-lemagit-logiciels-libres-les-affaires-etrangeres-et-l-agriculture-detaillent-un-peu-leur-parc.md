---
site: LeMagIT
title: "Logiciels libres: Les Affaires Etrangères et l’Agriculture détaillent un peu leur parc"
author: Cyrille Chausson
date: 2013-07-04
href: http://www.lemagit.fr/technologie/applications/open-source/2013/07/04/logiciels-libres-les-affaires-etrangeres-et-lagriculture-detaillent-un-peu-leur-parc
tags:
- Logiciels privateurs
- Administration
- Économie
- April
- Institutions
---

> Les ministères des Affaires Etrangères et de l’Agriculture sont les premiers ministères à livrer l’état du logiciel libre dans leur SI, en réponse à la question de la député Isabelle Attard.
