---
site: Agefi.com
title: "Le contribuable ne paie qu’une fois"
author: Grégoire Barbey
date: 2013-07-17
href: http://www.agefi.com/une/detail/archive/2013/july/artikel/vaud-les-collectivites-publiques-doivent-se-doter-de-logiciels-metier-libres-francois-marthaler-revient-sur-ses-actions-au-conseil-detat.html
tags:
- Entreprise
- Internet
- Administration
- Économie
- Institutions
- International
---

> Vaud. Les collectivités publiques doivent se doter de logiciels métier libres. François Marthaler revient sur ses actions au Conseil d’Etat.
