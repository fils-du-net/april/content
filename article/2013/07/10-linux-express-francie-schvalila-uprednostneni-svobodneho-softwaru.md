---
site: Linux Express
title: "Francie schválila upřednostnění svobodného softwaru"
author: Lukáš Jelínek
date: 2013-07-10
href: http://www.linuxexpres.cz/francie-schvalila-uprednostneni-svobodneho-softwaru
tags:
- April
- Institutions
- Éducation
---

> Francouzský parlament schválil novelu zákona o vyšším školství a výzkumu. Novela obsahuje upřednostnění svobodného softwaru. K platnosti chybí už jen podpis prezidenta.
