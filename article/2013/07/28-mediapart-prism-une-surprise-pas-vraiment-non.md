---
site: Mediapart
title: "Prism? Une surprise? Pas vraiment non…"
author: Skhaen
date: 2013-07-28
href: http://blogs.mediapart.fr/blog/skhaen/280713/prism-une-surprise-pas-vraiment-non
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Informatique-deloyale
---

> En 1988, le journaliste Duncan Campbell révèla l'existence d'un programme de renseignement, Echelon, dans un article pour The New Statesman qui s'intitulait «Somebody's listening». En 1996, c'était le journaliste néo-zélandais Nicky Hager qui publiait «Secret power» sur l'implication de son pays dans le programme. En 1999, suite à une demande du Parlement Européen, Duncan Campbell rend un rapport intitulé «Interception Capabilities 2000» pour le STOA (traduction française disponible sous le titre «surveillance electronique planétaire» aux éditions Allia).
