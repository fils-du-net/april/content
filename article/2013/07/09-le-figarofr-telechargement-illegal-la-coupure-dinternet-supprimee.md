---
site: Le Figaro.fr
title: "Téléchargement illégal: la coupure d'Internet supprimée"
author: Lucie Ronfaut
date: 2013-07-09
href: http://www.lefigaro.fr/hightech/2013/07/09/01007-20130709ARTFIG00440-telechargement-illegal-la-coupure-d-internet-supprimee.php
tags:
- Internet
- HADOPI
- Institutions
- Associations
---

> Une association de défense de droits et libertés des citoyens sur le Net dénonce «un effet d'annonce», après l'abrogation de cette peine, qui n'a été appliquée qu'une seule fois depuis 2009.
