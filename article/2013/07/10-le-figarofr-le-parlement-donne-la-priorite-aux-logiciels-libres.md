---
site: Le Figaro.fr
title: "Le Parlement donne la priorité aux logiciels libres"
author: Lucie Ronfaut
date: 2013-07-10
href: http://www.lefigaro.fr/hightech/2013/07/10/01007-20130710ARTFIG00512-le-parlement-donne-la-priorite-aux-logiciels-libres.php
tags:
- Entreprise
- April
- Institutions
- Éducation
---

> Le Parlement a adopté mardi un projet de loi donnant priorité à l'utilisation des logiciels libres par les établissements publics d'enseignement supérieur. En France, le marché de l'open source représentait 2,5 milliards d'euros en 2012.
