---
site: ZDNet
title: "Enseignement: l’Inria contre la priorité au logiciel libre, la controverse continue"
author: Thierry Noisette
date: 2013-07-09
href: http://www.zdnet.fr/actualites/enseignement-l-inria-contre-la-priorite-au-logiciel-libre-la-controverse-continue-39792240.htm
tags:
- Entreprise
- Administration
- April
- Institutions
- Éducation
---

> La signature du président du laboratoire public de recherche au côté de celles de l’Afdel et du Syntec numérique contre la priorité aux logiciels libres dans le projet de loi sur l’enseignement supérieur est critiquée par une lettre ouverte de personnels de l’Inria.
