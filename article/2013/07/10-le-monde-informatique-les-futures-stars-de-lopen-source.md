---
site: Le Monde Informatique
title: "Les futures stars de l'Open Source"
author: Jean Elyan
date: 2013-07-10
href: http://www.lemondeinformatique.fr/actualites/lire-les-futures-stars-de-l-open-source-1e-partie-54325.html
tags:
- Entreprise
- Sensibilisation
- Accessibilité
- Innovation
---

> Nous avons choisi de mettre en avant six développeurs annoncés comme les futurs stars de l'Open Source.
