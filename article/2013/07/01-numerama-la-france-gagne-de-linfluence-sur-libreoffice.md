---
site: Numerama
title: "La France gagne de l'influence sur LibreOffice"
author: Guillaume Champeau
date: 2013-07-01
href: http://www.numerama.com/magazine/26399-la-france-gagne-de-l-influence-sur-libreoffice.html
tags:
- Administration
- Institutions
- Promotion
---

> Le groupe de travail interministériel dédié à la promotion des logiciels libres dans l'administration, le MIMO, est devenu membre du Conseil consultatif de la Documentation Foundation, qui édite LibreOffice.
