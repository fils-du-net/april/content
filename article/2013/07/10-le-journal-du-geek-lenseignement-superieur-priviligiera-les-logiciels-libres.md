---
site: Le Journal du Geek
title: "L'enseignement supérieur priviligiera les logiciels libres"
author: Jerome
date: 2013-07-10
href: http://www.journaldugeek.com/2013/07/10/lenseignement-superieur-priviligiera-les-logiciels-libres
tags:
- April
- Institutions
- Éducation
---

> Cela ne sera pas fait sans heurt, mais les députés ont finalement adopté hier et définitivement la loi Fioraso, du nom du ministre de l’Enseignement supérieur et de la recherche.
