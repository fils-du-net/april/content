---
site: PC INpact
title: "Le logiciel libre obtient la priorité dans l'enseignement supérieur"
author: Marc Rees
date: 2013-07-10
href: http://www.pcinpact.com/news/81109-le-parlement-donne-priorite-au-logiciel-libre-dans-enseignement-superieur.htm
tags:
- Entreprise
- Administration
- April
- Éducation
- Marchés publics
- Promotion
---

> C’est une première dans l’histoire parlementaire: les députés et sénateurs ont voté un texte donnant la priorité au logiciel libre dans un secteur bien particulier. Ce vote a eu lieu dans le cadre du projet de loi sur l’enseignement supérieur et la recherche, adopté hier par l’Assemblée nationale. Le texte attend maintenant sa publication au Journal Officiel.
