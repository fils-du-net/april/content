---
site: infoDSI
title: "Le parlement français donne désormais la priorité au logiciel libre"
date: 2013-07-09
href: http://www.infodsi.com/articles/142047/parlement-francais-donne-desormais-priorite-logiciel-libre.html
tags:
- Entreprise
- April
- Institutions
- Éducation
---

> Le parlement français vient d'inscrire pour la première fois dans la loi la priorité au logiciel libre pour un service public, avec l'adoption du projet de loi enseignement supérieur et recherche. L'April, qui a contribué aux débats, se réjouit tout particulièrement de ce vote et félicite les députés et sénateurs d'avoir reconnu l'importance des logiciels libres pour le service public de l'enseignement supérieur, seuls permettant de garantir l'égal accès de tous à ce futur service public.
