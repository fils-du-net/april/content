---
site: Numerama
title: "Des OGM Open Source pour lutter contre Monsanto?"
author: zopzoup
date: 2013-07-20
href: http://www.numerama.com/f/126879-t-des-ogm-open-source-pour-lutter-contre-monsanto.html
tags:
- Entreprise
- Partage du savoir
- Innovation
- Licenses
- Sciences
---

> La version américaine de Slate vient de publier un article surprenant proposant de développer des Organismes Génétiquement Modifiés sous licence Open Source
