---
site: Joinup
title: "French parliament makes free software law for higher education"
author: Gijs Hillenius
date: 2013-07-11
href: http://joinup.ec.europa.eu/community/osor/news/french-parliament-makes-free-software-law-higher-education
tags:
- Entreprise
- April
- Institutions
- Éducation
- English
---

> France's higher education institutes must offer their digital services and learning resource materials primarily as free software, the country's parliament decided Tuesday afternoon. A new law on higher education and research comes with an article giving priority to free software.
