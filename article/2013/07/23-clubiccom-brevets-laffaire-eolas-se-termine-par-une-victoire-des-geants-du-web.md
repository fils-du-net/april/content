---
site: clubic.com
title: "Brevets: l'affaire Eolas se termine par une victoire des géants du Web"
author: Olivier Robillart
date: 2013-07-23
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/brevet-logiciel/actualite-574198-eolas-brevet.html
tags:
- Entreprise
- Internet
- Institutions
- Brevets logiciels
- International
---

> La société Eolas Technologies vient de perdre son procès en appel contre Google, Yahoo, Amazon et 19 autres géants du numérique. Elle estimait détenir plusieurs brevets permettant notamment aux navigateurs Internet d'interagir avec des applications web.
