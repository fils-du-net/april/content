---
site: ITespresso
title: "Les logiciels libres seront prioritaires dans l’enseignement supérieur"
author: Thibault Deschamps
date: 2013-07-10
href: http://www.itespresso.fr/logiciels-libres-prioritaire-enseignement-superieur-66176.html
tags:
- Entreprise
- Administration
- April
- Institutions
- Associations
- Éducation
---

> L’Assemblée Nationale a adopté le projet de loi Fioraso donnant la priorité à l’utilisation de logiciels libres dans l’enseignement supérieur. Au grand dam d’organisations professionnelles du secteur du logiciel (AFDEL, Syntec Numérique).
