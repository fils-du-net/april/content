---
site: MacGeneration
title: "Blog: 42, l'école en carton de Xavier Niel et Nicolas Sadirac"
author: Christophe Laporte
date: 2013-07-23
href: http://www.macg.co/web/voir/134599/42-l-ecole-en-carton-de-xavier-niel-et-nicolas-sadirac
tags:
- Entreprise
- Internet
- Éducation
---

> La fameuse école 42 de Xavier Niel a récemment ouvert ses portes. Un étudiant fait part de son témoignage sur l’un des blogs de Mediapart, un témoignage à charge.
