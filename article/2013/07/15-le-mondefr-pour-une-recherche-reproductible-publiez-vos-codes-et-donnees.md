---
site: Le Monde.fr
title: "Pour une recherche reproductible, publiez vos codes et données"
author: Antoine Blanchard, Elifsu Sabuncu et Yvan Stroppa
date: 2013-07-15
href: http://www.lemonde.fr/sciences/article/2013/07/15/pour-une-recherche-reproductible-publiez-vos-codes-et-donnees_3447825_1650684.html
tags:
- Économie
- Partage du savoir
- Sciences
---

> Selon Antoine Blanchard et Elifsu Sabuncu, fondateurs du Deuxième Labo, et Yvan Stroppa, ingénieur CNRS, la lutte contre la fraude scientifique passe par une meilleure transparence des résultats des chercheurs.
