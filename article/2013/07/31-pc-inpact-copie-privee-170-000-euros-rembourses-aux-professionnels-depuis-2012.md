---
site: PC INpact
title: "Copie privée: 170 000 euros remboursés aux professionnels depuis 2012"
author: Marc Rees
date: 2013-07-31
href: http://www.pcinpact.com/news/81489-copie-privee-seulement-170-000-euros-rembourses-aux-pro-depuis-2012.htm
tags:
- Entreprise
- Économie
- Institutions
---

> «Depuis la loi du 20 décembre 2011, la rémunération pour copie privée n’est pas due pour les usages strictement professionnels». Voilà ce qu’annonçaient la SACD et l’ADAMI dans un document présenté lors d’une récente conférence à Avignon. Des millions d'euros «ne seront jamais remboursés» répondaient les importateurs, distributeurs et industriels du secteur. On sait désormais combien ont été effectivement remboursés aux pros depuis cette date.
