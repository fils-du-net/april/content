---
site: clubic.com
title: "Logiciel libre: priorité adoptée, la polémique subsiste"
author: Ludwig Gallet
date: 2013-07-10
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/logiciel-libre-open-source/actualite-571262-enseignement-superieur-logiciel-libre-priorite-pol.html
tags:
- Entreprise
- Administration
- April
- Institutions
- Éducation
- Innovation
- Marchés publics
---

> Mardi soir, l'Assemblée nationale accordait un droit de priorité au logiciel libre dans l'Enseignement supérieur. Une disposition qui suscite la polémique et pour laquelle se confrontent les représentants des éditeurs libres d'un côté et propriétaires de l'autre.
