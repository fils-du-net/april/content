---
site: Developpez.com
title: "Les députés votent la priorité au logiciel libre dans l'enseignement supérieur"
author: Hinault Romaric
date: 2013-07-10
href: http://www.developpez.com/actu/58066/Les-deputes-votent-la-priorite-au-logiciel-libre-dans-l-enseignement-superieur-echec-du-lobbying-des-editeurs-de-logiciels-proprietaires
tags:
- Entreprise
- April
- Institutions
- Éducation
---

> Échec du lobbying des éditeurs de logiciels propriétaires
