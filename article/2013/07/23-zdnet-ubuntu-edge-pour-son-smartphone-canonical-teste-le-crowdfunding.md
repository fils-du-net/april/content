---
site: ZDNet
title: "Ubuntu Edge: pour son smartphone, Canonical teste le crowdfunding"
date: 2013-07-23
href: http://www.zdnet.fr/actualites/ubuntu-edge-pour-son-smartphone-canonical-teste-le-crowdfunding-39792648.htm
tags:
- Entreprise
- Innovation
---

> L'entreprise sud-africaine a 30 jours pour lever 32 millions de dollars sur Indiegogo, et financer ainsi Ubuntu Edge, un téléphone "au niveau d'un PC".
