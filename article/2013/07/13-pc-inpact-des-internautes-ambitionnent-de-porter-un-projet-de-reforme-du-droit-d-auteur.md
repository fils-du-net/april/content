---
site: PC INpact
title: "Des internautes ambitionnent de porter un projet de réforme du droit d’auteur"
author: Xavier Berne
date: 2013-07-13
href: http://www.pcinpact.com/news/81176-des-internautes-ambitionnent-porter-projet-reforme-droit-d-auteur.htm
tags:
- Internet
- April
- Institutions
- Associations
- Droit d'auteur
---

> Le Collectif Savoirs Com1 soutient actuellement un projet de réforme du droit d’auteur portant différentes mesures telle que la dépénalisation des échanges sans but lucratif ou la mise en place d’une contribution créative. Le texte, toujours en cours de discussion, devrait à l’issue de ce processus être adressé sous forme papier aux députés.
