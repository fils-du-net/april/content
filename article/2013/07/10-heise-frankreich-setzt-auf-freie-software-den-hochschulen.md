---
site: heise
title: "[heise] Frankreich setzt auf freie Software an den Hochschulen"
date: 2013-07-10
href: http://www.heise.de/newsticker/meldung/Frankreich-setzt-auf-freie-Software-an-den-Hochschulen-1915229.html
tags:
- April
- Institutions
- Éducation
---

> Die französische Nationalversammlung hat am Dienstag einen Gesetzentwurf verabschiedet, wonach im Hochschul- und Forschungssektor hauptsächlich Open Source verwendet werden soll. Die Initiative enthält kurz und knapp die Bestimmung für diesen Sektor: "Freie Software wird bevorzugt eingesetzt." Der Senat hatte den Vorstoß bereits Anfang Juli abgesegnet. Nach der Zustimmung des Parlaments fehlt nur noch die Unterschrift des französischen Präsidenten François Hollande, damit das Gesetz in Kraft treten kann.
