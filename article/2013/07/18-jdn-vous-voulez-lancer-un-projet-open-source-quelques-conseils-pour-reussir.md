---
site: JDN
title: "Vous voulez lancer un projet open source? Quelques conseils pour réussir"
author: Patrice Bertrand
date: 2013-07-18
href: http://www.journaldunet.com/web-tech/expert/54901/vous-voulez-lancer-un-projet-open-source---quelques-conseils-pour-reussir.shtml
tags:
- Sensibilisation
- Licenses
---

> Vous allez être utile, vivre une aventure extraordinaire, vous faire des amis, contribuer au progrès d'un monde de plus en plus numérique, connaître l'excitation des startups, et peut-être même vous enrichir. Faisons le tour des questions qui vont se poser à vous.
