---
site: ZDNet
title: "Logiciels libres: Bercy, un ministère exemplaire?"
date: 2013-07-17
href: http://www.zdnet.fr/actualites/logiciels-libres-bercy-un-ministere-exemplaire-39792499.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
---

> Interrogé, comme tous les autres ministères, sur ses investissements en logiciels libres et propriétaires, Bercy revendique sa «longue expérience des logiciels libres» et évalue à près de 23 millions ses dépenses de support entre 2008 et 2012.
