---
site: ZDNet
title: "Amnésies: les engagements de Hollande, Hamon et la vente liée"
author: Thierry Noisette
date: 2013-07-30
href: http://www.zdnet.fr/actualites/amnesies-les-engagements-de-hollande-hamon-et-la-vente-liee-39792863.htm
tags:
- Institutions
- Vente liée
- Associations
---

> L'Aful publie un communiqué documenté et cinglant sur les propos de Benoît Hamon contre un amendement anti-vente liée. En avril 2012, le candidat François Hollande s'engageait contre la vente liée. Le changement, c'est de changer d'avis?
