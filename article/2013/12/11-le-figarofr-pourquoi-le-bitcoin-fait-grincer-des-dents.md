---
site: Le Figaro.fr
title: "Pourquoi le bitcoin fait grincer des dents"
author: Guillaume Errard
date: 2013-12-11
href: http://www.lefigaro.fr/secteur/high-tech/2013/12/11/01007-20131211ARTFIG00469-pourquoi-le-bitcoin-fait-grincer-des-dents.php
tags:
- Économie
- Innovation
- International
---

> La banque d'affaires américaine, JP Morgan, a déposé un brevet pour lancer sa propre monnaie virtuelle. De son côté, Apple a bloqué plusieurs applications utilisant le bitcoin.
