---
site: InformatiqueNews.fr
title: "Etats des lieux des besoins en compétence Open Source"
date: 2013-12-11
href: http://www.informatiquenews.fr/etats-des-lieux-des-besoins-en-competence-open-source-7787
tags:
- Entreprise
- Sensibilisation
- Éducation
- Innovation
- Standards
---

> Dans un rapport qu’il vient de publier, l’observatoire Paritaire OPIIEC évalue les besoins des entreprises et des fournisseurs en technologie open source et dresse un bilan des filières de formation
