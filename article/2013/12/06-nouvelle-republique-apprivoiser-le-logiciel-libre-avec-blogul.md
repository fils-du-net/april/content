---
site: Nouvelle République
title: "Apprivoiser le logiciel libre avec Blogul"
author: Catherine Simon
date: 2013-12-06
href: http://www.lanouvellerepublique.fr/Loir-et-Cher/Loisirs/24H/n/Contenus/Articles/2013/12/06/Apprivoiser-le-logiciel-libre-avec-Blogul-1713635
tags:
- Logiciels privateurs
- Économie
- Sensibilisation
- Associations
---

> L'idée, c'est d'aider les gens à passer le cap du " Je ne vais pas savoir faire " pour leur permettre de découvrir l'univers du libre. Rien de licencieux dans les propos de Patrice Lépissier, tout récent président de la toute nouvelle association Blogul, le «groupe d'utilisateurs du libre» de Blois!
