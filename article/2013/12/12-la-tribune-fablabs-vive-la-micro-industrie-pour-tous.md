---
site: La Tribune
title: "FabLabs: vive la micro-industrie pour tous!"
author: Erick Haehnsen
date: 2013-12-12
href: http://www.latribune.fr/technos-medias/electronique/20131212trib000800696/fablabs-vive-la-micro-industrie-pour-tous-.html
tags:
- Partage du savoir
- Matériel libre
- Sensibilisation
- Éducation
- Innovation
---

> Les ateliers de fabrication numérique rendent accessibles au plus grand nombre des machines de prototypage rapide. Ce phénomène mondial développe une culture du concevoir global et du produire local. Reste que les FabLabs cherchent leurs modèles économiques.
