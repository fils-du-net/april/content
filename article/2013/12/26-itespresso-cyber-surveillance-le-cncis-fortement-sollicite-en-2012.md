---
site: ITespresso
title: "Cyber-surveillance: le CNCIS fortement sollicité en 2012"
author: Clément Bohic
date: 2013-12-26
href: http://www.itespresso.fr/cyber-surveillance-le-cncis-fortement-sollicite-par-le-renseignement-francais-71136.html
tags:
- Internet
- Institutions
- Vie privée
---

> Le dernier bilan d’activité de la Commission nationale de contrôle des interceptions de sécurité (CNICS) révèle que l’écosystème français du renseignement a formulé, en 2012, près de 200 000 demandes d’interception de données de connexion.
