---
site: Le Monde Informatique
title: "Le gouvernement allemand s'implique dans l'Open Source et la sécurité"
author: Oscar Barthe
date: 2013-12-18
href: http://www.lemondeinformatique.fr/actualites/lire-le-gouvernement-allemand-s-implique-dans-l-open-source-et-la-securite-56026.html
tags:
- Administration
- Interopérabilité
- Institutions
- Associations
- Informatique-deloyale
- International
---

> L'Open Source et la sécurité informatique font partie des priorités politiques du nouveau gouvernement allemand. La coalition formée en début de semaine par le CDU, le CSU et le SPD a signé un accord visant à développer le premier et à renforcer le second.
