---
site: RTBF Info
title: "Le Parlement européen incité à se tourner vers le logiciel open source"
author: Patrick Bartholomé
date: 2013-12-20
href: http://www.rtbf.be/info/medias/detail_le-parlement-europeen-incite-a-se-tourner-vers-le-logiciel-open-source?id=8159355
tags:
- Logiciels privateurs
- Institutions
- Informatique-deloyale
- Europe
- Vie privée
---

> Le Groupe des Verts/Alliance libre européenne a écrit au président du Parlement européen, Martin Schulz, une lettre l'exhortant à faire usage de logiciels libres et de standards ouverts en vertu d’une des Règles de procédure de l'institution qui indique qu’elle doit «s'assurer que ses activités sont menées dans une transparence absolue».
