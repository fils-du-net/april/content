---
site: Creanum
title: "L'architecture Open Source avec Paperhouses"
author: Louis Adam
date: 2013-12-09
href: http://www.creanum.fr/news/toutes-les-news/id/1951/l-architecture-open-source-avec-paperhouses.aspx
tags:
- Internet
- Partage du savoir
---

> L'architecte Joana Pacheco tente d'imposer le concept de l'open source au monde de l'architecture avec son site Paperhouses. Elle a rassemblé une douzaine d'architecte et propose aux visiteurs du site de télécharger gratuitement les plans d'une maison complète. Reste ensuite à la construire.
