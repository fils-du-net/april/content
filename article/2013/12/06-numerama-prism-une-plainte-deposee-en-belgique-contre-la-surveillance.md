---
site: Numerama
title: "PRISM: une plainte déposée en Belgique contre la surveillance"
author: Julien L.
date: 2013-12-06
href: http://www.numerama.com/magazine/27740-prism-une-plainte-deposee-en-belgique-contre-la-surveillance.html
tags:
- Institutions
- Associations
- Informatique-deloyale
- International
- Vie privée
---

> En Belgique, deux ONG consacrées à la défense des libertés individuelles ont initié une action en justice contre la surveillance de masse, révélée grâce à Edward Snowden. Cette plainte faite suite à celle déposée cet été en France, et qui est toujours en cours.
