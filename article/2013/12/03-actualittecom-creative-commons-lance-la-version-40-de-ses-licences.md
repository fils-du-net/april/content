---
site: ActuaLitté.com
title: "Creative Commons lance la version 4.0 de ses licences"
author: Marie Lebert
date: 2013-12-03
href: http://www.actualitte.com/legislation/creative-commons-lance-la-version-4-0-de-ses-licences-46714.htm
tags:
- Internet
- Partage du savoir
- Droit d'auteur
- Licenses
- International
---

> Fondé en 2001 à l'initiative de Lawrence (Larry) Lessig, juriste et ardent défenseur de la créativité sur l'internet, Creative Commons (CC) veut favoriser la diffusion des œuvres numériques - et leur réutilisation - tout en protégeant le droit d'auteur. Douze ans plus tard, la version 4.0 des licences CC propose d'emblée des licences internationales valables dans tous les pays.
