---
site: canoe
title: "Montréal: l'avenue des logiciels libres explorée"
date: 2013-12-19
href: http://fr.canoe.ca/techno/materiel/archives/2013/12/20131219-112332.html
tags:
- Logiciels privateurs
- Administration
---

> La Ville de Montréal, qui procédera prochainement à l'analyse de ses besoins bureautiques, ne ferme pas la porte à l'utilisation des logiciels libres, logiciels dont l'utilisation, la modification et la diffusion sont permises sans restriction.
