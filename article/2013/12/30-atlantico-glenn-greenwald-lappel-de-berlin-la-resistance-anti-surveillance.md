---
site: atlantico
title: "Glenn Greenwald, l'appel de Berlin à la résistance anti-surveillance"
date: 2013-12-30
href: http://www.atlantico.fr/rdv/minute-tech/glenn-greenwald-appel-berlin-resistance-anti-surveillance-louise-hoffmann-939292.html
tags:
- Internet
- Informatique-deloyale
- International
- Vie privée
---

> Le journaliste du Guardian qui a diffusé des documents d'Edward Snowden s'est exprimé lors du Congrès annuel du Chaos Computer Club et a appelé à lutter contre la surveillance de masse.
