---
site: Numerama
title: "Vie privée: les promesses de Microsoft n'ont pas de sens, selon la FSF"
author: Julien L.
date: 2013-12-07
href: http://www.numerama.com/magazine/27744-vie-privee-les-promesses-de-microsoft-n-ont-pas-de-sens-selon-la-fsf.html
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Associations
- Informatique-deloyale
- Vie privée
---

> Touché directement par le scandale de la NSA, Microsoft a promis de multiples mesures pour renforcer la confidentialité de ses services. Mais pour la Free Software Foundation, ces promesses n'ont pas de sens puisqu'elles ne peuvent pas être effectivement vérifiées.
