---
site: Silicon.fr
title: "Espionnage de masse: le problème réside aussi dans le logiciel..."
author: David Feugey
date: 2013-12-12
href: http://www.silicon.fr/espionnage-de-masse-le-probleme-reside-aussi-dans-le-logiciel-explique-la-fsf-91450.html
tags:
- Entreprise
- Institutions
- Associations
- Informatique-deloyale
- Vie privée
---

> La FSF réagit assez vivement aux déclarations faites par les grands noms du monde IT pour contrer les opérations de collecte illégale de données. Morceaux choisis…
