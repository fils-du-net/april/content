---
site: "Marches-Publics-PME"
title: "Marchés publics et logiciels libres dans l'Isère: Des pratiques à améliorer"
date: 2013-12-16
href: http://www.marchespublicspme.com/actualite/4125/
tags:
- Administration
- Associations
- Marchés publics
---

> Dans le département de l'Isère, le préfet est intervenu pour sensibiliser les acteurs du secteur public au respect des règles de concurrence en matière de marchés publics. Il a tenu plus particulièrement à attirer l'attention des institutionnels de son département sur les récriminations faites par l'association régionale représentant les professionnels du logiciel libre (PLOSS Rhône-Alpes).
