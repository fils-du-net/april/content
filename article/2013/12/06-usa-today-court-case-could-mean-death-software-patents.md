---
site: USA Today
title: "Court case could mean 'death' of software patents"
author: The Associated Press
date: 2013-12-06
href: http://www.usatoday.com/story/money/business/2013/12/06/court-case-could-mean-death-of-software-patents/3894877
tags:
- Institutions
- Brevets logiciels
- International
- English
---

> (D'après un juge fédéral, la décision de la Cour Suprême dans l'affaire Alice pourrait invalider les brevets de haute technologie) Federal judge says high court's ruling in Alice case could invalidate high-tech patents.
