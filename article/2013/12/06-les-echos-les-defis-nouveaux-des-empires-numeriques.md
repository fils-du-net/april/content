---
site: Les Echos
title: "Les défis nouveaux des empires numériques"
author: Denis Ettighoffer
date: 2013-12-06
href: http://blogs.lesechos.fr/intelligence-economique/-a13893.html
tags:
- Internet
- Institutions
- Informatique-deloyale
- International
---

> Dans le cyberespace se livre un combat silencieux, mais stratégique: sur quel logiciel nos salariés, nos enfants, vont-ils se former, sur quels réseaux, quels data-centers, nos entreprises vont-elles gérer leurs affaires, nos industries culturelles se développer, nos armées et notre diplomatie s’affirmer dans le cyberespace. Quels opérateurs? Quels Editeurs? Microsoft? Androïd? OSx, ou des logiciels libres?
