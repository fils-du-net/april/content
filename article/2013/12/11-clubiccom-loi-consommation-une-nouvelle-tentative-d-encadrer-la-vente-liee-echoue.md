---
site: clubic.com
title: "Loi consommation: une nouvelle tentative d’encadrer la vente liée échoue"
author: Olivier Robillart
date: 2013-12-11
href: http://pro.clubic.com/legislation-loi-internet/actualite-606492-loi-conso-vente-liee.html
tags:
- April
- Institutions
- Vente liée
- Associations
---

> Repoussée en septembre dernier, la vente liée est revenue dans les débats parlementaires suite au dépôt d'un amendement. L'Assemblée nationale a refusé à nouveau toute tentative d'encadrer la pratique.
