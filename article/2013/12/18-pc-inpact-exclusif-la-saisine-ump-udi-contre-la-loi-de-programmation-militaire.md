---
site: PC INpact
title: "Exclusif: la saisine UMP-UDI contre la loi de programmation militaire"
author: Marc Rees
date: 2013-12-18
href: http://www.pcinpact.com/news/85012-exclusif-saisine-ump-udi-contre-loi-programmation-militaire.htm
tags:
- Internet
- Institutions
- Informatique-deloyale
- Vie privée
---

> PC INpact dévoile en exclusivité le contenu de la saisine UMP/UDI visant la loi de programmation militaire. «Cet article méconnaît en effet plusieurs principes constitutionnels» expliquent les auteurs de la saisine. «Il est bien sûr nécessaire de donner les moyens aux services de renseignement de lutter efficacement contre le terrorisme. Mais cette nécessité, parce qu’elle implique des techniques attentatoires aux libertés individuelles, doit être proportionnée et encadrée» tempèrent-ils.
