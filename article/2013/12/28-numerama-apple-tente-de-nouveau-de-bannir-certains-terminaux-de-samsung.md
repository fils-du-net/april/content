---
site: Numerama
title: "Apple tente de nouveau de bannir certains terminaux de Samsung"
author: Julien L.
date: 2013-12-28
href: http://www.numerama.com/magazine/27911-apple-tente-de-nouveau-de-bannir-certains-terminaux-de-samsung.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> L'affrontement judiciaire entre Apple et Samsung rebondit. Aux États-Unis, la société californienne a demandé de nouveau à la justice de bannir certains terminaux du groupe sud-coréen, qui ne sont plus commercialisés aujourd'hui. Mais pour Apple, l'objectif est de préparer le terrain pour mieux combattre les produits actuels et futurs de son rival.
