---
site: Le Monde Informatique
title: "Cultura joue l'Open Source pour distribuer les livres électroniques"
author: Bertand Lemaire
date: 2013-12-06
href: http://www.lemondeinformatique.fr/actualites/lire-cultura-joue-l-open-source-pour-distribuer-les-livres-electroniques-55904.html
tags:
- Entreprise
- Internet
- DRM
---

> La chaîne de libraires et de distribution d'autres produits culturels a opté pour une plate-forme Open Source pour distribuer ses livres électroniques sur tous les environnements.
