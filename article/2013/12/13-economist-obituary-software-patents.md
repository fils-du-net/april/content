---
site: The Economist
title: "Obituary for software patents"
author: N.V.
date: 2013-12-13
href: http://www.economist.com/blogs/babbage/2013/12/difference-engine-0
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
- English
---

> AT LAST, it seems, something is to be done about the dysfunctional way America’s patent system works. Two encouraging events over the past week suggest the patent reformers are finally being heard.
