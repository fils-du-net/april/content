---
site: Le Point
title: "Données personnelles: la loi qui fâche Jacques Attali"
date: 2013-12-22
href: http://www.lepoint.fr/societe/donnees-personnelles-la-loi-qui-fache-jacques-attali-22-12-2013-1773516_23.php
tags:
- Administration
- Institutions
- Associations
- Vie privée
---

> L'économiste a jugé "ahurissant" qu'ait été voté l'article de la loi de programmation militaire permettant au renseignement d'accéder aux communications.
