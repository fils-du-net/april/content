---
site: PC INpact
title: "Munich enfin Libre"
author: Nil Sanyas
date: 2013-12-14
href: http://www.pcinpact.com/news/84932-munich-enfin-libre.htm
tags:
- Administration
- Économie
- Promotion
---

> Initié il y a un peu plus de dix ans, le passage de Windows vers Linux et l'open source dans l'administration de Munich est désormais quasi terminé. 14 800 machines sont ainsi concernées sur un parc d'un peu plus 15 500 PC.
