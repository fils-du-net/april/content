---
site: Numerama
title: "Contre la NSA, l'Europe déroule ses solutions"
author: Julien L.
date: 2013-12-20
href: http://www.numerama.com/magazine/27848-contre-la-nsa-l-europe-deroule-ses-solutions.html
tags:
- Institutions
- Informatique-deloyale
- Europe
- International
- Vie privée
---

> La commission des libertés civiles (LIBE) du Parlement européen a présenté ses premières conclusions sur la surveillance de masse menée par la NSA. Mercredi, elle a proposé plusieurs solutions pour limiter l'espionnage électronique et renforcer la vie privée des citoyens européens.
