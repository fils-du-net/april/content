---
site: Numerama
title: "Le calendrier 2014 du libre partage, par Framasoft et Nina Paley"
author: Julien L.
date: 2013-12-18
href: http://www.numerama.com/magazine/27839-le-calendrier-2014-du-libre-partage-par-framasoft-et-nina-paley.html
tags:
- Sensibilisation
- Associations
- Droit d'auteur
---

> Après le calendrier de l'Avent du domaine public, voici le calendrier 2014 du libre partage. Proposé par Framasoft, il est illustré par quelques comic strips de la dessinatrice américaine Nina Paley sur le thème de la propriété intellectuelle et du partage.
