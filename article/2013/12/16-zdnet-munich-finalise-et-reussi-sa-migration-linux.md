---
site: ZDNet
title: "Munich a finalisé et réussi sa migration Linux"
date: 2013-12-16
href: http://www.zdnet.fr/actualites/munich-a-finalise-et-reussi-sa-migration-linux-39796349.htm
tags:
- Administration
- Promotion
---

> Le projet LiMux de la ville allemande de Munich est désormais officiellement terminé et un succès avec l’utilisation sur 14.000 postes de Linux (Ubuntu), mais aussi de Thunderbird, Firefox et OpenOffice auprès de 15.000 utilisateurs. C'est moins que les 72.000 PC de le gendarmerie nationale.
