---
site: PC INpact
title: "Une députée veut que les logiciels commandés par l’État soient libres"
author: Xavier Berne
date: 2013-12-03
href: http://www.pcinpact.com/news/84695-une-deputee-veut-que-logiciels-commandes-par-l-etat-soient-libres.htm
tags:
- Institutions
- Open Data
---

> Et si les logiciels ou toute autre œuvre ou donnée immatérielle (travaux de recherche, etc.) produite grâce aux deniers publics étaient publiés sous licence libre par l’État ? Telle est la proposition d’une députée à l’attention du gouvernement.
