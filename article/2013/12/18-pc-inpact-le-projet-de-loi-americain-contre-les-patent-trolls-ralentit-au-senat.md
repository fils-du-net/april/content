---
site: PC INpact
title: "Le projet de loi américain contre les patent trolls ralentit au Sénat"
author: Vincent Hermann
date: 2013-12-18
href: http://www.pcinpact.com/news/85015-le-projet-loi-americain-contre-patent-trolls-ralentit-au-senat.htm
tags:
- Institutions
- Brevets logiciels
- International
---

> Depuis que l’administration Obama a affiché sa volonté de s’attaquer à la puissance des patent trolls aux États-Unis, un projet de loi a été mis sur pied et soutenu par des représentants des deux camps. Cependant, maintenant que le texte entre au Sénat, plusieurs voix se font entendre pour faire ralentir la cadence. Explications.
