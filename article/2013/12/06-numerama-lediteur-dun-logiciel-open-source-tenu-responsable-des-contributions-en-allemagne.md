---
site: Numerama
title: "L'éditeur d'un logiciel open-source tenu responsable des contributions, en Allemagne"
author: Julien L.
date: 2013-12-06
href: http://www.numerama.com/magazine/27738-l-editeur-d-un-logiciel-open-source-tenu-responsable-des-contributions-en-allemagne.html
tags:
- Entreprise
- Internet
- Institutions
- DRM
- International
---

> En Allemagne, un éditeur d'un logiciel open-source a été tenu pour responsable des contributions de tiers, au motif qu'il édite la version finale du programme.
