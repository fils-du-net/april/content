---
site: ZDNet
title: "LPM: la Cnil épingle une \"atteinte disproportionnée au respect de la vie privée\""
date: 2013-12-20
href: http://www.zdnet.fr/actualites/lpm-la-cnil-epingle-une-atteinte-disproportionnee-au-respect-de-la-vie-privee-39796514.htm
tags:
- Internet
- Institutions
- Vie privée
---

> La Commission nationale informatique et libertés revient sur l'article 13 de la loi de programmation militaire qui vient d'être promulguée par le gouvernement.
