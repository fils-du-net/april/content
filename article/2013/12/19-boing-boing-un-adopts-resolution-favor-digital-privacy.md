---
site: Boing Boing
title: "UN adopts resolution in favor of digital privacy"
author: Cory Doctorow
date: 2013-12-19
href: http://boingboing.net/2013/12/19/un-adopts-resolution-in-favor.html
tags:
- Internet
- Institutions
- International
- Vie privée
---

> (L'assemblée générale des Nations Unies a adopté à l'unanimité une résolution nommée "Le droit à la vie privée à l'ère digitale") The UN General Assembly has unanimously adopted a resolution called "The right to privacy in the digital age," introduced by Germany and Brazil. The resolution sets the stage for the adoption of broader privacy protection in UN treaties and resolution. The Electronic Frontier Foundation has written a set of (excellent) "People's Principles" (sign on here) for future work on digital privacy in the world.
