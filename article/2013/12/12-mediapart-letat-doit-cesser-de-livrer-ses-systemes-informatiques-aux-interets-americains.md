---
site: Mediapart
title: "L'Etat doit cesser de livrer ses systèmes informatiques aux intérêts américains"
author: Lionel Allorge
date: 2013-12-12
href: http://blogs.mediapart.fr/edition/les-invites-de-mediapart/article/121213/letat-doit-cesser-de-livrer-ses-systemes-informatiques-aux-interets-america
tags:
- Administration
- April
- Institutions
- Éducation
- Informatique-deloyale
- Promotion
---

> Dans une lettre ouverte au président de la République, Lionel Allorge, président de l'April (association de défense du logiciel libre), s'alarme de l'espionnage des institutions les plus sensibles grâce aux contrats d'exclusivité passés avec des entreprises internationales, notamment le contrat entre le ministère de la défense et la société Microsoft. Des solutions existent, comme celle choisie par la gendarmerie, rappelle-t-il.
