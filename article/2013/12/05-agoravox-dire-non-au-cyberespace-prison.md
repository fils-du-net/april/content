---
site: AgoraVox
title: "Dire non au cyberespace-prison"
author: Acid World
date: 2013-12-05
href: http://www.agoravox.fr/tribune-libre/article/dire-non-au-cyberespace-prison-144645
tags:
- Entreprise
- Internet
- Sensibilisation
- Informatique-deloyale
- Philosophie GNU
- Informatique en nuage
- Vie privée
---

> Comment s'évader des systèmes de contrôle pour un internet neutre et ouvert? Même la jeune génération, est maintenue dans le simple rôle de consommateur d'un internet centralisé.
