---
site: 01netPro.
title: "Cartographie des compétences open source en France"
author: Xavier Biseul
date: 2013-12-13
href: http://pro.01net.com/editorial/610330/cartographie-des-competences-open-source-en-france
tags:
- Entreprise
- Sensibilisation
- Éducation
- Innovation
---

> L’observatoire des métiers de la branche Syntec-Cinov vient de dresser un état des lieux complet sur les compétences en logiciels libres recherchées par les DSI comme par les prestataires.
