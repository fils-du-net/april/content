---
site: Blogs LeMonde.fr
title: "Open-data: de nouvelles données gouvernementales mises à disposition gratuitement"
author: Alexandre Léchenet
date: 2013-12-18
href: http://data.blog.lemonde.fr/2013/12/18/data-gouv-fr-nouvelle-formule-nouvelles-ambitions
tags:
- Internet
- Administration
- Associations
- Open Data
---

> «C'est décidément Noël», se félicite Regards citoyens, association militant pour l'ouverture de l'information publique. Le Comité interministériel pour la modernisation de l'action publique (Cimap) a annoncé le 18 décembre et le premier ministre a présenté, le même jour, la nouvelle version de data.gouv.fr, la plateforme gouvernemental de partage des données publiques.
