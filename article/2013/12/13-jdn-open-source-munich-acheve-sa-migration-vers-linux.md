---
site: JDN
title: "Open Source: Munich achève sa migration vers Linux"
author: Dominique Filippone
date: 2013-12-13
href: http://www.journaldunet.com/solutions/dsi/munich-migration-vers-linux-1213.shtml
tags:
- Administration
- Économie
- Promotion
- International
---

> La ville allemande a finalisé sa migration vers un OS open source dérivé de Linux. Il a été déployé sur 14 800 postes de travail. Elle encourage ses administrés à en faire de même.
