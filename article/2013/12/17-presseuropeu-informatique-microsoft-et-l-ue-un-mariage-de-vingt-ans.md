---
site: Presseurop.eu
title: "Informatique: Microsoft et l’UE, un mariage de vingt ans"
author: Jérôme Hourdeaux
date: 2013-12-17
href: http://www.presseurop.eu/fr/content/article/4410151-microsoft-et-l-ue-un-mariage-de-vingt-ans
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Institutions
- Associations
- Marchés publics
- Europe
---

> Partisanes de la concurrence et adversaires résolues des cartels et des monopoles, les institutions européennes sont pourtant liées par des contrats opaques au géant américain du logiciel. Le passage à des logiciels “open source”, qu’elles encouragent pourtant, serait trop compliqué et trop coûteux. Extraits.
