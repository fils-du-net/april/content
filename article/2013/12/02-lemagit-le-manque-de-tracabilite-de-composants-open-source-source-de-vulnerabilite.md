---
site: LeMagIT
title: "Le manque de traçabilité de composants Open Source, source de vulnérabilité"
author: Cyrille Chausson
date: 2013-12-02
href: http://www.lemagit.fr/actualites/2240210190/Le-manque-de-tracabilite-de-composants-Open-Source-source-de-vulnerabilite
tags:
- Entreprise
- Logiciels privateurs
---

> En dépit d’une sécurité considérée comme renforcée par les mécanismes du développement communautaire et par la transparence du code, les composants Open Source sont à l'origine de certaines vulnérabilités de logiciels commerciaux, révèle la dernière étude de l’éditeur White Source, qui développe des outils de suivi de licences Open Source et de conformité.
