---
site: Framablog
title: "Sortie du livre Utopie du logiciel libre - Interview de Sébastien Broca"
author: Alexis Kauffmann
date: 2013-12-02
href: http://www.framablog.org/index.php/post/2013/12/02/utopie-du-logiciel-libre-sebastien-broca
tags:
- Économie
- Partage du savoir
- Licenses
- Philosophie GNU
- Promotion
- Sciences
---

> Bonjour Alexis. Je suis sociologue, rattaché à la Sorbonne (Université Paris 1). Je viens de publier un livre sur le Libre, dans lequel je raconte l’histoire de l’extension du logiciel libre hors du domaine informatique. J’essaie d’y montrer comment on peut à travers cette histoire éclairer certaines questions liées au travail, à la technique ou à la connaissance.
