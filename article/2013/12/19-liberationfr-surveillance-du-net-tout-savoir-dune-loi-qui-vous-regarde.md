---
site: Libération.fr
title: "Surveillance du Net: tout savoir d'une loi qui vous regarde"
author: Sylvain MOUILLARD
date: 2013-12-19
href: http://ecrans.liberation.fr/ecrans/2013/12/19/surveillance-du-net-tout-savoir-d-une-loi-qui-vous-regarde_967476
tags:
- Entreprise
- Internet
- Institutions
- Informatique-deloyale
- Vie privée
---

> Plusieurs députés UMP souhaitaient une saisine du Conseil constitutionnel à propos d'une disposition contestée de la loi de programmation militaire. Les «sages» ne se pencheront finalement pas sur le texte, promulgué ce jeudi.
