---
site: L'usine Nouvelle
title: "La Loi de programmation militaire promulguée rapidement pour éviter tout recours"
author: Gwénaëlle Barzic, Emile Picy, Leila Abboud, Yves Clarisse
date: 2013-12-19
href: http://www.usinenouvelle.com/article/la-loi-de-programmation-militaire-promulguee-rapidement-pour-eviter-tout-recours.N227615
tags:
- Internet
- Institutions
- Informatique-deloyale
- Vie privée
---

> La loi de programmation militaire a été publiée le 19 décembre au Journal officiel, coupant court au projet de recours de parlementaires qui contestent un de ses articles prévoyant un large accès de la puissance publique aux données sur internet.
