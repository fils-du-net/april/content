---
site: Le Monde.fr
title: "L'heure de la revanche pour les lanceurs d'alerte"
author: Yves Eudes
date: 2013-12-11
href: http://www.lemonde.fr/technologies/article/2013/12/11/l-heure-de-la-revanche-pour-les-lanceurs-d-alerte_3529000_651865.html
tags:
- Internet
- Institutions
- Informatique-deloyale
- Vie privée
---

> Rencontre avec trois hommes qui ont tenté pendant des années d'avertir sur les dangers de la surveillance généralisée, en vain.
