---
site: ZDNet
title: "Article 13 LPM, vente liée, deux jours affligeants du Parlement et du gouvernement"
author: Thierry Noisette
date: 2013-12-10
href: http://www.zdnet.fr/actualites/article-13-lpm-vente-liee-deux-jours-affligeants-du-parlement-et-du-gouvernement-39796227.htm
tags:
- Internet
- April
- Institutions
- Vente liée
- Associations
- Informatique-deloyale
- Vie privée
---

> Vous avez aimé Prism? Vous adorerez la LPM. Les sénateurs ont voté l'extension de la surveillance en ligne, tandis que les députés ont fini d'enterrer une promesse du candidat Hollande sur la vente liée. VDM...
