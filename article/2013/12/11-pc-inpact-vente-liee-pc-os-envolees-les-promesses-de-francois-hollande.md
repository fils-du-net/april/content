---
site: PC INpact
title: "Vente liée PC-OS: envolées, les promesses de François Hollande"
author: Marc Rees
date: 2013-12-11
href: http://www.pcinpact.com/news/84855-vente-liee-pc-os-envolees-promesses-francois-hollande.htm
tags:
- April
- Institutions
- Vente liée
- Associations
---

> Lors des discussions autour du projet de loi sur la Consommation, défendu par Benoit Hamon, les députés ont rejeté un amendement visant à encadrer la vente liée, gommant dans le même temps les promesses de François Hollande.
