---
site: Slate.fr
title: "Leur mission: réparer votre Internet cassé par la surveillance"
author: Amaelle Guiton
date: 2013-11-05
href: http://www.slate.fr/monde/79450/mission-reparer-votre-internet-casse-surveillance
tags:
- Internet
- Institutions
- Sensibilisation
- Associations
- Informatique-deloyale
- Innovation
---

> Tandis que les gouvernements européens sont à leur tour éclaboussés par les révélations d'Edward Snowden, les «petites mains» du réseau, elles, s'attellent à compliquer la tâche des agences de renseignement.
