---
site: Slate.fr
title: "La TVA réduite réservée aux livres numériques sans DRM? Le projet des Verts dont le gouvernement ne veut pas"
author: Cécile Chalancon
date: 2013-11-16
href: http://www.slate.fr/culture/80081/livre-numerique-drm-tva-ebook
tags:
- Économie
- Interopérabilité
- April
- Institutions
- DRM
---

> C'est l'histoire d'un amendement qui aura été adopté un jour... Contre toute attente, l'Assemblée nationale avait adopté le 14 novembre un amendement du groupe écologiste sur la fiscalité du livre numérique. Le texte prévoit que le taux de TVA réduit ne s'applique aux livres numériques que si ces derniers sont «ouverts», c'est-à-dire sans DRM ni format fermé.
