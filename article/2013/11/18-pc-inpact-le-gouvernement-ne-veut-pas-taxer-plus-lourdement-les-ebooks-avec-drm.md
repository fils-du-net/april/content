---
site: PC INpact
title: "Le gouvernement ne veut pas taxer plus lourdement les eBooks avec DRM"
author: Marc Rees
date: 2013-11-18
href: http://www.pcinpact.com/news/84465-le-gouvernement-ne-veut-pas-taxer-plus-lourdement-ebooks-avec-drm.htm
tags:
- April
- Institutions
- DRM
---

> La discrimination du taux de TVA selon la présence ou l’absence de DRM sur un livre numérique (eBook) n’aura duré que quelques heures. Dans le cadre du projet de loi de finances, le gouvernement a fait passer un amendement de suppression pour faire tomber aux oubliettes cette idée défendue par le groupe EELV.
