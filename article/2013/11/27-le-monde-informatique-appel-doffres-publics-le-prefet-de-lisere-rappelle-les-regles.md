---
site: Le Monde Informatique
title: "Appel d'offres publics, le préfet de l'Isère rappelle les régles"
author: Bertrand Lemaire
date: 2013-11-27
href: http://www.lemondeinformatique.fr/actualites/lire-appel-d-offres-publics-le-prefet-de-l-isere-rappelle-les-regles-55807.html
tags:
- Administration
- Marchés publics
---

> Secteur public/privé: Les marchés publics reposent sur la mise en concurrence. Un utile rappel à la loi vient d'être réalisé par le préfet de l'Isère.
