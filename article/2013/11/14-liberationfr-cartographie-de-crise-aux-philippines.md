---
site: Libération.fr
title: "Cartographie de crise aux Philippines"
author: Camille Gévaudan
date: 2013-11-14
href: http://ecrans.liberation.fr/ecrans/2013/11/14/cartographie-de-crise-aux-philippines_946920
tags:
- Internet
- Partage du savoir
- Associations
- Droit d'auteur
- Licenses
- International
---

> Plus de 700 internautes travaillent à dessiner une carte libre et détaillée des villes touchées par le typhon Haiyan, pour faciliter l'intervention de l'aide humanitaire.
