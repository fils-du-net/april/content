---
site: Numerama
title: "Des DRM intrusifs dans la voiture électrique Renault ZOE?"
author: Guillaume Champeau
date: 2013-11-18
href: http://www.numerama.com/magazine/27529-des-drm-intrusifs-dans-la-voiture-electrique-renault-zoe.html
tags:
- Entreprise
- DRM
---

> Le contrat de location de la batterie des Renault ZOE prévoirait que le constructeur puisse retirer le droit à recharger la batterie, en cas de défaut de paiement ou de résiliation. Un contrôle que Renault se défend de pouvoir exercer en pratique.
