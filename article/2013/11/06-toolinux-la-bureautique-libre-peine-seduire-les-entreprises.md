---
site: toolinux
title: "La bureautique libre peine à séduire les entreprises"
author: Philippe SCOFFONI
date: 2013-11-06
href: http://toolinux.fr/La-bureautique-libre-peine-a
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Sensibilisation
- Innovation
---

> Une récente étude montre un recul de l’utilisation des suites bureautiques libres dans les entreprises. Un recul très marqué avec le passage de 13% à 5% seulement en l’espace de deux ans. Que s’est-il passé ces deux dernières années qui ait pu provoquer un tel recul? La faute aux suites bureautiques ou la montée en puissance d’autres formes de bureautique plus adaptées aux besoins des utilisateurs?
