---
site: PC INpact
title: "Le CNNum torpille le blocage administratif des sites voulu par le PS"
author: Marc Rees
date: 2013-11-22
href: http://www.pcinpact.com/news/84575-le-cnnum-torpille-blocage-administratif-sites-voulu-par-ps.htm
tags:
- Internet
- Institutions
- Neutralité du Net
---

> Le filtrage administratif organisé par l’article 1 de la proposition de loi renforçant la lutte contre le système prostitutionnel vient de subir de lourdes critiques par le Conseil National du Numérique qui s’est autosaisi à la demande de la députée UMP Laure de la Raudière.
