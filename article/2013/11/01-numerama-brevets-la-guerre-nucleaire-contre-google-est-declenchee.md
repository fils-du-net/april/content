---
site: Numerama
title: "Brevets: la guerre nucléaire contre Google est déclenchée"
author: Guillaume Champeau
date: 2013-11-01
href: http://www.numerama.com/magazine/27388-brevets-la-guerre-nucleaire-contre-google-est-declenchee.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
---

> Alors que la guerre des brevets semblait avoir baissé de rythme, elle est aujourd'hui relancée à un degré inédit avec la plainte déposée contre Google et sept fabricants de téléphones sous Android, dont Samsung, par un consortium formé par Microsoft, Apple, BlackBerry, Ericsson et Sony.
