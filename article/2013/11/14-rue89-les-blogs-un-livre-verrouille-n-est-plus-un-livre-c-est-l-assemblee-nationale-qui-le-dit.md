---
site: Rue89 Les blogs
title: "Un livre verrouillé n’est plus un livre, c’est l’Assemblée nationale qui le dit"
author: Pierre-Carl Langlais
date: 2013-11-14
href: http://blogs.rue89.com/les-coulisses-de-wikipedia/2013/11/14/un-livre-verrouille-nest-plus-un-livre-cest-lassemblee-nationale-qui-le-dit-231677
tags:
- Économie
- Interopérabilité
- Institutions
- DRM
---

> L’assemblée nationale vient de valider un amendement du groupe écologiste qui retire le statut de «livre» aux publications numériques protégées par des DRM.
