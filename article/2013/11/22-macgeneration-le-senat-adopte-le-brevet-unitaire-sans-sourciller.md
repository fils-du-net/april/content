---
site: MacGeneration
title: "Le Sénat adopte le brevet unitaire sans sourciller"
author: Pierrick Aubert
date: 2013-11-22
href: http://www.macg.co/ailleurs/2013/11/le-senat-adopte-le-brevet-unitaire-sans-sourciller-78174
tags:
- April
- Institutions
- Brevets logiciels
- Innovation
- Promotion
- Europe
---

> Le Sénat a adopté hier le projet de loi ouvrant la porte à une juridiction européenne unifiée en matière de brevet.
