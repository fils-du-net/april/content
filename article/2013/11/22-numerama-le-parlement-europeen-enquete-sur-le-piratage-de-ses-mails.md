---
site: Numerama
title: "Le Parlement européen enquête sur le piratage de ses mails"
author: Julien L.
date: 2013-11-22
href: http://www.numerama.com/magazine/27585-le-parlement-europeen-enquete-sur-le-piratage-de-ses-mails.html
tags:
- Logiciels privateurs
- April
- Institutions
- Informatique-deloyale
- Europe
---

> Suite aux révélations de Mediapart, le Parlement européen enquête sur le piratage de son service de messagerie interne. De son côté, l'association April souhaite que cet incident soit l'occasion pour l'Europe de repenser certains choix technologiques.
