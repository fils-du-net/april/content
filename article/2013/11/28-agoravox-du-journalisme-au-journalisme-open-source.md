---
site: AgoraVox
title: "Du journalisme au journalisme open source"
author: Vincent Verschoore
date: 2013-11-28
href: http://www.agoravox.fr/actualites/medias/article/du-journalisme-au-journalisme-open-144380
tags:
- Internet
- Partage du savoir
---

> La semaine dernière se tenait la conférence FOSSa (Free Open Source Software for Academia), organisée par le centre de recherche INRIA dans le complexe de Euratechnologies à Lille.
