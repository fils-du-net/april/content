---
site: basta!
title: "Ces agriculteurs et ingénieurs qui veulent libérer les machines"
author: Agnès Rousseaux
date: 2013-11-12
href: http://www.bastamag.net/article3450.html
tags:
- Économie
- Partage du savoir
- Matériel libre
- Innovation
---

> Ils fabriquent des machines libres de droits, sans brevet. Des engins à construire soi-même, sorte de meccano géant, écologique et à moindre coût. Pour bâtir des maisons, produire de l’énergie, faire cuire des aliments, extraire des matériaux ou cultiver la terre. De quoi construire un village. Ou une civilisation. Leur objectif: éditer plans et modes d’emploi, construire des prototypes, expérimenter, partager et diffuser à tous, pour faire vivre cette révolution industrielle d’un nouveau genre. Des États-Unis à l’Isère, bienvenue dans l’univers des pionniers de «l’écologie open source».
