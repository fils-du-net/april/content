---
site: La gazette.fr
title: "Le fouillis des licences open data s'éclaircit"
author: S. Blanc
date: 2013-11-25
href: http://www.lagazettedescommunes.com/208893/le-fouilli-des-licences-open-data-seclaircit-fiche-pratique
tags:
- Internet
- Administration
- Associations
- Licenses
- Open Data
---

> En deux ans, le paysage des licences compatibles avec un projet open data a bien évolué. On observe un resserrement sur deux licences libres, celle d’Etalab, et la licence ODbL. Le choix s’opère surtout en fonction de l’orientation politique et sociétale donnée à l’ouverture des données publiques.
