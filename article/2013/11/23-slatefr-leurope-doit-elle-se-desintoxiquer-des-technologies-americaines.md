---
site: Slate.fr
title: "L'Europe doit-elle se désintoxiquer des technologies américaines?"
author: Amaelle Guiton
date: 2013-11-23
href: http://www.slate.fr/monde/80327/europe-doit-se-desintoxiquer-technologies-americaines
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Administration
- April
- Institutions
- Associations
- Informatique-deloyale
- Europe
---

> Le piratage du courrier électronique de parlementaires européens en pleine négociation sur le Partenariat transatlantique de commerce et d'investissement, pose la question de la souveraineté technologique.
