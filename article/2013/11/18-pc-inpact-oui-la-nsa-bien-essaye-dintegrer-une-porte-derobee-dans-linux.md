---
site: PC INpact
title: "Oui, la NSA a bien essayé d'intégrer une porte dérobée dans Linux"
author: Vincent Hermann
date: 2013-11-18
href: http://www.pcinpact.com/news/84466-oui-nsa-a-bien-essaye-dintegrer-porte-derobee-dans-linux.htm
tags:
- Institutions
- Informatique-deloyale
- International
---

> La NSA a bien demandé à Linus Torvalds s’il était possible d’introduire au moins une porte dérobée au sein de Linux. Alors même que le père du noyau avait donné une réponse ambiguë il y a plus d’un mois, son père, député européen a répondu de manière beaucoup plus claire.
