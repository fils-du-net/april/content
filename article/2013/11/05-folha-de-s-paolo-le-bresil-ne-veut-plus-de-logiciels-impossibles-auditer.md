---
site: Folha de S. Paolo
title: "Le Brésil ne veut plus de logiciels impossibles à auditer"
author: Natuza Nery et Julia Borba
date: 2013-11-05
href: http://www.framablog.org/index.php/post/2013/11/07/bresil-snowden-logiciel-libre
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Institutions
- Informatique-deloyale
- International
---

> A partir de l’année prochaine, le gouvernement (brésilien) n’achètera plus d’ordinateurs ou de logiciels qui ne peuvent être pleinement audités par les pouvoirs publics. La directive a été publiée le 5 novembre dernier dans le journal officiel «Diario official da Uniao».
