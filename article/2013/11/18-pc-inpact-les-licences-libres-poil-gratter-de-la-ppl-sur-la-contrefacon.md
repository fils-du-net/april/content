---
site: PC INpact
title: "Les licences libres, poil à gratter de la PPL sur la contrefaçon"
author: Marc Rees
date: 2013-11-18
href: http://www.pcinpact.com/news/84473-les-licences-libres-poil-a-gratter-proposition-loi-sur-contrefacon.htm
tags:
- April
- Institutions
- Droit d'auteur
- Licenses
---

> La proposition de loi sur la contrefaçon, discutée au Sénat le 20 décembre, va accentuer les peines en matière de contrefaçon. Cependant, Hélène Lipietz et les membres du Groupe écologiste ont déposé un amendement en faveur du logiciel libre. Leur texte veut ainsi punir de la même peine ceux qui limitent les « droits associés » à ce type de licence.
