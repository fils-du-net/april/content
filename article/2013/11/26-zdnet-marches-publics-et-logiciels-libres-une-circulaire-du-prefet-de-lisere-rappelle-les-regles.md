---
site: ZDNet
title: "Marchés publics et logiciels libres: une circulaire du préfet de l'Isère rappelle les règles"
author:  Thierry Noisette
date: 2013-11-26
href: http://www.zdnet.fr/actualites/marches-publics-et-logiciels-libres-une-circulaire-du-prefet-de-l-isere-rappelle-les-regles-39795813.htm
tags:
- Administration
- Économie
- April
- Sensibilisation
- Associations
- Marchés publics
---

> Réagissant aux constats de discriminations répétées de l'association PLOSS Rhône-Alpes, le préfet de l'Isère écrit aux maires et autres décideurs publics pour rappeler le droit en la matière et l'importance du logiciel libre.
