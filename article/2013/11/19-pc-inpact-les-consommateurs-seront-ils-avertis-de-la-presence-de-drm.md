---
site: PC INpact
title: "Les consommateurs seront-ils avertis de la présence de DRM?"
author: Marc Rees
date: 2013-11-19
href: http://www.pcinpact.com/news/84490-les-consommateurs-seront-ils-avertis-presence-drm.htm
tags:
- April
- Institutions
- DRM
- Europe
---

> Le projet de loi sur la consommation de Benoît Hamon sera examiné ajourd'hui en seconde lecture par la Commission des affaires économiques de l’Assemblée nationale. À cette occasion, le député Lionel Tardy insiste pour que le consommateur soit clairement informé de la présence de DRM lorsqu’il acquiert un contenu culturel.
