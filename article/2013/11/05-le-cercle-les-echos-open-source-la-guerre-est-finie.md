---
site: Le Cercle Les Echos
title: "Open source, la guerre est finie?"
author: Patrice Bertrand
date: 2013-11-05
href: http://lecercle.lesechos.fr/entreprises-marches/high-tech-medias/informatiques/221183662/open-source-guerre-est-finie
tags:
- Entreprise
- Logiciels privateurs
- Interopérabilité
- Brevets logiciels
- Droit d'auteur
- Licenses
---

> "Tous amis, tous réunis! L’heure n’est plus aux oppositions stériles entre logiciel open source et logiciel propriétaire!" Un discours très en vogue, mais qu’en est-il vraiment? Les antagonismes sont derrière nous? Les combats d’arrière-garde?
