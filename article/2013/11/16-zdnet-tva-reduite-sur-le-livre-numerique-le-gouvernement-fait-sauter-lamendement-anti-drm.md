---
site: ZDNet
title: "TVA réduite sur le livre numérique: le gouvernement fait sauter l'amendement anti-DRM"
author: Thierry Noisette
date: 2013-11-16
href: http://www.zdnet.fr/actualites/tva-reduite-sur-le-livre-numerique-le-gouvernement-fait-sauter-l-amendement-anti-drm-39795596.htm
tags:
- Économie
- April
- Institutions
- Vente liée
- DRM
---

> Adopté contre l'avis du gouvernement, l'amendement EELV n'appliquant pas la TVA à taux réduit aux e-books en format fermé ou contenant des DRM n'aura vécu qu'une journée, supprimé par le vote des députés PS.
