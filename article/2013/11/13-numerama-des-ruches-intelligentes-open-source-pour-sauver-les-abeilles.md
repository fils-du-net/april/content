---
site: Numerama
title: "Des ruches intelligentes open-source pour sauver les abeilles"
author: Guillaume Champeau
date: 2013-11-13
href: http://www.numerama.com/magazine/27499-des-ruches-intelligentes-open-source-pour-sauver-les-abeilles.html
tags:
- Partage du savoir
- Matériel libre
- Innovation
---

> Open Source Beehives veut mettre l'open source et l'open hardware à profit des abeilles, en proposant des "ruches intelligentes" à réaliser en DIY, seul ou dans un Fab Lab.
