---
site: L'Informaticien
title: "Bernard Stiegler: «Former en masse les Français au numérique»"
author: Emilien Ercolani
date: 2013-11-06
href: http://www.linformaticien.com/actualites/id/30930/bernard-stiegler-former-en-masse-les-francais-au-numerique.aspx
tags:
- Économie
- Partage du savoir
- Éducation
- Innovation
---

> Pour contrer l’essor des extrêmes, le philosophe français préconise de miser sur une refonte de la formation et sur une «nouvelle conception du Web intégralement contributive».
