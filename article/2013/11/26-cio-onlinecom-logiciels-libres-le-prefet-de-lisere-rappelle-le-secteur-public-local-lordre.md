---
site: "cio-online.com"
title: "Logiciels libres: le préfet de l'Isère rappelle le secteur public local à l'ordre"
author: Bertrand Lemaire
date: 2013-11-26
href: http://www.cio-online.com/actualites/lire-logiciels-libres-le-prefet-de-l-isere-rappelle-le-secteur-public-local-a-l-ordre-5472.html
tags:
- Administration
- Marchés publics
---

> Les marchés publics reposent sur la mise en concurrence. Un utile rappel à la loi vient d'être réalisé par le préfet de l'Isère.
