---
site: vousnousils
title: "IGEN au siège de Microsoft: les acteurs du logiciel libre dénoncent une \"entorse à la neutralité scolaire\""
date: 2013-11-14
href: http://www.vousnousils.fr/2013/11/14/igen-au-siege-de-microsoft-les-acteurs-du-logiciel-libre-denoncent-une-entorse-a-la-neutralite-scolaire-551129
tags:
- Entreprise
- Logiciels privateurs
- Administration
- April
- Associations
- Éducation
---

> La participation d'inspecteurs généraux à une table ronde organisée par Microsoft sur le numérique éducatif dérange les défenseurs du logiciel libre.
