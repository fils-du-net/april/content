---
site: Framablog
title: "La NSA et la surveillance sur Internet pour les nuls"
author: Scott Cawley, Jemima Kiss, Paul Boyd et James Ball (traduction framablog)
date: 2013-11-27
href: http://www.framablog.org/index.php/post/2013/11/27/nsa-surveillance-video-guardian
tags:
- Internet
- Institutions
- Informatique-deloyale
- International
- Vie privée
---

> Maintenant que nous sommes presque tous en ligne, et connectés d’une façon ou d’une autre, les espions ne ciblent plus une ou deux personnes mal intentionnées. Au lieu de cela, les gouvernements du Royaume-Uni et des États-Unis pratiquent tous deux une collecte massive d’informations sur ce que nous faisons avec nos téléphones et sur Internet.
