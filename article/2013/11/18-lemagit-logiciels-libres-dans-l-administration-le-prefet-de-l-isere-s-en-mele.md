---
site: LeMagIT
title: "Logiciels libres dans l’administration: le préfet de l’Isère s’en mêle"
author: Cyrille Chausson
date: 2013-11-18
href: http://www.lemagit.fr/actualites/2240209211/Logiciels-libres-dans-ladministration-le-prefet-de-lIsere-sen-mele
tags:
- Internet
- Administration
- Associations
- Marchés publics
---

> Comme un rappel à l’ordre, Frédéric Périssat, préfet de l’Isère, a adressé une circulaire aux administrations locales de la région, leur rappelant les règles du code des marchés publics en matière d’achats informatiques et les invitant à respecter «les principes d’égalité de traitement des candidats et de liberté d’accès à la commande publique». En cause la mention dans les appels d’offres de marques, brevets ou technologies types qui écartent encore trop les logiciels libres des décisions d’achat des administrations - du moins dans cette région.
