---
site: Le Monde Informatique
title: "L'Open Innovation progresse en France, des freins persistent"
author: Maryse Gros
date: 2013-11-27
href: http://www.lemondeinformatique.fr/actualites/lire-l-open-innovation-progresse-en-france-des-freins-persistent-55814.html
tags:
- Entreprise
- Innovation
---

> De nombreuses initiatives favorisent déjà l'Open Innovation dans l'Hexagone, mettant en relation les compétences et les idées entre grandes entreprises, laboratoires de R&amp;D, PME et start-up. Mais il faut de la persévérance pour faire aboutir les partenariats. Et la France manque toujours de capitaux à investir dans les start-up. Une table ronde a réuni ce matin au Labo de l'Edition (Paris 5ème) quatre spécialistes du sujet : Jean-Luc Beylat de Bell Labs France, Bernard Scherrer d'EDF, Christian Travier de Laval Mayenne Technolopôle et Jean-Louis Liévin, d'ideXlab.
