---
site: Techniques de l'Ingénieur
title: "Smartphone espion: 7 moyens efficaces de préserver votre vie privée"
author: Sébastien Tribot
date: 2013-11-29
href: http://www.techniques-ingenieur.fr/actualite/informatique-electronique-telecoms-thematique_193/smartphone-espion-7-moyens-efficaces-de-preserver-votre-vie-privee-article_85317
tags:
- Internet
- Informatique-deloyale
---

> Par nos diverses activités Internet, l'utilisation GPS, certains opérateurs ainsi que les systèmes d'exploitation, nos chers smartphones récoltent nos informations personnelles, décortiquent notre vie et nous géolocalisent. Toutefois, il existe des moyens pour préserver sa vie privée.
