---
site: Mediapart
title: "Cher M. le hacker, on voulait quand même un peu te remercier"
author: Mélanie Vogel et Sonia Rouabhi
date: 2013-11-25
href: http://blogs.mediapart.fr/edition/les-invites-de-mediapart/article/251113/cher-m-le-hacker-voulait-quand-meme-un-peu-te-remercier
tags:
- Logiciels privateurs
- Institutions
- Sensibilisation
- Informatique-deloyale
- Europe
---

> Le 21 novembre, Mediapart révélait comment un hacker avait piraté les mails de plusieurs eurodéputés et assistants parlementaires pour montrer les failles dans la sécurité informatique du Parlement européen. Deux assistantes parlementaires visées par ce piratage écrivent aujourd'hui une lettre au hacker.
