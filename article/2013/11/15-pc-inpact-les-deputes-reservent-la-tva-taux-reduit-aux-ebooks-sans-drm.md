---
site: PC INpact
title: "Les députés réservent la TVA à taux réduit aux eBooks sans DRM!"
author: Marc Rees
date: 2013-11-15
href: http://www.pcinpact.com/news/84443-les-deputes-reservent-tva-a-taux-reduit-aux-ebooks-sans-drm.htm
tags:
- Économie
- Institutions
- DRM
---

> Le groupe des écologistes avait déposé un premier amendement visant à réserver la TVA à taux réduit aux seuls eBooks sans verrou numérique. Le texte avait été retiré avant le début des débats, mais le même parti avait sous le coude un autre amendement identique. Surprise! Contre l’avis du gouvernement et de la commission, ce texte a finalement été adopté par les députés.
