---
site: France Info
title: "Comment se protéger contre l'espionnage de la NSA?"
date: 2013-11-05
href: http://www.franceinfo.fr/high-tech/nouveau-monde/comment-se-proteger-contre-l-espionnage-de-la-nsa-1198915-2013-11-05
tags:
- Internet
- Sensibilisation
- Informatique-deloyale
---

> L'affaire Prism inquiète les Européens. Il existe des moyens techniques pour contrer les écoutes de la NSA. Mais ce n'est pas simple.
