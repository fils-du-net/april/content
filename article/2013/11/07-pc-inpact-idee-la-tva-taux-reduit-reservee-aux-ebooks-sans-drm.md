---
site: PC INpact
title: "Idée: la TVA à taux réduit réservée aux eBooks sans DRM"
author: Marc Rees
date: 2013-11-07
href: http://www.pcinpact.com/news/84281-idee-tva-a-taux-reduit-reservee-aux-ebooks-sans-drm.htm
tags:
- Économie
- Institutions
- DRM
---

> Est-il si normal que les finances publiques offrent généreusement un taux réduit de TVA à tous les eBooks, même ceux verrouillés? Les contribuables aident ainsi des éditeurs qui vont les enfermer dans des univers menottés par des Digital Rights Management (DRM) ou Mesures Techniques de Protection (MTP). Deux députés veulent tourner la page à cette injustice et conditionner finalement le taux réduit à l’absence de DRM.
