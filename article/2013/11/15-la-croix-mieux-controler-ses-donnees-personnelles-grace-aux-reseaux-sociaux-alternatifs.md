---
site: la Croix
title: "Mieux contrôler ses données personnelles grâce aux réseaux sociaux alternatifs"
author: Axel Bizel-Bizellot
date: 2013-11-15
href: http://www.la-croix.com/Culture/Nouvelles-technologies/Mieux-controler-ses-donnees-personnelles-grace-aux-reseaux-sociaux-alternatifs-2013-11-15-1061345
tags:
- Internet
- Sensibilisation
- Vie privée
---

> Séduisantes sur le plan éthique, ces initiatives restent pour l’instant confinées à une sphère technophile avertie.
