---
site: Numerama
title: "La proposition de loi Attard sur le domaine public est officialisée"
author: Guillaume Champeau
date: 2013-11-22
href: http://www.numerama.com/magazine/27586-la-proposition-de-loi-attard-sur-le-domaine-public-est-officialisee.html
tags:
- Institutions
- DRM
- Droit d'auteur
---

> Le bureau de l'Assemblée Nationale a enregistré jeudi la proposition de loi d'Isabelle Attard sur la consécration d'un domaine public, remis au coeur du droit d'auteur.
