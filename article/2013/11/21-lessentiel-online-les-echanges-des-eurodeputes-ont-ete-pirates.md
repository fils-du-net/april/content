---
site: L'essentiel Online
title: "Les échanges des eurodéputés ont été piratés"
author: Jean-Michel Hennebert
date: 2013-11-21
href: http://www.lessentiel.lu/fr/news/europe/story/Les---changes-des-eurod--put--s-ont---t---pirat--s-18397453
tags:
- Internet
- Logiciels privateurs
- Institutions
- Informatique-deloyale
- Europe
---

> Les données personnelles et des dizaines de milliers de mails d élus européens peuvent être accessibles en raison d une faille de sécurité majeure, indique jeudi le site «Mediapart».
