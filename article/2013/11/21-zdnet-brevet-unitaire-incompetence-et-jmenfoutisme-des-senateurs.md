---
site: ZDNet
title: "Brevet unitaire: «incompétence et j'menfoutisme» des sénateurs"
author: Christophe Auffray
date: 2013-11-21
href: http://www.zdnet.fr/actualites/brevet-unitaire-incompetence-et-j-menfoutisme-des-senateurs-39795719.htm
tags:
- Entreprise
- April
- Institutions
- Brevets logiciels
- Europe
---

> L’April tempête contre les sénateurs français qui ont adopté, sans débat, le projet de loi autorisant la ratification de l’accord sur le brevet unitaire, dénoncé comme une porte ouverte aux brevets logiciels.
