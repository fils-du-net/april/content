---
site: Numerama
title: "Un \"biohacker\" glisse un (gros) implant open-source DIY dans son bras"
author: Guillaume Champeau
date: 2013-11-01
href: http://www.numerama.com/magazine/27385-un-34biohacker34-glisse-un-gros-implant-open-source-diy-dans-son-bras.html
tags:
- Matériel libre
- Innovation
---

> C'est paraît-il l'avenir de la médecine. Avec des implants sous-cutané, il sera bientôt possible de contrôler en temps-réel son état de santé, et de réagir en conséquence. Mais pour le biohacker Tim Cannon, il est important que l'homme garde le contrôle sur son corps, ce qui passe par des implants open-source. Qu'il s'est lui-même implanté, sans anesthésie.
