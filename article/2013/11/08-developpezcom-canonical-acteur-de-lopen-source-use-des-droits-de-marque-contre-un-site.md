---
site: Developpez.com
title: "Canonical, acteur de l'open source, use des droits de marque contre un site"
author: Hinault Romaric
date: 2013-11-08
href: http://www.developpez.com/actu/63929/Canonical-acteur-de-l-open-source-use-des-droits-de-marque-contre-un-site-qui-critique-les-fonctions-d-Ubuntu-portant-atteintes-a-la-vie-privee
tags:
- Entreprise
- Internet
- Informatique-deloyale
---

> Qui critique les fonctions d'Ubuntu portant atteintes à la vie privée
