---
site: Numerama
title: "La TVA des ebooks ajustée en fonction des DRM suggérée dans un amendement"
author: Julien L.
date: 2013-11-07
href: http://www.numerama.com/magazine/27438-la-tva-des-ebooks-ajustee-en-fonction-des-drm-suggeree-dans-un-amendement.html
tags:
- Économie
- April
- Institutions
- DRM
---

> Afin de privilégier les livres numériques optant pour un format ouvert, des députés proposent d'ajuster le taux de TVA en fonction des DRM. Si un ebook n'en a pas, il accède au taux réduit; sinon, il paie le taux standard. Initialement proposée par la députée Isabelle Attard, cette idée est désormais reprise dans un amendement.
