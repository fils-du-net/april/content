---
site: wmc actualités
title: "Tunisie: L'application des logiciels libres dans les administrations est l'une des priorités"
author: Di
date: 2013-11-12
href: http://directinfo.webmanagercenter.com/2013/11/12/tunisie-lapplication-des-logiciels-libres-dans-les-administrations-est-lune-des-priorites
tags:
- Administration
- Éducation
- International
---

> La mise en oeuvre d’un ensemble de projets pilotes d’application des logiciels libres dans un nombre d’administrations compte parmi les priorités de l’année 2014, a indiqué à TAP la directrice des projets et programmes au ministère des technologies de l’information et de la communication Thoraya Ezzine.
