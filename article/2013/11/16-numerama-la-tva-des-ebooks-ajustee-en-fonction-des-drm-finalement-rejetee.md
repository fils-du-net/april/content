---
site: Numerama
title: "La TVA des ebooks ajustée en fonction des DRM finalement rejetée"
author: Julien L.
date: 2013-11-16
href: http://www.numerama.com/magazine/27523-la-tva-des-ebooks-ajustee-en-fonction-des-drm-finalement-rejetee.html
tags:
- Institutions
- DRM
---

> L'Assemblée nationale a changé d'avis. Après avoir adopté jeudi un amendement proposant de régler le taux de TVA des livres numériques selon la présence ou non des mesures techniques de protection (DRM), l'hémicycle a reculé vendredi en adoptant un amendement supprimant le premier.
