---
site: ZDNet
title: "LPM: Aux données personnelles, la patrie reconnaissante"
author: Antoine Duvauchelle
date: 2013-11-26
href: http://www.zdnet.fr/actualites/lpm-aux-donnees-personnelles-la-patrie-reconnaissante-39795822.htm
tags:
- Internet
- Institutions
- Vie privée
---

> La loi de programmation militaire passe aujourd'hui devant les députés. Elle pourrait faciliter la cybersurveillance, avec des conséquences inquiétantes pour la vie privée.
