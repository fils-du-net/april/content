---
site: Le Monde.fr
title: "La liberté (logicielle) s'use si on ne s'en sert pas"
author: Laurent Séguin
date: 2013-11-04
href: http://www.lemonde.fr/sciences/article/2013/11/04/la-liberte-logicielle-s-use-si-on-ne-s-en-sert-pas_3507909_1650684.html
tags:
- Sensibilisation
- Associations
- Innovation
- Licenses
- Philosophie GNU
---

> Bien que la diffusion du logiciel libre n'ait cessé de croître depuis trente ans, Laurent Séguin, président de l'Association francophone des utilisateurs de logiciels libres (AFUL), s'inquiète de ce que certains intérêts économiques mettent en danger sa pérennité et freinent ainsi l'innovation.
