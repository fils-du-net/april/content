---
site: Framablog
title: "Un livre numérique avec DRM n'est pas un livre nous dit l'Assemblée"
author: aKa
date: 2013-11-01
href: http://www.framablog.org/index.php/post/2013/11/15/livres-numeriques-drm-assemblee
tags:
- Entreprise
- Interopérabilité
- Institutions
- DRM
---

> En réalité, avec Apple ou Amazon, ce ne sont pas des livres qui sont vendus, mais des licences de lecture…
