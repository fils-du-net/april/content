---
site: Bilan
title: "Les experts appellent les internautes à se révolter"
author: Leila Ueberschlag
date: 2013-11-07
href: http://www.bilan.ch/techno-les-plus-de-la-redaction/les-experts-appellent-les-internautes-se-revolter
tags:
- Entreprise
- Internet
- Institutions
- Informatique-deloyale
- International
- Vie privée
---

> Les spécialistes fustigent l’indifférence générale face à l’espionnage des données personnelles et aux atteintes à la vie privée.
