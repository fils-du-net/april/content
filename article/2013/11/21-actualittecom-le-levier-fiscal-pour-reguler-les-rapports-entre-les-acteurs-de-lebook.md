---
site: ActuaLitté.com
title: "Le levier fiscal pour réguler les rapports entre les acteurs de l'ebook"
date: 2013-11-21
href: http://www.actualitte.com/legislation/le-levier-fiscal-pour-reguler-les-rapports-entre-les-acteurs-de-l-ebook-46463.htm
tags:
- Interopérabilité
- April
- Institutions
- Associations
- DRM
---

> Le gouvernement était opposé à l'amendement d'Isabelle Attard, proposant d'exercer une TVA plus forte sur la vente de licences de lecture, contrairement aux ventes réelles d'ouvrages numériques. En somme, taxer les systèmes propriétaires, et privilégier les écosystèmes ouverts. Et plus encore, valoriser l'absence de verrous numériques. L'organisation SavoisCom1 revient sur cet épisode législatif, assez lamentable.
