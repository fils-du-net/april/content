---
site: Mag14
title: "A quoi joue l’Etat tunisien avec Microsoft?"
author: Moez E.K
date: 2013-11-26
href: http://mag14.com/technologies/45-digital/2563-a-quoi-joue-letat-tunisien-avec-microsoft-.html
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Marchés publics
- International
---

> «Les programmes informatiques de l'administration tunisienne, des ministères de l'Education, de l'Enseignement supérieur, de la Formation professionnelle et de l'emploi, seront développés par Microsoft» annonce un communiqué de la présidence du gouvernement, qui fait état d’un contrat établi, avec la multinationale.
