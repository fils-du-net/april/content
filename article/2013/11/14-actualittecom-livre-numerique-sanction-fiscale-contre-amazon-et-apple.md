---
site: ActuaLitté.com
title: "Livre numérique: sanction fiscale contre Amazon et Apple"
author: Nicolas Gary
date: 2013-11-14
href: http://www.actualitte.com/usages/livre-numerique-sanction-fiscale-contre-amazon-et-apple-46330.htm
tags:
- Économie
- Interopérabilité
- Institutions
- DRM
---

> Les députés adoptent l'interopérabilité
