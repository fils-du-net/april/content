---
site: Pressenza
title: "Logiciel libre et cryptographie: des alliés pour éviter d'être espionnés sur Internet"
author: David Loor
date: 2013-08-06
href: http://www.pressenza.com/fr/2013/08/logiciel-libre-et-cryptographie-des-allies-pour-eviter-detre-espionnes-sur-internet
tags:
- Entreprise
- Internet
- Institutions
- Informatique-deloyale
---

> Depuis le révélation en juin des documents confidentiels à propos du programme d’espionnage PRISM réalisé par les États-Unis, qui permet à la NSA (National Security Agency) d’accéder de manière directe aux serveurs des plus grandes entreprises d’Internet (Google, Facebook, Apple, Microsoft, Yahoo, entre autres), les utilisateurs continuent d’utiliser les services et logiciel fournis par les entreprises mentionnées sans prendre de mesure afin d’éviter que leur information soit espionnée.
