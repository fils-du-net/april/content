---
site: PC INpact
title: "En Espagne, la région de Valence fait passer tous ses PC à LibreOffice"
author: Xavier Berne
date: 2013-08-24
href: http://www.pcinpact.com/news/81943-en-espagne-region-valence-fait-passer-tous-ses-pc-a-libreoffice.htm
tags:
- Administration
- Économie
- International
---

> Les 120 000 ordinateurs mis à la disposition des personnels administratifs de la région de Valence, en Espagne, sont désormais équipés de la suite bureautique LibreOffice. L'objectif affiché est avant tout de réaliser des économies, alors que nos voisins ibériques font face à d'importantes difficultés budgétaires.
