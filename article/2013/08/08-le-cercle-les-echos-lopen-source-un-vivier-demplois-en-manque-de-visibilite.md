---
site: Le Cercle Les Echos
title: "L'Open Source: un vivier d'emplois en manque de visibilité"
author: Pierre Queinnec
date: 2013-08-08
href: http://lecercle.lesechos.fr/economie-societe/social/emploi/221178122/lopen-source-vivier-demplois-manque-visibilite
tags:
- Entreprise
- Économie
- Éducation
- Innovation
- Informatique en nuage
---

> 300 PME et ETI spécialisées dans le Logiciel libre, plus de 30 000 salariés, 30 % de croissance annuelle, telle sont le panorama français de l’économie de l’Open Source en 2012. Pierre Queinnec, Président de l’édition 2013 de l’Open Word Forum, décrypte ces chiffres et analyse les opportunités de carrière à venir.
