---
site: PC INpact
title: "Le ministère de la Culture «bien conscient de l'intérêt des logiciels libres»"
author: Xavier Berne
date: 2013-08-07
href: http://www.pcinpact.com/news/81625-le-ministere-culture-bien-conscient-interet-logiciels-libres.htm
tags:
- Logiciels privateurs
- Institutions
---

> Cela fait maintenant plus de deux mois que la députée (écologiste) Isabelle Attard a demandé à chacun des 37 ministres du gouvernement Ayrault de fournir le détail de ses dépenses en logiciels, libres comme propriétaires, pour la période 2008-2012. Le ministère de la Culture est le quatorzième a fournir une réponse à l’élue.
