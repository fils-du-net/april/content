---
site: cnet France
title: "Transhumanisme: demain, l'Homme amélioré"
date: 2013-08-25
href: http://www.cnetfrance.fr/news/transhumanisme-demain-l-homme-ameliore-39793272.htm
tags:
- Partage du savoir
- Innovation
- Sciences
---

> Car reste à savoir si à l’avenir, nous resterons dans le monde bien gentil des body hackers où tout sera (peut-être) “open source” et où chacun disposera librement de son corps, ou si nous tomberons dans un avenir où chacun tentera de s’offrir des appareils très coûteux, vendus par des labos ou des entreprises, quasi monopolistiques... au risque de créer une nouvelle forme d’inégalité, celle de l’augmentation.
