---
site: Métro
title: "Dans l’antre de Savoir-faire Linux"
author: Mathias Marchal
date: 2013-08-16
href: http://journalmetro.com/plus/carrieres/358031/dans-lantre-de-savoir-faire-linux
tags:
- Entreprise
- Économie
- Institutions
- Marchés publics
- International
---

> «Pur le prix d’un soldat, on va te mettre une armée à disposition», affirme Cyrille Béraud, le président de Savoir-faire Linux qui a le sens de la formule. Ce Français d’origine dirige l’une des entreprises parmi les plus inspirantes de Montréal. Pour permettre au logiciel libre de percer dans le monde fermé de l’informatique d’entreprise, il n’hésite pas à jouer du coude, quitte à se faire quelques ennemis… et beaucoup d’amis.
