---
site: ZDNet
title: "Le premier ordinateur portable sous Linux à énergie solaire"
author: Steven J. Vaughan-Nichols
date: 2013-08-12
href: http://www.zdnet.fr/actualites/le-premier-ordinateur-portable-sous-linux-a-energie-solaire-39793136.htm
tags:
- Innovation
---

> Conçu par la telco canadienne WeWi et fonctionnant avec Ubuntu, le SOL est annoncé avec une autonomie de 8 à 10 heures. Ce nouveau portable, encore en développement, sera nommé le SOL. WeWi n’a révélé aucun prix, mais la rumeur le situe à 400 dollars. Il sera d’abord diffusé en Afrique. Le Ghana et le Kenya seront les premiers servis. L’objectif avec cet appareil, affirme l’entreprise, est que "avec une conception robuste et durable et un panneau solaire, vous pourrez emporter SOL dans les endroits les plus  reculés au monde".
