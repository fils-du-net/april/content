---
site: toolinux
title: "APRIL: Vie Privée en 2013: Pourquoi. Quand. Comment."
date: 2013-08-12
href: http://www.toolinux.com/APRIL-Vie-Privee-en-2013-Pourquoi
tags:
- Internet
- April
---

> iL’auteur de GNUPG, Werner Koch, donné une conférence dans le cadre des RMLL à Bruxelles. "Privacy 2013: Why. When. How" a été traduit en français par l’APRIL.
