---
site: Métro
title: "Les logiciels libres à nouveau écartés"
author: Mathias Marchal
date: 2013-08-06
href: http://journalmetro.com/actualites/montreal/354545/les-logiciels-libres-a-nouveau-ecartes
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Marchés publics
- International
---

> La Ville de Montréal a publié le 24 juillet un appel d’offres dirigé vers Microsoft, écartant de nouveau les avenues offertes par le logiciel libre.
