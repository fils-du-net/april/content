---
site: ActuaLitté.com
title: "Désobéissance civile: Abattre l'industrie scolaire avec l'Open Source"
author: Nicolas Gary
date: 2013-08-24
href: http://www.actualitte.com/education-international/desobeissance-civile-abattre-l-industrie-scolaire-avec-l-open-source-44602.htm
tags:
- Entreprise
- Internet
- Économie
- Partage du savoir
- Éducation
- ACTA
---

> Le PDF, c'est l'avenir?
