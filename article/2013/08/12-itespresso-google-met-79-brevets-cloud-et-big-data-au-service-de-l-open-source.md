---
site: ITespresso
title: "Google met 79 brevets cloud et big data au service de l’open source"
date: 2013-08-12
href: http://www.itespresso.fr/google-79-brevets-cloud-big-data-service-open-source-66983.html
tags:
- Entreprise
- Associations
- Brevets logiciels
- Innovation
---

> L’initiative Open Patent Non-Assertion Pledge, par laquelle Google ouvre une partie de sa propriété intellectuelle à la communauté open source, s’enrichit de 79 brevets cloud et big data.
