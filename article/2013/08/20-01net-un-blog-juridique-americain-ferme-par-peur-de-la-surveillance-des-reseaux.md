---
site: 01net.
title: "Un blog juridique américain ferme, par peur de la surveillance des réseaux"
author: Gilbert Kallenborn
date: 2013-08-20
href: http://www.01net.com/editorial/601470/un-blog-juridique-americain-ferme-par-peur-de-la-surveillance-des-reseaux
tags:
- Internet
- Informatique-deloyale
---

> Spécialisé dans les affaires high-tech, Groklaw.com arrête ses publications. La créatrice du site, Pamela Jones, estime qu'Internet ne garantit plus le niveau de confidentialité nécessaire pour faire ce travail.
