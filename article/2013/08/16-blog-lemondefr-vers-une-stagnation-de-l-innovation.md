---
site: Blog LeMonde.fr
title: "Vers une stagnation de l’innovation"
author: Rémi Sussan
date: 2013-08-16
href: http://internetactu.blog.lemonde.fr/2013/08/16/vers-une-stagnation-de-linnovation
tags:
- Entreprise
- Économie
- Institutions
- Innovation
---

> Et si, loin de vivre une explosion d'innovations, nous nous trouvions plutôt dans une phase de blocage? Car s'il est vrai que nous assistons aujourd’hui à une multiplication des usages, ainsi qu'à un raffinement et une simplification de technologies déjà existantes (smartphones, web 2, etc.) les véritables innovations de rupture tardent finalement à se manifester.
