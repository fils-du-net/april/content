---
site: vousnousils
title: "Logiciels libres dans l’Education nationale: le ministère détaille ses actions"
date: 2013-08-08
href: http://www.vousnousils.fr/2013/08/08/logiciels-libres-dans-leducation-nationale-le-ministere-detaille-ses-actions-549751
tags:
- Administration
- April
- Institutions
- Éducation
---

> Interrogé fin mai par la dépu­tée écolo­giste Isabelle Attard sur son usage des logi­ciels libres, le minis­tère de l'Education natio­nale a publié sa réponse ce mardi.
