---
site: Developpez.com
title: "Remercions le logiciel libre, ou en serions-nous, si il n'existait pas?"
author: imikado
date: 2013-08-06
href: http://blog.developpez.com/ducodeetdulibre/p12163/non-classe/remercions-le-logiciel-libre-ou-en-serions-nous-si-il-nexistait-pas
tags:
- Internet
- Partage du savoir
- Sensibilisation
- Innovation
- Licenses
---

> Depuis des années où deux hommes : Richard Stallman avec GNU et Linus Torvald avec Linux ont investit leur temps libre pour le bien de la communauté, on a pu assister à un cercle vertueux dont on ne réalise pas l’importance.
