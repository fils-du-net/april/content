---
site: ZDNet
title: "Google apporte 79 brevets supplémentaires en open source"
author: thierry-noisette
date: 2013-08-13
href: http://www.zdnet.fr/actualites/google-apporte-79-brevets-supplementaires-en-open-source-39793165.htm
tags:
- Entreprise
- Associations
- Brevets logiciels
---

> Pacte de non-agression, suite: tandis que d’autres comme Apple et Samsung mènent une guerre de tranchées avec leurs brevets, Google poursuit ses efforts pour éviter les batailles en s’engageant à ne pas attaquer sur la base de 79 brevets de plus, notamment liés aux centres de données.
