---
site: Rue89
title: "Pub: les annonceurs se révoltent contre les «extrémistes» de Firefox"
author: Philippe Vion-Dury
date: 2013-08-14
href: http://www.rue89.com/2013/08/14/les-annonceurs-publicitaires-revoltent-contre-les-extremistes-firefox-244946
tags:
- Entreprise
- Internet
- Économie
- Associations
- Innovation
---

> Dans le climat de méfiance sur Internet et de flicage des internautes, le coup de grâce est porté par... Mozilla Firefox. C’est du moins ce que prétend la Digital Advertising Alliance qui réunit des annonceurs publicitaires sur Internet en Europe et aux Etats-Unis. Le groupement a publié une lettre incendiaire [PDF] intitulée «Empêchez Mozilla de prendre en otage Internet».
