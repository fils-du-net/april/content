---
site: Numerama
title: "Les brevets logiciels n'ont plus droit de cité en Nouvelle Zélande"
author: Guillaume Champeau
date: 2013-08-28
href: http://www.numerama.com/magazine/26840-les-brevets-logiciels-n-ont-plus-droit-de-cite-en-nouvelle-zelande.html
tags:
- Institutions
- Brevets logiciels
- Europe
- International
---

> Après cinq années de débats et de rebondissements, le Parlement de Nouvelle-Zélande a adopté sa loi sur les brevets, qui exclue toute brevetabilité des logiciels.
