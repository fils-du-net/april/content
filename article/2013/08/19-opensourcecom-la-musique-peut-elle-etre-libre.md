---
site: Framablog
title: "La musique peut-elle être libre?"
author: Michael Tiemann
date: 2013-08-19
href: http://www.framablog.org/index.php/post/2013/08/22/musique-libre
tags:
- Partage du savoir
- Accessibilité
- Droit d'auteur
- Contenus libres
---

> De l’eau a coulé sous les ponts depuis que les logiciels «open source» ont été baptisés ainsi en 1998. Le livre La cathédrale et le bazar a contribué à expliquer ce nouveau paradigme de la création des logiciels, et, avec le temps, les conséquences importantes et crédibles que Raymond avait prévues dans son essai s’avèrent aujourd’hui évidentes.
