---
site: Numerama
title: "La politique Geek: sortir du Capitalisme"
author: lsga
date: 2013-08-05
href: http://www.numerama.com/f/127039-t-la-politique-geek-sortir-du-capitalisme.html
tags:
- Partage du savoir
- Video
---

> Comment sortir d'un capitalisme malade? imaginer une économie de la contribution inspirée des comportements propres à l'univers numérique.
