---
site: PC INpact
title: "Dépenses logicielles: quatre nouveaux ministères répondent à Isabelle Attard"
author: Xavier Berne
date: 2013-08-16
href: http://www.pcinpact.com/news/81793-depenses-logicielles-quatre-nouveaux-ministeres-repondent-a-isabelle-attard.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
---

> Quatre nouvelles réponses viennent d’être transmises à la députée Isabelle Attard, qui souhaitait savoir quelles étaient les dépenses logicielles (propriétaire et libre) des différents ministères. Cette fois, c’est aux ministères de l’Économie et des Finances, du Budget, de la Décentralisation, ainsi que celui de la Consommation de se plier à l’exercice.
