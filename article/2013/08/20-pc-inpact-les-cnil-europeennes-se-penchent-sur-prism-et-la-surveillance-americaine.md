---
site: PC INpact
title: "Les CNIL européennes se penchent sur Prism et la surveillance américaine"
author: Vincent Hermann
date: 2013-08-20
href: http://www.pcinpact.com/news/81839-les-cnil-europeennes-se-penchent-sur-prism-et-surveillance-americaine.htm
tags:
- Internet
- Institutions
- Informatique-deloyale
- Europe
- International
---

> Le programme américain de surveillance Prism continue de provoquer des échos. Alimenté par les révélations successives des documents d’Edward Snowden, le scandale fait réagir de nombreux politiques. C’est désormais au tour du G29, le regroupement des CNIL européennes, d’annoncer qu’une enquête indépendante sera menée pour évaluer les conséquences de la surveillance américaine sur l’Union européenne.
