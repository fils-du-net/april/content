---
site: Le Point
title: "Les hackers du FBI s'attaquent à Tor"
author: Guerric Poncet
date: 2013-08-07
href: http://www.lepoint.fr/chroniqueurs-du-point/guerric-poncet/le-fbi-pirate-un-internet-parallele-07-08-2013-1712193_506.php
tags:
- Internet
- Partage du savoir
- Institutions
- International
---

> Les "fédéraux" ont infecté certains utilisateurs du réseau ultra-sécurisé pour stopper des sites pédophiles. Du moins officiellement...
