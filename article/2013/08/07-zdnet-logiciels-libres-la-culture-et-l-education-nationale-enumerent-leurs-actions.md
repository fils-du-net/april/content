---
site: ZDNet
title: "Logiciels libres: la Culture et l’Education nationale énumèrent leurs actions"
date: 2013-08-07
href: http://www.zdnet.fr/actualites/logiciels-libres-la-culture-et-l-education-nationale-enumerent-leurs-actions-39793057.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- Interopérabilité
- Institutions
- Éducation
---

> Suite des réponses des ministères aux questions sur leurs usages et leurs dépenses en logiciel libre. La Culture veut "profiter au mieux des avantages du logiciel libre, mais aussi entretenir le cercle vertueux de son écosystème". L'Education nationale totalise plus de 4.000 serveurs sous Linux dans les rectorats.
