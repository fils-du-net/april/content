---
site: ZDNet
title: "Guerre des brevets: Washington donne un coup de pouce à Apple contre Samsung qui contre-attaque"
date: 2013-08-05
href: http://www.zdnet.fr/actualites/guerre-des-brevets-washington-donne-un-coup-de-pouce-a-apple-contre-samsung-qui-contre-attaque-39792972.htm
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> Le gouvernement américain a fait jouer son droit de veto pour bloquer l’application de l’interdiction de vente de plusieurs modèles d’iPhone et d’iPad obtenue par Samsung auprès de l’International Trade Commission.
