---
site: clubic.com
title: "Brevet logiciel: où en est la France?"
author: Olivier Robillart
date: 2013-08-30
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/brevet-logiciel/actualite-580676-brevet-logiciel-france.html
tags:
- Institutions
- Associations
- Brevets logiciels
- Droit d'auteur
- International
---

> La législation autour de la brevetabilité des logiciels est en évolution, si certains Etats comme la Nouvelle-Zélande viennent de barrer la route à ce type de brevets, la France, via les décisions de l'Europe, s'est engagée sur une voie très critiquée. Toutefois, sur notre territoire, un logiciel «pur» ne peut pas, par principe, être breveté.
