---
site: Le Monde Informatique
title: "Après Prism, le projet de messagerie sécurisée Caliop relancé"
author: Bertrand Lemaire
date: 2013-08-21
href: http://www.lemondeinformatique.fr/actualites/lire-apres-prism-le-projet-de-messagerie-securisee-caliop-relance-54755.html
tags:
- Entreprise
- Internet
- Innovation
---

> A la suite de l'arrêt de Lavabit et Silent Circle, intervenu à la suite des révélations de l'affaire Prism, le cofondateur du bureau d'enregistrement français Gandi veut combler le vide créé par cette interruption et relance son projet Caliop.
