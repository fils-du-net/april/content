---
site: Le Monde Informatique
title: "Des développeurs cassent la sécurité du client Dropbox"
author: Jacques Cheminat avec IDG NS
date: 2013-08-30
href: http://www.lemondeinformatique.fr/actualites/lire-des-developpeurs-cassent-la-securite-du-client-dropbox-54852.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
---

> Au-delà de leur travail, Dhiru Kholia et Przemysław Wegrzyn invite Dropbox à faire évoluer sa plateforme vers l'Open Source, plutôt que de rester une «boîte noire». «Nous espérons que notre travail inspirera la communauté de la sécurité à écrire un client Dropbx en Open Source et plus généralement de mener des recherches dans d'autres solutions de stockage cloud», ont écrit les développeurs.
