---
site: PC INpact
title: "«Internet est terminé», Groklaw ferme ses portes"
author: Vincent Hermann
date: 2013-08-21
href: http://www.pcinpact.com/news/81869-internet-est-termine-grokloaw-ferme-ses-portes.htm
tags:
- Internet
- Institutions
- Associations
- Informatique-deloyale
---

> Le site Groklaw est connu depuis plusieurs années pour son travail sur l’environnement juridique autour du monde open source. Mais sa fondatrice, Pamela Jones, a annoncé hier soir que l’aventure était désormais terminée. La raison invoquée en inquiètera plus d’un: la surveillance totale des communications internet ne lui permet plus d’assurer convenablement sa mission.
