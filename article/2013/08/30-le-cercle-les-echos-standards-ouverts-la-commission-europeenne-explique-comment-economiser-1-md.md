---
site: Le Cercle Les Echos
title: "Standards ouverts: la Commission européenne explique comment économiser 1 Md€"
author: Patrice Bertrand
date: 2013-08-30
href: http://lecercle.lesechos.fr/entreprises-marches/high-tech-medias/informatiques/221178885/standards-ouverts-commission-europeenne
tags:
- Économie
- Interopérabilité
- Institutions
- Innovation
- Marchés publics
- Standards
- Europe
---

> En juin, la Commission européenne a publié une communication relative au bon usage des standards dans les achats publics. Elle souligne les multiples bénéfices de systèmes d’informations s’appuyant sur des standards, et évalue à plus de 1 milliard d’euros les économies que feraient les états de l’UE s’ils basaient leurs consultations sur des standards, assurant une compétition plus ouverte.
