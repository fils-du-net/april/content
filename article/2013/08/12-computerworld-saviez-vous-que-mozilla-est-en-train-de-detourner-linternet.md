---
site: Framablog
title: "Saviez-vous que Mozilla est en train de détourner l'Internet?"
author: Glyn Moody
date: 2013-08-12
href: http://www.framablog.org/index.php/post/2013/08/14/interactive-advertising-bureau
tags:
- Entreprise
- Internet
- Associations
- Informatique-deloyale
---

> Il y a quelques semaines j’ai relaté l’attaque à peine croyable de la branche européenne du «Interactive Advertising Bureau (IAB)» envers Mozilla au motif que cette dernière aurait «renoncé à ses valeurs» car elle persisterait à défendre les droits des utilisateurs à contrôler comment les cookies sont utilisés sur leur système.
