---
site: PC INpact
title: "4,27 millions € de dépenses logicielles en 2012 pour l’Éducation nationale"
author: Xavier Berne
date: 2013-08-07
href: http://www.pcinpact.com/news/81634-427-millions-depenses-logicielles-en-2012-pour-l-education-nationale.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Éducation
---

> Alors que l’utilisation du libre au sein des écoles, collèges et lycées a agité les débats parlementaires au cours des derniers mois, le ministère de l’Éducation nationale vient de lever un voile - partiel - sur ses dépenses en logiciels propriétaires et libres, comme l’avait demandé la députée Isabelle Attard.
