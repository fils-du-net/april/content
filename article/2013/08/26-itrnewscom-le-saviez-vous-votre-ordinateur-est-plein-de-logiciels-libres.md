---
site: ITRnews.com
title: "Le saviez-vous? Votre ordinateur est plein de Logiciels Libres!"
author: Elisabeth Racine
date: 2013-08-26
href: http://www.itrnews.com/articles/142876/saviez-ordinateur-est-plein-logiciels-libres-elisabeth-racine-vice-presidente-thematique-experiment-open-world-forum-2013.html
tags:
- Sensibilisation
- Philosophie GNU
- Promotion
---

> Le logiciel libre, quézako? Ce nom, a priori barbare, fait pourtant partie du quotidien de tous et est en réalité, un concept très simple à comprendre pour tout utilisateur.
