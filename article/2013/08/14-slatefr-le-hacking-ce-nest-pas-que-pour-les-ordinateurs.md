---
site: Slate.fr
title: "Le hacking, ce n'est pas que pour les ordinateurs"
author: Sabine Blanc
date: 2013-08-14
href: http://www.slate.fr/story/76296/hacking-pas-ordinateurs-cuisine-musique
tags:
- Partage du savoir
- Droit d'auteur
- Innovation
- Sciences
---

> Des LEGOS aux recettes de cuisine en passant par la musique classique, tout se hacke. Reportage à l’OHM, sorte de Jeux olympiques du hacking, à la recherche du futur.
