---
site: Numerama
title: "Hadopi: le transfert au CSA pourrait se faire en douce avant l'hiver"
author: Guillaume Champeau
date: 2013-08-31
href: http://www.numerama.com/magazine/26858-hadopi-le-transfert-au-csa-pourrait-se-faire-en-douce-avant-l-hiver.html
tags:
- Internet
- HADOPI
- Institutions
---

> Selon Les Echos, une manoeuvre politique programmée de concert entre le Gouvernement et le Sénat pourrait aboutir à transférer dès cette année les pouvoirs de l'Hadopi vers de le CSA, sans attendre la grande loi sur l'audiovisuel promise pour 2014.
