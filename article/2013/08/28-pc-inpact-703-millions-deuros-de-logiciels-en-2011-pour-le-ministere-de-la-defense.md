---
site: PC INpact
title: "70,3 millions d'euros de logiciels en 2011 pour le ministère de la Défense"
author: Xavier Berne
date: 2013-08-28
href: http://www.pcinpact.com/news/82007-703-millions-deuros-logiciels-en-2011-pour-ministere-defense.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
---

> Selon des estimations réalisées par l'exécutif, le ministère de la Défense aurait dépensé plus de 70 millions d'euros en 2011 pour ses logiciels. C'est en tout cas ce qu'affirme Jean-Yves Le Drian, ministre de la Défense, au détour d'une réponse (avare en détails) à une question écrite de la députée écologiste Isabelle Attard.
