---
site: InformatiqueNews
title: "Etat des lieux de l’Open Source en France"
author: Guy Hervier
date: 2013-10-03
href: http://www.informatiquenews.fr/etat-lieux-lopen-source-en-france-4472
tags:
- Entreprise
---

> 79 % des entreprises et 82 % des fournisseurs utilisent des technologies Open Source, c’est ce qui ressort de l’étude commanditée par Syntec Numérique et réalisée par IDC et publié à l’occasion de l’Open World Forum
