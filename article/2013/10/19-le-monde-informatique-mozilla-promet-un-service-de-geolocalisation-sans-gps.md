---
site: Le Monde Informatique
title: "Mozilla promet un service de géolocalisation sans GPS"
author: Adrien Geneste
date: 2013-10-19
href: http://www.lemondeinformatique.fr/actualites/lire-mozilla-promet-un-service-de-geolocalisation-sans-gps-55517.html
tags:
- Internet
- Associations
- Innovation
- Open Data
---

> Mozilla travaille sur un service de géolocalisation collaboratif utilisant les antennes relais et les réseaux WiFi publics. La communauté est appelée à la rescousse pour cartographier le territoire.
