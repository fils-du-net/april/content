---
site: Numerama
title: "L'Intérieur se félicite du passage vers le logiciel libre, quitte à exagérer ses efforts"
author: Julien L.
date: 2013-10-18
href: http://www.numerama.com/magazine/27267-l-interieur-se-felicite-du-passage-vers-le-logiciel-libre-quitte-a-exagerer-ses-efforts.html
tags:
- Logiciels privateurs
- Administration
- Économie
---

> Le ministère de l'Intérieur a donné des précisions sur son engagement dans le logiciel libre, en prenant pour exemple le cas de la gendarmerie nationale. Or, la décision de migrer de Windows vers Ubuntu a été prise bien avant que cette force armée soit rattachée à la place Beauveau.
