---
site: Tsugi
title: "Pour la London School of Economics, HADOPI n’aura servi à rien"
author: Clémence Meunier
date: 2013-10-11
href: http://www.tsugi.fr/magazines/2013/10/11/pour-london-school-economics-hadopi-n-aura-servi-rien-1599
tags:
- Internet
- Économie
- HADOPI
---

> La très respectable université d’économie de Londres, la LSE, a sorti une nouvelle étude sur l’impact du téléchargement sur l’industrie musicale. Et les conclusions sont bien éloignées de ce qu’on nous rabâche depuis quelques années. Selon le rapport, la diminution des revenus ne résulte pas d’un manque d’adaptabilité au web, et les mesures punitives (HADOPI en France, DEA au Royaume-Uni) sont loin des résultats escomptés. Ainsi, la LSE affirme qu’il n’y a absolument aucune preuve que les 23% à 25% d’augmentation des achats sur iTunes soient dus à la loi HADOPI.
