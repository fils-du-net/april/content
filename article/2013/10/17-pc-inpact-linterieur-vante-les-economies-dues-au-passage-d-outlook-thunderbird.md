---
site: PC INpact
title: "L'Intérieur vante les économies dues au passage d’Outlook à Thunderbird"
author: Xavier Berne
date: 2013-10-17
href: http://www.pcinpact.com/news/83970-linterieur-vante-economies-dues-au-passage-d-outlook-a-thunderbird.htm
tags:
- Administration
- Économie
- Institutions
---

> Au travers d’une réponse à une question parlementaire, le ministère de l’Intérieur vient de lever un léger voile sur ses dépenses logicielles pour la période 2008-2011. La Place Beauvau ne manque pas de souligner que le passage d’Outlook à Thunderbird, le logiciel libre distribué par la fondation Mozilla, a permis de réaliser des économies substantielles.
