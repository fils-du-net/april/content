---
site: Silicon.fr
title: "S. Fermigier, Systematic: «Big Data, après-PC et qualité logicielle sont les défis du logiciel libre»"
author: Ariane Beky
date: 2013-10-07
href: http://www.silicon.fr/open-world-forum-fermigier-systematic-gtll-logiciel-libre-89832.html
tags:
- Entreprise
- Économie
- Associations
- Innovation
---

> Vice-président de la thématique «code» de l’Open World Forum 2013, Stéfane Fermigier détaille les priorités du Groupe Thématique Logiciel Libre (GTLL) du pôle Systematic Paris-Région.
