---
site: PC INpact
title: "L'EFF fustige le W3C pour l'acceptation des DRM dans la nouvelle charte"
author: Vincent Hermann
date: 2013-10-03
href: http://www.pcinpact.com/news/82731-leff-fustige-w3c-pour-acceptation-drm-dans-nouvelle-charte.htm
tags:
- Internet
- Associations
- DRM
- Standards
---

> Le W3C travaille depuis longtemps sur les Encrypted Media Extensions, un standard qui doit permettre la mise en place d’une infrastructure commune pour les DRM. En avril dernier, 27 organisations demandaient le rejet de ce futur standard. Mais coup de théâtre : c’est Tim Berners-Lee en personne, le directeur du Consortium, qui a annoncé hier l’inclusion des EME dans la nouvelle charte du groupe de travail HTML.
