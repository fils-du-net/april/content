---
site: Slate.fr
title: "La Société Moulinsart peut-elle empêcher Tintin d'entrer dans le domaine public?"
author: Lionel Maurel
date: 2013-10-23
href: http://www.slate.fr/story/79200/societe-moulinsart-empecher-tintin-entreref-domaine-public
tags:
- Entreprise
- Institutions
- Droit d'auteur
---

> Elle voudrait publier un nouveau Tintin en 2053 pour prolonger ses droits sur l’œuvre...
