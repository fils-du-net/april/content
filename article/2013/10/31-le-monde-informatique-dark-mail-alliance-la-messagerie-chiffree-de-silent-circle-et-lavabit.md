---
site: Le Monde Informatique
title: "Dark Mail Alliance: la messagerie chiffrée de Silent Circle et Lavabit"
author: Adrien Geneste
date: 2013-10-31
href: http://www.lemondeinformatique.fr/actualites/lire-dark-mail-alliance-la-messagerie-chiffree-de-silent-circle-et-lavabit-55548.html
tags:
- Entreprise
- Internet
- Informatique-deloyale
- Innovation
- Standards
---

> Deux fournisseurs de messagerie ont lancé The Dark Mail Alliance. Le projet de Silent Circle et Lavabit vise à concevoir un système de messagerie doté de solides remparts contre l'espionnage.
