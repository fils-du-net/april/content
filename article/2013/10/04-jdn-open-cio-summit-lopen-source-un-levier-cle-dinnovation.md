---
site: JDN
title: "Open CIO Summit: l'open source, un levier clé d'innovation"
author: Antoine Crochet-Damais
date: 2013-10-04
href: http://www.journaldunet.com/solutions/dsi/open-cio-summit-2013-open-source-et-innovation.shtml
tags:
- Entreprise
- Économie
- Interopérabilité
- Innovation
- Informatique en nuage
---

> Lors de l'Open CIO Summit, une table ronde de DSI été organisée sur la place de l'open source dans les stratégies d'innovation. Retour sur quelques idées fortes qui ont émergé du débat.
