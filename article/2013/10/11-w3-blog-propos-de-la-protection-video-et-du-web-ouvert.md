---
site: W3 Blog
title: "À propos de la protection vidéo et du Web ouvert"
author: Tim Berbers-Lee (traduction Framablog)
date: 2013-10-11
href: http://www.framablog.org/index.php/post/2013/10/11/drm-html5-video-tim-berbers-lee
tags:
- Internet
- DRM
- Standards
---

> Il y a eu beaucoup de réactions suite au message annonçant que le W3C considère que le sujet de la protection des vidéos devait être abordé au sein de son Groupe de Travail HTML. Dans cet article, je voudrais donner quelques arguments pour expliquer cela.
