---
site: Le Monde Informatique
title: "Mieux former les développeurs Open Source aux questions juridiques"
author: Loek Esser (adaptation Véronique Arène)
date: 2013-10-25
href: http://www.lemondeinformatique.fr/actualites/lire-mieux-former-les-developpeurs-open-source-aux-questions-juridiques-55478.html
tags:
- Institutions
- Associations
- Droit d'auteur
- Licenses
- Standards
---

> La Fondation Linux estime que l'Open Source nécessite des connaissances juridiques que ne possèdent pas toujours les développeurs.
