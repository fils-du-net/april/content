---
site: Blogs LeMonde.fr
title: "Ara: le téléphone en kit durable, libre et co-créé"
author: Anne-Sophie Novel
date: 2013-10-30
href: http://alternatives.blog.lemonde.fr/2013/10/30/ara-le-telephone-en-kit-durable-libre-et-co-cree
tags:
- Entreprise
- Matériel libre
- Innovation
---

> "Hourra hourra, Ara sera bientôt là" pourrait-on s'exprimer! Suite au succès mondial du projet de téléphone en kit Phoneblocks, Motorola a communiqué hier sur un projet similaire sur lequel l'entreprise plancherait depuis un an.
