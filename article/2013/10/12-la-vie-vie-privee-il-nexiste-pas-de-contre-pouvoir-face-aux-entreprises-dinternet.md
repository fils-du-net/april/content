---
site: La Vie
title: "Vie privée: \"il n'existe pas de contre-pouvoir face aux entreprises d'Internet\""
author: Claire Legros
date: 2013-10-12
href: http://www.lavie.fr/debats/etats-generaux-christianisme/vie-privee-il-n-existe-pas-de-contre-pouvoir-face-aux-entreprises-d-internet-10-10-2013-45165_519.php
tags:
- Internet
- Institutions
- Associations
- Informatique-deloyale
---

> Tristan Nitot participe ce samedi 12 octobre dans le cadre des Etats généraux du christianisme à Lyon au débat organisé par La Vie «Internet prend-il le pouvoir sur nos vies privées?» aux côtés d'Isabelle Falque Pierrotin, présidente de la Cnil. Membre du Conseil national du numérique, Tristan Nitot est «principal évangéliste» ou «fire starter» (boute-feu en anglais) chez Mozilla, une organisation à but non-lucratif américaine qui produit le navigateur Firefox, l'un des leaders du marché.
