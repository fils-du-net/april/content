---
site: Silicon.fr
title: "Oracle et l'Open Source: je t'aime, moi non plus"
author: Ariane Beky
date: 2013-10-16
href: http://www.silicon.fr/oracle-open-source-dod-90144.html
tags:
- Entreprise
- Administration
- Économie
- Europe
---

> Oracle suggère au Département américain de la Défense que les coûts des logiciels Open Source sont plus élevés que ceux des logiciels propriétaires.
