---
site: PC INpact
title: "Le patent troll Lodsys plie finalement l'échine face à Kaspersky"
author: Vincent Hermann
date: 2013-10-03
href: http://www.pcinpact.com/news/82742-le-patent-troll-lodsys-plie-finalement-echine-face-a-kaspersky.htm
tags:
- Entreprise
- Institutions
- Brevets logiciels
---

> La société Lodsys, considérée comme un patent troll, attaque des dizaines d’entreprises pour faire valoir quelques brevets particulièrement puissants. Mais alors qu’Apple a tenté en vain de mettre des bâtons dans les roues du troll, un combat de deux ans contre Kaspersky vient d’échouer. Pour l’éditeur de solutions de sécurité, c’est le signe qu’il ne faut pas reculer devant Lodsys.
