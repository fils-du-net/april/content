---
site: Blogs LeMonde.fr
title: "Les logiciels libres, source d’inspiration pour les villes"
author: Anne-Sophie Novel
date: 2013-10-09
href: http://alternatives.blog.lemonde.fr/2013/10/09/les-logiciels-libres-source-dinspiration-pour-les-villes
tags:
- Internet
- Administration
- Partage du savoir
- Associations
- Innovation
---

> Un bien commun peut servir à tout le monde. Mutuel et réciproque, il est partagé par différents êtres ou différentes choses. A l'heure du numérique et de l'économie du partage, cette notion longtemps cantonnée au monde des logiciels libres sort maintenant des écrans pour offrir des solutions à l'ensemble des ressources partagées par les citoyens dans les villes.
