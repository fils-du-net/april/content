---
site: Global Security Mag
title: "Le CNLL recommande l'usage de l'appellation «Entreprise du Numérique Libre», ou ENL, pour désigner les anciennes SSLL"
author: Marc Jacob
date: 2013-10-23
href: http://www.globalsecuritymag.fr/Le-CNLL-recommande-l-usage-de-l,20131023,40552.html
tags:
- Entreprise
- Associations
---

> En avril 2013, Syntec Numérique annonçait que le terme d’ESN, Entreprises de Services du Numériques, était appelé à remplacer l’ancien SSII. Outre le rajeunissement, cette nouvelle appellation porte une différence subtile: le service en matière de numérique va au delà de la seule dimension d’ingénierie et d’informatique.
