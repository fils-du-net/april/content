---
site: Cubiq
title: "Le logiciel libre a fait de moi l'homme que je suis"
author: Matteo Spinelli (traduction Framablog)
date: 2013-10-06
href: http://www.framablog.org/index.php/post/2013/10/06/logiciel-libre-homme-que-je-suis
tags:
- Économie
- Sensibilisation
- Licenses
---

> De la conception de sites web pour les entreprises nationales au développement d’applications web haut de gamme pour les plus grands acteurs internationaux, tout cela grâce aux logiciels libres.
