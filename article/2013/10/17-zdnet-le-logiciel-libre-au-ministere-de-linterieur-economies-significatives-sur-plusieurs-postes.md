---
site: ZDNet
title: "Le logiciel libre au ministère de l'Intérieur: économies significatives sur plusieurs postes"
author:  Thierry Noisette
date: 2013-10-17
href: http://www.zdnet.fr/actualites/le-logiciel-libre-au-ministere-de-l-interieur-economies-significatives-sur-plusieurs-postes-39794898.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- Marchés publics
---

> L'Intérieur expose à son tour sa politique en matière de logiciels libres et open source. Le ministère de Manuel Valls affirme "une stratégie de recours volontaire au logiciel libre" et cite plusieurs évolutions marquantes, comme la messagerie où le passage au libre a été cinq fois moins cher sur la durée qu'une solution propriétaire.
