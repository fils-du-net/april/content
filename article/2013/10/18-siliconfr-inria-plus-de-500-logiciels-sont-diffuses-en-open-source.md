---
site: Silicon.fr
title: "Inria: «Plus de 500 logiciels sont diffusés en Open Source»"
author: Ariane Beky
date: 2013-10-18
href: http://www.silicon.fr/inria-patrick-moreau-open-source-transfert-90205.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Brevets logiciels
- Droit d'auteur
- Innovation
- Licenses
- Sciences
---

> Patrick Moreau, responsable du patrimoine logiciels au sein d’Inria, revient sur les travaux de l’institut public de recherche en sciences du numérique dans l’Open Source et fait le point sur le transfert de technologie.
