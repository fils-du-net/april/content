---
site: negocios
title: "Governo vai privilegiar a aquisição de “software” livre"
author: Ana Torres Pereira
date: 2013-10-15
href: http://www.jornaldenegocios.pt/empresas/tecnologias/detalhe/governo_vai_privilegiar_a_aquisicao_de_software_livre.html
tags:
- Logiciels privateurs
- Institutions
- Marchés publics
---

> O Governo vai privilegiar a aquisição de licenças de “software” livre e só será possível comprar licenças proprietárias quando não houver alternativas e caso o preço do “software” livre seja superior.
