---
site: Le Monde Informatique
title: "Des DRM bientôt intégrées au HTML5 pour bloquer les vidéos"
author: Jean Elyan
date: 2013-10-07
href: http://www.lemondeinformatique.fr/actualites/lire-des-drm-bientot-integrees-au-html5-pour-bloquer-les-videos-55259.html
tags:
- Internet
- Associations
- DRM
- Standards
---

> Le World Wide Web Consortium veut intégrer des DRM dans le HTML5 pour bloquer le streaming vidéo.
