---
site: ZDNet
title: "Contrat Microsoft Open Bar: «jeux de pouvoir» et «décision politique» pour l’April"
date: 2013-10-14
href: http://www.zdnet.fr/actualites/contrat-microsoft-open-bar-jeux-de-pouvoir-et-decision-politique-pour-l-april-39794782.htm
tags:
- Logiciels privateurs
- Administration
- April
- Institutions
- Marchés publics
---

> Selon des documents administratifs obtenus par l’April, le choix d’un contrat cadre entre la Défense et Microsoft fait «suite à une décision politique» prise en amont des études sur la faisabilité et les risques. Des études qui seront finalement ignorées.
