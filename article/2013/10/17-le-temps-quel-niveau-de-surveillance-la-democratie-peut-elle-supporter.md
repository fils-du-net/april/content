---
site: Le Temps
title: "Quel niveau de surveillance la démocratie peut-elle supporter?"
author: Mehdi Atmani
date: 2013-10-17
href: http://www.letemps.ch/Page/Uuid/bdec9080-37d2-11e3-9b9f-fd431094fc11
tags:
- Institutions
- Informatique-deloyale
- International
---

> L’ampleur de la surveillance documentée par les révélations d’Edward Snowden redéfinit les frontières entre sécurité, sphère privée et transparence. Pour beaucoup d’experts, le niveau de surveillance engendrée par le numérique est tel qu’il en menace la démocratie
