---
site: PC INpact
title: "Cisco ouvre son codec H.264 et marche main dans la main avec Mozilla"
author: Vincent Hermann
date: 2013-10-31
href: http://www.pcinpact.com/news/84194-cisco-ouvre-son-codec-h-264-et-marche-main-dans-main-avec-mozilla.htm
tags:
- Entreprise
- Internet
- Brevets logiciels
- Standards
- Video
---

> Le H.264 est un codec vidéo qui a montré toute son importance depuis des années. Utilisé dans de très nombreuses plateformes multimédia, il est aussi connu pour être protégé par de très nombreux brevets qui empêchent notamment son utilisation dans les produits open source. Pourtant, Cisco vient de faire un pas significatif dans ce domaine, en accord avec Mozilla qui applaudit le mouvement.
