---
site: JDN
title: "Quelles sont les analogies entre l'open data et l'open source?"
author: Pirmin Lemberger
date: 2013-10-24
href: http://www.journaldunet.com/ebusiness/expert/55589/quelles-sont-les-analogies-entre-l-open-data-et-l-open-source.shtml
tags:
- Internet
- Administration
- Innovation
- Licenses
- Open Data
---

> L'open data comme moteur de l'innovation dans les usages. Pourquoi rendre publique des données est un sujet délicat?
