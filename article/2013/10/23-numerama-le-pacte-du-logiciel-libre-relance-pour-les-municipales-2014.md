---
site: Numerama
title: "Le Pacte du Logiciel Libre relancé pour les municipales 2014"
author: Julien L.
date: 2013-10-23
href: http://www.numerama.com/magazine/27309-le-pacte-du-logiciel-libre-relance-pour-les-municipales-2014.html
tags:
- April
- Institutions
---

> L'April s'apprête à se saisir à nouveau de son bâton de pèlerin. En vue des municipales de 2014, l'association va une fois encore battre la campagne pour promouvoir le logiciel libre. En conséquence, elle commence à mobiliser ses troupes.
