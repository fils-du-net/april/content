---
site: PC INpact
title: "Contrefaçon: vers des dommages et intérêts nettement plus musclés"
author: Marc Rees
date: 2013-10-01
href: http://www.pcinpact.com/news/82670-contrefacon-vers-dommages-et-interets-nettement-plus-muscles.htm
tags:
- Économie
- Institutions
- Brevets logiciels
---

> Le sénateur Richard Yung a déposé hier une proposition de loi sur la contrefaçon (PDF). S'il est voté, le dispositif devrait enclencher une hausse mécanique des dommages et intérêts alloués en matière de contrefaçon.
