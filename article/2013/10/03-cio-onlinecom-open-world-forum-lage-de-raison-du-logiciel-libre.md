---
site: "cio-online.com"
title: "Open World Forum: l'âge de raison du logiciel libre"
author: Bertrand Lemaire
date: 2013-10-03
href: http://www.cio-online.com/actualites/lire-open-world-forum-l-age-de-raison-du-logiciel-libre-5365.html
tags:
- Entreprise
- Économie
- Institutions
- Innovation
- Promotion
---

> L'Open World Forum 2013, sixième édition de la manifestation, s'est ouvert pour trois jours le jeudi 3 octobre 2013 au Beffroi de Montrouge, à côté de la toute nouvelle station de métro Mairie de Montrouge, dans la proche banlieue sud de Paris. Loin d'être une joyeuse fête de hackers, la manifestation organisée par le pôle de compétitivité Systematic et ses partenaires est orientée sur l'économie du logiciel libre avec une forte dimension institutionnelle.
