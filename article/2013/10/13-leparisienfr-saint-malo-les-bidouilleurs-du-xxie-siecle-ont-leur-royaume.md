---
site: leParisien.fr
title: "Saint-Malo: les bidouilleurs du XXIe siècle ont leur royaume"
author: Solenne Durox
date: 2013-10-13
href: http://www.leparisien.fr/bretagne/saint-malo-les-bidouilleurs-du-xxie-siecle-ont-leur-royaume-13-10-2013-3221039.php
tags:
- Internet
- Matériel libre
- Associations
- Innovation
---

> Une vis dans une main et une clé Allen dans l’autre, Bastien, apprenti roboticien de 10 ans, a intégré sans le savoir la grande communauté des Makers. Comme lui, deux milliers de curieux ont assisté vendredi 11 et samedi 12 octobre à la première Mini Maker Faire de France organisée à Saint-Malo (Ille-et-Vilaine). Cette foire à l’artisanat futuriste et intergénérationnelle est une ode à la créativité citoyenne.
