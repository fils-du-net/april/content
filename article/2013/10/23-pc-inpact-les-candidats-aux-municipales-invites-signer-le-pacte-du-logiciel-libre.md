---
site: PC INpact
title: "Les candidats aux municipales invités à signer le «Pacte du logiciel libre»"
author: Xavier Berne
date: 2013-10-23
href: http://www.pcinpact.com/news/84070-les-candidats-aux-municipales-invites-a-signer-pacte-logiciel-libre.htm
tags:
- Internet
- Administration
- April
- Institutions
- Éducation
- Promotion
- Standards
---

> À l’approche des élections municipales de mars 2014, l’April et son initiative Candidats.fr viennent de lancer une campagne visant à faire signer à un maximum de candidats le «Pacte du logiciel libre». Explications.
