---
site: Wired
title: "Quel niveau de surveillance la démocratie peut-elle endurer ?"
author: Richard Stallman (traduction Framablog)
date: 2013-10-20
href: http://www.framablog.org/index.php/post/2013/10/20/stallman-surveillance-democratie
tags:
- Internet
- Institutions
- Informatique-deloyale
---

> Le niveau de surveillance actuel dans nos sociétés est incompatible avec les droits de l’homme. Pour retrouver notre liberté et rétablir la démocratie, nous devons ramener la surveillance à un niveau qui permette à tout lanceur d’alerte de discuter avec des journalistes sans risquer d’être repéré. Pour y arriver de manière fiable, il nous faut réduire la capacité de surveillance des systèmes que nous utilisons.
