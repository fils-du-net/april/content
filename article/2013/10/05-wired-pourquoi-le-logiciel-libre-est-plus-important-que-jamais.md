---
site: Wired
title: "Pourquoi le logiciel libre est plus important que jamais"
author: Richard Stallman (traduction Framalang)
date: 2013-10-05
href: http://www.framablog.org/index.php/post/2013/10/05/stallman-logiciel-libre
tags:
- Logiciels privateurs
- Philosophie GNU
- Promotion
- Informatique en nuage
---

> Cela fait maintenant 30 ans que j’ai lancé la campagne pour la liberté en informatique, c’est-à-dire pour que le logiciel soit free ou «libre» (NdT: en français dans le texte — RMS utilise ce mot pour souligner le fait que l’on parle de liberté et non de prix). Certains programmes privateurs, tels que Photoshop, sont vraiment coûteux; d’autres, tels que Flash Player, sont disponibles gratuitement — dans les deux cas, ils soumettent leurs utilisateurs au pouvoir de quelqu’un d’autre.
