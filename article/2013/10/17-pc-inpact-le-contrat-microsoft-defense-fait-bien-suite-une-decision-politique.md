---
site: PC INpact
title: "Le contrat Microsoft-Défense «fait bien suite à une décision politique»"
author: Xavier Berne
date: 2013-10-17
href: http://www.pcinpact.com/news/83921-le-contrat-microsoft-defense-fait-bien-suite-a-decision-politique.htm
tags:
- Logiciels privateurs
- Administration
- April
- Marchés publics
---

> Pour l'April, le contrat «Open Bar» conclu il y a plus de quatre ans entre le ministère de la Défense et Microsoft «fait bien suite à une décision politique». L’association de promotion du logiciel libre étaye aujourd'hui ses affirmations à l’appui de documents internes à l'administration, obtenus suite à une procédure CADA.
