---
site: lefil
title: "Trois questions à Daniel Pascot"
author: Pascale Guéricolas
date: 2013-10-03
href: http://lefil.ulaval.ca/articles/trois-questions-daniel-pascot-35153.html
tags:
- Administration
- Économie
- Interopérabilité
- Institutions
- International
---

> Fin septembre, les députés de l’Assemblée nationale du Québec ont appuyé à l’unanimité par motion l’utilisation du logiciel libre dans l’administration publique. Ce choix ferait économiser 264 M$ selon l'Institut de recherche et d'informations socio-économiques (IRIS). Professeur au Département des systèmes d’information organisationnels, Daniel Pascot milite depuis une vingtaine d’années pour l’implantation du logiciel libre. Il approuve ce choix des élus du Québec.
