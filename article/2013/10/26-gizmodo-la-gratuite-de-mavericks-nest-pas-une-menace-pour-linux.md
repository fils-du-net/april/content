---
site: Gizmodo
title: "La gratuité de Mavericks n'est pas une menace pour Linux"
author: Morgan
date: 2013-10-26
href: http://www.gizmodo.fr/2013/10/26/gratuite-mavericks-pas-menace-linux.html
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Innovation
---

> Selon Linus Torvalds, le fait que Mac OS X Mavericks soit gratuit ne représente en rien une menace pour l'OS qu'il a créé, Linux.
