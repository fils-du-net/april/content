---
site: Le Cercle Les Echos
title: "Le libre n'est pas gratuit, il faut financer la R&amp;D"
author: Ludovic Dubost
date: 2013-10-22
href: http://lecercle.lesechos.fr/economie-societe/recherche-innovation/innovation/221182665/libre-nest-gratuit-faut-financer-rd
tags:
- Entreprise
- Économie
- Innovation
---

> Cet article traite du financement de la R&amp;D pour les logiciels libres. Lors de l'Open World Forum, j'ai eu l'occasion de participer à une table ronde concernant "l'achat" de logiciels libres.
