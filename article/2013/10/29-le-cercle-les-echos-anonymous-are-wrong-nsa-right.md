---
site: Le Cercle Les Echos
title: "Anonymous are wrong, NSA is right"
author: Éric Mahé et Jean-Marie Chauvet
date: 2013-10-29
href: http://lecercle.lesechos.fr/economie-societe/recherche-innovation/intelligence-economique/221183229/anonymous-are-wrong-nsa-is-r
tags:
- Internet
- Institutions
- Informatique-deloyale
- Standards
---

> Nous devrions donc, pour cette fois, remercier la NSA de nous avoir ouvert les yeux sur notre propre naïveté en matière de maîtrise des fondamentaux de la protection de notre patrimoine industriel et de la liberté de nos citoyens.
