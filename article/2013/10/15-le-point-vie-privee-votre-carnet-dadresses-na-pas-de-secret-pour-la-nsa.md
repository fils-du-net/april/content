---
site: Le Point
title: "Vie privée: Votre carnet d'adresses n'a pas de secret pour la NSA!"
date: 2013-10-15
href: http://www.lepoint.fr/high-tech-internet/la-nsa-collecte-des-centaines-de-millions-de-listes-de-contacts-numeriques-15-10-2013-1744028_47.php
tags:
- Internet
- Administration
- Informatique-deloyale
- International
---

> L'agence collecte des centaines de millions de listes de contacts numériques d'Américains via leurs courriels ou leurs messageries instantanées.
