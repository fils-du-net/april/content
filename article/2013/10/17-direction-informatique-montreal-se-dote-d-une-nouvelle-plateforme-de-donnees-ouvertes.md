---
site: Direction Informatique
title: "Montréal se dote d’une nouvelle plateforme de données ouvertes"
author: Benjamin Jébrak
date: 2013-10-17
href: http://www.directioninformatique.com/montreal-se-dote-dune-nouvelle-plateforme-de-donnees-ouvertes/22426
tags:
- Administration
- International
- Open Data
---

> À quelques jours de la tenue d’ÉcoHackMTL, la Ville de Montréal a mis en ligne une plateforme de données ouvertes qui est basée sur le logiciel libre CKAN.
