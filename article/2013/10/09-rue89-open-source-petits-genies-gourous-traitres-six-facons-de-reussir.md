---
site: Rue89
title: "Open source: petits génies, gourous, traîtres... Six façons de réussir"
author: Sabine Blanc
date: 2013-10-09
href: http://www.rue89.com/2013/10/09/open-source-petits-genies-gourous-traitres-six-facons-reussir-246371
tags:
- Entreprise
- Économie
- Matériel libre
- Associations
- Innovation
---

> La semaine dernière, Rome accueillait la première «Maker Faire» européenne, une immense foire grand public où les bidouilleurs en tous genres, amateurs comme professionnels, montrent leurs savoir-faire.
