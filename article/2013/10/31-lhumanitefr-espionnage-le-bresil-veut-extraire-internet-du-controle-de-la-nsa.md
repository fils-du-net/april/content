---
site: l'Humanité.fr
title: "Espionnage. Le Brésil veut extraire Internet du contrôle de la NSA"
author: Pi.M.
date: 2013-10-31
href: http://www.humanite.fr/monde/espionnage-le-bresil-veut-extraire-internet-du-con-552341
tags:
- Internet
- April
- Institutions
- Associations
- Informatique-deloyale
- Neutralité du Net
- International
---

> C’est l’ambition de Dilma Rousseff, la présidente du pays. Elle a relancé le mouvement d’une véritable constitution de l’Internet (Marco Civile), qui pourrait bien jeter les bases d’une neutralité du Net à l’échelle internationale et à la création d’infrastructures distribuées qui permettrait d'empêcher le contrôle d'une agence sur les réseaux.
