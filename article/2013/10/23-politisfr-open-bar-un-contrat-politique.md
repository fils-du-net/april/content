---
site: Politis.fr
title: "Open Bar: un contrat politique"
date: 2013-10-23
href: http://www.politis.fr/Open-Bar-un-contrat-politique,24190.html
tags:
- Logiciels privateurs
- Administration
- April
- Institutions
- Marchés publics
---

> Une branche du ministère de la Défense et Microsoft ont signé en 2009 un contrat opaque de gestion du matériel informatique.
