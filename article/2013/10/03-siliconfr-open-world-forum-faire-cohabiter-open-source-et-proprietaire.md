---
site: Silicon.fr
title: "Open World Forum: faire cohabiter Open Source et propriétaire"
author: Ariane Beky
date: 2013-10-03
href: http://www.silicon.fr/open-world-forum-2013-innovation-ouverte-89794.html
tags:
- Entreprise
- Économie
- Institutions
---

> Forum européen du Libre et de l’Open Source, l’Open World Forum a ouvert ses portes le 3 octobre à Paris. L’industrie, représentée par Syntec Numérique et le Cigref, s’est adressée à la communauté dès les keynotes. La ministre Fleur Pellerin est intervenue à distance.
