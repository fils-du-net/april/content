---
site: Numerama
title: "Le W3C valide l'ajout de DRM aux pages web"
author: Julien L.
date: 2013-10-03
href: http://www.numerama.com/magazine/27144-le-w3c-valide-l-ajout-de-drm-aux-pages-web.html
tags:
- Internet
- Associations
- DRM
- Standards
---

> La prochaine révision du HTML5 pourrait bien intégrer la présence des extensions de médias chiffrés, ouvrant la voie à l'intégration des DRM dans les pages web. Le W3C a confirmé cette direction, malgré les préoccupations de l'EFF.
