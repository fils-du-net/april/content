---
site: Developpez.com
title: "DRM et HTML5: Mozilla s'oppose farouchement aux travaux du W3C"
author: Hinault Romaric
date: 2013-10-14
href: http://www.developpez.com/actu/63058/DRM-et-HTML5-Mozilla-s-oppose-farouchement-aux-travaux-du-W3C
tags:
- Internet
- DRM
- Informatique-deloyale
- Standards
---

> La prise en charge des DRM dans le HTML5 continue à diviser les acteurs du Web. La fondation Mozilla s’oppose fermement aux travaux du W3C sur le support des DRM dans HTML5.
