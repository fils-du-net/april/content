---
site: La Tribune
title: "Mobile: avec Firefox OS, les opérateurs espèrent reprendre la main face au duopole Google-Apple"
author: Delphine Cuny
date: 2013-02-26
href: http://www.latribune.fr/technos-medias/telecoms/20130226trib000750968/mobile-avec-firefox-os-les-operateurs-esperent-reprendre-la-main-face-au-duopole-google-apple-.html
tags:
- Entreprise
- Internet
---

> Nouveau venu des systèmes d'exploitation pour mobile, Firefox OS est lancé sur des smartphones d'entrée de gamme des chinois TCL et ZTE. Les opérateurs mobiles espèrent diminuer leurs coûts de subventions, qui ont flambé avec l'iPhone et autres Galaxy SIII, et faire une incursion dans les applications.
