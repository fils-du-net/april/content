---
site: Framablog
title: "La propagande du copyright s’offre un nouvel acteur: votre fournisseur d’accès à Internet (FAI)"
author: Corynne McSherry (traduit par Framablog)
date: 2013-02-27
href: http://www.framablog.org/index.php/post/2013/02/27/hadopi-usa
tags:
- Entreprise
- Internet
- HADOPI
- Droit d'auteur
- International
---

> Voilà un moment qu’on le redoutait, la machine de surveillance du copyright connue sous le nom de Copyright Alert System (CAS) est finalement en marche. Le CAS est un accord entre les plus grands fournisseurs de contenus et les principaux fournisseurs d’accès (FAI) qui vise à surveiller les réseaux de peer-to-peer pour détecter la violation de copyright et de sanctionner les abonnés supposés coupables par des rappels à l’ordre éducatifs voire une réduction importante de la vitesse de connexion.
