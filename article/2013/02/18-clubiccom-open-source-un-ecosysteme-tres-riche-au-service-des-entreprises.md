---
site: Clubic.com
title: "Open source: un écosystème très riche au service des entreprises"
author: Philippe Richard
date: 2013-02-18
href: http://pro.clubic.com/it-business/article-542134-1-open-source-ecosysteme-riche-service-entreprises.html
tags:
- Entreprise
- Administration
- Économie
- Informatique en nuage
---

> Famille d'acteurs majeure du côté des serveurs et du Cloud Computing notamment, l'open source a mis une dizaine d'années pour convaincre de plus en plus d'entreprises. Exit son image d'amateurisme qui lui collait à la peau. Aujourd'hui, c'est un secteur d'activité en forte croissance. Une aubaine pour le «made in France» puisque de nombreux éditeurs nationaux proposent des solutions innovantes.
