---
site: PC INpact
title: "Open bar entre Microsoft et la Défense: le libre ne veut pas trinquer"
author: Marc Rees
date: 2013-02-06
href: http://www.pcinpact.com/news/77326-open-bar-entre-microsoft-et-defense-libre-ne-veut-pas-trinquer.htm
tags:
- Administration
- April
- Institutions
- Marchés publics
---

> Hier, PC INpact dévoilait un document rédigé en amont de la commission des marchés publics. Cette version montrait les doutes exprimés par le rapporteur quant à l’accord-cadre signé entre le ministère de la Défense et Microsoft. L’April a vivement réagi suite à cette diffusion. Dans le même temps, on apprend que l’accord-cadre pourrait être étendu à d’autres ministères.
