---
site: "cio-online.com"
title: "L'informatique du Ministère de la Défense sous le tir croisé de deux polémiques"
author: Bertrand Lemaire
date: 2013-02-17
href: http://www.cio-online.com/actualites/lire--4918.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- April
- Marchés publics
---

> Le Ministère de la Défense a une informatique doublement stigmatisée. Bogues à répétitions sur le logiciel de paye Louvois et contrat avec Microsoft alimentent les polémiques.
