---
site: ActuaLitté.com
title: "DRM: Trois libraires pour renverser le monopole d'Amazon"
author: Nicolas Gary
date: 2013-02-22
href: http://www.actualitte.com/usages/drm-trois-libraires-pour-renverser-le-monopole-d-amazon-40492.htm
tags:
- Entreprise
- Interopérabilité
- HADOPI
- Institutions
- DRM
- International
---

> C'est un retour de bâton terrible qui a été lancé hier, alors que trois librairies indépendantes annonçaient avoir porté plainte contre Amazon et les Big Six, les six grands groupes d'édition américains. Fiction Addiction de Greenville, S.C., Book House basée dans la ville d'Albanie,(Etat de New York) et Posman Books, qui détient trois établissements dans New York, devenus les nouveaux chevaliers blancs?
