---
site: PC INpact
title: "VLC, Hadopi et DRM: sans les clefs, quelle interopérabilité?"
author: Marc Rees
date: 2013-02-11
href: http://www.pcinpact.com/news/77459-vlc-hadopi-et-drm-sans-clefs-quelle-interoperabilite.htm
tags:
- Interopérabilité
- HADOPI
- Associations
- DRM
- Video
---

> L’association VideoLAN a eu la surprise de découvrir la consultation lancée par la Hadopi sur son dossier VLC. Dans un billet d'opinion, Jean-Baptiste Kempf, membre de l'équipe de développement estime que cette consultation est un non-sens. Et il réexplique ouvertement ses positions déjà exprimées dans le mémoire initial de VidéoLan.
