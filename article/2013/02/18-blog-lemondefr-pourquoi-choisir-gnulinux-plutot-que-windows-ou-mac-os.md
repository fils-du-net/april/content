---
site: Blog LeMonde.fr
title: "Pourquoi choisir GNU/Linux? (plutôt que Windows ou Mac OS)"
author: Davie Labrousserie
date: 2013-02-18
href: http://alasource.blog.lemonde.fr/2013/02/18/pourquoi-choisir-gnulinux-plutot-que-windows-ou-mac-os
tags:
- Internet
- Sensibilisation
- Innovation
- Philosophie GNU
---

> Tout a commencé par un Unix dans mon laboratoire, qui m’a conduit à installer une Red Hat sur mon PC à la maison. Puis j’ai opté pour un logiciel dérivé, Mandrake, devenu ensuite Mandriva. A la disparition de celle-ci j’ai testé un temps Kubuntu avant d’opter pour Linux Mint (en version KDE ou Lxde pour une machine moins puissante). J'ai même du GNU/Linux sur mon Raspberry Pi, le petit ordinateur à 40 euros.
> Pourquoi?
