---
site: ZDNet.fr
title: "Des points tabous dans le logiciel libre?"
author: Thierry Noisette
date: 2013-02-23
href: http://www.zdnet.fr/actualites/des-points-tabous-dans-le-logiciel-libre-39787536.htm
tags:
- Le Logiciel Libre
---

> Fin de l'enthousiasme suscité par Ubuntu, impact du cloud computing, chantiers inachevés, faible place des femmes... Un article du journaliste Bruce Byfield relève neuf sujets qui selon lui ne sont pas ou peu abordés dans le monde des logiciels libres.
