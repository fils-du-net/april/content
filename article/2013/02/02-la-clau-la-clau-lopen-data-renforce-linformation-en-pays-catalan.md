---
site: la clau
title: "La Clau - L'open data renforce l'information en Pays Catalan"
date: 2013-02-02
href: http://www.la-clau.net/info/7888/lopen-source-renforce-linformation-en-pays-catalan-7888
tags:
- Internet
- Administration
- Open Data
---

> Quelques clics suffisent pour accéder à de nombreuses statistiques officielles sur les Pyrénées-Orientales, parfois écartées par la presse traditionnelle. L'open data, en cours de généralisation dans le monde, permet un renforcement des pratiques démocratiques : du producteur au consommateur, les informations circulent sans l'interprétation des intermédiaires.
