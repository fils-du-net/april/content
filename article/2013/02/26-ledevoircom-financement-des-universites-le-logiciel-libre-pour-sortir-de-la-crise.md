---
site: LeDevoir.com
title: "Financement des universités: le logiciel libre pour sortir de la crise"
author: Fabien Deglise
date: 2013-02-26
href: http://www.ledevoir.com/societe/education/371867/financement-des-universites-le-logiciel-libre-pour-sortir-de-la-crise
tags:
- Logiciels privateurs
- Administration
- Économie
- Associations
- Éducation
- International
---

> Sortir des ornières pour sortir de la crise. En marge du Sommet sur l’enseignement supérieur qui a débuté lundi à Montréal, l’Association pour l’appropriation collective de l’informatique libre (FACIL) invite les universités du Québec à remettre en question leur dépendance coûteuse envers les géants de l’informatique privative, comme Microsoft, pour mettre leur parc informatique au diapason avec le logiciel dit libre.
