---
site: Joinup
title: "French advocacy group decries Defence's ignoring of procurement law"
author: Gijs Hillenius
date: 2013-02-08
href: http://joinup.ec.europa.eu/news/french-advocacy-group-decries-defences-ignoring-procurement-law
tags:
- Administration
- April
- Institutions
- Marchés publics
- English
---

> (Le groupe de défense des logiciels libres Aril proteste contre les renégociations par le ministère de la défense du contrat de licence de logiciel propriétaire qui a été signé il y a 4 ans sans marché public. Le groupe demande au ministère de stopper les discussions avec le vendeur et d'organiser une offre de marché public à la place) The French free software advocacy group April is protesting the renegotiation by the Ministry of Defence of a proprietary software licence contract that was signed four years ago without a public procurement.
