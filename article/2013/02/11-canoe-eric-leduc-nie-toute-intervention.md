---
site: canoe
title: "Éric Leduc nie toute intervention"
author: Jean-Nicolas Blanchet
date: 2013-02-11
href: http://argent.canoe.ca/lca/affaires/quebec/archives/2013/02/eric-leduc-nie-toute-intervention.html
tags:
- Entreprise
- Administration
- Économie
- Institutions
- International
---

> «C'est faux et erroné de mentionner que je sois impliqué d'une quelconque façon par n'importe quel dossier entre le gouvernement et Microsoft.»
