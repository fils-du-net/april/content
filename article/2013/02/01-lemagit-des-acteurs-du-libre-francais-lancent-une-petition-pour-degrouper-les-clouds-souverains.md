---
site: LeMagIT
title: "Des acteurs du Libre français lancent une pétition pour dégrouper les clouds souverains"
author: Cyrille Chausson
date: 2013-02-01
href: http://www.lemagit.fr/economie/business/2013/02/01/des-acteurs-du-libre-francais-lancent-une-petition-pour-degrouper-les-clouds-souverains
tags:
- Entreprise
- Administration
- Associations
- Informatique en nuage
---

> Certains membres de la communauté du cloud et du libre en France, associations et PME, ont décidé de monter au créneau face aux cloud souverains. A la clé, une pétition demandant à la ministre de l’Economie numérique Fleur Pellerin le «dégroupage» de ces infrastructures issues notamment de financement de l’Etat.
