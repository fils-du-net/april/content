---
site: OuiShare
title: "Michel Bauwens: “Le modèle P2P et le capitalisme sont encore interdépendants”"
author: Emile Hooge
date: 2013-02-26
href: http://ouishare.net/fr/2013/02/michel-bauwens-capitalisme-peer-p2p
tags:
- Entreprise
- Économie
- Matériel libre
- Innovation
---

> Selon Michel Bauwens, l'économie en peer to peer et l'ancien monde capitaliste sont en conflit mais demeurent encore interdépendants.
