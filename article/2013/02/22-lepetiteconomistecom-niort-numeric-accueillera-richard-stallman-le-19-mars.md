---
site: lepetiteconomiste.com
title: "Niort NUMERIC accueillera Richard Stallman le 19 mars"
date: 2013-02-22
href: http://lepetiteconomiste.com/Niort-NUMERIC-accueillera-Richard-4156
tags:
- Entreprise
- Administration
- Économie
- Sensibilisation
- Philosophie GNU
- Promotion
---

> Niort NUMERIC a pour but, lors d’une journée événement, de faire connaître les formations et les métiers du numérique, à valoriser les savoir-faire des entreprises locales et régionales dans ce domaine, à sensibiliser les professionnels de tous secteurs aux usages et technologies numériques et plus globalement à mettre en valeur le potentiel d’attractivité du Niortais.
