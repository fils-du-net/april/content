---
site: Le Huffington Post
title: "Mario Asselin: Ma première semaine dans les couloirs de l'Assemblée nationale"
author: Mario Asselin
date: 2013-02-15
href: http://quebec.huffingtonpost.ca/mario-asselin/ma-premiere-semaine-dans-les-couloirs-de-l-assemblee-nationale_b_2695457.html
tags:
- Administration
- Économie
- Institutions
- International
---

> Mon statut professionnel a changé depuis mon dernier billet. Je suis devenu conseiller politique à temps plein de l'Aile parlementaire du deuxième groupe d'opposition. J'emprunte un autre véhicule (celui de la politique), mais je garde le cap sur les mêmes préoccupations: la gouvernance des ressources informationnelles et technologiques, l'enseignement supérieur et l'éducation.
