---
site: Le Monde Informatique
title: "Etats généraux de l'Open Source, suivi en trois étapes jusqu'à l'automne"
author: Maryse Gros
date: 2013-02-07
href: http://www.lemondeinformatique.fr/actualites/lire-etats-generaux-de-l-open-source-suivi-en-trois-etapes-jusqu-a-l-automne-52405.html
tags:
- Entreprise
- Institutions
- Associations
---

> Réunis à l'initiative de Syntec Numérique, les Etats généraux de l'Open Source devraient déboucher sur la constitution de groupes de travail qui étudieront jusqu'en juillet les différentes propositions recueillies. Une convention de restitution devrait se tenir à l'automne.
