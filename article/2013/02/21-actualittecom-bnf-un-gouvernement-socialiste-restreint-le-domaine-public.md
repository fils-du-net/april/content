---
site: ActuaLitté.com
title: "BnF: Un gouvernement socialiste restreint le domaine public"
author: Claire Le Strat et Olivier Michel
date: 2013-02-21
href: http://www.actualitte.com/bibliotheques/bnf-un-gouvernement-socialiste-restreint-le-domaine-public-40466.htm
tags:
- Entreprise
- Internet
- Économie
- April
- Partage du savoir
- Institutions
- Droit d'auteur
- Contenus libres
---

> Ou comment la paysannerie britannique ressemble étrangement aux accords passés par la BnF
