---
site: Le Monde.fr
title: "A qui appartient le savoir?"
author: Sandrine Cabut et David Larousserie
date: 2013-02-28
href: http://www.lemonde.fr/sciences/article/2013/02/28/a-qui-appartient-le-savoir_1840797_1650684.html
tags:
- Entreprise
- Internet
- Économie
- Éducation
- Sciences
- Contenus libres
---

> Appels au boycottage des grands éditeurs scientifiques, création de revues en ligne en libre accès: face à ce qu'ils considèrent comme une appropriation indue de leurs travaux, des chercheurs se mobilisent pour renverser le modèle traditionnel de diffusion des connaissances.
