---
site: Sur la Toile
title: "Installer Linux cohabitant avec Windows 8 c'est possible? Oui!"
author: toy31
date: 2013-02-16
href: http://www.sur-la-toile.com/article-17702-Installer-Linux-cohabitant-avec-Windows-8-c-est-possible-Oui.html
tags:
- Entreprise
- Interopérabilité
- Associations
- DRM
---

> La Linux Foundation a publié la semaine dernière une nouvelle solution pour prendre en charge le controversé Secure Boot. Elle a également annoncé le développement d'une solution universelle combinant les avantages des deux existantes. Installer Linux sur un ordinateur certifié Windows 8 n’entraînera alors plus aucune complication.
