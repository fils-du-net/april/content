---
site: L'usine Nouvelle
title: "MWC 2013: Ford passe son véhicule connecté en open source"
author: Emmanuelle Delsol
date: 2013-02-27
href: http://www.usinenouvelle.com/article/mwc-2013-ford-passe-son-vehicule-connecte-en-open-source.N192345
tags:
- Entreprise
- Innovation
---

> A Barcelone, L'Usine Nouvelle a rencontré Paul Mascarenas, CTO et vice-président de Ford pour la recherche et l'ingénierie avancée. Il commente le passage en open source de son système AppLink et la façon dont il conçoit l'innovation automobile à l'heure du numérique.
