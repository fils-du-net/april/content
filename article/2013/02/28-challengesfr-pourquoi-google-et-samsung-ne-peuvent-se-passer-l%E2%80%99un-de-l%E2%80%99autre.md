---
site: challenges.fr
title: "Pourquoi Google et Samsung ne peuvent se passer l’un de l’autre"
author: Paul Loubière
date: 2013-02-28
href: http://www.challenges.fr/high-tech/20130228.CHA6848/pourquoi-google-et-samsung-ne-peuvent-pas-se-passer-l-un-de-l-autre.html
tags:
- Entreprise
- Brevets logiciels
---

> DECRYPTAGE Avec près de 40% des smartphones et tablettes équipés d'Android, le constructeur coréen est devenu plus q'un client pour Google.
