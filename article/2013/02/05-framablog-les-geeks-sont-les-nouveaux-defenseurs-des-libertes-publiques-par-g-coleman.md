---
site: Framablog
title: "Les geeks sont les nouveaux défenseurs des libertés publiques, par G. Coleman"
author: Gabriella Coleman
date: 2013-02-05
href: http://www.framablog.org/index.php/post/2013/02/05/geeks-libertes-publiques-coleman
tags:
- Internet
- Institutions
---

> Des évènements récents ont mis en évidence le fait que les hackers, les développeurs et les geeks sont porteurs d’une culture politique dynamique.
