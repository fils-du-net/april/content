---
site: Framablog
title: "Du bon usage des deniers publics par les municipalités danoises"
author: Gijs Hillenius (traduit par Framablog)
date: 2013-02-21
href: http://www.framablog.org/index.php/post/2013/02/21/danemark-open-source
tags:
- Internet
- Administration
- International
---

> Une douzaine de villes danoises se sont mises d’accord pour développer ensemble des solutions libres en partenariat avec des sociétés de services locales.
