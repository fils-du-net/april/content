---
site: Acrimed
title: "Quand la Bibliothèque nationale de France privatise le domaine public"
author: Jean Pérès
date: 2013-02-08
href: http://www.acrimed.org/article3997.html
tags:
- Entreprise
- Administration
- Institutions
- Droit d'auteur
---

> C’est le 15 janvier 2013, qu’ Aurélie Filippetti, ministre de la culture et de la communication, Louis Gallois, commissaire général à l’investissement, et Bruno Racine, président de la Bibliothèque nationale de France, se « félicitent de la conclusion de deux nouveaux accords de numérisation et de diffusion des collections de la BnF, couvrant les livres anciens et les fonds musicaux » [1]. Ils ont été bien avisés de se féliciter eux-mêmes car à ce jour, personne ne s’est précipité pour se joindre à eux ; bien au contraire : de tous côtés, les protestations fusent.
