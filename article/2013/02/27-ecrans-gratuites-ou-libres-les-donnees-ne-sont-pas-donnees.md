---
site: écrans
title: "Gratuites ou libres? Les données ne sont pas données"
author: Simon Descarpentries et Yohan Boniface
date: 2013-02-27
href: http://www.ecrans.fr/Gratuites-ou-libres-Les-donnees-ne,16018.html
tags:
- Administration
- Partage du savoir
- Innovation
- Open Data
---

> Une certaine confusion règne autour de la notion de « données libres ». Dans une récente tribune, en réponse à une demande d’ouverture des données de bien commun, l’Institut géographique national (IGN) avance la mise à disposition gratuite d’une partie du patrimoine géographique. Il est urgent de moderniser cette approche.
