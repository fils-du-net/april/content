---
site: Le Matin
title: "MWC 2013: Les mobiles Firefox doivent ruser contre les brevets"
author: Simon Koch
date: 2013-02-28
href: http://www.lematin.ch/high-tech/hard-software/Les-mobiles-Firefox-doivent-ruser-contre-les-brevets/story/21426760
tags:
- Entreprise
- Brevets logiciels
- Innovation
---

> Dessiner une nouvelle interface des smartphones est un parcours du combattant en raison des nombreux brevets déposés par Apple, Google &amp; Co. Le designer de Firefox OS a relevé ce défi. Rencontre.
