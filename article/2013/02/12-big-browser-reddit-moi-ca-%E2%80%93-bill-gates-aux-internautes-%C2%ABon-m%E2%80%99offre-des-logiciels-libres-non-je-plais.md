---
site: Big Browser
title: "REDDIT-MOI ÇA – Bill Gates aux internautes: «On m’offre des logiciels libres. Non, je plaisante.»"
date: 2013-02-12
href: http://bigbrowser.blog.lemonde.fr/2013/02/12/reddit-moi-ca-bill-gates-aux-internautes-on-moffre-des-logiciels-libres-non-je-plaisante
tags:
- Entreprise
- Logiciels privateurs
---

> Quand Salacious lui demande "ce qu'on [lui] offre pour son anniversaire, vu [qu'il peut] acheter ce qu'il veut", Bill Gates, symbole avec Microsoft du copyright et pour cela honni par les partisans du libre, répond avec humour: "Des logiciels libres. Non, je plaisante. Des livres, en fait."
