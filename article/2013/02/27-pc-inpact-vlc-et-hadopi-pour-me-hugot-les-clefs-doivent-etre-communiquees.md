---
site: PC INpact
title: "VLC et Hadopi: pour Me Hugot, les clefs doivent être communiquées"
author: Marc Rees
date: 2013-02-27
href: http://www.pcinpact.com/news/77853-vlc-et-hadopi-pour-me-hugot-clefs-doivent-etre-communiquees.htm
tags:
- HADOPI
- DADVSI
- DRM
- Droit d'auteur
---

> La «deadline» mise par la HADOPI pour répondre à sa consultation sur les DRM est épuisée depuis le 26 février. Nous avons pu nous procurer la contribution de Me Olivier Hugot qui a été adressée ce jour à la Rue de Texel.
