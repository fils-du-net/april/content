---
site: Métro
title: "Le logiciel libre peine à s’installer à Montréal"
author: Mathias Marchal
date: 2013-02-11
href: http://journalmetro.com/actualites/montreal/237449/le-logiciel-libre-peine-a-sinstaller-a-montreal
tags:
- Logiciels privateurs
- Administration
- Institutions
- Marchés publics
---

> La Ville de Montréal continue de négliger les logiciels libres, selon certains. Une option jugée pourtant flexible et économique. La Ville se défend de mal agir.
