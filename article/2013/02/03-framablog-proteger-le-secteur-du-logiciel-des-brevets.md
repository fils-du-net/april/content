---
site: Framablog
title: "Protéger le secteur du logiciel des brevets"
author: Richard Stallman
date: 2013-02-03
href: http://www.framablog.org/index.php/post/2013/02/03/brevets-logiciels-stallman
tags:
- Institutions
- Brevets logiciels
- English
---

> Les brevets menacent chaque concepteur de logiciel, et les guerres de brevet que nous avons longtemps craintes ont éclaté. Les développeurs et les utilisateurs – soit, dans notre société, la plupart des gens – ont besoin de logiciels libres de tout brevet.
