---
site: "Ecolo-Info"
title: "7 raisons pour lesquelles économie collaborative et écologie vont de pair"
author: Anne-Sophie
date: 2013-02-28
href: http://www.ecoloinfo.com/2013/02/28/7-raisons-qui-prouvent-queconomie-collaborative-et-ecologie-vont-de-pair
tags:
- Entreprise
- Économie
- Innovation
- Sciences
---

> L'économie collaborative qualifie l'ensemble des initiatives de consommation qui aujourd'hui permettent de louer, échanger, donner, troquer, revendre entre particuliers. Elle englobe également la finance participative et la production entre pairs, et elle fonctionne parfois pour certains projets selon les préceptes de l'open source. Décryptage.
