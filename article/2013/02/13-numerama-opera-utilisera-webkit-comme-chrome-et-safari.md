---
site: Numerama
title: "Opera utilisera WebKit, comme Chrome et Safari"
author: Julien L.
date: 2013-02-13
href: http://www.numerama.com/magazine/25075-opera-utilisera-webkit-comme-chrome-et-safari.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
---

> La décision est officielle. En 2013, Opera va abandonner le moteur de rendu HTML Presto au profit de WebKit. L'entreprise norvégienne estime qu'il est préférable de contribuer à un projet libre et ouvert, plutôt que de poursuivre dans son coin avec sa propre solution. En dehors d'Opera, WebKit est utilisé par Safari, Google Chrome et Chromium.
