---
site: Mediapart
title: "La révolution informationnelle antichambre autogestionnaire du communisme 2.0?"
author: communistes-unitaires
date: 2013-02-09
href: http://blogs.mediapart.fr/blog/communistes-unitaires/090213/la-revolution-informationnelle-antichambre-autogestionnaire-du-communisme-20
tags:
- Économie
- Institutions
---

> Dans son livre Une autre façon de faire de la politique, Jean Lojkine, sociologue et directeur de recherche émérite au CNRS, explore les transformations induites par les nouvelles technologies de l’information, dans la société et dans le combat pour la transformation de la société. Il plaide pour une stratégie autogestionnaire qui suppose de révolutionner la politique.
