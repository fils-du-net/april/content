---
site: canoe
title: "Québec: les logiciels libres écartés?"
author: Jean-Nicolas Blanchet
date: 2013-02-11
href: http://argent.canoe.ca/lca/affaires/quebec/archives/2013/02/quebec-les-logiciels-libres-ecartes.html
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Marchés publics
- International
---

> Le gouvernement Marois s'apprête à accorder des contrats de près d'un milliard de dollars pour la migration des centaines de milliers de postes informatiques vers les récents produits Microsoft.
