---
site: 01netPro.
title: "Plaidoyer pour un renouveau du numérique en France"
author: Vincent Bouthors
date: 2013-02-13
href: http://pro.01net.com/editorial/586163/plaidoyer-pour-un-renouveau-du-numerique-en-france-2
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Informatique en nuage
- International
---

> En France, les grands acteurs informatique sont des intégrateurs alors qu'aux Etats-Unis, ce sont plutôt des éditeurs. Ces derniers auraient pourtant un rôle non négligeable à jouer dans le renouveau du numérique dans l'Hexagone.
