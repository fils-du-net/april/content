---
site: Framablog
title: "10 propositions pour débuter dans le Libre (sans avoir rien à coder)"
author: Jason Hibbets (traduit par Framablog)
date: 2013-02-19
href: http://www.framablog.org/index.php/post/2013/02/19/10-facons-commencer-open-source
tags:
- Sensibilisation
- Standards
---

> Par expérience, je sais qu’un grand nombre de personnes veulent découvrir et participer à l‘open source, mais ne savent pas par où commencer; et l’idée que l’on est obligé d’écrire du code pour contribuer à un projet open source constitue une véritable barrière. J’ai donc esquissé 10 façons de commencer avec l‘open source et ce sans jamais écrire une seule ligne de code.
