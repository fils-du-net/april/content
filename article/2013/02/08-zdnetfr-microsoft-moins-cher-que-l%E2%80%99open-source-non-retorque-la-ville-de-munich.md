---
site: ZDNet.fr
title: "Microsoft moins cher que l’Open Source? Non rétorque la ville de Munich"
author: Thierry Noisette
date: 2013-02-08
href: http://www.zdnet.fr/actualites/microsoft-moins-cher-que-l-open-source-non-retorque-la-ville-de-munich-39787057.htm
tags:
- Entreprise
- Logiciels privateurs
- Administration
- International
---

> Microsoft et la municipalité de Munich s’affrontent sur le coût du projet LiMux, un vaste projet de migration vers Ubuntu et OpenOffice. Pour l’éditeur, le coût réel est de 60 millions, contre 23 pour la ville.
