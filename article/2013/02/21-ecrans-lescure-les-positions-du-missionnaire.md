---
site: écrans
title: "Lescure: les positions du missionnaire"
author: Sophian Fanen
date: 2013-02-21
href: http://www.ecrans.fr/Lescure-les-positions-du,15982.html
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Institutions
- Licenses
---

> La montagne était haute, mais, derrière elle, se dresse un autre sommet à gravir. Pierre Lescure et son équipe, chargés depuis septembre de rendre des propositions pour adapter le monde de la culture à l’économie numérique, ont achevé un long cycle d’auditions tous azimuts et doivent désormais donner une forme à leurs propositions finales. Prévues pour mars, celles-ci ne devraient finalement pas aboutir avant le mois d’avril, selon nos informations.
