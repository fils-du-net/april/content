---
site: Framablog
title: "...et c'est le modèle ouvert qui l'emporte à la fin? — ça dépend…"
author: Tim Wu (traduit par Framablog)
date: 2013-02-20
href: http://www.framablog.org/index.php/post/2013/02/20/modele-ouvert-ou-ferme
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Interopérabilité
- Innovation
---

> On dit depuis un bon moment dans le milieu techno que «le modèle ouvert l’emporte sur le modèle fermé». En d’autres termes, les systèmes technologiques ouverts, ou bien ceux qui permettent l’interopérabilité, finissent toujours par surpasser leurs concurrents fermés. C’est une véritable profession de foi chez certains ingénieurs. C’est aussi la leçon qu’on peut tirer de l’échec de MacIntosh face à Windows dans les années 90, du triomphe de Google au début des années 2000 et plus largement, de la victoire d’Internet sur ses rivaux au modèle fermé (vous souvenez-vous d’AOL?).
