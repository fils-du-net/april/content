---
site: Libération.fr
title: "«Le domaine public est aussi un moteur économique»"
author: S.F.A.
date: 2013-02-01
href: http://www.liberation.fr/culture/2013/02/01/le-domaine-public-est-aussi-un-moteur-economique_878691
tags:
- Entreprise
- Économie
- Droit d'auteur
---

> Conservateur à la Bibliothèque d’histoire internationale contemporaine, juriste et cofondateur de la plateforme SavoirsCom1 qui se mobilise pour la défense des «biens communs de l’existence», Lionel Maurel décrypte régulièrement sur son blog (1) les attaques qui visent le domaine public.
