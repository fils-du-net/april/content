---
site: challenges.fr
title: "Pourquoi les logiciels libres se lancent à l’assaut du mobile"
author: Paul Loubière
date: 2013-02-26
href: http://www.challenges.fr/entreprise/20130226.CHA6657/pourquoi-les-logiciels-libres-se-lancent-a-l-assaut-du-mobile.html
tags:
- Entreprise
- Internet
- Associations
---

> Les smartphones sont devenus de véritables petits PC. Leur "système d'exploitation" attire les professionnels du logiciel libre. Démonstration au Mobile World Congress de Barcelone.
