---
site: Framablog
title: "Débat: 9 points (ou tabous?) jamais (ou rarement) discutés dans le logiciel libre"
author: Bruce Byfield
date: 2013-02-08
href: http://www.framablog.org/index.php/post/2013/02/08/9-tabous-open-source
tags:
- Associations
- Innovation
---

> Quels sont les sujets tabous dans l‘open source de nos jours? Certains peuvent se deviner mais d’autres pourraient bien vous surprendre.
