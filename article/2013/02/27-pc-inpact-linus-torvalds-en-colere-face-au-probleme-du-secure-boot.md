---
site: PC INpact
title: "Linus Torvalds en colère face au problème du Secure Boot"
author: Vincent Hermann
date: 2013-02-27
href: http://www.pcinpact.com/news/77836-linus-torvalds-en-colere-face-au-probleme-secure-boot.htm
tags:
- DRM
---

> Depuis des mois maintenant, plusieurs développeurs ou sociétés travaillent sur des méthodes pour permettre à Linux de démarrer sur un PC équipé du Secure Boot. Plusieurs techniques existent, mais lorsqu’un développeur pose la question de l’intégration des clés dans le noyau, Linus Torvalds se fâche pour de bon.
