---
site: Bulletins Electroniques
title: "Développer les technologies de l'information libres à Berlin"
date: 2013-02-21
href: http://www.bulletins-electroniques.com/actualites/72316.htm
tags:
- Entreprise
- Innovation
- Standards
- International
---

> La Fondation pour la technologie de Berlin (TSB) et l'Open Source Business Alliance (OSB Alliance) soutiendront à présent les entreprises et les créateurs d'entreprises dans la mise en valeur des technologies d'information ouvertes.
