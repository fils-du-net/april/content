---
site: Slate.fr
title: "Numérique: le changement, c'est quand?"
author: Titiou Lecoq
date: 2013-02-07
href: http://www.slate.fr/story/68079/internet-libre-numerique-projet-gouvernement
tags:
- Internet
- Économie
- Institutions
- Éducation
- Neutralité du Net
---

> Accord avec Google, numérisation du fonds de la BNF, affaire Free, accueil du rapport Colin/Collin... Les derniers évènements autour du numérique sèment le doute sur la volonté du gouvernement de définir un projet d'ensemble sur le sujet et de défendre un Internet libre.
