---
site: Direction Informatique
title: "Renouvellement de licences: Un air de déjà-vu pour Cyrille Béraud"
author: Jean-François Ferland
date: 2013-02-11
href: http://www.directioninformatique.com/renouvellement-de-licences-un-air-de-deja-vu-pour-cyrille-beraud/17320
tags:
- Administration
- Institutions
- Associations
- Marchés publics
- International
---

> L’un des principaux acteurs québécois du logiciel libre est consterné que le gouvernement provincial n’ait pas fait d’appel d’offres pour le renouvellement de licences de bureautique au Comité exécutif.
