---
site: PC INpact
title: "Quand un contrat entre Microsoft et la Défense prenait des coups de canon"
author: Marc Rees
date: 2013-02-05
href: http://www.pcinpact.com/news/77280-quand-contrat-entre-microsoft-et-defense-prenait-coups-canon.htm
tags:
- Entreprise
- Administration
- April
- Institutions
- Marchés publics
---

> En avril 2008, nous révélions que Microsoft avait proposé à deux ministères au moins une offre «open bar». Contre une somme de quelques dizaines d’euros environ par poste, le ministère pouvait puiser dans toutes les solutions Microsoft, applications et même solutions de développements. Le marché de 4 ans reposait en fait sur un droit d’usage. À terme, le ministère profitait d’une option d’achat pour l’acquisition de l’ensemble des logiciels. Problème: tout n’a pas été aussi simple dans ce contrat passé sans appel d’offres ni procédure publique, mais actuellement renégocié.
