---
site: PC INpact
title: "Droit de lire de VLC : une consultation et un premier avis de la Hadopi"
author: Marc Rees
date: 2013-02-07
href: http://www.pcinpact.com/news/77329-droit-lire-vlc-consultation-et-premier-avis-hadopi.htm
tags:
- HADOPI
- DRM
- Video
---

> La Hadopi vient de lancer une consultation ouverte pour plancher sur le dossier VLC. Ce dossier touche à la possibilité pour le fameux lecteur de lire les disques Blu-ray, ce format garni de DRM. Dans son appel, la Hadopi a d'ores et déjà donné son sentiment sur ce dossier.
