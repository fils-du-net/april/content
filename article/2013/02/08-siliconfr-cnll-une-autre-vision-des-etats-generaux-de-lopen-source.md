---
site: Silicon.fr
title: "CNLL: Une autre vision des États Généraux de l'Open Source"
author: Ariane Beky
date: 2013-02-08
href: http://www.silicon.fr/patrice-bertrand-cnll-etats-generaux-open-source-83372.html
tags:
- Entreprise
- Administration
- Institutions
- Associations
- Éducation
---

> Le président du Conseil national du logiciel libre (CNLL) Patrice Bertrand expose sa vision des premiers États Généraux de l’Open Source organisés à Bercy en janvier 2013.
