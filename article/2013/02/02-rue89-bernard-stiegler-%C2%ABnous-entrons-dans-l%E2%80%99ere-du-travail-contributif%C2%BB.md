---
site: Rue89
title: "Bernard Stiegler: «Nous entrons dans l’ère du travail contributif»"
author: Elsa Fayner
date: 2013-02-02
href: http://www.rue89.com/2013/02/02/bernard-stiegler-nous-entrons-dans-lere-du-travail-contributif-238900
tags:
- Entreprise
- Économie
- Sciences
---

> Fab’ lab’, imprimantes 3D... «Le consumérisme a vécu», assène le philosophe pour qui, motivés par nos seuls centres d’intérêt, nous allons changer de mode de travail.
