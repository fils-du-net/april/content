---
site: Silicon.fr
title: "Quiz – Connaissez-vous les logiciels open source?"
author: David Feugey
date: 2013-02-22
href: http://www.silicon.fr/quiz-silicon-fr-connaissez-vous-les-logiciels-open-source-83791.html
tags:
- Sensibilisation
- Licenses
---

> Notre quiz se penche sur les applications open source. Leur nom, leur histoire, leurs anecdotes. Dix questions pour aborder ce sujet populaire.
