---
site: Numerama
title: "Bill Gates dit pour plaisanter qu'on lui offre des logiciels libres"
author: Julien L.
date: 2013-02-12
href: http://www.numerama.com/magazine/25052-bill-gates-dit-pour-plaisanter-qu-on-lui-offre-des-logiciels-libres.html
tags:
- Entreprise
- Logiciels privateurs
---

> Lors d'un échange avec les membres de Reddit, Bill Gates a été interrogé sur la nature des cadeaux qu'il reçoit à Noël et à son anniversaire, vu qu'il est très riche et peut en conséquence s'acheter ce qu'il veut. En plaisantant et un brin provocateur, le fondateur de Microsoft a expliqué que ses proches lui offrent généralement des logiciels libres.
