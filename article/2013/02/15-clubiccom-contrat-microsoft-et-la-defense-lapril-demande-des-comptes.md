---
site: Clubic.com
title: "Contrat Microsoft et la Défense: l'April demande des comptes"
author: Thomas Pontiroli
date: 2013-02-15
href: http://pro.clubic.com/entreprises/microsoft/actualite-541726-microsoft-open-bar.html
tags:
- Administration
- April
- Institutions
- Marchés publics
---

> L'accord-cadre liant Microsoft au ministère de la Défense arrive bientôt à terme. Signé en 2009 sans appel d'offres, ce qui aurait permis aux concurrents open source d'avoir une chance, il serait en passe d'être reconduit. Depuis, le gouvernement a pourtant signé une circulaire en faveur des logiciels libres…
