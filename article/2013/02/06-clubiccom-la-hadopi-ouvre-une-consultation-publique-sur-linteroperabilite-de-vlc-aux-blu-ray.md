---
site: clubic.com
title: "La Hadopi ouvre une consultation publique sur l'interopérabilité de VLC aux Blu-Ray"
author: Olivier Robillart
date: 2013-02-06
href: http://pro.clubic.com/legislation-loi-internet/hadopi/actualite-539668-hadopi-vlc.html
tags:
- Interopérabilité
- HADOPI
- DRM
- Standards
- Video
---

> La Hadopi demande aux personnes disposant d'une expertise dans le domaine des DRM et de l'interopérabilité de participer à une consultation publique portant sur VLC. VideoLAN, l'éditeur du logiciel a demandé à l'autorité qu'elle lui délivre une autorisation afin qu'il puisse déchiffrer l'ensemble des protections présentes sur les disques Blu-ray.
