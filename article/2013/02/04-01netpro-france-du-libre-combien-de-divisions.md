---
site: 01netPro.
title: "France (du) Libre, combien de divisions?"
author: Xavier Biseul
date: 2013-02-04
href: http://pro.01net.com/editorial/585969/la-france-libre-combien-de-divisions
tags:
- Entreprise
- April
- Associations
---

> Le Conseil national du logiciel libre (CNLL) dresse le panorama de l’open source en France. Une industrie qui pèse 2,5 milliards d’euros, soit 6 % du marché des logiciels et services. La filière open source fait vivre 30 000 personnes dont 3 000 seulement chez les pure players. Et 90 % de l’effectif se trouve disséminé au sein des SSII généralistes ou des DSI.
