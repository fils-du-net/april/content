---
site: ITRmanager.com
title: "La comptine du cloud"
author: Claude Computine
date: 2013-02-01
href: http://www.itrmanager.com/tribune/138424/comptine-cloud.html
tags:
- Entreprise
- Économie
- HADOPI
- Institutions
- Associations
- Informatique en nuage
---

> C'est fort à propos que le maître des cérémonies de cette soirée, Fabrice André, proposait, suivant la formule consacrée, de poursuivre la discussion durant le cocktail, autour d'une flûte de champagne. Car la conférence-débat «Quel avenir pour le cloud français?» menaçait insidieusement de tourner au pugilat dans les souterrains salpêtrés de Telecom Paris Tech!
