---
site: PC INpact
title: "Défense: nouveaux détails sur l’offre Open Bar de Microsoft"
author: Marc Rees
date: 2013-02-11
href: http://www.pcinpact.com/news/77439-defense-nouveaux-details-sur-l-offre-open-bar-microsoft.htm
tags:
- Logiciels privateurs
- Administration
- Marchés publics
---

> Nous avons pu consulter de nouveaux documents préalables à la signature du contrat «Open Bar» entre Microsoft et le ministère de la Défense. Fait notable, ils relatent une analyse risque/bénéfice de cet accord signé avec la bénédiction du comité directeur de la Défense. Ce contrat est aujourd’hui en phase de renégociation puisqu’il atteindra son terme en mai 2013.
