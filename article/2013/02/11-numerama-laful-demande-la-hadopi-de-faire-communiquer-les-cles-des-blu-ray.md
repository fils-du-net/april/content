---
site: Numerama
title: "L'AFUL demande à la Hadopi de faire communiquer les clés des Blu-Ray"
author: Guillaume Champeau
date: 2013-02-11
href: http://www.numerama.com/magazine/25044-l-aful-demande-a-la-hadopi-de-faire-communiquer-les-cles-des-blu-ray.html
tags:
- Interopérabilité
- HADOPI
- Associations
- DRM
- Video
---

> Pour l'Association Francophone des Utilisateurs de Logiciels Libres (AFUL), qui répond à la consultation publique ouverte par l'Hadopi dans le cadre de l'affaire VLC, il ne fait aucun doute que la clé de chiffrement des contenus utilisée dans le cadre des DRM fait partie des éléments qui doivent être transmis aux auteurs de logiciels libres pour permettre l'interopérabilité.
