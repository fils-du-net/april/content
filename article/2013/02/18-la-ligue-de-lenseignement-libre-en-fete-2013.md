---
site: la ligue de l'enseignement
title: "Libre en Fête 2013"
author: Denis Lebioda
date: 2013-02-18
href: http://www.laligue-alpesdusud.org/associatifs_leblog/?2013/02/18/3658-libre-en-fete-2013
tags:
- Administration
- April
- Sensibilisation
- Promotion
---

> Pour la treizième année consécutive, l'initiative Libre en Fête est relancée par l'April et ses partenaires.
