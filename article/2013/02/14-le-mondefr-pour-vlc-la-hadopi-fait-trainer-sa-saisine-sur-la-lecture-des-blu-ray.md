---
site: Le Monde.fr
title: "Pour VLC, la Hadopi \"fait traîner\" sa saisine sur la lecture des Blu-Ray"
author: Guénaël Pépin
date: 2013-02-14
href: http://www.lemonde.fr/technologies/article/2013/02/14/pour-vlc-la-hadopi-fait-trainer-sa-saisine-sur-la-lecture-des-blu-ray_1832087_651865.html
tags:
- Entreprise
- Interopérabilité
- HADOPI
- Associations
- DRM
---

> Une consultation publique par la Hadopi ne plaît guère aux développeurs du lecteur multimédia, qui souhaitent obtenir les clés protégeant les disques Blu-Ray.
