---
site: écrans
title: "Education nationale: priorité grillée pour les logiciels libres"
author: Camille Gévaudan
date: 2013-06-06
href: http://www.ecrans.fr/Education-nationale-priorite,16486.html
tags:
- Entreprise
- Administration
- April
- Institutions
- Éducation
- Marchés publics
---

> Monde de l’éducation et logiciel libre sont faits pour s’entendre. Mieux encore: ils sont faits l’uns pour l’autre. L’un cherche à diffuser la connaissance au plus grand nombre de citoyens possibles; l’autre font tomber les barrières techniques et juridiques qui contraignent la circulation des données. Ne devraient-ils pas se marier et avoir plein de petits enfants aussi bien connectés qu’éduqués?
