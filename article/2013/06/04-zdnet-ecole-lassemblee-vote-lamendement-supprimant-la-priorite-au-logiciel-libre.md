---
site: ZDNet
title: "Ecole: l'Assemblée vote l'amendement supprimant la priorité au logiciel libre"
author: thierry-noisette
date: 2013-06-04
href: http://www.zdnet.fr/actualites/ecole-l-assemblee-vote-l-amendement-supprimant-la-priorite-au-logiciel-libre-39791044.htm
tags:
- Entreprise
- Administration
- April
- Institutions
- Éducation
- Marchés publics
- Standards
---

> L'amendement du gouvernement, qui supprime la priorité au logiciel libre et au format ouvert inscrite par les sénateurs dans le projet de loi sur l'école, a été voté par les députés la nuit dernière.
