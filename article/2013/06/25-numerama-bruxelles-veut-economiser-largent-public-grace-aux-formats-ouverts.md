---
site: Numerama
title: "Bruxelles veut économiser l'argent public grâce aux formats ouverts"
author: Guillaume Champeau
date: 2013-06-25
href: http://www.numerama.com/magazine/26362-bruxelles-veut-economiser-l-argent-public-grace-aux-formats-ouverts.html
tags:
- Administration
- Institutions
- Marchés publics
- Europe
---

> La Commission Européenne demande aux administrations européennes de considérer les enjeux de long terme, et de privilégier les solutions basées sur les normes et les formats ouverts lorsqu'elles publient des appels d'offres. Elle estime que plus d'un milliard d'euros par an pourraient être économisés avec cette stratégie.
