---
site: L'Union
title: "Ils croient aux logiciels libres"
date: 2013-06-18
href: http://www.lunion.presse.fr/article/aisne/ils-croient-aux-logiciels-libres
tags:
- Associations
- Promotion
- Video
---

> Des Soissonnais veulent relancer la promotion du logiciel libre, d'après eux un bon levier pour l'emploi en période de crise.
