---
site: ZDNet
title: "Linux champion incontesté des supercalculateurs"
date: 2013-06-19
href: http://www.zdnet.fr/actualites/linux-champion-inconteste-des-supercalculateurs-39791550.htm
tags:
- Logiciels privateurs
- Innovation
- Sciences
---

> Plus de 95% des 500 plus puissants supercalculateurs mondiaux tournent sous Linux, dont le premier d’entre eux avec 33,8 pétaflops, Tianhe-2, sous Kylin Linux. Et Windows? L’OS n’équipe que 3 supercalculateurs.
