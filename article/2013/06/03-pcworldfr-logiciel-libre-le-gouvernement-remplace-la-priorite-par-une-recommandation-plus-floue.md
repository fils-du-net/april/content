---
site: PCWorld.fr
title: "Logiciel libre: le gouvernement remplace la priorité par une recommandation plus floue"
author: Michel Beck
date: 2013-06-03
href: http://www.pcworld.fr/logiciels/actualites,logiciel-libre-gouvernement-remplace-priorite-recommandation-plus-floue,538937,1.htm
tags:
- Entreprise
- April
- Institutions
- Éducation
---

> Le logiciel libre bientôt sur tous les postes de l'Education nationale? Il y a encore quelques jours on aurait presque pu secouer la tête de bas en haut tout en dressant fièrement deux pouces en direction du ciel. Aujourd'hui... c'est nettement moins évident.
