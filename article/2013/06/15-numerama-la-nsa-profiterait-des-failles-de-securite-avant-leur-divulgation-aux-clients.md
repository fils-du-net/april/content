---
site: Numerama
title: "La NSA profiterait des failles de sécurité avant leur divulgation aux clients"
author: Julien L.
date: 2013-06-15
href: http://www.numerama.com/magazine/26262-la-nsa-profiterait-des-failles-de-securite-avant-leur-divulgation-aux-clients.html
tags:
- Logiciels privateurs
- Administration
- April
- Informatique-deloyale
---

> Dans une enquête menée par Bloomberg, il apparaît que les agences de renseignements ont connaissance des vulnérabilités des logiciels bien avant les clients des entreprises qui les éditent. Officiellement, il s'agit pour les autorités gouvernementales de protéger très tôt les systèmes d'information. Mais rien n'empêche a priori de faire d'une pierre deux coups...
