---
site: LeMagIT
title: "Open Source: les entreprises en France considèrent encore la réduction des coûts comme principal avantage"
author: Cyrille Chausson
date: 2013-06-24
href: http://www.lemagit.fr/technologie/applications/open-source/2013/06/24/open-source-les-entreprises-en-france-considerent-encore-la-reduction-des-couts-comme-principal-avantage
tags:
- Entreprise
- Économie
- Sensibilisation
---

> Sécurité? Innovation? Performance? Richesse fonctionnelle? Non. Lorsque l’on parle d’Open Source, les entreprises françaises, sondées dans ce baromètre, place la réduction des coûts comme avantage n°1 des solutions à code ouvert.
