---
site: ragemag
title: "Frédéric Couchet: «Les citoyens méritent la liberté informatique.»"
author: Emma Dreyfus
date: 2013-06-28
href: http://ragemag.fr/frederic-couchet-les-citoyens-meritent-la-liberte-informatiqu-34307
tags:
- April
- Partage du savoir
- HADOPI
- Institutions
- Associations
- DRM
- Droit d'auteur
- Philosophie GNU
---

> Cela fait une semaine que nous avons lancé le cycle hackers sur RAGEMAG et déjà arrivons-nous à son terme. Après KheOps avec qui nous avons discuté de lutte numérique et de l’éthique des hackers, Sabine Blanc qui nous a informés sur le hack physique croisé à la bidouille et à l’artisanat et Amaelle Guiton, spécialiste ès «résistance numérique», nous rencontrons aujourd’hui Frédéric Couchet de l’April, l’association pour la promotion et la défense du logiciel libre.
