---
site: Rue89
title: "Quatre bonnes raisons de renoncer (si l’on peut) au cloud computing"
author: Philippe Vion-Dury
date: 2013-06-22
href: http://www.rue89.com/2013/06/22/quatre-bonnes-raisons-renoncer-si-peut-cloud-computing-243434
tags:
- Entreprise
- Internet
- Institutions
- Informatique-deloyale
- Innovation
- Informatique en nuage
- International
---

> Qu’on l’aime ou pas, le «cloud computing» est partout, et chacun l’utilise. Certains activement, lorsqu’ils décident d’archiver leurs fichiers sur des serveurs comme Mega ou Dropbox. Et presque tout le monde sans en avoir conscience.
