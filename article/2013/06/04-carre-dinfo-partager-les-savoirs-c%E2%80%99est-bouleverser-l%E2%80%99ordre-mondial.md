---
site: Carré d'info
title: "Partager les savoirs, c’est bouleverser l’ordre mondial"
author: Pauline Croquet
date: 2013-06-04
href: http://carredinfo.fr/debat-du-mois-partager-les-savoirs-cest-bouleverser-lordre-mondial-26992
tags:
- Économie
- Associations
- Innovation
- Neutralité du Net
---

> Pour leur débat du mercredi 29 mai, Carré d’info et le pôle actualité de la Médiathèque de Toulouse avaient choisi de discuter des enjeux du partage numérique des savoirs. A Toulouse plusieurs associations et collectifs ont vu le jour et en font d’elle une ville plutôt active dans ce domaine. Ils partagent et collaborent pour bidouiller, transmettre des informations, rendre plus accessibles des techniques, démocratiser des outils numériques ou encore mieux éclairer le citoyen.  Et si le partage n’a aucune frontière, il a bien, pour les débatteurs, des conséquences locales.
