---
site: atlantico
title: "Faut-il faire code informatique seconde langue"
author: Benjamin Weil
date: 2013-06-03
href: http://www.atlantico.fr/decryptage/faut-laisser-tomber-allemand-chinois-ou-espagnol-et-faire-code-informatique-seconde-langue-jean-pierre-archambault-742346.html
tags:
- Entreprise
- April
- Institutions
- Éducation
- Innovation
- Licenses
---

> La question de l’apprentissage de langages informatiques à l’école fait son chemin. C’est le sens du projet Code.org qui mobilise des stars du web et des nouvelles technologies pour inciter à l’enseignement de lignes de code aux têtes blondes outre-Atlantique. En France, le patron de Free, Xavier Niel, tenait des propos similaires lors de sa conférence annonçant son projet d’école informatique gratuite.
