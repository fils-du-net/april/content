---
site: Ekonomico.fr
title: "L’Assemblée supprime la priorité au logiciel libre à l’école"
date: 2013-06-05
href: http://www.ekonomico.fr/2013/06/lassemblee-supprime-la-priorite-au-logiciel-libre-a-lecole
tags:
- Entreprise
- April
- Institutions
- Éducation
---

> Dans le projet de loi sur la refondation de l’école, un amendement stipulait que la priorité serait donnée aux «logiciels libres et aux formats ouverts de document». Dorénavant, les projets devront seulement «tenir compte de l’offre des logiciels libres, si elle existe»
