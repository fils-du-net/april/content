---
site: Developpez.com
title: "Les logiciels libres ne seront plus prioritaires dans l'Education Nationale, ils seront envisagés mais plus préférés aux solutions propriétaires"
author: Stéphane le calme
date: 2013-06-05
href: http://www.developpez.com/actu/56459/Les-logiciels-libres-ne-seront-plus-prioritaires-dans-l-Education-Nationale-ils-seront-envisages-mais-plus-preferes-aux-solutions-proprietaires
tags:
- Entreprise
- Administration
- April
- Institutions
- Éducation
- Marchés publics
---

> Ils seront envisagés mais plus préférés aux solutions propriétaires
