---
site: Numerama
title: "Linkeo s'en prend à LinuxFr et récolte un effet Streisand"
author: Julien L.
date: 2013-06-01
href: http://www.numerama.com/magazine/26120-linkeo-s-en-prend-a-linuxfr-et-recolte-un-effet-streisand.html
tags:
- Entreprise
- Internet
- Associations
---

> L'effet Streisand peut aussi piéger les agences spécialisées dans le développement web. C'est ce que révèle l'affaire Linkeo. La société a cherché à faire retirer un commentaire négatif suite à une annonce publiée sur LinuxFr. Mais en menaçant ce dernier par une mise en demeure, Linkeo a écorné son image à un niveau autrement plus important qu'une banale réaction, aussi désobligeante soit-elle.
