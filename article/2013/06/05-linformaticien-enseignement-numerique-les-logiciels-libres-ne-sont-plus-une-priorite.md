---
site: L'Informaticien
title: "Enseignement numérique: les logiciels libres ne sont plus une priorité"
author: Margaux Duquesne
date: 2013-06-05
href: http://www.linformaticien.com/actualites/id/29283/enseignement-numerique-les-logiciels-libres-ne-sont-plus-une-priorite.aspx
tags:
- Entreprise
- Institutions
- Éducation
---

> Le gouvernement vient d’enterrer un amendement en cours d’examen à l’Assemblée nationale, qui faisait des logiciels libres une priorité dans le futur service public éducatif numérique.
