---
site: JDN
title: "Loi sur l'école et logiciel libre: un lobbying intense. Qui parle au nom des entreprises du Libre?"
author: Patrice Bertrand
date: 2013-06-11
href: http://www.journaldunet.com/web-tech/expert/54452/loi-sur-l-ecole-et-logiciel-libre---un-lobbying-intense--qui-parle-au-nom-des-entreprises-du-libre.shtml
tags:
- Entreprise
- Économie
- Institutions
- Éducation
- Marchés publics
- Video
- Europe
---

> La loi sur la Refondation de l’École inclut un article, introduit au Sénat, instituant une priorité au logiciel libre et aux formats ouverts. Le lobbying se met en marche, c'est bien naturel. Mais qui parle au nom du logiciel libre?
