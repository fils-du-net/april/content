---
site: ZDNet
title: "Ecole: le gouvernement retoque la priorité au logiciel libre"
author: thierry-noisette
date: 2013-06-03
href: http://www.zdnet.fr/actualites/ecole-le-gouvernement-retoque-la-priorite-au-logiciel-libre-39790971.htm
tags:
- Entreprise
- Administration
- April
- Institutions
- Éducation
- Marchés publics
---

> Votée par le Sénat et adoptée en commission à l’Assemblée, la priorité au logiciel libre et aux formats ouverts est remplacée par une mention plus floue dans un amendement gouvernemental.
