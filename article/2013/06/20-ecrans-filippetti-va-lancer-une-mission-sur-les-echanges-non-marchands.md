---
site: écrans
title: "Filippetti va lancer une mission sur les échanges non-marchands"
author: Sophian Fanen
date: 2013-06-20
href: http://www.ecrans.fr/Info-Libe-Aurelie-Filippetti-va,16584.html
tags:
- Internet
- Partage du savoir
- HADOPI
- Institutions
- DADVSI
- Droit d'auteur
---

> Après s’être déclarée, hier à l’Assemblée nationale, en faveur d’un débat de fond sur les échanges d’œuvres sans but commercial sur Internet, la ministre de la Culture compte nommer une personnalité pour étudier un éventuel changement de statut juridique.
