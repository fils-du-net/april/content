---
site: AgoraVox
title: "Genova, un militant des logiciels libres et open source"
author: Seydou Badiane
date: 2013-06-03
href: http://www.agoravox.fr/actualites/technologies/article/entretien-genova-un-militant-des-136692
tags:
- Sensibilisation
- International
---

> Mamadou Diagne, plus connu sous le nom de Genova, est un informaticien, évidement, un geek et hacker. C’est un monsieur qui est très actif dans la communauté sénégalaise du Libre.
