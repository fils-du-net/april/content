---
site: challenges.fr
title: "Face à Windows (Microsoft) et Mac OS (Apple), les logiciels libres n'ont pas dit leur dernier mot"
author: Paul Loubière
date: 2013-06-05
href: http://www.challenges.fr/high-tech/20130605.CHA0434/face-a-windows-microsoft-et-mac-os-apple-les-logiciels-libres-n-ont-pas-dit-leur-dernier-mot.html
tags:
- Entreprise
- Logiciels privateurs
- Économie
---

> Bien que Linux soit toujours un nain sur le marché des particuliers, le succès des logiciels libres dans les entreprises et sur les tablettes est prometteur.
