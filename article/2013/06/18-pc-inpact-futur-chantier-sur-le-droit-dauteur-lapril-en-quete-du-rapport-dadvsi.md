---
site: PC INpact
title: "Futur chantier sur le droit d'auteur: l'April en quête du rapport DADVSI"
author: Marc Rees
date: 2013-06-18
href: http://www.pcinpact.com/news/80632-futur-chantier-sur-droit-dauteur-april-en-quete-rapport-dadvsi.htm
tags:
- April
- HADOPI
- Institutions
- DADVSI
- DRM
- Droit d'auteur
---

> L’association pour la promotion du logiciel libre (April) a pris bonne note du chantier sur le droit d’auteur, ouvert au Conseil supérieur de la propriété littéraire et artistique. Elle rappelle cependant au ministère un bilan toujours promis, jamais publié, celui sur la loi DADVSI.
