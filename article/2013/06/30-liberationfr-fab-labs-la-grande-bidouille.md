---
site: Libération.fr
title: "Fab Labs: la grande bidouille"
author: Coralie Schaub
date: 2013-06-30
href: http://www.liberation.fr/economie/2013/06/30/fab-labs-la-grande-bidouille_914807
tags:
- Partage du savoir
- Matériel libre
- Innovation
---

> Pas besoin d’être ingénieur pour réparer son ordi ou fabriquer son vélo. Plans et machines sont mis à la disposition de tous sur le Web et dans des ateliers mécanico-numériques qui commencent à essaimer en France.
