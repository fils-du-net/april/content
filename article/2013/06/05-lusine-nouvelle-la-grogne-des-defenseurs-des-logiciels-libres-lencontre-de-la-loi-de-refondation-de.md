---
site: L'usine Nouvelle
title: "La grogne des défenseurs des logiciels libres à l'encontre de la loi de refondation de l’école"
author: Wassinia Zirar
date: 2013-06-05
href: http://www.usinenouvelle.com/article/la-grogne-des-defenseurs-des-logiciels-libres-a-l-encontre-de-la-loi-de-refondation-de-l-ecole.N198475
tags:
- April
- Institutions
- Éducation
- Marchés publics
- Europe
---

> Le projet de loi de refondation de l’école devait faire du recours aux logiciels libres une priorité. Mardi 4 juin 2013, les députés ont voté un amendement remplaçant la priorité par "une possibilité". L’association de défense des logiciels libres ne décolère pas.
