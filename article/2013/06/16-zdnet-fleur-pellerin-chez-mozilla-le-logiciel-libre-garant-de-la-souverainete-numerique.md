---
site: ZDNet
title: "Fleur Pellerin chez Mozilla: le logiciel libre, \"garant de la souveraineté numérique\""
author: Thierry Noisette
date: 2013-06-16
href: http://www.zdnet.fr/actualites/fleur-pellerin-chez-mozilla-le-logiciel-libre-garant-de-la-souverainete-numerique-39791458.htm
tags:
- Internet
- April
- Institutions
- Associations
- Éducation
- Promotion
---

> La fondation éditrice de Firefox a fêté cette semaine ses nouveaux locaux parisiens. À cette occasion, la ministre de l’Économie numérique a prononcé un bel hommage au logiciel libre.
