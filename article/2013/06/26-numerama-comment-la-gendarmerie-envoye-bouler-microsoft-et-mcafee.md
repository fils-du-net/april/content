---
site: Numerama
title: "Comment la Gendarmerie a envoyé bouler Microsoft et McAfee"
author: Guillaume Champeau
date: 2013-06-26
href: http://www.numerama.com/magazine/26373-comment-la-gendarmerie-a-envoye-bouler-microsoft-et-mcafee.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
---

> Après un échange de courriers dont Numerama a pris connaissance, la Gendarmerie a refusé de mettre en place une solution antivirus McAfee achetée par la centrale d'achat public de l'Etat, parce que Microsoft considérait qu'elle obligeait à acheter une licence client (CAL) de Windows Server pour chaque poste utilisateur, même sous Linux. La Gendarmerie a prévenu tous les ministères, et demande une solution antivirus basé sur un serveur Linux.
