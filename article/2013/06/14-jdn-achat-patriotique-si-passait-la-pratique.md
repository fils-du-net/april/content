---
site: JDN
title: "Achat patriotique? Si on passait à la pratique!"
author: Alexandre Zapolsky
date: 2013-06-14
href: http://www.journaldunet.com/web-tech/expert/54501/achat-patriotique---si-on-passait--a-la-pratique.shtml
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Éducation
- Marchés publics
---

> Lettre ouverte à Arnaud Montebourg, ministre du Redressement productif. Il y a loin de la coupe aux lèvres, et entre les déclarations d'un ministre, à qui je fais le crédit de la sincérité, et leurs traductions sur le terrain aussi. J'aimerais à ce titre vous faire part d'un cas très concret.
