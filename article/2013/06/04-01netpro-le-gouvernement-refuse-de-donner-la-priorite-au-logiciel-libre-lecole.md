---
site: 01netPro.
title: "Le gouvernement refuse de donner la priorité au logiciel libre à l'école"
author: Xavier Biseul
date: 2013-06-04
href: http://pro.01net.com/editorial/596719/le-gouvernement-refuse-de-donner-la-priorite-au-logiciel-libre-a-l-ecole
tags:
- April
- Institutions
- Éducation
- Marchés publics
- Standards
---

> Mardi 4 juin, vers minuit, les députés ont approuvé l’amendement du gouvernement visant à refuser de favoriser le recours au logiciel libre à l’école. En seconde lecture du projet de loi de refondation de l'école de la République, le Sénat avait introduit une disposition qui donne la priorité au logiciel libre et aux formats ouverts dans le futur service public du numérique éducatif.
