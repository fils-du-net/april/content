---
site: ZDNet
title: "Vente liée: un amendement parlementaire demande la mention du prix des logiciels installés"
author: Thierry Noisette
date: 2013-06-23
href: http://www.zdnet.fr/actualites/vente-liee-un-amendement-parlementaire-demande-la-mention-du-prix-des-logiciels-installes-39791701.htm
tags:
- Logiciels privateurs
- Vente liée
- Associations
- Standards
---

> Un amendement au projet de loi Consommation instaurerait la transparence du coût des logiciels préinstallés lors de la vente des ordinateurs. Une proposition applaudie par trois associations libristes, l'Adullact, l'Aful et Framasoft.
