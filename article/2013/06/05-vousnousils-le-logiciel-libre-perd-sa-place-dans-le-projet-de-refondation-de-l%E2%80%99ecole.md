---
site: vousnousils
title: "Le logiciel libre perd sa place dans le projet de refondation de l’école"
date: 2013-06-05
href: http://www.vousnousils.fr/2013/06/05/le-logiciel-libre-perd-sa-place-dans-le-projet-de-refondation-de-lecole-548711
tags:
- April
- Institutions
- Éducation
---

> Le recours aux logi­ciels libres n'est plus une prio­rité dans le pro­jet de loi de refonte de l'école exa­miné hier en seconde lec­ture à l'Assemblée.
