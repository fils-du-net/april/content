---
site: Le Nouvel Observateur
title: "Accord de la CMP sur le projet de loi sur l'enseignement supérieur"
author: Paul Laubacher
date: 2013-06-26
href: http://tempsreel.nouvelobs.com/education/20130626.AFP7679/accord-de-la-cmp-sur-le-projet-de-loi-sur-l-enseignement-superieur.html
tags:
- Institutions
- Éducation
---

> Sénateurs et députés sont parvenus à un accord sur le projet de loi sur l'enseignement supérieur mercredi en Commission mixte paritaire (CMP, 7 sénateurs, 7 députés), retirant notamment un amendement modifiant la procédure de qualification des enseignants-chercheurs, a annoncé à l'AFP la présidente de la commission de la culture et de l'éducation au Sénat, Marie-Christine Blandin (Ecolo).
