---
site: MacGeneration
title: "Un amendement sur la priorité du libre à l'école retoqué"
author: Stéphane Moussie
date: 2013-06-04
href: http://www.macg.co/news/voir/260090/un-amendement-sur-la-priorite-du-libre-a-l-ecole-retoque
tags:
- Entreprise
- Économie
- April
- Institutions
- Éducation
---

> Le logiciel libre n'aura été une priorité à l'école que pendant une semaine et demi. Le 25 mai, le Sénat a adopté le projet de loi pour la refondation de l'école de la République.
