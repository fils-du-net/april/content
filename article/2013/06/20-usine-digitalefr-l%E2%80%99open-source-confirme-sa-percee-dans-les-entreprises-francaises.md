---
site: "usine-digitale.fr"
title: "L’Open source confirme sa percée dans les entreprises françaises"
author: Sylvain Arnulf
date: 2013-06-20
href: http://www.usine-digitale.fr/article/l-open-source-confirme-sa-percee-dans-les-entreprises-francaises.N199839
tags:
- Entreprise
---

> "Red Hat", société éditrice de logiciels et systèmes d’exploitation libres, a commandé à l’agence Adelanto un panorama de l’open source en France. Principal enseignement: 89% des entreprises interrogées exploitent des applications Open source.
