---
site: ZDNet
title: "Projet de loi consommation: amendements contre les DRM et le filtrage"
author: Thierry Noisette
date: 2013-06-25
href: http://www.zdnet.fr/actualites/projet-de-loi-consommation-amendements-contre-les-drm-et-le-filtrage-39791774.htm
tags:
- Internet
- Économie
- Interopérabilité
- April
- HADOPI
- Institutions
- Vente liée
- DRM
---

> Plusieurs amendements parlementaires au projet de loi Hamon visent à mieux informer de la présence de DRM dans un matériel électronique, à empêcher le filtrage d'accès à la demande de la DGCCRF et le blocage de site sans passer par un juge.
