---
site: clubic.com
title: "Berlin ne veut toujours pas de l'open source"
author: Thomas Pontiroli
date: 2013-06-13
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/logiciel-libre-open-source/actualite-565258-berlin-open-source.html
tags:
- Administration
- Marchés publics
- Standards
- Informatique en nuage
---

> La capitale allemande ne donne toujours pas son aval pour une adoption de l'open source dans son administration. Une posture qui contraste avec celle de Munich, satisfaite d'avoir pris OpenOffice.
