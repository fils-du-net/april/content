---
site: Le Monde Informatique
title: "La ville de Berlin rejette encore le passage à l'Open Source"
author: Jean Elyan
date: 2013-06-13
href: http://www.lemondeinformatique.fr/actualites/lire-la-ville-de-berlin-rejette-encore-le-passage-a-l-open-source-53973.html
tags:
- Administration
- Marchés publics
- Standards
- Informatique en nuage
- International
---

> Pour la deuxième fois, la proposition de faire passer la capitale d'Allemagne à l'Open Source a été rejetée par la coalition qui dirige le land. La standardisation et la création d'un cloud gouvernemental sont privilégiés.
