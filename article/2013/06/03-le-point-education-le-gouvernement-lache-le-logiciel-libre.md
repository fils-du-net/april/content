---
site: Le Point
title: "Éducation: le gouvernement lâche le logiciel libre"
author: Guerric Poncet
date: 2013-06-03
href: http://www.lepoint.fr/chroniqueurs-du-point/guerric-poncet/education-le-gouvernement-lache-le-logiciel-libre-03-06-2013-1675889_506.php
tags:
- Entreprise
- Administration
- Économie
- April
- HADOPI
- Institutions
- Éducation
- Marchés publics
- Standards
---

> Vincent Peillon a anesthésié un amendement en faveur des formats ouverts dans le projet de loi pour la refondation de l'école. L'industrie du logiciel jubile.
