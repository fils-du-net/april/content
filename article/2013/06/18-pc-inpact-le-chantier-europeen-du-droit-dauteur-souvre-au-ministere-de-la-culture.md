---
site: PC INpact
title: "Le chantier européen du droit d'auteur s'ouvre au ministère de la Culture"
author: Marc Rees
date: 2013-06-18
href: http://www.pcinpact.com/news/80599-le-chantier-europeen-droit-dauteur-souvre-au-ministere-culture.htm
tags:
- Internet
- HADOPI
- Institutions
- DADVSI
- Droit d'auteur
- Europe
---

> La France s’apprête à envoyer plusieurs doléances pour la future révision de la directive de 2001/29 sur la société de l’information. C’est à partir de cette directive que les ayants droit ont pu voir voter les lois Dadvsi ou Hadopi. Le mouvement n’est pas près de stopper: au sein du ministère de la Culture, le Conseil supérieur de la propriété littéraire et artistique vient tout juste de lancer une mission sur ce futur chantier plein d’avenir.
