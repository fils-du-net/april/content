---
site: LeMagIT
title: "Préférence donnée au Libre: le gouvernement britannique fait aussi machine arrière"
author: Cyrille Chausson
date: 2013-06-18
href: http://www.lemagit.fr/technologie/applications/open-source/2013/06/18/preference-donnee-au-libre-le-gouvernement-britannique-fait-aussi-machine-arriere
tags:
- Logiciels privateurs
- Administration
- Institutions
- Marchés publics
- RGI
- International
---

> Dans une modification de son guide de bonnes pratiques pour les services numériques publics, le gouvernement a fortement atténué la notion de préférence donnée aux logiciels libres pour les traiter au même titre que les logiciels propriétaires. Un traitement identique pour une mise en concurrence, affirme le CTO de sa gracieuse Majesté.
