---
site: LeMagIT
title: "Le gouvernement endort la priorité aux logiciels libres dans l’éducation"
author: Cyrille Chausson
date: 2013-06-04
href: http://www.lemagit.fr/economie/business/2013/06/04/le-gouvernement-endort-la-priorite-aux-logiciels-libres-dans-leducation
tags:
- Entreprise
- Administration
- April
- Institutions
- Éducation
- Marchés publics
- Standards
---

> Les députés ont finalement voté en faveur d’une amendement du gouvernement qui ne donne plus la priorité aux logiciels libres dans le numérique éducatif. Le libre doit être seulement pris en compte, et quand l’offre existe. Un revers cinglant.
