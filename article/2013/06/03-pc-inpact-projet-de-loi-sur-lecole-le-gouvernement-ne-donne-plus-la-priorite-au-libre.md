---
site: PC INpact
title: "Projet de loi sur l'École: le gouvernement ne donne plus la priorité au libre"
author: Marc Rees
date: 2013-06-03
href: http://www.pcinpact.com/news/80171-projet-loi-sur-ecole-gouvernement-ne-donne-plus-priorite-au-libre.htm
tags:
- April
- Institutions
- Éducation
---

> Stop! Pour le futur service public du numérique éducatif, le gouvernement ne veut plus faire du libre une priorité. Après avoir laissé silencieusement passer un amendement en ce sens au Sénat, Jean-Marc Ayrault a visiblement prêté l’oreille à l’industrie du secteur qui craignait ce nouveau cap.
