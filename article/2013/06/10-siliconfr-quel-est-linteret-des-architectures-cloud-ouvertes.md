---
site: Silicon.fr
title: "Quel est l'intérêt des architectures Cloud ouvertes?"
author: Yves Grandmontagne
date: 2013-06-10
href: http://www.silicon.fr/special-cloud-1-le-choix-des-architectures-cloud-ouvertes-openstack-86835.html
tags:
- Entreprise
- Interopérabilité
- Innovation
- Informatique en nuage
---

> Le Cloud Computing change en profondeur notre rapport avec l’informatique. Les options prises en faveur de la standardisation sont sources d’économies. L’Open source y contribue-t-il?
