---
site: Framablog
title: "Comment aider le logiciel libre quand on ne sait pas coder"
author: Duncan McKean
date: 2013-06-12
href: http://www.framablog.org/index.php/post/2013/06/12/logiciel-libre-aider-sans-programmer
tags:
- Sensibilisation
- Licenses
- Promotion
- ACTA
---

> Beaucoup de gens intéressés par l’idée d’apporter leur aide à des projets open source mais n’ayant absolument aucune compétence en programmation m’ont demandé ce qu’ils pouvaient faire. Eh bien, voici quelques moyens pour ces non-programmeurs de contribuer à de tels projets.
