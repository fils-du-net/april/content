---
site: atlantico
title: "Pas de logiciel libre à l'école: histoire et conséquences d'un revirement"
date: 2013-06-25
href: http://www.atlantico.fr/decryptage/pas-logiciel-libre-ecole-histoire-et-consequences-revirement-remi-boulle-766967.html
tags:
- Entreprise
- Économie
- April
- Institutions
- Associations
- Éducation
- Marchés publics
- Standards
---

> Le projet de loi de refondation de l'école de la République, qui doit passer en seconde lecture au Sénat ce mardi, donnait la priorité aux logiciels libres et aux formats ouverts dans l’éducation. Un revirement soudain a vidé cette disposition de sa substance.
