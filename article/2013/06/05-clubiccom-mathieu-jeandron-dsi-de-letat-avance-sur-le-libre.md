---
site: clubic.com
title: "Mathieu Jeandron, DSI de l'État: \"On avance sur le libre\""
author: Thomas Pontiroli
date: 2013-06-05
href: http://pro.clubic.com/administration-electronique/actualite-563458-mathieu-jeandron-dsi-tat-avance-libre.html
tags:
- Administration
- Institutions
- Marchés publics
- RGI
---

> Huit mois après la signature de la circulaire sur le logiciel libre, les premiers groupes de travail ont été mis en place et ont permis d'élaborer des listes référençant des logiciels validés par les experts.
