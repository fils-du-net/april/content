---
site: PC INpact
title: "Le CNNum rend un avis sur l’enseignement de l’informatique en France"
author: Xavier Berne
date: 2013-06-25
href: http://www.pcinpact.com/news/80804-le-cnnum-rend-avis-sur-l-enseignement-l-informatique-en-france.htm
tags:
- Institutions
- Éducation
---

> Le second avis du nouveau Conseil national du numérique porte sur l’enseignement de l’informatique en France. Alors que le Parlement discute encore du projet de loi sur la refondation de l’école de la République, l’institution apporte aujourd’hui son soutien à un rapport s’alarmant du retard de la France en la matière.
