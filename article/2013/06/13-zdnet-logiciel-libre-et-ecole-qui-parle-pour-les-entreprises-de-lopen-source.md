---
site: ZDNet
title: "Logiciel libre et école: qui parle pour les entreprises de l'open source?"
author:  Thierry Noisette
date: 2013-06-13
href: http://www.zdnet.fr/actualites/logiciel-libre-et-ecole-qui-parle-pour-les-entreprises-de-l-open-source-39791369.htm
tags:
- Entreprise
- Institutions
- Éducation
- Marchés publics
---

> Lorsque le Syntec numérique met en avant ses membres entreprises de l'open source pour demander aux parlementaires de retirer la priorité aux logiciels libres et aux formats ouverts pour le service public du numérique éducatif, la manœuvre ne passe pas inaperçue.
