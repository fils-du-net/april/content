---
site: clubic.com
title: "Logiciel libre, le pas de recul des députés"
author: Olivier Robillart
date: 2013-06-04
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/logiciel-libre-open-source/actualite-563090-logiciel-libre-recul-senat.html
tags:
- Entreprise
- Administration
- April
- Éducation
- Marchés publics
- Standards
---

> L'April, l'association de défense du logiciel libre ne décolère pas. Elle reproche au gouvernement ainsi qu'aux élus de «supprimer la priorité au logiciel libre» dans le cadre de l'examen du projet de loi sur la refondation de l'école.
