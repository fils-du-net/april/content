---
site: L'Expansion
title: "L'Hadopi lance l'idée d'une licence globale payable par les sites internet"
author: PRaphaële Karayan
date: 2013-06-27
href: http://lexpansion.lexpress.fr/high-tech/l-hadopi-lance-l-idee-d-une-licence-globale-payable-par-les-sites-internet_391894.html
tags:
- Internet
- Économie
- HADOPI
- Droit d'auteur
---

> L'Hadopi va étudier la faisabilité d'un système autorisant les échanges d'oeuvres protégées sur internet en échange d'une compensation financière pour les ayants droit, qui serait payée par les sites qui en tirent profit. Un pavé dans la mare.
