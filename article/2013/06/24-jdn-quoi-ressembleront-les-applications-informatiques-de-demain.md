---
site: JDN
title: "À quoi ressembleront les applications informatiques de demain?"
author: Miguel Valdes-Faura
date: 2013-06-24
href: http://www.journaldunet.com/web-tech/expert/54546/a-quoi-ressembleront-les-applications-informatiques-de-demain.shtml
tags:
- Entreprise
- Innovation
---

> L’adoption croissante du cloud révolutionne le mode de distribution traditionnelle des applications professionnelles, pour autant les applications continuent d’être hébergées sur un serveur et accédées via un ordinateur ou un terminal mobile.
