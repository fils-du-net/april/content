---
site: Le Cercle Les Echos
title: "La priorité au logiciel libre est-elle légitime?"
author: Patrice Bertrand
date: 2013-06-30
href: http://lecercle.lesechos.fr/entreprises-marches/high-tech-medias/informatiques/221175502/priorite-logiciel-libre-est-elle-legiti
tags:
- Administration
- Économie
- Institutions
- Associations
- Éducation
- Marchés publics
- Video
- International
---

> Les sénateurs ont de la suite dans les idées, voici qu'à nouveau ils introduisent dans la loi un article demandant la priorité au logiciel libre dans des domaines relevant de l'éducation. Le gouvernement les a mis au pas il y a un mois, mais ils sont obstinés. Au-delà des jeux politiques, en quoi serait-il bon, serait-il légitime, de donner priorité au logiciel libre par la loi?
