---
site: Softonic
title: "Éducation nationale: la priorité au logiciel libre remise en question"
author: Baptiste Brassart
date: 2013-06-05
href: http://actualites.softonic.fr/2013-06-05-education-nationale-priorite-logiciel-libre-remise-en-question
tags:
- April
- Institutions
- Éducation
---

> Le mardi 4 juin en fin de soirée, les députés ont revu leur position quant à la priorité accordée au logiciel libre dans le cadre du projet de loi de refondation de l'école de la République.
