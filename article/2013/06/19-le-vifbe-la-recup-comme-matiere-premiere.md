---
site: Le Vif.be
title: "La récup comme matière première"
date: 2013-06-19
href: http://www.levif.be/info/belga-generique/la-recup-comme-matiere-premiere/article-4000332095190.htm
tags:
- Innovation
---

> (Belga) Liège dispose du premier «Relab» wallon. Il étudie de nouvelles manières de revaloriser les déchets.
