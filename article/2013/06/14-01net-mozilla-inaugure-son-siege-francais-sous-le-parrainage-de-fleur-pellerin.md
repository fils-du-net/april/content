---
site: 01net.
title: "Mozilla a inauguré son siège français sous le parrainage de Fleur Pellerin"
author: Pascal Samama
date: 2013-06-14
href: http://www.01net.com/editorial/597621/mozilla-inaugure-son-siege-francais-sous-le-parrainage-de-fleur-pellerin
tags:
- Internet
- Institutions
- Innovation
---

> C’est à Paris, sur le boulevard Montmartre, que la Fondation a installé son siège français. Pour cette inauguration, Tristan Nitot, patron des lieux, a reçu Mitchell Baker, présidente de la Fondation Mozilla, et Fleur Pellerin.
