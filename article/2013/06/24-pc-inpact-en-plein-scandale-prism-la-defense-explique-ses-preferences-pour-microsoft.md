---
site: PC INpact
title: "En plein scandale Prism, la Défense explique ses préférences pour Microsoft"
author: Marc Rees
date: 2013-06-24
href: http://www.pcinpact.com/news/80740-en-plein-scandale-prism-defense-explique-ses-preferences-pour-microsoft.htm
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Interopérabilité
- April
- Institutions
- Marchés publics
---

> PC INpact avait révélé la signature par la Défense d’un contrat «open bar» avec Microsoft Irlande permettant au ministère de puiser dans tout le catalogue de l’éditeur pour s’équiper en logiciels. Un contrat sans appel d’offres qui fait tiquer jusque chez les députés. En pleine affaire Prism, où le nom de Microsoft circule, des députés ont questionné des représentants du ministère sur le choix de l’éditeur américain lors d'un échange organisé le 12 juin à l'Assemblée nationale.
