---
site: LeMagIT
title: "Le CNLL et l'OSBA allemand font route commune"
author: Cyrille Chausson
date: 2013-06-20
href: http://www.lemagit.fr/technologie/applications/open-source/2013/06/20/le-cnll-et-le-obsa-allemand-font-route-commune
tags:
- Entreprise
- Institutions
- Europe
---

> Le Conseil national du logiciel libre (CNLL) qui représente les associations et groupements d’entreprises du libre en France, et son homologue allemand, l’Open Source Business Alliance (OSBA), un vaste réseau d’entreprises et d’utilisateurs de l’Open Source outre-Rhin, ont décidé d’unir leur force afin de mieux porter la  parole du libre au niveau européen.
