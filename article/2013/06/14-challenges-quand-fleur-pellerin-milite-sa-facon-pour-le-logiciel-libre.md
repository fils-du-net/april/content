---
site: Challenges
title: "Quand Fleur Pellerin milite à sa façon pour le logiciel libre"
author: Paul Loubière
date: 2013-06-14
href: http://www.challenges.fr/high-tech/20130614.CHA0813/quand-fleur-pellerin-milite-a-sa-facon-pour-le-logiciel-libre.html
tags:
- Internet
- Économie
- Institutions
- Innovation
---

> "Le logiciel libre évite à un pays d’être sous la dépendance technologique d’une entreprise ou d’un Etat" a jugé la ministre de l'Economie numérique lors de l'inauguration des nouveaux locaux français de Mozilla.
