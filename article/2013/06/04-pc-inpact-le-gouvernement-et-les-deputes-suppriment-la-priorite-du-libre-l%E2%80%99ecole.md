---
site: PC INpact
title: "Le gouvernement et les députés suppriment la priorité du libre à l’école"
author: Marc Rees
date: 2013-06-04
href: http://www.pcinpact.com/news/80222-le-gouvernement-et-deputes-suppriment-priorite-libre-a-l-ecole.htm
tags:
- Entreprise
- Logiciels privateurs
- Administration
- April
- Institutions
- Éducation
- Licenses
---

> Hier soir peu avant minuit, le gouvernement a fait passer son amendement visant à ne plus faire du libre, une priorité dans le service public éducatif numérique. Le vote a été acquis avec les voix du PS et de l’UMP, malgré l’opposition des écologistes et des radicaux.
