---
site: LeMagIT
title: "Libre dans le secteur public: les ministères français, via Mimo, rejoignent la Document Foundation"
author: Cyrille Chausson
date: 2013-06-19
href: http://www.lemagit.fr/economie/2013/06/19/libre-dans-le-secteur-public-les-ministeres-francais-via-mimo-rejoignent-la-document-foundation
tags:
- Administration
- Associations
---

> Le groupe de travail interministériel Mimo, en charge de valider une verison de LibreOffice pour plusieurs ministères français, rejoint le comité consultatif de la Document Foundation. Officialisant ainsi, avec ses 500 000 postes de travail, son statut de cadre de la communauté.
