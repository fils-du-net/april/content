---
site: LeDevoir.com
title: "La lente et difficile ascension du logiciel libre dans l’appareil gouvernemental"
author: Fabien Deglise
date: 2013-03-25
href: http://www.ledevoir.com/politique/quebec/374085/la-lente-et-difficile-ascension-du-logiciel-libre-dans-l-appareil-gouvernemental
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Marchés publics
- International
---

> Outré par l’exclusion systématique des logiciels libres dans les contrats gouvernementaux, Cyrille Béraud, patron de l’entreprise Savoir Faire Linux, intentait il y a cinq ans une poursuite contre la Régie des rentes du Québec. À la suite de la procédure, dont l’entreprise est sortie victorieuse, l’industrie du logiciel libre a réussi à mettre en lumière un environnement informatique gouvernemental dominé et malsain pour les fonds publics comme pour l’entrée du Québec dans la modernité.
