---
site: LesNews
title: "Logiciel libre: le gouvernement Marois refuse une motion de la CAQ, de Québec solidaire et du PLQ"
author: courrier parlementaire
date: 2013-03-20
href: http://lesnews.ca/politique/40695-logiciel-libre-le-gouvernement-marois-refuse-une-motion-de-la-caq-de-quebec-solidaire-et-du-plq
tags:
- Administration
- Économie
- Institutions
- International
---

> Le député Chrisitan Dubé de la Coalition Avenir Québec, appuyé par ses collègues Henri-François Gautrin du Parti libéral et Françoise David de Québec solidaire, a présenté, ce matin, une motion pour «que l’Assemblée nationale exige du gouvernement qu’il démontre une réelle ouverture en faveur de l’utilisation du logiciel libre au sein de ses ministères et organismes».
