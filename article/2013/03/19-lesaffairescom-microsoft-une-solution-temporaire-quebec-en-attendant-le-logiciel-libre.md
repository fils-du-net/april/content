---
site: LesAffaires.com
title: "Microsoft, une solution temporaire à Québec en attendant le logiciel libre?"
author: Valérie Lesage
date: 2013-03-19
href: http://www.lesaffaires.com/techno/technologies-et-telecommunications/microsoft-en-attendant-le-logiciel-libre/555459
tags:
- Logiciels privateurs
- Administration
- Économie
- Sensibilisation
- Éducation
- International
---

> Le renouvellement possible des licences de la suite Office de Microsoft sans appel d’offres par le gouvernement Marois apparaît comme une mesure transitoire, le temps que le secteur du logiciel libre prenne des forces au Québec. Le président du Conseil du trésor, Stéphane Bédard, annonce d’ailleurs aujourd’hui une série de mesures pour permettre au gouvernement de développer son expertise du logiciel libre et d’en intensifier l’utilisation au sein des organismes publics.
