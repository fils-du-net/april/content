---
site: Libération.fr
title: "Repères. Les valeurs de la famille internet"
date: 2013-03-01
href: http://www.liberation.fr/medias/2013/03/01/les-valeurs-de-la-famille-internet_885710
tags:
- Internet
- Économie
- Éducation
---

> La Richesse des réseaux: marchés et libertés à l’heure du partage social, de Yochai Benkler, PUL, 2006.
