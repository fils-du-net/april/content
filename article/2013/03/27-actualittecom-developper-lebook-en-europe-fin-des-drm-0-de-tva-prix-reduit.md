---
site: ActuaLitté.com
title: "Développer l'ebook en Europe: Fin des DRM, 0% de TVA, prix réduit"
author: Nicolas Gary
date: 2013-03-27
href: http://www.actualitte.com/legislation/developper-l-ebook-en-europe-fin-des-drm-0-de-tva-prix-reduit-41304.htm
tags:
- Internet
- Institutions
- DRM
- Europe
---

> La sénatrice Catherine Morin-Desailly, membre de l'Union centriste, vient de publier un rapport d'information, au nom de la commission des affaires européennes, intitulé L'Union européenne, colonie du monde numérique. Évoquant le monde numérique comme la seconde révolution cognitive, la sénatrice pose la problématique d'entrée: «Il est frappant de constater que, même sur ce sujet, le gouvernement raisonne de manière tellement franco-française.»
