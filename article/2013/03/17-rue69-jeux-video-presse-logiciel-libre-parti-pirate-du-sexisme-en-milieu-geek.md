---
site: Rue89
title: "Jeux vidéo, presse, logiciel libre, Parti pirate...: du sexisme en milieu geek"
author: Yann Guégan
date: 2013-03-17
href: http://www.rue89.com/rue69/2013/03/17/jeux-video-presse-logiciel-libre-parti-pirate-du-sexisme-en-milieu-geek-240615
tags:
- Internet
---

> C’est un réquisitoire articulé avec soin et nourri de dizaines d’exemples qui font froid dans le dos. Sur le site Genre! , la «gameuse» féministe Mar_Lard s’attaque au sexisme dans les communautés geek.
