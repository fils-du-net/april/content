---
site: Framablog
title: "DRM dans HTML5: la réponse de Cory Doctorow à Tim Berners-Lee"
author: Cory Doctorow (traduit par framablog)
date: 2013-03-14
href: http://www.framablog.org/index.php/post/2013/03/14/drm-html5-doctorow
tags:
- Internet
- Brevets logiciels
- DRM
- Innovation
---

> Ajouter des DRM au standard HTML aura des répercussions importantes qui seront incompatibles avec les règles fondamentales du W3C.
