---
site: Le Monde.fr
title: "Le Conseil national du numérique se prononce pour la neutralité du Net"
author: Guénaël Pépin
date: 2013-03-12
href: http://www.lemonde.fr/technologies/article/2013/03/12/le-conseil-national-du-numerique-se-prononce-pour-la-neutralite-du-net_1846863_651865.html
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Neutralité du Net
---

> L'institut consultatif s'est prononcé à l'unanimité en faveur d'une loi sur le principe de neutralité des réseaux, censé garantir l'accès universel aux contenus en ligne.
