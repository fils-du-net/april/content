---
site: Nouvelle République
title: "Richard Stallman: un génie à l'ère libre"
author: Hélène Echasseriau
date: 2013-03-20
href: http://www.lanouvellerepublique.fr/Deux-Sevres/Actualite/24-Heures/n/Contenus/Articles/2013/03/20/Richard-Stallman-un-genie-a-l-ere-libre-1377752
tags:
- Sensibilisation
- Philosophie GNU
- Promotion
---

> Il est mondialement connu. L’Américain Richard Matthew Stallman, génie de l’informatique, était à Niort hier pour défendre les logiciels libres.
