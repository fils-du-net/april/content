---
site: 01net.
title: "Microsoft attaqué par des défenseurs espagnols des logiciels open source"
author: Pierre Fontaine
date: 2013-03-27
href: http://www.01net.com/editorial/589781/microsoft-attaque-par-des-defenseurs-espagnols-des-logiciels-open-source
tags:
- Entreprise
- Institutions
- Associations
- DRM
- Informatique-deloyale
- Europe
---

> Comme aux grandes heures de la lutte contre le Microsoft des années 90, une association espagnole d’utilisateurs et de développeurs de logiciels open source, comptant 8 000 membres, a porté plainte contre Microsoft devant la Commission européenne.
