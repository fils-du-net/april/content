---
site: ZDNet
title: "Document Freedom Day, pour promouvoir les formats ouverts"
author: Thierry Noisette
date: 2013-03-28
href: http://www.zdnet.fr/actualites/document-freedom-day-pour-promouvoir-les-formats-ouverts-39788738.htm
tags:
- Internet
- April
- Standards
---

> Journée annuelle de célébration des formats ouverts qui permettent l'accès à l'information, destinée à toucher un public élargi, le Document Freedom Day a fêté sa sixième édition.
