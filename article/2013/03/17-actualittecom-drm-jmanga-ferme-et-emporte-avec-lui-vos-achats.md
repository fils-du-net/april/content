---
site: ActuaLitté.com
title: "DRM: JManga ferme et emporte avec lui vos achats"
author: Morel Bastien
date: 2013-03-17
href: http://www.actualitte.com/univers-manga/drm-jmanga-ferme-et-emporte-avec-lui-vos-achats-41000.htm
tags:
- Entreprise
- Internet
- DRM
- Informatique-deloyale
---

> La plateforme de lecture numérique Jmanga vient d'annoncer l'arrêt progressif de ses services sans donner plus de détail sur les raisons de cette fermeture. Le service ne proposant ni téléchargement ni lecture hors ligne, présence de DRM oblige, les lecteurs perdent ainsi toute trace de leur lecture, achetée pourtant légalement
