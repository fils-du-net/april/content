---
site: Le Huffington Post
title: "Mehdi Benchoufi: Des hyperliens aux hypermiens"
author: Mehdi Benchoufi
date: 2013-03-08
href: http://www.huffingtonpost.fr/mehdi-benchoufi/distribution-information-internet_b_2829438.html
tags:
- Internet
- Économie
- Institutions
- Open Data
---

> L'époque est féconde. Si nous devions hasarder une lecture de l'histoire des forces sociales ramenée à l'essentiel, nous dirions que la lutte entre possédants et possédés ou l'affrontement entre entreprenants et assistés, s'est toujours abstraite dans une opposition entre informants et informés.
