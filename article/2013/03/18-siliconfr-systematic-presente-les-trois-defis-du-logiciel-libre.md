---
site: Silicon.fr
title: "Systematic présente les trois défis du logiciel libre"
author: David Feugey
date: 2013-03-18
href: http://www.silicon.fr/les-trois-defis-du-logiciel-libre-la-qualite-logicielle-le-deluge-des-donnees-et-lapres-pc-84394.html
tags:
- Entreprise
- Économie
- Innovation
---

> Le Groupe Thématique Logiciel Libre du pôle de compétitivité Systematic Paris-Région livre les résultats d’un sondage effectué auprès de ses partenaires. Ces derniers identifient les défis futurs du monde open source.
