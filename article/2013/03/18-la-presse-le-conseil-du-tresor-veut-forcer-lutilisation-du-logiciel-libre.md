---
site: La Presse
title: "Le Conseil du trésor veut forcer l'utilisation du logiciel libre"
author: Simon Boivin
date: 2013-03-18
href: http://www.lapresse.ca/le-soleil/affaires/techno/201303/18/01-4632258-le-conseil-du-tresor-veut-forcer-lutilisation-du-logiciel-libre.php
tags:
- Logiciels privateurs
- Administration
- Institutions
- Éducation
- International
---

> Le président du Conseil du trésor, Stéphane Bédard, forcera l'utilisation du logiciel libre pour sortir l'État québécois de sa dépendance aux multinationales de l'informatique. Malgré cela, certains acteurs de l'industrie craignent que la situation demeure un «bar ouvert» pour les géants comme Microsoft.
