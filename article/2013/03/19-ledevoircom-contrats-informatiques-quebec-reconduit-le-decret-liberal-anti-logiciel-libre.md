---
site: LeDevoir.com
title: "Contrats informatiques - Québec reconduit le décret libéral anti-logiciel libre"
author: Fabien Deglise
date: 2013-03-19
href: http://www.ledevoir.com/societe/actualites-en-societe/373584/quebec-reconduit-le-decret-liberal-anti-logiciel-libre
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Éducation
- Marchés publics
- International
---

> Après l’avoir fortement décrié en 2011, le Parti québécois se prépare à reconduire d’ici à la fin du mois de mars un décret qui permet à l’appareil gouvernemental de faire fi des logiciels libres lors d’appels d’offres, et ce, au profit de solutions informatiques dites privatives, comme celles portées par la multinationale américaine Microsoft, a appris Le Devoir.
