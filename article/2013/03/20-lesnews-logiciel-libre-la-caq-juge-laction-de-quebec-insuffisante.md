---
site: LesNews
title: "Logiciel libre: la CAQ juge l'action de Québec insuffisante"
author: Courrier parlementaire
date: 2013-03-20
href: http://lesnews.ca/politique/40600-logiciel-libre-la-caq-juge-laction-de-quebec-insuffisante
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Marchés publics
- International
---

> Le 1er avril prochain, le gouvernement mettra sur pied le Centre d’expertise en logiciel libre.
