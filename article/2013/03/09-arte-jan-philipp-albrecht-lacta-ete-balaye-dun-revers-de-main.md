---
site: Arte
title: "Jan-Philipp Albrecht: \"L'ACTA a été balayé d'un revers de main\""
date: 2013-03-09
href: http://www.arte.tv/fr/jan-philipp-albrecht-l-acta-a-ete-balaye-d-un-revers-de-main/7377954.html
tags:
- Internet
- Économie
- Partage du savoir
- Institutions
- Droit d'auteur
- Europe
- ACTA
---

> En juillet 2012, l'ACTA a échoué au Parlement européen. Au préalable il y avait eu d'importantes manifestations dans le monde entier. Où en est l'ACTA aujourd'hui?
