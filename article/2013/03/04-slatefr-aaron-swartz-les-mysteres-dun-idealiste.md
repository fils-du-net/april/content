---
site: Slate.fr
title: "Aaron Swartz, les mystères d'un idéaliste"
author: Justin Peters
date: 2013-03-04
href: http://www.slate.fr/story/68985/aaron-swartz-suicide
tags:
- Internet
- Partage du savoir
- Institutions
- Droit d'auteur
- Contenus libres
- Open Data
---

> Poursuivi par la justice américaine, ce jeune activiste d'Internet s'est suicidé début janvier. Pourquoi ce petit génie qui voulait sauver le monde n’a-t-il pas réussi à se sauver lui-même?
