---
site: Sud Ouest
title: "Les militants des logiciels libres partagent leur savoir"
author: Philippe Ménard
date: 2013-03-04
href: http://www.sudouest.fr/2013/03/04/les-militants-des-logiciels-libres-partagent-leur-savoir-983738-882.php
tags:
- Sensibilisation
- Associations
---

> Une rencontre régionale des adeptes de Linux avait lieu samedi en Charente.
