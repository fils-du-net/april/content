---
site: nonfiction.fr
title: "Internet: les \"Modernes\" ont-ils vaincu les \"Anciens\"?"
author: Patrice Carré
date: 2013-03-18
href: http://www.nonfiction.fr/article-6431-internet__les_modernes_ont_ils_vaincu_les_anciens_.htm
tags:
- Internet
- Institutions
- Associations
- Licenses
- Sciences
- Europe
- Open Data
- ACTA
---

> Un monde "nouveau", porté par Internet, ses utopies, ses anonymes… est-il sur le point de se substituer au monde ancien ; celui dans lequel nous vivons?
