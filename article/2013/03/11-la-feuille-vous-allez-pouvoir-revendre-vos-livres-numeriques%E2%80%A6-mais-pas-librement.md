---
site: La feuille
title: "Vous allez pouvoir revendre vos livres numériques… mais pas librement"
author: Hubert Guillaud
date: 2013-03-11
href: http://lafeuille.blog.lemonde.fr/2013/03/11/vous-allez-pouvoir-revendre-vos-livres-numeriques
tags:
- Entreprise
- Économie
- Brevets logiciels
- DRM
- Droit d'auteur
---

> Un livre et sa version numérique sont exactement semblables, sauf que, si vous ne l'aimez pas ou une fois que vous l'avez lu, vous ne pouvez pas revendre sa version numérique. Mais cela pourrait prochainement changer, estime David Streitfeld pour le New York Times.
