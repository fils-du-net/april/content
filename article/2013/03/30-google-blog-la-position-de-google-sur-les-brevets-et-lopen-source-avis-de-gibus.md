---
site: Framablog
title: "La position de Google sur les brevets et l'open source (+ avis de Gibus)"
author: Duane Valz
date: 2013-03-30
href: http://www.framablog.org/index.php/post/2013/03/30/google-brevets-open-source
tags:
- Entreprise
- April
- Brevets logiciels
---

> Aujourd’hui, nous faisons un pas de plus vers ce but en annonçant l’Open Patent Non-Assertion Pledge (Engagement ouvert de non-application des Brevets): nous nous engageons à ne poursuivre aucun utilisateur, distributeur ou dévelopeur de logiciel open source sur la base des brevets spécifiés, à moins d’avoir d’abord été attaqués.
