---
site: LaDepeche.fr
title: "Gratentour. Logiciels et jeux en toute liberté"
date: 2013-03-08
href: http://www.ladepeche.fr/article/2013/03/08/1577180-gratentour-logiciels-et-jeux-en-toute-liberte.html
tags:
- Sensibilisation
- Associations
---

> Le Gratentelier (Atelier Informatique Gratentour) en association avec la mairie, les services Jeunesse, l'atelier Couture et le Club Informatique de Montrabé, participe à la manifestation nationale «Libre en Fête» le samedi 9 mars à partir de 14 heures et la nuit du samedi au dimanche pour les joueurs à la salle polyvalente
