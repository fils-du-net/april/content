---
site: Framablog
title: "La génération GitHub"
author: Mikeal Rogers (traduit par framablog)
date: 2013-03-11
href: http://www.framablog.org/index.php/post/2013/03/11/github-generation
tags:
- Entreprise
- Internet
- Sensibilisation
- Innovation
- Licenses
---

> GitHub a été conçu pour être une plate-forme de collaboration logicielle ouverte, mais c’est devenu une plate-forme pour déposer beaucoup plus de choses que du simple code. Elle est maintenant utilisée par des artistes, des créateurs, des propriétaires de maisons et des tas d’autres gens, par des entreprises entières… et même par des municipalités.
