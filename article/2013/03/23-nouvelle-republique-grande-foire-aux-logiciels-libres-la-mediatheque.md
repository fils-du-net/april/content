---
site: Nouvelle République
title: "Grande foire aux logiciels libres à la médiathèque"
author: Bertrand Slézak
date: 2013-03-23
href: http://www.lanouvellerepublique.fr/Toute-zone/Loisirs/24H/n/Contenus/Articles/2013/03/23/Grande-foire-aux-logiciels-libres-a-la-mediatheque-1382117
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Associations
- Promotion
---

> La médiathèque Maurice-Genevoix propose, aujourd’hui, de faire installer gratuitement des programmes alternatifs pour les ordinateurs.
