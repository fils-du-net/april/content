---
site: "France 3 Poitou-Charentes"
title: "Richard Stallman, le père du logiciel libre, fait salle comble dans la région"
author: M.H.
date: 2013-03-20
href: http://poitou-charentes.france3.fr/2013/03/20/richard-stallman-le-pere-du-logiciel-libre-fait-salle-comble-dans-la-region-219761.html
tags:
- Philosophie GNU
- Promotion
---

> Si vous n'êtes pas féru d'informatique, vous ne le connaissez peut-être pas mais pour nombre d'informaticiens ou d'amateurs, c'est un modèle à suivre. Richard Stallman est américain et de la même génération que Bill Gates mais les ressemblances s'arrêtent là.
