---
site: "cio-online.com"
title: "Jean-Marc Ayrault impose une gouvernance cohérente à tout l'Etat"
author: Bertrand Lemaire
date: 2013-03-15
href: http://www.cio-online.com/actualites/lire-jean-marc-ayrault-impose-une-gouvernance-coherente-a-tout-l-etat-4958-page-1.html
tags:
- Administration
- April
- RGI
---

> Une circulaire a été publiée par le Premier Ministre. Elle impose une gouvernance cohérente pour tous les systèmes d'information de l'Etat.
