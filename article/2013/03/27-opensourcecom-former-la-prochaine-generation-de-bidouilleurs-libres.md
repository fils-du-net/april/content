---
site: Framablog
title: "Former la prochaine génération de bidouilleurs libres"
author: Dave Neary (Traduction Framalang)
date: 2013-03-27
href: http://www.framablog.org/index.php/post/2013/03/27/generation-hacker-education
tags:
- Sensibilisation
- DADVSI
- Éducation
---

> En tant que père de trois enfants de 5, 7 et 10 ans, j’ai hâte de partager avec eux les valeurs qui m’ont attiré vers l’open source: partager et créer ensemble des choses géniales, prendre le contrôle de son environnement numérique, adopter la technologie comme moyen de communication plutôt qu’un média de consommation de masse.
