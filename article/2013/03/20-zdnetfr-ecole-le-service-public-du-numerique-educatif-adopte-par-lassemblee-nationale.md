---
site: ZDNet.fr
title: "Ecole: le \"service public du numérique éducatif\" adopté par l'Assemblée nationale"
author: Antoine Duvauchelle
date: 2013-03-20
href: http://www.zdnet.fr/actualites/ecole-le-service-public-du-numerique-educatif-adopte-par-l-assemblee-nationale-39788421.htm
tags:
- Entreprise
- April
- Institutions
- Éducation
---

> Le numérique à l'école était une promesse du ministère de l'Education nationale. Il a été inclus dans ce projet de loi pour la refondation de l'école, adopté en première lecture à l'Assemblée nationale.
