---
site: Developpez.com
title: "La FSF proteste contre l'ajout d'une API pour la DRM à HTML5 par le W3C et lance une pétition pour l'interdire"
author: Stéphane le calme
date: 2013-03-19
href: http://www.developpez.com/actu/54411/La-FSF-proteste-contre-l-ajout-d-une-API-pour-la-DRM-a-HTML5-par-le-W3C-et-lance-une-petition-pour-l-interdire
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Interopérabilité
- Associations
- DRM
- Standards
---

> La Free Software Foundation (FSF) s’insurge contre les efforts de Google, Microsoft et Netfix (voir section «Retrouvez le dossier complet de la rédaction»).
