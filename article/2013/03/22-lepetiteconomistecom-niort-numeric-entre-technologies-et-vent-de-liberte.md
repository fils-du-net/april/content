---
site: lepetiteconomiste.com
title: "Niort Numeric, entre technologies et vent de liberté"
date: 2013-03-22
href: http://www.lepetiteconomiste.com/Niort-Numeric-entre-technologies-4219
tags:
- Sensibilisation
- Innovation
- Philosophie GNU
- Promotion
---

> Mardi dernier, Niort était au "tout numérique" à travers cette journée événement créée à l’initiative de l’IUT: des ateliers pratiques autour des technologies, des rencontres avec des professionnels du secteur, des offres d’emploi (une soixantaine), des jeunes venus d’établissements scolaires de la région, et l’événement: la conférence de Richard Matthew Stallman, le "père" du logiciel libre.
