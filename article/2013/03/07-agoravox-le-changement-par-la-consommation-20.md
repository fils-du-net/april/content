---
site: AgoraVox
title: "Le changement par la consommation 2.0"
author: omarus
date: 2013-03-07
href: http://www.agoravox.fr/actualites/societe/article/le-changement-par-la-consommation-131913
tags:
- Entreprise
- Internet
- Économie
---

> Le modèle capitaliste de la société de consommation, qui existe depuis la fin de la deuxième guerre mondiale, a généré beaucoup d’espoirs. Il a permis une profusion de biens et de services et un gain de pouvoir d’achat était même envisageable pour tout le monde. Aujourd’hui, ce système est de plus en plus rejeté par les nouvelles générations. Un nouveau modèle se met progressivement en place pour le remplacer: La société de consommation 2.0!
