---
site: PC INpact
title: "DRM, brevets logiciels, etc. accusés d’empêcher la recherche informatique"
author: Marc Rees
date: 2013-03-20
href: http://www.pcinpact.com/news/78404-drm-brevets-logiciels-etc-accuses-d-empecher-recherche-informatique.htm
tags:
- Institutions
- Brevets logiciels
- DADVSI
- DRM
---

> Rattaché directement auprès du premier ministre, le Centre d’analyse stratégique a pour mission «d’éclairer le Gouvernement dans la définition et la mise en œuvre de ses orientations stratégiques en matière économique, sociale, environnementale ou technologique». Ses derniers travaux portent justement sur la sécurité informatique et notamment les verrous juridiques qui freinent les expérimentations (PDF).
