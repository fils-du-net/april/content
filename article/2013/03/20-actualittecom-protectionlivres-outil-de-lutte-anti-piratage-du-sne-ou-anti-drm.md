---
site: ActuaLitté.com
title: "ProtectionLivres: outil de lutte anti-piratage du SNE ou anti-DRM?"
author: Nicolas Gary
date: 2013-03-20
href: http://www.actualitte.com/legislation/protectionlivres-outil-de-lutte-anti-piratage-du-sne-ou-anti-drm-41114.htm
tags:
- Internet
- Économie
- Associations
- DRM
---

> Ce matin même, le Syndicat national de l'édition réunissait la presse pour faire un petit point avant que ne débutent les festivités du Salon du livre. Entre autres documents fournis dans le dossier de presse, un document présentant les Solutions collectives de lutte contre le piratage. Vincent Montagne, président du SNE, expliquait ainsi que l'un des outils avait été calqué sur le Copyright Infringement Portal, mis en place par le syndicat des éditeurs anglais.
