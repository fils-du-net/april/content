---
site: Silicon.fr
title: "Open source: Google dévoile un pacte de non-agression"
author: Ariane Beky
date: 2013-03-29
href: http://www.silicon.fr/google-pacte-open-source-brevets-84838.html
tags:
- Entreprise
- Brevets logiciels
---

> Par le biais du pacte Open Patent Non-Assertion (OPN), Google s’engage à ne pas poursuivre les éditeurs et utilisateurs de logiciels open source pour violation de certains brevets.
