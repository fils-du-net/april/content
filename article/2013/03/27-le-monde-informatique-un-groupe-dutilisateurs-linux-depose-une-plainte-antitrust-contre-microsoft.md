---
site: Le Monde Informatique
title: "Un groupe d'utilisateurs Linux dépose une plainte antitrust contre Microsoft"
author: Jean Elyan
date: 2013-03-27
href: http://www.lemondeinformatique.fr/actualites/lire-un-groupe-d-utilisateurs-linux-depose-une-plainte-antitrust-contre-microsoft-53002.html
tags:
- Entreprise
- Institutions
- Associations
- DRM
- Informatique-deloyale
- Europe
---

> Hier, le groupe d'utilisateurs Linux espagnol Hispalinux a déposé une plainte antitrust contre Microsoft devant la Commission européenne. Elle concerne l'utilisation de la fonction UEFI (Unified Extensible Firmware Interface) d'amorçage sécurisé Secure Boot dans les PC sous Windows 8.
