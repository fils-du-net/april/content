---
site: ITespresso
title: "Brevets: Google plus clément envers la communauté open source"
author: Clément Bohic
date: 2013-03-29
href: http://www.itespresso.fr/brevets-google-clement-communaute-open-source-63352.html
tags:
- Entreprise
- Brevets logiciels
---

> A l’égard des membres de la communauté open source qui exploitent certains de ses brevets, Google fait preuve d’indulgence avec un pacte de non-agression.
