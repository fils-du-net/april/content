---
site: Numerama
title: "La Californie s'ouvre aux cours universitaires sous licence Creative Commons"
author: Julien L.
date: 2013-03-14
href: http://www.numerama.com/magazine/25386-la-californie-s-ouvre-aux-cours-universitaires-sous-licence-creative-commons.html
tags:
- Internet
- Partage du savoir
- Institutions
- Droit d'auteur
- Éducation
- Licenses
- Contenus libres
- International
---

> En Californie, une loi propose de passer des cours universitaires sous licence de libre diffusion (Creative Commons) afin de permettre aux étudiants d'y accéder sans trop de contraintes. Une décision qui pourrait inspirer les parlementaires français, actuellement engagés dans l'examen du projet de loi sur la refondation de l'école républicaine.
