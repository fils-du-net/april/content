---
site: Métro
title: "Humanitaire 2.0: aider avec son clavier"
author: Mathias Marchal
date: 2013-03-01
href: http://journalmetro.com/plus/techno/266386/humanitaire-2-0-aider-avec-son-clavier
tags:
- Partage du savoir
- Institutions
- International
---

> Grâce aux logiciels libres, n’importe quel internaute peut devenir utile au cours d’une crise humanitaire. Sans même quitter son siège et son bureau d’ordinateur. Bienvenue dans le monde des cartes humanitaires 2.0, un domaine avec tellement de potentiel que le ministère des Affaires étrangères se penche sur le phénomène.
