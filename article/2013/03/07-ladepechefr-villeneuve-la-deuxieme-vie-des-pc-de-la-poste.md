---
site: LaDepeche.fr
title: "Villeneuve. La deuxième vie des PC de La Poste"
author: J.-L. A
date: 2013-03-07
href: http://www.ladepeche.fr/article/2013/03/07/1576849-villeneuve-la-deuxieme-vie-des-pc-de-la-poste.html
tags:
- Entreprise
- Administration
- Sensibilisation
- Associations
- Video
---

> «C'est la 7e convention de ce type que nous signons dans le Lot-et-Garonne». Pour Michel Faucher, délégué départemental du Groupe «La Poste», il s'agit bien plus que d'un partenariat avec comme finalité la mise à disposition d'ordinateurs anciens
