---
site: LeDevoir.com
title: "Soutien aux logiciels libres - De la «poudre aux yeux», dit la CAQ"
author: Fabien Deglise
date: 2013-03-20
href: http://www.ledevoir.com/politique/quebec/373653/de-la-poudre-aux-yeux-dit-la-caq
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Éducation
- Marchés publics
- International
---

> Les promesses de soutien de l’industrie du logiciel libre par Québec ne sont que de la «poudre aux yeux», un «écran de fumée», «visant à dissimuler des dépenses faramineuses en contrats avec les grandes firmes informatiques», a dénoncé mardi la Coalition avenir Québec (CAQ) au lendemain d’une annonce faite par le gouvernement disant soutenir cette industrie, tout en maintenant, paradoxalement, un décret qui éloigne le logiciel libre des appels d’offres de l’appareil gouvernemental.
