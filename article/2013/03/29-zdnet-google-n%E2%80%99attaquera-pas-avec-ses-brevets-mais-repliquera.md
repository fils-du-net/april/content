---
site: ZDNet
title: "Google n’attaquera pas avec ses brevets, mais répliquera"
date: 2013-03-29
href: http://www.zdnet.fr/actualites/open-source-google-n-attaquera-pas-avec-ses-brevets-mais-repliquera-39788791.htm
tags:
- Entreprise
- Brevets logiciels
---

> Au travers de l’Open Patent Non-Assertion (OPN) Pledge, Google met à disposition certains de ses brevets comme protection juridique pour les acteurs de l’Open Source, et s'engage à ne jamais en faire un usage devant la justice, à moins d'être attaqué
