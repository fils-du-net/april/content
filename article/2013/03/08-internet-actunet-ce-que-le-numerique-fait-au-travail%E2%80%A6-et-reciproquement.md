---
site: internet ACTU.net
title: "Ce que le numérique fait au travail… et réciproquement"
author: Invité extérieur
date: 2013-03-08
href: http://www.internetactu.net/2013/03/08/ce-que-le-numerique-fait-au-travail-et-reciproquement
tags:
- Entreprise
- Internet
- Économie
- Éducation
---

> C’est essentiellement sous l’angle de l’emploi et de la crise économique que la question du travail se pose aujourd’hui dans les médias. Or depuis les années 90, les économies des pays de l’OCDE se caractérisent par une croissance faible (ponctuée de crises économiques régulières) et un taux de chômage élevé. Le temps de travail a diminué de manière constante, l’emploi à temps partiel a augmenté ainsi que le chômage longue durée. La part du travail dans le Produit intérieur brut a elle aussi diminué. De là à croire que le travail rapporte moins, il n’y a qu’un pas…
