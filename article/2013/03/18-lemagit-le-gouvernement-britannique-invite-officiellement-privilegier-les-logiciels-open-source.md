---
site: LeMagIT
title: "Le gouvernement britannique invite officiellement à privilégier les logiciels Open Source"
author: Cyrille Chausson
date: 2013-03-18
href: http://www.lemagit.fr/technologie/applications/open-source/2013/03/18/le-gouvernement-britannique-invite-a-privilegier-les-logiciels-open-source
tags:
- Logiciels privateurs
- Administration
- Institutions
- International
---

> Dans une version bêta de ses bonnes pratiques pour les services numériques administratifs, le gouvernement britannique a inscrit noir sur blanc ses préférences pour le logiciel libre, au détriment des logiciels dits propriétaires. Ceux-ci y ont une place mais à de rares occasions.
