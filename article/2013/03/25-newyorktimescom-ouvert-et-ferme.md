---
site: Framablog
title: "Ouvert et fermé"
author: Evgeny Morozov (Traduction Framalang)
date: 2013-03-25
href: http://www.framablog.org/index.php/post/2013/03/25/open-closed-morozov
tags:
- Matériel libre
- Institutions
- Éducation
- Open Data
---

> «L’impression 3D peut-elle être subversive?» demande une voix dans la vidéo la plus terrifiante que vous puissiez trouver sur Internet ce mois-ci. Il s’agit de la bande-annonce pour Defcad.com, un moteur de recherche pour des modèles imprimables en 3D de choses que les «institutions et les industries ont pour intérêt commun de garder loin de nous», incluant des «appareils médicaux, médicaments, biens, pistolets».
