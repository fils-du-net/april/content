---
site: LeDevoir.com
title: "Québec adopte deux décrets contre le logiciel libre"
author: Fabien Deglise
date: 2013-03-29
href: http://www.ledevoir.com/politique/quebec/374522/quebec-adopte-deux-decrets-contre-le-logiciel-libre
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Marchés publics
- International
---

> Québec vient d’adopter deux décrets favorisant la multinationale Microsoft dans la mise à jour de plusieurs milliers de postes informatiques de la fonction publique, mais aussi permettant aux organismes publics d’octroyer des contrats informatiques à une dizaine de grands groupes informatiques, sans appel d’offres ni mise en concurrence de leurs produits avec les logiciels libres.
