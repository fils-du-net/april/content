---
site: internet ACTU.net
title: "La technologie est-elle toujours la solution? (1/2): le biais de l’internet-centrisme"
author: Hubert Guillaud
date: 2013-03-27
href: http://www.internetactu.net/2013/03/27/la-technologie-est-elle-toujours-la-solution-12-le-biais-de-linternet-centrisme
tags:
- Internet
- Institutions
---

> To Save Everything, Click HereDe 2005 à 2007, le chercheur et éditorialiste, spécialiste de politique étrangère d’origine Bielarusse, Evgeny Morozov (@evgenymorozov), pensait que la technologie numérique était peut-être un moyen pour nous débarrasser des régimes autocratiques. Sa déception a été racontée dans un livre, The Net Delusion, où il s’en prenait à l’utopie du projet internet. Dans son nouveau livre Pour tout sauver, cliquez-là, le chercheur iconoclaste élargit sa critique pour comprendre les schémas de pensée à l’oeuvre derrière la révolution numérique.
