---
site: LeMagIT
title: "Linux en entreprise, dopé par le cloud"
author: Cyrille Chausson
date: 2013-03-29
href: http://www.lemagit.fr/technologie/datacenter-technologie/cloud-grid-computing/2013/03/29/linux-en-entreprise-dope-par-le-cloud
tags:
- Entreprise
- Sensibilisation
- Informatique en nuage
---

> Une étude de la Linux Foundation met en avant l’avancée de Linux dans les SI des entreprises, poussé par les élans Cloud. Linux est également de plus en plus présent dans les applications critiques.
