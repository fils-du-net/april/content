---
site: "Radio-Canada.ca"
title: "Un centre d'expertise en logiciel libre, mais un contrat à Microsoft"
date: 2013-03-18
href: http://www.radio-canada.ca/nouvelles/Politique/2013/03/18/002-quebec-centre-expertise-logiciel-libre-contrat-microsoft.shtml
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Associations
- Marchés publics
- International
---

> Le gouvernement du Québec annoncera mardi la création d'un «centre d'expertise» en logiciel libre, mais s'apprête tout de même à adopter un nouveau décret qui lui permettra d'accorder sans appel d'offres à Microsoft le contrat de mise à niveau des centaines de milliers d'ordinateurs de l'État.
