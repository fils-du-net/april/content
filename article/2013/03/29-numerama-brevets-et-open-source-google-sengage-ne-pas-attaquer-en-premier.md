---
site: Numerama
title: "Brevets et open source: Google s'engage à ne pas attaquer en premier"
author: Julien L.
date: 2013-03-29
href: http://www.numerama.com/magazine/25532-brevets-et-open-source-google-s-engage-a-ne-pas-attaquer-en-premier.html
tags:
- Entreprise
- Brevets logiciels
---

> Agacé par la guerre des brevets, Google prend de la hauteur: l'entreprise américaine a lancé une initiative en faveur de l'open source. Elle s'engage à ne pas attaquer leurs acteurs, sauf si ceux-ci ouvrent le feu en premier. Google espère que sa démarche inspirera d'autres sociétés.
