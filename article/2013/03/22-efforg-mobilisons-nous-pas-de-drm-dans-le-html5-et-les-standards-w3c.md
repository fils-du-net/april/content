---
site: Framablog
title: "Mobilisons-nous! Pas de DRM dans le HTML5 et les standards W3C"
author: Peter Eckersley et Seth Schoen (traduit par framablog)
date: 2013-03-22
href: http://www.framablog.org/index.php/post/2013/03/22/stop-drm-html5
tags:
- Entreprise
- April
- Vente liée
- DRM
---

> Un nouveau front est ouvert dans la bataille contre les DRM. Ces technologies, qui sont censées permettre le respect du copyright, n’ont jamais permis de rémunérer les créateurs. Par contre, que ce soit volontairement ou par accident, leur véritable effet est d’interférer avec l’innovation, l’usage légitime, la concurrence, l’interopérabilité et notre droit légitime à posséder nos biens.
