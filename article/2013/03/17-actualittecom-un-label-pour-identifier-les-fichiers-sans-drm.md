---
site: ActuaLitté.com
title: "Un label pour identifier les fichiers sans DRM"
author: Nicolas Gary
date: 2013-03-17
href: http://www.actualitte.com/usages/un-label-pour-identifier-les-fichiers-sans-drm-41031.htm
tags:
- Associations
- DRM
- Informatique-deloyale
---

> L'organisation Defective By Design vient de frapper un nouveau coup, dans la promotion des fichiers ne contenant pas de DRM. Que ce soit pour les livres, la musique, les vidéos, les DRM sont une technologie de contrôle exercé sur les médias numériques et les appareils qui servent à leur lecture. Essentiellement, le DRM prive le consommateur d'un usage plein de ce qu'il a acheté.
