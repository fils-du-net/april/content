---
site: Slate.fr
title: "Salaire à vie: l'emploi est mort, vive le salariat!"
author: Emmanuel Daniel
date: 2013-03-02
href: http://www.slate.fr/story/68185/salaire-vie-travail
tags:
- Entreprise
- Économie
- Éducation
---

> Et si, de la même manière que le suffrage universel est venu récompenser notre capacité à tous participer à la chose publique, un salaire à vie venait consacrer notre capacité à tous produire de la valeur? C'est l'idée mise en avant par l'économiste et sociologue Bernard Friot.
