---
site: Programmez.com
title: "Le gouvernement britannique souhaite privilégier les logiciels open source"
author: Frédéric Mazué
date: 2013-03-20
href: http://www.programmez.com/actualites.php?titre_actu=Le-gouvernement-britannique-souhaite-privilegier-les-logiciels-open-source$urlid_actu=13087
tags:
- Logiciels privateurs
- Administration
- Économie
- Interopérabilité
- Institutions
- Standards
- International
---

> Dans le cadre d'une opération visant à améliorer la gestion de son IT, le gouvernement britannique a publié un document intitulé "Open Source, Open Standards and Re-Use:  Government Action Plan", qui comme le nom le suggère, encourage très fortement à utiliser des logiciels open source au sein de l'administration britannique.
