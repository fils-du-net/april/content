---
site: LeJournalduNet
title: "Solutions propriétaires vs logiciels libres: un choix stratégique"
author: Alan Hale
date: 2013-03-13
href: http://www.journaldunet.com/solutions/expert/53653/solutions-proprietaires-vs-logiciels-libres---un-choix-strategique.shtml
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Licenses
---

> Une entreprise intelligente intégrée sait prendre des décisions avisées, fondées sur les innombrables données qu’elle recueille chaque seconde, jour après jour. Cependant les solutions propriétaires qui permettent d’adopter ce modèle s’accompagnent de frais de licence prohibitifs pour de nombreuses entreprises.
