---
site: Joinup
title: "April publishes English translation of France's free software policy"
author: Gijs Hillenius
date: 2013-03-20
href: http://joinup.ec.europa.eu/news/april-publishes-english-translation-frances-free-software-policy
tags:
- Administration
- April
- Institutions
- Associations
- English
---

> (La politique du gouvernement Français sur le Logiciel Libre est maintenant accessible en Anglais) The French government policy on free software is now available in English. The translation was published earlier today by April, a French advocacy organisation. It is not an official translation. However, experts involved in the creation of the original French text have not found misinterpretations, the advocacy group commented. The group hopes other public administrations will use the guideline to their benefit.
