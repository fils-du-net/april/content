---
site: leral.net
title: "La Police des firmes informatiques se manifeste au Sénégal"
date: 2013-03-15
href: http://www.leral.net/PROPRIETE-INTELLECTUELLE-La-Police-des-firmes-informatiques-se-manifeste-au-Senegal_a77516.html
tags:
- Logiciels privateurs
- Économie
- Institutions
- Brevets logiciels
- Droit d'auteur
- International
- ACTA
---

> Après l’hyper décrié Anticonterfeiting Trade Agreement(ACTA), négocié en secret pendant trois ans par une dizaine de pays industriels et l’Union Européenne, voici, encore, que quelques 11 pays négocient en secret le Trans-Pacific Partnership (TPP), un autre accord dont le chapitre sur la propriété intellectuelle cherche, vraisemblablement, à corser davantage le régime juridique international régissant actuellement ce domaine.
