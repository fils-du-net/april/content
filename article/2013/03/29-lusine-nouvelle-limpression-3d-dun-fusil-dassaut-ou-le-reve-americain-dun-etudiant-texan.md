---
site: L'usine Nouvelle
title: "L'impression 3D d'un fusil d'assaut ou le rêve américain d'un étudiant texan"
author: Julien Bonnet
date: 2013-03-29
href: http://www.usinenouvelle.com/article/l-impression-3d-d-un-fusil-d-assaut-ou-le-reve-americain-d-un-etudiant-texan.N194222
tags:
- Entreprise
- Internet
- Matériel libre
- Contenus libres
---

> Le magazine américain Vice diffuse un reportage consacré à Cody Wilson, un étudiant texan qui a réussi à produire des pièces de fusil d'assaut grâce à une imprimante 3D. Prochaine étape : produire une arme entière grâce à cette technologie.
