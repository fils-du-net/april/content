---
site: Rue89
title: "Egypte: un contrat pour Microsoft énerve les défenseurs du logiciel libre"
author: Tarek Amr
date: 2013-01-01
href: http://www.rue89.com/2013/01/01/egypte-un-contrat-pour-microsoft-enerve-les-defenseurs-du-logiciel-libre-238233
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Institutions
- International
---

> Nombreux sont les commentaires soulignant le rôle libérateur joué par la technologie lors des soulèvements survenus au Moyen-Orient; il y aurait encore plus à dire sur le rôle des soulèvements dans la libération de la technologie dans la région.
