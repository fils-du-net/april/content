---
site: LeJournalduNet
title: "La filière open source va bien, merci!"
author: Patrice Bertrand
date: 2013-01-28
href: http://www.journaldunet.com/web-tech/expert/53280/la-filiere-open-source-va-bien--merci.shtml
tags:
- Entreprise
- Économie
- Institutions
- Associations
- Innovation
---

> Il y a une diversité d’associations dans l’écosystème du logiciel libre, mais pour celles dont la vocation est de représenter les entreprises, la situation est très simple: d’une part une dizaine d’associations régionales, d’autre part le CNLL au niveau national.
