---
site: Silicon.fr
title: "La charte libre emploi séduit l'écosystème open source"
author: Ariane Beky
date: 2013-01-17
href: http://www.silicon.fr/charte-libre-emploi-ecosysteme-open-source-82760.html
tags:
- Entreprise
- Associations
---

> Plus de 40 entreprises du logiciel libre et open source en France ont signé la charte libre emploi lancée l’automne dernier.
