---
site: Les Echos
title: "Vers une stagnation de l’innovation"
author: Hubert Guillaud
date: 2013-01-12
href: http://blogs.lesechos.fr/internetactu-net/vers-une-stagnation-de-l-innovation-a12301.html
tags:
- Économie
- Innovation
- Sciences
---

> Et si, loin de vivre une explosion d’innovations, nous nous trouvions plutôt dans une phase de blocage ? Car s’il est vrai que nous assistons aujourd’hui à une multiplication des usages, ainsi qu’à un raffinement et une simplification de technologies déjà existantes (smartphones, web 2, etc.) les véritables innovations de rupture tardent finalement à se manifester.
