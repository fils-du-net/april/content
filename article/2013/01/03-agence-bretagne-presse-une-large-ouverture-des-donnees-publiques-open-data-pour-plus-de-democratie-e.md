---
site: Agence Bretagne Presse
title: "Une large ouverture des données publiques (open data) pour plus de démocratie en Bretagne"
author: Christian Rogel
date: 2013-01-03
href: http://www.agencebretagnepresse.com/fetch.php?id=28675
tags:
- Internet
- Administration
- Associations
- Licenses
- Open Data
---

> Vue de l'extérieur, la Bretagne semble une des régions où les données publiques sont les plus ouvertes.
