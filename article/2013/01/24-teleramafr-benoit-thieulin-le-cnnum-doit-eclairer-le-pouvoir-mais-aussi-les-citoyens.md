---
site: Télérama.fr
title: "Benoît Thieulin: Le CNNum doit éclairer le pouvoir, mais aussi les citoyens"
author: Jean-Baptiste Roch
date: 2013-01-24
href: http://www.telerama.fr/medias/beno-t-thieulin-le-cnnum-doit-clairer-le-pouvoir-mais-aussi-les-citoyens,92582.php
tags:
- Entreprise
- Institutions
---

> A quoi ça sert, le Conseil national du numérique? Son nouveau président Benoît Thieulin nous en explique les enjeux et se défend de tout conflit d'intérêts.
