---
site: LeJournalduNet
title: "Open Source: les recrutements prévus en 2013 en hausse"
author: Virgile Juhan
date: 2013-01-14
href: http://www.journaldunet.com/solutions/emploi-rh/open-source-les-recrutements-prevus-par-les-ssll-en-2013-0113.shtml
tags:
- Entreprise
---

> Alors que le nombre d'emplois créés dans l'Open Source devrait encore augmenter cette année, 4 SSLL françaises détaillent leurs prévisions de recrutements. De nouveaux profils sont déjà en tension.
