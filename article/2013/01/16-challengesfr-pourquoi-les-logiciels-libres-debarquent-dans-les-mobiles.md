---
site: challenges.fr
title: "Pourquoi les logiciels libres débarquent dans les mobiles"
author: Paul Loubière
date: 2013-01-16
href: http://www.challenges.fr/high-tech/20130116.CHA5152/pourquoi-les-logiciels-libres-debarquent-dans-les-mobiles.html
tags:
- Entreprise
- Internet
- Associations
- Innovation
---

> DECRYPTAGE La guerre des navigateurs, qui fait rage dans l'univers des PC, a ouvert un nouveau front dans la jungle des smartphones.
