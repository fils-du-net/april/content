---
site: ZDNet.fr
title: "La Slovénie légifère sur la neutralité de l'Internet"
author: Thierry Noisette
date: 2013-01-04
href: http://www.zdnet.fr/actualites/la-slovenie-legifere-sur-la-neutralite-de-l-internet-39785892.htm
tags:
- Internet
- Institutions
- Neutralité du Net
- Europe
- International
---

> Les Pays-Bas l'ont adoptée en 2012, la Belgique en discute, la France renâcle... Preuve que les avancées ne viennent pas toujours des grands Etats de l'UE, la Slovénie vient d'adopter une loi sur la neutralité du net.
