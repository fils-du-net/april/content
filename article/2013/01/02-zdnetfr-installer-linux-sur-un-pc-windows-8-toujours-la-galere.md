---
site: ZDNet.fr
title: "Installer Linux sur un PC Windows 8? Toujours la galère..."
author: Thierry Noisette
date: 2013-01-02
href: http://www.zdnet.fr/actualites/installer-linux-sur-un-pc-windows-8-toujours-la-galere-39785810.htm
tags:
- Entreprise
- Logiciels privateurs
- Interopérabilité
- Associations
- DRM
- Informatique-deloyale
---

> Depuis quelques semaines, une fonctionnalité intégrée aux nouvelles générations de BIOS fait râler: le "secure boot" permet certes de sécuriser le démarrage... Mais aussi d'empêcher l'installation d'OS alternatif.
