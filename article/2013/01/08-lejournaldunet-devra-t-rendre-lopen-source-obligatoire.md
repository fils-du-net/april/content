---
site: LeJournalduNet
title: "Devra-t-on rendre l'open source obligatoire?"
author: Patrice Bertrand
date: 2013-01-08
href: http://www.journaldunet.com/web-tech/expert/53129/devra-t-on-rendre-l-open-source-obligatoire.shtml
tags:
- Entreprise
- Interopérabilité
- Partage du savoir
- Standards
---

> L’open source a bien des avantages, mais il est des domaines où il est pratiquement obligatoire, ou devrait l’être. Car il rend les appareils plus fiables et renforce le contrôle de la sécurité des équipements et logiciels. De plus, l'open source est le seul gage d'une pérennité de long terme, puisqu'il est la voie naturelle de mise en œuvre des standards.
