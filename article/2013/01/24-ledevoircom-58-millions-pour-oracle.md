---
site: LeDevoir.com
title: "58 millions pour Oracle"
author: Fabien Deglise
date: 2013-01-24
href: http://www.ledevoir.com/societe/consommation/369114/58-millions-pour-oracle
tags:
- Logiciels privateurs
- Administration
- Économie
- Associations
- Éducation
- International
- Open Data
---

> Stupeur et consternation. Les défenseurs des logiciels libres et des données publiques en format ouvert ont vertement dénoncé mercredi la décision du comité exécutif de la Ville de Montréal d’accorder 22,2 millions de dollars de plus sur quatre ans pour l’achat et l’entretien de logiciels gardant l’administration captive des produits privatifs de la compagnie Oracle.
