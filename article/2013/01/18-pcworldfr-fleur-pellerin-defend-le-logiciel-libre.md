---
site: PCWorld.fr
title: "Fleur Pellerin défend le logiciel libre"
author: Denis Leclercq
date: 2013-01-18
href: http://www.pcworld.fr/logiciels/actualites,fleur-pellerin-faveur-logiciel-libre-reponse-assemblee-nationale-pacte-brevet-europeen,535137,1.htm
tags:
- Administration
- Économie
- April
- Institutions
- Brevets logiciels
---

> Après six mois de réflexion, Fleur Pellerin et son ministère des PME, de l'innovation et de l'économie numérique, annoncent être en faveur du logiciel libre. Mais ça manque encore de propositions concrètes...
