---
site: LeJournalduNet
title: "Les 1ers Etats-Généraux de l'Open Source lancés en France"
author: Dominique Filippone
date: 2013-01-11
href: http://www.journaldunet.com/solutions/dsi/etats-generaux-de-l-open-source-0113.shtml
tags:
- Entreprise
- Institutions
- Associations
---

> Sous le haut patronage de l'Etat, Syntec Numérique lance le 21 janvier les premiers Etats-Généraux de l'Open Source où des personnalités interviendront. Une convention nationale se tiendra ensuite en septembre.
