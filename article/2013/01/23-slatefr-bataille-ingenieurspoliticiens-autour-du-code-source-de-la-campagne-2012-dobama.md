---
site: Slate.fr
title: "Bataille ingénieurs/politiciens autour du code source de la campagne 2012 d'Obama"
author: Cécile Dehesdin
date: 2013-01-23
href: http://www.slate.fr/life/67505/techos-developeurs-obama-open-source-code
tags:
- Internet
- Institutions
- Innovation
- International
---

> Les développeurs engagés pour la réélection du président américain veulent rendre le code qu'ils ont mis au point open-source, mais les responsables politiques de la campagne ont peur de donner un avantage aux Républicains.
