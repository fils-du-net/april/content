---
site: ActuaLitté.com
title: "Vendre des manuels Open source, pour les rendre crédibles"
author: Oury Antoine
date: 2013-01-11
href: http://www.actualitte.com/pedagogies/vendre-des-manuels-open-source-pour-les-rendre-credibles-39532.htm
tags:
- Internet
- Économie
- Associations
- Éducation
- International
---

> La fondation 20 Millions Minds souhaitait simplement faire acte de désintéressement total, en produisant une série de manuels scolaires numériques gratuits, et open source qui plus est. Mais, face aux soupçons ou simplement à la méconnaissance totale des universités quant à leur travail et sa qualité, les développeurs ont décidé de les mettre en vente, pour une somme modique, via la plateforme Chegg.
