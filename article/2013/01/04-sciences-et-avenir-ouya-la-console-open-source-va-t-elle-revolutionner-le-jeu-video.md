---
site: Sciences et Avenir
title: "Ouya, la console Open Source va-t-elle révolutionner le jeu vidéo?"
author: Xavier Humbert
date: 2013-01-04
href: http://sciencesetavenir.nouvelobs.com/high-tech/20130104.OBS4487/ouya-la-console-open-source-va-t-elle-revolutionner-le-jeu-video.html
tags:
- Entreprise
- Internet
- Innovation
---

> Financée par les internautes et pourvue de jeux gratuits: la petite Ouya devrait être disponible en mai 2013. Menacera-t-elle l’hégémonie de Sony et Nintendo?
