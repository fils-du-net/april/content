---
site: Framablog
title: "L'éducation utilise une licence Creative Commons défectueuse"
author: R. Stallman
date: 2013-01-31
href: http://www.framablog.org/index.php/post/2013/01/31/stallman-creative-commons-non-commercial
tags:
- Partage du savoir
- Droit d'auteur
- Éducation
- Licenses
- Contenus libres
---

> Des universités de premier plan utilisent une licence non-libre pour leurs ressources d’enseignement numérique. C’est déjà une mauvaise chose en soi, mais pire encore, la licence utilisée a un sérieux problème intrinsèque.
