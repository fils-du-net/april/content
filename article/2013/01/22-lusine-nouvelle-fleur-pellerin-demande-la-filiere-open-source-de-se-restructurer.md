---
site: L'usine Nouvelle
title: "Fleur Pellerin demande à la filière Open Source de se restructurer"
author: Ridha Loukil
date: 2013-01-22
href: http://www.usinenouvelle.com/article/fleur-pellerin-demande-a-la-filiere-open-source-de-se-restructurer.N189984
tags:
- Entreprise
- Administration
- Économie
- Interopérabilité
- April
- Institutions
- Associations
- Brevets logiciels
---

> En donnant le coup d’envoi des états généraux de l’Open source, la ministre de l’Economie numérique a insisté sur l’importance de ce secteur pour l’avenir du numérique en France. Elle a invité la filière à se restructurer pour favoriser l’émergence de grands champions.
