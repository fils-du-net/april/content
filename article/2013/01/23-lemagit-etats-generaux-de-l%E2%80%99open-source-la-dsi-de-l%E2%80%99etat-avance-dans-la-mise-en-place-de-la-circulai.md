---
site: LeMagIT
title: "Etats généraux de l’Open Source: la DSI de l’Etat avance dans la mise en place de la circulaire Ayrault"
author: Cyrille Chausson
date: 2013-01-23
href: http://www.lemagit.fr/technologie/open-source/2013/01/23/etats-generaux-de-lopen-source-la-dsi-de-letat-avance-dans-la-mise-en-place-de-la-circulaire-ayrault
tags:
- Entreprise
- Administration
- Institutions
---

> A l’occasion des premiers Etats généraux de l’Open Source en France, Jacques Marzin, le DSI de l’Etat, a livré un instantané des travaux en cours et à venir dans l’administration suite à la circulaire Ayrault.
