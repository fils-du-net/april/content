---
site: "Reseaux-Telecoms.net"
title: "Les dix projets Open Source les plus performants"
author: Jean Pierre Blettner
date: 2013-01-07
href: http://www.reseaux-telecoms.net/actualites/lire-les-dix-projets-open-source-les-plus-performants-25455.html
tags:
- Entreprise
---

> L'Open Source monte en puissance à la fois sur des fonctions traditionnelles comme la gestion de contenu sur le Web et surtout sur les nouveaux usages tels que le Big Data. Voici dix projets Open Source qui ont fait leurs preuves en 2012 et qui devraient encore croître en 2013.
