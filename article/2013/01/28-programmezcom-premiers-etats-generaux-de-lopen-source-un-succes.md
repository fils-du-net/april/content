---
site: Programmez.com
title: "Premiers états généraux de l'Open Source: un succès"
author: Frédéric Mazué
date: 2013-01-28
href: http://www.programmez.com/actualites.php?id_actu=12802
tags:
- Entreprise
- Institutions
- Associations
---

> L’ensemble de l’écosystème de l'Open Source et du logiciel libre s’est réuni pour la première fois, sous l'égide de Syntec Numérique, afin de réfléchir et échanger sur les grands défis qu'il doit relever.
