---
site: ZDNet.fr
title: "Secure boot et Linux: les critiques montent et la FSF pétitionne"
author: Thierry Noisette
date: 2013-01-31
href: http://www.zdnet.fr/actualites/secure-boot-et-linux-les-critiques-montent-et-la-fsf-petitionne-39786795.htm
tags:
- Entreprise
- April
- DRM
- Informatique-deloyale
---

> Nous rapportions il y a quelques semaines les difficultés pour installer Linux sur un ordinateur portable estampillé Windows 8. Celles-ci se confirment et poussent la Free Software Foundation à passer à l'action.
