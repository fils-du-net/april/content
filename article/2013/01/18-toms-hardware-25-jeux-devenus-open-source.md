---
site: Tom's Hardware
title: "25 jeux devenus open source"
author: David Civera
date: 2013-01-18
href: http://www.tomshardware.fr/articles/jeux-open-source,5-17.html
tags:
- Entreprise
- Logiciels privateurs
- Licenses
---

> Il y a des jeux qui ont marqué l’histoire. Les développeurs de certains de ces grands classiques ont publié tout ou une partie de leur code source pour que d’autres après eux maintiennent et modernisent le jeu, tout en encourageant la création de mods.
