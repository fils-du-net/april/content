---
site: LeJournalduNet
title: "Les technologies Open Source choisies par les géants du Web"
author: Antoine Crochet-Damais
date: 2013-01-04
href: http://www.journaldunet.com/solutions/dsi/open-source-chez-les-geants-du-web
tags:
- Entreprise
- Internet
- Innovation
---

> Facebook, Amazon, Google, Twitter, eBay... Tous misent très largement sur des logiciels Open Source pour supporter leur site web. Le point sur les choix de chacun.
