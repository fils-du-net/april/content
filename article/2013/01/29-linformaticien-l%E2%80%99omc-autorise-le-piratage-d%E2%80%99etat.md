---
site: L'Informaticien
title: "L’OMC autorise le piratage d’État"
author: Stéphane Larcher
date: 2013-01-29
href: http://www.linformaticien.com/actualites/id/27888/l-omc-autorise-le-piratage-d-etat.aspx
tags:
- Internet
- Institutions
- Droit d'auteur
- International
---

> L’Organisation Mondiale du Commerce vient de recréer la flibuste autorisant un État à s’affranchir des règles liées au copyright. En cause, une décision arbitraire des États-Unis.
