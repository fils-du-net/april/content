---
site: LeMagIT
title: "Alan Clark, président de l’OpenStack Foundation, croit en la percée de l’Open Cloud"
author: Cyrille Chausson
date: 2013-01-23
href: http://www.lemagit.fr/technologie/datacenter-technologie/cloud-grid-computing/2013/01/23/alan-clark-president-de-lopenstack-foundation-croit-en-la-percee-de-lopen-cloud
tags:
- Entreprise
- Internet
- Associations
- Innovation
- Standards
- Informatique en nuage
---

> Le président de l’OpenStack Foundation Alan Clark, dans entretien avec nos partenaires de Computerweekly, revient sur sa vision de l’Open Cloud et sur son parcours de pro-Open Source.
