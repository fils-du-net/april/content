---
site: Télérama.fr
title: "Aaron Swartz, un hacker qui avait soif de connaissance"
author: Nicolas Delesalle et Thomas Bécard
date: 2013-01-15
href: http://www.telerama.fr/medias/aaron-swartz-un-hacker-qui-avait-soif-de-connaissance,92075.php
tags:
- Internet
- Partage du savoir
- Institutions
- Sciences
- Contenus libres
- International
---

> Le suicide d'Aaron Swartz ne laisse pas les internautes indifférents. Car ce hacker de 26 ans avait fait de la libération de l'information un combat personnel.
