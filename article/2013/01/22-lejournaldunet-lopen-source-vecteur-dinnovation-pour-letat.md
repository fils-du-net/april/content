---
site: LeJournalduNet
title: "L'Open Source, vecteur d'innovation pour l'Etat"
date: 2013-01-22
href: http://www.journaldunet.com/solutions/dsi/1ers-etats-generaux-de-l-open-source-0113.shtml
tags:
- Entreprise
- Économie
- Institutions
---

> Après la Circulaire Ayrault, les premiers Etats-Généraux de l'Open Source ont confirmé la place incontournable de l'Open Source dans l'écosystème du numérique en France.
