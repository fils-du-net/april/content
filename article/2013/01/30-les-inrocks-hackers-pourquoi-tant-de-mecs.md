---
site: les inrocks
title: "Hackers: pourquoi tant de mecs?"
author: les Inrocks
date: 2013-01-30
href: http://www.lesinrocks.com/2013/01/30/actualite/hackers-pourquoi-tant-de-mecs-11347682
tags:
- Associations
---

> En 2013, la communauté des hackers, ces bidouilleurs créatifs, reste masculine à 90%, et les accusations de machisme ressortent régulièrement. Et sus aux clichés: les communautés arabes semblent bien plus mixtes.
