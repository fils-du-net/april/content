---
site: BFMtv
title: "Aaron Swartz, martyr de la libre circulation des connaissances?"
author: Olivier Laffargue
date: 2013-01-14
href: http://www.bfmtv.com/high-tech/aaron-swarz-martyr-libre-circulation-connaissances-424074.html
tags:
- Internet
- Partage du savoir
- Institutions
- Sciences
- Contenus libres
---

> Le suicide du jeune génie de l'Internet a provoqué de nombreuses réactions dans la presse spécialisée. Pour certains, il a payé de sa vie son combat contre les barrières créées par les éditeurs commerciaux.
