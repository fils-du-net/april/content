---
site: toolinux
title: "Logiciels libres et économie locale, le déni de bon sens"
author: Philippe Scoffoni
date: 2013-01-04
href: http://www.toolinux.com/Logiciels-libres-et-economie
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Licenses
- Marchés publics
---

> La promotion des logiciels libres passe souvent par une approche technique ou philosophique. L’approche économique présente l’avantage d’aller au-delà de l’apparente gratuité de ces derniers. L’impact de l’usage des logiciels libres notamment sur l’économie locale est un point qu’oublient ou ignorent bien souvent nos élus. Ils se trouvent de fait assis sur un “gisement” en matière de “redressement productif”.
