---
site: PC INpact
title: "Sur le numérique, le changement selon les Jeunes Socialistes"
author: Marc Rees
date: 2013-01-09
href: http://www.pcinpact.com/news/76613-sur-numerique-changement-selon-jeunes-socialistes.htm
tags:
- Internet
- Administration
- Interopérabilité
- HADOPI
- Vente liée
- Associations
- Brevets logiciels
- Droit d'auteur
- Neutralité du Net
- Open Data
---

> En décembre dernier, les Jeunes Socialistes ont publié leur «Dossier du Changement», intitulé «Démocratie, liberté, égalité: le socialisme pour le numérique» (document PDF). Le texte a été adopté en Conseil National le 11 novembre dernier puis remis à tous les ministres du gouvernement. «Les 89 propositions de ce texte, élaborées, discutées et amendées partout en France par les militants, se veulent une contribution à la réflexion et aux législations futures sur le numérique» expliquent les auteurs.
