---
site: "Le cercle - Les Echos"
title: "Comment une filière s'est structurée? Le logiciel libre et open source"
author: Patrice Bertrand
date: 2013-01-14
href: http://lecercle.lesechos.fr/entreprises-marches/high-tech-medias/informatiques/221162932/comment-filiere-sest-structuree-logicie
tags:
- Entreprise
- Économie
- April
- Institutions
- Associations
- Promotion
---

> Les entreprises ont toujours une double relation avec leurs confrères et concurrents: une relation de compétition, et une relation de coopération dans la défense d’intérêts communs, ou l’élaboration de partenariats. Elles doivent s'organiser, se doter d'une gouvernance, donner des interlocuteurs aux pouvoirs publics. Voyons comment la filière du logiciel libre s'est structurée.
