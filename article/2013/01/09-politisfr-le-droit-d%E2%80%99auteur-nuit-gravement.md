---
site: Politis.fr
title: "Le droit d’auteur nuit gravement"
author: Christine Tréguier
date: 2013-01-09
href: http://www.politis.fr/Le-droit-d-auteur-nuit-gravement,20549.html
tags:
- Internet
- Économie
- HADOPI
- Droit d'auteur
---

> 2013 sera-t-elle l’année du changement en matière de droits d’auteur, de la fin d’Hadopi et d’une politique enfin adaptée à la société numérique ? À entendre les bruits de coulisses en provenance de la mission Lescure, censée remettre ces questions et celles de l’exception culturelle à plat, rien n’est moins sûr. En début de semaine dernière, la SCPP (syndicat des majors de la musique) s’efforçait d’amener les preuves chiffrées de l’efficacité de la riposte graduée et demandait sanctions pécuniaires et filtrage.
