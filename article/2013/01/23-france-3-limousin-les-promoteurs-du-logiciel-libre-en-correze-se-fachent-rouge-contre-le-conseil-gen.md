---
site: France 3 Limousin
title: "Les promoteurs du logiciel libre en Corrèze se fâchent rouge contre le Conseil Général"
author: Christian Bélingard
date: 2013-01-23
href: http://limousin.france3.fr/2013/01/23/les-promoteurs-du-logiciel-libre-en-correze-se-fachent-rouge-contre-le-conseil-general-186593.html
tags:
- Administration
- Institutions
- Associations
- Éducation
- Promotion
---

> Ils reprochent à l'assemblée départementale de distribuer aux collégiens corréziens des tablettes Apple au lieu de se tourner vers des systèmes d'exploitations libres
