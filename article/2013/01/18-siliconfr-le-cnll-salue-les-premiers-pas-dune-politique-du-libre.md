---
site: Silicon.fr
title: "Le CNLL salue les premiers pas d'une politique du libre"
author: Ariane Beky
date: 2013-01-18
href: http://www.silicon.fr/cnll-politique-logiciel-libre-fleur-pellerin-82789.html
tags:
- April
- Institutions
- Associations
- Brevets logiciels
---

> Le Conseil national du logiciel libre se réjouit des premiers éléments d’une politique du logiciel libre présentés par Fleur Pellerin à la suite d’une question écrite du député Jean-Jacques Candelier.
