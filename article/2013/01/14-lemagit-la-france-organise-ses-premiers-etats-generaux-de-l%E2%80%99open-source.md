---
site: LeMagIT
title: "La France organise ses premiers états généraux de l’Open Source"
author: Cyrille Chausson
date: 2013-01-14
href: http://www.lemagit.fr/technologie/open-source/2013/01/14/la-france-organise-ses-premiers-etats-generaux-de-lopen-source
tags:
- Le Logiciel Libre
- Entreprise
- Administration
- Économie
- Institutions
---

> Le 21 janvier prochain seront lancés les premiers Etats généraux de l’Open Source (EGOS) en France.
