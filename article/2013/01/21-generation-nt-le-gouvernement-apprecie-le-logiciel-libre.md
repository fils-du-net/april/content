---
site: "Génération-NT"
title: "Le gouvernement apprécie le logiciel libre"
author: Jérôme G.
date: 2013-01-21
href: http://www.generation-nt.com/logiciel-libre-gouvernement-pellerin-cnll-france-actualite-1683482.html
tags:
- Entreprise
- Administration
- Économie
---

> Pour la ministre déléguée à l'Économie numérique, le logiciel libre est un modèle qui revêt un intérêt qui n'est plus à démontrer. En septembre dernier, le premier Ministre avait adressé une circulaire sur les orientations des logiciels libres dans l'administration.
