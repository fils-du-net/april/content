---
site: Libération.fr
title: "Aaron Swartz, le feu RSS"
author: Fabrice Rousselot
date: 2013-01-17
href: http://www.liberation.fr/medias/2013/01/17/aaron-swartz-le-feu-rss_874805
tags:
- Internet
- Partage du savoir
- Institutions
- Licenses
- Sciences
- International
---

> Le débat sur l’accès libre à l’information est relancé après le suicide, à 26 ans, de cet informaticien américain, inventeur des fameux flux en temps réel. Il devait être jugé en avril pour piratage.
