---
site: Numerama
title: "Fleur Pellerin livre une déclaration d'amour au logiciel libre"
author: Guillaume Champeau
date: 2013-01-16
href: http://www.numerama.com/magazine/24796-fleur-pellerin-livre-une-declaration-d-amour-au-logiciel-libre.html
tags:
- Administration
- April
- Institutions
- Brevets logiciels
- Europe
---

> Elle fera modèle chez les partisans du logiciel libre. Le ministère de l'économie numérique a publié au Journal Officiel de l'Assemblée Nationale une réponse à un député, qui fait office de parfait plaidoyer pour le logiciel libre et les nombreux avantages qu'il procure.
