---
site: PC INpact
title: "Tenter d'installer Linux peut verrouiller définitivement des portables Samsung"
author: Damien Labourot
date: 2013-01-31
href: http://www.pcinpact.com/news/77159-tenter-dinstaller-linux-peut-verrouiller-definitivement-portables-samsung.htm
tags:
- Entreprise
- Informatique-deloyale
---

> De nombreux utilisateurs rapportent qu'une tentative d'installation d'Ubuntu via une méthode classique ou l'utilisation d'un live CD sur certains PC portables Samsung exploitant l'UEFI aurait la fâcheuse conséquence de les verrouiller... définitivement.
