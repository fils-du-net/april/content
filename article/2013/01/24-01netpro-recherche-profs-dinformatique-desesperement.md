---
site: 01netPro.
title: "Recherche profs d'informatique désespérement"
author: Serge Abiteboul, Gilles Dowek et Colin de la Higuera
date: 2013-01-24
href: http://pro.01net.com/editorial/585095/recherche-profs-dinformatique-desesperement
tags:
- Internet
- Institutions
- Éducation
---

> Pour comprendre la société numérique actuelle, l'enseignement de l'informatique est devenu une nécessité. Et celui-ci ne sera efficace que s'il est assuré par des professeurs d'informatique, dédiés à cette discipline.
