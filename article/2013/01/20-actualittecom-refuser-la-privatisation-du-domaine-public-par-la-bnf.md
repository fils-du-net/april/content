---
site: ActuaLitté.com
title: "Refuser la privatisation du domaine public par la BnF"
author: Nicolas Gary
date: 2013-01-20
href: http://www.actualitte.com/usages/refuser-la-privatisation-du-domaine-public-par-la-bnf-39680.htm
tags:
- Entreprise
- Internet
- Partage du savoir
- Institutions
- Associations
- Licenses
---

> Depuis le début de la semaine, et l'annonce d'un double accord signé par la BnF avec deux acteurs de la numérisation, ActuaLitté a fait état des risques et dangers liés à ce projet. Il s'agit bien de privatiser des milliers d'oeuvres du domaine public, qui seront commercialisées par la suite, à travers de BnF Partenariats, sa filiale marchande. ActuaLitté publie ce jour une tribune signée de plusieurs organisations, révoltées par le projet, l'assentiment reçu du ministère de la Culture et le silence des autorités. Nous publions leur communiqué dans son intégralité.
