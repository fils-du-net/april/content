---
site: Tout Montpellier
title: "A Montpellier, l'open data au service des personnes handicapées"
author: Philippe Bourguet
date: 2013-01-29
href: http://www.toutmontpellier.fr/a-montpellier-l-open-data-au-service-des-personnes-handicapees--36268.html
tags:
- Internet
- Partage du savoir
- Accessibilité
- Associations
- Open Data
---

> Une association en faveur de l'utilisation des logiciels libres organise des opérations de saisies de données a travers la ville pour déterminer l'accessibilité des magasins aux personnes à mobilité réduite.
