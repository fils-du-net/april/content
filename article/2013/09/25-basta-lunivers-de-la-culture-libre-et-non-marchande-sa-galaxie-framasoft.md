---
site: basta!
title: "L'univers de la culture libre et non-marchande a sa galaxie: Framasoft"
author: Agnès Rousseaux
date: 2013-09-25
href: http://www.bastamag.net/article3277.html
tags:
- Entreprise
- Internet
- Économie
- Associations
- Promotion
---

> Des millions de logiciels, des services en ligne, des livres, de la musique... Diffusés non par une multinationale, mais par une association, portail francophone de la « culture du libre ». Fer de lance de la promotion des logiciels libres depuis une décennie, Framasoft rassemble tous ceux qui cherchent à inventer d’autres manières de faire tourner un ordinateur, d’échanger des contenus, de partager des œuvres, bien loin des logiques marchandes de Microsoft ou Google. Plongée dans l’univers du libre, à la découverte d’un réseau d’éducation populaire protéiforme.
