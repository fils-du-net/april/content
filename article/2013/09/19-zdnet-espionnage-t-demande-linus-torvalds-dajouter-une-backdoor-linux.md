---
site: ZDNet
title: "Espionnage: a-t-on demandé à Linus Torvalds d'ajouter une backdoor à Linux?"
author: Thierry Noisette
date: 2013-09-19
href: http://www.zdnet.fr/actualites/espionnage-a-t-on-demande-a-linus-torvalds-d-ajouter-une-backdoor-a-linux-39794153.htm
tags:
- Institutions
- Informatique-deloyale
---

> L'initiateur du système d'exploitation a répondu d'une mimique lorsqu'on lui a demandé hier si une agence de l'Etat américain lui a déjà demandé de placer une porte dérobée dans le logiciel libre.
