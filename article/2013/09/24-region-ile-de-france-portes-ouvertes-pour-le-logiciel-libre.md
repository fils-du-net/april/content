---
site: "Région Île-de-France"
title: "Portes ouvertes pour le logiciel libre"
author: Xavier Frison
date: 2013-09-24
href: http://www.iledefrance.fr/fil-actus-region/portes-ouvertes-logiciel-libre
tags:
- Administration
- April
- Associations
- Promotion
---

> Du 3 au 5 octobre, l’Open World Forum réunira les principaux acteurs européens du logiciel libre à Montrouge. L’occasion de faire le point sur la place désormais prépondérante du «libre» dans le monde du logiciel et du numérique.
