---
site: Direction Informatique
title: "Le 21 septembre, journée de l’informatique libre au Québec"
author: Benjamin Jébrak
date: 2013-09-19
href: http://www.directioninformatique.com/le-21-septembre-journee-de-linformatique-libre-au-quebec/21749
tags:
- Associations
- Promotion
- International
---

> Pour une neuvième année, les logiciels libres seront célébrés internationalement. Cette année ne fait pas exception et des activités seront organisées le 21 septembre prochain à travers le Québec.
