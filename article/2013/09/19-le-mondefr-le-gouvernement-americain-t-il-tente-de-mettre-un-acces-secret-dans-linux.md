---
site: Le Monde.fr
title: "Le gouvernement américain a-t-il tenté de mettre un accès secret dans Linux?"
date: 2013-09-19
href: http://www.lemonde.fr/technologies/article/2013/09/19/le-gouvernement-americain-a-t-il-tente-de-mettre-un-acces-secret-dans-linux_3481314_651865.html
tags:
- Internet
- Institutions
- Informatique-deloyale
---

> La question a été posée à Linus Torvalds, le créateur du système d'exploitation. Sa réponse a de quoi laisser perplexe.
