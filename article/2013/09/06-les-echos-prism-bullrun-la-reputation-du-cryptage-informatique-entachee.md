---
site: Les Echos
title: "Prism, Bullrun...: la réputation du cryptage informatique entachée"
author: Geoffrey Marain-Joris
date: 2013-09-06
href: http://www.lesechos.fr/entreprises-secteurs/tech-medias/actu/0202991768242-prism-bullrun-la-reputation-du-cryptage-informatique-entachee-602540.php
tags:
- Internet
- Partage du savoir
- Institutions
- Informatique-deloyale
---

> Tout n’est pas fiable dans le chiffrement. Edward Snowden identifie tout de même quelques systèmes de confiance, qu’il utilise.
