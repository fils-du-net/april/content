---
site: Métro
title: "Les candidats invités à signer le Pacte du logiciel libre"
author: Daphnée Hacker-B.
date: 2013-09-24
href: http://journalmetro.com/dossiers/les-elections-municipales-2013/377194/les-candidats-invites-a-signer-le-pacte-du-logiciel-libre
tags:
- Économie
- Institutions
- Associations
- Marchés publics
- International
---

> La question des logiciels informatiques dits ouverts s’est immiscée dans la campagne électorale, mardi, lorsqu’un groupe de pression a interpellé l’ensemble des candidats pour qu’ils signent le Pacte du logiciel libre.
