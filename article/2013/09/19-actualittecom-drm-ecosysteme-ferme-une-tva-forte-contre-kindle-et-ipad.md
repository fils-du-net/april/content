---
site: ActuaLitté.com
title: "DRM, écosystème fermé: une TVA forte contre Kindle et iPad"
author: Nicolas Gary
date: 2013-09-19
href: http://www.actualitte.com/legislation/drm-ecosysteme-ferme-une-tva-forte-contre-kindle-et-ipad-45159.htm
tags:
- Entreprise
- Économie
- Interopérabilité
- Institutions
- DRM
- Europe
---

> Et favoriser les formats ouverts ainsi que l'interopérabilité
