---
site: PC INpact
title: "Vente liée PC et OS: Benoît Hamon enterre les promesses d'Hollande"
author: Marc Rees
date: 2013-09-12
href: http://www.pcinpact.com/news/82322-vente-liee-pc-et-os-benoit-hamon-enterre-promesses-dhollande.htm
tags:
- April
- Institutions
- Vente liée
- Associations
---

> C’est la douche froide pour les différentes associations qui soutenaient ces textes: poussé par un avis défavorable de Benoit Hamon, le Sénat a finalement rejeté les amendements qui tentaient de vidanger la problématique de la vente liée ou du moins de la transparence des prix du matériel et des logiciels. Par la même occasion, le ministre a poussé sans ménagement aux oubliettes, les promesses du candidat Hollande.
