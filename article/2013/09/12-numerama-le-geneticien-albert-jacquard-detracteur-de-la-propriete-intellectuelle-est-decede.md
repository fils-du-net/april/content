---
site: Numerama
title: "Le généticien Albert Jacquard, détracteur de la propriété intellectuelle, est décédé"
author: Julien L.
date: 2013-09-12
href: http://www.numerama.com/magazine/26973-le-geneticien-albert-jacquard-detracteur-de-la-propriete-intellectuelle-est-decede.html
tags:
- Partage du savoir
- Sciences
---

> Albert Jacquard s'est éteint à l'âge de 87 ans. Généticien de renom, le scientifique était également connu pour ses multiples engagements.
