---
site: MacGeneration
title: "Brevet unitaire: un risque de patent trolls en Europe"
author: Pierrick Aubert
date: 2013-09-30
href: http://www.macg.co/news/voir/261366/brevet-unitaire-un-risque-de-patent-trolls-en-europe
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- Innovation
- Promotion
---

> Les géants du marché IT ne voient pas d'un bon œil le futur brevet unitaire européen. Destiné à protéger de manière uniforme les inventions dans toute l'Europe, celui-ci pourrait contribuer à l'émergence de patent trolls, s'alarment une quinzaine d'organisations.
