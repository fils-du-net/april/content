---
site: Framablog
title: "Le chiffrement, maintenant"
author: Goofy
date: 2013-09-14
href: http://www.framablog.org/index.php/post/chiffrement-maintenant-3
tags:
- Internet
- Logiciels privateurs
- Informatique-deloyale
- Licenses
- Standards
- ACTA
---

> Il existe un autre type de logiciel qui est plus fiable à cet égard. Les logiciels libres et open source ne sont pas forcément ergonomiques? et ne sont pas nécessairement sans risques? Cependant quand ils sont développés de façon ouverte, avec un logiciel de suivi de bogue ouvert, des listes de diffusion ouvertes, une architecture ouverte et un code open source, il est plus difficile pour ces projets d’avoir une politique de trahison de leurs utilisateurs comme celle de Microsoft.
