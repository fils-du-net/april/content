---
site: l'Humanité.fr
title: "La «démocratie liquide» des hackers"
author: Mehdi Fikri
date: 2013-09-03
href: http://www.humanite.fr/societe/la-democratie-liquide-des-hackers-548084
tags:
- Institutions
- Associations
- ACTA
---

> Amaelle Guiton, auteur du livre «Hackers, au cœur de la résistance numérique», dessine les contours des mouvements hackers. Entre les institutions et les marges, le bricolage et la politique.
