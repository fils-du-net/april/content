---
site: ZDNet
title: "Vente liée: le ministre déforme la promesse de Hollande pour rejeter les amendements"
author: Thierry Noisette
date: 2013-09-11
href: http://www.zdnet.fr/actualites/vente-liee-le-ministre-deforme-la-promesse-de-hollande-pour-rejeter-les-amendements-39793935.htm
tags:
- April
- Institutions
- Vente liée
- Associations
---

> Quand Benoît Hamon rejette, encore, des amendements anti-vente liée de logiciels, il nous cite, ce qui est gentil, mais en mentant piteusement sur la promesse de François Hollande en 2012...
