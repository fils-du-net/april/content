---
site: "France 3 Languedoc-Roussillon"
title: "IBM va investir 750 millions d'euros à Montpellier pour son logiciel Linux"
author: Fabrice Dubault
date: 2013-09-18
href: http://languedoc-roussillon.france3.fr/2013/09/18/ibm-va-investir-750-millions-d-euros-montpellier-pour-son-logiciel-linux-320771.html
tags:
- Entreprise
- Économie
- Innovation
- Promotion
---

> Le groupe informatique américain, IBM, va investir un milliard de dollars pour soutenir le logiciel libre Linux, un plan dont va profiter, entre autres, Montpellier. Il y aura notamment un nouveau centre client sur le site du Millénaire. IBM emploie 950 salariés, environ, à Montpellier.
