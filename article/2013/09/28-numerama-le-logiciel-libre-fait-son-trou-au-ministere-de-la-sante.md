---
site: Numerama
title: "Le logiciel libre fait son trou au ministère de la santé"
author: Julien L.
date: 2013-09-28
href: http://www.numerama.com/magazine/27106-le-logiciel-libre-fait-son-trou-au-ministere-de-la-sante.html
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
---

> Parmi les différents ministères qui ont répondu jusqu'à présent aux sollicitations de la députée EELV Isabelle Attard, celui de la santé est sans doute l'un des plus en pointe au niveau des logiciels libres. Selon les services de Marisol Touraine, une montée en puissance des programmes non-propriétaires est en cours, à différents niveaux.
