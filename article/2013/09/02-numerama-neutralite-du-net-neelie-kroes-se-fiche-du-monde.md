---
site: Numerama
title: "Neutralité du net: \"Neelie Kroes se fiche du monde\""
author: Guillaume Champeau
date: 2013-09-02
href: http://www.numerama.com/magazine/26864-neutralite-du-net-neelie-kroes-se-fiche-du-monde.html
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Neutralité du Net
---

> Vendredi, la Quadrature du Net a révélé la dernière version du projet de révision du Paquet Télécom, que la Commission Européenne doit soumettre au Parlement Européen. "C'est un texte technique absolument fondamental pour l'avenir d'Internet, qui mérite que l'on s'y intéresse", explique Félix Tréguer, co-fondateur de la Quadrature du Net, dans une interview accordée à Numerama. Il estime que la vice-présidente de la Commission, Neelie Kroes, "se fiche du monde" en prétendant défendre la neutralité du net tout en organisant la marchandisation d'une priorisation de certains flux.
