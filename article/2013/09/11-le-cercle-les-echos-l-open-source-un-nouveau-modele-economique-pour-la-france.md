---
site: Le Cercle Les Echos
title: "L’Open Source, un nouveau modèle économique pour la France?"
author: Philippe Montarges
date: 2013-09-11
href: http://lecercle.lesechos.fr/economie-societe/politique-eco-conjoncture/politique-economique/221179553/open-source-nouveau-modele
tags:
- Économie
- Institutions
- Innovation
---

> L’environnement économique de l’Open Source est porteur, les pouvoirs publics le favorisent pour sa capacité à se libérer des grands éditeurs de logiciels. Le dynamisme de l'intelligence collective peut expliquer le succès de ce nouveau modèle économique.
