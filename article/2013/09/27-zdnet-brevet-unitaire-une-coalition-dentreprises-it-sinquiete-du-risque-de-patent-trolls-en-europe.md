---
site: ZDNet
title: "Brevet unitaire: une coalition d'entreprises IT s'inquiète du risque de patent trolls en Europe"
author: Thierry Noisette
date: 2013-09-27
href: http://www.zdnet.fr/actualites/brevet-unitaire-une-coalition-d-entreprises-it-s-inquiete-du-risque-de-patent-trolls-en-europe-39794399.htmi
tags:
- Entreprise
- April
- Institutions
- Brevets logiciels
- Europe
---

> Apple, Microsoft, Google, Yahoo, Intel, Samsung... 14 entreprises et deux organisations high-tech s’alarment de la possibilité que le futur brevet unitaire européen permette des procès abusifs bloquant la vente de produits.
