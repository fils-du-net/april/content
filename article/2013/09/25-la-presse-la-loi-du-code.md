---
site: La Presse
title: "La loi du code"
author: Pierre Asselin
date: 2013-09-25
href: http://www.lapresse.ca/le-soleil/opinions/editoriaux/201309/24/01-4692812-la-loi-du-code.php
tags:
- Internet
- Administration
- Économie
- Institutions
- International
---

> Entre les débats sur le Conseil du statut de la femme et sur le déficit zéro, les députés de l'Assemblée nationale ont pris quelques minutes, mardi, pour adopter une motion - unanime - en faveur de l'usage par l'administration publique du logiciel libre.
