---
site: PC INpact
title: "30 ans de GNU: interview de Richard Stallman"
author: Marc Rees
date: 2013-09-23
href: http://www.pcinpact.com/news/82508-30-ans-gnu-interview-richard-stallman.htm
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Économie
- April
- Institutions
- DRM
- Informatique-deloyale
---

> En visite à Paris, Richard Stallman nous a accordé une longue interview à l’occasion des 30 ans de Gnu.org qui seront fêtés ce 27 septembre. Le fondateur du projet GNU, qui a corédigé la licence publique générale GNU GPL avec Eben Moglen, revient sur l'importance du soutien au mouvement du logiciel libre.
