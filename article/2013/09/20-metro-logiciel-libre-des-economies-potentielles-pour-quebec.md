---
site: Métro
title: "Logiciel libre: des économies potentielles pour Québec"
date: 2013-09-20
href: http://journalmetro.com/actualites/national/375523/logiciel-libre-des-economies-potentielles-pour-quebec
tags:
- Logiciels privateurs
- Économie
- Institutions
- International
---

> Le gouvernement du Québec peut réduire sa dépendance à des grandes compagnies informatiques et économiser de l’argent s’il adopte les logiciels libres, révèle une étude de l’Institut de recherche et d’information socio-économiques (IRIS) publiée vendredi.
