---
site: clubic.com
title: "Loi de consommation: obsolescence et vente liée (encore) au programme"
author: Ludwig Gallet
date: 2013-09-06
href: http://pro.clubic.com/legislation-loi-internet/actualite-582404-loi-conso-senat.html
tags:
- Entreprise
- Économie
- Institutions
- Vente liée
- Associations
---

> Le projet de loi relatif à la consommation va passer en première lecture au Sénat. Écartés à l'Assemblée, l'obsolescence programmée, la vente liée et de nombreux sujets liés au numérique seront de nouveau discutés. Et votés?
