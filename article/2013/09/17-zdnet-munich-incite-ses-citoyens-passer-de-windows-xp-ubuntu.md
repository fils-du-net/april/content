---
site: ZDNet
title: "Munich incite ses citoyens à passer de Windows XP à Ubuntu"
author: Thierry Noisette
date: 2013-09-17
href: http://www.zdnet.fr/actualites/munich-incite-ses-citoyens-a-passer-de-windows-xp-a-ubuntu-39794078.htm
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
---

> La capitale de la Bavière propose cette migration d'ici l'abandon en avril 2014 des mises à jour de Windows XP. Une marque de plus du soutien de Munich aux logiciels libres, après l'adoption pour tous les PC municipaux de Linux et d'OpenOffice.
