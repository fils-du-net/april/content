---
site: LaDepeche.fr
title: "Bout-du-Pont-de-Larn. Les logiciels libres sont accessibles"
date: 2013-09-19
href: http://www.ladepeche.fr/article/2013/09/19/1712176-bout-du-pont-de-larn-les-logiciels-libres-sont-accessibles.html
tags:
- Internet
- Sensibilisation
- Associations
---

> «Le supercalculateur, le plus puissant du monde, s’appelle Tianhe-2 (ou Milky Way) et est la propriété de la défense chinoise. Alors à votre avis quel système d’exploitation est capable de faire fonctionner un tel monstre: Linux Kylin (Ubuntu Chinois)» indique Laurent Virgos, un des membres de Lolipotou
