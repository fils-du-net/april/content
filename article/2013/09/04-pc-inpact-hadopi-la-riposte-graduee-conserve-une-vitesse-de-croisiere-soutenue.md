---
site: PC INpact
title: "Hadopi: la riposte graduée conserve une vitesse de croisière soutenue"
author: Xavier Berne
date: 2013-09-04
href: http://www.pcinpact.com/news/82134-hadopi-riposte-graduee-conserve-vitesse-croisiere-soutenue.htm
tags:
- Internet
- HADOPI
---

> Les chiffres de la riposte graduée pour le mois d’août 2013 viennent d’être diffusés par la Hadopi (PDF). Si l’on observe un léger ralentissement, manifestement lié à la période estivale, la vitesse de croisière du dispositif reste malgré tout soutenue.
