---
site: JDN
title: "Linus Torvalds a-t-il subi des pressions de la NSA?"
author: Dominique Filippone
date: 2013-09-20
href: http://www.journaldunet.com/solutions/dsi/linus-torvalds-backdoor-et-nsa-a-linuxcon-0913.shtml
tags:
- Institutions
- Informatique-deloyale
---

> Le créateur du système d'exploitation open source Linux a été mis dans l'embarras lors de la dernière LinuxCon. En s'exprimant une nouvelle fois sur le sujet, il risque la prison.
