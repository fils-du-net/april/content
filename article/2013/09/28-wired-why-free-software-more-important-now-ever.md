---
site: Wired
title: "Why Free Software Is More Important Now Than Ever Before"
author: Richard Stallman
date: 2013-09-28
href: http://www.wired.com/opinion/2013/09/why-free-software-is-more-important-now-than-ever-before
tags:
- Logiciels privateurs
- Informatique-deloyale
- Licenses
- Philosophie GNU
- Promotion
- Informatique en nuage
- English
---

> (Pour le Logiciel Libre est plus important maintenant que jamais) GNU just turned 30 years old. But much has changed since the beginning of the free software movement; now there's SaaS and more. Malware is common in proprietary software products since users don't have control over them. Why does this control matter? Because freedom means having control over your own life.
