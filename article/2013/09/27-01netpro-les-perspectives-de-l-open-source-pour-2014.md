---
site: 01netPro.
title: "Les perspectives de l'open source pour 2014"
author: Stéfane Fermigier
date: 2013-09-27
href: http://pro.01net.com/editorial/604146/les-perspectives-de-l-open-source-pour-2014
tags:
- Entreprise
- Économie
- Innovation
- Informatique en nuage
---

> Selon une étude récente réalisée auprès de 322 entreprises françaises, près de 90% d’entre elles ont mis en place des outils open source ces deux dernières années. Pourtant presque la moitié d’entre elles n’ont pas de véritable stratégie open source en 2013. Par ailleurs, 71% perçoivent toujours le coût comme principal avantage du logiciel libre.
