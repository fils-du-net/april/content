---
site: l'Humanité.fr
title: "A l'heure de Prism, \"les logiciels libres sont les plus sûrs\""
author: Pierric Marissal
date: 2013-09-27
href: http://www.humanite.fr/social-eco/lheure-de-prism-les-logiciels-libres-sont-les-plus-549882
tags:
- Logiciels privateurs
- Administration
- April
- Institutions
- Sensibilisation
- Droit d'auteur
- Informatique-deloyale
- Philosophie GNU
---

> Ce vendredi est célébré le trentième anniversaire de la création de GNU, le système d'exploitation libre de Richard Stallman, évènement fondateur du logiciel libre. L’occasion de revenir sur la philosophie du logiciel libre et son importance à l’heure de Prism et de la révolution mobile avec le président de l’April, association de promotion du libre, Lionel Allorge. Entretien.
