---
site: l'Humanité.fr
title: "Richard Stallman, le père du logiciel libre, au secours des utilisateurs du numérique"
author: Florent Lacaille-Albiges
date: 2013-09-24
href: http://www.humanite.fr/societe/richard-stallman-le-pere-du-logiciel-libre-en-croi-549590
tags:
- Entreprise
- Internet
- April
- Institutions
- Informatique-deloyale
- Promotion
---

> Le fondateur des logiciels libres Richard Stallman était de passage samedi dernier à Saint-Denis à l'invitation de l'April, association pour la promotion et la défense des logiciels libres. Dans un amphi de l'université Paris 8, il a appelé à la reconnaissance de droits fondamentaux de l'utilisateur d'Internet.
