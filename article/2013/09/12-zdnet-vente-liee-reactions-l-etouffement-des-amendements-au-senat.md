---
site: ZDNet
title: "Vente liée: réactions à l’étouffement des amendements au Sénat"
author: thierry-noisette
date: 2013-09-12
href: http://www.zdnet.fr/actualites/vente-liee-reactions-a-l-etouffement-des-amendements-au-senat-39793956.htm
tags:
- April
- Institutions
- Vente liée
- Associations
- Désinformation
---

> Le rejet des amendements sur l’affichage du prix des logiciels intégrés, hier soir au Sénat, suscite de vives critiques des associations du logiciel libre, qui pointent les contre-vérités du ministre Benoît Hamon et l’abandon d’une promesse du candidat Hollande.
