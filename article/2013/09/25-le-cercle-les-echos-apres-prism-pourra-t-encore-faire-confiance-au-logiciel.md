---
site: Le Cercle Les Echos
title: "Après PRISM, pourra-t-on encore faire confiance au logiciel?"
author: Patrice Bertrand
date: 2013-09-25
href: http://lecercle.lesechos.fr/entreprises-marches/high-tech-medias/informatiques/221180587/apres-prism-pourra-t-on-encore-faire-co
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Informatique-deloyale
- International
---

> Les logiciels contrôlent des systèmes critiques pour notre sécurité, nos entreprises, pour nos vies même, et cela chaque année davantage. On savait qu’ils avaient des bugs, on sait maintenant qu’ils ont aussi parfois des failles introduites par leurs programmeurs, sous la contrainte. La confiance est brisée. Contrôler le logiciel qui nous contrôle, c’est l’une des finalités du logiciel libre.
