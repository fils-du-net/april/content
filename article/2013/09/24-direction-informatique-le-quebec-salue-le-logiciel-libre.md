---
site: Direction Informatique
title: "Le Québec salue le logiciel libre"
author: Jean-François Ferland
date: 2013-09-24
href: http://www.directioninformatique.com/le-quebec-salue-le-logiciel-libre/21900
tags:
- Administration
- Institutions
- Associations
- International
---

> L’Assemblée nationale, à l’unanimité, a adopté une motion qui soulignait la récente Journée internationale du logiciel libre et les initiatives de diffusion du logiciel libre au sein de l’administration publique.
