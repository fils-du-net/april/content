---
site: 01net.
title: "Richard Stallman: les logiciels libres sont plus importants que jamais"
author: Pierre Fontaine
date: 2013-09-30
href: http://www.01net.com/editorial/604330/richard-stallman-les-logiciels-libres-sont-plus-importants-que-jamais
tags:
- Sensibilisation
- DRM
- Licenses
- Philosophie GNU
- Informatique en nuage
---

> Après trente ans de combat, Richard Stallman continue de porter haut ses idées et idéaux, appelant à plus de liberté et de prise de conscience. Une tribune publiée dans Wired pointe du doigt les dangers et espoirs nouveaux et anciens.
