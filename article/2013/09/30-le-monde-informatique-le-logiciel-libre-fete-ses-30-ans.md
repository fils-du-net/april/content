---
site: Le Monde Informatique
title: "Le logiciel libre fête ses 30 ans"
author: Jacques Cheminat
date: 2013-09-30
href: http://www.lemondeinformatique.fr/actualites/lire-le-logiciel-libre-fete-ses-30-ans-55184.html
tags:
- Associations
- Licenses
- Philosophie GNU
---

> Il y a 30 ans Richard Stallman lançait son projet GNU et fondait par la même occasion le mouvement du logiciel libre.
