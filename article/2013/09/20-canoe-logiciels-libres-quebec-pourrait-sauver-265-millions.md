---
site: canoe
title: "Logiciels libres: Québec pourrait sauver 265 millions$"
date: 2013-09-20
href: http://fr.canoe.ca/techno/materiel/archives/2013/09/20130920-153132.html
tags:
- Logiciels privateurs
- Économie
- Institutions
- Marchés publics
- International
---

> Le gouvernement du Québec pourrait épargner des centaines de millions s’il décidait d’opter pour les logiciels libres, avance l’Institut de recherche et d’informations socio-économiques (IRIS) de Montréal.
