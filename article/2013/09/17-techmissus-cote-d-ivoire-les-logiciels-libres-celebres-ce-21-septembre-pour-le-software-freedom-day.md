---
site: TechMissus
title: "Côte d’Ivoire: Les logiciels libres célébrés ce 21 Septembre pour le Software Freedom Day"
author: Jean-Patrick Ehouman
date: 2013-09-17
href: http://www.techmissus.com/a-la-une/evenements/cote-divoire-les-logiciels-libres-celebres-ce-21-septembre-pour-le-software-freedom-day-sfd2013
tags:
- Associations
- Philosophie GNU
- Promotion
- International
---

> Les activistes de Linux et du logiciel libre de Côte d’Ivoire se préparent pour le Software Freedom Day 2013 qui est la journée mondiale du logiciel libre.La journée du logiciel libre est une manifestation mondiale annuelle instaurée en 2004 dans le but d’initier le public au logiciel libre à l’échelle mondiale, par son utilisation personnelle, dans l’éducation, l’économie ou par les gouvernements.
