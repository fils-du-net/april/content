---
site: PC INpact
title: "Comment le CSA compte réguler tous les contenus culturels numériques"
author: Marc Rees
date: 2013-09-23
href: http://www.pcinpact.com/news/82510-comment-csa-compte-reguler-tous-contenus-culturels-numeriques.htm
tags:
- Internet
- Partage du savoir
- HADOPI
- Institutions
- Associations
- Droit d'auteur
---

> Le transfert de la Hadopi au CSA se fera finalement dans un projet de loi sur la création, qu’a confirmé Aurélie Filippetti. Celle-ci décrit déjà l’avenir de la régulation en France avec le Conseil supérieur de l’audiovisuel aux manettes des contenus du Net.
