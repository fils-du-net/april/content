---
site: Libération.fr
title: "Affaire Snowden: pour une régulation mondiale Informatique et Libertés"
author: Philippe Boucher, Louis Joinet, Philippe Lemoine
date: 2013-09-19
href: http://www.liberation.fr/monde/2013/09/19/affaire-snowden-pour-une-regulation-mondiale-informatique-et-libertes_933079
tags:
- Internet
- Institutions
- Informatique-deloyale
---

> Un ensemble de personnalités interpellent le gouvernement, via une pétition, sur «un des plus grands scandales de l’histoire du numérique et des libertés».
