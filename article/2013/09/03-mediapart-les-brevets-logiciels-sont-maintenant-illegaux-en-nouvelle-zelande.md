---
site: Mediapart
title: "Les brevets logiciels sont maintenant illégaux en Nouvelle-Zélande"
author: Mohamed SANGARE
date: 2013-09-03
href: http://blogs.mediapart.fr/blog/mohamed-sangare/030913/les-brevets-logiciels-sont-maintenant-illegaux-en-nouvelle-zelande
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> En un mot, les défenseurs des logiciels libres ont gagné une grande bataille dans un petit pays, mais les conséquences de cette décision sont encourageantes et potentiellement utile à la cause.
