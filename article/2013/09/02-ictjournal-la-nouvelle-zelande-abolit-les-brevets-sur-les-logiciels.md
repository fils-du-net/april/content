---
site: ICTjournal
title: "La Nouvelle-Zélande abolit les brevets sur les logiciels"
author: Mélanie Haab
date: 2013-09-02
href: http://www.ictjournal.ch/News/2013/09/02/La-Nouvelle-Zelande-abolit-les-brevets-sur-les-logiciels.aspx
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> La Nouvelle-Zélande a voté une nouvelle loi sur les brevets, qui abolit les licences des softwares. Le gouvernement veut favoriser l’innovation au sein des entreprises, qui se heurtent souvent aux tarifs et conditions des logiciels commerciaux.
