---
site: ZDNet
title: "Vente liée: retour au Sénat d'amendements dans le projet de loi Consommation"
date: 2013-09-06
href: http://www.zdnet.fr/actualites/vente-liee-retour-au-senat-d-amendements-dans-le-projet-de-loi-consommation-39793790.htm
tags:
- Institutions
- Vente liée
---

> Nouvel épisode après un passage à l'Assemblée et un autre en commission au Sénat où le ministre Benoît Hamon a fait écarter un amendement anti-vente liée: promise en avril 2012 par le candidat Hollande, la lutte contre la vente par lot de logiciels intégrés va-t-elle avancer?
