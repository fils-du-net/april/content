---
site: Numerama
title: "L'Etat lève une partie du voile sur les dépenses logicielles"
author: Julien L.
date: 2013-09-13
href: http://www.numerama.com/magazine/26983-l-etat-leve-une-partie-du-voile-sur-les-depenses-logicielles.html
tags:
- Logiciels privateurs
- Économie
- Institutions
---

> Poussés par la députée écologiste Isabelle Attard, les ministères français ont commencé à lever le voile sur les dépenses en logiciel (propriétaire ou libre) sur la période 2008 - 2012. Si toutes les administrations n'ont pas répondu et si certains retours sont incomplets, la démarche de la parlementaire permet de lever une partie du voile sur les dépenses logicielles de l'État.
