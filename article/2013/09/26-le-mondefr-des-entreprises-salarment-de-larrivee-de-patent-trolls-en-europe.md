---
site: Le Monde.fr
title: "Des entreprises s'alarment de l'arrivée de \"patent trolls\" en Europe"
date: 2013-09-26
href: http://www.lemonde.fr/technologies/article/2013/09/26/des-entreprises-s-alarment-de-l-arrivee-de-patent-trolls-en-europe_3485720_651865.html
tags:
- Entreprise
- Économie
- Institutions
- Associations
- Brevets logiciels
- Innovation
- Europe
---

> Le futur brevet unitaire européen, qui mettrait fin au dépôt d'un brevet dans chaque pays de l'Union européenne, serait une aubaine pour les "trolls de brevets", qui vivent en attaquant d'autres entreprises pour violation de brevet.
