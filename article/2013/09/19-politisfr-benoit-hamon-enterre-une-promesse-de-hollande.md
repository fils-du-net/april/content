---
site: Politis.fr
title: "Benoît Hamon enterre une promesse de Hollande"
author: Christine Tréguier
date: 2013-09-19
href: http://www.politis.fr/Benoit-Hamon-enterre-une-promesse,23716.html
tags:
- Institutions
- Vente liée
- Associations
---

> Les associations de défense du logiciel libre sont furieuses. Le candidat Hollande avait promis de mettre fin aux ventes forcées de logiciels lors de l’achat d’un ordinateur. Mais son ministre de la Consommation, Benoît Hamon, n’est visiblement pas de cet avis et fait tout ce qu’il peut pour… ne rien faire.
