---
site: cnet France
title: "Transhumanisme: un futur entre nos mains"
author: Fabien Soyez
date: 2013-09-05
href: http://www.cnetfrance.fr/news/transhumanisme-un-futur-entre-nos-mains-39793046.htm
tags:
- Entreprise
- Innovation
---

> Quel avenir nous réservent les améliorations prônées par le transhumanisme? Deux scénarios possibles: le rêve et le cauchemar. La vérité se trouve peut être entre les deux. Avec, bien sûr, la possibilité pour nous de prendre en main notre avenir.
