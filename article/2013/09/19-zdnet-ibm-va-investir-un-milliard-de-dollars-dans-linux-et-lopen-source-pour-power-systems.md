---
site: ZDNet
title: "IBM va investir un milliard de dollars dans Linux et l'open source pour Power Systems"
author: thierry-noisette
date: 2013-09-19
href: http://www.zdnet.fr/actualites/ibm-va-investir-un-milliard-de-dollars-dans-linux-et-l-open-source-pour-power-systems-39794142.htm
tags:
- Entreprise
- Économie
- Informatique en nuage
---

> Le centre IBM de Montpellier va bénéficier des trois quarts de cet investissement, qui doit aider les clients de Big Blue "à tirer avantage du Big Data et du cloud computing".
