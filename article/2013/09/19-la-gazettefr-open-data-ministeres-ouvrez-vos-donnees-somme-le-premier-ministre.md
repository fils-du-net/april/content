---
site: La gazette.fr
title: "Open data: ministères ouvrez vos données! somme le Premier Ministre"
author: V. Fauvel
date: 2013-09-19
href: http://www.lagazettedescommunes.com/195310/open-data-ministeres-ouvrez-vos-donnees-somme-le-premier-ministre
tags:
- Internet
- Administration
- Institutions
- Associations
- Licenses
- Standards
- Open Data
---

> Jean-Marc Ayrault, Premier ministre, a adressé le 13 septembre 2013 aux membres du Gouvernement une circulaire annonçant la publication du «Vade-mecum sur l’ouverture et le partage des données publiques» sur la plateforme www.datagouv.fr dont le nombre de jeux de données stagne depuis 2011.
