---
site: L'Express.fr
title: "\"Le Front national sera majoritaire\", prédit le philosophe Bernard Stiegler"
author: Benjamin Sportouch
date: 2013-09-12
href: http://www.lexpress.fr/actualite/politique/le-front-national-sera-majoritaire-predit-le-philosophe-bernard-stiegler_1280994.html
tags:
- Économie
- Partage du savoir
---

> Il a appris la philosophie en prison dans les années 1970-1980 et s'en sert aujourd'hui pour analyser les paysages politique et économique français. Pour Bernard Stiegler, il faut mettre en place un nouveau système, fondé sur le numérique. Grand entretien.
