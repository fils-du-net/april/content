---
site: 01netPro.
title: "IBM investit dans le logiciel libre à Montpellier"
author: Xavier Biseul
date: 2013-09-19
href: http://pro.01net.com/editorial/603508/ibm-investit-dans-le-logiciel-libre-a-montpellier
tags:
- Entreprise
- Économie
- Promotion
---

> A l’occasion du LinuxCon 2013, Big Blue a annoncé sa volonté d'investir un milliard de dollars dans les technologies Linux et open source pour ses serveurs Power Systems. Cet investissement a pour but d'aider les entreprises clientes, précise le communiqué, «à tirer avantage du big data et du cloud computing avec des systèmes modernes construits pour gérer la nouvelle vague d'applications arrivant dans les datacenters à l'ère post-PC».
