---
site: clubic.com
title: "Loi Hamon: le gouvernement recule encore sur la vente liée"
author: Ludwig Gallet
date: 2013-09-12
href: http://pro.clubic.com/legislation-loi-internet/actualite-584092-loi-hamon-le-gouvernement-recule-vente-liee.html
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
---

> Bis repetita pour la vente liée, qui vient d'être une nouvelle fois écartée du projet de loi sur la consommation portée par Benoît Hamon. Les associations crient au mensonge, alors que le candidat François Hollande avait promis d'agir sur le sujet.
