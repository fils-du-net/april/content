---
site: PC INpact
title: "Hadopi: la peine de 15 jours de suspension à Internet ne sera pas appliquée"
author: Marc Rees
date: 2013-09-05
href: http://www.pcinpact.com/news/82170-hadopi-peine-15-jours-suspension-a-internet-ne-sera-pas-appliquee.htm
tags:
- Internet
- HADOPI
- Institutions
---

> Le premier abonné condamné à une peine de suspension après une procédure Hadopi conservera finalement son accès à Internet. Un petit miracle que nous a confirmé la Hadopi. Explications.
