---
site: Numerama
title: "Un développeur de GIMP fait appel au crowdfounding pour ajouter une fonctionnalité"
author: Julien L.
date: 2013-09-18
href: http://www.numerama.com/magazine/27016-un-developpeur-de-gimp-fait-appel-au-crowdfounding-pour-ajouter-une-fonctionnalite.html
tags:
- Internet
- Économie
- Associations
- Innovation
---

> Nombreux sont les projets culturels à faire appel au crowdfunding pour voir le jour. Mais le financement participatif peut concerner des initiatives très diverses. Dans l'univers du logiciel libre, un développeur de GIMP (un logiciel d'édition et de retouche d'image) a demandé aux internautes un coup de main financier pour ajouter une fonctionnalité : la peinture en miroir.
