---
site: Numerama
title: "La Quadrature du Net censurée par le Robocop YouTube"
author: Guillaume Champeau
date: 2013-09-18
href: http://www.numerama.com/magazine/27015-la-quadrature-du-net-censuree-par-le-robocop-youtube.html
tags:
- Entreprise
- Internet
- Associations
- Droit d'auteur
- ACTA
---

> Quel est le comble d'une vidéo dénonçant les excès du droit d'auteur et le risque d'atteinte à la liberté d'expression, sinon d'être elle-même censurée pour violation de droits d'auteur?
