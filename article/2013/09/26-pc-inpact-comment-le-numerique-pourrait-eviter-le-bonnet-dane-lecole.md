---
site: PC INpact
title: "Comment le numérique pourrait éviter le bonnet d'âne à l'école"
author: Xavier Berne
date: 2013-09-26
href: http://www.pcinpact.com/news/82569-comment-numerique-pourrait-eviter-bonnet-dane-a-ecole.htm
tags:
- Administration
- DRM
- Éducation
- Standards
---

> Comment structurer, en France, une filière du numérique éducatif? Voilà le sujet sur lequel ont planché différents services de l’État avant de rendre un rapport complet sur le sujet, lequel fut d'ailleurs publié avec une série de recommandations. Formation des enseignants, gouvernance, fiscalité, interopérabilité... L'ensemble de ces questions est abordé.
