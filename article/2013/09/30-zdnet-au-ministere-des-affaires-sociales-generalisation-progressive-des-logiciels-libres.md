---
site: ZDNet
title: "Au ministère des Affaires sociales, \"généralisation progressive des logiciels libres\""
author: Thierry Noisette
date: 2013-09-30
href: http://www.zdnet.fr/actualites/au-ministere-des-affaires-sociales-generalisation-progressive-des-logiciels-libres-39794426.htm
tags:
- Administration
- Économie
- Institutions
---

> Le ministère des Affaires sociales et de la Santé, dirigé par Marisol Touraine, expose sa politique en matière de logiciels libres et open source: particulièrement volontariste, elle est marquée par un passage "systématique" aux logiciels libres.
