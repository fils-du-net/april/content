---
site: Direction Informatique
title: "Informatique libre: le gouvernement québécois a échoué, selon FACIL"
author: Benjamin Jébrak
date: 2013-09-09
href: http://www.directioninformatique.com/information-libre-le-gouvernement-a-echoue-selon-facil/21403
tags:
- Économie
- Institutions
- Associations
- Marchés publics
- International
---

> L’organisme FACIL conclut que le gouvernement du Québec a échoué dans la réalisation de son programme portant sur l’informatique libre.
