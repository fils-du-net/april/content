---
site: PC INpact
title: "Le procès de la diffusion des sources de Skype s'ouvre en France"
author: Marc Rees
date: 2013-09-02
href: http://www.pcinpact.com/news/82083-le-proces-diffusion-sources-skype-souvre-en-france.htm
tags:
- Entreprise
- Logiciels privateurs
- Interopérabilité
- Institutions
- Droit d'auteur
- Promotion
---

> Selon nos informations, le 24 septembre prochain, au Tribunal correctionnel de Caen, un important procès sera jugé à la demande de Skype contre ceux qui sont accusés d’avoir publié une partie des sources du fameux logiciel acheté 8,5 milliards de dollars par Microsoft.
