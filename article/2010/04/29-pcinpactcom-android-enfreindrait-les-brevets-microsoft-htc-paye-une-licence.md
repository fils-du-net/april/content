---
site: pcinpact.com
title: "Android enfreindrait les brevets Microsoft, HTC paye une licence"
author: Jeff
date: 2010-04-29
href: http://www.pcinpact.com/actu/news/56640-microsoft-android-htc-brevets.htm
tags:
- Logiciels privateurs
- Brevets logiciels
---

> [...] Microsoft ne peut pas être considéré comme un Patent troll, la firme n'ayant utilisé ses brevets logiciel contre une entreprise ne l'ayant pas attaqué en premier qu'à deux occasions ces trois dernières années. Mais cette action risque de ne pas être très bien vue dans le milieu, puisqu'elle s'attaque au constructeur et non à Google, propriétaire de l'OS qui contreviendrait à ces brevets. Le message est clair : Android n'est pas gratuit.
