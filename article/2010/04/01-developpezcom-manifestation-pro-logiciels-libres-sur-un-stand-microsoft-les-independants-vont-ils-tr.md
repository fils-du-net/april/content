---
site: developpez.com
title: "Manifestation pro logiciels libres sur un stand Microsoft, les indépendants vont-ils trop loin ? "
author: Katleen Erna
date: 2010-04-01
href: http://www.developpez.com/actu/15740/Manifestation-pro-logiciels-libres-sur-un-stand-Microsoft-les-independants-vont-ils-trop-loin
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> [...] C'est donc armés de seringues et déguisés en médecins et en porcs qu'ils sont partis en croisade contre le stand de Microsoft. Leur but : éradiquer la grippe 7 (en référence à Windows 7, le tout dernier OS de Microsoft). Un de leurs acolyte, déguisé en virus, se faisant poursuivre dans les allées.
