---
site: euractiv.fr
title: "L’agenda numérique de la Commission suscite certaines tensions"
author: La rédaction
date: 2010-04-09
href: http://www.euractiv.fr/economie-finance/article/2010/04/09/agenda-numerique-commission-suscite-certaines-tensions_66287
tags:
- Le Logiciel Libre
- April
- Europe
---

> Alors que le projet de la commissaire Neelie Kroes, devrait être dévoilé d’ici fin avril, beaucoup de crispations se font jour, notamment sur les questions de l'interopérabilité et des standards ouverts.
> [...] Quant à Alix Cazenave, chargée de mission pour l’April, elle estime que « les termes retenus par les brouillons de la révision du cadre européen d'interopérabilité (EIF) pour la définition de l'interopérabilité sont extrêmement inquiètants. Ce brouillon marquait un retour en arrière en enlevant toute mention du logiciel libre."
