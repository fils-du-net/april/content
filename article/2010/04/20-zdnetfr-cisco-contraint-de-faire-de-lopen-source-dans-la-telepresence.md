---
site: zdnet.fr
title: "Cisco contraint de faire de l'Open Source dans la téléprésence"
author: La rédaction
date: 2010-04-20
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39751023,00.htm
tags:
- Entreprise
- Internet
- Interopérabilité
---

> Afin d'obtenir le feu vert de Bruxelles pour le rachat de Tandberg, Cisco devait accepter certaines conditions, et en particulier de céder les droits de son protocole "Telepresence Interoperability Protocol" (TIP) à un organisme de normalisation. TIP passera sous licence Apache 2.0.
