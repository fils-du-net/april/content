---
site: pcinpact.com
title: "ACTA : la version officielle publiée mercredi, sans riposte graduée"
author: Marc Rees
date: 2010-04-16
href: http://www.pcinpact.com/actu/news/56426-acta-acac-sandrine-belier-transparence.htm
tags:
- ACTA
---

> Après le 8e round des négociations ACTA qui s’est déroulé à Wellington en Nouvelle-Zélande, entre le 12 et le 14 avril 2010, les participants ont voté à l'unanimité cette publication. La transparence a ses limites : on ne connaîtra pas les positions de chaque Etat autour de la table.
