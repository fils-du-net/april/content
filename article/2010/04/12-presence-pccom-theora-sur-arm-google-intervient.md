---
site: "presence-pc.com"
title: "Theora sur ARM : Google intervient"
author: Pierre Dandumont
date: 2010-04-12
href: http://www.presence-pc.com/actualite/google-theorarm-38859/
tags:
- Le Logiciel Libre
- Interopérabilité
- Brevets logiciels
- Licenses
- Video
---

> Google vient d'aider le projet TheorARM, destiné à proposer un décodeur optimisé pour le codec Theora avec les processeurs ARM. Expliquons. Theora est un codec vidéo libre, qui n'utilise pas de brevets (du moins en théorie) et qui a donc le gros avantage de ne pas nécessiter de paiements pour son utilisation. Theora est notamment utilisé par Mozilla comme codec pour les vidéos en HTML5, alors que d'autres (comme Microsoft ou Apple) préfèrent le H.264, plus efficace mais soumis à des brevets et à une redevance.
