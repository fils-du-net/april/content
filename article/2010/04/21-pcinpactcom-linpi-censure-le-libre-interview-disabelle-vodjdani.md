---
site: pcinpact.com
title: "L'INPI censure le Libre : Interview d'Isabelle Vodjdani"
author: Jeff
date: 2010-04-21
href: http://www.pcinpact.com/actu/news/56483-inpi-cite-des-sciences-libre-censure.htm
tags:
- Le Logiciel Libre
- Institutions
- Sensibilisation
---

> Dans un article publié hier sur Transactiv-exe.org, Isabelle Vodjdani expliquait que l'exposition de la Cité des Sciences &amp; de l'Industrie sur la Propriété Industrielle, qui a été ouverte au public hier et qui le restera jusqu'en février 2011, ne comporterai aucune référence au Libre, sur la demande de l'INPI.
