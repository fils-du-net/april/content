---
site: developpez.com
title: "\"Dans 5 ans, softwares et hardware seront gratuits\", prédit la Fondation Linux"
author: Katleen Erna
date: 2010-04-20
href: http://www.developpez.com/actu/16433/-nbsp-Dans-5-ans-logiciels-et-hardwares-seront-gratuits-nbsp-predit-la-Fondation-Linux
tags:
- Le Logiciel Libre
- Entreprise
---

> Dans 5 ans, softwares et hardware seront gratuits", prédit la Fondation Linux
> De nouvelles informations sur l'évolution économique de Linux ont été annoncées. Les dernières tendances macro-économiques auraient joué en sa faveur, en allant dans le sens de l'open source.
