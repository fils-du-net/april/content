---
site: linuxfr.org
title: "Un nouveau souffle pour Traduc.org "
author: apostle
date: 2010-04-12
href: http://linuxfr.org/2010/04/12/26721.html
tags:
- Le Logiciel Libre
- Internet
- Partage du savoir
---

> Traduc.org est une association « Loi 1901 » qui « encourage et réalise l'adaptation française des logiciels et documentations libres ». Elle émane d'un projet de traduction des guides pratiques (les fameux « HOWTO ») du Projet de documentation Linux (LDP) créé en 1992, ce qui en fait probablement le plus ancien projet libre qui existe en France, Linux en étant à cette époque tout juste à ses balbutiements.
