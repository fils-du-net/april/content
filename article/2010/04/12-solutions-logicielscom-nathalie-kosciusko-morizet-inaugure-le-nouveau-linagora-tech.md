---
site: "solutions-logiciels.com"
title: "Nathalie Kosciusko-Morizet inaugure le nouveau LINAGORA-TECH"
author: Frédéric Mazué
date: 2010-04-12
href: http://www.solutions-logiciels.com/actualites.php?titre=Nathalie-Kosciusko-Morizet-inaugure-le-nouveau-LINAGORA-TECH&actu=7241
tags:
- Le Logiciel Libre
- Entreprise
- Informatique en nuage
---

> [...] Au cours d'une table ronde qui réunissait quelques uns des plus brillants entrepreneurs du logiciel libre, la Secrétaire d'Etat a profité de sa présence pour redire l'importance et l'omniprésence du Logiciel Libre en France. Mais aussi sa nécessaire et naturelle implication dans les TIC pour favoriser l'innovation.
