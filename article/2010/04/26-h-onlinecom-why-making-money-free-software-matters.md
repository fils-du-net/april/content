---
site: "h-online.com"
title: "﻿Why Making Money from Free Software Matters"
author: Glyn Moody
date: 2010-04-26
href: http://www.h-online.com/open/features/Why-Making-Money-from-Free-Software-Matters-985505.html
tags:
- Le Logiciel Libre
- DRM
- Droit d'auteur
- Philosophie GNU
- English
---

> L'auteur détaille pourquoi il est important que le Logiciel Libre rapporte de l'argent et comment le monde de la musique pourrait s'en inspirer.
