---
site: silicon.fr
title: "Ubuntu, la distribution préférée de plus de 40% des utilisateurs Linux"
author: David Feugey
date: 2010-04-08
href: http://www.silicon.fr/fr/news/2010/04/08/ubuntu__la_distribution_linux_preferee_de_plus_de_40__des_utilisateurs
tags:
- Le Logiciel Libre
---

> Les responsables de Canonical viennent de dévoiler à nos confrères de LinuxPlanet qu’il y aurait aujourd’hui près de 12 millions d’utilisateurs de la distribution Linux Ubuntu dans le monde. C’est 50% de plus qu’au dernier pointage, effectué en 2008 (8 millions). Pour comparaison, une distribution comme la Fedora compte environ 4,5 millions d’utilisateurs.
