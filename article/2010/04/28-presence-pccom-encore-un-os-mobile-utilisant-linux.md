---
site: "presence-pc.com"
title: "Encore un OS mobile utilisant Linux"
author: David Civera
date: 2010-04-28
href: http://www.presence-pc.com/actualite/Android-Linux-39124/
tags:
- Le Logiciel Libre
- Entreprise
---

> Six firmes japonaises ont décidé de s’unir pour développer leur propre système d’exploitation mobile qui sera compatible avec Android et Symbian. [...] On pense qu’il s’agit surtout d’un désir de lutter contre l’iPhone qui représentait 46,1 % du marché des smartphones en 2009 au Japon. En s’associant, les sociétés réduisent les coûts de développement et le temps nécessaire pour l’écriture d’un tel système.
