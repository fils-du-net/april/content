---
site: 01net.com
title: "Le brevet d'Exalead invalidé après la poursuite engagée contre Sinequa"
author: Marie Jung
date: 2010-04-06
href: http://pro.01net.com/editorial/514918/une-histoire-de-brevet-dans-le-milieu-des-moteurs-de-recherche/
tags:
- Brevets logiciels
- Désinformation
---

> [...] Mi-mars, le tribunal de grande instance (TGI) de Paris a invalidé le brevet européen de l'éditeur Exalead, spécialiste des moteurs de recherche. [...] Instances européennes (avec l'Office européen des brevets) et française (avec l'INPI) ne semblent touours pas d'accord sur la définition de brevet logiciel. D'autant que jusqu'à nouvel ordre, cette notion n'a pas d'existence légale en Europe.
