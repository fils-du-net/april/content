---
site: developpez.com
title: "Les spammeurs aiment-ils Linux ? "
author: Katleen Erna
date: 2010-04-28
href: http://www.developpez.com/actu/16739/Les-spammeurs-aiment-ils-Linux-Les-PC-sous-Linux-enverraient-proportionnellement-plus-de-spams-que-les-autres-d-apres-Symantec
tags:
- Le Logiciel Libre
- Internet
---

> Malgré le peu de parts de marché détenues par Linux, les ordinateurs équipés par ce système serait les plus gros expéditeurs de spam. [...] 5.14 % des pourriels ont été envoyés par des machines Linux (alors que Linux ne représente que 1.03 % des parts de marché, d'après les dernières estimations de Symantec).
