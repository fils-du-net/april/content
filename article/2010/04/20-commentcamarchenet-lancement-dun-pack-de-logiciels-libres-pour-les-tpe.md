---
site: commentcamarche.net
title: "Lancement d'un pack de logiciels libres pour les TPE"
author: La rédaction
date: 2010-04-20
href: http://www.commentcamarche.net/news/5851883-lancement-d-un-pack-de-logiciels-libres-pour-les-tpe
tags:
- Le Logiciel Libre
- Entreprise
- Administration
---

> Lancement d'un pack de logiciels libres pour les TPE Des logiciels Open Source pour les petites entreprises : une idée saugrenue ? Pas pour la Chambre de Métiers et l'Artisanat de la Somme, le Fonds d'aide européen au développement régional et le Conseil régional de Picardie, qui viennent de lancer une suite de logiciels en licence Creative Commons spécialement conçue pour les créateurs et repreneurs d'entreprise artisanale.
