---
site: directioninformatique.com
title: "L'intelligence d'affaires à l'étroit dans le système d'information"
author: Gérard Blanc
date: 2010-04-13
href: http://www.directioninformatique.com/DI/client/fr/DirectionInformatique/Nouvelles.asp?id=57182
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
---

> Le logiciel libre, une solution à évaluer
> Le logiciel libre dispose assurément de tout ce qui est nécessaire, techniquement et technologiquement, pour se prévaloir d'ensembles stratégiques de l'informatique corporative. Le pire défaut du logiciel libre, dans ce cas, est la « capacité organisationnelle », pour entreprendre le projet et le réussir.
