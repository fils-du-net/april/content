---
site: pcinpact.com
title: "Rapport Gallo : La France déroule ses positions pro ACTA"
author: Marc Rees
date: 2010-04-28
href: http://www.pcinpact.com/actu/news/56616-acta-rapport-gallo-hadopi-loppsi2.htm
tags:
- International
- ACTA
---

> Le Secrétariat général des affaires européennes a fait connaître hier dans un courrier que nous nous sommes procuré, la position des autorités françaises sur les amendements au rapport Gallo. Ce document est essentiel dans la perspective du vote prévu initialement aujourd'hui et reporté fin mai concernant ce fameux document. Au fil des lignes, pas de doute : la France dévoile des positions pleinement compatibles avec l'ACTA et toutes ses dégénérescences.
