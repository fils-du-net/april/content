---
site: numerama.com
title: "Après l'APRIL, l'UFC-Que Choisir appelle la Commission européenne à respecter le libre"
author: Julien L.
date: 2010-04-03
href: http://www.numerama.com/magazine/15424-apres-l-april-l-ufc-que-choisir-appelle-la-commission-europeenne-a-respecter-le-libre.html
tags:
- Le Logiciel Libre
- Europe
---

> Les inquiétudes se multiplient suite aux rumeurs de nettoyage de l'agenda numérique de toute mention d'interopérabilité et de standards ouverts. Après l'APRIL il y a quelques jours, c'est au tour de l'UFC-Que Choisir d'appeler la Commission européenne à se conserver ces mentions dans la feuille de route finale.
