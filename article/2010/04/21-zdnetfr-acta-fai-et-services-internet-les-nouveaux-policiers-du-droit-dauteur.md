---
site: zdnet.fr
title: "ACTA: FAI et services Internet, les nouveaux policiers du droit d'auteur ?"
author: Christophe Auffray
date: 2010-04-21
href: http://www.zdnet.fr/actualites/internet/0,39020774,39751057,00.htm
tags:
- DRM
- ACTA
---

> [...] Autre point sensible du traité ACTA, celui relatif aux DRM, même si ceux-ci ne sont pas expressément cités dans le texte.
> [...] Non seulement l'acte visant à contourner ces protections serait puni, mais également la fourniture de services, technologies, équipements dont la fonction première serait de casser des mesures techniques de protection comme les DRM.
