---
site: siteduzero.com
title: "Une journée mondiale contre la gestion des droits numériques"
author: demenvil, Drumer67bts et O Mann
date: 2010-04-27
href: http://www.siteduzero.com/news-62-36306-p1-une-journee-mondiale-contre-la-gestion-des-droits-numeriques.html
tags:
- DRM
---

> Le mardi 4 mai 2010, à l'initiative de la Free Software Foundation, de l'Electronic Frontier Foundation et de l'Open Rights Group, aura lieu une journée contre la gestion des droits numériques ou Digital Rights Management. Ces restrictions peuvent par exemple empêcher ou limiter la copie d'œuvres culturelles (par exemple une piste sonore ou vidéo).
