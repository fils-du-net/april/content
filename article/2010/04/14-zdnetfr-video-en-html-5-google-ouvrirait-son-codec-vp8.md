---
site: zdnet.fr
title: "Vidéo en HTML 5 : Google ouvrirait son codec VP8"
author: Christophe Auffray
date: 2010-04-14
href: http://www.zdnet.fr/actualites/internet/0,39020774,39750874,00.htm
tags:
- Internet
- Interopérabilité
- Brevets logiciels
- Video
---

> [...] Google aurait-il entendu l'appel de la FSF de février dernier ? Après la finalisation du rachat par Google d'On2 Technologies, une société spécialisée dans les technologies de compression vidéo de haute qualité, la FSF demandait à la firme d'ouvrir le codec VP8 et de l'implémenter sur YouTube.
