---
site: lemagit.fr
title: "Open Source et brevets : IBM minimise la portée de ses promesses antérieures "
author: Reynald Fléchaux
date: 2010-04-08
href: http://www.lemagit.fr/article/ibm-brevets-mainframes-mainframe-emulation-open-source-brevet-turbohercules/6056/1/open-source-brevets-ibm-minimise-portee-ses-promesses-anterieures/
tags:
- Entreprise
- Brevets logiciels
---

> [...] Dans un communiqué, Big Blue affirme toutefois qu'il n'en est rien. Et explique : "En 2005, quand IBM a annoncé un accès libre à 500 de ses brevets, nous avions dit que cette promesse est applicable à des membres ou des sociétés qualifiés de la communauté Open Source. Nous avons des interrogations sérieuses autour de la qualification de TurboHercules. TurboHercules est membre d'organisations fondées et financées par des concurrents d'IBM, comme Microsoft, afin d'attaquer le mainframe. Nous avons des doutes quant aux motivations de TurboHercules".
