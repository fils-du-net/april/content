---
site: lemonde.fr
title: "Suppression de Linux sur PlayStation 3 : un joueur porte plainte"
author: La rédaction
date: 2010-04-29
href: http://www.lemonde.fr/technologies/article/2010/04/29/suppression-de-linux-sur-playstation-3-un-joueur-porte-plainte_1344316_651865.html
tags:
- Le Logiciel Libre
- Informatique-deloyale
---

> Un possesseur américain de PlayStation 3 vient de lancer une class action contre Sony Computer Entertainement America (SCEA), suite à la décision du constructeur de retirer la possibilité d'installer un système d'exploitation alternatif sur sa console. Selon le plaignant, un dénommé Anthony Ventura, "la décision de Sony de désactiver la fonction 'Installer un autre système d'exploitation' n'est basée que sur ses propres intérêts, aux dépenx de ses clients."
