---
site: owni.fr
title: "Le droit d’auteur est-il une notion périmée ?"
author: Guillaume de Lacoste
date: 2010-04-27
href: http://owni.fr/2010/04/27/le-droit-dauteur-est-il-une-notion-perimee/
tags:
- Droit d'auteur
---

> La notion de droit d'auteur est en train d'imploser, malgré les efforts désespérés des législateurs pour préserver le monde ancien. Pourtant, la fin du copyright ne marquera pas celle de la création. Au contraire, l'histoire fournit de nombreuses alternatives.
