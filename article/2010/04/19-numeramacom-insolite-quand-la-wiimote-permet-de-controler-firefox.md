---
site: numerama.com
title: "Insolite : quand la Wiimote permet de contrôler Firefox"
author: Julien L.
date: 2010-04-19
href: http://www.numerama.com/magazine/15544-insolite-quand-la-wiimote-permet-de-controler-firefox.html
tags:
- Le Logiciel Libre
- Entreprise
- Internet
---

> Chez Mozilla, la fondation à l'origine du navigateur Firefox, la "bidouillabilité" est un principe particulièrement essentiel.
> [...] Résultat, en s'appuyant sur quelques lignes de code (du C++ et du JavaScript), le développeur est parvenu à interagir avec le navigateur grâce... à la Wiimote de la Nintendo Wii. À travers une petite extension, il peut donc piloter Firefox par de larges mouvements, contrôler des éléments dans le navigateur ou jouer à des petits jeux vidéo.
