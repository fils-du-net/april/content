---
site: pcinpact.com
title: "Brevets Unix : victoire du libre dans le procès SCO/Novell"
author: Jeff
date: 2010-04-02
href: http://www.pcinpact.com/actu/news/56217-sco-novell-brevets-proces.htm
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] Le PDG de Novell Ron Hovsepian déclare sur le blog de l’entreprise : « Cette décision est une bonne nouvelle pour nous, pour Linux, et pour la communauté des logiciels libres. Nous avons longtemps affirmé que ce combat contre Linux n’avait aucune base solide, et sommes ravis que le jury, dans une décision à l’unanimité, soit d’accord. Je suis fier du rôle de Novell dans la protection des intérêts de Linux et de la communauté des logiciels libres ».
