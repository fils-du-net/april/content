---
site: pcinpact.com
title: "Firefox possède une part de marché mondiale de 30 %"
author: Vincent Hermann
date: 2010-04-02
href: http://www.pcinpact.com/actu/news/56224-firefox-parts-marche-mondial-rapport-state-internet.htm
tags:
- Le Logiciel Libre
- Internet
---

> [...] Selon Mozilla, Firefox bénéficierait d’une base de 350 millions d’utilisateurs, avec des parts de marché légèrement différentes en fonction des zones géographiques.
> [...] Dans les grandes lignes toutefois, les statistiques confirment le succès du navigateur de Mozilla, puisque la moyenne mondiale se situe à environ 30 %.
