---
site: zdnet.fr
title: "Le ministère de la Justice crée une licence «information publique librement réutilisable»"
author: Thierry Noisette 
date: 2010-04-08
href: http://www.zdnet.fr/blogs/l-esprit-libre/le-ministere-de-la-justice-cree-une-licence-information-publique-librement-reutilisable-39750743.htm
tags:
- Administration
- Partage du savoir
- Droit d'auteur
- Licenses
---

> Pourquoi tant de discrétion? C'est par un article dans le site Village-Justice.com, intitulé «Peut-on diffuser des données publiques sous licences libres et ouvertes?» (cassons tout de suite ce suspense intolérable: heureusement, la réponse est oui :-)) que l'on apprend la création de la licence IP, pour «information publique librement réutilisable», sur le répertoire des informations publiques du ministère de la Justice et des Libertés.
