---
site: progilibre.com
title: "Open source et biens communs"
author: Patrice Bertrand
date: 2010-04-15
href: http://www.progilibre.com/Open-source-et-biens-communs_a1093.html
tags:
- Le Logiciel Libre
- Entreprise
---

> Nous sommes dans la semaine du développement durable, et le moment est bien venu pour s'intéresser au logiciel libre en tant que bien commun.
>  L'un des aspects du logiciel libre les plus essentiels à mes yeux est de constituer un patrimoine de code source, comparable au patrimoine du savoir scientifique, un bien commun disponible à tous, et qui est la condition du progrès.
