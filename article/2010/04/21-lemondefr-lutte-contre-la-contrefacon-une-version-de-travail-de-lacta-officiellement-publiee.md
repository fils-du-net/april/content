---
site: lemonde.fr
title: "Lutte contre la contrefaçon : une version de travail de l'ACTA officiellement publiée"
author: La rédaction
date: 2010-04-21
href: http://www.lemonde.fr/technologies/article/2010/04/21/lutte-contre-la-contrefacon-une-version-de-travail-du-traite-acta-officiellement-publiee_1340760_651865.html
tags:
- DRM
- ACTA
---

> La Commission européenne a publié, mercredi 21 avril, une "version consolidée" de l'Anti-Counterfeiting Trade Agreement (ACTA), traité destiné à lutter de manière globale contre la contrefaçon. Négocié depuis plus de deux ans, l'ACTA ne faisait pas l'objet de communication officielle, jusqu'au vote d'une résolution du Parlement européen, en mars, appelant à plus de transparence.
