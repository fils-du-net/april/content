---
site: silicon.fr
title: "La fondation Linux chausse de nouvelles pointures"
author: David Feugey
date: 2010-04-07
href: http://www.silicon.fr/fr/news/2010/04/07/la_fondation_linux_chausse_de_nouvelles_pointures
tags:
- Le Logiciel Libre
- Entreprise
---

> La fondation Linux commence aujourd'hui à gagner en popularité. Ainsi, le nombre de ses membres croît rapidement depuis le début du mois de mars, avec même l’arrivée dans ses rangs de quelques grosses pointures.
