---
site: "ip-watch.org"
title: "L’éditeur d’un logiciel open source accuse IBM de concurrence déloyale – IBM demande une analyse"
author: Catherine Saez
date: 2010-04-26
href: http://www.ip-watch.org/weblog/2010/04/26/l%E2%80%99editeur-d%E2%80%99un-logiciel-open-source-accuse-ibm-de-concurrence-deloyale-ibm-demande-une-analyse/
tags:
- Le Logiciel Libre
- Entreprise
- Brevets logiciels
---

> IBM fait l’objet d’une plainte pour concurrence déloyale portée devant la Commission européenne par l’éditeur d’un logiciel open source, qui accuse le géant de l’informatique d’empêcher les consommateurs d’utiliser ce logiciel.
