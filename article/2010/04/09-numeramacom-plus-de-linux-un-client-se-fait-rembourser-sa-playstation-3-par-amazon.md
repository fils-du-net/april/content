---
site: numerama.com
title: "Plus de Linux ? Un client se fait rembourser sa Playstation 3 par Amazon"
author: Guillaume Champeau
date: 2010-04-09
href: http://www.numerama.com/magazine/15458-plus-de-linux-un-client-se-fait-rembourser-sa-playstation-3-par-amazon.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Lorsque Sony a annoncé qu'il allait retirer la possibilité d'installer un autre système d'exploitation sur la Playstation 3, et interdire l'accès au Playstation Network à ceux qui refuseraient de se plier à cette dégradation des fonctionnalités de la console, nous nous étions demandés si Sony n'aurait pas l'obligation de rembourser les joueurs qui le demanderaient.
