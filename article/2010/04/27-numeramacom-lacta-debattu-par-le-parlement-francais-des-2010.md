---
site: numerama.com
title: "L'ACTA débattu par le Parlement français dès 2010 ?"
author: Guillaume Champeau
date: 2010-04-27
href: http://www.numerama.com/magazine/15600-l-acta-debattu-par-le-parlement-francais-des-2010.html
tags:
- Europe
- ACTA
---

> [...] A cette occasion, Neelie Kroes a pu confirmer aux parlementaires l'accélération du calendrier. Il est prévu qu'un texte final soit signé par les négociateurs dès cette année, et qu'il soit présenté à la Commission Européenne et au Conseil européen fin 2010, pour une ratification courant 2011.
