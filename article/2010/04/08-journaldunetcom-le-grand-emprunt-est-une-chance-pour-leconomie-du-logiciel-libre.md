---
site: journaldunet.com
title: "\"Le grand emprunt est une chance pour l'économie du logiciel libre\""
author: Antoine Crochet-Damais
date: 2010-04-08
href: http://www.journaldunet.com/solutions/acteurs/nathalie-kosciusko-morizet-l-informatique-dans-le-grand-emprunt/la-place-du-libre-dans-le-grand-emprunt.shtml
tags:
- Le Logiciel Libre
- Administration
- Économie
---

> Consciente de la capacité d'innovation du logiciel libre, Nathalie Kosciusko-Morizet entend faire une place de choix à ce segment dans le cadre du grand emprunt. Cloud computing, e-santé, e-éducation, ville numérique... La secrétaire d'Etat à l'économie numérique égrène l'ensemble des thématiques informatiques identifiées dans le cadre du grand emprunt, sur lesquelles les acteurs de l'Open Source pourront se positionner.
