---
site: framablog.org
title: "Le CRM libre et citoyen CiviCRM adopté par la FSF"
author: aKa
date: 2010-04-19
href: http://www.framablog.org/index.php/post/2010/04/19/civicrm-fsf
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Associations
---

> CRM est l’acronyme anglais de Customer Relationship Management, que l’on traduit chez nous par Gestion de la relation client.
> Dans la communauté du libre, il y a un CRM qui a le vent en poupe actuellement, c’est CiviCRM. Déjà utilisé par Creative Commons ou la Fondation Wikimedia, c’est aujourd’hui la Free Software Foundation (FSF) de Richard Stallman qui a décidé de l’adopter (et nous invite à faire autant), en nous disant tout le bien qu’elle en pense dans un communiqué traduit ci-dessous.
