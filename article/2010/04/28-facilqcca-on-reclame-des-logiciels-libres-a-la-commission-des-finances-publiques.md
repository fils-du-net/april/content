---
site: facil.qc.ca
title: "On réclame des logiciels libres à la Commission des finances publiques"
author: acotte
date: 2010-04-28
href: http://facil.qc.ca/enbref/r%C3%A9clame-des-logiciels-libres-%C3%A0-la-commission-des-finances-publiques
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Administration
---

> Le 26 avril dernier lors de l'Étude des crédits budgétaires du ministère du Conseil du trésor et de l'Administration gouvernementale québécoise, Madame Marie Malavoy, députée péquiste du comté de Taillon a fait une intervention en faveur des logiciels libres.
> Elle a interrogé Mme Gagnon-Tremblay, présidente du Conseil du trésor, à propos des appels d'offres du gouvernement qui ne prévoient jamais l'utilisation de logiciels libres.
