---
site: alterinfos.org
title: "Amérique Latine - Favoriser l’essor du logiciel libre"
author: Sérgio Amadeu da Silveira
date: 2010-04-05
href: http://www.alterinfos.org/spip.php?article4152
tags:
- Le Logiciel Libre
- Administration
---

> La présence de plus en plus importante des ordinateurs dans nos vies donne aux logiciels, qui sont au cœur de l’utilisation que nous en faisons, une place centrale et un intérêt économique considérable. L’utilisation des logiciels libres permet de sortir de la logique marchande et de récupérer le contrôle sur nos outils de travail ou de loisir. L’article qui suit présente les multiples apports de l’Amérique latine dans ce domaine.
