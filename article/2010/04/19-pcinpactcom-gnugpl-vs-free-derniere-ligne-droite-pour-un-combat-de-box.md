---
site: pcinpact.com
title: "GNU/GPL vs Free : dernière ligne droite pour un combat de box"
author: Marc Rees
date: 2010-04-19
href: http://www.pcinpact.com/actu/news/56440-freebox-free-iptables-busybox.htm
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Licenses
---

> Nouvelle étape judiciaire dans l’affaire qui oppose les développeurs des solutions libres Iptables et Busybox à Free. Le juge de la mise en état de la 3e chambre du TGI a rendu une décision intermédiaire sur le contentieux opposant les créateurs d'un logiciel libre intégré au sein de la Freebox. C’est là une des étapes importantes permettant de vérifier si le dossier est complet et si chacune des parties s’est bien communiqué les différentes pièces. On apprend par la même occasion que les audiences sont attendues le 27 mai prochain.
