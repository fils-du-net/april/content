---
site: pcinpact.com
title: "TIC : l'Europe met le cap sur l'interopérabilité et les standards"
author: Marc Rees
date: 2010-04-20
href: http://www.pcinpact.com/actu/news/56453-decalaration-grenade-standards-ouverts-interoperabilite.htm
tags:
- Administration
- Économie
- Interopérabilité
- Europe
---

> Hier s’est tenue en Espagne, la réunion informelle des ministres européens des Télécommunications et de la Société de l’information, rendez-vous fondamental pour l’avenir des TIC en Europe. La Déclaration de Grenade, qui a été éditée veut ainsi définir les bases de l'avenir numérique européen dans le domaine des technologies de l'information.
