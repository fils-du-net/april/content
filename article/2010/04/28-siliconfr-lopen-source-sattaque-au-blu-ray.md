---
site: silicon.fr
title: "L'open source s'attaque au Blu-ray"
author: David Feugey
date: 2010-04-28
href: http://www.silicon.fr/fr/news/2010/04/28/le_format_blu_ray_disponible_en_open_source_un_premier_encodeur_open_source_pour_le_format_blu_ray
tags:
- Interopérabilité
- DRM
- Video
---

> La dernière version de x264 est capable de créer des flux vidéo H.264 compatibles avec les Blu-ray. Une avancée qui transformera le marché des solutions d’encodage Blu-ray.
> [...] Il convient toutefois de noter que l’utilisation d’une telle solution d’encodage nécessite le paiement de droits dans de nombreux pays (même s’il est vrai que le MPEG LA n’a jusqu’ici jamais cherché à inquiéter les concepteurs de solutions open source distribuées sous forme gratuite).
