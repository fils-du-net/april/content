---
site: zdnet.fr
title: "1er avril : les géants des TIC ont de l'humour"
author: Olivier Chicheportiche
date: 2010-04-01
href: http://www.zdnet.fr/actualites/internet/0,39020774,39750575,00.htm
tags:
- Le Logiciel Libre
- April
---

> Plus proche de nous, l'April, l'association de défense des logiciels libres, s'est fendue d'un communiqué de presse très ironique concernant le rapport de Christine Albanel sur le piratage de livres.
