---
site: silicon.fr
title: "Le code Android de retour sous Linux"
author: David Feugey
date: 2010-04-19
href: http://www.silicon.fr/fr/news/2010/04/19/le_code_android_de_retour_sous_linux
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] S’il est vrai que le code lié à Android n’est pas toujours adapté à un noyau Linux ‘classique’, sa réintégration ne devrait pas poser de réel problème. En fait, les seules véritables entraves sont les rythmes et stratégies de développement d’Android et de Linux, qui ne semblent pas toujours coïncider. La rançon du succès pour l’offre mobile de Google, adoptée actuellement au sein d’une trentaine de smartphones et comptant une logithèque de plus de 48 000 titres (dont 59,8 % sont des applications gratuites).
