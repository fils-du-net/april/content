---
site: auf.org
title: "Les 11es Rencontres Mondiales du Logiciel Libre"
author: La rédaction
date: 2010-04-07
href: http://www.auf.org/communication-information/actualites/rmll-2010.html
tags:
- Le Logiciel Libre
---

> Les Rencontres Mondiales du Logiciel Libre (RMLL) sont un cycle de conférences autour du logiciel libre. Ces Rencontres sont annuelles, existent depuis 2000 et se déroulent depuis 2003 dans une ville différente chaque année. Elles sont gratuites et libres d’accès à tous.
