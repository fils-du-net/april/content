---
site: numerama.com
title: "On avait presque oublié de fêter la Journée Mondiale de la Propriété Intellectuelle"
author: Guillaume Champeau
date: 2010-04-26
href: http://www.numerama.com/magazine/15598-on-avait-presque-oublie-de-feter-la-journee-mondiale-de-la-propriete-intellectuelle.html
tags:
- Désinformation
- Licenses
---

> Cette année, le thème de la Journée mondiale est "Innovation : lien entre les mondes". L'occasion pour le directeur général de l'OMPI de sortir une bonne blague. Dans un message qui note avec pertinence les révolutions apportées par Internet, et plus globalement par les nouvelles technologies qui lient les hommes et les sociétés en un même lieu, Francis Gurry conclut en disant que "le système de la propriété intellectuelle fait partie de ce processus de liaison", qu'il "facilite le partage de l’information", et "encourage l’innovation et la concurrence". Sacré Francis.
