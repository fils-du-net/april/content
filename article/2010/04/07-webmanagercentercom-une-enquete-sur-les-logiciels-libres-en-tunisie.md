---
site: webmanagercenter.com
title: "Une enquête sur les logiciels libres en Tunisie "
author: M.G.S
date: 2010-04-07
href: http://www.webmanagercenter.com/management/article-88846-technologie-une-enquete-sur-les-logiciels-libres-en-tunisie
tags:
- Le Logiciel Libre
- Administration
- International
---

> [...] Cette étude portera sur l’utilisation et le développement des logiciels libres en Tunisie, avec comme l'objectif de dresser l’état des lieux sur l’utilisation et le développement des logiciels libres, d’établir un benchmark sur la position de la Tunisie dans ce domaine et de proposer un plan d’action opérationnel pour promouvoir l’utilisation et le développement des logiciels libres.
