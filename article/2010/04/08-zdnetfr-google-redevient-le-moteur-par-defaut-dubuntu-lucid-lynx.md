---
site: zdnet.fr
title: "Google redevient le moteur par défaut d'Ubuntu Lucid Lynx"
author: Christophe Auffray
date: 2010-04-08
href: http://www.zdnet.fr/actualites/internet/0,39020774,39750732,00.htm
tags:
- Le Logiciel Libre
- Internet
---

> [...] Canonical a finalement décidé de faire machine arrière et contrairement à ce que le sponsor d'Ubuntu annonçait en janvier, Yahoo ne sera pas le moteur de recherche par défaut dans la nouvelle version de l'OS Linux.
