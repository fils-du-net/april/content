---
site: whitehouse.gov
title: "WhiteHouse.gov Releases Open Source Code"
author: Dave Cole
date: 2010-04-21
href: http://www.whitehouse.gov/tech
tags:
- Le Logiciel Libre
- Administration
- Philosophie GNU
- International
---

> Le blog du site la Maison Blanche aux USA annonce la publication de code source servant au site WhiteHouse.gov sous licence Gnu GPL.
