---
site: pcinpact.com
title: "Les 1er mails d'HADOPI partiraient sans logiciel de sécurisation"
author: Marc Rees
date: 2010-04-19
href: http://www.pcinpact.com/actu/news/56443-hadopi-mail-logiciel-securisation-courriers.htm
tags:
- Logiciels privateurs
- HADOPI
---

> Selon Marc Guez, à la tête de la SCPP, qui représente les majors de la musique, le mécanisme d’HADOPI sera bien en capacité juridique d’envoyer des messages d’avertissements sans que la question du logiciel sécurisation soit résolue.
> [...] On en arrive donc à une situation ubuesque où une autorité administrative reproche à des abonnés de ne pas sécuriser leur accès, sans qu’un (hypothétique) logiciel de sécurisation ne soit labélisé par cette instance pour les aider dans cette tâche.
