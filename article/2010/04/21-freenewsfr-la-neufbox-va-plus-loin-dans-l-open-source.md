---
site: freenews.fr
title: "La Neufbox va plus loin dans l’open source"
author: Yoann Ferret
date: 2010-04-21
href: http://www.freenews.fr/spip.php?article8163
tags:
- Le Logiciel Libre
- Entreprise
- Internet
---

> Après avoir mis en ligne les sources des logiciels open source utilisés dans sa Neufbox, SFR a décidé de pousser le concept plus loin en proposant un système permettant aux utilisateurs de compiler et de modifier eux-mêmes le firmware de leur boîtier.
