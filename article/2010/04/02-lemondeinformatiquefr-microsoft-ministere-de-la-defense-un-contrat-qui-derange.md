---
site: lemondeinformatique.fr
title: "Microsoft-Ministère de la Défense : un contrat qui dérange"
author: Jacques Cheminat
date: 2010-04-02
href: http://www.lemondeinformatique.fr/actualites/lire-microsoft-ministere-de-la-defense-un-contrat-qui-derange-30347-page-1.html
tags:
- Logiciels privateurs
- Administration
- Vente liée
---

> L'Association Francophone des Utilisateurs de Logiciels Libres s'inquiète de la signature d'un accord-cadre entre Microsoft Irlande et la Direction Interarmées des Réseaux d'Infrastructures et des Systèmes d'Information (DIRISI) portant sur la fourniture de la plupart des logiciels de l'éditeur à un prix préférentiel (100 € HT) et sur la mise en place d'un centre de compétence de l'éditeur au sein de cette direction.
