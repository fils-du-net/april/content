---
site: pcinpact.com
title: "Le bilan de la DADVSI ? Un « épouvantail à moineaux »"
author: Marc Rees
date: 2010-04-26
href: http://www.pcinpact.com/actu/news/56552-hadopi-tardy-dadvsi-moineaux-epouvantail.htm
tags:
- DADVSI
- Désinformation
- DRM
---

> Un « épouvantail à moineaux ». C’est en ces termes peu flatteurs - quoique printaniers - que Lionel Tardy qualifie la loi DADVSI (Droit d'auteur et droits voisins dans la société de l'information).
> [...] Pour mémoire, la loi DADVSI a sacralisé en France l’usage des DRM en imposant leur protection juridique large (usage, détention, communication, diffusion, etc.), épaulé par la fameuse Autorité de régulation des mesures techniques de protection.
