---
site: legalbiznext.com
title: "Peut-on diffuser des données publiques sous licences libres et ouvertes ?"
author: Thomas Saint-aubin
date: 2010-04-06
href: http://www.legalbiznext.com/droit/Peut-on-diffuser-des-donnees
tags:
- Administration
- Partage du savoir
- Licenses
- Contenus libres
---

> Avec un marché estimé à 27 milliards d’euros en Europe (1), la réutilisation des informations publiques représente un enjeu essentiel pour le développement de l’économie numérique et de la connaissance. [...] Pour favoriser la diffusion et la réutilisation de ce ’’patrimoine immatériel public’’, il est indispensable de s’interroger sur le régime de licence applicable.
