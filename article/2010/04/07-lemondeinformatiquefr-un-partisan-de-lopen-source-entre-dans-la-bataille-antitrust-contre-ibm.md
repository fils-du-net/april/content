---
site: lemondeinformatique.fr
title: "Un partisan de l'Open Source entre dans la bataille antitrust contre IBM"
author: Jacques Cheminat
date: 2010-04-07
href: http://www.lemondeinformatique.fr/actualites/lire-un-partisan-de-l-open-source-entre-dans-la-bataille-antitrust-contre-ibm-30373.html
tags:
- Le Logiciel Libre
---

> Un développeur, également lobbyiste, Florian Mueller, souhaite contribuer aux enquêtes de la Commission européenne sur l'abus de position dominante d'IBM, en accusant le géant de l'informatique de bloquer le travail des communautés du logiciel libre.
