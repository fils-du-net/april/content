---
site: numerama.com
title: "La France championne du monde de l'open-source ?"
author: Guillaume Champeau
date: 2010-04-30
href: http://www.numerama.com/magazine/15633-la-france-championne-du-monde-de-l-open-source.html
tags:
- Le Logiciel Libre
- International
---

> C'est une étude passée relativement inaperçue, mais qui a de quoi surprendre. Selon une cartographie et un classement réalisé par Red Hat, la France serait en terme d'activité le pays champion du monde de l'open-source, sur 75 pays étudiés. Elle précède l’Espagne, l’Allemagne, l’Australie, la Finlande, le Royaume-Uni, la Norvège et l'Estonie, preuve que l'Europe est particulièrement favorable au logiciel libre.
