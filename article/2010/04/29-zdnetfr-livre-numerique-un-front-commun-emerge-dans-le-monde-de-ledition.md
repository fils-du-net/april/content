---
site: zdnet.fr
title: "Livre numérique : un front commun émerge dans le monde de l'édition"
author: Guénaël Pépin
date: 2010-04-29
href: http://www.zdnet.fr/actualites/internet/0,39020774,39751241,00.htm
tags:
- Interopérabilité
- DRM
- Droit d'auteur
- Standards
---

> [...] Dans la même logique, les intervenants évoquent la création d' une norme standardisée de fichier pour le livre numérique. Un moyen de pérenniser l'achat numérique, comme l'achat physique, qui deviendrait interopérable. De son côté, le SDLC souhaite introduire des mesures de protection (DRM) pour les achats définitifs de livres.
