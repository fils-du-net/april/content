---
site: zdnet.fr
title: "Linux revient sur la PS3 de Sony grâce à un hacker"
author: Christophe Auffray
date: 2010-04-09
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39750755,00.htm
tags:
- Le Logiciel Libre
- Informatique-deloyale
---

> [...] L'installation d'un autre OS faisait partie des fonctionnalités introduites par Sony dès le lancement de sa console de jeu. Le 1er avril, le constructeur annonçait le retrait de cette fonction au gré de la diffusion de la version 3.21 du firmware.
> Pour tous les possesseurs d'une PS3 dotée d'une partition Linux, cela signifiait sa suppression, avec les données stockées sur cette dernière. L'installation du dernier firmware de Sony n'était pas imposé, toutefois le fabricant a fait en sorte de le rendre incontournable.
