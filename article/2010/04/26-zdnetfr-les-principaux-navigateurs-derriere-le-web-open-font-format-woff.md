---
site: zdnet.fr
title: "Les principaux navigateurs derrière le Web Open Font Format (WOFF)"
author: Guénaël Pépin
date: 2010-04-26
href: http://www.zdnet.fr/actualites/internet/0,39020774,39751172,00.htm
tags:
- Internet
- Interopérabilité
---

> Technologie - Jamais un format de police pour le Web n’aura autant fédéré. Supporté par Microsoft, Mozilla, Opera et Google, il promet de redéfinir le rapport des développeurs Web à l’utilisation des polices.
> Le Web Open Font Format (WOFF) sera-t-il le nouveau format de police standard sur le Web ? Soumise à l'organisme de standardisation World Wide Web Consortium (W3C) le 8 avril, la spécification a été publiée sur leur site le 19 avril.
