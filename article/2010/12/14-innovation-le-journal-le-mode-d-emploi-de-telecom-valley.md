---
site: innovation le journal
title: "Le mode d’emploi de Telecom Valley"
author: Elsa Bellanger
date: 2010-12-14
href: http://www.innovationlejournal.com/spip.php?article6253
tags:
- Le Logiciel Libre
- Entreprise
- Économie
- Licenses
---

> Journal quotidien gratuit, thématique, d'information générale, multimédia, interactif et tout public. Naja, agence de presse à la charte rédactionnelle éthique, humaniste et participative, propose reportages, interviews, débats, éditos, points de vue, réactions, photos, vidéos, diaporamas et infographies animées
