---
site: vousnousils
title: "Etherpad : logiciel chouchou des enseignants"
author: Sandra Ktourza
date: 2010-12-08
href: http://www.vousnousils.fr/2010/12/08/etherpad-logiciel-chouchou-des-enseignants-438498
tags:
- Le Logiciel Libre
- Internet
- Administration
- Éducation
---

> Ce logiciel open source donne de très bons résultats pédagogiques. Plusieurs enseignants l'utilisent avec succès, et il peut se révéler d'une grande aide avec les élèves handicapés. Retour sur quelques expériences.
