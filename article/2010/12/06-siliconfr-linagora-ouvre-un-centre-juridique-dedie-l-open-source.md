---
site: Silicon.fr
title: "Linagora ouvre un centre juridique dédié à l’open source"
author: David Feugey
date: 2010-12-06
href: http://www.silicon.fr/linagora-ouvre-un-centre-juridique-dedie-a-l%E2%80%99open-source-43251.html
tags:
- Le Logiciel Libre
- Entreprise
---

> Le Centre Juridique Open Source devient la vitrine juridique de Linagora. Un projet qui permettra de faciliter l’adoption des logiciels libres par les professionnels.
