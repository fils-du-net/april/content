---
site: Developpez.com
title: "Lancement du nom de domaine .42 par des militants de l'open-source, un projet soutenu par Tristan Nitot mais critiqué par d'autres"
author: La rédaction
date: 2010-12-28
href: http://www.developpez.com/actu/26408
tags:
- Le Logiciel Libre
- Internet
- Administration
- Associations
---

> Exemple de fermeture ou première brique d'un internet ouvert ?
