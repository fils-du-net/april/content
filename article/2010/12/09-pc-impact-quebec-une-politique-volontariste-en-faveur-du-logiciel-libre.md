---
site: PC Impact
title: "Québec : une politique volontariste en faveur du logiciel libre"
author: Marc Rees
date: 2010-12-09
href: http://www.pcinpact.com/actu/news/60770-quebec-logiciel-libre-projet-loi.htm
tags:
- Administration
---

> Un projet de loi vient d’être présenté à l’Assemblée nationale du Québec pour définir un cadre dans l’emploi des logiciels au sein de la plupart des organismes publics, centraux ou non. En coulisse, l’objectif visé est clair : mettre en concurrence le logiciel propriétaire et le logiciel libre, et préconiser ce dernier lorsqu'il s'avère le meilleur choix. Des mesures d’accompagnement sont également prévues pour inciter à son usage dans toutes les strates de l’administration.
