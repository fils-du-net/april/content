---
site: LeJournalduNet
title: "Java : Apache et Google votent contre les propositions d'Oracle"
author: Antoine CROCHET-DAMAIS, Journal du Net
date: 2010-12-08
href: http://www.journaldunet.com/developpeur/java-j2ee/java-apache-google-oracle-jcp.shtml
tags:
- Le Logiciel Libre
- Entreprise
- Associations
- Licenses
- Informatique en nuage
---

> Oracle a annoncé hier tambour battant que ses propositions de spécification pour Java SE 7 et 8 ont été approuvées par une écrasante majorité des membres du JCP.
