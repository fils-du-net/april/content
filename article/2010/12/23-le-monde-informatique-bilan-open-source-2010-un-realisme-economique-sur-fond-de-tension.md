---
site: Le Monde Informatique
title: "Bilan Open Source 2010 : Un réalisme économique sur fond de tension"
author: Jacques Cheminat
date: 2010-12-23
href: http://www.lemondeinformatique.fr/actualites/lire-bilan-open-source-2010-un-realisme-economique-sur-fond-de-tension-32487.html
tags:
- Le Logiciel Libre
- Entreprise
- Institutions
- Associations
- Innovation
---

> Si à tort les projets Open Source étaient assimilés à gratuité, l'année écoulée a permis de remettre les pendules à l'heure. Après une phase de consolidation, la plupart des entreprises qui fournissent des solutions Open Source parle maintenant de valorisation, commercialisation et non plus uniquement de technique. Cette position entraîne des contestations et des scissions.
