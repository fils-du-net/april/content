---
site: Le Monde Informatique
title: "Bilan Oracle-Sun 2010: Adoptions et abandons"
author: Maryse Gros
date: 2010-12-27
href: http://www.lemondeinformatique.fr/actualites/lire-bilan-oracle-sun-2010-adoptions-et-abandons-32501.html
tags:
- Entreprise
- Associations
- Brevets logiciels
---

> Si les technologies Sparc et Solaris ont été confortées par Oracle après le rachat de Sun, en revanche, l'éditeur s'est détourné de l'OS communautaire OpenSolaris. Les spécifications des prochains OpenJDK (kit de développement Java) ont été adoptées malgré les protestations de Google et d'Apache. Quant aux  utilisateurs de la base de données MySQL, ils ont vu augmenter sensiblement le tarif du support.
