---
site: LeJournalduNet
title: "La Chine cherche à s'affranchir de sa dépendance à Windows"
author: Dominique FILIPPONE
date: 2010-12-21
href: http://www.journaldunet.com/solutions/systemes-reseaux/windows-chine-neokylin-1210.shtml
tags:
- Entreprise
- Institutions
- International
---

> Deux concepteurs de systèmes d'exploitation chinois s'unissent pour créer un OS alternatif à Windows.
