---
site: Numerama
title: "Des noms de domaine en .42 pour un web libre et ouvert"
author: Guillaume Champeau
date: 2010-12-23
href: http://www.numerama.com/magazine/17668-des-noms-de-domaine-en-42-pour-un-web-libre-et-ouvert.html
tags:
- Internet
- Partage du savoir
- Associations
- Innovation
---

> "Une grande aventure, probablement de longue haleine, qui fait le pari de pouvoir réunir une philosophie et une communauté jugée nécessaire à la pérennité de l'Internet, des réseaux, et de l'informatique dans sa globalité, et de pouvoir faire face aux problèmes techniques engendrés par le choix d'un TLD équivoque et différent". C'est ainsi que se présente le projet de créer un réseau de pages web en .42, exclusivement dédiées au partage des savoirs à but non lucratif.
