---
site: ZDNet.fr
title: "Copie Privée : les redevances sur les tablettes ont été fixées"
author: Olivier Chicheportiche
date: 2010-12-16
href: http://www.zdnet.fr/actualites/copie-privee-les-redevances-sur-les-tablettes-ont-ete-fixees-39756905.htm
tags:
- Institutions
- DRM
---

> Copie Privée : les redevances sur les tablettes ont été fixées - Les ponctions pourront aller jusqu'à 12 euros en fonction de la capacité de stockage des tablettes. Elles seront effectives l'année prochaine.
