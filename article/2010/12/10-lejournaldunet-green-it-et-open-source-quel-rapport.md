---
site: LeJournalduNet
title: "Green IT et Open Source : quel rapport ?"
author: Antoine CROCHET-DAMAIS, Journal du Net
date: 2010-12-10
href: http://www.journaldunet.com/solutions/dsi/green-it-et-open-source.shtml
tags:
- Le Logiciel Libre
- Économie
- Institutions
- Innovation
- Philosophie GNU
- Standards
---

> L'Open Source est présent sur le terrain de l'informatique verte, notamment sur le segment de l'économie d'énergie. Mais, le lien entre les deux notions ne s'arrête pas là. Le point avec deux consultants.
