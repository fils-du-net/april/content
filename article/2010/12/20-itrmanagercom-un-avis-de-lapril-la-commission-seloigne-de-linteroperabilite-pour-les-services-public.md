---
site: ITRmanager.com
title: "Un avis de l'April La Commission s'éloigne de l'interopérabilité pour les services publics européens"
author: La rédaction
date: 2010-12-20
href: http://www.itrmanager.com/articles/113280/avis-april-commission-eloigne-interoperabilite-services-publics-europeens.html
tags:
- Interopérabilité
- April
- Institutions
- Standards
---

> La Commission européenne vient rendu publique le 16 décembre 2010 sa communication « Vers l'interopérabilité pour les services publics européens »  Ce texte opère un recul inacceptable sur les questions d'interopérabilité, et entérine la disparition des standards ouverts, déjà menacés par l'Agenda numérique européen.
