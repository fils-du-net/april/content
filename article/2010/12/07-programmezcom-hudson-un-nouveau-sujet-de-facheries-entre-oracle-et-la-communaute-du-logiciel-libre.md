---
site: Programmez.com
title: "Hudson : un nouveau sujet de fâcheries entre Oracle et la communauté du logiciel libre   "
author: Frédéric Mazué
date: 2010-12-07
href: http://www.programmez.com/actualites.php?id_actu=8659
tags:
- Le Logiciel Libre
- Entreprise
- Internet
---

> Hudson est une solution libre d'intégration continue en java, qui était hébergée par java.net, un portail de l'ère de Sun, pas vraiment connu pour sa solidité. C'est pourquoi les responsables du projet Hudson ont décidé de migrer vers GitHub,...
