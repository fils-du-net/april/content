---
site: OWNI
title: "WikiLeaks: Qui règne par le code tombera par le code"
author: Luis De Miranda
date: 2010-12-19
href: http://owni.fr/2010/12/15/wikileaks-qui-regne-par-le-code-tombera-par-le-code/
tags:
- Internet
- Partage du savoir
- Institutions
- Standards
---

> Pour le romancier et philosophe Luis de Miranda, WikiLeaks marque l'avènement d'une société dans laquelle la culture hacker se réapproprie les protocoles. Et les inverse.
