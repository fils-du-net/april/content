---
site: LE CERCLE Les Echos
title: "Repenser la délivrance des brevets"
author: Jacques Cremer
date: 2010-12-08
href: http://lecercle.lesechos.fr/economie-societe/recherche-innovation/221132270/repenser-la-delivrance-des-brevets
tags:
- Le Logiciel Libre
- Entreprise
- Économie
- Institutions
- Brevets logiciels
- International
---

> En entendant « refonte du capitalisme », nous pensons le plus souvent à la réforme de la régulation des marchés financiers et des banques.
