---
site: Eco89
title: "Un déjeuner en petit comité pour réconcilier Sarkozy et Internet"
author: François Krug
date: 2010-12-14
href: http://eco.rue89.com/2010/12/14/un-dejeuner-en-petit-comite-pour-reconcilier-sarkozy-et-internet-180779
tags:
- Entreprise
- Internet
- HADOPI
---

> Plusieurs personnalités d'Internet sont invitées à déjeuner par Nicolas Sarkozy jeudi. Des chefs d'entreprise, comme Xavier Niel, patron de Free, mais aussi des blogueurs, comme l'avocat anonyme Maître Eolas. Une liste étrange : de l'Hadopi au projet de loi Loppsi sur la sécurité, les invités du président de la République sont pour la plupart radicalement opposés à sa politique numérique.
