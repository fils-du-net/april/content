---
site: Numerama
title: "Le généticien Albert Jacquard attaque le concept de propriété intellectuelle"
author: Julien L.
date: 2010-12-06
href: http://www.numerama.com/magazine/17542-le-geneticien-albert-jacquard-attaque-le-concept-de-propriete-intellectuelle.html
tags:
- Le Logiciel Libre
- Brevets logiciels
- Désinformation
- Contenus libres
---

> Suite à la rencontre "Garantir les libertés publiques pour préserver les biens communs", le collectif Libre Accès a entrepris un travail visant à donner la parole aux artistes et aux chercheurs opposés au principe de la propriété intellectuelle, comme le généticien Albert Jacquard.
