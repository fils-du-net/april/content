---
site: PC INpact
title: "Le FBI aurait fait placer des backdoors dans le code de BSD"
author: Vincent Hermann
date: 2010-12-15
href: http://www.pcinpact.com/actu/news/60876-openbsd-backdoors-fbi-gregory-perry-theo-de-raadt.htm
tags:
- Le Logiciel Libre
- Internet
- Institutions
- International
---

> Coup de tonnerre dans le monde de l'informatique et d'UNIX. Des révélations faites par un ancien responsable technique d'une société indiquent que le FBI aurait intégré dans le code du projet BSD un certain nombre de portes dérobées, permettant ainsi à ceux qui savaient les trouver comment espionner l’activité des serveurs. Historique d’une révélation fracassante.
