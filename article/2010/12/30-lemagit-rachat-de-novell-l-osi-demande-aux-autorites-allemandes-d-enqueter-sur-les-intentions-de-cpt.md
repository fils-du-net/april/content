---
site: LeMagIT
title: "Rachat de Novell : l’OSI demande aux autorités allemandes d’enquêter sur les intentions de CPTN "
author: Cyrille Chausson
date: 2010-12-30
href: http://www.lemagit.fr/article/linux-novell-unix-brevets-rachat-open-source-attachmate/7800/1/rachat-novell-osi-demande-aux-autorites-allemandes-enqueter-sur-les-intentions-cptn/
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Associations
- Brevets logiciels
- International
---

> Plus d’un mois après le rachat de Novell par AttachMate, l’Open Source Initiative décide de demander aux autorités de la concurrence allemandes  d’enquêter sur les intentions de CPTN Holdings quant aux 882 brevets rachetés à l’éditeur de SuSe Linux. Selon l’organisme, ces technologies sont susceptibles de nuire à la concurrence en coupant radicalement l’herbe sous le pied de l’Open Source, voire en mettant en danger le modèle sur le marché.
