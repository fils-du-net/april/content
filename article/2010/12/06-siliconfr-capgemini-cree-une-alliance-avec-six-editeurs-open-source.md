---
site: Silicon.fr
title: "Capgemini crée une alliance avec six éditeurs open source"
author: David Feugey
date: 2010-12-06
href: http://www.silicon.fr/capgemini-cree-une-alliance-avec-six-editeurs-open-source-43247.html
tags:
- Le Logiciel Libre
- Entreprise
---

> Sous l’impulsion de Capgemini, Red Hat, Ingres, Pentaho, SugarCRM, Hippo et Zarafa proposent dorénavant une offre combinée. Une opportunité pour les entreprises attirées par les solutions open source.
