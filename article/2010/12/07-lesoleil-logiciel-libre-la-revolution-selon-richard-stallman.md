---
site: leSoleil
title: "Logiciel libre: la révolution selon Richard Stallman"
author: Pierre Asselin
date: 2010-12-07
href: http://www.cyberpresse.ca/le-soleil/actualites/science-et-technologie/201012/06/01-4349779-logiciel-libre-la-revolution-selon-richard-stallman.php
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Institutions
- Philosophie GNU
- Standards
---

> (Québec) La première chose qu'on remarque chez Richard Stallman, c'est son apparence, la barbe et les cheveux en désordre. Mais quand on l'écoute, c'est le pouvoir de ses idées qui frappe. Le «père» du logiciel libre a fondé un mouvement afin de protéger nos droits essentiels, notre «souveraineté» informatique.
