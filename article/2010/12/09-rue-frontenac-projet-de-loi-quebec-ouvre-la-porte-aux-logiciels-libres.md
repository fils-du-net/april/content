---
site: Rue Frontenac
title: "Projet de loi - Québec ouvre la porte aux logiciels libres"
author: Jean-François Codère
date: 2010-12-09
href: http://www.ruefrontenac.com/affaires/180-techno/31345-informatique-gouvernement-quebec
tags:
- Le Logiciel Libre
- Entreprise
- Administration
- Institutions
---

> Le gouvernement du Québec est passé de la parole aux actes en déposant mercredi matin un projet de loi qui devrait resserrer la gestion de ses ressources informatiques et, d'autre part, ouvrir davantage la porte aux logiciels libres.
