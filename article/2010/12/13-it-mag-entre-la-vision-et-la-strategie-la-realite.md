---
site: it mag
title: "Entre la vision et la stratégie, la réalité !"
author: Samir Tazaïrt
date: 2010-12-13
href: http://www.itmag-dz.com/spip.php?article1481
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Institutions
- International
---

> Est-il besoin de s’équiper d’un ordinateur et d’une connexion à Internet sans avoir préalablement intelligemment circonscrit les « besoins » ? Cette même question renvoie à la problématique du contenu, non en tant que « simple » ressource à lire ou à regarder mais plutôt en tant qu’usage sérieux et utile.
