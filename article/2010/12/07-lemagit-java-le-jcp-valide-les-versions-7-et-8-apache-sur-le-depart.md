---
site: LeMagIT
title: "Java : le JCP valide les versions 7 et 8, Apache sur le départ "
author: Cyrille Chausson
date: 2010-12-07
href: http://www.lemagit.fr/article/java-oracle-developpement-apache-jcp/7652/1/java-jcp-valide-les-versions-apache-sur-depart/
tags:
- Le Logiciel Libre
- Associations
- Licenses
---

> A 12 voix contre 3, le comité exécutif du JCP a approuvé la feuille de route des versions 7 et 8 de Java. Laissant la fondation Apache, qui avait menacé de quitter l’organisme en cas de vote en faveur du projet d’Oracle, au pied du mur.
