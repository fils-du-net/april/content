---
site: LeMagIT
title: "Etat du monde IT : Oracle souffle un vent glacial sur l'Open Source"
author: Cyrille Chausson
date: 2010-12-30
href: http://www.lemagit.fr/article/linux-linagora-opensource/7801/1/etat-monde-oracle-souffle-vent-glacial-sur-open-source/
tags:
- Entreprise
---

> En 2010, Oracle, en prenant place dans le monde de l’Open Source via Sun, a bouleversé une filière en pleine progression. Des communautés se sont détruites, d’autres sont nées, illustrant la capacité de rebond de l'open source. Les levées de fonds de Talend et BonitaSoft en sont une démonstration. Tout comme la réussite de Linux sur les téléphones portables.
