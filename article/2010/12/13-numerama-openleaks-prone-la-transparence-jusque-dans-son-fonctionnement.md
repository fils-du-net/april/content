---
site: Numerama
title: "Openleaks prône la transparence jusque dans son fonctionnement"
author: Julien L.
date: 2010-12-13
href: http://www.numerama.com/magazine/17583-openleaks-prone-la-transparence-jusque-dans-son-fonctionnement.html
tags:
- Internet
- Partage du savoir
- Institutions
- International
---

> Ancien porte-parole allemand de Wikileaks, Daniel Domscheit-Berg défend un projet alternatif qui corrigerait les travers du célèbre site lanceur d'alerte. En particulier, Daniel Domscheit-Berg reproche à Wikileaks d'avoir perdu de vue sa promesse open source.
