---
site: ICTjournal
title: "Logiciels libres: 1 milliards de francs d’économie pour les administrations suisses?"
author: Rodolphe Koller
date: 2010-12-13
href: http://www.ictjournal.ch/News/2010/12/13/Logiciels-libres-1-milliards-de-francs-deconomie-pour-les-administrations-suisses.aspx
tags:
- Le Logiciel Libre
- Administration
- Économie
- Institutions
- International
---

> Selon le conseiller d’Etat vaudois François Marthaler, l’utilisation de logiciels libres développés en commun pourraient réduire les dépenses IT des administrations suisses d’un milliard de francs par an.
