---
site: LeMagIT
title: "Open Source : Oracle fait valoir son droit de paternité pour garder le projet Hudson "
author: Cyrille Chausson
date: 2010-12-02
href: http://www.lemagit.fr/article/oracle-developpement-opensource/7621/1/open-source-oracle-fait-valoir-son-droit-paternite-pour-garder-projet-hudson/
tags:
- Le Logiciel Libre
- Entreprise
---

> Alors que la communauté d'Hudson souhaite extraire le projet d'intégration continue des serveurs d'Oracle pour des raisons techniques, la firme de Larry Ellison rappelle qu'elle est la seule détentrice de la marque. A moins de changer de nom, la communauté devra forker si elle souhaite migrer. Un dossier qui rappelle celui de la Document Foundation. Et qui marque une nouvelle étape dans les rapports conflictuels entre Oracle et la dimension communautaire de l'open source.
