---
site: OWNI
title: "Chaos Computer Club: Revendications pour un Net viable"
author: vasistas
date: 2010-12-29
href: http://owni.fr/2010/12/27/chaos-computer-club-revendications-pour-un-net-viable/
tags:
- Internet
- Administration
- Partage du savoir
- Institutions
- Brevets logiciels
- Droit d'auteur
- Standards
---

> Rappelant que l’accès à Internet pour tous est une des conditions favorisant la participation à la vie politique et culturelle, ce texte offre un condensé des positions du Chaos Computer Club, qui tient en ce moment son congrès annuel.
