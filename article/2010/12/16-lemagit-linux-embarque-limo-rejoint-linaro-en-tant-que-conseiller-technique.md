---
site: LeMagIT
title: "Linux embarqué : LiMo rejoint Linaro en tant que conseiller technique "
author: Cyrille Chausson
date: 2010-12-16
href: http://www.lemagit.fr/article/processeurs-limo-opensource-arm-linaro/7708/1/linux-embarque-limo-rejoint-linaro-tant-que-conseiller-technique/
tags:
- Le Logiciel Libre
- Entreprise
- Innovation
- International
---

> Enième signe de la convergence du monde Linux vers le secteur de l’embarqué. Le projet OS Open Source LiMo a décidé de rejoindre le comité technique (Linaro Technical Steering Committee) du projet Linaro, un groupement de constructeurs de puces embarquées ARM, en tant que conseiller (“Advisor Parter” selon le vocabulaire de Linaro).
