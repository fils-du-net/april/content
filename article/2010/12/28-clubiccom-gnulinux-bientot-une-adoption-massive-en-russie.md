---
site: clubic.com
title: "GNU/Linux : bientôt une adoption massive en Russie ?"
author: Guillaume Belfiore
date: 2010-12-28
href: http://www.clubic.com/linux-os/actualite-387652-gnu-linux-adoption-massive-russie.html
tags:
- Administration
- Institutions
- Associations
- International
---

> Le président du gouvernement russe Vladimir Poutine a récemment donné son accord à un document stipulant la migration massive vers le logiciel libre pour les infrastructures des antennes gouvernementales.
