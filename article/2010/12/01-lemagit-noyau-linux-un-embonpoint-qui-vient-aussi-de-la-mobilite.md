---
site: LeMagIT
title: "Noyau Linux : un embonpoint qui vient aussi de la mobilité "
author: Cyrille Chausson
date: 2010-12-01
href: http://www.lemagit.fr/article/mobilite-ibm-linux-novell-nokia-oracle-opensource-red-hat/7610/1/noyau-linux-embonpoint-qui-vient-aussi-mobilite/
tags:
- Le Logiciel Libre
- Entreprise
- Associations
---

> Dans son dernier rapport, la Linux Foundation révèle que Nokia, Samsung, Texas Instrument, Intel, AMD, sont désormais des contributeurs significatifs au développement du noyau Linux. Leur niveau de contribution se rapproche ainsi de celui des ténors du secteur, comme Red Hat, Novell ou IBM. Un signe que l’OS Open Source réussit sur le segment de la mobilité, et que l’unification des communautés doit se poursuivre.
