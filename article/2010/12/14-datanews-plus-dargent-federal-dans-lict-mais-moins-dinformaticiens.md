---
site: datanews
title: "Plus d'argent fédéral dans l'ICT, mais moins d'informaticiens"
author: Stefan Grommen
date: 2010-12-14
href: http://datanews.rnews.be/fr/ict/actualite/apercu/2010/12/14/plus-d-argent-federal-dans-l-ict-mais-moins-d-informaticiens/article-1194888031293.htm
tags:
- Le Logiciel Libre
- Administration
- Standards
- International
---

> Les pouvoirs publics fédéraux ont vu leurs budgets ICT disponibles augmenter de 6,5% sur les 5 dernières années. Le nombre d'informaticiens à temps plein a pour sa part régressé de 16,2%. Tels sont les chiffres avancés dans la deuxième enquête Fed-e View sur l'état de l'informatisation des administrations publiques. Cependant, note encore l'étude, les directeurs ICT des administrations estiment à 1.025 informaticiens le manque d'effectifs pour fonctionner de manière optimale.
