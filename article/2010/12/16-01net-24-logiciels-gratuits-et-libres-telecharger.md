---
site: 01net.
title: "24 logiciels gratuits et libres à télécharger "
author: La rédaction
date: 2010-12-16
href: http://www.01net.com/editorial/525104/24-logiciels-libres-et-gratuits-pour-tous-vos-besoins/
tags:
- Le Logiciel Libre
- April
---

> 01net. vous propose de découvrir et de télécharger les 24 logiciels sélectionnés par l'April dans la première édition de son « catalogue libre ».
