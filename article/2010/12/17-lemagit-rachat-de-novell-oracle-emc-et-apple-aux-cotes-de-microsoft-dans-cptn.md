---
site: LeMagIT
title: "Rachat de Novell : Oracle, EMC et Apple aux côtés de Microsoft dans CPTN "
author: Cyrille Chausson
date: 2010-12-17
href: http://www.lemagit.fr/article/microsoft-apple-novell-emc-brevets-oracle/7717/1/rachat-novell-oracle-emc-apple-aux-cotes-microsoft-dans-cptn/
tags:
- Entreprise
- Brevets logiciels
- International
---

> Le très secret consortium CPTN, géré par Microsoft et derrière le rachat de quelque 882 brevets de Novell, dévoile aujourd’hui un peu plus son visage. Selon le blog Floss Patents, qui observe le monde des logiciels et de l’éco-système Open Source, Redmond serait associé à Oracle, EMC et Apple dans ce groupement qui baserait, selon lui, sa principale activité sur les brevets.
