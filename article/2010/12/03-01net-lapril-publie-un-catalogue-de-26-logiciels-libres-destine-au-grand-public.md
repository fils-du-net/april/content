---
site: 01net.
title: "L'April publie un catalogue de 26 logiciels libres destiné au grand public"
author: Guillaume Deleurence
date: 2010-12-03
href: http://www.01net.com/editorial/524325/un-catalogue-de-26-logiciels-libres-destine-au-grand-public/
tags:
- Le Logiciel Libre
- April
- Sensibilisation
---

> L'April diffuse un guide d'applications open source correspondant à des usages quotidiens. L'association compte aussi le faire imprimer.
