---
site: ZDNet.fr
title: "La fondation Apache se retire du projet Java"
author: La rédaction
date: 2010-12-10
href: http://www.zdnet.fr/actualites/la-fondation-apache-se-retire-du-projet-java-39756754.htm
tags:
- Le Logiciel Libre
- Entreprise
- Associations
- Licenses
---

> La fondation Apache se retire du projet Java - Après la ratification des spécifications de Java 7 et 8, la fondation Apache, en conflit avec Oracle sur sa politique de licence, met fin à sa contribution à Java. Son président estime que le Java Community Process n'a rien d'un processus ouvert.
