---
site: cyberpresse.ca
title: "La politique sur le logiciel libre dévoilée la semaine prochaine"
author: Pierre Asselin
date: 2010-12-04
href: http://www.cyberpresse.ca/le-soleil/affaires/jeux-et-logiciels/201012/04/01-4349272-la-politique-sur-le-logiciel-libre-devoilee-la-semaine-prochaine.php
tags:
- Le Logiciel Libre
- Entreprise
- Administration
- Institutions
- Philosophie GNU
- International
---

> (Québec) Près de 700 participants sont attendus la semaine prochaine pour le Salon du logiciel libre du Québec, où la présidente du Conseil du trésor, Michelle Courchesne, devrait dévoiler les grandes lignes de sa politique-cadre sur les ressources informationnelles au gouvernement.
