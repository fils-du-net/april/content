---
site: Numerama
title: "L'April veut améliorer l'accessibilité des handicapés grâce au logiciel libre"
author: Julien L.
date: 2010-12-02
href: http://www.numerama.com/magazine/17510-l-april-veut-ameliorer-l-accessibilite-des-handicapes-grace-au-logiciel-libre.html
tags:
- Le Logiciel Libre
- April
- Accessibilité
---

> L'April organise ce samedi une cartopartie dont le but est de récolter des informations utiles aux personnes handicapées. L'association veut ainsi produire une cartographie qui facilitera à terme le déplacement dans les agglomérations des individus en situation de handicap.
