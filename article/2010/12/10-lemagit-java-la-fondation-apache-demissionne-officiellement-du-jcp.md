---
site: LeMagIT
title: "Java : La fondation Apache démissionne officiellement du JCP "
author: Cyrille Chausson
date: 2010-12-10
href: http://www.lemagit.fr/article/java-oracle-developpement-apache-opensource-jcp/7675/1/java-fondation-apache-demissionne-officiellement-jcp/
tags:
- Le Logiciel Libre
- Entreprise
- Associations
- Licenses
- Standards
---

> Deux jours après le vote du comité exécutif du JCP, validant la feuille de route de Java 7 et 8, la fondation Apache met sa menace à exécution et démissionne de l’organisme. Une décision lourde de sens qui vient cristalliser véritablement les tensions qui existent depuis 2006, clarifiant ainsi la situation auprès des développeurs.
