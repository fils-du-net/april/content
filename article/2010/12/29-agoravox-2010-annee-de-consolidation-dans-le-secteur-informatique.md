---
site: AgoraVox
title: "2010, année de consolidation dans le secteur informatique ?"
author: Denis Szalkowski
date: 2010-12-29
href: http://www.agoravox.fr/actualites/technologies/article/2010-annee-de-consolidation-dans-86509
tags:
- Entreprise
- Internet
- Logiciels privateurs
- HADOPI
- Innovation
- Informatique en nuage
---

> Pour ceux qui auraient été distraits par leurs "geekeries", iPhone ou iPad, voici une séance de rattrapage un peu longue sur les événements qui ont marqué le secteur informatique au cours de l’année 2010. Pour le cabinet selon Forrester, ce fut une année de consolidation...
