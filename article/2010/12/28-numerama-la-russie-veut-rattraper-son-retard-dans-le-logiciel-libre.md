---
site: Numerama
title: "La Russie veut rattraper son retard dans le logiciel libre"
author: Julien L.
date: 2010-12-28
href: http://www.numerama.com/magazine/17696-la-russie-veut-rattraper-son-retard-dans-le-logiciel-libre.html
tags:
- Logiciels privateurs
- Administration
- Institutions
- International
---

> Le gouvernement russe a décidé de mettre l'accent sur les logiciels libres dans les années à venir. Dès l'an prochain, certains ministères et organismes russes vont migrer vers des solutions alternatives. Cette première transition devra durer jusqu'en 2015, selon l'arrêté signé par Vladimir Poutine.
