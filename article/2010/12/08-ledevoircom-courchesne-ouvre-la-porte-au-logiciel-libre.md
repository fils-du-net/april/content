---
site: LeDevoir.com
title: "Courchesne ouvre la porte au logiciel libre"
author: Isabelle Porter
date: 2010-12-08
href: http://www.ledevoir.com/politique/quebec/312617/courchesne-ouvre-la-porte-au-logiciel-libre
tags:
- Le Logiciel Libre
- Administration
- Économie
- Institutions
- Licenses
- International
---

> Consultez l'article Courchesne ouvre la porte au logiciel libre sur Le Devoir en ligne. Le Devoir, le quotidien indépendant par excellence au Québec depuis 1910.
