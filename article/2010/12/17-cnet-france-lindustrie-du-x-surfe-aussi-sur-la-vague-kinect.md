---
site: cnet France
title: "L'industrie du X surfe aussi sur la vague Kinect"
author: La rédaction
date: 2010-12-17
href: http://www.cnetfrance.fr/news/l-industrie-du-x-surfe-aussi-sur-la-vague-kinect-39756934.htm
tags:
- Interopérabilité
- Innovation
- Video
---

> Deux mois après sa sortie, le Kinect a été adapté par l'industrie du X. C'est un spécialiste des jeux pour adultes en 3D, ThriXXX, qui est à l'origine de ce premier jeu.
