---
site: Developpez.com
title: "Microsoft investit dans la société française TurboHercules à l'origine d'une plainte contre IBM et ses mainframes « fermés »"
author: La rédaction
date: 2010-12-02
href: http://www.developpez.com/actu/24599/Microsoft-investit-dans-la-societe-francaise-TurboHercules-a-l-origine-d-une-plainte-contre-IBM-et-ses-mainframes-fermes
tags:
- Le Logiciel Libre
- Entreprise
- Interopérabilité
- Désinformation
- Licenses
---

> A l'origine d'une plainte contre IBM et ses mainframes « fermés ». Microsoft a injecté une sonne d'argent, non précisée, dans la société française TurboHercules.
