---
site: écrans
title: "WikiLeaks : une tempête de papier pour aujourd'hui"
author: Camille Gévaudan
date: 2010-12-17
href: http://www.ecrans.fr/Wikileaks-une-tempete-de-papier,11593.html
tags:
- Internet
- Institutions
- Philosophie GNU
---

> Depuis le coup d’éclat des internautes connus sous le nom d’Anonymous, qui s’en sont pris la semaine dernière aux sites Internet des « ennemis » de WikiLeaks, le mouvement de riposte et de soutien à Julian Assange commence à diversifier ses actions. Attention, précise un des multiples comptes Twitter s’exprimant au nom des « Anons » : « nous n’abandonnons aucune stratégie, nous en ajoutons simplement une nouvelle ».
