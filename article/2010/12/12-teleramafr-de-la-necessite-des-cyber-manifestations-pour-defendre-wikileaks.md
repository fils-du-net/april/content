---
site: Télérama.fr
title: "De la nécessité des cyber-manifestations pour défendre WikiLeaks"
author: Richard Stallman
date: 2010-12-12
href: http://www.telerama.fr/techno/de-la-necessite-des-cybermanifestations-pour-defendre-wikileaks-par-richard-stallman,63869.php
tags:
- Internet
- Institutions
- DRM
- International
---

> La semaine dernière, des internautes ont volé au secours du site WikiLeaks en organisant des manifestations virtuelles. Elles ont bloqué l'accès des sites Web d'Amazon, Visa ou Mastercard, accusés de collaborer avec le gouvernement américain dans sa bataille contre le site de Julian Assange. Richard Stallman, porte-parole très respecté du logiciel libre, voit dans ses cyber-ripostes un indispensable sursaut citoyen pour la défense de nos droits sur l'Internet. Le 17 décembre, il publiait une tribune sur le site du quotidien britannique "The Guardian". Voici sa traduction.
