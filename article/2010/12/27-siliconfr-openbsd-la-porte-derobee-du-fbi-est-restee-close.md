---
site: Silicon.fr
title: "OpenBSD : la porte dérobée du FBI est restée close"
author: David Feugey
date: 2010-12-27
href: http://www.silicon.fr/openbsd-la-porte-derobee-du-fbi-est-restee-close-43517.html
tags:
- Le Logiciel Libre
- Internet
- Administration
- Désinformation
- International
---

> Le FBI aurait tenté de s’aménager une porte d’entrée au sein d'OpenBSD. Suite à cette annonce, un audit a été réalisé. Conclusion : aucune faille de sécurité ne semble aujourd’hui présente dans la pile IPSEC de l’OS.
