---
site: webdo
title: "Vladimir Putin ordonne l’adoption des Logiciels Libres"
author: Rafik Ouerchefani
date: 2010-12-30
href: http://www.webdo.tn/2010/12/30/vladimir-putin-logiciels-libres/
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Administration
- Institutions
- Éducation
- International
---

> Le Krémlin était décidément sérieux quand il a annoncé l'adoption de GNU/Linux. La Russie tourne le dos à Microsoft et à tous les logiciels propriétaires
