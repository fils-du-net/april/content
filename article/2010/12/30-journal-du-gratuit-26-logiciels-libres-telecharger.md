---
site: Journal du Gratuit
title: "26 logiciels libres à télécharger."
author: Ajvar
date: 2010-12-30
href: http://www.journaldugratuit.com/actu/article.php?id=1293710455
tags:
- April
---

> Obtenez votre guide des logiciels libres et gratuits en version PDF avec "April". Vous y trouverez les liens pour télécharger vos logiciels gratuits de façon rapide, simple et sécuritaire.
