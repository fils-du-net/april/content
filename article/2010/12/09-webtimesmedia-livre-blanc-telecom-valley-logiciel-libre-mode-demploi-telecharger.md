---
site: WebTimesMedia
title: "Livre Blanc Telecom Valley : logiciel libre mode d'emploi à télécharger"
author: La rédaction
date: 2010-12-09
href: http://www.webtimemedias.com/entreprise06/wtm_article57130.fr.htm
tags:
- Le Logiciel Libre
- Entreprise
- Économie
- April
---

> Copie rendue pour la Commission Open Source de Telecom Valley : plus qu'un Livre Blanc, "Intégration et Publication de logiciels sous licences Open Source : Mode d'Emploi", représente un véritable guide à l'usage des entreprises et des collectivités qui veulent prendre le chemin du logiciel libre, présenté comme une arme "anti-délocalisation" efficace.
