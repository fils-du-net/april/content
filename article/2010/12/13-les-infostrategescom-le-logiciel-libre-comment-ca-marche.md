---
site: "les-infostrateges.com"
title: "Le logiciel libre, comment ça marche ?"
author: Fabrice MOLINARO
date: 2010-12-13
href: http://www.les-infostrateges.com/actu/10121090/le-logiciel-libre-comment-ca-marche
tags:
- Le Logiciel Libre
- April
- Sensibilisation
---

> Le groupe de travail Sensibilisation de l'April vient de publier un poster intitulé « Le logiciel libre, comment ça marche ? ».
