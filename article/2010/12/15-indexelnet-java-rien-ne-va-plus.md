---
site: indexel.net
title: "Java : rien ne va plus !"
author: Alain Bastide
date: 2010-12-15
href: http://www.indexel.net/actualites/java-rien-ne-va-plus-3243.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Associations
- Brevets logiciels
- Licenses
---

> Après Apple il y a 45 jours, la Fondation Apache s’oppose à son tour à Oracle et claque la porte du Java Community Process. La fondation Apache dénonce ainsi les dérives d’Oracle dans la gestion du langage de programmation.
