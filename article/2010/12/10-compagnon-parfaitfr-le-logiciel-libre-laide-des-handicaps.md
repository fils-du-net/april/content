---
site: "Compagnon-Parfait.fr"
title: "Le logiciel libre à l'aide des handicaps"
author: Isabelle Boucq
date: 2010-12-10
href: http://www.compagnon-parfait.fr/le-logiciel-libre-l-aide-des-handicaps-art-2645-1.html
tags:
- Le Logiciel Libre
- Accessibilité
- Innovation
- Contenus libres
---

> Constatant que les nouvelles technologies peuvent aider les personnes en situation de handicap dans leur vie quotidienne, des adeptes du logiciel libre se sont demandés comment mettre leur talent en action.
