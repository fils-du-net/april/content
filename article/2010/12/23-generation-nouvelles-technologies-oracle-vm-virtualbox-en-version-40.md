---
site: Génération nouvelles technologies
title: "Oracle VM VirtualBox en version 4.0"
author: Jérôme G.
date: 2010-12-23
href: http://www.generation-nt.com/telecharger-oracle-virtualbox-virtualisation-actualite-1133961.html
tags:
- Entreprise
- Interopérabilité
- Licenses
---

> La solution de virtualisation Oracle VM VirtualBox est disponible dans une nouvelle version majeure.
