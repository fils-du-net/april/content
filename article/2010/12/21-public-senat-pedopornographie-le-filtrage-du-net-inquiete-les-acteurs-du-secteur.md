---
site: PUBLIC SÉNAT
title: "Pédopornographie : le filtrage du Net inquiète les acteurs du secteur"
author: François Vignal
date: 2010-12-21
href: http://www.publicsenat.fr/lcp/politique/pedopornographie-filtrage-net-inquiete-acteurs-secteur-63113
tags:
- Entreprise
- Internet
- April
- Institutions
- Associations
---

> Au nom de la lutte contre la pédopornographie, les députés ont autorisé la création d’une liste noire de sites à bloquer, via le projet de loi Loppsi 2. Les acteurs du secteur numérique, comme certains députés UMP et PS, craignent une « censure collatérale » par du surblocage, voire de « la censure politique » directe, selon la Quadrature du Net.
