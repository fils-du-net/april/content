---
site: Numerama
title: "Un premier câble diplomatique de Wikileaks sur l'ACTA"
author: Guillaume Champeau
date: 2010-12-22
href: http://www.numerama.com/magazine/17657-un-premier-cable-diplomatique-de-wikileaks-sur-l-acta.html
tags:
- Entreprise
- Internet
- Institutions
- Europe
- International
- ACTA
---

> En novembre 2009, quatre mois avant la publication officielle du projet d'accord, la Suède avait prévenu les Etats-Unis que le secret entretenu sur le contenu de l'ACTA handicapait les discussions, en alimentant critiques et soupçons.
