---
site: L'USINE NOUVELLE
title: "Microsoft apporte un soutien discret au français..."
author: Christophe Dutheil
date: 2010-12-07
href: http://www.usinenouvelle.com/article/microsoft-apporte-un-soutien-discret-au-francais-turbohercules-en-guerre-avec-ibm.N142911
tags:
- Le Logiciel Libre
- Entreprise
- Interopérabilité
---

> L'éditeur américain vient d'investir dans la jeune pousse française TurboHercules, à l'origine d'un émulateur en open source pour les grands systèmes « mainframe ». Signe particulier : elle est en guerre avec IBM, qu'elle accuse d'abus de position dominante...
