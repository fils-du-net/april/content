---
site: "Altermonde-sans-frontières"
title: "Le code de la raison"
author: "8119"
date: 2010-12-27
href: http://www.altermonde-sans-frontiere.com/spip.php?article15498
tags:
- Le Logiciel Libre
- Institutions
---

> Tout a une raison, et le moins qu’on attende des lois c’est que ces raisons soient énumérées et soupesées. Le plus souvent je suis marginal chez les programmeurs parce que je n’utilise pas le « orienté objet ». Évidemment dit comme cela ça fait peur au badaud, mais si j’avais pris le temps de le leur (...)
