---
site: "ouest-france.fr"
title: "Le cadastre à l'heure du numérique - Belle-Isle-en-Terre"
author: La rédaction
date: 2010-12-18
href: http://www.ouest-france.fr/actu/actuLocale_-Le-cadastre-a-l-heure-du-numerique-_22005-avd-20101218-59574177_actuLocale.Htm
tags:
- Administration
- Innovation
- Contenus libres
---

> Le cadastre désormais numérique
> Franck Le Provost, directeur du pays de Guingamp est venu présenter le nouveau cadastre numérique. Conçu à partir de logiciels libres, ce programme n'aura coûté que 7 000 € et permettra d'équiper l'ensemble des communes.
