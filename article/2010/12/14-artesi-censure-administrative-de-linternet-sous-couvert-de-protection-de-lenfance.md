---
site: artesi
title: "Censure administrative de l'Internet sous couvert de protection de l'enfance"
author: La rédaction
date: 2010-12-14
href: http://www.artesi.artesi-idf.com/public/article/censure-administrative-de-l-internet-sous-couvert-de-protection-de-l-enfance.html?id=22457
tags:
- Le Logiciel Libre
- Internet
- April
- Institutions
---

> Niaiserie technologique ou volonté de contrôle accru ? interroge April.
