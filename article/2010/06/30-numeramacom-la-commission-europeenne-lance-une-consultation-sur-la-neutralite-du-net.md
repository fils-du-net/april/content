---
site: Numerama
title: "La Commission européenne lance une consultation sur la neutralité du net"
author: Julien L.
date: 2010-06-30
href: http://www.numerama.com/magazine/16117-la-commission-europeenne-lance-une-consultation-sur-la-neutralite-du-net.html
tags:
- Internet
- Neutralité du Net
---

> La Commission européenne a débuté aujourd'hui une consultation sur la neutralité du net. L'objectif pour l'exécutif européen est de recueillir les opinions des différents acteurs concernés par ce concept. L'enjeu est de taille puisqu'il permettra de déterminer - à l'issue de la consultation - la position de la Commission européenne. Avec en ligne de mire l'égalité de traitement des données de l'Internet, sans tenir compte de leur origine, de leur nature ou de leur destination.
