---
site: lemondeinformatique.fr
title: "NKM : financer et non pas subventionner le numérique innovant"
author: Bertrand Lemaire
date: 2010-06-08
href: http://www.lemondeinformatique.fr/actualites/lire-nkm-financer-et-non-pas-subventionner-le-numerique-innovant-30847.html
tags:
- Le Logiciel Libre
- Administration
- Informatique en nuage
---

> Nathalie Kosciusko-Morizet a présenté lundi 7 juin la consultation publique sur l'action de soutien aux usages et contenus numériques innovants. Le plan de financement de 2,5 milliards d'euros sera avant tout un plan d'investissements, pas de subventions.
> [...] Nous cherchons à découvrir des projets ou des idées que nous ne connaissons pas actuellement. Nous lançons d'ailleurs un appel particulier aux éditeurs de logiciels libres et aux créateurs d'outils open-source. »
