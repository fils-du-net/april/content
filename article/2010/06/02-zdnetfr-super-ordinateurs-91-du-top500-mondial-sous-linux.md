---
site: zdnet.fr
title: "Super-ordinateurs: 91% du top500 mondial sous Linux ! "
author: Didier Durand
date: 2010-06-02
href: http://www.zdnet.fr/blogs/media-tech/super-ordinateurs-91-du-top500-mondial-sous-linux-39752098.htm
tags:
- Le Logiciel Libre
- Innovation
- Sciences
- International
---

> La BBC a analysé le top500 des super-ordinateurs mondiaux récemment publié sous divers angles: répartition géographique, par constructeur, par domaine d'activités, par type de  processeur etc.
> Pour les systèmes d'exploitation, Linux domine outrageusement:  à 91% (selon Korben)
