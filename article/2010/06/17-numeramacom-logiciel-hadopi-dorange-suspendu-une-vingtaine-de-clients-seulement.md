---
site: Numerama
title: "Logiciel Hadopi d'Orange suspendu : une vingtaine de clients seulement"
author: Guillaume Champeau
date: 2010-06-17
href: http://www.numerama.com/magazine/15992-logiciel-hadopi-d-orange-suspendu-une-vingtaine-de-clients-seulement.html
tags:
- Internet
- HADOPI
- Informatique-deloyale
---

> Entre la nomination de Christine Albanel à la direction de la communication de France Télécom et le lancement du premier logiciel anti-P2P avant-même l'envoi des premiers e-mails de l'Hadopi, Orange s'est affirmé comme FAI pro-riposte graduée. Une position qui sera difficile à assumer. Il a déjà dû suspendre la commercialisation de son désastreux logiciel de contrôle du téléchargement.
