---
site: zdnet.fr
title: "Géoportail veut prendre du poids en séduisant les développeurs"
author: Guénaël Pépin
date: 2010-06-02
href: http://www.zdnet.fr/actualites/geoportail-veut-prendre-du-poids-en-seduisant-les-developpeurs-39752110.htm
tags:
- Administration
- Interopérabilité
- Partage du savoir
- Contenus libres
---

> L’IGN veut étendre les possibilités de son service Géoportail en aidant les développeurs tiers à utiliser sa base cartographique dans des services Web et mobiles.
