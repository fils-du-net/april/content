---
site: PCInpact
title: "Chrome surpasse Safari aux USA, et progresse dans le monde"
author: Nil Sanyas
date: 2010-06-29
href: http://www.pcinpact.com/actu/news/57948-google-chrome-safari-firefox-opera-internet-explorer.htm
tags:
- Le Logiciel Libre
- Internet
- International
---

> La montée en puissance de Chrome se confirme. Si le navigateur de Google reste toujours très loin d’Internet Explorer et Firefox, il surpasse Opera mais aussi Safari d’Apple. Et si c’est le cas dans le monde depuis la fin de l’année 2009, et en Europe depuis quelques mois, les États-Unis, où Apple a une part de marché très importante, ont résisté plus longtemps que les autres pays.
