---
site: lemondeinformatique.fr
title: "Des appels d'offres publics plus ouverts au Quebec"
author: Bertrand Lemaire
date: 2010-06-07
href: http://www.lemondeinformatique.fr/actualites/lire-des-appels-d-offres-publics-plus-ouverts-au-quebec-30837.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Administration
- April
---

> [...] L'April (Association pour la promotion et la recherche en informatique libre) exulte mais n'a pas, à ce jour, relayé la moindre action concernant des marchés publics similaires passés en France. En effet, de très nombreux appels d'offres de marchés publics sont encore actuellement publiés en France en précisant un produit défini d'un éditeur précis (par exemple la suite bureautique d'un éditeur américain), la concurrence étant réduite au distributeur final.
