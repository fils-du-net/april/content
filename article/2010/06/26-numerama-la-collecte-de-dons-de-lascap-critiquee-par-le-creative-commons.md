---
site: Numerama
title: "La collecte de dons de l'ASCAP critiquée par le Creative Commons"
author: Julien L.
date: 2010-06-26
href: http://www.numerama.com/magazine/16082-la-collecte-de-dons-de-l-ascap-critiquee-par-le-creative-commons.html
tags:
- Droit d'auteur
- Contenus libres
---

> La démarche entreprise par l'ASCAP, la Sacem américaine, ne fait décidément pas l'unanimité. Le mouvement Creative Commons a réagi en décortiquant les affirmations de la société d'auteur sur l'"asséchement" prétendu de la musique par les licences libres. Et de rappeler que beaucoup d'artistes membres de ces sociétés sont également des soutiens de ces licences libres.
