---
site: ZDNet
title: "\"Ubuntu est plus sur que Windows\" dixit Dell"
author: Christophe Sauthier
date: 2010-06-25
href: http://www.zdnet.fr/blogs/ubuntu-co/ubuntu-est-plus-sur-que-windows-dixit-dell-39752355.htm
tags:
- Le Logiciel Libre
- Entreprise
---

> Une information a été donnée il y a quelques jours sur le site officiel de Dell aux Etats Unis: Ubuntu est plus sûr que Windows, tout particulièrement pour les utilisateurs qui craignent les virus.
