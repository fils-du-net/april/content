---
site: Numerama
title: "Une amende d'un milliard de dollars pour tuer LimeWire ?"
author: Julien L.
date: 2010-06-09
href: http://www.numerama.com/magazine/15912-une-amende-d-un-milliard-de-dollars-pour-tuer-limewire.html
tags:
- Le Logiciel Libre
- Droit d'auteur
---

> LimeWire est sur la sellette. Après avoir perdu un premier procès contre les ayants droit américains, le logiciel libre est menacé par une demande de dommages et intérêts d'un montant très élevé. Dans un courrier, la RIAA estime que la somme pourrait flirter avec le milliard de dollars. De quoi tuer définitivement la société qui édite le logiciel.
