---
site: Numerama
title: "ACTA : des sanctions pénales pouvant aller au-delà de l'acquis européen ?"
author: Julien L.
date: 2010-06-26
href: http://www.numerama.com/magazine/16092-acta-des-sanctions-penales-pouvant-aller-au-dela-de-l-acquis-europeen.html
tags:
- Internet
- ACTA
---

> Ces jours-ci, les représentants de la Quadrature du Net et de l'April ont pu rencontrer les négociateurs français en charge du traité international contre la contrefaçon (ACTA). Une réunion qui n'a malheureusement pas tenu ses promesses, puisque les deux organisations sont ressorties avec davantage de doutes et d'interrogations.
