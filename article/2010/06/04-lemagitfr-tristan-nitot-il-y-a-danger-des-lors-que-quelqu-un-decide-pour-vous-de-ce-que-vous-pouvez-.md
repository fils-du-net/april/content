---
site: lemagit.fr
title: "Tristan Nitot : «il y a danger dès lors que quelqu’un décide pour vous de ce que vous pouvez installer»"
author: Valéry Marchive
date: 2010-06-04
href: http://blogs.lemagit.fr/2010/06/04/tristan-nitot
tags:
- Logiciels privateurs
- Interopérabilité
- DRM
---

> D’emblée, Tristan Nitot précise sa pensée sur l’iPad : pour lui, «c’est un ordinateur puisque l’on peut y installer des logiciels» et, surtout, «il y a un danger dès lors que quelqu’un décide pour vous de ce que vous pouvez installer.»
