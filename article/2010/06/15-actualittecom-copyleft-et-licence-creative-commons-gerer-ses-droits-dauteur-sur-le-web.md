---
site: actualitte.com
title: "Copyleft et licence Creative Commons, gérer ses droits d'auteur sur le web"
author: Marie Lebert
date: 2010-06-15
href: http://www.actualitte.com/actualite/19598-copyleft-creative-commons-droits-auteurs.htm
tags:
- Internet
- Partage du savoir
- Licenses
---

> Des auteurs et autres créateurs souhaitent respecter la vocation première du web, qui est d'être un réseau de diffusion. De ce fait, les adeptes de contrats flexibles - copyleft, licence GPL, licence GFDL et licence Creative Commons - sont de plus en plus nombreux.
