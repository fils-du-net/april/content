---
site: clubic.com
title: "Découverte d'un cheval de Troie ciblant Linux"
author: Guillaume Belfiore
date: 2010-06-14
href: http://www.clubic.com/antivirus-securite-informatique/virus-hacker-piratage/malware-logiciel-malveillant/actualite-346286-decouverte-cheval-troie-ciblant-linux.html
tags:
- Le Logiciel Libre
- Internet
- Informatique-deloyale
---

> [...] A l'avenir les fichiers mis au téléchargement devraient être accompagnés d'une signature électronique notamment via PGP (Pretty Good Privacy) et son équivalent sur Linux GPG (GNU Privacy Guard). Celui-ci permet de garantir l'authenticité d'un fichier ou la confidentialité d'un message.
