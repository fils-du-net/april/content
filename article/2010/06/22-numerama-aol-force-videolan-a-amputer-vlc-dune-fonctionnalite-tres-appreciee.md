---
site: Numerama
title: "AOL force VideoLan à amputer VLC d'une fonctionnalité très appréciée"
author: Julien L.
date: 2010-06-22
href: http://www.numerama.com/magazine/16029-aol-force-videolan-a-amputer-vlc-d-une-fonctionnalite-tres-appreciee.html
tags:
- Le Logiciel Libre
- Entreprise
- Video
---

> La version 1.1 de VLC est désormais disponible au téléchargement. Si elle apporte sans surprise son lot de nouveautés et de correctifs, elle voit aussi la fin du service SHOUTcast. En effet, les développeurs du projet VideoLan ont annoncé avoir subi d'importantes pressions de la part d'AOL pour retirer cette fonctionnalité.
