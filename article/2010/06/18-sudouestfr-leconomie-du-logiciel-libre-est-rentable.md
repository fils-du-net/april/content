---
site: sudouest.fr
title: "L'économie du logiciel libre est rentable"
author: Christophe Loubes   
date: 2010-06-18
href: http://www.sudouest.fr/2010/06/18/logiciels-libres-et-viables-120065-2780.php
tags:
- Le Logiciel Libre
- Économie
---

> L'économie du logiciel libre est rentable. L'idée fait l'objet d'une conférence aujourd'hui à la CUB.
> [...] Quelles retombées attendez-vous de cette conférence ?
> Aucune dans l'immédiat. Nous semons. Nous nous appliquons à dire aux gens qu'ils ne doivent pas avoir peur, que les logiciels libres sont accessibles à des non-spécialistes. Si la France est le pays qui compte le plus de contributeurs, c'est probablement parce que ces logiciels renvoient à des notions comme le service public ou la souveraineté qui comptent dans un pays comme le nôtre.
