---
site: lexpansion.com
title: "Les valeurs du logiciel libre sont-elles en danger ?"
author: Raphaële Karayan
date: 2010-06-02
href: http://www.lexpansion.com/economie/actualite-high-tech/les-valeurs-du-logiciel-libre-sont-elles-en-danger_233431.html
tags:
- Le Logiciel Libre
- Entreprise
- Informatique en nuage
---

> [...] 84% des entreprises que vous avez interrogées mettent au premier rang des menaces sur le logiciel libre "l'oubli des valeurs fondatrices". De quoi parlent-elles ?
> Historiquement, le mouvement du logiciel libre remonte aux années 80. Son penseur, Richard Stallman, estimait que pouvoir copier et adapter un logiciel était une sorte de liberté fondamentale, et y associait des valeurs de responsabilité sociétale. Dans les années 90, l'école de l'open source a dit, en gros, que l'aspect droit fondamental était exagéré. Que ce qui comptait, c'est que l'open source entre dans les entreprises, auxquelles le côté libertaire faisait un peu peur. C'est un système de valeurs différent. Dans l'oubli des valeurs dont parle l'étude, il y a la liberté, l'humanisme, et l'idée que le logiciel est aussi là pour enrichir le patrimoine de l'humanité.
