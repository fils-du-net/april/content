---
site: "Le Soleil - cyberpresse.ca"
title: "(Québec) Semaine-clé pour le logiciel libre"
author: Pierre Asselin
date: 2010-06-10
href: http://www.cyberpresse.ca/le-soleil/actualites/science-et-technologie/201006/10/01-4288529-semaine-cle-pour-le-logiciel-libre.php
tags:
- Le Logiciel Libre
- Administration
- Institutions
---

> « Marie Malavoy, porte-parole de l'opposition officielle en matière de recherche et développement et innovations technologiques, aura l'occasion aujourd'hui de présenter, au Bureau de l'Assemblée nationale, un projet visant à faire l'essai du logiciel libre sur la colline parlementaire.
> Le Bureau est formé de quelques députés, notamment le président de l'Assemblée nationale, le whip en chef du gouvernement ainsi que la whip de l'opposition officielle. «J'ai demandé à les rencontrer pour présenter un document qui présente le logiciel libre, explique quels sont ses avantages, et fait état de quelques expériences qui ont réussi ailleurs dans le monde», déclarait-elle en entrevue au Soleil.
