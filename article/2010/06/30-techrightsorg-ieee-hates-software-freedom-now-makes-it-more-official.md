---
site: techrights
title: "IEEE Hates Software Freedom, Now Makes it More Official"
author: Dr. Roy Schestowitz
date: 2010-06-30
href: http://techrights.org/2010/06/30/ieee-for-swpats-monopoly/
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Brevets logiciels
- Standards
- International
- English
---

> La IEEE prend une position de plus en plus pro logiciel propriétaire via des accords avec Microsoft et d'autres entités monopolistiques.
