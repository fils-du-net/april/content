---
site: numerama.com
title: "Une nouvelle génération de DRM proposée par l'lEEE"
author: Guillaume Champeau
date: 2010-06-16
href: http://www.numerama.com/magazine/15986-une-nouvelle-generation-de-drm-proposee-par-l-leee.html
tags:
- Internet
- DADVSI
- DRM
---

> L'IEEE souhaite mettre au point une norme de DRM qui permettrait aux consommateurs de partager, prêter et de même de vendre les oeuvres les oeuvres qu'ils achètent, mais d'une manière qui les oblige à un comportement responsable.
> [...] Sur le papier, c'est bien pensé. En pratique, ça ne fonctionnera probablement jamais. Pour des raisons techniques qui font que tout contenu peut être copié, et qu'aucun DRM n'est infaillible, mais surtout pour des raisons plus fondamentales. Le partage à des inconnus fait partie intégrante de l'univers numérique. Toute initiative qui ne prend pas en compte cette réalité est vouée à l'échec.
