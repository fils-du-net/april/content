---
site: numerama.com
title: "Google modifie (un peu) la licence WebM pour éviter un conflit avec le Libre"
author: Julien L.
date: 2010-06-05
href: http://www.numerama.com/magazine/15888-google-modifie-un-peu-la-licence-webm-pour-eviter-un-conflit-avec-le-libre.html
tags:
- Internet
- Interopérabilité
- Video
---

> "Comme elle était écrit originellement, si une plainte pour violation de brevet était intentée contre Google, la licence prenait fin. Cette disposition n'est pas en elle-même inhabituelle dans les licences Open Source Software, et des dispositions similaires existent dans la seconde licence Apache et dans la version 3 de GPL (General Public Licence)".
> "La subtilité était que notre licence mettait fin à "l'ensemble" des droits, et non pas uniquement aux brevets, ce qui a fait que notre licence était incompatible avec avec les versions 2 et 3 de GPL" a expliqué Chris DiBona. Désormais, le projet WebM affiche deux pages distinctes : l'une dédiée à la licence du conteneur et l'autre aux droits de propriété intellectuelle additionnels.
