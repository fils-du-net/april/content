---
site: Numerama
title: "Insolite : il développe vraiment un pare-feu Open Office !"
author: Julien L.
date: 2010-06-17
href: http://www.numerama.com/magazine/15993-insolite-il-developpe-vraiment-un-pare-feu-open-office.html
tags:
- Internet
- HADOPI
- Institutions
- Désinformation
---

> Souvenez-vous. Lors des débats sur le projet de la loi Hadopi, Christine Albanel avait fait "sensation" en expliquant sans rire que les suites bureautiques comme Microsoft Office ou Open Office étaient équipées d'un pare-feu destiné à protéger l'utilisateur de tout trafic hostile.
