---
site: lemonde.fr
title: "L'Europe pousse les gouvernements vers le logiciel libre"
author: La rédaction
date: 2010-06-10
href: http://www.lemonde.fr/technologies/article/2010/06/10/l-europe-pousse-les-gouvernements-vers-le-logiciel-libre_1370844_651865.html
tags:
- Le Logiciel Libre
- Europe
---

> La commissaire européenne en charge du numérique, Neelie Kroes, a mis en garde ce jeudi les gouvernements européens contre le risque de se voir "enfermés accidentellement dans une technologie propriétaire", visant sans le nommer Microsoft, et annoncé la mise en place d'un programme de conseils aux Etats pour qu'ils développent leur utilisation de logiciels libres.
