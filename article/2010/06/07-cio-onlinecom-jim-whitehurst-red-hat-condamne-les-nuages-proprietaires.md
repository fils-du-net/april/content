---
site: "cio-online.com"
title: "Jim Whitehurst, Red Hat, condamne les nuages propriétaires"
author: Jean Elyan
date: 2010-06-07
href: http://www.cio-online.com/actualites/lire-jim-whitehurst-red-hat-condamne-les-nuages-proprietaires-2971.html
tags:
- Internet
- Logiciels privateurs
- Informatique en nuage
---

> Le CEO de Red Hat veut que les utilisateurs de services de cloud computing puissent changer aisément de fournisseurs.
>  « L'architecture Cloud doit être certifiée pour permettre le déplacement des applications en son sein, sinon nous risquons de voir les clouds être à l'origine de blocages, » a mis en garde Jim Whitehurst le dirigeant de Red Hat. « Une fois que les utilisateurs sont engagés dans un mode de fonctionnement, il leur est difficile de bouger, » déclare le dirigeant.
