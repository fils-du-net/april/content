---
site: "actu-cci.com"
title: "La révolution est en marche"
author: Érick Haehnsen
date: 2010-06-01
href: http://www.actu-cci.com/dossier/Dossier_86/3391/
tags:
- Le Logiciel Libre
- Entreprise
- Éducation
- Licenses
- Informatique en nuage
---

> À côté de cela, vous avez lancé une initiative pédagogique mondiale auprès des universités. De quoi s’agit-il ?
> « Cela s’appelle OSOE, One Student, One ERP. Les universités qui ont besoin d’enseigner la gestion d’entreprise à leurs étudiants éprouvent des difficultés à s’équiper. Elles n’ont presque jamais les moyens de fournir, en toute légalité, une licence propriétaire à 1 000 étudiants et de les installer sur des serveurs… [...] À présent, nous voulons donner à chaque étudiant la possibilité d’utiliser gratuitement cet ERP, qui sera également destiné à terme à la formation continue. »
