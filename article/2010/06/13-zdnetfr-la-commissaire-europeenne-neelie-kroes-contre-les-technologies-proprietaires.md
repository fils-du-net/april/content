---
site: zdnet.fr
title: "La commissaire européenne Neelie Kroes contre les technologies propriétaires"
author: Thierry Noisette
date: 2010-06-13
href: http://www.zdnet.fr/blogs/l-esprit-libre/la-commissaire-europeenne-neelie-kroes-contre-les-technologies-proprietaires-39752370.htm
tags:
- Internet
- Interopérabilité
- Institutions
- Europe
---

> Neelie Kroes, commissaire européenne chargée du programme numérique (digital agenda en v.o.), a tenu jeudi un discours en faveur des formats et standards informatiques ouverts, devant l'OpenForum Europe à Bruxelles.
> Dans son discours (en tout cas dans sa version écrite – en anglais – sur laquelle figure la classique mention «seul le prononcé fait foi»), la commissaire européenne explique:
>   «Imaginons deux standards en compétition qui sont tous deux techniquement excellents pour une tâche donnée, mais qui différent dans leur niveau de contrainte et de mise en oeuvre. Lequel de ces deux standards aura selon vous le plus de mise en oeuvre [implementation] et d'utilisations, y compris pour des buts imprévus? Celui que vous pouvez télécharger d'un site web et utiliser sans restriction? Ou l'autre que vous devez acheter, qui est restreint à certains domaines d'utilisation et qui nécessite le paiement de royalties pour des droits de propriété intellectuelle intégrés?»
