---
site: lexpress.fr
title: "Ubuntu, l'électron libre qui défie Windows"
author: Emmanuel Paquette
date: 2010-06-02
href: http://www.lexpress.fr/actualite/high-tech/ubuntu-l-electron-libre-qui-defie-windows_896618.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> [...] L'arrivée, tous les six mois, d'une nouvelle mouture donne d'ailleurs l'occasion aux adeptes de se réunir lors d'une Ubuntu Party, comme le week-end dernier, à la Cité des sciences de Paris. "Nous pouvons recevoir jusqu'à 5 000 personnes en deux jours", raconte Olivier Fraysse, organisateur de l'événement, qui a eu le privilège, à l'automne 2009, d'accueillir Shuttleworth himself à ce grand rendez-vous.
