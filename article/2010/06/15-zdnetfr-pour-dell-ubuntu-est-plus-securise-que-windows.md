---
site: zdnet.fr
title: "Pour Dell, Ubuntu est plus sécurisé que Windows"
author: Christophe Auffray
date: 2010-06-15
href: http://www.zdnet.fr/actualites/pour-dell-ubuntu-est-plus-securise-que-windows-39752417.htm
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
---

> Technologie - Plus sécurisé que Windows, à égalité en termes d’usages, Dell fait la promotion d’Ubuntu, quitte à user de certains raccourcis. Si le marketing est au rendez-vous, Dell est toujours à la peine sur le plan commercial puisqu’il propose les modèles Windows et Ubuntu à des prix équivalents.
