---
site: 01net.com
title: "T. Nitot, Mozilla : « Firefox est toujours sur le devant de la scène »"
author: Christophe Guillemin
date: 2010-06-04
href: http://www.01net.com/editorial/517804/t-nitot-mozilla-firefox-est-toujours-sur-le-devant-de-la-scene/
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
---

> [...] Pour ma part, je ne considère pas Microsoft comme négligeable. Microsoft est toujours le leader en parts de marché, même s'il est en chute libre. Il bénéficie toujours de cet avantage de la vente liée. A dire vrai, Chrome et Firefox, ainsi qu'Opera, sont des alliés dans la promotion d'un Web ouvert et respectueux des standards.
