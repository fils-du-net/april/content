---
site: 01net
title: "Loppsi : les critiques de la Cnil"
author: Arnaud Devillard
date: 2010-06-22
href: http://www.01net.com/editorial/518517/loppsi-les-critiques-de-la-cnil/
tags:
- Internet
- Institutions
---

> La nouvelle loi sur la sécurité intérieure va être examinée au Sénat. La Commission nationale de l'informatique et des libertés a des réserves sur la vidéosurveillance et les scanners corporels.
