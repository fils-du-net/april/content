---
site: euractiv
title: "Creative economy is under attack: Time to act "
author: La rédaction
date: 2010-06-28
href: http://www.euractiv.com/en/enterprise-jobs/creative-economy-under-attack-time-act-acta-analysis-495689
tags:
- Internet
- Économie
- Désinformation
- Droit d'auteur
- International
- ACTA
- English
---

> L'Europe, les USA et le Japon souhaitent des lois fortes sur la "propriété intellectuelle" pour protéger leur économie créative selon des grandes entreprises.
