---
site: "generation-nt.com"
title: "ABI Research : les smartphones à base Linux en plein essor"
author: Christian D.
date: 2010-06-03
href: http://www.generation-nt.com/android-smartphones-croissance-abi-research-actualite-1026651.html
tags:
- Le Logiciel Libre
- Entreprise
- Économie
---

> Selon ABI Research, ce sont les smartphones à base Linux qui sont à l'origine de la forte croissance de ce segment, avec Google Android en tête.
> [...] " Du fait de leur faible coût et des possibilités de personnalisation, Linux représente aujourd'hui le même type de rupture sur le marché mobile qu'il le fut dans le domaine des serveurs il y a dix ans ", explique Victoria Fodale, analyste ABI.
