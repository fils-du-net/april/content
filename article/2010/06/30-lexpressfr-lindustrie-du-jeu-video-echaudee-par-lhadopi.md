---
site: lexpress.fr
title: "L'industrie du jeu vidéo échaudée par l'Hadopi"
author: Emmanuel Paquette
date: 2010-06-30
href: http://blogs.lexpress.fr/tic-et-net/2010/06/lindustrie-du-jeu-video-echaud.php
tags:
- Le Logiciel Libre
- Internet
- HADOPI
---

> Raté. Après la musique et le cinéma, les industriels des jeux vidéo, derrière Microsoft, Sony et Nintendo, ont délibéré hier soir pour choisir  la société qui traquera les pirates sur la Toile: DetectNet ou Trident Media Guard (TMG).
