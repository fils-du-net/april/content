---
site: "cio-online.com"
title: "Quatre mois pour tenter d'éclaircir le contrat Microsoft / Défense Nationale"
author: Bertrand Lemaire
date: 2010-06-17
href: http://www.cio-online.com/actualites/lire-quatre-mois-pour-tenter-d-eclaircir-le-contrat-microsoft-defense-nationale-3004.html
tags:
- Logiciels privateurs
- Administration
---

> [...] En février dernier, on apprenait que le Ministère de la Défense avait signé un accord cadre très complet avec Microsoft Irlande permettant à celui-ci d'équiper à un tarif très préférentiel (100 euros par poste) tous les postes de travail du ministère avec la plupart des logiciels bureautiques courants de l'éditeur. Un tel accord, passé sans appel d'offres, coupe l'herbe sous le pied à toutes les offres concurrentes, à commencer par les offres open-source.
