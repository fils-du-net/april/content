---
site: Numerama
title: "Les chiffres du piratage à nouveau contestés par des experts"
author: Julien L.
date: 2010-06-18
href: http://www.numerama.com/magazine/16010-les-chiffres-du-piratage-a-nouveau-contestes-par-des-experts.html
tags:
- Internet
- Économie
- Désinformation
---

> Ce n'est pas une surprise, le plus grand flou entoure la méthodologie utilisée par les ayants droit pour quantifier le piratage. Une chose est sûre, les conclusions amènent toujours au même constat : d'importants dégâts pour l'économie du secteur, voire pour l'économie toute entière. Or, des experts recommandent à la classe politique une certaine prudence face à ces rapports.
