---
site: Agoravox
title: "Open Data : des licences libres pour concilier innovation sociale et économique"
author: blog regards citoyens
date: 2010-06-16
href: http://www.agoravox.fr/tribune-libre/article/open-data-des-licences-libres-pour-76452
tags:
- Le Logiciel Libre
- Licenses
- Open Data
---

> Le mouvement OpenData vit depuis quelques mois un véritable essor avec l’adoption de bonnes pratiques par un nombre croissant d’institutions nationales comme locales... Partout ces actions suivent une démarche identique, adoptant les critères bien définis du savoir ouvert, conditions nécessaires au développement d’usages innovants socialement et économiquement...
