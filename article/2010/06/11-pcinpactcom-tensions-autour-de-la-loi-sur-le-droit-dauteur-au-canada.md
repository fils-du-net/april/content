---
site: pcinpact.com
title: "Tensions autour de la loi sur le droit d'auteur au Canada"
author: Jeff
date: 2010-06-11
href: http://www.pcinpact.com/actu/news/57601-canada-loi-anti-piratage-copie-privee.htm
tags:
- Internet
- DRM
- International
---

> Le gouvernement conservateur du Canada a présenté la semaine dernière une loi voulant mettre à jour la législation sur les droits d'auteurs pour l'adapter à l'ère numérique. Le texte, appelé « Bill C-32 : Canadian Copyright Modernization Act » se veut équilibrée, légalisant certaines pratiques communes tout en proposant de protéger les verrous numériques et d'améliorer le système de lutte contre le piratage de masse...
