---
site: ecrans.fr
title: "L'Allemagne n'aura pas son Hadopi"
author: Andréa Fradin 
date: 2010-06-17
href: http://www.ecrans.fr/L-Allemagne-n-aura-pas-son-Hadopi,10167.html
tags:
- HADOPI
- Europe
---

> [...] Le 14 juin, le gouvernement allemand a rappelé, par la voix de sa ministre fédérale de la justice Sabine Leutheusser-Schnarrenberger, son opposition au système de riposte graduée.
> Dans un discours prononcé devant l’Académie des Sciences de Berlin, la ministre a ainsi déclaré que cette solution, qui, en dernier recours, menace l’usager contrevenant d’une coupure de son accès à Internet, « constituerait une sévère interférence avec la liberté de communication ». Et d’enfoncer le clou en tâclant au passage la France, qui emprunte selon elle « un mauvais chemin ».
