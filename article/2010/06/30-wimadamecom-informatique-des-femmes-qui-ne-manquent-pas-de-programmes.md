---
site: wimadame
title: "Informatique: des femmes qui ne manquent pas de programmes"
author: David Carrel
date: 2010-06-30
href: http://www.wimadame.com/informatique-des-femmes-qui-ne-manquent-pas-de-programmes
tags:
- Le Logiciel Libre
- Sensibilisation
- Promotion
---

> La Cantine accueillait hier soir la Night des Tech Women, qui a rassemblé plusieurs réseaux féminins du web et des nouvelles technologies. Objectif : promouvoir la place des femmes dans cet univers jugé fermé. Wimadame.com y était. Tour d’horizon des participants…
