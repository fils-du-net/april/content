---
site: numerama.com
title: "Hadopi : des logiciels de sécurisation seront bien labellisés"
author: Guillaume Champeau
date: 2010-06-11
href: http://www.numerama.com/magazine/15948-hadopi-des-logiciels-de-securisation-seront-bien-labellises.html
tags:
- Internet
- Logiciels privateurs
- HADOPI
---

> [...] Mais Eric Walter nous assure que nous sommes dans l'erreur, et qu'une liste de fonctionnalités pertinentes sera bien publiée, et que seuls les logiciels qui respectent ce cahier des charges à la lettre seront labellisés, comme le prévoit la loi. En revanche, l'Hadopi ne se prononce pas encore sur le calendrier, qui devrait être précisé lors d'une communication à la fin du mois. C'est également la Commission des droits qui décidera si les logiciels labellisés seront les seuls susceptibles de dédouaner l'abonné suspecté de négligence caractérisée.
