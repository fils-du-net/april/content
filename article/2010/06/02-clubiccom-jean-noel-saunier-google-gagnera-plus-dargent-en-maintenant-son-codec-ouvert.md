---
site: clubic.com
title: "Jean-Noël Saunier : \"Google gagnera plus d'argent en maintenant son codec ouvert\""
author: Antoine Duvauchelle
date: 2010-06-02
href: http://pro.clubic.com/entreprises/actualite-343960-jean-noel-saunier-google-gagnera-argent-codec-ouvert.html
tags:
- Entreprise
- Internet
- Économie
- Interopérabilité
- Video
---

> [...] Ce qui intéresse Google, c'est de démultiplier l'utilisation de la vidéo. C'est là qu'est leur marché. Plus ils démocratisent un codec, moins il coûtera cher. Aujourd'hui, la gestion des licences coûte beaucoup d'argent, or si Google aide à démocratiser la vidéo en ligne, cela générera plus de trafic et plus de recherche, donc plus de publicité... Google gagnera au final plus d'argent en maintenant son codec ouvert. Et n'oublions pas que Youtube est le plus gros site du monde sur le secteur.
