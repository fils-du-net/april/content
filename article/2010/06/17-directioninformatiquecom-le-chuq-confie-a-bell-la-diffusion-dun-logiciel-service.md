---
site: directioninformatique.com
title: "Le CHUQ confie à Bell la diffusion d'un logiciel service"
author: Jean-François Ferland 
date: 2010-06-17
href: http://www.directioninformatique.com/DI/client/fr/DirectionInformatique/Nouvelles.asp?id=58044&cid=79
tags:
- Le Logiciel Libre
- Administration
---

> Le Centre hospitalier universitaire de Québec (CHUQ), au terme d'un appel d'offres, a confié au fournisseur de services de télécommunications Bell un mandat de promotion et d'intégration du logiciel service Cristal-Net auprès des établissements hospitaliers du Québec. [...] Cristal-Net a été développé selon l'approche du logiciel libre par le Centre hospitalier universitaire de Grenoble, avec la collaboration du CHUQ et en partenariat avec une quarantaine d'hôpitaux. Le personnel du CHUQ utilise la solution Cristal-Net depuis près de dix ans. [...] Les centres hospitaliers universitaires de Québec et de Grenoble coopèrent depuis plusieurs années à des projets fondés sur l'utilisation des TIC, par le biais de la Communauté Québec France Développement.
