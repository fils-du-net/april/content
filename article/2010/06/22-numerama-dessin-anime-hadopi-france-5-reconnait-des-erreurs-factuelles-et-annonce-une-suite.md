---
site: Numerama
title: "Dessin animé Hadopi : France 5 reconnait des \"erreurs factuelles\" et annonce une suite"
author: Julien L.
date: 2010-06-22
href: http://www.numerama.com/magazine/16032-dessin-anime-hadopi-france-5-reconnait-des-erreurs-factuelles-et-annonce-une-suite.html
tags:
- Internet
- HADOPI
- Désinformation
- Video
---

> Selon un responsable de France 5, c'est bien la chaine de télévision publique qui est à l'origine du dessin animé Super Crapule contre Super Hadopi. L'idée initiale était de faire une vidéo factuelle sur la loi, mais le résultat final était biaisé par des erreurs flagrantes. Un nouvel épisode est prévu, annoncé comme étant plus proche de la réalité.
