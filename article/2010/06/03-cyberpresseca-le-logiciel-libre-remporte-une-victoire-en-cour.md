---
site: cyberpresse.ca
title: "Le logiciel libre remporte une victoire en cour"
author: Pierre Asselin
date: 2010-06-03
href: http://www.cyberpresse.ca/le-soleil/actualites/science-et-technologie/201006/03/01-4286630-le-logiciel-libre-remporte-une-victoire-en-cour.php?utm_categorieinterne=trafficdrivers&utm_contenuinterne=cyberpresse_B4_en-manchette_2238_section_POS3
tags:
- Le Logiciel Libre
- Administration
- International
---

> La Régie des rentes du Québec (RRQ) a agi illégalement, en février 2008, lorsqu'elle a fait l'acquisition de logiciels Microsoft sans procéder par appel d'offres, conclut le juge Denis Jacques, de la Cour supérieure.
> Dans un jugement étoffé, d'une quarantaine de pages, le magistrat donne entièrement raison à la compagnie Savoir-Faire Linux (SFL), à l'origine de cette poursuite contre la RRQ et le Centre de services partagés du Québec.
