---
site: lemagit.fr
title: "Le ministère de la Défense dit ne pas rouler que pour Microsoft... sans réellement convaincre "
author: Reynald Fléchaux
date: 2010-06-07
href: http://www.lemagit.fr/article/microsoft-windows-licences-office-assemblee-nationale-contrat-defense-sharepoint-opensource-exchange-nuxeo-open-source-outlook/6499/1/le-ministere-defense-dit-pas-rouler-que-pour-microsoft-sans-reellement-convaincre
tags:
- Logiciels privateurs
- Administration
---

> Interpellé en avril par le député UMP Bernard Carayon, au sujet d'un contrat-cadre signé avec Microsoft, le ministère de la Défense réplique. Et assure que les technologies du premier éditeur mondial seront mises en balance avec des solutions Open Source. Insuffisant toutefois pour dissiper le climat de suspicion qui entoure ce contrat.
