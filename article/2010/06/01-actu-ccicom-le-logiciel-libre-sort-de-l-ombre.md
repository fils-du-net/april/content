---
site: "actu-cci.com"
title: "Le logiciel libre sort de l’ombre"
author: Érick Haehnsen
date: 2010-06-01
href: http://www.actu-cci.com/article/3389/
tags:
- Le Logiciel Libre
- Entreprise
---

> Affichant une santé économique insolente, le logiciel libre entre dans sa phase de maturité. En ouvrant son code-source, il n’en est que plus fort. Partant de ce principe, il suscite la créativité des développeurs dans tous les secteurs de l’informatique.
