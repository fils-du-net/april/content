---
site: numerama.com
title: "Eric Walter (Hadopi) trouve \"un peu hypocrite\" le débat sur les logiciels de sécurisation"
author: Guillaume Champeau
date: 2010-06-03
href: http://www.numerama.com/magazine/15869-eric-walter-hadopi-trouve-34un-peu-hypocrite34-le-debat-sur-les-logiciels-de-securisation.html
tags:
- Logiciels privateurs
- HADOPI
---

> [...] Les moyens de sécurisation ne sont pas une mince affaire. La loi elle-même prévoit que l'Hadopi doit vérifier "leur efficacité". Le professeur Riguidel, qui travaille à l'élaboration des fonctionnalités pertinentes que les moyens de sécurisation devront présenter, nous confiait lui-même que c'était là "l'une des missions les plus difficiles" qu'il ait eu à accomplir. Il a pourtant travaillé sur des projets de la plus haute importance, classés secrets défense.
