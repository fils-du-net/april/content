---
site: pcinpact.com
title: "Google se débarrasse de toutes ses machines sous Windows"
author: Vincent Hermann
date: 2010-06-01
href: http://www.pcinpact.com/actu/news/57313-google-windows-arret-choix-mac-osx-linux.htm
tags:
- Entreprise
- Logiciels privateurs
---

> [...] Google a en effet pris la décision de bannir Windows de son parc informatique, autant que faire se peut. Ce choix radical intervient après une réflexion déclenchée par les attaques en ligne du mois de janvier et qui a provoqué la fermeture du moteur de recherche en Chine. Ces attaques visaient une faille d’Internet Explorer 6 sous Windows XP, suffisante pour permettre aux pirates de récupérer certaines données.
