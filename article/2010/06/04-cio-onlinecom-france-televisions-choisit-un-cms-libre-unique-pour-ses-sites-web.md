---
site: "cio-online.com"
title: "France Télévisions choisit un CMS libre unique pour ses sites web"
author: Bertrand Lemaire
date: 2010-06-04
href: http://www.cio-online.com/actualites/lire-france-televisions-choisit-un-cms-libre-unique-pour-ses-sites-web-2970.html
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] Le site Sport.FranceTV.fr a été le premier à être refondu sur une nouvelle base technique destinée à être généralisée sur l'ensemble des sites de France Télévision. L'intégration a été opérée par Alterway sur une base Drupal pour un montant non-communiqué. Ce site génère 300 000 visiteurs uniques et 500 000 pages vues par jour, hors évènements tels que Rolland Garros (des pics de 15 millions de pages vues par jour pouvant être alors atteints).
