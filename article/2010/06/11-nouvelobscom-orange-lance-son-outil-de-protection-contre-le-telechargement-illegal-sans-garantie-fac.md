---
site: nouvelobs.com
title: "Orange lance son outil de protection contre le téléchargement illégal sans garantie face à Hadopi"
author: Guénaël Pépin
date: 2010-06-11
href: http://hightech.nouvelobs.com/actualites/depeche/20100612.ZDN2299/orange-lance-son-outil-de-protection-contre-le-telechargement-illegal-sans-garantie-face-a-hadopi.html
tags:
- Internet
- Logiciels privateurs
- HADOPI
---

> Le premier FAI de France met à disposition de ses abonnés un logiciel de filtrage des logiciels peer-to-peer en réponse à la loi Hadopi. Facturé 2 euros par mois, il ne garantit en fait aucune protection.
