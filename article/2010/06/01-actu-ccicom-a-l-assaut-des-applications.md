---
site: "actu-cci.com"
title: "À l’assaut des applications"
author: Érick Haehnsen
date: 2010-06-01
href: http://www.actu-cci.com/article/3390/
tags:
- Le Logiciel Libre
- Entreprise
- Administration
---

> Indépendance technologique, interopérabilité, maîtrise des coûts des projets informatiques… Depuis quatre ans, le gouvernement brésilien mise sur le logiciel libre. « C’est un droit social », explique Corinto Meffe, responsable des innovations technologiques au ministère de la Planification qui a lancé un portail Internet, SoftwarePublico.gov.br, sur lequel des contributeurs bénévoles ont déposé une trentaine de logiciels à télécharger gratuitement. Ces derniers intéressent aussi bien les administrations publiques que les entreprises privées ou les citoyens.
