---
site: slate.fr
title: "L'open source trop ouvert aux hackers?"
author: La rédaction
date: 2010-06-07
href: http://www.slate.fr/story/22611/open-source-hackers
tags:
- Le Logiciel Libre
- Internet
- Désinformation
---

> Le logiciel libre représente-t-il l'avenir du web? [...] Problème, cette transition pourrait apporter son lot de dangers. A en croire le Technology Review du MIT,  qui cite de récentes analyses, l'open source pourrait être «une porte ouverte pour les hackers» et multiplier les attaques.
