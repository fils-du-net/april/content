---
site: zdnet.fr
title: "Propriété d’Unix : victoire définitive de Novell sur SCO ?"
author: Guénaël Pépin
date: 2010-06-14
href: http://www.zdnet.fr/actualites/propriete-d-unix-victoire-definitive-de-novell-sur-sco-39752387.htm
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
---

> Juridique - Un jugement rendu jeudi dernier par le tribunal de l’Utah confirme la propriété de Novell sur Unix et signe, encore une fois, la possible fin de cette saga judiciaire.
> [...] Je suis très fier de ce dénouement et du travail que Novell a fourni pour s'assurer que Linux reste libre et ouvert » déclare ainsi le pdg de Novell, Ron Hovsepian.
