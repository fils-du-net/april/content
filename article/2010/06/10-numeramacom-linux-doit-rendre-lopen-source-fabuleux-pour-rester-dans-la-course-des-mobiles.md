---
site: numerama.com
title: "Linux doit rendre l'open-source \"fabuleux\" pour rester dans la course des mobiles"
author: Julien L.
date: 2010-06-10
href: http://www.numerama.com/magazine/15934-linux-doit-rendre-l-open-source-34fabuleux34-pour-rester-dans-la-course-des-mobiles.html
tags:
- Le Logiciel Libre
- Économie
---

> Pourtant, si ces avancées sont à saluer, il reste à convaincre les utilisateurs eux-mêmes afin que l'open-source continue de prendre des parts de marché. Et pour Jim Zemlin, directeur exécutif de la Fondation Linux, il n'y a qu'une seule façon pour y parvenir. [...] "Nous sommes en train de nous diriger vers un monde high-tech où Apple sera d'un côté tandis que tous les autres seront potentiellement de l'autre. Linux doit concurrencer plus efficacement Steve Jobs et la magie d'Apple.
