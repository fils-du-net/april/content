---
site: MyCoop
title: "Quand l'économie sociale rencontre le logiciel libre"
author: Laure Capblancq
date: 2010-06-25
href: http://www.mycoop.coop/sinformer/entreprendre-autrement/quand-leconomie-sociale-rencontre-le-logiciel-libre/
tags:
- Le Logiciel Libre
- Économie
- April
- Sensibilisation
- Associations
- Licenses
- Philosophie GNU
- Promotion
---

> Le logiciel libre, créé dans les années 1980, est l’héritier de l’esprit des pionniers de l’informatique qui formaient des groupes d’utilisateurs pour mutualiser leur expérience et faire évoluer les outils. Liberté, mutualisation, coopération : ces valeurs sont aussi celles de l’économie sociale qui aujourd’hui se rapproche du logiciel libre.
