---
site: Numerama
title: "La Sacem américaine sollicite des dons pour combattre le copyleft"
author: Guillaume Champeau
date: 2010-06-24
href: http://www.numerama.com/magazine/16065-la-sacem-americaine-sollicite-des-dons-pour-combattre-le-copyleft.html
tags:
- Désinformation
- Droit d'auteur
- Contenus libres
- International
---

> C'est une démarche qui serait vue en France comme un appel ouvert à la corruption, mais qui paraît naturel dans le paysage démocratique américain où les lobbys agissent avec plus de transparence, ou moins d'hypocrisie. L'ASCAP, la Sacem américaine, demande à ses membres de l'aider à financer des campagnes électorales pour que les vues du "copyright" soient toujours défendues contre celles, croissantes, du copyleft et des licences libres.
