---
site: facil
title: "Le Parti québécois mettra le libre à son programme dès l'an prochain."
author: acotte
date: 2010-06-29
href: http://facil.qc.ca/enbref/le-parti-qu%C3%A9b%C3%A9cois-mettra-le-libre-%C3%A0-son-programme-d%C3%A8s-lan-prochain
tags:
- Le Logiciel Libre
- Promotion
- International
---

> [...] Ouvrira les appels d’offres informatiques aux logiciels libres et mettra en place dès son arrivée un groupe conseil, libre d’intérêts particuliers, pour le guider sur le virage à prendre. Cette simple mesure pourrait faire économiser des dizaines de millions au gouvernement, en plus d’offrir plus de flexibilité. Le mot d’ordre : « modernité ».
