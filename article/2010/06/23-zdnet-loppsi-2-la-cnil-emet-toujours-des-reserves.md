---
site: ZDNet
title: "Loppsi 2 : la CNIL émet toujours des réserves"
author: Christophe Auffray
date: 2010-06-23
href: http://www.zdnet.fr/actualites/loppsi-2-la-cnil-emet-toujours-des-reserves-39752653.htm
tags:
- Internet
- Institutions
---

> Malgré les efforts consentis par le gouvernement pour rendre son texte plus respectueux de la vie privée des citoyens et des libertés, des progrès restent encore à accomplir selon la CNIL. Fichiers de police, captation de données sur les accès publics à Internet, vidéosurveillance posent problème.
