---
site: zdnet.fr
title: "Ma petite entreprise du logiciel libre ne connaît pas la crise"
author: Thierry Noisette
date: 2010-06-02
href: http://www.zdnet.fr/blogs/l-esprit-libre/ma-petite-entreprise-du-logiciel-libre-ne-connait-pas-la-crise-39752108.htm
tags:
- Le Logiciel Libre
- Entreprise
- Informatique en nuage
---

> Jeunes (la moitié ont été créées après 2005), petites et dynamiques: la centaine d'entreprises de logiciels libres sondées par le CNLL ont été peu éprouvées par la crise en 2009. 37% appartiennent à un pôle de compétitivité.
> Elles sont récentes, ont des petits effectifs et des chiffres d'affaires sous le million d'euros (75%), mais ont à 73% connu une croissance l'an dernier et participent à des projets dans le Libre: tel est le portrait-robot qui ressort de l'étude que publie le CNLL (Conseil national du logiciel libre, créé en février dernier).
