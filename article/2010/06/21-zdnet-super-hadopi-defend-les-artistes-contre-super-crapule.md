---
site: ZDNet
title: "Super Hadopi défend les artistes contre Super Crapule"
author: Guénaël Pépin
date: 2010-06-21
href: http://www.zdnet.fr/actualites/super-hadopi-defend-les-artistes-contre-super-crapule-39752578.htm#xtor=RSS-1
tags:
- HADOPI
- Video
---

> Une nouvelle vidéo éducative a fait son entrée sur le site Curiosphere.tv. Super Hadopi y défend les artistes face à l’insatiable et sans cœur Super Crapule. Mais qui se cache derrière cette vidéo ?
