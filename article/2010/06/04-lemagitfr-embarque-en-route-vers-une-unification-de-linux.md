---
site: lemagit.fr
title: "Embarqué : en route vers une unification de Linux "
author: Cyrille Chausson
date: 2010-06-04
href: http://www.lemagit.fr/article/mobilite-linux-opensource-embarque/6492/1/embarque-route-vers-une-unification-linux/
tags:
- Le Logiciel Libre
- Entreprise
---

> Le calendrier est chargé, depuis quelques mois, autour de Linux dans l’embarqué. Après le ralliement des automaticiens Linux à la Linux Foundation, c’est au tour des fondeurs reposant sur ARM de s’allier pour favoriser l’émergence de solutions mobiles et embarquées Open Source. Une montée en puissance du libre qui pioche dans ses vertus d’unité pour percer le marché.
