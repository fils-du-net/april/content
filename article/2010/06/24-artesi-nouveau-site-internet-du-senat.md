---
site: Artesi
title: "Nouveau site Internet du Sénat "
author: La rédaction
date: 2010-06-24
href: http://www.artesi.artesi-idf.com/public/article/nouveau-site-internet-du-senat.html?id=21371
tags:
- Le Logiciel Libre
- Internet
- Administration
---

> La nouvelle version de www.senat.fr, mise en ligne aujourd’hui, s’appuie sur un logiciel libre de gestion de contenus. Une véritable prouesse technique pour un site de cette envergure (600.000 pages, 20 millions de visites par an).
