---
site: owni.fr
title: "Paris: un pas vers l’opendata"
author: Caroline Goulard
date: 2010-06-08
href: http://owni.fr/2010/06/08/paris-un-pas-vers-l%E2%80%99opendata/
tags:
- Administration
- Partage du savoir
- Licenses
---

> Ce 8 juin 2010, a été adoptée une résolution sur la diffusion des données publiques, lors du Conseil Municipal de la ville de Paris.
> [...] Les échanges qui ont précédé cette adoption ont été nourris. Ils ont porté sur les atouts d’une politique d’ouverture des données : apporter plus de transparence, renforcer la démocratie, stimuler l’innovation, générer des initiatives citoyennes.
