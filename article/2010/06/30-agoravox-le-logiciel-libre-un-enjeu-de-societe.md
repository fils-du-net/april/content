---
site: agoravox
title: "Le logiciel libre, un enjeu de société"
author: François Poulain
date: 2010-06-30
href: http://www.agoravox.fr/actualites/societe/article/le-logiciel-libre-un-enjeu-de-77668
tags:
- Le Logiciel Libre
---

> ACTA, Hadopi, Dadvsi, Loppsi, ... Internet et le logiciel libre sont souvent au centre de ses enjeux. Mais de quels enjeux s’agit-il, au fait ? Et de quelles « libertés » est-il question ici ? Quelques clés pour comprendre.
