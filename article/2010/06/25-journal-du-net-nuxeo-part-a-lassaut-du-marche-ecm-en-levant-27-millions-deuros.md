---
site: Journal du Net
title: "Nuxeo part à l'assaut du marché ECM en levant 2,7 millions d'euros"
author: Dominique FILIPPONE
date: 2010-06-25
href: http://www.journaldunet.com/solutions/intranet-extranet/levee-de-fonds-nuxeo.shtml
tags:
- Le Logiciel Libre
- Entreprise
---

> Pour soutenir son programme de développement et de partenariat, l'éditeur Open Source en gestion de contenu se donne les moyens de son ambition. Cela suffira-t-il pour venir chatouiller les géants du marché ?
