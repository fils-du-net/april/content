---
site: écrans
title: "L’épouvantable menace de la culture libre"
author: Grégory Rozieres
date: 2010-06-25
href: http://www.ecrans.fr/La-Sacem-americaine-veut-se-faire,10236.html
tags:
- Partage du savoir
- Droit d'auteur
- Contenus libres
- International
---

> L’ASCAP part en guerre. L’équivalent américain de notre Sacem (American Society of Composers, Authors and Publishers) souhaite faire la peau aux méchants défenseurs du copyleft (opposé du copyright). Et le nerf de la guerre, c’est l’argent. L’institution a donc envoyé un mail à ses membres leur demandant de réaliser des donations pour aider l’ASCAP a se battre contre les vils et perfides pourfendeurs du copyright. Originellement publié par Mike Rugnetta sur Twitter (ici  et là) après avoir reçu le mail, le site BoingBoing en a fait écho.
