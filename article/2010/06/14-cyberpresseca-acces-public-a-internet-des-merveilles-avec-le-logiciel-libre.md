---
site: cyberpresse.ca
title: "Accès public à Internet: des merveilles avec le logiciel libre"
author: Pierre Asselin
date: 2010-06-14
href: http://www.cyberpresse.ca/le-soleil/actualites/science-et-technologie/201006/13/01-4289614-acces-public-a-internet-des-merveilles-avec-le-logiciel-libre.php
tags:
- Le Logiciel Libre
- Internet
---

> (Québec) Pour gérer un réseau de plus en plus vaste et complexe, ZAP Québec et les autres organismes communautaires du sans-fil ont besoin d'outils efficaces, qui ne leur coûtent pas une fortune. Et grâce au logiciel libre, ils font des merveilles.
> [...] Tous les groupes ont mis des sous en commun, et beaucoup de bénévolat. On espère arriver à l'automne avec ce remplaçant de WIFIDog, qui est arrivé à la fin de sa vie utile. «On a décidé de créer quelque chose de neuf, et les autres groupes ailleurs dans le monde qui nous regardent vont pouvoir se servir de ce qu'on a créé, et peut-être de l'améliorer. C'est la beauté du logiciel libre.»
