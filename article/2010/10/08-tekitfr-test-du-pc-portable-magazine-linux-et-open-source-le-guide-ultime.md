---
site: tekit.fr
title: "Test du PC portable Magazine : Linux et Open Source, le guide ultime"
author: La rédaction
date: 2010-10-08
href: http://www.tekit.fr/test-microportable_5804-magazine-linux-et-open-source-le-guide-ultime.html
tags:
- Le Logiciel Libre
- Internet
- Informatique en nuage
---

> Un nouveau numéro hors-série de Micro Portable Magazine est disponible en kiosque. Baptisé "Linux et Open Source, le guide ultime", il contient plus de 200 pages de tutoriels pour maitriser cet écosystème.
