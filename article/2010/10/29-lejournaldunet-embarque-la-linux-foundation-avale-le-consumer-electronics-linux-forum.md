---
site: LeJournalduNet
title: "Embarqué : la Linux Foundation avale le Consumer Electronics Linux Forum"
author: La rédaction
date: 2010-10-29
href: http://www.journaldunet.com/solutions/breve/49199/embarque---la-linux-foundation-avale-le-consumer-electronics-linux-forum.shtml
tags:
- Le Logiciel Libre
- Entreprise
- Associations
---

> Le Consumer Electronics Linux Forum, consortium spécialisé de Linux dans l'embarqué, va intégrer la Linux Foundation
