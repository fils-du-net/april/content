---
site: L'INFORMATICIEN
title: "VLC : un passage éclair sur les terminaux d’Apple"
author: Johanna Godet
date: 2010-10-26
href: http://www.linformaticien.com/Actualit%C3%A9s/tabid/58/newsid496/9340/vlc-un-passage-eclair-sur-les-terminaux-d-apple/Default.aspx
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Informatique-deloyale
- Licenses
---

> A peine sortie sur les plateformes iOS, l’application VLC Media Player va être supprimée de l’AppStore. C’est du moins ce que souhaite l’éditeur qui attaque Apple pour violation de copyright.
