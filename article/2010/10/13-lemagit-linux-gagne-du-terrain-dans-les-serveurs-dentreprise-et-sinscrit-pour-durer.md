---
site: LeMagIT
title: "Linux gagne du terrain dans les serveurs d'entreprise et s'inscrit pour durer"
author: Cyrille Chausson
date: 2010-10-13
href: http://www.lemagit.fr/article/serveurs-windows-linux-unix-entreprises-open-source/7290/1/linux-gagne-terrain-dans-les-serveurs-entreprise-inscrit-pour-durer/
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
---

> Selon une étude de la Linux Foundation, Linux poursuit sa croissance sur les serveurs d’entreprises et s’inscrit désormais dans la durée dans les stratégies des organisations. L’OS Open Source prend une part de plus en plus forte dans les système critiques, avec des entreprises qui y voient un système techniquement abouti, mieux pris en compte par les DSI. Tout cela en marchant sur les platebandes de Windows et d’Unix.
