---
site: Silicon.fr
title: "Oracle coupe l’herbe sous le pied des projets LibreOffice et Harmony"
author: David Feugey
date: 2010-10-18
href: http://www.silicon.fr/oracle-coupe-l%E2%80%99herbe-sous-le-pied-des-projets-libreoffice-et-harmony-42524.html
tags:
- Le Logiciel Libre
- Entreprise
- Licenses
- International
---

> Attaque frontale d’Oracle envers la communauté des logiciels libres et de Google : LibreOffice et Harmony sont aujourd’hui en ligne de mire de la firme.
