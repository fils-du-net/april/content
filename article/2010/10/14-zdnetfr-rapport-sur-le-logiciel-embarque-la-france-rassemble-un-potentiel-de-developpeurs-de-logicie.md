---
site: ZDNet.fr
title: "Rapport sur le logiciel embarqué: \"la France rassemble un potentiel de développeurs de logiciel libre remarquable\""
author: Thierry Noisette
date: 2010-10-14
href: http://www.zdnet.fr/blogs/l-esprit-libre/rapport-sur-le-logiciel-embarque-la-france-rassemble-un-potentiel-de-developpeurs-de-logiciel-libre-remarquable-39755409.htm
tags:
- Le Logiciel Libre
- Entreprise
- Institutions
- Licenses
---

> Le rapport «Briques génériques du logiciel embarqué», dirigé par Dominique Potier, vient d'être remis au gouvernement. Il souligne que les enjeux généraux du logiciel libre valent pour l'embarqué: pérennité des composants, droits équilibrés des contributeurs...
