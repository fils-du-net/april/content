---
site: Blog LeMonde.fr
title: "Journalistes : protégez vos sources !"
author: jean marc manach
date: 2010-10-27
href: http://bugbrother.blog.lemonde.fr/2010/10/27/journalistes-protegez-vos-sources/
tags:
- Le Logiciel Libre
- Institutions
- Sensibilisation
---

> C’est fou ce que les ordinateurs et les téléphones portables des journalistes qui enquêtent sur le scandale Woerth-Bettencourt semblent intéresser voleurs, politiques et magistrats, ces derniers temps.
