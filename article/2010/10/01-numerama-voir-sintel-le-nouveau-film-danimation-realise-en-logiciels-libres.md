---
site: Numerama
title: "A voir : Sintel, le nouveau film d'animation réalisé en logiciels libres"
author: Guillaume Champeau
date: 2010-10-01
href: http://www.numerama.com/magazine/16949-a-voir-sintel-le-nouveau-film-d-animation-realise-en-logiciels-libres.html
tags:
- Le Logiciel Libre
- Internet
- Associations
- Contenus libres
---

> Pré-financé en grande partie par les internautes, Sintel est le troisième court-métrage réalisé par la Fondation Blender pour démontrer la puissance des logiciels libres dans la création de films d'animation. A voir et à faire connaître.
