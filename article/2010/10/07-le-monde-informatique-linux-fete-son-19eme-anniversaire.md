---
site: Le Monde Informatique
title: "Linux a fêté son 19ème anniversaire"
author: Bertrand Lemaire
date: 2010-10-07
href: http://www.lemondeinformatique.fr/actualites/lire-linux-a-fete-son-19eme-anniversaire-31858.html
tags:
- Le Logiciel Libre
---

> Tout a commencé le 5 octobre 1991. Le système d'exploitation, largement diffusé, est devenu emblématique du logiciel libre.
