---
site: marsactu
title: "La mairie de Marseille zappe Linux pour Windows 7"
author: Julien VINZENT
date: 2010-10-07
href: http://www.marsactu.fr/2010/10/07/la-mairie-de-marseille-zappe-linux-pour-windows-7/
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Administration
- Désinformation
---

> Dans le monde du libre, la mairie de Marseille avait créé une petite sensation en décidant en 2008 de basculer tous ses services ...
