---
site: innovation le journal
title: "Cartographier les pollutions en temps réel"
author: Elsa Bellanger
date: 2010-10-18
href: http://www.innovationlejournal.com/spip.php?article6121
tags:
- Le Logiciel Libre
- Internet
- Institutions
- Innovation
- Licenses
- Europe
---

> Des chercheurs européens ont élaboré un logiciel libre, INTAMA, pour cartographier, en temps réel, les pollutions de l’air, du sol et de l’eau, mais aussi déterminer l’origine des pollutions et leurs destinations. Conçu comme un outil d’aide pour les pouvoirs publics, INTAMA est également porteur d’autres applications.
