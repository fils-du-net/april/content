---
site: macgenration
title: "Interview : dans les coulisses d'OOo4Kids"
author: Christophe Laporte
date: 2010-10-18
href: http://www.macgeneration.com/news/voir/172481/interview-dans-les-coulisses-d-ooo4kids
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- Éducation
- Innovation
- Licenses
- Video
---

> Le monde du libre est en pleine ébullition. Il y a quelques semaines, EducOO.org finalisait OOo4Kids (OpenOffice.org pour Kids). Comme son nom l’indique, ce petit frère d’OpenOffice.org a spécialement été conçu pour le monde de l’éducation afin que les plus jeunes puissent se familiariser avec les outils de base (traitement de textes, tableur…).
