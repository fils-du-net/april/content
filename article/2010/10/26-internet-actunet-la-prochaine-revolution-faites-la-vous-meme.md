---
site: internet ACTU.net
title: "La prochaine révolution? Faites-la vous même!"
author: Jean-Marc Manach
date: 2010-10-26
href: http://www.internetactu.net/2010/10/26/faites-le-vous-meme-mais-quoi-mais-tout/
tags:
- Le Logiciel Libre
- Matériel libre
- Innovation
---

> En janvier 2010, Chris Anderson, rédacteur en chef de Wired et auteur de La longue traîne, tentait ainsi de résumer la révolution en cours du Do It Yourself  (DIY, Faites-le vous même, en français). Après avoir considérablement contribué à développer, et démocratiser, ce que l’on appelait au siècle dernier la “micro-informatique“, puis l’internet, hackers et bidouilleurs s’attèlent aujourd’hui à la fabrication et au développement de nouveaux objets, “libres“.
