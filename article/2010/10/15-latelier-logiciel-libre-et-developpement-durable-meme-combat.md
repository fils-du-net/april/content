---
site: L'ATELIER
title: "Logiciel libre et développement durable : même combat ? "
author: Frédéric Bordage
date: 2010-10-15
href: http://www.atelier.fr/infrastructures/4/15102010/green-it-developpement-durable-open-source-logiciel-libre-frederic-bordage-40364-.html
tags:
- Le Logiciel Libre
- Institutions
- Droit d'auteur
- Innovation
- Licenses
- Philosophie GNU
---

> Les principales caractéristiques du modèle open source sont particulièrement bien adaptées à la profondeur et à l’urgence des enjeux du développement durable. Démonstration.
