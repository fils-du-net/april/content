---
site: écrans
title: "Microsoft n'aime pas du tout OpenOffice.org"
author: François Arias
date: 2010-10-14
href: http://www.ecrans.fr/Microsoft-n-aime-pas-du-tout,11084.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Désinformation
---

> On savait que Microsoft n’était pas fan des solutions open source, en particulier lorsqu’elles concurrençaient ses propres produits. Mais avec cette vidéo vous expliquant pourquoi OpenOffice.org c’est le mal, la firme de Redmond s’est surpassée.
