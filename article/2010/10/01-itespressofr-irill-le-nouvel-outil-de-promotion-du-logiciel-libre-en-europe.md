---
site: ITespresso.fr
title: "IRILL : le nouvel outil de promotion du logiciel libre en Europe"
author: Anne Confolant
date: 2010-10-01
href: http://www.itespresso.fr/irill-nouvel-outil-promotion-logiciel-libre-en-europe-36890.html
tags:
- Le Logiciel Libre
- Entreprise
- Innovation
---

> L’Initiative pour la Recherche et l’Innovation sur le Logiciel Libre (IRILL) est destinée à promouvoir le logiciel libre, via notamment le développement et le suivi de projets de recherche.
