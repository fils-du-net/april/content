---
site: Génération nouvelles technologies
title: "Java libre et OpenJDK : IBM rejoint Oracle"
author: Jérôme G.
date: 2010-10-12
href: http://www.generation-nt.com/openjdk-java-open-source-oracle-ibm-actualite-1098681.html
tags:
- Le Logiciel Libre
- Entreprise
---

> Dans le cadre du développement de la version open source de Java, Oracle et IBM ont décidé de collaborer.
