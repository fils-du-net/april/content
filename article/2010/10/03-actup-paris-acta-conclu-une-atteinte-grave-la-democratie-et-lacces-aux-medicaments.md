---
site: ACTUP PARIS
title: "ACTA conclu : une atteinte grave à la démocratie et à l'accès aux médicaments"
author: La rédaction
date: 2010-10-03
href: http://www.actupparis.org/spip.php?article4276
tags:
- Le Logiciel Libre
- Institutions
- Droit d'auteur
- Europe
- International
- ACTA
---

> L'annonce de la validation d'ACTA est un camouflet pour la démocratie et une mise en danger grave des malades des pays pauvres.
