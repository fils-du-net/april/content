---
site: PC INpact
title: "L'Hadopi sèche sur sa saisine éventuelle par le monde du libre "
author: Marc Rees
date: 2010-10-08
href: http://www.pcinpact.com/actu/news/59764-hadopi-creation-internet-logiciel-libre.htm
tags:
- Le Logiciel Libre
- Internet
- HADOPI
- Droit d'auteur
---

> Toujours lors du chat organisé par le Parti Pirate, l'Hadopi a été incapable de répondre à une question a priori simple : « La hadopi sert normalement à protéger les droits d'auteurs sur internet, que fait-elle pour les personnes physiques ou morales violant les licences dites libres ? ».
