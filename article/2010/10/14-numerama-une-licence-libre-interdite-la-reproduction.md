---
site: Numerama
title: "Une licence libre interdite à la reproduction"
author: Guillaume Champeau
date: 2010-10-14
href: http://www.numerama.com/magazine/17062-une-licence-libre-interdite-a-la-reproduction.html
tags:
- Le Logiciel Libre
- Droit d'auteur
- Licenses
---

> La poésie permet tout, même le plus absurde. Le développeur Gervase Markham a écrit une version poétique de la licence libre BSD, dont il refuse la reproduction.
