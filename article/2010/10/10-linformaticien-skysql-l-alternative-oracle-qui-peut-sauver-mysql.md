---
site: L'INFORMATICIEN
title: "SKYSQL : L’alternative à Oracle qui peut sauver MySQL !"
author: Stéphane Larcher
date: 2010-10-10
href: http://www.linformaticien.com/Actualit%C3%A9s/tabid/58/newsid496/9187/skysql-l-alternative-a-oracle-qui-peut-sauver-mysql/Default.aspx
tags:
- Le Logiciel Libre
- Entreprise
---

> En gestation depuis Juillet 2010, la société SkySQL, composée à 100% d’anciens de MySQL, démarrera officiellement ses activités mardi 12 octobre dans plusieurs pays dont la France. Avec pour objectif d’offrir une véritable alternative à Oracle et assurer un avenir à MySQL et à ses utilisateurs.
