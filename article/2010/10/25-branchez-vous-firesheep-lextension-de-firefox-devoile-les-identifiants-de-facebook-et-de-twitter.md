---
site: "BRANCHEZ-VOUS!"
title: "Firesheep: l'extension de Firefox dévoile les identifiants de Facebook et de Twitter"
author: Aude Boivin Filion
date: 2010-10-25
href: http://techno.branchez-vous.com/actualite/2010/10/firesheep-extension-firefox-devoile-identifiant-site-web.html
tags:
- Le Logiciel Libre
- Internet
---

> Un plugiciel présenté par un développeur vient de fouetter l'industrie de la sécurité informatique de plein fouet. En créant Firesheep, l'intention du développeur était de faire ressortir les lacunes des sites et des services Web en matière de sécurité et de confidentialité.
