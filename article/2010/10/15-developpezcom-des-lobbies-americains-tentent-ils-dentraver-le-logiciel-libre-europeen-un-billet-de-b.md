---
site: Developpez.com
title: "Des lobbies américains tentent-ils d'entraver le logiciel libre européen ? Un billet de blog et un rapport relancent le débat"
author: La rédaction
date: 2010-10-15
href: http://www.developpez.com/actu/22203/Des-lobbies-americains-tentent-ils-d-entraver-le-logiciel-libre-europeen-Un-billet-de-blog-et-un-rapport-relancent-le-debat
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Désinformation
- Licenses
- Europe
- International
---

> Wikileaks, le site on ne peut plus polémique spécialisé dans la publication des rapports confidentiels, avait mis en ligne il y a quelques mois un document dévoilant des plans de lobbying destinés, si le document est un vrai, à freiner la progression du logiciel libre en Europe.
