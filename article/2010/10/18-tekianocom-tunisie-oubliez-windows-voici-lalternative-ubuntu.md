---
site: tekiano.com
title: "Tunisie : Oubliez Windows, voici l'alternative Ubuntu"
author: Samy Ben Naceur
date: 2010-10-18
href: http://www.tekiano.com/tek/soft/3-10-2779/tunisie-oubliez-windows-voici-l-alternative-ubuntu.html
tags:
- Le Logiciel Libre
- Associations
- International
---

> La version finale d'Ubuntu 10.10 est déjà disponible depuis le 10 Octobre en téléchargement. Et en Tunisie ? Comment se porte la communauté liée au système d'exploitation open source le plus utilisé au monde ? Petit tour d'horizon.
