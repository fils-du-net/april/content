---
site: Macworld.fr
title: "Google répond à Steve Jobs"
author: Tanguy Andrillon
date: 2010-10-19
href: http://www.macworld.fr/2010/10/19/divers/google-repond-steve-jobs/507451/
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
---

> Le responsable du développement d'Android n'a visiblement pas apprécié la définition de système ouvert proposée par Steve Jobs.
