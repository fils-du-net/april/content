---
site: Programmez.com
title: "Forum PHP 2010 : l'AFUP reçoit en exclusivité SkySQL Ab et Monty Program Ab"
author: Frédéric Mazué
date: 2010-10-22
href: http://www.programmez.com/actualites.php?id_actu=8371
tags:
- Le Logiciel Libre
- Internet
- Associations
---

> L'AFUP, Association Française des Utilisateurs de PHP, rassemble cette année encore un panel prestigieux de speakers internationaux, pour la 10ème édition du Forum PHP et les 15 ans de PHP. Unique événement professionnel en France consacré à la plate-forme PHP et aux technologies Web associées, le Forum PHP est un carrefour de rencontres et d'échanges de l'ensemble des acteurs liés à PHP.
