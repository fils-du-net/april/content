---
site: cyberpresse.ca
title: "Québec prêt à ouvrir une porte au logiciel libre"
author: Pierre Asselin
date: 2010-10-15
href: http://www.cyberpresse.ca/le-soleil/actualites/politique/201010/15/01-4332810-quebec-pret-a-ouvrir-une-porte-au-logiciel-libre.php
tags:
- Le Logiciel Libre
- Institutions
- International
---

> (Québec) La présidente du Conseil du trésor, Michelle Courchesne, prévoit faire une place au logiciel libre dans la politique qu'elle doit bientôt déposer sur la gouvernance et les ressources informationnelles. Déjà, des idées commencent à circuler en dehors du gouvernement sur des moyens à prendre pour structurer le développement de ce modèle.
