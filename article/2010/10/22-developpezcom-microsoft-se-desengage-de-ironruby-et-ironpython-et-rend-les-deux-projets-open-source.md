---
site: Developpez.com
title: "Microsoft se désengage de IronRuby et IronPython et rend les deux projets open-source, le créateur d'IronPyton rejoint Google"
author: La rédaction
date: 2010-10-22
href: http://www.developpez.com/actu/22464/Microsoft-se-desengage-de-IronRuby-et-IronPython-et-rend-les-deux-projets-open-source-le-createur-d-IronPyton-rejoint-Google
tags:
- Le Logiciel Libre
- Entreprise
- Licenses
---

> Microsoft se désengage de IronRuby et IronPython
> Et confie leur support à la communauté, le créateur d'IronPyton rejoint Google
