---
site: PUBLI NEWS
title: "Les administrations françaises affirment l'open source comme un stimulant de l'innovation et envisagent de l'introduire en mode cloud"
author: La rédaction
date: 2010-10-07
href: http://www.publi-news.fr/data/07102010/07102010-092011.html
tags:
- Le Logiciel Libre
- Administration
- Innovation
- Informatique en nuage
---

> MARKESS International, société d'études et de conseil basée à Paris et à Washington, D.C., spécialisée dans l'analyse de la modernisation et de la transformation des entreprises et administrations avec les technologies de l'information, annonce la parution de sa dernière étude dédiée à l'open source et au secteur public intitulée " L'Open Source dans le Secteur Public - Facteur d'Innovation et Nouvelles Approches avec le Cloud Computing ".
