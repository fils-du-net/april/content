---
site: LeMagIT
title: "Open Source : le difficile pari de la contribution"
author: Cyrille Chausson
date: 2010-10-06
href: http://www.lemagit.fr/article/developpement-communaute-entreprises-opensource/7244/1/open-source-difficile-pari-contribution/
tags:
- Le Logiciel Libre
- Entreprise
- Administration
- Associations
---

> Poumons de l’Open Source, les contributions se retrouvent aujourd’hui au centre d’un modèle en pleine mutation, du fait notamment de la poussée des entreprises. Projets en progression, mais contributions à la baisse, l’occasion pour LeMagIT de cartographier l’état des contributions et de leur mise en place dans les organisations.
