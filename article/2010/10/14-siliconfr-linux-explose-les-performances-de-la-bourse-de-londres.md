---
site: Silicon.fr
title: "Linux explose les performances de la Bourse de Londres"
author: David Feugey
date: 2010-10-14
href: http://www.silicon.fr/linux-explose-les-performances-de-trading-de-la-bourse-de-londres-42489.html
tags:
- Le Logiciel Libre
- Entreprise
- International
---

> Le LSE vient de terminer la phase de test à grande échelle de sa plate-forme Linux. Les résultats sont intéressants, avec des temps de transmission des transactions de l’ordre de 0,125 ms.
