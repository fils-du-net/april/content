---
site: OSOR.EU
title: "FR: 'Marseille's desktop plans conflict with procurement rules'"
author: Gijs Hillenius
date: 2010-10-27
href: http://www.osor.eu/news/fr-marseilles-desktop-plans-conflict-with-procurement-rules
tags:
- Le Logiciel Libre
- Administration
- April
---

> (La ville de Marseille enfreint les règles des marchés publiques d'après l'april) Plans developed by the IT department of the French city of Marseille to replace its three desktops operating systems by a single proprietary operating system, are breaking procurement rules, alleges April, an association on free software and Libertis, a association of IT service companies specialising in this type of software.
