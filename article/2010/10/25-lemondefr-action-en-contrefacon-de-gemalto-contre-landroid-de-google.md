---
site: LeMonde.fr
title: "Action en contrefaçon de Gemalto contre l'Android de Google"
author: La rédaction
date: 2010-10-25
href: http://www.lemonde.fr/technologies/article/2010/10/25/action-en-contrefacon-de-gemalto-contre-l-android-de-google_1431042_651865.html
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Brevets logiciels
- Droit d'auteur
- International
---

> Le fabricant français de cartes à puces Gemalto, numéro un mondial du secteur, a annoncé, lundi 25 octobre, avoir engagé une action en contrefaçon aux Etats-Unis visant le système d'exploitation mobile Android de Google. La plainte concerne aussi les constructeurs HTC, Motorola et Samsung, qui équipent aussi leurs téléphones mobiles avec ce système.
