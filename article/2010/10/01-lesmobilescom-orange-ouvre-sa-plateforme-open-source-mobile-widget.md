---
site: lesmobiles.com
title: "Orange ouvre sa plateforme « Open Source Mobile Widget »"
author: La rédaction
date: 2010-10-01
href: http://www.lesmobiles.com/actualite/5346-orange-ouvre-sa-plateforme-open-source-mobile-widget.html
tags:
- Le Logiciel Libre
- Entreprise
---

> Dans le cadre de la conférence « Mobilize 2010 », Orange a annoncé la mise à disposition de tous les acteurs de l'industrie mobile de sa plateforme « Open Source Mobile Widget ».
