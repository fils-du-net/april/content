---
site: Framablog
title: "Avec Uniflow, Canon invente la photocopieuse qui espionne, refuse et dénonce"
author: Siltaar
date: 2010-10-15
href: http://www.framablog.org/index.php/post/2010/10/15/Uniflow-Canon-photocopieuse-espionne-refuse-et-d%C3%A9nonce
tags:
- Entreprise
- April
- DRM
- Informatique-deloyale
- Neutralité du Net
---

> Canon a créé des photocopieuses qui inspectent au plus près les documents qu’on leur donne à reproduire, et s’y refusent si ces derniers contiennent l’un des mots de la liste noire située sur le serveur central des installations Uniflow.
