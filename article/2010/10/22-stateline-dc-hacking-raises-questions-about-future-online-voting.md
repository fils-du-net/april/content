---
site: Stateline
title: "D.C. hacking raises questions about future of online voting"
author: Sean Greene
date: 2010-10-22
href: http://www.stateline.org/live/details/story?contentId=522635
tags:
- Le Logiciel Libre
- Internet
- Institutions
- Désinformation
- Vote électronique
- International
- English
---

> (Un test de dernière minute révèle nombre de problèmes avec un système de vote en ligne, pourtant basé sur du Logiciel Libre) Security remains an obstacle to voting over the Internet. But more states may be tempted to experiment in order to comply with a new law concerning the rights of military and overseas voters.
