---
site: PC INpact
title: "Les verrous de sécurisation d'Hadopi notifiés en secret à Bruxelles"
author: Marc Rees
date: 2010-10-13
href: http://www.pcinpact.com/actu/news/59818-hadopi-ministere-culture-notification-securisation.htm
tags:
- HADOPI
- Institutions
- Informatique-deloyale
- Europe
---

> Nous venons de le découvrir : le ministère de la Culture a notifié à Bruxelles le projet de décret organisant les moyens de sécurisation qui permettront de prévenir les usages illicites sur les ordinateurs des abonnés Internet.
