---
site: Numerama
title: "Microsoft s'attaque à OpenOffice dans une vidéo"
author: Julien L.
date: 2010-10-14
href: http://www.numerama.com/magazine/17059-microsoft-s-attaque-a-openoffice-dans-une-video.html
tags:
- Le Logiciel Libre
- Video
---

> La progression du logiciel libre est un réel problème pour Microsoft. Depuis quelques années, la firme de Redmond est fortement concurrencée dans de nombreux domaines. Preuve en est, le géant des logiciels accorde de plus en plus de temps pour critiquer les solutions libres.
