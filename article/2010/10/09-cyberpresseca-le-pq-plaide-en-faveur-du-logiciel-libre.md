---
site: cyberpresse.ca
title: "Le PQ plaide en faveur du logiciel libre"
author: Pierre Asselin
date: 2010-10-09
href: http://www.cyberpresse.ca/le-soleil/actualites/politique/201010/08/01-4331064-le-pq-plaide-en-faveur-du-logiciel-libre.php
tags:
- Le Logiciel Libre
- Administration
- Institutions
- International
---

> (Québec) Le Conseil du trésor se prive de millions de dollars d'économies en tournant le dos au logiciel libre, au moment où il veut donner un coup de barre dans les dépenses informatiques, soutient le Parti québécois.
