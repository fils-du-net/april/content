---
site: datanews
title: "Prévenir le \"trou noir numérique\""
author: Jean-Charles Morisseau
date: 2010-10-18
href: http://datanews.rnews.be/fr/ict/actualite/opinion/prevenir-le-trou-noir-numerique/article-1194836357198.htm
tags:
- Logiciels privateurs
- Standards
---

> Quelle politique de sauvegarde du patrimoine numérique doit-on mettre en place?
