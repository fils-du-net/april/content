---
site: toolinux
title: "FSFE : \"2286 sites web institutionnels font de la pub pour des logiciels non libres\""
author: La rédaction
date: 2010-10-19
href: http://www.toolinux.com/FSFE-2286-sites-web
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Administration
- Économie
- Associations
- Standards
---

> Au cours de la campagne pdfreaders.org de la Free Software Foundation Europe, des centaines de partisans des logiciels libres de 41 pays ont signalé 2286 institutions du secteur public faisant de la publicité "pour des lecteurs PDF non libres sur leurs sites web."
