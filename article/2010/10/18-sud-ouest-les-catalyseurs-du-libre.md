---
site: SUD OUEST
title: "Les catalyseurs du libre"
author: Jean-Louis Hugon
date: 2010-10-18
href: http://www.sudouest.fr/2010/10/18/les-cataly-seurs-du-libre-214825-3318.php
tags:
- Le Logiciel Libre
- Entreprise
- Administration
- Associations
- Innovation
---

> L'association @QuiNetiC, composée de passionnés, de chercheurs et d'enseignants, veut booster les logiciels libres.
