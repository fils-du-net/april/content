---
site: SUD OUEST
title: "Les enfants initiés"
author: Philippe Rebeix
date: 2010-10-25
href: http://www.sudouest.fr/2010/10/25/les-enfants-inities-221147-813.php
tags:
- Le Logiciel Libre
- Administration
- Associations
- Éducation
---

> L'initiation aux logiciels libres par l'association Linux-Angoulême était au programme des enfants de l'Accueil loisirs maternel, lors des trois ateliers d'une durée d'une heure chacun, qui ont eu lieu lors les trois premiers mercredis d'octobre.
