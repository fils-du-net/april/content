---
site: Le Monde Informatique
title: "Twitter mise sur l'Open Source pour doper son moteur de recherche"
author: Jacques Cheminat
date: 2010-10-08
href: http://www.lemondeinformatique.fr/actualites/lire-twitter-mise-sur-l-open-source-pour-doper-son-moteur-de-recherche-31873.html
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Innovation
---

> Twitter vient de modifier l'architecture  de son moteur de recherche, pour améliorer le traitement des requêtes en pleine expansion et donner plus rapidement les réponses.
