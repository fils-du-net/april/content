---
site: PC INpact
title: "Frédéric Mitterrand : « ça marche très bien Hadopi ! »"
author: Marc Rees
date: 2010-07-19
href: http://www.pcinpact.com/actu/news/58332-hadopi-frederic-mitterrand-decrets-septembre.htm
tags:
- HADOPI
---

> Frédéric Mitterrand était l’invité samedi de l’émission Mediabolique sur France Inter. Le ministre de la Culture a évidemment été interrogé sur l’avancement des lois Hadopi 1 et 2. On peut écouter ce passage via ce fichier après la 36e minute.
