---
site: Numerama
title: "Hadopi : Il faut arrêter de construire la ruineuse ligne Maginot !"
author: Guillaume Champeau
date: 2010-07-02
href: http://www.numerama.com/magazine/16141-hadopi-il-faut-arreter-de-construire-la-ruineuse-ligne-maginot.html
tags:
- Internet
- HADOPI
---

> Incroyablement coûteuses, juridiquement très fragiles, la riposte graduée et l'Hadopi n'auront sans doute aucun effet sur le piratage. Elles vont en revanche contribuer à creuser plus encore le déficit public, et les ressources des ayants droit. Il est encore temps de mettre fin à cette ligne Maginot qui sera rapidement contournée par les internautes.
