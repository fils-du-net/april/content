---
site: The Huffington Post
title: "ASCAP's attack on Creative Commons "
author: Lawrence Lessig
date: 2010-07-10
href: http://www.huffingtonpost.com/lawrence-lessig/ascaps-attack-on-creative_b_641965.html
tags:
- Sensibilisation
- Associations
- Désinformation
- Licenses
- Philosophie GNU
- Contenus libres
- International
- English
---

> (Lettre ouverte à la société de créateurs voulant attaquer le "copyleft") The American Society of Composers, Authors and Publishers (ASCAP) has launched a campaign to raise money from its members to hire lobbyists to protect them against the dangers of "Copyleft." Groups such as Creative Commons, Public Knowledge, and the Electronic Frontier Foundation are "mobilizing," ASCAP describes in a letter to its members, "to promote 'Copyleft' in order to undermine our 'Copyright.'" "[O]ur opponents are influencing Congress against the interests of music creators," ASCAP warns.
