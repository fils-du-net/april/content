---
site: Numerama
title: "Enquête : Oxford se penche sur la gouvernance mondiale et la propriété intellectuelle"
author: Julien L.
date: 2010-07-07
href: http://www.numerama.com/magazine/16185-enquete-oxford-se-penche-sur-la-gouvernance-mondiale-et-la-propriete-intellectuelle.html
tags:
- Institutions
- Sensibilisation
- Droit d'auteur
- International
---

> À l'heure où les fondements de la propriété intellectuelle sont bousculés par de nouvelles pratiques, l'université britannique d'Oxford a mis en ligne le 2 juillet dernier une enquête internationale, repérée par Astrid Girardeau, sur la "gouvernance mondiale de la connaissance et de la propriété intellectuelle".
