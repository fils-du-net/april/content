---
site: Brest Ouvert
title: "Quelle transformation écologique de l’économie ? "
author: Jerôme Gleizes
date: 2010-07-16
href: http://www.brest-ouvert.net/article8519.html
tags:
- Le Logiciel Libre
- Économie
---

> [...] Un nouvel imaginaire : passer du paradigme de l’automobile à celui du logiciel libre
