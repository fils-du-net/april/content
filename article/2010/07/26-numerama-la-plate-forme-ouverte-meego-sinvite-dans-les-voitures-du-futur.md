---
site: Numerama
title: "La plate-forme ouverte MeeGo s'invite dans les voitures du futur"
author: Guillaume Champeau
date: 2010-07-26
href: http://www.numerama.com/magazine/16311-la-plate-forme-ouverte-meego-s-invite-dans-les-voitures-du-futur.html
tags:
- Le Logiciel Libre
- Entreprise
- Innovation
- International
---

> La voiture du futur sera basée sur un système multimédia open-source. C'est en tout cas la promesse que semblent faire les industriels réunis au sein de la GENIVI Alliance, qui ont choisi la plate-forme MeeGo pour apporter les services connectés et le multimédia aux véhicules qu'ils produisent.
