---
site: The H Open
title: "﻿South Korea: Super fast, and finally free"
author: Glyn Moody
date: 2010-07-09
href: http://www.h-online.com/open/features/South-Korea-Super-fast-and-finally-free-1034389.html
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
- Institutions
- Neutralité du Net
- International
- English
---

> (En Corée le gouvernement bloquait de fait les usages de GNU/Linux) Imagine a country that has one of the best Internet infrastructures in the world, and yet its government effectively forbids the use of GNU/Linux through a requirement that everyone employ a decade-old Windows-only technology for many key online transactions. That country is South Korea, where 1 Gbits/second Internet connections are planned  for 2012; and that Windows-only technology is ActiveX.
