---
site: ars technica
title: "French legislators have second thoughts on three strikes law"
author: Nate Anderson
date: 2010-07-13
href: http://arstechnica.com/tech-policy/news/2010/07/first-anniversary-bfrench-legislators-have-second-thoughts-on-three-strikes-lawrings-second-thoughts-on-french-3-strikes.ars?utm_source=rss&utm_medium=rss&utm_campaign=rss
tags:
- Internet
- HADOPI
- Institutions
- Neutralité du Net
- English
---

> (Les législateurs Français ont-ils des doutes sur HADOPI ?) Are the French legislators who passed the country's tough new "three strikes" Internet disconnection law having second thoughts? Le Figaro caught up last week with Jean-François Copé, a leading member of the ruling right-leaning UMP party that wrote and supported the "Création et Internet" law passed last year. Copé helped rally support for the bill after it failed its first vote in the National Assembly because most UMP deputies had actually left the chamber without voting.
