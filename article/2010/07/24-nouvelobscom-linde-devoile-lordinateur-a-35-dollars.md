---
site: nouvelObs.com
title: "L'Inde dévoile l'ordinateur à 35 dollars"
author: La rédaction
date: 2010-07-24
href: http://tempsreel.nouvelobs.com/actualite/monde/20100724.FAP5960/l-inde-devoile-l-ordinateur-a-35-dollars.html
tags:
- Le Logiciel Libre
- Institutions
- Éducation
- Innovation
- International
---

> MUMBAI, Inde (AP) — C'est un ordinateur qui ressemble un peu à l'iPad, mais coûte infiniment moins cher: l'Inde vient de dévoiler le prototype d'une tablette à écran tactile destinée aux étudiants dont elle espère lancer la production en 2011. Son coût: seulement 35 dollars (27 euros).
