---
site: PC INpact
title: "Casser l'anonymat sur le net ? Inopportun et inefficace selon NKM"
author: Marc Rees
date: 2010-07-26
href: http://www.pcinpact.com/actu/news/58475-nkm-anonymat-levee-reponse-inutile.htm
tags:
- Internet
- Institutions
---

> Le député André Wojciechowski avait repris à son compte les préoccupations du sénateur Masson, visant à lever l'anonymat sur le net. Il avait questionné à cette fin NKM, secrétaire d'État à l’économie numérique.
