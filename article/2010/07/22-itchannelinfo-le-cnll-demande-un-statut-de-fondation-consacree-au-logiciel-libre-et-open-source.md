---
site: ITCHANNEL.info
title: "Le CNLL demande un statut de Fondation consacrée au Logiciel Libre et Open Source  "
author: La rédaction
date: 2010-07-22
href: http://www.itchannel.info/index.php/articles/108009/cnll-demande-statut-fondation-consacree-logiciel-libre-open-source.html
tags:
- Le Logiciel Libre
- Entreprise
- Institutions
- Associations
- Promotion
---

> Le Conseil National du Logiciel Libre demande la création d’un nouveau statut de Fondation consacrée au Logiciel Libre et Open Source (FLOSS). Constatant que des fondations américaines telles que la Fondation Linux ou encore la Fondation Apache ont permis l'éclosion de logiciels qui sont au cœur de L'économie numérique, le CNLL souligne qu'il n'y a pas de semblable fondation en France. La création d'un statut nouveau permettrait à la France de tenir son rang dans la construction du patrimoine mondial du FLOSS.
