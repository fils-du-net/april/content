---
site: Groklaw
title: "Sanity From the 1st Post-Bilski Decision from BPAI: In Re Proudler"
author: La rédaction
date: 2010-07-13
href: http://www.groklaw.net/article.php?story=20100713173032257
tags:
- Brevets logiciels
- International
- English
---

> (Une des premières décisions suivant l'affaire Bilski, semblant bloquer la brevetabilité des logiciels car étant abstraits) Look at this, will you? The first decision from the Board of Patents Appeals and Interferences post-Bilski  to reference that US Supreme Court decision, in  In Re Proudler [PDF], a ruling rejecting HP's application for a software patent, setting forth a rule stating, as I read it, as saying software is not patentable because it's an abstraction:
