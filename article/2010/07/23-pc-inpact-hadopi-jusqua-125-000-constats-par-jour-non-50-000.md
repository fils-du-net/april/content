---
site: PC INpact
title: "Hadopi : jusqu'à 125 000 constats par jour, non 50 000"
author: Marc Rees
date: 2010-07-23
href: http://www.pcinpact.com/actu/news/58447-hadopi-eric-walter-125000-constats.htm
tags:
- Internet
- Administration
- HADOPI
- Institutions
- Droit d'auteur
---

> Dans une interview accordée à Édition multimédia, à paraître ce lundi, Éric Walter, secrétaire général de la Hadopi, affirme une nouvelle fois que la machine Hadopi sera bientôt prête. « Nous allons pouvoir recevoir ces jours-ci les premières saisines de la part des ayants droit de la musique et du cinéma. Mécaniquement, l’envoi des premiers e-mails d’avertissement aux internautes interviendra après les premières saisines »
