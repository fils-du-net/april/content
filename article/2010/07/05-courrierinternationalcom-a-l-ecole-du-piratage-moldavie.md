---
site: Courrier International
title: "A l’école du piratage (Moldavie)"
author: Viorel Roman
date: 2010-07-05
href: http://www.courrierinternational.com/article/2010/07/05/a-l-ecole-du-piratage
tags:
- Le Logiciel Libre
- Promotion
- International
---

> En Moldavie, les logiciels piratés sont partout. Même dans les écoles et les universités, les ordinateurs sont chargés illégalement. Une pratique courante dans les pays de la CEI, explique Ziarul de Garda.
