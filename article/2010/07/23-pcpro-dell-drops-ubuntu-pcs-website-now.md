---
site: PCPRO
title: "Dell drops Ubuntu PCs from website... for now"
author: Barry Collins
date: 2010-07-23
href: http://www.pcpro.co.uk/news/359740/dell-drops-ubuntu-pcs-from-website-for-now
tags:
- Le Logiciel Libre
- Entreprise
- Sensibilisation
- International
---

> (Dell arrête les ventes de PC pré-installés avec ubuntu) Dell has stopped selling consumer PCs preloaded with Ubuntu from its website, and doesn't know when they're coming back.
