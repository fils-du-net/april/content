---
site: Toonux
title: "Toonux candidat à l'étude pour interopérabilité du logiciel de sécurisation HADOPI"
author: bluetouff
date: 2010-07-30
href: http://www.toonux.com/news/toonux-candidat-a-letude-pour-interoperabilite-du-logiciel-de-securisation-hadopi
tags:
- Le Logiciel Libre
- Internet
- Interopérabilité
- HADOPI
---

> Toonux se montre intéressé par l'étude des moyens de sécurisation proposés par la HADOPI à des fins d'intéropérabilité.
