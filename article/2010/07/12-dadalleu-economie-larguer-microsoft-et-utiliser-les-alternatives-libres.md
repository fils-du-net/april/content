---
site: Blog de dada
title: "Economie ? Larguer Microsoft et utiliser les alternatives libres"
author: dada
date: 2010-07-12
href: http://www.dadall.eu/blog/index.php?post/2010/07/12/Economie-Larguer-Microsoft-et-utiliser-les-alterntives-libres
tags:
- Le Logiciel Libre
- Administration
- International
---

> C'est ce qui ressort d'une consultation faite par le Premier Ministre David Cameron auprès des 600 000 employés du gouvernement anglais. But des consultations ? Faire des économies alors que le déficit budgétaire du pays atteint un record.
> Parmi les 32 avis publiés sur le site officiel du Ministère de l'Économie et des Finances anglais (Her Majesty's Treasury), certains suggèrent clairement qu'il faut se débarrasser des deux logiciels phares de l'entreprise de Redmond : Microsoft Windows et Microsoft Office, et les remplacer par leurs alternatives libres : GNU/Linux et OpenOffice.
