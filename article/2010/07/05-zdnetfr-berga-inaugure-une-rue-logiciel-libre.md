---
site: ZDNet
title: "Berga inaugure une rue \"Logiciel Libre\""
author: La rédaction
date: 2010-07-05
href: http://www.zdnet.fr/actualites/berga-inaugure-une-rue-logiciel-libre-39752935.htm
tags:
- Actualité locale
- Le Logiciel Libre
- Promotion
- Europe
---

> La ville de Berga, en Catalogne, a inauguré le 3 juillet une rue "Programari lliure" (logiciel libre en catalan) en présence de Richard Stallman.
