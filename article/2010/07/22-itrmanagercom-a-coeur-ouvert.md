---
site: ITRmanager.com
title: "A coeur ouvert"
author: Jean-Marie Chauvet
date: 2010-07-22
href: http://www.itrmanager.com/articles/108024/coeur-ouvert.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Licenses
- Informatique en nuage
---

> Il y a environ un mois, Consona Corporation un spécialiste du build-up financier dans le secteur des progiciels de gestion intégrés (PGI) et de la gestion de la relation client (GRC) faisait l'acquisition de l'PGI/GRC Open Source emblématique Compiere.
