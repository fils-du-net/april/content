---
site: Numerama
title: "La loi Loppsi à l'ordre du jour de la rentrée parlementaire"
author: Guillaume Champeau
date: 2010-07-27
href: http://www.numerama.com/magazine/16330-la-loi-loppsi-a-l-ordre-du-jour-de-la-rentree-parlementaire.html
tags:
- Internet
- Institutions
- Informatique-deloyale
---

> Les services du Parlement ont publié aujourd'hui l'ordre du jour de la session parlementaire extraordinaire convoquée à compter du mardi 7 septembre 2010. La loi Loppsi figure en bonne place dans un agenda très chargé, puisque le projet de loi figure en quatrième position parmi 26 textes devant être examiné d'ici la fin de l'année (l'essentiel étant cependant de petits textes de régulation fiscale).
