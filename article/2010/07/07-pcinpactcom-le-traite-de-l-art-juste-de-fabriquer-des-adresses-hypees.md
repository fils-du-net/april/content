---
site: PCInpact
title: "Le Traité de l’Art Juste de Fabriquer des Adresses Hypées"
author: Marc Rees
date: 2010-07-07
href: http://www.pcinpact.com/actu/news/58123-quadrature-net-gus-garage-fdn.htm
tags:
- Internet
- HADOPI
- Sensibilisation
- Neutralité du Net
- Promotion
---

> Alors qu’Hadopi se demande comment elle va vivre, survivre, et peut-être prospérer, un mystérieux manuel est apparu sur les ondes numériques. Rédigé en un simili vieux français bien de chez nous, ce « Traité de l’art juste de fabriquer des adresses hypées » dormait depuis des lustres dans les caves voutées de FDN, premier FAI associatif français.
