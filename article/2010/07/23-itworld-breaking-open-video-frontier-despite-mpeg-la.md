---
site: ITworld
title: "Breaking Open the Video Frontier, Despite MPEG-LA"
author: La rédaction
date: 2010-07-23
href: http://www.itworld.com/open-source/115063/breaking-open-video-frontier-despite-mpeg-la
tags:
- Le Logiciel Libre
- Innovation
- Standards
- Video
- Contenus libres
---

> (Ouvrir la frontière vidéo, malgré le format MPEG) Did you know that nearly every video produced for Web viewing has been, at one point or another, in MPEG format no matter in what format the video is ultimately saved?
