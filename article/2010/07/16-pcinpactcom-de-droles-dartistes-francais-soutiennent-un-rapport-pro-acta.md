---
site: PCInpact
title: "De drôles d'artistes français soutiennent un rapport pro ACTA"
author: Marc Rees
date: 2010-07-16
href: http://www.pcinpact.com/actu/news/58310-artistes-acta-rapport-gallo-sardou.htm
tags:
- Internet
- HADOPI
- Institutions
- ACTA
---

> Tous les moyens d’influence sont visiblement bons pour faire pencher les eurodéputés du bon côté. Dans une lettre adressée voilà peu aux parlementaires européens, des artistes de renom demandent à ces politiques de promouvoir les richesses européennes et « la diversité de la culture ». Comment ?  En adoptant au plus vite le rapport sur le renforcement de l'application des droits de propriété intellectuelle, dit Rapport Gallo. Du moins dans sa version pure et dure, celle approuvée par la Commission des affaires juridiques du Parlement, le 1er juin dernier.
