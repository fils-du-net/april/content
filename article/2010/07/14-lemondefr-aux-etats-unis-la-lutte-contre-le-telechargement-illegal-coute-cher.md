---
site: LeMonde.fr
title: "Aux Etats-Unis, la lutte contre le téléchargement illégal coûte cher"
author: La rédaction
date: 2010-07-14
href: http://www.lemonde.fr/technologies/article/2010/07/14/aux-etats-unis-la-lutte-contre-le-telechargement-illegal-coute-cher_1387758_651865.html
tags:
- Internet
- Économie
- HADOPI
- International
---

> Seize millions de dollars d'honoraires d'avocats (12,5 millions d'euros) pour récupérer 390 000 dollars (310 000 euros) de compensations financières : c'est le bilan partiel de la stratégie de lutte contre le téléchargement illégal menée l'an dernier par la RIAA, l'équivalent américain de la Sacem, d'après les chiffres compilés par le blog spécialisé Recording industry VS the people.
