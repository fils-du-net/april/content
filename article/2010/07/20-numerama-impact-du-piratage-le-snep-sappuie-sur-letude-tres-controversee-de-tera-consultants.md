---
site: Numerama
title: "Impact du piratage : le SNEP s'appuie sur l'étude très controversée de Tera Consultants"
author: Julien L.
date: 2010-07-20
href: http://www.numerama.com/magazine/16273-impact-du-piratage-le-snep-s-appuie-sur-l-etude-tres-controversee-de-tera-consultants.html
tags:
- Entreprise
- Internet
- HADOPI
- DADVSI
- Désinformation
- Droit d'auteur
---

> Dans son guide sur l'économie de la production musicale, le SNEP s'est appuyé sur les conclusions très controversées d'une étude menée en mars dernier par Tera Consultants. Or, ce document a été directement voulu par la BASCAP, un lobby anti-piratage co-présidé par Jean-René Fourtou, le président du conseil de surveillance de Vivendi.
