---
site: Ecrans
title: "« UltraViolet » verrouille le cinéma"
author: Andréa Fradin
date: 2010-07-22
href: http://www.ecrans.fr/UltraViolet-verrouille-le-cinema,10456.html
tags:
- Entreprise
- Internet
- DRM
- Video
---

> Soixante. Quelques soixante géants de l’industrie culturelle et technologique rassemblés en un consortium, le « Digital Entertainment Content Ecosystem » (DECE), ont lancé UltraViolet, nouveau projet qui vient relancer le système décrié et moribond des DRM (Digital Rights Management) comparables à des verrous numériques qui orientent l’utilisation d’une oeuvre. Microsoft et IBM, du côté de la technologie, NBC Universal, Paramount et Warner Bros du côté du cinéma et encore Philips, Sony et Panasonic, qui représentent les constructeurs, ont ainsi répondu à l’appel.
