---
site: Numerama
title: "Plus que 16 signatures à réunir pour la déclaration anti-ACTA"
author: Guillaume Champeau
date: 2010-07-08
href: http://www.numerama.com/magazine/16202-plus-que-16-signatures-a-reunir-pour-la-declaration-anti-acta.html
tags:
- Internet
- Droit d'auteur
- International
---

> C'est presque fait, et ça sera une grande victoire pour les adversaires de l'accord commercial anti-contrefaçon (ACTA). Lundi, il restait encore 116 signatures de députés européens à recueillir sur la feuille de la déclaration écrite n°12 "sur l’absence d’un processus transparent et la présence d’un contenu potentiellement controversé concernant l’accord commercial anti-contrefaçon".
