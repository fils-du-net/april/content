---
site: silicon.fr
title: "Jean-Noël de Galzain reprend Mandriva en main"
author: Christophe Lagane
date: 2010-07-07
href: http://www.silicon.fr/fr/news/2010/07/07/jean_noel_de_galzain_reprend_mandriva_en_main
tags:
- Le Logiciel Libre
- Entreprise
---

> Nommé au conseil d'administration de Mandriva, le président de IF Research Jean-Noël de Galzain nous dévoile sa stratégie pour remettre à flot le seul éditeur français d'une distribution Linux.
