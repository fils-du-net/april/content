---
site: LinuxFr
title: "La plainte des fournisseurs de logiciels libres contre le contrat de gré-à-gré avec Microsoft n'est pas recevable"
author: fbianco
date: 2010-07-07
href: http://linuxfr.org/2010/07/07/27097.html
tags:
- Le Logiciel Libre
- Entreprise
- Administration
- Europe
- International
---

> Les juges du Tribunal Fédéral Administratif (TAF) suisse ont statué sur la qualité pour agir des plaignants dans le cas de la plainte contre le contrat de 42 millions de francs suisse (30 millions d'euros). Celui-ci avait été passé sans appel d'offre public entre l'Office fédéral des constructions et de la logistique (OFCL) et Microsoft. Ce contrat comprenait de nouvelles licences logicielles, des mises-à-jour et des services de supports sur trois ans.
