---
site: toolinux
title: "Signature d’une convention AFUL - Master MBC de Paris-Dauphine"
author: La rédaction
date: 2010-07-15
href: http://www.toolinux.com/lininfo/toolinux-information/communaute/article/signature-d-une-convention-aful
tags:
- Le Logiciel Libre
- Administration
- Partage du savoir
- Associations
- Éducation
- Contenus libres
---

> Le Master Business Consulting (MBC) de l’université Paris-Dauphine et l’Association Francophone des Utilisateurs de Logiciels Libres (AFUL) ont signé une convention le 30 juin 2010. Cette convention établit les modalités d’une collaboration sur deux volets ; le premier, un volet enseignement, et le second, un volet travaux de recherche.
