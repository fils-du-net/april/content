---
site: Le dessous des cartes
title: "Les secrets de Skype rendus publics ?"
author: Philippe Richard
date: 2010-07-16
href: http://ledessousdescartes.blogspot.com/2010/07/les-secrets-de-skype-rendus-publics.html
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- Interopérabilité
- Standards
---

> Des experts en cryptographie ont diffusé sur internet des fichiers permettant de mieux comprendre les protocoles de cette application. Une grosse pierre dans le jardin secret de Skype.
