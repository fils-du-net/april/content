---
site: toolinux
title: "FSF : \"Une technologie propriétaire est un gaspillage financier\""
author: La rédaction
date: 2010-07-13
href: http://www.toolinux.com/lininfo/toolinux-information/communaute/article/fsf-une-technologie-proprietaire
tags:
- Le Logiciel Libre
- Interopérabilité
- Institutions
- Neutralité du Net
- Europe
---

> Non, il ne s’agit pas d’une citation émanant d’une personne de la communauté du Logiciel Libre, mais celle de Neelie Kroes, vice-présidente de la Commission Européenne, qui intervenait à un sommet organisé par l’OpenForum Europe (OFE) à Bruxelles le 10 juin.
