---
site: Numerama
title: "Un étrange processus secret prépare à la riposte graduée en Norvège"
author: Guillaume Champeau
date: 2010-07-05
href: http://www.numerama.com/magazine/16165-un-etrange-processus-secret-prepare-a-la-riposte-graduee-en-norvege.html
tags:
- Le Logiciel Libre
- HADOPI
- Europe
---

> Nous ne sommes pas du tout familiers du droit norvégien et encore moins de son système judiciaire, donc peut-être le processus est-il perçu comme totalement légitime et courant au pays nordique. Toujours est-il qu'il est curieux d'apprendre par Torrentfreak  que la Cour Suprême de Norvège vient de contraindre un fournisseur d'accès à Internet à révéler à un ayant droit l'identité d'un abonné connu par son adresse IP, après une procédure restée totalement secrète depuis le jugement en première instance.
