---
site: Numerama
title: "ACTA : \"vous pensez que la France est une dictature ?\""
author: Julien L.
date: 2010-07-03
href: http://www.numerama.com/magazine/16156-acta-vous-pensez-que-la-france-est-une-dictature.html
tags:
- Institutions
- ACTA
---

> Une certaine tension est apparue lors d'une réunion autour de l'ACTA entre les négociateurs du traité et les représentants de la société civile. À l'origine de l'accrochage, un débat autour de la responsabilité des fournisseurs d'accès à Internet dans la lutte contre la contrefaçon. Pour certains, le texte va transformer les FAI en milice privée au service du droit d'auteur. Une idée vivement contestée par les promoteurs de l'ACTA.
