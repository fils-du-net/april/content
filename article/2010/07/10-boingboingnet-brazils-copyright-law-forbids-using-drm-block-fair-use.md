---
site: boingboing
title: "Brazil's copyright law forbids using DRM to block fair use"
author: Cory Doctorow
date: 2010-07-10
href: http://www.boingboing.net/2010/07/10/brazils-copyright-la.html
tags:
- Interopérabilité
- Institutions
- DRM
- International
- English
---

> La loi sur le copyright du Brésil interdit l'usage des DRM pour limiter les droits des brésiliens.
