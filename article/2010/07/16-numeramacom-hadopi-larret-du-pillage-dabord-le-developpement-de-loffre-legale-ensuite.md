---
site: Numerama
title: "Hadopi : l'arrêt du pillage d'abord, le développement de l'offre légale ensuite"
author: Julien L.
date: 2010-07-16
href: http://www.numerama.com/magazine/16246-hadopi-l-arret-du-pillage-d-abord-le-developpement-de-l-offre-legale-ensuite.html
tags:
- Internet
- HADOPI
- Institutions
---

> À quelques jours de la publication attendue du dernier décret de la Hadopi, le secrétaire général de la Haute Autorité est revenu sur plusieurs sujets liés à la riposte gradué. Evoquant l'offre légale, il estime que celle-ci ne pourra se développer sans d'abord une réduction notable du piratage. Et si c'était plutôt l'inverse ?
