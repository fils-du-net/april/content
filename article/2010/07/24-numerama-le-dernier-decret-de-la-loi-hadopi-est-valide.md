---
site: Numerama
title: "Le dernier décret de la loi Hadopi est validé"
author: Julien L.
date: 2010-07-24
href: http://www.numerama.com/magazine/16301-le-dernier-decret-de-la-loi-hadopi-est-valide.html
tags:
- HADOPI
---

> Le ciel s'éclaircit pour la Haute Autorité. En effet, le Conseil d'État a validé le dernier décret relatif à la procédure appliquée devant la Commission de protection des droits. La semaine prochaine, le ministre de la culture et de la communication devrait donc informer ses collègues à l'occasion du prochain conseil des ministres. Mais tout n'est pas encore réglé pour le dispositif anti-piratage...
