---
site: Numerama
title: "Le vote du rapport Gallo sur la contrefaçon reporté à la rentrée"
author: Guillaume Champeau
date: 2010-07-05
href: http://www.numerama.com/magazine/16167-le-vote-du-rapport-gallo-sur-la-contrefacon-reporte-a-la-rentree.html
tags:
- Droit d'auteur
---

> C'était inattendu. Après l'adoption il y a un mois du rapport Gallo par la commission des affaires juridiques du Parlement Européen, l'ensemble des eurodéputés étaient appelés à entériner l'adoption du texte ce jeudi 8 juillet en séance plénière. Mais les partisans d'une vision ultra-protectrice et conservatrice du droit d'auteur sur Internet devront encore patienter.
