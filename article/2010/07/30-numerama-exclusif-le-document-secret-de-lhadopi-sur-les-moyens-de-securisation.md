---
site: Numerama
title: "Exclusif : le document secret de l'Hadopi sur les moyens de sécurisation"
author: Guillaume Champeau
date: 2010-07-30
href: http://www.numerama.com/magazine/16363-exclusif-le-document-secret-de-l-hadopi-sur-les-moyens-de-securisation.html
tags:
- Le Logiciel Libre
- Internet
- HADOPI
- Informatique-deloyale
---

> Malgré l'interdiction faite par l'Hadopi, et en vertu du droit à l'information, Numerama diffuse le document de consultation relatif au projet de spécifications fonctionnelles des moyens de sécurisation. On peut donc, enfin, parler de consultation publique.
