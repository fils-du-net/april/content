---
site: Numerama
title: "Hadopi : le recours déposé par FDN est enfin transmis aux parties adverses"
author: Julien L.
date: 2010-07-30
href: http://www.numerama.com/magazine/16364-hadopi-le-recours-depose-par-fdn-est-enfin-transmis-aux-parties-adverses.html
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
---

> En mai dernier, FDN déposait un recours devant le Conseil d'État pour mettre en lumière un vice de forme autour d'un décret de la loi Hadopi. Ce décret concerne le fichier des infractions de la Hadopi et les informations que les FAI seront amenés à lui fournir. Mais selon le FAI, le gouvernement n'a pas respecté la procédure, en oubliant de consulter l'ARCEP. Selon Benjamin Bayart, le vice de forme na été communiqué que récemment aux parties adverses.
