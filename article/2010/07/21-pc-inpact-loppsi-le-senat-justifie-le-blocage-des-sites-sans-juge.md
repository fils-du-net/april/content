---
site: PC INpact
title: "LOPPSI : le Sénat justifie le blocage des sites sans juge"
author: Marc Rees
date: 2010-07-21
href: http://www.pcinpact.com/actu/news/57509-senat-loppsi-juge-hadopi-blocage.htm
tags:
- Internet
- Institutions
- Neutralité du Net
---

> La version du texte mise en ligne voilà une semaine sur le site du Sénat confirme le filtrage des sites sans intervention de l'autorité judiciaire. Le texte (art.
