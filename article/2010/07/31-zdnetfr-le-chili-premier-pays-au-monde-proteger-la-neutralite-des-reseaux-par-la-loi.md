---
site: ZDNet.fr
title: "Le Chili, premier pays au monde à protéger la neutralité des réseaux par la loi"
author: Pierre Col
date: 2010-07-31
href: http://www.zdnet.fr/blogs/infra-net/le-chili-premier-pays-au-monde-a-proteger-la-neutralite-des-reseaux-par-la-loi-39753558.htm
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- International
---

> Peut-être est-ce dû à l’histoire particulière du Chili, qui a connu il y a plus de 30 ans des heures très sombres ? Je rappelle aux lecteurs les plus jeunes que le 11 septembre 1973 l’armée chilienne, soutenue par la CIA, déclenchait un coup d’état militaire provoquant la mort du président démocratiquement élu Salvador Allende et plongeant le Chili dans de longues années de dictature.
