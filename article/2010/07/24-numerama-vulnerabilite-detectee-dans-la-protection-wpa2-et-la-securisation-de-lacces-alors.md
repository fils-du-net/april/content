---
site: Numerama
title: "Vulnérabilité détectée dans la protection WPA2. Et la sécurisation de l'accès alors ?"
author: Julien L.
date: 2010-07-24
href: http://www.numerama.com/magazine/16304-vulnerabilite-detectee-dans-la-protection-wpa2-et-la-securisation-de-l-acces-alors.html
tags:
- Internet
- HADOPI
---

> Des chercheurs ont découvert une faille dans la protection WPA2, la plus forte actuellement disponible sur les réseaux Wi-Fi. Les internautes, qui ont désormais la responsabilité de sécuriser leur connexion Internet, sont donc potentiellement dans une situation insoluble : comment sécuriser correctement un accès Internet si les outils à disposition sont tous techniquement faillibles ?
