---
site: Numerama
title: "Très petit score pour le Parti Pirate aux législatives de Rambouillet"
author: Guillaume Champeau
date: 2010-07-05
href: http://www.numerama.com/magazine/16158-tres-petit-score-pour-le-parti-pirate-aux-legislatives-de-rambouillet.html
tags:
- Internet
- Institutions
- Contenus libres
---

> Alors qu'il avait recueilli 2,06 % des voix en 2009, le Parti Pirate n'a obtenu dimanche que 0.66 % des suffrages lors de l'élection législative partielle des Yvelines.
