---
site: toolinux
title: "Open Source Think Tank 2010 : pour Colin Bodell, l'opensource est devenu un organe vital d'Amazon"
author: La rédaction
date: 2010-07-22
href: http://www.toolinux.com/lininfo/toolinux-information/interviews-152/article/open-source-think-tank-2010-pour
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- International
---

> A l’occasion du 2010 US Open Source Think Tank, qui s’est tenu en avril dernier à Napa (CA), TOOLinux.com a rencontré Colin Bodell, Vice President Website Platform d’Amazon.com. Pour lui, l’opensource est un gage d’indépendance et un accélérateur de développements.
