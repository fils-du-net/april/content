---
site: Numerama
title: "La loi DADVSI toujours incomplète quatre ans après sa promulgation"
author: Guillaume Champeau
date: 2010-07-07
href: http://www.numerama.com/magazine/16187-la-loi-dadvsi-toujours-incomplete-quatre-ans-apres-sa-promulgation.html
tags:
- Internet
- Institutions
- DADVSI
- DRM
---

> Alors que tout le monde a les yeux fixés sur la loi Hadopi et la mise en oeuvre prochaine de la riposte graduée, jetons un oeil au rétroviseur. La loi DADVSI, votée en 2006 sous l'ère de Renaud Donnedieu de Vabres pour enrayer déjà le piratage sur Internet, n'est pas encore complète. Il lui manque plusieurs décrets d'application, dont celui qui doit encadrer le respect de la copie privée dans les offres de contenus sous DRM.
