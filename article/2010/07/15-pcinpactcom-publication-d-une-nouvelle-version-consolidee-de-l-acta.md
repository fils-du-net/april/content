---
site: PCInpact
title: "Publication d’une nouvelle version consolidée de l’ACTA"
author: Marc Rees
date: 2010-07-15
href: http://www.pcinpact.com/actu/news/58286-acta-version-consolidee-contrefacon-hadopi.htm
tags:
- Internet
- Institutions
- International
- ACTA
---

> La Quadrature du net a publié hier une version consolidée et intégrale du texte de l’ACTA. Cette version comprend les noms des parties autour de la table des négociations qui se sont tenues à Lucerne, accompagnés de leur position. Cette information est on ne peut plus utile puisqu’elle permettra de déterminer les positions influentes dans les futures versions du traité anticontrefaçon.
