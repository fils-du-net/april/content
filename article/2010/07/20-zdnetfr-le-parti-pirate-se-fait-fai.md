---
site: ZDNet.fr
title: "Le Parti Pirate se fait FAI"
author: Olivier Chicheportiche
date: 2010-07-20
href: http://www.zdnet.fr/actualites/le-parti-pirate-se-fait-fai-39753283.htm
tags:
- Internet
- HADOPI
- Institutions
- Europe
---

> Ayant pour objectif de "combattre la société de surveillance", ce fournisseur d'accès sera disponible dans quelques villes suédoises et assurera l'anonymat des abonnés.
