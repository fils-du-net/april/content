---
site: 01net.
title: "Hadopi : la plainte de l'ex-salarié de TF1 classée sans suite"
author: Guillaume Deleurence
date: 2010-07-29
href: http://www.01net.com/www.01net.com/editorial/519593/hadopi-la-plainte-de-lex-salarie-de-tf1-classee-sans-suite/?r=/rss/actus.xml
tags:
- Entreprise
- Internet
- HADOPI
---

> Jérôme Bourreau-Guggenheim avait été licencié par la chaîne pour avoir exprimé des critiques envers la loi contre le piratage. Sa plainte n'a pas abouti, il lance aujourd'hui une association.
