---
site: internetnews.com
title: "GPLv3 now dominates at Google Code #oscon"
author: Sean Michael Kerner
date: 2010-07-23
href: http://blog.internetnews.com/skerner/2010/07/gplv3-now-dominates-at-google.html
tags:
- Le Logiciel Libre
- Entreprise
- Licenses
- International
- English
---

> (La majorité des projets hébergés par Google sont sous GPL3) Google's open source programs manager Chris DiBona (pic left) took the stage at OSCON today and he had some interesting things to say, about licensing.
