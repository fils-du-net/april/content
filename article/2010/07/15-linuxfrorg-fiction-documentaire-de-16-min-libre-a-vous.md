---
site: LinuxFr
title: "Fiction documentaire de 16 min « Libre à vous »"
author: Benoît Sibaud
date: 2010-07-15
href: https://linuxfr.org//2010/07/15/27124.html
tags:
- Le Logiciel Libre
- Entreprise
- Sensibilisation
- Promotion
- Video
- Contenus libres
---

> Le BTS audiovisuel du lycée Suger (Saint-Denis) a réalisé une fiction documentaire de 16 minutes sur le logiciel libre intitulée « Libre à vous ». Ce travail, encadré par Diane Vattolo, est disponible sous licence Creative Commons By-Sa 2.0.
