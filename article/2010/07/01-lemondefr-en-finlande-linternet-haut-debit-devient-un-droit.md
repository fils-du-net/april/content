---
site: lemonde.fr
title: "En Finlande, l'Internet haut débit devient un droit"
author: La rédaction
date: 2010-07-01
href: http://www.lemonde.fr/technologies/article/2010/07/01/en-finlande-l-internet-haut-debit-devient-un-droit_1381867_651865.html
tags:
- Internet
- Institutions
- Europe
- International
---

> La Finlande est devenue jeudi le premier pays au monde à faire de l'accès à l'Internet haut débit un droit, assurant à tous les Finlandais une connexion de ce type. "Aujourd'hui, l'obligation de service universel concernant l'accès à l'Internet à un mégabit par seconde (1 Mbit/s) est entrée en vigueur", a déclaré à l'Agence France-Presse Olli-Pekka Rantala, le directeur du service des réseaux de communication au ministère des transports et des communications.
