---
site: tom's guide
title: "Pirate Bay hébergé au parlement suédois"
author: David Civera
date: 2010-07-07
href: http://www.infos-du-net.com/actualite/17202-Piratpartiet-Pirate-Bay.html
tags:
- Internet
- Droit d'auteur
- Europe
---

> Le parti politique suédois Piratpartiet, qui dispose de deux sièges au parlement européen, souhaiterait installer les serveurs de Pirate Bay directement dans les locaux du Riksdag afin de profiter de l’immunité parlementaire.
