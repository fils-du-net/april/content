---
site: Numerama
title: "Vimeo introduit les licences Creative Commons pour ses vidéos. Et YouTube ?"
author: Julien L.
date: 2010-07-15
href: http://www.numerama.com/magazine/16240-vimeo-introduit-les-licences-creative-commons-pour-ses-videos-et-youtube.html
tags:
- Entreprise
- Internet
- Partage du savoir
- Licenses
- Video
- Contenus libres
- International
---

> Ces dernières années, les licences Creative Commons ont pris une place de tout premier plan dans la façon dont sont gérés certains droits d'une oeuvre. Moins limitatives que le droit d'auteur classique, ces licences permettent de préserver les droits de l'auteur tout en introduisant une certaine souplesse d'utilisation pour les autres.
