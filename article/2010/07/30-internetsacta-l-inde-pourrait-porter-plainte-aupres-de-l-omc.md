---
site: The Internets
title: "[The Internets]ACTA : L’Inde pourrait porter plainte auprès de l’OMC"
author: Astrid Girardeau
date: 2010-07-30
href: http://www.theinternets.fr/2010/07/30/express-acta-linde-pourrait-porter-plainte-aupres-de-lomc/
tags:
- Internet
- Institutions
- Droit d'auteur
- International
- ACTA
---

> Depuis quelques mois, l’Inde et la Chine font part de leurs inquiétudes vis-vis de l’ACTA (Anti-Counterfeiting Trade Agreement). Cet accord réduirait leur marge de manœuvre au sein de l’OMPI (Organisation mondiale de la propriété intellectuelle) et de l’OMC (Organisation mondiale du commerce), expliquait Michael Geist début juin. Surtout « les Etats-Unis et l’Europe considèrent que l’ACTA ne doit pas s’appliquer aux seuls pays qui négocient, mais à tous les pays ».
