---
site: ZDNet.fr
title: "Neutralité des réseaux : le rapport du gouvernement vient d'être remis aux députés !"
author: Pierre Col
date: 2010-07-31
href: http://www.zdnet.fr/blogs/infra-net/neutralite-des-reseaux-le-rapport-du-gouvernement-vient-d-etre-remis-aux-deputes-39753559.htm
tags:
- Internet
- Institutions
- Neutralité du Net
---

> J'indiquais ce matin que l'on attendait toujours le rapport sur la neutralité des réseaux que Nathalie Kosciusko-Morizet devait remettre aux députés, afin d'orienter les travaux parlementaires de transposition des directives européennes du paquet Telecom.
