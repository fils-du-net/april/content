---
site: La boîte à monocle
title: "Retour sur RMLL 2010 : Les Rencontres Mondiales du Logiciel Libre"
author: Sophie Masure
date: 2010-07-26
href: http://www.laboiteamonocle.com/metier-graphiste/retour-sur-rmll-2010-les-rencontres-mondiales-du-logiciel-libre/
tags:
- Le Logiciel Libre
- Sensibilisation
- Associations
- Promotion
---

> Cette année, j’ai placé pile une semaine de vacances pendant les Rencontres Mondiales du Logiciel Libre à Bordeaux. J’ai donc allié l’utile à l’agréable en y consacrant une journée.
