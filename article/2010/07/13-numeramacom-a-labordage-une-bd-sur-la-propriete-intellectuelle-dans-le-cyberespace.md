---
site: Numerama
title: "À l'abordage ! Une BD sur la propriété intellectuelle dans le cyberespace"
author: Julien L.
date: 2010-07-13
href: http://www.numerama.com/magazine/16231-agrave-l-abordage-une-bd-sur-la-propriete-intellectuelle-dans-le-cyberespace.html
tags:
- Le Logiciel Libre
- Partage du savoir
- Sensibilisation
- DRM
- Droit d'auteur
- Philosophie GNU
- Contenus libres
---

> Anders Benston est l'auteur d'une BD sur la propriété intellectuelle dans le cyberespace. Dans celle-ci, il retrace les grandes évolutions du droit d'auteur, notamment à travers certaines étapes-clés. Un travail remarquable, notamment enrichi de références à d'autres ouvrages, comme "Free Culture" de Lawrence Lessig ou "Free software, free society" de Richard Stallman.
