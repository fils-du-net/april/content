---
site: toolinux
title: "Les Rencontres Mondiales du Logiciel Libre à l'heure du bilan"
author: La rédaction
date: 2010-07-23
href: http://www.toolinux.com/lininfo/toolinux-information/communique/article/les-rencontres-mondiales-du-14785
tags:
- Le Logiciel Libre
- Administration
- Sensibilisation
---

> Les 11es Rencontres Mondiales du Logiciel Libre qui se sont tenues sur le campus universitaire de Pessac et de Talence du mardi 6 au vendredi 9 juillet ont rencontré un vif succès avec environ 2 300 visiteurs en semaine, et environ 5 000 visiteurs sur l’ensemble de la manifestation.
