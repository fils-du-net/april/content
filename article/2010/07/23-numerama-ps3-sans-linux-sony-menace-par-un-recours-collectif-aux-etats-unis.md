---
site: Numerama
title: "PS3 sans Linux : Sony menacé par un recours collectif aux États-Unis"
author: Julien L.
date: 2010-07-23
href: http://www.numerama.com/magazine/16291-ps3-sans-linux-sony-menace-par-un-recours-collectif-aux-eacutetats-unis.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Interopérabilité
- Informatique-deloyale
- International
---

> Aux États-Unis, un tribunal californien a regroupé dans un recours collectif sept plaintes contre Sony. L'entreprise est accusée d'avoir retiré une fonctionnalité présente de base dans sa console PlayStation 3. À l'époque, c'est l'argument de la sécurité qui avait été avancé, mais beaucoup estimaient plutôt que cette décision avait été motivée par le hack récent de la console, ouvrant la voie au piratage des jeux PS3.
