---
site: La francophonie
title: "11es Rencontres mondiales du logiciel libre "
author: La rédaction
date: 2010-07-11
href: http://www.francophonie.org/11es-Rencontres-mondiales-du.html
tags:
- Le Logiciel Libre
- Promotion
---

> Une quinzaine de représentants des réseaux d’expertise de pays du Sud en logiciels libres ont participé, avec l’appui de l’Institut de la Francophonie numérique de l’OIF aux 11es Rencontres mondiales du logiciel libre (RMLL) qui se déroulaient à Bordeaux du 6 au 11 juillet 2010.
