---
site: LinuxFr
title: "Les résultats du LHC sous licence Creative Commons"
author: patrick_g
date: 2010-07-18
href: http://linuxfr.org/2010/07/18/27142.html
tags:
- Le Logiciel Libre
- Administration
- Partage du savoir
- Innovation
- Contenus libres
---

> L'accélérateur de particules LHC (Large Hadron Collider) monte doucement en puissance et les chercheurs du CERN  commencent à produire des résultats scientifiques exploitables à partir des premières données. Sous quelles licences ces articles de recherche sont-ils publiés ? Quelle est la politique officielle du CERN à ce sujet ?
