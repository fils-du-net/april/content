---
site: Numerama
title: "Brevets : une société menace plusieurs fabricants de mobiles"
author: Julien L.
date: 2010-07-10
href: http://www.numerama.com/magazine/16221-brevets-une-societe-menace-plusieurs-fabricants-de-mobiles.html
tags:
- Brevets logiciels
- International
---

> C'est typiquement un exemple de la dérive du système des brevets aux États-Unis. Le New York Times rapporte que la société NTP a décidé de passer à l'action en portant plainte contre les principales entreprises du secteur de la téléphonie mobile. Parmi celles-ci, nous retrouvons des géants américains comme Apple, Google, Microsoft et Motorola, mais également des sociétés étrangères comme HTC ou LG.
