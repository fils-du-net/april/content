---
site: ZDNet.fr
title: "La Nasa se met au cloud computing open source avec OpenStack"
author: Thierry Noisette
date: 2010-07-22
href: http://www.zdnet.fr/blogs/l-esprit-libre/la-nasa-se-met-au-cloud-computing-open-source-avec-openstack-39753361.htm
tags:
- Le Logiciel Libre
- Entreprise
- Administration
- Innovation
- Informatique en nuage
- International
---

> La Nasa participe avec Dell, Intel et une vingtaine d'autres entreprises au projet de plateforme de cloud computing OpenStack. Dans la même ligne que ses contributions aux logiciels libres et sa diffusion de milliers de photos sans copyright.
