---
site: WebDevOnLinux
title: "Premier succès pour Meego : La plateforme In-Vehicle Infotainment   "
author: Steph
date: 2010-07-24
href: http://www.webdevonlinux.fr/2010/07/premier-succes-pour-meego-la-plateforme-in-vehicle-infotainment/
tags:
- Le Logiciel Libre
- Entreprise
- Innovation
---

> Ce consortium réunit le monde de l’électronique (Intel), des télécoms (Nokia, Samsung) et de l’automobile (BMW, GM, Nissan, PSA, Renault) et utilise le logiciel libre (open source Linux) pour intégrer plus facilement l’univers de la téléphonie et de l’infodivertissement embarqué (IVI).
