---
site: Le Monde.fr
title: "\"Plus d'un pour cent des sites utilisent Drupal\""
author: Damien Leloup
date: 2010-07-27
href: http://www.lemonde.fr/technologies/article/2010/07/27/plus-d-un-pour-cent-des-sites-utilisent-drupal_1392758_651865.html
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Administration
- Sensibilisation
- Promotion
---

> Dries Buytaert est le créateur de Drupal, un système de gestion de sites Web (CMS) libre – modifiable par tout utilisateur – et gratuit. Il est également cofondateur et responsable technologique d'Acquia, une société de services liés à Drupal.
