---
site: Numerama
title: "Le Parti pirate veut héberger The Pirate Bay au parlement suédois"
author: Julien L.
date: 2010-07-02
href: http://www.numerama.com/magazine/16142-le-parti-pirate-veut-heberger-the-pirate-bay-au-parlement-suedois.html
tags:
- Internet
- Europe
- International
---

> Après être devenu le nouvel hébergeur de The Pirate Bay, le Parti pirate suédois va tenter un nouveau coup médiatique : héberger le site directement dans l'enceinte du parlement suédois. Avec les prochaines élections en septembre, le parti politique aimerait bien donner une nouvelle dynamique après le grand succès obtenu aux élections européennes, l'année dernière.
