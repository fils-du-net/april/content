---
site: PCInpact
title: "Pôle Emploi : le logiciel libre, un choix massif et pragmatique"
author: Vincent Hermann
date: 2010-07-12
href: http://www.pcinpact.com/actu/news/58219-pole-emploi-migration-open-source-firefox-michel-brouant.htm
tags:
- Le Logiciel Libre
- Administration
---

> Suite à la publication de notre actualité sur l’utilisation générale de Firefox par IBM, une source nous a contactés pour nous signaler que d’importants travaux étaient en cours dans le Pôle Emploi. La structure était manifestement en train de s’appuyer fortement sur les logiciels libres. Nous avons donc contacté le Pôle Emploi pour en savoir davantage, et il y a effectivement des projets en cours de réalisation, mais certains sont en fait déjà en place.
