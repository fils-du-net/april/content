---
site: NZCS
title: "It's official: Software will be unpatentable in NZ"
author: Paul Matthews
date: 2010-07-15
href: http://www.nzcs.org.nz/news/blog.php?/archives/97-.html
tags:
- Institutions
- Brevets logiciels
- International
- English
---

> (Malgré les lobbies, les logiciels seront non brevetables en Nouvelle-Zélande) Despite what appears to be a big-budget lobbying effort by the pro-patent fraternity, Hon Simon Power announced today that he wouldn't be modifying the proposed Patents Bill hence software will be unpatentable once the Bill passes into law.
