---
site: ZDNet
title: "ACTA : les divisions européennes ne sont toujours pas tranchées"
author: Guénaël Pépin
date: 2010-07-13
href: http://www.zdnet.fr/actualites/acta-les-divisions-europeennes-ne-sont-toujours-pas-tranchees-39753145.htm
tags:
- Internet
- Institutions
- Neutralité du Net
- Europe
- ACTA
---

> Le sort des deux textes européens sera décidé dans quelques semaines. Le rapport Gallo demande plus de sévérité envers le téléchargement iillégal, quand la déclaration 12 s'oppose à cette vision répressive.
