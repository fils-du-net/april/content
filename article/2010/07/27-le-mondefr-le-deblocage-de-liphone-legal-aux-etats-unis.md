---
site: Le Monde.fr
title: "Le déblocage de l'iPhone légal aux Etats-Unis"
author: La rédaction
date: 2010-07-27
href: http://www.lemonde.fr/technologies/article/2010/07/27/le-deblocage-de-l-iphone-legal-aux-etats-unis_1392437_651865.html
tags:
- Entreprise
- Logiciels privateurs
- Interopérabilité
- Associations
- DRM
- Droit d'auteur
- International
---

> Le "jail breaking", l'action de supprimer les verrous informatiques empêchant l'utilisation de programmes qui ne sont pas approuvés par le constructeur d'un téléphone, est une pratique légale. C'est ce qu'a estimé (pdf), lundi 26 juillet, le Copyright Office (bureau du droit d'auteur) de la bibliothèque du Congrès américain, chargé d'arbitrer les questions de licences.
