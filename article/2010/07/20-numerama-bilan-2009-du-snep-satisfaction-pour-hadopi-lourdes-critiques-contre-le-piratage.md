---
site: Numerama
title: "Bilan 2009 du SNEP : satisfaction pour Hadopi, lourdes critiques contre le piratage"
author: Julien L.
date: 2010-07-20
href: http://www.numerama.com/magazine/16266-bilan-2009-du-snep-satisfaction-pour-hadopi-lourdes-critiques-contre-le-piratage.html
tags:
- Internet
- HADOPI
- Droit d'auteur
---

> Le Syndicat national des producteurs phonographiques (SNEP) a publié son guide de l'économie de la production musicale, version 2010. Dans celui-ci, le syndicat revient sur l'année écoulée, en saluant notamment l'arrivée de la loi Hadopi. Un rapport qui permet également au SNEP de dresser les lignes de ces prochains combats, allant de l'allongement des droits voisins en passant par la mise en place de solutions techniques pour réguler les flux sur Internet.
