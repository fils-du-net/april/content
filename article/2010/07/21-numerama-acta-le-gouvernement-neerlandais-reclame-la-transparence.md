---
site: Numerama
title: "ACTA : le gouvernement néerlandais réclame la transparence"
author: Julien L.
date: 2010-07-21
href: http://www.numerama.com/magazine/16279-acta-le-gouvernement-neerlandais-reclame-la-transparence.html
tags:
- Internet
- Institutions
- Neutralité du Net
- Europe
- International
- ACTA
---

> Deux ministres du gouvernement néerlandais sont à l'origine d'un courrier adressé au parlement des Pays-Bas. Dans leur missive, ils expliquent être favorable à la transparence du traité international anti-contrefaçon (ACTA), et qu'ils s'opposent dans le même temps à toute modification touchant le droit européen.
