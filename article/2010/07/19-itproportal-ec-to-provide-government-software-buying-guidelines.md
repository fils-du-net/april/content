---
site: ITProPortal
title: "EC To Provide Government Software Buying Guidelines"
author: La rédaction
date: 2010-07-19
href: http://www.itproportal.com/portal/news/article/2010/7/19/ec-provide-government-software-buying-guidelines/
tags:
- Le Logiciel Libre
- Interopérabilité
- Institutions
- Europe
- English
---

> (L'Europe définirait des règles d'achats des logiciels pour ses états membres) The European Commission is set to lay down software purchasing guidelines for the governments of its 27 member states, the New York Times has reported.
