---
site: ZDNet.fr
title: "L'Indonésie veut migrer massivement vers le logiciel libre"
author: Thierry Noisette
date: 2010-07-29
href: http://www.zdnet.fr/blogs/l-esprit-libre/l-indonesie-veut-migrer-massivement-vers-le-logiciel-libre-39753532.htm
tags:
- Le Logiciel Libre
- Administration
- Économie
- Sensibilisation
- Désinformation
- International
---

> Dans le quatrième pays le plus peuplé du monde, l'Indonésie (environ 240 millions d'habitants), le gouvernement avait lancé en 2004 une campagne, «Indonesia, Go Open Source» (son site - en indonésien), peut-être pas sans lien avec la décision de Microsoft l'année suivante de proposer un Windows XP allégé, XPSE, en Indonésie, ainsi qu'en Thaïlande et en Malaisie.
