---
site: Fraifrai.net
title: "Logiciel Libre et Culture libre : Biens communs et impact sociaux"
author: FraiFrai
date: 2010-07-24
href: http://www.fraifrai.net/index.php?2010/07/24/114-logiciel-libre-et-culture-libre-biens-communs-et-impact-sociaux
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Interopérabilité
- Partage du savoir
- Sensibilisation
- Contenus libres
---

> Pour mieux comprendre le concept de licence libre et ses enjeux, il suffit de l'appliquer à des domaines tout public. L'exemple le plus communément utilisé est celui de la cuisine car chacun dispose du droit de cuisiner librement, d'étudier des recettes, de les modifier.
