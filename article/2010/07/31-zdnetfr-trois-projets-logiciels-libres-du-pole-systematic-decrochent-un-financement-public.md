---
site: ZDNet.fr
title: "Trois projets logiciels libres du pôle Systematic décrochent un financement public"
author: Thierry Noisette
date: 2010-07-31
href: http://www.zdnet.fr/blogs/l-esprit-libre/trois-projets-logiciels-libres-du-pole-systematic-decrochent-un-financement-public-39753560.htm
tags:
- Le Logiciel Libre
- Entreprise
- Institutions
- Innovation
---

> Trois projets subventionnés, Compatible One, un "Cloudware" libre, Easy SOA, une plateforme d'intégration SOA libre, et Squash, pour des tests fonctionnels, représentent un investissement total de 17,5 millions d'euros. Nuxeo, qui participe à deux d'entre eux, a par ailleurs annoncé en juin une nouvelle levée de fonds de 2,7 millions.
