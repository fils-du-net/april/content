---
site: LinuxFr
title: "Motorola : une nouvelle étape dans l'ignominie ?"
author: patrick_g
date: 2010-07-16
href: http://linuxfr.org/2010/07/16/27129.html
tags:
- Le Logiciel Libre
- Entreprise
- Interopérabilité
- Informatique-deloyale
---

> Le web commence à bouillonner au sujet des téléphones Motorola Droid X (basés sur le noyau Linux) et qui sont supposés s'autodétruire si on tente de flasher le système. Tout est parti de cet article sur le site Mydroidworld.com. L'info a ensuite été reprise sur Mobilecrunch.com puis sur Slashdot et maintenant Harald Welte (spécialiste des smartphones s'il en est) en parle sur son blog.
