---
site: LeMonde.fr
title: "Michael Cross : \"Les données publiques doivent pouvoir être réutilisées librement\""
author: Laurent Checola et Damien Leloup
date: 2010-07-01
href: http://www.lemonde.fr/technologies/article/2010/07/01/michael-cross-les-donnees-publique-s-doivent-pouvoir-etre-reutilisees-librement_1381453_651865.html
tags:
- Administration
- Partage du savoir
- Contenus libres
---

> Michael Cross est journaliste au Guardian et l'un des responsables de la campagne "Free our data", qui milite pour davantage de transparence et une plus large diffusion des données publiques.
