---
site: ReadWriteWeb
title: "Wordpress pourrait trainer en justice le developpeur du thème Thesis"
author:  Chris Cameron et Fabrice Epelboin
date: 2010-07-19
href: http://fr.readwriteweb.com/2010/07/19/a-la-une/wordpress-pourrait-trainer-en-justice-developpeur-du-thme-thesis/
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Licenses
- Contenus libres
---

> Le débat fait rage au sein de la com­mu­nauté open source ces der­niers temps autour d’une affaire dont l’issu pour­rait avoir de lourdes impli­ca­tion pour ce modèle de par­tage et de dis­tri­bu­tion dans le monde du logi­ciel.
