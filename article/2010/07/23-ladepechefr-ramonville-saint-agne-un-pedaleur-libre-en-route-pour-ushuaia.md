---
site: LADEPECHE.fr
title: "Ramonville-Saint-Agne. Un «pédaleur libre» en route pour Ushuaia"
author: La rédaction
date: 2010-07-23
href: http://www.ladepeche.fr/article/2010/07/23/877885-Ramonville-Saint-Agne-Un-pedaleur-libre-en-route-pour-Ushuaia.html
tags:
- Le Logiciel Libre
- Licenses
- Promotion
---

> A l'heure où se déroule Le Tour de France cycliste, Pierre Virlojeux, un Ramonvillois de 53 ans, effectue ses 60 kilomètres quotidien en tricycle couché à 28 vitesses sur un trajet qui va de Vancouver à Ushuaia. Pour ces 15 000 kilomètres de parcours en « Grand Pédaleur Libre » (GPL) commencés au printemps, cet ingénieur informatique au CNES a économisé sur ses congés et posé une année sabbatique. Pierre Virlojeux utilise actuellement son trike en autonomie maximale, c'est à dire en utilisant le moins possible les modes de transports motorisés.
