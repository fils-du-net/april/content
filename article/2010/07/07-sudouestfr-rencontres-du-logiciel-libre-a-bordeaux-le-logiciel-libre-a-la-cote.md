---
site: SudOuest
title: "Rencontres du Logiciel Libre à Bordeaux : le logiciel libre a la cote "
author: Bruno Béziat
date: 2010-07-07
href: http://www.sudouest.fr/2010/07/07/rencontres-du-logiciel-libre-a-bordeaux-le-logiciel-libre-a-la-cote-134316-2780.php
tags:
- Le Logiciel Libre
- Promotion
---

> Les promoteurs de ces logiciels utilisables librement et sans restriction, modèles de plus en plus répandus, sont à Bordeaux.
