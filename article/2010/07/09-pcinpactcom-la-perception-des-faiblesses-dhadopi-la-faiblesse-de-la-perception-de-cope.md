---
site: PCInpact
title: "La perception des faiblesses d'Hadopi, la faiblesse de la perception de Copé"
author: Marc Rees
date: 2010-07-09
href: http://www.pcinpact.com/actu/news/58167-jeanfrancois-cope-hadopi-perception-faiblesse.htm
tags:
- HADOPI
- Institutions
---

> Jean François Copé qui renie du bout des ongles Hadopi… c’est cette petite saynète que racontent Clubic et Le Figaro notamment. Cela s’est passé mercredi à l’Assemblée nationale pour une nouvelle rencontre du groupe UMP Éthique du Numérique.
