---
site: ReadWriteWeb
title: "Un groupe de presse américain passe à l’open source"
author: Curt Hopkins
date: 2010-07-30
href: http://fr.readwriteweb.com/2010/07/30/nouveautes/groupe-presse-americain-open-source/
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- International
---

> La bataille entre l’open source face aux logi­ciels pro­prié­taires vient de ral­lier un acteur de poids dans son camp : le Journal Register Company, un groupe de presse amé­ri­cain qui compte 170 parutions.
