---
site: Numerama
title: "Hadopi : il n'est plus question de tribunaux spéciaux en région"
author: Guillaume Champeau
date: 2010-07-06
href: http://www.numerama.com/magazine/16181-hadopi-il-n-est-plus-question-de-tribunaux-speciaux-en-region.html
tags:
- HADOPI
- Institutions
---

> Annoncée il y a un an, la création de tribunaux spécialisés en région pour traiter les milliers de dossiers d'amendes ou de suspension d'accès à Internet des abonnés avertis par l'Hadopi n'est plus à l'ordre du jour, a-t-on appris aujourd'hui.
