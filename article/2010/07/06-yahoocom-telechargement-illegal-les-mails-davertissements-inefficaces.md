---
site: Yahoo actualités
title: "Téléchargement illégal : les mails d'avertissements inefficaces ?"
author: La rédaction
date: 2010-07-06
href: http://fr.news.yahoo.com/68/20100706/tsc-tlchargement-illgal-les-mails-d-aver-04aaa9b.html
tags:
- Internet
- HADOPI
- International
---

> États-Unis - Les mails envoyés par les autorités aux internautes soupçonnés de piratage semble ne pas avoir d'impact. Le trafic P2P poursuit en effet sur sa lancée de manière stable. C'est du moins le cas aux États-Unis.
