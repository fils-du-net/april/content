---
site: tekiano.com
title: "Piratage en Tunisie : Bon pour Windows, mauvais pour l’Open source ?"
author: Welid Naffati
date: 2010-07-28
href: http://www.tekiano.com/tek/tek-news/3-4-2483/piratage-en-tunisie-bon-pour-windows-mauvais-pour-l-open-source-.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Économie
- Institutions
- Sensibilisation
- International
---

> Pour sauver les logiciels libres, les SSII demandent au ministère des TIC de combattre le piratage. Les stats d’utilisation de l’Open source dans notre pays sont en effet peu satisfaisantes malgré les efforts entrepris depuis près de dix ans. Un récent rapport dresse un état des lieux alarmant pour la survie du “Gnu” en Tunisie.
