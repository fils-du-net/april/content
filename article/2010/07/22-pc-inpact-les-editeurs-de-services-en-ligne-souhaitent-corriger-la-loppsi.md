---
site: PC INpact
title: "Les éditeurs de services en ligne souhaitent corriger la LOPPSI"
author: Marc Rees
date: 2010-07-22
href: http://www.pcinpact.com/actu/news/58425-geste-loppsi-blocage-identite-internet.htm
tags:
- HADOPI
- Institutions
- Droit d'auteur
- Neutralité du Net
---

> Le Groupement des Editeurs de Services En Ligne (*) vient de prendre officiellement position sur le projet de loi LOPPSI 2, qui sera examiné au Sénat à la rentrée. Du moins, certains des grands chapitres de ce texte qui touchent aux univers numériques.
