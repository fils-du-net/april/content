---
site: Numerama
title: "Le Chili vote une loi sur la neutralité du net, une première mondiale"
author: Julien L.
date: 2010-07-16
href: http://www.numerama.com/magazine/16252-le-chili-vote-une-loi-sur-la-neutralite-du-net-une-premiere-mondiale.html
tags:
- Internet
- Institutions
- Neutralité du Net
- International
---

> Pour la première fois au monde, un pays a voté une législation en faveur de la neutralité du net. Et il ne s'agit nullement d'un pays européen ou nord-américain, mais du Chili. Le texte, déposé en 2007 à la chambre des députés du Chili, a pour ambition de garantir aux internautes chiliens un accès libre et non-faussé au réseau Internet.
