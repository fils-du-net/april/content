---
site: PROgrammez!
title: "Alveole, une nouvelle plateforme collaborative de production de logiciels libres solidaires et responsables"
author: Frédéric Mazué
date: 2010-07-08
href: http://www.programmez.com/actualites.php?titre_actu=Alveole-une-nouvelle-plateforme-collaborative-de-production-de-logiciels-libres-solidaires-et-responsables&id_actu=7861
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Associations
---

> L'Association Internationale du Logiciel Libre (Ai2L), ses fondateurs français – le Crédit Coopératif, la Macif, le Groupe Chèque Déjeuner – et ses fondateurs québécois – Fondaction, Filaction et la Caisse d'économie solidaire –annoncent la mise en service d'une plateforme collaborative de production de logiciels libres solidaires et responsables : « Alveole ».
