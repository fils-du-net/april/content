---
site: WebTimesMedia
title: "SophiaConf 2010 : le nombre de mille participants dépassé !"
author: La rédaction
date: 2010-07-23
href: http://www.webtimemedias.com/webtimemedias/wtm_article56262.fr.htm
tags:
- Le Logiciel Libre
- Entreprise
- Sensibilisation
- Associations
---

> Le pari d'organiser un grand événement technique sur la technopole, lancé en janvier autour de Pascal Flamand, responsable de la commission Open Source de Telecom Valley, a été gagné. Plus de 1.000 participants ont été enregistrés pour les 20 conférences très pointues organisées sur dix jours par les principaux animateurs de l'écosystème de Sophia Antipolis.
