---
site: ZDNet.fr
title: "Google Android: utilisations militaires "
author: La rédaction
date: 2010-07-23
href: http://www.zdnet.fr/blogs/media-tech/google-android-utilisations-militaires-39753391.htm
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Administration
- Licenses
- Promotion
---

> Le choix de la licence très permissive Apache 2.0 de Google Android était intentionnel: il s'agissait de permettre l'utilisation la plus large possible du système sans imposer aucune réelle contrainte à ceux qui l'adoptent. Ils peuvent en particulier le rendre propriétaire: les modifications apportées ne doivent pas nécessairement être retournées à la communauté comme l'impose les licenses "virales" de type GPL.
