---
site: Numerama
title: "Neutralité du net : l'ARCEP ne peut pas éluder la question des libertés fondamentales"
author: Julien L.
date: 2010-07-13
href: http://www.numerama.com/magazine/16234-neutralite-du-net-l-arcep-ne-peut-pas-eluder-la-question-des-libertes-fondamentales.html
tags:
- Internet
- Institutions
- Neutralité du Net
---

> Le fournisseur d'accès à Internet FDN a publié aujourd'hui son avis sur la consultation publique initié par l'ARCEP, à propos de la neutralité du net. Pour le FAI, il est important de ne pas évacuer la question des libertés fondamentales (notamment la liberté d'expression) au profit d'autres enjeux, comme les facteurs économiques ou techniques.
