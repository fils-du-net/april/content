---
site: PCInpact
title: "Hadopi, moyen de sécurisation et motif de non-sécurisation"
author: Marc Rees
date: 2010-07-10
href: http://www.pcinpact.com/actu/news/58194-securisation-nonsecurisation-motif-legitime-.htm
tags:
- HADOPI
---

> Nous avons lancé voilà quelques jours un petit concours sur les moyens de sécurisation dans le cadre de la loi Hadopi.
> [...] Voilà peu, la Hadopi a donné cet exemple : « Si une mère met l'ordinateur dans un placard sous clé pour empêcher son fils de télécharger et que cela marche, c'est un moyen de sécurisation, pas besoin d'installer un logiciel ».
