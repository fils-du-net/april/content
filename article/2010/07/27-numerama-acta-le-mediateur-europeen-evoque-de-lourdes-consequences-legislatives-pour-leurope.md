---
site: Numerama
title: "ACTA : le médiateur européen évoque de \"lourdes conséquences législatives\" pour l'Europe"
author: Julien L.
date: 2010-07-27
href: http://www.numerama.com/magazine/16326-acta-le-mediateur-europeen-evoque-de-lourdes-consequences-legislatives-pour-l-europe.html
tags:
- Institutions
- Droit d'auteur
- International
- ACTA
---

> L'ACTA ne modifiera pas le droit européen. C'était ce qu'affirmait Bruxelles il y a encore quelques mois. Malheureusement pour l'exécutif européen, les inquiétudes autour du projet de traité international sont toujours vivaces. Dernièrement, c'est le médiateur européen qui a exprimé une inquiétude sur l'impact de l'ACTA dans l'Union européenne.
