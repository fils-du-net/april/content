---
site: Ledevoir.com
title: "Vive le logiciel libre!"
author: Fabien Deglise
date: 2010-07-08
href: http://www.ledevoir.com/societe/justice/292187/vive-le-logiciel-libre
tags:
- Le Logiciel Libre
- Entreprise
- Administration
- Institutions
- International
---

> Québec n'interjette pas appel du jugement confirmant l'illégalité des attributions de contrats de licences informatiques sans appel d'offres
