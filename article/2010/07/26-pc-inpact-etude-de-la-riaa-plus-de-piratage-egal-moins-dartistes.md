---
site: PC INpact
title: "Étude de la RIAA : plus de piratage égal moins d'artistes"
author: Jeff
date: 2010-07-26
href: http://www.pcinpact.com/actu/news/58477-analyse-piratage-nombre-artistes-riaa.htm
tags:
- Internet
- HADOPI
- Désinformation
- Droit d'auteur
- International
---

> Les chercheurs américains Felix Oberholzer-Gee et Koleman Strumpf avaient publié en juin une étude expliquant comment l'augmentation du piratage d'œuvres artistiques avait permis une explosion de la création.
