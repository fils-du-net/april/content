---
site: Framablog
title: "Centre de Formation Logiciels Libres : Demandez le programme !"
author: Alexis kaufman
date: 2010-02-25
href: http://www.framablog.org/index.php/post/2010/02/25/centre-de-formation-logiciels-libres-programme
tags:
- Le Logiciel Libre
---

> En décembre dernier nous vous annoncions la création d’un Centre de Formation Logiciels Libres (ou CF2L) dans le cadre de l’Université numérique Paris Île-de-France (UNPIdF), par l’entremise de Thierry Stœhr interviewé pour l’occasion.
