---
site: zdnet.fr
title: "Scoop : Google donne deux millions de dollars à Wikipédia"
author: Thierry Noisette 
date: 2010-02-17
href: http://www.zdnet.fr/blogs/l-esprit-libre/google-donne-deux-millions-de-dollars-a-wikipedia-39713111.htm
tags:
- Partage du savoir
- Droit d'auteur
---

> [...] "Wikipédia est un des plus grands triomphes d'Internet, estime le cofondateur de Google Sergey Brin. "Cette vaste mine de de contenu créé collectivement est une ressource d'une valeur incalculable pour quiconque est en ligne."
