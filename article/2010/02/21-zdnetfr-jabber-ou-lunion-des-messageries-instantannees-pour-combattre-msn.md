---
site: zdnet.fr
title: "Jabber ou l'union des messageries instantannées pour combattre MSN"
author: Christophe Sauthier
date: 2010-02-21
href: http://www.zdnet.fr/blogs/ubuntu-co/jabber-ou-l-union-des-messageries-instantannees-pour-combattre-msn-39713218.htm
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
- Interopérabilité
---

> [...] Jabber, technologie utilisant des protocoles ouverts, peut devenir le nouveau leader du marché. Et ça, c'est forcément une hypothèse enthousiasmante dans une optique de progression des logiciels libres.
