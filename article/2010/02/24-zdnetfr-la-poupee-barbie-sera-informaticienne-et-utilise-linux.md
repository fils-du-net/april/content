---
site: zdnet.fr
title: "La poupée Barbie sera informaticienne... et utilise Linux"
author: Thierry Noisette 
date: 2010-02-24
href: http://www.zdnet.fr/blogs/l-esprit-libre/la-poupee-barbie-sera-informaticienne-et-utilise-linux-39713313.htm
tags:
- Le Logiciel Libre
- Sensibilisation
---

> Le géant américain du jouet Mattel avait lancé un vote sur Internet pour choisir la prochaine (et 125e) profession de sa poupée Barbie. Parmi les carrières proposées, il y avait «informaticienne» (computer engineer). Résultat, un bel écho chez les geeks et surtout geekettes, comme le rapporte la directrice du marketing de Barbie,  Lauren Dougherty, qui indique que le site a été inondé de votes de femmes dans l'industrie high tech, et bien sûr de femmes informaticiennes du monde entier.
