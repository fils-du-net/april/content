---
site: "h-online.com"
title: "SourceForge turns off \"blanket blocking\""
author: La rédaction
date: 2010-02-08
href: http://www.h-online.com/open/news/item/SourceForge-turns-off-blanket-blocking-924021.html
tags:
- Le Logiciel Libre
- Entreprise
---

> SourceForge, l'hébergeur de projets libres, vient d'annoncer avoir supprimé le blocage des accès en provenance de Cuba, l'Iran, La Corée du Nord, le Soudan et la Sirie.
