---
site: guardian.co.uk
title: "When using open source makes you an enemy of the state"
author: Bobbie Johnson
date: 2010-02-23
href: http://www.guardian.co.uk/technology/blog/2010/feb/23/opensource-intellectual-property
tags:
- Le Logiciel Libre
- Administration
- Désinformation
---

> Quand les lobbies du dogme de la propriété intellectuelle tentent de faire lister des pays utilisant des logiciels libres dans le « 301 Report », liste noire de la chambre américaine de commerce.
