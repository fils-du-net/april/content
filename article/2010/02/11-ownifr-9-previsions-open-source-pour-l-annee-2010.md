---
site: owni.fr
title: "9 prévisions open source pour l’année 2010"
author: aKa (Framasoft) 
date: 2010-02-11
href: http://owni.fr/2010/02/11/9-previsions-open-source-pour-lannee-2010/
tags:
- Le Logiciel Libre
---

> [...] Je pense que nous pouvons considérer comme acquis que l’open source va continuer à gagner en popularité. 2010 ne sera pas cette légendaire « Année où Linux se démocratise », mais nous devrions continuer à observer la même lente et constante progression de son adoption que dans la dernière décennie.
