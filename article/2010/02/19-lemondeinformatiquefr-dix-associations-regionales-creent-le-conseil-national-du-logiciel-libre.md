---
site: lemondeinformatique.fr
title: "Dix associations régionales créent le Conseil national du logiciel libre"
author: Bertrand Lemaire
date: 2010-02-19
href: http://www.lemondeinformatique.fr/actualites/lire-dix-associations-regionales-creent-le-conseil-national-du-logiciel-libre-29983.html
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] Le CNLL vient s'ajouter à la FNILL (Fédération Nationale de l'Industrie du Logiciel Libre) où l'on trouve depuis des années les poids lourds du secteur, de Bull à Linagora, mais aussi des PME et des TPE. Sur sa Foire aux Questions, le CNLL précise clairement : « Ces associations et ces entreprises ont décidé de créer le CNLL car elles ne se reconnaissent pas dans le mode de gouvernance qui a été mis en place au sein de la FNILL. »
