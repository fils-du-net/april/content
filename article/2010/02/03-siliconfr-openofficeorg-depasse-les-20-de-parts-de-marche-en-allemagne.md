---
site: silicon.fr
title: "OpenOffice.org dépasse les 20 % de parts de marché en Allemagne"
author: David Feugey 
date: 2010-02-03
href: http://www.silicon.fr/fr/news/2010/02/03/openoffice_org_depasse_les_20___de_parts_de_marche_en_allemagne
tags:
- Le Logiciel Libre
- Entreprise
---

> Le site Webmasterpro.de  vient de publier une étude intéressante [...] Microsoft Office est le leader  du marché, avec 72 % d’utilisateurs. OpenOffice.org et ses dérivés (StarOffice, Lotus Symphony, NeoOffice, etc.) sont présents chez 21,5 % des utilisateurs. Un excellent résultat. D’autres acteurs comme Corel, Apple, SoftMaker et KOffice se partagent le reste du marché. L’étude montre aussi que Microsoft Office est plus favorisé en entreprise, alors qu’OpenOffice.org reste privilégié par les particuliers.
