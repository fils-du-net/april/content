---
site: 01net.com
title: "Pour Steve Jobs, Flash est aussi ringard que les disquettes"
author: Hélène Puel
date: 2010-02-19
href: http://www.01net.com/editorial/512810/pour-steve-jobs-flash-est-aussi-ringard-que-les-disquettes/
tags:
- Logiciels privateurs
- Interopérabilité
- Neutralité du Net
---

> A ceux qui s'étonnaient encore de l'absence du format Flash en natif dans l'iPhone et l'iPad, Steve Jobs vient d'en donner la raison. Lors d'une rencontre avec le staff du Wall Street Journal, il aurait déclaré, à propos du format d'Adobe : « Nous ne passons pas de temps sur une technologie dépassée. »
