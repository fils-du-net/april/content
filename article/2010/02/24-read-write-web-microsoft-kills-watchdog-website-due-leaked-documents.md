---
site: Read Write Web
title: "Microsoft Kills Watchdog Website Due to Leaked Documents"
author: Jolie O'Dell
date: 2010-02-24
href: http://www.readwriteweb.com/archives/_improper_use_of_copyright.php
tags:
- Logiciels privateurs
- DADVSI
- DRM
- Informatique-deloyale
- English
---

> Microsoft s'est servi du DMCA pour déconnecter le site cryptome, qui qui publié des fuites montrant comment Microsoft facilite l'espionnage des clients.
