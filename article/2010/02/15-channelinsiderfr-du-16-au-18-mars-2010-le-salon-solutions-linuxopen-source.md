---
site: channelinsider.fr
title: "Du 16 au 18 mars 2010: le salon Solutions Linux/Open Source"
author: Gérard Clech
date: 2010-02-15
href: http://www.channelinsider.fr/fr/actualite/2010/02/15/du_16_au_18_mars_2010__le_salon_solutions_linux_open_source
tags:
- Entreprise
- April
---

> [...] Parmi les nouveautés 2010, le village April. Tarsus France, organisateur du salon, s'est en effet associé à l'April afin de proposer aux 450 entreprises membres de l'association un espace situé au coeur du salon. Chaque entreprise exposante et membre de l'association bénéficiera d'un emplacement dédié et du service exposant. Afin de valoriser l'actualité des entreprises membres de l'April, chaque exposant du village pourra de plus organiser une session de prise de parole de 25 minutes.
