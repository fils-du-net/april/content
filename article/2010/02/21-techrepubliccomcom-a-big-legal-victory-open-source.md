---
site: techrepublic.com.com
title: "A big legal victory for open source"
author: Jack Wallen 
date: 2010-02-21
href: http://blogs.techrepublic.com.com/opensource/?p=1294
tags:
- Le Logiciel Libre
- Droit d'auteur
- English
---

> Un jugement aux USA autour d'une violation de la licence GNU GPL vient d'être obtenu et est très favorable pour les prochains cas de non respect de cette licence.
