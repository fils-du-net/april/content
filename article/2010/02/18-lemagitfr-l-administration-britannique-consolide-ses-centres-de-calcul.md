---
site: lemagit.fr
title: "L’administration britannique consolide ses centres de calcul "
author: Valery Marchive 
date: 2010-02-18
href: http://www.lemagit.fr/article/hp-ibm-wipro-tcs-cloud-computing-royaume-uni-centre-calcul/5653/1/l-administration-britannique-consolide-ses-centres-calcul/
tags:
- Le Logiciel Libre
- Administration
- Informatique en nuage
---

> [...] d’ici 2015, 80 % des postes de travail des administrations dépendant directement du gouvernement britannique devraient accéder à une offre de services partagés sur un mode proche de celui du client léger. Le tout basé sur le monde du logiciel libre ; de quoi économiser quelques centaines de millions de livres sterling supplémentaires en maintenance et en exploitation, chaque année.
