---
site: zdnet.fr
title: "Logiciel libre : Tangui Morlier prend la tête de l'April"
author: Christophe Auffray
date: 2010-02-17
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39713112,00.htm
tags:
- April
---

> [...] Dans un communiqué, l'April annonce l'élection de son nouveau conseil d'administration, qui sera en 2010 présidé par Tangui Morlier. Ce dernier n'est pas un inconnu dans le monde du logiciel libre puisqu'il était déjà administrateur de l'association depuis 2007 et membre de Copyleft Attitude et de StopDRM.
