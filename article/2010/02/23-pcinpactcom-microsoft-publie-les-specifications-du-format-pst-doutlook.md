---
site: pcinpact.com
title: "Microsoft publie les spécifications du format PST d'Outlook"
author: Vincent Hermann
date: 2010-02-23
href: http://www.pcinpact.com/actu/news/55527-microsoft-outlook-pst-specifications-techniques-documentation.htm
tags:
- Logiciels privateurs
- Interopérabilité
---

> [...] Microsoft indique que les requêtes des utilisateurs pour accéder plus facilement aux données incluses dans les PST se sont faites de plus en plus pressantes. Cela concerne aussi bien les courriers électroniques que les agendas, en passant par les contacts et les tâches. La publicisation de la documentation doit aider les développeurs et les entreprises à mieux importer les données depuis les fichiers PST dans leurs propres applications et services.
