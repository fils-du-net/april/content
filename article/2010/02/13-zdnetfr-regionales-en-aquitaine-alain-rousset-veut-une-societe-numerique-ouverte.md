---
site: Zdnet.fr
title: "Régionales: en Aquitaine, Alain Rousset veut \"une société numérique ouverte\""
author: Thierry noisette
date: 2010-02-13
href: http://www.zdnet.fr/blogs/l-esprit-libre/regionales-en-aquitaine-alain-rousset-veut-une-societe-numerique-ouverte-39712956.htm
tags:
- Administration
---

> Dans le projet pour l'Aquitaine, le candidat socialiste et président de région sortant, Alain Rousset, propose la création d'un pôle d'excellence en logiciels libres. Un projet à rapprocher de la présence sur sa liste d'un militant libriste de longue date, François Pellegrini.
