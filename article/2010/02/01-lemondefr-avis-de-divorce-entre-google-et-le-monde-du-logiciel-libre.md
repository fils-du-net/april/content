---
site: LeMonde.fr
title: "Avis de divorce entre Google et le monde du logiciel libre"
author: Damien Leloup
date: 2010-02-01
href: http://www.lemonde.fr/technologies/article/2010/02/01/avis-de-divorce-entre-google-et-le-monde-du-logiciel-libre_1299717_651865.html
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- Partage du savoir
- Philosophie GNU
---

> Il y a d'abord eu les déclarations d'Eric Schmidt, le PDG de Google, sur la vie privée : "Si vous souhaitez que personne ne soit au courant de certaines choses que vous faites, peut-être que vous ne devriez tout simplement pas les faire."
