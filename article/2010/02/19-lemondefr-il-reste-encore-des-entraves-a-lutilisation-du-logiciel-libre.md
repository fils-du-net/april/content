---
site: lemonde.fr
title: "\"Il reste encore des entraves à l'utilisation du logiciel libre\""
author: Damien Leloup
date: 2010-02-19
href: http://www.lemonde.fr/technologies/article/2010/02/19/il-reste-encore-des-entraves-a-l-utilisation-du-logiciel-libre_1308876_651865.html
tags:
- Le Logiciel Libre
- April
---

> [...] Parmi les priorités de l'association pour les cinq ans à venir, vous prévoyez de mettre l'accent sur le rôle du logiciel libre à l'école. Qu'entendez-vous par là ?
