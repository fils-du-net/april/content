---
site: Le Journal du Net
title: "David Sapiro (Conseil National du Logiciel Libre) \"Nous fédérons plus de 200 entreprises à travers la France\""
author: Interview de David Sapiro
date: 2010-02-24
href: http://www.journaldunet.com/solutions/intranet-extranet/david-sapiro-le-defi-du-conseil-national-du-logiciel-libre.shtml
tags:
- Le Logiciel Libre
---

> Promouvoir le Libre, dynamiser les synergies, répondre à des appels d'offres en commun... Tels sont les principaux objectifs du nouveau groupement français créé il y a moins d'un mois.
