---
site: commentcamarche.net
title: "Patrick Lerouge, de l'Inserm : « Des standards ouverts pour ne pas bloquer notre développement »"
author: La rédaction
date: 2010-02-19
href: http://www.commentcamarche.net/news/5851415-patrick-lerouge-de-l-inserm-des-standards-ouverts-pour-ne-pas-bloquer-notre-developpement
tags:
- Le Logiciel Libre
- Administration
---

> CCM - Vous avez désormais 9 ans de recul sur l'implantation de Linux à l'Inserm, qu'est-ce qui a vraiment changé ?
> PL - Au sein du DSI, nous avons constaté un gain de performance sur les systèmes informatiques, une sécurité et une stabilité accrue. Cette évolution a conforté la culture du logiciel libre à l'Inserm, que nous avions déjà acquise avec l'utilisation du logiciel de serveur Apache. Nos systèmes ouverts ont des paramétrages simples qui offrent à l'administrateur une bonne maîtrise des logiciels. Nous pouvons compter sur une large communauté, très réactive, qui intervient en cas de problème sur tel ou tel logiciel.
