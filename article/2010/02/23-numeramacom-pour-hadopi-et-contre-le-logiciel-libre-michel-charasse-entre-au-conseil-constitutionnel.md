---
site: Numerama.com
title: "Pour Hadopi et contre le logiciel libre, Michel Charasse entre au Conseil constitutionnel"
author: Guillaume Champeau
date: 2010-02-23
href: http://www.numerama.com/magazine/15133-pour-hadopi-et-contre-le-logiciel-libre-michel-charasse-entre-au-conseil-constitutionnel.html
tags:
- HADOPI
---

> En nommant le sénateur Michel Charasse au Conseil Constitutionnel, Nicolas Sarkozy apporte une voix dissidente aux soutiens de la liberté d'expression sur Internet. L'ancien socialiste est favorable à l'Hadopi qu'avait censurée le Conseil, et contre la protection du logiciel libre face aux logiciels propriétaires.
