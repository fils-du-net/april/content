---
site: pcinpact.com
title: "Office 2010 : l'écran de sélection des formats, un choix biaisé ?"
author: Vincent Hermann
date: 2010-02-19
href: http://www.pcinpact.com/actu/news/55481-office-2010-ballot-screen-selection-format-odf-ooxml.htm
tags:
- Logiciels privateurs
- Interopérabilité
---

> [...] Chez Microsoft, on en est seulement à la première version de cet écran de sélection, et les premiers retours sont négatifs. Richard Stallman, figure emblématique du monde du logiciel libre, n’y va pas par quatre chemins : « Il est clair que Microsoft essaye de faire de l’ODF un mauvais choix apparent, pour que peu d’utilisateurs se servent du format. J’en conclus que Microsoft veut pouvoir dire qu’il offre le support de l’ODF, tout en ayant peu d’utilisateurs qui s’en servent. »
