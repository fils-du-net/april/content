---
site: pcinpact.com
title: "Linux privé de Jeux Olympiques sur France 2, éconduit sur Bing"
author: Nil Sanyas
date: 2010-02-17
href: http://www.pcinpact.com/actu/news/55434-jeux-olympiques-france-televisions-bing-ubuntu.htm
tags:
- Internet
- Logiciels privateurs
- Interopérabilité
- Partage du savoir
---

> Mise à jour : France Télévisions a vivement réagi à notre article en modifiant son script.
