---
site: zdnet.fr
title: "Les changements à la tête de Canonical se précisent"
author: Christophe Sauthier
date: 2010-02-12
href: http://www.zdnet.fr/blogs/ubuntu-co/les-changements-a-la-tete-de-canonical-se-precisent-39712996.htm
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] Avec cette arrivée Canonical recrute donc un vieux routard de l'Open Source professionnel, depuis Lineo (société qu'il a fondée) à Novell et dernièrement chez l'éditeur de la GED Alfresco. Mais avec cette arrivée, Canonical va sûrement encore une fois s'attirer les foudres de quelques membres de la communauté du libre, puisque Matt Asay est également connu pour ses positions assez critiques par rapport à Richard Stallman et la Free Software Foundation.
