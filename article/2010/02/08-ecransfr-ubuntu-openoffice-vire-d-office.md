---
site: ecrans.fr
title: "Ubuntu : OpenOffice viré d’office"
author: Camille Gévaudan
date: 2010-02-08
href: http://www.ecrans.fr/Ubuntu-OpenOffice-vire-d-office,9144.html
tags:
- Le Logiciel Libre
---

> C’est un sacré ménage de printemps que va connaître la prochaine version d’Ubuntu, Lucid Lynx, à sa sortie en avril 2010. On avait appris en novembre que GIMP, la plus sérieuse alternative libre à Photoshop, disparaîtra de la liste des programmes pré-installés sur la populaire distribution de Linux. [....] Vendredi, le site Digitizor rapportait qu’un autre logiciel incontournable pourrait être partiellement laissé de côté : OpenOffice (OpenOffice.org, ou OOo pour les intimes).
