---
site: 01net.com
title: "Symbian devient complètement open source"
author: Renaud Bonnet
date: 2010-02-04
href: http://pro.01net.com/editorial/512234/symbian-devient-completement-open-source/
tags:
- Le Logiciel Libre
- Entreprise
---

> Avec quatre bons mois d'avance sur son calendrier, Nokia ouvre l'intégralité du code source du système d'exploitation Symbian, celui qui équipe ses ordiphones. L'opération a été conduite à marche forcée, puisque fin octobre dernier, il restait encore à ouvrir 118 des 134 paquets constituant la plate-forme complète. Une nouvelle génération de terminaux, basée sur les versions open source de Symbian OS, devrait voir le jour début 2011.
