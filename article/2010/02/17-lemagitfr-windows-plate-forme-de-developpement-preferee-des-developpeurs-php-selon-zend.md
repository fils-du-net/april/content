---
site: lemagit.fr
title: "Windows : plate-forme de développement préférée des développeurs PHP, selon Zend "
author: Cyrille Chausson 
date: 2010-02-17
href: http://www.lemagit.fr/article/windows-web2-0-developpement-php-framework/5632/1/windows-plate-forme-developpement-preferee-des-developpeurs-php-selon-zend/
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
---

> [...] Lors d’un sondage mené auprès des utilisateurs du framework PHP de la marque, 42% des répondants ont déclaré utiliser Windows pour leurs phases de développement contre 38,5% qui préfèrent quant à eux Linux. 19,1% ont de leur côté choisi Mac OS X.
> Lorsqu’on aborde l’environnement de production, la grande majorité des répondants choisissent Linux (à 85% ) alors que seulement 11% ont cité Windows et un petit 2%, Mac OS X.
