---
site: silicon.fr
title: "La neuvième fête du logiciel libre saluera l’arrivée du printemps"
author: David Feugey
date: 2010-02-23
href: http://www.silicon.fr/fr/news/2010/02/23/la_neuvieme_fete_du_logiciel_libre_saluera_l_arrivee_du_printemps
tags:
- Le Logiciel Libre
- Internet
- Administration
- April
---

> [...] « La délégation aux usages de l'Internet a plaisir à s’associer de nouveau à l'événement Libre en Fête cette année. En effet, les logiciels libres jouent un rôle essentiel dans le développement des technologies qui permettent la diffusion des contenus de l´Internet.
