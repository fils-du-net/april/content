---
site: fluctuat.net
title: "Google et le logiciel libre, début d’une scission ?"
author: Mathias Riquier
date: 2010-02-10
href: http://www.fluctuat.net/7055-Google-face-a-l-open-source
tags:
- Le Logiciel Libre
- Internet
---

> Ces dernières semaines, Google semble montrer au monde qu’il ne joue plus avec les mêmes règles. Les déclarations et les petits changements de position de notre ami numérique le plus intime n’enchantent pas particulièrement la communauté du libre et de l’open source, avec laquelle Google a maintenu pendant longtemps une relation saine. On en cause avec Alexis Kauffmann de Framasoft.
