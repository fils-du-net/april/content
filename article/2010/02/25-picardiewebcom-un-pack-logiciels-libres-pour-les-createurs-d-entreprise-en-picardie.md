---
site: picardieweb.com
title: "Un pack 'Logiciels libres' pour les créateurs d’entreprise en Picardie"
author: Charline Galls
date: 2010-02-25
href: http://www.picardieweb.com/article-picardie-un-pack-logiciels-libres-pour-les-createurs-dentreprise-en-picardie-1289.htm
tags:
- Le Logiciel Libre
- Économie
---

> La Chambre de Métiers et de l’Artisanat de la Somme, en partenariat avec le Conseil Régional de Picardie, a créé début 2010 un pack de logiciels libres à destination des créateurs de TPE. Un outil présenté comme une 'première' pour cette clientèle spécifique.
> Né après les Rencontres Mondiales du Logiciel Libre, organisées en 2007 à Amiens, le pack 'Logiciels Libres pour Créateur Repreneur d'Entreprise Artisanale' est une sorte de 'boîte à outils' très pratique pour le démarrage d’une activité: Bureautique, comptabilité et gestion commerciale... Le pack regroupe une petite dizaine de logiciels, pour lesquels une formation est assurée par la Chambre des Métiers.
