---
site: rue89.com
title: "Wikipédia : est-on vraiment plus intelligent à plusieurs ? "
author: Books
date: 2010-02-25
href: http://www.rue89.com/2010/02/25/wikipedia-est-on-vraiment-plus-intelligent-a-plusieurs-137517
tags:
- Le Logiciel Libre
- Partage du savoir
- Contenus libres
---

> L'universitaire américain Paul Duguid, spécialiste des médias, a travaillé sur Wikipédia. Il évoque le modèle de savoir démocratique de l'encyclopédie en ligne et ses limites.
> [...] Benkler exalte la culture des logiciels libres. Il tire de ce milieu l'idée que, si un projet peut être morcelé en unités « modulaires » de petite taille (« granularité ») destinées à être travaillées séparément, ces modules peuvent ensuite être réassemblés et transformer le projet.
> Ma thèse est que si cela marche généralement pour les logiciels, c'est beaucoup plus difficile pour les projets culturels.
