---
site: lesoleil.sn
title: "Le Cobess lance « Open access »"
author: Mamadou Gueye
date: 2010-02-11
href: http://www.lesoleil.sn/article.php3?id_article=55845
tags:
- Le Logiciel Libre
---

> Le Consortium des bibliothèques de l'Enseignement supérieur du Sénégal (Cobess) a procédé, hier, au lancement de « l'Open access », lors de l'ouverture de l'atelier national sur l'accès aux publications scientifiques et techniques.
