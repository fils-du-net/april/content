---
site: linformaticien.com
title: "Les conséquences du rachat d’On2, créateur d’Ogg Theora, par Google"
author: Emilien Ercolani 
date: 2010-02-09
href: http://www.linformaticien.com/Actualit%E9s/tabid/58/newsid496/7769/les-consequences-du-rachat-d-on2-createur-d-ogg-theora-par-google/Default.aspx
tags:
- Internet
- Interopérabilité
- Brevets logiciels
- Video
---

> [...] Google joue l’avenir de la vidéo sur le web à coups de millions de dollars. En effet, le géant de la recherche a fait une offre pour racheter On2, le créateur du codec Ogg Theora. Cette acquisition pourrait avoir de très lourdes conséquences sur la guerre des standards qui se joue actuellement.
