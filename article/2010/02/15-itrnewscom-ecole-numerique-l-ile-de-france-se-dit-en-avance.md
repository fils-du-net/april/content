---
site: itrnews.com
title: "Ecole numérique : l’Île-de-France se dit en avance"
author: La rédaction
date: 2010-02-15
href: http://www.itrnews.com/articles/100801/ecole-numerique-ile-france-dit-avance.html
tags:
- Le Logiciel Libre
- Éducation
---

> [...] Mobilisée sur ce sujet depuis 2003, la Région Île-de-France est justement en train de généraliser Lilie, l’ENT des lycées franciliens, qui sera déployé dans tous les établissements d’ici la fin 2012. Logiciel libre, Lilie est à la disposition de toutes les collectivités qui souhaiteraient l’adopter pour les établissements dont elles ont la charge ", précise la région dans un communiqué.
