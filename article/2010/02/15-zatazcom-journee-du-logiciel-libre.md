---
site: zataz.com
title: "Journée du logiciel libre"
author: Damien Bancal
date: 2010-02-15
href: http://www.zataz.com/news/19903/linux-party-journees-du-logiciel-libre-beauvais.html
tags:
- Le Logiciel Libre
---

> Les JOURNEES DU LOGICIEL LIBRE (JD2L) seront organisées les 26 et 27 fevrier prochains dans la ville de Beauvais (Nord de la France). L'association ASCA et l'association OISUX vont proposer 48 heures de Linux and Co.
