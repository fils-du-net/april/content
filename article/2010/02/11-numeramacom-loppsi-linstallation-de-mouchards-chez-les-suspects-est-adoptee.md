---
site: Numerama
title: "LOPPSI : l'installation de mouchards chez les suspects est adoptée"
author: Guillaume Champeau
date: 2010-02-11
href: http://www.numerama.com/magazine/15076-loppsi-l-installation-de-mouchards-chez-les-suspects-est-adoptee.html
tags:
- Neutralité du Net
---

> [...] Les députés ont adopté jeudi presque sans débat l'article 23 du projet de loi Loppsi qui prévoit de donner aux officiers et agents de police judiciaire la possibilité de pénétrer chez les suspects de crimes et délits pour installer des mouchards sur leur système informatique. Ces mouchards doivent permettre à la police de voir et d'enregister à distance les données "telles qu'elles s'affichent sur un écran pour l'utilisateur d'un (ordinateur) ou telles qu'il les y introduit par saisie de caractères".
