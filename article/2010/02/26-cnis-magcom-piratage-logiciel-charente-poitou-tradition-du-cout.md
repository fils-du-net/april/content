---
site: "cnis-mag.com"
title: "Piratage logiciel : Charente-Poitou, tradition du coût…"
author: La rédaction
date: 2010-02-26
href: http://www.cnis-mag.com/piratage-logiciel-charente-poitou-tradition-du-cout.html
tags:
- Logiciels privateurs
- Administration
---

> Stéphane Urbajtel, dans la Charente Libre, enquête sur cette inquiétante statistique : les logiciels de la région seraient (foi de Microsoft) piratés à 49%. Un écart considérable comparé aux pays anglo-saxons, notamment l’Allemagne et l’Angleterre. Deux pays, doit-on préciser, qui militent fortement pour la pratique de Linux dans les milieux scolaires et Universitaires…
