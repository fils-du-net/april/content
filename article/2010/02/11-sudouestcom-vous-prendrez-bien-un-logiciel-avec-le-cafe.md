---
site: sudouest.com
title: "Vous prendrez bien un logiciel, avec le café"
author: La rédaction
date: 2010-02-11
href: http://www.sudouest.com/lot-et-garonne/actualite/villeneuvois/article/863873/mil/5698746.html
tags:
- Le Logiciel Libre
---

> Autour d'un petit noir, ils proposent de faire découvrir le système d'exploitation Linux, ainsi que les logiciels nécessaires pour ce très cher ordinateur : ceux que l'on peut utiliser sans débourser un euro et ce, en toute légalité.
