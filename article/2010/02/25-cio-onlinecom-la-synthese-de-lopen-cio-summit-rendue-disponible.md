---
site: "cio-online.com"
title: "La synthèse de l'Open CIO Summit rendue disponible"
author: Bertrand Lemaire 
date: 2010-02-25
href: http://www.cio-online.com/actualites/lire-la-synthese-de-l-open-cio-summit-rendue-disponible-2743.html
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] A l'inverse, les utilisateurs finaux constituent le premier frein à l'adoption de logiciels libres en dehors des seules infrastructures : le design et l'ergonomie sont « faits par des techniciens pour des techniciens ». Une autre difficulté est la gestion des ressources humaines liée aux compétences sur les produits open-source : les certifications et parcours de formations ne sont pas forcément aussi clairs qu'avec des éditeurs propriétaires.
