---
site: lemonde.fr
title: "Nominations"
author: Pascal Galinier
date: 2010-02-25
href: http://www.lemonde.fr/carnet/article/2010/02/25/nominations-le-monde-date-vendredi-26-fevrier_1311248_3382.html
tags:
- April
---

> [...] Tangui Morlier est le nouveau président de l'Association pour la promotion et la recherche en informatique libre (April). Cette association de 5 360 adhérents (dont 447 personnes morales, entreprises, associations...) défend le logiciel libre et combat la loi Hadopi. Elle lancera fin mars l'opération Libre en fête, en partenariat avec la délégation aux usages de l'Internet et le réseau Cyberbase de la Caisse des dépôts.
