---
site: numerama.com
title: "Google dépose un brevet dédié au filtrage des contenus protégés"
author: Julien L.
date: 2010-02-18
href: http://www.numerama.com/magazine/15107-google-depose-un-brevet-dedie-au-filtrage-des-contenus-proteges.html
tags:
- Internet
- Brevets logiciels
- Neutralité du Net
---

> En 2004, Google a déposé un brevet qui pourrait potentiellement filtrer les contenus protégés sur Internet. Validé par l'USPTO, ce dispositif serait avant tout destiné à préserver la numérisation entamée avec Google Books des ayants droit et de leurs tendances procédurières. Mais est-ce vraiment souhaitable que tous les internautes n'aient plus accès au même contenu ?
