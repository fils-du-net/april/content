---
site: guardian.co.uk
title: "Si vous voulez faire une bonne science, publiez aussi vos codes sources"
author: Darrel Ince 
date: 2010-02-05
href: http://www.guardian.co.uk/technology/2010/feb/05/science-climate-emails-code-release
tags:
- Partage du savoir
- Sciences
- English
---

> « So, if you are publishing research articles that use computer programs, if you want to claim that you are engaging in science, the programs are in your possession and you will not release them then I would not regard you as a scientist; I would also regard any papers based on the software as null and void. »
