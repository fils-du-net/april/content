---
site: BioMed Central Blog
title: "UK government adopts Creative Commons licenses for open data"
author: Matthew Cockerill
date: 2010-02-02
href: http://blogs.openaccesscentral.com/blogs/bmcblog/entry/uk_government_adopts_creative_commons
tags:
- Partage du savoir
- Droit d'auteur
- Sciences
---

> Le gouvernement anglais adopte les licences Creative Commons pour les données ouvertes. C'est une bonne nouvelle pour les chercheurs du secteur public qui publient dans des journaux a accès libre.
