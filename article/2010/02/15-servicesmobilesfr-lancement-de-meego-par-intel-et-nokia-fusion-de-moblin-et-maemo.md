---
site: servicesmobiles.fr
title: "Lancement de MeeGo par Intel et Nokia (fusion de Moblin et Maemo)"
author: La rédaction
date: 2010-02-15
href: http://www.servicesmobiles.fr/services_mobiles/2010/02/lancement-de-meego-par-intel-et-nokia-fusion-de-moblin-et-maemo.html
tags:
- Entreprise
---

> Intel et Nokia fusionnent Moblin et Maemo pour créer Meego, une plate-forme Linux de logiciel qui seront le support matériel de multiples architectures à travers la plus vaste gamme de segments d'appareil, y compris les mobiles, netbooks, tablettes, mediaphones, les téléviseurs connectés et les systèmes embarqués.
