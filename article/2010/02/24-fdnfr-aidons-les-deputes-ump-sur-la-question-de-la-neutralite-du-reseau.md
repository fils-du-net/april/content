---
site: fdn.fr
title: "Aidons les députés UMP sur la question de la neutralité du réseau..."
author: Benjamin Bayart
date: 2010-02-24
href: http://blog.fdn.fr/post/2010/02/24/Aidons-les-deputes-UMP-sur-la-question-de-la-neutralite-du-reseau...
tags:
- Neutralité du Net
---

> Le groupe UMP à l'Assemblée Nationale à ouvert le site web Éthique du numérique. J'y ait déposé une contribution, longue, dans la rubrique sur la neutralité du réseau.
> [...] Internet n'étant, au final, qu'un accord technique entre des opérateurs, sa neutralité est presque sa définition. Si chaque opérateur, sur son petit réseau à lui, décide de porter atteinte à la neutralité, alors, on aura la somme de ces 40.000 atteintes comme résultante, qui donnera un réseau qui ne fonctionne pas.
