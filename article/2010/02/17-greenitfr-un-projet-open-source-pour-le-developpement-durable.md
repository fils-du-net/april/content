---
site: greenit.fr
title: "Un projet open source pour le développement durable"
author: Nicolas Brunet
date: 2010-02-17
href: http://www.greenit.fr/article/outils/un-projet-open-source-pour-le-developpement-durable
tags:
- Le Logiciel Libre
- Entreprise
- Partage du savoir
---

> [...] En effet, une meilleure gestion des données permet aux organisations de mieux comprendre et évaluer l’impact environnemental et social de leurs décisions. De même, un meilleur partage des connaissances favorisera la capitalisation de l’expérience. Il s’agit en outre d’encourager une approche ouverte et transparente auprès des entreprises et plus particulièrement des éditeurs et intégrateurs de solutions, le développement durable faisant l’objet d’une politisation et d’investissements croissants…
