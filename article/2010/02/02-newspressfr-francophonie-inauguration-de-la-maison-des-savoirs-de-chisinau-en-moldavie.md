---
site: newspress.fr
title: "Francophonie : Inauguration de la Maison des savoirs de Chisinau en Moldavie "
author: OIF
date: 2010-02-02
href: http://www.newspress.fr/Communique_FR_224776_2016.aspx
tags:
- Le Logiciel Libre
- Partage du savoir
- Institutions
---

> [...] À Chisinau, la Maison des savoirs est un espace public de 585 m2, désormais ouvert à la population, en particulier aux jeunes et aux femmes qui offrira un accès facile et peu coûteux aux savoirs et à la culture numérique et proposera différentes activités éducatives et pédagogiques, notamment la promotion et le perfectionnement de la langue française ainsi que l'initiation aux logiciels libres pour les éducateurs et les étudiants pré-universitaires.
