---
site: lepoint.fr
title: "Le \"ballot screen\" arrive enfin dans Windows"
author: Guerric Poncet 
date: 2010-02-22
href: http://www.lepoint.fr/actualites-technologie-internet/2010-02-22/choix-du-navigateur-le-ballot-screen-arrive-enfin-dans-windows/1387/0/426708
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
---

> L'Union européenne a gagné : Microsoft s'apprête à déployer l'écran de choix du navigateur (ou ballot screen ) dans toute l'Europe. La Commission européenne avait exigé du géant du logiciel qu'il cesse de profiter de son monopole sur le marché des systèmes d'exploitation pour imposer son navigateur Internet Explorer, installé par défaut dans Windows.
> [...] Mozilla profite de l'occasion pour insister sur le point fort de Firefox : la protection de la vie privée de l'utilisateur. Par la voix de ses deux dirigeants, Mitchell Baker et John Lilly, l'éditeur rappelle : "Le navigateur Web est aujourd'hui le gardien de nos données, de nos habitudes, de cette vie numérique dont il a une connaissance quasi totale."
