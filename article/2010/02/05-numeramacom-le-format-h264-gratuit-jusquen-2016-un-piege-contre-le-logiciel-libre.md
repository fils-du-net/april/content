---
site: numerama.com
title: "Le format H.264 gratuit jusqu'en 2016 : un piège contre le logiciel libre"
author: Guillaume Champeau
date: 2010-02-05
href: http://www.numerama.com/magazine/15023-le-format-h264-gratuit-jusqu-en-2016-un-piege-contre-le-logiciel-libre.html
tags:
- Le Logiciel Libre
- Interopérabilité
- Video
---

> Les utilisateurs du format vidéo H.264 auraient dû commencer à payer des licences y compris pour les vidéos gratuites diffusées sur Internet dès 2011. Mais le consortium MPEG LA qui administre les droits sur le format a décidé de repousser de six ans le passage à la caisse. Une décision qui sonne comme un coup dur pour le logiciel libre.
