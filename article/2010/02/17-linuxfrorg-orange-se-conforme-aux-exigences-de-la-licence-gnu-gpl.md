---
site: linuxfr.org
title: "Orange se conforme aux exigences de la licence GNU GPL"
author: Sébastien Dinot
date: 2010-02-17
href: http://linuxfr.org/2010/02/17/26488.html
tags:
- Le Logiciel Libre
---

> Orange a enfin décidé de se conformer aux exigences d'information des utilisateurs faites par la plupart des licences libres. Une page dédiée - et référencée dans la rubrique « Les essentiels » du portail d'assistance d'Orange - est désormais en ligne.
