---
site: toolinux.com
title: "Libre en Fête 2010 : le printemps sera Libre"
author: La rédaction
date: 2010-02-08
href: http://www.toolinux.com/lininfo/toolinux-information/evenements-et-seminaires/article/libre-en-fete-2010-le-printemps
tags:
- Le Logiciel Libre
- April
---

> L’hiver est rude ? a été rude ? Autour du 21 mars 2010, découvrez le Logiciel Libre partout en France ! Libre en Fête reprend du service.
> Initiée et coordonnée par l’April, l’initiative Libre en Fête est relancée pour la dixième année consécutive : pour accompagner l’arrivée du printemps, des évènements de découverte des Logiciels Libres et du Libre en général seront proposés partout en France autour du 21 mars 2010, dans une dynamique conviviale et festive.
