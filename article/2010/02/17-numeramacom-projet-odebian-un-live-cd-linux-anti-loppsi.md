---
site: numerama.com
title: "Projet Odebian : un live CD Linux anti-LOPPSI"
author: Julien L.
date: 2010-02-17
href: http://www.numerama.com/magazine/15102-projet-odebian-un-live-cd-linux-anti-loppsi.html
tags:
- Le Logiciel Libre
- Internet
- Neutralité du Net
---

> [...] À peine adoptée en première lecture à l'Assemblée nationale que déjà des outils anti-LOPPSI se mettent en place. C'est le cas de la Ligue Odebi, qui s'est montrée très critique envers le projet de loi portée successivement par Michelle Alliot-Marie puis Brice Hortefeux, en déployant le projet Odebian. Il s'agit d'une distribution Linux basée sur Debian et proposée sous la forme d'un Live CD, le tout "paramétré pour une protection maximale de la vie privée".
