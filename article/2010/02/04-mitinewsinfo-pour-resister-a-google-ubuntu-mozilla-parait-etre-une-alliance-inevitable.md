---
site: mitinews.info
title: "Pour résister à Google ? Ubuntu + Mozilla paraît être une alliance inévitable "
author: BM
date: 2010-02-04
href: http://www.mitinews.info/Pour-resister-a-Google-Ubuntu-Mozilla-parait-etre-une-alliance-inevitable_a3487.html
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
---

> [...] Il est intéressant de noter que, dans le même temps, Mac OS X continue sa progression et déjà certains gourous imaginent le marché des OS grand public futur réparti entre les trois géants, Microsoft, Apple et Google reléguant totalement à la marge GNU/Linux. Ce qui en termes clairs signifierait que GNU/Linux perdrait de son ambition grand public pour revenir à la case départ.
