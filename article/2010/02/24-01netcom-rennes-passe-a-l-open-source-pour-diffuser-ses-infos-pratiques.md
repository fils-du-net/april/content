---
site: 01net.com
title: "Rennes passe à l’open source pour diffuser ses infos pratiques"
author: Christophe Guillemin
date: 2010-02-24
href: http://www.01net.com/editorial/512931/rennes-passe-a-l-open-source-pour-diffuser-ses-infos-pratiques/
tags:
- Le Logiciel Libre
- Administration
---

> Toutes les données publiques de l'agglomération rennaise, des horaires de bus aux coordonnées des associations de quartier, vont être proposées gratuitement aux développeurs en herbe, selon le principe de l'open source.
> Objectif : favoriser le développement d'applications pour smartphones, de sites Internet ou encore de widgets dédiés à la vie dans la cité bretonne.
