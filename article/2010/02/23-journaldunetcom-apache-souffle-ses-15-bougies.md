---
site: journaldunet.com
title: "Apache souffle ses 15 bougies"
author: La rédaction
date: 2010-02-23
href: http://www.journaldunet.com/developpeur/outils/15eme-anniversaire-d-apache.shtml
tags:
- Le Logiciel Libre
- Internet
---

> Le serveur HTTP Apache a vu le jour il y a tout juste 15 ans. Sa première version était lancée le 23 février 1995. Depuis, Apache s'est imposé comme le serveur Web le plus utilisé à travers le monde. Il est aujourd'hui installé par plus de 100 millions de sites Web.
