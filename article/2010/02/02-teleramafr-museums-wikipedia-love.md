---
site: telerama.fr
title: "Museums + Wikipedia = love"
author: Sophie Lherm
date: 2010-02-02
href: http://www.telerama.fr/techno/quand-les-musees-anglais-aiment-wikipedia,52217.php
tags:
- Partage du savoir
- Éducation
---

> D’habitude, il est interdit de photographier dans les musées. Mais ce mois-ci, stupeur : quelques-uns des plus grands musées anglais lancent carrément un appel aux photographes amateurs et professionnels pour qu’ils viennent piller leur trésors. Damned, mais pourquoi donc ? Pour illustrer les articles de l’encyclopédie en ligne… et ainsi faire connaître leurs œuvres.
