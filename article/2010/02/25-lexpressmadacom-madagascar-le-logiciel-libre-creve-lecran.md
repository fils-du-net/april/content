---
site: lexpressmada.com
title: "Madagascar : Le logiciel libre crève l'écran"
author: Lova Rafidiarisoa
date: 2010-02-25
href: http://www.lexpressmada.com/display.php?p=display&id=34091
tags:
- Le Logiciel Libre
- Éducation
- International
---

> [...] Les logiciels libres gagnent du terrain dans les parcs informatiques malgaches. De plus en plus d'utilisateurs adoptent ce programme. C'est pourquoi l'Agence universitaire de la Francophonie (AUF) annonce son soutien par la mise en place d'un centre de ressources. « La formation et la promotion des logiciels libres sont les principaux objectifs de ce centre », annonce une source auprès de cette agence.
