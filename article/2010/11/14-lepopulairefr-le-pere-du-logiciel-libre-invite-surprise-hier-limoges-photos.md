---
site: lepopulaire.fr
title: "Le père du logiciel libre, invité surprise hier à Limoges (photos)"
author: Muriel Mingau
date: 2010-11-14
href: http://www.lepopulaire.fr/editions_locales/limoges/le_pere_du_logiciel_libre_invite_surprise_hier_a_limoges_photos_@CARGNjFdJSsBFx8GBxk-.html
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
- Licenses
- Philosophie GNU
---

> L'Américain Richard Stallman, père du logiciel libre, était hier l'invité surprise du "Mois du Logiciel Libre" à la BFM. Il a défendu l'informatique "libre" et, par conséquent, la liberté au sens large.
