---
site: PC INpact
title: "Avenir incertain pour la prohibition de la vente liée PC et OS"
author: Marc Rees
date: 2010-11-16
href: http://www.pcinpact.com/actu/news/60389-cour-cassation-vente-liee-lenovo.htm
tags:
- Logiciels privateurs
- Institutions
- Vente liée
- Europe
---

> C'est une décision qui aura des conséquences importantes sur le dossier de la vente liée. Et pas forcément dans un sens très favorable aux adversaires de ces ventes groupées PC et logiciels. Dans son arrêt du 15 novembre, la Cour de cassation vient de redéfinir les conditions pour qu’une telle vente puisse être qualifiée de loyale au regard du droit de la consommation, interprété cependant à la lumière du droit européen.
