---
site: artesi
title: "Porter le débat sur les Biens Communs"
author: La rédaction
date: 2010-11-22
href: http://www.artesi.artesi-idf.com/public/article/porter-le-debat-sur-les-biens-communs.html?id=22284
tags:
- Le Logiciel Libre
- Contenus libres
---

> Le Manifeste pour la récupération des biens communs est née dans le creuset du Forum Social Mondial 2009 à Belém. En février 2011, le Forum social mondial et le Forum mondial science et démocratie seront deux opportunités pour les “commoners” d’interagir avec d’autres mouvements civiques et sociaux pour leur permettre d'aller de l'avant en s'appropriant cette idée.
