---
site: Le Monde Informatique
title: "L'accès au code source de l'OS Symbian fermé le 17 décembre"
author: Maryse Gros
date: 2010-11-29
href: http://www.lemondeinformatique.fr/actualites/lire-l-acces-au-code-source-de-l-os-symbian-ferme-le-17-decembre-32280.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Associations
---

> Il reste un peu moins de vingt jours pour télécharger le code source de l'OS mobile Symbian. Un compte à rebours qui concerne surtout les entreprises utilisatrices du système d'exploitation. Quant aux développeurs qui exploitent la plateforme, ils vont poursuivre leur route avec le framework Qt.
