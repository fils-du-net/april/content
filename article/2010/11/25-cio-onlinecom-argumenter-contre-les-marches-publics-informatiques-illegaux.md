---
site: "cio-online.com"
title: "Argumenter contre les marchés publics informatiques illégaux"
author: Bertrand Lemaire
date: 2010-11-25
href: http://www.cio-online.com/actualites/lire-argumenter-contre-les-marches-publics-informatiques-illegaux-3310.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Administration
- April
- Institutions
- Marchés publics
---

> L'APRIL sort le premier document de sa campagne durant le Salon des Maires et des Collectivités Locales.
