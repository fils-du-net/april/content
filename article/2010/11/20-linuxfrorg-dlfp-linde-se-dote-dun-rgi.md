---
site: linuxfr.org
title: "DLFP: L'Inde se dote d'un RGI"
author: Pierre Jarillon
date: 2010-11-20
href: https://linuxfr.org/2010/11/20/27607.html
tags:
- Le Logiciel Libre
- Entreprise
- Institutions
- Standards
- International
---

> Le gouvernement indien vient de se doter d'un référentiel général d'interopérabilité (RGI). C'est ce que vient de nous apprendre la FFII dans son communiqué.
