---
site: Numerama
title: "DRM : l'April publie une synthèse sur les verrous numériques"
author: Julien L.
date: 2010-11-10
href: http://www.numerama.com/magazine/17305-drm-l-april-publie-une-synthese-sur-les-verrous-numeriques.html
tags:
- Le Logiciel Libre
- Interopérabilité
- April
- Institutions
- Associations
- DRM
- ACTA
---

> En pointe sur le chapitre des DRM lors des débats autour de la loi DADVSI, l'association April a commencé à publier une série de synthèses sur les dangers pesant sur le logiciel libre. Pour cette première publication, c'est le cas des mesures techniques de protection qui est analysé.
