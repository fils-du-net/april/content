---
site: Xboxygen
title: "Driver Open Source Kinect : MS laisse faire"
author: La rédaction
date: 2010-11-22
href: http://www.xboxygen.com/Actualite-Xbox-360/Accessoires/Driver-Open-Source-Kinect-MS-laisse-couler
tags:
- Le Logiciel Libre
- Entreprise
- Innovation
---

> Avec l’outil de capture 3D, le robot ménager qui scanne la pièce ou encore le marionnettiste et le head tracking , Kinect a beaucoup fait parler de lui ces derniers jours sur Xboxygen. Tout ça grâce au driver open source qui a été mis à disposition sur la toile.
