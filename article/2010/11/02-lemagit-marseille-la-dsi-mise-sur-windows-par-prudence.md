---
site: LeMagIT
title: "A Marseille, la DSI mise sur Windows par prudence"
author: Valery Marchive
date: 2010-11-02
href: http://www.lemagit.fr/article/linux-april-mac-osx-windows-7-windows-xp-marseille/7419/1/a-marseille-dsi-mise-sur-windows-par-prudence/
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Administration
- April
- Désinformation
---

> La Mairie de Marseille ne migrera pas ses postes de travail sous Linux. Du moins pas tout de suite. Elle entend également remplacer progressivement son parc de Mac par des PC sous Windows. Un choix que l’April dénonce, fustigeant des «choix personnels» de la nouvelle direction du système d’information. Interrogé par LeMagIT, Jean-Marie Angi, DSI de la ville, assure quant à lui jouer la prudence et la rationalisation alors que ses équipes vont être mobilisées sur plusieurs projets lourds.
