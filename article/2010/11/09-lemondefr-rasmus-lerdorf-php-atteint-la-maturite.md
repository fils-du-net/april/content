---
site: LeMonde.fr
title: "Rasmus Lerdorf : PHP a atteint la maturité"
author: Damien Leloup
date: 2010-11-09
href: http://www.lemonde.fr/technologies/article/2010/11/09/rasmus-lerdorf-php-a-atteint-la-maturite_1437785_651865.html
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
- Neutralité du Net
---

> Rasmus Lerdorf est le créateur de PHP, le principal langage utilisé pour créer des pages web dynamiques, utilisé par la plupart des sites Internet.
