---
site: LeMonde.fr
title: "\"Les logiciels libres doivent être plus présents dans l'éducation\""
author: Jean-Pierre Archambault et Patrice Bertrand
date: 2010-11-25
href: http://www.lemonde.fr/idees/article/2010/11/25/les-logiciels-libres-doivent-etre-plus-presents-dans-l-education_1444598_3232.html
tags:
- Le Logiciel Libre
- Administration
- Économie
- Éducation
- Informatique en nuage
---

> Le libre est en adéquation parfaite avec les missions du système éducatif et avec la culture enseignante d'accès et d'appropriation par tous de la connaissance.
