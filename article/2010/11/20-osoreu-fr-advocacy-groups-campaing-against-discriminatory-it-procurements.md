---
site: OSOR.EU
title: "FR: Advocacy groups campaing against discriminatory IT procurements"
author: Gijs Hillenius
date: 2010-11-20
href: http://www.osor.eu/news/fr-advocacy-groups-campaing-against-discriminatory-it-procurements
tags:
- Le Logiciel Libre
- Entreprise
- Administration
- Économie
- Promotion
- English
---

> (Les administrations publiques Françaises et les marchés publics) French public administrations will be educated on how to properly procure IT solutions. April, a French association promoting free and open source software, and the National Council of Free Software (CNLL), a group representing French providers of free and open source software services, last Thursday announced a campaign to raise awareness on illegal IT procurement.
