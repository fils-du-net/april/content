---
site: nouvelObs.com
title: "\"Sarkozy et Fillon ont décidé de dissoudre le numérique\""
author: Boris Manenti
date: 2010-11-16
href: http://tempsreel.nouvelobs.com/actualite/opinion/20101116.OBS3006/interview-sarkozy-et-fillon-ont-decide-de-dissoudre-le-numerique.html
tags:
- Le Logiciel Libre
- Économie
- April
- Institutions
---

> Le délégué général de l'April revient sur la suppression du secrétariat à l'Economie numérique, "une régression" voulue par des "gens qui n'ont rien compris au numérique" et qui "risque de la payer en 2012".
