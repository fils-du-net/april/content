---
site: TF1 NEWS
title: "Linaro gagne du terrain et démontre les progrès réalisés dans l’accélération du développement open source"
author: La rédaction
date: 2010-11-10
href: http://bourse.lci.fr/bourse-en-ligne.hts?idnews=BNW101110_00005873&date=101110
tags:
- Le Logiciel Libre
- Entreprise
- Innovation
---

> Linaro présente les progrès et l'élan de son organisation collaborative pour Linux intégré au salon ARM Techcon de Santa Clara.
