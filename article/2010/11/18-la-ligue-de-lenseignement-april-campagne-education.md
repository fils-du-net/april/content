---
site: la ligue de l'enseignement
title: "APRIL : Campagne éducation"
author: Denis Lebioda
date: 2010-11-18
href: http://www.laligue-alpesdusud.org/associatifs_leblog/?2010/11/18/2536-april-campagne-education
tags:
- Le Logiciel Libre
- Administration
- Éducation
- Promotion
---

> Le groupe de travail éducation de l'April lance la campagne « les logiciels libres : à partager sans compter ! ».
