---
site: PC INpact
title: "La Gendarmerie nationale migre tous ses PC vers Ubuntu"
author: Vincent Hermann
date: 2010-11-02
href: http://www.pcinpact.com/actu/news/60157-canonical-gendarmerie-nationale-migration-85000-machines.htm
tags:
- Le Logiciel Libre
- Administration
---

> Cela faisait un moment que nous n'avions abordé un sujet pourtant très courant à une époque : une passation de pouvoir de Windows à Linux dans une grande entreprise ou un secteur public. Nous y revenons donc avec le cas de la Gendarmerie nationale, qui a décidé de franchir le pas et de passer intégralement à Ubuntu.
