---
site: gnet
title: "Android seul maitre à bord pour les smartphones"
author: Amine KOCHLEF
date: 2010-11-02
href: http://www.gnet.tn/net-informatique/android-seul-maitre-a-bord-pour-les-smartphones/id-menu-473.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- International
---

> Android a 44 % de parts de marché aux Etats-Unis, atteignant ainsi le double du chiffre d’affaires réalisé par Apple avec l'iPhone. Le plus important dans ces chiffres relatifs au troisième trimestre de 2010 et qui viennent d’être publiés par le NPD, c’est la réussite de la politique choisie par Google pour la promotion de son système d’exploitation mobile, surtout après le grand flop de son expérience dans les téléphones.
