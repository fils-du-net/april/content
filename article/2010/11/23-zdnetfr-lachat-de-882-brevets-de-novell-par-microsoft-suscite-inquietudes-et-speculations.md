---
site: ZDNet.fr
title: "L'achat de 882 brevets de Novell par Microsoft suscite inquiétudes et spéculations"
author: Christophe Auffray
date: 2010-11-23
href: http://www.zdnet.fr/actualites/l-achat-de-882-brevets-de-novell-par-microsoft-suscite-inquietudes-et-speculations-39756287.htm
tags:
- Le Logiciel Libre
- Entreprise
- Brevets logiciels
- Licenses
---

> L'achat de 882 brevets de Novell par Microsoft suscite inquiétudes et spéculations - Microsoft, via une holding mystérieuse, visiblement fondée seulement quelques jours avant le rachat de Novell, s'est emparé de 882 brevets pour 450 millions de dollars. Microsoft refuse catégoriquement de dévoiler la nature de ces brevets.
