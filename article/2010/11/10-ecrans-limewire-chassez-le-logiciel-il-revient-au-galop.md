---
site: écrans
title: " Limewire : chassez le logiciel, il revient au galop"
author: Camille Gévaudan
date: 2010-11-10
href: http://www.ecrans.fr/Limewire-chassez-le-logiciel-il,11295.html
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Associations
- Désinformation
- Droit d'auteur
- International
---

> Un logiciel libre est un logiciel publié sous une licence autorisant l’étude de son code source, sa modification et la libre diffusion du programme modifié. Or, Limewire était un logiciel libre. Donc, il est parfaitement inefficace d’interdire Limewire, puisqu’il est clonable à volonté et en toute légalité par n’importe qui. Amer syllogisme à méditer pour la Recording Industry Association of America (RIAA), qui avait obtenu fin octobre la désactivation du logiciel de partage p2p.
