---
site: GIZMODO
title: "Le Kinect de Microsoft hacké ? Déjà ?!"
author: fred
date: 2010-11-07
href: http://www.gizmodo.fr/2010/11/07/le-kinect-de-microsoft-hacke-deja.html
tags:
- Le Logiciel Libre
- Entreprise
- DRM
- Innovation
---

> Cela fait seulement quelques jours qu’Adafruit a annoncé offrir 2.000$ de récompense à quiconque produira un driver open source pour le Kinect de Microsoft, et quelqu’un aurait déjà atteint cet objectif.
