---
site: it mag
title: "\"Moi je vais lutter, et vous...\""
author: Samir Tazaïrt
date: 2010-11-15
href: http://www.itmag-dz.com/spip.php?article1444
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Licenses
- Philosophie GNU
---

> Richard Matthew Stallman, dans un entretien inédit, revient sur ses années de militantisme pour que le logiciel ne soit pas l’unique apanage des « développeurs privateurs ». Bien qu’il n’écrive plus de programmes informatiques ; il reste très actif sur le front de la sensibilisation et de la vulgarisation de la philosophie du « logiciel libre », à travers la fondation qu’il a créée et dont il est à la tête, la Free Software Foundation (FSF).
