---
site: WebTimesMedia
title: "Livre Blanc Open Source : Telecom Valley rend sa copie le 2 décembre"
author: Jean-Pierre Largillet
date: 2010-11-21
href: http://www.webtimemedias.com/webtimemedias/wtm_article57006.fr.htm
tags:
- Le Logiciel Libre
- Entreprise
- April
- Institutions
- Associations
- Licenses
- Standards
---

> Telecom Valley remettra officiellement son Livre Blanc sur l'Open Source le 2 décembre au CICA de Sophia Antipolis. Il s'agit du "Livre Blanc : Intégration et Publication de logiciels sous licences Open Source : Mode d’Emploi" -Aspects juridiques, économiques et  communautaires, réalisé par la commission Open Source de l'association. Il sera présenté de 12 à 14 heures par la commission et remis à l’association APRIL (www.april.org).
