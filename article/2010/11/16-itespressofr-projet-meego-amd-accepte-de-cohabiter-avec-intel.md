---
site: ITespresso.fr
title: "Projet MeeGo : AMD accepte de cohabiter avec Intel"
date: 2010-11-16
href: http://www.itespresso.fr/projet-meego-amd-accepte-de-cohabiter-avec-intel-39052.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Associations
- Innovation
---

> Le projet de développement de l'OS mobile open source MeeGo, supervisé par Intel et Nokia, accueille un nouveau membre : AMD.
