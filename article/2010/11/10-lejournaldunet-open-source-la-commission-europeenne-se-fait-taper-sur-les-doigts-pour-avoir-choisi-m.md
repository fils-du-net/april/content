---
site: LeJournalduNet
title: "Open Source : la Commission européenne se fait taper sur les doigts pour avoir choisi Microsoft"
author: La rédaction
date: 2010-11-10
href: http://www.journaldunet.com/solutions/breve/international/49365/open-source---la-commission-europeenne-se-fait-taper-sur-les-doigts-pour-avoir-choisi-microsoft.shtml
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Institutions
- Europe
---

> La Commission européenne va devoir revoir sa stratégie pour l'usage interne de logiciels Open Source
