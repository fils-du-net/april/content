---
site: Developpez.com
title: "Le « plan de développement des usages du numérique à l'école » officialisé, les jeunes appréhenderont-ils mieux l'informatique ?"
author: Katleen
date: 2010-11-28
href: http://www.developpez.com/actu/24363/Le-plan-de-developpement-des-usages-du-numerique-a-l-ecole-officialise-les-jeunes-apprehenderont-ils-mieux-l-informatique
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Administration
- Institutions
- Éducation
---

> Luc Chatel officialise son « plan de développement des usages du numérique à l’école », aidera-t-il les jeunes à mieux appréhender l'informatique ?
