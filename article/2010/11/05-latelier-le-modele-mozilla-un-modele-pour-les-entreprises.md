---
site: L'ATELIER
title: "Le modèle Mozilla : un modèle pour les entreprises ? "
author: Klaus-Peter Speidel
date: 2010-11-05
href: http://www.atelier.fr/chroniques/4/05112010/modele-mozilla-modele-entreprises--40444-.html
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- Associations
---

> L'Open World Forum s'est intéressé à l'Open Innovation et à sa relation potentielle à l'Open Source. L'occasion de faire un point sur ce que les entreprises ont vraiment à apprendre du libre.
