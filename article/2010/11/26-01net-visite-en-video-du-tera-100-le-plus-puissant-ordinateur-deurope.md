---
site: 01net.
title: "Visite en vidéo du Tera 100, le plus puissant ordinateur d'Europe"
author: Eric le Bourlout
date: 2010-11-26
href: http://www.01net.com/editorial/524111/plongee-au-coeur-du-plus-puissant-ordinateur-deurope/
tags:
- Le Logiciel Libre
- Administration
- Innovation
- Video
---

> Le CEA nous a ouvert ses portes pour nous faire découvrir le Tera 100, son supercalculateur flambant neuf. Visite guidée en vidéo.
