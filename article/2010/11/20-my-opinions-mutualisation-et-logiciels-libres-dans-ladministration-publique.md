---
site: My Opinions
title: "Mutualisation et Logiciels Libres dans l'administration publique"
author: Roberto Di Cosmo
date: 2010-11-20
href: http://www.dicosmo.org/MyOpinions/index.php/2010/11/20/108-mutualisation-et-logiciels-libres-dans-l-administration-publique
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Administration
- Standards
- Contenus libres
---

> Gérer le système d'information d'un service public n'est pas la même chose que gérer celui d'une grosse entreprise. La raison est que le service public a des obligations que les grosses entreprises n'ont pas
