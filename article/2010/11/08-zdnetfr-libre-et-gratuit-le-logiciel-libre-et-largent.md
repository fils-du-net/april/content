---
site: ZDNet.fr
title: "Libre et Gratuit: le logiciel libre et l'argent"
author: Patrice Bertrand
date: 2010-11-08
href: http://www.zdnet.fr/actualites/libre-et-gratuit-le-logiciel-libre-et-l-argent-39755963.htm
tags:
- Le Logiciel Libre
- Entreprise
- Économie
- Philosophie GNU
---

> Libre et Gratuit: le logiciel libre et l'argent - Patrice Bertrand, directeur général de l'intégrateur Smile, éclaire le contresens, souvent à l'oeuvre, entre logiciel libre et logiciel gratuit. L'occasion de revisiter les modèles économiques qui régissent l'activité du secteur de l'Open Source.
