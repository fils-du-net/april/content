---
site: LeMagIT
title: "Les contributeurs Ooo.org adoptent le parti pris de LibreOffice"
author: Cyrille Chausson
date: 2010-11-03
href: http://www.lemagit.fr/article/oracle-openoffice-opensource-libreoffice/7425/1/les-contributeurs-ooo-org-adoptent-parti-pris-libreoffice/
tags:
- Le Logiciel Libre
- Entreprise
- Associations
---

> Une trentaine de contributeurs de la communauté d’OpenOffice.org ont décidé de se ranger derrière le projet LibreOffice de la Document Foundation. Un ralliement qu’ils veulent salvateur pour faire évoluer le projet, estimant que la gouvernance d’Oracle restera inchangée, et donc peu fertile.
