---
site: ZDNet.fr
title: "China Mobile adhère à la fondation Linux"
author: Thierry Noisette
date: 2010-11-07
href: http://www.zdnet.fr/blogs/l-esprit-libre/china-mobile-adhere-a-la-fondation-linux-39755930.htm
tags:
- Le Logiciel Libre
- Entreprise
- Associations
- Informatique en nuage
- International
---

> Plus gros opérateur mobile mondial avec plus d'un demi-milliard d'abonnés, China Mobile veut marquer l'énorme valeur qu'il reconnaît à Linux et vient d'adhérer à la fondation Linux.
