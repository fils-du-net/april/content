---
site: Numerama
title: "Diaspora commence à s'ouvrir aux internautes, sur invitation"
author: Julien L.
date: 2010-11-24
href: http://www.numerama.com/magazine/17432-diaspora-commence-a-s-ouvrir-aux-internautes-sur-invitation.html
tags:
- Le Logiciel Libre
- Innovation
---

> Depuis mardi, le réseau social libre et décentralisé Diaspora a commencé à envoyer les premières invitations pour participer à la version alpha privée du projet. Le site va progressivement tester ses performances et ses fonctionnalités avec un nombre de participants de plus en plus important.
