---
site: Numerama
title: "VLC pour iOS crée des tensions dans la communauté open-source"
author: Guillaume Champeau
date: 2010-11-02
href: http://www.numerama.com/magazine/17216-vlc-pour-ios-cree-des-tensions-dans-la-communaute-open-source.html
tags:
- Le Logiciel Libre
- Entreprise
- Associations
- DRM
- Licenses
- Video
---

> Ceux qui imaginent encore que la communauté open-source n'est faite que d'amour et de partage se trompent lourdement. Il s'y joue souvent des querelles d'églises très virulentes, à l'image de celle qui oppose un contributeur important de VLC soutenu par la FSF au président de VideoLAN, à propos du portage du lecteur multimédia sur la plateforme iOS d'Apple.
