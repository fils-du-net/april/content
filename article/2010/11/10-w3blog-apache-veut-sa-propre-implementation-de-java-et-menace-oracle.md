---
site: w3blog
title: "Apache veut sa propre implémentation de Java et menace Oracle"
author: Thibaut
date: 2010-11-10
href: http://w3blog.fr/2010/11/10/apache-software-fondation-ultimatum-oracle/
tags:
- Le Logiciel Libre
- Entreprise
- Associations
- Brevets logiciels
- Droit d'auteur
- Licenses
---

> Avec Oracle, les évènements se répètent mais ne se ressemblent pas, on savait depuis longtemps que l'open-source n'était pas la priorité numéro un de cette compagnie, spécialiste de la base de données du même nom. Ces derniers temps, les accusations, procès et plaintes se font de tous les côtés, et de nombreux articles sur internet en font écho.
