---
site: silicon.fr
title: "L'April: «Les DRM visent à créer de la rareté»"
author: Christophe Lagane
date: 2010-05-03
href: http://www.silicon.fr/fr/news/2010/05/03/l_april___les_drm_visent_a_creer_de_la_rarete_
tags:
- Le Logiciel Libre
- DRM
- Video
---

> Pour rappeler les problèmes et dangers que posent les verrous numériques, l'April soutient la journée mondiale contre les DRM.
> [...] « Les DRM (menottes numériques) visent à créer artificiellement de la rareté et à établir un contrôle de l’usage dans le cercle privé, jusqu’ici impossible à mettre en œuvre », résume Jean-Christophe Becquet, vice-président de l'April. Conséquences potentielles: impossible de faire une copie légitime (à titre privée de sauvegarde notamment ou pour l'utiliser sur plusieurs supports différents sans avoir à transporter l'original) d'une œuvre dument achetée.
