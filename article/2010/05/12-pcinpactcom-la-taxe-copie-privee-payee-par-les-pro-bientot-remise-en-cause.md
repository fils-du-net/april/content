---
site: pcinpact.com
title: "La taxe Copie privée payée par les pro bientôt remise en cause ?"
author: Marc Rees
date: 2010-05-12
href: http://www.pcinpact.com/actu/news/56907-copie-privee-cjce-question-prejudicielle.htm
tags:
- Entreprise
- Économie
- Droit d'auteur
---

> C’est depuis Bruxelles que les régimes nationaux de l’exception pour copie privée risquent de subir de profonds bouleversements. Dans le cadre d’une affaire interne, la Cour de justice des communautés européennes a été saisie par les juridictions espagnoles d’une question préjudicielle touchant ce secteur. Les conclusions de l’avocat général, Mme V. Trstenjak viennent d’être diffusée sur le site de l‘institution européenne.
