---
site: fraifrai.net
title: "Libre Pensée, Logiciel Libre, Culture libre : quels rapports ?"
author: Fraifrai
date: 2010-05-19
href: http://www.fraifrai.net/index.php?2010/05/19/112-libre-pensee-logiciel-libre-culture-libre-quels-rapports
tags:
- Le Logiciel Libre
- Partage du savoir
- Promotion
---

> [...] Le lien entre Culture Libre et Logiciel Libre existe non seulement sous la forme de la mouvance culturelle et philosophique induite par ce dernier mais aussi par une « contamination » de ses préceptes en direction des mondes de la culture et de la connaissance. En effet, au 21ème siècle, ce modèle de licence « libre » dédiée au logiciel s'est vu dérivé pour toutes sortes de contenu (musique, texte, vidéo, enseignement, …) afin d'ouvrir au plus grand nombre l'accès à des connaissances ou à des œuvres d'ordre culturel.
