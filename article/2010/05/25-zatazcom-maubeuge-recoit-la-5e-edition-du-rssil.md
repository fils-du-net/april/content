---
site: zataz.com
title: "Maubeuge reçoit la 5e édition du RSSIL"
author: Damien Bancal
date: 2010-05-25
href: http://www.zataz.com/news/20262/rssil--Rencontres-des-Solutions-de-Securite-et-Informatique-Libre.html
tags:
- Le Logiciel Libre
- April
- Éducation
---

> Les 4 et 5 juin, Maubeuge accueille les 5e Rencontres des Solutions de Sécurité et d'Informatique Libre. Le Logiciel Libre représente en 2010 une alternative sérieuse au monde du « tout est propriété personnelle ». Le Logiciel Libre est fait de partage, de mise en commun, de liberté mais aussi de règles et de méthodes, qui lui ont permit d'acquérir une qualité et une fiabilité reconnues.
