---
site: pcinpact.com
title: "Pour débattre, un Dictionnaire Politique d'Internet et du Numérique"
author: Jeff
date: 2010-05-03
href: http://www.pcinpact.com/actu/news/56714-dictionnaire-politique-internet-numerique-debat.htm
tags:
- Le Logiciel Libre
- Internet
---

> [...] C'est un livre collaboratif qui regroupe du beau monde, hommes politiques, chefs d'entreprise et intellectuels. Les députés croisent les anciens ministres, et Laurence Parisot (la Présidente du Medef) apporte elle aussi sa contribution. De nombreux acteurs du domaine de l'Internet et des droits d'auteurs ont aussi rejoint le projet. Par exemple le co-fondateur de la Quadrature du Net Jérémie Zimmermann ou le délégué général de l'APRIL, Frédéric Couchet, font face au Président de l'ALPA Nicolas Seydoux ou celui de la SACD Pascal Rogard.
