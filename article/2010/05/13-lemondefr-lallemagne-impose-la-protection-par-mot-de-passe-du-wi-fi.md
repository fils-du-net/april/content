---
site: lemonde.fr
title: "L'Allemagne impose la protection par mot de passe du Wi-Fi"
author: La rédaction
date: 2010-05-13
href: http://www.lemonde.fr/technologies/article/2010/05/13/l-allemagne-impose-la-protection-par-mot-de-passe-du-wi-fi_1350888_651865.html
tags:
- Internet
- HADOPI
---

> [...] Les internautes qui recevront les avertissements de la Hadopi et qui souhaiteraient prouver leur bonne foi pourront installer un "logiciel de sécurisation", agréé par la Haute Autorité. Le cahier des charges et a fortiori la liste de ces logiciels ne sont pas encore connus. L'Allemagne a donc choisi une option plus simple ; les sanctions encourues sont également moins lourdes, puisque dans les cas les plus graves, la Hadopi pourra transmettre le dossier à la justice, où le téléchargeur risquera prison avec sursis et d'importantes amendes.
