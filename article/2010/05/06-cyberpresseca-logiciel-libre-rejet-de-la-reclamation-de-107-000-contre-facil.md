---
site: cyberpresse.ca
title: "Logiciel libre: rejet de la réclamation de 107 000 $ contre FACIL"
author: Pierre Asselin
date: 2010-05-06
href: http://www.cyberpresse.ca/le-soleil/actualites/science-et-technologie/201005/05/01-4277601-logiciel-libre-rejet-de-la-reclamation-de-107-000-contre-facil.php
tags:
- Le Logiciel Libre
- Administration
- International
---

> (Québec) L'association qui fait la promotion de l'informatique libre au Québec, FACIL, n'aura pas à payer les 107 000 $ que lui réclamait la firme d'avocats Tremblay, Bois, Mignault.
