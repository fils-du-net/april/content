---
site: pcinpact.com
title: "UNIX (et Linux) va-t-il un jour dominer le monde ?"
author: Nil Sanyas
date: 2010-05-22
href: http://www.pcinpact.com/actu/news/57129-edito-linux-dominer-le-monde.htm
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] Finalement, annoncer que Linux va dominer le monde n’est peut-être pas une exagération, même si la plupart des gens ne le remarqueront peut-être jamais.
>  Ses multiples qualités (diversités, sécurité, etc.) en font depuis longtemps maintenant un système incontournable dans certains marchés. Il ne lui reste en fait que deux marchés où il n'arrive pas à s'imposer massivement : celui des PC particuliers et des PC professionnels. Mais le but de Linux n'est peut-être pas là justement.
