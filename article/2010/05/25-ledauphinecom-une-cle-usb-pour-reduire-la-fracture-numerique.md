---
site: ledauphine.com
title: "Une clé USB pour réduire la fracture numérique"
author: La rédaction
date: 2010-05-25
href: http://www.ledauphine.com/index.jspz?chaine=21&article=306130
tags:
- Le Logiciel Libre
- Administration
---

> Le Département de la Drôme met à la disposition de ses concitoyens un "Bureau universel mobile" embarqué sur une clef USB qui permettra à chacun d'utiliser en permanence et où qu'il se trouve son environnement numérique, sa propre interface, en présence ou non d'une connexion internet.
> [...] Tous ces logiciels sont libres. Cela permet de s'affranchir des coûts de licence et contribue à diminuer le piratage de logiciels coûteux.
