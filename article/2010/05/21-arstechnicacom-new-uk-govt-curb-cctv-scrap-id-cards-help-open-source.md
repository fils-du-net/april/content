---
site: arstechnica.com
title: "New UK govt to curb CCTV, scrap ID cards, help open source"
author: Nate Anderson
date: 2010-05-21
href: http://arstechnica.com/tech-policy/news/2010/05/new-uk-govt-to-curb-cctv-scrap-id-cards-help-open-source.ars
tags:
- Le Logiciel Libre
- Administration
---

> Le nouveau gouvernement anglais souhaite créer des conditions de concurrences non faussées pour le logiciel libre (We will create a level playing field for open-source software and will enable large ICT projects to be split into smaller components)
