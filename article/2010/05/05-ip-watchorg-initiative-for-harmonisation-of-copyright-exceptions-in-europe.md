---
site: "ip-watch.org"
title: "Initiative For Harmonisation Of Copyright Exceptions In Europe"
author: La rédaction
date: 2010-05-05
href: http://www.ip-watch.org/weblog/2010/05/05/initiative-for-harmonisation-of-copyright-exceptions-in-europe/
tags:
- Droit d'auteur
- International
---

> Initiative pour l'harmonisation des exceptions au copyright à l'ère de l'économie de la connaissance, appel de la Free Software Foundation et de la Electronic Frontier Foundation.
