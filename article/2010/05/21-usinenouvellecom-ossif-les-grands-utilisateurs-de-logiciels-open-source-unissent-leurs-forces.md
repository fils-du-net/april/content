---
site: usinenouvelle.com
title: "Ossif : les grands utilisateurs de logiciels open source unissent leurs forces"
author: Christophe Dutheil
date: 2010-05-21
href: http://www.usinenouvelle.com/article/ossif-les-grands-utilisateurs-de-logiciels-open-source-unissent-leurs-forces.N132227
tags:
- Le Logiciel Libre
- Entreprise
---

> Opees et Babylone dans l'aéronautique. Genivi dans l'automobile. Open Healthcare dans la santé... La tendance est au rassemblement des grands utilisateurs de logiciels libres. En ligne de mire : une réduction des coûts de développement des briques de base et une montée en compétence sur les composants à plus forte valeur ajoutée.
