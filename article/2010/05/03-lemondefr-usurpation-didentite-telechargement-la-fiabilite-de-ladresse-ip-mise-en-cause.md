---
site: lemonde.fr
title: "Usurpation d'identité, téléchargement : la fiabilité de l'adresse IP mise en cause"
author: La rédaction
date: 2010-05-03
href: http://www.lemonde.fr/technologies/article/2010/05/03/usurpation-d-identite-telechargement-la-fiabilite-de-l-adresse-ip-mise-en-cause_1345814_651865.html
tags:
- Internet
- HADOPI
---

> [...] Pour sanctionner les internautes qui téléchargent illégalement, la Haute autorité se basera principalement sur cette donnée. Or, il est relativement simple de masquer son adresse IP, voire "d'emprunter" celle d'un voisin en piratant son réseau Wi-Fi.
