---
site: owni.fr
title: "Espaces de hackers"
author: Guillaume Ledit
date: 2010-05-17
href: http://owni.fr/2010/05/17/espaces-de-hackers/
tags:
- Le Logiciel Libre
- Partage du savoir
---

> [...] Les valeurs propres à l’open-source et aux logiciels libres irriguent également la pratique des hackers. Toutefois, il s’agit de rester vigilant, comme l’explique Philippe Langlois par rapport à la licence GPL. En effet, elle s’applique parfois à des logiciels libres utilisés par des gouvernements pour contrôler ou réprimer certains membres de la population comme les sans-papiers. Le /tmp/lab a donc créé une licence permettant d’introduire des exceptions à ces licences libres, comme celle visant à ne pas utiliser le logiciel à des fins immorales.
