---
site: ecrans.fr
title: "Ce soir, le libre s’invite à la Cité des sciences"
author: Camille Gévaudan
date: 2010-05-04
href: http://www.ecrans.fr/Ce-soir-le-libre-s-invite-a-la,9796.html
tags:
- Le Logiciel Libre
- Administration
---

> C’est ce soir, à la Cité des Sciences et de l’Industrie, que sera lancée officiellement la nouvelle « Vraie expo qui parle du faux » sur le thème de la contrefaçon. [...] En zappant l’alternative des licences libres à la philosophie du copyright à tout-va. La polémique déclenchée par le retrait d’un texte d’Isabelle Vodjdani et les réclamations de l’April, association de promotion du logiciel libre, n’ont rien pu y changer.
