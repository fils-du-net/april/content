---
site: zdnet.fr
title: "Le CEA dispose désormais d’un des plus puissants supercalculateurs : Tera 100"
author: Christophe Auffray
date: 2010-05-27
href: http://www.zdnet.fr/actualites/le-cea-dispose-desormais-d-un-des-plus-puissants-supercalculateurs-tera-100-39751961.htm
tags:
- Le Logiciel Libre
- Institutions
- Innovation
- Sciences
---

> Technologie - Le dernier supercalculateur français, Tera 100, a été mis en service le 26 mai par la Direction des Applications Militaires du CEA et Bull, le fournisseur des serveurs. Puissance de calcul théorique : 1,25 petaflops.
> [...] Quant au système d'exploitation, il s'agit d'un OS Linux, comme cela est le cas pour la plupart des supercalculateurs en production dans le monde.
