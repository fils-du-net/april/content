---
site: lemagit.fr
title: "Mandriva sur le point de se vendre "
author: Cyrille Chausson
date: 2010-05-07
href: http://www.lemagit.fr/article/france-linux-rachat-mandriva-linagora/6296/1/mandriva-sur-point-vendre/
tags:
- Le Logiciel Libre
- Entreprise
---

> En dépit de larges efforts sur le front des distributions Linux et des outils d’administration de parc, le Français Mandriva serait entré en négociation en vue d’une éventuelle revente.
