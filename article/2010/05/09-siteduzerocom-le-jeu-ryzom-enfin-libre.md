---
site: siteduzero.com
title: "Le jeu Ryzom enfin libre !"
author: iPoulet
date: 2010-05-09
href: http://www.siteduzero.com/news-62-36437-p1-le-jeu-ryzom-enfin-libre.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Le MMORPG commercial Ryzom vient d'être libéré par son éditeur, Winch Gate Properties. En collaboration avec la Free Software Foundation, la société a publié non seulement les codes sources des clients et serveurs, mais également placé sous licence Creative Commons BY-SA les données graphiques du jeu, ce qui en autorise la distribution et la modification, sous la condition de citer l'auteur initial.
