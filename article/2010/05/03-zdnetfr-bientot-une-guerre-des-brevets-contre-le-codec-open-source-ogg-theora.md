---
site: zdnet.fr
title: "Bientôt une guerre des brevets contre le codec Open Source Ogg Theora ?"
author: Christophe Auffray
date: 2010-05-03
href: http://www.zdnet.fr/actualites/internet/0,39020774,39751339,00.htm
tags:
- Interopérabilité
- Brevets logiciels
- Video
---

> Dean Hachamovitch de Microsoft annonçait qu’IE9 supporterait le HTML 5 et le codec H.264, et uniquement celui-ci. Steve Jobs et Apple ont aussi choisi H.264. Autre point commun, ils soupçonnent les codecs alternatifs comme Theora d’enfreindre des brevets.
> [...] Une façon pour le responsable de Microsoft de mettre en cause, sans le nommer, le codec libre Ogg Theora, une alternative à H.264. Cette allusion à de possibles violations de brevets de la part d'Ogg Theora n'est pas passée inaperçue.
