---
site: lemonde.fr
title: "Téléchargement illégal : Limewire reconnu coupable"
author: La rédaction
date: 2010-05-13
href: http://www.lemonde.fr/technologies/article/2010/05/13/telechargement-illegal-limewire-reconnu-coupable_1350848_651865.html
tags:
- Le Logiciel Libre
- Internet
---

> [...] Limewire, un logiciel populaire permettant d'échanger des fichiers, créé en 2000, avait fait l'objet d'une plainte regroupant treize organisations de défense d'ayants droit en 2006, qui l'accusaient d'avoir créé le logiciel principalement pour le téléchargement illégal, et d'avoir tiré des revenus de cette activité. Le tribunal leur a donné raison. La peine et les dommages et intérêts devraient être fixés le premier juin.
