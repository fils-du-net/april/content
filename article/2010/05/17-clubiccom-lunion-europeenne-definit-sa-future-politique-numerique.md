---
site: clubic.com
title: "L'Union européenne définit sa future politique numérique"
author: Olivier Robillart 
date: 2010-05-17
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/logiciel-libre-open-source/actualite-340784-union-europeenne-definit-future-politique-numerique.html
tags:
- Le Logiciel Libre
- Économie
- Europe
---

> C'est mardi 18 mai que l'agenda de la commissaire aux questions numérique de Neelie Kroes sera dévoilé. L'intérêt de cette feuille de route est de définir clairement quelles seront les priorités de l'Union européenne en matière de numérique et de Telecoms pour les années à venir. Des points sensibles seront donc évoqués comme le Libre, le renforcement de la protection des droits d'auteurs et le combat contre le cybercrime.
