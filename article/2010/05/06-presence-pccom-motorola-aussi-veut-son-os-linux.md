---
site: "presence-pc.com"
title: "Motorola aussi veut son OS Linux"
author: David Civera
date: 2010-05-06
href: http://www.presence-pc.com/actualite/Azingo-39234/
tags:
- Le Logiciel Libre
- Entreprise
---

> Plusieurs signes laissent penser que Motorola vient d’acquérir Azingo dans le but de développer son propre OS mobile utilisant un noyau Linux.
