---
site: pcinpact.com
title: "Android serait le deuxième OS aux États-Unis devant l'iPhone"
author: Nil Sanyas
date: 2010-05-11
href: http://www.pcinpact.com/actu/news/56884-google-android-iphone-apple-blackberry-rim.htm
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Économie
---

> Les ventes de téléphones sous Google Android ont surpassé celles de l’iPhone 3GS au premier trimestre 2010 aux États-Unis révèle une étude de NPD Group, qui confirme plusieurs tendances remarquées ces derniers mois.
