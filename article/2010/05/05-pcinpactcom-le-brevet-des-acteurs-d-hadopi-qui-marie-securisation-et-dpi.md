---
site: pcinpact.com
title: "Le brevet des acteurs d’HADOPI qui marie sécurisation et DPI"
author: Marc Rees
date: 2010-05-05
href: http://www.pcinpact.com/actu/news/56771-hadopi-brevet-securisation-filtrage-dpi.htm
tags:
- HADOPI
- Brevets logiciels
---

> On le sait, HADOPI a découplé l’envoi des mails d’avertissement du fameux dispositif de sécurisation qui pourrait pourtant permettre à un abonné de ne pas subir les foudres de l’autorité. Surprise : un dépôt de brevet diffusé sur le site de l'OMPI montre une solution qui cette fois marie sécurisation et filtrage par inspection profonde. Parmi ses auteurs, on retrouve l’un des architectes des accords Olivennes et Michel Riguidel, ancien prof des télécoms.
