---
site: pcinpact.com
title: "HADOPI : le ministère de la culture explique comment sécuriser"
author: Marc Rees
date: 2010-05-21
href: http://www.pcinpact.com/actu/news/57100-negligence-caracterisee-securisation-hadopi-hadopi.htm
tags:
- Logiciels privateurs
- HADOPI
---

> [...] Sur les détails techniques, le ministère précise que « deux catégories de solutions de sécurisation de l'accès à Internet des particuliers et des très petites entreprises sont d'ores et déjà disponibles à titre gratuit ou à titre onéreux ».
> [...] Ainsi : Négligence caractérisée pour les foyers et les PME = contrôle parental ou pare-feu ou antivirus (sur les PC) et, clé WEP ou WPA éventuellement complétées par un filtrage MAC (sur les box).
