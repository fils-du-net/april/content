---
site: "ip-watch.org"
title: "Nouveau projet de l’Equateur : le logiciel libre pour lutter contre le piratage "
author: Catherine Saez
date: 2010-05-17
href: http://www.ip-watch.org/weblog/2010/05/17/nouveau-projet-de-l%E2%80%99equateur-le-logiciel-libre-pour-lutter-contre-le-piratage/
tags:
- Le Logiciel Libre
---

> Le logiciel libre est actuellement utilisé comme une alternative légale au piratage de logiciels, dans le cadre d’un projet conjoint lancé par un groupe régional à but non lucratif et par l’office de propriété intellectuelle équatorien (IEPI). Ce projet s’appuie sur les bibliothèques publiques pour diffuser des logiciels libres.
