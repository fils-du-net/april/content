---
site: pcinpact.com
title: "Google propose un ensemble de polices libres pour les sites"
author: Vincent Hermann
date: 2010-05-20
href: http://www.pcinpact.com/actu/news/57092-google-web-font-directory-polices-libres.htm
tags:
- Contenus libres
---

> [...] Le projet de Google, qui se nomme simplement Google Font Directory (« répertoire de polices »), rassemble sur un même serveur une série de polices qui sont disponibles pour tous. Pour y accéder, un développeur a besoin de faire référence à la police qui l’intéresse dans le code de la page web [...] Pourquoi l’éditeur aurait-il envie de proposer un tel lot de polices ? Pour rendre le web plus « beau », tout simplement.
