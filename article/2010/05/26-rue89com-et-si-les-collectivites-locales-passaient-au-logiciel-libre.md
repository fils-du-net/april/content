---
site: rue89.com
title: "Et si les collectivités locales passaient au logiciel libre ? "
author: Léo Trouillet
date: 2010-05-26
href: http://www.rue89.com/passage-a-lacte/2010/05/26/et-si-les-collectivites-locales-passaient-au-logiciel-libre-152403
tags:
- Le Logiciel Libre
- Administration
---

> [...] Aujourd'hui, quasiment toutes les collectivités locales utilisent des logiciels libres, c'est-à-dire des logiciels dont la propriété intellectuelle garantit la libre utilisation, la libre diffusion et la libre modification. Aurélie Courtaudon, chargée d'études au cabinet Markess International explique : « 96% des collectivités sont concernées. En 2010, elles y ont investi 16% du budget informatique total de l'administration française, soit un milliard d'euros environ. »
