---
site: clubic.com
title: "Bataille des OS : Microsoft, Apple et Linux tiennent position"
author: Olivier Robillart
date: 2010-05-24
href: http://www.clubic.com/systemes-exploitation-os/actualite-342162-bataille-os-tient-position.html
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
---

> [...] Enfin, Linux se cramponne à la troisième place de ce classement. Le système d'exploitation campe avec une part de trafic de 0,9 % soit inchangée par rapport au mois d'avril 2009. Rien de bien nouveau donc dans la bataille des OS.
