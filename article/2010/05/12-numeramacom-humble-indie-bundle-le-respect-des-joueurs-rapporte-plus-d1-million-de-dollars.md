---
site: numerama.com
title: "Humble Indie Bundle : le respect des joueurs rapporte plus d'1 million de dollars"
author: Guillaume Champeau
date: 2010-05-12
href: http://www.numerama.com/magazine/15717-humble-indie-bundle-le-respect-des-joueurs-rapporte-plus-d-1-million-de-dollars.html
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
- Économie
---

> [...] Mieux encore, l'opération ayant très bien fonctionné, quatre éditeurs ont choisi de diffuser le code source de leur jeu vidéo : Aquaria, Gish, Lugaru HD, et Penumbra Overture.
> [...] Dans le détail, les utilisateurs de Linux sont les plus généreux, avec un prix moyen de 14,95 $, contre 8,03 $ pour les utilisateurs de Windows, et 10,24 $ pour les utilisateurs de Mac OS.
