---
site: journaldunet.com
title: "La Sacem met en musique son éditique grâce à l'Open Source"
author: Dominique Filippone
date: 2010-05-26
href: http://www.journaldunet.com/solutions/intranet-extranet/sacem-et-gestion-documentaire-open-source.shtml?f_id_newsletter=3034&utm_source=benchmail&utm_medium=ML5&utm_campaign=E10172280&f_u=2831942
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] "La Sacem est une PME qui a la culture Open Source dans la peau et cela a permis de faciliter l'adoption de cette nouvelle solution car nous avions des compétences pointues en interne à l'aise avec ce types de technologies", fait savoir Jérôme Duchemin.
