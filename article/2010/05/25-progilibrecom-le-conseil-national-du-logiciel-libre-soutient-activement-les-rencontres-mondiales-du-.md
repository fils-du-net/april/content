---
site: progilibre.com
title: "Le conseil national du logiciel libre soutient activement les rencontres mondiales du logiciel libre "
author: La rédaction
date: 2010-05-25
href: http://www.progilibre.com/Le-Conseil-National-du-Logiciel-Libre-soutient-activement-les-Rencontres-Mondiales-du-Logiciel-Libre_a1124.html
tags:
- Le Logiciel Libre
- Entreprise
- April
---

> Les Rencontres Mondiales du Logiciel Libre (RMLL 2010) se tiendront autour de Bordeaux, du 6 au 11 juillet. Depuis 10 ans, les RMLL sont le rendez-vous incontournable du Logiciel Libre en France, une occasion pour tous les acteurs du Libre d'aller à la rencontre du grand public pour faire connaître leurs logiciels mais aussi leurs valeurs.
