---
site: abclinuxu.cz
title: "BSA: Hardware Without Software Not Tax Deductible"
author: Robert Krátký
date: 2010-05-26
href: http://www.abclinuxu.cz/clanky/mfcr-bsa-open-source?page=1
tags:
- Logiciels privateurs
- Désinformation
- Informatique-deloyale
- Europe
- English
---

> Le lobby des vendeurs de logiciels, la Business Software Alliance (BSA) et le le ministre des finances de la République tchèque, ont annoncé que les ordinateurs vendu sans logiciels seraient surtaxés.
