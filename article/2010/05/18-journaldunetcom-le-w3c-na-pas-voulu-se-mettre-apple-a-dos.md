---
site: journaldunet.com
title: "\"Le W3C n'a pas voulu se mettre Apple à dos\""
author: Dominique Filippone
date: 2010-05-18
href: http://www.journaldunet.com/solutions/acteurs/hugo-roy-fsf-et-violation-de-brevet-apple.shtml?f_id_newsletter=2979&utm_source=benchmail&utm_medium=ML5&utm_campaign=E10171622
tags:
- Le Logiciel Libre
- Video
---

> Pour éviter au codec Ogg Theora de s'imposer face à la technologie H264 dans les spécifications HTML5, Steve Jobs a joué la carte de l'intimidation. Rien ne dit qu'il ira jusqu'au tribunal.
