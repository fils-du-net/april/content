---
site: macgeneration.com
title: "WebM, pas encore vraiment ouvert"
author: Arnauld de La Grandière
date: 2010-05-25
href: http://www.macgeneration.com/news/voir/155871/webm-pas-encore-vraiment-ouvert
tags:
- Interopérabilité
- Brevets logiciels
---

> Pour la publication de son codec libre, WebM, Google a mis au point une nouvelle licence d'utilisation. Celle-ci est basée sur la licence de BSD, en y ajoutant une clause qui stipule que toute attaque en justice fait immédiatement renoncer à l'utilisation de WebM.
