---
site: numerama.com
title: "Plus de 125.000 dollars de dons en quelques jours pour Diaspora, le Facebook libre et ouvert"
author: Guillaume Champeau
date: 2010-05-14
href: http://www.numerama.com/magazine/15720-plus-de-125000-dollars-de-dons-en-quelques-jours-pour-diaspora-le-facebook-libre-et-ouvert.html
tags:
- Le Logiciel Libre
- Contenus libres
---

> Diaspora, le projet d'un réseau social libre, open-source et décentralisé à la manière des logiciels de P2P, a réuni en deux semaines plus de 125.000 dollars de dons, et continue pendant encore près de vingt jours à lever des fonds. L'objectif de 10.000 dollars réunis d'ici la fin du mois est pulvérisé, assurant déjà que le projet ne sera pas qu'une simple promesse laissée dans un tiroir.
