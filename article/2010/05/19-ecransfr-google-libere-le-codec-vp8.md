---
site: ecrans.fr
title: "Google libère le codec VP8"
author: Camille Gévaudan 
date: 2010-05-19
href: http://www.ecrans.fr/Google-libere-le-codec-VP8,9942.html
tags:
- Interopérabilité
- Licenses
- Standards
- Video
---

> [...] Le seul réel point d’interrogation flottait sur l’épineuse question du codec vidéo racheté par le géant de Mountain View il y a quelques mois (lire l’article). La réponse vient de tomber à l’instant en Californie, et devrait ravir la Free Software Foundation qui en avait fait la demande appuyée dans une lettre ouverte : Google a bien l’intention de faire du VP8 un format vidéo libre et open-source.
