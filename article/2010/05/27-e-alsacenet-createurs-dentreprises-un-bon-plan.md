---
site: "e-alsace.net"
title: "Créateurs d'entreprises : un bon plan"
author: La rédaction
date: 2010-05-27
href: http://e-alsace.net/index.php/smallnews/get?newsId=3224
tags:
- Le Logiciel Libre
- Entreprise
- Économie
---

> [...] C'est une initiative de la Chambre des Métiers et de l’Artisanat de la Somme, en partenariat avec le Conseil Régional de Picardie.
> Il s'agit d'un pack gratuit de logiciels libres spécialement destiné aux créateurs de TPE et PME.
> Cette compilation de logiciels libres à dominante professionnelle est une idée issue des Rencontres Mondiales du Logiciel Libre dont l’édition 2007 s’est déroulée à Amiens.
