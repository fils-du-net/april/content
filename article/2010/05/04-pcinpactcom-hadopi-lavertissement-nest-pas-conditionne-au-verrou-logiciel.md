---
site: pcinpact.com
title: "Hadopi : l'avertissement n'est pas conditionné au verrou logiciel "
author: Marc Rees
date: 2010-05-04
href: http://www.pcinpact.com/actu/news/56725-hadopi-email-avertissement-surveillance-tmg.htm
tags:
- Logiciels privateurs
- HADOPI
---

> [...] Ce découplage total entre avertissement et logiciel de sécurisation fait qu’un abonné averti n’aura aucune information sur l’outil qui lui aurait permis de ne pas se faire flasher par Trident Média Guard. Situation absurde : voilà une autorité qui se place sous le signe de la pédagogie… et qui perd sa langue lorsqu’on lui demande d’en faire.
