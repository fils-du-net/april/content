---
site: 01net.com
title: "Recours en annulation contre un décret d'Hadopi devant le Conseil d'Etat"
author: Guillaume Deleurence
date: 2010-05-07
href: http://www.01net.com/editorial/516466/un-decret-dhadopi-fait-lobjet-dun-recours-devant-le-conseil-detat/
tags:
- Le Logiciel Libre
- HADOPI
---

> Le fournisseur d'accès associatif French Data Network estime qu'il existe un vice de forme concernant un décret en date du 5 mars. Le gouvernement n'aurait pas consulté l'Arcep, comme la loi l'y obligerait.
> [...] La Quadrature du Net ne compte pas en rester là, et veut s'attaquer à d'autres décrets attendus, tel celui relatif aux logiciels de sécurisation permettant aux internautes de prouver leur bonne foi s'ils venaient à être accusés à tort de téléchargement illicite. « Encore faut-il qu'il voie le jour. Pour nous, il est impossible à rédiger. On ne parle pas de logiciels de sécurité informatique, mais de contrôle d'un usage privé. C'est bien plus compliqué. Et s'il sort, on aura des angles d'attaque, ne serait-ce que par rapport aux logiciels libres. »
