---
site: clubic.com
title: "L'absence de Linux sur la PS3 pose problème aux scientifiques américains"
author: Audrey Oeillet
date: 2010-05-14
href: http://www.clubic.com/technologies-d-avenir/actualite-340444-absence-linux-ps3-pose-probleme-scientifiques-americains.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Innovation
- Sciences
---

> En bloquant l'installation d'un autre OS que celui proposé par défaut sur la Playstation 3, Sony a mis en colère certains utilisateurs, en particulier les linuxiens, mais pas seulement : le fabricant a également posé un véritable problème aux forces aériennes américaines. L'année dernière, le laboratoire de recherche de l'Air Force, situé à Rome dans l'Etat de New York, avait en effet fait l'acquisition de plus de 2000 PS3 pour effectuer des calculs complexes à moindre coût.
