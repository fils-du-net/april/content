---
site: numerama.com
title: "Jean-Luc Godard : \"Je suis contre Hadopi. Il n'y a pas de propriété intellectuelle\""
author: Guillaume Champeau
date: 2010-05-18
href: http://www.numerama.com/magazine/15749-jean-luc-godard-je-suis-contre-hadopi-il-n-y-a-pas-de-propriete-intellectuelle.html
tags:
- Partage du savoir
- HADOPI
- Droit d'auteur
---

> Interrogé par les Inrocks à l'occasion de la sortie de Film Socialisme, Jean-Luc Godard a livré un plaidoyer contre les dérives du droit d'auteur et l'idée-même de propriété intellectuelle.
> [...] "Le droit d’auteur, vraiment c’est pas possible. Un auteur n’a aucun droit. Je n’ai aucun droit. Je n’ai que des devoirs", explique-t-il. "Je suis contre Hadopi, bien sûr. Il n’y a pas de propriété intellectuelle."
