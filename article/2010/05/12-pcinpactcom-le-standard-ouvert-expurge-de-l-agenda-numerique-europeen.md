---
site: pcinpact.com
title: "Le standard ouvert expurgé de l’agenda numérique européen "
author: Marc Rees
date: 2010-05-12
href: http://www.pcinpact.com/actu/news/56922-agenda-numerique-europeen-open-standard.htm
tags:
- Interopérabilité
- Europe
---

> [...] Pour l’April, « ces pressions s'inscrivent dans un contexte dans lequel les lobbies du logiciel propriétaire, Microsoft en tête, tentent de réviser à la baisse la définition de standard ouvert. (...) l’adoption en l'état prouverait ainsi qu'en matière de politique sur le numérique, il n'est plus question d'opter pour le statu quo et ainsi de céder à des intérêts particuliers basant leurs rentes sur le contrôle et la fermeture. »
