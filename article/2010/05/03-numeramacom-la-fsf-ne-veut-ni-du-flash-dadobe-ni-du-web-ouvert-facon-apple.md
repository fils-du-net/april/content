---
site: numerama.com
title: "La FSF ne veut ni du Flash d'Adobe, ni du \"web ouvert\" façon Apple"
author: Guillaume Champeau
date: 2010-05-03
href: http://www.numerama.com/magazine/15644-la-fsf-ne-veut-ni-du-flash-d-adobe-ni-du-web-ouvert-facon-apple.html
tags:
- Internet
- Logiciels privateurs
- Interopérabilité
- Video
---

> La Fondation pour le Logiciel Libre (FSF) ne veut pas arbitrer entre Adobe et Apple dans le combat qui oppose les deux sociétés et leur conception du web. Selon la FSF, les deux entreprises ne cherchent qu'à imposer un web qu'ils contrôlent, à l'antithèse du "web libre" officiellement défendu par Apple.
