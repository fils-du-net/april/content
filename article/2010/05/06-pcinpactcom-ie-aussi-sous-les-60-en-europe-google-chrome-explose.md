---
site: pcinpact.com
title: "IE aussi sous les 60 % en Europe, Google Chrome explose"
author: Nil Sanyas
date: 2010-05-06
href: http://www.pcinpact.com/actu/news/56806-navigateurs-internet-explorer-google-chrome-firefox.htm
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
---

> Toutes les statistiques le confirment : Microsoft Internet Explorer continue son irrémédiable chute, ceci au profit des autres navigateurs, dont Google Chrome principalement.
> [...] Autre statistique intéressante, ATII confirme lui aussi que le fameux ballot screen de Windows n’a pas encore eu d’impact significatif.
