---
site: agoravox.fr
title: "L’Ubuntu Party revient les 29 et 30 mai à La Villette"
author: Winael
date: 2010-05-25
href: http://www.agoravox.fr/actualites/technologies/article/l-ubuntu-party-revient-les-29-et-75407
tags:
- Le Logiciel Libre
- April
---

> Devenu un évènement incontournable pour les aficionados du logiciel libre, l’Ubuntu-Party de Paris accueille de nouveau le grand public à la Cité des sciences et de l’industrie de la Villette pour célébrer la sortie de la nouvelle version 10.04 (Lucid Lynx).
