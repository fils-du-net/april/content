---
site: cyberpresse.ca
title: "Le logiciel libre veut entrer à l'Assemblée nationale"
author: Pierre Asselin
date: 2010-05-04
href: http://www.cyberpresse.ca/le-soleil/actualites/science-et-technologie/201005/03/01-4276772-le-logiciel-libre-veut-entrer-a-lassemblee-nationale.php
tags:
- Le Logiciel Libre
- Administration
- International
---

> (Québec) La porte-parole de l'opposition officielle en matière de recherche et développement et innovations technologiques, Marie Malavoy, veut convaincre ses collègues de suivre l'exemple des députés français, et d'implanter le logiciel libre sur les postes de travail de l'Assemblée nationale du Québec.
