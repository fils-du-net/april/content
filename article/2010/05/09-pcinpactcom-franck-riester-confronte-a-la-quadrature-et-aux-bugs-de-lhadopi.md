---
site: pcinpact.com
title: "Franck Riester confronté à la Quadrature et aux bugs de l'Hadopi"
author: Marc Rees
date: 2010-05-09
href: http://www.pcinpact.com/actu/news/56856-hadopi-franck-riester-jeremie-zimmermann.htm
tags:
- Logiciels privateurs
- HADOPI
---

> [...] le député Riester pourtant membre de la Hadopi, n’a été capable de fournir de détails sur ces fameux logiciels de sécurisation. Une cachoterie d'autant plus pesante que ce même Riester n'avait pas voulu que ces logiciels soient impérativement gratuits ou interopérables.
