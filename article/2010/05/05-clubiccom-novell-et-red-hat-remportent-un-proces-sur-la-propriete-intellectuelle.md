---
site: clubic.com
title: "Novell et Red Hat remportent un procès sur la propriété intellectuelle"
author: Antoine Duvauchelle
date: 2010-05-05
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/actualite-338992-novell-red-hat-remportent-proces-propriete-intellectuelle.html
tags:
- Entreprise
- Brevets logiciels
---

> Une cour fédérale américaine vient de disculper Red Hat et Novell dans un cas de violation de propriété intellectuelle. Ils étaient accusés par IP Innovation, une filiale d'Acacia Research Corporation et de Technology Licensing Corporation, de violer trois de ses brevets.
