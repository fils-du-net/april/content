---
site: gnet.tn
title: "Un bunker pour protéger les formats de données"
author: A.K
date: 2010-05-20
href: http://www.gnet.tn/net-informatique/un-bunker-pour-proteger-les-formats-de-donnees/id-menu-473.html
tags:
- Interopérabilité
- Partage du savoir
---

> Un groupe de chercheurs européens a créé une salle du «génome numérique », dont le but principal est de garder les clés et les des formats numériques éteints ou en voie d’extinction. Cette chambre ultra-protégée sera préservée dans les Alpes suisses enfouie sous un bunker capable de résister, pour les 25 prochaines années, à une attaque nucléaire ou une catastrophe naturelle hors norme.
> [...] «Contrairement aux hiéroglyphes sculptés dans la pierre, ou à l'encre sur le papier, les données numériques ont une espérance de vie qui se mesure en années», a déclaré le Prof. Andreas Rauber de l'Université de Vienne, qui a ajouté: «Si nous ne parvenons pas à développer des mesures appropriées pour la préservation numérique, dans l'avenir, ceci pourrait nous coûter des milliards ».
