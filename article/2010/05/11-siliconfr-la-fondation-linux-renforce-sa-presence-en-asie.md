---
site: silicon.fr
title: "La Fondation Linux renforce sa présence en Asie"
author: David Feugey
date: 2010-05-11
href: http://www.silicon.fr/fr/news/2010/05/11/la_fondation_linux_renforce_sa_presence_en_asie
tags:
- Le Logiciel Libre
---

> Cliff Miller rallie la Fondation Linux en tant que directeur des opérations chinoises. En 1992, il avait participé à la création de TurboLinux, une distribution Linux très populaire en Chine et au Japon.
> [...] Autre annonce, Alex Lu devient le directeur des opérations taïwanaises pour la Fondation Linux. « La pression de la concurrence et l’élargissement du marché poussent les grandes entreprises taïwanaises à adopter encore plus Linux », souligne Jim Zemlin.
