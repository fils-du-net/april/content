---
site: bastamag.net
title: "Fab-labs : quand chacun fabrique ce dont il a besoin"
author: Nedjma Bouakra
date: 2010-05-05
href: http://www.bastamag.net/article1024.html
tags:
- Le Logiciel Libre
- Partage du savoir
- Innovation
---

> Pour 1% de leur prix sur le marché, des objets et outils se fabriquent dans de petits ateliers industriels utilisant allègrement logiciels libres et nouvelles technologies. Les « fabuleux ateliers », ou fab-labs, ouvrent une nouvelle voie alliant hautes technologies, auto-production, récupération et recyclage. La seule condition reste votre participation.
