---
site: ITRmanager.com
title: "Un projet de recherche européen sur le Cloud Computing"
author: La rédaction
date: 2010-09-23
href: http://www.itrmanager.com/articles/109846/projet-recherche-europeen-cloud-computing.html
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Institutions
- Innovation
- Informatique en nuage
- Europe
---

> Les ingénieurs de Red Hat se sont vus confier la direction de l’un des plus importants projets de recherche européens relatifs au Cloud : Cloud-TM.
