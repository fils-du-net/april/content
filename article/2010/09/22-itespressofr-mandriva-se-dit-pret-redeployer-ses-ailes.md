---
site: ITespresso.fr
title: "Mandriva se dit prêt à (re)déployer ses ailes"
author: La rédaction
date: 2010-09-22
href: http://www.itespresso.fr/mandriva-se-dit-pret-a-redeployer-ses-ailes-36762.html
tags:
- Le Logiciel Libre
- Entreprise
- Innovation
---

> Optimiste, Mandriva se restructure et a mis sur pied une nouvelle stratégie pour sortir de l'ornière. L'éditeur compte notamment partir à la conquête des marchés français, brésilien et russe et capitalise sur ses innovations.
