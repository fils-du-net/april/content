---
site: écrans
title: "Diaspora, les premiers pas d'un « Facebook libre »"
author: Andréa Fradin
date: 2010-09-16
href: http://www.ecrans.fr/Diaspora,10726.html
tags:
- Le Logiciel Libre
- Internet
- Sensibilisation
- Innovation
- International
---

> Le réseau social open source respectueux de la vie privée, contrôlé par les utilisateurs et entièrement à construire.
