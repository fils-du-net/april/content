---
site: "cio-online.com"
title: "L'open-source perçu comme vecteur d'innovation dans le secteur public"
author: Bertrand Lemaire
date: 2010-09-28
href: http://www.cio-online.com/actualites/lire-l-open-source-percu-comme-vecteur-d-innovation-dans-le-secteur-public-3179.html
tags:
- Le Logiciel Libre
- Administration
- Innovation
- Informatique en nuage
---

> Selon la dernière étude du cabinet Markess International sur le sujet, seules 6% des administrations ne trouvent pas d'innovations en lien avec l'usage de l'open-source.
