---
site: europpe écologie
title: "Propriété intellectuelle : La répression est une réponse aberrante qui ne répond pas à la protection des droits d'auteurs"
author: Sandrine Bélier
date: 2010-09-22
href: http://europeecologie.eu/Propriete-intellectuelle-La
tags:
- Internet
- Économie
- Partage du savoir
- Institutions
- Désinformation
- Droit d'auteur
- Contenus libres
- Europe
- ACTA
---

> Ce mercredi 22 septembre 2010, le Parlement européen a adopté le rapport Gallo, non législatif, sur la propriété intellectuelle, le téléchargement et les droits d’auteurs sur internet.
