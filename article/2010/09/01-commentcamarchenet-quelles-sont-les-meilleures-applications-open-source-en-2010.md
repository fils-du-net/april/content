---
site: Commentçamarche.net
title: "Quelles sont les meilleures applications Open Source en 2010 ?"
author: La rédaction
date: 2010-09-01
href: http://www.commentcamarche.net/news/5852878-quelles-sont-les-meilleures-applications-open-source-en-2010
tags:
- Le Logiciel Libre
- Sensibilisation
- Promotion
- International
---

> Chaque année, le webzine américain "InfoWorld" distingue les applications Open Source les plus performantes pour les entreprises, après une mise à l'épreuve dans son atelier de tests. Les "Bossie Awards" (Best of Open Source Software awards)...
