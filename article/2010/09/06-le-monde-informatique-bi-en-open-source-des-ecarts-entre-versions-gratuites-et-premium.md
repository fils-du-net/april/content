---
site: Le Monde Informatique
title: "BI en Open Source : des écarts entre versions gratuites et premium"
author: Maryse Gros avec IDG NS
date: 2010-09-06
href: http://www.lemondeinformatique.fr/actualites/lire-bi-en-open-source-des-ecarts-entre-versions-gratuites-et-premium-31558.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Sensibilisation
---

> Il n'est pas toujours aisé de comparer entre elles les solutions décisionnelles disponibles en Open Source, avertit Forrester dans un rapport publié en août sur le sujet.
