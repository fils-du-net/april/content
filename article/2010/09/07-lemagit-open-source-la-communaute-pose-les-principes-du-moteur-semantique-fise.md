---
site: LeMagIT
title: "Open Source : la communauté pose les principes du moteur sémantique Fise "
author: Cyrille Chausson
date: 2010-09-07
href: http://www.lemagit.fr/article/europe-semantique-gestion-contenus-opensource-nuxeo-projet/7032/1/open-source-communaute-pose-les-principes-moteur-semantique-fise/
tags:
- Le Logiciel Libre
- Internet
- Administration
- Innovation
- Europe
---

> Le projet européen IKS donne naissance à Fise, un moteur sémantique Open Source destiné au monde de la gestion de contenu et de la gestion documentaire. Après un an de travaux techniques, l’heure est à la confrontation des prototypes fonctionnels pour faire gonfler la communauté.
