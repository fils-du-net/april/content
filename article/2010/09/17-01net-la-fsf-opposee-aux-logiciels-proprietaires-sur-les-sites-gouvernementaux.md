---
site: 01net.
title: "La FSF opposée aux logiciels propriétaires sur les sites gouvernementaux"
author: Arnaud Devillard
date: 2010-09-17
href: http://www.01net.com/editorial/520972/la-fsf-veut-que-le-libre-aussi-soit-promu-sur-les-sites-publics/
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Administration
- Interopérabilité
- Accessibilité
- Licenses
- Contenus libres
---

> L'organisation pro-logiciel libre lance une pétition. Selon elle, promouvoir ou imposer des logiciels propriétaires revient à faire de la publicité aux éditeurs et à réduire les libertés citoyennes.
