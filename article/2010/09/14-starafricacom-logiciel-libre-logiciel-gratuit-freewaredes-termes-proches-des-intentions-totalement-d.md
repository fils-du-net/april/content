---
site: StarAfrica.com
title: "Logiciel libre, logiciel gratuit, freeware...des termes proches, des intentions totalement différentes"
author: Nicolas Brunot
date: 2010-09-14
href: http://www.starafrica.com/fr/actualites/news-tech/article/logiciel-libre-logiciel-gratuit-freewa-85707.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Sensibilisation
- Droit d'auteur
---

> Logiciel libre, logiciel gratuit, freeware...des termes proches, des intentions totalement différentes : comment les internautes sont-ils leurrés ?
