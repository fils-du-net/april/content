---
site: ZDNet.fr
title: "Démarrage laborieux pour la hotline de la Hadopi"
author: Christophe Auffray
date: 2010-09-24
href: http://www.zdnet.fr/actualites/demarrage-laborieux-pour-la-hotline-de-la-hadopi-39754869.htm
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- April
- HADOPI
- Institutions
---

> Grâce au numéro de téléphone communiqué sur son site par Free, plusieurs sites d'informations ont pu tester la hotline de la Hadopi et sans doute aussi les qualités pédagogiques de la loi. Le résultat est mitigé, en partie en raison du manque d'expérience inhérent à tout démarrage.
