---
site: ars technica
title: "Zombie cookie wars: evil tracking API meant to \"raise awareness\""
author: Jacqui Cheng
date: 2010-09-22
href: http://arstechnica.com/web/news/2010/09/evercookie-escalates-the-zombie-cookie-war-by-raising-awareness.ars
tags:
- Le Logiciel Libre
- Internet
- Informatique-deloyale
- International
- English
---

> The war against persistent zombie cookies—cookies that never seem to lose your data, even when you delete them—rages on, as users learn more about the technology. While awareness is rising thanks to widespread coverage of Flash cookies and, more recently, HTML5's storage capabilities, we have a long way to go before Internet users can avoid persistent tracking. Like all zombie wars, this one will take some time to win; and if you thought things were bad now, they're about to get worse.
