---
site: clubic.com
title: "La France débloque 750 millions d'euros pour les contenus numériques"
author: Olivier Robillart
date: 2010-09-23
href: http://www.clubic.com/television-tv/video-et-streaming/vod/actualite-367828-france-debloque-750-contenu-numerique.html
tags:
- HADOPI
- Institutions
- Contenus libres
---

> Dans le cadre du grand Emprunt, une enveloppe devait être dégagée afin de favoriser les contenus numériques et l'offre légale d'œuvres culturelles et de divertissement. 750 millions d'euros seront donc débloqués par Frédéric Mitterrand et Nathalie Kosciusko-Morizet.
