---
site: Génération nouvelles technologies
title: "Lutter contre les préjugés liés à l'open source"
author: Jérôme G.
date: 2010-09-18
href: http://www.generation-nt.com/logiciel-libre-entreprise-talend-tribune-diard-actualite-1081781.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Sensibilisation
---

> Une tribune libre de Bertrand Diard, co-fondateur et PDG de Talend, éditeur français de logiciels open source dans le domaine de la gestion des données.
