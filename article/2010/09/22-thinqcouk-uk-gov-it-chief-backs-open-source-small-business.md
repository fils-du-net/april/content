---
site: THINQ.co.uk
title: "UK Gov IT chief backs open source, small business"
author: James Nixon
date: 2010-09-22
href: http://www.thinq.co.uk/2010/9/22/uk-gov-it-chief-backs-open-source-small-business/
tags:
- Le Logiciel Libre
- Entreprise
- Institutions
- English
---

> (Un plan Britannique pour réduire les coûts en passant par les petites entreprises et le logiciel libre) The UK government’s deputy CIO has outlined plans to hand public sector IT contracts over to small businesses (...)
