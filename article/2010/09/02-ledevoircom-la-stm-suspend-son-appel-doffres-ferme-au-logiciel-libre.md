---
site: LeDevoir.com
title: "La STM suspend son appel d'offres fermé au logiciel libre"
author: Fabien Deglise
date: 2010-09-02
href: http://www.ledevoir.com/economie/transport/295457/la-stm-suspend-son-appel-d-offres-ferme-au-logiciel-libre
tags:
- Le Logiciel Libre
- Administration
- International
---

> On efface tout et on recommence. Sous un feu nourri de critiques, la Société de transport de Montréal (STM) a décidé hier de suspendre son appel d'offres pour l'acquisition de logiciels de marque Microsoft, et ce, afin d'évaluer la possibilité d'ouvrir ce processus d'achat à des logiciels libres de droits.
