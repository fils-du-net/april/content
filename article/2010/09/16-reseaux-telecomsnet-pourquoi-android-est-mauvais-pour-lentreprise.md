---
site: "Reseaux-Telecoms.net"
title: "Pourquoi Android est mauvais pour l'entreprise"
author: Johanna Godet
date: 2010-09-16
href: http://www.reseaux-telecoms.net/actualites/lire-pourquoi-android-est-mauvais-pour-l-entreprise-22598.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Désinformation
- Innovation
---

> Les facteurs qui influent sur la croissance d'Android sont aussi des facteurs qui en font une plate-forme cauchemardesque pour les administrateurs.
