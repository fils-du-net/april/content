---
site: Slate.fr
title: "Quand la Russie se sert de Microsoft pour faire taire ses opposants"
author: La rédaction
date: 2010-09-13
href: http://www.slate.fr/lien/27225/russie-microsoft-ecologie-logiciel
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Institutions
- Sensibilisation
- Désinformation
- Droit d'auteur
- International
---

> En janvier 2010, le Premier ministre russe Vladimir Poutine a décidé de remettre en service une usine de production de papier fermée depuis deux ans et située aux abords du lac Baïkal, en Sibérie. Celle-ci, qui emploie 2.000 personnes dans la région, est accusée par certains groupes écologistes de déverser des produits toxiques dans ce réservoir naturel qui détient 20% des réserves d’eau douce de la planète.
