---
site: Progilibre.com
title: "Les Aéroports de Lyon refondent leur site Internet en Open Source"
author: La rédaction
date: 2010-09-03
href: http://www.progilibre.com/Les-Aeroports-de-Lyon-refondent-leur-site-Internet-en-Open-Source_a1167.html
tags:
- Le Logiciel Libre
- Entreprise
---

> Open Wide, intégrateur de solutions Open Source, accompagne les Aéroports de Lyon dans la mise en œuvre et l’infogérance d’un nouveau portail Internet intégrant le meilleur des technologies Open Source du marché.
