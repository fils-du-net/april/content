---
site: ITCHANNEL.info
title: "Le Conseil National du Logiciel Libre soutient activement l'Open World Forum"
author: La rédaction
date: 2010-09-21
href: http://www.itchannel.info/index.php/articles/109751/conseil-national-logiciel-libre-soutient-activement-open-world-forum.html
tags:
- Le Logiciel Libre
- Entreprise
- Associations
- Promotion
---

> L'Open World Forum se tiendra à Paris les 30 septembre et 1er octobre 2010. En l'espace de trois ans, cet événement international a démontré sa capacité à rassembler à Paris les plus grands acteurs et penseurs d'un monde plus ouvert.
