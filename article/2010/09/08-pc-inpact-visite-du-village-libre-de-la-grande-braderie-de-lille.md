---
site: PC INpact
title: "Visite du village libre de la grande braderie de Lille"
author: David Legrand
date: 2010-09-08
href: http://www.pcinpact.com/dossiers/village-libre-lille-braderie-linux/175-1.htm
tags:
- Le Logiciel Libre
- April
- Sensibilisation
- Associations
- DADVSI
- DRM
- Promotion
- Contenus libres
---

> C'est une tradition dans le Nord depuis maintenant plusieurs centaines d'années : la braderie de Lille... et ses tas de moules. Elle a lieu tous les ans le premier week-end du mois de septembre, elle s'étend sur plus de 100 km et accueille plus de 3 millions de visiteurs.
