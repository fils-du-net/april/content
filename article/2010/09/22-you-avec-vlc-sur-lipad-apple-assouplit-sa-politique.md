---
site: YOU
title: "Avec VLC sur l'iPad, Apple assouplit sa politique"
author: emilie
date: 2010-09-22
href: http://you.leparisien.fr/conso-passions/2010/09/22/avec-vlc-sur-l-ipad-apple-assouplit-sa-politique-3647.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Interopérabilité
- International
---

> C'est la fin d'une époque et c'est tant mieux ! Désormais pour regarder un DivX sur son iPad, plus besoin de le convertir au format défini par (...)
