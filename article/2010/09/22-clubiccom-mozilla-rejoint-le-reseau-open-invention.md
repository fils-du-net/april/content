---
site: clubic.com
title: "Mozilla rejoint le réseau Open Invention"
author: Guillaume Belfiore
date: 2010-09-22
href: http://pro.clubic.com/entreprises/fondation-mozilla/actualite-367282-mozilla-rejoint-reseau-open-invention.html
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Associations
- Brevets logiciels
- International
---

> La fondation Mozilla, éditant le navigateur open source Firefox, annonce avoir rejoint le groupe Open Invention Network.  Fondé en novembre 2005 par de grands acteurs du marché de l'informatique comme [...]
