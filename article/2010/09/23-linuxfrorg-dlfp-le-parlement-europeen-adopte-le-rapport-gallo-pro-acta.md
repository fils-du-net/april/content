---
site: linuxfr.org
title: "DLFP: Le Parlement Européen adopte le rapport Gallo pro-ACTA"
author: Tiste
date: 2010-09-23
href: https://linuxfr.org/2010/09/23/27409.html
tags:
- Institutions
- Droit d'auteur
- Europe
- ACTA
---

> Le 22 septembre dernier, le parlement européen a adopté le rapport Gallo sur l'application du droit d'auteur. Qu'est ce que cela signifie ?
