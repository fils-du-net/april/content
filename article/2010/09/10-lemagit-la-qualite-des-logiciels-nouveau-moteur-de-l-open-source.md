---
site: LeMagIT
title: "La qualité des logiciels, nouveau moteur de l’Open Source "
author: Cyrille Chausson
date: 2010-09-10
href: http://www.lemagit.fr/article/logiciel-communaute-qualite-opensource/7054/1/la-qualite-des-logiciels-premier-moteur-adoption-open-source-par-les-entreprises/
tags:
- Le Logiciel Libre
- Entreprise
- Sensibilisation
- International
---

> Accenture révèle dans une étude sortie en août que les entreprises américaines, britanniques et irlandaises adoptent désormais l’Open Source, non plus pour ses coûts réduits, mais bien pour sa qualité logicielle, sa fiabilité et … sa sécurité. Un changement de mentalité évident qui s'accompagne pourtant d'une baisse des contributions des entreprises dans les communautés.
