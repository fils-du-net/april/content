---
site: 01net.
title: "Le hack de la PS3 disponible en open source"
author: Pierre Fontaine
date: 2010-09-02
href: http://www.01net.com/editorial/520379/hack-de-la-ps3-sony-nest-pas-au-bout-de-ses-peines/
tags:
- Le Logiciel Libre
- Entreprise
- Interopérabilité
- Droit d'auteur
- International
---

> Pendant longtemps la PS3 a su résister aux assauts des hackers. Fin août 2010, des programmeurs dévoilaient une méthode de jailbreak facile et rapide n'impliquant que l'utilisation d'une clé USB un peu particulière, la PSJailbreak, immédiatement annoncée comme disponible à la précommande moyennant environ 130 euros.
