---
site: itnews
title: "Stallman crashes European Patent session"
author: Liz Tay
date: 2010-09-21
href: http://www.itnews.com.au/News/232825,stallman-crashes-european-patent-session.aspx
tags:
- Le Logiciel Libre
- Institutions
- Brevets logiciels
- Philosophie GNU
- Europe
- International
- English
---

> (L'activiste Richard Stallman fait une apparition impromptue lors d'une présentation du Bureau des Brevets Européen à Brisbane) Software freedom activist Richard Stallman made an unexpected appearance at a European Patent Office presentation in Brisbane today.
