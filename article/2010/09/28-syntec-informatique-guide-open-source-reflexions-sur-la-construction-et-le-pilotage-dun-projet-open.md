---
site: Syntec informatique
title: "GUIDE OPEN SOURCE : Réflexions sur la construction et le pilotage d'un projet Open Source"
author: La rédaction
date: 2010-09-28
href: http://www.syntec-informatique.fr/bibliotheque/liste-des-publications/guide-open-source-reflexions-sur-la-construction-et-le-pilotage-d-un-projet-open-source
tags:
- Le Logiciel Libre
- Entreprise
- Économie
- Promotion
---

> Cette publication, résultat d'un foisonnement intellectuel intense ayant duré plus d'une année, représente à la fois une vulgarisation, un partage d'expérience et de préconisations quant à l'usage de l'Open Source.
