---
site: ZDNet.fr
title: "Wikipédia en français dépasse le million d'articles"
author: Thierry Noisette
date: 2010-09-23
href: http://www.zdnet.fr/blogs/l-esprit-libre/wikipedia-en-francais-depasse-le-million-d-articles-39754828.htm
tags:
- Internet
- Contenus libres
---

> Wikipédia, l'encyclopédie collaborative libre, fêtera ses dix ans en janvier 2011, et a débuté le 23 mars 2001 en version française, mais dès ce jeudi 23 septembre, à neuf ans et demi exactement, la Wikipédia francophone a enregistré officiellement le cap du million d'articles. Le français est ainsi la troisième langue à atteindre ce seuil après l'anglais (3,4 millions d'articles) et l'allemand (1,1 million), devant l'italien et le polonais qui pourraient être les prochains "millionnaires" de Wikipédia (voir le communiqué sur le million).
