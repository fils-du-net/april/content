---
site: LeJournalduNet
title: "La FSF encourage Google dans sa lutte contre Oracle"
author: La rédaction
date: 2010-09-10
href: http://www.journaldunet.com/solutions/breve/48406/la-fsf-encourage-google-dans-sa-lutte-contre-oracle.shtml
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Associations
- Brevets logiciels
- International
- English
---

> Brett Smith, l'un des piliers de la Free Software Foundation, a posté sur le blog officiel de l'association Open Source un billet pour soutenir Google dans la bataille juridique qui l'oppose à Oracle depuis cet été.
