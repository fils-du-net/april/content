---
site: techautes.cyberpresse.ca
title: "Libérez les logiciels!"
author: Marie-Eve Morasse
date: 2010-09-19
href: http://technaute.cyberpresse.ca/nouvelles/logiciels/201009/16/01-4316293-liberez-les-logiciels.php
tags:
- Le Logiciel Libre
- Administration
- Sensibilisation
- International
---

> Demain, quand vous ouvrirez votre ordinateur et que le logo de Windows ou celui d'iTunes s'affichera, ayez une petite pensée pour ceux feront  la fête en l'honneur des logiciels libres.
