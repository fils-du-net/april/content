---
site: GamePro.FR
title: "PS3 &amp; Linux : Sony demande un non-lieu"
author: Mathieu CHARTIER
date: 2010-09-23
href: http://www.gamepro.fr/2010/09/23/playstation/ps3-other-os/506415/
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- Interopérabilité
- International
---

> C'est début novembre que devrait se tenir le procès opposant Sony aux possesseurs d'une PS3 se plaignant de ne plus pouvoir installer Linux depuis la mise à jour 3.21 du 1er avril dernier. Le ton monte...
