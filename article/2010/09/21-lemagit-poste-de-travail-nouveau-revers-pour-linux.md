---
site: LeMagIT
title: "Poste de travail : nouveau revers pour Linux"
author: La rédaction
date: 2010-09-21
href: http://www.lemagit.fr/article/linux-suisse-soleure/7133/1/poste-travail-nouveau-revers-pour-linux/
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Administration
- Interopérabilité
- International
---

> Après la ville de Munich, c’est au tour d’un canton suisse de renoncer à ses projets de généralisation de Linux sur le poste de travail. Retour à Windows, donc, notamment pour des questions d’interopérabilité mais aussi, sinon surtout, de conduite du changement.
