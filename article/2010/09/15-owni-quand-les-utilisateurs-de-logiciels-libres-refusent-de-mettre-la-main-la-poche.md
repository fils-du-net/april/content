---
site: Owni
title: "Quand les utilisateurs de logiciels libres refusent de mettre la main à la poche"
author: Philippe Scoffoni
date: 2010-09-15
href: http://owni.fr/2010/09/14/quand-les-utilisateurs-de-logiciels-libres-refusent-de-mettre-la-main-a-la-poche/
tags:
- Le Logiciel Libre
---

> L’objet de cet article est de nous interroger à nouveau sur les travers de la gratuité et de son association aux logiciels libres, mais par un exemple concret. C’est l’histoire d’Olivier et de son plugin Wats qui vous est ici comptée
