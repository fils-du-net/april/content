---
site: LeMagIT
title: "L’Open Source, acteur clé pour la création d’une industrie logicielle en Europe "
author: Cyrille Chausson
date: 2010-09-15
href: http://www.lemagit.fr/article/europe-logiciel-cloud-computing-middleware-opensource-marche/7087/1/l-open-source-acteur-cle-pour-creation-une-industrie-logicielle-europe/
tags:
- Le Logiciel Libre
- Entreprise
- Économie
- Innovation
- Europe
---

> Selon PAC, l’Open Source doit aujourd’hui aider l’Europe à se doter d’une industrie du logiciel et des services performante en jouant un rôle de catalyseur sur les technologies de Cloud et du middleware. Deux segments aujourd’hui peu représentés sur le Vieux continent, qui pourraient toutefois décoller à condition de favoriser l’émergence du modèle ouvert.
