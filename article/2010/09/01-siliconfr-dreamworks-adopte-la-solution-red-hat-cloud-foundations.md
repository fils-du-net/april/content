---
site: Silicon.fr
title: "DreamWorks adopte la solution Red Hat Cloud Foundations"
author: David Feugey
date: 2010-09-01
href: http://www.silicon.fr/dreamworks-adopte-la-solution-red-hat-cloud-foundations-41703.html
tags:
- Le Logiciel Libre
- Entreprise
- Informatique en nuage
- International
---

> Le spécialiste de l'animation DreamWorks compte utiliser l'offre Red Hat Cloud Foundations au sein de son infrastructure matérielle. La compagnie va tout d'abord commencer par adopter les composants MRG de Red Hat.
