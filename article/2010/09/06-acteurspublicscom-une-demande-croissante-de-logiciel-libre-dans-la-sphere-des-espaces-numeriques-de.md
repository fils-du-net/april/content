---
site: Acteurs publics
title: "Une demande croissante de logiciel libre dans la sphère des espaces numériques de travail"
author: Olivier Vigneau
date: 2010-09-06
href: http://www.acteurspublics.com/club-des-acteurs-de-la-modernisation/ap66/logica/olivier-vigneau
tags:
- Le Logiciel Libre
- Entreprise
- Administration
- Sensibilisation
- Éducation
- Innovation
---

> Soutenus depuis 2003 par le Ministère de l’éducation et la Caisse des Dépôts, les Espaces Numériques de Travail (ENT) sont des environnements collaboratifs dédiés au monde scolaire et s’adressant à la communauté éducative (enseignants, personnels, élèves et familles) tout au long du parcours scolaire.
