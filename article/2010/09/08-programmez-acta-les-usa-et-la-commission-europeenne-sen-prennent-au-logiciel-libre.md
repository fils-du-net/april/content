---
site: PROgrammez!
title: "ACTA : Les USA et la Commission européenne s'en prennent au logiciel libre"
author: Frédéric Mazué
date: 2010-09-08
href: http://programmez.com/actualites.php?id_actu=8066
tags:
- Le Logiciel Libre
- April
- DRM
- Informatique-deloyale
- Europe
- ACTA
---

> "En adoptant ce mercredi 8 septembre 2010 la déclaration écrite 12/2010, le Parlement européen prend position pour l'ACTA, accord «anti-contrefaçon» négocié en secret par la Commission européenne, qui attaque le Logiciel Libre" déclare l'APRIL dans un communiqué.
