---
site: LeMonde.fr
title: "Le logiciel OpenOffice s'émancipe"
author: Damien Leloup
date: 2010-09-28
href: http://www.lemonde.fr/technologies/article/2010/09/28/le-logiciel-openoffice-s-emancipe_1417321_651865.html
tags:
- Le Logiciel Libre
- Entreprise
- Associations
---

> Des membres de la communauté qui développe le numéro deux de la bureautique ont créé une fondation pour s'affranchir en partie d'Oracle, six mois après le rachat de Sun Microsystems.
