---
site: Electron Libre
title: "La guerre des OS va (enfin) commencer !"
author: Richard Grandmorin
date: 2010-09-07
href: http://www.electronlibre.info/La-guerre-des-OS-va-enfin,00855
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- Sensibilisation
- Innovation
---

> Le lancement de Google chrome OS et les succès d’Apple vont mettre au fin au monopole de Microsoft sur le marché des systèmes opérationnels (OS).
