---
site: Progilibre.com
title: "La Poste s’équipe en Xivo, la solution VoIP Open Source de Avencall"
author: Xivo
date: 2010-09-15
href: http://www.progilibre.com/La-Poste-s-equipe-en-Xivo-la-solution-VoIP-Open-Source-de-Avencall_a1177.html
tags:
- Le Logiciel Libre
- Entreprise
- Sensibilisation
---

> Deux déménagements coup sur coup en 2 ans pour une entité de près de 1 000 personnes, la nécessité de s’installer dans un ancien bâtiment où le réseau doit être refait à neuf : c’est le déclic ! « Simplifions, économisons, faisons le choix de la VoIP, une technologie qui la reconnaissance du marché. », propose Olivier Lenoir, responsable des infrastructures informatiques de ce service. Au regard du défi que comportait cette installation pour la start-up Proformatique (Avencall), retenir ce prestataire était, pour la Poste, un choix audacieux.
