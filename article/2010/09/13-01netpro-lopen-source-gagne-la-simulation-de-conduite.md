---
site: 01netPro.
title: "L'open source gagne la simulation de conduite"
author: Boris Mathieux
date: 2010-09-13
href: http://pro.01net.com/editorial/520859/l-open-source-gagne-la-simulation-de-conduite/
tags:
- Le Logiciel Libre
- Entreprise
- Innovation
---

> Soutenu par l'éditeur de logiciels de simulation de conduite Oktal, le format ouvert RoadXML renforce son organisation de fonctionnement. Dans le même temps, Arts et Métiers Paristech et Renault lancent le logiciel libre OpenSD2S.
