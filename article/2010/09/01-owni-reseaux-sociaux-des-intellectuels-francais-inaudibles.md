---
site: OWNI
title: "Réseaux Sociaux: des intellectuels français inaudibles"
author: Cecil Dijoux
date: 2010-09-01
href: http://owni.fr/2010/09/01/reseaux-sociaux-des-intellectuels-francais-inaudibles/
tags:
- Le Logiciel Libre
- Sensibilisation
- Philosophie GNU
---

> Alors que les réseaux sociaux modifient chaque jour un peu plus notre perception du monde et nos rapports aux autres, les penseurs anglo-saxons s'emparent du sujet. Du côté des intellectuels français, on fait face à un silence éloquent.
