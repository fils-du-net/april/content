---
site: OWNI
title: "OpenBTS: un réseau mobile open source qui pourrait changer le monde"
author: Julie Bort (trad. Sylvain Lapoix)
date: 2010-09-10
href: http://owni.fr/2010/09/10/openbts-un-reseau-mobile-open-source-qui-pourrait-changer-le-monde/
tags:
- Le Logiciel Libre
- Entreprise
- Interopérabilité
- Innovation
- Licenses
- International
---

> Compact, open source et très peu coûteux, le réseau mobile OpenBTS a tout pour plaire. Testé au festival Burning Man, en plein désert du Nevada, il pourrait bien révolutionner le quotidien de pays déshérités et de zones isolées, comme il l'a fait en Haïti ou dans une base de chercheurs en Antarctique.
