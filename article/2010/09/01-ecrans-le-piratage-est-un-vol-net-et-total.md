---
site: écrans
title: "« Le piratage est un vol net et total »"
author: Andréa Fradin
date: 2010-09-01
href: http://www.ecrans.fr/Le-piratage-est-un-vol-net-et,10711.html
tags:
- Le Logiciel Libre
- Partage du savoir
- Institutions
- Désinformation
- Droit d'auteur
- International
---

> C’est une demi-surprise. Hier, le Secrétaire d’État au Commerce américain Gary Locke a assimilé vol et piratage à l’occasion d’un discours sur la propriété intellectuelle et sa mise en application. « Comme le vice-Président Biden l’a dit à plus d’une occasion, “le piratage est un vol net et total", et en conséquence, il devrait être traité comme tel” » .
