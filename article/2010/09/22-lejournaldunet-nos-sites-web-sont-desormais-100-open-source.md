---
site: LeJournalduNet
title: "\"Nos sites Web sont désormais 100% Open Source\""
author: Virgile Juhan
date: 2010-09-22
href: http://www.journaldunet.com/solutions/dsi/arnaud-barbosa-leclercq-dsi-et-sites-du-groupe-bel.shtml
tags:
- Le Logiciel Libre
- Entreprise
- Internet
---

> Le groupe Bel ne cache de fortes ambitions pour sa dizaine de sites Web. Plusieurs changements techniques majeurs ont déjà été opérés, à commencer par un nouvel hébergeur.
