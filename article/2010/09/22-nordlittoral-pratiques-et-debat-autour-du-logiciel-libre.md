---
site: NordLittoral
title: "Pratiques et débat autour du logiciel libre"
author: O.F
date: 2010-09-22
href: http://www.nordlittoral.fr/actualite/calais/Vie_locale/article_1280891.shtml
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Licenses
- Philosophie GNU
- Promotion
- Contenus libres
---

> Nord Littoral - April des amis de l'Alhambra a organisé samedi dernier avec l'association UNG, des étudiants du Master ingénierie du logiciel libre (ILL) de l'université du littoral Côte d'Opale une petite journée d'information et de prise en main de logiciels libres comme Linux par exemple.
