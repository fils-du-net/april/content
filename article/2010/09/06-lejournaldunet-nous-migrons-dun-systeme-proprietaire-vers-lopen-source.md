---
site: LeJournalduNet
title: "\"Nous migrons d'un système propriétaire vers l'Open Source\""
author: Virgile Juhan
date: 2010-09-06
href: http://www.journaldunet.com/solutions/dsi/stephane-rios-le-site-d-e-commerce-met-le-cap-sur-l-open-source.shtml
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- Innovation
---

> Intégration de TopAchat.com et d'ALaPage.com, changement d'hébergeur, refonte du moteur de recherche... Le site d'e-commerce multiplie les projets de rationalisation et d'amélioration de son interface.
