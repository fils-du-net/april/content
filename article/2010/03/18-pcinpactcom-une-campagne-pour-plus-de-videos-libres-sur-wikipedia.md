---
site: pcinpact.com
title: "Une campagne pour plus de vidéos (libres) sur Wikipedia"
author: Jeff
date: 2010-03-18
href: http://www.pcinpact.com/actu/news/55949-wikipedia-encyclopedie-wikimedia-open-video-alliance-video.htm
tags:
- Partage du savoir
- Accessibilité
- Brevets logiciels
- Éducation
- Video
- Contenus libres
---

> [...] Il est déjà possible de mettre des vidéos sur l’encyclopédie libre. Mais la campagne, baptisée « Mettons la vidéo sur Wikipedia », veut démocratiser et généraliser ces contributions, le nombre de vidéos sur Wikimedia Commons étant encore très faible.
> [...] Les vidéos seront lues sans l’aide de Flash ou de Silverlight, grâce à la plateforme open source Kaltura HTML5. Toujours dans l’esprit du libre, ces contributions doivent utiliser une licence Creative Commons BY-SA ou un équivalent car « tout Wikipedia peut être partagée et réutilisée librement ».
