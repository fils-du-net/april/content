---
site: cyberpresse.ca
title: "Contrat sans appel d'offres: les logiciels libres montent au front"
author: Ian Bussières
date: 2010-03-08
href: http://www.cyberpresse.ca/le-soleil/actualites/justice-et-faits-divers/201003/07/01-4258266-contrat-sans-appel-doffres-les-logiciels-libres-montent-au-front.php
tags:
- Le Logiciel Libre
- Administration
---

> (Québec) La Cour supérieure entendra cette semaine au palais de justice de Québec la poursuite de l'entreprise québécoise Savoir-faire Linux, spécialisée dans le service et le développement de logiciels libres, contre la Régie des rentes du Québec (RRQ), qu'elle accuse d'avoir octroyé sans appel d'offres à la multinationale Microsoft un contrat de 720 000 $ pour la mise à jour de ses postes de travail.
