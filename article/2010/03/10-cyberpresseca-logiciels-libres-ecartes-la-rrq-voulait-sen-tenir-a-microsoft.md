---
site: cyberpresse.ca
title: "Logiciels libres écartés: la RRQ voulait s'en tenir à Microsoft"
author: Pierre Asselin
date: 2010-03-10
href: http://www.cyberpresse.ca/le-soleil/actualites/justice-et-faits-divers/201003/09/01-4259024-logiciels-libres-ecartes-la-rrq-voulait-sen-tenir-a-microsoft.php
tags:
- Logiciels privateurs
- Administration
---

> La Régie des rentes du Québec voulait s'en tenir à une seule plate-forme, Microsoft, pour éviter de se disperser dans plusieurs technologies, a-t-on appris mardi au deuxième jour du procès intenté par Savoir-faire Linux (SFL).
> Bernard Bourret était chef des services technologiques au moment de la réalisation du contrat contesté par SFL. La Régie avait fait le choix d'adopter un seul «écosystème» technologique, celui de Microsoft, pour plusieurs raisons, notamment pour faciliter l'intégration de toutes les composantes.
