---
site: "generation-nt.com"
title: "Rennes : les données du réseau de transport en accès ouvert"
author: Christian D.
date: 2010-03-09
href: http://www.generation-nt.com/rennes-keolis-metropole-data-reseau-public-actualite-975581.html
tags:
- Administration
- Interopérabilité
---

> [...] C'est une première en France : Kéolis Rennes, exploitant du réseau STAR ( Service des Transports de l'Agglomération Rennaise ), épaulé par Rennes Métropole et la Ville de Rennes, met à disposition de tous, et en open source, les données publiques liées au réseau de transport, aux infrastructures et à la disponibilité des équipements, " ainsi que les données d'informations pratiques géolocalisées de 1500 programmes publics et associatifs. "
