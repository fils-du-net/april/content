---
site: journaldunet.com
title: "Banalisation de l'Open Source, bonne nouvelle ?"
author: Patrice Bertrand
date: 2010-03-24
href: http://www.journaldunet.com/solutions/expert/45894/banalisation-de-l-open-source--bonne-nouvelle.shtml?utm_source=benchmail&utm_medium=ML5&utm_campaign=E10168071
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] L'Open Source continue de monter en puissance, et il n'est pratiquement plus une entreprise dont une part du système d'information ne soit construit avec des composants et solutions Open Source.   L'Open Source est partout, et tous les acteurs se disent désormais Open Source.  Les anglo-saxons disent « Open Source is going mainstream », c'est à dire, en quelque sorte, entre dans la normalité, voire même devient le standard.  Est-ce la consécration ?  Oui, d'une certaine manière.  Est-ce la fin de l'histoire ?  Certainement pas.
