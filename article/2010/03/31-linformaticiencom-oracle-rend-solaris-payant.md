---
site: linformaticien.com
title: "Oracle rend Solaris payant "
author: Emilien Ercolani  
date: 2010-03-31
href: http://www.linformaticien.com/Actualit%C3%A9s/tabid/58/newsid496/8041/oracle-rend-solaris-payant/Default.aspx
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] Jusqu’à maintenant, vous pouviez télécharger et utiliser Solaris (dans sa dernière version stable 10) gratuitement. Mais les choses vont changer : Oracla a annoncé que l’OS Unix de Sun, racheté l’année dernière, deviendra désormais payant. Il faudra désormais souscrire à un contrat de service pour l’utiliser. La messe est dite !
