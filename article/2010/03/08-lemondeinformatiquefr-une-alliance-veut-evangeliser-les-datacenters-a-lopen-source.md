---
site: lemondeinformatique.fr
title: "Une alliance veut évangéliser les datacenters à l'open source"
author: Jacques Cheminat
date: 2010-03-08
href: http://www.lemondeinformatique.fr/actualites/lire-une-alliance-veut-evangeliser-les-datacenters-a-l-open-source-30091.html
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] L'Open Source Data Center Initiative a été fondée par Dave Ohara, un ingénieur formateur (Hewlett Pakcard, Apple et Microsoft) qui travaille maintenant sur le blog Green Data Center. Il a collaboré avec l'université du Missouri pour construire un datacenter, où il a testé des technologies issues de l'open source. Ce projet a été réalisé en plusieurs étapes et a soulevé plusieurs questions sur sa façon de fonctionner.
