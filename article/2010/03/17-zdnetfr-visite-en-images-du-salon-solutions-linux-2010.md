---
site: zdnet.fr
title: "Visite en images du salon Solutions Linux 2010"
author: Christophe Auffray
date: 2010-03-17
href: http://www.zdnet.fr/galerie-image/0,50018840,39750198-5,00.htm
tags:
- Le Logiciel Libre
- April
- Sensibilisation
- Vente liée
- Brevets logiciels
- DRM
---

> April : non aux brevets, aux DRM et à la vente liée. Acteur de premier plan du monde Open Source, l’April (qui a récemment élu un nouveau président, Tangui Morlier), affiche quelques uns de ses combats sur la devanture de son stand : la lutte contre les brevets, les DRM et la vente liée.
