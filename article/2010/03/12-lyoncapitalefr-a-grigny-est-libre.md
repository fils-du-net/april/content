---
site: lyoncapitale.fr
title: "« A Grigny, on est libre ! »"
author: Florent Deligia
date: 2010-03-12
href: http://www.lyoncapitale.fr/lyoncapitale/journal/univers/Guide/High-tech-Web-Jeux-Videos/A-Grigny-on-est-libre-%21
tags:
- Le Logiciel Libre
- Administration
---

> Pourquoi avoir choisi de promouvoir le logiciel libre ?
> Il s’agit avant tout d’une décision politique et philosophique. Nous refusons que notre usage de l’informatique soit guidé par un monopôle. Si nous voulons sensibiliser les gens aux Technologie de l’information et de la communication (TIC), il faut choisir les logiciels libres. Nous pouvons les partager comme nous le souhaitons et il y a une idée d’entraide très importante derrière tout ça.
