---
site: pcinpact.com
title: "Wikipédia annonce des changements pour avril prochain"
author: Nil Sanyas 
date: 2010-03-29
href: http://www.pcinpact.com/actu/news/56112-wikipedia-edition-benevoles-contributeurs-articles.htm
tags:
- Partage du savoir
- Éducation
---

> [...] Le but de ces nombreuses rénovations est principalement d’attirer les utilisateurs actifs, c’est-à-dire ceux qui amélioreront les articles, voire mieux encore, ceux qui en créeront. « La participation des bénévoles est l'essence de tout ce que nous faisons ; notre travail consiste à faciliter et à soutenir le travail des bénévoles » résume ainsi la fondation Wikimedia.
