---
site: zdnet.fr
title: "Linux sur la console PS3, c'est fini a décidé Sony"
author: Christophe Auffray
date: 2010-03-29
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39750447,00.htm
tags:
- Entreprise
- Logiciels privateurs
- Informatique-deloyale
---

> [...] Le 1er avril, Sony diffusera une mise à jour du firmware de sa console de jeu, la PS3. Particularité notable de la version 3.21 du firmware : elle n'autorisera plus l'installation sur la PS3 d'un système d'exploitation tiers comme Linux.
> Fonctionnalité présente dès les origines de la PS3, elle va désormais disparaître a décidé le constructeur japonais. Raison invoquée : la sécurité (sans plus de détails de la part de Sony).
