---
site: pcinpact.com
title: "Salon Solutions Linux 2010 : Interview de l'APRIL "
author: Marc Rees 
date: 2010-03-16
href: http://www.pcinpact.com/actu/news/55871-tangui-morlier-april-benoit-sibaud.htm
tags:
- April
---

> Alors que s’ouvre aujourd’hui le salon Solutions Linux 2010, Benoît Sibaud et Tangui Morlier, ex et nouveau présidents de l’APRIL ont bien voulu répondre à nos questions autour des thèmes de l’association : la promotion et la défense du logiciel libre.  Une interview croisée et l’occasion de faire un point avec cette structure qui compte désormais plus de 5300 adhérents.
