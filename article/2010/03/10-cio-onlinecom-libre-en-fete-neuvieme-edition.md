---
site: "cio-online.com"
title: "Libre en Fête : neuvième édition"
author: La rédaction
date: 2010-03-10
href: http://www.cio-online.com/agenda/lire-libre-en-fete-neuvieme-edition-6039.html
tags:
- Le Logiciel Libre
- April
- Sensibilisation
---

> Initiée et coordonnée par l'April, l'initiative Libre en Fête est relancée pour la neuvième année consécutive : pour accompagner l'arrivée du printemps, des évènements de découverte des Logiciels Libres et du Libre en général seront proposés partout en France autour du 21 mars 2010, dans une dynamique conviviale et festive.
