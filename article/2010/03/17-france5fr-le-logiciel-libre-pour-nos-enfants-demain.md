---
site: france5.fr
title: "Le logiciel libre pour nos enfants demain ?"
author: Emma Rota
date: 2010-03-17
href: http://blog.france5.fr/maternelles/index.php/2010/03/17/173692-emma-rota-clair-et-net
tags:
- Le Logiciel Libre
- Éducation
---

> [...] Si ces enseignants optent pour le logiciel libre à l’école c’est parce qu’ils adhèrent à une éthique que personne ne peut décemment rejeter : notre école publique n’est pas un lieu marchand ; donc il devrait être interdit de laisser toute la place à des logiciels propriétaires type ceux de Microsoft alors qu’il existe d’autres solutions plus neutres.
> Ce qui ne veut pas dire que ces initiatives n’ont pas un coût. Elles ont un coût comme toute chose sur cette Terre, au moins le coût de la sueur et du temps de celui qui le fabrique.
