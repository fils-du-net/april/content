---
site: pcinpact.com
title: "Une cour espagnole juge tous les échanges P2P légaux"
author: Jeff
date: 2010-03-15
href: http://www.pcinpact.com/actu/news/55852-espagne-echanges-p2p-legaux-justice.htm
tags:
- Internet
- HADOPI
- Neutralité du Net
---

> [...] Dans un jugement de 8 pages (en espagnol), Raúl N. García Orejudo, magistrat au Tribunal de Commerce de Barcelone, affirme que « le système de liens hypertextes constitue la base même d'Internet. Une multitude de pages et de moteurs de recherche (comme Google) permettent techniquement de faire ce que la plainte veut interdire : trouver des liens vers les réseaux P2P. » M. Calderón ne touchait pas d'argent de son site, et n'hébergeait aucun fichier illégal. Le juge en a conclu qu'il n'a donc enfreint aucune loi espagnole.
