---
site: numerama.com
title: "Bruxelles organisera un tour de table sur l'ACTA le 22 mars prochain"
author: Guillaume Champeau
date: 2010-03-01
href: http://www.numerama.com/magazine/15162-bruxelles-organisera-un-tour-de-table-sur-l-acta-le-22-mars-prochain.html
tags:
- ACTA
---

> [...] Elle devrait tenter de rassurer sur les objectifs de l'ACTA, ce qu'elle a déjà fait partiellement en assurant que l'Europe s'opposera à toute mesure de riposte graduée imposée par le traité. Ce qui est consistant avec l'analyse que nous faisions du texte issu du round de négociation de Mexico, qui n'impose pas la riposte graduée mais en fait un simple exemple de bonne pratique à l'égard des FAI.
