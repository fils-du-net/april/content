---
site: mondediplo.net
title: "ACTA : chapitre deux"
author: Philippe Rivière
date: 2010-03-20
href: http://blog.mondediplo.net/2010-03-20-ACTA-chapitre-deux
tags:
- ACTA
---

> Le Monde diplomatique vient d’obtenir une copie de la section 2 du projet de traité ACTA, intitulée « Mesures aux frontières » (Border Measures). Soit une dizaine de pages qui exposent, dans un grand luxe de détails pratiques, le futur fonctionnement des douanes au regard de tous les « biens contrefaisant des droits de propriétés intellectuelle ».
