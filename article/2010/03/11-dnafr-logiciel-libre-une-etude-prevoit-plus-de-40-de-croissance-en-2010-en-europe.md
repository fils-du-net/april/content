---
site: dna.fr
title: "Logiciel libre: une étude prévoit plus de 40% de croissance en 2010 en Europe"
author: AFP
date: 2010-03-11
href: http://actu.dna.fr/27425b624509fd0f62fd39.16.056.html
tags:
- Le Logiciel Libre
- Économie
---

> [...] En France, le logiciel libre a progressé de 33% en 2009, à 1,47 milliards d'euros, et est attendu en hausse de 30% cette année.
> "La France est le plus gros marché en Europe" et "le plus mûr", ce qui explique que "sa croissance est un peu inférieure aux autres marchés", a déclaré à l'AFP l'analyste Mathieu Poujol.
