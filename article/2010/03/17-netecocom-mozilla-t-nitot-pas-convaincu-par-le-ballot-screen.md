---
site: neteco.com
title: "Mozilla : T. Nitot pas convaincu par le ballot screen"
author: Guillaume Belfiore
date: 2010-03-17
href: http://www.neteco.com/330590-mozilla-nitot-convaincu-ballot-screen.html
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
---

> [...] En revanche, chez Mozilla, la prudence est de mise. Interrogé par nos soins, Tristan Nitot avoue rester perplexe sur l'efficacité réelle de cet écran d'installation : « on ne peut pas mesurer l'efficacité de la chose ». Et d'ajouter : « Microsoft ne propose pas la mise à jour à tout le monde, ils n'ont pas ouvert le robinet en grand ».
