---
site: pcinpact.com
title: "UE : Menaces sur l'open source et l'interopérabilité"
author: Marc Rees
date: 2010-03-26
href: http://www.pcinpact.com/actu/news/56095-nellies-kroes-agenda-politique-numerique.htm
tags:
- Institutions
- ACTA
---

> Selon plusieurs sources concordantes, Nelly Kroes subirait actuellement des pressions via la Direction Générale Entreprise afin d’évincer la notion de « standards ouverts », des normes open-source et de l’interopérabilité dans l’agenda de la politique numérique de l’Union européenne. Des notions qui seraient jugées trop floues pour y être conservées.
