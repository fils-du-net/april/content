---
site: programmez.com
title: "Linux contrôlera les votes au Royaume-Uni"
author: Frédéric Mazué
date: 2010-03-09
href: http://www.programmez.com/actualites.php?titre_actu=Linux-controlera-les-votes-au-Royaume-Uni&id_actu=6993
tags:
- Le Logiciel Libre
- Administration
---

> D'après notre confrère delimiter, la Victoria’s Electoral Commission a annoncé son intention d'utiliser des bornes de votes électroniques pilotées par des systèmes d'exploitation Linux.
