---
site: pcinpact.com
title: "Menace sur le libre en Europe : lettre ouverte de l’APRIL"
author: Marc Rees
date: 2010-03-30
href: http://www.pcinpact.com/actu/news/56153-logiciel-libre-agenda-numerique-april.htm
tags:
- Le Logiciel Libre
- April
- Europe
---

> Nous le disions la semaine dernière, des pressions seraient actuellement exercées sur Nellie Kroes afin de supprimer de l’agenda de la politique numérique de l’Union européenne, les notions de standards ouverts et d’interopérabilité.
> Pour l’April, cet épisode s’inscrit dans une certaine logique : « ces pressions s'inscrivent dans un contexte dans lequel les lobbies du logiciel propriétaire, Microsoft en tête, tentent de réviser à la baisse la définition de standard ouvert. »
