---
site: liberation.fr
title: "«Inventons ensemble une Coopérative politique»"
author: Daniel Cohn-Bendit
date: 2010-03-22
href: http://www.liberation.fr/politiques/0101625905-inventons-ensemble-une-cooperative-politique
tags:
- Le Logiciel Libre
---

> [...] Il est nécessaire de «repolitiser» la société civile en même temps que de «civiliser» la société politique et faire passer la politique du système propriétaire à celui du logiciel libre.
