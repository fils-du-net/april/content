---
site: lemondeinformatique.fr
title: "Microsoft ne fait toujours pas l'unanimité sur le salon Linux"
author: Serge Leblal
date: 2010-03-18
href: http://www.lemondeinformatique.fr/actualites/lire-microsoft-ne-fait-pas-l-unanimite-sur-le-salon-linux-30197.html
tags:
- Logiciels privateurs
---

> Dans une ambiance très potache, les membres du GCU ont gentiment chahuté Microsoft sur le salon Solutions Linux le mercredi 17 mars en milieu d'après midi. Alors que le stand était complétement fermé par un grand ruban rouge pour éviter la contamination et qu'une inscription rouge sang « MS m'a tuer » était gribouillée sur les murs du pavillon, le personnel de Microsoft - toujours très professionnel - tentait de travailler comme si de rien n'était.
