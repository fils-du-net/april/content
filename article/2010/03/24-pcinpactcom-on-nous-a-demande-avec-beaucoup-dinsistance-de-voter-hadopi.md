---
site: pcinpact.com
title: "«On nous a demandé avec beaucoup d'insistance de voter Hadopi»"
author: Marc Rees
date: 2010-03-24
href: http://www.pcinpact.com/actu/news/56034-deputes-godillots-assemblee-nationale-hadopi.htm
tags:
- HADOPI
---

> [...] Evoquant les pressions subies par les parlementaires, le député raconte : « J'ai pris comme exemple la loi Hadopi, qui était ce qu'elle était, mais elle nous a mis à dos un certain nombre de jeunes. Elle a hésité, tergiversé, puis finalement elle a été votée parce qu'on nous l'a demandé avec beaucoup d'insistance... euh voilà, c'était pas une bonne chose [...] La position des députés était très inconfortable. »
