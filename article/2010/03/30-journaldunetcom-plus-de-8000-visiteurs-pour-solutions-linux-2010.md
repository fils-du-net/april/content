---
site: journaldunet.com
title: "Plus de 8000 visiteurs pour Solutions Linux 2010"
author: La rédaction
date: 2010-03-30
href: http://www.journaldunet.com/solutions/breve/45990/plus-de-8000-visiteurs-pour-solutions-linux-2010.shtml
tags:
- Le Logiciel Libre
- Entreprise
---

> Le salon Solutions Linux 2010 qui s'est tenu les 16, 17 et 18 mars 2010 au Parc des expositions de Paris a accueilli quelque 8200 visiteurs. Plusieurs géants de l'informatique, comme Microsoft et Oracle, avaient répondu présents. Malgré tout, les visiteurs ont pu regretter l'absence de deux poids lourds de l'Open Source : Red Hat et Novell.
