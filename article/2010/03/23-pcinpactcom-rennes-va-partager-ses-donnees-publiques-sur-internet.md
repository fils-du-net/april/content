---
site: pcinpact.com
title: "Rennes va partager ses données publiques sur Internet"
author: Jeff
date: 2010-03-23
href: http://www.pcinpact.com/actu/news/56028-rennes-donnees-publiques-creative-commons-ville.htm
tags:
- Administration
- Contenus libres
---

> La communauté d’agglomération Rennes Métropole, la Ville de Rennes et Kéolis Rennes ont ouvert leurs données publiques depuis le 1er mars et les partagent sur Internet.
> [...] Toutes ces données sont partagées sous la licence Creative Commons, avec une restriction empêchant l’utilisation commerciale de la base de données. Les développeurs ne pourront donc pas vendre leurs applications au public (sur l’App Store par exemple) ou y intégrer de publicité, bien que la ville n’exclut pas de changer la licence à terme pour autoriser les applications commerciales.
