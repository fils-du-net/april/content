---
site: zdnet.fr
title: "La France disposerait de deux négociateurs à l'ACTA"
author: Christophe Auffray
date: 2010-03-01
href: http://www.zdnet.fr/actualites/internet/0,39020774,39713412,00.htm
tags:
- ACTA
---

> Politique - Selon le fondateur de la Quadrature du Net, la France participe aux négociations secrètes de l’ACTA et dispose de deux négociateurs de la DGTPE, la direction générale du Trésor et de la politique économique.
