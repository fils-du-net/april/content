---
site: cyberpresse.ca
title: "La conquête planétaire du logiciel libre"
author: Pierre Asselin
date: 2010-03-27
href: http://www.cyberpresse.ca/le-soleil/actualites/science-et-technologie/201003/26/01-4264851-la-conquete-planetaire-du-logiciel-libre.php
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Administration
---

> (Québec) Pendant que le gouvernement québécois se bat devant les tribunaux pour lui fermer la porte, le logiciel libre s'impose tranquillement à la grandeur de la planète, de la Norvège jusqu'au Brésil, dans des projets à grande échelle.
> Dans la bataille juridique opposant Savoir-faire Linux (SFL) à la Régie des rentes du Québec, le gouvernement défend son «droit» d'éviter les appels d'offres, quand vient le temps de se procurer des logiciels, pour acheter directement de Microsoft. Curieusement, le Québec préfère le monopole de la multinationale à la concurrence.
