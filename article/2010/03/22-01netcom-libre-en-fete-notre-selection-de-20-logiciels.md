---
site: 01net.com
title: "Libre en fête : notre sélection de 20 logiciels"
author: La rédaction
date: 2010-03-22
href: http://www.01net.com/editorial/514377/libre-en-fete-nos-20-logiciels-preferes/
tags:
- Le Logiciel Libre
- April
---

> [...] L'association de promotion et de défense du logiciel libre April coordonne chaque année depuis neuf ans la manifestation Libre en fête à l'arrivée du printemps. Le but : faire découvrir les logiciels libres (Firefox, GNU...) et les valeurs de la communauté. Pour cette édition 2010, ce sont 141 événements qui sont organisés, un peu partout en France, tout au long du mois de mars.
