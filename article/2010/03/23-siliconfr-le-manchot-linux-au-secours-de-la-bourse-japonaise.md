---
site: silicon.fr
title: "Le manchot Linux au secours de la Bourse japonaise"
author: David Feugey
date: 2010-03-23
href: http://www.silicon.fr/fr/news/2010/03/23/le_manchot_linux_au_secours_de_la_bourse_japonaise
tags:
- Le Logiciel Libre
- Entreprise
---

> Le Tokyo Stock Exchange (TSE), plus prosaïquement connu comme étant la Bourse de Tokyo, est la deuxième place boursière au monde. Elle a récemment basculé son système informatique vers une solution Red Hat Enterprise Linux, laquelle fonctionne sur des serveurs Intel fournis par Fujitsu.
