---
site: lemagit.fr
title: "LeMagIT::Open Source : Mozilla veut rendre sa licence compatible avec Apache"
author: Cyrille Chausson
date: 2010-03-16
href: http://www.lemagit.fr/article/licences-mozilla-apache-opensource/5860/1/open-source-mozilla-veut-rendre-licence-compatible-avec-apache/
tags:
- Le Logiciel Libre
- Internet
- Licenses
---

> Mozilla a annoncé son intention de refondre la licence Open Source MPL (Mozilla Public Licence) dans le but de la rendre compatible avec la licence Apache. La MPL encadre les projets clé de la fondation, comme Firefox ou Thunderbird, ou encore OpenSolaris, l’Unix Open Source de Sun.
