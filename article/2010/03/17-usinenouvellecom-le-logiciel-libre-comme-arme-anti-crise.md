---
site: usinenouvelle.com
title: "Le logiciel libre comme arme anti-crise"
author: Christophe Dutheil
date: 2010-03-17
href: http://www.usinenouvelle.com/article/le-logiciel-libre-comme-arme-anti-crise.N128028
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] Crise ou pas crise ? Selon Pierre Audoin Consultants (PAC), il n'y a pas lieu de s'inquiéter : le « logiciel libre ne s'est quasiment jamais aussi bien porté » en France où il a généré un chiffre d'affaires de 1,47 milliard d'euros en 2009. Un chiffre en croissance de 33% par rapport à l'année précédente. Mieux, ce segment de marché devrait encore croître de 30% cette année, le « logiciel libre étant souvent une alternative peu onéreuse pour réaliser certains projets durant les périodes de vaches maigres ».
