---
site: lemagit.fr
title: "Free Cloud Alliance : un coup de projecteur sur un cloud 100 % libre "
author: Cyrille Chausson
date: 2010-03-31
href: http://www.lemagit.fr/article/saas-france-cloud-computing-libre-mandriva-nexedi-open-source-cloud/5988/1/free-cloud-alliance-coup-projecteur-sur-cloud-100-libre/
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Informatique en nuage
---

> La Free Cloud Alliance vise à attirer l’attention sur l’existence d’une pile logicielle, composée de briques technologiques libres, couvrant les concepts du Iaas, Paas et Saas. Une opération qui a pour but de se faire entendre des entreprises et des pouvoirs publics.
