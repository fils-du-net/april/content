---
site: lequotidien.editpress.lu
title: "On n'arrête pas le progrès"
author: Isabelle Ducreuzet
date: 2010-03-06
href: http://lequotidien.editpress.lu/le-pays/9239.html
tags:
- Le Logiciel Libre
- Administration
- Éducation
---

> [...] Le projet a pour objectif la promotion des logiciels libres. Carlos Breda, agent de développement local rappelle que le CIGL a un centre de formation multimédia : «Nous avons une Internetstuff rue de l'Alzette à Esch-sur-Alzette et les salariés ont participé à une formation Linux. C'est un système d'exploitation libre et gratuit. Nous avions remarqué qu'au Luxembourg, il existait très peu de promotion sur les logiciels libres.»
