---
site: lepoint.fr
title: "Google s'inquiète du filtrage du Web en France"
author: Guerric Poncet
date: 2010-03-12
href: http://www.lepoint.fr/actualites-technologie-internet/2010-03-12/internet-google-s-inquiete-du-filtrage-du-web-en-france/1387/0/433094
tags:
- Internet
- HADOPI
- Neutralité du Net
- ACTA
---

> Les oreilles de la France ont sifflé, jeudi soir, lors de la remise par Reporters sans frontières du prix du Net-Citoyen. David Drummond, vice-président de Google, a glissé dans son discours une pique fort acérée contre le gouvernement de Nicolas Sarkozy. Après avoir rappelé la situation catastrophique des libertés fondamentales dans certains régimes totalitaires, le haut responsable du géant de l'Internet a dit tout haut ce que beaucoup pensent tout bas.
