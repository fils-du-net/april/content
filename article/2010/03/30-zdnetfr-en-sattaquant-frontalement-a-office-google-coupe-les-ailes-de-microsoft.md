---
site: zdnet.fr
title: "En s'attaquant frontalement à Office, Google \"coupe les ailes\" de Microsoft"
author: Didier Durand
date: 2010-03-30
href: http://www.zdnet.fr/blogs/media-tech/en-s-attaquant-frontalement-a-office-google-coupe-les-ailes-de-microsoft-39750507.htm
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
---

> [...] Pourquoi cette virulence (qui ne date pas d'aujourd'hui...)  contre la bureautique MS ? Parce que, dans la lutte qui oppose les 2 titans de Mountain View et Redmond, c'est une des clefs de la bataille: en rognant des parts de marché sur Office, Google va clairement appauvrir drastiquement Microsoft et lui couper les ailes de ses investissements futurs.
