---
site: tekiano.com
title: "Tunisie-Logiciel Libre : Pas assez cher, mon fils !"
author: Samy Ben Naceur
date: 2010-03-04
href: http://www.tekiano.com/tek/soft/3-10-1853/tunisie-logiciel-libre-pas-assez-cher-mon-fils-.html
tags:
- Le Logiciel Libre
---

> Les gens pensent que tout ce qui est cher est forcément meilleur. Ils se font arnaquer sans même s’en rendre compte. Le défaut de l’Open Source ? Pas assez cher, mon fils ! Les défenseurs tunisiens du monde libre n’ont pas froid aux yeux.
