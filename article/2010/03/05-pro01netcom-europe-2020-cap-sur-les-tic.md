---
site: pro.01net.com
title: "Europe 2020, cap sur les TIC"
author: Xavier Biseul
date: 2010-03-05
href: http://pro.01net.com/editorial/513666/bruxelles-devoile-sa-strategie-numerique-pour-2020-sans-convaincre/
tags:
- Institutions
- Europe
---

> Bruxelles dévoile sa stratégie numérique pour 2020... sans convaincre
> [...] « Le marché unique a été conçu avant l'arrivée d'internet, avant que les TIC ne deviennent l'un des principaux moteurs de la croissance », précise l'étude. « La demande mondiale de technologies de l'information et de la communication est évaluée à 2 000 milliards d'euros, mais seulement un quart provient des entreprises européennes. » A noter que Google, l'April ou la Quadrature du net ont été consultés lors de l'élaboration de cette stratégie.
