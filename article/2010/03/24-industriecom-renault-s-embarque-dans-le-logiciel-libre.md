---
site: industrie.com
title: "Renault s’embarque dans le logiciel libre"
author: Ridha Loukil
date: 2010-03-24
href: http://www.industrie.com/it/automobile/renault-s-embarque-dans-le-logiciel-libre.9536
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] Le logiciel libre intéresse de plus en plus l’industrie automobile, du moins pour les fonctions d’information et de loisirs (infotainment) dans le véhicule. Après BMW, General Motors et PSA Peugeot Citroën, c’est au tour de Renault de rejoindre l’alliance GENIVI en tant que membre du conseil d’administration de l’organisation.
