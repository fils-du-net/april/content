---
site: journaldunet.com
title: "Novell ciblé par une OPA hostile de 2 milliards de dollars"
author: Dominique Filippone
date: 2010-03-04
href: http://www.journaldunet.com/solutions/systemes-reseaux/opa-novell-par-elliott-associates.shtml?f_id_newsletter=2567&utm_source=benchmail&utm_medium=ML5&utm_campaign=E10166266
tags:
- Entreprise
---

> L'éditeur de SuSE Linux et principal sponsor d'OpenSuse est attaqué par le fonds Elliott Associates. La marge de manoeuvre du directoire de Novell reste plus étroite que jamais.
