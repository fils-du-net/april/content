---
site: pcinpact.com
title: "Le ballot screen entraine un fort recul d'Internet Explorer"
author: Nil Sanyas
date: 2010-03-23
href: http://www.pcinpact.com/actu/news/56026-internet-explorer-firefox-opera-chrome-ballot-screen.htm
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
---

> [...] Durant la même période, toujours pour la France, Firefox a grimpé d’1,2 point, avec 36,57 % de PDM. Chrome a pour sa part gagné 0,8 point (6,65 % de PDM). Opera, par contre, n’a que peu progressé dans l’Hexagone, avec une hausse minime de 0,11 point (1,19 %).
