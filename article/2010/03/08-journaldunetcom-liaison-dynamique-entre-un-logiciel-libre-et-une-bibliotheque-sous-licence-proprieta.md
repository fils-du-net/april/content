---
site: journaldunet.com
title: "Liaison dynamique entre un logiciel libre et une bibliothèque sous licence propriétaire"
author: Olivia Flipo
date: 2010-03-08
href: http://www.journaldunet.com/developpeur/expert/45475/liaison-dynamique-entre-un-logiciel-libre-et-une-bibliotheque-sous-licence-proprietaire.shtml?utm_source=benchmail&utm_medium=ML5&utm_campaign=E10167938
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Licenses
---

> L’auteur d'une bibliothèque, sous licence propriétaire, est-il tenu de communiquer les codes sources de celle-ci, quand elle se retrouve connectée à un logiciel sous licence libre ? Analyse de la General Public Licence version 2.
