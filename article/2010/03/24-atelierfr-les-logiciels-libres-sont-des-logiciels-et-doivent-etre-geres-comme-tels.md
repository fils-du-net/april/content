---
site: atelier.fr
title: "Les logiciels libres sont des logiciels ! Et doivent être gérés comme tels"
author: Pierre Bosche
date: 2010-03-24
href: http://www.atelier.fr/informatique/4/24032010/logiciels-libres-accenture-open-source-strategie-logicielle-systeme-d-information-39544-.html
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] Pour libérer pleinement leur potentiel, les logiciels libres doivent s’inscrire dans une stratégie (externalisation, optimisation du portefeuille applicatif ou encore migration de l'infrastructure en place) qui prend en compte la totalité de l'environnement de l’entreprise. L’utilisation de l’open source est une idée intéressante mais requiert des compétences et une attention accrue de la société utilisatrice.
