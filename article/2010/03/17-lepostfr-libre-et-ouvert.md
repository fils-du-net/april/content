---
site: lepost.fr
title: "\"Libre et ouvert\""
author: La rédaction
date: 2010-03-17
href: http://www.lepost.fr/article/2010/03/10/1981306_libre-et-ouvert.html
tags:
- Le Logiciel Libre
---

> [...] Lui, s'intéresse au logiciels libres, qu'il explique d'une métaphore: "Il y a ceux qui pensent qu'un logiciel n'est qu'un outil. Comme un crayon, mais en plus compliqué, pour simplifier. Et que tout ce qu'on lui demande, c'est de fonctionner. Et il y a ceux qui pensent que ce qui compte, ce n'est pas tant que le crayon fonctionne, mais qu'il n'enferme pas l'utilisateur à certains usages ("vous pouvez écrire ceci mais pas cela", "vous ne pouvez pas prêter votre stylo à votre voisin", "vous devez acheter telle marque d'encre pour le recharger").
