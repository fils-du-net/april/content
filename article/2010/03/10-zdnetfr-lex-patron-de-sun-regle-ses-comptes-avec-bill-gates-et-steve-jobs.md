---
site: zdnet.fr
title: "L'ex-patron de Sun règle ses comptes avec Bill Gates et Steve Jobs"
author: Christophe Auffray
date: 2010-03-10
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39750045,00.htm
tags:
- Entreprise
- Logiciels privateurs
---

> [...] Sans tergiverser Bill Gates serait entré directement dans le vif du sujet : « Microsoft détient le marché des suites bureautiques, et nos brevets sont partout dans OpenOffice. »
> [...] Le patron de Microsoft aurait en effet à l'époque proposé à Sun une licence Office, offrant ainsi de payer de l'argent à l'éditeur pour chaque téléchargement d'OpenOffice. Une version numérique du racket juge Jonathan Schwartz sur son blog.
