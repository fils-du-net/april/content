---
site: zdnet.fr
title: "Bercy et le ministère de la Culture diffusent une extension libre pour OpenOffice"
author: Christophe Auffray
date: 2010-03-24
href: http://www.zdnet.fr/actualites/it-management/0,3800005311,39750360,00.htm
tags:
- Le Logiciel Libre
- Administration
---

> Afin de simplifier l'application de la loi relative à l’utilisation des termes étrangers dans les documents administratifs, les ministères des Finances et de la Culture ont développé et diffusé au sein de l’administration un correcteur terminologique pour OpenOffice.
> [...] Le code source a été reversé à la communauté OpenOffice. Ce logiciel libre (LGPL v3) se présente comme une extension pour la suite bureautique.
