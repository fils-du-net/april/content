---
site: zdnet.fr
title: "ACTA : les eurodéputés votent une résolution pour obtenir plus de transparence"
author: Christophe Auffray
date: 2010-03-10
href: http://www.zdnet.fr/actualites/internet/0,39020774,39750040,00.htm
tags:
- Internet
- ACTA
---

> Législation - A une large majorité, 663 voix contre 13, les députés européens ont voté une résolution exigeant de la Commission européenne la divulgation au public des documents relatifs à l’ACTA et que soit écarté tout dispositif de riposte graduée.
> [...] Ils estiment ainsi que « l'accord proposé ne doit pas offrir la possibilité d'imposer une procédure de riposte graduée en trois temps » et rappellent à l'exécutif européen que le respect de la vie privée et la protection des données sont des valeurs essentielles de l'UE.
