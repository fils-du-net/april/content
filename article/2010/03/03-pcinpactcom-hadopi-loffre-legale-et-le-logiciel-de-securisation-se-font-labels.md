---
site: pcinpact.com
title: "HADOPI : l'offre légale et le logiciel de sécurisation se font labels"
author: Marc Rees
date: 2010-03-03
href: http://www.pcinpact.com/actu/news/55669-offre-legal-labelisation-logiciel-securisation.htm
tags:
- HADOPI
---

> [...] Le second décret serait celui visé par les articles L331-32 et 33. Il concerne la procédure d'évaluation et de labellisation des moyens de sécurisation. C’est là la pierre angulaire d’HADOPI puisque c’est « la mise en œuvre » de ces logiciels de sécurisation qui permettra à l’abonné d’échapper à la riposte graduée. Plusieurs questions se posent : « mise en œuvre » signifie-t-il « installation » ou « usage » ?
