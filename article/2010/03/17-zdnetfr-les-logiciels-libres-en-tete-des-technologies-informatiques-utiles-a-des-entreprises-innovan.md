---
site: zdnet.fr
title: "Les logiciels libres en tête des technologies informatiques utiles à des entreprises innovantes"
author: Thierry Noisette
date: 2010-03-17
href: http://www.zdnet.fr/blogs/l-esprit-libre/les-logiciels-libres-en-tete-des-technologies-informatiques-utiles-a-des-entreprises-innovantes-39750203.htm
tags:
- Le Logiciel Libre
- Entreprise
- April
---

> [...] On devrait en parler à Solutions Linux ce jeudi, par exemple lors du débat sur le libre et la relance économique (à 14 heures): une étude* de Tarsus (organisateur du salon) avec l'April, intitulée «Solutions Linux 2010» montre le rôle déterminant que jouent pour les entreprises répondantes les logiciels libres dans leur capacité à innover.
