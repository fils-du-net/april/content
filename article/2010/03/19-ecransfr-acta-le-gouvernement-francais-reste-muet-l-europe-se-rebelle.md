---
site: ecrans.fr
title: " ACTA : le gouvernement français reste muet, l’Europe se rebelle"
author: Alexandre Hervaud
date: 2010-03-19
href: http://www.ecrans.fr/ACTA-le-gouvernement-francais,9476.html
tags:
- April
- ACTA
---

> [...] Frédéric Couchet, délégué général de l’April, ne cache pas déception. « On a vraiment eu l’impression d’être pris pour des charlots, des bizuts incapables de lire des textes de loi. Alors qu’au contraire, on ne demande qu’à pouvoir contribuer au débat ».
