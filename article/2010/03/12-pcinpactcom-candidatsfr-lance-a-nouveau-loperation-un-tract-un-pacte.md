---
site: pcinpact.com
title: "Candidats.fr lance (à nouveau) l'opération \"un tract, un pacte\""
author: theocrite
date: 2010-03-12
href: http://www.pcinpact.com/actu/news/55831-pacte-logiciel-libre-regionales-2010.htm
tags:
- Le Logiciel Libre
---

> [...] À la veille du premier tour des élections régionales, l'initiative de l'April candidats.fr réitère l'opération "un tract, un Pacte !". Pour rappel, candidats.fr est une initiative originale de l'April visant, depuis 2007, à faire signer aux candidats aux diverses élections (présidentielle, législative, municipale, cantonale, européenne puis régionale) un pacte du Logiciel Libre dans lequel les candidats déclarent avoir pris conscience que le Logiciel Libre est un enjeu important et s'engagent à le promouvoir.
