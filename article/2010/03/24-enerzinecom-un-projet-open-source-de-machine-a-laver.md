---
site: enerzine.com
title: "Un projet \"Open Source\" de machine à laver "
author: La rédaction
date: 2010-03-24
href: http://www.enerzine.com/603/9404+un-projet-open-source-de-machine-a-laver+.html
tags:
- Partage du savoir
- Licenses
---

> Une équipe d'étudiants en 2ème année de l'Ecole Supérieure d'Art d'Aix-en-Provence ont bricolé une machine à laver rudimentaire qui utilise uniquement des matériaux disponibles dans les déserts comme le Sahara.
> La machine comprend entre autres, des éléments de vélos (roues + pneus), du bambou, un panneau solaire le tout directement relié à un moteur électrique recyclé provenant d'un photocopier.
