---
site: businessmobile.fr
title: "Mozilla, Opera : 'Halte aux diktats d'Apple dans la validation des applications iPhone!'"
author: Olivier Chicheportiche
date: 2010-03-10
href: http://www.businessmobile.fr/actualites/analyses/0,39044174,39750034,00.htm
tags:
- Logiciels privateurs
- Interopérabilité
---

> [...] Il est également interdit aux développeurs de faire interagir leurs applications avec d'autres technologies, réduisant ainsi les possibilités des terminaux d'Apple d'être interopérables avec des outils open source, souligne l'EFF.
