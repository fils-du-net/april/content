---
site: lemondeinformatique.fr
title: "Les entreprises de Rhône-Alpes s'unissent autour du logiciel libre"
author: Jacques Cheminat
date: 2010-03-05
href: http://www.lemondeinformatique.fr/actualites/lire-les-entreprises-de-rhone-alpes-s-unissent-autour-du-logiciel-libre-30082.html
tags:
- Le Logiciel Libre
- Entreprise
---

> Ploss Rhône-Alpes vient de naître officiellement. Cette association, initiée sur Lyon fin 2009, réunit les entrepreneurs du logiciel libre et a pour objectif de promouvoir un écosystème régional.
