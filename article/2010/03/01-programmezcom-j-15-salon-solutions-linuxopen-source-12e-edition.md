---
site: programmez.com
title: "J-15 : Salon Solutions Linux/Open Source 12e édition"
author: Frédéric Mazué
date: 2010-03-01
href: http://www.programmez.com/actualites.php?titre_actu=J-15---Salon-Solutions-Linux-Open-Source-12e-edition&id_actu=6919
tags:
- Le Logiciel Libre
- Entreprise
- Informatique en nuage
---

> Le rendez-vous majeur du marché de l’Open Source dévoile le programme de sa 12e édition. Le salon Solutions Linux/Open Source réunira cette année 220 exposants et partenaires.
> Les experts présents - juristes, économistes, analystes, intégrateurs, éditeurs, communautés et associations, - délivreront une analyse complète du secteur, orientée autour de thèmes centraux : interopérabilité, progiciels, virtualisation, mobilité et bonnes pratiques.
