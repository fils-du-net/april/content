---
site: awt.be
title: "Open ERP. Le logiciel libre comme levier de croissance"
author: La rédaction
date: 2010-03-31
href: http://www.awt.be/web/ebu/index.aspx?page=ebu,fr,tem,005,001
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] Contrairement à ses grands concurrents, Tiny ne touche pas donc pas d'argent sur l'installation des logiciels. Pour autant, Open ERP est rentable et génère son flux de revenus pour la société. "Les grands groupes comme SAP, après tout, ne se rémunère qu'à 25% sur la vente de licences, dit Fabien Pinckaers. En revanche, ils ont des frais des développements très significatifs. La différence de chiffre d'affaires est compensée par le fait que notre structure de coûts est moindre".
