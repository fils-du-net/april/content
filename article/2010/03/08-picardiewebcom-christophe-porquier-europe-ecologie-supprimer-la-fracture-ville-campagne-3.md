---
site: picardieweb.com
title: "Christophe Porquier (Europe Ecologie): 'Supprimer la fracture ville - campagne' (3)"
author: Charline Galls
date: 2010-03-08
href: http://www.picardieweb.com/article-picardie-christophe-porquier-europe-ecologie-supprimer-la-fracture-ville--campagne-3-1316.htm
tags:
- Le Logiciel Libre
- Administration
---

> [...] Le logiciel libre doit être encouragé car il s'agit d'une approche d'équité: Les logiciels sont accessibles à tous quelque soient les moyens financiers. Le logiciel libre permet d'autre part d'optimiser une démarche de recyclage des machines, le libre contrairement au système propriétaire, supporte très bien l'utilisation de machines relativement anciennes.
> Développer l'usage du libre dans les établissements présente aussi l'intérêt de donner un recul technique plus important aux jeunes plutôt que d'en faire des consommateurs de logiciels commerciaux.
