---
site: ecrans.fr
title: " « Les jeunes ne sont plus intéressés par l’outil-ordi »"
author: Astrid Girardeau
date: 2010-03-10
href: http://www.ecrans.fr/Les-jeunes-ne-sont-plus-interesses,9392.html
tags:
- Le Logiciel Libre
- Internet
- Éducation
---

> [...] Avec le développement du logiciel libre et du do-it-yourself (faites-le vous-mêmes), l’idée de prendre le pouvoir sur la machine existe. Mais la tendance de l’informatique ne va pas vers ça. Le grand public est de plus en plus un consommateur passif. La volonté de maîtriser la machine a disparu. On ne fait que l’utiliser ou être utilisé par elle.
