---
site: adjb.net
title: "Microsoft Fails the Standards Test"
author: Alex Brown
date: 2010-03-31
href: http://www.adjb.net/post/Microsoft-Fails-the-Standards-Test.aspx
tags:
- Logiciels privateurs
- Interopérabilité
- English
---

> Il y a deux ans l'ISO approuvait de justesse le format de bureautique ouvert OOXML de Microsoft destiné a concurencer l'ODF. Mais ce format n'est toujours pas utilisé correctement par les produits Microsoft.
