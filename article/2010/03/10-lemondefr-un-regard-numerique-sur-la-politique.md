---
site: lemonde.fr
title: "Un regard numérique sur la politique"
author: Damien Leloup
date: 2010-03-10
href: http://www.lemonde.fr/technologies/article/2010/03/10/un-regard-numerique-sur-la-politique_1316813_651865.html
tags:
- Le Logiciel Libre
- April
- Institutions
---

> [...] "Nous venons tous les deux de l'activisme en faveur du logiciel libre", note Benjamin Ooghe-Tabanou, "mais ce n'est pas le cas de tous les membres." "D'ailleurs, nous sommes parfois un peu tatillons lorsque nous travaillons à plusieurs sur un projet. Pas question d'utiliser des librairies ou du code qui n'est pas libre", précise Tangui Morlier. Ce dernier vient d'ailleurs d'être élu président de l'April, la principale association française de défense du logiciel libre.
