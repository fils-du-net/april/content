---
site: 01net.com
title: "Humeur : « Le logiciel libre est un obscur, un clandestin »"
author: Renaud Bonnet
date: 2010-03-26
href: http://pro.01net.com/editorial/514666/humeur-le-logiciel-libre-cet-obscur-ce-clandestin/
tags:
- Le Logiciel Libre
---

> Nicolas de Cues comparait Dieu à « un cercle dont le centre se trouve partout et la périphérie nulle part ». Coluche interprétait à sa façon la métaphore, en une formule applicable aux logiciels libres et open source : comme le sucre dans le lait chaud, ils sont partout mais on ne les voit pas, et plus on les cherche, moins on les trouve.
