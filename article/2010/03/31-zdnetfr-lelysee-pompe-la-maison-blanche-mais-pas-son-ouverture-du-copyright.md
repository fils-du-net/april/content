---
site: zdnet.fr
title: "L'Elysée pompe la Maison Blanche... mais pas son ouverture du copyright"
author: Thierry Noisette
date: 2010-03-31
href: http://www.zdnet.fr/blogs/l-esprit-libre/l-elysee-pompe-la-maison-blanche-mais-pas-son-ouverture-du-copyright-39750547.htm
tags:
- Le Logiciel Libre
- Administration
---

> [...] "Partager, quelle drôle d'idée", est-ce là la vision présidentielle d'Internet? Dans le même esprit, on se rappellera que l'oubli de la mention des logiciels libres qui ont permis à des agences web de créer des sites semble de tradition autour de l'actuel président de la République: il y a quelques mois, tant le site de Carla Bruni que celui du «débat sur l'identité nationale» d'Eric Besson négligeaient de mentionner qu'ils étaient développés, l'un avec Wordpress, l'autre avec Spip.
