---
site: zinfos974.com
title: "Le vrai président de la Région Réunion, c'est Microsoft "
author: La rédaction
date: 2010-03-11
href: http://www.zinfos974.com/Le-vrai-president-de-la-Region-Reunion,-c-est-Microsoft_a15592.html?com
tags:
- Logiciels privateurs
- Administration
---

> [...] Avec une quarantaine de lycées sur l'Île de la Réunion, le budget "spécial Bill Gates" du Conseil Régional s'élève à environ 70.000 euros par an. Ce n'est certes pas beaucoup dans un budget mais
> [...] Ces dépenses sont totalement inutiles, et évitables; en effet il existe des équivalents libres à chacun de ces logiciels de chez tonton Bill, dont certains faits ou faisables à la Réunion. En effet mes concitoyens réunionnais, contrairement à leurs dirigeants, ont toutes les compétences pour cela.
