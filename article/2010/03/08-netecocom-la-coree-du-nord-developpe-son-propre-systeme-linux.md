---
site: neteco.com
title: "La Corée du Nord développe son propre système Linux"
author: Guillaume Belfiore
date: 2010-03-08
href: http://www.neteco.com/328808-coree-du-nord-developpe-propre-systeme-linux.html
tags:
- Le Logiciel Libre
- Institutions
---

> [...] Notons une différence sur la date du calendrier (99 au lieu de 2010). Cette différence est le fruit de l'idéologie Juche selon laquelle la date de naissance de l'ancien président Kim II Sung (1912) équivaut à l'an 1.
