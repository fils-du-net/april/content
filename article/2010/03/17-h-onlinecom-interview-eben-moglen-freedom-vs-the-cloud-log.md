---
site: "h-online.com"
title: "Interview: Eben Moglen - Freedom vs. The Cloud Log"
author: Glyn Moody
date: 2010-03-17
href: http://www.h-online.com/open/features/Interview-Eben-Moglen-Freedom-vs-the-Cloud-Log-955421.html
tags:
- Internet
- Partage du savoir
- English
---

> Interview d'Eben Moglen, juriste américain et ancien conseillé de la FSF, où il exprime ses craintes face au danger que représente la collecte de données personnelles par les grandes entreprises du Web.
