---
site: ictjournal.ch
title: "Microsoft Live@edu ne devrait pas entrer dans les écoles suisses"
author: La rédaction
date: 2010-03-18
href: http://www.ictjournal.ch/News/SendRecommandationNews.aspx?ObjectId=2634&ObjectType=1&Digest=lBFRIHgoqxdCh10B5IHgmg
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Éducation
---

> [...] Il arrive à la conclusion que la nouvelle proposition de Microsoft, qui souhaite lier son offre Microsoft Live@edu, présente des inconvénients majeurs pour les écoles. Il recommande donc dans un communiqué «de renoncer à l'acquisition ou au renouvellement de licences pour des produits Microsoft dans le cadre d'un School Agreement et de miser progressivement sur des infrastructures souples et des logiciels libres.»
