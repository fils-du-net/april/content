---
site: Zdnet.fr
title: "Biens communs: contenus et logiciels libres font leurs débuts en régions"
author: Thierry Noisette
date: 2010-03-11
href: http://www.zdnet.fr/blogs/l-esprit-libre/biens-communs-contenus-et-logiciels-libres-font-leurs-debuts-en-regions-39750077.htm
tags:
- Le Logiciel Libre
- Administration
- April
- Contenus libres
---

> Pas moins de dix régions (Alsace, Aquitaine, Champagne-Ardenne, Centre, Ile-de-France, Limousin, Lorraine, Haute-Normandie, Poitou-Charentes et Rhône-Alpes) sur les 22 en métropole sont membres de l'Adullact (Association des développeurs et des utilisateurs de logiciels libres pour l'administration et les collectivités territoriales), et la région francilienne est aussi membre de l'April et soutient entre autres de la fondation Wikimédia.
