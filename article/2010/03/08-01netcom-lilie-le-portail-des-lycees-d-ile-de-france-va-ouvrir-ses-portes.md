---
site: 01net.com
title: "Lilie, le portail des lycées d’île-de-France, va ouvrir ses portes"
author: Marie Jung
date: 2010-03-08
href: http://pro.01net.com/editorial/513691/lilie-le-portail-des-lycees-d-ile-de-france-va-ouvrir-ses-portes/
tags:
- Le Logiciel Libre
- Éducation
---

> Le déploiement de l’environnement numérique de travail pour les lycées franciliens commence dans 60 établissements. A terme, cet outil fondé sur des logiciels open source équipera 471 lycées.
> Après avoir effectué des pilotes avec une quinzaine d'établissements sur des solutions différentes, y compris propriétaires, le président de la région, Jean-Paul Huchon, a fait le choix de l'open source. Nicolas Tissot, DSI de la région Ile-de-France, commente : « Il n'existait pas de solution nativement capable de supporter la volumétrie dont nous avions besoin (plus d'un million d'utilisateurs à terme). A partir du moment où un investissement important était nécessaire pour faire évoluer la solution, il nous a paru plus logique d'investir dans l'open source. »
