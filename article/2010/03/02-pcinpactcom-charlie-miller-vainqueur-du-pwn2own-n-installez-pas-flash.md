---
site: pcinpact.com
title: "Charlie Miller, vainqueur du Pwn2Own : « N’installez pas Flash »"
author: Vincent Hermann
date: 2010-03-02
href: http://www.pcinpact.com/actu/news/55647-pwn2own-charlie-miller-navigateurs-avis-securite-failles.htm
tags:
- Internet
- Logiciels privateurs
- Interopérabilité
- Accessibilité
- Standards
---

> Chaque année, une compétition un peu spéciale a lieu : le concours Pwn2Own. Le but tourne autour de la sécurité : des hackers s’affrontent pour être les premiers à percer une plateforme particulière. Les années précédentes, ce sont les Mac qui ont fait parler d’eux, Safari ayant permis aux « gentils » pirates d’entrer dans le système. Mais ce n’était pas Safari lui-même qui présentait une faille : c’était le plug-in Flash.
