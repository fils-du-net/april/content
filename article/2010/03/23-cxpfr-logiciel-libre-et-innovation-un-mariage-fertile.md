---
site: CXP.fr
title: "Logiciel libre et innovation, un mariage fertile"
author: Claire Leroy
date: 2010-03-23
href: http://www.cxp.fr/flash-cxp/edito-logiciel-libre-innovation-mariage-fertile_930
tags:
- Le Logiciel Libre
---

> Le logiciel libre est-il forcément synonyme d'innovation pour l'entreprise ? Peut-on superposer allégrement, comme s'il s'agissait de la même chose, les formules "Open Source" et "Open Innovation" ? Le salon Solutions Linux 2010, qui vient de fermer ses portes après trois jours d'intense activité et une affluence record, a été l'occasion de relancer le débat sur le rapport entre le libre et la capacité de l'édition logicielle à innover.
