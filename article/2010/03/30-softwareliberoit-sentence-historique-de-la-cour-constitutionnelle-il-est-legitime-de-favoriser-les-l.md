---
site: softwarelibero.it
title: "Sentence historique de la Cour Constitutionnelle: Il est légitime de favoriser les logiciels libres"
author: marco
date: 2010-03-30
href: http://softwarelibero.it/Corte_Costituzionale_favorisce_softwarelibero_fr
tags:
- Le Logiciel Libre
- Administration
---

> [...] En définitif, selon la Cour, préférer le logiciel libre n'enfreint pas la libre de concurrence, car la liberté du logiciel est une caractéristique juridique de caractère général et non une caractéristique technologique liée à un produit ou marque spécifique: cette sentence expose l'inconsistance des arguments qui, jusqu'à maintenant, se sont opposés à l'adoption de normes qui favorisent l'utilisation du logiciel libre en disant que conflit avec le principe de neutralité technologique.
