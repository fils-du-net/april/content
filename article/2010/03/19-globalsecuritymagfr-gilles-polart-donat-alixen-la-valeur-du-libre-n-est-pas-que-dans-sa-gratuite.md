---
site: globalsecuritymag.fr
title: "Gilles Polart-Donat, Alixen : « la valeur du libre n’est pas que dans sa gratuité »"
author: Emmanuelle Lamandé 
date: 2010-03-19
href: http://www.globalsecuritymag.fr/Gilles-Polart-Donat-Alixen-la,20100319,16655
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] GS Mag : Quel état faites-vous du marché du libre aujourd’hui ?
> Gilles Polart-Donat : Le libre continue très nettement à gagner des parts de marché, cependant on observe une perversion du logiciel libre avec des solutions qui sont faussement libres. Seule une partie est open source, principalement en façade. Cela fausse le message et l’apport aux entreprises. Ce phénomène est souvent la résultante de problèmes de financement.
> [...] La valeur du libre n’est pas forcément dans sa gratuité. Il n’y a pas que la gratuité qui doit faire choisir le libre.
