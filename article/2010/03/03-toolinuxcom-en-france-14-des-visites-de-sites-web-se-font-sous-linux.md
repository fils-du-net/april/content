---
site: toolinux.com
title: "En France, 1.4% des visites de sites web se font sous Linux "
author: La rédaction
date: 2010-03-03
href: http://www.toolinux.com/lininfo/toolinux-information/revue-de-presse/article/en-france-1-4-des-visites-de-sites
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
---

> [...] Dans les 4 pays étudiés ici, Apple connaît une belle progression entre janvier 2009 et janvier 2010 au détriment de Microsoft. Elle est de 2,4 points en France et 2,5 au Royaume-Uni. Sur les 12 derniers mois, Linux se maintient en troisième position dans ces 4 pays. C’est en France qu’il est le plus utilisé avec 1.4% des visites en moyenne pour un site en janvier 2010.
