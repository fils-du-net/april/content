---
site: wikimedia.fr
title: "Appréhender la véritable taille de Wikipédia"
author: Johann Dréo
date: 2010-03-03
href: http://blog.wikimedia.fr/apprehender-la-veritable-taille-de-wikipedia-1249
tags:
- Internet
- Partage du savoir
---

> [...] Un des aspects de cette différence est la taille. Il existe des Wikipédia en 250 langues (avec des contenus différents). 250 langues différentes, deux-cent cinquante langues. Considérant toutes ces langues, Wikipédia est formée [1] de 28 500 000 articles, vingt-huit millions cinq-cent mille articles. On peut y ajouter la médiathèque (Wikimédia Commons), formée de 5,6 millions de fichiers [2]. Tout cela est visité chaque mois par 346 000 000 visiteurs uniques, trois-cent quarante-six millions de visiteurs.
