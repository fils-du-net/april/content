---
site: arretsurimages.net
title: "Subventions : Rue89 reversera en partie à Drupal"
author: La rédaction
date: 2010-01-09
href: http://www.arretsurimages.net/vite.php?id=6681
tags:
- Le Logiciel Libre
---

> Thierry Crouzet, blogueur invité sur le plateau de la ligne j@une, a expliqué que les développements de Drupal accomplis grâce à la subvention de l'État devaient, selon lui, être intégralement reversés à la communauté.
> "Toutes les innovations qu'on apporte sur Drupal, on les remet dans la communauté, où nous sommes très actifs", a alors assuré Pierre Haski, le président de Rue89, tout en indiquant devoir se renseigner auprès de son directeur technique.
