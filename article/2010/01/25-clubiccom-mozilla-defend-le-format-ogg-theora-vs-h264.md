---
site: clubic.com
title: "Mozilla défend le format Ogg Theora vs H.264 "
author: Guillaume Belfiore 
date: 2010-01-25
href: http://www.clubic.com/actualite-321558-mozilla-defend-ogg-theora-vs-264.html
tags:
- Internet
- Interopérabilité
- Video
---

> La semaine dernière, nous apprenions que les sites Internet YouTube et Vimeo redoublaient leurs efforts concernant la prise en charge du HTML 5. La filiale de Google a décidé d'étendre davantage sa phase de tests tandis que Vimeo précise qu'environ 90% des vidéos mises en ligne l'année dernière fonctionneront avec ce nouveau lecteur. Les deux services se basent sur la technologie propriétaire H.264.
