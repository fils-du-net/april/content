---
site: journaldugeek.com
title: "Internet Explorer perd de nouvelles parts de marché"
author: Ben
date: 2010-01-04
href: http://www.journaldugeek.com/2010/01/04/internet-explorer-perd-de-nouvelles-parts-de-marche/
tags:
- Internet
- Logiciels privateurs
---

> Selon une étude menée par Net Application lors du trimestre dernier, il semble que le monopole d’Internet Explorer s’effrite peu à peu. Ainsi, le navigateur de Microsoft ne truste plus “que” 62.7% de PDM. Les analystes de Net Application ne sont d’ailleurs pas vraiment optimistes concernant son futur, puisqu’ils laissent entendre que sa part de marché pourrait passer sous les 50% dans le courant de l’année 2010.
