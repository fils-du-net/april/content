---
site: cio.com.au
title: "NZ school ditches Microsoft and goes totally open source"
author: Angus Kidman
date: 2010-01-25
href: http://www.cio.com.au/article/333686/nz_school_ditches_microsoft_goes_totally_open_source
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Administration
- International
- English
---

> En Nouvelle Zelande, une école fonctionne entièrement avec des logiciels libres, malgré l'obligation par le gouvernement d'utiliser le logiciel de Microsoft.
