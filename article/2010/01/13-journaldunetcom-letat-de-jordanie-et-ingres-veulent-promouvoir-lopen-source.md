---
site: journaldunet.com
title: "L'Etat de Jordanie et Ingres veulent promouvoir l'Open Source"
author: La rédaction
date: 2010-01-13
href: http://www.journaldunet.com/solutions/breve/international/44433/l-etat-de-jordanie-et-ingres-veulent-promouvoir-l-open-source.shtml
tags:
- Le Logiciel Libre
- Administration
- International
---

> Ingres Corporation et l'État jordanien annoncent leur coopération en vue d'établir le premier accord entre une société internationale et des pouvoirs publics pour promouvoir et permettre l'adoption de l'Open Source.
