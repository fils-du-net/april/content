---
site: zdnet.fr
title: "Vente liée : des consommateurs italiens veulent engager une action de groupe contre Microsoft"
author: La rédaction
date: 2010-01-06
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39711967,00.htm
tags:
- Logiciels privateurs
- Vente liée
---

> Juridique - En vertu d'une nouvelle loi autorisant les recours collectifs, une association de consommateurs transalpins va poursuivre Microsoft en justice. Elle réclame le remboursement de Windows pour des clients qui ont acheté un PC et qui ne veulent pas utiliser cet OS. C'est un tribunal de Florence qui devrait se prononcer dans le courant de la semaine sur la recevabilité de la plainte déposée par l'ADUC.
