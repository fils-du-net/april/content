---
site: journaldunet.com
title: "La fondation Linux lance son propre job board"
author: Francois Thierry
date: 2010-01-15
href: http://www.journaldunet.com/solutions/emploi-rh/job-board-fondation-linux.shtml
tags:
- Le Logiciel Libre
- Entreprise
---

> La fondation Linux vient d'ouvrir son Linux Jobs Board, un centre d'emploi disponible à tous. L'utilisation croissante de Linux dans les entreprises a crée unedemande forte autour de profils capables de maîtriser la distribution et ses outils, et aujourd'hui nombreux sont ceux qui recherchent des salariés selon ce critère.
