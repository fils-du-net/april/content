---
site: lemonde.fr
title: "L'ACTA, le traité secret qui doit réformer le droit d'auteur"
author: La rédaction
date: 2010-01-25
href: http://www.lemonde.fr/technologies/article/2010/01/25/l-acta-le-traite-secret-qui-doit-reformer-le-droit-d-auteur_1296265_651865.html
tags:
- Neutralité du Net
---

> L'Anti-counterfeiting Trade Agreement (ACTA) est un traité international sur le droit d'auteur, négocié de façon informelle entre une dizaine de pays ou d'organisations [...] Le but du traité serait d'harmoniser la manière dont ces pays protègent la propriété intellectuelle, tant en ce qui concerne la contrefaçon "classique" (médicaments...) que la contrefaçon numérique (téléchargement illégal).
