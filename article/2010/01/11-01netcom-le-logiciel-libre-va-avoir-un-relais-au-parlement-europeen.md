---
site: 01net.com
title: "Le logiciel libre va avoir un relais au Parlement Européen"
author: Alain Clapaud
date: 2010-01-11
href: http://pro.01net.com/editorial/510913/le-logiciel-libre-aura-un-relais-au-parlement-europeen/
tags:
- Le Logiciel Libre
- Institutions
- Europe
---

> Il faudra attendre l'une des prochaines sessions du Parlement européen, probablement au mois de février, pour savoir quels seront les parlementaires qui formeront l'intergroupe intitulé « Nouveaux médias, logiciel libre et société de l'information ouverte ». [...] Le rôle de celui-ci sera notamment de défendre les intérêts du logiciel libre dans les discussions du Parlement. « Un intergroupe est transversal et interdisciplinaire par nature, précise Alix Cazenave, responsable des affaires publiques de l'April, l'association de promotion et de défense du logiciel libre.
