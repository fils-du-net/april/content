---
site: zdnet.fr
title: "16 000 personnes ont signé la pétition contre le rachat de MySQL par Oracle"
author: Christophe Auffray
date: 2010-01-04
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39711918,00.htm
tags:
- Le Logiciel Libre
- Entreprise
---

> Juridique - Une pétition d’opposants à l’acquisition de MySQL par Oracle rassemblait le 4 janvier un peu plus de 16.000 signatures. La pétition sera présentée à la commission européenne ainsi qu’à d’autres organes de régulation, chinois et russe notamment.
> Lancée fin décembre 2009 à l'initiative de Michael Monty Widenius, la pétition en ligne contre l'acquisition de MySQL par Oracle rassemble déjà plus de 16 000 signatures, les signataires se déclarant favorables à une cession à un tiers, pour un développement du logiciel sous licence GPL.
