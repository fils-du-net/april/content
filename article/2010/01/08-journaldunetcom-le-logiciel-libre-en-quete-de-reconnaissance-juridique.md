---
site: journaldunet.com
title: "Le Logiciel Libre en quête de reconnaissance juridique"
author: Pascal Agosti
date: 2010-01-08
href: http://www.journaldunet.com/developpeur/expert/44330/le-logiciel-libre-en-quete-de-reconnaissance-juridique.shtml
tags:
- Licenses
- Philosophie GNU
---

> Le Logiciel Libre en quête de reconnaissance juridique
> L'arrêt de la Cour d'appel de Paris du 16 septembre 2009 s'intéresse aux effets juridiques d'une licence portant sur un logiciel libre. Elle a condamné une société IT de ne pas avoir fourni à son client les sources d’un logiciel libre et d’avoir supprimé le texte de la licence GNU-GPL.
