---
site: zdnet.fr
title: "Linux, OBM, LinShare : l'Inserm privilégie le libre pour garder la maîtrise"
author: Christophe Auffray
date: 2010-01-26
href: http://www.zdnet.fr/actualites/it-management/0,3800005311,39712373,00.htm
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
- Administration
- Partage du savoir
- Innovation
- Sciences
---

> L'Open Source est entré à l'Institut de la recherche médicale en 2001 par les serveurs, désormais à 95% sous Linux, avant de se propager sur la messagerie avec OBM et l'application de partage de fichiers LinShare. Retour sur ces déploiements avec Patrick Lerouge, son responsable réseaux et sécurité.
