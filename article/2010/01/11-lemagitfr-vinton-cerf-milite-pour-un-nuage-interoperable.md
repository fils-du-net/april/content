---
site: lemagit.fr
title: "Vinton Cerf milite pour un nuage interopérable "
author: Cyrille Chausson
date: 2010-01-11
href: http://www.lemagit.fr/article/google-standards-interoperabilite-cloud-computing-open-source/5269/1/vinton-cerf-milite-pour-nuage-interoperable/
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- Informatique en nuage
---

> Selon l’un des deux créateurs du protocole TCP/IP, Vinton Cerf, également évangéliste chez Google, le Cloud Computing a un réel besoin de standards et de protocoles d’interopérabilité, tant au niveau de la portabilité des données que des infrastructures. [...] “Les entreprises vont avoir besoin de modifier et migrer leur données, et elles vont solliciter le nuage pour réaliser l’opération”, affirme-t-il.
