---
site: progilibre.com
title: "Le salon Solutions Linux/Open Source dévoile le programme de sa 12e édition"
author: La rédaction
date: 2010-01-11
href: http://www.progilibre.com/Le-salon-Solutions-Linux-Open-Source-devoile-le-programme-de-sa-12e-edition_a1014.html
tags:
- Le Logiciel Libre
- Entreprise
- Informatique en nuage
---

> Reconnu comme étant un carrefour d'échanges incontournable pour tout l'écosystème du logiciel libre, le salon Solutions Linux/Open Source réunira cette année 220 exposants et partenaires.
> Les experts présents - juristes, économistes, analystes, intégrateurs, éditeurs, communautés et associations, - délivreront une analyse complète du secteur, orientée autour de thèmes centraux : interopérabilité, progiciels, virtualisation, mobilité et bonnes pratiques.
