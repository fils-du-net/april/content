---
site: lemonde.fr
title: "Pic de téléchargements des navigateurs Firefox et Opéra en France et en Allemagne"
author: La rédaction
date: 2010-01-21
href: http://www.lemonde.fr/technologies/article/2010/01/21/pic-de-telechargements-des-navigateurs-firefox-et-opera-en-france-et-en-allemagne_1295089_651865.html
tags:
- Le Logiciel Libre
- Internet
---

> Les navigateurs Firefox et Opera ont connu un important pic de téléchargement en France et en Allemagne ces derniers jours, juste après que les gouvernements de ces deux pays eurent conseillé à leurs ressortissants de ne plus utiliser Internet Explorer tant qu'une faille de sécurité importante n'aurait pas été corrigée.
