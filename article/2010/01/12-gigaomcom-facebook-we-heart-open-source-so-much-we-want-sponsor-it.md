---
site: gigaom.com
title: "Facebook: We Heart Open Source So Much We Want to Sponsor It"
author: Liz Gannes
date: 2010-01-12
href: http://gigaom.com/2010/01/12/facebook-we-heart-open-source-so-much-we-want-to-sponsor-it/
tags:
- Le Logiciel Libre
- Internet
- English
---

> Facebook devient un sponsor "Gold" de la fondation Apache, soit un don de plus de 40.000 dollars par an.
