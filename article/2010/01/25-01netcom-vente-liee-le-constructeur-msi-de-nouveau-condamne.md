---
site: 01net.com
title: "Vente liée : le constructeur MSI de nouveau condamné "
author: Arnaud Devillard
date: 2010-01-25
href: http://www.01net.com/editorial/511543/vente-liee-le-constructeur-msi-de-nouveau-condamne/
tags:
- Logiciels privateurs
- Vente liée
---

> La justice commence apparemment à comprendre qu'un utilisateur d'ordinateur peut avoir envie de choisir les logiciels qu'il installe sur sa machine et refuser ceux qui y sont déjà. Le 18 janvier dernier, le constructeur MSI Computer a ainsi été condamné pour avoir rechigné à rembourser à un client le prix des logiciels préinstallés. Le fabricant l'avait déjà été en novembre dernier.
