---
site: lemagit.fr
title: "Un groupe de conseil européen déconseille OpenOffice aux gouvernements UE"
author: Cyrille Chausson
date: 2010-01-21
href: http://www.lemagit.fr/article/oracle-openoffice-opensource-commission/5383/1/un-groupe-conseil-europeen-deconseille-openoffice-aux-gouvernements-ue/
tags:
- Le Logiciel Libre
- Entreprise
- Europe
---

> [...] Dans un rapport de 12 pages, la société raconte qu’Oracle devra au moins faire aussi bien que Sun, qui dominait les développements d’OpenOffice, même si le groupe avait depuis un certain temps lâcher du lest dans ses contributions au code. Le rapport explique notamment qu’Oracle risquerait de priver la communauté d’OpenOffice de certains de ses contributeurs phares ainsi que de ressources.
