---
site: neteco.com
title: "Une fondation pour assurer la pérénnité de Wordpress"
author: Guillaume Belfiore
date: 2010-01-22
href: http://www.neteco.com/321418-fondation-wordpress.html
tags:
- Le Logiciel Libre
- Internet
---

> Matt Mullenweg, le fondateur de la plateforme de publication en ligne Wordpress, annonce le lancement de la fondation Wordpress, une entité à but non lucratif. Cette initiative vise à promouvoir « la publication de contenu au travers d'un logiciel open source et disponible sous licence GPL ».
