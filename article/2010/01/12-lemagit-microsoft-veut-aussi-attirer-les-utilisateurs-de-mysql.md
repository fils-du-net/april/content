---
site: lemagit.fr
title: "Microsoft veut aussi attirer les utilisateurs de MySQL "
author: Cyrille Chausson
date: 2010-01-12
href: http://www.lemagit.fr/article/microsoft-mysql-sgbd-sql-sql-server-opensource/5285/1/microsoft-veut-aussi-attirer-les-utilisateurs-mysql/
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Économie
---

> Microsoft a décidé d’élargir son portefeuille d’outils de migration vers SQL Server. Après Sybase et Oracle, Redmond s’attaque désormais... à MySQL, la base de données Open Source dont le sort reste aujourd’hui incertain en raison d'une enquête de la Commission européenne.
