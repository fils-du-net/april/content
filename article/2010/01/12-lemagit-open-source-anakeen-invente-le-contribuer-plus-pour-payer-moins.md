---
site: lemagit.fr
title: "Open Source : Anakeen invente le “contribuer plus pour payer moins”"
author: Cyrille Chausson
date: 2010-01-12
href: http://www.lemagit.fr/article/saas-france-licences-economie-ged-open-source-anakeen-ecm/5279/1/open-source-anakeen-invente-contribuer-plus-pour-payer-moins/
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- Économie
- Partage du savoir
- Innovation
- Licenses
- Philosophie GNU
- Informatique en nuage
---

> Le Français Anakeen, spécialiste de l’ECM Open Source, a décidé d’encadrer son outil Freedom Toolbox d’un modèle économique qui permet aux entreprises d’ajuster leur facture finale en fonction du niveau de leur contribution à la communauté et du type de licence Open Source choisie. Un modèle original qui vient combler une lacune du mécanisme de l’Open Source : la fluctuation des contributions.
