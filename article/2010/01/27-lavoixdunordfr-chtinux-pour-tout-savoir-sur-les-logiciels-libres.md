---
site: lavoixdunord.fr
title: "Chtinux : pour tout savoir sur les logiciels libres"
author: La rédaction
date: 2010-01-27
href: http://www.lavoixdunord.fr/Locales/Lille/actualite/Secteur_Lille/2010/01/27/article_chtinux-pour-tout-savoir-sur-les-logicie.shtml
tags:
- Le Logiciel Libre
---

> C'est quoi, Chtinux ? Un jeu vidéo à la manière de Zelda, qui verrait un Link lillois ...
> battre la campagne des Flandres pour sauver sa virtuelle princesse ? Pas du tout. Chtinux est une association, qui a vu le jour en 2002, et qui a pour cheval de bataille la promotion des logiciels libres dans la métropole lilloise. Pour les membres de ce GULL (pour « groupe d'utilisateurs de logiciels libres »), les logiciels fournis par défaut sur les ordinateurs ne représentent pas forcément la réponse adaptée à leurs envies informatiques.
