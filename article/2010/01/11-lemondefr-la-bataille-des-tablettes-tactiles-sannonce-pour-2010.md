---
site: LeMonde.fr
title: "La bataille des tablettes tactiles s'annonce pour 2010"
author: Le Monde.fr
date: 2010-01-11
href: http://www.lemonde.fr/technologies/article/2010/01/11/la-bataille-des-tablettes-tactiles-s-annonce-pour-2010_1290014_651865.html
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
---

> [...] Quasiment tous les constructeurs ont prévu de sortir en 2010 un ou plusieurs "tablet PC", ces mini-ordinateurs sans clavier à mi-chemin entre le téléphone portable et le netbook. Dotées d'un écran tactile, ces machines multifonctions peuvent servir à la fois d'ordinateur classique, de livre électronique, voire à téléphoner, pour un encombrement minimal. Les analystes financiers prévoient que ces machines seront l'un des produits phares de l'année 2010. [...] De son côté, Google espère conquérir des parts de marché sur ce secteur grâce à son système d'exploitation Androïd, initialement développé pour les téléphones portables à partir du logiciel libre et gratuit Linux. Plusieurs constructeurs ont d'ores et déjà adopté le logiciel de Google, notamment pour des raisons de coût. D'autres fabricants, comme Lenovo, ont fait le choix de développer leur propre système d'exploitation à partir de Linux.
