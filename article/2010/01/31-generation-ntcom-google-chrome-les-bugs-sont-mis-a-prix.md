---
site: "generation-nt.com"
title: "Google Chrome : les bugs sont mis à prix"
author: Jérôme G. 
date: 2010-01-31
href: http://www.generation-nt.com/google-chrome-bugs-securite-recompense-chromium-actualite-952861.html
tags:
- Le Logiciel Libre
- Entreprise
---

> Que ce soit Google Chrome ou son socle open source Chromium, et pour toutes les branches ( stable, bêta, dev ), Google lance un défi à tous les chercheurs en sécurité informatique, et promet une récompense pour les trouvailles les plus intéressantes.
> La récompense sera de 500 dollars et elle pourra atteindre 1 337 dollars ( somme étonnamment précise* ) pour tout bug déniché qui sera jugé particulièrement sévère ou à l'exploitation intelligente. Les extensions et les plugins tiers ( non intégrés dans le projet Chromium ) ne sont pas concernés.
