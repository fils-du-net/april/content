---
site: humanite.fr
title: "Partout dans la société, un autre monde est déjà là  !"
author: Thomas Coutrot
date: 2010-01-28
href: http://www.humanite.fr/article2759730,2759730
tags:
- Le Logiciel Libre
---

> [...] L’exemple le plus spectaculaire est celui du logiciel libre  : sur la base de la coopération gratuite et décentralisée, une communauté mondiale de quelques centaines de milliers d’informaticiens a édifié une architecture dont les performances défient les géants de l’informatique. Grâce à elle des millions d’internautes partagent désormais gratuitement informations et culture sur Internet, rendant nécessaire et à terme inévitable une révolution de l’économie du secteur culturel.
