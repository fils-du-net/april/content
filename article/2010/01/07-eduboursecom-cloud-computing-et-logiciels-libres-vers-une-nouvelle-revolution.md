---
site: edubourse.com
title: "Cloud Computing et logiciels libres, vers une nouvelle révolution ?"
author: Patrick Benichou
date: 2010-01-07
href: http://www.edubourse.com/finance/actualites.php?actu=58693
tags:
- Le Logiciel Libre
- Informatique en nuage
---

> Si l’arrivée du logiciel libre a définitivement marqué l’histoire de l’ingénierie logicielle sur la façon de concevoir, de réaliser ou de diffuser des logiciels, le SAAS et le Cloud computing présagent-t-ils d’une nouvelle révolution sur la façon de les consommer ? Quels rôles les logiciels libres pourront-ils jouer en la matière ?
