---
site: ladepeche.fr
title: "Pont-de-L'arn. Ne pas mettre au rebut son ordinateur"
author: La rédaction
date: 2010-01-12
href: http://www.ladepeche.fr/article/2010/01/12/752700-Pont-de-L-arn-Ne-pas-mettre-au-rebut-son-ordinateur.html
tags:
- Le Logiciel Libre
---

> Jean-Pierre Aubanton et Michel Palouzier animent la Section Informatique Logiciels Libres de la MJC de Saint-Baudille. Voici plus de deux ans que la MJC St Baudille s'est investie dans le développement des logiciels libres en parallèle avec son importante activité informatique. Cette activité nouvelle s'adresse à tous ceux qui possèdent des PC que l'on peut qualifier « d'anciens » car ils ont plus de trois ans.
