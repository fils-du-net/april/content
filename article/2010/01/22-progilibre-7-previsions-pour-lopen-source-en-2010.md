---
site: progilibre.com
title: "7 prévisions pour l'Open Source en 2010 "
author: Roger Burkhardt
date: 2010-01-22
href: http://www.progilibre.com/7-previsions-pour-l-Open-Source-en-2010_a1025.html
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Économie
- Interopérabilité
- Licenses
- Informatique en nuage
---

> [...] Prévision N° 1 : L'open source aura le vent en poupe même dans un contexte économique plus propice
> Le génie est sorti de sa bouteille. La récession de 2003 nous l'a appris : les utilisateurs finaux qui ont goûté à l’open source (plus précisément Linux) ne sont pas revenus automatiquement aux systèmes propriétaires, même s’ils avaient davantage de moyens. C'est même le contraire qui s'est produit. Linux a gagné du terrain dès lors que les clients pouvaient entreprendre des projets plus ambitieux et se sentaient plus à l'aise avec l'open source, sa performance et le support associé. Nous sommes encouragés par la demande croissante que nous avons suscitée en 2009 et pensons que 2010 verra toujours plus d'open source. Malgré l'amélioration de l'économie, nous sommes convaincus que les clients finaux surveilleront de près leur budget IT et continueront à se tourner vers l'open source. Ce faisant, ils diminueront leur dépense informatique globale, à court et à long terme, en évitant toute sujétion vis-à-vis d'un fournisseur.
