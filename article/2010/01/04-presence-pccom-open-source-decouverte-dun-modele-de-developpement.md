---
site: "presence-pc.com"
title: "Open Source : découverte d'un modèle de développement "
author: Marc Hypollite 
date: 2010-01-04
href: http://www.presence-pc.com/tests/open-source-developpement-23244/
tags:
- Le Logiciel Libre
- Informatique en nuage
---

> Qui ne s’est jamais demandé comment Firefox s’était petit à petit imposé face à Internet Explorer ? Qui s’est déjà interrogé sur la gratuité d’OpenOffice alors que cette suite bureautique reste gorgée de fonctions et d’outils toujours plus innovants ? Ces applications, nombre de postes de travail, qu’ils soient sous Windows, Mac OS ou encore Linux, en sont équipés. Leur point commun : elles sont Open Source.
