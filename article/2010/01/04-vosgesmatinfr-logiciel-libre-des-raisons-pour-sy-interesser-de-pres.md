---
site: vosgesmatin.fr
title: "Logiciel libre : des raisons pour s'y intéresser de près"
author: La rédaction
date: 2010-01-04
href: http://www.vosgesmatin.fr/fr/loisirs/multimedia/article/2535722,1211/Logiciel-libre-des-raisons-pour-s-y-interesser-de-pres.html
tags:
- Le Logiciel Libre
---

> Indispensables au quotidien, les logiciels libres sont, malgré leurs avantages, loin d'avoir conquis le grand public. La route est encore longue, mais la voie est libre…
> [...] " Les logiciels libres redonnent à l'utilisateur le contrôle de son PC, explique Eric Beauvière, de l'atelier informatique à Nomexy. Avec un logiciel propriétaire installé, vous ne pouvez jamais être sûr de ce qui se passe réellement car vous n'avez pas accès au code source. Et puis ce sont en général des systèmes plus stables et moins vulnérables que Windows face aux virus."
