---
site: lepays.fr
title: "Une « install party » pendant les portes ouvertes de l’IUT Belfort-Montbéliard"
author: G.M.
date: 2010-01-19
href: http://www.lepays.fr/fr/article/2592332,1214/Une-install-party-pendant-les-portes-ouvertes-de-l-IUT-Belfort-Montbeliard.html
tags:
- Le Logiciel Libre
---

> [...] Les deux étudiants sont des inconditionnels de Linux : « C’est gratuit et c’est sûr, car on a accès au code source, c’est-à-dire à la recette de programmation. » Le chef du département informatique de l’IUT de Belfort-Montbéliard, Raphaël Couturier, va dans le même sens. « Au sein du département informatique, nous ne travaillons que sur Linux, commente-t-il. C’est stable et on en a marre d’acheter des licences avec des logiciels buggés. On peut aussi apprendre à nos étudiants comment programmer, car c’est libre. »
