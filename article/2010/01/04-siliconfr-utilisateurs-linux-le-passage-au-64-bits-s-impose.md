---
site: silicon.fr
title: "Utilisateurs Linux, le passage au 64 bits s’impose !"
author: David Feugey
date: 2010-01-04
href: http://www.silicon.fr/fr/news/2010/01/04/utilisateurs_linux__le_passage_au_64_bits_s_impose__
tags:
- Le Logiciel Libre
- Innovation
---

> Aujourd’hui, le passage à une version 64 bits de Linux est relativement aisé. Les pilotes propriétaires des constructeurs de cartes graphiques sont disponibles en 64 bits, tout comme une version de test du greffon Flash. Les logiciels libres couvrent sans problèmes tous les autres besoins. Nous avons déjà vanté les qualités de la Fedora 64 bits, qui intègre une surcouche efficace permettant – au besoin – d’installer des binaires 32 bits.
