---
site: zdnet.fr
title: "Fondation Mozilla: l'infrastructure pour gérer Firefox"
author: Didier Durand
date: 2010-01-16
href: http://www.zdnet.fr/blogs/media-tech/fondation-mozilla-l-infrastructure-pour-gerer-firefox-39712271.htm
tags:
- Le Logiciel Libre
---

> [...] Datacenter Knowledge publie des chiffres forts intéressants car ils montrent l'infrastructure nécessaire à la distribution d'un navigateur Internet qui a dépassé le milliard de téléchargements il y quelques mois.
> Pour une communauté d'utilisateurs ayant passé de 40m à 350 millions en 3 ans:
> le nombre de serveurs a été multiplié par 11
> l'équipe qui les gère par 3
> la bande passante pour les téléchargements a crû d'un facteur 18
