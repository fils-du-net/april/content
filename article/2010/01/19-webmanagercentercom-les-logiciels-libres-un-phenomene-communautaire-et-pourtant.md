---
site: webmanagercenter.com
title: "Les Logiciels libres, un phénomène communautaire… Et pourtant! "
author: La rédaction
date: 2010-01-19
href: http://www.webmanagercenter.com/management/article-85559-tunisie-informatique-les-logiciels-libres-un-phenomene-communautaire-et-pourtant
tags:
- Le Logiciel Libre
- Administration
---

> Bien que la Tunisie soit relativement avancée en termes d’utilisation des Technologies de l’information et de la communication (TIC) en Afrique et qu’elle dispose même d’un secrétariat d’Etat en charge de l’Informatique, de l’Internet et des Logiciels libres, certains estiment que peu d’actions concrètes ou de projets logiciels libres ont été menés dans notre pays, ce qui présente, selon eux, une contradiction entre, d’une part, une politique et une stratégie, et des faits sur le terrain, d’autre part.
