---
site: zdnet.fr
title: "Google va migrer son stockage de Ext2 vers Ext4"
author: La rédaction
date: 2010-01-18
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39712288,00.htm
tags:
- Le Logiciel Libre
---

> Google travaille actuellement à la migration de son infrastructure de stockage du système de fichier Ext2 vers la dernière version, Ext4.
> [...] D'après Michael Rubin, un ingénieur de Google, l'abandon d'Ext2 au profit de la dernière version du système de fichier génère des gains en termes de performances. En outre, d'après des tests réalisés par Google, Ext4 rivaliserait avec XFS, un autre système de fichiers développé sous licence GPL.
