---
site: ecrans.fr
title: "Haïti : Mobilisation autour d’une carte libre"
author: Camille Gévaudan 
date: 2010-01-15
href: http://www.ecrans.fr/Haiti-Mobilisation-autour-d-une,8961.html
tags:
- Internet
- Partage du savoir
---

> Quelques heures seulement après le violent séisme qui a secoué Haïti le 12 janvier, la communauté OpenStreetMap lançait un appel aux contributions sur la carte de Port-au-Prince. La réponse des cartographes a été rapide et massive : en deux jours, plus de 800 modifications ont déjà été effectuées et le plan de la ville est passé du stade d’ébauche à un époustouflant niveau de précision (bien supérieur à celui de Google Maps).
