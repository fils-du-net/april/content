---
site: numerama.com
title: "Droits d'auteur : L'Europe fait pression sur le Canada pour qu'il ignore les Canadiens"
author: Guillaume Champeau
date: 2010-01-19
href: http://www.numerama.com/magazine/14878-droits-d-auteur-l-europe-fait-pression-sur-le-canada-pour-qu-il-ignore-les-canadiens.html
tags:
- DADVSI
- Droit d'auteur
- International
- ACTA
---

> [...] Parmi les griefs faits au Canada, Bruxelles souhaiterait voir Ottawa adopter une extension de la durée de protection des droits d'auteur à 70 ans après la mort de l'auteur, une protection des DRM, l'instauration d'un droit de suite, et des dispositions de responsabilité pénale pour les FAI et les hébergeurs. Autant de mesures que l'Union Européenne et les Etats-Unis entendent bien imposer au monde entier à travers l'ACTA.
