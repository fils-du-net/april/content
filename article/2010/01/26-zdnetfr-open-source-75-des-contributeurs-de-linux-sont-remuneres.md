---
site: zdnet.fr
title: "Open Source : 75% des contributeurs de Linux sont rémunérés"
author: La rédaction
date: 2010-01-26
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39712463,00.htm
tags:
- Le Logiciel Libre
---

> Technologie - Entre le 24 décembre 2008 et le 10 janvier 2010, 2,8 millions de lignes de code ont été ajoutées au noyau Linux. Ce travail est en majeure partie délivré par des développeurs employés par des éditeurs ; les contributeurs bénévoles collaborant à hauteur de 18%.
