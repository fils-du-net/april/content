---
site: 01net.com
title: "L’open source à l'assaut des applications Internet riches"
author: Jonathan Charton
date: 2010-01-21
href: http://pro.01net.com/editorial/511457/l-open-source-a-lassaut-des-applications-internet-riches/
tags:
- Le Logiciel Libre
- Internet
---

> L'exécution des applications Internet riches (RIA) depuis un navigateur Web ne sera peut-être plus l'exclusivité des technologies propriétaires. Depuis peu, Mozilla et Apache, deux acteurs leaders des logiciels libres, proposent chacun une alternative aux moteurs Flex d'Adobe et Silverlight de Microsoft. L'enjeu est de fournir la plate-forme sur laquelle reposeront les logiciels en ligne de demain.
