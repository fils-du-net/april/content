---
site: pcinpact.com
title: "Elle est née, la divine Hadopi"
author: Marc Rees
date: 2010-01-04
href: http://www.pcinpact.com/actu/news/54781-hadopi-conseil-etat-decret-organisation2.htm
tags:
- Logiciels privateurs
- HADOPI
---

> [...] En clair, c’est ce collège qui devra déterminer le portrait-robot du logiciel de sécurisation dont l’installation protègera l’abonné des foudres de la déconnexion. Une labellisation trop exigeante, et la preuve deviendra presque impossible pour Mme Michu. Des critères trop souples, et voilà des internautes qui pourront télécharger sans compter, après avoir acheté et installé un logiciel mouchard poreux.
