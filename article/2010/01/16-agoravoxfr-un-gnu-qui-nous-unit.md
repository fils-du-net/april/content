---
site: agoravox.fr
title: "Un GNU qui nous Unit !"
author: Biaise
date: 2010-01-16
href: http://www.agoravox.fr/actualites/technologies/article/un-gnu-qui-nous-unit-68241
tags:
- Le Logiciel Libre
- Philosophie GNU
---

> Ce mercredi 13 janvier avait lieu une conférence enthousiasmante pour tous les geeks de la cité des Gaules : Richard Stallman lui-même au Grand Amphithéâtre de l’université Lumière Lyon 2 ! RMS, comme il se fait appeler, fondateur de GNU, venait nous parler du logiciel libre. Qui de mieux qualifié ?
> Son arrivée dans l’amphithéâtre fut marquée par un mouvement de masse vers la position debout et une salve d’applaudissements. Le président de l’université était bien dépité. Il n’avait pas conscience qu’une telle « star » était face à lui. Il introduisit la conférence d’un bref discours sur les utopies et les utopistes. C’est approprié quand on reçoit le papa de GNU.
