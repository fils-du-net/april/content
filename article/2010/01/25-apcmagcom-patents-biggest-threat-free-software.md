---
site: apcmag.com
title: "Patents biggest threat to free software"
author: Angus Kidman
date: 2010-01-25
href: http://apcmag.com/patents-biggest-threat-to-free-software.htm
tags:
- Le Logiciel Libre
- Brevets logiciels
- English
---

> Le devellopeur Samba, Andrew Tridgell, note que la communauté open source ne sait pas bien combattre les attaques via les brevets mais pense qu'un changement de stratégie pourrait améliorer la situation.
