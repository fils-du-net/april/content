---
site: ruefrontenac.com
title: "Contrat informatique à la RRQ — Cyrille Béraud contre les Goliath de ce monde"
author: Valérie Dufour
date: 2010-01-21
href: http://www.ruefrontenac.com/nouvelles-generales/justice/16706-linux-rrq
tags:
- Entreprise
- Administration
- Économie
---

> Cyrille Béraud sait qu’il s’attaque à gros, mais la question de principe est trop importante pour qu’il se laisse intimider par la chose, car les conséquences d’un jugement favorable seraient énormes pour toute l’industrie du logiciel libre. Sa cause : dénoncer la décision de la Régie des rentes du Québec qui a accordé sans appel d’offres un contrat de 722 848 $ à Microsoft Canada.
