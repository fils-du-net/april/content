---
site: pcinpact.com
title: "Microsoft Word n'est provisoirement plus en vente aux États-Unis"
author: Vincent Hermann
date: 2010-01-12
href: http://www.pcinpact.com/actu/news/54918-microsoft-office-i4i-retrait-vente.htm
tags:
- Logiciels privateurs
- Brevets logiciels
- Informatique-deloyale
---

> On le sait depuis peu, Microsoft doit modifier son logiciel Word. À la suite d’une plainte déposée par la société i4i, la firme de Redmond a dû modifier l’ambassadeur de la suite Office pour lui retirer les fonctionnalités exploitant le XML personnalisé, puisque i4i en détient le brevet/ malgré les assurances de Microsoft, cela provoque tout de même quelques retombées. Premièrement, la grande majorité des éditions d’Office 2007 ne sont plus accessibles à la vente sur le Microsoft Store américain.
