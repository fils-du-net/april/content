---
site: cnet.com
title: "Microsoft Web-graphics move signals IE ambitions"
author: Stephen Shankland
date: 2010-01-06
href: http://news.cnet.com/8301-30685_3-10426321-264.html
tags:
- Logiciels privateurs
- Interopérabilité
- Standards
- English
---

> Après des années passées à  mépriser les standards, MS annonce qu'il rejoindra le groupe de travaille du W3c sur le SVG. A Noter que Mozilla Firefox, Google Chrome, Safari et Opera savent déjà  gérer le SVG.
