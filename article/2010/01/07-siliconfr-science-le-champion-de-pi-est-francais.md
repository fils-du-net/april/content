---
site: silicon.fr
title: "Science : le champion de pi est français"
author: David Feugey
date: 2010-01-07
href: http://www.silicon.fr/fr/news/2010/01/07/science___le_champion_de_pi_est_francais
tags:
- Le Logiciel Libre
- Innovation
- Sciences
---

> Le développeur Fabrice Bellard vient de calculer près de 2700 milliards de décimales de pi. Un record insolite, mais d’une importance stratégique pour les scientifiques.
> [...] Le précédent record, qui datait de 1995, avait été décroché par un supercalculateur coutant plusieurs millions de dollars. Fabrice Bellard a pour sa part utilisé un ordinateur classique pour l’essentiel des calculs. Un record dans le record, ce PC coutant moins de 2000 euros ! Il est équipé d’un processeur Intel Core i7 cadencé 2,93 GHz, de 6 Go de mémoire vive et de 7,5 To d’espace disque. Le tout fonctionne avec la mouture 64 bits de la distribution Linux Fedora 10.
