---
site: silicon.fr
title: "Red Hat lance le site communautaire opensource.com"
author: David Feugey 
date: 2010-01-26
href: http://www.silicon.fr/fr/news/2010/01/26/red_hat_lance_le_site_communautaire_opensource_com
tags:
- Le Logiciel Libre
- Entreprise
- Innovation
---

> Les principes qui régissent l’open source peuvent avoir des implications hors du monde du logiciel. Red Hat y croit dur comme fer, et lance un site communautaire dédié à cette problématique.[...]
