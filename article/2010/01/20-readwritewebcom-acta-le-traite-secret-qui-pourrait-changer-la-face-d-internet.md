---
site: readwriteweb.com
title: "ACTA : le traité secret qui pourrait changer la face d’internet"
author: Fabrice Epelboin
date: 2010-01-20
href: http://fr.readwriteweb.com/2010/01/20/a-la-une/traite-acta-censure-loppsi-hadopi/
tags:
- Internet
- Économie
- ACTA
---

> Si le traité ACTA venait à être appliqué, l’internet tel qu’on le connait aujourd’hui serait radicalement changé. Le traité renforce de façon démesurée le pouvoir du copyright (ou du droit d’auteur, en l’occurrence la différence est insignifiante), forcera les fournisseurs de services sur internet à faire la police du copyright, rendant du même coup l’hébergement de contenus générés par les utilisateurs impossible à assumer financièrement,
