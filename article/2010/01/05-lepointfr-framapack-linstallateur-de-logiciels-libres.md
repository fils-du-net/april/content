---
site: LePoint.fr
title: "Framapack, l'installateur de logiciels libres"
author: Guerric Poncet
date: 2010-01-05
href: http://www.lepoint.fr/actualites-technologie-internet/2010-01-05/pratique-framapack-l-installateur-de-logiciels-libres/1387/0/410680
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
- Économie
- Partage du savoir
- Associations
- Promotion
---

> Framapack, c'est un peu le rêve de tout utilisateur d'ordinateur. En quelques instants, l'outil lancé le 1er janvier peut installer dans Windows des dizaines de logiciels gratuits et de qualité. L'utilisateur peut simplement se rendre sur le site de Framapack et choisir les logiciels qu'il désire installer, parmi un catalogue d'une cinquantaine d'applications (et bientôt plus). Quelques instants plus tard, un petit utilitaire est téléchargé sur le PC et va automatiquement installer les dernières versions des logiciels sélectionnés, avec les paramètres par défaut. Aucune action n'est requise de la part de l'utilisateur.
> [...] Le mécanisme fait immanquablement penser à ce qui existe depuis longtemps pour certaines distributions de Linux. Sous Ubuntu par exemple, un gestionnaire de logiciels permet d'installer des milliers d'applications en quelques clics. Microsoft, le géant du logiciel qui incarne la vision commerciale du secteur, n'apprécie probablement pas l'initiative. Non seulement les logiciels libres concurrencent ses propres applications (Microsoft Office et OpenOffice jouent par exemple sur le même terrain), mais, en plus, Framapack risque d'inculquer des habitudes de gratuité chez ses clients.
