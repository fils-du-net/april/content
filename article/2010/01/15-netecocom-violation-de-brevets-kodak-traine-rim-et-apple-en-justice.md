---
site: neteco.com
title: "Violation de brevets : Kodak traine RIM et Apple en justice"
author: Guillaume Belfiore
date: 2010-01-15
href: http://www.neteco.com/320212-violation-brevets-kodak-traine-rim-apple-justice.html
tags:
- Brevets logiciels
---

> [...] La société spécialisée dans le domaine de la photographie et du cinéma estime en effet, que les iPhone et BlackBerry embarqueraient des technologies déposées pour la prévisualisation des images et la manière dont un programme fait appel à un autre logiciel pour opérer le calcul de certaines fonctions. La firme a donc déposé plainte auprès de la court du district ouest de l'état de New York.
