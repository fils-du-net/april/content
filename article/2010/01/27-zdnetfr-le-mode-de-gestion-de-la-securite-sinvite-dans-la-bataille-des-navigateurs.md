---
site: zdnet.fr
title: "Le mode de gestion de la sécurité s'invite dans la bataille des navigateurs"
author: Christophe Sauthier
date: 2010-01-27
href: http://www.zdnet.fr/blogs/ubuntu-co/le-mode-de-gestion-de-la-securite-s-invite-dans-la-bataille-des-navigateurs-39712488.htm
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
---

> [...] Mais un évènement récent propulse au grand jour une des forces des logiciels libres : la réactivité par rapport aux problèmes de sécurité. Je parle bien évidemment de l'attaque subie par Google et 30 autres grandes sociétés il y a quelques jours.
