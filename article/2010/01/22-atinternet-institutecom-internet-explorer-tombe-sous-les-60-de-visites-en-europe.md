---
site: "atinternet-institute.com"
title: "Internet Explorer tombe sous les 60% de visites en Europe"
author: La rédaction
date: 2010-01-22
href: http://www.atinternet-institute.com/fr-fr/barometre-des-navigateurs/barometre-des-navigateurs-decembre-2009/index-1-1-3-186.html
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
---

> La perte de terrain du leader Internet Explorer observée dans nos dernières études ne fait que s’accentuer en Europe fin 2009, et l'annonce récente de sa faille de sécurité ne devrait pas modifier la tendance dans les semaines à venir… Ce qui n’est pas pour déplaire à ses concurrents qui renforcent leurs positions, particulièrement Google Chrome.
