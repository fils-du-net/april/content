---
site: lemagit.fr
title: "Face à SharePoint, l'ECM Open Source Alfresco et les outils collaboratifs de Lotus jouent la carte de l'intégration "
author: La rédaction
date: 2010-01-14
href: http://www.lemagit.fr/article/ibm-ged-collaboratif-sharepoint-alfresco-lotus-gestion-contenus-exchange/5310/1/face-sharepoint-ecm-open-source-alfresco-les-outils-collaboratifs-lotus-jouent-carte-integration/
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- Interopérabilité
---

> A quelques jours de l'ouverture de Lotusphere, la conférence utilisateurs de Lotus, l'éditeur Open Source Alfresco a annoncé travailler avec IBM à l'intégration de sa plate-forme de gestion de contenus avec les applications collaboratives de Big Blue.
