---
site: slate.fr
title: "Le gros coup de flip d'un vieux geek"
author: Michael Aggertraduit par Peggy Sastre
date: 2010-01-08
href: http://www.slate.fr/story/15327/wired-lanier-critique-internet-avenir-web
tags:
- Le Logiciel Libre
- Internet
---

> [...] Comme dans la standardisation et corporatisation générales du Web, Lanier voit dans la «culture ouverte» du Web un échec. Au lieu de créer de nouvelles chansons ou vidéos, nous ne faisons que voler les décennies pop précédentes et créons des parodies et des collages. Au lieu d'écrire de brillants nouveaux programmes informatiques, les ados informaticiens d'aujourd'hui ne cherchent qu'à améliorer Linux, gratuit et open-source, qui n'offre pas de réelle innovation par rapport à plusieurs décennies d'Unix.
