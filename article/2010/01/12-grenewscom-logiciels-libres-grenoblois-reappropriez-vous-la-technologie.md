---
site: grenews.com
title: "Logiciels libres : Grenoblois, réappropriez-vous la technologie !"
author: Nicolas Robert
date: 2010-01-12
href: http://www.grenews.com/logiciels-libres-grenoblois-reappropriez-vous-la-technologie--@/article.jspz?article=17660
tags:
- Le Logiciel Libre
- Éducation
---

> [...] Résumons : des logiciels pour tous, accessibles gratuitement... et pourtant on n’a pas forcément l’impression que le nombre d’utilisateurs augmente vraiment. “Vous raisonnez en parts de marché. Ici, on est dans une logique de partage, portée par une communauté d’utilisateurs : les clients sont les premiers testeurs et un nouveau modèle économique se met en place. On est dans une économie de service, où le logiciel et le service d’accompagnement sont liés. On va dire que c’est de la croissance raisonnée”.
