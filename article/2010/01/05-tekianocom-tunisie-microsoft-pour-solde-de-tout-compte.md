---
site: tekiano.com
title: "Tunisie : Microsoft, pour solde de tout compte"
author: Samy Ben Naceur
date: 2010-01-05
href: http://www.tekiano.com/informatique/logiciels/3-10-1579/tunisie-microsoft-pour-solde-de-tout-compte.html
tags:
- Logiciels privateurs
- Administration
- International
---

> [...] Mohamed Ikbel Boulabiar, lui, est carrément sarcastique, et relève : « on a beaucoup d'argent, donc c'est pour ça qu'on paye Microsoft puisque c'est une petite société et qu'on doit l'encourager à se développer... ». Plus sérieusement, Mohamed Ikbel estime ainsi que : «6 milliards permettent de payer au moins 500 ingénieurs durant une année complète.
