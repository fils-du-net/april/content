---
site: europa.eu
title: "Concentrations: la Commission autorise le projet de rachat de Sun Microsystems par Oracle "
author: La rédaction
date: 2010-01-21
href: http://europa.eu/rapid/pressReleasesAction.do?reference=IP/10/40&format=HTML&aged=0&language=FR&guiLanguage=en
tags:
- Entreprise
- Économie
- Europe
---

> La Commission européenne a autorisé, en vertu du règlement sur les concentrations de l’UE, le projet de rachat du vendeur de matériel informatique et éditeur de logiciels américain Sun Microsystems Inc. par Oracle Corporation, société américaine de logiciels. Au terme d'un examen approfondi lancé en septembre 2009 (voir   IP/09/1271   ), la Commission est parvenue à la conclusion que l'opération n'entraverait pas de manière significative le jeu d'une concurrence effective dans l’Espace économique européen (EEE) ou une partie substantielle de celui-ci.
