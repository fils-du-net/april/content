---
site: law.com
title: "PwC Study Shows Patent Trolls Are Thriving"
author: Susan Beck
date: 2010-01-31
href: http://www.law.com/jsp/tal/digestTAL.jsp?id=1202441718346
tags:
- Brevets logiciels
- Informatique-deloyale
---

> Une étude montre que les spéculateurs de brevets (qui ne déposent des brevets que dans le but de faire de la rente ou d'aller au procès) sont une part de plus en plus importante du système de brevet (pas uniquement logiciel).
