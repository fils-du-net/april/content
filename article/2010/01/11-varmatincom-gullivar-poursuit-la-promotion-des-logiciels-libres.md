---
site: varmatin.com
title: "Gullivar poursuit la promotion des logiciels libres"
author: G. L
date: 2010-01-11
href: http://www.varmatin.com/ra/societe/234007/brignoles-gullivar-poursuit-la-promotion-des-logiciels-libres
tags:
- Le Logiciel Libre
---

> Les adhérents de Gullivar étaient rassemblés pour la deuxième assemblée générale de l'association dans la salle de Saints-Anges.
> Pour beaucoup, l'informatique est une jungle obscure où il est périlleux de s'aventurer. Pour d'autres, c'est un monde attrayant qui suscite une passion dévorante.
