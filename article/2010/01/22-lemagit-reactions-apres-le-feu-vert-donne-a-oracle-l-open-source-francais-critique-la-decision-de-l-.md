---
site: lemagit.fr
title: "Réactions : après le feu vert donné à Oracle, l’Open Source français critique la décision de l’UE"
author: Cyrille Chausson
date: 2010-01-22
href: http://www.lemagit.fr/article/mysql-oracle-rachat-sgbd-opensource-widenius-bruxelles/5391/1/reactions-apres-feu-vert-donne-oracle-open-source-francais-critique-decision-ue/
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- Europe
---

> [...] Malgré tout, reconnaissent nos interlocuteurs, la décision de la Commission a au moins le mérite de dégeler la situation. “Il fallait en prendre une”, constate Cyril Pierre de Geyer, qui rappelle combien le marché autour de MySQL souffrait de l’incertitude planant sur le rachat du Sun (notamment le marché de la formation à MySQL, où les demandes ont radicalement chuté depuis 6 mois). Reste encore à savoir ce que fera réellement Oracle et surtout les résultats que l'intégration produira dans 1 ou 2 ans, souligne-t-il.
