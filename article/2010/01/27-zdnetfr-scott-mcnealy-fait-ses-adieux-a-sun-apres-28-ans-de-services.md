---
site: zdnet.fr
title: "Scott McNealy fait ses adieux à Sun après 28 ans de services"
author: La rédaction
date: 2010-01-27
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39712541,00.htm
tags:
- Le Logiciel Libre
- Administration
---

> [...] Le co-fondateur n'indique pas dans son message s'il compte créer une nouvelle entreprise ou rejoindre un autre groupe. En juin 2009, il s'est vu confier la réalisation d'un rapport sur l'Open Source par le gouvernement Obama.
