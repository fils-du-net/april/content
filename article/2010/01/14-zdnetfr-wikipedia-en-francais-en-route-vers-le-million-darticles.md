---
site: zdnet.fr
title: "Wikipédia en français en route vers le million d'articles"
author: Thierry Noisette
date: 2010-01-14
href: http://www.zdnet.fr/blogs/l-esprit-libre/wikipedia-en-francais-en-route-vers-le-million-d-articles-39712224.htm
tags:
- Internet
- Partage du savoir
---

> Un jour avant le «Wikipedia Day» qui fête la création le 15 janvier 2001 de l'encyclopédie libre, sa version en français a franchi il y a quelques minutes le cap des 900.000 articles. Au même rythme (elle a grandi de 120.000 nouveaux articles en moins de dix mois), le million devrait être atteint avant la fin de l'année.
