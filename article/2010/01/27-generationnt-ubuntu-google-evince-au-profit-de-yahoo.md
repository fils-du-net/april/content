---
site: "generation-nt.com"
title: "Ubuntu : Google évincé au profit de Yahoo!"
author: Jérôme G.
date: 2010-01-27
href: http://www.generation-nt.com/ubuntu-firefox-yahoo-google-actualite-950601.html
tags:
- Le Logiciel Libre
- Entreprise
- Internet
---

> Pour la distribution Linux Ubuntu, le moteur de recherche par défaut dans Firefox sera Yahoo! qui prendra la place de Google. L'utilisateur gardera néanmoins la maîtrise de la configuration.
