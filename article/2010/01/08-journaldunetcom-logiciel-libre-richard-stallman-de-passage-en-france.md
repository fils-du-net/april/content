---
site: journaldunet.com
title: "Logiciel libre : Richard Stallman de passage en France"
author: La rédaction
date: 2010-01-08
href: http://www.journaldunet.com/solutions/breve/44343/logiciel-libre---richard-stallman-de-passage-en-france.shtml
tags:
- Le Logiciel Libre
- Philosophie GNU
---

> Le 12 janvier prochain, le fondateur de la licence GNU GPL sera sur Paris à l'occasion de la sortie de sa biographie officielle ("Richard Stallman et la révolution du logiciel libre"). Il donnera à cette occasion une conférence lors de laquelle il commentera également l'actualité des logiciels libres dans le monde.
