---
site: francemobiles.com
title: "Selon Microsoft, l'offre sur le marché des OS mobiles est trop étendue  "
author: La rédaction
date: 2010-01-20
href: http://www.francemobiles.com/actualites/id/201001201263892373/selon_microsoft_l_offre_sur_le_marche_des_os_mobiles_est_trop_etendue.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> [...] Le président de la division divertissement et produits mobiles de Microsoft reproche surtout aux fabricants de « feature phones », les terminaux intermédiaires entre les téléphones de base et les smartphones, de proposer plus de 15 systèmes d'exploitation, ayant recours au noyau Linux.
