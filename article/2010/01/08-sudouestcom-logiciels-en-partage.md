---
site: sudouest.com
title: "Logiciels en partage"
author: Thomas Saintourens
date: 2010-01-08
href: http://www.sudouest.com/gironde/actualite/rive-gauche/article/828900/mil/5565436.html
tags:
- Le Logiciel Libre
---

> Au départ affaire d'un cercle restreint d'initiés, l'usage des logiciels libres est en plein essor. Que ce soit dans les entreprises, les administrations (telle que la mairie de Pessac, récemment convertie) ou chez les particuliers, cette alternative aux logiciels « à licence », s'impose en douceur dans les ordinateurs.
> Deux samedis par mois, à la médiathèque Jacques-Ellul, les rendez-vous « à libre ouvert » mettent l'accent sur les utilisations concrètes des logiciels libres.
