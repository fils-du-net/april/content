---
site: journaldunet.com
title: "Bonnes feuilles : la biographie de Richard Stallman"
author: Fabrice Deblock
date: 2010-01-13
href: http://www.journaldunet.com/solutions/acteurs/biographie-de-richard-stallman-les-bonnes-feuilles/
tags:
- Le Logiciel Libre
- Philosophie GNU
---

> A l'origine du système d'exploitation libre GNU, de la licence GPL et de la Free Software Foundation, Richard Stallman était à Paris pour présenter sa biographie autorisée.
