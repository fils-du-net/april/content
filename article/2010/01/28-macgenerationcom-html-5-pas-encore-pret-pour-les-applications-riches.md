---
site: macgeneration.com
title: "HTML 5, pas encore prêt pour les applications \"riches\""
author: Arnauld de La Grandière
date: 2010-01-28
href: http://www.macgeneration.com/news/voir/141541/html-5-pas-encore-pret-pour-les-applications-riches
tags:
- Internet
- Logiciels privateurs
- Interopérabilité
---

> [...] Les conclusions qu'en tirent les développeurs sont pour le moins amères : étant donné la domination d'Internet Explorer, en l'état il est difficilement envisageable d'utiliser HTML 5 pour les web apps "riches". Même sur les autres navigateurs, HTML 5 donne de bien piètres résultats comparés à une solution native, puisqu'au mieux ils obtiennent près de 8 images par seconde (contre une centaine en natif de manière générale).
