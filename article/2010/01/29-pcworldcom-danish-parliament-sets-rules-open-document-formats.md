---
site: pcworld.com
title: "Danish Parliament Sets Rules for Open Document Formats"
author: Mikael Ricknäs
date: 2010-01-29
href: http://www.pcworld.com/businesscenter/article/188153/danish_parliament_sets_rules_for_open_document_formats.html
tags:
- Administration
- Interopérabilité
- English
---

> Le Danemark choisit d'utiliser le format OpenDocument pour les échanges de documents au sein et entre les ministère et le gouvernement. Les collectivités locales devraient suivre le mouvent.
