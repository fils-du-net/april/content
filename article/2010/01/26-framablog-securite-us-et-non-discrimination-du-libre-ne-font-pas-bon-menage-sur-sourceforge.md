---
site: framablog.org
title: "Sécurité US et non discrimination du Libre ne font pas bon ménage sur SourceForge"
author: Dan Goodin (traduit par Olivier de Framalang)
date: 2010-01-26
href: http://www.framablog.org/index.php/post/2010/01/26/sourceforge-censure
tags:
- Le Logiciel Libre
- Internet
- Partage du savoir
- Accessibilité
- Neutralité du Net
- Philosophie GNU
- International
---

> La forge logicielle SourceForge n’est plus à présenter. C’est le plus grand dépôt d’applications libres au monde, qui se comptent en centaine de milliers.
