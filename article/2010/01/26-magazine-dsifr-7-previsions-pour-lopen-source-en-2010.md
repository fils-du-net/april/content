---
site: "magazine-dsi.fr"
title: "7 prévisions pour l'Open Source en 2010"
author: Roger Burkhardt
date: 2010-01-26
href: http://www.magazine-dsi.fr/experts-it/7-pr%C3%A9visions-pour-lopen-source-en-2010
tags:
- Le Logiciel Libre
- Entreprise
- Informatique en nuage
---

> [...] Le génie est sorti de sa bouteille. La récession de 2003 nous l'a appris : les utilisateurs finaux qui ont goûté à l’open source (plus précisément Linux) ne sont pas revenus automatiquement aux systèmes propriétaires, même s’ils avaient davantage de moyens. C'est même le contraire qui s'est produit. Linux a gagné du terrain dès lors que les clients pouvaient entreprendre des projets plus ambitieux et se sentaient plus à l'aise avec l'open source, sa performance et le support associé.
