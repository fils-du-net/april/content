---
site: sudouest.com
title: "Des pirates en politique"
author: Frédéric Sallet
date: 2010-01-17
href: http://www.sudouest.com/accueil/actualite/france/article/837217/mil/5593938.html
tags:
- Institutions
---

> Déjà représenté depuis juin 2009 au Parlement européen par deux Suédois, le Parti pirate cherche désormais à s'implanter en France
> Martin a 18 ans. Étudiant en classe prépa à Bordeaux, il est membre de l'April, une association de défense des logiciels libres. Quand il surfe sur le Web, lemonde.fr est sa page de démarrage, mais il consulte très régulièrement le Framablog, sebsauvage.net et de nombreux blogs informatiques indépendants. Sur Linux, il est intarissable : ses parents, son entourage, ses camarades de lycée ont été évangélisés par ses soins à l'informatique libre, sans copyright.
