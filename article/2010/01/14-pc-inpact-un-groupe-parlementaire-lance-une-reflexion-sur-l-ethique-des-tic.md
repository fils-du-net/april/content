---
site: pcinpact.com
title: "Un groupe parlementaire lance une réflexion sur l’éthique des TIC"
author: Marc Rees
date: 2010-01-14
href: http://www.pcinpact.com/actu/news/54960-lionel-tardy-ethique-numerique-groupe.htm
tags:
- Internet
- HADOPI
- Institutions
- Droit d'auteur
- Neutralité du Net
---

> L’UMP s’est doté en novembre dernier d’un groupe de travail interne nommé « Éthique et numérique », avec pour but de réfléchir à ces questions-là. L’idée est aussi d’éviter la démarche entreprise par HADOPI : le groupe veut démultiplier les auditions, les consultations autour de ces questions, avant de réagir. Thème de prédilection : le profilage, le traçage le droit à l’oubli, mais aussi et surtout les nanotechnologies, grand thème à venir.
