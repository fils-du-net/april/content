---
site: 01net.com
title: "Richard Stallman : « l’avenir du libre dépend de vous tous »"
author: Rémi Langlet
date: 2010-01-29
href: http://www.01net.com/editorial/511892/richard-stallman-l-avenir-du-logiciel-libre-depend-de-vous-tous/
tags:
- Le Logiciel Libre
- HADOPI
- Philosophie GNU
---

> [...] Notre force est d'avoir développé des systèmes d'exploitation et des applications libres pour presque toutes les activités informatiques ordinaires. Notre faiblesse est que la plupart de leurs utilisateurs n'ont pas conscience de l'idée même de la liberté pour laquelle nous nous battons.
