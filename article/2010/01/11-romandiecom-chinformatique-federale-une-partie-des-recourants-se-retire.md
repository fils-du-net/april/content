---
site: romandie.com
title: "CH/Informatique fédérale: une partie des recourants se retire"
author: La rédaction
date: 2010-01-11
href: http://www.romandie.com/infos/news2/201001110625041AWPCH.asp
tags:
- Administration
---

> Berne (awp/ats) - Dans le litige entre la Confédération et des fournisseurs de logiciels "open source", une partie des recourants - sept sur dix-huit - a décidé de se retirer. Il s'agit de ne pas retarder la procédure devant le Tribunal administratif fédéral (TAF), a indiqué dimanche le Swiss Open Systems User Group.
