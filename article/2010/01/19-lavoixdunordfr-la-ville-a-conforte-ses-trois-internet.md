---
site: lavoixdunord.fr
title: "La ville a conforté ses trois @ Internet"
author: La rédaction
date: 2010-01-19
href: http://www.lavoixdunord.fr/Locales/Valenciennes/actualite/Autour_de_Valenciennes/Agglomeration_de_Valenciennes/2010/01/19/article_la-ville-a-conforte-ses-trois-internet.shtml
tags:
- Le Logiciel Libre
- Administration
---

> [...] Sur l'ensemble du territoire, 224 villes ont ainsi été labellisées. Pour cette édition 2010, les critères de sélection avaient été sérieusement renforcés. La prise en compte des dialogues entre la population et ses élus, la promotion des logiciels libres de droit et l'importance de l'Internet pour les publics dit fragiles sont autant de critères pour lesquels le jury a été très attentif. Seules trois villes de la région Nord - Pas-de-Calais ont reçu les trois @ : Lambersart, Billy-Berclau et Lille.
