---
site: lemonde.fr
title: "La France et l'Allemagne déconseillent l'utilisation d'Internet Explorer"
author: La rédaction
date: 2010-01-17
href: http://www.lemonde.fr/technologies/article/2010/01/17/la-france-et-l-allemagne-deconseillent-l-utilisation-d-internet-explorer_1292928_651865.html
tags:
- Logiciels privateurs
- Europe
---

> Deux organismes allemands et français ont émis une mise en garde en fin de semaine contre l'utilisation d'Internet Explorer, demandant à Microsoft de régler les défauts de sécurité de son navigateur. Jeudi 14 janvier, le géant américain du logiciel avait annoncé qu'une faille de sécurité de son navigateur avait été exploitée pour mener les cyber-attaques qui ont poussé Google à menacer de cesser ses activités en Chine (Voir l'enquête du Monde "L'avenir incertain de Google en Chine").
