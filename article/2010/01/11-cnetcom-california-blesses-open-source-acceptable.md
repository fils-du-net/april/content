---
site: cnet.com
title: "California blesses open source as 'acceptable'"
author: Matt Asay
date: 2010-01-11
href: http://news.cnet.com/8301-13505_3-10432052-16.html
tags:
- Le Logiciel Libre
- Administration
- International
- English
---

> En Californie le logiciel libre devient « acceptable», suite aux difficultés budgétaires.
