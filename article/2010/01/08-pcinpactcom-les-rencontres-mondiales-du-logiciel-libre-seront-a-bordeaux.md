---
site: pcinpact.com
title: "Les Rencontres Mondiales du Logiciel Libre seront à Bordeaux"
author: Vincent Hermann
date: 2010-01-08
href: http://www.pcinpact.com/actu/news/54868-rmll-2010-bordeaux-open-source.htm
tags:
- Le Logiciel Libre
---

> [...] Après Mont-de-Marsan en 2008 et Nantes l’année dernière, l’édition 2010 des Rencontres Mondiales du Logiciel Libre (RMLL) se déroulera à Bordeaux du 6 au 11 juillet. Cela donne bien sûr le temps de se préparer, et nous serons sur place pour dresser un portrait de l’évènement.
