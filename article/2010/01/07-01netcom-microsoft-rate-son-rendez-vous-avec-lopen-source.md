---
site: 01net.com
title: "Microsoft rate son rendez-vous avec l'open source"
author: Jonathan Charton
date: 2010-01-07
href: http://pro.01net.com/editorial/510711/microsoft-rate-son-rendez-vous-avec-lopen-source/
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> [...] Souvenez-vous. Il y a quatre mois de cela, Microsoft annonçait fièrement la création d'une fondation Codeplex.org, censée fédérer des éditeurs commerciaux autour d'une infrastructure web d'avant-garde pour .Net. Sam Ramji, alors président intérimaire, déclarait que la fondation nommerait d'ici à cent jours un bureau permanent, composé de pointures de l'open source pour encadrer les projets.
