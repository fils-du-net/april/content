---
site: ReadWriteWeb
title: "Google et Verizon seraient sur le point de s’entendre pour abandonner la Neutralité du Net"
author:  Fabrice Epelboin
date: 2010-08-05
href: http://fr.readwriteweb.com/2010/08/05/nouveautes/google-verizon-net-neutrality/
tags:
- Entreprise
- Internet
- Désinformation
- Neutralité du Net
- International
---

> Google et Verizon, un acteur majeur de l’accès à inter­net aux Etats-Unis, seraient sur le point de trou­ver un com­pro­mis concer­nant la net neu­tra­lity. Cet accord – qui reste à vali­der avec les auto­ri­tés amé­ri­caines – per­met­trait à Verizon de don­ner la prio­rité à cer­tains conte­nus sur son réseau en échange d’un paie­ment fourni par les ser­vices four­nis­sant ces conte­nus [update: Google dément que l’accord soit de nature finan­cière]. Selon le New York Times, cet accord pour­rait à terme signi­fier une hausse du prix de l’accès à inter­net pour les consommateurs.
