---
site: Numerama
title: "Hourra ! Le gouvernement répond sur la licence globale... 3 ans plus tard !"
author: Guillaume Champeau
date: 2010-08-04
href: http://www.numerama.com/magazine/16402-hourra-le-gouvernement-repond-sur-la-licence-globale-3-ans-plus-tard.html
tags:
- Internet
- Partage du savoir
- HADOPI
- Institutions
- DADVSI
- DRM
- Droit d'auteur
- Licenses
---

> Dans une longue réponse à une question qu'avait posé le député UMP Christian Vanneste il y a trois ans, le ministère de la Culture défend sa politique en matière de riposte graduée, et copie privée et d'opposition à la licence globale.
