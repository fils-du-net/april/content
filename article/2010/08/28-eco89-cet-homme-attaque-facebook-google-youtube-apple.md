---
site: Eco89
title: "Cet homme attaque Facebook, Google, YouTube, Apple..."
author: Augustin Scalbert
date: 2010-08-28
href: http://eco.rue89.com/2010/08/28/cet-homme-attaque-facebook-google-youtube-yahoo-apple-164255
tags:
- Entreprise
- Internet
- Brevets logiciels
- International
---

> Paul Allen, cofondateur de Microsoft, se dit propriétaire de technologies très utilisées de recherche d'information en ligne.
