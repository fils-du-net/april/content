---
site: Mediapart
title: "La neutralité expliquée à mon chien."
author: replikart
date: 2010-08-19
href: http://www.mediapart.fr/club/edition/internet-et-si-affinites/article/190810/la-neutralite-expliquee-mon-chien
tags:
- Le Logiciel Libre
- Internet
- Institutions
- Sensibilisation
- Neutralité du Net
---

> Je n'ai pas de chien.
