---
site: ZDNet.fr
title: "Violation de la licence GPL : une entreprise condamnée"
author: Christophe Auffray
date: 2010-08-05
href: http://www.zdnet.fr/actualites/violation-de-la-licence-gpl-une-entreprise-condamnee-39753689.htm
tags:
- Le Logiciel Libre
- Entreprise
- Institutions
- Licenses
- International
---

> Pour non respect de la licence GPL dans l'utilisation du logiciel libre BusyBox au sein de produits HDTV, la société Westinghouse Digital Electronics a été condamnée. Treize autres entreprises sont poursuivies.
