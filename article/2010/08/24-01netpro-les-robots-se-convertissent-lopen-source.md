---
site: 01netPro.
title: "Les robots se convertissent à l'Open Source"
author: Alain Clapaud
date: 2010-08-24
href: http://pro.01net.com/editorial/519977/les-robots-se-convertissent-a-l-open-source/
tags:
- Le Logiciel Libre
- Entreprise
- Innovation
- Licenses
---

> La plate-forme Urbi, qui anime les Lego Mindstorm ou le Segway, est désormais publiée sous licence Affero GPL 3. Gostai, son éditeur, espère ainsi booster sa diffusion dans les produits destinés au grand public.
