---
site: nouvelObs.com
title: "Litige entre Oracle et Google - high tech"
author: Jim Finkle et Alexei Oreskovic
date: 2010-08-15
href: http://hightech.nouvelobs.com/actualites/depeche/20100815.REU6677/litige-entre-oracle-et-google.html
tags:
- Entreprise
- Internet
- Économie
- Brevets logiciels
- International
---

> BOSTON/SAN FRANCISCO (Reuters) - Le géant de la recherche sur internet Google et le numéro deux des éditeurs de logiciels d'entreprises Oracle ne sont généralement pas vus comme des concurrents directs.
