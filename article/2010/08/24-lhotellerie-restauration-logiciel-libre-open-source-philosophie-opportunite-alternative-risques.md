---
site: L'Hôtellerie Restauration
title: "Logiciel libre : Open source, philosophie, opportunité, alternative, risques"
author: Thierry Longeau
date: 2010-08-24
href: http://www.lhotellerie-restauration.fr/journal/equipement-materiel/2010-08/Logiciel-libre-Open-source-philosophie-opportunite-alternative-risques.htm
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Sensibilisation
- Licenses
- Philosophie GNU
- Promotion
---

> Vous avez peut-être déjà utilisé un logiciel libre. Le navigateur internet Firefox, le lecteur vidéo VLC, la suite bureautique Open Office, sont sans doute les trois exemples les plus parlant. Les logiciels libres occupent de plus en plus une place de choix dans la sphère familiale et celle de l’entreprise que l’on soit sous Windows, Linux ou Mac.
