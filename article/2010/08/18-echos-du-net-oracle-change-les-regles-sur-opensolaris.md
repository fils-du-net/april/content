---
site: Echos du Net
title: "Oracle change les règles sur OpenSolaris"
author: Infested Grunt
date: 2010-08-18
href: http://www.echosdunet.net/dossiers/dossier_5669_oracle+change+regles+sur+opensolaris.html
tags:
- Le Logiciel Libre
- Entreprise
- International
---

> Oracle est en train de prendre possession des technologies de Sun. Après Java et Android, Oracle change le fonctionnement d'OpenSolaris, la version open-source du système d'exploitation Solaris, vis-à-vis du système primaire.
