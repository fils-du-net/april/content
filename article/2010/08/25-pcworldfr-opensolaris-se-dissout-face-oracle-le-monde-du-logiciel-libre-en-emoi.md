---
site: PCWorld.fr
title: "OpenSolaris se dissout face à Oracle : le monde du logiciel libre en émoi"
author: Mathieu Chartier
date: 2010-08-25
href: http://www.pcworld.fr/2010/08/25/logiciels/opensolaris-dissout-face-oracle/505327/
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
---

> OpenSolaris n'aura pas droit à son baroud d'honneur. Les membres de la communauté ont préféré lâcher prise face à Oracle, muet depuis le rachat de Sun et soucieux d'orienter les entreprises vers de nouvelles solutions se situant hors cadre du logiciel libre...
