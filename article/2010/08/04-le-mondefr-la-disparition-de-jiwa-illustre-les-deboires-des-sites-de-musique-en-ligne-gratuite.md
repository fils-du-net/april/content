---
site: Le Monde.fr
title: "La disparition de Jiwa illustre les déboires des sites de musique en ligne gratuite"
author: Cécile Ducourtieux
date: 2010-08-04
href: http://www.lemonde.fr/technologies/article/2010/08/04/la-disparition-de-jiwa-illustre-les-deboires-des-sites-de-musique-en-ligne-gratuite_1395456_651865.html
tags:
- Internet
- Économie
- Institutions
- Licenses
- Contenus libres
---

> Le tribunal de commerce de Paris a prononcé, le 29 juillet, la liquidation judiciaire de Jiwa, site d'écoute de musique en "streaming" (sans possibilité de téléchargement). Jiwa.fr devrait progressivement disparaître à partir de mercredi 4 août. C'était un des pionniers du genre dans l'Hexagone, à proposer de la musique gratuitement mais légalement, contre la vente d'espaces publicitaires en ligne.
