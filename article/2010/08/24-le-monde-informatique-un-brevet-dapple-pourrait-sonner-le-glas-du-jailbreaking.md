---
site: Le Monde Informatique
title: "Un brevet d'Apple pourrait sonner le glas du jailbreaking"
author: Véronique Arène
date: 2010-08-24
href: http://www.lemondeinformatique.fr/actualites/lire-un-brevet-d-apple-pourrait-sonner-le-glas-du-jailbreaking-31438.html
tags:
- Entreprise
- Logiciels privateurs
- Brevets logiciels
- DRM
- Droit d'auteur
- Informatique-deloyale
- International
---

> La demande de brevet effectuée par Apple, déposée plus tôt cette année, mais mise en ligne depuis jeudi dernier, pourrait mettre un terme au jailbreaking des iPhone, iPod et iPad. Ce système permet d'utiliser des applications piratées ou non absentes de l'AppStore. Pourtant, la méthode a récemment été déclarée conforme aux lois du copyright définies par le US Digital Millennium Copyright Act.
