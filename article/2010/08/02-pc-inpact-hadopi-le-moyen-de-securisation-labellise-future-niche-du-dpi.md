---
site: PC INpact
title: "Hadopi : le moyen de sécurisation labellisé, future niche du DPI"
author: Marc Rees
date: 2010-08-02
href: http://www.pcinpact.com/actu/news/58565-hadopi-moyen-securisation-dpi-filtrage.htm
tags:
- Internet
- HADOPI
- Institutions
- Informatique-deloyale
- Neutralité du Net
---

> Le projet de spécifications fonctionnelles des moyens de sécurisation (le document) diffusé la semaine dernière tente de décrire les qualités que devront présenter ces moyens pour être labellisés par la Hadopi. Le cas échéant, il permettra à l’abonné qui l’aura installé d’espérer une grande bienveillance de la part de la Hadopi si son IP venait à être flashée par les ayants droits et leur prestataire, l’entreprise TMG. Pourquoi ?
