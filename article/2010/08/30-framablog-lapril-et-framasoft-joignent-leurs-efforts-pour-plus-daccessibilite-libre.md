---
site: Framablog
title: "L'April et Framasoft joignent leurs efforts pour plus d'accessibilité... libre !"
author: aKa
date: 2010-08-30
href: http://www.framablog.org/index.php/post/2010/08/30/april-framasoft-accessibilite-libre
tags:
- Le Logiciel Libre
- April
- Accessibilité
---

> Avec le concours du groupe de travail accessibilité et logiciels libres de l’April, une nouvelle rubrique vient de voir le jour dans l’annuaire des logiciels libres Framasoft : la rubrique « Accessibilité, technologies d’assistance », classée dans la rubrique utilitaires.
