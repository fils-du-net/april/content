---
site: Le Monde Informatique
title: "La Fondation Linux clarifie l'utilisation du code Open Source"
author: Jean Elyan avec IDG NS
date: 2010-08-12
href: http://www.lemondeinformatique.fr/actualites/lire-la-fondation-linux-clarifie-l-utilisation-du-code-open-source-31363.html
tags:
- Le Logiciel Libre
- Entreprise
- Licenses
---

> Un programme rassemblant outils et formation est mis en place par la Fondation Linux en réponse à la montée en flèche du code Open Source dans les équipements mobiles et l'électronique grand public. Une initiative dont les entreprises devraient également tirer profit.
