---
site: La Voix du Nord
title: "Les bidouilleurs, des « réacs » férus de nouvelles technologies mais critiques"
author: Hervé Naudot
date: 2010-08-07
href: http://www.lavoixdunord.fr/Locales/Lille/actualite/Secteur_Lille/2010/08/07/article_les-bidouilleurs-des-reacs-ferus-de-nouv.shtml
tags:
- Le Logiciel Libre
---

> C'est un rituel bien connu des hackers lillois. Chaque premier vendredi du mois, le meeting 2600 réunit une dizaine de bidouilleurs qui se retrouvent à 19 h 30 devant le Furet du Nord, avant de cheminer vers un kebab pour papoter informatique, phreaking et autres réjouissances que seuls les initiés maîtrisent.
