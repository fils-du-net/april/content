---
site: PC INpact
title: "La Hadopi vante la clef Wep, Wifi Robin l'explose"
author: Marc Rees
date: 2010-08-02
href: http://www.pcinpact.com/actu/news/58577-wifirobin-wifi-robin-wep-hadopi.htm
tags:
- Internet
- HADOPI
- Désinformation
- Informatique-deloyale
---

> Éric Walter, secrétaire général de la Hadopi, expliquait qu’« Il existe de nombreux outils, connus, pour sécuriser son accès: logiciels de contrôle parental, pare-feu, clés WEP pour les accès Wi-Fi... ».
