---
site: ZDNet.fr
title: "La plainte d'Oracle contre Android : une attaque dirigée contre l'Open Source ?"
author: Guénaël Pépin
date: 2010-08-17
href: http://www.zdnet.fr/actualites/la-plainte-d-oracle-contre-android-une-attaque-dirigee-contre-l-open-source-39753919.htm
tags:
- Le Logiciel Libre
- Entreprise
- Brevets logiciels
- International
---

> Pour Florian Mueller, défenseur du logiciel libre, et James Gosling, créateur de Java, le procès d'Oracle contre Google vise à mettre à mal la communauté Open Source supportant Java.
