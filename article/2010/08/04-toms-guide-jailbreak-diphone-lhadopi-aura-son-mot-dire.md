---
site: Tom's guide
title: "Jailbreak d'iPhone : l'Hadopi aura son mot à dire"
author: Jean-Sébastien Zanchi
date: 2010-08-04
href: http://www.bestofmicro.com/actualite/28092-jailbreak-iphone-hadopi.html
tags:
- Entreprise
- Internet
- Interopérabilité
- HADOPI
- DRM
---

> On croyait l’Hadopi seulement cantonnée aux problématiques de piratage des oeuvres en ligne, mais c’est également cette autorité qui pourrait décider de la légalisation du jailbreak d’iPhone en France.
