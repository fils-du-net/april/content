---
site: AgoraVox
title: "Marseille 2013 : Des pingouins sur le Vieux-Port ?"
author: psion
date: 2010-08-03
href: http://www.agoravox.fr/actualites/technologies/article/marseille-2013-des-pingouins-sur-79243
tags:
- Le Logiciel Libre
- Internet
- Administration
- Sensibilisation
- Promotion
---

> [...] En 2009, Marseille traçait la voie pour d’autres collectivités en passant à Open-Office, la suite bureautique libre et gratuite.
