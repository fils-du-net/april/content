---
site: walfadjri
title: "Propriété intellectuelle II - Le système actuel, principal obstacle au transfert de technologie et à l'accès aux médicaments"
author: Ibrahima DIOP
date: 2010-08-11
href: http://www.walf.sn/contributions/suite.php?rub=8&id=66467
tags:
- Le Logiciel Libre
- Internet
- HADOPI
- Droit d'auteur
- International
- ACTA
---

> L’Acta préconise le durcissement des mesures techniques de protection des produits (Digital Rights Management System, Drm), comme les logiciels anti-copie présents sur les Dvd. L’Acta serait également gros de ce que d’aucuns appellent déjà une ‘shoah de l’Internet’, par référence au génocide des juifs par le régime hitlérien.
