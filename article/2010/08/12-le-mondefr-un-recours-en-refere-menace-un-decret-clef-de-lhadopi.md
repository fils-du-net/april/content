---
site: Le Monde.fr
title: "Un recours en référé menace un décret-clef de l'Hadopi"
author: La rédaction
date: 2010-08-12
href: http://www.lemonde.fr/technologies/article/2010/08/12/un-recours-en-refere-menace-un-decret-clef-de-l-hadopi_1398173_651865.html
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- HADOPI
- Institutions
- Associations
---

> Le fournisseur d'accès à Internet (FAI) associatif FDN a déposé un recours en référé devant le Conseil d'Etat contre le décret 2010-872, un texte-clef définissant la commission de protection des droits de la Haute Autorité pour la diffusion des œuvres et la protection des droits sur Internet (Hadopi).
