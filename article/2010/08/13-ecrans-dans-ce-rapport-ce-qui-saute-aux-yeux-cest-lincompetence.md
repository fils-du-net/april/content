---
site: écrans
title: "« Dans ce rapport, ce qui saute aux yeux, c'est l'incompétence »"
author: Andréa Fradin
date: 2010-08-13
href: http://ecrans.fr/Dans-ce-rapport-ce-qui-saute-aux,10599.html
tags:
- Le Logiciel Libre
- Internet
- HADOPI
- Institutions
- Associations
- Désinformation
- Neutralité du Net
---

> Benjamin Bayart est l’un des précurseurs dans la lutte pour la neutralité du net en France. Président de FDN (French Data Network), le plus vieux fournisseur d’accès Internet de l’Hexagone, il connait parfaitement les structures techniques et économiques qui régissent le réseau mondial. Nous l’avions déjà interviewé début 2009 et le résultat ,« Tout le monde a intérêt à transformer Internet en Minitel », fut mémorable.
