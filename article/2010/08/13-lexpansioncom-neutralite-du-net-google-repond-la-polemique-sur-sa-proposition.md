---
site: L'Expansion.com
title: "Neutralité du Net: Google répond à la polémique sur sa proposition"
author: La rédaction
date: 2010-08-13
href: http://www.lexpansion.com/economie/actualite-high-tech/neutralite-du-net-google-repond-a-la-polemique-sur-sa-proposition_237264.html
tags:
- Entreprise
- Internet
- Neutralité du Net
- International
---

> Google s'est justifié sur son blog, se défendant de s'être "vendu" sur sa position relative à la régulation de la neutralité du net.
