---
site: Silicon.fr
title: "Java: James Gosling demande à Oracle de tenir ses promesses"
author: David Feugey
date: 2010-08-31
href: http://www.silicon.fr/java-james-gosling-demande-a-oracle-de-tenir-ses-promesses-41692.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Sensibilisation
- International
---

> En 2007, Oracle avait proposé de libérer le processus de développement de Java. À la veille de la JavaOne 2010, James Gosling veut rappeler à Larry Ellison qu'il lui faudra tenir ses engagements.
