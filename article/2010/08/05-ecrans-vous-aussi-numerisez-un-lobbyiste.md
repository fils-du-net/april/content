---
site: Ecrans
title: "Vous aussi, numérisez un lobbyiste"
author: Sophie Gindensperger
date: 2010-08-05
href: http://ecrans.fr/Les-lobbyistes-dans-la-mire-des,10566.html
tags:
- Institutions
- Associations
- Contenus libres
---

> Exercer sa vigilance citoyenne est désormais à portée de clic de tout internaute. Depuis hier, le collectif Regards citoyens a mis en place une vaste opération de « crowdsourcing », afin d’identifier et surtout de cartographier les lobbyistes qui œuvrent à l’Assemblée nationale. En clair ici, utiliser les capacités (et la tendance à procrastiner ?) d’un grand nombre d’internautes pour abattre une tâche à première vue fastidieuse.
