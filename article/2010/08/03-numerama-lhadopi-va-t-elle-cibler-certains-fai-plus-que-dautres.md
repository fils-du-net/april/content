---
site: Numerama
title: "L'Hadopi va-t-elle cibler certains FAI plus que d'autres ?"
author: Guillaume Champeau
date: 2010-08-03
href: http://www.numerama.com/magazine/16393-l-hadopi-va-t-elle-cibler-certains-fai-plus-que-d-autres.html
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
---

> En l'absence d'accord global entre les fournisseurs d'accès à Internet et le gouvernement, les FAI décideront eux-mêmes du tarif qu'ils souhaiteront facturer à l'Hadopi pour l'identification des abonnés auxquels seront envoyés des avertissements. Le prix choisi par les opérateurs pourra aller de zéro centime à 8,50 euros maximum par adresse IP. Tous ne factureront pas le prix fort...
