---
site: Lesinrocks
title: "Achetez vos films sur clé USB au cinéma"
author: Christophe Payet
date: 2010-08-10
href: http://www.lesinrocks.com/cine/cinema-article/t/49172/date/2010-08-10/article/achetez-vos-films-sur-cle-usb-au-cinema/
tags:
- HADOPI
---

> Le réseau de cinémas Utopia lance un circuit de distribution alternatif, "Vidéo en poche". Le principe : acheter un film au cinéma et l’emporter sur une clé USB. Le tout via un logiciel libre et sans DRM.
