---
site: Developpez.com
title: "Le H.264 devient gratuit"
author: La rédaction
date: 2010-08-27
href: http://www.developpez.com/actu/20323/Le-H-264-devient-gratuit-sans-limitation-dans-le-temps-pour-les-videos-librement-accessibles-par-l-utilisateur-final-l-effet-WebM
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- Sensibilisation
- Licenses
- Video
- International
---

> Sans limitation dans le temps pour les vidéos librement accessibles par l'utilisateur final, l'effet WebM ?
