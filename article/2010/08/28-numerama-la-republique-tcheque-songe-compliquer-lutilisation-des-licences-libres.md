---
site: Numerama
title: "La République Tchèque songe à compliquer l'utilisation des licences libres"
author: Julien L.
date: 2010-08-28
href: http://www.numerama.com/magazine/16616-la-republique-tcheque-songe-a-compliquer-l-utilisation-des-licences-libres.html
tags:
- Le Logiciel Libre
- Institutions
- Droit d'auteur
- Licenses
- Contenus libres
- International
---

> Selon l'EDRI, la République Tchèque souhaite limiter la propagation des licences libres. En effet, un projet de loi a récemment fuité et celui-ci cherche à compliquer leur utilisation. Un auteur devrait notamment signaler sa démarche auprès d'un "administrateur" et lui apporter la preuve de sa paternité sur une oeuvre donnée.
