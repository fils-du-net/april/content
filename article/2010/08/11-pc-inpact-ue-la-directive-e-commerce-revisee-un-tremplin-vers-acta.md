---
site: PC INpact
title: "UE : la directive e-commerce révisée, un tremplin vers ACTA"
author: Marc Rees
date: 2010-08-11
href: http://www.pcinpact.com/actu/news/58735-michel-barnier-directive-ecommerce-acta.htm
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> La Commission européenne vient de lancer une « consultation publique sur l'avenir du commerce électronique dans le marché intérieur et la mise en œuvre de la directive commerce électronique (2000/31/CE) ».
