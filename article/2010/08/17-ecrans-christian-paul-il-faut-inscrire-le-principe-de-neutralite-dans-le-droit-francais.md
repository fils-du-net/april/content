---
site: écrans
title: "Christian Paul : « Il faut inscrire le principe de neutralité dans le droit français »"
author: Andréa Fradin
date: 2010-08-17
href: http://www.ecrans.fr/Christian-Paul-Il-faut-inscrire-le,10617.html
tags:
- Internet
- Institutions
- Neutralité du Net
---

> En moins de deux jours, la proposition a déjà suscité de nombreuses réactions : Christian Paul revient pour Ecrans sur son texte et ses intentions.
