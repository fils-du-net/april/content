---
site: AgoraVox
title: "Le libre au delà de l'économie standard"
author: IceFinger
date: 2010-08-26
href: http://www.agoravox.fr/actualites/economie/article/le-libre-au-dela-de-l-economie-80203
tags:
- Le Logiciel Libre
- Administration
- Économie
- Partage du savoir
- Sensibilisation
- DADVSI
- DRM
- Droit d'auteur
- Licenses
- Philosophie GNU
- Contenus libres
---

> Cet article est le résumé du document suivant, demandé par Nicole Hugon, maire de Marseille 13 et 14ème arrondissement de Marseille, afin d’éclairer la position des Verts sur le libre.
