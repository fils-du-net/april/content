---
site: Zinfos974.com
title: "Dons d'ordinateurs : la Région veut libérer les Réunionnais de Windows"
author: Karine Maillot
date: 2010-08-23
href: http://www.zinfos974.com/Dons-d-ordinateurs-la-Region-veut-liberer-les-Reunionnais-de-Windows_a20510.html
tags:
- Le Logiciel Libre
- Administration
- Sensibilisation
- Éducation
---

> Les ordinateurs destinés aux élèves de secondes qui feront valoir leur bon d'achat de 500 euros accordés par la Région et valables chez une quarantaine de revendeurs locaux seront équipés avec le système d'exploitation Linux qui est libre de droit et inaccessibles aux virus informatiques.
