---
site: The Register
title: "GPL scores historic court compliance victory"
author: Gavin Clarke
date: 2010-08-04
href: http://www.theregister.co.uk/2010/08/04/gpl_violation_westinghouse/
tags:
- Le Logiciel Libre
- Entreprise
- Institutions
- Licenses
- International
- English
---

> (Victoire majeure dans une cour Américaine) Open sourcers have scored a major victory in a US court over violation of the GPL.
