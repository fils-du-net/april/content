---
site: paperblog
title: "UMP: loi en faveur de la neutralité du net"
author: Actweo
date: 2010-08-07
href: http://www.paperblog.fr/3500397/ump-loi-en-faveur-de-la-neutralite-du-net/
tags:
- Internet
- HADOPI
- Neutralité du Net
---

> L’Union pour un mouvement populaire peut-elle encore se réconcilier avec la génération Internet et les acteurs du monde numérique ? Pour l’heure, cela semble plutôt mal engagé. Après trois ans de pouvoir, la majorité en place a laissé de profondes stigmates dans la sphère numérique. Des cicatrices qui auront toutes les peines du monde à cicatriser.
