---
site: Le Cercle Les Echos
title: "L’open source alimentera la consumérisation de l’information"
author: Brian Gentile
date: 2010-08-24
href: http://lecercle.lesechos.fr/entreprises-marches/high-tech-medias/221130977/l-open-source-alimentera-la-consumerisation-de-l-info
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Économie
- Sensibilisation
---

> Il y a plus de dix ans, l’open source a considérablement modifié le développement et l’économie des logiciels au niveau des infrastructures. En dépit d’être l’âme-même des organisations internationales, de nombreux directeurs financiers et PDG  refusent de se laisser immerger dans ce qu’ils considèrent comme « un problème IT naissant ». Toutefois, ce bouleversement touche aujourd’hui les entreprises.
