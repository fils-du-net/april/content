---
site: Numerama
title: "Des ayants droit exigent par erreur la suppression de vidéos dont ils n'ont pas les droits"
author: Julien L.
date: 2010-08-13
href: http://www.numerama.com/magazine/16494-des-ayants-droit-exigent-par-erreur-la-suppression-de-videos-dont-ils-n-ont-pas-les-droits.
tags:
- Entreprise
- Internet
- Droit d'auteur
- Licenses
- Contenus libres
- International
---

> En Allemagne, une technologie anti-piratage a signalé par erreur des vidéos sous licence Creative Commons. L'organisation GVU a alors demandé à Vimeo de retirer les contenus désignés, sans véritable vérification a posteriori.
