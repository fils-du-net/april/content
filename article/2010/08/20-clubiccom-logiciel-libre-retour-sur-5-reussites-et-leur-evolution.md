---
site: clubic.com
title: "Logiciel libre : retour sur 5 réussites et leur évolution"
author: Stéphane Ruscher
date: 2010-08-20
href: http://www.clubic.com/linux-os/article-359004-1-logiciel-libre-zoom-reussites-evolutions.html
tags:
- Le Logiciel Libre
- Internet
- Sensibilisation
- Philosophie GNU
- Promotion
---

> Linux, Mozilla Firefox, OpenOffice.org, Gimp ou encore VLC ont pour point commun d'être des projets libres ayant réussi à subsister, évoluer (plus ou moins bien) et trouver leur place dans notre utilisation quotidienne. Nous revenons sur plusieurs de ces réussites...
