---
site: Numerama
title: "Avec MeeGo, Nokia et Intel veulent séduire la communauté Linux"
author: Guillaume Champeau
date: 2010-08-16
href: http://www.numerama.com/magazine/16520-avec-meego-nokia-et-intel-veulent-seduire-la-communaute-linux.html
tags:
- Le Logiciel Libre
- Entreprise
- Sensibilisation
- Innovation
- Philosophie GNU
---

> Pour contrer l'iPhone et le système Android de Google, Nokia et Intel veulent embrasser la philosophie open-source et Linux avec MeeGo. Ils l'ont fait savoir lors du dernier LinuxCon à Boston.
