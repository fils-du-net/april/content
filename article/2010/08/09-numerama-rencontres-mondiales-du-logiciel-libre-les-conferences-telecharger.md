---
site: Numerama
title: "Rencontres Mondiales du Logiciel Libre : les conférences à télécharger"
author: Guillaume Champeau
date: 2010-08-09
href: http://www.numerama.com/magazine/16448-rencontres-mondiales-du-logiciel-libre-les-conferences-a-telecharger.html
tags:
- Le Logiciel Libre
- Internet
- Sensibilisation
- Associations
- Licenses
- Contenus libres
---

> Après Nantes l'an dernier, c'est à Bordeaux qu'étaient organisées cette année les célèbres Rencontres Mondiales du Logiciel Libre, du 6 au 11 juillet 2010. Ceux qui n'ont pas eu la chance de s'y rendre peuvent se rattraper en profitant des vacances pour écouter les conférences, désormais disponibles sur BitTorrent à travers le tracker FreeTorrent.
