---
site: nytimes.com
title: "A Software Populist Who Doesn’t Do Windows"
author: Ashlee Vance
date: 2009-01-10
href: http://www.nytimes.com/2009/01/11/business/11ubuntu.html?_r=1
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Innovation
---

> -----------------------
> “If we’re successful, we would fundamentally change the operating system
> market,” Mr. Shuttleworth said during a break at the gathering, the Ubuntu
> Developer Summit. “Microsoft would need to adapt, and I don’t think that
> would
> be unhealthy.”
> -----------------------
> "Si nous sommes couronnés de succès, nous aurons changé fondamentalement le
> marché du système d'exploitation", a dit M. Shuttleworth au cours d'une
> pause du Sommet des Développeurs Ubuntu. "Microsoft aurait alors besoin de
