---
site: 01net.com
title: "Yahoo! place son serveur web en open source"
author: Jonathan Charton
date: 2009-11-04
href: http://pro.01net.com/editorial/508037/yahoo-place-son-serveur-web-en-open-source/
tags:
- Le Logiciel Libre
- Internet
---

> Les développeurs de la société californienne placent Traffic Server, un serveur à 30 000 requêtes/seconde dans l’incubateur Apache et corrigent les bogues de leur distribution Hadoop.
> Alors que la grand-messe ApacheCon bat son plein à Oakland (pour les dix ans de la fondation Apache) les équipes de développement de Yahoo! viennent en effet, d'un coup d'un seul, de placer le code du serveur proxy-cache Traffic Server dans l'incubateur d'Apache et de livrer une nouvelle mise à jour de l'implémentation maison d'Hadoop, le Framework Java massivement extensible inspiré de l'environnement de développement pour les architectures Web distribuée MapReduce et du Google File System.
