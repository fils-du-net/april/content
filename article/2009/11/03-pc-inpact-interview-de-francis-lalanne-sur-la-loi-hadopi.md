---
site: pcinpact.com
title: "Interview de Francis Lalanne sur la loi Hadopi"
author: Marc Rees
date: 2009-11-03
href: http://www.pcinpact.com/actu/news/53915-francis-lalanne-hadopi-bataille-interview.htm
tags:
- HADOPI
---

> Au cours de la conférence de presse sur la Bataille Hadopi, Francis Lalanne, coauteur de l’ouvrage, a annoncé la sortie en janvier 2010 d'un disque, d'un film et d'un livre sous licence libre. On pourra retrouver le podcast intégral sur le site d’OxyRadio. En marge de la soirée, nous avons pu interroger le chanteur sur son engagement plus général dans ce débat et ses positions sur la question Hadopi.
