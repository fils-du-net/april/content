---
site: Ecrans.fr
title: "Dessiner à l’œil"
author: Erwan Cario
date: 2009-11-16
href: http://www.ecrans.fr/Dessiner-a-l-oeil,8554.html
tags:
- Le Logiciel Libre
- Accessibilité
- Video
---

> Les collectifs Free Art and Technology (FAT), OpenFrameworks, the Graffiti Research Lab (dont on a déjà parlé, ici par exemple), et The Ebeling Group se sont regroupés dernièrement pour lui permettre de reprendre le dessin grâce à un capteur de mouvements oculaires.
