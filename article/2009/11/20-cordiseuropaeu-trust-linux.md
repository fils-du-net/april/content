---
site: cordis.europa.eu
title: "Trust Linux! "
author: La rédaction
date: 2009-11-20
href: http://cordis.europa.eu/ictresults/index.cfm?section=news&tpl=article&ID=91011
tags:
- Le Logiciel Libre
- Informatique-deloyale
- English
---

> Une équipe de chercheurs à implémenté un système d'"informatique dite de confiance" dans une version  commerciale du système d'exploitation open source Linux.
