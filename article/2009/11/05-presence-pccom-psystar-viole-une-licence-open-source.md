---
site: "presence-pc.com"
title: "Psystar viole une licence open source"
author: Pierre Dandumont 
date: 2009-11-05
href: http://www.presence-pc.com/actualite/psystar-opensource-37050/
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Interopérabilité
---

> Psystar a annoncé il y a quelques jours un programme, Rebel EFI, qui permet (en théorie) d'installer Mac OS X (10.6) sur n'importe quel PC. Même si nos efforts se sont soldés par des échecs, l'idée est intéressante. Sauf que Psystar viole la licence open source.
