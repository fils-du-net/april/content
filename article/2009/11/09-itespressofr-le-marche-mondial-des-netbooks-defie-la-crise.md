---
site: itespresso.fr
title: "Le marché mondial des netbooks défie la crise"
author: Anne Confolant
date: 2009-11-09
href: http://www.itespresso.fr/le-marche-mondial-des-netbooks-defie-la-crise-32293.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> [...] Par ailleurs, ABI Research note que près de 32% des netbooks écoulés en 2009 tourneront sous l’OS open source Linux, un système d’exploitation mis en concurrence avec Windows XP et maintenant Windows 7 sur le créneau des netbooks. Les mini-PC sous l’OS de Microsoft restent majoritaires : Windows s’octroie ainsi 68% du marché.
> Mais ce chiffre reste encourageant pour Linux, qui peut compter sur les pays en voie de développement pour populariser l’OS ouvert au sein de ces ordinateurs portables bon marché.
