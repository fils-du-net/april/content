---
site: edubourse.com
title: "UGO : un logiciel libre pour l’Aide Sociale à l’Enfance"
author: La rédaction
date: 2009-11-27
href: http://www.edubourse.com/finance/actualites.php?actu=57653
tags:
- Le Logiciel Libre
- Administration
---

> [...] Développé par le Conseil général de l’Essonne, ce logiciel est un outil au service des travailleurs sociaux afin de faciliter leur recherche d’hébergement d’urgence dans l’intérêt des enfants pris en charge au titre de l’Aide Sociale à l’Enfance.
> [...] Développé en interne et conçu avec une volonté collaborative inter-services, partenariale et avec les utilisateurs, le logiciel a été versé à la communauté du libre en septembre 2009. Cette démarche permet ainsi à tous les départements qui le souhaitent de télécharger librement le logiciel et de l’utiliser sans frais de licence, de modifier les programmes et de les adapter à leurs besoins, avec la possibilité de mutualiser les retours d’expérience. Plusieurs départements ont d’ailleurs déjà signifié leur intérêt pour la plateforme.
