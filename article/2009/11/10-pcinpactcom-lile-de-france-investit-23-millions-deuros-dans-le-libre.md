---
site: pcinpact.com
title: "L'Île-de-France investit 23 millions d'euros dans le libre"
author: Marc Rees
date: 2009-11-10
href: http://www.pcinpact.com/actu/news/54054-mozilla-jean-paul-huchon-libre.htm
tags:
- Le Logiciel Libre
- Administration
---

> [...] Jean Paul Huchon a rappelé l’attachement de la région Île-de-France pour le libre : « nous avons été de toutes les aventures du libre depuis plusieurs années. La grande nouvelle de cette année est que nous avons décidé que les espaces numériques de travail – les ENT – dans les 471 lycées d’île de France, plus une centaine de lycées privés, et qui doivent commencer à être mis en place pour les 500 000 lycéens en janvier se fonderaient sur une solution totalement libre ».
