---
site: "livebox-news.com"
title: "orange ouvre les logiciels libres de sa livebox"
date: 2009-11-27
href: http://livebox-news.com/2009/11/27/1301/orange-ouvre-les-logiciels-libres-de-sa-livebox/
tags:
- Le Logiciel Libre
- Entreprise
- Internet
---

> [...] Orange a ouvert le site Livebox-Opensource. Ce dernier permet à la communauté Open Source et Logiciels Libre d’accéder aux codes sources des logiciels libre présents dans les différentes Livebox de l’opérateur. Orange répond enfin à la communauté qui l’accuse de ne pas proposer les versions utilisés comme l’impose la GNU GPL.
