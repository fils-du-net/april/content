---
site: clubic.com
title: "VoIP : Skype deviendrait bientôt open source"
author: Guillaume Belfiore
date: 2009-11-02
href: http://www.clubic.com/actualite-308796-skype-open-source-voip.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Sur son blog personnel, le développeur français Olivier Faurax explique que le logiciel de voix sur IP Skype serait en passe de devenir open source. Alors qu'il cherchait un gestionnaire de paquets Red Hat (RPM) pour Skype, M. Faurax reçu de la part de l'équipe de développement cette réponse : « nous sommes bien conscients qu'il n'existe pas de version pour Mandriva pour le moment. Nous sommes heureux de vous informer que Skype rejoindra la communauté open source ».
