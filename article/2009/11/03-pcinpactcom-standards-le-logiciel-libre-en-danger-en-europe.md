---
site: pcinpact.com
title: "Standards : le logiciel libre en danger en Europe"
author: Vincent Hermann
date: 2009-11-03
href: http://www.pcinpact.com/actu/news/53945-europe-eif-standards-version-2-lobbies.htm
tags:
- Interopérabilité
- Institutions
- Europe
---

> Ces dernières années, l’Europe a pu apparaître comme une entité très à cheval sur les règles de libre concurrence. Les procès retentissants contre Microsoft et Intel ont principalement participé à cette image, une réputation encore renforcée par un document sorti en 2004 par l’IDABC (Interoperable delivery of pan-European eGovernment services to public administrations, businesses and citizens) et mettant clairement l’accent sur l’interopérabilité et les logiciels libres dans les instances gouvernementales.
