---
site: liberation.fr
title: "L’avenir sera «coopétitif» ou risque de ne pas être! "
author: Nicolas Quint
date: 2009-11-26
href: http://resultat-exploitations.blogs.liberation.fr/finances/2009/11/lavenir-sera-coop%E9titif-ou-risque-de-ne-pas-%EAtre-.html
tags:
- Le Logiciel Libre
---

> [...] Un autre exemple? Les logiciels libres. A l’heure où j’écris ces lignes, des milliers de personnes contribuent à améliorer bénévolement des logiciels. Le temps qu’ils passent à ce travail pourrait être très correctement monnayé là où ils ne récoltent que la satisfaction du travail accompli et un peu de gloire personnelle (restreinte à un petit cercle d’initiés) dans le meilleur des cas. Là aussi, la compétition n’est pas absente. Les logiciels libres sont en concurrence entre eux, ce qui permet de tirer la qualité vers le haut.
