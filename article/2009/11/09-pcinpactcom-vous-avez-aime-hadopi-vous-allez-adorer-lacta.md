---
site: pcinpact.com
title: "Vous avez aimé Hadopi ? Vous allez adorer l'ACTA"
author: Marc Rees
date: 2009-11-09
href: http://www.pcinpact.com/actu/news/54030-acta-hadopi-riposte-surveillance-internet.htm
tags:
- HADOPI
- DADVSI
- DRM
---

> [...] L’ACTA serait par ailleurs très inspirée du fameux DMCA, lui même fils du traité de l’OMPI sur le droit d’auteur et toute la législation anticontournement (dont la DADVSI). Les modalités pourraient cependant être nettement plus nerveuses à l’instar du traité signé entre les États unis et la Corée du Sud : ce traité permet des exceptions à l’interdiction du contournent aux dispositifs anticopie (reverse engineering, test, etc.) mais il n’inclut pas la limitation pour fair use ou usage loyal.
