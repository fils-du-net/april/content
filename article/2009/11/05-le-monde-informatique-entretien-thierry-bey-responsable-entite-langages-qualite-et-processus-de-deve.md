---
site: lemondeinformatique.fr
title: "Entretien Thierry Bey, responsable entité Langages, Qualité et Processus de Développement de PSA Peugeot"
author: Olivier Rafal
date: 2009-11-05
href: http://www.lemondeinformatique.fr/entretiens/lire-thierry-bey-responsable-entite-langages-qualite-et-processus-de-developpement-de-psa-peugeot-citroen-136.html
tags:
- Le Logiciel Libre
- Entreprise
---

> Il était important que ce fût en Open Source ?
> Oui, d'abord parce que cela élimine des coûts logiciels, et ensuite parce que cela nous donne la possibilité de mettre en place une solution qui correspond au plus juste à nos besoins. Enfin et surtout, cela nous permettait de démontrer notre capacité à développer une méthodologie innovante pour la réalisation de projets. Ce partage d'informations avec d'autres grands comptes donne de la valeur à la solution et facilite son acceptation en interne. Tout le monde est d'accord pour faire de la qualité, mais quand on regarde les contraintes que cela implique, c'est tout de suite plus compliqué. Il fallait donc une véritable crédibilité. Ce partage avec d'autres grandes entreprises partageant des problématiques similaires nous permettait d'aller vers une standardisation mieux acceptée.
