---
site: numerama.com
title: "Hadopi : Mitterrand pressé de préciser l'obligation de sécurisation"
author: Guillaume Champeau
date: 2009-11-12
href: http://www.numerama.com/magazine/14471-hadopi-mitterrand-presse-de-preciser-l-obligation-de-securisation.html
tags:
- HADOPI
---

> Le député UMP François Loos a demandé au ministre de la Culture Frédéric Mitterrand de "clarifier au plus vite" la nature exacte des obligations de sécurisation dont devront s'acquitter les abonnés à Internet pour éviter de se trouver en violation du délit de "négligence caractérisée" instauré par la loi Hadopi 2.
