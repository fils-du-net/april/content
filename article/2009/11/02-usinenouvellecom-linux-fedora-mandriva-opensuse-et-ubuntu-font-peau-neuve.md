---
site: usinenouvelle.com
title: "Linux : Fedora, Mandriva, Opensuse et Ubuntu font peau neuve"
author: Christophe Dutheil
date: 2009-11-02
href: http://www.usinenouvelle.com/article/linux-fedora-mandriva-opensuse-et-ubuntu-font-peau-neuve.N120458
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Deux semaines après le lancement commercial de Windows 7, quatre des principales distributions Linux sont en passe de sortir dans une nouvelle version. Avec l'espoir de percer (enfin) sur les postes de travail en entreprise.
> Au lendemain du lancement commercial de Windows 7, les distributions Linux font elles aussi leur révolution, quoiqu'en ordre un peu plus dispersé. Du 29 octobre au 17 novembre, les lancements des nouvelles versions de quatre distributions majeures sont planifiés. Ubuntu (sponsorisé par Canonical) a ouvert le bal dès le 29 octobre avec l'arrivée de la 9.10, plus connue sous son nom de code « Karmic Koala ». La société française Mandriva sortira de son côté dès le 3 novembre Mandriva Linux 2010, la dernière mouture de son système d'exploitation Linux pour postes de travail. Opensuse (soutenu par Novell) et Fedora (piloté par Red Hat) ne sont pas en reste : les lancements d'Opensuse 11.2 et de Fedora 12 (Constantine) sont prévus pour le 12 et le 17 novembre.
