---
site: lemonde.fr
title: "En cinq ans, le navigateur libre Firefox a conquis un quart des internautes"
author: Damien Leloup
date: 2009-11-09
href: http://www.lemonde.fr/technologies/article/2009/11/09/en-cinq-ans-le-navigateur-libre-firefox-a-conquis-un-quart-des-internautes_1264512_651865.html
tags:
- Le Logiciel Libre
- Internet
---

> [...] Firefox parviendra-t-il un jour à détrôner Internet Explorer ? Depuis 2004, la situation a bien changé. Alors qu'il y a cinq ans Firefox représentait l'alternative face à un monopole quasi total, le paysage est aujourd'hui foncièrement différent. En plus de Firefox, d'autres navigateurs se sont développés : Safari pour Apple, Chrome pour Google, sans oublier Opera, un autre projet moins connu mais qui détient tout de même environ 2 % du marché.
