---
site: 59hardware.net
title: "OS et Netbook, une grande histoire!"
author: Benoit Lamamy
date: 2009-11-23
href: http://www.59hardware.net/dossier/portable/200911238974.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> [...] Mobiles et peu chères, ces machines séduisent de plus en plus de personnes. Néanmoins, il convient de garder à l’esprit que le niveau de performance qu’elles proposent est assez faible, limité par l’omniprésent Atom d’Intel… Par ailleurs, le père du concept (Asus, avec son Eee PC 700) ne s’y était pas trompé : rappelez-vous, les premiers netbooks Asus, Acer ou encore Hercules étaient fournis avec une distribution Linux, système généralement réputé moins gourmand que Windows.
