---
site: "cio-online.com"
title: "La TVA désormais gérée sur une solution à base Open-Source"
author: Bertrand Lemaire
date: 2009-11-24
href: http://www.cio-online.com/actualites/lire-la-tva-desormais-geree-sur-une-solution-a-base-open-source-2560.html
tags:
- Le Logiciel Libre
- Administration
---

> La Direction générale des Finances Publiques a opté pour une architecture basée sur 600 serveurs HP à processeurs Itanium, sous HP-UX, et utilisant la solution open-source Cobol-IT Compiler Suite.
> La Direction générale des Finances Publiques (DGFiP) fait évoluer en permanence les logiciels gérant la TVA en France, écrits en langage cobol. Ces applications ont des obligations de haute disponibilité.
