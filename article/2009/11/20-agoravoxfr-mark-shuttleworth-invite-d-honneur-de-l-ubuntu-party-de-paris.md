---
site: agoravox.fr
title: "Mark Shuttleworth, invité d’honneur de l’Ubuntu-Party de Paris "
author: Winael 
date: 2009-11-20
href: http://www.agoravox.fr/actualites/technologies/article/mark-shuttleworth-invite-d-honneur-65321
tags:
- Le Logiciel Libre
---

> Devenu un évènement incontournable pour les aficionados du logiciel libre, Ubuntu-Party accueille de nouveau le grand public à la Villette pour célébrer la sortie de la nouvelle version 9.10 (Karmic Koala).
> Fait exceptionnel pour cette édition : le "Papa" de la distribution GNU/Linux la plus populaire, Mark Shuttleworth, sera l’invité d’honneur de l’Ubuntu-Party. Il tiendra une grande conférence plénière le samedi 28 novembre 2009 pour clore cette première journée.
