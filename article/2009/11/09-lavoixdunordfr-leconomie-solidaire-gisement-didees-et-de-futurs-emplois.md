---
site: lavoixdunord.fr
title: "L'économie solidaire, gisement d'idées et de futurs emplois ?"
author: L.B.
date: 2009-11-09
href: http://www.lavoixdunord.fr/Locales/Lille/actualite/Secteur_Lille/2009/11/09/article_l-economie-solidaire-gisement-d-idees-et.shtml
tags:
- Actualité locale
- Le Logiciel Libre
- Entreprise
- Sensibilisation
- Associations
---

> Ces logiciels comportent quatre libertés, explique Alexandre Storoz, 22 ans. Ils servent pour n'importe quel usage, sans restriction. On peut étudier leur fonctionnement. On peut les améliorer et on peut les redistribuer, y compris à titre onéreux pour ce qui est de leurs améliorations. »
