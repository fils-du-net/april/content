---
site: 01net.com
title: "Une Ubuntu Party à Paris en présence de Mark Shuttleworth"
author: Guillaume Deleurence
date: 2009-11-26
href: http://www.01net.com/editorial/509079/une-ubuntu-party-a-paris-en-presence-de-mark-shuttleworth/
tags:
- Le Logiciel Libre
---

> [...] Selon Olivier Fraysse, le public qui se déplace se partage entre personnes qui ne connaissent rien à Ubuntu, qui « confondent encore souvent logiciel libre et gratuit », et nouveaux utilisateurs d'Ubuntu, qui viennent pour poser des questions ou remercier la communauté faisant vivre cette distribution.
