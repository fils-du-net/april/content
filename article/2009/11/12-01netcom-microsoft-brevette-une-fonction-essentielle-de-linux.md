---
site: 01net.com
title: "Microsoft brevette une fonction essentielle de Linux"
author: Yann Serra
date: 2009-11-12
href: http://pro.01net.com/editorial/508593/microsoft-brevette-une-fonction-essentielle-de-linux/
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Brevets logiciels
---

> La cour suprême américaine vient d'attribuer à Microsoft un brevet pour une fonction qu'il n'a pas du tout inventée. Il s'agit de la commande sudo. Même si cette fonction n'est pas citée sous ce nom dans le nouveau brevet numéro 7617530, la description d'un logiciel « pour élever temporairement les droits d'un utilisateur » correspond bien à cette commande, essentielle dans Linux et Mac OS X. Elle fait partie d'Unix depuis 1980 et a été mise au point dans les années 70 sur les mainframes.
