---
site: arstechnica.com
title: "SFLC tech director finds one new GPL violator every day"
author: Ryan Paul
date: 2009-11-09
href: http://arstechnica.com/open-source/news/2009/11/sflc-tech-director-finds-one-new-gpl-violator-every-day.ars
tags:
- Le Logiciel Libre
- Droit d'auteur
- Licenses
---

> Le chef de la SFLC publie un guide pour gérer les violations de la GPL : la plupart relèvent de la négligence, mieux vaut contacter en privé que de lyncher en public.
