---
site: "presence-pc.com"
title: "Microsoft a volé du code pour Windows 7 USB ?"
author: David Civera
date: 2009-11-11
href: http://www.presence-pc.com/actualite/Microsoft-USB-Windows-7-37119/
tags:
- Logiciels privateurs
- Droit d'auteur
- Licenses
---

> L’outil permettant l’installation de Windows 7 depuis une clé USB aurait été conçu à partir de codes que Microsoft aurait copié d’un autre programme [ndlr : sous licence GPLv2] sans avoir acquis les droits nécessaires.
