---
site: zdnet.fr
title: "La santé financière de Mozilla est solide selon sa présidente, Mitchell Baker"
author: Christophe Auffray
date: 2009-11-20
href: http://www.zdnet.fr/actualites/internet/0,39020774,39710946,00.htm
tags:
- Le Logiciel Libre
---

> La Fondation Mozilla se porte bien et dispose des fonds suffisants pour financer ses différents projets Web. En 2008, elle a cumulé 78,6 millions de dollars de ressources, la majeure partie provenant de son partenariat avec Google.
