---
site: lemagit.fr
title: "Le Libre français s’insurge contre le brouillon du cadre d’intéropérabilité européen "
author: Cyrille Chausson
date: 2009-11-25
href: http://www.lemagit.fr/article/standards-europe-lobby-opensource-logiciel-libre/4915/1/le-libre-francais-insurge-contre-brouillon-cadre-interoperabilite-europeen/
tags:
- Interopérabilité
- Institutions
- Europe
---

> [...] Trouble dans l’intéropérabilité en Europe. Le brouillon de la version 2 du European Interoperability Framework (EIF), un cadre d’interopérabilité européen dont le but est de faire la promotion des standards ouverts dans les administrations européennes, a subi quelques modifications capitales dans sa rédaction depuis sa première version. En ligne de mire, une définition alambiquée de l’intéropérabilité qui, dans cette mouture, ne repose plus uniquement sur des standards ouverts.
