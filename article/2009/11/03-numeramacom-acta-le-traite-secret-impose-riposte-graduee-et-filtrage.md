---
site: numerama.com
title: "ACTA : le traité secret impose riposte graduée et filtrage"
author: Guillaume Champeau
date: 2009-11-03
href: http://www.numerama.com/magazine/14410-acta-le-traite-secret-impose-riposte-graduee-et-filtrage.html
tags:
- HADOPI
- DADVSI
- DRM
---

> C'est cette semaine que doivent reprendre à Séoul les négociations sur l'Accord international de Commerce Anti-Contrefaçon (ACTA), initié par les Etats-Unis et élaboré dans la plus totale opacité. Les documents de négocation sont classés secret-défense, tandis que seul un petit nombre d'organisations et d'industriels triés sur le volet ont accès aux textes de référence. Les citoyens n'ont pas de droit de regard sur ce traité international qui aura pourtant d'importantes répercussions sur la libre circulation des informations et des oeuvres.
