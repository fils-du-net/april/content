---
site: ecrans.fr
title: "Tristan Nitot : « Le futur ne va pas être inventé dans des labos »"
author: Astrid Girardeau 
date: 2009-11-06
href: http://www.ecrans.fr/Tristan-Nitot-Le-futur-ne-va-pas,8491.html
tags:
- Le Logiciel Libre
- Internet
---

> Le 9 novembre 2004 sortait Firefox. Dans un marché monopolisé par l’Internet Explorer de Microsoft, le lancement, par la Mozilla Foundation, de ce navigateur web libre et innovant a suscité un engouement rapide. Aujourd’hui, il a 24 % du marché et plus de 300 millions d’utilisateurs. A l’occasion de ses 5 ans, Tristan Nitot, président de Mozilla Europe, et « dino » de l’Internet, revient sur cette histoire et livre sa vision personnelle du Net.
