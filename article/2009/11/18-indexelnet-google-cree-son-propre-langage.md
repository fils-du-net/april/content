---
site: indexel.net
title: "Google crée son propre langage"
author: Antoine Robin
date: 2009-11-18
href: http://www.indexel.net/actualites/google-cree-son-propre-langage-2982.html
tags:
- Le Logiciel Libre
- Interopérabilité
---

> Inspiré de la syntaxe de Python et de C++, Go est le nouveau langage de programmation de Google. Il est facile à écrire et rapide à compiler. Il pourrait entraîner l'adhésion des développeurs grâce à sa licence open source.
