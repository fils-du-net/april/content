---
site: dot.kde.org
title: "Matthias Ettrich Receives German Federal Cross of Merit"
author: Mirko Boehm
date: 2009-11-06
href: http://dot.kde.org/2009/11/06/matthias-ettrich-receives-german-federal-cross-merit
tags:
- Le Logiciel Libre
- Innovation
---

> Cet article en anglais présente la remise de la médaille du mérite au fondateur de KDE Matthias Ettrich :
