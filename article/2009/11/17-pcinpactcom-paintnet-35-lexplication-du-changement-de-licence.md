---
site: pcinpact.com
title: "Paint.NET 3.5 : l'explication du changement de licence"
author: Vincent Hermann
date: 2009-11-17
href: http://www.pcinpact.com/actu/news/54156-paint-net-changement-licence-mit-rick-brewster.htm
tags:
- Logiciels privateurs
- Licenses
---

> [...] Paint.NET n’a jamais été un logiciel libre dans le sens où la communauté pouvait participer à son développement et donc influer sur son avenir. Mais la disponibilité du code source a provoqué des dérives, comme celles que l’on a pu observer avec le logiciel VLC. De nombreuses personnes ont en effet recompilé des versions différentes qui, si elles n’étaient pas forcément contaminées par de multiples malwares, étaient parfois vendues ou présentées comme étant une nouvelle version plus récente.
