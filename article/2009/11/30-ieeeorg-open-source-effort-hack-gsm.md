---
site: ieee.org
title: "Open-Source Effort to Hack GSM"
author: John Blau
date: 2009-11-30
href: http://spectrum.ieee.org/telecom/wireless/open-source-effort-to-hack-gsm
tags:
- Le Logiciel Libre
- Informatique-deloyale
---

> Un groupe d'internautes veut prouver la faiblaisse des standarts des mobiles en dévoilant leur clé.
