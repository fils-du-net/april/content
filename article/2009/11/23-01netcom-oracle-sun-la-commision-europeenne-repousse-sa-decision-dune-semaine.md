---
site: 01net.com
title: "Oracle-Sun : la Commision européenne repousse sa décision d'une semaine"
author: Pierre Berlemont
date: 2009-11-23
href: http://pro.01net.com/editorial/508871/oracle-sun-la-commision-europeenne-repousse-sa-decision-dune-semaine/
tags:
- Entreprise
- Europe
---

> [...] Début novembre, on avait appris que l'Europe s'inquiétait de l'avenir de MySQL si jamais la base de données open source passait dans les mains d'Oracle. Ce à quoi celui-ci avait répondu que la Commission européenne ne comprenait rien aux SGBD et à l'open source. Va-t-elle mieux « comprendre » après ce laps de temps supplémentaire ?
