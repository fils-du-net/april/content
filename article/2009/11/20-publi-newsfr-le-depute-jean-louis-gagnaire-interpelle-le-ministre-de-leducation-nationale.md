---
site: "publi-news.fr"
title: "Le député Jean-Louis Gagnaire interpelle le ministre de l'éducation nationale"
author: La rédaction
date: 2009-11-20
href: http://www.publi-news.fr/data/20112009/20112009-094108.html
tags:
- Le Logiciel Libre
- April
- Institutions
- Éducation
---

> Le député Jean-Louis Gagnaire interpelle le ministre de l'éducation nationale « à propos de la plateforme SIALLE qui propose aux enseignants des informations sur les logiciels libres éducatifs. Il dénonce le conflit de licences actuel alors que la licence d'utilisation de SIALLE s'avère plus restrictive que les licences des logiciels libres proposés. Il demande au gouvernement d'apporter une solution respectant la volonté des auteurs de logiciels libres. ».
