---
site: pcinpact.com
title: "Un député s’interroge sur HADOPI, après l’avoir votée deux fois"
author: Marc Rees
date: 2009-11-24
href: http://www.pcinpact.com/actu/news/54271-franois-loos-vote-hadopi-securisation.htm
tags:
- HADOPI
---

> [...] C’est là toute la finesse d’Hadopi qui parvient à renverser la charge des ennuis : ce n’est plus à l’ayant droit de s’engluer dans une procédure fastidieuse, c’est à l’abonné de démontrer qu’il a sécurisé son accès. À défaut, contravention, amende, suspension. Belle affaire. Pour démontrer son innocence, l’abonné devra sans doute acheter un coûteux logiciel de sécurisation, ce fameux mouchard d’Hadopi dont le gouvernement a refusé l’interopérabilité ou la gratuité...
