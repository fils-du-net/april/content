---
site: pcinpact.com
title: "Marché des navigateurs : quand l'Europe favorise Windows"
author: Marc Rees 
date: 2009-11-30
href: http://www.pcinpact.com/actu/news/54364-vente-liee-microsoft-europe-internet.htm
tags:
- Interopérabilité
- Institutions
- Europe
---

> [...] Toutefois, dans l’inépuisable série du « faite ce que je dis, mais pas ce que je fais », l’Europe sait aussi cultiver la contradiction : pour profiter de ces flux, seuls les systèmes embarquant Windows sont acceptés, avec éventuellement un plug-in adapté à Firefox si l’on utilise ce navigateur. Conclusion : l’Europe, qui sait si bien critiquer les cas de vente liée de Windows avec le lecteur Windows Media Player ou avec Internet Explorer, participe justement à ce marché monocolore qu’elle réprouve.
