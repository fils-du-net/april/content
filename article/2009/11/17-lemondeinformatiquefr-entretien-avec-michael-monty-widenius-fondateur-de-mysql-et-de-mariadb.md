---
site: lemondeinformatique.fr
title: "Entretien avec Michael 'Monty' Widenius, Fondateur de MySQL et de MariaDB"
author: Olivier Rafal
date: 2009-11-17
href: http://www.lemondeinformatique.fr/entretiens/lire-michael-monty-widenius-fondateur-de-mysql-et-de-mariadb-137.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
---

> [...] Vous militez pour que l'UE oblige Oracle à se séparer de MySQL. Dans la mesure où MySQL est Open Source, quelle différence cela fait-il qu'elle passe de Sun à Oracle ?
> Sun avait tout intérêt à développer le produit. Au contraire, Oracle a toutes les raisons de ne pas le faire. Selon mes estimations, Oracle perd 1 Md$ de bénéfices chaque année rien que parce que MySQL existe. MySQL avait l'ambition de réduire le marché des bases de données de 15 Md$ à 1 Md$, or Oracle est le plus gros bénéficiaire de cette manne ! Pourquoi diable continueraient-ils de développer le produit ?
