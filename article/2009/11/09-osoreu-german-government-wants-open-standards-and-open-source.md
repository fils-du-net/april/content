---
site: osor.eu
title: "German government wants open standards and open source"
author: La rédaction
date: 2009-11-09
href: http://www.osor.eu/news/de-german-government-wants-open-standards-and-open-source
tags:
- Le Logiciel Libre
- Administration
- Interopérabilité
---

> Le nouveau gouvernement allemand entend soutenir les standards ouverts et les logiciels libres :
