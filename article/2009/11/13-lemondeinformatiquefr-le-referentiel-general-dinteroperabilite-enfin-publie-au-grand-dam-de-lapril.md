---
site: lemondeinformatique.fr
title: "Le Référentiel général d'interopérabilité enfin publié, au grand dam de l'April"
author: Bertrand Lemaire 
date: 2009-11-13
href: http://www.lemondeinformatique.fr/actualites/lire-le-referentiel-general-d-interoperabilite-enfin-publie-au-grand-dam-de-l-april-29422.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
- April
- RGI
---

> [...] Pour l'April, il s'agit d'un cadeau fait à Microsoft, qui « condamne [les données des administrations publiques] à demeurer prisonnières de formats propriétaires ». L'association de défense des logiciels libres estime que « loin de favoriser l'interopérabilité, cela engendrera des  discriminations entre les citoyens pour l'accès à l'administration électronique ».
