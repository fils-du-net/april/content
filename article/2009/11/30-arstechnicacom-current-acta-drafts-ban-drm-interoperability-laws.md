---
site: arstechnica.com
title: "Current ACTA drafts ban DRM interoperability laws"
author: Nate Anderson
date: 2009-11-30
href: http://arstechnica.com/tech-policy/news/2009/11/current-acta-drafts-bans-drm-interoperability-laws.ars
tags:
- Institutions
- DRM
- Neutralité du Net
---

> Le brouillon actuel des accorts ACTA interdit le contournement des DRM pour raison d'interoperabilité.
