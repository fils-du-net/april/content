---
site: "cio-online.com"
title: "La Bourse de Londres quitte une plateforme .Net aux performances insuffisantes pour de l'Open Source"
author: Jean Pierre Blettner
date: 2009-11-30
href: http://www.cio-online.com/actualites/lire-la-bourse-de-londres-quitte-une-plateforme-net-aux-performances-insuffisantes-pour-de-l-open-source-2570.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
---

> La plateforme de transactions électroniques de la Bourse de Londres accumule les interruptions dûes à des bugs. Elle va être remplacée par une plateforme Open Source fin 2010.
