---
site: zdnet.fr
title: "Microsoft contraint de publier Windows 7 USB/DVD Download Tool sous GPL"
author: La rédaction
date: 2009-11-16
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39710772,00.htm
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Microsoft a dû retirer de son site un logiciel gratuit destiné à l’installation de Windows 7 sur netbook après avoir été informé que celui-ci reprenait du code source d’un logiciel libre sous GPL v2. L’utilitaire de Microsoft sera remis en ligne cette semaine, mais désormais sous licence GPL.
