---
site: phoronix.com
title: "Samsung Sponsors The Development Of Enlightenment"
author: Michael Larabel 
date: 2009-11-17
href: http://www.phoronix.com/scan.php?page=news_item&px=NzcxNQ
tags:
- Le Logiciel Libre
- Entreprise
---

> Enlightenment se fera sponsoriser par un des 5 premiers constructeurs d'électronique. Probablement Samsung.
