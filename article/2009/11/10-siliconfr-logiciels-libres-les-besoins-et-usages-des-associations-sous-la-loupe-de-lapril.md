---
site: silicon.fr
title: "Logiciels libres : les besoins et usages des associations sous la loupe de l'April"
author: David Feugey
date: 2009-11-10
href: http://www.silicon.fr/fr/news/2009/11/10/logiciels_libres___les_besoins_et_usages_des_associations_sous_la_loupe_de_l_april
tags:
- Le Logiciel Libre
- April
- Associations
---

> Fin 2008, l'April (association nationale de promotion et de défense du logiciel libre) et la CPCA (conférence permanente des coordinations associatives) ont soumis le questionnaire « Associations, informatique et logiciels libres » à de nombreuses associations.
> Aujourd’hui, le résultat de ces travaux est rendu public et se trouve résumé dans ce rapport. « Cette synthèse permet de préciser les orientations du groupe de travail "Libre Association" de l'April et de mieux cerner les profils des structures associatives et leurs besoins. En effet, grâce à cette étude, le groupe de travail va commencer la deuxième phase du projet qui consistera, entre autre, à mieux répondre aux associations en les guidant vers des logiciels libres adaptés », déclare Laurent Costy, conseil bénévole sur les questions associatives pour l'April.
