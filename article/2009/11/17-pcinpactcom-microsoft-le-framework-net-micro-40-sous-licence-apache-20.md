---
site: pcinpact.com
title: "Microsoft : le Framework .NET Micro 4.0 sous licence Apache 2.0"
author: Vincent Hermann
date: 2009-11-17
href: http://www.pcinpact.com/actu/news/54141-framework-net-micro-microsoft-apache.htm
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] Maintenant, pourquoi Microsoft a-t-il choisi de rendre la plus grande partie du code de son Framework .NET Micro open source ? Il faut dire que le marché de l’embarqué et de la mobilité est nettement plus complexe pour l’éditeur de Redmond que sur les ordinateurs classiques. La position de Windows Mobile n’est clairement pas assurée, et la version 6.5 du système risque de ne pas améliorer les choses tant l’iPhone a du succès et Android commence à déployer son ascendant.
