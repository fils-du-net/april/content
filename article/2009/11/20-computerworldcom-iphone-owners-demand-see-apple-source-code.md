---
site: computerworld.com
title: "iPhone owners demand to see Apple source code"
author: Gregg Keizer
date: 2009-11-20
href: http://www.computerworld.com/s/article/9141222/iPhone_owners_demand_to_see_Apple_source_code
tags:
- Logiciels privateurs
- Vente liée
---

> Un juge fédéral des USA demande à Apple de montrer le code source de son iPhone suite à une plainte antitrust :
