---
site: lemondeinformatique.fr
title: "Oracle/Sun : Bruxelles craint une concurrence faussée sur les bases de données"
author: Maryse Gros
date: 2009-11-10
href: http://www.lemondeinformatique.fr/actualites/lire-oracle-sun-bruxelles-craint-une-concurrence-faussee-sur-les-bases-de-donnees-29402.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
---

> La Commission Européenne a communiqué hier, lundi 9 novembre, une première évaluation de ses objections concernant le rachat de Sun par Oracle. Dans un échange avec la SEC (Securities and exchange commission), l'autorité américaine de contrôle des marchés, Sun explique que cette évaluation préliminaire de Bruxelles se focalise sur le marché des bases de données évoquant les effets négatifs sur la concurrence qu'induirait un rapprochement de la base Open Source MySQL et de la base d'entreprise d'Oracle.
