---
site: zdnet.fr
title: "L'April s'élève à son tour contre la redéfinition de l'interopérabilité"
author: Christophe Auffray
date: 2009-11-30
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39711172,00.htm
tags:
- Interopérabilité
- Institutions
- Europe
---

> [..] L'association française de promotion et de défense du logiciel libre, l'April, vient elle aussi de se saisir de cette polémique, dénonçant « les lobbies du logiciel propriétaire, Microsoft et la BSA en tête », qui selon elle plaident « pour que "fermé" soit considéré comme "presque ouvert". »
> Selon l'April, le brouillon de la version 2 de l'EIF, outre le fait de retirer toute mention du logiciel libre, confond interopérabilité et compatibilité, et ce en ne conditionnant plus l'interopérabilité à l'utilisation de standards ouverts.
