---
site: zdnet.fr
title: "Le père d’Ubuntu, Canonical, contribue au développement de Chrome OS"
author: Christophe Auffray
date: 2009-11-23
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39710972,00.htm
tags:
- Le Logiciel Libre
- Entreprise
---

> Ubuntu et Chrome OS partagent des composants systèmes et Canonical contribue, dans le cadre d’un contrat, depuis juin au développement de l’OS de Google. Ce partenariat ne remet pas en cause l’avenir d’Ubuntu assure Canonical.
> [...] C'est Chris Kenyon, le vice président de la branche services OEM de Canonical, qui a révélé cette information le 19 novembre sur le blog officiel de l'entreprise. Ainsi, Canonical collabore directement au développement du système d'exploitation, et ce dans le cadre d'un contrat commercial signé avec Google (les modalités financières ne sont toutefois pas communiquées).
