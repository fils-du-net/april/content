---
site: Le Journal Du Net
title: "Supercalculateurs : le Jaguar a sorti ses griffes"
author: Dominique Filippone
date: 2009-11-18
href: http://www.journaldunet.com/solutions/systemes-reseaux/actualite/supercalculateurs-le-jaguar-a-sorti-ses-griffes.shtml
tags:
- Le Logiciel Libre
- Économie
---

> [...] Parmi les autres statistiques intéressantes de ce 34e classement Top500, on notera également la percée continue du système d'exploitation Linux. Alors que ce dernier équipait 87,8% des 500 plus grands supercalculateurs mondiaux en novembre 2008 et 88,6% en juin 2009, la part de Linux frise aujourd'hui la barre des 90% (89,2%).
