---
site: lesechos.fr
title: "L'UE s'apprêterait à bloquer le rachat de Sun Microsystems par Oracle"
author: La rédaction
date: 2009-11-04
href: http://www.lesechos.fr/info/hightec/afp_00198330-l-ue-s-appreterait-a-bloquer-le-rachat-de-sun-microsystems-par-oracle.htm
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
---

> La Commission européenne va engager des démarches en vue de bloquer l'acquisition de Sun Microsystems, le propriétaire du langage de programmation Java, par l'éditeur de logiciels professionnels Oracle, a affirmé mercredi le Financial Times.
> [...] En septembre la Commission avait indiqué que son enquête initiale soulevait "des doutes sérieux quant à la compatibilité (de cette acquisition) avec le marché unique, en raison de problèmes de concurrence sur le marché des bases de données".
