---
site: cio.com.au
title: "Google releases core development tools as open source"
author: Juan Carlos Perez
date: 2009-11-06
href: http://www.cio.com.au/article/325235/google_releases_core_development_tools_open_source
tags:
- Le Logiciel Libre
- Internet
---

> Cet article en anglais annonce que Google a décidé de publier plusieurs de ses applications sous licences libres :
