---
site: pcinpact.com
title: "Vente liée : MSI condamné pour ne pas avoir appliqué le CLUF de Microsoft"
author: Marc Rees
date: 2009-11-23
href: http://www.pcinpact.com/actu/news/54249-msi-vente-liee-cluf-licence.htm
tags:
- Logiciels privateurs
- Vente liée
---

> [...] Ensuite, le juge balaiera l’argument de MSI : « MSI ne prouve pas que l’ordinateur en cause serait effectivement proposé sous d’autres configurations et ce, de manière fréquente, non confidentielle, et facilement accessible au consommateur ». Une configuration qui aurait pu diminuer la pression de cette vente cadenassée… Non, ici « le consommateur n’a d’autre choix que d’acquérir le matériel dans la configuration proposée à la vente, et de prendre ensuite des dispositions pour l’installation des logiciels de son choix ».
