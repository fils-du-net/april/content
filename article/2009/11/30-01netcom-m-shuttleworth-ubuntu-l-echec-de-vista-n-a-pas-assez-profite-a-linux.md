---
site: 01net.com
title: "M. Shuttleworth (Ubuntu) : « L’échec de Vista n’a pas assez profité à Linux »"
author: Christophe Guillemin
date: 2009-11-30
href: http://www.01net.com/editorial/509308/m-shuttleworth-(ubuntu)-l-echec-de-vista-n-a-pas-assez-profite-a-linux/
tags:
- Le Logiciel Libre
- Économie
- Vente liée
---

> [...] Windows Vista n'a pas suscité une forte adhésion du public, et bon nombre d'utilisateurs ont cherché des alternatives. Pourquoi cela n'a-t-il pas plus profité à Linux ?
> Il est vrai que Linux n'a pas assez profité de l'échec de Vista. S'il avait été prêt à être utilisé par n'importe qui, même les plus novices, alors nous aurions bénéficié davantage de la situation. Nous continuons à travailler en ce sens.
> Ce qui est positif, c'est que nous faisons des progrès constants. Linux devient de plus en plus abordable et intéresse de plus en plus d'utilisateurs. Et la porte ouverte par Vista n'est pas encore fermée. Beaucoup d'utilisateurs se sont effectivement demandé s'il n'y avait pas d'alternative à Windows. Et l'effet de ce questionnement est encore palpable.
