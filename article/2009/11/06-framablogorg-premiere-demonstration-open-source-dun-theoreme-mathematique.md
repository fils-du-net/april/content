---
site: framablog.org
title: "Première démonstration « open source » d'un théorème mathématique"
author: aKa
date: 2009-11-06
href: http://www.framablog.org/index.php/post/2009/11/06/mathematiques-open-source
tags:
- Partage du savoir
- Licenses
---

> [...] Les mathématiques seraient donc « libres », les professeurs également (enfin surtout en France dans le secondaire avec Sésamath), mais quid des mathématiciens eux-même et de leurs pratiques ? Et si ils s’organisaient à la manière d’un projet de développement d’un logiciel libre pour chercher et éventuellement trouver ensemble des résultats importants ?
