---
site: emarrakech.info
title: "Ouverture au Liban d’un centre du logiciel libre dans la région arabe "
author: La rédaction
date: 2009-11-12
href: http://www.emarrakech.info/Ouverture-au-Liban-d-un-centre-du-logiciel-libre-dans-la-region-arabe_a28883.html
tags:
- Le Logiciel Libre
---

> Beyrouth - Sous le parrainage du ministère libanais de l’Education, le centre de support du logiciel libre dans la région arabe, baptisé Ma3bar, a été officiellement inauguré le 5 novembre 2009 à Beyrouth. Le nouveau centre est le fruit de la coopération entre l’UNESCO, le PNUD-ICTDAR et l’Université de Balamand. D’autres grandes institutions académiques de la région devraient s’associer au projet.
