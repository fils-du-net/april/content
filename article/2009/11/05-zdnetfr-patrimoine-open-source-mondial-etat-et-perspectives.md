---
site: zdnet.fr
title: "Patrimoine Open Source mondial: état et perspectives"
author: Chris DiBona
date: 2009-11-05
href: http://www.zdnet.fr/blogs/media-tech/patrimoine-open-source-mondial-etat-et-perspectives-39710545.htm
tags:
- Le Logiciel Libre
- Internet
- Philosophie GNU
---

> Chris DiBona, le responsable du programme Open Source chez Google, a livré les résultats d'une étude intéressante:
> le logiciel en Open Source sur Internet, c'est actuellement
> 2.5 milliards de lignes de code organisées en
> 30 millions de fichiers.
