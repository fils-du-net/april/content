---
site: romandie.com
title: "Breveter des mathématiques financières: la Cour suprême des USA réticente "
author: AFP 
date: 2009-11-09
href: http://www.romandie.com/infos/news2/091109214435.9uwdhnk6.asp
tags:
- Brevets logiciels
---

> WASHINGTON - La Cour suprême des Etats-Unis s'est révélée réticente lundi à l'idée d'autoriser le dépôt d'un brevet sur des formules de mathématiques financières, plusieurs juges estimant que cette forme d'invention était trop abstraite pour être protégée.
> L'affaire a été portée devant la plus haute juridiction du pays par deux mathématiciens, Bernard Bilski et Rand Warsaw, inventeurs d'une méthode pour se protéger contre le risque de fluctuation des prix d'une marchandise, comme par exemple une source d'énergie.
