---
site: itrmanager.com
title: "11e édition de Solutions Linux / Open Source 2010"
author: La rédaction
date: 2009-11-23
href: http://www.itrmanager.com/articles/97696/11e-edition-solutions-linux-open-source-2010.html
tags:
- Le Logiciel Libre
---

> [...] L'objectif du salon est d'offrir un panorama de l'activité du secteur pour apporter la réponse la plus complète et transverse à ses enjeux et préoccupations. Tout en conservant les fondamentaux intrinsèques au succès de l'événement depuis sa création, comme Le Village associatif, où Tarsus propose 65 espaces gratuits aux associations regroupant les communautés du logiciel libre. Une singularité qui permet de faire cohabiter l'ensemble des acteurs du mouvement.
