---
site: rue89.com
title: "Nespresso : un arrière-goût de Microsoft dans votre café"
author: François Krug
date: 2009-11-13
href: http://eco.rue89.com/2009/11/13/nespresso-un-arriere-gout-de-microsoft-dans-votre-cafe-125834
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Interopérabilité
- Vente liée
---

> Pour Denis Fages, l'affaire est l'occasion de défendre les systèmes « ouverts », les seuls vendus sur Chacunsoncafe. Il est partisans du standard ESE (« Easy Serving Espresso »), des dosettes utilisables sur des cafetières de marques différentes :
> « C'est le même combat que pour le logiciel libre, un gros qui va imposer un standard et obliger les gens à payer pour quelque chose qui existe ailleurs. Ce combat, c'est celui de Linux contre Microsoft et Apple. »
