---
site: itrmanager.com
title: "Classement du logiciel européen Truffle 100 "
author: La rédaction
date: 2009-11-03
href: http://www.itrmanager.com/articles/96962/classement-logiciel-europeen-truffle-100-br-france-derriere-allemagne-royaume-uni.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> [...] Tel un marronnier qui fleurit chaque année, le Truffle 100 ne peut pas modifier en profondeur la situation d'une année sur l'autre. Le constat est tel que la situation du logiciel européen est connue et fait montre d'une faiblesse patente par rapport à son concurrent américain. Beaucoup place des espoirs dans le logiciel libre pour renforcer la position du logiciel européen, mais cela prendra du temps. Il convient donc de regarder de manière plus détaillée pour voir si les choses évoluent dans la bonne direction.
