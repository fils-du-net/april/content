---
site: zdnet.fr
title: "Le logiciel libre Spip est-il français?"
author: Thierry Noisette
date: 2009-11-03
href: http://www.zdnet.fr/blogs/l-esprit-libre/le-logiciel-libre-spip-est-il-francais-39710488.htm
tags:
- Le Logiciel Libre
- Administration
---

> Spip, CMS libre utilisé par bien des sites, privés comme publics, et maintenant par celui dédié au "grand débat sur l'identité nationale" lancé par Eric Besson (dont le ministère a déjà un site sous Spip, immigration.gouv.fr), est-il un logiciel français? C'est la question qu'aborde dans un billet ce mardi Fil, un des créateurs de Spip.
> Point de départ de sa réflexion, la découverte que le site debatidentitenationale.fr est réalisé sous Spip - même si le système de gestion de contenu n'est pas mentionné dans le site; son agence web a fait montre de la même discrétion de violette que celle du nouveau web de Carla Bruni-Sarkozy.
