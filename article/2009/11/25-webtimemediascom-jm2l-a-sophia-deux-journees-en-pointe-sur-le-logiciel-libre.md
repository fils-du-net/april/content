---
site: webtimemedias.com
title: "JM2L à Sophia : deux journées en pointe sur le Logiciel Libre"
author: Jean-Pierre Largillet
date: 2009-11-25
href: http://www.webtimemedias.com/webtimemedias/wtm_article54129.fr.htm
tags:
- Le Logiciel Libre
- April
- Video
---

> [...] Un coup d'œil sur le programme des 4èmes Journées Méditerranéennes du Logiciel Libre à Polytech'Nice-Sophia, vendredi et samedi 27 et 28 novembre, et cela saute aux yeux : les JM2L ont pris une nouvelle dimension. Organisées par l'association Linux Azur avec Polytech'Nice, l'European Smalltalk User Group et OpenSides, elle s'étendent désormais sur deux journées. Mais surtout, le programme s'est singulièrement étoffé et diversifié. Au total, pas moins de 30 conférences allant du technique au communautaire.
