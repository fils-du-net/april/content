---
site: lejdd.fr
title: "Firefox à la merci de Google? "
author: Gaël Vaillant 
date: 2009-11-24
href: http://www.lejdd.fr/Medias/Internet/Actualite/Firefox-a-la-merci-de-Google-152977/
tags:
- Le Logiciel Libre
- Entreprise
- Internet
---

> Firefox, le navigateur de Mozilla, va bien, très bien même comparé à ses concurrents sur ordinateurs. Mais l'essentiel de ses revenus publicitaires sont issus d'un accord avec Google, qui a lancé son propre navigateur il y a plus d'un an.
> [...] Mais la belle histoire pourrait mal finir. Firefox doit avant tout son succès à Google. En effet, l'essentiel des revenus publicitaires du produit (qui truste les finances de la fondation) proviennent d'un accord avec la régie de Google, en vigueur jusqu'en 2011. Seulement, Google a lancé il y a plus d'un an, en septembre 2008, son propre navigateur: Chrome. Et nombreux sont ceux, au sein du géant de la Silicon Valley, à remettre en question l'accord conclu avec Mozilla. Pourquoi un concurrent profiterait-il des recettes publicitaires maison?
