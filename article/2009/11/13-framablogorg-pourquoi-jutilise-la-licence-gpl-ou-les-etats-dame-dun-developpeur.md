---
site: framablog.org
title: "Pourquoi j'utilise la licence GPL ou les états d'âme d'un développeur"
author: aKa
date: 2009-11-13
href: http://www.framablog.org/index.php/post/2009/11/13/pourquoi-j-utilise-la-licence-gpl
tags:
- Le Logiciel Libre
- Licenses
---

> [...] « Je veux que les gens apprécient le travail que j’ai fait et la valeur de ce que j’ai réalisé. Pas qu’ils passent en criant « pigeon » au volant de leurs voitures de luxe. »
