---
site: linuxjournal.com
title: "Nicaragua Builds An Innovative Agricultural Information System Using Open Source Software"
author: Johannes Wilm
date: 2009-11-12
href: http://www.linuxjournal.com/content/nicaragua-builds-innovative-agricultural-information-system-using-open-source-software
tags:
- Le Logiciel Libre
- Administration
---

> Le Nicaragua expérimente l'utilisation de logiciels libres dans l'agriculture :
