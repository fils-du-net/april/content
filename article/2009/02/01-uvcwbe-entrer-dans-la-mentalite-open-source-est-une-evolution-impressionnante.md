---
site: uvcw.be
title: "Entrer dans la mentalité open source est une évolution impressionnante"
author: Alain Depret
date: 2009-02-01
href: http://www.uvcw.be/articles/3,90,39,39,2847.htm
tags:
- Le Logiciel Libre
- Administration
---

> Vous vous êtes intéressés tout de suite au monde du logiciel libre?
> Non. Notre première réaction a été de nous adresser au monde propriétaire. Mais, notre ambition était trop onéreuse. Cela nous engageait, de plus, avec des coûts récurrents, sans pour cela obtenir une plus-value extérieure. Les solutions open source se sont vite imposées à nous, notamment Plone et ses applications. En 2004, ces solutions nous paraissaient déjà fort professionnelles. Comme les fournisseurs habituels ne maîtrisaient pas ces outils, nous avons contacté, par dépit, les personnes qui sont à l’origine de Plone.
