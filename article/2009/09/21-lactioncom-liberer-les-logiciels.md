---
site: laction.com
title: "Libérer les logiciels "
author: Isabelle Burgun
date: 2009-09-21
href: http://www.laction.com/article-378453-Liberer-les-logiciels.html
tags:
- Le Logiciel Libre
- Administration
---

> « L’avenir des logiciels est là », annonce même Louis Martin, chercheur au département d’informatique de l'UQAM et titulaire de la nouvelle Chaire de logiciel libre, finance sociale et solidaire dont le principal objectif est de concevoir une famille de logiciels libres dédiés au secteur financier alternatif, celui de l'épargne et des investissements sociaux et solidaires, plus souvent connu sous l'appellation « économie sociale ».
> Pour supporter ce projet, de grands acteurs du milieu de l’économie solidaire — tels le Fonds de développement de la CSN, Fondaction, le Crédit coopératif et l’assureur français MACIF — se sont réunis en une ONG : l’Association internationale de logiciel libre en finance sociale et solidaire.
