---
site: silicon.fr
title: "Sénateurs et députés valident la loi Hadopi2"
author: Christophe Lagane
date: 2009-09-22
href: http://www.silicon.fr/fr/news/2009/09/22/senateurs_et_deputes_valident_la_loi_hadopi2
tags:
- HADOPI
---

> Le projet de loi contre le téléchargement illégal, dit hadopi2, est définitivement adoptée par les deux chambres parlementaires. Dernière étape : le Conseil constitutionnel que l'opposition entend saisir.
> Après les sénateurs, hier, les députés ont adopté à leur tour aujourd'hui le projet de loi relatif à « protection pénale de la propriété littéraire et artistique sur Internet » dit Hadopi2. Ce vote de l'Assemblée valide le texte commun fixé la semaine dernière par la CMP (commission mixte paritaire).
