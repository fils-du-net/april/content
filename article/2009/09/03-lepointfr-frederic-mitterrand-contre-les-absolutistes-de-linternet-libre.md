---
site: lepoint.fr
title: "Frédéric Mitterrand contre les \"absolutistes de l'Internet libre\""
author: Guerric Poncet
date: 2009-09-03
href: http://www.lepoint.fr/actualites-technologie-internet/2009-09-03/creation-et-internet-hadopi-frederic-mitterrand-contre-les-absolutistes-de-l-internet-libre/1387/0/374116
tags:
- Internet
- Institutions
- Droit d'auteur
---

> Certains auraient pu le croire frileux à l'idée de se frotter à l'Hadopi. Il n'en est rien : Frédéric Mitterrand, ministre de la Culture, s'est lancé à corps perdu dans la bataille.
> [...] Peu importe que le dossier ait eu raison de l'ancienne ministre, le nouveau locataire de la rue de Valois a affiché une détermination sans faille face aux "absolutistes de l'Internet libre". Selon lui, Internet "ne doit pas être un espace où le droit se volatilise et devient virtuel" et "l'utopie n'est pas une alternative à l'Hadopi".
