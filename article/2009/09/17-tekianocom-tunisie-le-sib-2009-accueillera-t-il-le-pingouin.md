---
site: tekiano.com
title: "Tunisie : le SIB 2009 accueillera-t-il le pingouin ?"
author: Welid Naffati
date: 2009-09-17
href: http://www.tekiano.com/informatique/informatik-news/3-4-1077/tunisie-le-sib-2009-accueillera-t-il-le-pingouin-.html
tags:
- Le Logiciel Libre
- Le Logiciel Libre
- Administration
- Sensibilisation
---

> Une information est en train de circuler dans la communauté tunisienne des logiciels libres selon laquelle le ministère des technologies de la communication souhaiterait faire participer les groupes actifs dans le domaine des solutions logicielles Open source dans la prochaine édition du Salon international de l’Informatique et de la Bureautique (SIB 2009).
