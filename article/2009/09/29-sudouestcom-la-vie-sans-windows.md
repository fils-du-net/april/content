---
site: sudouest.com
title: "La vie sans Windows"
author: La rédaction
date: 2009-09-29
href: http://www.sudouest.com/bearn/actualite/pau/article/720726/mil/5161857.html
tags:
- Le Logiciel Libre
---

> Voilà dix ans que Paulla (Pau logiciel libre association) traîne son bâton de pèlerin, en prosélyte du logiciel libre, répétant aux profanes que la lumière ne vient pas forcément de Windows, afin de les convertir à l'univers Linux.
