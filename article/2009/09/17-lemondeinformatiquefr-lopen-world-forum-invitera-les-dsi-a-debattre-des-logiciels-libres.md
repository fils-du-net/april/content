---
site: lemondeinformatique.fr
title: "L'Open World Forum invitera les DSI à débattre des logiciels libres"
author: Olivier Rafal 
date: 2009-09-17
href: http://www.lemondeinformatique.fr/actualites/lire-l-open-world-forum-invitera-les-dsi-a-debattre-des-logiciels-libres-29147.html
tags:
- Le Logiciel Libre
- Le Logiciel Libre
- Entreprise
---

> « Même si les logiciels libres s'imposent de plus en plus dans les systèmes d'information, le sujet global reste confus pour les DSI. » Valérie Humery, elle-même DSI (de Car &amp; boat media), faisait ce constat ce matin lors de la présentation des grands thèmes de l'Open World Forum (OWF), qui se tiendra à Paris les 1er et 2 octobre prochains.
