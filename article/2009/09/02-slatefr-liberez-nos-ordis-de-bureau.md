---
site: slate.fr
title: "Libérez nos ordis de bureau!"
author: Farhad Manjoo, Nora Bouazzouni
date: 2009-09-02
href: http://www.slate.fr/story/9845/liberez-nos-ordis-de-bureau
tags:
- Le Logiciel Libre
- Internet
- Administration
---

> Lors d'un meeting public rassemblant des employés du ministère des Affaires Etrangères américain, Jim Finkle, un fonctionnaire, a posé la question suivante à Hillary Clinton: «Pourriez-vous s'il vous plaît nous laisser utiliser un navigateur Web qui s'appelle Firefox?». Le courageux fut immédiatement acclamé. Finkle expliqua alors que lorsqu'il travaillait pour la National Geospatial Intelligence Agency, tout le monde utilisait Firefox. «Donc je ne vois pas pourquoi on ne pourrait pas faire la même chose au ministère», dit-il. «C'est un logiciel bien plus sûr que d'autres.»
