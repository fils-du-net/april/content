---
site: journaldunet.com
title: "Il n'existe pas de liste noire sur quelque sujet que ce soit chez Microsoft"
author: Antoine Crochet-Damais
date: 2009-09-10
href: http://www.journaldunet.com/solutions/dsi/chat/alfonso-castro-il-n-existe-pas-de-liste-noire-sur-quelque-sujet-que-ce-soit-chez-microsoft.shtml
tags:
- Entreprise
- Logiciels privateurs
- Informatique-deloyale
---

> Quant au fait de savoir si Linux est une technologie concurrente, la réponse est oui. Mais cela n'est pas du tout en contradiction avec nos stratégies d'ouverture, au contraire.
> Les exemples de collaboration ou de travaux communs avec les communautés Linux ou autres ne manquent pas. Comme indiqué précédemment, nous avons publié du code source Linux afin d'en optimiser le fonctionnement sur nos environnements de virtualisation. Cette initiative a d'ailleurs été soulignée publiquement par Linus Torvald lui-même en estimant que Microsoft avait fait ce qu'il fallait.
