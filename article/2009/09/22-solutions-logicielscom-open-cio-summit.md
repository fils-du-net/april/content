---
site: "solutions-logiciels.com"
title: "Open CIO Summit"
author: Frédéric Mazué
date: 2009-09-22
href: http://www.solutions-logiciels.com/actualites.php?titre=Open-CIO-Summit&actu=5795
tags:
- Entreprise
- Économie
---

> SOA, mobilité, SAAS, Cloud,… les DSI sont aujourd'hui confrontés à une mutation rapide des systèmes d'information. Au coeur de ces révolutions : l'Open Source. Après s'être progressivement imposé via les infrastructures puis le middleware, les Logiciels Libres commencent aujourd'hui à atteindre les applications métiers. Avec un impact majeur : selon Forrester, l'Open Source devient dorénavant "la dorsale cachée de l'industrie du logiciel".
