---
site: neteco.com
title: "e-administration open source : le Brésil et la France font cause commune"
author: Matthieu Dailly
date: 2009-09-08
href: http://www.neteco.com/298486-administration-open-source-bresil-france-font-cause-commune.html
tags:
- Le Logiciel Libre
- Entreprise
- Administration
---

> La société Serpro (Serviço Federal de Processamento de Dados), un important service informatique du gouvernement fédéral brésilien, et Bull, un leader franco-européen des technologies de l'information, compte développer conjointement des technologies open source pour l'administration en ligne.
