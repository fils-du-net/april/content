---
site: Neteco.com
title: "Open Invention Network veut protéger Linux de Microsoft"
author: Ariane Beky
date: 2009-09-08
href: http://www.neteco.com/298298-open-invention-network-proteger-linux-microsoft.html
tags:
- Le Logiciel Libre
- Entreprise
- Économie
- Institutions
- Brevets logiciels
---

> Dans le but d'écarter le risque de procès qui pourrait décourager l'adoption de Linux, des rivaux de Microsoft seraient prêts à acquérir un ensemble de brevets anciennement détenus par la firme de Redmond, rapporte le Wall Street Journal.
> L'Open Invention Network, organisation américaine qui regroupe IBM, Red Hat, Novell, Sony, Nec et Philips, serait sur le point d'acquérir 22 brevets. Ces derniers auraient été vendus en début d'année par Microsoft à une autre organisation : l'Allied Security Trust.
