---
site: 01net.com
title: "Google fait breveter sa page d'accueil"
author: La rédaction
date: 2009-09-04
href: http://www.01net.com/editorial/505711/google-fait-breveter-sa-page-daccueil/
tags:
- Brevets logiciels
---

> [...] Le US Patent and Trademark Office, le bureau américain qui accorde brevets et marques, vient de délivrer ce 1er septembre 2009 à Google un certificat pour la « conception ornementale de l'interface utilisateur graphique d'un écran d'un terminal de communication ». Le brevet consiste en cette simple image de la page d'accueil de Google telle qu'elle pouvait apparaître dès 2004, lorsque la société a déposé sa demande.
