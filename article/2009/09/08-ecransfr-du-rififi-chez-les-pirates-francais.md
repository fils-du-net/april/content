---
site: ecrans.fr
title: "Du rififi chez les pirates français"
author: Camille Gévaudan 
date: 2009-09-08
href: http://www.ecrans.fr/Du-rififi-chez-les-pirates,7972.html
tags:
- Le Logiciel Libre
- Associations
---

> Début de l'histoire : 2006. La première association portant le nom de « Parti pirate » (PP) est créée en réaction au projet de loi DADVSI. Un désaccord sur l'inclusion de la promotion du logiciel libre dans le programme a été l'une des causes de sa rapide scission en deux - le Hard reset, comme elle a été appelée dans leur jargon geek. Les séparatistes qui n’approuvaient pas le militantisme libriste et voulaient se concentrer sur « la lutte contre les processus liberticides » ont pris le nom de Parti pirate français canal historique (PPFCH)
