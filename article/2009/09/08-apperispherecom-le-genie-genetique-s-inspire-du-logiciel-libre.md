---
site: apperisphere.com
title: "Le génie génétique s’inspire du logiciel libre"
author: Julien Appert
date: 2009-09-08
href: http://apperisphere.com/le-genie-genetique-s-inspire-du-logiciel-libre.html
tags:
- Le Logiciel Libre
- Licenses
- Philosophie GNU
---

> D’ailleurs, la philosophie de cette nouvelle activité s’inspire directement du monde du logiciel libre : la communauté des biohackeurs met en commun ses découvertes et les protègent avec des licences libres, ce qui permet à quiconque de les utiliser à condition de publier son propre travail sous la même licence.
