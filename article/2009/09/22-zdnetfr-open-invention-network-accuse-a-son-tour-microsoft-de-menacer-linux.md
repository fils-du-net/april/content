---
site: zdnet.fr
title: "Open Invention Network accuse à son tour Microsoft de menacer Linux"
author: Christophe Auffray
date: 2009-09-22
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39707520,00.htm
tags:
- Logiciels privateurs
- Brevets logiciels
---

> Juridique - Lors de l’événement LinuxCon, le directeur de l’OIN a accusé Microsoft d’avoir essayé de vendre récemment des brevets à des sociétés connues pour être procédurières et susceptibles d'attaquer Linux.
> La Fondation Linux et l'OIN, deux associations de défense de Linux, ne digèrent définitivement pas la récente vente de 22 brevets (liés à Linux) par Microsoft. Après Jim Zemlin, le directeur de la Fondation Linux, c'est au tour de Keith Bergelt de l'Open Invention Network (OIN) d'accuser Microsoft de menacer Linux en recourant comme arme à la propriété intellectuelle.
