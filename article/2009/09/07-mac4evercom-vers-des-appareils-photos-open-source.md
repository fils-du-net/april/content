---
site: mac4ever.com
title: "Vers des appareils photos Open Source ?"
author: arnaud
date: 2009-09-07
href: http://www.mac4ever.com/news/47445/vers_des_appareils_photos_open_source/
tags:
- Le Logiciel Libre
- Standards
---

> Intéressant projet que celui conduit à l'université de Stanford : une projet d'appareil photo Open Source, susceptible de recevoir des logiciels tiers. L'idée est simple : les appareils photos de constructeur embarquent des logiciels propriétaires et il n'est pas possible, pour des programmeurs tiers, de développer pour ces plate-formes.
