---
site: programmez.com
title: "Firefox interdit par l'administration wallonne"
author: Frédéric Mazué
date: 2009-09-23
href: http://www.programmez.com/actualites.php?titre_actu=Firefox-interdit-par-l-administration-wallonne&id_actu=5801
tags:
- Internet
- Administration
---

> On croit toujours avoir tout vu, mais c'est une erreur. Pour preuve cette décision pour le moins surprenante de l'administration wallonne: l'interdiction du navigateur Firefox dans ses services. Cette interdiction est justifiée ainsi dans une note d'information: "Une faille de sécurité a été détectée pour l'application de navigation internet Mozilla Firefox.
