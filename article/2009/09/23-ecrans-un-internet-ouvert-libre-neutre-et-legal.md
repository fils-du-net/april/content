---
site: ecrans.fr
title: "Un Internet ouvert, libre, neutre,... et légal"
author: Astrid Girardeau
date: 2009-09-23
href: http://www.ecrans.fr/Un-Internet-ouvert-libre-neutre-et,8173.html
tags:
- Internet
- Neutralité du Net
---

> « Aujourd’hui, on ne peut pas imaginer nos vies sans Internet, pas davantage qu’on ne peut l’imaginer sans eau courante ou sans ampoule électrique. (…) C’est pourquoi le Congrès et le Président ont chargé la FCC de développer un plan national afin d’assurer à chaque américain l’accès à un réseau ouvert et résistant, a déclaré lundi Julius Genachowski, président de la FCC, le régulateur américain des communications.
