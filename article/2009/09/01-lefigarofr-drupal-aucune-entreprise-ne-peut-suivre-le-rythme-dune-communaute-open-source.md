---
site: lefigaro.fr
title: "Drupal: « Aucune entreprise ne peut suivre le rythme d'une communauté open-source »"
author: Laurent Suply
date: 2009-09-01
href: http://blog.lefigaro.fr/hightech/2009/09/drupal-aucune-entreprise-ne-pe.html
tags:
- Le Logiciel Libre
- Entreprise
---

> Faire de Drupal un projet libre et open-source était-il pour vous une évidence, ou avez-vous pensé à en faire un logiciel propriétaire ?
> J'avais déjà vu ce système fonctionner pour d'autres projets comme Linux, mais pour être honnête, c'était surtout une décision pratique à l'époque. Je ne pouvais tout simplement plus tenir le rythme seul... Cette décision a permis à d'autres de s'impliquer et d'avancer plus vite. Les gens qui s'impliquent dans Drupal le font, la plupart du temps, parce qu'ils veulent participer à l'invention du futur du web. Encore une chose impossible avec un logiciel propriétaire.
