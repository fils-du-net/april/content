---
site: zdnet.fr
title: "L'Open Source est bien implanté dans les SI des entreprises françaises"
author: Christophe Auffray
date: 2009-09-17
href: http://www.zdnet.fr/galerie-image/0,50018840,39707056,00.htm
tags:
- Le Logiciel Libre
- Entreprise
---

> Selon l'édition 2009 de l'étude de Markess consacrée à l'Open Source en France, le secteur pèse désormais (services compris) pas moins de 2,1 milliards d’euros. Et pour la grande majorité des entreprises interrogées (160 issues des secteurs public et privé), le recours au logiciel libre est un cap déjà franchi, avec un taux d’utilisation de 92% en 2009.
