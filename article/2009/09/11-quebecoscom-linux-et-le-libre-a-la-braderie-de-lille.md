---
site: quebecos.com
title: "Linux et le libre à la braderie de Lille"
author: petitbob
date: 2009-09-11
href: http://www.quebecos.com/modules/news2/article.php?storyid=251
tags:
- Le Logiciel Libre
- April
---

> A côté de Chtinux, se trouvait le stand de l'association APRIL avec des documents, auto-collants et pins. Nul besoin de présenter APRIL et son action en faveur du Logiciel Libre et sa lutte contre la vente liée ou la loi HADOPI.
