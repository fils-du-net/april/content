---
site: "generation-nt.com"
title: "Microsoft : des arguments commerciaux anti-Linux"
author: Jérôme G.
date: 2009-09-08
href: http://www.generation-nt.com/microsoft-best-buy-windows-linux-actualite-864311.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Sensibilisation
- Vente liée
- Désinformation
---

> Sous couvert d'anonymat, un employé de la chaîne de magasins Best Buy met en ligne un diaporama de formation attribué à Microsoft où Linux est largement attaqué. Des arguments de vente en faveur de Windows 7.
