---
site: rue89.com
title: "Attention quand vous installez Firefox, demandez l'original"
author: Hugues Serraf
date: 2009-09-24
href: http://www.rue89.com/tribune-vaticinateur/2009/09/24/attention-quand-vous-installez-firefox-demandez-loriginal
tags:
- Désinformation
---

> Tristan Nitot, le président de Mozilla Europe, la fondation sans but lucratif qui diffuse le navigateur, déplore :
> C'est comme ça, on n'y peut pas grand chose. Et le Firefox que vous téléchargez sur des sites autres que les nôtres peut tout à fait avoir été transformé pour des raisons que nous ignorons, ce qui est légal. »
