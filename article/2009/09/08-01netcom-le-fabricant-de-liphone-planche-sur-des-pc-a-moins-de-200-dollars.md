---
site: 01net.com
title: "Le fabricant de l'iPhone planche sur des PC à moins de 200 dollars"
author: La rédaction
date: 2009-09-08
href: http://www.01net.com/editorial/505760/le-fabricant-de-liphone-planche-sur-des-pc-a-moins-de-200-dollars/
tags:
- Le Logiciel Libre
- Vente liée
---

> Selon Young Liu, les nouveaux PC à bas coût devraient fonctionner avec le système d'exploitation libre et gratuit Linux. « Il n'était pas possible de fabriquer un PC à moins de 200 dollars en utilisant les puces d'Intel et Windows [de Microsoft, NDLR] », a expliqué Liu.
> Foxconn pourrait avoir les moyens de ses ambitions. Premier sous-traitant mondial en électronique, il est aussi le fabricant de l'iPhone d'Apple et de la Wii de Nintendo.
