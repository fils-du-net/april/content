---
site: zdnet.fr
title: "Bière, tricot ou voiture open source, le Libre ce n'est pas que du logiciel"
author: Thierry Noisette
date: 2009-09-02
href: http://www.zdnet.fr/blogs/l-esprit-libre/biere-tricot-ou-voiture-open-source-le-libre-ce-n-est-pas-que-du-logiciel-39705681.htm
tags:
- Le Logiciel Libre
---

> En cette fin d'été bien chaud, les libristes amateurs de boissons fraîches auront une pensée émue pour la bière open source, la Free Beer (dont le nom serait un clin d'œil à la formule de Richard Stallman, "free as free speech, not free beer") ou "Vores Øl" à l'origine ("notre bière"), due à la créativité des artistes militants du collectif Superflex et d'étudiants de Copenhague en juin 2005: recette libre sous licence Creative Commons paternité-partage à l'identique, ingrédients et processus de fabrication expliqués pas à pas.
