---
site: journalducameroun.com
title: "Cameroun: Le logiciel libre fêté à Yaoundé"
author: Mohamadou Houmfa
date: 2009-09-21
href: http://www.journalducameroun.com/article.php?aid=2797
tags:
- Le Logiciel Libre
- Administration
---

> La journée mondiale du logiciel libre a été célébrée dans une centaine de pays samedi 19 Septembre dernier. A Yaoundé, c’est l’organisation non gouvernementale (ONG), Promotion des Technologies Garantes de l’environnement et de la Qualité de Vie (PROTEGE QV) qui a animé la célébration au British Council.
