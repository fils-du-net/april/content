---
site: 01net.com
title: "Une entreprise condamnée pour violation de licence libre"
author: Arnaud Devillard
date: 2009-09-23
href: http://www.01net.com/editorial/506522/une-entreprise-condamnee-pour-violation-de-licence-libre/
tags:
- Le Logiciel Libre
- Entreprise
- Philosophie GNU
---

> Des licences libres au cœur d'un procès pour violation de droit d'auteur, ce n'est pas si courant. La cour d'appel de Paris a pourtant eu à traiter un dossier de ce genre et a condamné le 16 septembre 2009 une société spécialisée dans les outils multimédias pour la formation. Celle-ci n'avait pas respecté les termes de la licence GNU GPL.
> [...] « Edu4 a fait trois erreurs, détaille Loïc Dachary, porte-parole français de la Free Software Foundation, consultée lors du procès en tant que partie sachante. Ils n'ont pas fourni les codes sources modifiés comme le demande la licence GNU/GPL. Ils ont retiré le texte qui normalement informe des droits sur la licence. Enfin, ils ont prétendu que c'étaient eux qui avaient conçu le logiciel. »
