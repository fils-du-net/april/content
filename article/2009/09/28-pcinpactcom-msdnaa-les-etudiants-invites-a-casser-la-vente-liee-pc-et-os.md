---
site: pcinpact.com
title: "MSDNAA : les étudiants invités à casser la vente liée PC et OS"
author: Marc Rees
date: 2009-09-28
href: http://www.pcinpact.com/actu/news/53235-aful-vente-liee-racketiciel-msdnaa.htm
tags:
- Logiciels privateurs
- Vente liée
---

> [...] Pour ses organisateurs, qu'on soit étudiant fan de Microsoft ou de logiciel libre, « vous et nous sommes victimes du même abus : nous sommes tous forcés d'acheter des logiciels que nous ne voulons pas. Vous, parce que vous pouvez les obtenir via le programme MSDNAA. Nous, simplement parce que nous utilisons d'autres logiciels. C'est pourquoi cette campagne est à la fois pour vous et pour nous ».
