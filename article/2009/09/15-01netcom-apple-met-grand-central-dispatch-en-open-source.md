---
site: 01net.com
title: "Apple met Grand Central Dispatch en open source"
author: Jonathan Charton
date: 2009-09-15
href: http://pro.01net.com/editorial/506043/apple-met-grand-central-dispatch-en-open-source/
tags:
- Le Logiciel Libre
- Interopérabilité
---

> Pour rappel, GCD est une technologie permettant de gérer la répartition des fils d'instruction (threads) lorsqu'un programme est en cours dans une architecture multicœur. Elle présente donc un intérêt de premier ordre pour les développeurs, qui peuvent se concentrer sur leur programme sans avoir à se préoccuper de la gestion de ces fils d'instruction.
> [...] L'une des motivations principales d'Apple est d'adapter GCD à la plus large frange du monde Unix et plus particulièrement Linux. Microsoft n'ayant que plus ou moins d'intérêt pour cette technologie, Apple sait qu'il y a là des bénéfices évidents à cibler d'autres plates-formes.
