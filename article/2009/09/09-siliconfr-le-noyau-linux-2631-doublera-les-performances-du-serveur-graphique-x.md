---
site: Silicon.fr
title: "Le noyau Linux 2.6.31 doublera les performances du serveur graphique X"
author: David Feugey
date: 2009-09-09
href: http://www.silicon.fr/fr/news/2009/09/09/le_noyau_linux_2_6_31_doublera_les_performances_du_serveur_graphique_x
tags:
- Le Logiciel Libre
- Innovation
---

> Le futur noyau Linux 2.6.31 se concentrera sur les performances, en particulier avec les machines desktop pourvues de peu de mémoire vive.
> [...]
> L’essentiel du travail est cependant centré sur les performances. Ainsi, la gestion de la mémoire virtuelle sera en net progrès. [...] D’autres technologies seront également présentes : les compteurs de performance intégrés à certains processeurs modernes (x86, PowerPC, S390, etc.) pourront être exploités. Ils permettront aux développeurs de logiciels d’atteindre un niveau d’optimisation inaccessible jusqu’alors sous Linux.
