---
site: cxp.fr
title: "L'irrésistible ascension du Floss"
author: Claire Leroy
date: 2009-09-29
href: http://www.cxp.fr/flash-cxp/chronique-irresistible-ascension-floss_863
tags:
- Le Logiciel Libre
- Entreprise
- Administration
- Informatique en nuage
---

> [...] Une fois n'est pas coutume. La France apparaît particulièrement motrice sur le chantier du libre : elle serait numéro 1 mondial en termes d'activités dans ce domaine (Etude Red Hat et Georgia Institute of Technology) et numéro 1 en Europe et devant l'Amérique du nord en termes d'utilisation des logiciels libres (Source : Pierre Audouin Consultant).
