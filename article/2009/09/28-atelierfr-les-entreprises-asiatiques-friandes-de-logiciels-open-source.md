---
site: atelier.fr
title: "Les entreprises asiatiques friandes de logiciels open source "
author: L'Atelier
date: 2009-09-28
href: http://www.atelier.fr/veille-internationale/10/28092009/logiciel-open-source-asie-indonesie-marche-commerce-entreprises-solutions-gestion--38762-.html
tags:
- Le Logiciel Libre
- Entreprise
---

> En Asie du Sud-est, les logiciels libres sont de plus en plus considérés comme une solution fiable pour réduire ses frais, révèle IDC dans son étude Open source software adoption. Cette tendance est notamment accentuée en Indonésie, où plus d’un tiers des sociétés interrogées prévoit de déployer des applications open source de gestion de relation clients (CRM) dans les dix-huit mois à venir.
