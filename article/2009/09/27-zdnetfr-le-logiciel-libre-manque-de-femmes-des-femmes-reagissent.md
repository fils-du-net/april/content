---
site: zdnet.fr
title: "Le logiciel libre manque de femmes, des femmes réagissent"
author: Thierry Noisette
date: 2009-09-27
href: http://www.zdnet.fr/blogs/l-esprit-libre/le-logiciel-libre-manque-de-femmes-des-femmes-reagissent-39707954.htm
tags:
- Le Logiciel Libre
- Sensibilisation
---

> Les chiffres sont peu flatteurs pour le Libre: les femmes compteraient pour 28% des développeurs de logiciels propriétaires, et... 1,5 à 2% dans les logiciels libres [...]
> Objectif de la réunion «Women in Free Software», discuter sur les moyens d'améliorer la participation féminine dans les communautés de développeurs et de militants du logiciel libre.
