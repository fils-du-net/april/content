---
site: zdnet.fr
title: "Firefox réclamé à Hillary Clinton, Drupal à Paris, cinq millions de fichiers dans Wikimedia"
author: Thierry Noisette
date: 2009-09-06
href: http://www.zdnet.fr/blogs/l-esprit-libre/firefox-reclame-a-hillary-clinton-drupal-a-paris-cinq-millions-de-fichiers-dans-wikimedia-39705929.htm
tags:
- Le Logiciel Libre
- Administration
- Partage du savoir
---

> Les fonctionnaires du département d'Etat (Department of State, ou DoS), le ministère des Affaires étrangères américain, ont demandé à la  secrétaire d'État, Hillary Clinton, de pouvoir utiliser Firefox. C'est Slate qui rapporte cet échange, tenu le 10 juillet dernier au DoS:
>  «Jim Finkle, un fonctionnaire, a posé la question suivante à Hillary Clinton: "Pourriez-vous s'il vous plaît nous laisser utiliser un navigateur Web qui s'appelle Firefox?" Le courageux fut immédiatement acclamé. Finkle expliqua alors que lorsqu'il travaillait pour la National Geospatial Intelligence Agency, tout le monde utilisait Firefox. "Donc je ne vois pas pourquoi on ne pourrait pas faire la même chose au ministère", dit-il. "C'est un logiciel bien plus sûr que d'autres." (...)
