---
site: lefigaro.fr
title: "L'Assemblée adopte le projet de loi Hadopi 2"
author: Samuel Laurent
date: 2009-09-15
href: http://www.lefigaro.fr/politique/2009/09/15/01002-20090915ARTFIG00468-hadopi-revient-a-l-assemblee-.php
tags:
- Logiciels privateurs
- HADOPI
---

> [...] Par exemple, il sera conseillé aux internautes d'installer sur leur ordinateur un «mouchard», logiciel capable, en théorie, de détecter si l'utilisateur télécharge illégalement. Or, ce logiciel, qui n'existe pas encore, a fait l'objet de nombre de controverses. Contrairement à ce que voulaient l'opposition, il ne sera ni gratuit, ni interopérable, c'est à dire qu'il sera conçu pour PC sous Windows, mais pas nécessairement adapté aux systèmes d'exploitation Mac OS ou Linux.
