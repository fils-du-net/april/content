---
site: 01net.com
title: "D'après Linus, le noyau Linux est « bouffi et énorme »"
author: Renaud Bonnet
date: 2009-09-22
href: http://pro.01net.com/editorial/506405/dapres-linus-le-noyau-linux-est-bouffi-et-enorme/
tags:
- Le Logiciel Libre
- Innovation
---

> Connu pour son franc-parler, Linus Torvalds, le créateur du noyau Linux, constate que ce dernier prend de l'embonpoint, et qu'aucune solution miracle ne se présente pour remédier à cet état de fait.
