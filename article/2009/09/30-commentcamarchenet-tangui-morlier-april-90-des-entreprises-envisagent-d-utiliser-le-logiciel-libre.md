---
site: commentcamarche.net
title: "Tangui Morlier, April : « 90% des entreprises envisagent d’utiliser le logiciel libre »"
author: CommentCaMarche
date: 2009-09-30
href: http://www.commentcamarche.net/actualites/tangui-morlier-april-90-des-entreprises-envisagent-d-utiliser-le-logiciel-libre-5849764-actualite.php3
tags:
- Le Logiciel Libre
- Entreprise
---

> D’une manière générale, déployer un logiciel dans une entreprise, qu'il soit libre ou propriétaire, ça a toujours un coût. Il faut toujours prendre cela en compte lorsqu'on calcule son retour sur investissement. Le logiciel libre n’est pas gratuit, mais il coûte moins d’argent. Vous ne payez pas directement le logiciel, en revanche, vous aurez toujours des coûts d’installation et de maintenance, que ce soit en interne si l'entreprise dispose d’un service informatique ou en externe via un prestataire de services.
