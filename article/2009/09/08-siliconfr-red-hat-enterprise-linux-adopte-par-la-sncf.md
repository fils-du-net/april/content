---
site: Silicon.fr
title: "Red Hat Enterprise Linux adopté par la SNCF"
author: David Feugey
date: 2009-09-08
href: http://www.silicon.fr/fr/news/2009/09/08/red_hat_enterprise_linux_adopte_par_la_sncf
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Économie
---

> Red Hat Enterprise Linux et JBoss seront utilisés dans le nouveau système de réservation et de paiement des billets régionaux de la SNCF.
> [...]
> Actuellement déployée en phase pilote en Bretagne et dans le Centre, cette solution peut supporter près de 30 000 connexions simultanées.
