---
site: silicon.fr
title: "Microsoft lance sa propre fondation open source"
author: David Feugey 
date: 2009-09-11
href: http://www.silicon.fr/fr/news/2009/09/11/microsoft_lance_sa_propre_fondation_open_source
tags:
- Logiciels privateurs
- Interopérabilité
---

> Après avoir proposé de multiples projets open source, ainsi qu’une plate-forme d’hébergement destinée aux logiciels libres (CodePlex), Microsoft souhaite aller plus loin. La fondation CodePlex fait ainsi son apparition. Elle est financée directement par l'éditeur de Redmond.
> Le président intérimaire de cette fondation est Sam Ramji, directeur senior platform strategy chez Microsoft, et grand promoteur des logiciels libres.
