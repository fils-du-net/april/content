---
site: lemagit.fr
title: "Qualité logicielle : l'Open Source dit merci aux grands noms de l'industrie "
author: Cyrille Chausson
date: 2009-09-24
href: http://www.lemagit.fr/article/developpement-developpeurs-qualite-open-source/4353/1/qualite-logicielle-open-source-dit-merci-aux-grands-noms-industrie/
tags:
- Le Logiciel Libre
- Entreprise
---

> Dans son rapport annuel, la société spécialisée dans le développement Coverity souligne l'amélioration de la qualité du code des logiciels Open Source. L'Open Source, qui connaît une mutation dans son modèle, devient de plus en plus sécurisé grâce notamment à la contribution des acteurs traditionnels, qui s'arment de développeurs dédiés.
