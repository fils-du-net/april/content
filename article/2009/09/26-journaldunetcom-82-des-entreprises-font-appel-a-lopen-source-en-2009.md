---
site: journaldunet.com
title: "82% des entreprises font appel à l'Open Source en 2009"
author: Dominique Filippone
date: 2009-09-26
href: http://www.journaldunet.com/solutions/dsi/actualite/82-des-entreprises-font-appel-a-l-open-source-en-2009.shtml
tags:
- Le Logiciel Libre
- Entreprise
- Administration
---

> En 2009, alors que 82% des entreprises annoncent avoir fait appel à un moins une application Open Source, ce taux d'adoption monte à 96% en ce qui concerne les administrations. Pour ces dernières, le poids des dépenses consacrées à l'Open Source dans leur budget informatique global atteint d'ailleurs les 14%.
