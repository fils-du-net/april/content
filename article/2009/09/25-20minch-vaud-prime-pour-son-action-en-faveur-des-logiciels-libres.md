---
site: 20min.ch
title: "Vaud primé pour son action en faveur des logiciels libres"
author: ats
date: 2009-09-25
href: http://www.20min.ch/ro/news/vaud/story/Vaud-prime-pour-son-action-en-faveur-des-logiciels-libres-14287596
tags:
- Le Logiciel Libre
- Administration
---

> Le canton de Vaud est distingué pour son action en faveur des logiciels libres et open source.
> L'association /ch/open a décerné un prix au conseiller d'Etat François Marthaler, grand défenseur de cette politique.
