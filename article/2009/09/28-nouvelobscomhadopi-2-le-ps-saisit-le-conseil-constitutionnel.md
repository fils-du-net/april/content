---
site: nouvelobs.com
title: "[Nouvelobs.com]Hadopi 2 : le PS saisit le Conseil constitutionnel"
author: Jérôme Hourdeaux
date: 2009-09-28
href: http://tempsreel.nouvelobs.com/actualites/vu_sur_le_web/20090928.OBS2866/hadopi_2__le_ps_saisit_le_conseil_constitutionnel.html
tags:
- HADOPI
---

> [...] La saisine s’en prend également au fait que des internautes pourront être sanctionnés pour "négligence caractérisée" si leur adresse IP a été utilisée pour téléchargée illégalement. Au mois de juin, les Sages avaient en effet estimé que "les atteintes à la liberté d’expression doivent nécessaires, adaptées et proportionnées à l'objectif poursuivi".
