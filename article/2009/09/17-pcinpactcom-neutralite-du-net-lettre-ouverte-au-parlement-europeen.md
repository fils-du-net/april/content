---
site: pcinpact.com
title: "Neutralité du Net : lettre ouverte au Parlement européen"
author: Nil Sanyas
date: 2009-09-17
href: http://www.pcinpact.com/actu/news/53102-neutralite-net-lettre-ouverte-europe.htm
tags:
- Neutralité du Net
---

> De très nombreuses associations, dont l'April, la Quadrature du Net, Framasoft, et l'UFC-Que Choisir, ont signé une lettre envoyée hier au Parlement européen, en rapport avec le fameux Paquet Télécom.
> En jeu, la Neutralité du Net, qui consiste à ce que tous les protocoles et même les sites soient égaux. Votre FAI n'a pas à avantager Google en défaveur de Bing, ou vice versa, par exemple. Il n'a pas non plus à brider les protocoles exploités par le P2P ou la messagerie instantanée. La Neutralité du Net vise aussi à ne pas espionner les contenus utilisés par les Internautes.
