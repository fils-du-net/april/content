---
site: zdnet.fr
title: "Trois projets à dominante open source labellisés par System@tic financés dans l'appel \"Web innovant\""
author: Thierry Noisette
date: 2009-09-15
href: http://www.zdnet.fr/blogs/l-esprit-libre/trois-projets-a-dominante-open-source-labellises-par-system-tic-finances-dans-l-appel-web-innovant-39706772.htm
tags:
- Le Logiciel Libre
- Économie
- Informatique en nuage
---

> Fin mai, la secrétaire d'Etat à l'économie numérique, Nathalie Kosciusko-Morizet, avait lancé un appel à projet "Web innovant" dans le cadre du plan de relance.
> Le pôle de compétitivité System@tic Paris Région (groupe thématique logiciel libre) vient d'annoncer que trois projets de R&amp;D collaborative à dominante open source labellisés par System@tic ont été retenus pour financement dans le cadre de cet appel à projet.
