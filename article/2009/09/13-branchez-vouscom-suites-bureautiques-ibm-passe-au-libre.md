---
site: "branchez-vous.com"
title: "Suites bureautiques: IBM passe au libre"
author: Gabriel Rodrigue
date: 2009-09-13
href: http://techno.branchez-vous.com/actualite/2009/09/suites_bureautiques_ibm_passe.html
tags:
- Le Logiciel Libre
- Entreprise
---

> Alors que la plupart des entreprises qui optent pour de tels changements prévoient une durée d'adaptation relativement longue, IBM souhaite que les employés concernés changent leurs habitudes en dix jours. Ils pourront toutefois utiliser Microsoft Office dans certains cas précis, à condition d'obtenir l'autorisation des supérieurs.
> Selon l'attaché de presse d'IBM, le coût des licences de Microsoft Office n'a pas eu un poids très important dans la décision; IBM souhaite seulement faire part de son appréciation des standards ouverts.
