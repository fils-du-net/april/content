---
site: itrmanager.com
title: "Trois questions que se posent les DSI à propos des logiciels libres "
author: Matt Light
date: 2009-09-17
href: http://www.itrmanager.com/articles/95161/trois-questions-posent-dsi-propos-logiciels-libres-br-matt-light-vice-president-recherche-gartner.html
tags:
- Le Logiciel Libre
- Entreprise
- Économie
---

> En l'absence de définition des logiciels en code source ouvert par une autorité juridique, de droits d'auteur sur le terme et d'organisme de normalisation gouvernemental ou international pour régir le concept, la définition des logiciels en code source ouvert repose sur son système de licence.
