---
site: "futura-sciences.com"
title: "Internet a quarante ans !"
author: La rédaction
date: 2009-09-04
href: http://www.futura-sciences.com/fr/news/t/internet/d/internet-a-quarante-ans_20375/
tags:
- Internet
---

> Il existe plusieurs dates pour fêter les anniversaires d'Internet. Certains retiennent le premier janvier 1983, jour où le réseau Arpanet, ancêtre d'Internet, a définitivement abandonné le protocole NCP pour adopter le TCP/IP que nous connaissons encore aujourd'hui. D'autres attendent le 13 mars pour fêter l'anniversaire du Web, mis au point en 1989, par Tim Berners-Lee et ses collègues. Le 2 septembre, cependant, reste la date la plus juste pour l'anniversaire d'Internet. Voici quelques étapes à retenir.
