---
site: LeMonde.fr
title: "Le mécénat global, alternative à Hadopi ? "
author: Laurent Checola
date: 2009-09-09
href: http://www.lemonde.fr/technologies/article/2009/09/09/le-mecenat-global-alternative-a-hadopi_1238166_651865.html
tags:
- Internet
- Économie
- Partage du savoir
- HADOPI
- Institutions
- Associations
- Droit d'auteur
- Innovation
- Philosophie GNU
---

> Proposer un nouveau mécanisme de financement des créateurs : c'était le but d'artistes, de membres d'associations soutenant le logiciel libre et ses déclinaisons artistiques, et d'acteurs d'Internet, réunis mardi 8 septembre à la mairie du 3e arrondissement de Paris. Clef de voûte de leurs proposition, une  Société d'acceptation et de répartition des dons (SARD), dont le but est de faciliter le don des internautes aux artistes, plutôt que réprimer le téléchargement illégal, a été fondée.
