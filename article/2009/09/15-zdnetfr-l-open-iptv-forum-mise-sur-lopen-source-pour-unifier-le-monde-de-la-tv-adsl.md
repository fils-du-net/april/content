---
site: zdnet.fr
title: "L’Open IPTV Forum mise sur l'open source pour unifier le monde de la TV ADSL"
author: Vincent Birebent
date: 2009-09-15
href: http://www.zdnet.fr/actualites/internet/0,39020774,39706728,00.htm
tags:
- Le Logiciel Libre
- Interopérabilité
---

> Le but de cette organisation est de simplifier le monde de la TV par ADSL en améliorant l'interopérabilité des solutions matérielles et logicielles via le partage des connaissances entre ses membres. Un moyen de réduire les coûts en R&amp;D pour les nouvelles box et services.
> Cap sur l'open source
> Pour parvenir à cet objectif, l'Open IPTV Forum veut  recourir davantage à des solutions Open Source plutôt que propriétaires.
