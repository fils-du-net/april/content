---
site: zdnet.fr
title: "La virtualisation favorise Linux face à Windows"
author: Christophe Auffray
date: 2009-09-16
href: http://www.zdnet.fr/actualites/it-management/0,3800005311,39706823,00.htm
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Selon une enquête du cabinet  Gabriel Consulting Group, Linux bénéficierait d'un avantage sur Windows en matière de TCO, le coût total de possession, en favorisant la réduction des achats de matériel et la charge en frais de licences.
> D'après l'étude, les entreprises où les serveurs Linux prédominent recourent 30% plus à la virtualisation que celles exploitant majoritairement des plates-formes Windows. Une différence qui s'expliquerait notamment par la complexité et les coûts des licences Microsoft.
