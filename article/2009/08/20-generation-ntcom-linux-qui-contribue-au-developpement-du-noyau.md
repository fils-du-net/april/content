---
site: "generation-nt.com"
title: "Linux : qui contribue au développement du noyau ?"
author: Jérôme G.
date: 2009-08-20
href: http://www.generation-nt.com/linux-developpement-contribution-code-actualite-855011.html
tags:
- Le Logiciel Libre
- Entreprise
---

> La Linux Foundation annonce la publication d'une mise à jour de son étude relative au développement du noyau Linux. Une étude qui permet de rendre compte de l'évolution de ce projet open source d'envergure et savoir quels sont ses principaux contributeurs et soutiens.
> [...]
> Sans véritable surprise, Red Hat, IBM et Novell sont les sociétés qui emploient le plus de contributeurs. Depuis la version 2.6.24, le pourcentage de contributions au noyau Linux de ces sociétés s'évalue ainsi respectivement à 12 %, 6,3 % et 6,1 %. À souligner que pour 21,1 % il s'agit de développeurs indépendants. Parmi les autres noms de sociétés mentionnés, Intel ( 6 % ), Oracle ( 3,1 % ), Sun Microsystems et HP ( 1 % ), Google et AMD ( 0,8 % ).
