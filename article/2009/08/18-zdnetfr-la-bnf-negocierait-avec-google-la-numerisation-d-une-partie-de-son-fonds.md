---
site: zdnet.fr
title: "La BNF négocierait avec Google la numérisation d’une partie de son fonds"
author: La rédaction
date: 2009-08-18
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39704734,00.htm
tags:
- Partage du savoir
- Institutions
- Droit d'auteur
---

> Technologie - Le quotidien La Tribune révèle que la Bibliothèque Nationale de France aurait entamé des discussions avec Google en vue de lui confier la numérisation d’une partie de son fonds. Une démarche que la BNF avait jusqu’à présent refusée.
> [...]
> Ce retournement à 180 degrés illustre donc l'impossibilité de la France de mener à bien la numérisation de son patrimoine, un défi pourtant crucial pour les générations futures.
