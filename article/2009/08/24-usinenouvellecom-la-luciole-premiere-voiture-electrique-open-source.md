---
site: usinenouvelle.com
title: "La Luciole, première voiture électrique open source"
author: Raphaële Karayan
date: 2009-08-24
href: http://www.usinenouvelle.com/article/la-luciole-premiere-voiture-electrique-open-source.N116133
tags:
- Entreprise
- Licenses
- Standards
---

> Une société japonaise va mettre sa technologie de châssis à roues motorisées à la disposition des constructeurs, en échange de leur participation industrielle et de leur soutien à la R&amp;D. Objectif de ce modèle collaboratif : une voiture électrique à 12.000 euros en 2013.
