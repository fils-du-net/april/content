---
site: pcinpact.com
title: "Nec et Panasonic préparent neuf mobiles sous Linux"
author: Nicolas.G
date: 2009-08-11
href: http://www.pcinpact.com/actu/news/52445-nec-panasonic-mobiles-neuf-linux.htm
tags:
- Le Logiciel Libre
- Entreprise
---

> L'association de Panasonic et Nec débouchera sur l'arrivée de neuf nouveaux modèles de téléphones portables, lesquels reposeront tous sur un système d'exploitation Linux, vient-on d'apprendre de Reuters.
> Si les deux grands que sont Google et Apple ont pris place dans l'arène et occupent des places significatives, reste que le monde du libre pourrait devenir une solution attractive pour les constructeurs, et pas simplement financièrement.
