---
site: pcinpact.com
title: "FramaDVD, une compilation qui carbure au libre"
author: Marc Rees
date: 2009-08-26
href: http://www.pcinpact.com/actu/news/52748-framasoft-framadvd-logiciel-libre-dvd.htm
tags:
- Le Logiciel Libre
- Sensibilisation
---

> Début septembre, l'association Framasoft va lancer son FramaDVD, compilation gavée de logiciels et autres contenus libres. Sur la galette, plus de 90 logiciels pour les trois systèmes (Windows, Linux et Mac OS) seront offerts, prêts à être installés.
>  Ce pack est rangé par catégories (Éducation, Jeux, Culture) avec évidemment une section « Indispensables » avec une vingtaine de logiciels.
