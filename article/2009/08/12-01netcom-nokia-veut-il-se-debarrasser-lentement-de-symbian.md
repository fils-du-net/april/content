---
site: 01net.com
title: "Nokia veut-il se débarrasser lentement de Symbian?"
author: Gilbert Kallenborn
date: 2009-08-12
href: http://pro.01net.com/editorial/504895/nokia-veut-il-se-debarrasser-lentement-de-symbian/
tags:
- Le Logiciel Libre
- Entreprise
---

> Selon un quotidien allemand, Nokia ne croit plus en Symbian et préfère miser sur Maemo, sa plate-forme Linux. Mais au final, ce sont les développeurs qui trancheront.
> C'est un beau pavé dans la mare qu'a jeté hier le Financial Times Deutschland. Le quotidien économique explique, selon une source anonyme, que Nokia aurait perdu confiance en Symbian, le système d'exploitation qu'il utilise pour ses terminaux haut de gamme. Dans ce segment, il aurait prévu de s'appuyer de plus en plus sur Maemo, sa plate-forme mobile basée sur Linux. Selon nos confrères allemands, le premier smartphone Nokia basé sur Maemo devrait ainsi sortir dans les toutes prochaines semaines.
