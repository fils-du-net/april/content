---
site: progilibre.com
title: "Tarification du cloud computing... pourquoi faire simple ?"
author: Philippe Nieuwbourg
date: 2009-08-17
href: http://www.progilibre.com/Tarification-du-cloud-computing-pourquoi-faire-simple_a912.html
tags:
- Le Logiciel Libre
- Entreprise
- Économie
- Informatique en nuage
---

> Oui, on peut gagner de l’argent comme éditeur de logiciels open source. C’est en tous cas ce que nous affirment depuis quelques années les éditeurs d’open source « commercial ». Ils nous l’affirment, mais les faits semblent un peu longs à venir confirmer ces objectifs.
