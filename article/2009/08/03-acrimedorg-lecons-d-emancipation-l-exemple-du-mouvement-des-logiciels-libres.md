---
site: acrimed.org
title: "Leçons d’émancipation : l’exemple du mouvement des logiciels libres"
author: Hervé Le Crosnier
date: 2009-08-03
href: http://www.acrimed.org/article3160.html
tags:
- Le Logiciel Libre
---

> Approche « pragmatique » et approche « philosophique » ne sont pas incompatibles, c’est du moins la principale leçon politique que je pense tirer de ce mouvement et de son impact plus global sur toute la société. Car si un mouvement ne parle pas de lui-même, il « fait parler » et exprime autant qu'il ne s’exprime. Le mouvement des logiciels libres, et ses diverses tendances, est plus encore dans ce cas de figure, car son initiateur, Richard M. Stallman, n’hésite pour sa part jamais à placer les fondements philosophiques au cœur de l'action du mouvement.
