---
site: lemondeinformatique.fr
title: "Red Hat domine le marché Linux, seul Novell lui résiste"
author: François Lambel 
date: 2009-08-20
href: http://www.lemondeinformatique.fr/actualites/lire-red-hat-domine-le-marche-linux-seul-novell-lui-resiste-29022.html
tags:
- Le Logiciel Libre
- Entreprise
---

> D'après ce qui filtre de la dernière étude d'IDC sur le marché Linux*, Red Hat s'est accaparé 64,7% des 567 M$ (+23,4%) de chiffre d'affaires généré autour de Linux en 2008, autrement dit, le support. Si l'on ajoute les 29% attribués à Novell, il ne reste pas grand-chose pour les autres distributions. Red Hat et Novell se tailleraient aussi la part du lion de la base installée gratuite (c'est-à-dire sans support). La base installée totale de RHEL (Red Hat Enterprise Linux) représenterait 47,6% du marché.
