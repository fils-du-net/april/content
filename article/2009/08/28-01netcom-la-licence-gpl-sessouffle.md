---
site: 01net.com
title: "La licence GPL s'essouffle"
author: Alain Clapaud
date: 2009-08-28
href: http://pro.01net.com/editorial/505579/la-licence-gpl-sessouffle/
tags:
- Le Logiciel Libre
- Licenses
---

> [...] A partir de l'analyse de ces dizaines de millions de lignes de code, les ordinateurs de Black Duck Software déterminent quotidiennement le nombre de projets par type de licence open source.
> De ces chiffres, il ressort que la GNU-General Public Licence (GPL) 2.0 reste la licence la plus courante, et de loin. Toutefois sa part diminue et passe sous les 50 %. En cumulant l'ensemble des licences GPL (GPL 2.0, LGPL 2.1 et GPL 3.0), on atteint 65 % des projets. C'est énorme, mais les licences GNU accusent une baisse de 5 % en un an.
