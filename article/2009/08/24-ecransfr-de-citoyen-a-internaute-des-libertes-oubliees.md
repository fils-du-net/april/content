---
site: ecrans.fr
title: "De citoyen à internaute, des libertés oubliées"
author: C.M.
date: 2009-08-24
href: http://www.ecrans.fr/De-citoyen-a-internaute-des,7921.html
tags:
- Le Logiciel Libre
- Internet
- Partage du savoir
- HADOPI
- Institutions
- Philosophie GNU
---

> Cet enjeu a été [ndlr : défendre les libertés individuelles], depuis longtemps, parfaitement assimilé par le mouvement du logiciel libre : les réseaux et appareils électroniques (lesquels seront tous à terme connectés aux réseaux) doivent être ouverts et libres car c’est la seule façon d’en assurer le contrôle par les citoyens. Cette liberté doit s’exercer par un contrôle possible de l’architecture même des réseaux et un contrôle effectif, par chaque citoyen, de ses appareils électroniques
