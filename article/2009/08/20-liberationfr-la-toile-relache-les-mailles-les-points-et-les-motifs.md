---
site: liberation.fr
title: "La Toile relâche les mailles, les points et les motifs"
author: Marie Lechner
date: 2009-08-20
href: http://www.liberation.fr/culture/0101586193-la-toile-relache-les-mailles-les-points-et-les-motifs
tags:
- Partage du savoir
- Droit d'auteur
---

> Qu’est-ce qu’une conférence sur le tricot peut bien faire au milieu d’un congrès de hackers ? La scène se déroule en décembre 2007, à Berlin, à l’occasion de la rencontre annuelle du Chaos Computer Club, l’une des plus influentes organisations de hackers (1). Devant un parterre viril de programmeurs, Rose White, étudiante en sociologie boulotte, tisse des liens entre le tricot et l’informatique, démontrant comment la pratique contemporaine de la maille se rapproche de la programmation open source.
