---
site: zdnet.fr
title: "Faille critique (corrigée) dans le noyau Linux"
author: Olivier Chicheportiche
date: 2009-08-17
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39704673,00.htm
tags:
- Le Logiciel Libre
---

> Linux n'est pas exempt de failles critiques. Deux ingénieurs de Google ont découvert une vulnérabilité critique dans le noyau Linux 2.4 et 2.6, soit la plupart des versions diffusées depuis 2001.
