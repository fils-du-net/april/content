---
site: zdnet.fr
title: "Brevets logiciels, le retour du fils de la vengeance"
author: Thierry Noisette
date: 2009-08-14
href: http://www.zdnet.fr/blogs/l-esprit-libre/brevets-logiciels-le-retour-du-fils-de-la-vengeance-39704541.htm
tags:
- Brevets logiciels
---

> [...]
> Quand Bill Gates prévoyait les dangers des brevets
> C'est l'occasion(1) de se souvenir que le fondateur de Microsoft avait exprimé son opinion sur ces brevets, dans un e-mail interne à son entreprise, le 16 mai 1991. Dans ce mémorandum classé confidentiel, qui avait fui quelques semaines plus tard(2), Bill Gates déclarait: «Si les gens avaient compris comment seraient accordés des brevets, à l'époque où la plupart des idées actuelles ont été inventées, et avaient déposé des brevets, l'industrie serait aujourd’hui complètement bloquée.»
