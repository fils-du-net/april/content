---
site: ledevoir.com
title: "Libre opinion - Informatique libre et démocratie locale"
author: Cyrille Béraud
date: 2009-08-12
href: http://www.ledevoir.com/2009/08/12/262513.html
tags:
- Le Logiciel Libre
- Administration
- Institutions
---

> En rendant accessibles et transparents les traitements faits sur les données, nous rendons responsables les acteurs des processus décisionnels et nous garantissons la traçabilité de toutes les décisions. Nous permettons et encourageons les interactions en temps réel entre le citoyen, l'élu et l'administration.
