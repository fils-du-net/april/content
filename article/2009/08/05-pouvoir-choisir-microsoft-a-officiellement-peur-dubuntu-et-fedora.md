---
site: "pouvoir-choisir.org"
title: "Microsoft a officiellement peur d'Ubuntu et Fedora"
author: Gilles Coulais
date: 2009-08-05
href: http://pouvoir-choisir.org/logiciel-libre/index.php/post/2009/08/05/Microsoft-a-officiellement-peur-d-Ubuntu
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Économie
- Vente liée
- Informatique-deloyale
- Promotion
---

> Dans son rapport annuel à la SEC (organisme chargé de suivre les opérations de bourse aux États-Unis), Microsoft admet officiellement qu'Ubuntu et Fedora sont des concurrents sérieux pour sa division client. [...] Cette concurrence est aujourd'hui identifiée par Microsoft, sur le poste de travail. Je cite le rapport :
> [...]
> « Des produits logiciels commerciaux concurrents, dont des variantes d'Unix, sont fournis par des concurrents tels qu'Apple, Canonical et Red Hat. (...) Le système d'exploitation Linux, qui est également dérivé d'Unix, et est disponible gratuitement sous licence GPL, a gagné en reconnaissance, particulièrement sur les marchés émergents, alors que la pression de la concurrence, pousse les fabriquants à réduire les coûts, et que les nouveaux PC à bas coûts influencent le comportement d'adoption. Des partenaires tels que Hewlett-Packard et Intel ont activement contribué aux systèmes alternatifs basés sur Linux. »
