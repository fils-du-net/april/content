---
site: 01net.com
title: "ODF ou OOXML, il est urgent de choisir !"
author: Alain Clapaud
date: 2009-08-21
href: http://pro.01net.com/editorial/505383/odf-ou-ooxml-il-est-urgent-de-choisir/
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Interopérabilité
- Standards
---

> [...] Le prestigieux institut Fraunhofer de Berlin s'est livré à une étude très poussée de l'interopérabilité des deux formats concurrents de la bureautique. Open Document Format (ODF) 1.0, le format implémenté dans OpenOffice.org, StarOffice, NeoOffice ou encore KOffice, a été créé par Sun au début des années 2000, adopté par l'Oasis en 2005 pour devenir une norme ISO en 2006. Norme qui n'a pas été retenue par Microsoft comme format XML d'Office 2007, l'éditeur ayant choisi de créer son propre format XML, Office Open XML (OOXML).
