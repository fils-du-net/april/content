---
site: zdnet.fr
title: "Firefox : une extension permet de consulter gratuitement les documents des tribunaux américains"
author: La rédaction
date: 2009-08-17
href: http://www.zdnet.fr/actualites/internet/0,39020774,39704649,00.htm
tags:
- Administration
- Partage du savoir
---

> Technologie - Avec RECAP, une extension pour Firefox, les internautes peuvent consulter les documents publics issus des tribunaux de district, d’appel et des faillites qui étaient jusqu’à présent payants.
> [...]
> Or, ces documents sont libres de droits et peuvent être reproduits sans permission précise PACER. C'est sur ce point que s'appuie RECAP (PACER écrit à l'envers) pour offrir la consultation gratuite. Chaque document consulté par un internaute est automatiquement ajouté à la base de données. RECAP dispose déjà d'une base d'un million de documents.
