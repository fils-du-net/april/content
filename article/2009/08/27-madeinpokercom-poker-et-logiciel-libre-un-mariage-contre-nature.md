---
site: madeinpoker.com
title: "Poker et logiciel libre, un mariage contre nature ? "
author: Cyril Fievet 
date: 2009-08-27
href: http://www.madeinpoker.com/zooms/poker-et-logiciel-libre-un-mariage-contre-nature-3104.html
tags:
- Le Logiciel Libre
- Licenses
- Philosophie GNU
---

> [...]
> Pour qui s'intéresse au développement d'Internet et au logiciel libre, Dachary est loin d'être un inconnu. Initiateur dès le début des années 1990 de la branche française de la Free Software Foundation (l'une des principales associations assurant la promotion du logiciel libre dans le monde), Dachary est l'un des pionniers de l'Internet français. Informaticien, conférencier et ardent défenseur de la "culture du libre", on lui doit notamment la création de Ecila, l'un des tous premiers moteurs de recherche francophone (1995-2002) et le développement d'une pléthore de programmes, librairies et autres morceaux de code, tous mis librement à disposition de la communauté informatique mondiale. Au début des années 2000, Dachary s'intéresse au jeu en ligne puis, tout naturellement, au poker.
