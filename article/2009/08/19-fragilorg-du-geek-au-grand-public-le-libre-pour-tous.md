---
site: fragil.org
title: "Du geek au grand public, le Libre pour tous ?"
author: Romain Ledroit
date: 2009-08-19
href: http://www.fragil.org/focus/1248
tags:
- Le Logiciel Libre
- Vente liée
---

> Linux incarne depuis longtemps l’esprit du Libre et de la communauté. Jusqu’alors réservé aux geeks et informaticiens avertis, le logiciel Libre tend à se démocratiser et s’ouvre désormais à l’utilisateur lambda. Il devient tout à fait imaginable de quitter les traditionnels Windows et MacOs, pour adopter une distribution Linux et découvrir l’univers du Libre. Cependant, Le Libre reste une alternative, loin de l’économie de marché, loin des réseaux grand public.
