---
site: lemondeinformatique.fr
title: "Mozilla organise une semaine solidaire autour du Web"
author: Emmanuelle Delsol
date: 2009-08-24
href: http://www.lemondeinformatique.fr/actualites/lire-mozilla-organise-une-semaine-solidaire-autour-du-web-29034.html
tags:
- Le Logiciel Libre
- Internet
- Sensibilisation
---

> Entre le 14 et le 21 septembre prochain, Mozilla invite les internautes à partager leur savoir-faire du Web avec d'autres. Le niveau de compétences importe peu. Une formation à l'utilisation des messageries instantanées sera tout autant la bienvenue que le développement Web.
