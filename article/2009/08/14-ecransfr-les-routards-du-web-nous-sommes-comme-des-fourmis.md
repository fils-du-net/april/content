---
site: ecrans.fr
title: "Les routards du web : « Nous sommes comme des fourmis »"
author: Camille Gévaudan
date: 2009-08-14
href: http://www.ecrans.fr/Les-routards-du-web-Qui-sont-ils,7888.html
tags:
- Internet
- Interopérabilité
- Accessibilité
---

> En six épisodes, nous avons tenté de comprendre (et par là-même de vous expliquer) comment fonctionne le projet de cartographie libre OpenStreetMap. En conclusion de cette série, nous avons voulu nous intéresser à ceux qui font vivre ce projet au jour le jour. En l'absence d’un représentant « officiel » pour la partie française, nous avons choisi un format d’« interview communautaire » avec les contributeurs qui ont bien voulu répondre à nos questions.
