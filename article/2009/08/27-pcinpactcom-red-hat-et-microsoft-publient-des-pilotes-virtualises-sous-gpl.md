---
site: pcinpact.com
title: "Red Hat et Microsoft publient des pilotes virtualisés sous GPL"
author: Vincent Hermann
date: 2009-08-27
href: http://www.pcinpact.com/actu/news/52768-microsoft-redhat-virtualisation-pilotes-kvm.htm
tags:
- Le Logiciel Libre
- Interopérabilité
- Licenses
---

> [...]
> Du côté de Red Hat, un pas important a été fait vers Microsoft. L’éditeur vient en effet de lancer ses propres pilotes à destination des clients Windows virtualisés sur KVM (Kernel-based Virtual Machine), pour qu'eux aussi soient optimisés pour un hyperviseur particulier. Ces composants sont eux aussi sous licence GPL 2 et intègrent les pilotes pour le réseau et pour le stockage.
> Dans un cas comme dans l'autre, ce sont de bonnes nouvelles pour l'interopérabilité.
