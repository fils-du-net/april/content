---
site: journaldunet.com
title: "Unix fête ses 40 ans dans une forme olympique"
author: Dominique Filippone
date: 2009-08-27
href: http://www.journaldunet.com/solutions/systemes-reseaux/actualite/unix-fete-ses-40-ans-dans-une-forme-olympique.shtml
tags:
- Interopérabilité
- Standards
---

> En août 1969, un programmeur des laboratoires Bell mettait la main aux dernières lignes de codes du système d'exploitation Unix. Quarante ans plus tard, Unix est plus que jamais vivant.
> Ken Thompson. Tel est le nom du programmeur de génie qui a mis au monde le système d'exploitation serveurs Open Source le plus emblématique de sa génération : Unix.
