---
site: "decision-achats.fr"
title: "Paris propose gratuitement aux autres collectivités son logiciel d'aide à la gestion des marchés publics"
author: La rédaction
date: 2009-08-04
href: http://www.decision-achats.fr/xml/Breves/2009/08/04/30205/Paris-propose-gratuitement-aux-autres-collectivites-son-logiciel-d-aide-a-la-gestion-des-marches-publics/?iPageNum=1
tags:
- Le Logiciel Libre
- Administration
---

> Comme nous vous le révélions en exclusivité sur notre site, la Ville de Paris vient de mettre à disposition des autres entités publiques, en accès libre, son logiciel d’aide à la gestion des marchés.
> [...]
> Découvrez l'outil (sur le site de la ville de Paris) : http://www.epm.paris.fr/
