---
site: nouvelobs.com
title: "L'économie du libre sur Internet"
author: Boris Manenti 
date: 2009-08-07
href: http://tempsreel.nouvelobs.com/actualites/vu_sur_le_web/20090721.OBS4974/leconomie_du_libre_sur_internet.html
tags:
- Le Logiciel Libre
- Internet
---

> L'arrivée du web a bouleversé les modèles économiques pré-établis de la logique commerciale. Maintenant, les créateurs travaillent pour une communauté dans une logique de partage, d'échange de l'information, de diffusion de la culture.
> Tout commence avec une recette de cuisine.
> Après avoir obtenu une recette, tout le monde aime à l'utiliser, la modifier, l'adapter, la recopier et la partager. Pourtant, dans le monde des droits d'auteurs, cette recette devenue payante ne peut plus être utilisée que dans une seule cuisine, pour une seule personne et est interdite de copie. L'informaticien Richard Stallman tente un parallèle avec les logiciels pour insister sur les restrictions posées par le marché des logiciels dits "propriétaires", c'est-à-dire régis par des droits d'auteurs.
