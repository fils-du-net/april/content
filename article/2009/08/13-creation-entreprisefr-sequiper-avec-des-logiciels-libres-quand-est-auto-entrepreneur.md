---
site: "creation-entreprise.fr"
title: "S'équiper avec des logiciels libres quand on est auto-entrepreneur"
author: La rédaction
date: 2009-08-13
href: http://www.creation-entreprise.fr/equiper-logiciels-libres-on-auto-f13222.html
tags:
- Le Logiciel Libre
- Entreprise
---

> L’auto-entrepreneur qui crée son entreprise ne déduit pas de charges, on le sait bien. Et il peut être intéressant, pour cette raison là, de se doter d’outils gratuits, en informatique notamment. Mais la problématique réside dans la qualité du produit, et dans la difficulté du choix qui est peut aisée dans l'univers prolifique de l'Open Source (le logiciel libre).
