---
site: zdnet.fr
title: "SCO renaît de ses cendres et peut de nouveau menacer Linux"
author: La rédaction
date: 2009-08-25
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39705155,00.htm
tags:
- Le Logiciel Libre
- Brevets logiciels
---

> Une cour d'appel américaine vient de remettre en selle l'éditeur en cassant un jugement de 2007 octroyant à Novell la propriété d'Unix. On pensait pourtant SCO et ses prétendions à l'égard d'Unix définitivement enterrées.
> Cette nouvelle décision de justice autorise ainsi SCO à demander de nouveaux procès dans le différend qui l'oppose à Novell et IBM. Au premier, SCO peut une nouvelle fois tenter de ravir les droits sur Unix et les licences associées.
