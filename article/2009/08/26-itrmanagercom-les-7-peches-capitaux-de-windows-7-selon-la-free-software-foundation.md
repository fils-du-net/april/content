---
site: itrmanager.com
title: "Les 7 péchés capitaux de Windows 7 selon la Free Software Foundation"
author: La rédaction
date: 2009-08-26
href: http://www.itrmanager.com/articles/94365/7-peches-capitaux-windows-7-selon-free-software-foundation.html
tags:
- Logiciels privateurs
---

> Tout ce qui est excessif est insignifiant avait déclaré le comte de Talleyrand. Cette vérité pourrait sans doute être appliquée à la récente campagne Windows 7 Sins: The case against Microsoft and proprietary software lancée par la Free Software Foundation. On n'en attendait pas moins de l'association de défense de l'Open Source pour qui Windows 7, Vista et XP ont le même problème : ce sont des logiciels propriétaires.
