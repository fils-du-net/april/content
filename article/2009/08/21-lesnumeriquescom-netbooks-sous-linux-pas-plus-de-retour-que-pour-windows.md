---
site: lesnumeriques.com
title: "Netbooks sous Linux : pas plus de retour que pour Windows"
author: Fabien Pionneau
date: 2009-08-21
href: http://www.lesnumeriques.com/news_id-10318.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Malgré des débuts difficiles, Microsoft a su rapidement imposer Windows XP dans les netbooks. On parle désormais d'un taux d'utilisation de 96% pour Windows XP dans les netbooks. Microsoft avait d'ailleurs annoncé la raison de ce succès : les machines sous Linux auraient un taux de retour 4 à 5 fois supérieur. C'était sans compter sur Dell qui vient de contredire cette information.
