---
site: zdnet.fr
title: "Wikipedia en anglais atteint 3 millions d'articles"
author: Thierry Noisette
date: 2009-08-17
href: http://www.zdnet.fr/blogs/l-esprit-libre/wikipedia-en-anglais-atteint-3-millions-d-articles-39704713.htm
tags:
- Partage du savoir
---

> [...] Bénéfice de la domination de l'anglais, la version dans cette langue de l'encyclopédie libre est de loin la plus importante, en nombre d'articles, devant l'allemand, 943.000 articles, le français, 840.000, le polonais, 627.000, puis le japonais, l'italien etc. Au total 267 langues, dont 27 au-dessus de 100.000 articles chacune. L'ensemble devrait atteindre 14 millions d'articles d'ici la fin de l'année, selon l'annonce officielle des trois millions d'articles en anglais.
