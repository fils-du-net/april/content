---
site: lemondeinformatique.fr
title: "Les fournisseurs prennent de plus en plus le contrôle de l'Open source, selon le Gartner"
author: François Lambel 
date: 2009-08-12
href: http://www.lemondeinformatique.fr/actualites/lire-les-fournisseurs-prennent-de-plus-en-plus-le-controle-de-l-open-source-selon-le-gartner-28992.html
tags:
- Le Logiciel Libre
- Entreprise
- Économie
---

> Si le logiciel libre gagne du terrain, c'est, selon le Gartner Group, au prix d'une prise de contrôle par les grands fournisseurs. Selon le cabinet, d'ici 2012, la moitié au moins du CA généré par les logiciels open source et les services associés proviendra de projets "sponsorisés" par un seul fournisseur. Le Gartner, qui ne s'aventure pas à citer de noms, explique ce phénomène par le développement d'une offre open source qui propose des logiciels à un stade précoce, sans soutien d'une réelle communauté, dont les quelques membres actifs sont, en fait, tous salariés d'une seule et même entreprise.
