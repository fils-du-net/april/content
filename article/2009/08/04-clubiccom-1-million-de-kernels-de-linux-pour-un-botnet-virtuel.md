---
site: clubic.com
title: "1 million de kernels de Linux pour un botnet virtuel"
author: Guillaume Belfiore
date: 2009-08-04
href: http://www.clubic.com/actualite-291908-botnet-virtuel-kernels-linux.html
tags:
- Internet
- Innovation
---

> Une équipe de scientifiques spécialisés dans la cyber-sécurité a réussi à faire tourner simultanément plus d'un million de kernels de linux au sein d'une machine virtuelle des laboratoires nationaux de Sandia. Le kernel est le coeur du système d'exploitation permettant de passer des instructions entre les composants et le système. Cette initiative vise à observer et à mieux comprendre le fonctionnement d'un botnet malicieux, c'est-à-dire un réseau composé de plusieurs machines infectées d'un malware et contrôlées par une personne malintentionnée.
