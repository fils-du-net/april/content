---
site: zdnet.fr
title: "Concours de beauté pour start-ups de l'open source pendant l'Open World Forum"
author: Thierry Noisette
date: 2009-08-10
href: http://www.zdnet.fr/blogs/l-esprit-libre/concours-de-beaute-pour-start-ups-de-l-open-source-pendant-l-open-world-forum-39704270.htm
tags:
- Le Logiciel Libre
- Entreprise
- Économie
- Institutions
---

> «Vingt sociétés, sélectionnées sur dossier de candidature, seront nominées et invitées à se présenter devant un jury d’experts en innovation (investisseurs, intégrateurs et experts de l’open source), et un public composé notamment d’investisseurs et d’intégrateurs susceptibles de les aider à mettre sur le marché leurs technologies et leurs solutions.
> Les projets les plus prometteurs seront récompensés par des “Open Innovation Awards” qui seront remis au cours de la keynote de clôture du forum.»
