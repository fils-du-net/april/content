---
site: pcinpact.com
title: "Ministère de la défense : Herisson piquera le web dès mars 2010"
author: Marc Rees
date: 2009-08-18
href: http://www.pcinpact.com/actu/news/52566-herisson-surveillance-echelon-dga-sources.htm
tags:
- Informatique-deloyale
---

> Herisson ou « Habile Extraction du Renseignement d'Intérêt Stratégique à partir de Sources Ouvertes Numérisées » de la Délégation Générale pour l'Armement est un projet français visant à exploiter les sources dites « ouvertes » du web. Peu d’informations ont été données jusqu’alors, si ce n’est la diffusion du CCTP ou cahier des clauses techniques particulières (voir notre actualité) suivie de l’interview par Ecrans.fr d’un responsable de la DGA destinée à rassurer l’opinion sur ce qu’est Hérisson.
> [...]
> « Herisson (Habile extraction du renseignement d'intérêt stratégique à partir de sources ouvertes numérisées) est un démonstrateur technologique qui n'a pas vocation à devenir opérationnel. Ce démonstrateur vise à automatiser, à l'aide d'outils logiciels libres ou du commerce, des actions de consultation, de téléchargement et de visualisation de données en libre accès sur Internet, que tout internaute peut réaliser.
