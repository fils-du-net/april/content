---
site: zdnet.fr
title: "Les utilisateurs Linux et Mac privilégient la recherche sur Google"
author: Christophe Auffray
date: 2009-08-24
href: http://www.zdnet.fr/actualites/internet/0,39020774,39705078,00.htm
tags:
- Le Logiciel Libre
- Internet
---

> Technologie - Selon une étude de Chitika (basée sur 163 millions de recherches), un spécialiste de la publicité ciblée sur Internet, le choix du moteur de recherche varie fortement en fonction du système d’exploitation installé sur l’ordinateur de l’utilisateur.
> Google est largement privilégié par les utilisateurs de systèmes Linux et Mac. 93% des requêtes faites depuis un poste Mac l'ont été depuis le moteur de Google. 4,96% de ces mêmes recherches sous Mac sont faites sur Yahoo. La technologie de Microsoft, Bing, représente enfin 1,13% des requêtes.
