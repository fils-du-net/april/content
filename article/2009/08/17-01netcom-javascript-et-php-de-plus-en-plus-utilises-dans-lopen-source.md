---
site: 01net.com
title: "Javascript et PHP, de plus en plus utilisés dans l'open source"
author: Gilbert Kallenborn
date: 2009-08-17
href: http://pro.01net.com/editorial/504927/javascript-et-php-de-plus-en-plus-utilises-dans-lopen-source/
tags:
- Le Logiciel Libre
- Licenses
---

> Ces statistiques sont obtenues au travers d'une collecte automatique des codes sources. Black Duck analyse 200 000 projets au travers de 4 300 forges de développement. Cette collecte couvre également les licences open source. Ainsi, on apprend, sans surprise, que la licence la plus utilisée est GPL 2.0, avec 49,6 % des projets. Elle est suivie par LGPL 2.1 (9,5 %), Artistic License (8,6 %), BSD License 2.0 (6,3 %) et GPL 3.0 (5,3 %).
