---
site: zdnet.fr
title: "Jacques Attali : \"Hadopi ne servira à rien\""
author: Olivier Chicheportiche
date: 2009-08-18
href: http://www.zdnet.fr/actualites/internet/0,39020774,39704746,00.htm
tags:
- HADOPI
---

> Avis d'expert - L'intellectuel tire une nouvelle fois à boulet rouge sur cette loi censée combattre le téléchargement illégal mais qui selon lui a été faite pour protéger les Majors.
> Quasiment adoptée, la loi Hadopi continue à susciter une forte polémique. Jacques Attali, l'ancien conseiller spécial de François Mitterrand n'a jamais caché son opposition au texte. Dans une récente interview accordée à Ecrans (Libération), il en remet une couche.
