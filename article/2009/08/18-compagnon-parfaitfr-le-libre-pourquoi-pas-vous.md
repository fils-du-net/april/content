---
site: "compagnon-parfait.fr"
title: "Le libre, pourquoi pas vous ?"
author: Isabelle Boucq
date: 2009-08-18
href: http://www.compagnon-parfait.fr/le-libre-pourquoi-pas-vous-art-533-1.html
tags:
- Le Logiciel Libre
- April
---

> Fut un temps où installer Linux sur son ordinateur était une opération un peu ésotérique réservée aux initiés. Plus maintenant, nous raconte Frédéric Couchet qui est l'un des principaux champions du logiciel libre en France.
> « Promouvoir et défendre le logiciel libre », c’est la devise d’April qui fait des pieds et des mains depuis 1996 pour démocratiser le logiciel libre auprès du grand public, mais aussi des professionnels et des administrations. Avec un succès notable auprès de ces deux dernières catégories depuis quelques années déjà et plus récemment auprès du grand public.
