---
site: lesechos.fr
title: "Dans l'univers du logiciel, Google  est schizophrène"
author: Emmanuel Paquette
date: 2009-10-05
href: http://www.lesechos.fr/journal20091005/lec2_technologies_de_l_information/020159985648.htm
tags:
- Le Logiciel Libre
- Entreprise
---

> Chris Dibona directeur des programmes du logiciel libre de Google
> [...] C'est vrai. Nous avons une attitude schizophrène. Il y a des services sur lesquels nous pouvons ouvrir le code source et d'autres sur lesquels nous ne le pouvons pas. En particulier, nous n'ouvrons pas le code de notre moteur de recherche et le système de vente de liens publici­taires. Si nous faisions cela sur l'algorithme de recherche, nous donnerions des armes à nos ennemis au cas où ils voudraient faire des choses répréhensibles. Cela peut aller de la diffusion de logiciels malveillants jusqu'à la manipulation de l'algorithme pour apparaître en tête des résultats de recherche. Nous devons également respecter les lois de certains pays qui nous obligent à ne pas référencer certains contenus sur le moteur de recherche.
