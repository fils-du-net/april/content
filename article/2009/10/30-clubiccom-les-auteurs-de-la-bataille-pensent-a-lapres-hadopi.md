---
site: clubic.com
title: "Les auteurs de la \"Bataille\" pensent à l'après Hadopi"
author: Alexandre Laurent
date: 2009-10-30
href: http://www.clubic.com/actualite-308336-auteurs-bataille-hadopi-pensent.html
tags:
- April
- HADOPI
---

> Ils sont venus nombreux jeudi soir au Fouquet's pour assister à la présentation de La Bataille Hadopi, un ouvrage collectif édité par In Libro Veritas regroupant les écrits d'une quarantaine d'auteurs impliqués, à un niveau ou à un autre, dans les questions relatives au droit d'auteur, à la culture et à Internet.
