---
site: arstechnica.com
title: "Adobe pushes Flash and PDF for open government, misses irony"
author: Chris Foresman
date: 2009-10-30
href: http://arstechnica.com/tech-policy/news/2009/10/adobe-pushes-flash-and-pdf-for-open-government-misses-irony.ars
tags:
- Logiciels privateurs
- Interopérabilité
- Institutions
---

> Adobe essaye d'encourager sa technologie fermée Flash pour promouvoir l'"ouverture" du gouvernement américain :
