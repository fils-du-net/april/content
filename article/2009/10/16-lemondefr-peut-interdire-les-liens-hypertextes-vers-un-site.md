---
site: lemonde.fr
title: "Peut-on interdire les liens hypertextes vers un site ?"
author: Damien Leloup
date: 2009-10-16
href: http://www.lemonde.fr/archives/article/2009/10/16/peut-on-interdire-les-liens-hypertextes-vers-un-site_1254977_0.html
tags:
- Internet
- Désinformation
- Neutralité du Net
---

> [...] Cette pratique est abusive et sans fondement, estime Benoît Sibaud, de l'April – une association qui promeut le logiciel libre –, qui maintient une liste des sites pratiquant le "pdlsa" (pas de lien sans autorisation préalable). "Le lien hypertexte est le fondement même du Web, explique-t-il. Et cette interdiction est impossible à mettre en place : si je clique sur un lien dans une messagerie instantanée, un e-mail, ou que j'utilise un service de raccourcisseur d'adresse, il est impossible de savoir qui a créé le lien."
