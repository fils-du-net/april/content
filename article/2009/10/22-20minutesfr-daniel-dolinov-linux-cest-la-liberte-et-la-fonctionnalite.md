---
site: 20minutes.fr
title: "Daniel Dolinov: «Linux, c'est la liberté et la fonctionnalité»"
author: Philippe Berry
date: 2009-10-22
href: http://www.20minutes.fr/article/357485/High-Tech-Daniel-Dolinov-Linux-c-est-la-liberte-et-la-fonctionnalite.php
tags:
- Le Logiciel Libre
- Économie
- Sensibilisation
---

> Si Windows 7 sort aujourd'hui, Ubuntu Karmic Koala, la nouvelle version de la distribution Linux la plus populaire auprès du grand public, sera disponible le 29 octobre. Entretien avec David Dolinov, chargé de la promotion du système d'exploitation chez Canonical, l'entreprise privée qui sponsorise Ubuntu.
