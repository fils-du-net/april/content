---
site: datanews.be
title: "Les logiciels libres ne doivent pas être oubliés dans l'histoire"
author: Jean-Luc Manise
date: 2009-10-21
href: http://www.datanews.be/fr/news/90-57-26680/-les-logiciels-libres-ne-doivent-pas-etre-oublies-dans-l-histoire-.html
tags:
- Le Logiciel Libre
- Administration
---

> L'interdiction d'utiliser Firefox et l'obligation pour l'Administration wallonne de se standardiser sur Internet Explorer 6 (cfr notre info du 25 septembre) a récemment suscité une question orale de Hervé Jamart (MR) à Rudy Demotte, Ministre-Président de la Région wallonne. Jamart: "Eh oui, la Région wallonne, soucieuse de sa sécurité informatique et pour empêcher toute exploitation d'une faille de Firefox, a demandé à ses fonctionnaires de désinstaller ce navigateur et d'utiliser plutôt Internet Explorer 6.
