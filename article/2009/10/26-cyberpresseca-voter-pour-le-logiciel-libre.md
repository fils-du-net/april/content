---
site: cyberpresse.ca
title: "Voter pour... le logiciel libre"
author: Pierre Asselin
date: 2009-10-26
href: http://technaute.cyberpresse.ca/nouvelles/logiciels/200910/26/01-914982-voter-pour-le-logiciel-libre.php
tags:
- Le Logiciel Libre
- Administration
---

> Québec, ville ouverte... aux logiciels libres? L'idée ne vient pas du maire Régis Labeaume ni de ses adversaires, mais ceux qui militent en faveur du logiciel libre demandent aux candidats de faire un premier pas dans ce sens en signant le «Pacte du logiciel libre».
> [...]  «Dans un contexte de crise économique où la question de privilégier l'achat local revient sans cesse, nous voulons rappeler que l'utilisation du logiciel libre crée des emplois localement et renforce la compétitivité des entreprises québécoises», affirme Cyrille Béraud, président de FACIL.
