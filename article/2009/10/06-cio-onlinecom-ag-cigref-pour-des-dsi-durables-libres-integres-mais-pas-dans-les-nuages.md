---
site: "cio-online.com"
title: "AG Cigref : pour des DSI durables, libres, intégrés... mais pas dans les nuages"
author: Bertrand Lemaire 
date: 2009-10-06
href: http://www.cio-online.com/actualites/lire-ag-cigref-pour-des-dsi-durables-libres-integres-mais-pas-dans-les-nuages-2458.html
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] et le logiciel libre.
> Ce dernier devient une préoccupation pour les DSI à cause, d'une part, des difficultés propres aux différences entre licences, et, d'autre part, du problème des licences contaminantes. Autrement dit : les DSI, qui préfèrent avoir à la fois le beurre et l'argent du beurre, veulent bien disposer de logiciels gratuits mais hésitent à redistribuer du code écrit à leurs frais.
> Une solution pourrait être un financement public des développements de logiciels libres. « Nous souhaitons que le secteur du logiciel libre soit appuyé par le Plan de Relance » a ainsi martelé Bruno Ménard, président du Cigref.
