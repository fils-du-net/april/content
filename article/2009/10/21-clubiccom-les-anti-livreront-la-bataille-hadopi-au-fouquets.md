---
site: clubic.com
title: "Les anti livreront la Bataille Hadopi au Fouquet's"
author: Alexandre Laurent
date: 2009-10-21
href: http://www.clubic.com/actualite-306530-anti-livreront-bataille-hadopi-fouquet.html
tags:
- HADOPI
---

> Du côté des anti-Hadopi, on a des arguments, mais on a aussi un certain sens de l'humour. C'est en effet au Fouquet's, célèbre restaurant des Champs-Elysées parisiens que quarante personnalités des mondes politique, artistique, médiatique ou associatif ont donné rendez-vous à la presse et aux internautes le 29 octobre prochain, afin de présenter La Bataille Hadopi, un ouvrage collectif qui veut dénoncer les errements de la loi Hadopi et proposer des solutions concrètes de substitution tenant compte des bouleversements induits par l'essor du numérique.
