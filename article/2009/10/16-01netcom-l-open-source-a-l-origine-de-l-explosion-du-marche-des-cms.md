---
site: 01net.com
title: "L’open source à l’origine de l’explosion du marché des CMS ?"
author: Alain Clapaud
date: 2009-10-16
href: http://pro.01net.com/editorial/507269/l-open-source-a-l-origine-de-l-explosion-du-marche-des-cms/
tags:
- Le Logiciel Libre
- Internet
---

> [...] David Goldes, PDG et analyste en chef de Basex explique : « Les solutions de gestion de contenu open source sont de plus en plus demandées sur différent marchés et le marché global des solutions open source est en croissance rapide. »  Il ajoute : « Des entreprises qui ont dépensé des centaines de milliers de dollars pour des CMS peuvent aujourd'hui faire la même chose pour des plates-formes qui ne coûtent qu'un dixième de ce prix. »
