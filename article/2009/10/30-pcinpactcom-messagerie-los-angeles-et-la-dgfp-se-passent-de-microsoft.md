---
site: pcinpact.com
title: "Messagerie : Los Angeles et la DGFP se passent de Microsoft"
author: Vincent Hermann
date: 2009-10-30
href: http://www.pcinpact.com/actu/news/53906-microsoft-exchange-contrats-los-angeles-dgfp.htm
tags:
- Le Logiciel Libre
- Administration
- Vente liée
---

> Double mauvaise nouvelle pour Microsoft dans le monde de la messagerie : deux structures importantes ont annoncé qu'elles se passeraient dorénavant de ses produits. La première n’est rien de moins que la ville de Los Angeles. La seconde est de chez nous : la direction générale des finances publiques (DGFP).
