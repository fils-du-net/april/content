---
site: clubic.com
title: "Hadopi 2 validée, Nicolas Sarkozy se réjouit"
author: Alexandre Laurent
date: 2009-10-22
href: http://www.clubic.com/actualite-307002-hadopi-validee-nicolas-sarkozy-rejouit.html
tags:
- HADOPI
---

> Dans un communiqué, l'Elysée indique que le chef de l'Etat « se réjouit » de la prochaine entrée en vigueur de cette nouvelle loi qui vient compléter la loi Création et Internet du 12 juin 2009.
> [...]
> Tous ne partagent pas cet enthousiasme. « Le Conseil Constitutionnel valide donc les mouchards filtrants, les atteintes aux droits de la défense et le flou juridique qui règne autour de la "négligence caractérisée" », estime pour sa part l'April, qui promeut les intérêts du logiciel libre.
