---
site: lefigaro.fr
title: "La Lettonie abandonnerait Microsoft"
author: AFP 
date: 2009-10-22
href: http://www.lefigaro.fr/flash-actu/2009/10/22/01011-20091022FILWWW00672-la-lettonie-abandonnerait-microsoft.php
tags:
- Le Logiciel Libre
- Administration
---

> La Lettonie, durement frappée par la crise économique, envisage de remplacer les logiciels payants du géant informatique américain Microsoft par des logiciels libres gratuits, a annoncé aujourd'hui le gouvernement. "Cela est fait dans le but unique d'économiser de l'argent", a expliqué une porte-parole.
