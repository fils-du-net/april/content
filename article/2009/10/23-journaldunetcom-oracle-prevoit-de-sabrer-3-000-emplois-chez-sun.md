---
site: journaldunet.com
title: "Oracle prévoit de sabrer 3 000 emplois chez Sun"
author: Dominique FILIPPONE
date: 2009-10-23
href: http://www.journaldunet.com/solutions/acteurs/actualite/oracle-prevoit-de-sabrer-3-000-emplois-chez-sun.shtml
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] Sans compter Richard Stallman, évangéliste du logiciel libre, qui n'a pas hésité, lui aussi, à venir mettre son grain de sel, aux côtés de James Love et Malini Aisola de Knowledge Ecology International et de Jim Killock de l'Open Rights Group et prendre la plume pour écrire directement à la Commission Européenne. "Si Oracle est autorisé à acquérir MySQL, cela limitera immanquablement le développement, les fonctionnalités et la performance de la plate-forme MySQL", indiquent-ils. Ce cri du cœur saura-t-il trouver un écho auprès de la Commission ?
