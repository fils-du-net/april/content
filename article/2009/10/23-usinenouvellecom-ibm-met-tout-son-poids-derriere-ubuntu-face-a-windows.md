---
site: usinenouvelle.com
title: "IBM met tout son poids derrière Ubuntu... face à Windows"
author: Christophe Dutheil
date: 2009-10-23
href: http://www.usinenouvelle.com/article/ibm-met-tout-son-poids-derriere-ubuntu-face-a-windows.N119915
tags:
- Le Logiciel Libre
- Entreprise
- Informatique en nuage
---

> [...] N'en déplaise à Microsoft, plutôt que d'investir leurs précieux deniers dans des licences Windows, les entreprises ont tout intérêt à s'appuyer sur Linux et / ou à virtualiser leurs stations de travail en s'appuyant sur des logiciels libres. C'est en substance le message que martèle IBM depuis un an. Son argument - « choc » en cette période de ralentissement des investissements dans les systèmes d'information - l'a déjà conduit fin 2008 à proposer de premiers bureaux virtuels sous Linux avec Canonical.
