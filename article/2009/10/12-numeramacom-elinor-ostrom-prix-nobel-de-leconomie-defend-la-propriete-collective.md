---
site: numerama.com
title: "Elinor Ostrom, Prix Nobel de l'économie, défend la propriété collective"
author: Guillaume Champeau
date: 2009-10-12
href: http://www.numerama.com/magazine/14207-elinor-ostrom-prix-nobel-de-l-economie-defend-la-propriete-collective.html
tags:
- Partage du savoir
- Philosophie GNU
---

> [...] Jamais cependant Elinor Ostrom ne défend dans ses travaux la suppression de toute propriété, qui serait une hérésie économique. Elle met en avant une sorte de co-gestion au bénéfice du plus grand nombre. En somme, elle défend la co-propriété des biens communs lorsqu'ils peuvent bénéficier au plus grand nombre, mais prévient qu'une telle co-propriété a besoin pour être efficace de règlements de co-propriétaires qui soient respectés.
> Ce qui est le rôle des licences libres de type Creative Commons ou GPL, que les tribunaux font désormais respecter.
