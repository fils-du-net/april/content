---
site: ecrans.fr
title: "Le logiciel open-source se convertit à l’esprit d’entreprise"
author: Erwan Cario 
date: 2009-10-07
href: http://www.ecrans.fr/Le-logiciel-open-source-se,8291.html
tags:
- Le Logiciel Libre
- Entreprise
---

> D’habitude, lorsqu’on se rend à une manifestation autour du logiciel libre, on se retrouve avec une horde d’informaticiens chevronnés qui aident les débutants à installer un système libre dans une ambiance très coca-rillettes-ordinateurs portables. Du coup, lorsqu’on lit l’adresse de l’Open World Forum, avenue George-V à Paris, à deux pas des Champs-Elysées, il y a comme un air de changement.
