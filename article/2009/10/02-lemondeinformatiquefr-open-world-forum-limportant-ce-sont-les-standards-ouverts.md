---
site: lemondeinformatique.fr
title: "Open World Forum : L'important, ce sont les standards ouverts"
author: Olivier Rafal
date: 2009-10-02
href: http://www.lemondeinformatique.fr/actualites/lire-open-world-forum-l-important-ce-sont-les-standards-ouverts-29216.html
tags:
- Administration
- Standards
---

> [...] Pour Pim Bliek, monsieur standards et logiciels ouverts des Pays-Bas, le coût n'est qu'un facteur secondaire dans le choix de l'Open Source. "Cela a un intérêt dans le long terme, a-t-il dit, mais même si c'était à coût équivalent, le choix des standards ouverts resterait." Il a donné l'exemple de la migration des données des citoyens déménageant d'un endroit à l'autre : "avant, il fallait imprimer tout le dossier, aujourd'hui, cela peut se faire électroniquement."
