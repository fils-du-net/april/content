---
site: lemondeinformatique.fr
title: "OpenWorld 2009 : Oracle et Sun s'auto-félicitent d'un rachat qui reste à valider"
author: Emmanuelle Delsol 
date: 2009-10-12
href: http://www.lemondeinformatique.fr/actualites/lire-openworld-2009-oracle-et-sun-s-auto-felicitent-d-un-rachat-qui-reste-a-valider-29265.html
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] Oracle a aussi promis d'augmenter ses investissements dans MySQL, la base Open Source de Sun. Il a rappelé que, alors que tout le monde pensait qu'il allait le 'tuer', il avait continué de supporter le moteur transactionnel Innobase de MySQL. Pour évoquer Java, morceau de choix du package hérité de Sun, Scott McNealy a appelé sur scène celui que l'on considère comme l'un des pères du langage, James Gosling, vice-président de Sun.
