---
site: progilibre.com
title: "L’open source cantonné aux projets gadgets pour certains grands dsi "
author: Philippe Nieuwbourg
date: 2009-10-16
href: http://www.progilibre.com/L-open-source-cantonne-aux-projets-gadgets-pour-certains-grands-DSI_a957.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> [...] Pour Jean Leroux, qui s’exprimait plus en DSI qu’en Président de l’USF sur ce sujet, « je n’envisage pas de mettre du logiciel libre dans le moteur de mon système d’information ». [...] « Les logiciels sont intéressants, mais je les réserverais à la moumoute autour du volant ou pour personnaliser mes appuies-têtes », précise-t-il avec ironie.
