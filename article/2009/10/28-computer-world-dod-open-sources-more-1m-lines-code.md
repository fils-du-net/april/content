---
site: computerworld.com
title: "DOD open-sources more than 1M lines of code"
author: Patrick Thibodeau
date: 2009-10-28
href: http://www.computerworld.com/s/article/9140053/DOD_open_sources_more_than_1M_lines_of_code
tags:
- Administration
- Interopérabilité
- Institutions
---

> Cet article en anglais présente la politique du Département de la Défense américain qui publie de plus en plus ces logiciels sous licences libres :
