---
site: "generation-nt.com"
title: "Symbian : noyau open source EKA2 livré avec 9 mois d'avance"
author: Christian D. 
date: 2009-10-22
href: http://www.generation-nt.com/symbian-open-source-kernel-noyau-see-eka2-actualite-893781.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Lentement mais sûrement, le système d'exploitation mobile Symbian prépare sa conversion d'un OS propriétaire à un modèle open source. Enfin, pas si lentement que ça puisque la Symbian Foundation annonce avoir finalisé le noyau EKA2 du futur système...avec neuf mois d'avance.
