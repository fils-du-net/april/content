---
site: "presence-pc.com"
title: "Linux alternatif pour les télévisions Samsung"
author: Pierre Dandumont 
date: 2009-10-19
href: http://www.presence-pc.com/actualite/linux-samsung-36822/
tags:
- Le Logiciel Libre
- Entreprise
---

> Sous quel système fonctionne votre téléviseur ? S'il est récent et de marque Samsung, il fonctionne sous... Linux. Et certains utilisateurs ont donc décidé de profiter de ce système pour améliorer les fonctions des téléviseurs. Il existe maintenant un firmware alternatif, SamyGo, qui permet de débloquer certaines fonctions et en ajoute d'autres.
