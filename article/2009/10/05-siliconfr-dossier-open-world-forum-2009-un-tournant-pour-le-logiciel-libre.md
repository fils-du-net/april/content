---
site: silicon.fr
title: "Dossier Open World Forum 2009 : un tournant pour le logiciel libre?"
author: David Feugey
date: 2009-10-05
href: http://www.silicon.fr/fr/news/2009/10/05/dossier_open_world_forum_2009___un_tournant_pour_le_logiciel_libre__
tags:
- Le Logiciel Libre
---

> [...] la plupart des intervenants que nous avons interrogés nous confirment que la France est très en avance en terme d’adoption de l’open source. Alors que la plupart des pays se tâtent encore, l’Hexagone est en pleine phase d’industrialisation des solutions libres. Un mouvement qui a impressionné le quart de visiteurs étrangers présent à ce rassemblement… mais aussi nos confrères de la presse écrite, le monde open source décrochant la une du journal La Tribune. Inattendu.
