---
site: "cio-online.com"
title: "La région Ile de France équipe les lycées d'un ENT"
author: Bertrand Lemaire 
date: 2009-10-27
href: http://www.cio-online.com/actualites/lire-la-region-ile-de-france-equipe-les-lycees-d-un-ent-2494.html
tags:
- Le Logiciel Libre
- Administration
---

> A partir de début 2010, les 472 lycées d'Ile de France seront équipés d'un environnement numérique de travail (ENT) pour un budget global d'environ 20 millions d'euros. Soixante lycées seront équipés dans un premier temps.
>  L'ENT est destiné autant aux lycéens qu'aux enseignants, personnels administratifs et parents. Il comprend bien entendu des fonction de communication de base (messagerie) que collaboratives (espaces de partage de ressources...) et des services liés à la scolarité (consultation des relevés de notes, de l'emploi du temps...).
