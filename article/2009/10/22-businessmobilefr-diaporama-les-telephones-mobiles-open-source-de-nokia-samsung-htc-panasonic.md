---
site: businessmobile.fr
title: "Diaporama : les téléphones mobiles open source de Nokia, Samsung, HTC, Panasonic..."
author: Olivier Chicheportiche
date: 2009-10-22
href: http://www.businessmobile.fr/actualites/technologies/0,3800003790,39710031,00.htm
tags:
- Le Logiciel Libre
- Économie
- Interopérabilité
---

> [...] Le décollage a eu lieu. Avec l'essor de Google Android mais aussi grâce aux initiatives de la Fondation Limo et d'autres (Lips...), et dans une certaine mesure de Symbian, la part de marché des smartphones open source ne cesse de progresser.
> Ces terminaux ont aujourd'hui le vent en poupe, notamment en Asie. Selon Juniper Research, il devrait se vendre 223 millions de mobiles animés d'un système d'exploitation libre en 2013 contre 106 millions cette année.
