---
site: zdnet.fr
title: "Carla Bruni sous WordPress, logiciel libre dans Femme Actuelle, Women and Mozilla..."
author: Thierry Noisette
date: 2009-10-22
href: http://www.zdnet.fr/blogs/l-esprit-libre/carla-bruni-sous-wordpress-logiciel-libre-dans-femme-actuelle-women-and-mozilla-39710067.htm
tags:
- Le Logiciel Libre
- Sensibilisation
---

> [...] Des femmes, un panda roux et un dinosaure. Le logiciel libre manque de femmes, mais des femmes réagissent. Notamment au sein de Womoz, pour Women &amp; Mozilla, un petit groupe qui s'est constitué récemment sur le sujet. Womoz s'est doté d'un blog (grâce auquel j'ai découvert, tout se tient, la page de Femme Actuelle citée ci-dessus) et accueille les bonnes volontés sur le sujet. Comme on dit chez Wikipédia, n'hésitez pas!
