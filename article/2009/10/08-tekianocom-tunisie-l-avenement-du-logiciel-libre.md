---
site: tekiano.com
title: "Tunisie : L’avènement du logiciel libre ?"
author: MBH &amp; WN
date: 2009-10-08
href: http://www.tekiano.com/informatique/informatik-news/3-4-1171/tunisie-l-avenement-du-logiciel-libre-.html
tags:
- Le Logiciel Libre
- Entreprise
---

> «Nous avons pu tester plusieurs solutions payantes et gratuites et on a constaté qu'il y aucune différence pour ne pas dire que ces dernières sont meilleures que les produits sous licence. Qui plus est, la communauté du libre est très active sur le web et assure l'amélioration des logiciels Open source à un rythme élevé. Nous ne rencontrons aucune difficulté à trouver les dernières mises à jour de sécurités pour les logiciels qu'on a adoptés.
