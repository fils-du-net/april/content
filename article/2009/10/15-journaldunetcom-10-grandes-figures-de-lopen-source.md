---
site: journaldunet.com
title: "10 grandes figures de l'Open Source"
author: La rédaction
date: 2009-10-15
href: http://www.journaldunet.com/solutions/acteurs/selection/10-grandes-figures-de-l-open-source/10-grandes-figures-de-l-open-source.shtml?f_id_newsletter=1801
tags:
- Le Logiciel Libre
---

> Le monde du libre regorge de personnalités qui ont marqué leur temps. Nombreuses sont les technologies et logiciels Open Source à avoir bénéficié de leurs talents.
