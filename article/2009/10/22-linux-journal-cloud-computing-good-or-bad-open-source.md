---
site: linuxjournal.com
title: "Cloud Computing: Good or Bad for Open Source?"
author: Glyn Moody
date: 2009-10-22
href: http://www.linuxjournal.com/content/cloud-computing-good-or-bad-open-source
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Informatique-deloyale
- Informatique en nuage
---

> Cet article en anglais propose une reflexion sur le logiciel libre et le Cloud Computing :
