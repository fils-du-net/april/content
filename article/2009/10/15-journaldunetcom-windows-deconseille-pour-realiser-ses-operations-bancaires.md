---
site: journaldunet.com
title: "Windows déconseillé pour réaliser ses opérations bancaires"
author: La rédaction
date: 2009-10-15
href: http://www.journaldunet.com/solutions/breve/securite/42603/windows-deconseille-pour-realiser-ses-operations-bancaires.shtml
tags:
- Logiciels privateurs
---

> La police informatique de Nouvelle-Galles du Sud en Australie est formelle : si l'on souhaite réaliser des opérations bancaires par Internet, mieux vaut ne pas utiliser Windows. Et cela pour éviter le phishing, véritable fléau sur la Toile. Les autorités australiennes préconisent le changement pour ces types d'opérations, en utilisant notamment un Live CD de Linux.
