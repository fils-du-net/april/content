---
site: zdnet.fr
title: "Chris DiBona, Google : \"Nous publions probablement plus de logiciels libres que n'importe qui\""
author: Philippe Davy
date: 2009-10-01
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39708215,00.htm
tags:
- Le Logiciel Libre
- Entreprise
- Internet
---

> Peut on dire que Google et le logiciel libre sont cousins ?
> J'ai toujours dit que le logiciel libre et Internet n'auraient pas pu exister l'un sans l'autre, ils ont grandi ensemble. Et puis Google est arrivé : Internet est le terrain sur lequel il s'est développé, c'est sa source de données et donc de revenus par la publicité. Les trois sont effectivement connectés.
