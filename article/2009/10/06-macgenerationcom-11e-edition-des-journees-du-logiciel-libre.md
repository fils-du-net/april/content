---
site: macgeneration.com
title: "11e édition des Journées du Logiciel Libre"
author: Arnauld de La Grandière
date: 2009-10-06
href: http://www.macgeneration.com/news/voir/136734/11e-edition-des-journees-du-logiciel-libre
tags:
- Le Logiciel Libre
---

> Les 15, 16, et 17 octobre se tiendront à Lyon-Villeurbanne les onzièmes Journées du Logiciel Libre.
> Vous pourrez y retrouver une trentaine d'exposants et suivre une cinquantaine de conférences sur la place du logiciel libre dans notre quotidien.
