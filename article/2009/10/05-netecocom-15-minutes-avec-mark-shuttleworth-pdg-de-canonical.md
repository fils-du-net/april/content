---
site: neteco.com
title: "15 minutes avec Mark Shuttleworth, PDG de Canonical"
author: Guillaume Belfiore
date: 2009-10-05
href: http://www.neteco.com/303290-15-minutes-mark-shuttleworth-pdg-canonical.html
tags:
- Le Logiciel Libre
---

> Comment voyez-vous Ubuntu dans cinq ou dix ans ?
> MS : Pour commencer, je souhaiterais voir les logiciels open source par défaut sur l'ensemble des ordinateurs. Ils permettent d'obtenir de meilleurs résultats tout en assurant une bonne stabilité. Et j'aimerais qu'Ubuntu représente le meilleur de cette idée. Aussi vous savez Canonical n'est pas une oeuvre caritative, nous fournissons un système gratuitement mais il y a aussi différents partenariats. J'aimerais que d'ici 5 ou 10 ans la balance entre ces deux segments se stabilise.
