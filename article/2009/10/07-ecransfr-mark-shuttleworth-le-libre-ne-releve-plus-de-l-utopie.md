---
site: ecrans.fr
title: "Mark Shuttleworth : « Le libre ne relève plus de l’utopie »"
author: Erwan Cario
date: 2009-10-07
href: http://www.ecrans.fr/Mark-Shuttleworth-Le-libre-ne,8290.html
tags:
- Le Logiciel Libre
- Économie
---

> Pourquoi avez-vous décidé de vous lancer dans l’aventure Ubuntu  ?
> Pour moi, c’est la convergence de trois intérêts. Je m’intéresse à l’économie, aux questions sociétales et à la technologie. Le logiciel libre représente la partie logicielle d’un changement majeur dans la société. Un changement dû au fait que les gens sont aujourd’hui connectés les uns aux autres. Connectés par la voix, mais aussi dans leur travail, dans leurs idées, dans leur réflexion. Et ces connexions entraînent un bouleversement dans l’économie, dans la poli­tique, dans la société civile, dans les contenus…
