---
site: sudouest.com
title: "François Pellegrini : Homme du libre"
author: Gilles Guitton
date: 2009-10-02
href: http://www.sudouest.com/gironde/actualite/article/724284/mil/5182651.html
tags:
- Le Logiciel Libre
- Internet
- HADOPI
---

> [...] Il défend l'idée de la « licence globale » de téléchargement, parce que « c'est l'option qui n'est jamais considérée. Quant à la forme, aux modalités, à la redistribution, il y a matière à réfléchir ». La liberté ? Elle ne brille pas dans Hadopi à ses yeux : « Le Conseil constitutionnel ne l'a que partiellement relevée. C'est une loi qui détruit le principe de la présomption d'innocence.
