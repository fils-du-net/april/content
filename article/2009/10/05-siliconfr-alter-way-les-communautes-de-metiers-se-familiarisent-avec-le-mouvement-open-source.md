---
site: silicon.fr
title: "Alter Way : «Les communautés de métiers se familiarisent avec le mouvement open source»"
author: David Feugey 
date: 2009-10-05
href: http://www.silicon.fr/fr/news/2009/10/05/alter_way____les_communautes_de_metiers_se_familiarisent_avec_le_mouvement_open_source_
tags:
- Le Logiciel Libre
- Entreprise
---

> Constatez-vous certains changements dans la sphère du logiciel libre ?
> Le monde PHP se professionnalise rapidement. Nous avons d'ailleurs mis en ligne un livre blanc sur ce sujet (NDLR Industrialisation PHP). Nous notons également une montée en puissance des clubs d'utilisateurs ainsi que des communautés de métiers (milieu médical, télécommunications, mobilité), qui adoptent rapidement l'open source.
