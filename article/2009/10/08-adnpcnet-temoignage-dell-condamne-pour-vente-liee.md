---
site: adnpc.net
title: "Témoignage : Dell condamné pour vente liée"
author: Julien Thérin
date: 2009-10-08
href: http://www.adnpc.net/articles/142-temoignage-dell-condamne-pour-vente-liee/1-introduction.html
tags:
- Vente liée
---

> Nous avons ainsi fait l'achat en mars 2008 de deux ordinateurs portables Dell XPS M1530 et Inspiron 1525 pour un total d'environ 1950 euros. [...] Il nous aura fallu pas loin d'un an pour se faire rembourser auprès de Dell : voici notre témoignage. [...] Rendu du jugement le 9 mars 2009 [...] monsieur le Juge [...] a conclu que les méthodes employées par Dell peuvent être entendues comme une pratique de vente liée ou subordonnée.
