---
site: "solutions-logiciels.com"
title: "Enquête sur l'adoption et la perception de l'Open Source"
author: Frédéric Mazué
date: 2009-10-23
href: http://www.solutions-logiciels.com/actualites.php?titre=Enquete-sur-ladoption-et-la-perception-de-lOpen-Source&actu=6039
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] « L’enquête d’Actuate s’inscrit dans le mouvement des leaders du marché qui viennent des Etats-Unis, de France ou du Royaume-Uni, et qui demandent une plus grande utilisation des logiciels libres. Nous continuons de montrer la voie vers une adoption et une acceptation encore plus importantes de l’Open Source.
