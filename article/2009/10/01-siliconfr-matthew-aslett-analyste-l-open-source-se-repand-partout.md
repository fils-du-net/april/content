---
site: silicon.fr
title: "Matthew Aslett (analyste) : «L’open source se répand partout»"
author: David Feugey
date: 2009-10-01
href: http://www.silicon.fr/fr/news/2009/10/01/matthew_aslett__analyste_____l_open_source_se_repand_partout_
tags:
- Le Logiciel Libre
- Entreprise
- Administration
---

> Lors de la séance d’ouverture, Matthew Aslett (notre photo), analyste pour le 451 Group, rappelle l’historique du mouvement open source : « Au cours des années 80, les choses se sont mises en place, afin de fournir une alternative aux offres propriétaires des grands éditeurs. Dans les années 90, l’adoption de Linux et d’Apache a marqué le démarrage réel du marché de l’open source. Il a fallu toutefois attendre les années 2000 pour que l’open source soit reconnu comme étant un véritable moteur de l’innovation. »
