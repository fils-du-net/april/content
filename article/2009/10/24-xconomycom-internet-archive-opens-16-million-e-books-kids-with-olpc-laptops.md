---
site: xconomy.com
title: "Internet Archive Opens 1.6 Million E-Books to Kids with OLPC Laptops"
author: Wade Roush
date: 2009-10-24
href: http://www.xconomy.com/boston/2009/10/24/internet-archive-opens-1-6-million-e-books-to-olpc-laptops/
tags:
- Partage du savoir
---

> Traduction de Lionel Allorge :
> L'ensemble des 1,6 millions de livres numérisés par l'Internet Archive, l'organisation, basée à San Francisco, pour le partage de la connaissance, sera disponible pour l'ensemble des enfants de la planète qui ont des ordinateurs fournis par l'organisation One Laptop Per Child Foundation (OLPC).
