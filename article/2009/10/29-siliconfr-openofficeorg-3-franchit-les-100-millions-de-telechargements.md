---
site: silicon.fr
title: "OpenOffice.org 3 franchit les 100 millions de téléchargements"
author: David Feugey
date: 2009-10-29
href: http://www.silicon.fr/fr/news/2009/10/29/openoffice_org_3_franchit_les_100_millions_de_telechargements
tags:
- Le Logiciel Libre
- Entreprise
- Économie
- Interopérabilité
---

> Le succès de la suite bureautique open source OpenOffice.org 3 se confirme. Ainsi, le cap des 100 millions de téléchargements a été franchi récemment. Un beau cadeau pour ce projet, qui vient de souffler sa neuvième bougie.
> Ce chiffre est toutefois à prendre avec des pincettes. De fait, il cumule les téléchargements d’OpenOffice.org 3.0 (près de 60 millions) et d’OpenOffice.org 3.1 (plus de 40 millions). Nous pouvons supposer qu’une écrasante majorité des moutures d’OpenOffice.org 3.1 sont des mises à jour de la version précédente.
