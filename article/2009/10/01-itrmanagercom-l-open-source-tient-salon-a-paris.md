---
site: itrmanager.com
title: "L’Open Source tient salon à Paris"
author: La rédaction
date: 2009-10-01
href: http://www.itrmanager.com/articles/95733/open-world-forum-br-open-source-tient-salon-paris.html
tags:
- Le Logiciel Libre
- Entreprise
- Administration
---

> [...] A commencer par la brillante intervention de Jacques Attali, forcément de haut vol et voyant les choses de très haut. Le Président de PlaNet Finance, la plus importante ONG du monde du micro crédit, a souhaité établir un parallèle entre le logiciel libre et ce qu'il appelle le « social business ». « Une entreprise appartenant à cet univers du social business renverse la proposition des entreprises traditionnelles dont le finalité est de faire du profit en vendant des produits. Dans l'univers du social business, le profit n'est qu'un moyen pour diffuser des produits ».
