---
site: zdnet.fr
title: "Europe : Internet Explorer continue à perdre du terrain malgré la version 8"
author: Olivier Chicheportiche
date: 2009-10-27
href: http://www.zdnet.fr/actualites/internet/0,39020774,39710253,00.htm
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
---

> Les mois se suivent et se ressemblent pour Internet Explorer en Europe. La dernière étude de AT Internet Institute en septembre (sur 23 pays européens) illustre une nouvelle fois la perte de vitesse du navigateur de Microsoft.
> [...] Ainsi, en Hongrie, en Slovaquie, en Bulgarie et en Autriche, Internet Explorer enregistre le plus fort recul de sa part de visites, en moyenne pour un site web. En Hongrie, Mozilla prend la place de leader à Internet Explorer qui perd 9,3 points en part de visites entre mars et septembre 2009. Même constat en Slovaquie où Internet Explorer perd 6,9 points et concède ainsi sa 1ère place à Mozilla.
