---
site: progilibre.com
title: "L'open source dans sa logique mutualiste, un vecteur de compétitivité."
author: Patrice Bertrand
date: 2009-10-23
href: http://www.progilibre.com/L-open-source-dans-sa-logique-mutualiste,-un-vecteur-de-competitivite_a965.html
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] Pour ces entreprises, Linux fonctionne sur une logique de coopérative agricole, une logique de mutualisation des moyens donc de division des coûts. Une variante de ce qu'on appelle parfois « coopétition », c'est à dire que l'intérêt bien réfléchi de chacun conduit à identifier un terrain de coopération dans un contexte général de compétition.
