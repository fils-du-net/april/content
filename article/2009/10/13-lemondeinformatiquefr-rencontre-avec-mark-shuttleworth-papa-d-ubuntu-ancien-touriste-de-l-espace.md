---
site: lemondeinformatique.fr
title: "Rencontre avec Mark Shuttleworth, papa d’Ubuntu, ancien touriste de l’espace"
author: Olivier Rafal
date: 2009-10-13
href: http://blog1.lemondeinformatique.fr/ingenierie_logicielle/2009/10/rencontre-avec-mark-shuttleworth-papa-dubuntu-ancien-touriste-de-lespace.html
tags:
- Le Logiciel Libre
---

> Durant votre discours à l’Open World Forum, vous avez plaidé pour des développements de produits coordonnés dans le monde de l’Open Source. Voilà un projet très ambitieux…
> Oui, l’objectif est très ambitieux, et il y a de nombreux obstacles sur la route, mais les bénéfices sont clairs : le test en serait grandement facilité, car on s’appuierait sur des logiciels stabilisés et des standards, cela nous simplifierait grandement la vie à tous, et donnerait aux logiciels libres une bien meilleure visibilité.
