---
site: lemonde.fr
title: "Mark Shuttleworth : \"Ubuntu croît de 100 % chaque année\""
author: Damien Leloup
date: 2009-10-29
href: http://www.lemonde.fr/technologies/article/2009/10/29/mark-shuttleworth-ubuntu-croit-de-100-chaque-annee_1260126_651865.html
tags:
- Le Logiciel Libre
- Internet
---

> [...] Bien sûr, en ce qui concerne les ordinateurs, nous sommes encore très petits comparés à Windows et à Mac OS. Mais nous connaissons chaque année une croissance de 100 % en nombre d'utilisateurs, ce qui est un véritable succès [soit environ 16 millions d'utilisateurs aujourd'hui contre 8 millions fin 2008, NDLR]. Nous sommes très heureux d'avoir fait des progrès en termes de facilité d'installation, de simplicité d'utilisation, mais il nous reste encore beaucoup de choses à accomplir.
