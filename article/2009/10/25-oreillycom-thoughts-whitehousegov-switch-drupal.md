---
site: oreilly.com
title: "Thoughts on the Whitehouse.gov switch to Drupal"
author: Tim O'Reilly
date: 2009-10-25
href: http://radar.oreilly.com/2009/10/whitehouse-switch-drupal-opensource.html
tags:
- Le Logiciel Libre
- Administration
---

> Traduction proposée par Paul Muraille :
> Hier, l'équipe New Media de la Maison Blanche a annoncé par le biais de
> l'agence Associated Press que le site whitehouse.gov exploitait
> désormais le système de gestion de contenu open source Drupal.
> ...
> Cette initiative constitue sans conteste une avancée de poids pour
> l'Open Source. Comme me l'écrivait dans un courriel John Scott d'Open
> Source for America (association qui milite pour l'adoption de l'Open
> Source dans l'administration et dont je suis membre du Board of
> Advisors) : « C'est formidable non seulement en ce qui concerne
> l'utilisation des logiciels open source, mais aussi en termes de
> validation du modèle de développement open source. L'adoption par la
> Maison Blanche d'un logiciel communautaire est un exemple marquant pour
> le reste du gouvernement ». C'est tout à fait vrai : même si l'Open
> Source est déjà répandu dans l'administration fédérale, son adoption par
> la Maison Blanche elle-même apparaîtra à coup sûr comme une invitation à
> une adoption encore plus large.
> ...
> L'origine publique du code pourrait être perçue comme un risque pour la
> sécurité. C'est exactement l'inverse, avancent les spécialistes de la
> sphère gouvernementale et en dehors. Les programmeurs travaillant en
> coopération pour traquer les erreurs ou les possibilités d'exploitation
> du code web à des fins nuisibles, le produit final n'en est que plus sûr.
