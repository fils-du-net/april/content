---
site: zdnet.fr
title: "Disparition de Francis Muguet, concepteur du mécénat global"
author: Thierry Noisette
date: 2009-10-18
href: http://www.zdnet.fr/blogs/l-esprit-libre/disparition-de-francis-muguet-concepteur-du-mecenat-global-39709570.htm
tags:
- Internet
- Partage du savoir
- HADOPI
---

> Francis Muguet a été trouvé mort chez lui mercredi 14 octobre. Il avait 55 ans. Son nom est associé depuis mars dernier à son concept de "mécénat global".
