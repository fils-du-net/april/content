---
site: itrmanager.com
title: "La crise, l’informatique, les ingénieurs, et l’innovation"
author: La rédaction
date: 2009-10-09
href: http://www.itrmanager.com/articles/96034/crise-informatique-ingenieurs-innovation.html
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] Dans les orientations stratégiques de System@tic, le logiciel libre tient une place éminente. « Lorsque le Premier ministre nous a demandé il y a deux ans d'intégrer le logiciel libre, cela nous a fait très peur et nous avions une image d'un milieu regroupant des esprits libertaires, ne croyant pas à la propriété intellectuelle et appartenant à une nuée de petites entreprises. Mais nous avons rapidement changé d'avis. Il faut savoir que la France est leader dans l'Open Source et que le libre constitue un formidable outil de compétitivité », explique-t-il.
