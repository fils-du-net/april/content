---
site: silicon.fr
title: "Red Hat : «La crise oblige les professionnels à sortir de leur zone de confort»"
author: David Feugey
date: 2009-10-05
href: http://www.silicon.fr/fr/news/2009/10/05/red_hat____la_crise_oblige_les_professionnels_a_sortir_de_leur_zone_de_confort_
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] La crise est difficile, mais c’est un vecteur d’accélération fort pour l'open source, car il oblige les utilisateurs à sortir de leur zone de confort. La résistance naturelle au changement des professionnels est remise en cause par les problèmes de budget. La recherche d’alternatives aux solutions propriétaires s’intensifie donc, en particulier sur les parties les moins visibles du système d’information, bref, sur l'infrastructure.
