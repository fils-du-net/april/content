---
site: lesechos.fr
title: "La vague Android s'apprête à déferler sur la planète mobile"
author: Guillaume de Calignon
date: 2009-10-27
href: http://www.lesechos.fr/investisseurs/actualites-boursieres/020190873529-la-vague-android-s-apprete-a-deferler-sur-la-planete-mobile.htm
tags:
- Le Logiciel Libre
- Entreprise
---

> Un an après les débuts du système d'exploitation pour mobiles de Google , une douzaine de modèles de téléphones utilisent déjà Android. L'appétit des constructeurs asiatiques et de Motorola est très fort.
> [...] En France, une dizaine de modèles différents utilisant Android pourraient être commercialisés pour Noël. Pour l'institut Gartner, en 2012, Android pourrait ainsi équiper 18 % des « smartphones » vendus dans le monde cette année-là. Derrière Nokia, mais devant BlackBerry, l'iPhone et Windows Mobile.
