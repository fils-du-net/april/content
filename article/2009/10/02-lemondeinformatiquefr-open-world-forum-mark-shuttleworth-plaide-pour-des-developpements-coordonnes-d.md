---
site: lemondeinformatique.fr
title: "Open World Forum : Mark Shuttleworth plaide pour des développements coordonnés dans l'Open Source"
author: Olivier Rafal
date: 2009-10-02
href: http://www.lemondeinformatique.fr/actualites/lire-open-world-forum-mark-shuttleworth-plaide-pour-des-developpements-coordonnes-dans-l-open-source-29220.html
tags:
- Le Logiciel Libre
- Sensibilisation
---

> Pourquoi les nouvelles versions des distributions Linux se succèdent-elles à ce rythme effréné, sans aucune concertation ? Pourquoi les développeurs de l'Open Source persistent-ils à croire que leur code est bon à partir du moment où il est plus rapide, plus efficace, mais pas utilisable par le commun des mortels ? En cette seconde journée de l'Open World Forum, Mark Shuttleworth aura amené le public à se poser ces questions, à prendre du recul.
