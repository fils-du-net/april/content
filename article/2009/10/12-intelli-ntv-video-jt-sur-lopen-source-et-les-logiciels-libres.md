---
site: "intelli-n.tv"
title: "Vidéo: JT sur l'open source et les logiciels libres"
author: La rédaction
date: 2009-10-12
href: http://www.intelli-n.tv/JT/Video-JT-sur-l-open-source-et-les-logiciels-libres/
tags:
- April
- Video
---

> Le JT, bi-mensuel, d'Intelli'N TV est consacré aux logiciels libres et à l'open source, il vous propose des actualités marquantes sur ces technologies. A l'occasion de cette édition nous vous présentons deux nouvelles chroniques: Un Zoom de l'APRIL en partenariat avec l'association et une autre qui mettra en avant l'art et les projets libres en général.
> [...] Voici la version du JT en format libre OGV :  http://blip.tv/file/2709840?filename=IntelliNTV-JTSurLesLogicielsLibresE...
