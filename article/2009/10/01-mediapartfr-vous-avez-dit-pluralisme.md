---
site: mediapart.fr
title: "Vous avez dit pluralisme ?"
author: qdemouliere  
date: 2009-10-01
href: http://www.mediapart.fr/club/blog/qdemouliere/011009/vous-avez-dit-pluralisme
tags:
- Vente liée
---

> [...] Sachez qu'à l'heure actuelle, le système commercial en place, nous impose des systèmes d'exploitation ainsi que des logiciels sans nous demander notre avis (navigateur internet entre autres choses). "Mais ce n'est pas grave, me direz-vous, on ne paye pas le système d'exploitation ni les logiciels puisqu'ils sont inclus avec l'ordinateur !" Et bien détrompez-vous, ces programmes sont pris en compte dans le prix global de la machine.
