---
site: "presence-pc.com"
title: "Les internautes les plus lents utilisent Linux"
author: Matthieu Lamelot 
date: 2009-10-06
href: http://www.presence-pc.com/actualite/barometre-vitesse-internet-36667/
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
---

> Navigateurs internet, fournisseurs d'accès, systèmes d'exploitation, tous se prétendent toujours plus rapides les uns que les autres. Mais, derrière les promesses commerciales, quelle configuration offre réellement la plus grande vitesse de navigation ? Pour le savoir, la rédaction s'est associée à la société ip-label.newtest pour analyser les performances des millions d'internautes qui ont visité Tom's Guide pendant le mois de septembre. Les résultats sont passionnants et tordent le cou à quelques idées reçues.
