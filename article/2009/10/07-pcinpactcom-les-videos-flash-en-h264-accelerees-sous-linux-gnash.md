---
site: pcinpact.com
title: "Les vidéos flash en H.264 accélérées sous Linux via Gnash"
author: David Legrand
date: 2009-10-07
href: http://www.pcinpact.com/actu/news/53435-splitted-desktop-gnash-vaapi-flash.htm
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Video
---

> Alors qu'Adobe annonçait il y a de cela quelques jours que l'accélération des vidéos H.264 seraient enfin de la partie avec la prochaine mouture 10.1 de Flash, Splitted-Desktop Systems, une jeune société française, vient de nous faire savoir qu'il en serait de même sous Linux via Gnash, une version Open Source de Flash.
