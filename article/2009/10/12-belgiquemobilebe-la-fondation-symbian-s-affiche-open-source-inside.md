---
site: belgiquemobile.be
title: "La Fondation Symbian s’affiche « Open Source Inside »"
author: Cedric Godart
date: 2009-10-12
href: http://www.belgiquemobile.be/2009/10/12/la-fondation-symbian-saffiche-open-source-inside/
tags:
- Le Logiciel Libre
- Entreprise
---

> La Symbian Foundation fait la part belle à l’Open Source. C’est ce que l’on apprend dans un billet intitulé « Open Source inside », publié sur son blog officiel.
> Ce n’est pas tous les jours qu’une start-up publie son bulletin de santé informatique. Ainsi, une véritable autopsie de ses outils internes, allant des serveurs aux postes de travail, est présentée sur le blog officiel du système d’exploitation mobile, désormais entièrement libre et ouvert.
