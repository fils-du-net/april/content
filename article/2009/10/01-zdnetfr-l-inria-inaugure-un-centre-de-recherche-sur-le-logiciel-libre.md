---
site: zdnet.fr
title: "L’Inria inaugure un centre de recherche sur le logiciel libre"
author: La rédaction
date: 2009-10-01
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39708326,00.htm
tags:
- Le Logiciel Libre
- Administration
---

> L'institut national de recherche en informatique et automatique, l'Inria, vient d'annoncer à l'occasion du salon Open World Forum 2009, la création d'une nouvelle organisation dans le domaine du logiciel libre : le CIRILL.
> Ce Centre d'Innovation et de Recherche en Informatique sur le logiciel libre, crée en partenariat avec des acteurs de la recherche publique et du logiciel libre, se veut  un lieu d'expertises scientifiques, technologiques et industrielles.
