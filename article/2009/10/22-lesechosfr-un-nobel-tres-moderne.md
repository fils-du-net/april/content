---
site: lesechos.fr
title: "Un Nobel très moderne"
author: Françoise Benhamou
date: 2009-10-22
href: http://www.lesechos.fr/info/analyses/020181690841-un-nobel-tres-moderne.htm
tags:
- Le Logiciel Libre
---

> [...] De même, l'aventure des logiciels libres, dont le code-source est rendu publiquement disponible et peut être librement réutilisé, est celle de la construction collective d'une alternative à la propension de quelques-uns à dominer le marché des technologies numériques. La force des logiciels libres procède de la mise en commun des connaissances qui permet d'améliorer la qualité du produit ; l'administration française l'a compris lorsqu'elle a décidé d'adopter le système d'exploitation Linux.
