---
site: impactcampus.qc.ca
title: "Linux à la rescousse de l’Australie"
author: La rédaction
date: 2009-10-13
href: http://www.impactcampus.qc.ca/article.jsp?issue=2009-10-13&article=Linux-a-la-rescousse-de-l_Australie
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Des techniciens en informatique employés par le distributeur d’électricité australien Integral Energy ont habilement déjoué une infection virale qui s’en est pris au système de contrôle du réseau électrique. Grâce au système d’exploitation informatique Linux, ils ont pu empêcher le virus de menacer l’alimentation électrique d’environ 800 000 foyers dans l’est de l’Australie.
