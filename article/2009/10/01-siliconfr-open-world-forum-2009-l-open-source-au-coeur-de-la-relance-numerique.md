---
site: silicon.fr
title: "Open World Forum 2009 : l’open source au cœur de la relance numérique"
author: David Feugey
date: 2009-10-01
href: http://www.silicon.fr/fr/news/2009/10/01/open_world_forum_2009___l_open_source_au_c_ur_de_la_relance_numerique
tags:
- Le Logiciel Libre
- Administration
- Économie
---

> Lors de la séance d’ouverture du forum mondial de l’open source (Open World Forum). Jean-Louis Missika (notre photo), adjoint au Maire de Paris chargé de l’innovation de la recherche et des universités, a créé la surprise par son avis éclairé : « la France concentre le plus d’utilisateurs de l’open source. C’est un fait que les gens ignorent trop souvent. »
> « C’est également une activité qui ne connait pas la crise… ou presque, poursuit-il. La croissance dans ce secteur sera probablement de 20 % en 2009 comme en 2010. Nous attendons un chiffre d’affaires de 3 milliards d’euros en 2012, contre 1 milliard aujourd’hui. De plus, Paris concentre les deux tiers de l’activité française en terme de logiciels open source. »
