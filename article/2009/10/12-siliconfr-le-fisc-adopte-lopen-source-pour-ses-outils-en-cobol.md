---
site: silicon.fr
title: "Le Fisc adopte l'open source pour ses outils en Cobol"
author: David Feugey 
date: 2009-10-12
href: http://www.silicon.fr/fr/news/2009/10/12/le_fisc_adopte_l_open_source_pour_ses_outils_en_cobol
tags:
- Le Logiciel Libre
- Administration
---

> Afin de réduire les coûts d’exploitation de sa solution de gestion de la TVA, la DGFiP bascule vers un outil de compilation Cobol open source et des serveurs Itanium signés HP.
