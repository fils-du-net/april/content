---
site: lemagit.fr
title: "En France, l'Open Source dit encore une fois merci au secteur public "
author: Cyrille Chausson 
date: 2009-10-26
href: http://www.lemagit.fr/article/france-entreprises-opensource-secteur-public/4640/1/en-france-open-source-dit-encore-une-fois-merci-secteur-public/
tags:
- Le Logiciel Libre
- Administration
---

> Selon la dernière étude de Survey Interactive, sponsorisée par Actuate, l'Open Source est davantage implanté dans le secteur public que dans l'industrie ou les services financiers en France. Face à une montée en puissance du mouvement dans l'hexagone, les entreprises considèrent l'Open Source pour sa gratuité ou la rejette pour ses incompatibilités, le manque de compétences associées disponibles sur le marché ou encore un manque de discernement du support.
