---
site: Framablog
title: "Paris Descartes vend ses étudiants à Micro$oft !"
author: aKa
date: 2009-10-20
href: http://www.framablog.org/index.php/post/2009/10/20/universite-paris-descartes-microsoft
tags:
- Logiciels privateurs
- Éducation
---

> Encore un titre qui ne fait pas dans la dentelle ! Il ne vient pas de moi mais d’un tract qui serait actuellement distribué sur le campus de l’univesité Paris Descartes par le SNESup, le Syndicat national de l’enseignement supérieur (à confirmer). Tract que nous avons reproduit ci-dessous dans son intégralité.
