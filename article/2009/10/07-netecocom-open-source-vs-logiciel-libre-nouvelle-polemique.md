---
site: neteco.com
title: "Open source vs logiciel libre : nouvelle polémique"
author: Guillaume Belfiore
date: 2009-10-07
href: http://www.neteco.com/303812-tensions-scene-open-source.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Microsoft a ouvert  sa fondation open source CodePlex.org le mois dernier. Celle-ci se distingue des autres groupes en prenant en compte les dépôts de licences et les besoins de propriété intellectuelle des éditeurs commerciaux. Père du projet GNU et président de la Free Software Foundation, M.Stallman critique notamment le fait que Microsoft utilise le terme « open source » (logiciel dont le code est ouvert) plutôt que Free Software (logiciel libre) qui met en avant la philosophie de la Free Software Foundation comme « la liberté et la solidarité sociale ».
