---
site: 01net.com
title: "Microsoft ouvre enfin le format .PST d’Outlook"
author: Anicet Mbida
date: 2009-10-27
href: http://pro.01net.com/editorial/507743/microsoft-ouvre-enfin-le-format-pst-d-outlook/
tags:
- Logiciels privateurs
- Interopérabilité
---

> Les données des dossiers personnels Outlook seront facilement accessibles à n'importe quelle application. Mais les usages resteront limités.
> [...] Il n'empêche. Cette plus grande ouverture de Microsoft lui permettra de faire meilleure figure devant l'administration et les organismes très à cheval sur l'interopérabilité et les formats ouverts. Pourtant, beaucoup de chemin reste à parcourir.
