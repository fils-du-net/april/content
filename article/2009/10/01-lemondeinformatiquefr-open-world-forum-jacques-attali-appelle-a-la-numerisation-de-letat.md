---
site: lemondeinformatique.fr
title: "Open World Forum : Jacques Attali appelle à la numérisation de l'Etat"
author: Olivier Rafal 
date: 2009-10-01
href: http://www.lemondeinformatique.fr/actualites/lire-open-world-forum-jacques-attali-appelle-a-la-numerisation-de-l-etat-29213.html
tags:
- Le Logiciel Libre
---

> [...] L'ancien conseiller de François Mitterrand a terminé son allocution par une petite pique, qui lui a sans doute aliéné une grande partie de l'auditoire : "Je suis toujours très méfiant quand je vois employé le mot libre, [...] un logiciel libre, cela correspond à une mystification. On peut dire logiciel ouvert, gratuit, libre de droit... Soyez gentils de ne pas utiliser à tort et à travers le mot, ce sont les gens qui sont libres, pas les choses."
