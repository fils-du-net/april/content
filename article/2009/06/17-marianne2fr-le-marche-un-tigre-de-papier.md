---
site: marianne2.fr
title: "Le marché? Un tigre de papier "
author: Bernard Maris
date: 2009-06-17
href: http://www.marianne2.fr/Le-marche-Un-tigre-de-papier_a180900.html
tags:
- Le Logiciel Libre
- Partage du savoir
---

> Comment se peut-il que des informaticiens, extrêmement talentueux, passent du temps à améliorer pour la collectivité des logiciels, sans en retirer une rémunération ? Bizarre, hein ? Il faut croire que le plaisir de développer est plus fort que l'appât du gain, comme le plaisir de la recherche est plus fort que le profit tiré du brevet résultant de cette même recherche, même si l'un n’exclut pas l'autre. Mais comment mesurer cette richesse ? Comment calculer, évaluer tout ce savoir dû à la coopération bénévole ?
