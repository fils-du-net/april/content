---
site: africanmanager.com
title: "Interview : Jean Michel Armand : » Moins d’off shoring aiderait le logiciel libre en Tunisie » "
author: Haykel Tlili 
date: 2009-06-15
href: http://www.africanmanager.com/articles/123210.html
tags:
- Le Logiciel Libre
- Entreprise
- Économie
---

> Quelles sont vos ambitions en tant que clusters spécialisés dans le logiciel libre
> L’ambition est d’avoir l’opportunité de créer des clusters de logiciels libres. Cela nous permettra de regrouper les entreprises de la spécialité et puis attaquer les marchés ensemble, de façon à fédérer les entreprises et leurs pouvoirs financiers notamment. Aujourd’hui, 200 entreprises sont capables de développer cette industrie de logiciels libres, qui est un marché qui est en train d’exploser ; le chiffre d’affaires du marché des logiciels libres est passé de 2,4 Milliards de dollars en  2007 à 3,8 MD en 2008. Selon les résultats d’une enquête, les entreprises de logiciels libres seront celles qui auront la plus grande capacité à recruter des ingénieurs et des techniciens, ce qui révèle l'importance des perspectives de cette industrie.
