---
site: "generation-nt.com"
title: "L'apparition de Mono dans Debian chagrine Stallman"
author: Jérôme G.
date: 2009-06-29
href: http://www.generation-nt.com/stallman-mono-debian-c-actualite-826901.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Brevets logiciels
- Philosophie GNU
---

> Le gourou du Logiciel Libre voit d'un œil réprobateur l'entrée de Mono au sein du projet Debian. Au-delà de l'implémentation libre de Microsoft .NET, c'est une trop forte dépendance au langage C# de Microsoft qui l'inquiète.
