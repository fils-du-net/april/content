---
site: "e-alsace.net"
title: "APRIL : 5000 membres et un pique-nique"
author: J.D
date: 2009-06-12
href: http://e-alsace.net/index.php/smallnews/get?newsId=1181
tags:
- April
---

> L'April, l'association qui oeuvre en faveur du logiciel libre et des standards ouverts, vient d’atteindre les 5 000 adhérents. À cette occasion un pique-nique est organisé à Strasbourg, une occasion de rencontrer des membres des RMLL 2011 à Strasbourg...
