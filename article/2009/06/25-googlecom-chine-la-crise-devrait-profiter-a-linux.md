---
site: google.com
title: "Chine: la crise devrait profiter à Linux"
author: AFP
date: 2009-06-25
href: http://www.google.com/hostednews/afp/article/ALeqM5hjVNuJYa0sGk5uBvKBwVpsBoQjgA
tags:
- Le Logiciel Libre
- Entreprise
- Licenses
---

> PEKIN (AFP) — La crise économique devrait profiter en Chine au logiciel libre et bon marché Linux, qui y a vu ses revenus progresser de 26% en glissement annuel en 2008, a estimé mercredi le cabinet de marketing IDC
> [...]
> L'étude d'IDC souligne que les vendeurs de Linux ont investi lourdement pour éduquer le consommateur chinois et développer leur marché, avec des résultats visibles.
> Les systèmes libres basés sur Linux sont très populaires en Chine, au Brésil, en Inde et autres pays émergents, selon le distributeur de logiciels libres Novell.
