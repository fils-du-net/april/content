---
site: Le Monde
title: "Faisons passer la politique du système propriétaire à celui du logiciel libre"
author: Daniel Cohn-Bendit
date: 2009-06-15
href: http://www.lemonde.fr/opinions/article/2009/06/15/faisons-passer-la-politique-du-systeme-proprietaire-a-celui-du-logiciel-libre-par-daniel-cohn-bendit_1207001_3232.html
tags:
- Le Logiciel Libre
- Europe
---

> Les idées, pas plus que les personnes, n’appartiennent pas à quelqu’un. Elles ont vocation à circuler librement, à se propager et à évoluer aux contacts des autres. [...]
> Alors, à la lancinante question du “qu’allons-nous faire”, je réponds que nous allons continuer à briser la logique du “système propriétaire” qui domine notre vie politique nationale, tant au niveau global qu’au niveau local, tant par l’Europe qu’à l’occasion des élections régionales. Plus que jamais, nous allons promouvoir la notion de “logiciel libre” appliquée à la politique et à la société.
