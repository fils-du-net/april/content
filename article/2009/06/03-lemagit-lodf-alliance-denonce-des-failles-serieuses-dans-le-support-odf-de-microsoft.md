---
site: LeMagIT
title: "L'ODF Alliance dénonce des failles sérieuses dans le support ODF de Microsoft"
author: Cyrille Chausson, Christophe Bardy
date: 2009-06-03
href: http://www.lemagit.fr/article/microsoft-odf-standards-office/3450/1/l-odf-alliance-denonce-des-failles-serieuses-dans-support-odf-microsoft/
tags:
- Interopérabilité
- Standards
---

> En se basant sur des tests réalisés par un employé d'IBM, l'ODF Alliance -qui compte parmi ses membres fondateurs EMC, IBM, Novell, Oracle, Red Hat ou Sun - reproche à Microsoft d'avoir réalisé une piètre implémentation de la norme ODF dans le SP2 d'Office 2007. Des tests que Microsoft conteste en déclarant qu'on lui reproche surtout de ne pas être compatible avec OpenOffice.
