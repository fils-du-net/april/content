---
site: "Sud-Ouest.com"
title: "C'est ma première Install Party "
author: Emmanuelle Frère
date: 2009-06-19
href: http://www.sudouest.com/gironde/actualite/libournais/article/624433/mil/4678946.html
tags:
- Actualité locale
- Le Logiciel Libre
- Sensibilisation
---

> Pour une bonne Install Party, conviez quelques passionnés d'informatique, accompagnés de leur ordinateur portable préféré. Annoncez le thème de la fête : « Mandriva, dans un esprit libre. »
