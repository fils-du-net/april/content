---
site: 01net.com
title: "Faisons une place à l'open source dans le plan de relance"
author: Tristan Nitot 
date: 2009-06-01
href: http://pro.01net.com/editorial/502840/faisons-une-place-a-lopen-source-dans-le-plan-de-relance/
tags:
- Le Logiciel Libre
- Entreprise
- Économie
---

> Au début du mois de mai, la secrétaire d'Etat à l'Economie numérique annonçait un volet numérique au plan de relance de 250 millions d'euros par an pendant trois ans. C'est maintenant au tour de la jeune association Ploss (réseau des entreprises open source en Ile-de-France) de faire dix propositions pour relancer l'économie numérique grâce au logiciel libre.
