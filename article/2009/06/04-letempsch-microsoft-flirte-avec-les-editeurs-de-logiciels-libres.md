---
site: letemps.ch
title: "Microsoft flirte avec les éditeurs de logiciels libres"
author: Anouch Seydtaghia
date: 2009-06-04
href: http://www.letemps.ch/Page/Uuid/5f435e82-507e-11de-bb0a-399467258055/Microsoft_flirte_avec_les_editeurs_de_logiciels_libres
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Informatique en nuage
---

> «Lors de notre première apparition à cet événement, on nous regardait un peu de travers, reconnaît Manuel Michaud, qui dirige les initiatives «open source» de Microsoft en Suisse. Désormais, la communauté open source, celle qui évolue dans le monde commercial, comprend notre approche pragmatique. Microsoft reconnaît que l’open source est une réalité du marché et cherche à collaborer avec les fournisseurs de ces solutions.
> [...]
> La grande crainte de Microsoft, c’est bien sûr que ses clients délaissent Windows ou Office pour des logiciels open source, c’est-à-dire des programmes dont le code source peut être modifié librement.
