---
site: ladepeche.fr
title: "Condom. Infos et services d'un simple clic"
author: J.R.
date: 2009-06-22
href: http://www.ladepeche.fr/article/2009/06/22/627584-Condom-Infos-et-services-d-un-simple-clic.html
tags:
- Le Logiciel Libre
- Administration
---

> « Je suis partie d'un logiciel libre et on a travaillé la charte graphique, avec du vert, des vagues évoquant les collines, pour lui donner une image plus gaie », explique Cécile Roy-Dubourdieu, webmaster du nouveau site.
> [...]
> « On est très content de cette réalisation en interne et de la sollicitation des ressources intellectuelles des services pour créer et alimenter le site. Ce côté « débrouille » aussi, avec l'utilisation d'un logiciel libre, cela nous va bien », se satisfait Eric Lanxade.
