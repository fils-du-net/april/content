---
site: tv5.org
title: "Coup d'envoi du salon LinuxTag à Berlin"
author: Centre d'Information et de Documentation sur l'Allemagne
date: 2009-06-26
href: http://www.tv5.org/TV5Site/info/communiques-de-presse-article.php?NPID=FR216947
tags:
- Le Logiciel Libre
- Sensibilisation
---

> Mercredi s'est ouverte la 15e édition du salon LinuxTag au parc des expositions de Berlin. Jusqu'au 27 juin, elle présentera aux développeurs, utilisateurs et visiteurs professionnels plus de 300 ateliers, conférences et présentations. 69 entreprises et 66 indépendants exposeront leurs nouveautés dans le domaine de l'"open source". Les organisateurs attendent plus de 10.000 visiteurs.
