---
site: digitalworld.fr
title: "Après deux ans de succès, les netbooks préparent leur révolution"
author: Serge Leblal 
date: 2009-06-08
href: http://www.digitalworld.fr/apres-deux-ans-de-succes-les-netbooks-preparent-leur-revolution,8753,a.html
tags:
- Le Logiciel Libre
- Interopérabilité
---

> Deux ans après leur lancement par Asustek, les netbooks ont créé une nouvelle catégorie sur le marché des PC.
> [...]
> Pour la partie système d'exploitation, la domination de Windows XP est aujourd'hui contestée par des solutions Linux. Bien loin des systèmes très basiques proposés il y a déjà deux ans par Asus sur ses premiers EeePC, ces distributions Linux sont le fruit du travail de Google (Android OS) et d'Intel (Moblin OS) même si, pour ce dernier, le flambeau a été repris par la Linux Foundation.
