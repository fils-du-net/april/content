---
site: numerama.com
title: "Hadopi : le Conseil constitutionnel devrait censurer le texte"
author: Guillaume Champeau 
date: 2009-06-05
href: http://www.numerama.com/magazine/13090-Hadopi-le-Conseil-constitutionnel-devrait-censurer-le-texte.html
tags:
- HADOPI
---

> Le Conseil constitutionnel rendra dans les prochains jours son avis sur la constitutionnalité de la loi Création et Internet. Sans attendre, deux docteurs en Droit publient, l'un sur une revue très reconnue des experts en Droit, l'autre sur un très sérieux site de veille juridique, leur analyse. Ils concluent tous les deux à la très probable censure, au moins partielle, de la riposte graduée.
