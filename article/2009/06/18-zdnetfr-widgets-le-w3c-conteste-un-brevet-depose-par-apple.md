---
site: zdnet.fr
title: "Widgets : le W3C conteste un brevet déposé par Apple"
date: 2009-06-18
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39700064,00.htm
tags:
- Le Logiciel Libre
- Internet
- Interopérabilité
- Brevets logiciels
---

> Réglementation - Le consortium chargé de gérer les standards du web est en conflit avec Apple au sujet d’un brevet que possède la firme sur la mise à jour des widgets. Ce qui bloque la publication d’un standard ouvert.
> [...]
> La firme à la pomme veut faire valoir ses droits à des royalties sur ce brevet alors que le consortium W3C estime qu'en tant que membre, Apple doit partager gratuitement sa propriété intellectuelle.
