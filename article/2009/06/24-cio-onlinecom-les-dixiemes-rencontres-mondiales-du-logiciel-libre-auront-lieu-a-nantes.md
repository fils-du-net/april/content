---
site: "cio-online.com"
title: "Les dixièmes Rencontres Mondiales du Logiciel Libre auront lieu à Nantes"
author: Bertrand Lemaire 
date: 2009-06-24
href: http://www.cio-online.com/actualites/lire-les-dixiemes-rencontres-mondiales-du-logiciel-libre-auront-lieu-a-nantes-2279.html
tags:
- Le Logiciel Libre
- Matériel libre
- Sensibilisation
- Associations
---

> Les Rencontres Mondiales du Logiciel Libre (RMLL) sont désormais bien installées dans le paysage des évènements informatiques en France. La dixième édition se déroulera à Nantes du 7 au 11 juillet 2009.
> Sur cinq jours, les RMLL avaient attiré en 2008 à Mont-de-Marsan (nettement moins accessible que Nantes !) 4000 visiteurs et 200 conférenciers (dont un cinquième d'étrangers) pour 300 conférences et ateliers. Tous les ans, un village associatif permet aux visiteurs de rencontrer de nombreux clubs.
