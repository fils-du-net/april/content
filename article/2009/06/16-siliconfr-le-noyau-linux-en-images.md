---
site: silicon.fr
title: "Le noyau Linux en images"
author: David Feugey 
date: 2009-06-16
href: http://www.silicon.fr/fr/news/2009/06/16/le_noyau_linux_en_images
tags:
- Le Logiciel Libre
- Standards
---

> Les travaux de Razvan Musaloiu-E. sont intéressants à plus d’un titre. Cet étudiant a créé des graphes qui mettent en valeur certains éléments clés du noyau Linux.
> Son dossier sur les systèmes de fichiers du noyau Linux fait un point précis sur la situation en la matière : il montre les points de convergence entre les systèmes de fichiers et le noyau Linux, ainsi que les relations entre les divers systèmes de fichiers (A Visual Expedition Inside the Linux File Systems).
> http://cs.jhu.edu/~razvanm/fs-expedition/
