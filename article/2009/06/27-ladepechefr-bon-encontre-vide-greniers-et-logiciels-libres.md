---
site: ladepeche.fr
title: "Bon-Encontre. Vide-greniers et logiciels libres"
author: La rédaction
date: 2009-06-27
href: http://www.ladepeche.fr/article/2009/06/27/630532-Bon-Encontre-Vide-greniers-et-logiciels-libres.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Peut-être le saviez-vous, il existe une solution pour une première approche : l'installation de Wubi, un programme qui permet d'installer et désinstaller Ubuntu (ou ses variantes) sous Microsoft® Windows®, comme n'importe quel autre programme sous ce système d'exploitation. Et si vous changiez ?
