---
site: zdnet.fr
title: "Linux supplante Windows dans le support de l’USB 3.0"
author: La rédaction
date: 2009-06-17
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39504770,00.htm
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Interopérabilité
---

> La version 3 de l'USB offrant des débits de 4,8 Gbit/s, soit dix fois plus que l'USB 2, dispose déjà d'un pilote pour le noyau Linux, mis au point par Intel.
> [...]
> Et le premier système d'exploitation à bénéficier du support de l'USB 3.0 devrait être Linux. Sarah Sharp du centre de développement technologique Open Source d'Intel vient de finaliser le pilote Linux. Celui-ci sera prochainement intégré au noyau 2.6.30, avant d'être implémenté dans les différentes distributions Linux.
