---
site: Journal du Net
title: "Logiciel libre / Open Source : l'innovation ouverte en point de mire"
author: Francois Letellier
date: 2009-06-22
href: http://www.journaldunet.com/solutions/expert/40125/logiciel-libre---open-source---l-innovation-ouverte-en-point-de-mire.shtml
tags:
- Le Logiciel Libre
- Économie
- Innovation
---

> Vu son rythme de croissance exponentiel, la logique Open Source pourrait devenir devenir la référence, et l'édition propriétaire un phénomène à la marge. Le Libre permet aux entreprises européennes d'économiser 36% de leur R&amp;D en logiciel pour mieux investir ailleurs.
