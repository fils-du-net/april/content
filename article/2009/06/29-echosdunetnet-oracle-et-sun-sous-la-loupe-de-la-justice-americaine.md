---
site: echosdunet.net
title: "Oracle et Sun sous la loupe de la justice américaine"
author: Infested Grunt
date: 2009-06-29
href: http://www.echosdunet.net/dossiers/dossier_3709_oracle+sun+sous+loupe+justice+americaine.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Économie
---

> L'autre problème vient des projets OpenSource de Sun et de l'attitude très propriétaires d'Oracle. Sun Microsystems possède ou participe à de nombreux projets OpenSource tel que la suite bureautique OpenOffice.org (basé sur une version de Sun StarOffice), la base de données MySQL, le système d'exploitation OpenSolaris, le Java OpenJDK ou la plateforme de développement Eclipse. Le passage sous Oracle fait craindre la disparition de certains projets, remplacé par des solutions propriétaires.
