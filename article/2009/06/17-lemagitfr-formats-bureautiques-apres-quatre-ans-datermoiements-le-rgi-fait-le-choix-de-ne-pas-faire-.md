---
site: lemagit.fr
title: "Formats bureautiques : après quatre ans d'atermoiements, le RGI fait le choix... de ne pas faire de choix "
author: Christophe Bardy 
date: 2009-06-17
href: http://www.lemagit.fr/article/microsoft-openxml-rgi-odf-gouvernement-formats-interoperabilite/3573/1/formats-bureautiques-apres-quatre-ans-atermoiements-rgi-fait-choix-pas-faire-choix/
tags:
- Logiciels privateurs
- Administration
- Interopérabilité
- Partage du savoir
- RGI
- Standards
---

> La direction générale de la modernisation de l'Etat vient de publier la version 1.0 du référentiel Général d'intéropérabilité (RGI), le document qui fixe les principes d'interopérabilité entre SI de la sphère publique et qui s'attaque notamment à la question du choix des formats de fichiers normalisés préconisés par l'administration. ODF comme Open XML font partie de cette sélection. Le fruit d'une bataille de plusieurs années pour Microsoft, dont le format avait été "oublié" de la version préliminaire du RGI.
