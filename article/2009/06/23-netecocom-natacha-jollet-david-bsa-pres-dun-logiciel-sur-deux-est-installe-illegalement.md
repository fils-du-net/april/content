---
site: neteco.com
title: "Natacha Jollet-David, BSA : \"près d'un logiciel sur deux est installé illégalement\" "
author: Ariane Beky
date: 2009-06-23
href: http://www.neteco.com/284010-jollet-david-bsa-business-software-piratage.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
---

> AB - Ne pensez-vous pas que "le combat" mené par la BSA puisse amener un plus grand nombre de structures à s'intéresser aux logiciels libres et open source ?
> [...]
> Chacun de ces modèles – le modèle commercial et celui de la communauté de développement – a sa place dans un marché du logiciel sain et diversifié. L'innovation dans le domaine des logiciels constitue un moteur pour le progrès économique, social et technologique. Permettre que plusieurs modèles de développement de logiciels et plusieurs types de licence puissent être en compétition et être sélectionnés en fonction de leurs avantages respectifs constitue le meilleur moyen de promouvoir l'innovation dans le domaine du logiciel.
