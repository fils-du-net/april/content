---
site: lemagit.fr
title: "Linux à Munich : le projet pédale dans la choucroute "
author: Cyrille Chausson
date: 2009-06-29
href: http://www.lemagit.fr/article/microsoft-windows-licences-linux-poste-travail-budgets-allemagne-open-source/3683/1/linux-munich-projet-pedale-dans-choucroute/
tags:
- Le Logiciel Libre
- Administration
---

> Après de multiples décalages, le projet de migration des postes de travail de la Ville de Munich vers Linux ne connaîtra de vrais résultats qu'à la mi-2012, nous apprend le quotidien Heise. Des retards à répétition qui font gonfler inévitablement la facture de la migration, décalant au passage les premières retombées financières. De quoi faire réfléchir les autres organisations tentées par l'abandon de Windows...
