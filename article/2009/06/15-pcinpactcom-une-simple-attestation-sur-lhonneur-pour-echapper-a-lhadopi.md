---
site: pcinpact.com
title: "Une simple attestation sur l'honneur pour échapper à l'Hadopi !"
author: Marc Rees
date: 2009-06-15
href: http://www.pcinpact.com/actu/news/51387-hadopi-attestation-honneur-coupure-acces.htm
tags:
- Logiciels privateurs
- HADOPI
---

> L’une des incertitudes dans l’usage de ces solutions logicielles était la preuve de leur utilisation. Comment Madame Michu, dont l'IP a été flashée le 1er avril à 10h30, pouvait-elle démontrer que son logiciel de sécurisation était actif à l’instant T ? Christine Albanel n'a jamais répondu à cette question devant la représentation nationale.
