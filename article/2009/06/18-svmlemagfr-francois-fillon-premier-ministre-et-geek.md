---
site: svmlemag.fr
title: "François Fillon : premier ministre... et geek !"
author: Cécile Dard
date: 2009-06-18
href: http://www.svmlemag.fr/dossier/04673/francois_fillon_premier_ministre_geek
tags:
- Le Logiciel Libre
- Institutions
---

> SVM : Êtes-vous un défenseur du logiciel libre ?
> François Fillon : Je n’ai jamais utilisé personnellement de logiciel libre, mais je suis convaincu de l’utilité de l’open source pour faciliter l’entrée de nouveaux acteurs et stimuler la concurrence. Matignon, comme beaucoup de ministères, utilise des logiciels libres, comme OpenOffice. La nouvelle version du portail du gouvernement utilise le CMS open source “Drupal”, réputé pour être l’un des meilleurs du moment.
