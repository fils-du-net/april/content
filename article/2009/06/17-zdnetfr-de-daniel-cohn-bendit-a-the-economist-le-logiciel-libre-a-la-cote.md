---
site: zdnet.fr
title: "De Daniel Cohn-Bendit à The Economist, le logiciel libre a la cote"
author: Thierry Noisette
date: 2009-06-17
href: http://www.zdnet.fr/blogs/2009/06/17/de-daniel-cohn-bendit-a-the-economist-le-logiciel-libre-a-la-cote/
tags:
- Le Logiciel Libre
- Informatique en nuage
---

> Le logiciel libre est-il en voie de conquérir sa place dans la foulée? La rencontre dans la presse de sa mention quasi simultanée par le député européen Daniel Cohn-Bendit et par le très libéral hebdomadaire anglais The Economist laisse en tout cas songeur sur l'étendue des soutiens que gagne l'open source.
> [...]
> Pour The Economist,"l'open source a remporté le débat"
> Pas du même côté de l'échiquier politique que les altermondialistes, Verts et autres anti-consumérisme, The Economist publiait récemment un article sur le cloud computing, "Unlocking the cloud" complété par un second sur les entreprises de l'open source ("Born free", tous deux le 28 mai). Affirmant d'emblée que "l'open source a remporté le débat", l'article pointe les dangers sur l'ouverture que fait peser le cloud computing.
