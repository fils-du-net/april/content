---
site: nouvo.ch
title: "Le boum des logiciels libres"
author: Zian Marro
date: 2009-06-01
href: http://www.nouvo.ch/s-014
tags:
- Le Logiciel Libre
- Administration
- Éducation
---

> La suite de logiciels libres OpenOffice équipe aujourd'hui tous les ordinateurs pédagogiques des écoles primaires et secondaires du canton de Genève. Cette année, l'Etat de Genève a même édité un CD afin que les élèves puissent installer ces logiciels gratuits chez eux. Le canton comme les familles font donc ainsi l'économie des couteuses licences d'utilisation des produits dits «propriétaires».
