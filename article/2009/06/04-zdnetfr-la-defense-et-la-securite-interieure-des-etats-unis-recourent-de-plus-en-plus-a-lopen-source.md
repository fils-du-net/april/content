---
site: zdnet.fr
title: "La Défense et la Sécurité intérieure des Etats-Unis recourent de plus en plus à l'open source"
author: Thierry Noisette
date: 2009-06-04
href: http://www.zdnet.fr/blogs/2009/06/04/la-defense-et-la-securite-interieure-des-etats-unis-recourent-de-plus-en-plus-a-l-open-source/
tags:
- Le Logiciel Libre
- Institutions
---

> Le département de la Sécurité intérieure lance le programme Homeland Open Security Technology (HOST) destiné à améliorer l'emploi de l'open source dans l'informatique du gouvernement américain. Tandis que la Défense vient d'ouvrir Forge.mil, un système de développement collaboratif.
