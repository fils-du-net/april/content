---
site: pcinpact.com
title: "Hadopi : la suspension couplée avec une amende de 1.500 euros"
author: Marc Rees
date: 2009-06-24
href: http://www.pcinpact.com/actu/news/51576-amende-1500-euros-hadopi-suspension.htm
tags:
- HADOPI
---

> Amende ou suspension ? Lors des débats sur le projet Hadopi, les discussions avaient oscillé entre ces deux bords. Finalement, le système Hadopi corrigé et révisé après la censure partielle du Conseil constitutionnel adoptera les deux : la suspension par la loi, l'amende par le décret. Une information exclusive révélée par la Tribune dans son édition de mercredi.
