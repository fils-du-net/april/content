---
site: framablog.org
title: "Le logiciel libre européen est-il politiquement de gauche ?"
author: aKa
date: 2009-06-19
href: http://www.framablog.org/index.php/post/2009/06/19/logiciel-libre-europe-politique-gauche
tags:
- Le Logiciel Libre
- Institutions
---

> L’April se félicite du succès rencontré par l'initiative du Pacte du Logiciel Libre au lendemain des élections européennes. Bravo pour la mobilisation, c’est effectivement une bonne nouvelle que d’avoir 34 députés issus de 7 pays différents signataires de ce Pacte (même si on pourra m’objecter que cela représente à peine 5% des 736 députés que totalise l'assemblée).
