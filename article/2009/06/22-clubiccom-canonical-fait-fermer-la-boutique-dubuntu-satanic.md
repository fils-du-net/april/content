---
site: clubic.com
title: "Canonical fait fermer la boutique d'Ubuntu Satanic"
author: Guillaume Belfiore
date: 2009-06-22
href: http://www.clubic.com/actualite-283724-canonical-ubuntu-satanic.html
tags:
- Le Logiciel Libre
- Droit d'auteur
---

> Nous apprenons que Canonical a fait fermer la boutique CafePress d'Ubuntu Satanic sur laquelle étaient vendus des t-shirts et autocollants à moindre prix. La société de Mark Shuttleworth aurait en effet pris des positions plus claires et justifie cet acte en expliquant qu'il s'agit d'une violation de marque déposée. L'édition d'Ubuntu Satanic embarque des thèmes relativement sombres de Gnome ainsi que différents papiers peints (avec des flammes, des pentacles, des chauves-souris ou encore des têtes de mort) ainsi qu'un économiseur d'écran spécial.
