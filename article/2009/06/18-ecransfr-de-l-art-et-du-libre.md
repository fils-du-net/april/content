---
site: ecrans.fr
title: "De l’art et du Libre"
author: Astrid Girardeau 
date: 2009-06-18
href: http://www.ecrans.fr/De-l-art-et-du-Libre,7513.html
tags:
- Le Logiciel Libre
- April
- HADOPI
---

> Le collectif Libre Accès organise ce samedi, à Paris, la deuxième édition du Festival des Arts Libres.
> [...]
> La thématique des débats, animés par Alix Cazenave de l’April, et Jérémie Nestel de Libre Accès, est centrée sur la loi Création et Internet. De 14 à 15 heures, trois députés — Martine Billard, Nicolas Dupont-Aignan, et Jean-Bierre Brard — s’exprimeront sur la loi.
