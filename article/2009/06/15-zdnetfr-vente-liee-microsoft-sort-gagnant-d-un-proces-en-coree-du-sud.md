---
site: zdnet.fr
title: "Vente liée : Microsoft sort gagnant d’un procès en Corée du Sud"
author: Christophe Auffray
date: 2009-06-15
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39504587,00.htm
tags:
- Logiciels privateurs
- Vente liée
---

> Aux Etats-Unis, la justice de l'Etat du Mississippi a également condamné Microsoft pour abus de position dominante. Pour mettre fin au contentieux, l'éditeur a accepté de verser 100 millions de dollars, dont 60 millions sous forme de bons d'achats pour les consommateurs lésés.
