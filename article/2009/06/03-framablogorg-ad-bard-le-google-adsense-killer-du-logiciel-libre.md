---
site: framablog.org
title: "Ad Bard, le Google Adsense (killer) du logiciel libre ?"
author: aKa
date: 2009-06-03
href: http://www.framablog.org/index.php/post/2009/06/03/ad-bard-le-google-adsense-killer-du-logiciel-libre
tags:
- Le Logiciel Libre
- Économie
- Droit d'auteur
---

> Que ne diriez-vous d’une régie publicitaire de type Google Adsense mais qui n’afficherait sur vos sites que des liens promotionnels en liaison avec le logiciel libre. Une sacrée bonne idée non ?
> On se débarrasserait de l’emprise de Google sur ce secteur. On aurait l’assurance que les liens proposés ne pointent pas vers du logiciel propriétaire. Et on permettrait à l’économie du logiciel libre de se développer !
> C’est ni plus ni moins ce que propose le nouveau service Ad Bard, « The ad network for ethical computing », soutenu par la Free Software Foundation (FSF) dans un récent communiqué que nous avons traduit ci-dessous.
