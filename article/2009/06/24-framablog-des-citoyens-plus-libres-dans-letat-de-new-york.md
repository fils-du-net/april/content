---
site: Framablog
title: "Des citoyens plus libres dans l'État de New York"
author: aKa
date: 2009-06-24
href: http://www.framablog.org/index.php/post/2009/06/24/senat-etat-new-york-go-open-source
tags:
- Administration
- Institutions
---

> Le Sénat de l’État de New York vient d’afficher sa volonté d’ouverture en créant un espace dédié (dont nous avons traduit la page d’accueil ci-dessous) où il annonce son intention de proposer à ses administrés des « Free and Open-Source Software &amp; Services ».
