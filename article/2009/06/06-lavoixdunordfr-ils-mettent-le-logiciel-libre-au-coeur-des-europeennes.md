---
site: lavoixdunord.fr
title: "Ils mettent le logiciel libre au coeur des Européennes !"
author: Benjamin Merieau
date: 2009-06-06
href: http://www.lavoixdunord.fr/Locales/Maubeuge/actualite/Secteur_Maubeuge/2009/06/06/article_ils-mettent-le-logiciel-libre-au-coeur-d.shtml
tags:
- Le Logiciel Libre
- Administration
- April
- Europe
---

> On ne peut pas les rater, les membres de l'association April, avec leur bagout et leur place de choix à l'entrée du salon informatique.
>  Et ce qu'ils n'ont pas raté, eux qui militent pour le logiciel libre, c'est d'interpeller les candidats aux Européennes sur le sujet. Comment ? En leur proposant de signer un pacte où les politiques promettent, s'ils sont élus, de prendre des mesures en faveur ou n'allant pas contre ce type de logiciel, gratuit, modifiable et en lequel tous les informaticiens voient le futur.
