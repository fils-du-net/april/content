---
site: sudouest.com
title: "Le maître à penser du logiciel libre "
author: Stéphane Durand
date: 2009-06-30
href: http://www.sudouest.com/charente/actualite/article/635262/mil/4739855.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Administration
- Partage du savoir
- Sensibilisation
- Associations
- Éducation
- Promotion
---

> Linux, Open office, Firefox, The Gimp... Non, ce n'est pas de l'hébreu. Juste les noms donnés en informatique à un système d'exploitation, des logiciels ou encore un navigateur internet. Tous ont comme particularité d'être libres. Le grand mot. François Élie, conseiller municipal d'opposition à la mairie d'Angoulême, est un de leurs plus ardents défenseurs.
