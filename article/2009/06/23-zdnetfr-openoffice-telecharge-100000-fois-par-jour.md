---
site: zdnet.fr
title: "OpenOffice téléchargé 100.000 fois par jour"
author: Thierry Noisette
date: 2009-06-23
href: http://www.zdnet.fr/blogs/2009/06/23/openoffice-telecharge-100-000-fois-par-jour/
tags:
- Le Logiciel Libre
---

> Dans une interview au grand quotidien indien en anglais The Times of India ('Open source is more stable and better supported'), le P-DG (CEO) de Sun Microsystems, Jonathan Schwartz, donne en exemple de la croissance des logiciels libres le fait que la suite bureautique libre OpenOffice est téléchargée 100.000 fois par jour, contre la moitié un an plus tôt.
