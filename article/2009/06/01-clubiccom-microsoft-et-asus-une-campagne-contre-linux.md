---
site: clubic.com
title: "Microsoft et Asus : une campagne contre Linux"
author: la redaction
date: 2009-06-01
href: http://www.clubic.com/actualite-279266-microsoft-asus-campagne-linux.html
tags:
- Logiciels privateurs
- Informatique-deloyale
- Video
---

> Microsoft et Asus se sont associés afin de lancer une nouvelle campagne publicitaire visant à mettre en avant l'utilisation du système d'exploitation de la firme de Redmond sur les netbooks. Pour cette initiative, un site Internet promotionnel a été créé sur lequel nous retrouvons une vidéo mettant en valeur les outils de Microsoft pré-installés sur les netbooks tels que Microsoft Works, la suite Windows Live Messenger ou encore les outils de sécurité.
