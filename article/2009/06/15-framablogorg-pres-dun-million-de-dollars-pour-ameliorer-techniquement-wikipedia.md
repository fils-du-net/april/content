---
site: framablog.org
title: "Près d'un million de dollars pour améliorer techniquement Wikipédia"
author: aKa
date: 2009-06-15
href: http://www.framablog.org/index.php/post/2009/06/15/mediawiki-wikipedia-usability-initiative
tags:
- Le Logiciel Libre
- Éducation
---

> Participer à Wikipédia est semble-t-il d’une simplicité enfantine : il suffit de cliquer sur le bouton « Modifier » qui figure en haut de chaque article.
> Sauf qu’apparait alors à l’écran une fenêtre, de prime abord assez déroutante, où se mélangent textes et « signes cabalistiques » propre à la syntaxe wiki de MediaWiki, le logiciel moteur de wiki sur lequel repose l’encyclopédie libre[1].
> [...]
> L’objectif principal de la Usability Initiative est l’amélioration du logiciel afin de le rendre moins rebutant pour les nouveaux wikipédiens. Mais ce projet pourrait avoir une conséquence inattendue : un regain de considération pour MediaWiki en entreprise.
