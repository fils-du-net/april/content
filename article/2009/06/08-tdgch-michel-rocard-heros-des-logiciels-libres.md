---
site: tdg.ch
title: "Michel Rocard, héros des logiciels libres"
author: Luca Sabbatini
date: 2009-06-08
href: http://www.tdg.ch/actu/hi-tech/michel-rocard-heros-logiciels-libres-2009-06-07
tags:
- Le Logiciel Libre
- Administration
- Europe
---

> «Je vais vous raconter une histoire, celle d’un combat que j’ai mené un peu par hasard, mais avec enthousiasme.» Vingt ans après avoir dirigé la France, Michel Rocard s’est investi à l’orée du XXIe siècle dans une autre cause, peut-être plus importante encore que le sort d’une nation. Eurodéputé depuis 1994 jusqu’à cette année, l’ancien premier ministre français a dirigé au Parlement européen la croisade victorieuse contre la «brevetabilité» des logiciels. Une lutte qu’il est venu raconter vendredi dernier aux Linux Days, le Salon genevois de l’open source.
