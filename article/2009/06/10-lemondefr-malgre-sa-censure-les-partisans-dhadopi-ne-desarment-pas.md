---
site: lemonde.fr
title: "Malgré sa censure, les partisans d'Hadopi ne désarment pas"
author: Le Monde.fr, avec AFP
date: 2009-06-10
href: http://www.lemonde.fr/technologies/article/2009/06/10/les-partisans-d-hadopi-ne-desarment-pas_1205324_651865.html
tags:
- HADOPI
---

> La censure du dispostif de riposte graduée par le Conseil constitutionnel, mercredi 10 juin, est une nouvelle épreuve difficile pour le gouvernement, comme pour les sociétés d'auteurs qui soutenaient le texte. Cette censure "est nette, sans appel, claire et particulièrement motivée. C'est la plus sévère depuis une bonne dizaine d'années", résume Dominique Rousseau, professeur de droit constitutionnel à l'université de Montpellier.
