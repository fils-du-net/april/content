---
site: ecrans.fr
title: "« Nous sommes des milliers de \"gus dans des garages\" »"
author: Astrid Girardeau
date: 2009-06-12
href: http://www.ecrans.fr/Nous-sommes-des-milliers-de-gus,7428.html
tags:
- Le Logiciel Libre
- HADOPI
- Institutions
---

> Justificator : Qu’implique selon vous la censure d’Hadopi pour les utilisateurs et concepteurs de logiciels libres ? Pouvez vous expliquer les enjeux d’une telle loi pour le Libre ?
> Jérémie Zimmermann : Je vous invite à consulter l’abondante documentation rédigée par l’April (remarquable association de promotion et défense du logiciel libre) à ce sujet ! ;) En gros le logiciel libre était surtout concerné par l’imposition de ces logiciels de sécurisation/contrôle. Maintenant qu’ils ne sont plus obligatoires, on peut respirer. Mais pour autant l’Hadopi va tout de même tenter d’édicter la liste de leurs spécifications fonctionnelles, puis de labéliser de tels logiciels (s’ils existent un jour). Il conviendra de rester vigilant quant aux modalités de cette labélisation, pour s’assurer qu’elle n’est pas discriminante à l’égard des auteurs et utilisateurs de Logiciel Libre.
