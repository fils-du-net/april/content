---
site: neteco.com
title: "Tristan Nitot, Mozilla : \"Firefox 3.5 est jusqu'à dix fois plus rapide que Firefox 2\""
author: Ariane Beky
date: 2009-06-25
href: http://www.neteco.com/284608-nitot-shaver-nightingale-mozilla-firefox-3-5.html
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
- Innovation
---

> TN – Nous travaillons constamment à améliorer Firefox sous Linux, comme nous le faisons pour les systèmes d'exploitation Mac OS X, Windows et, demain, pour des OS mobiles avec Fennec. Pour chaque plateforme, nous rencontrons des difficultés techniques auxquelles nous devons parer. Avec Linux, les choses se corsent : il existe un nombre impressionnant de distributions et nous devons adapter le navigateur à différents types, par exemple à Debian et Ubuntu, d'un côté, à Red Hat et Suse de l'autre. Nous pensons avoir nettement progressé avec Firefox 3.5.
