---
site: lepoint.fr
title: "Hadopi : les \"mouchards filtrants\", l'autre censure du Conseil constitutionnel"
author: Guerric Poncet
date: 2009-06-12
href: http://www.lepoint.fr/actualites-technologie-internet/2009-06-12/interview-hadopi-les-mouchards-filtrants-l-autre-censure-du-conseil/1387/0/352178
tags:
- Le Logiciel Libre
- April
- HADOPI
---

> Le Conseil constitutionnel a censuré une partie de la loi Hadopi. Alors que tous les regards se sont tournés vers les paragraphes concernant la riposte graduée et la suspension de l'abonnement, l'April (Association de promotion et de défense du logiciel libre) rappelle que les systèmes de sécurisation que le gouvernement voulait imposer aux internautes ont, eux aussi, été censurés.
