---
site: zdnet.fr
title: "Red Hat ne connaît pas la crise grâce à Linux"
author: La rédaction
date: 2009-06-25
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39700551,00.htm
tags:
- Le Logiciel Libre
- Entreprise
- Économie
---

> Contrairement à la grande majorité des éditeurs de logiciels, et en particulier propriétaires, le spécialiste du Libre, Red Hat, enregistre au premier trimestre une croissance de 11% de son chiffre d'affaires.
> [...]
> Durant le trimestre, Red Hat a pu s'appuyer sur un réseau de distributeurs performant. Ce réseau représente désormais à lui seul 61% des commandes enregistrées par l'éditeur durant le trimestre. Sur les trois derniers mois de l'année fiscale précédente, la vente indirecte représentait 56% du carnet de commandes de Red Hat.
