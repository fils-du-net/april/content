---
site: itespresso.fr
title: "Hadopi 2 : les opposants au texte ne lâchent rien"
author: La Rédaction
date: 2009-06-25
href: http://www.itespresso.fr/hadopi-2-les-opposants-au-texte-ne-lachent-rien-30167.html
tags:
- April
- HADOPI
---

> Le son de cloche est similaire du côté de l’April. L’association de promotion et de défense du logiciel libre dénonce le “jusqu’au-boutisme du Président de la République dans sa politique de surveillance et de contrôle des usages privés”.
