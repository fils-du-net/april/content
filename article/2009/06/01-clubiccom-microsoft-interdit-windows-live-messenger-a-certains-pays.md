---
site: clubic.com
title: "Microsoft interdit Windows Live Messenger à certains pays"
author: Romain
date: 2009-06-01
href: http://www.clubic.com/actualite-279322-microsoft-censure-windows-live-messenger.html
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
---

> De nombreux internautes n'ont plus accès au service de messagerie instantanée de Microsoft depuis déjà un peu plus d'une semaine. Les ressortissants de Cuba, de la Syrie, de l'Iran, du Soudan et de la Corée du Nord obtiennent effectivement un message d'erreur lorsqu'ils tentent de se connecter à Windows Live Messenger. La firme de Redmond a en fait volontairement coupé l'accès à ses services Windows Live aux utilisateurs des pays sous embargo américain.
> [...]
> Quoi qu'il en soit le gouvernement cubain, qui utilise des produits Microsoft malgré les restrictions sur l'export de licences, envisagerait d'ailleurs de migrer vers des logiciels libres. D'aucuns estimant que les cubains devraient en faire autant...
