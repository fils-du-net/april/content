---
site: lemonde.fr
title: "En Estonie, sept clics pour voter"
author: Yves Eudes
date: 2009-06-02
href: http://www.lemonde.fr/elections-europeennes/article/2009/06/02/en-estonie-sept-clics-pour-voter_1201236_1168667.html
tags:
- Le Logiciel Libre
- Internet
- Administration
- Accessibilité
---

> En Estonie, l'élection des députés au Parlement européen s'étale sur onze jours. Depuis le 28 mai, les citoyens peuvent voter sur Internet en se connectant au site de la commission électorale, de jour comme de nuit, à partir de n'importe quel ordinateur : chez eux, au bureau, chez un ami, ou sur un poste Internet en accès public dans une bibliothèque, une banque ou un cybercafé.
> [...]
> Tarvi Martens se dit sûr de son coup : "Nous avons procédé à des consultations élargies, tout a été contrôlé par différents groupes d'experts indépendants. Nous utilisons des logiciels libres et ouverts, dont le fonctionnement est vérifiable par tous."
