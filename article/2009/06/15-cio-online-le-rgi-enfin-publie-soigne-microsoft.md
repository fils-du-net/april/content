---
site: "CIO-online"
title: "Le RGI, enfin publié, soigne Microsoft"
author: Bertrand Lemaire
date: 2009-06-15
href: http://www.cio-online.com/actualites/lire-le-rgi-enfin-publie-soigne-microsoft-2258.html
tags:
- Administration
- Interopérabilité
- RGI
- Standards
---

> Après plus de deux ans de retard, le Référentiel Général d'Interopérabilité vient d'être publié. Ce guide pour la conception des SI publics laisse une place à la pluralité des normes bureautiques au détriment de l'interopérabilité.
