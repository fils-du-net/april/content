---
site: zdnet.fr
title: "Vente liée : Bruxelles veut savoir si Microsoft exerce des pressions sur les constructeurs"
author: Christophe Auffray
date: 2009-06-10
href: http://www.zdnet.fr/actualites/internet/0,39020774,39504219,00.htm
tags:
- Logiciels privateurs
- Vente liée
---

> Juridique - La Commission européenne, qui enquête sur la vente liée d'IE dans Windows, a soumis un questionnaire aux constructeurs de PC pour savoir notamment si Microsoft tente de les influencer sur le choix des navigateurs proposés dans leur PC.
> Microsoft est actuellement sous le coup d'une enquête de la Commission européenne concernant l'intégration d'Internet Explorer dans Windows. Suite à la plainte d'Opera, éditeur du navigateur éponyme, Bruxelles tente de déterminer si les pratiques de Microsoft nuisent à la concurrence.
