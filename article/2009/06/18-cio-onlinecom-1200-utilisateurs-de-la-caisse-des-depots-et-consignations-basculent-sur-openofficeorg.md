---
site: "cio-online.com"
title: "1200 utilisateurs de la Caisse des Dépôts et Consignations basculent sur OpenOffice.org"
author: Bertrand Lemaire 
date: 2009-06-18
href: http://www.cio-online.com/actualites/lire-1200-utilisateurs-de-la-caisse-des-depots-et-consignations-basculent-sur-openofficeorg-2270.html
tags:
- Le Logiciel Libre
- Administration
- Interopérabilité
---

> 1200 utilisateurs de la Caisse des Dépôts et Consignations basculent sur OpenOffice.orgInformatique CDC, GIE informatique du groupe Caisse des Dépôts et Consignations, s'est fait aidé par Linagora. Le déploiement s'achève en juin 2009.
> [...]
> Parmi les points de coûts pouvant être réduits, les logiciels bureautiques ont été repérés depuis plusieurs années. De plus, la pérennité des documents créés grâce à ces logiciels est un problème récurrent. L'essentiel de la « mémoire vive de l'entreprise » est en effet issu des documents bureautiques.
