---
site: Framablog
title: "Difficile de supporter Microsoft quand il supporte ainsi l'ODF"
author: aKa
date: 2009-06-25
href: http://www.framablog.org/index.php/post/2009/06/25/odf-ooxml-support-microsoft-office-guerre-des-formats
tags:
- Interopérabilité
---

> La lecture régulière du Framablog nous apprend, si ce n’est à nous méfier, en tout cas à être très prudent face aux effets d’annonce de Microsoft. Et pourtant, en avril dernier, un brin naïf je titrais : Le jour où la suite bureautique MS Office devint fréquentable ? en évoquant le support imminent du format ouvert OpenDocument (ou ODF) dans le tout nouveau service pack 2 de la version 2007 de la célèbre suite propriétaire de la firme de Redmond.
