---
site: "cio-online.com"
title: "Européennes : l'UMP absente du Pacte du Logiciel Libre"
author: Bertrand Lemaire 
date: 2009-06-23
href: http://www.cio-online.com/actualites/lire-europeennes-l-ump-absente-du-pacte-du-logiciel-libre-2277.html
tags:
- Le Logiciel Libre
- Administration
- April
---

> Ainsi, l'APRIL (association pour la recherche en informatique libre) vient de publier la liste des eurodéputés élus ayant signé son Pacte du Logiciel Libre. On y trouve des représentants du Front de Gauche (2 signataires sur 4 élus), du Parti Socialiste (5 sur 14), d'Europe Ecologie (5 sur 14) et du MoDem (5 sur 6). Mais ni le député Libertas (MPF-CPNT), ni les trois du Front National, ni les 29 de l'UMP n'ont signé ce fameux pacte. Droite et Extrême-Droite sont donc bien fâchés avec le logiciel libre.
