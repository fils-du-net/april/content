---
site: lemensuel.net
title: "Hadopi : «Le droit ne doit pas contraindre l’innovation»"
author: Guilhem Fabre
date: 2009-06-18
href: http://www.lemensuel.net/2009/06/18/hadopi-%C2%AB-le-droit-ne-doit-pas-contraindre-l%E2%80%99innovation-%C2%BB/
tags:
- Le Logiciel Libre
- Internet
- Partage du savoir
- HADOPI
- Droit d'auteur
---

> Entretien avec Guilhem Fabre, socio-économiste spécialiste des questions de propriété intellectuelle.
> [...]
> LMU : Vous dites donc que la diffusion « libre » de la culture est source d’innovation en soi ?
> Absolument. Si vous déposez des brevets logiciels dans un domaine où toutes les innovations sont très cumulatives, vous ajournez la diffusion de votre invention et vous retardez la diffusion du savoir.
> Avec les logiciels libres, l’appropriation est rejetée pour un modèle que j’appelle la « coo-pétition », c’est-à-dire une coopération très poussée au niveau de la recherche, puis une appropriation par le marché pour une utilisation spécifique. La valeur ajoutée est ici liée aux services informatiques découlant de l’utilisation logicielle, et non à l’édition du logiciel proprement dite.
