---
site: datanews.be
title: "4 entreprises du Benelux sur 10 utilisent de l'open source"
author: Stefan Grommen
date: 2009-06-08
href: http://www.datanews.be/fr/news/90-57-24403/4-entreprises-du-benelux-sur-10-utilisent-de-l-open-source.html
tags:
- Le Logiciel Libre
- Entreprise
---

> 41 pour cent des organisations du Benelux disposent de logiciels à code ouvert (open source). Et nombre de CIO souhaitent soit en augmenter encore la quantité, soit commencer à les utiliser.
> Voilà ce qui ressort d'une étude de l'analyste de marché TNS Gallup à la demande de Sun Microsystems. Ces résultats résultent de conversations menées avec 310 CIO et managers IT du Benelux et de Scandinavie. En Scandinavie, l'utilisation de l'open source est du reste encore supérieure pour s'établir à 46 pour cent. C'est la Finlande qui mène la danse avec 54 pour cent.
