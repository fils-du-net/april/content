---
site: lemagit.fr
title: "Nantes accueille les RMLL du 7 au 11 juillet "
author: Cyrille Chausson
date: 2009-06-05
href: http://www.lemagit.fr/article/france-libre/3468/1/nantes-accueille-les-rmll-juillet/
tags:
- Le Logiciel Libre
- Partage du savoir
- Matériel libre
- Sensibilisation
---

> Après Mont-de-Marsan, c'est au tour de la ville de Nantes de recevoir cette année les Rencontres Mondiales du Logiciel Libre (RMLL), du 7 au 11 juillet. Financée en partie par la Région Pays de Loire et portée par l'association Linux-Nantes ainsi que, comme il est de tradition, par un parterre d'associations locales entièrement dédiées à la cause du Libre, cette 10e édition sera placée sous le signe de l'ouverture, avec une volonté affichée d' "ouvrir sur les non-utilisateurs et décideurs en manque d'informations ", explique les organisateurs.
