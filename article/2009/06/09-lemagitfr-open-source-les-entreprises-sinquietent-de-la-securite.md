---
site: lemagit.fr
title: "Open Source : les entreprises s'inquiètent de la sécurité "
author: Cyrille Chausson 
date: 2009-06-09
href: http://www.lemagit.fr/article/securite-enterprise-support-open-source/3500/1/open-source-les-entreprises-inquietent-securite/
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
---

> Forrester dépeint dans deux rapports les craintes des entreprises en matière de sécurité et de support des logiciels Open Source. Un pavé dans la mare et un vrai paradoxe alors que les solutions libres semblent de plus en plus gagner en popularité.
> Un vieux démon qui réapparait. Alors que l'Open Source immisce de plus en plus dans les systèmes d'informations les plus critiques, les entreprises estimeraient encore que l'Open Source souffre de grosses lacunes, comme la sécurité, qui freinent sa progression sur le marché.
