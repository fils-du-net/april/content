---
site: lemagit.fr
title: "L'innovation dans les interfaces graphiques est la clé de l'adoption de Linux"
author: Cyrille Chausson 
date: 2009-06-19
href: http://www.lemagit.fr/article/linux-poste-travail-mandriva-innovation/3610/1/arnaud-laprevote-mandriva-innovation-dans-les-interfaces-graphiques-est-cle-adoption-linux/
tags:
- Le Logiciel Libre
- Entreprise
- Standards
---

> Avec l'arrivée d'OS comme Moblin 2.0 ou encore de projets comme JoliCloud sur le populaire segment de netbooks, l'innovation en terme d'interface utilisateur et de chemins d'accès à l'information semble enfin avancer à grand pas. Et c'est Linux qui permet de concrétiser cette rupture avec l'existant en matière d'ergonomie. Une avancée commentée par Arnaud Laprévote, directeur R&amp;D de Mandriva, éditeur de la distribution Linux francophone.
