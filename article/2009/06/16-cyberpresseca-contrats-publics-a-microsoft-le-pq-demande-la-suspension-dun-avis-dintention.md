---
site: cyberpresse.ca
title: "Contrats publics à Microsoft: le PQ demande la suspension d'un avis d'intention"
author: Tristan Péloquin
date: 2009-06-16
href: http://technaute.cyberpresse.ca/nouvelles/logiciels/200906/16/01-876237-contrats-publics-a-microsoft-le-pq-demande-la-suspension-dun-avis-dintention.php
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Administration
---

> Joignant sa voix à celle d'un organisme de défense du logiciel libre, le Parti québécois demande au ministre de la Santé de suspendre un avis d'intention accordant à Microsoft un contrat de plus de 5,5 millions de dollars.
> [...]
> À Québec, la demande de l'organisme a vite trouvé écho dans les bureaux du critique de l'opposition officielle en matière d'Administration publique et du Trésor, Sylvain Simard. «Nous sommes dans le domaine du faux appel d'offres. On sent qu'il y a un malaise. Ce n'est pas normal qu'on donne si peu de temps à des entreprises concurrentes pour faire connaître leur offre», a affirmé le député péquiste, en entrevue à Cyberpresse.
