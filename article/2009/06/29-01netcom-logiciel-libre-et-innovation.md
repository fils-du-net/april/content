---
site: 01net.com
title: "Logiciel libre et innovation"
author: Tristan Nitot
date: 2009-06-29
href: http://pro.01net.com/editorial/503851/logiciel-libre-et-innovation/
tags:
- Le Logiciel Libre
- Entreprise
- Innovation
---

> En ces temps de crise, où il est plus essentiel que jamais de savoir tirer parti de toutes les contributions des employés, brider les bonnes volontés serait suicidaire. Les DSI risquent certes de voir d'un mauvais œil la perte de pouvoir au profit des utilisateurs. Mais si la compétitivité de l'entreprise est à ce prix, il vaut mieux laisser les employés s'emparer de la technologie, open source et standards ouverts, qui permettront alors d'innover de façon distribuée.
