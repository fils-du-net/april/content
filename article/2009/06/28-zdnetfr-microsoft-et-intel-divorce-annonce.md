---
site: zdnet.fr
title: "Microsoft et Intel : divorce annoncé !"
author: Louis Naugès
date: 2009-06-28
href: http://www.zdnet.fr/blogs/2009/06/28/microsoft-et-intel-divorce-annonce-/
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Standards
---

> De très nombreux systèmes d’exploitation, en majorité Open Source, sont proposés pour ces OAI mobiles : Android de Google, Symbian de Nokia, LiMo (Linux Mobile) et de très nombreuses variantes de Linux, à commencer par Ubuntu.
> Pour réussir sur ces nouveaux marchés, la clef de la réussite tient en deux mots :
> + Communautés de développeurs
> + Open Source
> C’est bien la raison qui a poussé (obligé ?) Nokia à mettre en Open Source Symbian !
