---
site: pcinpact.com
title: "Logiciel libre : l'April franchit la barre des 5 000 adhérents"
author: Marc Rees
date: 2009-06-03
href: http://www.pcinpact.com/actu/news/51185-april-5000-association-libre-promotion.htm
tags:
- April
---

> L’objectif avait été fixé pour la fin 2008. Il aura été franchi début juin. Un peu de retard, mais qu’importe : l’April, association pour la promotion et la défense du logiciel libre, vient d’annoncer son 5 000e adhérent.
> [...]
> Dans une interview en novembre 2008, Alix Cazenave, la chargée de mission  de l'association sur les dossiers institutionnels, législatifs et réglementaires anticipait déjà ce cap : « Nous souhaitons atteindre dans un premier temps, d'ici fin 2008, le nombre de 5000 membres qui nous donnerait à la fois le poids politique pour être convenablement entendus par nos décideurs, mais aussi la solidité financière qui nous permettrait de pérenniser les trois postes actuels et d'envisager un nouveau recrutement. Nous pourrions alors intervenir sur davantage de dossiers et être plus présents pour promouvoir et défendre le logiciel libre auprès de tous les publics et sur des enjeux économiques, sociaux et politiques plus variés ».
