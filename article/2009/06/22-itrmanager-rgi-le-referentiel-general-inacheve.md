---
site: ITRmanager
title: "RGI, le Référentiel Général Inachevé"
author: Jean-Marie Gouarné
date: 2009-06-22
href: http://www.itrmanager.com/tribune/244/rgi-referentiel-general-inacheve-br-jean-marie-gouarne.html
tags:
- Administration
- Interopérabilité
- RGI
- Standards
---

> Depuis le temps, on ne l'attendait plus, et finalement il est là : en chantier depuis trois ans, le Référentiel Général d'Interopérabilité est arrivé ! Avec un titre aussi ambitieux et après une aussi longue gestation, cette publication devrait être un jalon décisif dans le devenir des services de l'État en matière de systèmes  d'information. Hélas, en lisant ces 119 pages dont l'accouchement fut si douloureux, on reste sur sa faim et on comprend vite qu'on n'a fait que quelques pas sur la longue route de l'interopérabilité.
