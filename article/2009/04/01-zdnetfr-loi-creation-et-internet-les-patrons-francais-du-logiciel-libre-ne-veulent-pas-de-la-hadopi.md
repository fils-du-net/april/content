---
site: zdnet.fr
title: "Loi Création et Internet : les patrons français du logiciel libre ne veulent pas de la Hadopi"
author: La rédaction
date: 2009-04-01
href: http://www.zdnet.fr/actualites/internet/0,39020774,39389162,00.htm
tags:
- Le Logiciel Libre
- Entreprise
- April
- HADOPI
---

> Législation - Quatre-vingt entreprises du logiciel libre ont adressé une lettre ouverte à Christine Albanel pour dénoncer l’absence de prise en compte de l’interopérabilité dans le projet de loi Création et Internet. Une absence qui les pénaliserait.
