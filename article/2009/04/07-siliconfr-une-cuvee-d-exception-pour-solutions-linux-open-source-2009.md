---
site: silicon.fr
title: "Une cuvée d’exception pour Solutions Linux / Open Source 2009"
author: David Feugey
date: 2009-04-07
href: http://www.silicon.fr/fr/news/2009/04/07/une_cuvee_d_exception_pour_solutions_linux___open_source_2009
tags:
- Le Logiciel Libre
- Entreprise
---

> Le salon Solutions Linux / Open Source 2009 a été celui de tous les records. Avec 220 exposants, il a su attirer plus de 8.400 visiteurs, soit une fréquentation en progression de 12,5 % par rapport à l’an dernier. Les organisateurs ont noté un intérêt croissant des grands comptes qui ont formé 31 % des visiteurs. Les PME et TPE représentaient pour leur part 39 % et 30 % des visites. Ce constat est renforcé par la présence massive de dirigeants : 29 % en 2009 contre 16 % en 2008.
