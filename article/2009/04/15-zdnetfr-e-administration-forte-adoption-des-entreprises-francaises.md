---
site: zdnet.fr
title: "e-administration : forte adoption des entreprises françaises"
author: Christophe Auffray
date: 2009-04-15
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39392236,00.htm
tags:
- Le Logiciel Libre
- Entreprise
---

> Technologie - D’après une étude de l’Insee, le haut débit favorise le développement des démarches administratives sur Internet. Le télétravail progresse aussi, à hauteur de 22% des entreprises, tout comme les OS libres (14%).
> [...] Dernier constat de l'Insee : l'utilisation des systèmes d'exploitation libres se développent, bien que légèrement. 14% des entreprises d'au moins 10 salariés exploitaient un OS issu de l'Open Source en janvier 2007, contre 12% un an plus tôt. L'usage tend à croître avec la taille de l'entreprise. Le taux d'utilisation grimpe ainsi à 16% pour les sociétés employant 20 à 249 personnes (50% pour celles de 2000 salariés ou plus).
