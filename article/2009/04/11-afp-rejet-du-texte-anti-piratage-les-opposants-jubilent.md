---
site: AFP
title: "Rejet du texte anti-piratage: les opposants jubilent"
author: AFP
date: 2009-04-11
href: http://www.google.com/hostednews/afp/article/ALeqM5gOOzcXU4qjzl4TQ3UZXt2DhMUvzg
tags:
- April
---

> PARIS (AFP) — Le rejet surprise du projet de loi contre le téléchargement illégal par les députés jeudi a été accueilli avec jubilation par les opposants à ce texte qui prévoit la possibilité de couper l'accès internet aux pirates.
> [...] L'April, qui défend le logiciel libre, a "remercié" dans un communiqué les parlementaires qui ont "préféré la sauvegarde des libertés dans l'espace numérique à la protection d'intérêts particuliers".
