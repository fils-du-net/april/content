---
site: lavoixdunord.fr
title: "Hadopi : « C'est techniquement inapplicable »"
author: La Voix du Nord 
date: 2009-04-28
href: http://www.lavoixdunord.fr/Region/actualite/Secteur_Region/2009/04/28/article_c-est-techniquement-inapplicable.shtml
tags:
- Le Logiciel Libre
- HADOPI
---

> Avant, Yvan ne s'intéressait guère aux débats parlementaires. Jusqu'au projet HADOPI. Trentenaire lillois, il travaille dans la sécurité informatique, parle virus et processeurs avec une aisance déconcertante et s'investit dans une association, CLX, qui prône l'utilisation de logiciels libres. Ce qui le fait bondir, c'est d'abord l'aspect technique de la loi. « Il y a des mesures à cent lieues de la réalité. C'était vrai au début de l'ADSL (connexions rapides à Internet) et encore ! »
