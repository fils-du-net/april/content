---
site: AFP
title: "Avec la crise, le logiciel libre profite d'un regain d'interêt des sociétés"
author: 
date: 2009-04-02
href: http://www.google.com/hostednews/afp/article/ALeqM5jrc7EVgu8G7p33PQGjIJsavxKCbw
tags:
- Le Logiciel Libre
---

> PARIS (AFP) — A contre-courant d'un secteur informatique qui fait grise mine, le logiciel libre avance ses pions à la faveur de la crise qui incite les entreprises à faire des économies et met ainsi au défi ce jeune marché de faire ses preuves.
