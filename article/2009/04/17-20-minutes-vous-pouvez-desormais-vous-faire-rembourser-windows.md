---
site: 20minutes.fr
title: "Vous pouvez désormais vous faire rembourser Windows"
date: 2009-04-17
href: http://www.20minutes.fr/article/320591/High-Tech-Vous-pouvez-desormais-vous-faire-rembourser-Windows.php
tags:
- Le Logiciel Libre
- Vente liée
---

> HIGH-TECH - Les fans de logiciel libre vont pouvoir gagner quelques dizaines d'euros, mais le grand public trouvera certainement la procédure trop contraignante...
