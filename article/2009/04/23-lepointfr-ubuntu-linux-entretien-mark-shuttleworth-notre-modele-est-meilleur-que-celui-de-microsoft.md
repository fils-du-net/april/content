---
site: LePoint.fr
title: "Ubuntu Linux - Entretien - Mark Shuttleworth : \"Notre modèle est meilleur que celui de Microsoft\""
author: Guerric Poncet
date: 2009-04-23
href: http://www.lepoint.fr/actualites-technologie-internet/2009-04-23/ubuntu-linux-entretien-mark-shuttleworth-notre-modele-est-meilleur-que-celui/1387/0/337220
tags:
- Le Logiciel Libre
- Entreprise
- Innovation
- Promotion
---

> Mark Shuttleworth est un personnage extraordinaire, au sens premier du terme. Riche self-made-man sud-africain, il a investi sa fortune dans Ubuntu (prononcer "oubountou"), une distribution de Linux destinée aux débutants, dont une nouvelle version a été publiée aujourd'hui. En 2002, il s'est offert un voyage de dix jours dans l'espace, avec la mission russe Soyouz TM-34. Rencontre avec un extraterrestre de l'informatique.
