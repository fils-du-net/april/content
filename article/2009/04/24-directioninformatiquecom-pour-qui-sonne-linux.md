---
site: directioninformatique.com
title: "Pour qui sonne Linux?"
author: Nelson Dumais
date: 2009-04-24
href: http://www.directioninformatique.com/DI/client/fr/DirectionInformatique/Nouvelles.asp?id=52937&cid=84
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Linux est une alternative intéressante, mais sa petite part de marché lui confère un statut de minorité qui cause quelquefois des problèmes. Un exemple.
> Le statut de minorité
> C'est quoi 1 %? Pas grand-chose. Parlez-en aux 200 000 Tamouls qui vivent dans le Toronto Métro, ce qui leur confère un gros 4 % de la population, ou encore aux 100 000 Haïtiens qui ont élu domicile sur l'île de Montréal, soit 5 % de l'ensemble, ou même aux 2 millions de diabétiques canadiens, c'est-à-dire quelque 6,5 % de la grande feuille d'érable.
