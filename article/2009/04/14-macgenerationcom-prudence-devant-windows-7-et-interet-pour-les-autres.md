---
site: macgeneration.com
title: "Prudence devant Windows 7 et intérêt pour les autres"
author: Florian Innocente
date: 2009-04-14
href: http://www.macgeneration.com/news/voir/134548/prudence-devant-windows-7-et-interet-pour-les-autres
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Pas de ruée à court terme vers Windows 7, un intérêt marqué pour Mac OS X et croissant pour Ubuntu. Voilà dans les grandes lignes les constats établis par une étude américaine (PDF) signée Dimensional Research et menée le mois dernier auprès de 1154 spécialistes ou responsables informatiques au sein d'entreprises de toutes tailles (17% avaient installé la bêta de Win 7).
