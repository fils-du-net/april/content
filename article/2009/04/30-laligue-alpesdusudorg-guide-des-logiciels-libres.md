---
site: "laligue-alpesdusud.org"
title: "Guide des logiciels libres"
author: Denis Lebioda
date: 2009-04-30
href: http://www.laligue-alpesdusud.org/associatifs_leblog/?2009/04/30/1782-guide-des-logiciels-libres
tags:
- Le Logiciel Libre
- Administration
---

> « Le guide pratique d’usage des logiciels libres dans les administrations » répond à diverses questions - notamment juridiques - que les usagers peuvent se poser lorsqu'il s'agit de mettre en place des Logiciels Libres.
> Ce guide tente de répondre, simplement mais très précisément, à des questions récurrentes que se posent, au sein de l’Administration, les chefs de projet, les responsables des services informatiques, les responsables de marché...
