---
site: zdnet.fr
title: "Hadopi : les Verts et le PC demandent un vote solennel "
date: 2009-04-17
href: http://www.zdnet.fr/actualites/internet/0,39020774,39392671,00.htm
tags:
- HADOPI
---

> Martine Billard, députée écologiste de Paris, a déposé une demande, lors de la conférence des présidents, afin que la loi Création et Internet (dite Hadopi) fasse l'objet d'un vote solennel, impliquant la présence des députés dans l'hémicycle.
