---
site: cyberpresse.ca
title: "Le ministère de l'Éducation remplace ses logiciels sans appel d'offres"
author: Tristan Péloquin
date: 2009-04-01
href: http://technaute.cyberpresse.ca/nouvelles/logiciels/200903/30/01-841475-le-ministere-de-leducation-remplace-ses-logiciels-sans-appel-doffres.php
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Sans lancer le moindre appel d'offres, le ministère de l'Éducation a accordé un contrat de 1,32 million à Microsoft pour acquérir 1800 licences de la suite de bureautique Microsoft Office Professionnel 2007.
> [...] Cette explication fait bien rire Cyrille Béraud, président de Savoir-faire Linux, une firme montréalaise experte dans l'implantation de logiciels libres en entreprise. «C'est comme si le gouvernement changeait son parc automobile pour des BMW en disant qu'il n'y a que BMW qui fabrique des BMW», affirme-t-il.
