---
site: zdnet.fr
title: "Oracle tente de rassurer la communauté MySQL"
author: Christophe Auffray
date: 2009-04-22
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39393180,00.htm
tags:
- Le Logiciel Libre
- Entreprise
---

> Technologie - A l’occasion de la conférence annuelle de MySQL, Ken Jacobs - vice-président d’Oracle- a tenu à rassurer développeurs et utilisateurs sur l’avenir de la base de données Open Source. Objectif : prévenir les migrations vers PostgreSQL.
> Le rachat de Sun par Oracle suscite l'nquiétude quant à l'avenir de MySQL, la base de données Open Source. Même si les deux technologies ne sont pas nécessairement en concurrence frontale (il n'est pas rare par exemple que Oracle et MySQL cohabitent dans les entreprises), le futur de MySQL fait l'objet de spéculations.
