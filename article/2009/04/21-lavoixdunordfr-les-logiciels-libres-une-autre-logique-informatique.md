---
site: lavoixdunord.fr
title: "Les logiciels libres, une autre logique informatique"
author: La Voix du Nord 
date: 2009-04-21
href: http://www.lavoixdunord.fr/Locales/Calais/actualite/Secteur_Calais/2009/04/21/article_les-logiciels-libres-une-autre-logique-i.shtml
tags:
- Le Logiciel Libre
- Éducation
---

> Les Amis de l'Alhambra ont organisé jeudi au cinéma du centre-ville une soirée dédiée aux logiciels libres. La projection du documentaire américain «  Revolution OS » a été suivie d'une discussion animée par François Horn, enseignant-chercheur à Lille I et Lille III.
> [...] L'importance des logiciels libres, pour le moment, continue de croître, remettant en cause le monopole de certaines applications propriétaires, dans des domaines tels que la bureautique ou la navigation sur Internet. L'engouement qu'ils suscitent s'est traduit, au terme de la projection-débat, par le regret des spectateurs de ne pas avoir eu davantage de bonnes adresses et de bons plans pour les télécharger et se former à leur utilisation.
