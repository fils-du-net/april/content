---
site: clubic.com
title: "La loi Création et Internet est votée : et maintenant ?"
author: Alex
date: 2009-04-03
href: http://www.clubic.com/actualite-268454-loi-creation-internet-votee.html
tags:
- April
- HADOPI
---

> [...] Sans surprise, des organisations comme l'April ou la Quadrature du Net déplorent le vote de cette loi, qui fait adopter à la France une position diamétralement opposée à elle du parlement européen. « Cette loi est inadaptée, inacceptable, inapplicable, d'ores et déjà dépassée, et liberticide. Elle rejoindra donc prochainement la DADVSI dans les poubelles de l'histoire législative » estime ainsi Benoît Sibaud, président de l'April.
