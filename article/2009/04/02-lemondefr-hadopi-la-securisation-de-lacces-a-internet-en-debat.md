---
site: lemonde.fr
title: "Hadopi : la \"sécurisation de l'accès à Internet\" en débat"
author: Olivier Dumons
date: 2009-04-02
href: http://www.lemonde.fr/technologies/article/2009/04/02/l-art-et-la-maniere-de-securiser-sa-ligne-internet_1176015_651865.html
tags:
- Logiciels privateurs
- HADOPI
- Matériel libre
---

> [...]
> Ce point aura finalement tourné court, jeudi 2 avril à l'Assemblée nationale, notamment en raison des problèmes liés à l'utilisation de logiciels libres et de logiciels antispams. Cette obligation que doit instaurer le projet de loi soulève de nombreuses questions : les moyens de sécurisation préconisés par le gouvernement seront-ils compatibles avec l'utilisation de logiciels libres, comme le système d'exploitation Linux ?
