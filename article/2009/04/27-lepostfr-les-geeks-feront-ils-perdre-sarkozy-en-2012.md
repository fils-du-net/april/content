---
site: lepost.fr
title: "Les geeks feront-ils perdre Sarkozy en 2012?"
author: AdrienS
date: 2009-04-27
href: http://www.lepost.fr/article/2009/04/27/1512529_les-geeks-feront-ils-perdre-sarkozy-en-2012.html
tags:
- Internet
- HADOPI
- DADVSI
---

> La loi Hadopi a été la goutte d'eau d'un ras-le-bol déjà bien généralisé au sein de la vaste et contrastée communauté des geeks : informaticiens, développeurs, programmeurs, blogueurs amateurs, militants des logiciels libres, simples utilisateurs avancés mais actifs sur les forums d'entraide, etc. Les brevets logiciels au niveau européen, la "Tasca-taxe" sur les supports de mémoire, la loi Dadvsi, le paquet télécom, les volontés de contrôle de l'information, les délires sur les "labels" de site d'info...
