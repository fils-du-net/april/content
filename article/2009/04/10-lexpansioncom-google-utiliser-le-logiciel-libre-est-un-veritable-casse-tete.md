---
site: lexpansion.com
title: "Google: «Utiliser le logiciel libre est un véritable casse-tête»"
author: Jean-Baptiste Su
date: 2009-04-10
href: http://www.lexpansion.com/economie/actualite-high-tech/google-utiliser-le-logiciel-libre-est-un-veritable-casse-tete_179062.html
tags:
- Le Logiciel Libre
- Philosophie GNU
---

> Le célèbre moteur de recherche est un grand adepte du logiciel libre. Mais il en connaît aussi les risques au point de s'être organisé pour s'en prémunir. Les explications de Chris DiBona, le Monsieur Open Source de Google.
> Logiciel libre ne veut pas forcement dire libre de droit. Une leçon que sont en train d'apprendre à leurs dépens Cisco et Illiad, le distributeur de la Freebox, qui utilisent dans leurs produits des bouts de logiciels libres.
