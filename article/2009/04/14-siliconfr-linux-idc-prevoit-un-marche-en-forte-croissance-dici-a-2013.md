---
site: silicon.fr
title: "Linux: IDC prévoit un marché en forte croissance d'ici à 2013"
author: Par David Feugey
date: 2009-04-14
href: http://www.silicon.fr/fr/news/2009/04/14/linux__idc_prevoit_un_marche_en_forte_croissance_d_ici_a__2013
tags:
- Le Logiciel Libre
- Informatique en nuage
---

> À la demande de la fondation Linux, IDC a évalué le potentiel de Linux dans la nouvelle économie.
