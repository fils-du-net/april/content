---
site: lemondeinformatique.fr
title: "Hadopi : des réactions divergentes après le plébiscite des députés"
author: Vincent Delfau 
date: 2009-04-03
href: http://www.lemondeinformatique.fr/actualites/lire-hadopi-des-reactions-divergentes-apres-le-plebiscite-des-deputes-28379.html
tags:
- Le Logiciel Libre
- HADOPI
---

> [...] Si la Fédération française des télécoms s'emporte également contre le texte, elle ne reprend pas les reproches adressés par les deux acteurs précédents. La FFT point en revanche que les personnes sanctionnées n'auront plus à acquitter le montant de leur abonnement lorsque leur connexion sera suspendue. « Si une telle disposition était maintenue dans la loi, cela signifierait tout simplement la fin du modèle économique des offres composites qui a permis le succès du haut débit en France ».
