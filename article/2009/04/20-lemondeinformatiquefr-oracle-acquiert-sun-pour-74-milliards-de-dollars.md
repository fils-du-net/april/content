---
site: lemondeinformatique.fr
title: "Oracle acquiert Sun pour 7,4 milliards de dollars"
author: Olivier Rafal 
date: 2009-04-20
href: http://www.lemondeinformatique.fr/actualites/lire-oracle-acquiert-sun-pour-7-4-milliards-de-dollars-28464.html
tags:
- Entreprise
---

> Le Wall Street Journal et Bloomberg News avaient tout faux : c'est finalement Oracle, et non IBM, qui rachète Sun. Pour un montant de 9,50$ par action, soit environ 7,4 Md$. Déduction faite des immenses réserves de cash de Sun, l'acquisition coûtera en fait à Oracle 5,6 Md$.
