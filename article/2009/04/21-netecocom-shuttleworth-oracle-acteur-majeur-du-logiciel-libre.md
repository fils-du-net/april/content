---
site: neteco.com
title: "Shuttleworth : Oracle, acteur majeur du logiciel libre"
author: Guillaume Belfiore
date: 2009-04-21
href: http://www.neteco.com/271716-oracle-shuttleworth-sun-libre-open-source.html
tags:
- Le Logiciel Libre
- Entreprise
---

> Mark Shuttleworth, président de Canonical Ltd : « Ce qui est intéressant dans cette histoire, c'est que cela renforce l'idée que le logiciel libre et open source reste véritablement le moteur du marché aujourd'hui », avant d'ajouter qu'Oracle est désormais : « la plus grande société de logiciels open source ».
