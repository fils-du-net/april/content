---
site: tsr.ch
title: "Les livres électroniques à la conquête de l'Europe"
author: ats/sbo
date: 2009-04-14
href: http://www.tsr.ch/tsr/index.html?siteSect=200001&sid=10570935
tags:
- Interopérabilité
- DRM
---

> Présents depuis longtemps aux Etats-Unis, les livres électroniques sont maintenant disponibles en Europe. Appelés «livrels» au Canada, ces appareils sont plus légers que les livres en papier et peuvent afficher des milliers de pages, mais présentent encore des désavantages.
