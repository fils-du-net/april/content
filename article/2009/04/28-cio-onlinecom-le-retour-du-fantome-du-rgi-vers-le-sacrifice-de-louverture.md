---
site: "cio-online.com"
title: "Le retour du fantôme du RGI : vers le sacrifice de l'ouverture"
author: Bertrand Lemaire
date: 2009-04-28
href: http://www.cio-online.com/actualites/lire-le-retour-du-fantome-du-rgi-vers-le-sacrifice-de-l-ouverture-2179.html
tags:
- Administration
- Interopérabilité
- RGI
---

> Le Référentiel Général d'Interopérabilité (RGI) est de nouveau annoncé pour l'été 2009. Ces normes garantissant la pérennité et la pertinence des investissements informatiques semblent pourtant bien allégées...
> [...] Le RGI vise à établir des règles tant techniques que procédurales dans la conception et la réalisation des SI d'Etat. En France, non seulement les marchés publics d'Etat informatiques sont très importants mais ces règles ne pourront qu'inspirer également les collectivités locales et le secteur privé. Ce référentiel sera donc extrêmement important pour tous les fournisseurs informatiques.
> Une version presque finalisée avait été publiée il y a près de deux ans qui donnait une large place aux formats ouverts normalisés (PDF/A, OpenDocument...), ouverture et normalisation étant des gages de pérennité et d'interopérabilité entre systèmes fournis par des éditeurs ou des concepteurs différents. Mais cet attachement à la concurrence n'avait guère plu à certains fournisseurs, parmi lesquels surtout Microsoft. Un intense lobbying avait alors abouti au gel du RGI.
