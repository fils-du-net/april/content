---
site: zdnet.fr
title: "Hadopi : Christine Albanel reste confiante malgré le vote des eurodéputés"
date: 2009-04-22
href: http://www.zdnet.fr/actualites/internet/0,39020774,39393206,00.htm
tags:
- HADOPI
- Institutions
---

> Le ministère de la Culture affiche sa confiance pour le vote de la loi Création et Internet, dite Hadopi, malgré l'adoption de l'amendement 46 par les eurodéputés.
> Interrogé par Nouvelobs.com, le cabinet de Christine Albanel estime que «sur le plan juridique, cet amendement n'empêchera pas la France d'adopter son projet de loi car la suspension de l'accès internet à domicile après de multiples avertissements n'est pas une atteinte aux droits et libertés fondamentaux.»
