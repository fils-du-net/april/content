---
site: pcinpact.com
title: "Hadopi : réaction de la Quadrature du net et du monde du Libre"
author: Marc Rees
date: 2009-04-03
href: http://www.pcinpact.com/actu/news/50172-april-quadrature-net-reactions-hadopi.htm
tags:
- April
- HADOPI
---

> [...] Jérémie Zimmermann, cofondateur de la Quadrature « félicite et remercie les innombrables citoyens qui ont participé à une formidable opération de sensibilisation de l'opinion publique et des élus. Nous sommes tous ensemble intervenus dans le débat, et nos arguments ont résonné en continu dans l'hémicycle, par la bouche de députés de tous les bancs, face au mur coupable des godillots de l'UMP. Nous devons continuer cette information de nos concitoyens et rester vigilants contre toute tentative de contrôle du Net.
