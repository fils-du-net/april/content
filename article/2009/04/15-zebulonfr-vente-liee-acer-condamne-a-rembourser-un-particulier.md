---
site: zebulon.fr
title: "Vente liée : Acer condamné à rembourser un particulier"
author: Relaxnews
date: 2009-04-15
href: http://www.zebulon.fr/actualites/3562-vente-liee-acer-condamne-rembourser-particulier.html
tags:
- Vente liée
---

> [...] Conformément à la décision du tribunal de Chinon, Acer devra rembourser un de ses clients qui n'avait pas l'usage des logiciels installés sur l'ordinateur qu'il venait d'acheter, à commencer par le système d'exploitation. Le fabricant taïwanais devra lui rembourser la somme de 50 euros au titre des licences.
