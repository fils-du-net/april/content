---
site: vnunet.fr
title: "Open source en entreprise : \"Une migration à réaliser en plusieurs étapes consécutives\""
author: Anne Confolant
date: 2009-04-01
href: http://www.vnunet.fr/news/open_source_en_entreprise_une_migration_a_realiser_en_plusieurs_etapes_consecutives_-2030477
tags:
- Le Logiciel Libre
- Entreprise
---

> Pour quelles raisons les entreprises devraient-elles délaisser les systèmes propriétaires pour migrer vers des solutions open source ? A l'occasion d'une table ronde organisée sur ce thème lors du salon Solutions Linux de Paris (Palais des Expositions, Porte de Versailles), des professionnels de l'open source ont partagé leur expertise dans ce domaine pour expliquer les bénéfices apportés par un déploiement contrôlé et sécurisé vers des systèmes open source.
