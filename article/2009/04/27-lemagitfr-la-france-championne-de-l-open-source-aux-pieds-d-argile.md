---
site: lemagit.fr
title: "La France, championne de l’open source aux pieds d’argile "
author: David Castaneira
date: 2009-04-27
href: http://www.lemagit.fr/article/france-redhat-open-source-index/3135/1/la-france-championne-open-source-aux-pieds-argile/
tags:
- Le Logiciel Libre
- Administration
---

> Une tendance au reflux ou du moins à la précarité de la vague open-source qui semble confirmée par le second volet du travail des chercheurs du Georgia Tech. En terme d’« environnement », la France se classe au 15ème rang, loin derrière la Suède – numéro un – mais surtout les Etats-Unis, qui prennent la deuxième place de ce classement plus spéculatif et prospectif, fondé sur des critères éducatifs, de pénétration des technologies, ou encore socio-économiques.
