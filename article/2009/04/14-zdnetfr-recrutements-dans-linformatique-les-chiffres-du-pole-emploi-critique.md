---
site: zdnet.fr
title: "Recrutements dans l'informatique : les chiffres du Pôle Emploi critiqué"
author: Christophe Auffray
date: 2009-04-14
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39392107,00.htm
tags:
- Entreprise
---

> Analyse - Le Pôle Emploi et le Credoc prévoient en 2009 près de 37 000 recrutements de cadres et ingénieurs de l’informatique. Or selon d’autres sources, les créations nettes d’emplois devraient pourtant être nulles. Explications.
