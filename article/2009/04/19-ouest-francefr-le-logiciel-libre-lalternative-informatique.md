---
site: "ouest-france.fr"
title: "Le logiciel libre, l'alternative informatique"
author: Bruno ALVAREZ
date: 2009-04-19
href: http://www.ouest-france.fr/actu/actu_BN_-Le-logiciel-libre-l-alternative-informatique_8619-902681_actu.Htm
tags:
- Le Logiciel Libre
- April
---

> Promouvoir les logiciels libres auprès du grand public, c'est aussi l'objectif de votre rencontre régionale qui se tient à Saint-Brieuc jusqu'à ce soir ?
> Oui. C'est la deuxième édition de cet événement. Nous organisons des conférences, des démonstrations d'applications et des ateliers d'installation. Nous avons dix exposants présents dont l'association April, pionnière du logiciel libre en France. Elle compte plus de 4 000 adhérents. L'an dernier, nous avions accueilli 200 visiteurs.
