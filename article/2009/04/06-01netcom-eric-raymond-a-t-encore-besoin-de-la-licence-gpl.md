---
site: 01net.com
title: "Eric Raymond : « A-t-on encore besoin de la licence GPL ? »"
author: Tristan Nitot
date: 2009-04-06
href: http://pro.01net.com/editorial/500894/eric-raymond-a-t-on-encore-besoin-de-la-licence-gpl/
tags:
- Le Logiciel Libre
- Licenses
---

> Il y a quelques jours, Eric Raymond, l'un des principaux théoriciens de l'open source, jetait un pavé dans la mare en remettant en cause la légitimité de la licence GPL. Avant de voir pourquoi ce pavé, comme tout pavé, fait des vagues quand il arrive dans la mare, il faut bien comprendre ce qu'est la licence GPL.
