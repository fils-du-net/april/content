---
site: nordeclair.fr
title: "Une install party Linux avec l'école d'ingénieurs IG2I"
author: Agnès Bourahla-Farine
date: 2009-04-20
href: http://www.nordeclair.fr/Locales/Lens/2009/04/20/une-install-party-linux-avec-l-ecole-d-i.shtml
tags:
- Le Logiciel Libre
---

> Le principe d'une recette de cuisine
> Florent et Yohan expliquent que le principe de cette journée, qui a lieu deux fois par an depuis 2002, est de faire découvrir aux non initiés les logiciels libres, et plus particulièrement Linux. Un système d'exploitation libre que les deux étudiants connaissent bien. « C'est le même principe qu'une recette de cuisine, indique Florent. Pour réaliser le "plat", on compile des éléments. Chaque membre de la communauté peut faire grossir le système, apporter des améliorations ».
