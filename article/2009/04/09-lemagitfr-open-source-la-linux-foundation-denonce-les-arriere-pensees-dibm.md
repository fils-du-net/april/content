---
site: lemagit.fr
title: "Open Source : la Linux Foundation dénonce les arrière-pensées d'IBM "
author: Cyrille Chausson
date: 2009-04-09
href: http://www.lemagit.fr/article/ibm-linux-communaute-open-source/2985/1/open-source-linux-foundation-denonce-les-arriere-pensees-ibm/
tags:
- Le Logiciel Libre
- Entreprise
---

> La participation des grands groupes dans les communautés de l'Open Source doit être davantage mesurée et structurée. C'est le sentiment de la Linux Foundation qui, à l'occasion d'une conférence organisée par cette fondation en charge de protéger et de standardiser Linux, a pointé du doigt IBM. Lui reprochant d'avoir privilégié ses propres intérêts, avant ceux du noyau Linux, lorsque le groupe a décidé de participer activement aux développements de Linux en 1999.
