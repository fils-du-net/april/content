---
site: 01 Informatique
title: "10 ans de Libre"
author: Renaud Bonnet, Oliver Roberget, Yann Serra, Francisco Villacampa
date: 2009-04-16
tags:
- Entreprise
---

> Partage de code, développement communautaire, réutilisation... Les pionniers du libre ont transformé les modèles des éditeurs propriétaires. Mais ils ont aussi dû composer avec la réalité du marché.
