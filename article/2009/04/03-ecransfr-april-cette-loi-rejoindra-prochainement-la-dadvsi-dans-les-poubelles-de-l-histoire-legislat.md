---
site: ecrans.fr
title: "April : « Cette loi rejoindra prochainement la DADVSI dans les poubelles de l’histoire législative »"
author: Astrid Girardeau
date: 2009-04-03
href: http://www.ecrans.fr/April-Cette-loi-rejoindra,6828.html
tags:
- April
- HADOPI
- DADVSI
---

> Le logiciel libre a eu la vie dure ces derniers jours lors des séances à l’Assemblée Nationale sur le projet Création et Internet. Malgré la défense de quelques députés, la ministre de la Culture, Christine Albanel, le rapporteur Franck Riester, et la majorité des députés UMP se sont montrés hermétiques à toute proposition d’interopérabilité, et plus généralement au logiciel libre. Selon Frédéric Couchet, délégué général de l’April, ils « pénalisent un secteur économique dynamique et innovant en s’en prenant injustement à ses acteurs ».
