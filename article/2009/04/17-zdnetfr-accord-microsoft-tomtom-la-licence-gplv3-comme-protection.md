---
site: zdnet.fr
title: "Accord Microsoft - TomTom : la licence GPLv3 comme protection"
author: La rédaction
date: 2009-04-17
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39392605,00.htm
tags:
- Le Logiciel Libre
- Licenses
- Philosophie GNU
---

> Selon le Software Freedom Law Center, la licence GPL version 3 permettrait d'interdire à Microsoft d'attaquer les utilisateurs du logiciel libre, comme TomTom contraint à un accord juridique.
> Après s'être mutuellement attaqués en justice pour violation de brevets, Microsoft et TomTom avaient finalement conclu un accord à la fin du mois de mars. Le constructeur de GPS acceptait ainsi de payer Microsoft pour un montant non divulgué en échange du droit d'exploiter, pour une durée de cinq ans, certains brevets.
