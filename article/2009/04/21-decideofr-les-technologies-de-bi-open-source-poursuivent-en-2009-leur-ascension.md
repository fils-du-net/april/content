---
site: decideo.fr
title: "Les technologies de BI Open Source poursuivent en 2009 leur ascension"
author: Renaud Finaz de Villaine
date: 2009-04-21
href: http://www.decideo.fr/Les-technologies-de-BI-Open-Source-poursuivent-en-2009-leur-ascension_a3193.html
tags:
- Le Logiciel Libre
- Entreprise
- Informatique en nuage
---

> [...] Les rapports des analystes informatiques, et notamment ceux qui donnent le "la" au niveau mondial en matière de BI, confirment cette mutation. Tous prennent désormais en compte les solutions proposées par les acteurs du Libre et les positionnent face aux offres propriétaires.
