---
site: xinhuanet.com
title: "Informatique: Accord Chine-Sun Microsystem pour l'enseignement de l'open-source"
author: xinhuanet
date: 2009-04-22
href: http://www.french.xinhuanet.com/french/2009-04/22/content_863896.htm
tags:
- Le Logiciel Libre
- Entreprise
- Institutions
---

> "Le développement de la technologie d'open source représente  pour la Chine à la fois un défi et une opportunité" a déclaré Li  Bing, le directeur adjoint de la SAFEA. "Ce projet aidera  l'enseignement supérieur du pays à s'approcher des premières  lignes sur le front de l'industrie mondial de l'informatique".
