---
site: clubic.com
title: "La Fondation Linux veut une alternative au FAT"
author: Guillaume
date: 2009-04-02
href: http://www.clubic.com/actualite-268222-microsoft-linux-fat.html
tags:
- Le Logiciel Libre
- Brevets logiciels
---

> Jim Zemlin, directeur au sein de la Fondation Linux, a publié un billet dans lequel il invite la communauté à développer une alternative au système de fichiers propriétaire FAT de Microsoft. En effet, il estime que la firme de Redmond entretien une position hypocrite sur le paysage open source et les questions relatives à l' interopérabilité.
