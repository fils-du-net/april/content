---
site: "cio-online.com"
title: "Le FDI prépare l'avenir français d'Internet et sa disparition"
author: Bertrand Lemaire 
date: 2009-04-30
href: http://www.cio-online.com/actualites/lire-le-fdi-prepare-l-avenir-francais-d-internet-et-sa-disparition-2187.html
tags:
- Internet
- April
---

> Le Forum des Droits sur Internet (FDI) a présenté le 29 avril son septième et probable dernier rapport d'activité. Il devrait en effet être fusionné avec d'autres organismes d'ici l'été dans le CNN (Conseil National du Numérique). Le travail sur le filtrage d'Internet débouchera sur une loi d'ici peu. Par ailleurs, le rôle de médiateur du FDI est en pleine croissance.
