---
site: lemondeinformatique.fr
title: "Minix, un OS Open Source stable désormais financé par l'UE"
author: Olivier Rafal 
date: 2009-04-28
href: http://www.lemondeinformatique.fr/actualites/lire-minix-un-os-open-source-stable-desormais-finance-par-l-ue-28517.html
tags:
- Le Logiciel Libre
- Europe
---

> Pourquoi est-on habitué à ce que les télévisions ou les téléphones fonctionnent sans souci des années durant, et à ce qu'il faille régulièrement réinitialiser des ordinateurs ? C'est avec cette question en tête que le professeur Andy Tanenbaum, qui enseigne au département des sciences informatiques de la Vrije Universiteit d'Amsterdam, a démarré la version 3 de Minix et obtenu pour ce faire une subvention de l'Union européenne de 2,4 M€.
