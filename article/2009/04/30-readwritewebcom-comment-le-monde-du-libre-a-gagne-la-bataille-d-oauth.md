---
site: readwriteweb.com
title: "Comment le monde du Libre à gagné la bataille d’OAuth"
author: Fabrice Epelboin
date: 2009-04-30
href: http://fr.readwriteweb.com/2009/04/30/a-la-une/comment-le-monde-du-libre-gagne-bataille-oauth/
tags:
- Le Logiciel Libre
- Interopérabilité
---

> OAuth est une technologie permettant de gérer l'authentification d’un utilisateur d’un site à l'autre, c’est une pièce critique de la gestion de l'identité sur le web, et de plus en plus de grandes sociétés de l'internet l'ont adopté. Cette technologie est « libre », elle n’appartient à personne, elle est le fruit d’une longue collaboration impliquant une multitude de sociétés et d’acteurs du Libre, au sein d’une communauté « ouverte ».
