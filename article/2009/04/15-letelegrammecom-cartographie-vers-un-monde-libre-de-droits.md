---
site: letelegramme.com
title: "Cartographie. Vers un monde libre... de droits"
author: Cathy Tymen
date: 2009-04-15
href: http://www.letelegramme.com/ig/generales/regions/commentaires/cartographie-vers-un-monde-libre-de-droits-15-04-2009-336060.php
tags:
- Administration
- Partage du savoir
- Licenses
---

> Parce qu'il ne pouvait pas reproduire sans autorisation l'équivalent des cartes IGN dans son pays, un Anglais, Steeve Coast, a eu l'idée, en juillet2004, de créer un site sur lequel des internautes bénévoles pourraient apporter leur pierre à la réalisation d'une cartographie à l'échelle... mondiale. Open Street Map (OSM) a été créé dans l'esprit des logiciels libres ou de Wikipedia. C'est une carte numérique sur laquelle chacun peut apporter sa pierre.
