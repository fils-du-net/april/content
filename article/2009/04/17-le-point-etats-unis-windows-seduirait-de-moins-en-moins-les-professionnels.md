---
site: lepoint.fr
title: "Etats-Unis - Windows séduirait de moins en moins les professionnels"
author: Guillaume Chauvigné
date: 2009-04-17
href: http://www.lepoint.fr/actualites-technologie-internet/2009-04-18/etats-unis-windows-seduirait-de-moins-en-moins-les-professionnels/1387/0/335896
tags:
- Entreprise
- Économie
---

> [...] le prochain Windows devrait redorer le blason de la famille Windows. Pourtant, près de 50 % des sondés réfléchissent à l'éventualité de basculer vers des systèmes d'exploitation concurrents.
