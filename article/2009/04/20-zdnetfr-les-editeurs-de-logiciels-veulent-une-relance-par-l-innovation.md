---
site: zdnet.fr
title: "Les éditeurs de logiciels veulent une relance par l’innovation"
author: Christophe Auffray
date: 2009-04-20
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39392950,00.htm
tags:
- Le Logiciel Libre
- Entreprise
- Économie
---

> Des exonérations pour le mécénat dans l'Open Source
> Dans le domaine du libre, Tangui Morlier défend l'idée d'exonérations attachées au mécénat. Une entreprise donnant de l'argent à des projets Open Source bénéficierait ainsi d'avantages fiscaux. Le porte-parole de l'April, comme ceux l'Afdel et de Syntec Informatique, appellent également à adapter le small business act américain (SBA).
