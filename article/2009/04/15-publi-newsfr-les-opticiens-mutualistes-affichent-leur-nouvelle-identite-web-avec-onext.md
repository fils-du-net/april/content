---
site: "publi-news.fr"
title: "Les Opticiens Mutualistes affichent leur nouvelle identité web avec Onext   "
author: PUBLI-NEWS
date: 2009-04-15
href: http://www.publi-news.fr/data/15042009/15042009-085252.html
tags:
- Le Logiciel Libre
- Entreprise
---

> Dans tous les cas le groupement mutualiste souhaitait s’appuyer sur un outil venant du libre. « Nous nous appuyons sur les logiciels libres depuis plusieurs années. Dès que nous engageons une réflexion sur un nouvel outil informatique nous évaluons les outils du marché libre ou non. Nous recherchons en permanence des technologies matures et nous permettant de ne pas rester enfermés dans un système propriétaire.
