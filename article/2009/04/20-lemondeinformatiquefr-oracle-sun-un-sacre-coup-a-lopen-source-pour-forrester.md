---
site: lemondeinformatique.fr
title: "Oracle-Sun : un sacré coup à l'Open Source, pour Forrester"
author: Olivier Rafal 
date: 2009-04-20
href: http://www.lemondeinformatique.fr/actualites/lire-sun-oracle-un-sacre-coup-a-l-open-source-pour-forrester-28466.html
tags:
- Entreprise
---

> LeMondeInformatique.fr : Quel est votre sentiment après l'annonce de cette acquisition ?
> Henry Peyret : D'abord je pense que cela fera énormément de mal à l'Open Source. Oracle a fait de l'Open Source pour soutenir Linux, mais sur les autres segments, les offres Open Source sont fortement concurrentes.
