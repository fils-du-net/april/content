---
site: tekiano.com
title: "Tunisie: Journée du logiciel (pas encore) libéré "
author: Lotfi Ben Cheikh
date: 2009-04-29
href: http://www.tekiano.com/informatique/informatik-news/3-4-428/tunisie-journee-du-logiciel-pas-encore-libere.html
tags:
- Le Logiciel Libre
- Institutions
---

> A croire que malgré toute l'énergie déployée pour le promouvoir, le logiciel libre ne brille pas vraiment par sa popularité en Tunisie. Ainsi selon Chaker Zaafouri, d’alpha Engineering « les sociétés fuient les solutions open sources pour des questions de sécurité. Vu que le code est ouvert et disponible à tout le monde »… Malgré toutes les dénégations de la communauté du libre, qui se plaît à rappeler les mises à jour continuelles des systèmes de sécurité.
