---
site: Clubic.com
title: "Ubuntu : netbook et intox médiatique de Microsoft "
author: Guillaume
date: 2009-04-09
href: http://www.clubic.com/actualite-269488-ubuntu-microsoft-marche-chiffres-netbook.html
tags:
- Le Logiciel Libre
- Entreprise
- Économie
- Vente liée
- Désinformation
- Promotion
---

> Mardi dernier, nous rapportions que Brandon LeBlanc, en charge du département Windows Communications,  annonçait sur le blog officiel de Microsoft que le système Windows était distribué sur 96% des netbooks commercialisés aux Etats-Unis. Ce message ne fut pas sans conséquences et Chris Kenyon, du groupe OEM Services chez Canonical, accuse la firme de Redmond d'intox médiatique.
> [...]
> Le magazine Computerworld souligne d'ailleurs que même si 96% des netbooks vendus aux Etats-Unis sont sous Windows, l'Amérique du Nord ne représenterait que 20% du marché mondial. A travers le monde, 30% des netbooks commercialisés seraient sous Linux.
