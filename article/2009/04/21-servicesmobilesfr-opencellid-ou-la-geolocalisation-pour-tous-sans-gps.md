---
site: servicesmobiles.fr
title: "OpenCellID, ou la geolocalisation pour tous sans GPS !"
author: servicesmobiles
date: 2009-04-21
href: http://www.servicesmobiles.fr/services_mobiles/2009/04/opencellid-ou-la-geolocalisation-pour-tous.html
tags:
- Partage du savoir
- Innovation
---

> Comment amener la géo-localisation sur les téléphones n’ayant pas (encore) de GPS ? Tel est le but de l’initiative d’OpenCellID. En effet, l’objectif de ce projet ambitieux, Open Source et collaboratif, est d’arriver à cartographier l’ensemble des "Cellules"  du réseau GSM – les fameuses antennes  relais. Chaque mobile est en permanence relié à la "cellule" la plus proche qui couvre une zone de quelques centaines de mètres en zone urbaine à quelques dizaines de kilomètres en zone rurale.
