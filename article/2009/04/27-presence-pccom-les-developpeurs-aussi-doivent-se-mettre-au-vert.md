---
site: "presence-pc.com"
title: "Les développeurs aussi doivent se mettre au vert "
author: Loic Duval
date: 2009-04-27
href: http://www.presence-pc.com/tests/developpement-vert-23104/
tags:
- Le Logiciel Libre
- Innovation
---

> Comme le résumait Arjan  Van De Ven, patron de la branche Linux d’Intel, lors d’une récente conférence, "le matériel est aujourd'hui capable de réduire automatiquement sa consommation, encore faut il que le logiciel le lui permette!". Il est temps pour les développeurs de se mettre au "vert". Techniques, bonnes pratiques, outils et opportunités… Voici un tour d'horizon de l'univers du "Green Developer".
