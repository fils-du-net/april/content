---
site: challenges.fr
title: "Mais où est passé Microsoft ?"
author: Gilles Fontaine
date: 2009-04-06
href: http://tech.blogs.challenges.fr/archive/2009/04/06/mais-ou-est-passe-microsoft.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Sale printemps pour Microsoft. La semaine dernière, coup sur coup, nous apprenions que le groupe informatique arrêtait le service de son encyclopédie en ligne Encarta, impuissant à rivaliser avec l'incontournable Wikipedia et que, pour la première fois, Firefox 3.0 était passé devant Internet Explorer en Europe. Une écrasante victoire de l'Open source sur le géant du logiciel, comme me le rappelait Stéphane Zibi de Spread Factory.
