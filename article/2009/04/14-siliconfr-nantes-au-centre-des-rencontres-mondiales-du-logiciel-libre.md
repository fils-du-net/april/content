---
site: silicon.fr
title: "Nantes au centre des Rencontres Mondiales du Logiciel Libre"
author: David Feugey
date: 2009-04-14
href: http://www.silicon.fr/fr/news/2009/04/14/nantes_au_centre_des_rencontres_mondiales_du_logiciel_libre
tags:
- Le Logiciel Libre
- Matériel libre
---

> Les dixièmes Rencontres Mondiales du Logiciel Libre (RMLL) se dérouleront du 7 au 11 juillet 2009, à l’Université de Nantes (Loire-Atlantique). Ce rendez-vous, avec entrée gratuite, sera l’occasion de découvrir le monde de l’open source. Il fait suite aux sessions précédentes, organisées à Mont-de-Marsan, Amiens, Vandœuvre-lès-Nancy, Dijon et Bordeaux.
