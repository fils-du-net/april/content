---
site: toutenbd.com
title: "Un manga pour expliquer Ubuntu"
author: toutenbd.com
date: 2009-04-18
href: http://www.toutenbd.com/article.php3?id_article=2898
tags:
- Le Logiciel Libre
- Éducation
---

> Il y a quelques mois, Google avait fait réaliser une bande dessinée expliquant les avantages de son navigateur (info du 2/09/08). Sur le même principe et à quelques jours de la sortie de la nouvelle version d’Ubuntu ("Jaunty Jackalope"), un manga est disponible en japonais, anglais et français.
