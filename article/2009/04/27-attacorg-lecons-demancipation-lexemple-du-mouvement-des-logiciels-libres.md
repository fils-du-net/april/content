---
site: attac.org
title: "Leçons d'émancipation : l'exemple du mouvement des logiciels libres"
author: Hervé Le Crosnier
date: 2009-04-27
href: http://www.france.attac.org/spip.php?article9864
tags:
- Le Logiciel Libre
---

> Un mouvement ne parle que rarement de lui-même. Il agit, propose, théorise parfois sa propre pratique, mais ne se mêle qu’exceptionnellement de la descendance de son action dans les autres domaines, qu’ils soient analogues, tels ici les autres mouvement dans le cadre de la propriété immatérielle, ou qu’ils soient plus globalement anti-systémiques. [...] Approche « pragmatique » et approche « philosophique » ne sont pas incompatibles, c’est du moins la principale leçon politique que je pense tirer de ce mouvement et de son impact plus global sur toute la société.
