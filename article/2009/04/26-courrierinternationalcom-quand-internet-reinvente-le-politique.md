---
site: courrierinternational.com
title: "Quand Internet réinvente le politique"
author: Xavier Molénat
date: 2009-04-26
href: http://trappesenemispheresud.blogs.courrierinternational.com/archive/2009/04/23/quand-internet-reinvente-le-politique.html
tags:
- Internet
---

> Une enquête menée auprès de cyberactivistes montre comment Internet est devenue un véritable laboratoire politique. Scripts, blogs, forums, listes de discussions reformulent sans cesse les cadres du débat public.
> [...] Les auteurs, dans un livre informé et riche, montrent que les cyberactivistes n’ont rien des technophiles naïfs et béats que moquait naguère Serge Halimi. Ils ont une vraie culture politique (présentant même souvent un passé de militant traditionnel) et mènent une réflexion sur leurs propres pratiques. Leur désaffiliation vis-à-vis des structures politiques institutionnelles ne peut guère être lue comme une simple défection, elle constitue le point de départ d’une autre prise de parole. Les références de ces « orphelins de la politique » sont hétérogènes : ils puisent autant dans la culture des hackers, ces pirates informatiques provocateurs et créatifs, que dans les avant-gardes artistiques, le situationnisme ou la contre-culture américaine. S’y retrouvent aussi bien des partisans du logiciel libre, attachés au droit au savoir et à l’information, que des altermondialistes, des écologistes ou des artistes engagés.
