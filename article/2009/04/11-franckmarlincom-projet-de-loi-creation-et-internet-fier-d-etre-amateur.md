---
site: franckmarlin.com
title: "Projet de loi création et internet : \"fier d’être amateur !\""
author: Franck Marlin
date: 2009-04-11
href: http://www.franckmarlin.com/site/content/view/150/1/index.html
tags:
- Le Logiciel Libre
- Institutions
---

> Le rejet à l’Assemblée nationale ce jeudi 9 avril 2009 du projet de loi « création et internet » qui a pour objet notamment de lutter contre le téléchargement illégal aurait, selon la presse, suscité la colère de Nicolas Sarkozy qui aurait ainsi tonné : « C’est de l’amateurisme lamentable. » En réaction à ces propos qui visaient le Président du groupe UMP à l’Assemblée nationale et le secrétaire d’Etat en charge des relations avec le Parlement, Franck Marlin tient à exprimer sa solidarité avec les personnalités ciblées par les commentaires présidentiels et dire sa fierté d’être amateur !
