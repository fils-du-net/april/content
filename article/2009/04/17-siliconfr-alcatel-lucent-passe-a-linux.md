---
site: silicon.fr
title: "Alcatel-Lucent passe à Linux !"
author: David Feugey 
date: 2009-04-17
href: http://www.silicon.fr/fr/news/2009/04/17/alcatel_lucent_passe_a_linux__
tags:
- Le Logiciel Libre
- Entreprise
---

> Pour la firme, l’adoption de Linux permettra d’augmenter le nombre de paquetages logiciels disponibles pour ses produits réseau. Elle profitera ainsi de l’importante dynamique des logiciels libres. [...] L’adoption de Linux par le constructeur est une nouvelle victoire de taille pour les logiciels libres, qui investissent aujourd’hui presque toute l’industrie. Au final, il n’y a plus guère que dans les PC et les stations de travail classiques (ainsi que dans certains serveurs et le monde de la téléphonie mobile) que Linux tarde à s’imposer.
