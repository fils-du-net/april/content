---
site: OWNI
title: "Et si le droit d’auteur s’appliquait aux tatouages?"
author: Calimaq
date: 2009-04-09
href: http://owni.fr/2011/04/09/droit-dauteur-tatouages/
tags:
- Droit d'auteur
- ACTA
---

> Une question qui peut sembler saugrenue, voire absurde, et qui pourtant est un véritable casse-tête pour les juristes. Décidément, le droit d'auteur n'a pas fini de nous étonner !
