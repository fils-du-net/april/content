---
site: Silicon.fr
title: "Selon Red Hat, la France est la championne de l’open source"
author: David Feugey
date: 2009-04-22
href: http://www.silicon.fr/fr/news/2009/04/22/selon_red_hat__la_france_est_la_championne_de_l_open_source
tags:
- Le Logiciel Libre
- Entreprise
- Économie
---

> Et pourtant le rapport de la firme montre que notre pays n’est pas le territoire le plus propice à ce marché.
> Red Hat a lancé une étude portant sur l’activité dans le domaine des logiciels libres constatée au sein de 75 pays différents.
> Bonne surprise, la France prend la tête de ce classement, suivie par l’Espagne, l’Allemagne, l’Australie, la Finlande, le Royaume-Uni, la Norvège, l’Estonie, Les États-Unis et le Danemark. Les pays d’Europe semblent donc particulièrement actifs dans le domaine de l’open source, les États-Unis devant se contenter de la neuvième place.
