---
site: neteco.com
title: "Hadopi : la Sacem pleure, l'April rit"
author: Ariane Beky
date: 2009-04-10
href: http://www.neteco.com/269808-hadopi-sacem-pleure-april-rit-creation-internet.html
tags:
- HADOPI
---

> Le rejet surprise du projet de loi création et internet (Hadopi) jeudi devant une Assemblée très clairsemée fait jaser. Alors que la Sacem se déclare « consternée » par ce rejet, l'April parle « d'une victoire pour les citoyens et les libertés informatiques ».
