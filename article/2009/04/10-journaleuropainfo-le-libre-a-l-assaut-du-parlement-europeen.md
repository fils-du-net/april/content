---
site: journaleuropa.info
title: "Le Libre à l’assaut du parlement européen"
author: Julien Fournier
date: 2009-04-10
href: http://www.journaleuropa.info/FR_article/n503t0j0d0-europe-pacte-du-logiciel-libre-hadopi-.html
tags:
- Le Logiciel Libre
- Institutions
- Europe
---

> À l’initiative de l’April, le récent « Pacte du logiciel libre » propose aux citoyens de s’emparer des enjeux des logiciels libres et de sensibiliser les eurodéputés contre le retour de l’informatique déloyale. Les élections approche, et c’est à l’échelle européenne que cela se décise.
