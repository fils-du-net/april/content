---
site: lepoint.fr
title: "REJET LOI HADOPI - Entre déception, détermination ou soulagement"
author: lepoint.fr
date: 2009-04-09
href: http://www.lepoint.fr/actualites-technologie-internet/2009-04-09/rejet-loi-hadopi-entre-deception-determination-ou-soulagement/1387/0/333867
tags:
- HADOPI
---

> Après le rejet de la loi antipiratage par l'Assemblée nationale, les réactions affluent. Florilège.
> [...] Catherine Deneuve, Victoria Abril, Louis Garrel, réunis avec d'autres artistes de cinéma  au sein d'un collectif  pour lequel vivre une révolution numérique est "une chance" : "Ce rejet est un séisme, et marque une victoire pour les libertés, pour le cinéma et pour la culture. La diversité de la création doit dès aujourd'hui revenir au centre de la réflexion sur les enjeux de la révolution numérique, au même titre que la protection des libertés individuelles et des auteurs. Complètement oubliées dans ce projet de loi, elles sont pourtant les poumons de la création."
