---
site: reuters.com
title: "Surveillance antitrust prolongée de 18 mois pour Microsoft"
author: Diane Bartz, Wilfrid Exbrayat
date: 2009-04-16
href: http://fr.reuters.com/article/technologyNews/idFRPAE53F10N20090416
tags:
- Logiciels privateurs
- Informatique-deloyale
---

> WASHINGTON (Reuters) - Microsoft et le département de la Justice des Etats-Unis ont accepté d'un commun accord de prolonger de 18 mois certains aspects d'une surveillance fédérale antitrust, selon des documents judiciaires.
