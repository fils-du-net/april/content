---
site: "presence-pc.com"
title: "L'EFI de plus en plus présent dans les PC"
author: Pierre Dandumont 
date: 2009-04-17
href: http://www.presence-pc.com/actualite/efi-pc-bios-34573/
tags:
- Le Logiciel Libre
- Interopérabilité
---

> L'EFI, le remplaçant annoncé du BIOS de nos PC (présent depuis bientôt 30 ans), est de plus en plus présent. Depuis que Windows Vista, Mac OS X et Linux sont compatibles, les constructeurs osent enfin utiliser l'EFI sur autre chose que les machines à base d'Itanium.
