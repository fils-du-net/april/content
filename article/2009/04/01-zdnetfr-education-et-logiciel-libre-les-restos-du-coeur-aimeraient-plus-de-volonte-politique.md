---
site: zdnet.fr
title: "Éducation et logiciel libre: les \"Restos du Cœur\" aimeraient plus de volonté politique"
author: Thierry Noisette
date: 2009-04-01
href: http://www.zdnet.fr/blogs/2009/04/01/education-et-logiciel-libre-les-restos-du-c-ur-aimeraient-plus-de-volonte-politique/
tags:
- Le Logiciel Libre
- Éducation
---

> Les logiciels libres comptent de fervents soutiens parmi les enseignants, les associations dans ce domaine sont nombreuses, mais au sommet la volonté politique manque dans le domaine de l'Éducation nationale, ont critiqué plusieurs intervenants aux tables rondes de Solutions Linux.
> [...]
