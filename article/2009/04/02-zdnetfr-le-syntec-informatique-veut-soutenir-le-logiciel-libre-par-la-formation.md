---
site: zdnet.fr
title: "Le Syntec informatique veut soutenir le logiciel libre par la formation"
author: Thierry Noisette
date: 2009-04-02
href: http://www.zdnet.fr/blogs/2009/04/02/le-syntec-informatique-veut-soutenir-le-logiciel-libre-par-la-formation/
tags:
- Le Logiciel Libre
- Entreprise
- Éducation
---

> Le Syntec informatique vient de diffuser un communiqué (pas encore sur son site, mais que l'on trouve sur Toolinux.com), titré "Résultats de l’étude OPIIEC « Impact du logiciel libre en France »: investir dans la formation pour optimiser les apports du logiciel libre".
> [...] Une opportunité pour neuf développeurs sur dix
> Parmi les enseignements de cette étude:
> - 78% des développeurs interrogés utilisent du logiciel libre dans leur entreprise.
> - 87% des développeurs interrogés pensent que le logiciel libre est une opportunité.
