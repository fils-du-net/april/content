---
site: silicon.fr
title: "Open World Forum, le rassemblement du monde des logiciels libres"
author: David Feugey 
date: 2009-04-28
href: http://www.silicon.fr/fr/news/2009/04/28/open_world_forum__le_rassemblement_du_monde_des_logiciels_libres
tags:
- Le Logiciel Libre
- Institutions
---

> L’Open World Forum a pour vocation de réunir les acteurs du logiciel libre. Cette seconde édition prendra place à Paris, les 1er et 2 otobre prochains, en partenariat avec l’Open Source Think Tank.
> L’an dernier, cet évènement avait rassemblé plus de 1200 visiteurs et 160 speakers en provenance de 20 pays différents, au sein de 17 conférences. L’Open World Forum se veut le sommet des communautés du libre, des centres de compétence et de promotion des logiciels libres, de la politique régissant cet environnement, des DSI et de l’innovation.
