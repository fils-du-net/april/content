---
site: journaleuropa.info
title: "Libre n’est pas gratuit"
author: Julien Fournier
date: 2009-04-08
href: http://www.journaleuropa.info/FR_article/n496t3j0d0-europe-open-source-logiciels-libres-linux-eric-s-raymond-osi-.html
tags:
- Le Logiciel Libre
---

> Face aux monopoles des systèmes propriétaires, le Libre développe un modèle économique viable et éthique, basé sur le respect des clients, des collaborations entre les éditeurs et une répartition juste des richesses...
