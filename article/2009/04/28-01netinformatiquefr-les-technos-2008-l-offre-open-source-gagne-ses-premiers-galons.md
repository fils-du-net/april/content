---
site: 01netinformatique.fr
title: "Les technos 2008 : L’offre open source gagne ses premiers galons"
author: Alain Clapaud
date: 2009-04-28
href: http://www.01informatique.fr/infrastructures-stockage-serveurs-116/open-source-premiers-galons-50855/
tags:
- Le Logiciel Libre
- Entreprise
- Économie
- Institutions
- Innovation
---

> Les logiciels libres d’intelligence économique ont commencé à trouver un écho auprès des entreprises et du secteur public. Grâce aux services d’assistance, ils sont devenus crédibles face aux offres propriétaires.
> Alors que le marché des solutions décision­nelles se concentre à la vitesse grand V, l’open source monte en puissance. Des solutions spécialisées et immatures, les logiciels libres d’intelligence économique (ou business intelligence, BI) se sont constitués en véritables suites, assorties d’offres de support professionnelles. Elles se sont placées en concurrence directe avec les propositions traditionnelles. Des arguments techniques et commerciaux qui ont séduit de plus en plus d’entreprises.
