---
site: "presence-pc.com"
title: "Linux sur netbooks : 4 % du marché"
author: Pierre Dandumont
date: 2009-04-06
href: http://www.presence-pc.com/actualite/linux-netbook-34398/
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Le marché des netbooks est souvent vu comme la porte d'entrée de Linux dans le monde du grand public, l'OS libre étant très médiatisé dans ce secteur. Pourtant, depuis le premier Eee PC (disponible à l'époque uniquement sous Linux), les choses ont bien changée. Les derniers chiffres montrent que Microsoft est passé de 10 % de netbooks sous Windows en mars 2008 à 96 % en mars 2009.
