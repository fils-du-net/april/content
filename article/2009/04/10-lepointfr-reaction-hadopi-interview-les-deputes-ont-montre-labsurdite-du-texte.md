---
site: lepoint.fr
title: "RÉACTION HADOPI - INTERVIEW - \"Les députés ont montré l'absurdité du texte\""
author: Guerric Poncet
date: 2009-04-10
href: http://www.lepoint.fr/actualites-technologie-internet/2009-04-10/reaction-hadopi-interview-les-deputes-ont-montre-l-absurdite-du-texte/1387/0/334113
tags:
- Le Logiciel Libre
- April
- HADOPI
---

> [...] Lepoint.fr : Quelle est votre réaction après le rejet de la loi Création et Internet par l'Assemblée nationale ?
> Alix Cazenave : L'April salue les députés qui ont voté contre la loi. Ils promeuvent une vision du numérique beaucoup plus en phase avec les usages potentiels, et préfèrent voir le numérique comme une opportunité plutôt que comme une menace. Le vote montre que la majorité de l'UMP est fragile, alors que les députés ont voulu mettre en lumière l'ineptie du processus et l'absurdité du texte.
