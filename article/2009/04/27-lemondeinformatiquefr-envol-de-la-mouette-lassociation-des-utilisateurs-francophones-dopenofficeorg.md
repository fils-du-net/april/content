---
site: LeMondeInformatique.fr
title: "Envol de la Mouette, l'association des utilisateurs francophones d'OpenOffice.org"
author: Vincent Delfau
date: 2009-04-27
href: http://www.lemondeinformatique.fr/actualites/lire-envol-de-la-mouette-l-association-des-utilisateurs-francophones-d-openofficeorg-28504.html
tags:
- Le Logiciel Libre
- Entreprise
- Économie
- Sensibilisation
- Associations
- Promotion
---

> Promouvoir l'utilisation et le développement d'OpenOffice.org. C'est l'objectif prioritaire de la Mouette, une association fondée au début du mois pour structurer la communauté francophone adepte de la suite bureautique libre.
> « L'idée, explique Jean-François Donikian, le président de la structure nouvellement constituée, est de disposer d'une organisation représentant à la fois les utilisateurs grand public et professionnels. » Si le produit bénéficie d'une bonne notoriété dans les cercles spécialisés, le dirigeant de la Mouette regrette en effet qu'il pâtisse d'une relative méconnaissance dans le grand public.
