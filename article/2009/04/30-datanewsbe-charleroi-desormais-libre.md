---
site: datanews.be
title: "Charleroi désormais libre"
author: Marc Husquinet
date: 2009-04-30
href: http://www.datanews.be/fr/news/90-57-23804/charleroi-desormais-libre.html
tags:
- Le Logiciel Libre
- Administration
---

> Depuis un an, la ville de Charleroi migre son parc de 1.600 PC vers les logiciels libres OpenOffice en bureautique.
> La migration porte sur l'ensemble de la suite, y compris Access remplacé par PostgreSQL. Pour l'heure, environ 300 nouvelles machines sont installées et quelque 400 agents ont été formés. "On a constaté que beaucoup d'agents n'avaient jamais eu de formation en bureautique", note l'échevin de l'informatique, Paul Ficheroulle, qui insiste sur le fait que l'ensemble de la suite a été remplacé.
