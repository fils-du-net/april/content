---
site: zdnet.fr
title: "Hadopi : premières réactions après l'adoption de la loi"
date: 2009-04-03
href: http://www.zdnet.fr/actualites/internet/0,39020774,39390853,00.htm
tags:
- HADOPI
---

> [...] April  (Association pour la promotion et la recherche en informatique libre)
> « La ministre de la Culture Christine Albanel, le rapporteur Franck Riester et presque tous les députés UMP ont validé de nouvelles discriminations à l'encontre du Logiciel Libre. Malgré la mobilisation des employeurs du Libre contre HADOPI 2, ils ont refusé tout encadrement des mouchards filtrants que les abonnés devront installer pour garantir leur sécurité juridique. Ils se sont également opposés à toute mesure favorable à l'interopérabilité et à la libre concurrence, et ont persisté à limiter le droit moral de divulgation des auteurs de logiciels libres 3.
> La dizaine de députés ayant voté ce texte prétend donc bouleverser un écosystème qui concerne 18 millions de foyers connectés à internet et dont ils n'ont pour la plupart pas la moindre connaissance. »
