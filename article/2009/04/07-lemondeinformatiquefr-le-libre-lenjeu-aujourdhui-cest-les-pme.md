---
site: lemondeinformatique.fr
title: "Le Libre : \"L'enjeu aujourd'hui, c'est les PME\""
author: Olivier Rafal 
date: 2009-04-07
href: http://solutionspme.lemondeinformatique.fr/articles/lire-le-libre-remede-a-la-crise-l-enjeu-aujourd-hui-c-est-les-pme-interview-2689.html
tags:
- Le Logiciel Libre
- Entreprise
---

> Je crois qu'il y a plus un problème d'expérience que d'ergonomie. Les éditeurs qui développent des progiciels pour les PME ont l'expertise nécessaire, la connaissance du métier et des interlocuteurs. Il faut juste que les acteurs du Libre se forment, se regroupent pour montrer un visage unique et se faire connaître des CCI et des syndicats professionnels. Ils doivent expliquer aux patrons de PME qu'au lieu de subir les prix et les évolutions de produits imposés par les acteurs traditionnels, ils peuvent choisir une offre Libre, fiable, dont ils auront la maîtrise.
