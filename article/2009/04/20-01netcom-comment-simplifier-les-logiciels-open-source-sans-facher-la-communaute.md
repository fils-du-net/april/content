---
site: 01net.com
title: "Comment simplifier les logiciels open source sans fâcher la communauté?"
author: Tristan Nitot
date: 2009-04-20
href: http://pro.01net.com/editorial/501239/comment-simplifier-les-logiciels-open-source-sans-facher-la-communaute/
tags:
- Le Logiciel Libre
---

> En résumé, tout concourt à rajouter sans cesse de nouvelles possibilités et de nouveaux boutons dans le logiciel libre. Mais en ne cherchant pas à contrer ces tendances, on risque de se retrouver avec des logiciels certes puissants, mais avec des interfaces touffues et donc trop difficiles d'abord pour les utilisateurs ordinaires.
