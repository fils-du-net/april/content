---
site: pcinpact.com
title: "Hadopi : des mouchards payants et non interopérables (MàJ)"
author: Marc Rees
date: 2009-04-02
href: http://www.pcinpact.com/actu/news/49218-hadopi-interoperabilite-logiciel-libre-payant.htm
tags:
- Le Logiciel Libre
- Logiciels privateurs
- HADOPI
- Désinformation
---

> Mise à jour 2 avril 2009 : les logiciels de sécurisation, ces mouchards qui surveilleront les activités des internautes seront payants. Riester comme Albanel ont refusé que les "outils de sécurisation" soient impérativement gratuits, comme le réclamait le député Bloche coauteur d'un amendement 307 allant en ce sens.
