---
site: lemonde.fr
title: "Le taïwanais Acer profite de l'engouement du grand public pour les mini-ordinateurs"
author: Cécile Ducourtieux
date: 2009-04-14
href: http://www.lemonde.fr/technologies/article/2009/04/13/le-taiwanais-acer-profite-de-l-engouement-du-grand-public-pour-les-mini-ordinateurs_1180044_651865.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> [...] "Nous ne croyions alors pas vraiment aux PC avec des écrans de 7 ou 8 pouces et un système d'exploitation Linux (logiciels libres) pas assez confortables d'utilisation pour que les ventes décollent. Nous avons attendu d'avoir des PC avec une taille d'écran d'au moins 10 pouces et un système d'exploitation Microsoft, plus simple d'utilisation pour le grand public, pour nous lancer", explique Olivier Gillet, directeur marketing chez HP France.
