---
site: clubic.com
title: "FreeBSD s'invite au sein du système Debian"
author: Guillaume
date: 2009-04-06
href: http://www.clubic.com/actualite-268726-freebsd-sein-linux-debian.html
tags:
- Le Logiciel Libre
---

> Joerg Jasper, de l'équipe de développement du système Linux Debian, vient de publier un nouveau message à destination des utilisateurs sur la liste de diffusion officielle. Le système dispose désormais de deux architectures optionnelles FreeBSD à savoir : kfreebsd-i386 AKA GNU/kFreeBSD i386 et kfreebsd-amd64 AKA GNU/kFreeBSD amd64.
