---
site: zdnet.fr
title: "Vente liée d’Internet Explorer : Microsoft reçoit le soutien de l'ACT"
author: La rédaction
date: 2009-04-22
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39393157,00.htm?xtor=EPR-101
tags:
- Internet
- Logiciels privateurs
- Vente liée
- Informatique-deloyale
---

> Juridique - Afin d’éviter le retrait d’Internet Explorer de Windows, Microsoft a obtenu le soutien de l’ACT. Ce lobby, connu pour son hostilité envers l’Open Source, s’est déjà engagé en faveur de l’éditeur dans ses procès antitrust.
> [...] De son côté, l'ACT compte parmi ses membres des entreprises comme eBay, Oracle ou VeriSign et n'a jamais fait mystère de son hostilité à l'égard de l'Open Source. En 2006, son président, Jonathan Zuck, avait par ailleurs critiqué la Free Software Foundation (FSF) et sa décision de s'opposer aux DRM dans la licence GPLv3.
