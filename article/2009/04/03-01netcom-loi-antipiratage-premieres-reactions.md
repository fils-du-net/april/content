---
site: 01net.com
title: "Loi antipiratage : premières réactions"
author: Guillaume Deleurence
date: 2009-04-03
href: http://www.01net.com/editorial/500592/loi-antipiratage-premieres-reactions/
tags:
- April
- HADOPI
---

> [...] L'April s'en prend aussi à Christine Albanel et à « presque tous les députés UMP » qui ont « refusé tout encadrement des mouchards filtrants que les abonnés devront installer pour garantir leur sécurité juridique » et qui « se sont également opposés à toute mesure favorable à l'interopérabilité ainsi qu'à la libre concurrence et ont persisté à limiter le droit moral de divulgation des auteurs de logiciels libres ».
