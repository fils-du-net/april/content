---
site: pcinpact.com
title: "Remboursement de Windows XP/Vista chez Auchan : les détails"
author: Nil Sanyas
date: 2009-04-14
href: http://www.pcinpact.com/actu/news/50330-remboursement-windows-xp-vista-auchan.htm
tags:
- Logiciels privateurs
- Vente liée
---

> Plus de 100 euros de différence parfois !
> Comme nous vous l'annoncions la semaine dernière, la vente liée, tout du moins concernant les systèmes d'exploitation, appartiendra très bientôt au passé dans toute la France. Auchan propose ainsi le remboursement du système pour cinq marques uniquement (pour le moment), à savoir : Acer, ASUS, Toshiba, Fujitsu, et Packard Bell (qui appartient à Acer).
