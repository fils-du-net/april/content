---
site: programmez.com
title: "L'observatoire du libre vient de sortir !"
author: Frédéric Mazué
date: 2009-03-26
href: http://www.programmez.com/actualites.php?titre_actu=L-observatoire-du-libre-vient-de-sortir-&id_actu=4701
tags:
- Le Logiciel Libre
- Entreprise
---

> La dernière édition de l'observatoire du libre vient d'être publiée. Cet observatoire est un indicateur basé sur les formations open source de Anaska-Alter Way et ib - groupe Cegos.
> [...] L'étude est proposée sous la Licence "Creative Commons" (http://creativecommons.org/licenses/by-nc-sa/2.0/fr)
> Le document est à consulter ici : http://www.ob2l.com
