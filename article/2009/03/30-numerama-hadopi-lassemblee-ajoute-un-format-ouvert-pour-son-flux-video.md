---
site: Numerama.com
title: "Hadopi : L'Assemblée ajoute un format ouvert pour son flux vidéo"
author: Guillaume Champeau
date: 2009-03-30
href: http://www.numerama.com/magazine/12466-Hadopi-L-Assemblee-ajoute-un-format-ouvert-pour-son-flux-video.html
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
- Interopérabilité
- HADOPI
- Institutions
- Video
---

> Pour la reprise des débats [sur le projet de loi Création &amp; Internet] ce lundi à 16H [...], le site de l'Assemblée Nationale propose désormais un flux au format Mpeg4 H264, que les utilisateurs de Linux ou Mac OS pourront lire par exemple avec le logiciel libre VLC. On regrettera juste que cette adoption des formats libres ne concerne pas encore la VOD, toujours proposée au seul format Windows Media. Mais c'est une avancée qu'il faut saluer.
