---
site: ZDNet
title: "TomTom et Microsoft entérinent un accord amiable"
author: ZDNet France
date: 2009-03-31
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39389096,00.htm
tags:
- Entreprise
- Économie
- Partage du savoir
- Brevets logiciels
- Innovation
- Licenses
---

> Les deux compagnies qui avaient entamé des procédures l’une contre l’autre pour violation de brevet ont annoncé avoir conclu un accord valable pendant 5 ans pour lequel TomTom va débourser une somme non divulguée.
