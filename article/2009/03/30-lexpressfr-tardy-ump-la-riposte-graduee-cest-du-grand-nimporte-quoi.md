---
site: lexpress.fr
title: "Tardy (UMP): \"La riposte graduée, c'est du grand n'importe quoi!\""
author: Julie Saulnier
date: 2009-03-30
href: http://www.lexpress.fr/actualite/high-tech/tardy-ump-la-riposte-graduee-c-est-du-grand-n-importe-quoi_750318.html
tags:
- Le Logiciel Libre
- HADOPI
---

> Lionel Tardy, député UMP de Haute-Savoie, est opposé à la riposte graduée prévue par le projet de loi Création et Internet, débattu ce lundi à l'Assemblée nationale. Il explique à LEXPRESS.fr pourquoi il préfère le principe de l'amende à celui de la coupure d'abonnement.
