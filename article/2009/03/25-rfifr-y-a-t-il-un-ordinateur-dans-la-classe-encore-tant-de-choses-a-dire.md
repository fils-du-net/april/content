---
site: rfi.fr
title: "Y-a-t-il un ordinateur dans la classe ? Encore tant de choses à dire..."
author: Anne-Laure Marie
date: 2009-03-25
href: http://www.rfi.fr/actufr/articles/111/article_79516.asp
tags:
- Le Logiciel Libre
- Sensibilisation
---

> La place de l’ordinateur à l’école en Afrique ? Le sujet est à l’évidence trop vaste pour être traité en quelques articles, même inspirés d'une enquête participative riche de plus de 230 témoignages. Il nous faut pourtant clore cette discussion. De « l’ordinateur à 100 dollars » à la question du prix des logiciels en passant par le problème de la maintenance informatique, retour pour ce dernier article sur tous ces points que vous avez soulignés.
