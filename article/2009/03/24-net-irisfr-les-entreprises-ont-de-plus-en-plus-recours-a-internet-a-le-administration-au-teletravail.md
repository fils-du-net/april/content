---
site: "Net-iris.fr"
title: "Les entreprises ont de plus en plus recours à internet, à l'e-administration, au télétravail et aux logiciels libr"
author: Net-iris
date: 2009-03-24
href: http://www.net-iris.fr/veille-juridique/actualite/21886/les-entreprises-ont-de-plus-en-plus-recours-a-internet-a-e-administration-au-teletravail-et-aux-logiciels-libres.php
tags:
- Le Logiciel Libre
- Entreprise
- Économie
---

> Selon une étude Insee sur les usages des nouvelles technologies et de l'internet en entreprise, en janvier 2008, presque toutes les grandes entreprises avaient accès à l'internet haut débit en 2008.
