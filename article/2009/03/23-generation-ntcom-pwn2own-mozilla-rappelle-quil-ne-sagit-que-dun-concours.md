---
site: "generation-nt.com"
title: "Pwn2Own : Mozilla rappelle qu'il ne s'agit que d'un concours"
author: Jérôme G.
date: 2009-03-23
href: http://www.generation-nt.com/pwn2own-mozilla-doztler-vulnerabilites-concours-hacking-actualite-251761.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Désinformation
---

> Pour Mozilla, le concours de hacking Pwn2Own doit être pris en tant que tel et ne pas servir de test comparatif pour la sécurité des navigateurs Web.
