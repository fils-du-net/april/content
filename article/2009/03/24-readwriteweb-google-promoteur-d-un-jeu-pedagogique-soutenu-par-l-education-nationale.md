---
site: Readwriteweb.com
title: "Google promoteur d’un jeu pédagogique soutenu par l’Education Nationale"
author: Damien Douani
date: 2009-03-24
href: http://fr.readwriteweb.com/2009/03/24/analyse/google-promoteur-dun-jeu-pedagogique-soutenu-par-leducation-nationale/
tags:
- Le Logiciel Libre
- Internet
- Institutions
- Éducation
---

> En pleine fièvre hadopique l’Education Nationale lance le 26 mars un grand jeu en ligne pour les collégiens français, ChercheNet. L’objectif est de leur inculquer un usage “citoyen et responsable” d’Internet. Surprise : c’est Google France qui est maitre d’ouvrage, avec la société Calysto. Démarche désintéressée ou pédagogie sponsorisée ? [...] Un kit pédagogique est fourni aux professeurs, et les élèves doivent relater leur parcours dans un blog.
