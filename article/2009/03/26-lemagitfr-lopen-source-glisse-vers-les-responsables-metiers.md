---
site: lemagit.fr
title: "L'Open Source glisse vers les responsables métiers "
author: Cyrille Chausson
date: 2009-03-26
href: http://www.lemagit.fr/article/linux-salon-open-source/2863/1/l-open-source-glisse-vers-les-responsables-metiers/
tags:
- Le Logiciel Libre
- Entreprise
- Informatique en nuage
---

> [...] Poussée par la pression sans cesse grandissante sur les prix, par la réduction des dépenses IT – crise oblige –, et par la montée en puissance de Cloud Computing – dont l'Open Source fournit pour l'essentiel l'infrastructure -, la sphère du logiciel libre et son modèle de développement communautaire se fraie un chemin dans les entreprises par la porte des départements métiers.
