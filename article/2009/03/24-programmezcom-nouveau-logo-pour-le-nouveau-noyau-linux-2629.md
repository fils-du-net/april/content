---
site: programmez.com
title: "Nouveau Logo pour le nouveau noyau Linux 2.6.29"
author: Frédéric Mazué
date: 2009-03-24
href: http://www.programmez.com/actualites.php?titre_actu=Nouveau-Logo-pour-le-nouveau-noyau-Linux-2629&id_actu=4678
tags:
- Le Logiciel Libre
- Sensibilisation
---

> Linus Torvalds vient d'annoncer la sortie du noyau 2.6.29.
> Le plus remarquable est un changement de logo:
> Le bon vieux pingouin Tux s'est transformé en Tuz. Une façon de soutenir le diable de Tasmanie qui est une espèce en voie d'extinction.
