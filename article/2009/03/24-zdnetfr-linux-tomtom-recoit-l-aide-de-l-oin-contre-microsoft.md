---
site: zdnet.fr
title: "Linux : TomTom reçoit l’aide de l’OIN contre Microsoft"
author: La rédaction
date: 2009-03-24
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39388901,00.htm
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Brevets logiciels
---

> Juridique - TomTom a rejoint l'Open Invention Network (OIN), une association destinée à aider les utilisateurs de Linux à se défendre contre les poursuites pour violation de brevets.
> Le fabricant de GPS va désormais pouvoir bénéficier de l'aide d'un allié de poids face à la menace juridique de Microsoft. Pour rappel, la firme de Redmond avait assigné en justice TomTom fin février, l'accusant de violer huit de ses brevets.
