---
site: 01net.com
title: "Le logiciel libre célèbre l'arrivée du printemps"
author: Philippe Crouzillacq
date: 2009-03-20
href: http://www.01net.com/editorial/500193/le-logiciel-libre-celebre-larrivee-du-printemps/
tags:
- Le Logiciel Libre
- April
- Sensibilisation
---

> Pour la neuvième année, les fondus de logiciels libres se rassemblent partout en France dans les médiathèques, les associations et les espaces publics numériques pour faire découvrir le libre au grand public.
