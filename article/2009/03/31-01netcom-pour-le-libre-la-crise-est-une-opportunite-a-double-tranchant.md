---
site: 01net.com
title: "Pour le libre, la crise est une opportunité à double tranchant"
author: Gilbert Kallenborn
date: 2009-03-31
href: http://pro.01net.com/editorial/500531/pour-le-libre-la-crise-est-une-opportunite-a-double-tranchant/
tags:
- Le Logiciel Libre
- Entreprise
---

> Les acteurs open source sont de plus en plus sollicités par les responsables informatiques. Mais certains redoutent une braderie des services et des solutions.
