---
site: pcinpact.com
title: "Retour du flux MPEG4 / H.264 pour le débat Création et Internet"
author: Marc Rees
date: 2009-03-30
href: http://www.pcinpact.com/actu/news/50008-flux-flash-mpeg4-h264-vlc.htm
tags:
- Le Logiciel Libre
- Institutions
- Accessibilité
---

> On se souvient que l’univers du Libre avait regretté que la retransmission de la séance publique de l'Assemblée nationale soit assurée au seul format Flash, depuis début mars. « Pour ce qui est du lecteur du flux de la séance en direct de l'AN, cela constitue un véritable retour en arrière par rapport au flux en MPEG4-H.264 qui avait été ajouté en 2007. Il permettait d'accéder facilement, par Internet, en direct et avec un lecteur libre, aux débats à l'Assemblée nationale » nous avait expliqué Alix Cazenave.
