---
site: techcrunch.com
title: "Sommes-nous tous des Pirates?"
author: Alain Eskenazi
date: 2009-03-23
href: http://fr.techcrunch.com/2009/03/23/fr-sommes-nous-tous-des-pirates/
tags:
- Le Logiciel Libre
- HADOPI
---

> [...] L’économie du logiciel libre a prouvé qu’en pratique cela était compatible avec la création de valeurs (intellectuelle et économique). Cela a démontré une réelle efficacité dans la mise en oeuvre de produits compétitifs, efficaces et de qualité. L’Open Source base son modèle économique sur le travail et non sur le capital. C’est le travail des créateurs ou des utilisateurs de logiciels qui est rémunéré dans l’industrie du libre. Le capital, à savoir “le droit d’usage” est “libre”, “gratuit” et sa “distribution” encouragée.
