---
site: 01net.com
title: "A Solutions Linux, deux mondes cohabitent"
author: Tristan Nitot
date: 2009-03-23
href: http://pro.01net.com/editorial/500258/a-solutions-linux-deux-mondes-cohabitent/
tags:
- Le Logiciel Libre
---

> La semaine prochaine se tiendra la 10e édition du salon Solution Linux et open source. Il y a des dizaines d'événements open source en Europe et en France, mais Solutions Linux occupe une place particulière parmi eux. C'est le seul où vous verrez professionnels et associations se côtoyer si intimement.
