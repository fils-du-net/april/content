---
site: zdnet.fr
title: "Des chercheurs créent un rootkit qui résiste au formatage"
author: Vincent Birebent
date: 2009-03-25
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39388963,00.htm
tags:
- Le Logiciel Libre
- Informatique-deloyale
- Sciences
---

> Sécurité - La société Core Security Technology a mis au point un rootkit qui, une fois stocké dans le BIOS d'un ordinateur, est capable de résister à l'effacement du disque dur.
> Deux chercheurs de la société Core Security Technology ont présenté la semaine dernière à la conférence CanSecWest de Vancouver, un nouveau type de rootkit qui s'attaque directement aux BIOS de deux machines témoins, l'une sous Windows et l'autre sous OpenBSD.
