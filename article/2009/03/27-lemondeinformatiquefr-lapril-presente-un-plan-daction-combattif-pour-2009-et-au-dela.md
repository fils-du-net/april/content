---
site: lemondeinformatique.fr
title: "L'April présente un plan d'action combattif pour 2009 et au-delà"
author: Olivier Rafal
date: 2009-03-27
href: http://www.lemondeinformatique.fr/actualites/lire-l-april-presente-un-plan-d-action-combattif-pour-2009-et-au-dela-28340.html
tags:
- Le Logiciel Libre
- April
---

> Fière du travail accompli, l'April entend continuer sa lutte contre les « lobbies qui ne comprennent pas que le monde change, ou qui ne peuvent ou ne veulent pas embrasser ce changement ». L'association de défense et de promotion du logiciel libre vient de publier son rapport moral, suite à l'assemblée générale de février dernier.
