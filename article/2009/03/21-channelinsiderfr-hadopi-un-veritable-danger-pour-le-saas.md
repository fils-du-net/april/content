---
site: channelinsider.fr
title: "HADOPI: Un véritable danger pour le SaaS"
author: N. Guillaume
date: 2009-03-21
href: http://www.channelinsider.fr/fr/actualite/2009/03/21/hadopi__un_veritable_danger_pour_le_saas
tags:
- Le Logiciel Libre
- HADOPI
- Informatique en nuage
---

> La loi Création et Internet pourrait s’avérer être plus dangereuse qu’on n’aurait pu l’imaginer jusqu’à aujourd’hui. Notamment pour les modèles techniques basés sur le SaaS (Software as a service) et le Cloud Computing.
> C’est en tout cas ce que pense Jean-Paul Smets-Solanes, le CEO de Nexedi, une entreprise basée dans le Nord de la France qui conçoit des solutions d’entreprise (ERP, CRM et E-commerce) autour d’architectures et de logiciels libres.
