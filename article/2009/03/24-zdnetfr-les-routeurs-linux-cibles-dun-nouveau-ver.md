---
site: zdnet.fr
title: "Les routeurs Linux cibles d'un nouveau ver"
author: Vincent Birebent
date: 2009-03-24
href: http://www.zdnet.fr/actualites/internet/0,39020774,39388927,00.htm
tags:
- Le Logiciel Libre
---

> Sécurité - La société DroneBL, spécialisée dans la surveillance des réseaux, a identifié l’apparition d’un ver ciblant les routeurs basés sur une architecture Mipsel et utilisant Linux. Une fois infectés, ils intègrent un réseau de botnets aptes à lancer des attaques par déni de service.
