---
site: zdnet.fr
title: "Mozilla et Google veulent généraliser la 3D sur le Web"
author: Vincent Birebent
date: 2009-03-25
href: http://www.zdnet.fr/actualites/internet/0,39020774,39388949,00.htm
tags:
- Interopérabilité
- Brevets logiciels
---

> Technologie - L'éditeur de Firefox et le moteur de recherches vont participer aux travaux du Khronos Group en vue de développer des API pour généraliser des effets en 3D sur les sites Web. Elles s'appuieraient sur un standard ouvert et libre de droit.
