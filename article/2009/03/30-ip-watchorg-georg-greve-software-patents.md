---
site: "ip-watch.org"
title: "Georg Greve on Software Patents"
author: Georg Greve
date: 2009-03-30
href: http://www.ip-watch.org/weblog/2009/03/30/georg-greve-on-software-patents/
tags:
- Partage du savoir
- Institutions
- Brevets logiciels
- Innovation
---

> The World Intellectual Property Organization (WIPO) Standing Committee on the Law of Patents discussed exceptions and limitations to patentability on 24 March. Intellectual Property Watch spoke with Georg Greve of the Free Software Foundation Europe about exceptions on patents and software.
