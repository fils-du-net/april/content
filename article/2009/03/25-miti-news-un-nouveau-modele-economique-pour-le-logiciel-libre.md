---
site: Miti News
title: "Un nouveau modèle économique pour le logiciel libre"
author: La rédaction
date: 2009-03-25
href: http://www.mitinews.info/Un-nouveau-modele-economique-pour-le-logiciel-libre_a2124.html
tags:
- Le Logiciel Libre
- Internet
- Économie
- Partage du savoir
- Innovation
- Informatique en nuage
---

> Totalement inédit, ce nouveau service Web 2.0, disponible dès le 31 mars en version bêta, créé par ARGIA met en relation les consommateurs et les producteurs de logiciels libres.
