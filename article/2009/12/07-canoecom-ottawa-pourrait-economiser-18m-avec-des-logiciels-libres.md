---
site: canoe.com
title: "Ottawa pourrait économiser 18M$ avec des logiciels libres"
author: Agence QMI  
date: 2009-12-07
href: http://www.canoe.com/techno/nouvelles/archives/2009/12/20091207-100350.html
tags:
- Le Logiciel Libre
- Administration
---

> Le gouvernement canadien pourrait économiser jusqu'à 18M$ par année en utilisant le logiciel Open Office au lieu de faire affaire avec ses fournisseurs habituels qui exigent des droits d'autorisation pour l'utilisation de leurs logiciels.
> C'est ce qu'indique un document de Travaux publics et Services gouvernementaux Canada obtenu par le chercheur Ken Rubin en vertu de la Loi d'accès à l'information.
