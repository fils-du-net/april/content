---
site: pcinpact.com
title: "Attali : \"plus personne n'acceptera de payer, malgré HADOPI\""
author: Marc Rees
date: 2009-12-29
href: http://www.pcinpact.com/actu/news/54741-jacques-attali-bruits-hadopi-gratuit.htm
tags:
- Économie
- HADOPI
---

> [...] « La gratuité ne signifie pas que ceux qui produisent ne sont pas payés. (…) La gratuité signifie que le consommateur ne paye pas. Et on va vers cette tendance, de plus en plus, une socialisation des coûts où les gens sont payés par quelqu’un d’autres que celui qui consomme, ce qui créé une dynamique très forte » affirme Attali. Le choix politique de toute société sera alors de déterminer l’endroit où il faut placer ce curseur entre gratuit et payant. Aux extrêmes, deux certitudes : « ce qui est abondant va devenir gratuit, ce qui est rare va devenir payant. »
