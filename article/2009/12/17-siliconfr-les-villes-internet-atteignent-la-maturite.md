---
site: silicon.fr
title: "Les «Villes Internet» atteignent la maturité"
author: Anne Daubrée
date: 2009-12-17
href: http://www.silicon.fr/fr/news/2009/12/17/les__villes_internet__atteignent_la_maturite
tags:
- Le Logiciel Libre
- Administration
---

> [...] Depuis onze ans, l’association soutient le développement d’un Internet citoyen, accessible à tous. Elle fédère un réseau de 1218 collectivités, qui partagent expériences et savoir faire. Les projets exposés fournissaient quelques exemples. Nombre d’entre eux, pour l’essentiel développés en logiciels libres, faisaient la part belle aux outils collaboratifs.
