---
site: pcinpact.com
title: "Vente liée : la ventilation des prix s'envole"
author: Marc Rees
date: 2009-12-21
href: http://www.pcinpact.com/actu/news/54650-darty-ufc-vente-liee-licence.htm
tags:
- Vente liée
---

> En matière de vente liée, la décision Darty n’aura été que de courte durée. Nous revenons plus en détail sur une décision de la Cour d’Appel de Paris qui fut déjà commentée par l’AFP et nos confrères de 01net notamment : dans cette décision Darty s’est vu dispensée de dissocier les prix du matériel et des logiciels, mais aussi de transmettre la licence Windows avant achat.
