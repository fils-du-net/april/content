---
site: framablog.org
title: "Économie Sociale et Logiciels Libres : Le temps de l'alliance ?"
author: Bastien Sibille
date: 2009-12-01
href: http://www.framablog.org/index.php/post/2009/12/01/economie-sociale-logiciel-libre
tags:
- Le Logiciel Libre
- Économie
---

> [...] Deux mondes co-existent qui dressent des remparts contre la tentation hégémonique du capitalisme : l’un est ancien et puise ses racines dans le XIXe siècle industriel – le monde de l’économie sociale (coopératives, mutuelles, associations…) ; l’autre est plus jeune et tisse ses réseaux dans le XXIe siècle informatique – le monde du logiciel libre. Si les communautés du libre et les entreprises d’économie sociale se connaissent et se côtoient depuis plus d’une décennie, elles ne voient pas souvent combien leurs luttes sont proches.
