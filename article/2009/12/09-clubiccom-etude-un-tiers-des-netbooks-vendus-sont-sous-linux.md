---
site: Clubic.com
title: "Etude : un tiers des netbooks vendus sont sous Linux"
author: Guillaume Belfiore
date: 2009-12-09
href: http://www.clubic.com/actualite-314964-etude-tier-netbooks-vendus-linux.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Selon une étude du cabinet ABI Research, rapportée par OSNews, la part de marché de Linux sur les netbooks atteindrait prêt de 30%. Sur les 35 millions de machines commercialisées à travers le monde cette année, 11 millions auraient été livrées avec Linux. Ces chiffres ne prennent pas en compte les modifications effectuées à postériori par l'utilisateur telles que le remplacement de Windows par Linux et vice-versa.
