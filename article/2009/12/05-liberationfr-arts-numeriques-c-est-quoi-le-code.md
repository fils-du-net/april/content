---
site: liberation.fr
title: "Arts numériques. C’est quoi le code ?"
author: Marie Lechner 
date: 2009-12-05
href: http://www.liberation.fr/culture/0101606732-arts-numeriques-c-est-quoi-le-code
tags:
- Le Logiciel Libre
---

> Make Art du 8 au 13 décembre à Poitiers. http://makeart.goto10.org
> Depuis quatre ans, Poitiers héberge un festival international pointu au croisement de l’art et de la programmation, pour tous les amoureux des lignes de code. Make Art s’applique à populariser les pratiques dans les arts numériques et la philosophie du logiciel libre.
