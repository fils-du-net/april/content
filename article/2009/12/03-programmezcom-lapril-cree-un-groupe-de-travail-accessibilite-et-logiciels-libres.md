---
site: programmez.com
title: "L'April crée un groupe de travail \"Accessibilité et logiciels libres\""
author: Frédéric Mazué
date: 2009-12-03
href: http://www.programmez.com/actualites.php?titre_actu=L-April-cree-un-groupe-de-travail-Accessibilite-et-logiciels-libres&id_actu=6347
tags:
- Le Logiciel Libre
- Accessibilité
---

> [...] Partant du constat d'une méconnaissance mutuelle entre le monde du Libre et celui du handicap, ce groupe de travail a été imaginé dans une perspective d'échanges pluridisciplinaires pour réconcilier ces deux univers et accroître la diffusion de logiciels libres et accessibles à tous.
