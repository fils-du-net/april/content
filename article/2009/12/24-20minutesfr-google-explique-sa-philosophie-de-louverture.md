---
site: 20minutes.fr
title: "Google explique sa philosophie de l'ouverture"
author: Philippe Berry
date: 2009-12-24
href: http://www.20minutes.fr/article/372604/High-Tech-Google-explique-sa-philosophie-de-l-ouverture.php
tags:
- Le Logiciel Libre
- Entreprise
- Internet
---

> [...] Entre ses projets Android, Chrome et Chrome OS, Google est devenu l'un des acteurs majeurs de l'open source. Le code est ouvert, accessible à chacun et modifiable par tous «parce qu'à la fin, nous croyons que les systèmes ouverts gagnent», explique Google. Rosenberg oppose «la vieille mentalité» des Microsoft et Apple à celle de Google. Il reconnaît que les systèmes fermés peuvent être «rentables» (et donne l'exemple de l'iPod/iTunes), mais selon lui, le principal gain n'est que pour l'entreprise, et pas pour le consommateur.
