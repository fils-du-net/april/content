---
site: neteco.com
title: "Nouveaux rebondissements du côté de la vente liée"
author: Alexandre Laurent
date: 2009-12-04
href: http://www.neteco.com/314390-rebondissements-vente-liee.html
tags:
- Logiciels privateurs
- Vente liée
---

> [...]  « Il est tout simplement incroyable que le juge fasse primer l'intérêt de multinationales sur les droits des consommateurs. Le code de la consommation est là pour protéger les consommateurs des pratiques abusives des professionnels. Voilà plus de dix ans qu'il est violé par la vente liée, il serait temps de le faire appliquer », s'insurge Alix Cazenave, chargée des affaires publiques à l'April.
