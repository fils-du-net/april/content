---
site: tekiano.com
title: "Tunisie : Logiciels libres contre casse-bonbons"
author: Oualid Chine
date: 2009-12-22
href: http://www.tekiano.com/informatique/logiciels/3-10-1520/tunisie-logiciels-libres-contre-casse-bonbons.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Administration
- International
---

> [...] Mais malgré les campagnes de sensibilisation, l’open source est resté cantonné, en Tunisie, dans la sphère des spécialistes. Quelques institutions comme la STEG, la CNRPS, utilisent en effet des logiciels très pointus, en l’occurrence des systèmes d’informations basés sur l’Open Source. Mais les fonctionnaires lambda dans nos ministères, les secrétaires, les professeurs dans leurs salles de classes restent, au niveau informatique, dépendant du bon vouloir d’une multinationale.
