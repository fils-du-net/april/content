---
site: pcworld.com
title: "Google Releases EtherPad as Open Source to Calm Users"
author: Juan Carlos Perez
date: 2009-12-18
href: http://www.pcworld.com/businesscenter/article/185088/google_releases_etherpad_as_open_source_to_calm_users.html
tags:
- Le Logiciel Libre
- Entreprise
- English
---

> Google publie le traitement de texte collaboratif EtherPad sous licence libre pour calmer les inquiétudes des utilisateurs qui craignaient sa disparition.
> Google has released the source code of EtherPad, a Web-hosted word processor designed for real-time workgroup collaboration, in a move aimed at appeasing users of the product who complained about plans to discontinue it.
