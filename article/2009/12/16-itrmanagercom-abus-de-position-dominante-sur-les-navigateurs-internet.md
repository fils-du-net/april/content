---
site: itrmanager.com
title: "Abus de position dominante sur les navigateurs Internet "
author: La rédaction
date: 2009-12-16
href: http://www.itrmanager.com/articles/98670/abus-position-dominante-navigateurs-internet-br-commission-europeenne-accepte-engagements-microsoft-br.html
tags:
- Entreprise
- Institutions
- Europe
---

> La Commission européenne souffle le chaud et le froid. En début de semaine, elle annonce que les démarches entreprises par Oracle concernant MySQL laissent entrevoir une issue favorable sur l'autorisation du rachat de Sun. Ce mercredi, elle impose à Microsoft de laisser le choix aux utilisateurs de leur navigateur Internet. Une décision qui pourrait renforcer la montée des navigateurs alternatifs face à Internet Explorer.
