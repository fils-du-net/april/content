---
site: lepetiteconomiste.com
title: "5000 micro-ordinateurs reconditionnés en Deux-Sèvres pour des familles de collégiens modestes"
author: La rédaction
date: 2009-12-05
href: http://lepetiteconomiste.com/5000-micro-ordinateurs,1931
tags:
- Le Logiciel Libre
- Administration
---

> [...] Le processus est relativement simple : MAAF Assurances fait don des unités centrales issues du renouvellement d’une partie de son parc informatique. Le Conseil général prend en charge les écrans, le reconditionnement des unités centrales (effectué par l’entreprise d’insertion les Ateliers du Bocage) qui sont équipées de logiciels libres et participe aux frais de connexion à Internet des familles de collégiens bénéficiaires d’une bourse départementale. De plus, un accompagnement personnalisé aux bénéficiaires du dispositif est proposé.
