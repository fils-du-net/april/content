---
site: Framablog
title: "Un Centre de Formation Logiciels Libres pour les Universités d'Île-de-France !"
author: aKa
date: 2009-12-08
href: http://www.framablog.org/index.php/post/2009/12/08/centre-de-formation-logiciels-libres
tags:
- Le Logiciel Libre
- Éducation
---

> Il reste beaucoup à faire pour améliorer la situation du logiciel libre dans l’éducation en général et dans l’enseignement supérieur en particulier.
