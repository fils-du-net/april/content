---
site: 01net.com
title: "Quatorze entreprises poursuivies pour violation de la licence GNU-GPLa"
author: Guillaume Deleurence
date: 2009-12-15
href: http://www.01net.com/editorial/510007/quatorze-entreprises-poursuivies-pour-violation-de-la-licence-gnu-gpl/
tags:
- Le Logiciel Libre
- Entreprise
- Licenses
- Philosophie GNU
---

> Des sociétés comme Samsung ou JVC sont visées par une plainte du Software Freedom Law Center pour n'avoir pas respecté le contenu de la licence GPL version 2 du logiciel BusyBox.
