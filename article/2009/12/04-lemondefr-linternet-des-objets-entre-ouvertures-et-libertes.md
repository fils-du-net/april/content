---
site: lemonde.fr
title: "\"L'Internet des objets\", entre ouvertures et libertés ?"
author: Hubert Guillaud
date: 2009-12-04
href: http://www.lemonde.fr/technologies/article/2009/12/04/l-internet-des-objets-entre-ouvertures-et-libertes_1276455_651865.html
tags:
- Internet
- Interopérabilité
- Vente liée
- Informatique en nuage
---

> [...] Par contre, l’internet des nouveaux objets, lui nous impose déjà l’inacceptable : “Qui achèterait un ordinateur qui imposerait un fournisseur d’accès et limiterait les applications auxquelles vous pouvez accédez ?”, interroge Christian Fauré en désignant notamment l’iPhone. “En passant de la machine à l’objet, le particulier privilégie (dans un premier temps en tout cas, peut-être que cela va être appelé à changer) les usages au détriment des pratiques”
