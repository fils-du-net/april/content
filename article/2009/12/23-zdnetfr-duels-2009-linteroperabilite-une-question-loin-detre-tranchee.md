---
site: zdnet.fr
title: "Duels 2009 - L'interopérabilité, une question loin d'être tranchée"
author: Christophe Auffray
date: 2009-12-23
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39711788,00.htm
tags:
- Interopérabilité
---

> [...] Pour Roberto Di Cosmo, l'interopérabilité ne peut se faire sans formats ouverts. « Les formats et protocoles sont complexes. S'ils ne sont pas publiés de façon à ce que tous puissent les implémenter, je ne vois pas comment les logiciels pourraient interopérer. La seule solution pour s'en sortir, c'est lorsque les règles pour échanger l'information sont claires et stabilisées. »
