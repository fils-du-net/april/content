---
site: "cio-online.com"
title: "Sondage flash : bientôt les PGI libres dans votre entreprise"
author: Bertrand Lemaire
date: 2009-12-14
href: http://www.cio-online.com/actualites/lire-sondage-flash-bientot-les-pgi-libres-dans-votre-entreprise-2605.html
tags:
- Le Logiciel Libre
- Entreprise
---

> [...] Est-ce que les incidents réguliers sur la considération apportée à leurs clients par les éditeurs de PGI (licences fluctuantes, maintenance explosive remplaçant la garantie légale gratuite...) y sont pour quelque chose ? Difficile de le dire mais nos lecteurs semblent bien décidés à se diriger vers les PGI libres comme Compiere, OpenBravo ou ERP5 pour les plus célèbres.
