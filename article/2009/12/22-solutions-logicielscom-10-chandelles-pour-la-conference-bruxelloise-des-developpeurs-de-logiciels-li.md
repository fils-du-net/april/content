---
site: "solutions-logiciels.com"
title: "10 chandelles pour la conférence bruxelloise des développeurs de logiciels libre"
author: Frédéric Mazué
date: 2009-12-22
href: http://www.solutions-logiciels.com/actualites.php?titre=10-chandelles-pour-la-conference-bruxelloise-des-developpeurs-de-logiciels-libre&actu=6507
tags:
- Le Logiciel Libre
- Europe
---

> FOSDEM est une conférence annuelle libre et non-commerciale pour développeurs de logiciels libres et « open source », organisée par et pour la communauté.  FOSDEM offre aux chefs de projets et contributeurs de logiciels libres et « open source » un lieu de rencontre et une plate-forme pour des présentations.  Il informe les visiteurs des derniers développements dans le monde du logiciel libre et open source, et promeut les avantages des solutions logiciel libre
