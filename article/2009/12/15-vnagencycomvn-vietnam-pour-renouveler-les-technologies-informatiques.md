---
site: vnagency.com.vn
title: "Vietnam : Pour renouveler les technologies informatiques"
author: Bao Trân
date: 2009-12-15
href: http://lecourrier.vnagency.com.vn/default.asp?page=newsdetail&newsid=58488
tags:
- Le Logiciel Libre
- Administration
- International
---

> Le Vietnam figure parmi le top 10 mondial des pays ayant le rythme de croissance le plus rapide dans le domaine des technologies informatiques. Hô Chi Minh-Ville en est la locomotive.
> Selon 2 programmes de développement des industries des logiciels et du numérique d'ici 2012, 88,6 milliards de dôngs d'investissement seront injectés dans les 3 ans à venir pour développer les logiciels libres.
