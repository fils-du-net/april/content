---
site: LeMagIT
title: "L'état du monde IT : l'antitrust européen fait plier Microsoft et Intel... et oblige Oracle à composer"
author: Reynald Fléchaux
date: 2009-12-23
href: http://www.lemagit.fr/article/microsoft-sun-amd-windows-intel-mysql-processeurs-oracle-antitrust-ie-x86-ellison-widenius-amende-bruxelles-commission-kroes/5136/1/l-etat-monde-antitrust-europeen-fait-plier-microsoft-intel-oblige-oracle-composer/
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Économie
- Institutions
- Vente liée
- Europe
---

> Dernière année très IT pour Neelie Kroes, la Commissaire européenne à la concurrence, qui s'est frottée en 2009 à trois ténors du secteur. Intel, qui a hérité d'une amende record, Microsoft, qui a plié aux pressions de Bruxelles après 10 ans de guérilla, et Oracle, qui a fini par donner des gages à une Commission qui bloque sa fusion avec Sun.
