---
site: CommentCaMarche
title: "Vente liée : avertissement aux revendeurs"
author: La rédaction
date: 2009-12-07
href: http://www.commentcamarche.net/news/5850596-vente-liee-avertissement-aux-revendeurs
tags:
- Entreprise
- Logiciels privateurs
- Vente liée
---

> L’affaire a bousculé le landerneau des revendeurs informatiques la semaine passée ! Le fabricant Asus a été condamné à verser la somme de 1405,90 € à l’un de ses clients qui ne souhaitait pas utiliser un système d’exploitation Windows pourtant installé sur le PC qu’il avait acheté. Le tribunal de Lorient a tranché : pour lui, cela s’apparente à de la vente liée. Pour autant les vendeurs ne sont pas tenus d’afficher le prix des OS qui équipent les ordinateurs.
