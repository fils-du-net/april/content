---
site: lepoint.fr
title: "Neelie Kroes, une dame de fer, chargée des TIC"
author: Guerric Poncet 
date: 2009-12-01
href: http://www.lepoint.fr/actualites-technologie-internet/2009-12-01/commission-europeenne-neelie-kroes-une-dame-de-fer-chargee-des-tic/1387/0/400472
tags:
- Le Logiciel Libre
- Internet
- Interopérabilité
- Institutions
- Europe
---

> [...] Elle devrait bientôt faire peur aussi aux gouvernements, qui ont parfois tendance à titiller le droit au respect de la vie privée sur Internet, auquel elle s'intéressera dès 2010. Elle est très attendue, notamment dans les domaines du logiciel libre et des standards informatiques ouverts, deux sujets extrêmement sensibles. Pour mener son action, elle disposera de la direction générale de la Société de l'information et des médias, et de l'Agence européenne pour la sécurité informatique.
