---
site: zdnet.fr
title: "Les Pays-Bas accroissent leur emploi de logiciel libre"
author: Thierry Noisette 
date: 2009-12-06
href: http://www.zdnet.fr/blogs/l-esprit-libre/les-pays-bas-accroissent-leur-emploi-de-logiciel-libre-39711319.htm
tags:
- Le Logiciel Libre
- Administration
- Europe
---

> Le gouvernement néerlandais veut davantage de formats ouverts et d'open source dans ses projets informatiques clés. C'est ce qu'a déclaré jeudi Frank Heemskerk, secrétaire d'État hollandais au Commerce, propos que rapporte le site Osor.eu (dédié à l'emploi du Libre dans le secteur public en Europe). Le secrétaire d'État a estimé que l'adoption de logiciels libres n'allait pas assez vite, et que leur emploi devrait être un objectif des administrations publiques.
