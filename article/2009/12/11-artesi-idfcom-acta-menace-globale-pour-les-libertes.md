---
site: "artesi-idf.com"
title: "ACTA: Menace globale pour les Libertés "
author: La rédaction
date: 2009-12-11
href: http://www.artesi.artesi-idf.com/public/article/acta-menace-globale-pour-les-libertes.html?id=20149&crt=359
tags:
- April
- Institutions
- Informatique-deloyale
---

> Une coalition mondiale d'organisations non-gouvernementales, d'associations de consommateurs et de fournisseurs de services en ligne publie une lettre ouverte adressée aux institutions européennes concernant l'accord commercial relatif à la contrefaçon (ACTA), actuellement en négociation. Ces organisations appellent le Parlement européen et les négociateurs de l'UE à s'opposer à toute disposition dans l'accord multilatéral qui porteraient atteinte aux droits et libertés fondamentaux des citoyens en Europe et à travers le monde.
