---
site: "artesi.artesi-idf.com"
title: "Former les citoyens de demain "
author: La rédaction
date: 2009-12-02
href: http://www.artesi.artesi-idf.com/public/article/former-les-citoyens-de-demain.html?id=20070&crt=355
tags:
- April
---

> [...] Le Premier ministre François Fillon a confié en août 2009 au député Jean-Michel Fourgous une « mission de réflexion et de propositions pour la promotion des technologies de l'information et de la communication dans l'enseignement scolaire ».
> L'association a contribué par des commentaires sur les forums du site de la mission : www.missionfourgous-tice.fr  L'April a également transmis par courrier au député Jean-Michel Fourgous la synthèse de ses commentaires.
