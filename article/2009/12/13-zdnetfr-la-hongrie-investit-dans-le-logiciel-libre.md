---
site: zdnet.fr
title: "La Hongrie investit dans le logiciel libre"
author: Thierry Noisette 
date: 2009-12-13
href: http://www.zdnet.fr/blogs/l-esprit-libre/la-hongrie-investit-dans-le-logiciel-libre-39711497.htm
tags:
- Le Logiciel Libre
- Administration
---

> [...] un consortium de sept sociétés menées par FreeSoft PLC, fournisseur hongrois de services en développement logiciel, a été choisi par le gouvernement de la Hongrie.
> À l'issue d'un appel d'offres, le consortium, dont fait partie Ingres, a remporté l'attribution d'un projet logiciel en open source, pour un budget de 4 milliards de forints hongrois (environ 15 millions d'euros) sur quatre ans.
