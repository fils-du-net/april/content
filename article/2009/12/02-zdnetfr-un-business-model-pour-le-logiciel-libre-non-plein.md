---
site: zdnet.fr
title: "Un business model pour le logiciel libre? Non, plein"
author: Thierry Noisette
date: 2009-12-02
href: http://www.zdnet.fr/blogs/l-esprit-libre/un-business-model-pour-le-logiciel-libre-non-plein-39711254.htm
tags:
- Le Logiciel Libre
- Économie
---

> Il y a quelques jours, un article du New York Times sur MySQL émettait des doutes sur la possibilité d'un modèle économique pour l'open source. L'article citait Red Hat comme «un cas rare».
> [...] Le papier du quotidien américain arrive un peu après la bataille, si on se souvient qu'en mai dernier, le très capitaliste hebdomadaire anglais The Economist écrivait: «Le débat a été remporté. Il est maintenant généralement admis que l'avenir comprendra un mélange de logiciels propriétaires et open source. Les éditeurs de logiciel traditionnels ont ouvert certains de leurs produits, et de nombreuses entreprises open source ont adopté un modèle hybride.»
