---
site: zdnet.fr
title: "Ballot Screen Windows : Microsoft accepterait le libre choix du navigateur"
author: Christophe Auffray
date: 2009-12-04
href: http://www.zdnet.fr/actualites/internet/0,39020774,39711295,00.htm
tags:
- Internet
- Logiciels privateurs
- Vente liée
---

> Juridique - D’après des sources citées par Bloomberg, Microsoft aurait accepté les modifications d’Opera, Mozilla et Google concernant le ballot screen de Windows, destiné à offrir un libre choix du navigateur Web dans XP, Vista et Windows 7.
> Sous la férule de la Commission européenne, Microsoft travaille à la mise en place d'une solution dans Windows permettant aux utilisateurs de choisir librement leur navigateur Web par défaut. La technique retenue est celle du ballot screen, c'est-à-dire un écran listant les principaux navigateurs accompagnés d'un lien d'installation.
