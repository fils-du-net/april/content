---
site: pcinpact.com
title: "Violation de brevet : Word va perdre une fonctionnalité"
author: Vincent Hermann
date: 2009-12-30
href: http://www.pcinpact.com/actu/news/54765-word-i4i-custom-xml-microsoft.htm
tags:
- Logiciels privateurs
- Brevets logiciels
---

> Dernièrement, une société avait déposé plainte contre Microsoft pour violation de brevet. Il s’agissait d’i4i, qui avait d’ailleurs obtenu réparation puisque Word avait été temporairement menacé de retrait des ventes, et que Microsoft devait payer 290 millions de dollars d’amende. Au final, seule l’amende est restée, mais la firme avait obligation de retirer le code incriminé au plus vite.
