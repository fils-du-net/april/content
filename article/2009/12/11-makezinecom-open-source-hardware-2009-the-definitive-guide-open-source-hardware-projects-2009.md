---
site: makezine.com
title: "Open source hardware 2009 - The definitive guide to open source hardware projects in 2009"
author: Phillip Torrone
date: 2009-12-11
href: http://blog.makezine.com/archive/2009/12/open_source_hardware_2009_-_the_def.html
tags:
- Interopérabilité
- Partage du savoir
- Innovation
- Philosophie GNU
---

> Guide des projets de matériels sous licences libres en 2009
> [...] First up - What is open source hardware? These are projects in which the creators have decided to completely publish all the source, schematics, firmware, software, bill of materials, parts list, drawings and "board" files to recreate the hardware - they also allow any use, including commercial. Similar to open source software like Linux, but this hardware centric.
