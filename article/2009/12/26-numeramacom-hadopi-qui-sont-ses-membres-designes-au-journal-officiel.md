---
site: numerama.com
title: "Hadopi : qui sont ses membres désignés au Journal Officiel ?"
author: Guillaume Champeau
date: 2009-12-26
href: http://www.numerama.com/magazine/14762-hadopi-qui-sont-ses-membres-designes-au-journal-officiel.html
tags:
- HADOPI
---

> La liste des membres du collège et de la commission de la Haute Autorité pour la diffusion des oeuvres et la protection des droits sur Internet (Hadopi) a été publiée samedi au Journal Officiel. On connaît aussi l'équipe experte qui sera chargée de lutter contre les pirates, composée notamment de Frank Riester, Michel Thiollière, Jacques Toubon, ou Jean Berbineau. Détails.
