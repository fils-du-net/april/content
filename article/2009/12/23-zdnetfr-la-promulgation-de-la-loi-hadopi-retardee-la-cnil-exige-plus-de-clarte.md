---
site: ZDNet
title: "La promulgation de la loi Hadopi retardée, la CNIL exige plus de clarté "
author: Olivier Chicheportiche
date: 2009-12-23
href: http://www.zdnet.fr/actualites/internet/0,39020774,39711789,00.htm
tags:
- HADOPI
---

> Réglementation - La Commission nationale de l'informatique et des libertés refuserait de valider un des décrets d'application de la loi qui vise à créer un fichier des internautes téléchargeurs condamnés.
