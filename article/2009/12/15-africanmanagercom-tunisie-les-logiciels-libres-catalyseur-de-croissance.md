---
site: africanmanager.com
title: "Tunisie : Les Logiciels Libres, catalyseur de croissance"
author: Fatnassi Abderraouf 
date: 2009-12-15
href: http://www.africanmanager.com/articles/125696.html
tags:
- Le Logiciel Libre
- Administration
- International
---

> La Tunisie a fait , voici  cinq ans, le choix  des logiciels libres comme plateforme de développement économique et social pour faire face à l’augmentation des prix des logiciels propriétaires ou commerciaux.
> C’est dans ce cadre qu’a été organisée, mardi,  la 5ème conférence sur les logiciels libres en Tunisie, à l’initiative du ministère des Technologies de la Communication sur le thème «  Logiciels Libres : Levier d’Innovation et de Croissance » . Il s’est agi de débattre et d’échanger des informations et les connaissances sur plusieurs aspects du Logiciel Libre  ainsi que de mettre en exergue son rôle catalyseur et incontournable en matière d’innovation.
