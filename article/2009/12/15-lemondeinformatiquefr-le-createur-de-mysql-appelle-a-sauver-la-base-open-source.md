---
site: lemondeinformatique.fr
title: "Le créateur de MySQL appelle à 'sauver' la base Open Source"
author: Maryse Gros 
date: 2009-12-15
href: http://www.lemondeinformatique.fr/actualites/lire-le-createur-de-mysql-appelle-a-sauver-la-base-open-source-29588.html
tags:
- Le Logiciel Libre
- Institutions
- Europe
---

> Inquiet du risque que le rachat de Sun par Oracle fait peser sur l'avenir de MySQL, la base de données Open Source qu'il a créé, Michael 'Monty' Widenius demande aux utilisateurs du produit d'écrire à la Commission européenne. Dans le cadre de son enquête anticoncurrentielle sur cette opération, Bruxelles a demandé des garanties à Oracle sur l'avenir de la base.
