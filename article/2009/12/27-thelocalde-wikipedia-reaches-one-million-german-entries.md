---
site: thelocal.de
title: "Wikipedia reaches one million German entries"
author: La rédaction
date: 2009-12-27
href: http://www.thelocal.de/sci-tech/20091227-24203.html
tags:
- Internet
- Partage du savoir
- English
---

> La wikipedia allemande atteint le million d'entrées. C'est la seconde langue la plus importante en nombre d'articles après la langue anglaise (plus de trois millions d'articles) et avant la française (moins de 900 000).
