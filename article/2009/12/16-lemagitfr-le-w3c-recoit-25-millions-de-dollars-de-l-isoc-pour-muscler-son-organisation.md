---
site: LeMagIT
title: "Le W3C reçoit 2,5 millions de dollars de l’Isoc pour muscler son organisation "
author: Cyrille Chausson
date: 2009-12-16
href: http://www.lemagit.fr/article/internet-standards-w3c-financement-isoc-berners-lee/5095/1/le-w3c-recoit-millions-dollars-isoc-pour-muscler-son-organisation/
tags:
- Internet
- Interopérabilité
- Institutions
- Accessibilité
- Standards
---

> Pour renforcer la structure organisationnelle de W3C, l’Internet Society injecte 2,5 millions de dollars dans le consortium de standardisation de Tim Berners-Lee. Objectif : consolider l’ossature de l’organisme pour lui donner les moyens de se rapprocher des utilisateurs et des développeurs.
> [...] L’idée principale au coeur de ce financement, expliquent Ian Jacobs, directeur de la communication du W3C, et Ralph Swick, Pdg du W3C, dans un mail conjoint envoyé à la rédaction, est d’assister le consortium dans l’évolution de sa structure organisationnelle. Cette donation vise également à soutenir le W3C, en charge du développement des standards du Web, “dans son effort pour devenir plus agile et complet dans ses réponses aux développeurs ainsi qu’à la communauté d’utilisateurs”.
