---
site: marianne2.fr
title: "2009, l'année de l'autre économie"
author: Bernard Maris
date: 2009-12-30
href: http://www.marianne2.fr/2009,-l-annee-de-l-autre-economie_a183289.html
tags:
- Le Logiciel Libre
- Économie
---

> [...] Ceci permet de comprendre que l’autre économie n’est pas une économie ringarde, rétrograde, au contraire : l’un des meilleurs exemples de l’autre économie est le logiciel libre, fondé en amont sur la coopération désintéressée, même si en aval, des entreprises peuvent faire des profits en installant des logiciels issus du libre.
