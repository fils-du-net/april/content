---
site: zdnet.fr
title: "Canonical : Mark Shuttleworth va laisser sa place de PDG à Jane Silber pour se concentrer sur Ubuntu"
author: Thierry Noisette 
date: 2009-12-17
href: http://www.zdnet.fr/blogs/l-esprit-libre/canonical-mark-shuttleworth-va-laisser-sa-place-de-pdg-a-jane-silber-pour-se-concentrer-sur-ubuntu-39711656.htm
tags:
- Le Logiciel Libre
- Entreprise
---

> Mark Shuttleworth, fondateur de Canonical et de la fondation Ubuntu [...] a annoncé lors d'une conférence de presse ce jeudi, et sur son propre site, qu'à partir de mars 2010 il laissera sa place de PDG (CEO) de Canonical à Jane Silber, qui en est actuellement directrice générale (COO) et directrice des services en ligne.
