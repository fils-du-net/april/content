---
site: nypost.com
title: "Oracle deal is not EC"
author: Josh Kosman
date: 2009-12-09
href: http://www.nypost.com/p/news/business/oracle_deal_is_not_ec_Rz4L3qL4XceB4riiRbH7xO
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Économie
- Europe
---

> Oracle serait prèt à signer un accord avec la CE pour offrir certaines garanties sur l'indépendance de MySQL.
