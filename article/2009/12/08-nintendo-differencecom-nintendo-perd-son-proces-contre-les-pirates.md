---
site: "nintendo-difference.com"
title: "Nintendo perd son procès contre les pirates"
author: La rédaction
date: 2009-12-08
href: http://www.nintendo-difference.com/news19113-nintendo-perd-son-proces-contre-les-pirates.htm
tags:
- Le Logiciel Libre
---

> [...] La défense a également soulignée que ces linkers servaient également à stockers des logiciels libres réalisés par des indépendants et que les interdire revenait à supprimer une certaine liberté d'expression. Les arguments de la défense semblent avoir convaincus les juges qui se sont prononcés en faveur des revendeurs (magasins et grossistes).
