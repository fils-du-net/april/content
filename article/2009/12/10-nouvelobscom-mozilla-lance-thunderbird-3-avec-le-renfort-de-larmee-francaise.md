---
site: nouvelobs.com
title: "Mozilla lance Thunderbird 3 avec le renfort de l'armée française"
author: Marie Mawad et David Lawsky
date: 2009-12-10
href: http://tempsreel.nouvelobs.com/depeches/medias/20091210.REU2356/mozilla_lance_thunderbird_3_avec_le_renfort_de_larmee_f.html
tags:
- Le Logiciel Libre
- Entreprise
- Administration
---

> [...] Mozilla, porte-drapeau de "l'open source", compte ainsi désormais l'armée française parmi ses contributeurs volontaires, et a lancé cette semaine une nouvelle version de son logiciel de messagerie Thunderbird qui intègre des innovations à mettre au crédit du ministère de la Défense.
