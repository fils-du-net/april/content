---
site: lemagit.fr
title: "L’édition Open Source, encore profitable grâce au Saas "
author: Cyrille Chausson 
date: 2009-12-07
href: http://www.lemagit.fr/article/saas-cloud-computing-opensource-marche/5020/1/l-edition-open-source-encore-profitable-grace-saas/
tags:
- Le Logiciel Libre
- Entreprise
- Informatique en nuage
---

> Selon la dernière analyse de Saugatuck Technology, les éditeurs Open Source peuvent encore vivre de la vente pure de leurs applications grâce aux besoins d’intéropérabilité et de développements à bas coût des éditeurs de services Saas et de Cloud Computing. Un modèle valide jusqu’en 2015 au moins.
