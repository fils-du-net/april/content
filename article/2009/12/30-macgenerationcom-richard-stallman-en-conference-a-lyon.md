---
site: macgeneration.com
title: "Richard Stallman en conférence à Lyon"
author: Anthony Nelzin
date: 2009-12-30
href: http://www.macgeneration.com/news/voir/138381/richard-stallman-en-conference-a-lyon
tags:
- Le Logiciel Libre
- Philosophie GNU
---

> Le geek alpha initiateur du projet GNU et ardent défenseur du libre Richard Stallmann donnera le 13 janvier 2010 une conférence (en français) au Grand amphithéâtre de l'Université Lyon 2 sur le thème « Logiciel libre. Société libre. »
