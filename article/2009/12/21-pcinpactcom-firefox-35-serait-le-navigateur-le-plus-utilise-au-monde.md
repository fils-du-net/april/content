---
site: pcinpact.com
title: "Firefox 3.5 serait le navigateur le plus utilisé au monde"
author: Vincent Hermann
date: 2009-12-21
href: http://www.pcinpact.com/actu/news/54661-statcounter-firefox-navigateur-parts-marche.htm
tags:
- Le Logiciel Libre
---

> D’après la société StatCounter, Firefox 3.5 est désormais le navigateur le plus utilisé dans le monde. Cet état ne durera peut-être pas longtemps, mais il est important de le signaler tant la situation est significative de la guerre qui fait rage entre les navigateurs et des changements d’habitudes des utilisateurs.
