---
site: 01net.com
title: "Un an après, le Conseil national du numérique pourrait enfin voir le jour"
author: Xavier Biseul
date: 2009-12-18
href: http://pro.01net.com/editorial/510159/un-an-apres-le-conseil-national-du-numerique-pourrait-enfin-voir-le-jour
tags:
- Internet
- April
- Institutions
---

> [...] Selon l'April, il devrait voir le jour courant 2010. L'association en faveur du logiciel libre a mis en ligne un compte-rendu d'une réunion interministérielle qui s'est tenue le 20 novembre. Lors de cette conférence au sommet, le cabinet du Premier ministre a validé le principe de réactiver le Conseil national du numérique sous forme d'association. Une association qui aura pour but d'assurer le règlement des litiges entre professionnels, d'accueillir l'observatoire économique du numérique, mais aussi d'élaborer les outils de régulation « à travers des chartes et labels ».
