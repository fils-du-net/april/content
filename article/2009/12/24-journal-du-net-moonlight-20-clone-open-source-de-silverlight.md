---
site: journaldunet.com
title: "Moonlight 2.0 : clone Open Source de Silverlight"
date: 2009-12-24
href: http://www.journaldunet.com/developpeur/breve/44173/moonlight-2-0---clone-open-source-de-silverlight.shtml
tags:
- Le Logiciel Libre
- Entreprise
- Internet
---

> Avec trois mois de retard sur le calendrier prévu, le projet Mono a dévoilé la deuxième version de son infrastructure de diffusion de contenu riches. Avec Silverlight 3.0 en ligne de mire.
> Dévoilé par Novell en début de semaine, Moonlight 2.0 se présente sous la forme d'une extension au navigateur Firefox. Clone Open Source de l'infrastructure de diffusion audio/vidéo de Microsoft (Silverlight), cette application est conçue pour les distributions Linux optimisées pour les architectures x86 (32 bits et 64 bits).
