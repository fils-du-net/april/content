---
site: journalducameroun.com
title: "Cameroun: Lauriers pour les étudiants du campus numérique francophone de Ngaoundéré"
author: Mahurin Petsoko
date: 2009-12-11
href: http://www.journalducameroun.com/article.php?aid=3471
tags:
- Le Logiciel Libre
- Éducation
- International
---

> [...] La cérémonie de mercredi dernier a permis aux trois premières promotions qui ont pris part à des formations au sein du Campus Numérique Francophone d’apprécier toute l’importance des différents ateliers auxquels ils ont participé, de l’utilisation des logiciels libres au système d’exploitation Ubuntu en passant par l’usage avancée de la bureautique et d’Internet ainsi que le traitement des textes scientifiques en Latex.
