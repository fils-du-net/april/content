---
site: rue89.com
title: "Au « fab lab », fabriquez vous-même votre machine à laver"
author: Raphaël Moran
date: 2009-12-27
href: http://www.rue89.com/innovation/2009/12/27/au-fab-lab-fabriquez-vous-meme-votre-machine-a-laver-130828
tags:
- Le Logiciel Libre
- Partage du savoir
- Innovation
---

> [...] Le concept de fab lab applique au monde industriel l'esprit de partage, d'innovation et de gratuité que l'on trouve sur Internet comme avec les logiciels libres et les réseaux sociaux. En permettant à n'importe qui d'accéder à des machines industrielles simples et à bas coût, on sort du mode de production classique. C'est en cela que des communautés de hackers veulent s'investir dans les fab labs.
