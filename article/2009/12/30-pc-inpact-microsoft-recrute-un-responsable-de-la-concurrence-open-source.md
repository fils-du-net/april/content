---
site: pcinpact.com
title: "Microsoft recrute un responsable de la concurrence open source"
author: Vincent Hermann
date: 2009-12-30
href: http://www.pcinpact.com/actu/news/54763-microsoft-concurrence-responsable-linux-openoffice.htm
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Administration
- Économie
---

> L’histoire de Microsoft et du monde de l’open source a longtemps été pleine de déni et d’attaques en tous genres de la part du géant du logiciel. Ces dernières années, le ton s’est calmé, en dehors de quelques assertions de Steve Ballmer, PDG de la firme. Réorganisation interne, publication des protocoles, avancée timide dans l’open source lui-même, mais surtout une reconnaissance des logiciels libres comme compétiteurs réels.
