---
site: ecrans.fr
title: "Asus condamné pour une vente forcée de Windows"
author: Manuel Raynaud
date: 2009-12-02
href: http://www.ecrans.fr/Asus-condamne-pour-une-vente,8670.html
tags:
- Logiciels privateurs
- Vente liée
---

> Eric Magnien est doublement soulagé. Le tribunal de Lorient vient de lui donner une seconde fois raison dans le différend qui l’opposait à Asus. Le 27 mai 2008, il s’était offert un ordinateur portable mais, comme la loi le prévoit, ne souhaitait pas conserver Windows Vista vendu avec. Il préfère utiliser des logiciels libres.
