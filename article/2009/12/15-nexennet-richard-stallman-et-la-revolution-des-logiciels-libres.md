---
site: nexen.net
title: "Richard Stallman et la révolution des logiciels libres"
author: Christophe Villeneuve
date: 2009-12-15
href: http://www.nexen.net/articles/communique_de_presse/19383-richard_stallman_et_la_revolution_des_logiciels_libres.php
tags:
- Le Logiciel Libre
- Philosophie GNU
---

> [...] Projet à l'initiative de Framasoft, cette biographie autorisée, revue et enrichie par lui- même, donne un éclairage nouveau sur ce personnage intègre aux allures de prophète. Une référence indispensable pour qui veut comprendre l’origine et les
>  évolutions de cette lame de fond qu’est le logiciel libre.
