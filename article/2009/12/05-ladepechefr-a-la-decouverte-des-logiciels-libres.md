---
site: ladepeche.fr
title: "A la découverte des logiciels libres"
author: La rédaction
date: 2009-12-05
href: http://www.ladepeche.fr/article/2009/12/04/729984-A-la-decouverte-des-logiciels-libres.html
tags:
- Le Logiciel Libre
- April
- Sensibilisation
---

> [...] « Ubuntu est une distribution GNU/Linux qui réunit stabilité et convivialité, rappellent Christophe Sautier, président de l'association Ubuntu-fr, et Thomas Petazzoni, président de l'association Toulibre.. Elle est constituée de logiciels libres, que tout un chacun peut librement utiliser, copier, modifier et redistribuer en toute liberté avec de nombreux avantages : faible coût, transparence, sécurité, indépendance, valeurs de solidarité et d'entraide, partage des connaissances.
