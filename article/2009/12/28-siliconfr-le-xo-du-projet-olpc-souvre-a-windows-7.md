---
site: silicon.fr
title: "Le XO du projet OLPC s'ouvre à Windows 7"
author: Christophe Lagane 
date: 2009-12-28
href: http://www.silicon.fr/fr/news/2009/12/28/le_xo_du_projet_olpc_s_ouvre_a_windows_7
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Éducation
---

> [...] Pour Wayan Vota, responsable des actualités du projet OLPC, l'arrivée de Windows 7 marque « la fin des racines open source de l'OLPC » écrit-il. « Je continue de dire que Windows -quel qu'en soit la saveur- est une erreur marketing pour OLPC. » Selon lui, l'OS de Microsoft reste incompatible avec la vision d'outil pédagogique que constitue le XO et que Linux permet de tailler sur mesure.
