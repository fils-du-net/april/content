---
site: ladepeche.fr
title: "Tournefeuille. Pot de terre contre pot de fer à Utopia : les nouveaux modes de diffusion"
author: PAUL MOLLA
date: 2009-12-10
href: http://www.ladepeche.fr/article/2009/12/10/733542-Tournefeuille-Pot-de-terre-contre-pot-de-fer-a-Utopia-les-nouveaux-modes-de-diffusion.html
tags:
- Le Logiciel Libre
- Partage du savoir
- Licenses
---

> [...] Bruno Coudoin créateur d'un logiciel libre éducatif « G Compris » explique : « Créative Commons essaie de faire pour la culture ce qui a été fait pour les logiciels libres, et ainsi inventer un nouveau mode de diffusion de la culture. C'est le système de don contre don, les licences libres ne remettent pas en cause les droits d'auteurs. Chaque auteur dispose de son œuvre et choisi la manière dont il veut être diffusé, il suffit d'aller sur le site creativecommons.org ».
