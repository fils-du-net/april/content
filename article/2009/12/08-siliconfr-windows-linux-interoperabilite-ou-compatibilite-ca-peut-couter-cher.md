---
site: silicon.fr
title: "Windows - Linux: Interopérabilité ou compatibilité, ça peut coûter... cher"
author: Pierre Mangin 
date: 2009-12-08
href: http://www.silicon.fr/fr/news/2009/12/08/windows___linux__interoperabilite_ou_compatibilite__ca_peut_couter____cher
tags:
- Logiciels privateurs
- Interopérabilité
---

> Interopérabilité ou simple compatibilité? Le débat rebondit depuis quelques jours en Europe. Mais le vrai débat serait ailleurs: le coût !   [...] L'un des sujets chauds de cette fin d'année 2009 est incontestablement la question de l'interopérabilité, plutôt que  la rivalité souvent stérile  entre le 'logiciel libre' et  les  logiciels dits "propriétaires"
