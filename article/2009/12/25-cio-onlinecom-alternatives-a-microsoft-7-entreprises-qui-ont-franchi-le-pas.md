---
site: "cio-online.com"
title: "Alternatives à Microsoft : 7 entreprises qui ont franchi le pas"
author: Réseaux Télécoms
date: 2009-12-25
href: http://www.cio-online.com/actualites/lire-alternatives-a-microsoft-7-entreprises-qui-ont-franchi-le-pas-2627-page-6.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Allianz Insurance Australia, qui emploie 3000 personnes, avait pour objectif de réduire de 20 % ses émissions de gaz à effet de serre d'ici 2012. Pour y parvenir, le groupe a entamé en 2009 une refonte de son infrastructure, qui reposait sur des machines Intel sous Windows Server.
> [...] La consolidation et la virtualisation ont permis de réduire la consommation électrique tout en réalisant 500 000 € d'économies sur le coût des licences.
