---
site: itespresso.fr
title: "La disparition du secrétariat d’État à l’Economie numérique est-elle programmée ?"
author: Nicolas Guillaume
date: 2009-12-28
href: http://www.itespresso.fr/vie-publique-vers-la-disparition-du-secretaire-detat-a-leconomie-numerique-32956.html
tags:
- Administration
---

> [...] Après la tenue des élections régionales qui se dérouleront en mars 2010 et en fonction des résultats obtenus par les ministres engagés dans la bataille (et plus globalement de l’UMP), des changements devraient survenir au sein de l’équipe gouvernementale.
> On évoque de manière récurrente l’arrivée du bouillonnant Frédéric Lefebvre, ex-député des Hauts-de-Seine et porte-parole UMP, que l’on voit souvent à la tête d’un secrétariat d’État à la Communication voire du Numérique.
> Mais une autre rumeur plus chaude circule depuis quelques semaines : le secrétariat d’État chargé de la Prospective et du Développement de l’économie numérique serait amené à disparaître avec le départ de Nathalie Kosciusko-Morizet (surnommée NKM) vers d’autres horizons.
