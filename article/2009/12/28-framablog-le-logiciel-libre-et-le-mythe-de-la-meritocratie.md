---
site: framablog.org
title: "Le logiciel libre et le mythe de la méritocratie"
author: aKa
date: 2009-12-28
href: http://www.framablog.org/index.php/post/2009/12/28/meritocratie-logiciel-libre-open-source
tags:
- Le Logiciel Libre
- Partage du savoir
- Innovation
- Philosophie GNU
- Promotion
---

> En janvier 2008, Bruce Byfield écrivait, dans un article que nous avions traduit ici-même (Ce qui caractérise les utilisateurs de logiciels libres) : « La communauté du Libre peut se targuer d’être une méritocratie où le statut est le résultat d’accomplissements et de contributions ».
