---
site: techworld.com.au
title: "Lawsuit alleges Palm Pre violates copyright"
author: Mikael Ricknäs
date: 2009-12-07
href: http://www.techworld.com.au/article/328719/lawsuit_alleges_palm_pre_violates_copyright
tags:
- Le Logiciel Libre
- Entreprise
- Brevets logiciels
---

> La société Artifex Software attaque en justice Palm pour non respect de la licence GNU GPL
