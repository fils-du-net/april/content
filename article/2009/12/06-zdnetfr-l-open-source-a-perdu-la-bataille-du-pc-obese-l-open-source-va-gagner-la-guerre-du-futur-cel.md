---
site: zdnet.fr
title: "L’Open Source a perdu la bataille du PC obèse. L’Open Source va gagner la guerre du futur, celle de la mobilité"
author: Louis Naugès
date: 2009-12-06
href: http://www.zdnet.fr/blogs/entreprise-2-0/l-open-source-a-perdu-la-bataille-du-pc-obese-l-open-source-va-gagner-la-guerre-du-futur-celle-de-la-mobilite-39711318.htm
tags:
- Le Logiciel Libre
- Entreprise
- Informatique en nuage
---

> J’ai deux messages forts pour la communauté Open Source :
>  - La mauvaise nouvelle : vous ne gagnerez jamais la bataille du poste obèse.
>  - La bonne nouvelle : le poste obèse deviendra marginal, demain, et sera remplacé par une nouvelle génération de postes de travail, mobiles et allégés, sur lesquels les solutions Open Source vont... gagner la guerre.
