---
site: channelnews.fr
title: "La Drôme offre un \"bureau universel mobile\" à tous ses habitants"
author: Dirk Basyn
date: 2009-12-22
href: http://www.channelnews.fr/regions/75/5336-la-drome-offre-un-qbureau-universel-mobileq-a-tous-ses-habitants.html
tags:
- Le Logiciel Libre
- Administration
---

> Après avoir accompagné la couverture des zones blanches en haut débit et impulsé la création d'un réseau à très haut-débit en 2006, le département va distribuer des clés USB intégrant 50 logiciels libres.
> Afin de réduire la fracture numérique, le département de la Drôme va mettre à la disposition de ses habitants un « bureau universel mobile ». Embarqué sur une clef USB, celui-ci permettra à chacun d’utiliser en permanence et où qu'il se trouve son environnement numérique et sa propre interface, en présence ou non d’une connexion Internet.
