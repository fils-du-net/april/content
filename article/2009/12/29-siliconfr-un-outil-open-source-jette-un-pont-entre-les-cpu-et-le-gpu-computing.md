---
site: Silicon.fr
title: "Un outil open source jette un pont entre les CPU et le GPU Computing"
author: David Feugey 
date: 2009-12-29
href: http://www.silicon.fr/fr/news/2009/12/29/un_outil_open_source_jette_un_pont_entre_les_cpu_et_le_gpu_computing
tags:
- Le Logiciel Libre
- Innovation
- Sciences
---

> Ocelot simplifie le développement d’outils massivement parallèles en permettant la création d’applications GPU et CPU depuis un même code source.
> Le projet Ocelot intéressera les scientifiques, les mathématiciens et – de façon plus générale – toutes les personnes en mal de puissance de calcul. Cette solution sous licence open source BSD est capable de convertir des programmes CUDA (l’outil de programmation des cartes graphiques NVIDIA) en code exploitable sur des processeurs classiques.
