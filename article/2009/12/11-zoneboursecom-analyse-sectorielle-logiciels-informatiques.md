---
site: zonebourse.com
title: "Analyse sectorielle / Logiciels informatiques"
author: La rédaction
date: 2009-12-11
href: http://www.zonebourse.com/informations/actualite-bourse/Analyse-sectorielle-Logiciels-informatiques--13293643/
tags:
- Le Logiciel Libre
- Entreprise
- Économie
---

> [...] Néanmoins, le développement des logiciels libres (pour lesquels chacun peut accéder aux lignes de code) ne se dément pas. En France, les entreprises et les administrations recourent énormément à ce type de logiciels. Selon le cabinet d'étude Markess, 96% des personnes interrogées dans les administrations y ont eu recours en 2009. Le chiffre d'affaires de cette activité devrait fortement croître, à un rythme annuel de 16,5% entre 2009 et 2011, et ainsi atteindre 3 milliards d'euros, contre 2,1 aujourd'hui.
