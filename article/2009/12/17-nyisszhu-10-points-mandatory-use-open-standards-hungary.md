---
site: nyissz.hu
title: "10 points on the mandatory use of open standards in Hungary"
author: La rédaction
date: 2009-12-17
href: http://nyissz.hu/blog/10-points-on-the-mandatory-use-of-open-standards-in-hungary/
tags:
- Le Logiciel Libre
- Administration
- Europe
- English
---

> Le parlement hongrois impose l'utilisation des standards ouverts dans les échanges entre les administrations et le public.
