---
site: marianne2.fr
title: " Robert Rochefort: «Le grand emprunt n'est qu'un saupoudrage» "
author: Sylvain Lapoix
date: 2009-12-02
href: http://www.marianne2.fr/Robert-Rochefort-Le-grand-emprunt-n-est-qu-un-saupoudrage_a182978.html
tags:
- Le Logiciel Libre
- Économie
---

> [...] La loi Hadopi continue de faire débat autour de la défense de la création artistique : quel est le programme du Modem pour le financement de la culture ?
>  Nous sommes favorables à un forfait illimité de consultation de contenus culturels de type licence globale. Par ailleurs, il nous semble essentiel de conserver un secteur non marchand dans la culture, notamment pour gérer les oeuvres libres de droit. Nous contredisons également l'idée selon laquelle les logiciels libres seraient une menace pour l'économie : nous sommes persuadés qu'ils peuvent favoriser la création d'emplois.
