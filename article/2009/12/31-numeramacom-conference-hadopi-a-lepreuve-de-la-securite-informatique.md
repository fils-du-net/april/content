---
site: numerama.com
title: "Conférence : Hadopi à l'épreuve de la sécurité informatique"
author: Guillaume Champeau
date: 2009-12-31
href: http://www.numerama.com/magazine/14788-conference-hadopi-a-l-epreuve-de-la-securite-informatique.html
tags:
- HADOPI
---

> [...] Les étudiants et les personnes extérieures qui assisteront à la conférence pourront ainsi débattre avec une moquerie sans doute non dissimulée de la fameuse obligation de sécuriser son accès à Internet imposée par la loi Hadopi. Les internautes qui n'auraient pas suffisamment protégé leur accès à Internet pour éviter qu'il soit utilisé à des fins de piratage pourront en effet être condamnés pour "négligence caractérisée". C'est en tout cas ce que prévoit la loi, qui aura bien du mal à affronter les réalités pratiques.
