---
site: clubic.com
title: "Comment se faire rembourser Windows ?"
author: Aurélien Audy
date: 2009-12-09
href: http://www.clubic.com/article-303304-1-reboursement-microsoft-windows-achat-pc.html
tags:
- Vente liée
---

> Tous les ordinateurs des grands fabricants sont vendus avec un système d'exploitation et une batterie de logiciels plus ou moins conséquente et intéressante. Motif invoqué : les appareils sont ainsi prêts à l'emploi, parfaitement fonctionnels. Si la pratique satisfait effectivement nombre de consommateurs, notamment les moins aguerris à l'informatique, elle pose en revanche problème pour ceux qui disposent déjà d'une licence (OS payant) ou qui souhaitent utiliser Linux (OS gratuit).
