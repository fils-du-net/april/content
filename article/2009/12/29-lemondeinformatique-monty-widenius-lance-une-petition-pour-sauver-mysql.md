---
site: lemondeinformatique.fr
title: "Monty Widenius lance une pétition pour « sauver MySQL »"
author: Olivier Rafal
date: 2009-12-29
href: http://www.lemondeinformatique.fr/actualites/lire-monty-widenius-lance-une-petition-pour-%C2%A0sauver-mysql%C2%A0-29636.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Institutions
- Innovation
- Licenses
- Philosophie GNU
- Europe
---

> « Sauvez MySQL ! » C'est un appel au monde entier que lance Monty Widenius. Le père de MySQL a en effet mis en place un site appelant les internautes à signer une pétition, dont une première version sera remise dès le 4 janvier prochain aux autorités de régulation de l'Union européenne. Plusieurs pages, traduites en français, expliquent pourquoi la base Open Source, et partant ses utilisateurs, auraient à souffrir si l'UE autorisait en l'état l'acquisition de Sun par Oracle.
