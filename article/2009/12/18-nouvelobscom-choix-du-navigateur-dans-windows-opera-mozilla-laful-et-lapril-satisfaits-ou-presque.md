---
site: nouvelobs.com
title: "Choix du navigateur dans Windows : Opera, Mozilla, l'Aful et l'April satisfaits ou presque"
author: Christophe Auffray
date: 2009-12-18
href: http://hightech.nouvelobs.com/actualites/depeche/20091219.ZDN1075/choix_du_navigateur_dans_windows__opera_mozilla_laful_e.html
tags:
- Internet
- Logiciels privateurs
---

> Si Opera, Mozilla, l'April et l'Aful se félicitent de cet accord prévoyant l'intégration d'un écran de choix du navigateur dans Windows, ils émettent aussi des réserves. Aful espère en outre voir l'Europe s'intéresser à présent à la vente liée de Windows.
> Le 16 décembre, Microsoft et la Commission européenne trouvaient un accord sur la question de l'intégration d'Internet Explorer dans Windows (XP, Vista et Seven), jugée comme déloyale sur le plan de la concurrence et contraire aux intérêts des consommateurs.
> Mi-mars 2010, Microsoft proposera une mise à jour automatique de Windows permettant d'afficher un écran de choix du navigateur dans l'OS. Cette page multi-choix ou ballot screen offrira aux utilisateurs européens de choisir entre 12 navigateurs, classés de manière aléatoire.
