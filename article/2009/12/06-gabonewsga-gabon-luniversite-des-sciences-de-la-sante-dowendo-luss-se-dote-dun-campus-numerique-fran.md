---
site: gabonews.ga
title: "Gabon: L'Université des Sciences de la Santé d'Owendo (l'USS) se dote d'un Campus numérique francophone "
author: Rostano Eloge Mombo Nziengui
date: 2009-12-06
href: http://www.gabonews.ga/actualite/actualites_2009.php?Article=9482
tags:
- Le Logiciel Libre
- Éducation
- Sciences
---

> [...] Relevant l'engagement de l'AUF à promouvoir les logiciels libres afin de réduire « la facture et la fracture numérique », M. ZANOUVI a par ailleurs, fait remarquer que l'une des « particularités de ce campus numérique partenaire est que l'ensemble des outils informatiques et le réseau tourne sous Linux, logiciels libres téléchargeables gratuitement sur le Net et largement utilisés mondialement dans les universités, car sans coût de licences et plus sécurisés vis à vis des attaques informatiques.
