---
site: ladepeche.fr
title: "Coup de jeune à aGeNUx"
author: Richard Hecht
date: 2009-12-16
href: http://www.ladepeche.fr/article/2009/12/16/738327-Coup-de-jeune-a-aGeNUx.html
tags:
- Le Logiciel Libre
- Administration
---

> [...] En prolongement de la journée régionale « Aquitaine libre » qui s'était tenue le 6 juin dernier sur le campus Michel-Serres (600 participants), aGeNUx s'investit dans l'éducation au développement durable avec le conseil général. Des contacts sont pris cette semaine avec la ville d'Agen par le président élu samedi, âgé de 23 ans, et son prédécesseur pour arrêter des initiatives en direction du jeune public.
