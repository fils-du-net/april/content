---
site: numerama.com
title: "La TV publique norvégienne offre 7 heures de vidéo HD en Creative Commons"
author: Guillaume Champeau
date: 2009-12-21
href: http://www.numerama.com/magazine/14739-la-tv-publique-norvegienne-offre-7-heures-de-video-hd-en-creative-commons.html
tags:
- Droit d'auteur
- Licenses
- Philosophie GNU
- Video
---

> On se souvient que NRK, la chaîne de télévision publique norvégienne, avait mis en place un tracker BitTorrent pour faciliter la distribution de ses programmes par les internautes, sans DRM. Ce mois-ci, NRK a été plus loin encore en proposant 7 heures et demi de vidéos haute-définition sous licence Creative Commons. La chaîne a ainsi mis à disposition l'intégralité de ce qu'a filmé une caméra Sony 700 en XDCAM HD 1080 50i placée à la tête d'un train, dans un voyage entre Bergen à l'ouest du pays et la capitale Oslo.
