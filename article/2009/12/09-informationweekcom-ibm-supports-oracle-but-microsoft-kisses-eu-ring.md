---
site: informationweek.com
title: "IBM Supports Oracle But Microsoft Kisses EU Ring"
author: Bob Evans
date: 2009-12-09
href: http://www.informationweek.com/news/global-cio/security/showArticle.jhtml?articleID=222001290&tcss=global-cio
tags:
- Le Logiciel Libre
- Entreprise
---

> IBM soutient Oracle auprès de la Commission européenne et affirme que MySQL ne peut par être utilisé pour des taches sophistiquées.
