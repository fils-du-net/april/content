---
site: lexpress.fr
title: "\"openCimetiere\", un logiciel libre pour gérer les caveaux"
author: Reuters
date: 2009-12-10
href: http://www.lexpress.fr/actualites/2/opencimetiere-un-logiciel-libre-pour-gerer-les-caveaux_835033.html
tags:
- Le Logiciel Libre
- Administration
---

> [...] openCimetiere, logiciel "open source" de gestion des concessions de cimetière, figure au rang des projets phares de l'Adullact, l'Association des développeurs et utilisateurs de logiciels libres pour l'administration et les collectivités territoriales.
> Celle-ci a créé un site de partage de logiciels libres à l'usage des hôpitaux, écoles et entreprises notamment, mais aussi des personnels communaux.
