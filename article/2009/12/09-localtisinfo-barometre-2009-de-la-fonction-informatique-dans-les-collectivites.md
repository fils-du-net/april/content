---
site: localtis.info
title: "Baromètre 2009 de la fonction informatique dans les collectivités"
author: Luc Derriano
date: 2009-12-09
href: http://www.localtis.info/cs/ContentServer?c=artVeille&pagename=Localtis/artVeille/artVeille&cid=1250259226075
tags:
- Le Logiciel Libre
- Administration
- Informatique en nuage
---

> Le recours aux logiciels libres se confirme
> [...] Enfin, dans l'évolution des choix technologiques, la tendance vers le choix des logiciels libres se confirme (56%), "soutenue par les élus et notamment pour une meilleure maîtrise des coûts même si les DSI sont bien conscients des budgets supplémentaires nécessaires à mettre en œuvre pour la maintenance, la formation ou le développement de solutions sur mesure", précise le consultant.
