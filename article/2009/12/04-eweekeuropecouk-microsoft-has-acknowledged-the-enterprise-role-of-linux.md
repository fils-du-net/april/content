---
site: eweekeurope.co.uk
title: "Microsoft 'Has Acknowledged The Enterprise Role Of Linux'"
author: Peter Judge
date: 2009-12-04
href: http://www.eweekeurope.co.uk/interview/microsoft--has-acknowledged-the-enterprise-role-of-linux---2667
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
---

> D'après le responsable Novell d'OpenSUSE, Microsoft reconnait la valeur de Linux en entreprise.
