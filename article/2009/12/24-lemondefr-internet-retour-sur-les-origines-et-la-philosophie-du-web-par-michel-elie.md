---
site: lemonde.fr
title: "Internet, retour sur les origines et la \"philosophie\" du Web, par Michel Elie"
author: Michel Elie
date: 2009-12-24
href: http://www.lemonde.fr/opinions/article/2009/12/24/internet-retour-sur-les-origines-et-la-philosophie-du-web-par-michel-elie_1284618_3232.html
tags:
- Le Logiciel Libre
- Internet
---

> [...] Le partage des ressources, en matériel, logiciels, données ainsi que des ressources humaines était un objectif majeur. S'y ajoute une culture de l'échange. Le réseau devient vite aussi un moyen de soumettre à la communauté des utilisateurs des algorithmes à vérifier, des programmes à tester, des données à archiver. Il deviendra un levier pour les promoteurs du logiciel libre. Il a su galvaniser des énergies et des intelligences désintéressées, individuelles et collectives.
