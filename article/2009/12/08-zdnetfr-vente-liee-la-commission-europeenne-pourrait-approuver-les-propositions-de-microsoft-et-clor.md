---
site: zdnet.fr
title: "Vente liée : la Commission européenne pourrait approuver les propositions de Microsoft et clore son enquête"
author: La rédaction
date: 2009-12-08
href: http://www.zdnet.fr/actualites/informatique/0%2C39040745%2C39711356%2C00.htm
tags:
- Logiciels privateurs
- Vente liée
---

> Réglementation - Le bras de fer entre la Commission européenne et Redmond devrait prendre fin mardi prochain lors de la dernière réunion annuelle du collège des commissaires. Les ultimes concessions faites par Microsoft sur le "Ballot screen" auraient convaincu.
> Microsoft en aura peut-être bientôt terminé avec 10 années de procédures qui ont vu la Commission européenne sanctionner lourdement certaines de ses pratiques commerciales.
