---
site: charentelibre.com
title: "Chazelles reine de la Toile"
author: Niels Goumy
date: 2009-12-25
href: http://blog.charentelibre.com/journal/index.php?post/2009/12/25/4511-chazelles-reine-de-la-toile
tags:
- Le Logiciel Libre
- Administration
---

> «A l'origine, le label a principalement récompensé notre volonté d'asseoir le logiciel libre en zone rurale. Il est pérennisé aujourd'hui, c'est bien mais ce n'est pas suffisant. L'esprit des villes Internet ce n'est pas une seule personne, un seul site, c'est un tout. C'est aussi une prise de risque collective qui est saluée, une dynamique commune. Maintenant il va falloir passer à l'étape supérieure ».
