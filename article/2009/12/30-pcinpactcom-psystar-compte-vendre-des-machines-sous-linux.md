---
site: pcinpact.com
title: "Psystar compte vendre des machines sous Linux"
author: Vincent Hermann
date: 2009-12-30
href: http://www.pcinpact.com/actu/news/54753-psystar-rebel-efi-ordinateurs-linux2.htm
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> [...] Plusieurs lecteurs ont trouvé étrange que le constructeur parle d’informatique ouverte avec un système d’exploitation comme Mac OS X. pour beaucoup, cette notion d’ouverture fait surtout référence au logiciel libre. Et justement, puisque Psystar ne peut plus commercialiser de machines avec le système d’Apple, il est temps de passer à… Linux !
