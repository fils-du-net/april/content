---
site: zdnet.fr
title: "Asa Dotzler, Mozilla : utilisateurs de Firefox, laissez Google, passez à Bing"
author: Christophe Auffray
date: 2009-12-11
href: http://www.zdnet.fr/actualites/internet/0,39020774,39711467,00.htm
tags:
- Internet
- Neutralité du Net
---

> Impliqué dès le début dans la création de Firefox et désormais directeur de la communauté de développement du navigateur, Asa Dotzler conseille aux utilisateurs d’adopter Bing en moteur par défaut après les déclarations du PDG de Google sur la vie privée.
> Les récentes déclarations du patron de Google, Eric Schmidt, lors d'une interview à CNBC dévoilant le peu de cas qu'il fait du respect de la vie privée des internautes, ne sont manifestement pas tombées dans l'oreille d'un sourd.
