---
site: lefigaro.fr
title: "Microsoft Office accessible gratuitement en ligne"
author: Samuel Laurent
date: 2009-07-15
href: http://www.lefigaro.fr/hightech/2009/07/13/01007-20090713ARTFIG00354-avec-office-2010-microsoft-dans-les-pas-de-google-.php
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Innovation
---

> Microsoft offrira à partir de l'année prochaine une version gratuite en ligne de ses logiciels phares, Word, Excel ou Powerpoint. Un pari risqué, qui vise à répondre à Google.
> Microsoft opère un virage à 180 degrés. La firme annonce mardi que la prochaine version de sa suite logicielle bureautique, Office 2010, sera disponible gratuitement en ligne au premier semestre de l'année prochaine.
> Word, Excel, Powerpoint... les logiciels de bureautique de Microsoft, utilisés par des millions d'entre nous chaque jour au travail, seront accessibles depuis n'importe quel navigateur, dans une version allégée. Un bouleversement pour Microsoft, qui tente visiblement de regagner du terrain face à Google et à sa suite Google Documents, lancée en 2006, que n'importe qui peut utiliser gratuitement depuis son navigateur.
