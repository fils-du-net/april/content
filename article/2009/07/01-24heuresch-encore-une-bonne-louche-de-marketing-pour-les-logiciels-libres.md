---
site: 24heures.ch
title: "Encore une bonne louche de marketing pour les logiciels libres! "
author: Emmanuel Barraud
date: 2009-07-01
href: http://www.24heures.ch/actu/economie/bonne-louche-marketing-logiciels-libres-2009-06-30
tags:
- Le Logiciel Libre
---

> Mais les programmes libres auront encore besoin de temps – et d’un bon marketing, façon Apple – pour s’imposer. Et ce malgré tous leurs avantages, énumérés récemment à Yverdon-les-Bains par des spécialistes dans le cadre du lancement d’un nouveau «master of advanced studies».
