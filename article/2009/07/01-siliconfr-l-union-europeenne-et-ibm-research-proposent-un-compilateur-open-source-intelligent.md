---
site: silicon.fr
title: "L’Union européenne et IBM Research proposent un compilateur open source \"intelligent\""
author: David Feugey 
date: 2009-07-01
href: http://www.silicon.fr/fr/news/2009/07/01/l_union_europeenne_et_ibm_research_proposent_un_compilateur_open_source__intelligent_
tags:
- Le Logiciel Libre
- Entreprise
- Administration
---

> MILEPOST (Machine Learning for Embedded Programs Optimisation) est un projet européen ambitieux qui vise à fournir des technologies permettant d’optimiser automatiquement le code compilé pour les processeurs reconfigurables… ou tout simplement pour les nouvelles architectures.
> [...] Ce projet open source permet de transformer le compilateur GCC en un véritable outil capable d’optimiser automatiquement le code, et ce, quelle que soit l’architecture cible.
