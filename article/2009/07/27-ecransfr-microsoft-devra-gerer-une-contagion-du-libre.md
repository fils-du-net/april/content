---
site: ecrans.fr
title: "Microsoft devra gérer une contagion du libre"
author: Christophe Alix
date: 2009-07-27
href: http://www.ecrans.fr/Microsoft-devra-gerer-une,7817.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Dans un environnement informatique en pleine mutation, ­Microsoft, dont le chiffre d’affaires et le ­bénéfice ont reculé en 2008 – une première  ! –, est contraint à bouger. Vendredi, le géant du logiciel a dû faire une concession majeure à Bruxelles, à propos de son navigateur Explorer. Quelques jours plus tôt, il avait fait « don » à la communauté Linux du code source de 20 000 lignes ­concernant trois gestionnaires de périphériques.
