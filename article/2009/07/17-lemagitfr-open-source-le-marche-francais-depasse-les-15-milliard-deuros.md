---
site: lemagit.fr
title: "Open Source : le marché français dépasse les 1,5 milliard d'euros"
author: Reynald Fléchaux
date: 2009-07-17
href: http://www.lemagit.fr/article/bureautique-mysql-oracle-developpement-sgbd-etude-support-openoffice-open-source-ingres/3802/1/open-source-marche-francais-depasse-les-milliard-euros/
tags:
- Le Logiciel Libre
- Entreprise
- Économie
---

> Présentée lors des Rencontres mondiales du logiciel libre, qui se tenait du 7 au 11 juillet à Nantes, une étude de Pierre Audoin Consultants décrit un marché français du logiciel libre en forte croissance, même si celle-ci tend à ralentir. Selon le cabinet, ce segment pourrait dépasser les 3 milliards d'euros en 2012.
