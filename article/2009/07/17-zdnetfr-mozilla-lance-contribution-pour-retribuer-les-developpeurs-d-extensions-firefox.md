---
site: zdnet.fr
title: "Mozilla lance « Contribution » pour rétribuer les développeurs d’extensions Firefox "
author: Eureka Presse
date: 2009-07-17
href: http://www.zdnet.fr/actualites/internet/0,39020774,39702264,00.htm
tags:
- Le Logiciel Libre
- Économie
---

> Technologie - Mozilla lance une expérience pilote d’un système permettant aux développeurs d’extensions pour Firefox de solliciter une rétribution de la part des utilisateurs.
> Baptisé « Contribution », le programme que vient de lancer la Fondation Mozilla vise à « soutenir un écosystème en offrant à nos usagers la possibilité d'apporter leur soutien à leurs développeurs d'extensions favoris ».
