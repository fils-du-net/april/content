---
site: romandie.com
title: "La Confédération est provisoirement autorisée à acheter Microsoft"
author: ats
date: 2009-07-03
href: http://www.romandie.com/infos/ats/display2.asp?page=20090703121811230172019048000_brf016.xml
tags:
- Logiciels privateurs
- Vente liée
---

> L'administration fédérale peut continuer à acheter Microsoft. Le Tribunal administratif fédéral (TAF) a débouté des fournisseurs de logiciels libres qui demandaient une interdiction provisoire.
> [...]
> Dix-huit fournisseurs de logiciels libres ont recouru auprès du TAF. Ils déplorent l'absence d'appel d'offres. Parallèlement, ils demandent que le contrat ne puisse être exécuté tant que la procédure judiciaire suit son cours et que Berne ne puisse pas dans l'intervalle introduire de nouveaux logiciels ou remplacer Windows XP par Vista.
