---
site: pcinpact.com
title: "Vente liée : l'immobilisation du PC doit être dédommagée"
author: Marc Rees
date: 2009-07-02
href: http://www.pcinpact.com/actu/news/51752-racketiciel-vente-liee-subordonnee-consommateur.htm
tags:
- Vente liée
---

> Et de trois : la vente subordonnée d’un ordinateur avec un système d’exploitation vient de s’enrichir de trois nouvelles victoires pour les adversaires réunis notamment au sein de l'équipe « Racketiciel ». Trois jugements qui apportent de nouvelles pierres à un édifice chaîné aujourd’hui un peu plus fragilisé.
