---
site: ITR Manager
title: "Bison Fûté améliore l’information routière avec du logiciel libre"
author: ITR Manager
date: 2009-07-08
href: http://www.itrmanager.com/articles/93337/bison-fute-ameliore-information-routiere-logiciel-libre.html
tags:
- Le Logiciel Libre
- Administration
- Économie
- Innovation
- Sciences
- Europe
---

> « Tipi », la solution mise en œuvre par Capgemini pour le compte de Bison Futé va permettre aux centres régionaux d'information et de coordination routière de disposer d'informations sur l'état du trafic routier en France en temps réel.
> [...]
> Pour apporter une information plus précise, « Tipi » s'appuie sur un système d'information géographique destiné à géolocaliser les événements sur les réseaux routiers. Capgemini utilise OpenLayers et Mapserver, et s'appuie exclusivement sur les normes libres OGC (Open Geospatial Consortium) pour la gestion des données cartographiques. « Tipi » s'appuie sur une intégration d'outils open source qui permettent de localiser automatiquement les événements survenus sur le réseau routier en les affichant du bon côté de la route et d'obtenir une visualisation synthétique de leur impact sur le trafic routier.
