---
site: lemonde.fr
title: "Mozilla annonce le milliardième téléchargement de son navigateur Firefox "
author: La rédaction
date: 2009-07-31
href: http://www.lemonde.fr/technologies/article/2009/07/31/mozilla-annonce-le-milliardieme-telechargement-de-son-navigateur-firefox_1224840_651865.html
tags:
- Le Logiciel Libre
- Interopérabilité
---

> Mozilla a dévoilé en juin la dernière version de son navigateur, Firefox 3.5, affirmant qu'il était plus de deux fois plus rapide que Firefox 3 et dix fois plus rapide que Firefox 2.
> Selon la société spécialisée Net Applications, Internet Explorer se taille 65 % de parts de marché en matière de navigateurs Internet, suivi par Firefox avec 22 %, le navigateur Safari d'Apple avec environ 8 % et Chrome (Google) avec environ 2 %.
