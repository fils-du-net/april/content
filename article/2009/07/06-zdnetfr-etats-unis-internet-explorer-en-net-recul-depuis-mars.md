---
site: zdnet.fr
title: "Etats-Unis : Internet Explorer en net recul depuis mars"
date: 2009-07-06
href: http://www.zdnet.fr/actualites/internet/0,39020774,39701303,00.htm
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
---

> Les dernières statistiques d’audience révèlent que depuis la sortie d’Internet Explorer 8 en mars, le navigateur de Microsoft, toutes versions confondues, a cédé du terrain face à la concurrence : un recul de 11,4 points de part de marché.
