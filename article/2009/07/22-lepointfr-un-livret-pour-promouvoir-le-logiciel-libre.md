---
site: lepoint.fr
title: "Un livret pour promouvoir le logiciel libre"
author: Guerric Poncet
date: 2009-07-22
href: http://www.lepoint.fr/actualites-technologie-internet/2009-07-22/initiative-un-livret-pour-promouvoir-le-logiciel-libre/1387/0/363377
tags:
- Le Logiciel Libre
- Sensibilisation
- Philosophie GNU
---

> Le monde du logiciel libre se réunit chaque année lors des Rencontres mondiales du logiciel libre (RMLL). Organisées en 2008 par l'association Landinux , elles ont attiré plus de quatre mille personnes à Mont-de-Marsan, dans les Landes. "Suite à cet événement, j'ai souhaité donner un petit coup de pouce à l'association Landinux et je l'ai vivement encouragé à proposer au Conseil général des Landes une action à destination du grand public, qui sortirait des limites administratives du département", explique Henri Emmanuelli, président du Conseil général des Landes. Une demande qui n'est pas tombée dans l'oreille d'un sourd, puisque l'association lance ce jeudi 23 juillet 2009 un livret, "Sur la route du logiciel libre". À destination du grand public et à vocation nationale, le guide est une introduction aux logiciels libres. Il présente les plus connus (Firefox, OpenOffice, VLC, etc.), ainsi que des logiciels moins populaires mais tout aussi utiles.
> Le lien direct vers le livret : livret.pdf
