---
site: progilibre.com
title: "Open Source : 2 forums en octore, sans doute un trop !"
author: Philippe Nieuwbourg
date: 2009-07-21
href: http://www.progilibre.com/Open-Source-2-forums-en-octore,-sans-doute-un-trop-_a906.html
tags:
- Le Logiciel Libre
- Entreprise
- Administration
---

> Du rififi sur fond de grandes SSII du logiciel libre en France... La crise actuelle, même si elle touche moins le logiciel libre que les autres secteurs de l'informatique, aiguise la concurrence entre les membres de la communauté. En effet, même si chacun se drappe dans la toge de l'ouverture et de la liberté, l'écosystème de l'open source n’est reste pas moins une facette de notre société capitaliste. Chacun, au-delà de son investissement personnel dans une communauté, est toujours en parallèle soumis à des contraintes économiques.
