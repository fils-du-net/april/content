---
site: LeMagIT
title: "Opinion : Google Chrome OS, le Minitel des années 2000  "
author: Christophe Bardy
date: 2009-07-09
href: http://www.lemagit.fr/article/google-linux-systeme-exploitation-chrome-os/3787/1/opinion-google-chrome-minitel-des-annees-2000/
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Sensibilisation
- Vente liée
- Informatique-deloyale
- Innovation
- Informatique en nuage
---

> Finalement ce ne sera pas Google Linux mais Chrome OS, Google ayant pris soin, en annonçant hier son propre OS, de se distancier de Linux. Et que sera cet OS ? En fait rien d'autre qu'un noyau Linux et une couche graphique limitée à la seule exécution du navigateur maison, Chrome. Une sorte de Minitel des années 2000, dont la religion sera celle des applications Web. Pas de quoi rendre l'informatique meilleure ou plus intelligente...
