---
site: "Siné-Hebdo"
title: "Il est urgent de mettre de l'éthique dans les TIC"
author: Héléne Caubel/Laurent Vannini
date: 2009-07-01
href: http://www.sinehebdo.eu/
tags:
- Le Logiciel Libre
- April
---

> Double page sur les TIC, comportant plusieurs articles:
> - "La fracture se réduit, la facture augmente": "dans les pays développés, les foyers sont sur-consommateurs de nouvelles technologies. Parfois au détriment du budget consacré à l'alimentation"
> - "Toile à gratter": "Le Net dévoile des petits secrets jusque-là bien gardés. Mais la répression rôde." Référence, entre autre, à la loi Hadopi.
