---
site: zdnet.fr
title: "Canonical libère le code source de Launchpad"
author: Bernard Opic
date: 2009-07-21
href: http://www.zdnet.fr/blogs/ubuntu-co/canonical-libere-le-code-source-de-launchpad-39702546.htm
tags:
- Le Logiciel Libre
- Internet
- Partage du savoir
---

> La société Canonical à l'origine du projet Ubuntu a annoncé aujourd'hui qu'elle avait ouvert le code source de Launchpad, sa plateforme de développement collaborative utilisée par des dizaines de milliers de développeurs.
