---
site: PC INpact
title: "W3C : les balises audio et video vidées de leur substance"
author: Vincent Hermann
date: 2009-07-07
href: http://www.pcinpact.com/actu/news/51839-html-video-audio-balises-w3c.htm
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Interopérabilité
- Institutions
- Standards
- Video
---

> Contrairement à toutes les prévisions sur le sujet, le W3C taille dans le vif concernant des projets qui étaient en cours. Plus précisément, les balises audio et video sont abandonnées, de même que le XHTML 2 en tant que tel. Cependant, le premier abandon est une vraie disparition, au moins momentanée, tandis que la seconde est en fait un transfert.
