---
site: zdnet.fr
title: "RMLL 2009: programmes étoffés et affluence pour le logiciel libre à Nantes"
author: Thierry Noisette 
date: 2009-07-10
href: http://www.zdnet.fr/blogs/l-esprit-libre/rmll-2009-programmes-etoffes-et-affluence-pour-le-logiciel-libre-a-nantes-39701780.htm
tags:
- Le Logiciel Libre
- April
- Institutions
---

> La richesse et la variété des thèmes couverts - plus de 300 conférences, tables rondes et ateliers, dont le tiers en anglais, par 200 conférenciers - montrent l'ampleur prise par les logiciels libres: sciences, création graphique et impression, accessibilité et handicap, systèmes et sécurité, culture et art libre, santé, jeux (le poker est bien représenté cette année :-)), éducation, entreprises, économie sociale et solidaire, etc.
