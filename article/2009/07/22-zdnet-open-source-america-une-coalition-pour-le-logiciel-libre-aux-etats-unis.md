---
site: ZDNet
title: "Open Source for America, une coalition pour le logiciel libre aux Etats-Unis"
author: Thierry Noisette
date: 2009-07-22
href: http://www.zdnet.fr/blogs/l-esprit-libre/open-source-for-america-une-coalition-pour-le-logiciel-libre-aux-etats-unis-39702713.htm
tags:
- Le Logiciel Libre
- Entreprise
- Administration
- Sensibilisation
---

> Réunissant 70 entreprises et organismes, Open Source for America (OSFA) est officiellement lancé aujourd'hui. La coalition, qui refuse l'étiquette de lobby, veut faire prendre conscience au gouvernement fédéral des Etats-Unis des bénéfices des logiciels open source, et l'encourager à leur usage.
> [...]
> Les initiateurs d'OSFA pensent que « le moment est venu de lancer une nouvelle coalition pour aider nos nouveaux dirigeants technologiques à comprendre les logiciels open source ». Leur FAQ précise au passage que pour eux, "open source" ou "logiciel libre" sont équivalents pour des logiciels qui respectent les principes de la Free Software Foundation et de l'Open Source Initiative.
