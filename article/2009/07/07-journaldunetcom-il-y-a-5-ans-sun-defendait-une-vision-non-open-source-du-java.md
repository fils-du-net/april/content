---
site: Journal du Net
title: "Il y a 5 ans : Sun défendait une vision non Open Source du Java"
author: Antoine CROCHET-DAMAIS
date: 2009-07-07
href: http://www.journaldunet.com/solutions/acteurs/actualite/il-y-a-5-ans-sun-defendait-une-vision-non-open-source-du-java.shtml
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Économie
- Interopérabilité
- Partage du savoir
- Innovation
- Philosophie GNU
- Promotion
---

> Le débat avait été lancé en février 2004 par IBM. Dans une lettre ouverte à Sun, Big Blue invitait ce dernier à ouvrir le code source de Java, en se disant prêt à l'accompagner dans la démarche. Raison invoquée par le groupe : une telle stratégie contribuerait à dynamiser les processus de développement de la communauté Java, et indirectement à accélérer la standardisation de la plate-forme J2EE (Java 2 Enterprise Edition).
