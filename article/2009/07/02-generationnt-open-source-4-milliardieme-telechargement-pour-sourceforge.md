---
site: GenerationNT
title: "Open source : 4 milliardième téléchargement pour SourceForge"
author: Jérôme G.
date: 2009-07-02
href: http://www.generation-nt.com/sourceforge-nombre-telechargement-open-source-actualite-828881.html
tags:
- Le Logiciel Libre
- Internet
---

> La célèbre forge de logiciels open source a récemment livré son 4 milliardième téléchargement.
