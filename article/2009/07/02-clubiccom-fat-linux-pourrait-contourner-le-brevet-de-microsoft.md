---
site: Clubic
title: "FAT : Linux pourrait contourner le brevet de Microsoft"
author: Guillaume Belfiore
date: 2009-07-02
href: http://www.clubic.com/actualite-285968-fat-linux-contourner-brevet-microsoft.html
tags:
- Logiciels privateurs
- Brevets logiciels
- Innovation
---

> Au mois d'avril dernier, Jim Zemlin, le directeur de la Fondation Linux avait publié un message dans lequel il invitait la communauté a développer une alternative au système de fichiers propriétaire FAT de Microsoft développé par Bill Gates et Marc McDonald en 1976. Cette annonce fit suite à l'affaire opposant le fabricant de GPS TomTom à Microsoft, ce dernier reprochant au développeur néerlandais d'avoir repris son système de fichiers FAT en violant les brevets associés.
> [...]
> Depuis les choses ont quelque peu évolué et le magazine Ars Technica rapporte que le développeur Andrew Tridgell a mis au point un patch modifiant le fonctionnement du système de fichiers FAT afin qu'il ne génère pas automatiquement à la fois un nom de fichier court et un long. Lorsque le nom du fichier ne dépasse pas les 11 caractères, seul un nom court sera créé. Si le nom du fichier en question comporte plus de 11 caractères, seul un nom long sera créé, le nom court présentera des signes invalides non reconnus par le système d'exploitation.
