---
site: lepoint.fr
title: "Google déclare la guerre à Microsoft"
author: Clémzent Pétreault
date: 2009-07-20
href: http://www.lepoint.fr/actualites-economie/2009-07-16/google-declare-la-guerre-a-microsoft/916/0/361853
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
- Informatique en nuage
---

> D’autant que tout ne joue pas en faveur de Google. Le grand public est frileux dès qu'il s’agit d’adopter un nouveau système d’exploitation, même gratuit. L’échec commercial des netbooks tournant sous Linux l'a prouvé. La concentration d’informations personnelles ou stratégiques dans le nuage informatique de Google pourrait aussi constituer un frein. Car Google peut faire tout aussi peur aux internautes que Microsoft.
