---
site: zdnet.fr
title: "Le chômage des informaticiens a progressé de 40% en un an"
author: Christophe Auffray
date: 2009-07-30
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39703370,00.htm
tags:
- Entreprise
- Économie
---

> L'informatique, contrairement au reste de l'économie, n'a pas connu de recul technique du chômage en juin. En un an, le nombre de demandeurs d'emploi parmi les informaticiens a progressé de 40%.
> En France le taux de chômage a explosé depuis le début de la crise, mais certains secteurs comme l'informatique sont plus durement frappés comme le démontrent les derniers chiffres publiés par la Dares.
