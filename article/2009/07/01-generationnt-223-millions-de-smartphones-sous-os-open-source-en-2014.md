---
site: "generation-nt.com"
title: "223 millions de smartphones sous OS open source en 2014 ?"
author: Christian D.
date: 2009-07-01
href: http://www.generation-nt.com/juniper-research-etude-smartphone-open-source-prevision-actualite-828561.html
tags:
- Le Logiciel Libre
- Entreprise
- Interopérabilité
- Partage du savoir
- Innovation
---

> C'est en tous les cas l'ordre de grandeur des ventes estimées par le cabinet d'études Juniper Research. Les OS open source auraient-ils enfin trouvé leur voie ?
