---
site: lesechos.fr
title: "Israël-Palestine : un fantôme high-tech traverse le mur"
author: Jean-Claude Hazera
date: 2009-07-16
href: http://www.lesechos.fr/info/hightec/02067974665-israel-palestine-un-fantome-high-tech-traverse-le-mur.htm
tags:
- Le Logiciel Libre
- Interopérabilité
- Institutions
---

> [...]
> Distantes géographiquement d'une vingtaine de kilomètres, elles se parlent en permanence par liaison vidéo et les 35 Palestiniens qui ont fait le gros du développement sont actionnaires. Le nom de la société n'est pas un hasard. Les fantômes (« ghosts » en anglais) traversent les murs sans entrave, comme chacun sait. Mais G.ho.st signifie officiellement « global hosted operating system ». « Pas de mur », peut-on lire sur le site. Mais il ne s'agit pas vraiment d'une déclaration politique. Le logiciel libre d'accès (qui espère se rémunérer plus tard sur le stockage des données et la publicité) veut libérer l'internaute de son PC personnel. Il lui permet de se connecter de n'importe où sur le Web, en toute liberté, « sans murs », à un PC virtuel qui a pour ambition de lui rendre les mêmes services que le sien s'il le transportait partout.
