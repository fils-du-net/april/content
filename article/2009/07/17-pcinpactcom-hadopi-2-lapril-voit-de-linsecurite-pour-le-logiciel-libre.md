---
site: pcinpact.com
title: "Hadopi 2 : l'April voit de l'insécurité pour le logiciel libre"
author: Marc Rees
date: 2009-07-17
href: http://www.pcinpact.com/actu/news/52021-hadopi-insecurite-logiciel-libre-mouchard.htm
tags:
- Le Logiciel Libre
- April
- HADOPI
---

> Comme pour Hadopi 1, l'April remonte au créneau à la lecture du texte Hadopi 2 amendé par la Commission des affaires culturelles de l'Assemblée Nationale.
> L’association pour la défense du logiciel libre vient d’envoyer un courrier aux parlementaires signataires du Pacte du Logiciel libre pour leur signaler les risques que soulève ce texte pour l'avenir de ce secteur économique.
