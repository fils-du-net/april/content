---
site: numerama.com
title: "L'ancien patron du SNEP reconnaît des \"résistances démocratiques\""
author: Guillaume Champeau
date: 2009-07-21
href: http://www.numerama.com/magazine/13504-L-ancien-patron-du-SNEP-reconnait-des-34resistances-democratiques34.html
tags:
- HADOPI
- DADVSI
- DRM
- Droit d'auteur
---

> Ancien président du SNEP, le lobby des majors de l'industrie du disque en France, Gilles Bressand dit comprendre aujourd'hui que les résistances à la vision répressive du téléchargement sont de nature "démocratiques". Mais le repenti cache une stratégie : après le bâton, l'aumône.
> [...]
> "En terme de résistance, il ne s’agit pas ici de lobbys, d’aficionados pro-libertaires de l’Internet, ou de fournisseurs d’accès aussi rétifs à appliquer Hadopi qu’à reconnaître leur responsabilité passive dans le pillage de oeuvres, mais de trois instances emblématiques du pacte social : les assemblées du peuple d’un côté, le gardien de la constitution de l’autre. Il serait déraisonnable de ne pas tenter de comprendre la portée de ces résistances démocratiques".
