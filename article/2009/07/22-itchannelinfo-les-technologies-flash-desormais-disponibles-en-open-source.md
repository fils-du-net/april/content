---
site: itchannel.info
title: "Les technologies Flash désormais disponibles en Open Source"
author: La rédaction
date: 2009-07-22
href: http://www.itchannel.info/articles/93703/technologies-flash-desormais-disponibles-open-source.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Interopérabilité
---

> Adobe a décidé de proposer en Open Source les frameworks Media et Text de la plate-forme Flash afin de faciliter la création de lecteurs multimédias et la présentation de texte. Cette nouvelle initiative Open Source d'Adobe Flash Platform est destinée aux développeurs ainsi qu'aux sociétés spécialisées dans le multimédia et aux éditeurs.
