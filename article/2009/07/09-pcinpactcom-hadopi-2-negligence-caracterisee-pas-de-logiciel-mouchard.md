---
site: pcinpact.com
title: "Hadopi 2 : négligence caractérisée = pas de logiciel mouchard"
author: Marc Rees
date: 2009-07-09
href: http://www.pcinpact.com/actu/news/51874-frederic-mitterrand-negligence-caracterisee-mouchard.htm
tags:
- Le Logiciel Libre
- HADOPI
---

> La négligence sera d'utiliser Linux
> Hier la sénatrice Mme Marie-Christine Blandin expliqua : « la non-négligence, c'est utiliser un certain logiciel, un certain type de pare-feu et d'antivirus. Demain, la négligence sera d'utiliser Linux ou tout logiciel libre autre que ceux d'une grande multinationale ! » Pour faire bonne mesure, Franck Riester, rapporteur à l’Assemblée nationale et Christine Albanel s’étaient en effet opposés à ce que ce logiciel de sécurisation soit impérativement gratuit et interopérable (notre actualité). Si vous avez une vieille version de Windows ou une distribution Linux un peu trop exotique avec laquelle le logiciel de sécurisation n’est pas interopérable, vous n’aurez aucun moyen d’échapper aux mâchoires de cette procédure et à la suspension éventuelle plombée de 1500 euros d'amende. Merci Hadopi.
