---
site: vnagency.com.vn
title: "Les logiciels libres au Vietnam : leur utilisation actuelle et perspectives"
author: AUF/CVN
date: 2009-07-12
href: http://lecourrier.vnagency.com.vn/default.asp?xt=XT34&page=newsdetail&newsid=54030
tags:
- Le Logiciel Libre
- Administration
- Sensibilisation
---

> Le responsable du Campus numérique francophone de Hanoi, également membre du groupe des utilisateurs hanoïens de GNU/Linux, Vu Dô Quynh, a fait une présentation intitulée "Les logiciels libres au Vietnam : leur utilisation actuelle et perspectives", par visioconférence, le mercredi 8 juillet. La présentation s'est faite au Vietnam dans l'amphithéâtre de l'Institut de la Francophonie pour l'informatique (IFI), à Hanoi.
