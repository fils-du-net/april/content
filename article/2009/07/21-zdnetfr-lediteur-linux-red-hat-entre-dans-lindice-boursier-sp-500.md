---
site: zdnet.fr
title: "L'éditeur Linux Red Hat entre dans l'indice boursier S&amp;P 500"
author: Thierry Noisette 
date: 2009-07-21
href: http://www.zdnet.fr/blogs/l-esprit-libre/l-editeur-linux-red-hat-entre-dans-l-indice-boursier-s-p-500-39702583.htm
tags:
- Entreprise
- Économie
---

> Signe des temps... et de sa bonne santé, Red Hat intègrera vendredi 24 juillet l'indice boursier S&amp;P 500. Cette distinction qui salue la croissance continue, malgré la crise économique, de l'éditeur spécialiste de Linux, est un signe de reconnaissance pour une des principales entreprises de logiciels libres.
