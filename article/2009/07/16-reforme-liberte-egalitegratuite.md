---
site: Réforme
title: "Liberté, égalité...gratuité"
author: Marie Lefebvre-Billiez
date: 2009-07-16
href: http://www.reformevirtuel.net/journal/redirection.php?numa=4095&titre=Le%20mirage%20de%20la%20gratuit%C3%A9
tags:
- Le Logiciel Libre
---

> 
> «Nous sommes dans le vrai monde. Les développeurs, au bout du
> compte, doivent bien manger » reconnaît Véronique Lefiot,
> administratrice de l'Association francophone des utilisateurs de
> Linux (AFUL). 
> 
