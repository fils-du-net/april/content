---
site: PC INpact
title: "Mono : la « promesse » de Microsoft de ne pas armer ses brevets"
author: Vincent Hermann
date: 2009-07-07
href: http://www.pcinpact.com/actu/news/51825-microsoft-mono-icaza-community-promise.htm
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Interopérabilité
- Brevets logiciels
- Innovation
---

> En 2003, Novell rachète la société Ximian, et avec elle un projet initié par Miguel di Icaza : Mono. Ce dernier est une implémentation libre de l’environnement .NET de Microsoft, principalement marqué par l’utilisation de code managé et d’un Runtime, c’est-à-dire un concurrent direct de Java. Mono étant directement lié à cet environnement .NET, de très nombreuses questions ainsi que des doutes persistants sont liés aux brevets qui se tiennent derrière chez Microsoft. L’éditeur de Redmond vient justement de faire un pas vers un premier éclaircissement.
