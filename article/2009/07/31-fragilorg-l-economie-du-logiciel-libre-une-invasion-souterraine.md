---
site: fragil.org
title: "L’économie du logiciel libre, une invasion souterraine"
author: Timothée Blit
date: 2009-07-31
href: http://www.fragil.org/focus/1236
tags:
- Le Logiciel Libre
- Entreprise
- Économie
---

> Le pingouin de Linux peut avoir le sourire. Le logiciel libre n’est plus seulement une philosophie, mais aussi une économie florissante. En dix ans, le libre a infiltré l'économie de services en ingénierie informatique et joue désormais sur le même terrain que les logiciels propriétaires.
