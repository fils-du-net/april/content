---
site: numerama.com
title: "L'APRIL dénonce un acharnement du gouvernement contre le logiciel libre"
author: Guillaume Champeau
date: 2009-07-22
href: http://www.numerama.com/magazine/13524-L-APRIL-denonce-un-acharnement-du-gouvernement-contre-le-logiciel-libre.html
tags:
- Le Logiciel Libre
- April
- HADOPI
---

> L'association de défense du logiciel libre April a dénoncé mercredi un "acharnement aveugle" du gouvernement contre le logiciel libre. Elle estime que le rapporteur Frank Riester et le ministre de la Culture Frédéric Mitterrand nient les contenus sous licences libres et les logiciels libres, et "persistent dans la discrimination en soutenant une vision profondément propriétaire des ressources numériques".
