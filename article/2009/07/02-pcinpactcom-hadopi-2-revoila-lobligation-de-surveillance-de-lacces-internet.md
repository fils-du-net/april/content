---
site: pcinpact.com
title: "Hadopi 2: revoilà l'obligation de surveillance de l'accès internet"
author: Marc Rees
date: 2009-07-02
href: http://www.pcinpact.com/actu/news/51759-hadopi-surveillance-negligence-caracterisee-senat.htm
tags:
- Logiciels privateurs
- Interopérabilité
- HADOPI
---

> La Commission des affaires culturelles vient de rendre sa copie. Elle a amendé à plusieurs endroits le projet de loi pénale contre le piratage. Surprise : alors que Hadopi 2 se concentrait sur la contrefaçon, la Commission réintègre le délit de négligence dans la sécurisation de la ligne internet.
