---
site: ZDNet.fr
title: "Kindle : Amazon écope d’un procès pour avoir effacé des livres à distance"
author: La rédaction
date: 2009-07-31
href: http://www.zdnet.fr/actualites/kindle-amazon-ecope-d-un-proces-pour-avoir-efface-des-livres-a-distance-39703585.htm
tags:
- DRM
- Droit d'auteur
- Informatique-deloyale
---

> En supprimant 1984, l’ouvrage d’Orwell, des Kindle, Amazon aurait également supprimé le projet scolaire d’un lycéen américain, qui a déposé plainte en action collective, avant tout pour prévenir toute récidive d'Amazon.
