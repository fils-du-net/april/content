---
site: zdnet.fr
title: "Six projets Open Source récompensés sur SourceForge"
author: Christophe Auffray
date: 2009-07-27
href: http://www.zdnet.fr/galerie-image/0,50018840,39703054,00.htm
tags:
- Le Logiciel Libre
- Sensibilisation
---

> Technologie - phpMyAdmin, PortableApps, OpenOffice, eeebuntu, ScummVm, Firebird : SourceForge a révélé le noms des projets Open Source gagnants de la 4e édition de son concours. Présentation de 6 des logiciels primés.
