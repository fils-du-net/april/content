---
site: ZDNet
title: "Microsoft ouvre-t-il du code sous GPL pour éviter un procès ?"
author: Christophe Auffray
date: 2009-07-24
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39702808,00.htm
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Interopérabilité
- Partage du savoir
- Brevets logiciels
- Licenses
- Philosophie GNU
---

> Lors de la convention OSCON, Microsoft a annoncé ouvrir à Linux le code de plusieurs pilotes. Selon le développeur Stephen Hemminger, cette annonce cherche à masquer le fait que l’éditeur violerait la licence GPL en utilisant du code Open source avec son code propriétaire. Et si la décision de Microsoft de mettre à disposition de la communauté Open Source le code de différents pilotes tenait plus à la crainte de l'éditeur d'être poursuivi pour violation de la licence GPL qu'à sa volonté d'ouverture ?
