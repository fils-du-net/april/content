---
site: zdnet.fr
title: "Logiciel libre : la licence GPLv3 gagne du terrain"
author: Christophe Auffray
date: 2009-07-01
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39700957,00.htm
tags:
- Le Logiciel Libre
- Licenses
---

> Une étude de Black Duck Software souligne l'adoption croissante de la version 3 de la licence GPL qui représente désormais 5,1% des licences Open Source utilisées en 2009.
