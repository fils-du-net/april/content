---
site: pcinpact.com
title: "Hadopi 2 version Assemblée nationale, c’est de pire en pire"
author: Marc Rees
date: 2009-07-17
href: http://www.pcinpact.com/actu/news/52017-hadopi-suspension-riester-assemblee-nationale.htm
tags:
- Logiciels privateurs
- HADOPI
- Informatique-deloyale
---

> Légalisation du mouchard
> Le mouchard est légalisé. Quand un abonné est soupçonné d’infraction de négligence caractérisée sur sa ligne, il recevra un email puis une lettre recommandée l’invitant fortement à installer un moyen de sécurisation de son accès à internet. C’est le fameux mouchard qui sera payant, non interopérable et à code fermé. Les Linuxiens et autres détenteurs d'OS non Redmondiens devront croiser des doigts.
