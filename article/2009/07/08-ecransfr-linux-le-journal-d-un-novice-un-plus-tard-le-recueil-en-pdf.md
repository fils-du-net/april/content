---
site: ecrans.fr
title: "Linux : Le journal d’un novice, un an plus tard, le recueil en pdf"
author: Erwan Cario 
date: 2009-07-08
href: http://www.ecrans.fr/Linux-Le-journal-d-un-novice-un-an,7672.html
tags:
- Le Logiciel Libre
- Internet
- Sensibilisation
---

> Drôle d’histoire que celle de cette petite série d’été. Tout a commencé avec la commande d’un ordinateur portable destiné principalement à surfer sur Internet. Ça faisait quelques temps déjà que je voulais m’essayer à Linux, j’ai donc choisi de me lancer à cette occasion. Au moment d’ouvrir le carton, je me suis dit que ce serait rigolo à raconter, cette découverte.
