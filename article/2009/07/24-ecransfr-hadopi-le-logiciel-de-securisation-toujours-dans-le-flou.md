---
site: ecrans.fr
title: "Hadopi : Le logiciel de sécurisation toujours dans le flou"
author: Camille Gévaudan
date: 2009-07-24
href: http://www.ecrans.fr/Hadopi-flou-artistique-sur-le,7811.html
tags:
- Le Logiciel Libre
- Interopérabilité
- HADOPI
---

> [...]
> D’autre part, l'interopérabilité. Jean-Pierre Brard a demandé à plusieurs reprises si le logiciel pourra être installé sur tous les systèmes d’exploitation que peuvent posséder les abonnés. Ce qui épargnerait aux utilisateurs de logiciels libres, par exemple, d’avoir à choisir entre acheter une licence Windows et renoncer à la possibilité de se défendre.
> L’interopérabilité sera « une mission » de l'Hadopi mais elle ne lui sera pas imposée, répond Riester. Plein de bons sentiments, il est persuadé que les FAI « feront tout ce qu'ils pourront à cette fin », selon les propos rapportés par PC INpact.
