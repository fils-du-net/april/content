---
site: ZDNet
title: "Richard Stallman aux RMLL 2009: \"Attaquer le partage c'est attaquer la société\""
author: Thierry Noisette
date: 2009-07-12
href: http://www.zdnet.fr/blogs/l-esprit-libre/richard-stallman-aux-rmll-2009-attaquer-le-partage-c-est-attaquer-la-societe-39701873.htm?xtor=RSS-8
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Partage du savoir
- HADOPI
- Philosophie GNU
- Promotion
---

> Pour inaugurer la journée grand public des Rencontres mondiales du logiciel libre (RMLL) 2009, hier à Nantes, Richard Stallman a donné une conférence suivie par un bon demi-millier de personnes. L'occasion de rappeler les principes des logiciels libres, de jeter un coup de griffe à Sarkozy et Hadopi, et quelques gouttes d'acide à Linus Torvalds et aux tenants de l'"open source".
