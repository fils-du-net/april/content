---
site: Numerama.com
title: "Le W3C ne désignera pas de codec Vidéo et Audio pour HTML 5"
author: Guillaume Champeau
date: 2009-07-02
href: http://www.numerama.com/magazine/13352-Le-W3C-ne-designera-pas-de-codec-Video-et-Audio-pour-HTML-5.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Interopérabilité
- Institutions
- Brevets logiciels
- Standards
- Video
---

> C'est un coup dur porté à ceux qui espèrent voir se développer rapidement un standard ouvert pour la lecture des vidéos et de la musique sur les navigateurs Internet. Le Worldwide Web Consortium (W3C) a annoncé qu'il supprimait des spécifications du HTML 5 les clauses qui devaient désigner les codecs que doivent supporter les navigateurs internet. La guerre des formats va continuer jusqu'à ce qu'un standard s'impose de fait, ce qui pourra prendre de nombreuses années.
