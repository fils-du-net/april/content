---
site: lemensuel.net
title: "Des logiciels libres pour une finance sociale et solidaire"
author: Louis Martin
date: 2009-07-07
href: http://www.lemensuel.net/2009/07/07/des-logiciels-libres-pour-une-finance-sociale-et-solidaire-2/
tags:
- Le Logiciel Libre
- Économie
- Institutions
- Associations
---

> La crise économique mondiale actuelle est un moment propice pour explorer d’autres modes d’organisation des échanges financiers. L’économie sociale, s’appuyant sur les technologies de l’information et des communications (TIC), peut constituer une alternative valable en s’appuyant sur des logiciels libres de qualité prenant en compte les normes internationales les plus rigoureuses du domaine.
