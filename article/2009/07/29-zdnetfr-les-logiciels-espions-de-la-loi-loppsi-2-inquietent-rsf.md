---
site: zdnet.fr
title: "Les logiciels espions de la loi Loppsi 2 inquiètent RSF"
author: Christophe Auffray
date: 2009-07-29
href: http://www.zdnet.fr/actualites/internet/0,39020774,39703225,00.htm
tags:
- Logiciels privateurs
- Informatique-deloyale
---

> Reporters sans frontières, qui a pris connaissance de l'avis remis par la CNIL, s'inquiète du projet de loi Loppsi II du ministère de l'intérieur et des possibles atteintes aux libertés qu'il pourrait introduire. Un point en particulier suscite l'inquiétude de l'association : la captation de données par des logiciels espions.
> Reporters sans frontière craint en effet que ces mécanismes d'écoute informatique puissent être utilisés à l'encontre des journalistes. « Nous redoutons une utilisation excessive de ce système d'espionnage par la police, qui pourrait mettre en danger la protection des sources journalistiques. Le cadre de mise en oeuvre de la captation des données informatiques doit être plus clairement défini. Nous demandons aux parlementaires de présenter des amendements pour mieux encadrer ce projet » peut-on lire sur son site Internet.
