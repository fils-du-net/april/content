---
site: romandie.com
title: "Le plus puissant super-ordinateur du Japon dédié à la simulation nucléaire "
author: AFP
date: 2009-07-16
href: http://www.romandie.com/infos/news2/090716070944.nm48xmcz.asp
tags:
- Le Logiciel Libre
- Innovation
- Sciences
---

> [...]
> Fujitsu a indiqué jeudi avoir reçu de la part de cet organisme public de recherche une commande portant sur un système constitué de plus de 2.000 unités centrales de serveurs sous système d'exploitation libre Linux, comprenant au total plus de 4.000 processeurs multi-coeurs (plusieurs unités de calcul pour chacun). Le tout, contrôlé d'un seul tenant, offre une capacité totale de calcul de 200 teraflops (200.000 milliards d'opérations à virgule flottante par seconde).
