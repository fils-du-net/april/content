---
site: programmez.com
title: "Richard Stallman a une dent bien aiguisée contre C#"
author: Frédéric Mazué
date: 2009-07-06
href: http://www.programmez.com/actualites.php?titre_actu=Richard-Stallman-a-une-dent-bien-aiguisee-contre-C&id_actu=5414
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Interopérabilité
- Brevets logiciels
---

> La décision de l'équipe Debian d'inclure Mono pour le fonctionnement de Tomboy, qui est une application écrite en C#, entraine la communauté dans une direction risquée. Il est dangereux de dépendre de C# et nous devons décourager son utilisation. Le problème n'est pas unique à Mono mais commun à toute implémentation de C#. Le danger est que Microsoft projette probablement de soumettre un jour ou l'autre toutes les implémentations libres de C# aux brevets logiciels. C'est un danger sérieux que seuls les inconscients ignoreront jusqu'a jour où ils rencontreront le problème.
