---
site: lemagit.fr
title: "Microsoft publie le code de pilote Linux pour Hyper-V en licence GPL "
author: Christophe Bardy
date: 2009-07-21
href: http://www.lemagit.fr/article/microsoft-virtualisation-linux-hyper-v-open-source/3850/1/microsoft-publie-code-pilote-linux-pour-hyper-licence-gpl/
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Interopérabilité
- Partage du savoir
- Innovation
---

> Microsoft a pris prétexte de l'Open Source Convention qui s'est ouverte hier à San José pour annoncer l'arrivée prochaine de pilotes sous licence GPL permettant aux distributions Linux de fonctionner de façon optimisée sur sa plate-forme de virtualisation Hyper-V. L'adoption de la licence GPL 2, certes pour 20 000 lignes de code, est un vrai revirement pour la firme dont le fondateur avait, il n'y a pas si longtemps assimilé les principes aux communisme.
