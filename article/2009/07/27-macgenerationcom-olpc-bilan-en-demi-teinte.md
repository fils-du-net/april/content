---
site: macgeneration.com
title: "OLPC : bilan en demi-teinte"
author: Arnauld de La Grandière
date: 2009-07-27
href: http://www.macgeneration.com/unes/voir/127310/olpc-bilan-en-demi-teinte/2
tags:
- Le Logiciel Libre
- Institutions
- Accessibilité
- Éducation
---

> Quatre ans après le lancement de l'initiative One Laptop Per Child (OLPC, un portable par enfant), qui avait pour vocation de proposer un ordinateur portable pour moins de $100 aux enfants du tiers monde, son instigateur, Nicholas Negroponte, en fait un bilan en demi-teinte.
> [...]
> Côté logiciel, il est livré avec Linux, et utilise Sugar comme environnement de bureau. Negroponte formule quelques regrets vis-à-vis de ce choix : «La plus grosse erreur était de ne pas avoir fait Sugar OS en tant qu'application sur un environnement Linux classique. Au lieu de cela nous avons utilisé Sugar pour gérer la batterie et les réseaux sans fil... c'est devenu une sorte d'omelette.
