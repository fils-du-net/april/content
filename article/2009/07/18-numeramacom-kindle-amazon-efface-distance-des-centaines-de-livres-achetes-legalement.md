---
site: Numerama.com
title: "Kindle : Amazon efface à distance des centaines de livres achetés légalement "
author: Julien L
date: 2009-07-18
href: http://www.numerama.com/magazine/13484-kindle-amazon-efface-a-distance-des-centaines-de-livres-achetes-legalement-maj.html
tags:
- DRM
- Droit d'auteur
- Informatique-deloyale
---

> Amazon a supprimé des centaines de livres achetés légalement sur son site. À l'origine de cette décision se trouve le revirement de l'éditeur, MobileReference, qui a changé d'avis sur les versions électroniques de certains romans. Cette mesure inattendue n'incitera certainement pas les internautes à se porter massivement vers les offres légales si les éditeurs peuvent ordonner la suppression des contenus achetés légalement.
