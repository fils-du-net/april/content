---
site: EducPros.fr
title: "L'université Pierre Mendès-France adopte les logiciels libres"
author: Ludivine Coste
date: 2009-07-22
href: http://www.educpros.fr/detail-article/h/8bc43e65f4/a/l-universite-pierre-mendes-france-adopte-les-logiciels-libres.html
tags:
- Logiciels privateurs
- Administration
- April
- Éducation
---

> L'article revient sur l'adhésion de l'UPMF à l'April, qui date de fin janvier.
