---
site: zdnet.fr
title: "Bruxelles pourrait infliger une amende record à Intel"
author: ZDNet France
date: 2009-05-11
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39501250,00.htm
tags:
- Entreprise
- Interopérabilité
- Vente liée
---

> [...]
> Ce dernier est accusé d'avoir payé des fabricants de PC pour qu'ils n'utilisent pas les puces d'AMD et consenti des rabais aux distributeurs privilégiant les produits Intel.
> Selon le dispositif légal, l'Europe peut infliger une amende égale à 10% du chiffre d'affaires réalisé l'année précédent le verdict. Dans le cas d'Intel, dont le CA fut de 37,6 milliards de dollars, l'amende pourrait grimper à 3,7 milliards, un record historique.
