---
site: pcinpact.com
title: "Hadopi votée, Guy Bono estime « la France bientôt hors la loi »"
author: Marc Rees
date: 2009-05-12
href: http://www.pcinpact.com/actu/news/50817-hadopi-traite-guy-bono-infraction.htm
tags:
- HADOPI
- Europe
---

> Les premières réactions n’ont pas tardé suite au vote du projet de loi Hadopi. Guy Bono, père de l'amendement 138, a d’ores et déjà annoncé qu’il allait demander à la Commission européenne de lancer une procédure d'infraction contre le gouvernement français pour non-respect du droit communautaire.
