---
site: "la-croix.com"
title: "Le logiciel libre paraît-il utopique ?"
author: Stéphane DREYFUS
date: 2009-05-11
href: http://www.la-croix.com/article/index.jsp?docId=2373314&rubId=786
tags:
- Le Logiciel Libre
---

> « Non, cette philosophie s’étend et un équilibre s’installe »
> Nathalie Kosciusko-Morizet, secrétaire d’État auprès du premier ministre, chargée de la prospective et du développement de l'économie numérique
> « Le logiciel libre est d’abord une philosophie, avant d’être un standard ou un modèle économique. Je n’irais pas jusqu’à dire que c’est une religion, bien que l'on parle des commandements du logiciel libre ! Je ne dirais pas non plus qu'il s’agit d’une utopie. Évidemment, tout le monde ne sait pas modifier un logiciel, mais cela permet à tous ceux qui peuvent le faire d’avoir accès à une bibliothèque universelle de codes sources.
