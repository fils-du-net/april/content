---
site: toolinux.com
title: "Pourquoi le logiciel libre est un choix technologique stratégique"
author: Pierre Rivenez 
date: 2009-05-26
href: http://www.toolinux.com/lininfo/toolinux-information/opinion/article/pourquoi-le-logiciel-libre-est-un
tags:
- Le Logiciel Libre
- Entreprise
---

> Dans une tribune libre, l’opérateur Celeste vante les mérites du logiciel libre et donne quelques éléments d’explication pour "comprendre cette évolution, voire révolution des systèmes d’information."
