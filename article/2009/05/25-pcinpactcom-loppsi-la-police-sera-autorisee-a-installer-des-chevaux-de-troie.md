---
site: pcinpact.com
title: "LOPPSI : la police sera autorisée à installer des chevaux de Troie"
author: Marc Rees
date: 2009-05-25
href: http://www.pcinpact.com/actu/news/51027-police-opj-cheval-troie-loppsi.htm
tags:
- Internet
- HADOPI
- Informatique-deloyale
---

> Alors que le bruit autour de l’Hadopi résonne encore, le tintamarre autour de la LOPPSI se fait de plus en plus assourdissant. Ce texte va en effet organiser le filtrage des sites pédopornographiques mais également la mise en place des mouchards légaux, véritables chevaux de Troie que les OPJ pourront installer sur les machines des suspects dans le cadre d'infractions graves commises en bande organisée.
