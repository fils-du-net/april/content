---
site: tdg.ch
title: "Genève, capitale des logiciels libres "
author: Luca Sabbatini
date: 2009-05-19
href: http://www.tdg.ch/actu/hi-tech/geneve-capitale-logiciels-libres-2009-05-18
tags:
- Le Logiciel Libre
---

> Ils coûtent moins cher, fonctionnent souvent mieux que la concurrence et permettent une très grande souplesse de configuration. Les logiciels libres ont tout pour plaire. D’ailleurs, leur succès s’affirme lentement mais sûrement, aussi bien auprès des administrations que des entreprises ou des simples particuliers. A Genève, les Linuxdays viennent rappeler pendant trois jours, du 3 au 5 juin, l'étonnante diversité du monde de l'open source.
