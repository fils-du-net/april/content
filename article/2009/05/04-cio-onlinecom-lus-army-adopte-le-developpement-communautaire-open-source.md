---
site: "cio-online.com"
title: "l'US Army adopte le développement communautaire open-source"
author: CIO Etats-Unis 
date: 2009-05-04
href: http://www.cio-online.com/actualites/lire-cio-en-vo-l-us-army-adopte-le-developpement-communautaire-open-source-2185.html
tags:
- Le Logiciel Libre
- Administration
---

> Le développement communautaire open-source n'est pas un mode de travail évident pour une organisation très hiérarchisée comme l'armée. Pourtant, l'armée américaine a franchi le pas...
