---
site: programmez.com
title: "10 propositions pour relancer l'économie numérique grâce au logiciel libre"
author: Frédéric Mazué
date: 2009-05-29
href: http://www.programmez.com/actualites.php?titre_actu=10-propositions-pour-relancer-l-economie-numerique-grace-au-logiciel-libre&id_actu=5160
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- Administration
- Économie
- Interopérabilité
- Partage du savoir
- Sensibilisation
- Associations
- Éducation
- Innovation
- Philosophie GNU
- Promotion
- Standards
---

> Les entrepreneurs de PLOSS, l'association des acteurs du logiciel libre à Paris et en Ile-de-France, proposent dès à présent 10 pistes d'action pour le Gouvernement, sur la base d'analyses et de rapports récents consacrés à l'économie numérique et à la compétitivité des entreprises. Ils souhaitent engager le dialogue sur une mise en oeuvre rapide de ces propositions, qui se distinguent par un coût nul ou réduit et un impact important sur la relance et le développement économique.
