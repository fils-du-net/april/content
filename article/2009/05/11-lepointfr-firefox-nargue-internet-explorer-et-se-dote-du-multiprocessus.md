---
site: lepoint.fr
title: "Firefox nargue Internet Explorer et se dote du multiprocessus"
author: Guerric Poncet
date: 2009-05-11
href: http://www.lepoint.fr/actualites-technologie-internet/2009-05-11/internet-firefox-nargue-internet-explorer-et-se-dote-du-multiprocessus/1387/0/342279
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Les résultats du dernier baromètre des navigateurs d'AT Internet Institute (ex-Xiti Monitor, très réputé) montrent une nouvelle fois une progression de Firefox, logiciel libre de Mozilla . Avec 28,4 % de parts de marché en Europe, et plus de 42 % en Allemagne, le concurrent d'Internet Explorer conforte sa place de numéro deux, et flirte avec la première position. En France, 30,6 % des internautes utilisent Firefox.
