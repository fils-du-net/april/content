---
site: journaldunet.com
title: "On ne se tourne plus vers l'Open Source sur le seul critère de la gratuité"
author: Dominique Filippone
date: 2009-05-29
href: http://www.journaldunet.com/solutions/intranet-extranet/interview/elie-auvray-on-ne-se-tourne-plus-vers-l-open-source-sur-le-seul-critere-de-la-gratuite.shtml
tags:
- Le Logiciel Libre
- Entreprise
---

> On avait coutume de dire il n'y a pas si longtemps que les solutions ECM Open Source représentaient une alternative aux offres propriétaires. Nous disons que cette période est révolue et que les offres Open Source sont devenues des offres à part entière qui tiennent largement tête aux solutions propriétaires. De la même façon, les entreprises ne se tournent plus vers l'Open Source sur le seul critère de la gratuité mais commencent à s'apercevoir qu'en termes de montée en charge et de capacité volumétrique, les performances de ces solutions tiennent largement tête à celles de leurs homologues.
