---
site: numerama.com
title: "Wikipedia passe sous licence Creative Commons"
author: Guillaume Champeau
date: 2009-05-25
href: http://www.numerama.com/magazine/12981-Wikipedia-passe-sous-licence-Creative-Commons.html
tags:
- Partage du savoir
- Licenses
- Philosophie GNU
---

> Désormais, tous les contenus diffusés sur Wikipedia seront proposés sous licence Creative Commons By-sa, qui autorise leur exploitation à condition de citer la source et de préserver la licence. Un choix qui enterre un peu plus la licence GNU Free Documentation License, qui n'a jamais été très adaptée à la diffusion d'oeuvres littéraires et artistiques.
