---
site: hardmicro.fr
title: "Hadopi : un vote de l'Assemblée nationale fait avec des oeillères"
author: Beugré Jean-Augustin
date: 2009-05-19
href: http://www.hardmicro.fr/news-article.storyid-3559.htm
tags:
- April
- HADOPI
---

> « Les députés qui ont voté le projet de loi HADOPI démontrent ainsi que seule compte pour eux la volonté toute puissante du président de la République, au mépris de la séparation des pouvoirs. Tant du législatif, qui n'est plus là que pour avaliser les ordres de l'exécutif, que de l'autorité judiciaire censée pourtant décider de sanctions privatives de libertés » a ajouté Frédéric Couchet, délégué général de l'April.
> « Cet aveuglement se fera inévitablement rattraper par la réalité : la loi Hadopi est politiquement et juridiquement morte et les citoyens trouveront avec les artistes des solutions efficaces de financement de la création à l'heure d'Internet » a conclu Benoît Sibaud, président de l'April.
