---
site: lecourrier.ch
title: "Ecoles: l'informatique en logiciels libres"
author: Philippe Bach
date: 2009-05-22
href: http://www.lecourrier.ch/index.php?name=NewsPaper&file=article&sid=442501
tags:
- Le Logiciel Libre
- Éducation
---

> D'ici à 2013, les ordinateurs du primaire tourneront avec un système d'exploitation GNU/Linux et des logiciels open source.
> L'école genevoise va progressivement migrer vers des logiciels libres pour équiper les ordinateurs du corps enseignant et ceux des élèves. Dès l'automne, deux établissements du primaire essuieront les plâtres avec, en soutien, un sérieux appui informatique.
> [...]
> Pour l'Etat, les avantages sont multiples. «L'information gérée par l'Etat est une ressource stratégique dont l'accessibilité par l'administration et par les citoyens, la pérennité et la sécurité ne peuvent être garanties que par l'utilisation de standards ouverts et de logiciels dont le code source est ouvert au public», peut-on lire dans le plan de déploiement. Le processus enclenché par le Service écoles-médias (SEM) se veut participatif. Le processus a été mené de manière transparente, sur la place publique dès le début.
