---
site: silicon.fr
title: "270 millions d’utilisateurs pour Firefox et 60 millions pour OpenOffice.org"
author: David Feugey 
date: 2009-05-06
href: http://www.silicon.fr/fr/news/2009/05/06/270_millions_d_utilisateurs_pour_firefox_et_60_millions_pour_openoffice_org
tags:
- Le Logiciel Libre
- Internet
- Promotion
---

> Selon Asa Dotzler, le nombre d’utilisateurs de Mozilla Firefox serait aujourd’hui de 270 millions. Une adoption très rapide, qui s’est faite essentiellement aux dépens d’Internet Explorer, dont la croissance s’est nettement ralentie.
