---
site: silicon.fr
title: "Windows 7 plus cher que Vista"
author: Olivier Robillart 
date: 2009-05-18
href: http://www.silicon.fr/fr/news/2009/05/18/windows_7_plus_cher_que_vista
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> [...]
> Selon un responsable de Dell, les tarifs des licences du nouvel OS de Microsoft seraient plus chères que celles de Vista.
> Selon le site Cnet.com, Microsoft jouerait alors un jeu dangereux dans le sens où la firme jouerait sur l’effet d’attente de Windows 7. De même, le responsable de Dell, craint que de nombreuses administrations et milieux scolaires ne puissent supporter ces coûts et choisissent une solution Linux. Une stratégie plutôt difficile à comprendre en période de crise économique…
