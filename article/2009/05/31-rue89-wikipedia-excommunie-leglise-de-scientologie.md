---
site: Rue89
title: "Wikipedia excommunie l'Eglise de scientologie"
author: Laurent Mauriac
date: 2009-05-31
href: http://www.rue89.com/2009/05/31/wikipedia-excommunie-leglise-de-scientologie
tags:
- Internet
- Partage du savoir
- Désinformation
---

> C'est au terme d'une procédure de cinq mois, détaillée sur le site, que le comité d'arbitrage de Wikipedia s'est prononcé, votant le blocage en écriture des adresses IP incriminées (par dix voix et une abstention), ainsi qu'une série d'autres décisions visant notamment à empêcher plusieurs utilisateurs enregistrés d'intervenir dans des articles relatifs à la scientologie.
