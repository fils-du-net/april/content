---
site: silicon.fr
title: "Red Hat attaque Microsoft en Suisse"
author: Christophe Lagane
date: 2009-05-26
href: http://www.silicon.fr/fr/news/2009/05/26/red_hat_attaque_microsoft_en_suisse
tags:
- Le Logiciel Libre
- Économie
- Institutions
- Licenses
- Europe
---

> Un contrat de 9 millions d'euros attribué sans appel d'offre à Redmond est à l'origine de la colère de Red Hat soutenu par 17 autres entreprises.
> [...]
> Red Hat annonce attaquer, indirectement, Microsoft en Suisse. Raison du litige? L'absence d'appel d'offre public pour un contrat de 14 millions de francs suisses (9,2 millions d'euros) passé par bureau fédéral suisse de la construction et de la logistique (BBL).
