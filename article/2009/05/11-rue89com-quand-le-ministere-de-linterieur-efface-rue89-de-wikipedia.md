---
site: rue89.com
title: "Quand le ministère de l'Intérieur efface Rue89 de Wikipedia"
author: Augustin Scalbert
date: 2009-05-11
href: http://www.rue89.com/2009/05/11/quand-le-ministere-de-linterieur-efface-rue89-de-wikipedia
tags:
- Internet
- Partage du savoir
- Institutions
- Désinformation
---

> En février, Rue89 révélait que la fille du secrétaire d'Etat Alain Marleix allait bénéficier d'un piston pour décrocher un poste à la Sorbonne.  En mars, comme le révèle le blog Tech/notes, un agent de la place Beauvau a tenté plusieurs fois de supprimer la référence à notre article dans la bio Wikipedia du ministre.
