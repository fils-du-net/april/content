---
site: "branchez-vous.com"
title: "Lobbying pour le logiciel libre à l'approche des élections municipales"
author: Carine Salvi
date: 2009-05-27
href: http://benefice-net.branchez-vous.com/actubn/2009/05/lobbying_pour_le_logiciel_libr.html
tags:
- Le Logiciel Libre
- Institutions
- Sensibilisation
---

> Lors du congrès de Projet Montréal tenu le week-end dernier, Facil a non seulement réussi à ce que les troupes du parti progressiste prennent position en faveur du logiciel libre, mais a fait inscrire une nouvelle proposition dans le programme du parti, celle de «faire de Montréal la capitale du logiciel libre en Amérique du Nord».
