---
site: Linux Journal
title: "Should Software Developers Be Liable for their Code?"
author: Glyn Moody
date: 2009-05-09
href: http://www.linuxjournal.com/content/should-software-developers-be-liable-their-code
tags:
- Le Logiciel Libre
- Institutions
- Europe
---

> Quid de la responsabilité du développeur ? Et, surtout, du développeur de logiciel libre ! La Commission Européenne examine un projet de loi susceptible de rendre responsables les éditeurs de logiciels des pertes induites par les défauts de leurs produits… Une responsabilité dont ils s’exonèrent encore largement via les contrats de licences. Mais que donnerait une telle disposition appliquée… au logiciel libre ? La question est ouverte mais a de quoi inquiéter. De quoi fournir aux assureurs un nouveau relais de croissance ?
