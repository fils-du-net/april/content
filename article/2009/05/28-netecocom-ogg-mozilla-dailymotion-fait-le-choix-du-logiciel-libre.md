---
site: neteco.com
title: "OGG, Mozilla... Dailymotion fait le choix du logiciel libre"
author: Jérôme Bouteiller
date: 2009-05-28
href: http://www.neteco.com/278780-ogg-mozilla-dailymotion-choix-logiciel-libre.html
tags:
- Le Logiciel Libre
- Entreprise
- Interopérabilité
- Video
---

> Champion hexagonal de la vidéo en ligne avec près d'un milliard diffusés chaque mois, Dailymotion a décidé de prendre ses distances avec les logiciels et autres codes propriétaires.
