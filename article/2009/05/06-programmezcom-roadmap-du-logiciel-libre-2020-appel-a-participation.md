---
site: programmez.com
title: "Roadmap du logiciel libre 2020 : Appel à participation"
author: Frédéric Mazué
date: 2009-05-06
href: http://www.programmez.com/actualites.php?titre_actu=Roadmap-du-logiciel-libre-2020--Appel-a-participation&id_actu=5000
tags:
- Le Logiciel Libre
- Entreprise
---

> Les porteurs de l'initiative "FLOSS Roadmap 2020" annoncent l'ouverture du site http://www.2020flossroadmap.org dédié à l'élaboration collaborative de la prochaine version de la feuille de route du logiciel libre à l'horizon 2020.
> Ce site coopératif, donc la création a été financée en partie par les pôles de compétitivité Cap Digital et System@tic, vise à permettre à l'ensemble de la communauté mondiale du logiciel libre de commenter et de proposer des améliorations à la version initiale de la roadmap, et d'élaborer de nouvelles thématiques liées à la conjoncture économique actuelle.
