---
site: UsineNouvelle
title: "Ossif : l'open source avance à grands pas dans l'industrie"
author: Christophe Dutheil
date: 2009-05-27
href: http://www.usinenouvelle.com/article/ossif-l-open-source-avance-a-grands-pas-dans-l-industrie.165428
tags:
- Le Logiciel Libre
- Entreprise
- Économie
- Partage du savoir
---

> Organisé le 26 mai à Toulouse, l'Ossif (« Open Source Industry Forum ») a passé au crible les bénéfices à attendre de l'open source et les principaux enjeux à relever pour les entreprises industrielles intéressées. 
