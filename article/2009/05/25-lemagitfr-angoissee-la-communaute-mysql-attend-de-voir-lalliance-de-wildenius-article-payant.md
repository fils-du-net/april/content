---
site: lemagit.fr
title: "Angoissée, la communauté MySQL attend de voir l'alliance de Wildenius article payant"
author: Cyrille Chausson 
date: 2009-05-25
href: http://www.lemagit.fr/article/sun-mysql-oracle-opensource/3368/1/angoissee-communaute-mysql-attend-voir-alliance-wildenius/
tags:
- Le Logiciel Libre
- Entreprise
---

> La communauté MySQL ne sait plus à quel saint se vouer. S'interrogeant sur le devenir du gestionnaire de bases de données Open Source dans le giron d'Oracle, l'angoisse laisse des traces sur les forums de la communauté. Seule alternative rassurante, l'Open Database Alliance, projet du charismatique Monty Wildenius, mais qui doit encore faire ses preuves quant au modèle économique.
