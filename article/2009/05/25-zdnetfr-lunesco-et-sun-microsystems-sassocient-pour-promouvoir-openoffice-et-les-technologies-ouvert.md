---
site: zdnet.fr
title: "L'Unesco et Sun Microsystems s'associent pour promouvoir OpenOffice et les technologies ouvertes"
author: Thierry Noisette
date: 2009-05-25
href: http://www.zdnet.fr/blogs/2009/05/25/l-unesco-et-sun-microsystems-s-associent-pour-promouvoir-openoffice-et-les-technologies-ouvertes/
tags:
- Partage du savoir
- Institutions
- Sensibilisation
- Éducation
---

> L'Unesco et Sun annoncent avoir signé le 18 mai lors du Sommet mondial sur la société de l'information (World Summit on the Information Society, ou WSIS) à Genève un partenariat. Ils s'engagent à  travailler pour l'éducation et le développement soutenus par les technologies ouvertes.
