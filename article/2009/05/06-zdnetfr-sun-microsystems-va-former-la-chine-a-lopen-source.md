---
site: zdnet.fr
title: "Sun Microsystems va former la Chine à l'open source"
author: Thierry Noisette
date: 2009-05-06
href: http://www.zdnet.fr/blogs/2009/05/06/sun-microsystems-va-former-la-chine-a-l-open-source/
tags:
- Le Logiciel Libre
- Administration
---

> [...]
> Le programme, baptisé CHIPS (China Innovation Program for Students), formera des étudiants au logiciel libre. Lin Lee, vice-présidente "Global communities" de Sun, affirme que la maîtrise de compétences en open source aidera ces étudiants à être compétitifs.
> L'accord prévoit un programme de trois ans avec "des séminaires sur mesure, des cours pratiques, des ateliers et des programmes d'intégration qui s'adresseront principalement aux responsables des universités, aux enseignants et aux étudiants de filières liées à l'informatique de même qu'aux employés des sociétés d'informatique", indique Chine Nouvelle.
