---
site: vnunet.fr
title: "Licence GNU : Cisco trouve un terrain d'entente avec la Free Software Foundation"
author: La rédaction
date: 2009-05-26
href: http://www.vnunet.fr/news/licence_gnu_cisco_trouve_un_terrain_d_entente_avec_la_free_software_foundation-2030856
tags:
- Le Logiciel Libre
- Entreprise
- Droit d'auteur
- Philosophie GNU
---

> La Free Software Foundation et Cisco ont trouvé un accord à l'amiable concernant le litige qui les opposait depuis fin 2008.
> Le contentieux portait sur une présumée violation de licences open source (dont la GNU General Public License) dans sa gamme de produits Linksys (réseaux numérique à domicile). La plainte initialement déposée est donc caduque.
