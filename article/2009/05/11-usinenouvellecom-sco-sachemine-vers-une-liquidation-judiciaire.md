---
site: usinenouvelle.com
title: "SCO s'achemine vers une liquidation judiciaire"
author: Christophe Dutheil
date: 2009-05-11
href: http://www.usinenouvelle.com/article/sco-s-achemine-vers-une-liquidation-judiciaire.164346
tags:
- Le Logiciel Libre
- Entreprise
- Brevets logiciels
- Standards
---

> Sorti vaincu d'une longue bataille juridique l'ayant opposé à Novell et IBM quant à la paternité supposée du code source d'Unix, l'éditeur d'UnixWare pourrait bien mettre prochainement la clef sous la porte.
> SCO s'achemine vers une liquidation judiciaire Le coup de grâce... Sous la protection du Chapitre 11 de la loi contre les faillites depuis 2007, le groupe américain SCO, le « père » du système d'exploitation UnixWare, risque d'être contraint à la liquidation d'ici la fin du mois (sous le Chapitre 7 de la loi américaine). Dans une décision datée du 5 mai, le syndic américain chargé d'examiner son dossier (US Trustee) s'est en effet prononcé pour un dépôt de bilan pur et simple.
