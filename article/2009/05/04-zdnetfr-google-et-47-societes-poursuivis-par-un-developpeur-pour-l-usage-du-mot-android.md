---
site: zdnet.fr
title: "Google et 47 sociétés poursuivis par un développeur pour l’usage du mot Android"
author: ZDNet France
date: 2009-05-04
href: http://www.zdnet.fr/actualites/it-management/0,3800005311,39500610,00.htm
tags:
- Le Logiciel Libre
- Entreprise
- Licenses
---

> Juridique - Un développeur de l’Illinois qui a déposé le nom "android" a décidé de poursuivre Google, l'Open Handset Alliance et des opérateurs pour l'utilisation de ce mot pour l’OS open source. Il réclame quelque 100 millions de dollars.
> Google et 47 sociétés, dont Intel, Motorola, Nvidia, Samsung, T-Mobile, Toshiba et Vodafone, tous membres de l'Open Handset Alliance ou opérateurs de téléphonie mobile, sont attaquées par la personne qui a déposé le mot « android » pour sa société.
