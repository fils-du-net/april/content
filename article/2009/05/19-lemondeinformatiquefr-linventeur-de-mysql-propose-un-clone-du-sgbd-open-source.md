---
site: lemondeinformatique.fr
title: "L'inventeur de MySQL propose un clone du SGBD Open Source"
author: Olivier Rafal 
date: 2009-05-19
href: http://www.lemondeinformatique.fr/actualites/lire-l-inventeur-de-mysql-propose-un-clone-du-sgbd-open-source-28618.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Et si MySQL vivait de nouveau de ses propres ailes... sous un autre nom ? C'est en quelque sorte l'ambition de Michael Widenius, alias Monty, le développeur originel de la base de données Open Source et fondateur de MySQL AB. Monty Widenius promeut aujourd'hui MariaDB, un clone de MySQL, ou plutôt « la même base, avec plus de fonctions et moins de bugs », nous a-t-il confié.
