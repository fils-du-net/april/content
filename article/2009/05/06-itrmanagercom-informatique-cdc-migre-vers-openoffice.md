---
site: itrmanager.com
title: "Informatique CDC migre vers OpenOffice"
author: itrmanager
date: 2009-05-06
href: http://www.itrmanager.com/articles/90787/informatique-cdc-migre-vers-openoffice.html
tags:
- Le Logiciel Libre
- Entreprise
---

> Dans un contexte de volonté de réduction des coûts mais aussi d'amélioration de l'intéroperabilité et de la pérennité du système d'information, Informatique CDC a dressé très tôt une stratégie autour de l'intégration des produits Open Source à son offre de service.
> La suite bureautique apparait comme un composant à inscrire dans cette stratégie. Des études d'opportunité et de faisabilité ont été menées dés 2005. Une solution alternative émerge avec OpenOffice.org. Un pilote est mis en place pour une centaine d'utilisateur afin de déterminer au mieux les coûts et les moyens nécessaires à la mise en place d'un projet de migration vers la suite bureautique Open Source.
