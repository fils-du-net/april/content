---
site: lefaso.net
title: "Utilisation du logiciel libre : Un système sûr et sécurisé"
author: Gabriel SAMA
date: 2009-05-27
href: http://www.lefaso.net/spip.php?article31857&rubrique227
tags:
- Le Logiciel Libre
- Entreprise
---

> A la faveur de la Ve édition de la Semaine nationale de l’Internet (SNI) et des autres Technologies de l’information et de la communication (TIC), une journée du logiciel libre s’est tenue le 26 mai 2009 à Ouagadougou.
