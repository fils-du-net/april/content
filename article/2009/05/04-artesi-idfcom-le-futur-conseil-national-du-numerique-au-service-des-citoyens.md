---
site: "artesi-idf.com"
title: "Le futur Conseil national du numérique au service des citoyens ?"
author: artesi-idf.com
date: 2009-05-04
href: http://www.artesi.artesi-idf.com/public/article.tpl?id=18630
tags:
- Internet
- April
---

> L'April a appris que Nathalie Kosciusko-Morizet, secrétaire d'État à la prospective et au développement de l'économie numérique , souhaitait la mise en oeuvre avant l'été du Conseil national du numérique [1] (CNN) proposé par le plan France Numérique 2012.
> L'April, membre du Forum des Droits sur l'Internet qui devrait être intégré à ce CNN, tient à rappeler les principes de gouvernance nécessaires pour le succès d'une telle démarche de concertation multi-acteurs.
> L'April considère que l'existence d'une structure de co-régulation est plus que jamais nécessaire. Concernant le statut d'une telle structure, l'April est attachée à un statut associatif qui permet notamment d'accueillir toute organisation désireuse de participer à la concertation ouverte multi-acteurs, sans recours à un quelconque acte administratif ou parrainage politique.
