---
site: La Voix du Nord
title: "Les logiciels libres s'installent dans les disques durs "
author: Valérie Savage
date: 2009-05-28
href: http://www.lavoixeco.com/actualite/Secteurs_activites/Informatique_et_High_Tech/2009/05/28/article_les-logiciels-libres-s-installent-dans-l.shtml
tags:
- Entreprise
- Administration
- Économie
- Associations
- Philosophie GNU
---

> « Le marché devrait progresser de 30 % cette année » (...) «  On n'en est plus aux balbutiements. On est dans l'industrialisation », estime Gabriel Foin
