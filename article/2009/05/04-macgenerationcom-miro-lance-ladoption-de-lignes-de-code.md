---
site: macgeneration.com
title: "Miro lance l'adoption de lignes de code"
author: Florian Innocente
date: 2009-05-04
href: http://www.macgeneration.com/news/voir/134776/miro-lance-l-adoption-de-lignes-de-code
tags:
- Le Logiciel Libre
- Philosophie GNU
---

> Miro, développeur du lecteur Internet multimédia du même nom et fonctionnant sur le principe d'une organisation à but non lucratif, cherche à trouver de nouveaux modes de financement. Il a imaginé de faire adopter les lignes de code de son logiciel par ses utilisateurs.
