---
site: ecrans.fr
title: "L’Internet, sortie de crise et solidarité"
author: Nathalie Kosciusko-Morizet
date: 2009-05-11
href: http://www.ecrans.fr/L-Internet-sortie-de-crise-et,7185.html
tags:
- Internet
- Économie
---

> La solidarité numérique exige d’abord que tous les foyers aient accès à l’Internet. Si le développement des logements sociaux est bien une priorité aujourd’hui, alors nous devons équiper ces logements, et faire que leurs habitants y trouvent d’emblée la commodité essentielle qu’est devenu l’accès à Internet.
