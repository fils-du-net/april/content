---
site: Nord Eclair
title: "Microsoft partenaire d'une association de microcrédits"
author: Martin Youenn
date: 2009-05-22
href: http://www.nordeclair.fr/France-Monde/France/2009/05/22/microsoft-partenaire-d-une-association-d.shtml
tags:
- Logiciels privateurs
- Économie
- Associations
---

> [L'Adie], qui propose des microcrédits à des taux avoisinant les 10 %, offre des formations gratuites à ses clients. Pour l'informatique, elle s'est alliée à un géant du secteur, Microsoft. Ce partenariat existe depuis 5 ans pour tout ce qui concerne la bureautique. Lundi, une nouvelle signature a étendu la collaboration aux outils de création de sites Web. (...) Les promoteurs des logiciels libres ne manqueront pas de faire remarquer que le partenariat entre l'Adie et Microsoft va rendre « dépendants » à Windows les petits créateurs d'entreprise.
