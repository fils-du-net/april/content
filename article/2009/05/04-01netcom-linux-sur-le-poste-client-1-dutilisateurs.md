---
site: 01net.com
title: "Linux sur le poste client : 1 % d'utilisateurs"
author: Tristan Nitot
date: 2009-05-04
href: http://pro.01net.com/editorial/501961/linux-sur-le-poste-client-1-pour-cent-dutilisateurs/
tags:
- Le Logiciel Libre
---

> La part de marché de Linux sur le poste de travail vient de dépasser 1 %, selon Net Applications. Faut-il s'écrier « déjà » ou « seulement » ? Replaçons déjà le contexte : sur le marché des serveurs, Linux s'en sort très bien, en particulier pour ce qui est des serveurs dédiés à l'Internet (e-mail, Web, DNS, etc.). Mais sur le poste client, c'est une autre affaire. En effet, sur ce marché, Linux doit se battre contre deux idées reçues particulièrement tenaces.
