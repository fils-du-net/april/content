---
site: zdnet.fr
title: "Le piratage de logiciels en hausse dans le monde, selon la BSA"
author: Christophe Auffray
date: 2009-05-12
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39501478,00.htm
tags:
- Vente liée
- DRM
---

> [...]
> La BSA favorable à la vente liée et aux DRM
> Mais cette méthodologie de calcul n'est pas le seul point du rapport du BSA sujet à controverse. Pour lutter contre le piratage, l'organisation se déclare ainsi favorable aux DRM, et à la préinstallation de logiciels sur les ordinateurs. De nombreux utilisateurs de l'outil informatique et d'associations ont à de multiples reprises témoigné de leur opposition à cette mise en oeuvre de la vente liée.
