---
site: Unesco.org
title: "L’UNESCO et Sun Microsystems unissent leurs efforts pour renforcer l'éducation et le développement communautaire"
author: La rédaction
date: 2009-05-25
href: http://portal.unesco.org/ci/fr/ev.php-URL_ID=28685&URL_DO=DO_TOPIC&URL_SECTION=201.html
tags:
- Le Logiciel Libre
- Institutions
- Éducation
---

> Les technologies open source, éléments essentiels de la croissance sociale, éducative et économique et de la réduction de la fracture numérique. Dans un effort pour soutenir le développement économique et social, l'UNESCO et Sun Microsystems ont signé le 18 mai un accord lors du Forum 2009 du Sommet mondial sur la société de l'information (SMSI).
