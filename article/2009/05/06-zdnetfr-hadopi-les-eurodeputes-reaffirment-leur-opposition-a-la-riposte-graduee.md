---
site: zdnet.fr
title: "Hadopi : les eurodéputés réaffirment leur opposition à la riposte graduée"
author: Christophe Auffray
date: 2009-05-06
href: http://www.zdnet.fr/actualites/internet/0,39020774,39500972,00.htm?xtor=EPR-100
tags:
- HADOPI
---

> Législation - Les députés européens se sont de nouveau opposés à une coupure de l’accès à Internet sans décision de justice, telle que défendue par le projet Hadopi. Conséquence : le Paquet télécoms est rejeté et devra être renégocié à l'issue des prochaines élections européennes.
> Le Parlement européen vient une nouvelle fois de marquer son opposition au projet de loi français Création et Internet, dit Hadopi. Les députés ont, pour la troisième reprise, voté largement en faveur de l'amendement 138 qui stipule que l'accès à Internet est un droit fondamental qui ne peut être restreint « sans décision préalable des autorités judiciaires ».
