---
site: "cio-online.com"
title: "Reconstruction du paysage informatique mondial, risques et opportunités"
author: Jean-Pierre Corniou
date: 2009-05-25
href: http://www.cio-online.com/contributions/lire-reconstruction-du-paysage-informatique-mondial-risques-et-opportunites-282.html
tags:
- Le Logiciel Libre
- Entreprise
- Logiciels privateurs
- Informatique en nuage
---

> Au foisonnement primal d'un marché émergent succède progressivement une réduction du nombre d'acteurs et une stabilisation des parts de marché. C'est la cas de l'automobile, de l'aéronautique, de l'énergie à des stades différents. Deux options majeures s'ouvrent aux DSI soucieux de ne pas limiter leurs marges de manoeuvre aux offres des quatre majors de l'IT, IBM, SAP, Oracle ou Microsoft : faire plus soi-même en exploitant les potentiels du logiciel libre, ou ne plus faire du tout en se remettant à l'informatique du nuage.
