---
site: neteco.com
title: "Un décret pour l'accessibilité des sites publics"
author: Alexandre Laurent 
date: 2009-05-25
href: http://www.neteco.com/278020-decret-accessibilite-sites-publics.html
tags:
- April
- Accessibilité
- RGI
- Standards
---

> Quatre ans après l'adoption de la loi fixant les conditions de déploiement d'un référentiel d'accessibilité destiné aux sites Internet de l'administration publique, le décret signant son entrée en vigueur a été publié dans l'édition du Journal Officiel datée du 16 mai. Il reste maintenant à définir précisément le champ d'application du Référentiel Général d'Accessibilité pour les Administrations.
> [...]
> Très engagée sur la question, ainsi que sur l'épineux dossier du Référentiel Général d'Interopérabilité (RGI), l'April (Association de promotion du logiciel libre), regrette pour sa part que la date de publication du RGAA ne soit pas communiquée, et que le texte « ne précise pas les modalités de contrôle de conformité des sites publics et de sanction ».
