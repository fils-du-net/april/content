---
site: jesuislibre.net
title: "Orange Hello : la Debian facile?"
author: jesuislibre.net
date: 2009-05-02
href: http://tuxicoman.jesuislibre.net/2009/05/orange-hello.html
tags:
- Le Logiciel Libre
- Accessibilité
---

> [...] L'offre est intéressante à plusieurs titres. Premièrement les séniors ont largement été laissés sur la touche de la révolution Internet. Il est vital que cette génération ne soit pas mise à l'écart des nouveaux usages de la vie courante.
> Deuxièmement, c'est un bel exemple de l'intérêt du logiciel libre qui permet à des acteurs ayant de bonnes idées de créer un produit novateur avec peu de moyens sans avoir besoin de réinventer la roue de l'informatique. Il en découle que les briques logicielles favorisent l'utisation des standards  de communication et fichiers libres.
