---
site: fsf.org
title: "FSF Settles Suit Against Cisco"
author: Brett Smith
date: 2009-05-20
href: http://www.fsf.org/news/2009-05-cisco-settlement.html
tags:
- Le Logiciel Libre
- Licenses
- Philosophie GNU
---

> […] "We are glad that Cisco has affirmed its commitment to the free software community by implementing additional measures within its compliance program and dedicating appropriate resources to them, further reassuring the users' freedoms under the GPL," said Peter Brown, Executive Director of the FSF.
