---
site: zdnet.fr
title: "Les ventes de serveurs s’effondrent au premier trimestre"
author: La rédaction
date: 2009-05-28
href: http://www.zdnet.fr/actualites/informatique/0,39040745,39503146,00.htm
tags:
- Le Logiciel Libre
- Entreprise
- Économie
---

> Entre janvier et mars 2009, ce sont un peu plus de 1,4 million de serveurs qui ont été livrés, soit 26,5% de moins qu'au premier trimestre de l'année précédente. Et IDC ne s'attend pas à une amélioration de la situation pour la fin du premier semestre.
> [...]
> Côté OS, ce sont les machines sous Windows qui sont le plus durement frappées (-28,9%), à 3,7 milliards de dollars. Les serveurs Unix et Linux enregistrent un recul de respectivement  17,5% et 24,8%, à 3,3 et 1,4 milliards de dollars.
