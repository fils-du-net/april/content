---
site: vnunet.fr
title: "Microsoft et Linux font front commun pour défendre la concurrence dans le monde des logiciels"
author: Iain Thomson
date: 2009-05-19
href: http://www.vnunet.fr/news/microsoft_et_linux_font_front_commun_pour_defendre_la_concurrence_dans_le_monde_des_logiciels-2030815
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Droit d'auteur
---

> L'American Law Institute veut instaurer un système de garantie contre les défauts de logiciels qui ne mettrait plus sur un pied d'égalité les éditeurs open source et propriétaires.
> [...]
> Le principal point d'achoppement a trait à une disposition mise au point par l'ALI qui prévoit que chaque logiciel distribué par un éditeur open source ne peut pas forcément faire l'objet de garantie en cas de défauts. En revanche, cette garantie doit jouer si des défauts sont constatés dans un logiciel commercialisé par un éditeur propriétaire.
