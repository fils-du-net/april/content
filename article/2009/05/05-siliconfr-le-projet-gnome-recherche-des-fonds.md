---
site: silicon.fr
title: "Le projet GNOME recherche des fonds"
author: David Feugey 
date: 2009-05-05
href: http://www.silicon.fr/fr/news/2009/05/05/le_projet_gnome_recherche_des_fonds
tags:
- Le Logiciel Libre
- Associations
---

> Effet de la crise, le budget de la fondation GNOME est en danger. Elle se met donc en quête de dons.
> [...]
> La fondation GNOME se charge de planifier le développement de ce projet, mais aussi d’en assurer la promotion. Le budget 2009 de la fondation a été établi avec soin. Les revenus devraient être de 221.665 dollars et les dépenses de 304.620 dollars. La fondation Gnome doit donc trouver près de 83.000 dollars pour boucler son budget.
