---
site: "generation-nt.com"
title: "Linux : un patch pour la polémique VFAT"
author: Jérôme G.
date: 2009-05-04
href: http://www.generation-nt.com/linux-vfat-fichiers-noms-longs-tomtom-actualite-733861.html
tags:
- Le Logiciel Libre
- Brevets logiciels
---

> Un patch pour le noyau Linux a été concocté afin de mettre un terme radical à la polémique entourant la prise en charge du système de fichiers VFAT née de la rixe judiciaire ayant opposé Microsoft et TomTom.
> Le fabricant de systèmes GPS TomTom a récemment eu maille à partir avec Microsoft. Des accusations de violations de brevets technologiques ont fusé des deux côtés pour finalement aboutir à un accord amiable paraissant surtout au bénéfice de la firme de Redmond avec la signature d'un accord de licence.
