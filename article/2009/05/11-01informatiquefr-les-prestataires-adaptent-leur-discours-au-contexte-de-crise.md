---
site: 01informatique.fr
title: "Les prestataires adaptent leur discours au contexte de crise"
author: Olivier Discazeaux
date: 2009-05-11
href: http://www.01informatique.fr/infrastructures-stockage-serveurs-116/Les-prestataires-adaptent-leur-discours-au-contexte-de-crise/
tags:
- Le Logiciel Libre
- Entreprise
---

> Dans le domaine du logiciel libre, le constructeur propose, par exemple, des « Essentiels », une vingtaine de solutions open source éprouvées qui apportent des économies comparées aux offres propriétaires. Côté « Green IT », Bull s’appuie sur son partenariat récent avec APC (filiale de Schneider Electric) pour la gestion des économies d’énergie de la salle informatique dans toutes ses composantes : électricité, froid et informatique avec la virtualisation.
