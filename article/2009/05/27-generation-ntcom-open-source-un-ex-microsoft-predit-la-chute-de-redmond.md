---
site: "generation-nt.com"
title: "Open Source : un ex-Microsoft prédit la chute de Redmond"
author: Jérôme G.
date: 2009-05-27
href: http://www.generation-nt.com/curtis-keith-microsoft-open-source-concurrence-actualite-746151.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
---

> Après plus d'une dizaine d'années passées à programmer pour le compte de Microsoft, Keith Curtis explique dans un livre que le logiciel libre va causer la chute de la firme de Redmond.
> [...]
> Interrogé par CIO, l'ex-employé Microsoft estime que si le logiciel propriétaire a fait de cette société l'une des plus puissantes de tous les temps, c'est un modèle qui est aujourd'hui handicapant, ne laissant pas les programmeurs coopérer et contribuer, ce qui étouffe l'innovation.
