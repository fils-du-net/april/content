---
site: zdnet.fr
title: "Vancouver, ville libre"
author: Thierry Noisette
date: 2009-05-24
href: http://www.zdnet.fr/blogs/2009/05/24/vancouver-ville-libre/
tags:
- Le Logiciel Libre
- Administration
---

> Développer vers et pour les citoyens l'open source, l'ouverture et le partage des informations, l'interopérabilité, les formats ouverts: la motion adoptée par la ville canadienne devrait en faire un modèle. Elle marque la montée d'une informatique publique plus tournée vers les citoyens, illustrée par des services comme les applications libres développées à Washington à l'initiative du futur CIO fédéral Vivek Kundra.
> [...]
> "Données accessibles et ouvertes : la ville de Vancouver partagera librement avec les citoyens, les entreprises et les autres juridictions la plus grande quantité de données possible en respectant la vie privée et la sécurité.
> - Standards Ouverts : La ville de Vancouver adoptera le plus rapidement possible les standards ouverts en vigueur pour les données, documents, cartes et autres formats de diffusion.
> - Logiciels Open Source : la ville de Vancouver, au moment du remplacement des logiciels existants ou de l’étude de nouvelles applications, mettra les logiciels Open Source à pied d’égalité avec les systèmes commerciaux au cours de la passation de marché."
