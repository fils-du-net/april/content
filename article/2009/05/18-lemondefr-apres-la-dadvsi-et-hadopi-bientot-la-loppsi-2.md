---
site: lemonde.fr
title: "Après la Dadvsi et Hadopi, bientôt la Loppsi 2"
author: Olivier Dumons
date: 2009-05-18
href: http://www.lemonde.fr/technologies/article/2009/05/18/apres-la-dadvsi-et-hadopi-bientot-la-loppsi-2_1187141_651865.html
tags:
- Internet
- HADOPI
---

> La loi Hadopi est donc - avec quelques houleux retards à l'allumage - finalement votée. Avec la loi Dadvsi de 2006 sur le droit d'auteur, qui avait également provoqué un tollé, voici que se profile Loppsi, élargissant encore le champ sécuritaire lié aux nouvelles technologies. Mis bout à bout, ces trois éléments forment le véritable arsenal de la "cybersécurité", promue priorité par Nicolas Sarkozy.
