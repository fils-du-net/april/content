---
site: numerama.com
title: "Nicolas Dupont-Aignan : \"Internet change la démocratie dans le bon sens\""
author: Guillaume Champeau
date: 2009-05-20
href: http://www.numerama.com/magazine/12964-Nicolas-Dupont-Aignan-Internet-change-la-democratie-dans-le-bon-sens.html
tags:
- Le Logiciel Libre
- April
---

> Opposé à la loi Hadopi, le député de l'Essonne et président du mouvement Debout La République, Nicolas Dupont-Aignan, a publié récemment son Petit livre mauve sur Internet, sous licence Creative Commons. Il nous livre, à trois semaines des élections européennes, son sentiment sur ce que l'Hadopi et l'imbroglio de l'amendement Bono révèlent des institutions françaises et européennes. Interview.
> [...]
>  Sur les droits de propriété intellectuelle, j’ai déjà fait des propositions détaillées dans mes réponses au questionnaire de l’APRIL de 2007, je me permets d’y renvoyer les internautes.
