---
site: 01net.com
title: "Pourquoi choisir Linux ?"
author: Tristan Nitot
date: 2009-05-11
href: http://pro.01net.com/editorial/502202/pourquoi-choisir-linux/
tags:
- Le Logiciel Libre
- Sensibilisation
- Philosophie GNU
---

> Mon billet de la semaine dernière, « Linux sur le poste client : 1 % d'utilisateurs », a occupé nos discussions lors des pauses-café. Ce qui a déclenché le débat, c'est cette réponse de Mark Shuttleworth (le « dictateur éclairé » d'Ubuntu Linux), à qui on demandait si exécuter les applications Windows était la chose la plus importante pour le succès de GNU/Linux, « L'écosystème du logiciel libre doit se développer en suivant ses propres règles. Il est différent de l'univers du logiciel propriétaire et doit réussir suivant ses propres critères. Si Linux est juste une alternative pour faire tourner des applications Windows, c'est perdu d'avance. OS/2 l'a démontré. »
