---
site: silicon.fr
title: "Hadopi adoptée, subsitent les épineuses questions oubliées du débat"
author: Olivier Robillart 
date: 2009-05-13
href: http://www.silicon.fr/fr/news/2009/05/13/hadopi_adoptee__subsitent_les_epineuses_questions_oubliees_du_debat
tags:
- Logiciels privateurs
- HADOPI
---

> En cas d’utilisation d’une connexion sans-fil (Wi-Fi) par un tiers, le propriétaire de la ligne sera le seul visé par les sanctions. Un risque que Christine Albanel, ministre de la Culture espère endiguer par un logiciel de sécurisation. Payant, il devrait permettre d’échapper aux sanctions. Là aussi se pose la question de son efficacité puisque son interopérabilité, à savoir la possibilité de l’installer quel que soit son système d’exploitation (Windows, Mac OS, Linux) n’est pas garantie…
