---
site: La Voix du Nord
title: "Le boom des logiciels Libres dans le Nord-Pas-de-Calais"
author: 
date: 2009-05-28
href: http://www.lavoixeco.com/actualite/Secteurs_activites/Informatique_et_High_Tech/2009/05/28/article_le-boom-des-logiciels-libres-dans-le-nord-pas-de.shtml
tags:
- Entreprise
- Économie
- Sensibilisation
---

> Difficile pour l'instant d'estimer le nombre de personnes qui travaillent dans le secteur des logiciels libres dans le Nord-Pas-de-Calais.(...) Philippe Pary, de l'association April (qui promeut et défend les logiciels libres), estime à environ 500 le nombre de personnes qui travaillent dans des entreprises spécialisées Neoweb, Webpulser, Noparking ) ou non (Atos, Cap Gemini, Altran ).
