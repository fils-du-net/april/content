---
site: actualitte.com
title: "Schwarzenegger veut des manuels scolaires gratuits open source"
author: Victor de Sepausy
date: 2009-05-14
href: http://www.actualitte.com/actualite/10404-Schwarzenegger-manuels-scolaires-open-source.htm
tags:
- Éducation
---

> Gouverneur de Californie, Arnold Schwarzenegger vient de lancer une campagne visant à apporter aux étudiants de l'État des manuels scolaires, bien évidemment numériques, mais surtout gratuits et open source de surcroît.
