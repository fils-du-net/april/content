---
site: tsr.ch
title: "L'Assemblée interjurassienne n'a pas proposé de recette miracle"
author: sbo
date: 2009-05-05
href: http://www.tsr.ch/tsr/index.html?siteSect=200700&sid=10626968&cKey=1241508720000
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Administration
---

> [...]
> 42 millions pour Microsoft
> A Berne, ce qui fait jaser, c'est une grosse commande informatique de 42 millions de francs. Une somme que la Confédération s'apprête à dépenser auprès de Microsoft pour renouveler des licences de son parc d'ordinateurs. La Neue Zuercher Zeitung  a déniché une trace de cette commande dans la Feuille officielle suisse du commerce et le quotidien zurichois s'étonne. Pourquoi n'y a-t-il pas eu d'appel d'offres public pour cette affaire, alors que la loi l'exige? Une question que se posent aussi les concurrents de Microsoft - et parmi eux les entreprises suisses qui installent des logiciels libres dans les administrations.
