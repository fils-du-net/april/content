---
site: sudouest.com
title: "Des auteurs de science-fiction flinguent la loi Hadopi"
author: Philippe Ménard
date: 2009-05-12
href: http://www.sudouest.com/accueil/actualite/france/article/586649/mil/4518292.html
tags:
- Le Logiciel Libre
- HADOPI
---

> « Le seul moyen de prouver son innocence est d'installer un "mouchard" sur son ordinateur, que l'on doit payer. C'est le renversement de la preuve. De plus, ce système ne peut s'appliquer sur les logiciels libres, du type Linux, ce qui oblige à passer par Windows », poursuivent-ils. Selon eux, ce dispositif vise, pour les industriels de la culture, à « verrouiller l'offre », et pour les politiques à mettre Internet sous contrôle.
