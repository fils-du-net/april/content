---
site: Le Monde
title: "Les bons plans des logiciels libres"
author: Joël Morio
date: 2009-05-21
href: http://www.lemonde.fr/aujourd-hui/article/2009/05/21/les-bons-plans-des-logiciels-libres_1196343_3238.html
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Logiciels privateurs
- April
- Promotion
---

> Peut-on équiper son ordinateur de programmes gratuits en toute légalité ? Alors que l'étau se resserre pour ceux qui téléchargent illégalement de la musique ou des films sur Internet, l'installation de logiciels dits "libres", souvent gratuits, reste autorisée et se développe rapidement. [...]
