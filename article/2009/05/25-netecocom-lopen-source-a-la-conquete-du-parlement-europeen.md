---
site: neteco.com
title: "L'open source à la conquête du Parlement Européen"
author: Guillaume Belfiore
date: 2009-05-25
href: http://www.neteco.com/277944-open-source-parlement-europeen.html
tags:
- Le Logiciel Libre
- April
---

> Les 6 et 7 juin prochains auront lieu les élections européennes. A cette occasion, l'April - l'association française de promotion du logiciel libre - a décidé de remettre sur le devant de la scène le Pacte du Logiciel Libre. Cette initiative permet de recueillir la position des candidats, qui représenteront la France au Parlement européen pendant cinq ans, vis-à-vis de l'industrie du logiciel. Pour l'utilisateur, il s'agit d'obtenir une meilleure transparence sur le sujet.
