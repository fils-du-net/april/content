---
site: "mag-securs.com"
title: "Le nouvel OpenOffice, version 3, n’a toujours pas corrigé les grosses erreurs de conception identifiées"
author: Mag Securs
date: 2009-05-05
href: http://www.mag-securs.com/spip.php?article13460
tags:
- Le Logiciel Libre
---

> « OpenSource et gratuit » n’implique en rien que le système soit sécurisé
> La première étude menée par le professeur Filiol et son équipe montre en 2006 et 2007 que cette confiance n’a pas lieu d’être. OpenOffice est extrêmement vulnérable aux attaques des macrovirus. Les commentaires entendus suite à cette première étude avaient été consternants : l’étude aurait été à la solde de Microsoft, elle s’intéresse à des attaques improbables, etc. Bref, beaucoup de réactions émotionnelles à chaud, voire de prises de position purement dogmatiques. Le laboratoire du professeur Filiol conçoit des macros-virus démontrant la faisabilité des attaques identifiées et communique le plus ouvertement possible avec les développeurs d’OpenOffice dans le but d’aider à concevoir une « Trusted OpenOffice Suite ».
