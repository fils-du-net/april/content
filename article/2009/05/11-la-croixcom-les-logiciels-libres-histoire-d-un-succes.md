---
site: "la-croix.com"
title: "Les logiciels libres, histoire d’un succès"
author: Stéphane DREYFUS
date: 2009-05-11
href: http://www.la-croix.com/article/index.jsp?docId=2373317&rubId=5547
tags:
- Le Logiciel Libre
---

> [...]
> Mais les informaticiens ne vivent pas d’amour et d’eau fraîche. Le modèle économique du secteur se construit sur les services qui accompagnent la mise en place des logiciels libres et gratuits : la maintenance, le conseil, la formation…
> Certains font des entorses au modèle en se lançant dans la vente de logiciels mi-libres, mi-propriétaires. Par exemple, MySQL, système de gestion de bases de données, joue sur les deux tableaux : une partie de son code source (texte écrit en langage informatique qui régit le fonctionnement du programme) est ouverte, mais les fonctionnalités pour les entreprises sont payantes. C’est une tendance forte du marché du libre aux États-Unis, alors qu’en France prédomine un modèle où le code est totalement ouvert et gratuit.
