---
site: "la-croix.com"
title: "Peaufiner les logiciels est un modèle de science participative"
author: Camille POLLONI
date: 2009-05-11
href: http://www.la-croix.com/article/index.jsp?docId=2373311&rubId=786
tags:
- Le Logiciel Libre
- April
---

> "Il y a des médecins, des plombiers, des couturières"
> L’esprit du « libre » repose sur cette idée que chacun peut apporter sa pierre à l'édifice, sans prérequis techniques. « La communauté a grandi, explique Tony Bassette, administrateur de l'association Promouvoir et défendre le logiciel libre (April). Autour de chaque projet, une communauté virtuelle se forme et communique grâce à des outils comme les listes de diffusion, les forums et IRC. »
