---
site: nouvelobs.com
title: "OpenVibe ou comment piloter un ordinateur par la pensée"
author: nouvelobs
date: 2009-05-14
href: http://tempsreel.nouvelobs.com/actualites/medias/multimedia/20090514.OBS6903/openvibe_ou_comment_piloter_un_ordinateur_par_la_pensee.html
tags:
- Le Logiciel Libre
- Accessibilité
- Innovation
---

> Ecrire sur un écran d’ordinateur ou déplacer des objets virtuels par la seule force de la pensée est désormais possible grâce à OpenVibe, étonnante innovation scientifique portée par l’INRIA (Institut national de recherche en informatique et en automatique) et l’Inserm (Institut national de la santé et de la recherche médicale). OpenVibe, présenté à la presse mercredi 13 mai 2009, est le premier projet français multipartenaires sur les interfaces cerveau-machine.
> [...]
> OpenVibe a été conçu comme un logiciel libre et gratuit, d’ores et déjà téléchargeable. Le code source, ouvert, peut ainsi être enrichi par les particuliers, chercheurs ou informaticiens.
> En revanche si le logiciel est gratuit, le casque avec les électrodes (système EEG) coûtent entre 5000 et 30 000 euros.
