---
site: lemonde.fr
title: "Les censeurs du Net"
author: Claire Ulrich
date: 2009-05-29
href: http://www.lemonde.fr/archives/article/2009/05/29/les-censeurs-du-net_1199993_0.html
tags:
- Le Logiciel Libre
- Internet
- Logiciels privateurs
- Sensibilisation
- Désinformation
---

> Parce qu'ils sont bien informés, Edwin en Chine et Ahmed aux Emirats peuvent lire et écrire ce qui leur plaît, tout en restant invisibles sur l'écran de contrôle des censeurs. Ils utilisent un " proxy anonymiseur ", nom barbare d'un outil de cryptage qui permet d'emprunter discrètement l'adresse d'un autre ordinateur, quelque part dans le monde, le temps de se retrouver en haute mer, sur le Web non censuré.
> [...]
> L'un des plus populaires et des plus militants est actuellement TOR. En 2001, l'US Navy mettait dans le domaine public un de ses outils d'encryptage des communications. Robert Dingledine, alors étudiant au MIT, a les cheveux longs et les petites lunettes rondes des militants du logiciel libre. C'est lui qui a décidé de l'adapter au Web et de le diffuser gratuitement. TOR, géré aux Etats-unis par une association à but non lucratif, a déjà été téléchargé des millions de fois.
