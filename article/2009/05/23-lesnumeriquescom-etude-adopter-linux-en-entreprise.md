---
site: lesnumeriques.com
title: "Étude : adopter Linux en entreprise"
author: Franck Mée 
date: 2009-05-23
href: http://www.lesnumeriques.com/news_id-8936.html
tags:
- Le Logiciel Libre
- Entreprise
---

> La guerre entre pro et anti-Linux manque parfois d'avis objectifs, des opinions idéologiques se heurtant souvent à un mur d'appréhension dû à l'image «geek» du système au manchot. L'étude de Freeform Dynamics publiée cette semaine est donc intéressante : elle porte sur les observations des entreprises qui ont envisagé Linux et, souvent, l'ont adopté au moins partiellement.
> [...]
>  Pourquoi une entreprise envisage-t-elle de passer au manchot, tout d'abord ? À cette question, une réponse simple : le coût. Plus de 70% des entreprises interrogées le considèrent comme un critère essentiel, pas tant pour les licences de Windows à acheter que pour les frais annexes. Par exemple, un bureau typique sous Windows va accueillir la suite Office, un outil comme Photoshop Elements, une suite de sécurité etc., autant de logiciels payants ; sous Linux, on s'orientera plutôt vers des outils gratuits (Open Office, GIMP...), et l'entreprise n'achètera pas de suite de sécurité en misant sur la rareté des virus.
