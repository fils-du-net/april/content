---
site: Le Telegramme
title: "À Lannion, un ingénieur d'Orange labs primé pour son action dans l'open source"
author: Valérie Le Moigne
date: 2021-04-04
href: https://www.letelegramme.fr/cotes-darmor/lannion/a-lannion-un-ingenieur-d-orange-labs-prime-pour-son-action-dans-l-open-source-04-04-2021-12730351.php
featured_image: https://www.letelegramme.fr/images/2021/04/04/sylvain-desbureaux-ingenieur-chez-orange-vient-de-recevoir_5615473_676x439p.jpg
tags:
- Promotion
series:
- 202113
---

> Sylvain Desbureaux vient de recevoir le Top achievement Award. Une reconnaissance de ses pairs dans le cadre d’un projet Open source auquel il participe avec d’autres collègues chez Orange labs, à Lannion.
