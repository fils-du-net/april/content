---
site: ZDNet France
title: "Circulaire Castex: création d'une mission logiciel libre et communs numériques à la DINUM (€)"
author: Thierry Noisette
date: 2021-04-29
href: https://www.zdnet.fr/blogs/l-esprit-libre/circulaire-castex-creation-d-une-mission-logiciel-libre-et-communs-numeriques-a-la-dinum-39921871.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/04/Jean_Castex_WMC.jpg
tags:
- april
- Institutions
series:
- 202117
---

> Une mission 'dédiée à l'animation et la promotion interministérielles en matière de logiciel libre et de communs numériques' sera mise en place au sein de la Direction interministérielle du numérique, confirme le Premier ministre. Pour l'April, 'un premier pas dans la bonne direction' à confirmer.
