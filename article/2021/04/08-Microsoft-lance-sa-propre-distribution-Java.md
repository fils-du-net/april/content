---
site: InformatiqueNews.fr
title: "Microsoft lance sa propre distribution Java…"
author: Loïc Duval
date: 2021-04-08
href: https://www.informatiquenews.fr/microsoft-lance-sa-propre-distribution-java-78422
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2021/04/shutterstock_1840963633.jpg
tags:
- Entreprise
series:
- 202114
series_weight: 0
---

> Alors qu’Oracle fait le forcing pour se réapproprier le langage et encourager les entreprises à adhérer à ses programmes de licences, Microsoft annonce sa propre distribution de l’OpenJDK…
