---
site: Next INpact
title: "Dortmund bascule dans l'open source"
date: 2021-04-07
href: https://www.nextinpact.com/lebrief/46679/dortmund-bascule-dans-lopen-source
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/square-linked-media/1902.jpg
tags:
- Administration
series:
- 202114
---

> Deux importantes décisions ont été prises par le conseil municipal le 11 février, dont la session n’a été pleinement rendue publique que le 30 mars.
