---
site: Radio-Canada.ca
title: "La NASA adopte des logiciels libres pour son prochain robot lunaire"
date: 2021-04-13
href: https://ici.radio-canada.ca/nouvelle/1784592/nasa-logiciel-libre-robot-lunaire-viper
featured_image: https://images.radio-canada.ca/q_auto,w_960/v1/ici-info/16x9/viper-robot-lunaire-nasa.jpg
tags:
- Sciences
- Innovation
series:
- 202115
series_weight: 0
---

> La mission lunaire de la NASA prévue en 2023 pourrait marquer un nouveau tournant pour l'industrie spatiale.
