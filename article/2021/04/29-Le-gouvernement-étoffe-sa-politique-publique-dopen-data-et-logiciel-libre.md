---
site: Le Monde Informatique
title: "Le gouvernement étoffe sa politique publique d'open data et logiciel libre"
author: Jacques Cheminat
date: 2021-04-29
href: https://www.lemondeinformatique.fr/actualites/lire-le-gouvernement-etoffe-sa-politique-publique-d-open-data-et-logiciel-libre-82773.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000078511.jpg
seeAlso: "[Circulaire données et codes sources: un premier pas dans la bonne direction qui doit être confirmé](https://www.april.org/circulaire-donnees-et-codes-sources-un-premier-pas-dans-la-bonne-direction-qui-doit-etre-confirme)"
tags:
- april
- Institutions
- Open Data
series:
- 202117
series_weight: 0
---

> Le Premier ministre a publié une circulaire sur la politique publique des données, des algorithmes et des codes sources. Création d'administrateurs ministériels des données, préfiguration d'un médiateur de la donnée d'intérêt général, mission interministérielle sur le logiciel libre et élaboration du portail code.gouv.fr sont autant d'initiatives prises.
