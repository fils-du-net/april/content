---
site: EurActiv
title: "Jean Castex invite les services de l'État à ouvrir davantage leurs données et à se tourner vers les logiciels libres"
author: Mathieu Pollet
date: 2021-04-30
href: https://www.euractiv.fr/section/economie/news/jean-castex-invite-les-services-de-letat-a-ouvrir-davantage-leurs-donnees-et-a-se-tourner-vers-les-logiciels-libres
featured_image: https://www.euractiv.fr/wp-content/uploads/sites/3/2021/04/w_56857792-1-800x450.jpg
tags:
- april
- Institutions
- Open Data
series:
- 202117
---

> Dans une circulaire signée cette semaine (27 avril), le Premier ministre Jean Castex rappelle aux services de l'État l'importance «stratégique» d'une politique de la donnée publique «ambitieuse» et les exhorte à s'y impliquer «personnellement».
