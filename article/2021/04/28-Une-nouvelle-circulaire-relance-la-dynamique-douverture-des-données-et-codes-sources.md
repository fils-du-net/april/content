---
site: Acteurs Publics 
title: "Une nouvelle circulaire relance la dynamique d'ouverture des données et codes sources (€)"
author: Emile Marzolf
date: 2021-04-28
href: https://acteurspublics.fr/articles/une-nouvelle-circulaire-relance-la-dynamique-douverture-des-donnees-et-codes-sources
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/34/e15e64fcb613e1b183dfbe3403e6064fcd902d53.jpeg
tags:
- Institutions
- Open Data
series:
- 202117
---

> Dans une circulaire signée le 27 avril, le Premier ministre demande aux ministres de “s’impliquer personnellement” pour faire de la politique de la donnée “une priorité stratégique de l’État” et de renforcer l’ouverture des codes sources et des algorithmes publics et l'usage des logiciels libres. Des indicateurs de pilotage seront mis en place pour évaluer la bonne mise en œuvre de cet engagement par les directeurs d’administrations centrales et déconcentrées
