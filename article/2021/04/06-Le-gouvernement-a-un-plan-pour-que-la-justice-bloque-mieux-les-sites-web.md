---
site: Numerama
title: "Le gouvernement a un plan pour que la justice bloque mieux les sites web"
author: Julien Lausson
date: 2021-04-06
href: https://www.numerama.com/politique/702015-le-gouvernement-a-un-plan-pour-que-la-justice-bloque-mieux-les-sites-web.html
featured_image: https://www.numerama.com/wp-content/uploads/2021/04/sens-interdit.jpg
tags:
- Internet
- Institutions
- april
series:
- 202114
series_weight: 0
---

> Conscient que les mesures de blocage actuelles décidées par la justice peuvent être contournées relativement facilement, le gouvernement profite du projet de loi sur le séparatisme pour réécrire le droit. Objectif: bloquer plus efficacement, en impliquant davantage d'intermédiaires techniques.
