---
site: RFI
title: "Les logiciels libres d'Ingenuity"
author: Dominique Desaunay
date: 2021-04-24
href: https://www.rfi.fr/fr/podcasts/nouvelles-technologies/20210424-les-logiciels-libres-d-ingenuity
featured_image: https://s.rfi.fr/media/display/8afc8768-a505-11eb-8371-005056bf87d6/w:1280/p:16x9/AP21112554055133%20%281%29.webp
tags:
- Sciences
- Sensibilisation
series:
- 202116
series_weight: 0
---

> Les roboticiens de l'agence spatiale américaine ont travaillé d’arrache-pied pour mettre au point le petit hélicoptère martien Ingenuity. L’appareil a réalisé un premier vol historique en complète autonomie sur la Planète rouge. La Nasa a fait appel à des milliers développeurs de logiciels libres du monde entier, qui ont contribué au succès de cet exploit technologique et de programmation informatique!
