---
site: Le Monde Informatique
title: "L'Université du Minnesota interdite de contribution au noyau Linux"
author: Célia Seramour
date: 2021-04-26
href: https://www.lemondeinformatique.fr/actualites/lire-l-universite-du-minnesota-interdite-de-contribution-au-noyau-linux-82727.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000078435.jpg
tags:
- Innovation
series:
- 202117
series_weight: 0
---

> Branle-bas de combat au sein de la communauté open source après l'annonce d'un bannissement de l'Université du Minnesota par la Fondation Linux. En cause, deux chercheurs ayant expérimenté l'ajout de code vulnérable dans le noyau Linux.
