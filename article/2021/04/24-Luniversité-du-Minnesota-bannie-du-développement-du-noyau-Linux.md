---
site: ZDNet France
title: "L'université du Minnesota bannie du développement du noyau Linux (€)"
author: Steven J. Vaughan-Nichols
date: 2021-04-24
href: https://www.zdnet.fr/actualites/l-universite-du-minnesota-bannie-du-developpement-du-noyau-linux-39921579.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/02/Linux%20Illustration%20A%20620__w1200.jpg
tags:
- Innovation
series:
- 202116
series_weight: 0
---

> Certains chercheurs ont essayé de glisser de correctifs non fonctionnels dans le noyau Linux en guise de 'test'. Greg Kroah-Hartman, le mainteneur du noyau Linux, a mis fin à leurs efforts.
