---
site: LeMagIT
title: "La stratégie gagnante d’une migration du poste de travail sous Linux"
author: Christophe Auffray
date: 2021-04-15
href: https://www.lemagit.fr/etude/La-strategie-gagnante-dune-migration-du-poste-de-travail-sous-Linux
featured_image: https://cdn.ttgtmedia.com/visuals/German/article/virtual-desktop-laptop-internet-adobe.jpg
tags:
- Administration
series:
- 202115
series_weight: 0
---

> Dans les municipalités, les migrations des postes de travail sous Linux ont parfois connu des échecs retentissants. Les principales raisons? La méthode et des résistances aux changements sous-estimées. Le témoignage de Nicolas Vivant de la mairie d’Échirolles.
