---
site: ZDNet France
title: "Vidéo: Ingenuity, voler sur Mars grâce aux logiciels libres"
author: Steven J. Vaughan-Nichols
date: 2021-04-20
href: https://www.zdnet.fr/actualites/video-ingenuity-voler-sur-mars-grace-aux-logiciels-libres-39921321.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/09/pia23720-1041__w630.jpg
tags:
- Sciences
- Innovation
series:
- 202116
series_weight: 0
---

> Pour la première fois dans l'histoire, nous avons fait voler un aéronef, le mini-hélicoptère Ingenuity, sur une autre planète. Un logiciel libre a soutenu toute son ingénierie.
