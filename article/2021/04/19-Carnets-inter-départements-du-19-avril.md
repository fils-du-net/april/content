---
site: France Inter
title: "Carnets inter-départements du 19 avril"
author: Philippe Bertrand
date: 2021-04-19
href: https://www.franceinter.fr/emissions/carnets-de-campagne/carnets-de-campagne-19-avril-2021
featured_image: https://cdn.radiofrance.fr/s3/cruiser-production/2016/06/9aa0bf2b-2ca1-4dcd-9be4-1619b8f1c469/1200x680_bertrand_philippe.jpg
tags:
- Sensibilisation
series:
- 202116
series_weight: 0
---

> Faire par soi-même, relocaliser et gagner en indépendance loin des réseaux de la mondialisation qu'ils soient industriels ou numériques, tels sont les thèmes abordés aujourd'hui.
