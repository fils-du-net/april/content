---
site: RTBF Info
title: "Digitalisation de la Justice: le ministre veut relancer le processus, le chantier est immense"
author:  Annick Merckx
date: 2021-04-30
href: https://www.rtbf.be/info/belgique/detail_digitalisation-de-la-justice-le-ministre-veut-relancer-le-processus-le-chantier-est-immense?id=10752519
featured_image: https://ds1.static.rtbf.be/article/image/770x433/d/e/6/3b37d7a275704e1f7dc516e880ad2cc7-1619795931.jpg
tags:
- Administration
- Open Data
series:
- 202117
series_weight: 0
---

> 12.000 nouveaux ordinateurs portables dont 3500 distribués ce vendredi. Le vice-premier et ministre de la Justice, Vincent Van Quickenborne veut faire oublier les ratés du passé. Notre pays est à la traîne de l’Union européenne, dit-il en annonçant, pour les années à venir, quelque 100 millions d’investissement dans le département et un "plan d’approche réaliste pour faire entrer la Justice dans le XXIe siècle".
