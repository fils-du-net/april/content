---
site: Silicon
title: "Amazon en quête de confiance pour son fork d'Elasticsearch"
author: Clément Bohic
date: 2021-04-14
href: https://www.silicon.fr/open-source-amazon-confiance-fork-elasticsearch-opensearch-405096.html
featured_image: https://www.silicon.fr/wp-content/uploads/2021/04/Amazon-OpenSearch.jpg
tags:
- Licenses
- Informatique en nuage
series:
- 202115
series_weight: 0
---

> Sous la marque OpenSearch, Amazon ouvre au public son fork d'Elasticsearch et de Kibana. Le sujet de la gouvernance fait débat au sein de la communauté.
