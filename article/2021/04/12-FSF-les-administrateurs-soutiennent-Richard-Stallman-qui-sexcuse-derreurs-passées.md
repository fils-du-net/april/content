---
site: ZDNet France
title: "FSF: les administrateurs soutiennent Richard Stallman, qui s'excuse d'erreurs passées (€)"
author: Thierry Noisette
date: 2021-04-12
href: https://www.zdnet.fr/blogs/l-esprit-libre/fsf-les-administrateurs-soutiennent-richard-stallman-qui-s-excuse-d-erreurs-passees-39920927.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/03/RMS_GNU_and_Stallman_2012_WMC.JPG
tags:
- Promotion
series:
- 202115
series_weight: 0
---

> La Free Software Foundation justifie le retour au conseil d'administration de son fondateur. RMS publie une lettre où il expose sa maladresse sociale et présente des excuses.
