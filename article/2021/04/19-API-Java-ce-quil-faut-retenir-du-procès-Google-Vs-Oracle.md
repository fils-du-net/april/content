---
site: Le Monde Informatique
title: "API Java, ce qu'il faut retenir du procès Google Vs Oracle"
author: Matt Asay
date: 2021-04-19
href: https://www.lemondeinformatique.fr/actualites/lire-api-java-ce-qu-il-faut-retenir-du-proces-google-vs-oracle-82649.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000078297.jpg
tags:
- Droit d'auteur
- Entreprise
- Institutions
series:
- 202116
series_weight: 0
---

> Peu de juges de la Cour suprême semblent avoir compris ce qu'est ou ce que fait une API. La victoire de Google dans le procès qui l'oppose à Oracle pour copie de l'API Java est aussi celle de tous les développeurs de logiciels, y compris open source, même si elle esquive la question de fond du copyright.
