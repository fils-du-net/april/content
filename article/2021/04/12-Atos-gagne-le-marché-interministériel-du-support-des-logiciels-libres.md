---
site: Le Monde Informatique
title: "Atos gagne le marché interministériel du support des logiciels libres"
author: Jacques Cheminat
date: 2021-04-12
href: https://www.lemondeinformatique.fr/actualites/lire-atos-gagne-le-marche-interministeriel-du-support-des-logiciels-libres-82578.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000078150.jpg
tags:
- Administration
- Marchés publics
series:
- 202115
series_weight: 0
---

> SSII: Pendant 4 ans, Atos en collaboration avec le CNLL aura la charge du support des logiciels libres au sein de l'administration.
