---
site: InformatiqueNews.fr
title: "Programmation: pourquoi tout le monde devrait s'y mettre?"
author: Sébastien Blanc
date: 2021-04-23
href: https://www.informatiquenews.fr/programmation-pourquoi-tout-le-monde-devrait-sy-mettre-sebastien-blanc-red-hat-78837
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2021/04/shutterstock_1032637258.jpg
tags:
- Sensibilisation
series:
- 202116
series_weight: 0
---

> Le secteur du numérique est celui qui connaît la croissance la plus rapide. Selon le Syntec Numérique, c'est le marché le moins impacté par la pandémie
