---
site: Le Monde Informatique
title: "La fondation Linux lance son pôle de recherche sur l'open source"
author: Jacques Cheminat
date: 2021-04-15
href: https://www.lemondeinformatique.fr/actualites/lire-la-fondation-linux-lance-son-pole-de-recherche-sur-l-open-source-82618.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000078243.jpg
tags:
- Innovation
series:
- 202115
series_weight: 0
---

> En complément du soutien à différents projets, la fondation Linux vient de lancer une division dédiée aux études. Elle sera en charge d'analyser et de mieux comprendre les initiatives open source.
