---
site: L'Informaticien
title: "Bagarre ouverte dans l'Open Source autour de la licence Elastic"
author: Thierry Thaureaux
date: 2021-04-19
href: https://linformaticien.com/dossier-bagarre-ouverte-dans-lopen-source-autour-de-la-licence-elastic
featured_image: https://linformaticien.com/wp-content/uploads/2021/04/DEVOPS_ELASTIC_00.jpg
tags:
- Licenses
series:
- 202116
---

> Elastic a passé la licence d’ElasticSearch et Kibana en SSPL, provoquant l’ire de nombre de membres de la communauté open source du projet et la réaction d’AWS avec l’annonce de forks. S’agit-il d’une transition «normale» ou d’une grave atteinte à l’Open Source ?
