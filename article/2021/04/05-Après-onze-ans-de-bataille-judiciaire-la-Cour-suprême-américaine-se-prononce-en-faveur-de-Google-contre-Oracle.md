---
site: Le Monde.fr
title: "Après onze ans de bataille judiciaire, la Cour suprême américaine se prononce en faveur de Google contre Oracle"
date: 2021-04-05
href: https://www.lemonde.fr/pixels/article/2021/04/05/apres-onze-ans-de-bataille-judiciaire-la-cour-supreme-americaine-se-prononce-en-faveur-de-google-contre-oracle_6075633_4408996.html
tags:
- Droit d'auteur
- Institutions
- Entreprise
series:
- 202114
series_weight: 0
---

> La plus haute instance judiciaire des Etats-Unis estime que l’usage du code Java par Google constitue un «usage légitime» en matière de propriété intellectuelle. Le litige portait sur des milliards de dollars.
