---
site: Silicon
title: "Open source: Grafana change à son tour de licence"
author: Clément Bohic
date: 2021-04-23
href: https://www.silicon.fr/open-source-grafana-change-licence-406048.html
featured_image: https://www.silicon.fr/wp-content/uploads/2021/04/Grafana-AGPL.jpg
tags:
- Licenses
- Entreprise
- Informatique en nuage
series:
- 202116
series_weight: 0
---

> Dans la lignée d'Elastic, Grafana Labs abandonne Apache 2.0. Il ne bascule pas vers la même licence, mais ses motivations sont comparables.
