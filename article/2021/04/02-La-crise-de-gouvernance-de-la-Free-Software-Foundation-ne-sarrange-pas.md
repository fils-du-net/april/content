---
site: ZDNet France
title: "La crise de gouvernance de la Free Software Foundation ne s'arrange pas (€)"
author: Steven J. Vaughan-Nichols
date: 2021-04-02
href: https://www.zdnet.fr/actualites/la-crise-de-gouvernance-de-la-free-software-foundation-ne-s-arrange-pas-39920513.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2010/01/richard-stallman-ignucius-184x138__w1200.jpg
seeAlso: "[Retour de Richard Stallman au conseil d'administration de la Fondation pour le Logiciel Libre (FSF)](https://www.april.org/retour-de-richard-stallman-au-conseil-d-administration-de-la-fondation-pour-le-logiciel-libre-fsf)"
tags:
- Promotion
series:
- 202113
series_weight: 0
---

> RMS a été exclu du comité directeur du CCG, tandis que les membres de l'équipe de direction de la FSF démissionnent.
