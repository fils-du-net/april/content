---
site: Vice
title: "La communauté du logiciel libre se déchire autour de son fondateur"
author: Sébastien Wesolowski"
date: 2021-04-02
href: https://www.vice.com/fr/article/dy8adx/la-communaute-du-logiciel-libre-se-dechire-autour-de-son-fondateur
featured_image: https://video-images.vice.com/articles/60649e85201cc4009a55851e/lede/1617262975361-epd5np.jpeg
tags:
- Promotion
series:
- 202113
---

> Richard Stallman est-il un vieux dégueulasse ou l'incarnation de tout un mouvement? Peut-être les deux à la fois, et c'est ce qui pose problème.
