---
site: L'usine Nouvelle
title: "Des chercheurs publient en open source les séquences ARN des vaccins de Pfizer et Moderna"
author: Simon Philippe
date: 2021-04-07
href: https://www.usinenouvelle.com/editorial/des-chercheurs-publient-en-open-source-les-sequences-arn-des-vaccins-de-pfizer-et-moderna.N1079599
featured_image: https://www.usinenouvelle.com/mediatheque/9/0/9/000888909_896x598_c.jpg
tags:
- Sciences
series:
- 202114
series_weight: 0
---

> Des chercheurs de Stanford ont publié en accès libre les séquences ARN des vaccins contre le Covid-19 de Moderna et BioNTech-Pfizer. Un travail qui relance le débat sur le brevetage de ces vaccins et des outils thérapeutiques.
