---
site: Next INpact
title: "Connaître les machines, une question d'autonomie pour les humains (€)"
author: Stéphane Crozat
date: 2021-04-16
href: https://www.nextinpact.com/article/46800/connaitre-machines-question-dautonomie-pour-humains
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/814.jpg
tags:
- Sensibilisation
series:
- 202115
series_weight: 0
---

> 1958. Les premiers ordinateurs sont là. Pour le philosophe Gilbert Simondon, la méconnaissance de la machine est la cause majeure de l’aliénation du monde contemporain, les hommes qui connaissent les objets techniques cherchent à s’imposer en leur conférant le statut d’objets sacrés.
