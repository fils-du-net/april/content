---
site: usine-digitale.fr
title: "Google remporte sa bataille juridique face à Oracle autour des API Java"
author: Léna Corot
date: 2021-04-06
href: https://www.usine-digitale.fr/article/google-remporte-sa-bataille-juridique-face-a-oracle-autour-des-api-java.N1079339
featured_image: https://www.usine-digitale.fr/mediatheque/9/3/6/000944639_homePageUne/google.jpg
tags:
- Droit d'auteur
series:
- 202114
---

> Oracle perd sa longue bataille contre Google. L'entreprise réclamait 9 milliards de dollars à Google pour avoir copié des API Java afin de développer son système d'exploitation Android, aujourd'hui embarqué sur des milliards d'appareils mobiles.
