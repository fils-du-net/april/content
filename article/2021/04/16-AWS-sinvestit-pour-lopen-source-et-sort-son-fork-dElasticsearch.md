---
site: ICTjournal
title: "AWS s'investit pour l'open source et sort son fork d'Elasticsearch"
author: Yannick Chavanne
date: 2021-04-16
href: https://www.ictjournal.ch/articles/2021-04-16/aws-sinvestit-pour-lopen-source-et-sort-son-fork-delasticsearch
feature_image: https://www.ictjournal.ch/sites/default/files/styles/np8_full/public/media/2021/04/16/aws-forks.jpg
tags:
- Licenses
series:
- 202115
---

> AWS a tenu promesse. La firme vient de publier sous licence open source des forks d'Elasticsearch et Kibana.
