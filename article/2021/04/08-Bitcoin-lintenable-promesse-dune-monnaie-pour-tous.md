---
site: The Conversation
title: "Bitcoin: l'intenable promesse d'une monnaie pour tous"
author: Jean-Michel Servet
date: 2021-04-08
href: https://theconversation.com/bitcoin-lintenable-promesse-dune-monnaie-pour-tous-158475
featured_image: https://images.theconversation.com/files/393601/original/file-20210406-13-fcywmg.jpg?ixlib=rb-1.1.0&rect=0%2C0%2C1920%2C1279&q=45&auto=format&w=926&fit=clip
tags:
- Économie
series:
- 202114
series_weight: 0
---

> La cryptomonnaie, loin d'être un «bien commun», apparaît comme un instrument spéculatif qui ne reconnaît pas la participation de parties prenantes dans sa gestion.
