---
site: Le Monde Informatique
title: "Microsoft dévoile sa propre distribution Java"
author: Paul Krill
date: 2021-04-09
href: https://www.lemondeinformatique.fr/actualites/lire-microsoft-devoile-sa-propre-distribution-java-82551.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000078111.png
tags:
- Entreprise
series:
- 202114
---

> Développement et Tests: La Microsoft Build d'OpenJDK pourrait permettre au fournisseur de concurrencer Oracle dans le domaine de la distribution Java.
