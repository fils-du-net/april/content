---
site: Acteurs Publics 
title: "Le ministère de la Santé refuse de dévoiler les rouages techniques du Health Data Hub (€)"
author: Emile Marzolf
date: 2021-04-27
href: https://www.acteurspublics.fr/articles/le-ministere-de-la-sante-refuse-de-devoiler-les-rouages-techniques-du-health-data-hub
featured_image: https://www.acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/28/88f6f42babdece76736bc65d468414ab7cb12e57.jpeg
tags:
- Institutions
series:
- 202117
---

> Le ministère de la Santé a refusé d'ouvrir les codes sources du Health Data Hub pour lever le capot sur les rouages de la plateforme des données de santé. Conforté par un avis de la Cada, celui-ci a invoqué le risque que ferait peser une telle ouverture sur la sécurité des données de santé.
