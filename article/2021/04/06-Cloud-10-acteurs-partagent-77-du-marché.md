---
site: Siècle Digital
title: "Cloud: 10 acteurs partagent 77% du marché"
author: Julia Guinamard
date: 2021-04-06
href: https://siecledigital.fr/2021/04/06/cloud-marche
featured_image: https://thumbor.sd-cdn.fr/u_reaYIfJS5mM_ApH5obLLmuRio=/940x550/cdn.sd-cdn.fr/wp-content/uploads/2021/04/Marche%CC%81-du-cloud-hyperscalers-scaled.jpg
tags:
- Informatique en nuage
- Entreprise
- Internet
series:
- 202114
series_weight: 0
---

> Pendant la pandémie de Covid-19 AWS, Microsoft Azure, Alibaba et quelques autres entreprises ont enregistré une croissance sans précédent.
