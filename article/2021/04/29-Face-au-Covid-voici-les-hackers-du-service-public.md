---
site: Challenges
title: "Face au Covid, voici les hackers du service public"
author: Léa Lejeune
date: 2021-04-29
href: https://www.challenges.fr/high-tech/ces-hackers-du-service-public-qui-proposent-de-nouvelles-solutions-par-algorithmes-pour-lutter-contre-le-covid_762543
featured_image: https://www.challenges.fr/assets/img/2021/04/29/cover-r4x3w1000-608a81c33d136-696-actu-france-hackers-du-service-publicpaul-duan-vincent.jpg
tags:
- Vie privée
- Innovation
series:
- 202117
series_weight: 0
---

> Face au Covid, les hackers du service public proposent de nouvelles solutions qui reposent sur des algorithmes, les moyens et les outils des start-up.
