---
site: Le Monde Informatique
title: "RTE mise sur l'open source pour gérer les réseaux électriques"
author: Célia Seramour
date: 2021-10-08
href: https://www.lemondeinformatique.fr/actualites/lire-rte-mise-sur-l-open-source-pour-gerer-les-reseaux-electriques-84413.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000081398.png
tags:
- Innovation
- Entreprise
- Associations
series:
- 202140
series_weight: 0
---

> L'opérateur de transport d'électricité, RTE, s'est associé à LF Energy et Savoir-faire Linux pour développer une plateforme d'automatisation et de protection open source. L'objectif est d'avoir une solution interopérable pour gérer avec agilité les réseaux électriques.
