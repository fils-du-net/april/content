---
site: ZDNet France
title: "Une plateforme de données territoriales sous licence libre pour La Rochelle"
author: Thierry Noisette
date: 2021-10-28
href: https://www.zdnet.fr/blogs/l-esprit-libre/une-plateforme-de-donnees-territoriales-sous-licence-libre-pour-la-rochelle-39931607.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/10/Hotel_de_Ville_de_La_Rochelle_WMC.jpg
tags:
- Open Data
series:
- 202143
series_weight: 0
---

> La Rochelle Territoire Zéro Carbone prépare une plateforme de données territoriales, sous licence Apache. Elle rassemblera tout type d'information utile à son objectif, la neutralité carbone en 2040.
