---
site: Silicon
title: "6 conseils pour bien démarrer au sein de la communauté open source"
author: Hervé Lemaitre
date: 2021-10-21
href: https://www.silicon.fr/avis-expert/6-conseils-pour-bien-demarrer-au-sein-de-la-communaute-open-source
featured_image: https://www.silicon.fr/wp-content/uploads/2020/02/open-source_photo-via-visualhunt.jpg
tags:
- Sensibilisation
series:
- 202142
series_weight: 0
---

> C'est bien connu, les débuts sont toujours difficiles; une maxime qui s'applique particulièrement aux développeurs et à toute personne désireuse de mettre ses compétences au service de la communauté open source. Ce secteur, en pleine croissance, est au cœur d'enjeux économiques, de souveraineté, de sécurité et d'éthique majeurs.
