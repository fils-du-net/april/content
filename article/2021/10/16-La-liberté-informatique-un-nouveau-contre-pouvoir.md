---
site: LeDevoir.com
title: "La liberté informatique, un nouveau contre-pouvoir"
author: Mathieu Bergeron
date: 2021-10-16
href: https://www.ledevoir.com/opinion/libre-opinion/640794/libre-opinion-la-liberte-informatique-un-nouveau-contre-pouvoir
featured_image: https://media1.ledevoir.com/images_galerie/nwd_1023648_832131/image.jpg
tags:
- Sensibilisation
- Internet
series:
- 202141
series_weight: 0
---

> Dans un sens, l’informatique a déjà été admirablement bien démocratisée, c’est-à-dire rendue accessible au plus grand nombre (surtout depuis l’avènement des téléphones multifonctions). Mais dans le sens le plus profond du terme, qui implique de redonner du pouvoir à la population, l’informatique s’avère de nos jours principalement non démocratique, voire antidémocratique (alors que l’industrie semble trop souvent reposer sur des privations de droits: violation de la vie privée, appareils artificiellement impossibles à réparer, décisions prises par des algorithmes qui n’ont pas la capacité d’expliquer leur raisonnement).
