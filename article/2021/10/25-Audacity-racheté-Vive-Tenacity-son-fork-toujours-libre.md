---
site: Clubic.com
title: "Audacity racheté? Vive Tenacity, son fork toujours libre"
author: Florent Lanne
date: 2021-10-25
href: https://www.clubic.com/telecharger/actus-logiciels/actualite-389958-audacity-rachete-vive-tenacity-son-fork-toujours-libre.html
featured_image: https://pic.clubic.com/v1/images/1930438/raw.webp?fit=max&width=1200&hash=2d569efbd4843a3ee37ebc9fde34675e781a33db
tags:
- Innovation
series:
- 202143
series_weight: 0
---

> Le logiciel d’enregistrement audionumérique Audacity n’est plus libre. En revanche, un fork est en ligne.

Note: à notre connaissance, audacity est toujours distribué sous license Libre...
