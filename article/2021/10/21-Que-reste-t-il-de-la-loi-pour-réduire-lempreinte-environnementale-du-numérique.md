---
site: 01net.
title: "Que reste-t-il de la loi pour réduire l'empreinte environnementale du numérique?"
author: Amélie Charnay
date: 2021-10-21
href: https://www.01net.com/actualites/que-reste-t-il-de-la-loi-pour-reduire-l-empreinte-environnementale-du-numerique-2050134.html
featured_image: https://img.bfmtv.com/c/630/420/80e0ba4/3825a9b7f7b36d9ca8d7df509.jpeg
tags:
- Institutions
series:
- 202142
series_weight: 0
---

> Alors que le projet de loi s'apprête à passer en seconde lecture au Sénat, on peut s'interroger sur les reculs d'un texte qui se voulait initialement très ambitieux.
