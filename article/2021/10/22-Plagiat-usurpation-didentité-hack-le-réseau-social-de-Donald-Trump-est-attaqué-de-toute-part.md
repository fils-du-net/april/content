---
site: Numerama
title: "Plagiat, usurpation d'identité, hack... le réseau social de Donald Trump est attaqué de toute part"
author: Corentin Bechade
date: 2021-10-22
href: https://www.numerama.com/tech/749706-plagiat-usurpation-didentite-hack-le-reseau-social-de-donald-trump-est-attaque-de-toute-part.html
featured_image: https://www.numerama.com/wp-content/uploads/2020/11/donald-trump-1.jpg
tags:
- Licenses
series:
- 202142
series_weight: 0
---

> Le réseau social créé par l'ancien président américain Donald Trump a tout juste 2 jours d'existence, mais il fait déjà face à de nombreux problèmes. Entre hack et accusations de plagiats, Truth Social est attaqué de toute part.
