---
site: cio-online.com
title: "Stéphane Rousseau (DSI, Eiffage): «l'open-source est une chance pour les entreprises»"
author: Bertrand Lemaire
date: 2021-10-29
href: https://www.cio-online.com/actualites/lire-stephane-rousseau-dsi-eiffage--l-open-source-est-une-chance-pour-les-entreprises-13622.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000017726.jpg
tags:
- Entreprise
- Promotion
series:
- 202143
series_weight: 0
---

> Le 9 novembre 2021 aura lieu à Paris l'Open CIO Summit dont le parrain est Stéphane Rousseau, DSI du groupe Eiffage et vice-président du Cigref.
