---
site: Next INpact
title: "L'extension de la redevance au reconditionné gagne un cran au Sénat (€)"
description: "Texte remis à neuf"
author: Marc Rees
date: 2021-10-21
href: https://www.nextinpact.com/article/48543/lextension-redevance-au-reconditionne-gagne-cran-au-senat
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/10044.jpg
tags:
- Institutions
series:
- 202142
---

> Sans surprise, au Sénat, la commission de développement durable a adopté «conforme» la proposition de loi sur l’empreinte environnementale, extension de la redevance comprise. Le texte part pour la séance, prévue le 2 novembre prochain. La manifestation des reconditionneurs n'a pour l'heure rien changé face à ce bulldozer législatif.
