---
site: Next INpact
title: "Empreinte environnementale du numérique: vers un vote conforme au Sénat, copie privée comprise"
date: 2021-10-18
href: https://www.nextinpact.com/lebrief/48495/empreinte-environnementale-numerique-vers-vote-conforme-au-senat-copie-privee-comprise
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/wide-linked-media/5139.jpg
tags:
- Institutions
series:
- 202142
---

> La proposition de loi visant à réduire l’empreinte environnementale du numérique entame son examen au Sénat. Le 20 octobre en Commission de l'aménagement du territoire et du développement, le 2 novembre en séance.
