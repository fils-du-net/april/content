---
site: ZDNet France
title: "Open Source: Alter Way vendu à Smile (€)"
date: 2021-10-12
href: https://www.zdnet.fr/actualites/open-source-alter-way-vendu-a-smile-39930605.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2021/09/alterway_1200__w1200.jpg
tags:
- Entreprise
series:
- 202141
---

> Alter Way est cédé par le groupe Econocom, qu'il avait intégré ce groupe il y a six ans. La marque Alter Way devient dans ce nouveau projet le fer de lance de l'offre infrastructure et cloud du groupe Smile, et ses services applicatifs sont intégrés aux offres du groupe Smile.
