---
site: L'Echo
title: "Décompiler un logiciel reste prohibé, sauf erreur à corriger"
author:  Michel Lauwers
date: 2021-10-19
href: https://www.lecho.be/entreprises/tic/decompiler-un-logiciel-reste-prohibe-sauf-erreur-a-corriger/10339910.html
featured_image: https://images.lecho.be/view?iid=Elvis:EJ_FfUp8KxxBxuAbwC9cU5&context=ONLINE&ratio=16/9&width=1280&u=1634643166000
tags:
- Droit d'auteur
series:
- 202142
---

> Le Selor pouvait-il corriger d'autorité une erreur dans un programme acheté à Top Systems? La Cour européenne de justice répond oui, avec des garde-fous.
