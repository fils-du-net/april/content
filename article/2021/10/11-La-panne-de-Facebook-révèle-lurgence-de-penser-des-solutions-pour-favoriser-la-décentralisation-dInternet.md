---
site: Le Monde.fr
title: "«La panne de Facebook révèle l'urgence de penser des solutions pour favoriser la décentralisation d'Internet» (€)"
author: Frédérick Douzet
date: 2021-10-11
href: https://www.lemonde.fr/idees/article/2021/10/11/la-panne-de-facebook-revele-l-urgence-de-penser-des-solutions-pour-favoriser-la-decentralisation-d-internet_6097872_3232.html
tags:
- Informatique en nuage
- Internet
series:
- 202141
---

> TRIBUNE. La défaillance du réseau social et de ses applications, lundi 4 octobre, illustre magistralement une faille importante de cybersécurité et met en lumière la trop grande concentration du trafic des données autour de quelques acteurs majeurs, prévient le professeur de géopolitique Frédérick Douzet.
