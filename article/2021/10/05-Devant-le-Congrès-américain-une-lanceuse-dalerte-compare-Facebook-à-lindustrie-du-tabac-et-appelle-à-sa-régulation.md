---
site: Le Monde.fr
title: "Devant le Congrès américain, une lanceuse d’alerte compare Facebook à «l’industrie du tabac» et appelle à sa régulation (€)"
author: Damien Leloup et Alexandre Piquard
date: 2021-10-05
href: https://www.lemonde.fr/pixels/article/2021/10/05/devant-le-congres-americain-une-lanceuse-d-alerte-compare-facebook-a-l-industrie-du-tabac-et-appelle-a-sa-regulation_6097257_4408996.html
featured_image: https://img.lemde.fr/2021/10/05/0/0/3500/2333/1328/0/45/0/01c5e6d_83390369ba424ab39d78c03652c24a2f-83390369ba424ab39d78c03652c24a2f-0.jpg
tags:
- Internet
- Entreprise
series:
- 202140
series_weight: 0
---

> L’audition au Sénat de Frances Haugen, qui a transmis des milliers de pages de documents internes de l’entreprise à des médias et des régulateurs, était le point culminant d’une série de mises en cause.
