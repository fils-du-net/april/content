---
site: RFI
title: "Droits voisins: en Irlande, Google débute un nouveau partenariat avec la presse en ligne"
author: Emeline Vin
date: 2021-10-25
href: https://www.rfi.fr/fr/europe/20211025-droits-voisins-en-irlande-google-d%C3%A9bute-un-nouveau-partenariat-avec-la-presse-en-ligne
featured_image: https://s.rfi.fr/media/display/645d37b2-26c2-11ec-a21f-005056bfb2b6/w:1280/p:16x9/5e328c45dcc8c2bd0108e28e7927d30e51bd0e18.webp
tags:
- Droit d'auteur
- Entreprise
- Europe
series:
- 202143
---

> En Irlande, huit groupes de presse viennent de signer un accord avec Google, pour autoriser le moteur de recherche à utiliser certains de leurs articles. Google News Showcase, c'est le nom de cette nouvelle offre qui propose gratuitement aux internautes des contenus enrichis.
