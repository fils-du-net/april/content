---
site: Reporterre
title: "Contre l’obsolescence des ordinateurs, des ateliers de «bidouille numérique»"
author: Moran Kerinec
date: 2021-10-04
href: https://reporterre.net/Contre-l-obsolescence-des-ordinateurs-des-ateliers-de-bidouille-numerique
featured_image: https://reporterre.net/local/cache-vignettes/L720xH480/arton23619-9d677.jpg?1632773704
tags:
- Associations
- Sensibilisation
- Logiciels privateurs
series:
- 202140
series_weight: 0
---

> Une nouvelle version de Windows — la onzième — arrive le 5 octobre. Elle provoquera la mise au rebut d’une pléthore d’ordinateurs devenus inadaptés. Cette gabegie de matériel informatique n’est pas une fatalité. Des associations organisent des soirées ouvertes à tous pour apprendre à installer des logiciels libres, alternatives plus sobres à Microsoft et MacOS.
