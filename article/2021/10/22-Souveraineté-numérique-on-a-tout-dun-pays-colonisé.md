---
site: Next INpact
title: "Souveraineté numérique: «on a tout d'un pays colonisé» (€)"
description: "Cédric O rhabillé pour l’hiver qui approche"
author: Sébastien Gavois
date: 2021-10-22
href: https://www.nextinpact.com/article/48461/souverainete-numerique-on-a-tout-dun-pays-colonise
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/10029.jpg
tags:
- Informatique en nuage
- Institutions
series:
- 202142
series_weight: 0
---

> La semaine dernière, l’association NAOS (Nouvelle-Aquitaine Open Source) organisait à La Rochelle la deuxième édition de son B.Boost, un salon sur le logiciel libre et l’open source. Une des premières conférences avait pour thème la souveraineté numérique et la résilience. Nous étions sur place, voici notre compte rendu.
