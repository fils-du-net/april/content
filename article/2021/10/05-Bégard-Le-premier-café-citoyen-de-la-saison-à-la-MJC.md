---
site: ouest-france.fr
title: "Bégard. Le premier café citoyen de la saison à la MJC"
date: 2021-10-05
href: https://www.ouest-france.fr/bretagne/begard-22140/le-premier-cafe-citoyen-de-la-saison-a-la-mjc-4fc1606d-41e0-449a-afe8-b2d7ce8df693
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMTEwZTBiMzIxNTJjOTlhYTQ3NjdkNmYwYTM0OTEyOWJjNzc?width=1260&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=61910d56d362b99f58b7a245f4212376c773fb4249f1f4431ff3365ee69e46a5
tags:
- Promotion
- Associations
series:
- 202140
series_weight: 0
---

> Dans le cadre de la deuxième édition «Un pas pour ma planète», organisée par Guingamp-Paimpol agglomération et en partenariat avec la Maison des jeunes et de la culture (MJC). L’organisme a organisé son premier café citoyen depuis la rentrée scolaire. L’association Infothema a présenté une conférence autour du thème: «Dégooglisons nos ordinateurs».
