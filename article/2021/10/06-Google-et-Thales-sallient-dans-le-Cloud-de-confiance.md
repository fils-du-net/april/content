---
site: Le Monde.fr
title: "Google et Thales s'allient dans le «Cloud de confiance»"
author: Alexandre Piquard et Vincent Fagot
date: 2021-10-06
href: https://www.lemonde.fr/economie/article/2021/10/06/google-et-thales-s-allient-dans-le-cloud-de-confiance_6097303_3234.html
tags:
- Informatique en nuage
series:
- 202140
---

> Les deux géants annoncent la création d’une entité chargée de développer «une offre de cloud souverain», en adéquation avec le label français lancé en mai. Mais cette solution, dépendante d’une entreprise américaine, inquiète les acteurs du secteur.
