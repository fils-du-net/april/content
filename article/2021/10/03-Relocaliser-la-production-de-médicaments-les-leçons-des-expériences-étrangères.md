---
site: L'Express.fr
title: "Relocaliser la production de médicaments: les leçons des expériences étrangères"
author: Stéphanie Benz
date: 2021-10-03
href: https://www.lexpress.fr/actualite/sciences/relocaliser-la-production-de-medicaments-les-lecons-des-experiences-etrangeres_2159724.html
featured_image: https://static.lexpress.fr/medias_11484/w_2048,h_1146,c_crop,x_0,y_41/w_1936,h_1090,c_fill,g_north/v1632084700/les-francais-ont-rapporte-l-an-passe-pres-de-12-000-tonnes-de-medicaments-non-utilises-en-pharmacie-soit-63-du-total-estime-a-indique-mardi-l-association-cyclamed-chargee-d-organiser-la-recuperation-de-ces-medicaments_5880213.jpg
tags:
- Sciences
series:
- 202139
series_weight: 0
---

> L'Observatoire de la transparence dans la politique des médicaments publie un rapport sur la maîtrise de leurs approvisionnements par les pays européens.
