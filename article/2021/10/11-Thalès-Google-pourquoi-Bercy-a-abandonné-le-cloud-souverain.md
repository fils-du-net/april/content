---
site: Challenges
title: "Thalès-Google: pourquoi Bercy a abandonné le cloud souverain (€)"
author: Paul Loubière
date: 2021-10-11
href: https://www.challenges.fr/high-tech/accord-thales-google-bercy-abandonne-le-cloud-souverain-pour-le-plus-grand-bonheur-des-gafam_784302
featured_image: https://www.challenges.fr/assets/img/2021/08/04/cover-r4x3w1000-61640fdb435ee-f8dd86e34ae71c07a3e979181ec8556c54bae491-jpg.jpg
tags:
- Informatique en nuage
- Administration
- Entreprise
series:
- 202141
series_weight: 0
---

> La France remplace le cloud souverain par un cloud de confiance. Ce nouveau concept, moins contraignant, permet aux géants américains en collaboration avec des entreprises françaises de s'inviter dans les données des administrations publiques. Les hébergeurs français s'estiment trahis.
