---
site: La Tribune
title: "Décompiler un logiciel n'est pas légal"
author: Jean-Jacques Quisquater et Charles Cuvelliez
date: 2021-10-18
href: https://www.latribune.fr/opinions/tribunes/decompiler-un-logiciel-n-est-pas-legal-894600.html
featured_image: https://static.latribune.fr/full_width/896748/composite-quisquater-cuvelliez.jpg
tags:
- Droit d'auteur
- Institutions
- Logiciels privateurs
series:
- 202142
series_weight: 0
---

> OPINION. Un arrêt de la Cour européenne de Justice du 6 octobre dernier fera date car il prend position sur la décompilation des logiciels, ce que cela veut dire exactement et l'atteinte qu'elle porte à la propriété intellectuelle de leurs concepteurs.
