---
site: InformatiqueNews.fr
title: "Alter Way s'apprête à rejoindre Smile"
author: cpresse
date: 2021-10-12
href: https://www.informatiquenews.fr/alter-way-sapprete-a-rejoindre-smile-81959
tags:
- Entreprise
series:
- 202141
series_weight: 0
---

> Après 6 années fructueuses de croissance au sein du groupe Econocom, Alter Way, acteur emblématique de l’open source en France spécialiste notamment du conseil, des services managés et de l’hébergement de plateformes digitales autour du cloud, s’apprête à rejoindre Smile, leader européen du digital et de l’open source.
