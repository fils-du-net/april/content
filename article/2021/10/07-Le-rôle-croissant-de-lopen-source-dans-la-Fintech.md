---
site: ZDNet France
title: "Le rôle croissant de l'open source dans la Fintech (€)"
author: Steven J. Vaughan-Nichols
date: 2021-10-07
href: https://www.zdnet.fr/actualites/le-role-croissant-de-l-open-source-dans-la-fintech-39930413.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/10/OpenSourceSoftware__w1200.jpg
tags:
- Innovation
series:
- 202140
---

> Les dirigeants des entreprises du secteur financier savent que les méthodes et les logiciels open source sont utiles à leurs activités, mais ils hésitent à les adopter. Voici pourquoi.
