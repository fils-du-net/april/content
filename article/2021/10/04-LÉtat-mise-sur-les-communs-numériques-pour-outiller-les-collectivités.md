---
site: Acteurs Publics 
title: "L'État mise sur les communs numériques pour outiller les collectivités (€)"
author: Emile Marzolf
date: 2021-10-04
href: https://www.acteurspublics.fr/articles/letat-mise-sur-les-communs-numeriques-pour-outiller-les-collectivites
featured_image: https://www.acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/33/aaf3cfe91d6ca020ca276b4527929391243445a1.jpeg
tags:
- Administration
- Open Data
series:
- 202140
series_weight: 0
---

> Dans sa feuille de route intitulée “Données, codes sources et algorithmes”, publiée le 27 septembre, le ministère de la Cohésion des territoires mise sur l’ouverture des données et des codes sources pour faire émerger des “communs numériques” au bénéfice de l’ensemble des collectivités locales.
