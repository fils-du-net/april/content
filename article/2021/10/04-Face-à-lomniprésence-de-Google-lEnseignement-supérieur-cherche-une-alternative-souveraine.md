---
site: usine-digitale.fr
title: "Face à l'omniprésence de Google, l'Enseignement supérieur cherche une alternative souveraine"
author: Alice Vitard
date: 2021-10-04
href: https://www.usine-digitale.fr/article/face-a-l-omnipresence-de-google-l-enseignement-superieur-cherche-une-alternative-souveraine.N1146987
featured_image: https://www.usine-digitale.fr/mediatheque/7/9/1/001000197_homePageUne/universite.jpg
tags:
- Éducation
series:
- 202140
series_weight: 0
---

> Le ministère de l'enseignement supérieur et de la recherche veut une alternative à Google pour les besoins des étudiants, enseignants et chercheurs. L'association Framasoft, promotrice du logiciel libre, a été contactée pour avis par les services de Frédérique Vidal. Une initiative qui fait suite à un avis de la Cnil.
