---
site: Next INpact
title: "Quatre associations demandent à Microsoft de participer aux surcoûts engendrés par ses patchs de sécurité"
date: 2021-10-12
href: https://www.nextinpact.com/lebrief/48412/quatre-associations-demandent-a-microsoft-participer-aux-surcouts-engendres-par-ses-patchs-securite
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/wide-linked-media/3139.jpg
tags:
- Logiciels privateurs
- Associations
series:
- 202141
series_weight: 0
---

> En réponse au lancement de Windows 11, quatre associations européennes d’utilisateurs appellent Microsoft «à mettre en cohérence son discours public et la réalité de sa politique commerciale» en matière d'empreinte environnementale et sécurité numérique. Selon les quatre associations, la belge Beltug, la française Cigref (ex-Club informatique des grandes entreprises françaises), CIO Platform Nederland et VOICE, l’association des organisations utilisatrices allemandes: «alors que Microsoft communique largement sur ses engagements en matière de sustainability, le cycle de vie de ses produits et services provoque une implacable logique d’obsolescence programmée de parcs d’équipements parfaitement fonctionnels».
