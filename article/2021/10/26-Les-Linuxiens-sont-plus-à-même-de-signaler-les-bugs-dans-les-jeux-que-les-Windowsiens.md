---
site: Clubic.com
title: "Les Linuxiens sont plus à même de signaler les bugs dans les jeux que les Windowsiens..."
author: Fanny Dufour
date: 2021-10-26
href: https://www.clubic.com/linux-os/actualite-389989-les-linuxiens-plus-a-meme-de-signaler-les-bugs-dans-les-jeux-que-les-windowsiens.html
featured_image: https://pic.clubic.com/v1/images/1928736/raw.webp?hash=69296c0d97475639de11169bbf40935ac0261d49
tags:
- Innovation
- Sensibilisation
series:
- 202143
series_weight: 0
---

> D'après le développeur du jeu ΔV: Rings of Saturn, les utilisateurs sous Linux rapportent plus de bugs, et d'une meilleure façon, que ceux évoluant sur d'autres OS.
