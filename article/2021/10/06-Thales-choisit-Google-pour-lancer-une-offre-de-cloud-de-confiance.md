---
site: Les Numeriques
title: "Thales choisit Google pour lancer une offre de 'cloud de confiance'"
author: Patrick Randall
date: 2021-07-01
href: https://www.lesnumeriques.com/pro/thales-choisit-google-pour-lancer-une-offre-de-cloud-de-confiance-n169527.html
featured_image: https://dyw7ncnq1en5l.cloudfront.net/optim/news/16/169527/0a29ea5a-thales-choisit-google-pour-lancer-une-offre-de-cloud-de-confiance__w800.webp
tags:
- Informatique en nuage
- Institutions
- Entreprise
series:
- 202140
series_weight: 0
---

> Après le partenariat cloud entre Orange, Capgemini et Microsoft, c'est au tour du français Thales de s'allier à un géant américain, Google, pour lancer une nouvelle offre de 'cloud de confiance'. Une stratégie loin de faire l'unanimité.
