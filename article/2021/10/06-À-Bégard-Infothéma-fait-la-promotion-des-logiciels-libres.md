---
site: Le Telegramme
title: "À Bégard, Infothéma fait la promotion des logiciels libres"
date: 2021-10-06
href: https://www.letelegramme.fr/cotes-darmor/begard/a-begard-infothema-fait-la-promotion-des-logiciels-libres-06-10-2021-12841461.php
featured_image: https://www.letelegramme.fr/images/2021/10/06/optez-pour-les-logiciels-libres-ils-presentent-davantage_5936885_676x418p.jpg?v=1
tags:
- Promotion
series:
- 202140
---

> Samedi matin, une douzaine de personnes ont participé à la réunion organisée par Infothéma, à Bégard. Cette dernière a le problème des enregistrements de données lors de communications avec les médias Gafam (Google, Amazon, Facebook, Microsoft; etc.). Ces données sont ensuite transmises notamment à des sociétés commerciales.
