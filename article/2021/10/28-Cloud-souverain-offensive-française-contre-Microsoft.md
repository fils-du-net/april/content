---
site: Entreprendre.fr
title: "Cloud souverain: offensive française contre Microsoft"
author: Angelina Hubner
date: 2021-10-28
href: https://www.entreprendre.fr/cloud-souverain-offensive-francaise-contre-microsoft
featured_image: https://www.entreprendre.fr/wp-content/uploads/AdobeStock_158448081-1068x713.jpg
tags:
- Informatique en nuage
- Entreprise
series:
- 202143
series_weight: 0
---

> Suite au retard pris par le cloud souverain, 8 acteurs français de la filière se regroupe dans un collectif afin de proposer un «cloud de confiance», une alternative à Microsoft 365. L’objectif est de prouver qu’il est possible de se passer des acteurs américains et de la bureautique de Microsoft.
