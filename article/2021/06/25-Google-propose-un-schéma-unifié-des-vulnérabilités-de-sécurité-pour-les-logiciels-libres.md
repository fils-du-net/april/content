---
site: ZDNet France
title: "Google propose un schéma unifié des vulnérabilités de sécurité pour les logiciels libres"
author: Steven J. Vaughan-Nichols
date: 2021-06-25
href: https://www.zdnet.fr/actualites/google-propose-un-schema-unifie-des-vulnerabilites-de-securite-pour-les-logiciels-libres-39925213.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/02/open-source-code-620__w1200.jpg
tags:
- Sensibilisation
series:
- 202125
---

> Avant de pouvoir comprendre quelque chose, il faut le mesurer. Google propose un moyen de mesurer les erreurs de sécurité dans les logiciels libres.
