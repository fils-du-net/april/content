---
site: Global Security Mag
title: "LINAGORA remporte l'appel d'offre de la Centrale d'Achats de l'Informatique Hospitalière (CAIH)"
author: Marc Jacob
date: 2021-06-18
href: https://www.globalsecuritymag.fr/LINAGORA-remporte-l-appel-d-offre,20210616,112876.html
tags:
- Marchés publics
- Entreprise
series:
- 202124
series_weight: 0
---

> Après avoir remporté le grand marché national «expertise des logiciels libres» conclue pour 26 Administrations et établissements publics, il y a quelques semaines, LINAGORA a été choisi par la CAIH, pour une durée de 4 ans en tant que fournisseur privilégié d’écosystèmes informatiques globaux en open source, incluant une image de poste de travail LINUX, des solutions de bases de données et de travail collaboratif, ainsi que l’ensemble des prestations de services nécessaires. Dans ce cadre, les 2000 établissements adhérents de la CAIH qui souhaitent se libérer de solutions propriétaires pourront décider d’envisager l’utilisation de logiciels libres, fiables et éprouvés, avec un retour sur investissement rapide.
