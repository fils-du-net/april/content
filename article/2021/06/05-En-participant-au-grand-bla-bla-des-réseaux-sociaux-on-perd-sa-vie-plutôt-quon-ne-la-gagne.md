---
site: France Inter
title: "\"En participant au grand bla bla des réseaux sociaux, on perd sa vie plutôt qu'on ne la gagne\""
author: Christine Siméone
date: 2021-06-05
href: https://www.franceinter.fr/culture/en-participant-au-grand-bla-bla-des-reseaux-sociaux-on-perd-sa-vie-plutot-qu-on-ne-la-gagne
featured_image: https://cdn.radiofrance.fr/s3/cruiser-production/2021/06/01e305a2-ed78-4503-ad22-c60e1c51976b/1136_71owv2kbdws.webp
tags:
- Sensibilisation
series:
- 202122
series_weight: 0
---

> Quels conseils de stratégie numérique donner à des candidats à l'élection présidentielle? On en trouve quelques uns dans un roman réjouissant et érudit "Paresse pour tous". L'auteur Hadrien Klent y défend l'informatique libre, les réseaux sociaux alternatifs, et la journée de travail de 3 heures seulement.
