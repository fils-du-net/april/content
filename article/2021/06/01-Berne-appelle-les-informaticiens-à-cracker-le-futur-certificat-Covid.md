---
site: Heidi.news
title: "Berne appelle les informaticiens à «cracker» le futur certificat Covid (€)"
author: Lorène Mesot
date: 2021-06-01
href: https://www.heidi.news/sante/berne-appelle-les-informaticiens-a-cracker-le-futur-certificat-covid
featured_image: https://heidi-17455.kxcdn.com/photos/99af9473-f03c-4954-a447-b0ffd1415c34/large
tags:
- Vie privée
- Institutions
series:
- 202122
series_weight: 0
---

> Les premiers certificats Covid devraient être délivrés dès le 7 juin. A une semaine de leur introduction, ce lundi 31 mai, la Confédération publie en open source le code de la première version du certificat, dans le but de réaliser un test public de sécurité à large échelle. Autrement dit, les professionnels de la cybersécurité et toute autre personne intéressée peuvent désormais mettre à l’épreuve le code source et tenter d’identifier des failles.
