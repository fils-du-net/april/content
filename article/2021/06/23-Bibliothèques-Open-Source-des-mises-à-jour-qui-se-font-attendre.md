---
site: Silicon
title: "Bibliothèques Open Source: des mises à jour qui se font attendre"
author: Ariane Beky
date: 2021-06-23
href: https://www.silicon.fr/bibliotheques-open-source-mises-a-jour-411006.html
featured_image: https://www.silicon.fr/wp-content/uploads/2020/02/open-source_photo-via-visualhunt.jpg
tags:
- Sensibilisation
series:
- 202125
---

> Près de 80% des bibliothèques tierces utilisées dans les applications ne sont jamais mises à jour par les développeurs qui les intégrent.
