---
site: Silicon
title: "La filière open source française en attend plus du secteur public"
author: Clément Bohic
date: 2021-06-02
href: https://www.silicon.fr/filiere-open-source-francaise-secteur-public-408916.html
featured_image: https://www.silicon.fr/wp-content/uploads/2021/06/CNLL-open-source-action-publique.jpg
tags:
- Institutions
series:
- 202122
---

> En matière d'open source, comment les offreurs perçoivent-ils l'action publique? Une enquête du CNLL fournit des indicateurs.
