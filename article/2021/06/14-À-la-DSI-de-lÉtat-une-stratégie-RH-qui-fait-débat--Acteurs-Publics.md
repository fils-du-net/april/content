---
site: Acteurs Publics 
title: "À la DSI de l'État, une stratégie RH qui fait débat (€)"
author: Emile Marzolf
date: 2021-06-14
href: https://acteurspublics.fr/articles/a-la-dsi-de-letat-une-strategie-rh-qui-fait-debat
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/29/f35d2466bc51442711f3591b6d5fb535105194a8.jpeg
tags:
- Institutions
- Administration
series:
- 202124
---

> Alors que le ministère de la Transformation et de la Fonction publiques remet un coup d’accélérateur pour attirer et fidéliser les “talents du numérique”, le directeur interministériel du numérique, Nadi Bou Hanna, a mis en place une politique désormais assumée – et contestée – de non-renouvellement des agents arrivés au bout de leurs deux CDD.
