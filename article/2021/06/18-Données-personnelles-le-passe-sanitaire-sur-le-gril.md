---
site: l'Humanité.fr
title: "Données personnelles: le passe sanitaire sur le gril (€)"
date: 2021-06-15
author: Lola Scandella
href: https://www.humanite.fr/donnees-personnelles-le-passe-sanitaire-sur-le-gril-710839
featured_image: https://www.humanite.fr/sites/default/files/styles/1048x350/public/images/86554.HR.jpg?itok=qsT2ZWpD
tags:
- Vie privée
- Institutions
series:
- 202124
series_weight: 0
---

> Pour l'association La Quadrature du net, le dispositif n'est pas conforme à la loi, du point de vue du traitement des données personnelles.
