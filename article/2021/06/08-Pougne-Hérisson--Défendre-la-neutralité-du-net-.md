---
site: ouest-france.fr
title: "Pougne-Hérisson. «Défendre la neutralité du net» (€)"
date: 2021-06-08
href: https://www.ouest-france.fr/nouvelle-aquitaine/pougne-herisson-79130/defendre-la-neutralite-du-net-849a5ee3-18dd-472b-89ae-34098c7b1dfd
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMTA2YzZjZWEwOGM1MTZlZTk5MmZlNDg0NGUyMTdhMjM0ZGQ?width=1260&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=d579c39469fb58aa7ecf9ea255b4aa4f301aa06f8fb3f5ebece987b378b3ab0e
tags:
- Neutralité du net
series:
- 202123
series_weight: 0
---

> Simon Descarpentries, ingénieur en informatique, vient de créer son entreprise Meta-press.es. Il a créé un moteur de recherche dédié à la presse, une solution alternative à Google actualités.
