---
site: Next INpact
title: "Copie privée: les députés adoptent la redevance sur les tablettes et téléphones reconditionnés (€)"
description: "Pile, je gagne. Face, tu perds"
author: Marc Rees
date: 2021-06-11
href: https://www.nextinpact.com/article/46229/copie-privee-deputes-adoptent-redevance-sur-reconditionne
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/9842.jpg
tags:
- Droit d'auteur
- Institutions
- Économie
series:
- 202124
series_weight: 0
---

> Un exploit. Dans le cadre d’une loi qui devait alléger le poids environnemental du numérique, ils ont donc réussi à frapper le reconditionné d’une redevance culturelle. Compte rendu des échanges, explications de l’amendement gouvernemental adopté et calendrier de déploiement de cette nouvelle ponction asséné aux acteurs de l’écologie.
