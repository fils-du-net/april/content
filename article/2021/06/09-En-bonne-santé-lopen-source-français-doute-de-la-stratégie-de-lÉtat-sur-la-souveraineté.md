---
site: Next INpact
title: "En bonne santé, l'open source français doute de la stratégie de l'État sur la souveraineté (€)"
description: "D'amour et d'eau fraîche"
author: Vincent Hermann
date: 2021-06-09
href: https://www.nextinpact.com/article/46130/en-bonne-sante-lopen-source-francais-doute-strategie-letat-sur-souverainete
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/1902.jpg
tags:
- Entreprise
- Institutions
series:
- 202123
series_weight: 0
---

> Le CNLL (Conseil national du logiciel libre) publiait il y a quelques jours un bilan affichant, dans les grandes lignes, un secteur français de l’open source en pleine croissance. Mais avec d’importants doutes sur la stratégie actuellement menée par l’État français sur la souveraineté et l’industrie logicielle.
