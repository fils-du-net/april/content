---
site: Le Monde Informatique
title: "Plaidoyer de l'open source Français sur la souveraineté et le business"
author: Dominique Filippone
date: 2021-06-02
href: https://www.lemondeinformatique.fr/actualites/lire-plaidoyer-de-l-open-source-francais-sur-la-souverainete-et-le-business-83125.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000079150.jpg
tags:
- Institutions
- Entreprise
- Économie
series:
- 202122
series_weight: 0
---

> La dernière étude menée par le CNLL fait ressortir des doutes concernant la stratégie industrielle open source de la France. Manque d'encouragement des administrations à utiliser des logiciels libres et formats ouverts et rouleau compresseur des grands fournisseurs étrangers ne facilitent pas leur adoption massive et pérenne.
