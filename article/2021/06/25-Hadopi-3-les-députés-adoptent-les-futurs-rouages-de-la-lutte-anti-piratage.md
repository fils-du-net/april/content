---
site: Next INpact
title: "Hadopi 3: les députés adoptent les futurs rouages de la lutte anti-piratage (€)"
description: "2009-2021"
author: Marc Rees
date: 2021-06-25
href: https://www.nextinpact.com/article/47511/hadopi-3-deputes-adoptent-futurs-rouages-lutte-anti-piratage
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/1719.jpg
tags:
- HADOPI
- Institutions
- Internet
series:
- 202125
series_weight: 0
---

> Alors que les débats devaient perdurer jusqu’en juillet, par 59 voix contre 4, l’Assemblée nationale a adopté ce 23 juin le projet de loi l'ensemble du projet de loi relatif à la régulation et à la protection de l'accès aux œuvres culturelles à l'ère numérique.
