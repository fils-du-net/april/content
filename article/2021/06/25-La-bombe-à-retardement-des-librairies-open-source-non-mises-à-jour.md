---
site: Le Monde Informatique
title: "La bombe à retardement des librairies open source non mises à jour"
author: Jacques Cheminat
date: 2021-06-25
href: https://www.lemondeinformatique.fr/actualites/lire-la-bombe-a-retardement-des-librairies-open-source-non-mises-a-jour-83384.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000079682.jpg
tags:
- Sensibilisation
- Entreprise
series:
- 202125
series_weight: 0
---

> Dans un rapport sur l'état de la sécurité des logiciel, Veracode alerte sur l'incidence des bibliothèques open source non mises à jour sur les applications actuelles. Près de 80% de ces librairies ne sont pas actualisées ou comportent au moins une vulnérabilité.
