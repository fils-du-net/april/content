---
site: Usbek & Rica
title: "«Le numérique peut-il accomplir la promesse d'une école pour tous?»"
date: 2021-06-21
href: https://usbeketrica.com/fr/article/le-numerique-peut-il-accomplir-la-promesse-d-une-ecole-pour-tous
featured_image: https://usbeketrica.com/media/79126/download/sans_tabou_5.png?v=1&inline=1
tags:
- Éducation
series:
- 202125
series_weight: 0
---

> Ce dialogue a lieu entre Carlo Purassanta, président de Microsoft France, et un invité. Pour ce cinquième épisode consacré à l’éducation, Sans Tabou reçoit l’un des plus grands experts en innovation pédagogique de France : François Taddéi. Polytechnicien, docteur en génétique et directeur de recherche à l’Inserm, il est également le cofondateur du Centre de Recherche Interdisciplinaire (CRI) où il défend et développe inlassablement de nouvelles manières d’apprendre et d’enseigner.
