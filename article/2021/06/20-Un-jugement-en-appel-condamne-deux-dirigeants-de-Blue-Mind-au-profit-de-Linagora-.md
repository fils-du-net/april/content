---
site: ZDNet France
title: "Un jugement en appel condamne deux dirigeants de Blue Mind au profit de Linagora"
author: Thierry Noisette
date: 2021-06-20
href: https://www.zdnet.fr/blogs/l-esprit-libre/un-jugement-en-appel-condamne-deux-dirigeants-de-blue-mind-au-profit-de-linagora-39924815.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/06/marteau_justice_pxhere.jpeg
tags:
- Licenses
- Entreprise
series:
- 202125
series_weight: 0
---

> Linagora et son PDG Alexandre Zapolsky obtiennent satisfaction, avec un arrêt de la cour d'appel de Paris qui condamne Pierre Baudracco et Pierre Carlier à leur verser un total de 457.000 euros.
