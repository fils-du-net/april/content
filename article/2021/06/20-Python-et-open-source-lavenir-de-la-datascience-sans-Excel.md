---
site: Le Monde Informatique
title: "Python et open source: l'avenir de la datascience sans Excel"
author: Matt Asay
date: 2021-06-20
href: https://www.lemondeinformatique.fr/actualites/lire-python-et-open-source-l-avenir-de-la-datascience-sans-excel-83277.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000079459.jpg
tags:
- Logiciels privateurs
- Innovation
series:
- 202124
series_weight: 0
---

> Si le tableur de Microsoft est incontournable pour la science des données, Python et l'open source pourrait prendre la relève. Le soutien de la communauté nécessaire à la succession d'Excel s'avère indispensable.
