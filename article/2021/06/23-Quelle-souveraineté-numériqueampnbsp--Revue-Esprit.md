---
site: Esprit Presse
title: "Quelle souveraineté numérique?"
author: Pierre-Antoine Chardel
date: 2021-06-23
href: https://esprit.presse.fr/actualites/pierre-antoine-chardel/quelle-souverainete-numerique-43431
featured_image: https://esprit.presse.fr/prod/file/esprit_presse/article/img_resize/43431_large.jpg
tags:
- Vie privée
- Informatique en nuage
- Entreprise
- Institutions
- Europe
series:
- 202125
series_weight: 0
---

> L’intégration de la société Palantir au projet Gaia-X (censé favoriser la souveraineté européenne en matière de numérique et de traitement des données) suscite de nombreuses interrogations. Elle questionne l’extension du traitement des données à une sphère aussi cruciale que celle de la santé, ainsi que ses possibles effets discriminatoires.
