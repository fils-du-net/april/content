---
site: L'OBS
title: "TRIBUNE. «Le futur d'internet sera celui d'un bien commun»"
author: Philippe Dewost
date: 2021-06-25
href: https://www.nouvelobs.com/economie/20210625.OBS45741/tribune-le-futur-d-internet-sera-celui-d-un-bien-commun.html
featured_image: https://focus.nouvelobs.com/2021/06/23/380/0/5206/2603/633/306/75/0/508bc04_682952832-049-f0334218.jpg
tags:
- Internet
series:
- 202125
series_weight: 0
---

> Philippe Dewost, cofondateur de Wanadoo et multi-entrepreneur, réfléchit à l'avenir du réseau internet, à l'heure des méga-plateformes américaines et de la «grande muraille informatique» chinoise.
