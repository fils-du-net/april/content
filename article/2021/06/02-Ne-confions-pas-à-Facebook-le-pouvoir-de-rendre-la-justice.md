---
site: Le Monde.fr
title: "«Ne confions pas à Facebook le pouvoir de rendre la justice» (€)"
author: Pierre Louette
date: 2021-06-02
href: https://www.lemonde.fr/idees/article/2021/06/03/ne-confions-pas-a-facebook-le-pouvoir-de-rendre-la-justice_6082585_3232.html
featured_image: https://img.lemde.fr/2020/11/30/0/0/3500/2290/1328/0/45/0/68e8c65_fw1-usa-tech-biden-1130-11.JPG
tags:
- Entreprise
- Institutions
- Europe
series:
- 202122
series_weight: 0
---

> TRIBUNE. Pierre Louette, spécialiste des médias, invite les Etats, dans une tribune au «Monde», à ne pas déléguer à des entreprises privées leur compétence dans le domaine de la justice, et ce au moment où l'Union européenne travaille sur un projet de directive traitant de la responsabilité des plates-formes.
