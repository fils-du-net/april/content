---
site: La gazette.fr
title: "Le logiciel libre dans la démat' (€)"
date: 2021-06-08
href: https://www.lagazettedescommunes.com/749575/le-logiciel-libre-dans-la-demat
featured_image: https://www.lagazettedescommunes.com/wp-content/uploads/2021/06/logiciel-libre-techno-numerique-une.jpg
tags:
- Administration
series:
- 202123
series_weight: 0
---

> Les logiciels libres sont loin d’être étrangers à la dématérialisation des services publics dématérialisés. Mais quelle place y prennent-ils ? Comment les adopter, pour quels usages, dans les collectivités? Découvrez les réponses, et d’autres, exposées par nos experts lors des 4è Assises de la dématérialisation.
