---
site: Reseaux-Telecoms.net
title: "Les composants open source ne sont pas suffisamment mis à jour dans les apps réseau"
author: Jon Gold
date: 2021-06-04
href: https://www.reseaux-telecoms.net/actualites/lire-les-composants-open-source-ne-sont-pas-suffisamment-mis-a-jour-dans-les-apps-reseau-28221.html
featured_image: http://www.reseaux-telecoms.net/images/actualite/000000012220.jpg
tags:
- Entreprise
series:
- 202122
series_weight: 0
---

> Les failles découvertes et corrigées par les équipes de projet open-source dans le code open-source des logiciels réseau commerciaux ne seront pas forcément adoptées dans les éditeurs. D'où l'intérêt de conférer contractuellement au fournisseur de service la responsabilité de l'analyse de la sécurité et de la résolution des problèmes.
