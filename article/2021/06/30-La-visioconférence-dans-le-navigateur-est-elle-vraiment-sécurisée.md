---
site: ZDNet France
title: "La visioconférence dans le navigateur est-elle vraiment sécurisée?"
author: Renaud Ghia, Tixe
date: 2021-06-30
href: https://www.zdnet.fr/actualites/la-visioconference-dans-le-navigateur-est-elle-vraiment-securisee-39925525.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/01/visioconference__w1200.jpg
tags:
- Sensibilisation
- Innovation
series:
- 202126
series_weight: 0
---

> Rejoindre une visioconférence dans le navigateur est d’une grande simplicité: mais est-ce vraiment sécurisé? Voici une mise au point sur les faits, qui sont souvent éloignés des idées reçus, assure Renaud Ghia de Tixeo.
