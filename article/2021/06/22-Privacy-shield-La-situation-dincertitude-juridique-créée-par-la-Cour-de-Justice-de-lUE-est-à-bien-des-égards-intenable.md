---
site: Le Monde.fr
title: "Privacy shield: «La situation d'incertitude juridique créée par la Cour de Justice de l'UE est à bien des égards intenable» (€)"
author: Nicolas Mazzucchi
date: 2021-06-22
href: https://www.lemonde.fr/idees/article/2021/06/22/privacy-shield-la-situation-d-incertitude-juridique-creee-par-la-cour-de-justice-de-l-ue-est-a-bien-des-egards-intenable_6085154_3232.html
tags:
- Vie privée
series:
- 202125
---

> TRIBUNE. Après l'invalidation par la justice européenne du Privacy Shield (accord transatlantique sur l'échange des données), Nicolas Mazzucchi, spécialiste en cyberstratégie plaide dans une tribune au «Monde», pour que la France et l'Allemagne poussent de concert à un nouvel accord Europe/États-Unis.
