---
site: Le Monde Informatique
title: "L'Education Nationale nomme Audran Le Baron directeur du numérique"
author: Bertrand Lemaire
date: 2021-06-28
href: https://www.lemondeinformatique.fr/actualites/lire-l-education-nationale-nomme-audran-le-baron-directeur-du-numerique-83408.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000079732.jpg
tags:
- Éducation
- Institutions
series:
- 202126
series_weight: 0
---

> Nomination: Succédant à Jean-Marc Merriaux le 12 juillet 2021, Audran Le Baron prendra en charge le numérique de l'Education Nationale.

