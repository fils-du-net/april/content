---
site: Next INpact
title: "Pass sanitaire, données en clair, open source: «On fait au plus simple»"
author: Sébastien Gavois
description: "Vivement le prochain «sprint»"
date: 2021-06-08
href: https://www.nextinpact.com/article/46147/pass-sanitaire-donnees-en-clair-open-source-on-fait-au-plus-simple
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/9351.jpg
tags:
- Vie privée
series:
- 202123
---

> Avant l’entrée en vigueur du pass sanitaire demain, Cédric O tenait une conférence de presse pour donner les derniers détails. L’occasion pour nous de demander des explications sur la présence de données en clair, l’utilisation d’un serveur de vérification et de composants Firebase (Google).
