---
site: Numerama
title: "TousAntiCovid-Verif: pourquoi l'app vérifiant les données du pass sanitaire fait polémique"
author: Julien Lausson
date: 2021-06-07
href: https://www.numerama.com/tech/717219-tousanticovid-verif-pourquoi-lapp-verifiant-les-donnees-du-pass-sanitaire-fait-polemique.html
featured_image: https://www.numerama.com/wp-content/uploads/2021/06/tac-verif-1.jpg
tags:
- Vie privée
- Institutions
- Interopérabilité
series:
- 202123
series_weight: 0
---

> L'application TousAntiCovid-Verif, qui s'adresse aux professionnels pour vérifier les informations du public dans le cadre du passe sanitaire, fait l'objet depuis plusieurs jours de vives critiques. Certains choix techniques sont controversés et le fonctionnement annoncé de l'application ne serait pas tout à fait raccord avec son fonctionnement réel.
