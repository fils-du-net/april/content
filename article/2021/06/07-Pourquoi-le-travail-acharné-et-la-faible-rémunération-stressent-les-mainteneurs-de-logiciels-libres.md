---
site: ZDNet France
title: "Pourquoi le travail acharné et la faible rémunération stressent les mainteneurs de logiciels libres?"
author: Steven J. Vaughan-Nichols
date: 2021-06-07
href: https://www.zdnet.fr/actualites/pourquoi-le-travail-acharne-et-la-faible-remuneration-stressent-les-mainteneurs-de-logiciels-libres-39924021.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/Pierre%20temp/Cybersecurite%20A__w1200.jpg
tags:
- Économie
- Sensibilisation
series:
- 202123
series_weight: 0
---

> La maintenance du code open source est un travail essentiel mais stressant. Malgré cela, une récente enquête de Tidelift a révélé que près de la moitié des mainteneurs de code ne sont pas payés du tout.
