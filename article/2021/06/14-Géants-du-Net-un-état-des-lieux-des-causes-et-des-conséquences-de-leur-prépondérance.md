---
site: Next INpact
title: "Géants du Net: un état des lieux «des causes et des conséquences» de leur prépondérance (€)"
description: "En Europe? On a des idées et on régule"
author: Sébastien Gavois
date: 2021-06-14
href: https://www.nextinpact.com/article/46158/geants-net-etat-lieux-causes-et-consequences-leur-preponderance
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/8795.jpg
tags:
- Informatique en nuage
- Entreprise
- Internet
- Institutions
- Europe
series:
- 202124
series_weight: 0
---

> La France et l’Europe n‘ont pas de géant du numérique à opposer aux plateformes américaines et chinoises. Le Vieux continent a des idées et met en place des outils de régulation pour essayer de nager dans ces eaux troubles, mais peine à se distinguer. Un rapport parlementaire fait le point de la situation.
