---
site: Siècle Digital
title: "Doctolib a transféré des données sensibles à Facebook et Outbrain"
author:  Valentin Cimino
date: 2021-06-23
href: https://siecledigital.fr/2021/06/23/doctolib-donnees-medicales-facebook-outbrain
featured_image: https://siecledigital.fr/wp-content/uploads/2021/06/DOCTOLIB_DONNEES-940x550.jpg
tags:
- Vie privée
series:
- 202125
---

> Selon le média allemand Mobilsicher, Doctolib aurait envoyé les mots-clés tapés par les utilisateurs à Facebook et Outbrain pendant plusieurs mois.
