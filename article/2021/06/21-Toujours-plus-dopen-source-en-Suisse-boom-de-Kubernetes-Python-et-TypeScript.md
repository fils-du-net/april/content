---
site: ICTjournal
title: "Toujours plus d'open source en Suisse: boom de Kubernetes, Python et TypeScript"
author: Yannick Chavanne et Ludovic de Werra
date: 2021-06-21
href: https://www.ictjournal.ch/etudes/2021-06-21/toujours-plus-dopen-source-en-suisse-boom-de-kubernetes-python-et-typescript
featured_image: https://www.ictjournal.ch/sites/default/files/styles/np8_full/public/media/2021/06/21/tim-mossholder-zybl6vnud_0-unsplash_web.jpg?itok=hmDn4Oaa
tags:
- Entreprise
- Interopérabilité
- Associations
series:
- 202125
series_weight: 0
---

> L'open source devient la norme pour les entreprises suisses dont la quasi-totalité utilise au moins un logiciel libre. Un essor qui s’explique notamment par le fait que ces solutions sont interopérables par nature, indique une récente étude de l’Université de Berne.
