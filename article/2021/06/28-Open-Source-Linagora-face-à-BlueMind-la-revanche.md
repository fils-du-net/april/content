---
site: Silicon
title: "Open Source: Linagora face à BlueMind, la revanche?"
author: Ariane Beky
date: 2021-06-28
href: https://www.silicon.fr/open-source-linagora-bluemind-revanche-411253.html
featured_image: https://www.silicon.fr/wp-content/uploads/2015/03/facebook-justice.jpg
tags:
- Entreprise
- Institutions
series:
- 202126
series_weight: 0
---

> L'open source français bousculé. Statuant contradictoirement, la cour d'appel de Paris a condamné les dirigeants de BlueMind. Une victoire pour Linagora?
