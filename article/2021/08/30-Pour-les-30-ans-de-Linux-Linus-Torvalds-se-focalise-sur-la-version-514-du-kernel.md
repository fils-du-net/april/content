---
site: Le Monde Informatique
title: "Pour les 30 ans de Linux, Linus Torvalds se focalise sur la version 5.14 du kernel"
author: Jacques Cheminat
date: 2021-08-30
href: https://www.lemondeinformatique.fr/actualites/lire-pour-les-30-ans-de-linux-linus-torvalds-se-focalise-sur-la-version-514-du-kernel-83983.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000080652.png
tags:
- Sensibilisation
series:
- 202135
series_weight: 0
---

> Le père de Linux a sa manière bien à lui de fêter les 30 ans des débuts de l'OS open source. Loin des festivités, Linus Torvalds préfère se focaliser sur la publication de la dernière version du kernel, 5.14.
