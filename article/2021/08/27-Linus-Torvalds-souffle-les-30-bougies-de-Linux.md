---
site: ZDNet France
title: "Linus Torvalds souffle les 30 bougies de Linux"
author: Steven J. Vaughan-Nichols
date: 2021-08-27
href: https://www.zdnet.fr/actualites/linus-torvalds-souffle-les-30-bougies-de-linux-39928163.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/Pierre%20temp/Linus%20Torvalds__w1200.jpg
tags:
- Sensibilisation
series:
- 202134
---

> Cela fait 30 ans que Linus Torvalds, étudiant finlandais diplômé, a rédigé une brève note indiquant qu'il lançait un système d'exploitation amateur. Le monde de l'informatique n'a plus jamais été le même depuis.
