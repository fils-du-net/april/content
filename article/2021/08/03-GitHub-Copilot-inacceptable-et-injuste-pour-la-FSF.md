---
site: Le Monde Informatique
title: "GitHub Copilot inacceptable et injuste pour la FSF"
author: Paul Krill
date: 2021-08-03
href: https://www.lemondeinformatique.fr/actualites/lire-github-copilot-inacceptable-et-injuste-pour-la-fsf-83772.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000080326.jpg
tags:
- Droit d'auteur
series:
- 202131
series_weight: 0
---

> L'organisation à but non lucratif de promotion et de défense du logiciel libre (FSF) pointe du doigt l'équité, la légitimité et la légalité de Copilot, l'assistant de codage piloté par l'IA de GitHub.
