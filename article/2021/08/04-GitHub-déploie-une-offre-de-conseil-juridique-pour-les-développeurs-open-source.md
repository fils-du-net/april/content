---
site: Clubic.com
title: "GitHub déploie une offre de conseil juridique pour les développeurs open source"
author: Fanny Dufour
date: 2021-08-04
href: https://www.clubic.com/github/actualite-379905-github-deploie-une-offre-de-conseil-juridique-pour-les-developpeurs-open-source.html
featured_image: https://pic.clubic.com/v1/images/1696199/raw.webp?hash=695dcacf13d2f6e5ae080028584ebdd12c7fc66b
tags:
- Droit d'auteur
series:
- 202131
series_weight: 0
---

> GitHub a annoncé mettre en place une aide juridique pour les développeurs open source dans le but de combattre les demandes de retrait liées au DMCA.
