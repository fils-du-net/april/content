---
site: ICTjournal
title: "La Société Numérique suisse porte plainte contre le projet Justitia.Swiss"
author: Maximilian Schenner
date: 2021-08-11
href: https://www.ictjournal.ch/news/2021-08-11/la-societe-numerique-suisse-porte-plainte-contre-le-projet-justitiaswiss
featured_image: https://www.ictjournal.ch/sites/default/files/styles/np8_full/public/media/2021/08/11/mateus-campos-felipe-zd8px974bc8-unsplash_web.jpg
tags:
- Administration
- Logiciels privateurs
- Vie privée
- Associations
series:
- 202132
series_weight: 0
---

> «Justitia.Swiss», la future plateforme fédérale numérisant les procédures judiciaires, devrait être introduite dans deux ans. Or, la base juridique le permettant ne sera pas encore en place avant 2025. L’association Société Numérique a déposé plainte.
