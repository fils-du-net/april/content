---
site: Clubic.com
title: "Urbit, un nouvel OS pour redonner le pouvoir aux internautes?"
author: Cyril Fiévet
date: 2021-08-04
href: https://www.clubic.com/systemes-exploitation-os/dossier-379842-urbit-un-nouvel-os-pour-redonner-le-pouvoir-aux-internautes-.html
featured_image: https://pic.clubic.com/v1/images/1905741/raw
tags:
- Vie privée
- Innovation
series:
- 202131
series_weight: 0
---

> Peut-on encore imposer un nouveau système d'exploitation s'affranchissant de la domination des géants du numériques? Certains le croient et bâtissent patiemment Urbit, un OS alternatif libre et gratuit, décentralisé, respectueux de la vie privée et replaçant l'usager au coeur de l'écosystème numérique. Un pari fou?
