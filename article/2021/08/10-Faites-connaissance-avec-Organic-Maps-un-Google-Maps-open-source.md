---
site: Frandroid
title: "Faites connaissance avec Organic Maps, un Google Maps open source"
description: "Avec OpenStreetMap et Maps.me pour fondations"
author: Nathan Le Gohlisse
date: 2021-08-10
href: https://www.frandroid.com/android/applications/1023529_faites-connaissance-avec-organic-maps-un-google-maps-open-source
featured_image: https://images.frandroid.com/wp-content/uploads/2021/08/organic-maps-1-1200x900.jpg
tags:
- Innovation
- Internet
- Logiciels privateurs
- Vie privée
series:
- 202132
series_weight: 0
---

> Petit à petit des alternatives open source de grande qualité se développent dans l'ombre de Google Maps et son concurrent Apple Plans. Basée sur les cartes participatives d'OpenStreetMap, Organic Maps en fait partie. Cette nouvelle application de cartographie est maintenant disponible gratuitement sur iOS et Android.
