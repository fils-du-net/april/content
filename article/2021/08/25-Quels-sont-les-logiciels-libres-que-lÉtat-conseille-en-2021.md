---
site: Numerama
title: "Quels sont les logiciels libres que l'État conseille en 2021?"
author: Julien Lausson
date: 2021-08-25
href: https://www.numerama.com/tech/734501-quels-sont-les-logiciels-libres-que-letat-conseille-en-2021.html
featured_image: https://www.numerama.com/wp-content/uploads/2021/08/manchot-animal.jpg
tags:
- Référentiel
- Administration
series:
- 202134
series_weight: 0
---

> Finies les listes actualisées une fois par an des logiciels libres recommandés par l'État. Désormais, les mises à jour se font au fil de l'eau. Plusieurs programmes font leur apparition en 2021.
