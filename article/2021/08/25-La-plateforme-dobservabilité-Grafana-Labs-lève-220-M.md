---
site: Le Monde Informatique
title: "La plateforme d'observabilité Grafana Labs lève 220 M$"
author: Maryse Gros
date: 2021-08-25
href: https://www.lemondeinformatique.fr/actualites/lire-la-plateforme-d-observabilite-grafana-labs-leve-220-m%24-83938.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000080595.jpg
tags:
- Économie
- Entreprise
series:
- 202134
series_weight: 0
---

> Financement: Avec sa levée de fonds en série C qui le valorise à 3 Md$, Grafana Labs va se concentrer sur le développement de sa plateforme d'observabilité.
