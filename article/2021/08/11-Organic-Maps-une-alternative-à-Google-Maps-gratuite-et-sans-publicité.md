---
site: Geeko
title: "Organic Maps: une alternative à Google Maps gratuite et sans publicité"
author: David Manfredini
date: 2021-08-11
href: https://geeko.lesoir.be/2021/08/11/organic-maps-une-alternative-a-google-maps-gratuite-et-sans-publicite
featured_image: https://geeko.lesoir.be/wp-content/uploads/2021/08/compilation-hiking-Copie.png
tags:
- Innovation
series:
- 202132
---

> L’application promet de respecter la vie privée de ses utilisateurs et de ne pas les tracker.
