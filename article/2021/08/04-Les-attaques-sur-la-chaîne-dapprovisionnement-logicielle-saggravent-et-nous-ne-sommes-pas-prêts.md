---
site: ZDNet France
title: "Les attaques sur la chaîne d'approvisionnement logicielle s'aggravent et nous ne sommes pas prêts"
author: Liam Tung
date: 2021-08-04
href: https://www.zdnet.fr/actualites/les-attaques-sur-la-cha-ne-d-approvisionnement-logicielle-s-aggravent-et-nous-ne-sommes-pas-prets-39927089.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2021/03/Malware__w1200.jpg
tags:
- Vie privée
- Entreprise
series:
- 202131
series_weight: 0
---

> L'agence européenne de la cybersécurité s'est penchée sur 24 attaques récentes visant la chaîne d'approvisionnement logicielle et met en garde contre l'insuffisance des moyens de défense.
