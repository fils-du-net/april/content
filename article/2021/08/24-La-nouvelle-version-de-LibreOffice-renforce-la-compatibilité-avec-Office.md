---
site: ZDNet France
title: "La nouvelle version de LibreOffice renforce la compatibilité avec Office"
author: Liam Tung
date: 2021-08-24
href: https://www.zdnet.fr/actualites/la-nouvelle-version-de-libreoffice-renforce-la-compatibilite-avec-office-39927883.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2021/08/libreoffice-7-2-banner__w630.jpg
tags:
- Économie
- Entreprise
- Interopérabilité
series:
- 202134
series_weight: 0
---

> LibreOffice propose une meilleure compatibilité avec Office et une prise en charge native du processeur M1 d'Apple, mais la Document Foundation souhaite vraiment que les entreprises paient pour cette alternative libre et gratuite à Microsoft Office.
