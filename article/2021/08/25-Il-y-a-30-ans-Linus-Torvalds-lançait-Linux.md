---
site: Next INpact
title: "Il y a 30 ans, Linus Torvalds lançait Linux"
author: Sébastien Gavois
date: 2021-08-25
href: https://www.nextinpact.com/article/47850/il-y-a-30-ans-linus-torvalds-lancait-linux
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/689.jpg
tags:
- Sensibilisation
series:
- 202134
series_weight: 0
---

> L'annonce de Linux fête ses 30 ans, il a donc exactement le même âge que le Web. Au cours des dernières années, le noyau a continué d'évoluer et de se répandre jusqu'à se faire une place… dans Windows.
