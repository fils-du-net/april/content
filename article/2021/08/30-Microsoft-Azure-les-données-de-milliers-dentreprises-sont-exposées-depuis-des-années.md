---
site: Siècle Digital
title: "Microsoft Azure: les données de milliers d'entreprises sont exposées depuis des années"
author: Valentin Cimino
date: 2021-08-30
href: https://siecledigital.fr/2021/08/30/microsoft-azure-les-donnees-de-milliers-entreprises-exposees
featured_image: https://siecledigital.fr/wp-content/uploads/2021/08/microsoft-q2-940x550.jpg
tags:
- Informatique en nuage
- Vie privée
series:
- 202135
---

> Microsoft Azure a averti les 3 300 clients concernés par cette faille de sécurité, dont les données sont exposées depuis des années.
