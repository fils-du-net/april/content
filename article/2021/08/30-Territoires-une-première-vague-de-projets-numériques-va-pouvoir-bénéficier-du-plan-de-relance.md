---
site: Acteurs Publics
title: "Territoires: une première vague de projets numériques va pouvoir bénéficier du plan de relance (€)"
author: Emile Marzolf
date: 2021-08-30
href: https://www.acteurspublics.fr/articles/collectivites-une-premiere-vague-de-projets-numeriques-va-pouvoir-beneficier-du-plan-de-relance
featured_image: https://www.acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/27/c804eb722235cc1287f72a2ebeb10051297ecdca.jpeg
tags:
- Administration
series:
- 202135
---

> Le comité de sélection du volet “coconstruction de services numériques” du plan de relance numérique des territoires - doté de 30 millions d’euros - a sélectionné 47 premiers projets issus de la consultation des collectivités initiée en début d’année. Avec quelques réserves toutefois, notamment sur la capacité des projets à passer à l’échelle et à devenir des “logiciels libres”.
