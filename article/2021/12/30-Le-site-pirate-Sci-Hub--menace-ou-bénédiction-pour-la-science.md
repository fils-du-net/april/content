---
site: ActuaLitté.com
title: "Le site pirate Sci-Hub: menace ou bénédiction pour la science?"
date: 2021-12-30
href: https://actualitte.com/article/104041/legislation/le-site-pirate-sci-hub-menace-ou-benediction-pour-la-science
featured_image: https://actualitte.com/media/cache/width_944_webp/uploads/images/bertinn-61cd7ada4793a761147064.jpg
tags:
- Sciences
- Partage du savoir
- Droit d'auteur
series:
- 202152
series_weight: 0
---

> Vit-on les derniers instants du site Sci-Hub? Un procès est en tout cas engagé par Elsevier et d’autres éditeurs universitaires en Inde pour permettre de bloquer enfin le site pirate. Ces entreprises considèrent le site Sci-Hub comme une menace majeure pour la science, par corollaire, leur propre industrie florissante. Un avis que ne partage pas la fondatrice du site, Alexandra Elbakyan, pour qui les éditeurs sont la véritable menace pour le progrès de la science, décidant ainsi de contre-attaquer.
