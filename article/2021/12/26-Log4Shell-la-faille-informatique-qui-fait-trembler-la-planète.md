---
site: francetv info
title: "Log4Shell, la faille informatique qui fait trembler la planète"
author: Benjamin Vincent
date: 2021-12-26
href: https://www.francetvinfo.fr/replay-radio/nouveau-monde/log4shell-la-faille-informatique-qui-fait-trembler-la-planete_4877969.html
featured_image: "https://www.francetvinfo.fr/pictures/DsdUGTzpY32oLuNd6IKyev1jE7k/0x256:5565x3383/944x531/filters:format(webp)/2021/12/26/phpd2phC8.jpg"
tags:
- Sensibilisation
series:
- 202151
series_weight: 0
---

> Elle s’appelle Log4Shell. Elle est présentée comme la faille la plus critique de l’histoire de l’informatique. Une course contre la montre est engagée depuis une quinzaine de jours à travers le monde.
