---
site: Le Figaro.fr
title: "Le directeur interministériel du numérique va quitter ses fonctions"
date: 2021-12-08
href: https://www.lefigaro.fr/flash-eco/le-directeur-interministeriel-du-numerique-va-quitter-ses-fonctions-20211208
tags:
- Administration
series:
- 202149
---

> Le directeur interministériel du numérique, Nadi Bou Hanna, va quitter ses fonctions début 2022 après trois ans en poste, sur fond de tensions dans ses services, a-t-on appris mercredi de sources concordantes, confirmant des informations du quotidien Le Monde.
