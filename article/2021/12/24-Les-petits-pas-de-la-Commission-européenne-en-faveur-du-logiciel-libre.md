---
site: Le Monde Informatique
title: "Les petits pas de la Commission européenne en faveur du logiciel libre"
author: Bertrand Lemaire
date: 2021-12-24
href: https://www.lemondeinformatique.fr/actualites/lire-les-petits-pas-de-commission-europeenne-en-faveur-du-logiciel-libre-85220.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000082925.jpg
tags:
- april
- Europe
series:
- 202151
---

> Open Source: L'April affiche une certaine déception devant un manque d'engagement ferme mais la Commission Européenne progresse bien vers le logiciel libre.
