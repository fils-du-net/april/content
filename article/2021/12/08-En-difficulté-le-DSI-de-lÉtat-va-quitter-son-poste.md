---
site: Acteurs Publics
title: "En difficulté, le DSI de l'État va quitter son poste"
author: Emile Marzolf
date: 2021-12-08
href: https://www.acteurspublics.fr/articles/en-difficulte-le-dsi-de-letat-va-quitter-son-poste
featured_image: https://www.acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/27/2315560f2090addbced5d20c53519bac0437b3b5.jpeg
tags:
- Administration
series:
- 202149
series_weight: 0
---

> Après trois années passées à la tête de la direction interministérielle du numérique de l’État, Nadi Bou Hanna a annoncé, le 7 décembre au soir, son départ en janvier. Une annonce qui intervient à la veille de la parution, ce mercredi, d’une nouvelle enquête du Monde sur ses méthodes de management.
