---
site: 20minutes.fr
title: "Transition écologique: Comment l'open data et l'open source permettent aux citoyens d'agir sur l'environnement"
author: Martin Régner
date: 2021-12-01
href: https://www.20minutes.fr/planete/3184775-20211201-transition-ecologique-comment-open-data-open-source-permettent-citoyens-agir-environnement
featured_image: https://img.20mn.fr/aqNgQj24Qdut1AeOxtgs-Ck/830x532_simulateurs-ouverts-reutilisables-crees-ademe-aider-citoyens-reduire-impact-carbone.jpg
tags:
- Open Data
series:
- 202148
series_weight: 0
---

> Découvrez, chaque jour, une analyse de notre partenaire The Conversation. Aujourd'hui, un ingénieur nous explique l'intérêt de l'accès aux données environnementales
