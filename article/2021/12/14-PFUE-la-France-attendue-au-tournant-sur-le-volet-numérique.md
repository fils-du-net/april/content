---
site: EurActiv
title: "PFUE: la France attendue au tournant sur le volet numérique"
author: Mathieu Pollet
date: 2021-12-14
href: https://www.euractiv.fr/section/economie/news/pfue-la-france-attendue-au-tournant-sur-le-volet-numerique
featured_image: https://www.euractiv.fr/wp-content/uploads/sites/3/2021/12/w_57346184-800x450.jpg
tags:
- Europe
- Informatique en nuage
series:
- 202150
series_weight: 0
---

> Sans surprise, la régulation du numérique et l'émergence d'une souveraineté technologique font partie des sujets que Paris ambitionne de porter lors du premier semestre 2022.
