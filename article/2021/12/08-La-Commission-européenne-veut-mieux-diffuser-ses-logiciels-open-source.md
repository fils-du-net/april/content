---
site: ZDNet France
title: "La Commission européenne veut mieux diffuser ses logiciels open source"
author: Thierry Noisette
date: 2021-12-08
href: https://www.zdnet.fr/blogs/l-esprit-libre/la-commission-europeenne-veut-mieux-diffuser-ses-logiciels-open-source-39933847.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2018/12/Bruxelles_Commission_UE_Berlaymont.jpg
tags:
- Europe
- Administration
series:
- 202149
series_weight: 0
---

> Un dépôt unique sera utilisé pour proposer les logiciels open source que la Commission européenne veut mettre à la disposition de tous.
