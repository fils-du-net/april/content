---
site: cio-online.com
title: "La Commission Européenne fait un pas de plus en faveur des logiciels libres"
author: Bertrand Lemaire
date: 2021-12-23
href: https://www.cio-online.com/actualites/lire-la-commission-europeenne-fait-un-pas-de-plus-en-faveur-des-logiciels-libres-13766.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000017929.jpg
tags:
- april
- Europe
- Licenses
series:
- 202151
series_weight: 0
---

> L'April affiche une certaine déception devant un manque d'engagement ferme mais la Commission Européenne progresse bien vers le logiciel libre.
