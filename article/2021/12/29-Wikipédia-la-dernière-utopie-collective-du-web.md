---
site: Numerama
title: "Wikipédia, la dernière utopie collective du web?"
description: Les rouages de la plus grande encyclopédie du monde
author: Marcus Dupont-Besnard
date: 2021-12-29
href: https://www.numerama.com/tech/805447-wikipedia-la-derniere-utopie-collective-du-web.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2021/12/wikipedia-book.jpg?resize=1024,576
tags:
- Partage du savoir
- Internet
series:
- 202152
series_weight: 0
---

> L’encyclopédie en ligne Wikipédia fait partie de notre quotidien. Tantôt décrite comme utile ou comme imparfaite, cet épisode du Meilleur des Mondes, émission partenaire de Numerama, plonge dans les rouages d’un projet qui a fêté ses 20 ans en 2021.
