---
site: ZDNet France
title: "Donald Trump lève un milliard de dollars pour financer son réseau social"
date: 2021-12-06
href: https://www.zdnet.fr/actualites/donald-trump-leve-un-milliard-de-dollars-pour-financer-son-reseau-social-39933641.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/Pierre%20temp/Trump%20B__w1200.jpg
tags:
- Licenses
series:
- 202149
series_weight: 0
---

> L'ancien président américain vient de lever un milliard de dollars sur les marchés américains pour financer son futur réseau social.
