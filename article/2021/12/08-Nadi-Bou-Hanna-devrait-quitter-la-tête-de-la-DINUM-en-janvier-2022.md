---
site: ZDNet France
title: "Nadi Bou Hanna devrait quitter la tête de la DINUM en janvier 2022"
author: Clarisse Treilles
date: 2021-12-08
href: https://www.zdnet.fr/actualites/nadi-bou-hanna-devrait-quitter-la-tete-de-la-dinum-en-janvier-2022-39933791.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/01/illustration%20fond%20rouge__w630.jpg
tags:
- Administration
series:
- 202149
---

> Le torchon brûle à la DINUM. A la tête de la direction interministérielle du numérique de l'Etat depuis trois ans, Nadi Bou Hanna s'apprête à quitter ses fonctions, accusé d'un management désastreux.
