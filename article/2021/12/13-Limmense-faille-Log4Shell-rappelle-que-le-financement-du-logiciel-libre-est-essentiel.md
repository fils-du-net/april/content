---
site: Numerama
title: "L'immense faille Log4Shell rappelle que le financement du logiciel libre est essentiel"
description: Quand le développement est assuré par 5 bénévoles
author: Corentin Bechade
date: 2021-12-13
href: https://www.numerama.com/tech/789245-limmense-faille-log4shell-rappelle-que-le-financement-du-logiciel-libre-est-essentiel.html
featured_image: https://c1.lestechnophiles.com/www.numerama.com/wp-content/uploads/2021/12/log4j.jpg?resize=1024,576
tags:
- Sensibilisation
series:
- 202150
series_weight: 0
---

> Derrière l’inquiétante faille Log4Shell qui fait trembler l’internet, une question plus philosophique se pose: comment financer le logiciel libre pour éviter ce genre de problème?

