---
site: Siècle Digital
title: "La Commission européenne adopte de nouvelles règles pour favoriser l'open source"
author: Valentin Cimino
date: 2021-12-14
href: https://siecledigital.fr/2021/12/14/la-commission-europeenne-favoriser-open-source
featured_image: https://siecledigital.fr/wp-content/uploads/2021/12/tezos-wPN1xnccGPw-unsplash-940x550.jpg
tags:
- Europe
- Innovation
series:
- 202150
series_weight: 0
---

> La Commission européenne adopte un nouveau règlement pour favoriser l'open source et stimuler l'innovation au sein de l'Union.
