---
site: Next INpact
title: "Log4Shell: derrière l'importante faille, l'éternelle question du soutien au logiciel libre"
description: "Heartbleed, on pense à toi!"
author: Sébastien Gavois
date: 2021-12-13
href: https://www.nextinpact.com/article/49180/log4shell-derriere-importante-faille-eternelle-question-soutien-au-logiciel-libre
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/1877.jpg
tags:
- Sensibilisation
series:
- 202150
---

> Ce week-end, une très importante faille – baptisée Log4Shell – a été découverte dans la bibliothèque de journalisation Apache log4j. On se trouve dans le pire scénario puisqu’elle permet d’exécuter du code arbitraire à distance, sans authentification. Comme avec Heartbleed, certains sites ont... fermé.
