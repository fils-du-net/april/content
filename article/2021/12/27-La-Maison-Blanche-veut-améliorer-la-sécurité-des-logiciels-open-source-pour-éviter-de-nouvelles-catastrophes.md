---
site: 01net.
title: "La Maison Blanche veut améliorer la sécurité des logiciels open source pour éviter de nouvelles catastrophes"
author: François Bedin
date: 2021-12-27
href: https://www.01net.com/actualites/la-maison-blanche-veut-ameliorer-la-securite-des-logiciels-open-source-pour-eviter-de-nouvelles-catastrophes-2052984.html
featured_image: https://img.bfmtv.com/c/630/420/244/c9a395015f3b329e6273ab01844f4.jpg
tags:
- Institutions
- Entreprise
series:
- 202152
series_weight: 0
---

> Après deux années compliquées d'un point de vue de la cybersécurité, le gouvernement américain invite les éditeurs et les développeurs à travailler ensemble pour éviter de rencontrer à nouveau des failles de sécurité importantes, comme Log4Shell.
