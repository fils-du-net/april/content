---
site: cio-online.com
title: "Nadi Bou Hanna (directeur de la DINUM): chronique d'un départ annoncé [MAJ]"
author: Bertrand Lemaire
date: 2021-12-08
href: https://www.cio-online.com/actualites/lire-nadi-bou-hanna-directeur-de-la-dinum-chronique-d-un-depart-annonce-13733.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000017889.jpg
tags:
- Administration
series:
- 202149
---

> Selon plusieurs sources, la rumeur persistante du départ du Directeur Interministériel du Numérique deviendrait une réalité mi-janvier 2022.
