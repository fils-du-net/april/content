---
site: Le Monde.fr
title: "L'Europe avance sur la régulation du monde numérique (€)"
author: Virginie Malingre
date: 2021-12-14
href: https://www.lemonde.fr/economie/article/2021/12/14/l-europe-avance-sur-la-regulation-du-monde-numerique_6106046_3234.html
tags:
- Entreprise
- Internet
- Institutions
series:
- 202150
series_weight: 0
---

> Le Parlement européen se penche sur deux textes visant à encadrer les géants du Web, Google, Amazon, Facebook et Apple (GAFA) en tête.
