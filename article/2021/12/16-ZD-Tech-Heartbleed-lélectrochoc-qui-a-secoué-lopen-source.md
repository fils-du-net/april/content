---
site: ZDNet France
title: "ZD Tech: Heartbleed, l'électrochoc qui a secoué l'open source"
author: Louis Adam
date: 2021-12-16
href: https://www.zdnet.fr/actualites/zd-tech-heartbleed-l-electrochoc-qui-a-secoue-l-open-source-39934309.htm
tags:
- Sensibilisation
series:
- 202150
---

> Bien avant le tremblement de terre log4j, en 2014, la faille Heartbleed touchait le logiciel open source OpenSSL, lui aussi largement répandu. Un premier électrochoc qui posait déjà à grande échelle la question de la sécurisation de l'open source.
