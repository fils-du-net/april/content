---
site: ZDNet France
title: "Tendances tech 2022: une année faste pour l'open source et le cloud"
author: George Anadiotis
date: 2021-12-28
href: https://www.zdnet.fr/actualites/tendances-tech-2022-une-annee-faste-pour-l-open-source-et-le-cloud-39934417.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/Pierre%20temp/Teletravail%20F__w1200.jpg
tags:
- Promotion
series:
- 202152
series_weight: 0
---

> La rédaction de ZDNet liste les tendances technologiques qui marqueront l'année 2022. Première étape: l'open-source et le cloud, qui vont devenir encore plus incontournables au cours de l'année prochaine.
