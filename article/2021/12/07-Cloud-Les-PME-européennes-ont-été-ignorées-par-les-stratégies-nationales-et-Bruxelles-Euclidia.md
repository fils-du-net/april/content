---
site: La Tribune
title: "Cloud: «Les PME européennes ont été ignorées par les stratégies nationales et Bruxelles» (Euclidia)"
author: Sylvain Rolland
date: 2021-12-07
href: https://www.latribune.fr/technos-medias/informatique/cloud-les-pme-europeennes-ont-ete-ignorees-par-les-strategies-nationales-et-bruxelles-jean-paul-smets-897955.html
featured_image: https://static.latribune.fr/full_width/1821826/jean-paul-smets.jpg
tags:
- Informatique en nuage
- Europe
series:
- 202149
series_weight: 0
---

> ENTRETIEN. Euclidia, association qui fédère 26 acteurs du cloud européen, dont de nombreux Français (Clever Cloud, Jamespot, Scaleway...) demande à l'Union européenne de revoir sa stratégie sur le cloud pour donner moins de place aux acteurs étrangers et ne pas oublier la centaine de PME européennes du secteur qui proposent déjà des solutions souveraines. Le groupement demande également aux Etats membres, à commencer par la France, de réaliser un moratoire sur leurs stratégies nationales «aux conséquences négatives sur la filière». Pour Jean-Paul Smets, Pdg de Nexedi (qui commercialise le logiciel en open source ERP5 utilisé par plusieurs groupes du CAC40) et membre d'Euclidia, «il n'est pas trop tard pour bien faire les choses».
