---
site: ZDNet France
title: "L'UFC Que-Choisir tacle l'indice de réparabilité"
author: Pierre Benhamou
date: 2021-12-14
href: https://www.zdnet.fr/actualites/l-ufc-que-choisir-tacle-l-indice-de-reparabilite-39934065.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/Pierre%20temp/Smartphone%20colere%20C__w1200.jpg
tags:
- Innovation
- Entreprise
series:
- 202150
series_weight: 0
---

> Pour l'association de défense des consommateurs, l'indice de réparabilité ne tient pas ses promesses. Et d'en appeler à une réforme de cet outil afin de le rendre «utile» au grand public.
