---
site: Le Monde.fr
title: "Changement de tête à la direction interministérielle du numérique, sur fond d’accusations de management brutal (€)"
author: Anaïs Coignac
date: 2021-12-08
href: https://www.lemonde.fr/politique/article/2021/12/08/changement-de-tete-a-la-direction-interministerielle-du-numerique-en-pleine-crise-interne_6105148_823448.html
featured_image: https://img.lemde.fr/2021/12/08/0/0/3188/2906/664/0/75/0/37ee0e2_961388862-harce-lement-dinum.jpg
tags:
- Administration
series:
- 202149
---

> Depuis plus de deux ans, selon une vingtaine d’agents interrogés par «Le Monde», arrêts maladie, burn-out et prescriptions d’antidépresseurs se sont multipliés au sein de ce service central de la modernisation de l’Etat. Mardi, le directeur a annoncé son départ prochain.
