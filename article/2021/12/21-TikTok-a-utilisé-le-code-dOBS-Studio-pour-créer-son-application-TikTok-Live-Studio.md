---
site: 01net.
title: "TikTok a utilisé le code d'OBS Studio pour créer son application TikTok Live Studio"
author: Geoffroy Ondet
date: 2021-12-21
href: https://www.01net.com/actualites/tiktok-a-utilise-le-code-d-obs-studio-pour-creer-son-application-tiktok-live-studio-2052818.html
featured_image: https://img.bfmtv.com/c/630/420/349/fc42fa9cf5c37f2bd45fbcf9761f8.jpg
tags:
- Licenses
- Entreprise
series:
- 202151
series_weight: 0
---

> Le réseau social s'est servi directement dans le code source du logiciel Open Broadcast Software pour développer sa propre application de diffusion en direct, sans en ouvrir son code, violant ainsi la licence d'utilisation du logiciel.
