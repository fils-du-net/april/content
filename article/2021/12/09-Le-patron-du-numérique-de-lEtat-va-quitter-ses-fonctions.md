---
site: La gazette.fr
title: "Le patron du numérique de l'Etat va quitter ses fonctions (€)"
author: Alexandre Léchenet, Laura Fernandez Rodriguez
date: 2021-12-09
href: https://www.lagazettedescommunes.com/779650/le-patron-du-numerique-de-letat-va-quitter-ses-fonctions
featured_image: https://www.lagazettedescommunes.com/wp-content/uploads/2019/04/nadi-bou-hanna-dinsic-une.jpg
tags:
- Administration
series:
- 202149
---

> Après trois années à la tête de la Direction interministérielle du numérique de l'Etat, Nadi Bou Hanna vient d'annoncer son départ pour janvier 2022. Les associations d'élus parties prenantes du programme Transformation numérique des territoires font le bilan et expriment leurs attentes pour la suite.
