---
site: Next INpact
title: "Doctolib accusé d'être trop bavard sur les données de santé"
date: 2021-03-11
href: https://www.nextinpact.com/lebrief/46406/doctolib-accuse-detre-trop-bavard-sur-donnees-sante
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/wide-linked-media/1372.jpg
tags:
- Vie privée
- Associations
- Informatique en nuage
series:
- 202110
series_weight: 0
---

> C’est un article de France Inter qui a déclenché la guerre de communication. Nos confrères se sont demandés si les données manipulées par Doctolib étaient correctement protégées, notamment si leur chiffrement était suffisamment fort.
