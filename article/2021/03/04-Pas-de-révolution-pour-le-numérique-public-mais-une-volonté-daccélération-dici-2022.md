---
site: Acteurs Publics 
title: "Pas de révolution pour le numérique public, mais une volonté d'accélération d'ici 2022 (€)"
author: Emile Marzolf
date: 2021-03-04
href: https://www.acteurspublics.fr/articles/pas-de-revolution-pour-le-numerique-public-mais-une-volonte-dacceleration-dici-2022
tags:
- Administration
series:
- 202109
series_weight: 0
---

> La ministre de la Transformation et de la Fonction publiques, Amélie de Montchalin, présente ce 4 mars sa feuille de route en matière de transformation numérique de l’État. Au menu: accélération de la dématérialisation “de qualité”, du déploiement de FranceConnect et de l’équipement informatique des agents publics.
