---
site: 01net.
title: "L'Open Source Initiative menace de stopper sa collaboration avec la FSF après le retour de Richard Stallman"
author: François Bedin
date: 2021-03-29
href: https://www.01net.com/actualites/l-open-source-initiative-menace-de-stopper-sa-collaboration-avec-la-fsf-apres-le-retour-de-richard-stallman-2039702.html
featured_image: https://img.bfmtv.com/c/630/420/14e/9730d702605d85a2a4c7909bdc0a7.jpg
tags:
- Promotion
series:
- 202113
---

> L'Open Source Initiative est contre le retour du pape du logiciel libre au sein de l'organisation qu'il a dirigée jusqu'en 2019.
