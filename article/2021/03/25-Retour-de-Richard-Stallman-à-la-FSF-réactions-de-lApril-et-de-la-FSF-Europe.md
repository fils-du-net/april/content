---
site: ZDNet France
title: "Retour de Richard Stallman à la FSF: réactions de l'April et de la FSF Europe (€)"
author: Thierry Noisette
date: 2021-03-25
href: https://www.zdnet.fr/blogs/l-esprit-libre/retour-de-richard-stallman-a-la-fsf-reactions-de-l-april-et-de-la-fsf-europe-39920085.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/03/orage_thunderstorm-Pixabay.jpg
tags:
- Promotion
- april
series:
- 202112
---

> La Free Software Foundation Europe critique le retour de Richard Stallman au conseil d'administration de son homologue américaine, et annonce qu'elle ne collaborera plus avec elle en l'état.
