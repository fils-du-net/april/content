---
site: LeMagIT
title: "Changement de licence open source: comment s’en prémunir"
author: Gaétan Raoul
date: 2021-03-01
href: https://www.lemagit.fr/conseil/Changement-de-licence-open-source-comment-sen-premunir
featured_image: https://cdn.ttgtmedia.com/visuals/ComputerWeekly/Hero%20Images/Penguin-fotolia.jpg
tags:
- Licenses
series:
- 202109
series_weight: 0
---

> Le changement de politique d’Elastic provoque des remous dans la communauté open source. Mais les usagers, qu’ils soient des organisations, des développeurs, ou des amateurs chevronnés, veulent éviter ce type de situation. En réalité, le choix de la licence compte autant que le mode de distribution de la propriété intellectuelle entre les contributeurs.
