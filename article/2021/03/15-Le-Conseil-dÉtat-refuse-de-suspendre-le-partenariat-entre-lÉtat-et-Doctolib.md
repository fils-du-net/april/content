---
site: Next INpact
title: "Le Conseil d'État refuse de suspendre le partenariat entre l'État et Doctolib (€)"
description: Test sérologique négatif
author: Marc Rees
date: 2021-03-15
href: https://www.nextinpact.com/article/45531/le-conseil-detat-refuse-suspendre-partenariat-entre-etat-et-doctolib
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/1954.jpg
seeAlso: "[Décision du Conseil d’Etat du 12 mars 2021](https://interhop.org/2021/03/12/communique-presse-decision-ce-rendezvous)"
tags:
- Vie privée
- Institutions
- Entreprise
series:
- 202111
series_weight: 0
---

> Les arguments de ces treize requérants n’ont pas suffi à convaincre. Ce 12 mars, le Conseil d’État a refusé de suspendre le partenariat passé entre l’État et Doctolib pour la prise des rendez-vous de la campagne de vaccination anti-covid. Un partenariat qui s’appuie sur les services d’Amazon Web Services.
