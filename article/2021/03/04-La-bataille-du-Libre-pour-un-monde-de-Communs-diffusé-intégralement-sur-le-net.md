---
site: ActuaLitté.com
title: "La bataille du Libre: pour un monde de Communs, diffusé intégralement sur le net"
author: Clément Solym
date: 2021-03-04
href: https://actualitte.com/article/99191/cinema/la-bataille-du-libre-pour-un-monde-de-communs-diffuse-integralement-sur-le-net
featured_image: https://actualitte.com/media/cache/width_944_webp/uploads/images/22la-bataille-du-libre-22-4-6040ced3d184a274393654.jpg
tags:
- Promotion
- Économie
series:
- 202109
---

> Magazine littéraire pour professionnels et curieux: les univers de l'édition décryptés à travers l'actualité du livre et ses acteurs. Rencontre de Gutenberg et du numérique.
