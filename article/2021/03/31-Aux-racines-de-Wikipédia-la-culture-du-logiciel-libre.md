---
site: ZDNet France
title: "Aux racines de Wikipédia, la culture du logiciel libre (€)"
author: Thierry Noisette
date: 2021-03-31
href: https://www.zdnet.fr/blogs/l-esprit-libre/aux-racines-de-wikipedia-la-culture-du-logiciel-libre-39920373.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/03/ADN_Wikipedia_Gene_Wiki_logo.jpg
tags:
- Partage du savoir
- Associations
series:
- 202113
series_weight: 0
---

> Lorsque est née en 2001 l'encyclopédie libre et collaborative, elle s'est appuyée sur le mode de fonctionnement et le droit des logiciels libres.
