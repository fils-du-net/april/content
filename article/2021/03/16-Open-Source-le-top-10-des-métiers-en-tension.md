---
site: Silicon
title: "Open Source: le top 10 des métiers en tension"
author: Ariane Beky
date: 2021-03-16
href: https://www.silicon.fr/open-source-top-10-metiers-tension-402532.html
featured_image: https://www.silicon.fr/wp-content/uploads/2016/02/shutterstock_316980971-e1615900488410.jpg
tags:
- Entreprise
series:
- 202111
series_weight: 0
---

> Data scientist, développeur, consultant technique... font partie des métiers les plus demandés par les entreprises de la branche en France.
