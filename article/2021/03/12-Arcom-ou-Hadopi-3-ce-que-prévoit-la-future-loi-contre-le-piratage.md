---
site: Next INpact
title: "Arcom ou Hadopi 3: ce que prévoit la future loi contre le piratage (€)"
description: "Hadopi + CSA = ARCOM"
author: Marc Rees
date: 2021-03-12
href: https://www.nextinpact.com/article/46409/arcom-ou-hadopi-3-ce-que-prevoit-future-loi-contre-piratage
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/9057.jpg
tags:
- HADOPI
- Internet
- Économie
series:
- 202110
series_weight: 0
---

> Hadopi 3… ou future loi relative à la «protection de l'accès du public aux oeuvres culturelles à l'ère numérique» (PAPOCEN). Voilà le nouveau visage de la lutte anti-piratage bientôt examiné au Sénat.
