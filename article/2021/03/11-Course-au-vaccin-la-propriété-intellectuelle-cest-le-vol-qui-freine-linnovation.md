---
site: The Conversation
title: "Course au vaccin: la propriété intellectuelle, c'est le «vol» qui freine l'innovation"
author: Michel Ferrary
date: 2021-03-11
href: https://theconversation.com/course-au-vaccin-la-propriete-intellectuelle-cest-le-vol-qui-freine-linnovation-156806
featured_image: https://images.theconversation.com/files/388565/original/file-20210309-13-1mz6vkv.jpg?ixlib=rb-1.1.0&rect=0%2C0%2C3994%2C2250&q=45&auto=format&w=926&fit=clip
tags:
- Sciences
- Innovation
series:
- 202110
series_weight: 0
---

> Actuellement, un brevet appartient à l'entreprise qui emploie l'inventeur. Un principe qui limite l'innovation, comme semble l'illustrer l'échec de grands groupes pharmaceutiques face à la Covid-19.
