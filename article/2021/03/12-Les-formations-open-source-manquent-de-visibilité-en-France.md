---
site: Le Monde Informatique
title: "Les formations open source manquent de visibilité en France"
author: Véronique Arène
date: 2021-03-12
href: https://www.lemondeinformatique.fr/actualites/lire-les-formations-open-source-manquent-de-visibilite-en-france-82270.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000077585.jpg
tags:
- Entreprise
series:
- 202110
series_weight: 0
---

> Avec un marché estimé à 5,2 Md€ en 2019, et 78 000 postes à pourvoir d'ici 2023, les technologies open source jouent un rôle de plus en plus important dans l'évolution des métiers du numérique, relève une étude présentée récemment par l'Opiiec. Alors que les besoins en compétences continuent d'augmenter, l'offre de formation initiale sur le territoire français est peu ou pas connue des professionnels comme des éventuels apprenants.
