---
site: l'Humanité.fr
title: "Sécurité. Les cyberattaques, une menace tout sauf virtuelle (€)"
author: Pierric Marissal
date: 2021-03-03
href: https://www.humanite.fr/securite-les-cyberattaques-une-menace-tout-sauf-virtuelle-700818
featured_image: https://www.humanite.fr/sites/default/files/styles/1048x350/public/images/cybercriminalite.jpg
tags:
- Vie privée
- april
series:
- 202109
series_weight: 0
---

> Les pirates informatiques ont récemment mis à l'arrêt des hôpitaux français. Les prochaines victimes pourraient être des réseaux d'énergie ou de transports. Seule une maîtrise de toute la chaîne de production permettrait de se protéger.
