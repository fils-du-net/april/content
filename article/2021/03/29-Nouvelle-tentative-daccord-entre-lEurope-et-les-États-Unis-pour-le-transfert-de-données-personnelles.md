---
site: Acteurs Publics 
title: "Nouvelle tentative d'accord entre l'Europe et les États-Unis pour le transfert de données personnelles (€)"
author: ActeursPublics" 
date: 2021-03-29
href: https://acteurspublics.fr/articles/nouvelle-tentative-daccord-entre-leurope-et-les-etats-unis-pour-le-transfert-de-donnees-personnelles
tags:
- Vie privée
- Europe
- International
series:
- 202113
---

> L’été dernier, la Cour de justice de l’Union européenne avait invalidé le mécanisme de transfert de données personnelles de l’UE vers les États-Unis, estimant qu’il n’offrait pas de garanties au regard du RGPD.
