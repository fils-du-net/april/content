---
site: RTBF Info
title: "La séquence ARN du vaccin Moderna publiée en open source par des chercheurs de Stanford"
date: 2021-03-31
href: https://www.rtbf.be/info/societe/detail_la-sequence-arn-du-vaccin-moderna-publiee-en-open-source-par-des-chercheurs-de-stanford?id=10731194
featured_image: https://ds1.static.rtbf.be/article/image/370x208/5/d/2/b47422c84bd49942d148c2396b3b0536-1617135298.png
tags:
- Sciences
- Partage du savoir
series:
- 202113
series_weight: 0
---

> Pour combler les manques liés à la frilosité des firmes pharmaceutiques de révéler des "ingrédients" de leurs vaccins contre le covid, des chercheurs de l’Université de Stanford ont décidé de publier en open source le séquençage des deux vaccins ARNm Moderna et Pfizer/BioNTech. Leur objectif: permettre aux chercheurs et cliniciens, qui pour des raisons de recherche ou thérapeutique doivent séquencer des ARN, de pouvoir facilement identifier ces séquences vaccinales.
