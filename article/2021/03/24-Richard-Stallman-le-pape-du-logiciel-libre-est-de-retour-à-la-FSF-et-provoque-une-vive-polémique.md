---
site: 01net.
title: "Richard Stallman, le pape du logiciel libre, est de retour à la FSF... et provoque une vive polémique"
author: Gilbert Kallenborn
date: 2021-03-24
href: https://www.01net.com/actualites/richard-stallman-le-pape-du-logiciel-libre-est-de-retour-a-la-fsf-et-provoque-une-vive-polemique-2039088.html
featured_image: https://img.bfmtv.com/c/630/420/3aa/b063bb2a7dde0a081b205bfa66178.jpg
tags:
- Promotion
series:
- 202112
---

> L'apôtre du libre réintègre à nouveau le conseil d'administration de la Free Software Foundation, après en avoir démissionné en 2019 à la suite de propos douteux. Mais une pétition circule déjà pour demander son éviction de toutes ses fonctions.
