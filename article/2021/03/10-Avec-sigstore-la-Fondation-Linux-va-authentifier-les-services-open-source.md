---
site: Le Monde Informatique
title: "Avec sigstore, la Fondation Linux va authentifier les services open source"
author: Lucian Constantin
date: 2021-03-10
href: https://www.lemondeinformatique.fr/actualites/lire-avec-sigstore-la-fondation-linux-va-authentifier-les-services-open-source-82238.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000077542.jpg
tags:
- Innovation
series:
- 202110
---

> Les composantes open source des logiciels sont de plus en plus ciblées par les pirates informatiques ces dernières années. Pour sécuriser la chaîne d'approvisionnement des logiciels, la Fondation Linux vient d'annoncer le projet sigstore permettant de signer les versions de logiciels, images de containers et code binaire. Un projet sur lequel travaillent Google, Red Hat et l'Université Purdue;
