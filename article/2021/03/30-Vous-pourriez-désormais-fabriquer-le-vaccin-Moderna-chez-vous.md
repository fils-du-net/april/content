---
site: korii.
title: "Vous pourriez désormais fabriquer le vaccin Moderna chez vous"
author: Thomas Burgel
date: 2021-03-30
href: https://korii.slate.fr/tech/vaccin-moderna-covid-19-sequence-arnm-publiee-github-chercheurs-stanford#xtor=RSS-2
featured_image: https://korii.slate.fr/sites/default/files/styles/1440x600/public/063_1232008786.jpg
tags:
- Sciences
- Entreprise
series:
- 202113
---

> Sa séquence ARNm a été publiée sur GitHub par des scientifiques de Stanford. Bientôt des vaccins DIY?
