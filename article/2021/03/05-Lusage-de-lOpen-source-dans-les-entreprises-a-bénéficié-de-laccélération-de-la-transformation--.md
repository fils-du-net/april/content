---
site: IT Social
title: "L'usage de l'Open source dans les entreprises a bénéficié de l'accélération de la transformation"
author: Mourad Krim
date: 2021-03-05
href: https://itsocial.fr/enjeux-it/enjeux-strategie/dsi/lusage-de-lopen-source-dans-les-entreprises-a-beneficie-de-lacceleration-de-la-transformation
featured_image: https://itsocial.fr/wp-content/uploads/2021/03/Capture-d%E2%80%99%C3%A9cran-2021-03-12-065313.jpg
tags:
- Entreprise
series:
- 202109
---

> Au cours de la dernière décennie, la perception de l’Open source dans les entreprises est passée d’une diminution de la facture logicielle à une flexibilité et un accès à l’innovation. Et ce changement semble s’accélérer, malgré les doutes qui persistent sur le support, la compatibilité, et la sécurité du code.
