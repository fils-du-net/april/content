---
site: Le Monde.fr
title: "Dans l'éducation nationale, la pandémie relance le chantier sur la protection des données"
author: Cécile Peltier
date: 2021-03-10
href: https://www.lemonde.fr/campus/article/2021/03/10/dans-l-education-nationale-la-pandemie-relance-le-chantier-sur-la-protection-des-donnees_6072623_4401467.html
featured_image: https://img.lemde.fr/2021/03/10/0/0/5800/3859/1328/0/45/0/19f6eb4_823855109-000-8z73wm.jpg
tags:
- Éducation
- Vie privée
- Internet
series:
- 202110
series_weight: 0
---

> Depuis le début de la crise sanitaire, l’explosion du numérique à l’école a mis le doigt sur les nombreuses failles en matière de respect des données personnelles. Les opérations de sensibilisation, indispensables, se multiplient. Le point, à l’occasion du lancement d’In-FINE, forum international du numérique pour l’éducation.
