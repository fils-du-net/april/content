---
site: BFMtv
title: "Données de santé: le chiffrement de Doctolib décrié, la plateforme dément"
author: Elsa Trujillo
date: 2021-03-11
href: https://www.bfmtv.com/tech/donnees-de-sante-chez-doctolib-un-chiffrement-insuffisant_AN-202103110002.html
featured_image: https://images.bfmtv.com/S09p6UOdS-j4h8ijoxDFYyTEmiY=/0x39:768x471/768x0/images/Le-leader-francais-de-la-teleconsultation-Doctolib-a-annonce-avoir-ete-victime-dun-vol-de-donnees-portant-sur-pres-de-6000-rendez-vous-tout-en-assurant-quaucune-information-relative-au-dossier-medical-des-patients-navait-ete-derobee-378683.jpg
tags:
- Vie privée
series:
- 202110
---

> La plateforme de prise de rendez-vous médicaux, qui a trouvé ces dernières semaines sa place dans la campagne de vaccination, est accusée de chiffrer ses données de façon incomplète. Doctolib s'en défend fermement.
