---
site: ouest-france.fr
title: "Gullivigne, l’informatique libre et innovante"
date: 2021-03-20
href: https://www.ouest-france.fr/pays-de-la-loire/clisson-44190/clisson-gullivigne-l-informatique-libre-et-innovante-7193897
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMTAzZTkzOTc1M2RiZWNmNTYyMjg5YWIyZTc3OTllMmY1ZTk
tags:
- Associations
series:
- 202111
series_weight: 0
---

> L’association Gullivigne, spécialisée dans le logiciel libre et l’informatique, a désormais des locaux à elle au sein du Mini hub d’ADI. Ici, lorsque la situation sanitaire le permettra, ils pourront proposer à leurs adhérents leurs activités liées à l’informatique.
