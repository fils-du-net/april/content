---
site: Silicon
title: "Open source d'entreprise: d'abord une affaire d'infrastructure?"
author: Clément Bohic
date: 2021-03-04
href: https://www.silicon.fr/open-source-entreprise-infrastructure-401459.html
featured_image: https://www.silicon.fr/wp-content/uploads/2021/03/Red-Hat-open-source-entreprise.jpg
tags:
- Entreprise
series:
- 202109
series_weight: 0
---

> La modernisation d'infrastructure demeure, dans les statistiques de Red Hat, le principal usage de l'open source d'entreprise. Les bénéfices et les barrières perçus évoluent.
