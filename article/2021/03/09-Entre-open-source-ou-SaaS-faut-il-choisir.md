---
site: ITforBusiness
title: "Entre open source ou SaaS, faut-il choisir?"
date: 2021-03-09
href: https://www.itforbusiness.fr/entre-open-source-ou-saas-faut-il-choisir-42711
featured_image: https://www.itforbusiness.fr/wp-content/uploads/2021/03/shutterstock_1684911019.jpg
tags:
- Informatique en nuage
- Entreprise
series:
- 202110
series_weight: 0
---

> De nombreuses entreprises ont amorcé leur transformation numérique, parfois à marche forcée en raison de la crise de la Covid-19. Mais l’évolution des technologies au cours des dix dernières années les met aujourd’hui face à un problème: que choisir entre «l’ancien monde», représenté par l’open source, et les nouvelles possibilités offertes par le logiciel en tant que service (SaaS)?
