---
site: Numerama
title: "Pourquoi le retour de Richard Stallman agace le monde du logiciel libre"
author: Julien Lausson
date: 2021-03-26
href: https://www.numerama.com/tech/699948-pourquoi-le-retour-de-richard-stallman-agace-le-monde-du-logiciel-libre.html
featured_image: https://www.numerama.com/wp-content/uploads/2016/04/richard-stallman-1900-e1616777101649.jpg
seeAlso: "[Retour de Richard Stallman au conseil d'administration de la Fondation pour le Logiciel Libre (FSF)](https://www.april.org/retour-de-richard-stallman-au-conseil-d-administration-de-la-fondation-pour-le-logiciel-libre-fsf)"
tags:
- Promotion
- april
series:
- 202112
series_weight: 0
---

> Que se passe-t-il avec Richard Stallman? Depuis une semaine, cette figure du logiciel libre est contestée. En cause, son retour à un poste de direction à la FSF, sur fond d'affaire Epstein.
