---
site: Next INpact
title: "Comment le gouvernement veut élargir le blocage des sites"
author: Marc Rees
date: 2021-03-29
href: https://www.nextinpact.com/article/46592/comment-gouvernement-veut-elargir-blocage-sites
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/463.jpg
tags:
- Internet
- Institutions
series:
- 202113
---

> Dans un amendement au projet de loi Séparatisme, le gouvernement étend la procédure de blocage des sites. Des hébergeurs aux FAI aujourd’hui, demain l’exécutif souhaite que des actions puissent être dirigées contre «toute personne» susceptible de contribuer à ces restrictions d’accès. Explications.
