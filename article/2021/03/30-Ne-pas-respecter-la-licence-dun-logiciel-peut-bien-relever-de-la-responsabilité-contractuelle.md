---
site: cio-online.com
title: "Ne pas respecter la licence d'un logiciel peut bien relever de la responsabilité contractuelle"
date: 2021-03-30
href: https://www.cio-online.com/actualites/lire-ne-pas-respecter-la-licence-d-un-logiciel-peut-bien-relever-de-la-responsabilite-contractuelle-13068.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000016845.jpg
tags:
- Licenses
- Institutions
series:
- 202113
series_weight: 0
---

> La Cour d'Appel de Paris a confirmé un jugement établissement qu'un non-respect de la licence GNU GPL relève de la faute contractuelle.
