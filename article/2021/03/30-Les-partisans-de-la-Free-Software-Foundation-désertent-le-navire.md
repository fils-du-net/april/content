---
site: ZDNet France
title: "Les partisans de la Free Software Foundation désertent le navire (€)"
author: Steven J. Vaughan-Nichols
date: 2021-03-30
href: https://www.zdnet.fr/actualites/les-partisans-de-la-free-software-foundation-desertent-le-navire-39920263.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2021/03/richard-m-stallman__w1200.jpg
tags:
- Promotion
series:
- 202113
---

> Les principales entreprises qui soutiennent la FSF l'abandonnent, tandis que les membres du conseil d'administration et les dirigeants démissionnent de l'organisation. L'indignation suscitée par le retour de Richard M. Stallman au conseil d'administration ne cesse de croître.
