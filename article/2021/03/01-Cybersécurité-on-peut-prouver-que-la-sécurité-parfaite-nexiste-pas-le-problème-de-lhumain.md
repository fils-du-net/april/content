---
site: Next INpact
title: "Cybersécurité: on peut prouver que «la sécurité parfaite n'existe pas», le problème de l'humain (€)"
author: Sébastien Gavois
date: 2021-03-01
href: https://www.nextinpact.com/article/46257/cybersecurite-on-peut-prouver-que-la-securite-parfaite-nexiste-pas-probleme-humain
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/9662.jpg
tags:
- Vie privée
- Institutions
series:
- 202109
---

> La cybersécurité est un vaste domaine, aux conséquences importantes. La recherche y est cruciale pour améliorer les défenses, mais aussi réagir en cas d'attaque. En marge d'annonces gouvernementales, CEA, CNRS et Inria brossent un portrait de la situation et des pistes d'action.
