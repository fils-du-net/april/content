---
site: Heidi.news
title: "Wikipédia veut faire passer les GAFA à la caisse"
date: 2021-03-19
href: https://www.heidi.news/sciences/wikipedia-veut-faire-passer-les-gafa-a-la-caisse
featured_image: https://heidi-17455.kxcdn.com/photos/12da9ede-5084-45d2-a8e0-d81c7ee043f8/large
tags:
- Partage du savoir
series:
- 202111
series_weight: 0
---

> Les services de Google, Apple ou Amazon exploitent fréquemment les contenus de l’encyclopédie en ligne Wikipédia. Une nouvelle division de la Fondation, Wikimedia Enterprise, aura pour mission de faire payer les géants du net pour un accès premium à son contenu.
