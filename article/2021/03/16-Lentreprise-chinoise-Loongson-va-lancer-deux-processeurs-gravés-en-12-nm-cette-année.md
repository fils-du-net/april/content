---
site: tom's Hardware
title: "L'entreprise chinoise Loongson va lancer deux processeurs gravés en 12 nm cette année"
author: Rémi Bouvet
date: 2021-03-16
href: https://www.tomshardware.fr/lentreprise-chinoise-loongson-va-lancer-deux-processeurs-graves-en-12-nm-cette-annee
featured_image: https://cdn.tomshardware.fr/content/uploads/sites/3/2021/03/loongson.jpg
tags:
- Matériel libre
series:
- 202111
series_weight: 0
---

> Les Loongson 3A5000 et Loongon 3C5000, peut-être les derniers représentants MIPS avant le passage à l'architecture RISC-V.
