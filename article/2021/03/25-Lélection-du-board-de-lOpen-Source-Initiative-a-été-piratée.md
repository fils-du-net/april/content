---
site: ZDNet France
title: "L'élection du board de l'Open Source Initiative a été piratée (€)"
author: Steven J. Vaughan-Nichols
date: 2021-03-25
href: https://www.zdnet.fr/actualites/l-election-du-board-de-l-open-source-initiative-a-ete-piratee-39920035.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2015/12/Apple-open-source-140x105__w1200.jpg
tags:
- Associations
- Promotion
series:
- 202112
series_weight: 0
---

> L'organisation dédiée à la promotion des logiciels et des licences open source compte enquêter sur le piratage et relancer une nouvelle élection par la suite.
