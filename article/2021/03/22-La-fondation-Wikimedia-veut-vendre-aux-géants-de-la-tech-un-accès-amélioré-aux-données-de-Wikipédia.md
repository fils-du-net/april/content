---
site: ZDNet France
title: "La fondation Wikimedia veut vendre aux géants de la tech un accès amélioré aux données de Wikipédia (€)"
author: Thierry Noisette
date: 2021-03-22
href: https://www.zdnet.fr/blogs/l-esprit-libre/la-fondation-wikimedia-veut-vendre-aux-geants-de-la-tech-un-acces-ameliore-aux-donnees-de-wikipedia-39919851.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/03/Wikipedia_Paid_contributions_slogan.svg.jpg
tags:
- Partage du savoir
- Entreprise
series:
- 202112
series_weight: 0
---

> Un accès pour les très grandes entreprises utilisant du contenu Wikimedia 'et qui ont des besoins considérables en termes de volume, rapidité, et fiabilité de service' leur sera proposé moyennant finances d'ici quelques mois.
