---
site: lesoir.be
title: "Didier Pittet, l’homme qui a mis le gel hydroalcoolique à la portée de tous"
date: 2021-02-09
href: https://plus.lesoir.be/354084/article/2021-02-09/didier-pittet-lhomme-qui-mis-le-gel-hydroalcoolique-la-portee-de-tous
featured_image: https://plus.lesoir.be/sites/default/files/dpistyles_v2/ena_16_9_extra_big/2021/02/09/node_354084/27914548/public/2021/02/09/B9726085537Z.1_20210209163822_000+GT0HIMN9D.1-0.jpg
tags:
- Sciences
- Partage du savoir
series:
- 202106
---

> L’infectiologue suisse a imaginé une solution de gel hydroalcoolique efficace et libre d’accès. Une démarche cruciale pour cette épidémie et qui sort du lot alors que la bataille des vaccins fait rage.
