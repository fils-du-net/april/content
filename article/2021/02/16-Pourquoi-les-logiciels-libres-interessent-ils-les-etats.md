---
site: France Culture
title: "Pourquoi les logiciels libres intéressent-ils les Etats?"
author: Guillaume Erner
date: 2021-02-16
href: https://www.franceculture.fr/emissions/la-question-du-jour/pourquoi-les-logiciels-libres-interessent-ils-les-etats
featured_image: https://cdn.radiofrance.fr/s3/cruiser-production/2021/02/0d9ddf91-0f7c-47e1-980b-0261c22ce414/838_gettyimages-1182904303.webp
tags:
- Sensibilisation
- Open Data
- Institutions
series:
- 202107
series_weight: 0
---

> Enjeux politiques, démocratiques, économiques, géopolitiques…  pourquoi les logiciels libres peuvent-ils intéresser les Etats, à l'heure où plusieurs signaux laissent penser à une ouverture en leur faveur?
