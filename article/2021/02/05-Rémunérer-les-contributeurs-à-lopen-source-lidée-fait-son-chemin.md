---
site: Le Monde Informatique
title: "Rémunérer les contributeurs à l'open source: l'idée fait son chemin"
author: Maryse Gros
date: 2021-02-05
href: https://www.lemondeinformatique.fr/actualites/lire-remunerer-les-contributeurs-a-l-open-source-l-idee-fait-son-chemin-81887.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000076905.jpg
tags:
- Économie
series:
- 202105
series_weight: 0
---

> Une enquête menée auprès des développeurs montre que les contributeurs au projet open source devraient être rétribués pour leur travail. Une tendance marquée chez les jeunes développeurs.
