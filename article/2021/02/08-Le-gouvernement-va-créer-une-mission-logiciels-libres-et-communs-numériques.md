---
site: ZDNet France
title: "Le gouvernement va créer une mission logiciels libres et communs numériques"
author: Thierry Noisette
date: 2021-02-08
href: https://www.zdnet.fr/blogs/l-esprit-libre/le-gouvernement-va-creer-une-mission-logiciels-libres-et-communs-numeriques-39917615.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/02/transformation_loading_Pixabay.jpg
tags:
- Open Data
- april
- Institutions
series:
- 202106
series_weight: 0
---

> Jean Castex a annoncé que cette mission, qui fait suite au rapport Bothorel, sera créée au sein de la direction interministérielle du numérique (Dinum).
