---
site: Siècle Digital
title: "D'après l'OpenForum Europe l'open source se révèle lucratif"
date: 2021-02-17
href: https://siecledigital.fr/2021/02/17/openforum-open-source-lucratif
featured_image: https://thumbor.sd-cdn.fr/vwFUCQw27IMA-MulDpMwbx-oDQ0=/940x550/cdn.sd-cdn.fr/wp-content/uploads/2020/10/ta%CC%82ches-logiciel-de-comptabilite%CC%81-scaled.jpg
tags:
- Économie
series:
- 202107
series_weight: 0
---

> Une étude réalisée par l'OpenForum Europe estime que les retombées économiques de l'open source sont comptées entre 65 et 95 milliards d'euros.
