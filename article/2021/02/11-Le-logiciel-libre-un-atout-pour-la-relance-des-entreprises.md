---
site: Les Echos
title: "Le logiciel libre, un atout pour la relance des entreprises"
author: Frank Niedercorn
date: 2021-02-11
href: https://business.lesechos.fr/entrepreneurs/numerique-cybersecurite/0610235010723-le-logiciel-libre-un-atout-pour-la-relance-des-pme-342172.php#xtor=CS1-35
featured_image: https://business.lesechos.fr/medias/2021/02/11/342172_le-logiciel-libre-un-atout-pour-la-relance-le-logiciel-libre-un-atout-pour-la-relance-web-tete-061367705072-2396110_1000x533.jpg
tags:
- Open Data
series:
- 202106
---

> Très dynamique en France, le monde du logiciel «open source» pèse près de 6 milliards d'euros. Il est désormais au coeur des enjeux de souveraineté, d'indépendance et de transformation numérique des entreprises poussés par la crise sanitaire.
