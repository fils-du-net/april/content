---
site: internet ACTU.net
title: "Regouverner (2/2): multiplier les outils d'une gouvernance distribuée"
date: 2021-02-18
href: https://www.internetactu.net/2021/02/18/regouverner-2-2-multiplier-les-outils-dune-gouvernance-distribuee
featured_image: https://www.internetactu.net/wp-content/uploads/2021/02/The-Open-Governance-Index-Visual-ly.png
tags:
- Licenses
series:
- 202107
---

> Continuons avec la remarquable critique des errances de l’open source que formule Nathan Schneider (@ntnsndr) dans son article de recherche «la tyrannie de l’ouverture». La contestation ne s’arrête pas à la démultiplication de nouvelles licences ouvertes, elle interroge profondément les questions de gouvernance pour faire de nouvelles propositions, forcément passionnantes!
