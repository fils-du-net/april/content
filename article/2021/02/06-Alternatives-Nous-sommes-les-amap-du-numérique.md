---
site: l'Humanité.fr
title: "Alternatives. «Nous sommes les amap du numérique!» (€)"
href: https://www.humanite.fr/alternatives-nous-sommes-les-amap-du-numerique-699656
date: 2021-02-06
featured_image: https://www.humanite.fr/sites/default/files/styles/1048x350/public/images/gafam_4_hd3mon_web_0.jpg
tags:
- Sensibilisation
- Associations
- Internet
series:
- 202105
series_weight: 0
---

> Entretien  PeerTube, Mobilizon, Mastodon... Grâce à la communauté du logiciel libre, Framasoft, «association d'éducation populaire» du numérique, proposent des plate-formes décentralisés et des outils partagés alternatifs à Youtube ou Facebook. Le but, court-circuiter les géants du Web. Rencontre avec son délégué général, Pierre-Yves Gosset.
