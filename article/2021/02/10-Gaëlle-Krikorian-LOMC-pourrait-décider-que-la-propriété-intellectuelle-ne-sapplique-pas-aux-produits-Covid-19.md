---
site: Le Monde.fr
title: "Gaëlle Krikorian: «L'OMC pourrait décider que la propriété intellectuelle ne s'applique pas aux produits Covid-19»"
author: Claire Legros
date: 2021-02-10
href: https://www.lemonde.fr/idees/article/2021/02/10/gaelle-krikorian-l-omc-pourrait-decider-que-la-propriete-intellectuelle-ne-s-applique-pas-aux-produits-covid-19_6069403_3232.html
featured_image: https://img.lemde.fr/2021/02/09/0/0/1890/2598/1328/0/45/0/46cd8a3_289901691-gae-lle-krikorian-2.jpg
tags:
- Sciences
- Innovation
series:
- 202106
series_weight: 0
---

> En pleine pandémie, la spécialiste des questions de propriété intellectuelle appelle, dans un entretien au «Monde», à une remise à plat de l'économie du médicament et à la nécessaire transparence des négociations.
