---
site: Le Point
title: "Merci «Perseverance»! Le logiciel libre arrive sur Mars"
author: Aurélie Jean
date: 2021-02-28
href: https://www.lepoint.fr/invites-du-point/aurelie-jean-merci-perseverance-le-logiciel-libre-arrive-sur-mars-28-02-2021-2415748_420.php
featured_image: https://static.lpnt.fr/images/2021/02/28/21370254lpw-21372222-article-jpg_7733787_660x281.jpg
tags:
- Sciences
series:
- 202108
series_weight: 0
---

> Le petit rover est désormais sur la planète rouge avec ses logiciel et système d'exploitation libres, et ça change tout.
