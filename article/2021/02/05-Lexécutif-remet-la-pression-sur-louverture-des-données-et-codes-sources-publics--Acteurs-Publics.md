---
site: Acteurs Publics 
title: "L'exécutif remet la pression sur l'ouverture des données et codes sources publics (€)"
author: Emile Marzolf
date: 2021-02-05
href: https://acteurspublics.fr/articles/lexecutif-remet-la-pression-sur-louverture-des-donnees-et-codes-sources-publics
tags:
- Institutions
- Open Data
series:
- 202105
---

> Le gouvernement Castex a annoncé, lors du cinquième Comité interministériel de la transformation publique, organisé ce vendredi 5 février, l’ouverture de plusieurs bases de données, codes sources et algorithmes publics d’ici la fin de l’année. Les ministres sont priés de définir une feuille de route et une gouvernance pour mettre en œuvre une politique de la donnée “volontariste” au service de la transparence et de l'efficacité de l'action publique.
