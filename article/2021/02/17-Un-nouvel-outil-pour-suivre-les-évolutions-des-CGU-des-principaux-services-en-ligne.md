---
site: ZDNet France
title: "Un nouvel outil pour suivre les évolutions des CGU des principaux services en ligne"
author: Clarisse Treilles
date: 2021-02-17
href: https://www.zdnet.fr/actualites/un-nouvel-outil-pour-suivre-les-evolutions-des-cgu-des-principaux-services-en-ligne-39918097.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/02/Whatsapp%20Illustration%20A%20620__w1200.jpg
tags:
- Internet
series:
- 202107
---

> Open Terms Archive, un logiciel libre et collaboratif, et l'outil Scripta Manent permettent de suivre les évolutions des conditions générales d'utilisation (CGU) des principaux fournisseurs de services en ligne.
