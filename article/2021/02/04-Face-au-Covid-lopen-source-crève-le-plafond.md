---
site: Journal du Net
title: "Face au Covid, l'open source crève le plafond (€)"
author: Antoine Crochet-Damais
date: 2021-02-04
href: https://www.journaldunet.com/solutions/dsi/1497337-face-au-covid-l-open-source-creve-le-plafond
featured_image: https://img-0.journaldunet.com/JrI1wHIvbOsjkssP5mhSlZdwI1o=/1080x/smart/564e36c3109b4af29e66d0250aed9059/ccmcms-jdn/22067263.jpg
tags:
- Innovation
series:
- 202105
---

> Face au Covid, l'open source crève le plafond Sur GitHub, le nombre de nouveaux projets s'envolent. Sur le terrain, la montée en puissance de WordPress s'accélère. Quant aux recrutements de profils open source, ils restent soutenus.
