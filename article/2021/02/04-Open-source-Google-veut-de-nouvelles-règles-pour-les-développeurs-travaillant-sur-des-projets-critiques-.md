---
site: ZDNet France
title: "Open source: Google veut de nouvelles règles pour les développeurs travaillant sur des projets 'critiques'"
author: Liam Tung
date: 2021-02-04
href: https://www.zdnet.fr/actualites/open-source-google-veut-de-nouvelles-regles-pour-les-developpeurs-travaillant-sur-des-projets-critiques-39917455.htm
featured_image: https://zdnet4.cbsistatic.com/hub/i/r/2020/11/10/001249d8-e399-463d-a4ec-09e0d452b448/resize/1200xauto/ded3d983c7fc69daeb7099fd19acd1d8/computeruseristock-910999684a.jpg
tags:
- Innovation
- Entreprise
series:
- 202105
series_weight: 0
---

> Si votre projet de logiciel open source est considéré comme 'critique', vous pourriez être confronté à beaucoup plus de travail et de responsabilités à l'avenir. Mais pour l'instant, il ne s'agit que de quelques idées de quelques uns des meilleurs ingénieurs de Google.
