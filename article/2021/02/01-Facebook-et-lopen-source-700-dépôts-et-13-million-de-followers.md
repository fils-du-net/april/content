---
site: ZDNet France
title: "Facebook et l'open source: 700 dépôts et 1,3 million de followers"
author: Daphne Leprince-Ringuet
date: 2021-02-01
href: https://www.zdnet.fr/actualites/facebook-et-l-open-source-700-depots-et-13-million-de-followers-39917177.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2021/01/FacebookHQ__w1200.jpg
tags:
- Innovation
- Entreprise
series:
- 202105
---

> La plateforme open source de Facebook ne cesse de croître depuis son lancement et son succès ne se dément pas. L'an dernier, le projet a touché près de 1,3 million de followers.
