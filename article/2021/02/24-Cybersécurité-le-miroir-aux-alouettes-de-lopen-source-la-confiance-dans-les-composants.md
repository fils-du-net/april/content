---
site: Next INpact
title: "Cybersécurité: le miroir aux alouettes de l'open source, la confiance dans les composants (€)"
author: Sébastien Gavois
date: 2021-02-24
href: https://www.nextinpact.com/article/46216/cybersecurite-miroir-aux-alouettes-lopen-source-confiance-dans-composants
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/9658.jpg
tags:
- Innovation
series:
- 202108
series_weight: 0
---

> En matière de cybersécurité, l’open source est un bon point de départ, mais pas suffisant pour autant. La traçabilité des composants est aussi importante, comme nous le rappelle l’affaire Supermicro. Quels sont les risques et les solutions envisagées sur ces sujets? Une demi-douzaine de chercheurs répondent à ces questions.
