---
site: Acteurs Publics
title: "La DSI de l'État se pose en arbitre des solutions numériques pour le secteur public (€)"
author: Emile Marzolf
date: 2021-02-01
href: https://www.acteurspublics.fr/articles/la-dsi-de-letat-se-pose-en-arbitre-des-solutions-numeriques-pour-le-secteur-public
tags:
- Institutions
- Référentiel
series:
- 202105
---

> La direction interministérielle du numérique de l’État entre dans la dernière phase de la conception d’un catalogue de produits numériques sélectionnés par ses soins pour les recommander aux administrations publiques. Un appel est lancé pour identifier les solutions qui mériteraient d’y être référencées.
