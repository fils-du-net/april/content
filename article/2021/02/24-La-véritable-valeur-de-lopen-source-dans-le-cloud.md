---
site: Le Monde Informatique
title: "La véritable valeur de l'open source dans le cloud"
author: Matt Asay
date: 2021-02-24
href: https://www.lemondeinformatique.fr/actualites/lire-la-veritable-valeur-de-l-open-source-dans-le-cloud-82069.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000077274.jpg
tags:
- Informatique en nuage
- Entreprise
series:
- 202108
series_weight: 0
---

> C'est une erreur de croire que l'open source dans le cloud peut protéger du verrouillage des fournisseurs. En revanche, l'open source apporte certainement aux développeurs une liberté et une indépendance dans leur parcours professionnel.
