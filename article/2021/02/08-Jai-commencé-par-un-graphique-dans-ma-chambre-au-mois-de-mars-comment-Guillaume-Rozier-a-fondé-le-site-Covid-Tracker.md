---
site: BFMtv
title: "«J'ai commencé par un graphique, dans ma chambre au mois de mars»: comment Guillaume Rozier a fondé le site «Covid Tracker»"
author:  Guillaume Descours
date: 2021-02-08
href: https://rmc.bfmtv.com/emission/au-depart-je-voulais-voir-comment-se-comportait-la-france-par-rapport-a-l-italie-comment-guillaume-rozier-a-fonde-le-site-covid-tracker-2033883.html
featured_image: https://img.bfmtv.com/i/0/0/bcd/3a81df9be4875bcf28bc40f68e8b6.jpeg
tags:
- Innovation
- Open Data
series:
- 202106
series_weight: 0
---

> Le site, devenu une référence en pleine épidémie, collecte une multitude de chiffres sur la pandémie de Covid-19 et propose des data-visualisations, pour mieux comprendre l'épidémie. Il était dans les «Grandes Gueules» ce lundi.
