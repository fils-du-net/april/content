---
site: ZDNet France
title: "Wikipédia se dote d'un code de conduite universel"
date: 2021-02-05
href: https://www.zdnet.fr/actualites/wikipedia-se-dote-d-un-code-de-conduite-universel-39917487.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/09/Wikipedia__w1200.jpg
tags:
- Associations
- Partage du savoir
series:
- 202105
series_weight: 0
---

> La Wikimedia Foundation, l'association à but non lucratif qui administre Wikipédia, a lancé un code de conduite universel, le premier du genre, pour lutter contre les harcèlements au sein de sa communauté et promouvoir une connaissance libre.
