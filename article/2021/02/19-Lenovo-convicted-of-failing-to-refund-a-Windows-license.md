---
site: Aroged
title: "Lenovo convicted of failing to refund a Windows license"
author: Aroged
date: 2021-02-19
href: https://www.aroged.com/2021/02/19/lenovo-convicted-of-failing-to-refund-a-windows-license
featured_image: https://www.aroged.com/wp-content/uploads/2021/02/Lenovo-convicted-of-failing-to-refund-a-Windows-license-1140x570.jpg
tags:
- Logiciels privateurs
- English
series:
- 202107
---

> In this article I bring you an interesting news, released by theItalian Linux Society, which will certainly be of interest to most Italian GNU / Linux users. The story stars Luca Bonissi, a passionate supporter of free software, champion of the right to reimbursement of the Windows license, who managed to obtain compensation of twenty thousand euros from Lenovo.
