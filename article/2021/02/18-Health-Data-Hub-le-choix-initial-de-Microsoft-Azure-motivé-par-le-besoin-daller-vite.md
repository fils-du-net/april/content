---
site: ZDNet France
title: "Health Data Hub: le choix initial de Microsoft Azure motivé par le besoin d'aller vite"
author: Clarisse Treilles
date: 2021-02-18
href: https://www.zdnet.fr/actualites/health-data-hub-le-choix-initial-de-microsoft-azure-motive-par-le-besoin-d-aller-vite-39918157.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/01/e-sante__w1200.jpg
tags:
- Vie privée
- Entreprise
- Informatique en nuage
series:
- 202107
series_weight: 0
---

> Le choix de Microsoft Azure pour héberger les données santé du Health Data Hub a été motivé par la volonté d'aller vite. Même si, à long terme, cette solution se révèle plus onéreuse que d'autres alternatives française.
