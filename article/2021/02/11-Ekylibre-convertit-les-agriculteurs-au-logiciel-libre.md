---
site: Les Echos
title: "Ekylibre convertit les agriculteurs au logiciel libre"
author: Frank Niedercorn
date: 2021-02-11
href: https://www.lesechos.fr/pme-regions/nouvelle-aquitaine/ekylibre-convertit-les-agriculteurs-au-logiciel-libre-1289355
featured_image: https://media.lesechos.com/api/v1/images/view/6024d6f2d286c271c0006f95/1280x720/061368324120-web-tete.jpg
tags:
- Entreprise
series:
- 202106
series_weight: 0
---

> Son logiciel permet de gérer toutes les activités d'une exploitation agricole. Une nouvelle version destinée à la viticulture sera utilisable par l'exploitant dans les vignes grâce à son smartphone repéré par GPS et doté de la reconnaissance vocale.
