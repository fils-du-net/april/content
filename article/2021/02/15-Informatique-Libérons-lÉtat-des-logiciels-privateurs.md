---
site: l'Humanité.fr
title: "Informatique. «Libérons l'État des logiciels privateurs» (€)"
author: Pierric Marissal
date: 2021-02-15
href: https://www.humanite.fr/informatique-liberons-letat-des-logiciels-privateurs-700130
featured_image: https://www.humanite.fr/sites/default/files/styles/wysiwyg_portrait_petit/public/images/82600.HR.jpg
tags:
- april
- Institutions
- Promotion
- Éducation
- Marchés publics
series:
- 202107
series_weight: 0
---

> Jean Castex vient d'annoncer la création d'une mission sur les logiciels libres. Avec des moyens et une volonté politique, elle pourrait changer la face des services publics.
