---
site: LeMagIT
title: "Financement de l’open source: le changement de licence d’Elasticsearch inquiète"
author: Beth Pariseau
date: 2021-02-18
href: https://www.lemagit.fr/actualites/252496544/Financement-de-lopen-source-le-changement-de-licence-dElasticsearch-inquiete
featured_image: https://cdn.ttgtmedia.com/visuals/German/article/invisible-face-questionmark-adobe.jpg
tags:
- Économie
- Licenses
- Entreprise
series:
- 202107
series_weight: 0
---

> Une confrontation très médiatisée entre Elastic, éditeur de logiciels, et AWS, géant du cloud computing, inquiète certains observateurs de l’industrie quant aux implications plus larges pour le financement des logiciels libres.
