---
site: L'OBS
title: "Selon l'assurance-maladie, les conditions «ne sont pas réunies» pour que Microsoft gère les données de santé"
date: 2021-02-19
href: https://www.nouvelobs.com/sante/20210219.OBS40425/selon-l-assurance-maladie-les-conditions-ne-sont-pas-reunies-pour-que-microsoft-gere-les-donnees-de-sante.html
featured_image: https://focus.nouvelobs.com/2021/02/19/237/0/5472/2736/633/306/75/0/aac32ef_662637445-sipa-ap22536109-000003.jpg
tags:
- Vie privée
- Entreprise
- Informatique en nuage
series:
- 202107
---

> «Seul un dispositif souverain et uniquement soumis au RGPD permettra de gagner la confiance des assurés», assure l'assurance-maladie.
