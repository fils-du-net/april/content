---
site: l'Humanité.fr
title: "Huit pistes pour mettre au pas les Gafam (€)"
date: 2021-02-06
author: Pierric Marissal
href: https://www.humanite.fr/huit-pistes-pour-mettre-au-pas-les-gafam-699605
featured_image: https://www.humanite.fr/sites/default/files/styles/1048x350/public/images/gafam_3_hd3mon_web.jpg
tags:
- Sensibilisation
- Interopérabilité
series:
- 202105
---

> Portabilité des données, droit de la concurrence... les propositions ne manquent pas pour donner plus de pouvoirs aux usagers, briser les monopoles et en finir avec les diktats des géants du numériques.
