---
site: Le Grand Continent
title: "Le logiciel libre, outil de soft power et de lutte contre le Covid-19"
author: Grégoire Mialon, Gustave Ronteix
date: 2021-02-24
href: https://legrandcontinent.eu/fr/2021/02/24/le-logiciel-libre-outil-de-soft-power-lutte-contre-le-covid-19
tags:
- Sciences
series:
- 202108
---

> La recherche médicale a tourné à plein régime pendant la crise pandémique mondiale. Mais comment au juste se passe la recherche dans le domaine de la santé aujourd'hui, notamment avec l'importance qu'a pris l'intelligence artificielle dans le secteur?
