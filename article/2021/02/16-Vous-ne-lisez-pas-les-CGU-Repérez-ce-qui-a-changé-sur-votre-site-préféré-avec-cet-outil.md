---
site: Numerama
title: "Vous ne lisez pas les CGU? Repérez ce qui a changé sur votre site préféré avec cet outil"
author: Julien Lausson
date: 2021-02-16
href: https://www.numerama.com/tech/689120-vous-ne-lisez-pas-les-cgu-reperez-ce-qui-a-change-sur-votre-site-prefere-avec-cet-outil.html
featured_image: https://c2.lestechnophiles.com/www.numerama.com/wp-content/uploads/2021/02/cgu.jpg?fit=1024,1024
tags:
- Internet
- Vie privée
- Institutions
series:
- 202107
series_weight: 0
---

> Les services de l'État lancent Scripta Manent, une plateforme qui passe en revue les conditions d'utilisation (CGU) des principaux services en ligne afin de souligner ce qui a changé au fil du temps.
