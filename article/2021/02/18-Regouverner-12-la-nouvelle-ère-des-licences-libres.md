---
site: internet ACTU.net
title: "Regouverner (1/2): la nouvelle ère des licences libres"
author: Hubert Guillaud
date: 2021-02-18
href: https://www.internetactu.net/2021/02/17/regouverner-1-2-la-nouvelle-ere-des-licences-libres
featured_image: https://www.internetactu.net/wp-content/uploads/2021/02/Contributor-Covenant.png
tags:
- Licenses
- Économie
series:
- 202107
series_weight: 0
---

> En 1972, l’avocate et politologue féministe Jo Freeman (Wikipedia) publiait un article sur «la tyrannie de l’absence de structure» (voir sa traduction en français) où elle critiquait les formes d’organisation ouvertes, sans chefs. Elle remarquait que ces structures implicites rendaient plus difficiles de reconnaître, contester où éliminer les rapports de domination qui y prospéraient…
