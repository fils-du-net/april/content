---
site: Numerama
title: "L'Assurance Maladie critique le rôle de Microsoft dans l'hébergement des données de santé"
author: Julien Lausson
date: 2021-02-22
href: https://www.numerama.com/sciences/690510-lassurance-maladie-critique-le-role-de-microsoft-dans-lhebergement-des-donnees-de-sante.html
featured_image: https://www.numerama.com/wp-content/uploads/2021/02/assurance-maladie.jpg
tags:
- Vie privée
- Entreprise
- Informatique en nuage
series:
- 202108
series_weight: 0
---

> Le conseil d'administration de l'Assurance Maladie exprime aussi des réserves sur le rôle que joue Microsoft dans le Health Data Hub. Et suggère qu'il faudrait plutôt une entreprise européenne ou française pour héberger un service aussi sensible.
