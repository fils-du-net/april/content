---
site: Acteurs Publics 
title: "Pourquoi l'identité numérique n'est pas près d'ouvrir la voie au vote par Internet (€)"
author: Emile Marzolf
date: 2021-02-22
href: https://acteurspublics.fr/articles/pourquoi-lidentite-numerique-nest-pas-prete-douvrir-la-voie-au-vote-par-internet
tags:
- Vote électronique
series:
- 202108
---

> Avec l’évolution des technologies et l’arrivée de l’identité numérique sécurisée, le vote en ligne, qui ne fait qu’exacerber les limites du vote électronique, peut-il devenir une réalité? Retour sur un (déjà vieux) serpent de mer, qui n’en finit pas de resurgir au rythme de l’actualité et à la faveur de la pandémie.
