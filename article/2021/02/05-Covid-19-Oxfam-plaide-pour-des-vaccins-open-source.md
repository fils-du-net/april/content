---
site: EconomieMatin
title: "Covid-19: Oxfam plaide pour des vaccins «open source»"
date: 2021-02-05
href: http://www.economiematin.fr/news-vaccins-covid19-propriete-intellectulle-inegalites-production-oxfam
featured_image: http://www.economiematin.fr/img/vaccins-covid19-pays-riches-pauvres-inegalites.jpg
tags:
- Sciences
series:
- 202105
series_weight: 0
---

> Oxfam appelle l’industrie pharmaceutique et les pouvoirs publics à lever les obstacles afin que des vaccins contre le Covid-19 soient produits en masse par autant d'acteurs que possible, notamment dans des pays en développement.
