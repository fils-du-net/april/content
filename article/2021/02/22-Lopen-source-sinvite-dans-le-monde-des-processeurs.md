---
site: ITforBusiness
title: "L'open source s'invite dans le monde des processeurs"
author: David Feugey
date: 2021-02-22
href: https://www.itforbusiness.fr/lopen-source-sinvite-dans-le-monde-des-processeurs-42542
featured_image: https://www.itforbusiness.fr/wp-content/uploads/2021/02/shutterstock_1368987602.jpg
tags:
- Matériel libre
- Innovation
series:
- 202108
series_weight: 0
---

> Pourquoi ne pas appliquer la recette des logiciels libres au matériel et aux processeurs? Cette idée, certains l’ont eu dès le début des années 2000. Il aura toutefois fallu attendre 2020 pour assister au véritable décollage des processeurs libres, sous l’impulsion du mouvement RISC-V.
