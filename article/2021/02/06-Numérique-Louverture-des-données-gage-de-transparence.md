---
site: ouest-france.fr
title: "Numérique. L’ouverture des données, gage de transparence (€)"
author: Renée-Laure Euzen
date: 2021-02-06
href: https://www.ouest-france.fr/bretagne/lannion-22300/numerique-l-ouverture-des-donnees-gage-de-transparence-7145618
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMTAyNDFlZjRiZmNjOWNhZjhiNTQ2YTdkMGUyNDQ4OTY5NjE?width=1260&focuspoint=51%2C17&cropresize=1&client_id=bpeditorial&sign=9bbc7e83b279a6988d72215f004965f35b3bb85ebeb63df350120d265ba2eafd
tags:
- Open data
series:
- 202105
---

> Éric Bothorel, député LREM de la circonscription Lannion-Paimpol (Côtes-d’Armor), a remis son rapport sur l’ouverture des données au Premier ministre, fin 2020. Cette question, qui peut paraître technique, est un enjeu fort sur le plan politique, mais aussi économique.
