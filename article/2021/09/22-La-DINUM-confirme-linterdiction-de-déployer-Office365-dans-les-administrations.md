---
site: cio-online.com
title: "La DINUM confirme l'interdiction de déployer Office365 dans les administrations"
author: Bertrand Lemaire
date: 2021-09-22
href: https://www.cio-online.com/actualites/lire-la-dinum-confirme-l-interdiction-de-deployer-office365-dans-les-administrations-13537.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000017617.jpg
tags:
- Administration
- Informatique en nuage
series:
- 202138
series_weight: 0
---

> Le 15 septembre, Nadi Bou Hanna a envoyé une note aux secrétaires généraux des différents ministères pour rappeler la non-conformité d'Office365.
