---
site: Siècle Digital
title: "Le code source de FranceConnect va s'ouvrir"
author: Julia Guinamard
date: 2021-09-13
href: https://siecledigital.fr/2021/09/13/code-source-franceconnect-ouvrir
featured_image: https://siecledigital.fr/wp-content/uploads/2021/09/Franceconnect-940x550.png
tags:
- Administration
series:
- 202137
series_weight: 0
---

> Dès novembre 2021, le code source de FranceConnect sera accessible aux internautes, a annoncé la ministre Amélie de Montchalin.
