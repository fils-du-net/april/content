---
site: Numerama
title: "Pourquoi les administrations ont-elle tant de mal à passer au logiciel libre à grande échelle?"
author: Moran Kerinec
date: 2021-09-26
href: https://www.numerama.com/tech/742425-pourquoi-les-administrations-ont-elle-tant-de-mal-a-passer-au-logiciel-libre-a-grande-echelle.html
featured_image: https://www.numerama.com/wp-content/uploads/2021/09/arles.jpg
tags:
- Logiciels privateurs
- Administration
series:
- 202138
series_weight: 0
---

> Par souci d'économiser les deniers publics, logique pragmatique ou idéologique, certaines collectivités françaises basculent leur parc informatique vers le logiciel libre. Une démarche semée d'embûches techniques et pratiques.
