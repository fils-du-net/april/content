---
site: Geek Junior
title: "Je débute sur PC: c'est quoi Linux?"
author: Solène Kutzner
date: 2021-09-17
href: https://www.geekjunior.fr/je-debute-sur-pc-cest-quoi-linux-44884
featured_image: https://www.geekjunior.fr/wp-content/uploads/2021/09/Linux.jpg
tags:
- Sensibilisation
series:
- 202137
series_weight: 0
---

> Dans ce tuto, tu vas en apprendre plus sur cette famille de systèmes d'exploitation et pourquoi il est intéressant d'utiliser Linux.
