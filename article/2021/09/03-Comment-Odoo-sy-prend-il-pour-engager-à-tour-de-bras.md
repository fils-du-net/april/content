---
site: L'Echo
title: "Comment Odoo s'y prend-il pour engager à tour de bras?"
author: Laurent Fabri
date: 2021-09-03
href: https://www.lecho.be/entreprises/technologie/comment-odoo-s-y-prend-il-pour-engager-a-tour-de-bras/10330005.html
featured_image: https://images.lecho.be/view?iid=Elvis:FR5yxZmsq0XBoS78yICt-V&context=ONLINE&ratio=16/9&width=1280&u=1630835890000
tags:
- Entreprise
series:
- 202135
series_weight: 0
---

> La licorne wallonne recrute à tour de bras. Plus de la moitié du personnel a moins d'un an de maison. Un joyeux désordre qui cache une gestion des ressources humaines éprouvée.
