---
site: Silicon
title: "Open Source: un puissant moteur pour l'économie européenne"
author: Ariane Beky
date: 2021-09-07
href: https://www.silicon.fr/open-source-moteur-economie-europeenne-415795.html
featured_image: https://www.silicon.fr/wp-content/uploads/2020/02/open-source_photo-via-visualhunt.jpg
tags:
- Économie
series:
- 202136
series_weight: 0
---

> Une hausse de 10% des contributions aux logiciels libres et open source dans l'UE apporterait annuellement un gain de PIB de 0,4% à 0,6%.
