---
site: GreenIT.fr
title: "L'open source au service du numérique durable"
author: Thomas Lemaire
date: 2021-09-28
href: https://www.greenit.fr/2021/09/28/lopen-source-au-service-du-numerique-durable
featured_image: https://i0.wp.com/www.greenit.fr/wp-content/uploads/2014/02/code-informatique-ecran.jpg?resize=270%2C270&ssl=1
tags:
- Économie
- Europe
- Sensibilisation
series:
- 202139
---

> Quels sont les apports des logiciels libres et open source au numérique responsable? analyse sur les 3 piliers du développement durable.
