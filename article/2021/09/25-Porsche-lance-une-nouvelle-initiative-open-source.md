---
site: 4Legend.com
title: "Porsche lance une nouvelle initiative open source"
date: 2021-09-25
href: https://www.4legend.com/2021/porsche-lance-une-nouvelle-initiative-open-source
featured_image: https://www.4legend.com/wp-content/uploads/2021/09/big-porsche-logiciel-open-source.jpg
tags:
- Sensibilisation
- Entreprise
series:
- 202138
series_weight: 0
---

> Porsche renforce sa présence et son expertise dans le monde du logiciel open source via différentes initiatives et participations.
