---
site: ZDNet France
title: "La fintech du patron de Twitter, Square, plonge dans le grand bain de l'open source"
author: Steven J. Vaughan-Nichols
date: 2021-09-16
href: https://www.zdnet.fr/actualites/la-fintech-du-patron-de-twitter-square-plonge-dans-le-grand-bain-de-l-open-source-39929293.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/Pierre%20temp/Jack%20Dorsey%20B__w1200.jpg
tags:
- Brevets logiciels
- Entreprise
series:
- 202137
series_weight: 0
---

> La fintech Square reconnaît la dépendance de ses technologies financières à l'égard des logiciels libres en rejoignant l'Open Invention Network.
