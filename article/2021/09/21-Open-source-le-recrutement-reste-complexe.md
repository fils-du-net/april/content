---
site: ZDNet France
title: "Open source: le recrutement reste complexe (€)"
author: Steven J. Vaughan-Nichols
date: 2021-09-21
href: https://www.zdnet.fr/actualites/open-source-le-recrutement-reste-complexe-39929579.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/02/open-source-code-620__w1200.jpg
tags:
- Entreprise
series:
- 202138
---

> Les entreprises étaient depuis longtemps à la recherche d'employés compétents en matière de logiciels libres, mais aujourd'hui, la tâche se complexifie encore.
