---
site: Clubic.com
title: "Google s'associe à l'OSTIF pour améliorer la sécurité des projets open source tels que Git"
author: Fanny Dufour
date: 2021-09-16
href: https://www.clubic.com/pro/entreprises/google/actualite-384814-google-s-associe-a-l-ostif-pour-ameliorer-la-securite-des-projets-open-source-tels-que-git.html
featured_image: https://pic.clubic.com/v1/images/1772538/raw.webp?fit=max&width=1200&hash=f02eafc80536e0a7637ca2d780ce857b37d009dd
tags:
- Innovation
series:
- 202137
series_weight: 0
---

> Google et l'OSTIF ont annoncé un partenariat pour améliorer la sécurité de huit projets open source.
