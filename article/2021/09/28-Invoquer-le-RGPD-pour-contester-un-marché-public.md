---
site: L'Echo
title: "Invoquer le RGPD pour contester un marché public (€)"
date: 2021-09-28
href: https://www.lecho.be/economie-politique/belgique/general/invoquer-le-rgpd-pour-contester-un-marche-public/10335348.html
tags:
- Marché public
series:
- 202139
---

> Une entreprise évincée d'un marché public a invoqué le RGPD pour le contester, estimant que le transfert de données privées vers les États-Unis violait la législation européenne.
