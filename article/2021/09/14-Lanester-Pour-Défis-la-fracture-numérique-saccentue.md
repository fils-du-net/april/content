---
site: ouest-france.fr
title: "Lanester. Pour Défis la fracture numérique s’accentue (€)"
date: 2021-09-14
href: https://www.ouest-france.fr/bretagne/lanester-56600/pour-defis-la-fracture-numerique-saccentue-591b5c99-3421-40e9-91b8-9fbeac884938
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMTA5NTZmNWU4ZGY2YjUxYzU4YTUyMDJmMjI1MjE5MmJhZDE?width=1260&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=2fa7a3e86d60acedc7fff4a5d26b1f64acea13edcd553b730edf27f89b264c7f
tags:
- Associations
series:
- 202137
---

> Lanester — L’association Défis continue sa mission afin de lutter contre la fracture numérique. Elle reconditionne le matériel et propose accompagnement et formations aux usagers.
