---
site: ChannelNews
title: "Les talents open source toujours aussi rares"
author: Lucie Robet
date: 2021-09-22
href: https://www.channelnews.fr/les-talents-open-source-toujours-aussi-rares-105795
featured_image: https://www.channelnews.fr/wp-content/uploads/2021/09/Capture-decran-2021-09-21-a-16.28.05.jpeg
tags:
- Entreprise
series:
- 202138
series_weight: 0
---

> La demande de talents en matière de logiciels libres est plus forte que jamais. Lors du sommet Open Source de Seattle, la Fondation Linux et l’éditeur de MOOC edX ont publié une étude intitulée 2021 Open Source Jobs Report. Menée auprès de 200 responsables du recrutement et de 750 professionnel.le.s, l’enquête constate que 92% des personnes interrogées ont des difficultés à recruter les profils expérimentés et les garder dans l’entreprise.
