---
site: Next INpact
title: "Arcom: le Parlement adopte la fusion Hadopi-CSA et les nouveaux outils contre le piratage (€)"
description: Hadopi Rip Lol
author: Marc Rees
date: 2021-09-29
href: https://www.nextinpact.com/article/48252/arcom-parlement-adopte-fusion-hadopi-csa-et-nouveaux-outils-contre-piratage
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/10006.jpg
tags:
- Internet
- HADOPI
series:
- 202139
series_weight: 0
---

> Après les sénateurs, les députés ont adopté définitivement le projet de loi «relatif à la régulation et à la protection de l’accès aux œuvres culturelles à l’ère numérique». C’est la mort annoncée de la Hadopi, mais pas de ses compétences, transférées à l’Arcom, nouveau nom du CSA. Des outils de lutte contre le piratage en ligne sont introduits pour l’occasion.
