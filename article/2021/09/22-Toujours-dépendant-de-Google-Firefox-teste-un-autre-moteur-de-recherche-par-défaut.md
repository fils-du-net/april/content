---
site: Numerama
title: "Toujours dépendant de Google, Firefox teste un autre moteur de recherche par défaut"
author: Julien Lausson
date: 2021-09-22
href: https://www.numerama.com/tech/741390-toujours-dependant-de-google-firefox-teste-un-autre-moteur-de-recherche-par-defaut.html
featured_image: https://www.numerama.com/wp-content/uploads/2021/09/panda-roux-firefox.jpg
tags:
- Internet
series:
- 202138
series_weight: 0
---

> Alors qu'un accord avec Google est en cours jusqu'en 2023, la fondation Mozilla regarde si elle peut aller voir ailleurs. C'est ce que suggère un test dans Firefox qui concerne le choix du moteur de recherche par défaut.
