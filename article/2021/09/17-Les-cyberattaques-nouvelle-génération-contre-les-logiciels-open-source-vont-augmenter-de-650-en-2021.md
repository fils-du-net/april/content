---
site: Siècle Digital
title: "Les cyberattaques «nouvelle génération» contre les logiciels open source vont augmenter de 650% en 2021"
author: Valentin Cimino
date: 2021-09-17
href: https://siecledigital.fr/2021/09/17/cyberattaques-logiciels-650-2021
featured_image: https://siecledigital.fr/wp-content/uploads/2021/09/RANSOMWARE_CYBER-scaled-1-940x550.jpg
tags:
- Innovation
series:
- 202137
---

> Les cyberattaques «nouvelle génération» contre les logiciels open source vont augmenter de 650% au cours de l'année 2021.
