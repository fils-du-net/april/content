---
site: ZDNet France
title: "Les 8 projets open source financés à hauteur de 100 millions de dollars par Google (€)"
author: Liam Tung
date: 2021-09-17
href: https://www.zdnet.fr/actualites/les-8-projets-open-source-finances-a-hauteur-de-100-millions-de-dollars-par-google-39929399.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/02/open-source-code-620__w1200.jpg
tags:
- Innovation
series:
- 202137
---

> Google finance l'audit de sécurité de huit projets clés de logiciels libres. Mais 17 autres ont besoin urgemment d'un soutien financier.
