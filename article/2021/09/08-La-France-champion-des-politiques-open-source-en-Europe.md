---
site: Journal du Net
title: "La France, champion des politiques open source en Europe (€)"
author: Antoine Crochet-Damais
date: 2021-09-08
href: https://www.journaldunet.com/solutions/dsi/1505173-la-france-champion-des-politiques-open-source-en-europe
featured_image: https://img-0.journaldunet.com/zfN65guxDkX4xeDm7nKxnt-sIlc=/1500x/smart/d2d8f1a7c2884ecf8e9bc528ac91cf0d/ccmcms-jdn/27735161.jpg
tags:
- Économie
series:
- 202136
---

> La France, champion des politiques open source en Europe Un rapport de la Commission européenne classe l'Hexagone en tête des états les plus actifs dans les logiciels libres sur le Vieux Continent. Au niveau mondial, le pays tire aussi son épingle du jeu.
