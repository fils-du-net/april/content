---
site: ZDNet France
title: "Infographie: l'open source en France, un marché dynamique"
author: Thierry Noisette
date: 2021-09-29
href: https://www.zdnet.fr/blogs/l-esprit-libre/infographie-l-open-source-en-france-un-marche-dynamique-39930051.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/09/open-source%20panneau_picpedia.jpg
tags:
- Entreprise
series:
- 202139
series_weight: 0
---

> La filière des entreprises de l'open source en France est constituée «d'entreprises matures et résilientes», «ayant de belles perspectives de croissance», commente le CNLL.
