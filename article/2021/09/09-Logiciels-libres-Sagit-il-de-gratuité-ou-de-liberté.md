---
site: ZDNet France
title: "Logiciels libres: S'agit-il de gratuité ou de liberté?"
author: Daphne Leprince-Ringuet
date: 2021-09-09
href: https://www.zdnet.fr/actualites/logiciels-libres-s-agit-il-de-gratuite-ou-de-liberte-39928879.htm
featured_image: https://www.zdnet.com/a/hub/i/r/2021/09/01/81581e68-6971-43e6-ac99-c14c2817ca29/resize/1200xauto/bd6e3d95a9fcf2c7c0c9b9ecfecf8c39/shutterstock-1500204824.jpg
tags:
- Économie
series:
- 202136
---

> Les développeurs de logiciels libres pourraient changer l'équilibre du pouvoir dans le secteur des technologies, s'ils veulent saisir l'occasion, mentionne un rapport de l'Union européenne.
