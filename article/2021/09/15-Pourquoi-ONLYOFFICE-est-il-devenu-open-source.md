---
site: InformatiqueNews.fr
title: "Pourquoi ONLYOFFICE est-il devenu open source?"
date: 2021-09-15
href: https://www.informatiquenews.fr/pourquoi-onlyoffice-est-il-devenu-open-source-81251
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2021/09/lev-bannov.png
tags:
- Licenses
series:
- 202137
---

> Si nous parlons d’affaires, être open-source est un excellent moyen de gagner la confiance des gens, c’était exactement ce qu’OnlyOffice voulait faire. De nombreuses entreprises sont préoccupées par la confidentialité de leurs données. Donc, un code source ouvert peut être important pour elles.
