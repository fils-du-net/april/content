---
site: Le Monde Informatique
title: "Les grands acteurs IT ont l'open source intéressé"
author: Matt Asay
date: 2021-09-11
href: https://www.lemondeinformatique.fr/actualites/lire-les-grands-acteurs-it-ont-l-open-source-interesse-84090.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000080819.jpg
tags:
- Entreprise
- Économie
series:
- 202136
series_weight: 0
---

> Ce n'est pas par altruisme que les grands fournisseurs de logiciels soutiennent l'open source. Ceux qui le font en attendent un retour sur investissement.
