---
site: La Tribune
title: "Comment Microsoft est redevenu cool"
author: Guillaume Renouard
date: 2021-09-02
href: https://www.latribune.fr/technos-medias/comment-microsoft-est-redevenu-cool-891447.html
featured_image: https://static.latribune.fr/full_width/333457/satya-nadella.jpg
tags:
- Informatique en nuage
- Entreprise
- Logiciels privateurs
series:
- 202135
---

> Depuis sa prise de poste en 2014, l’actuel directeur-général de Microsoft, Satya Nadella, a su dépoussiérer l’entreprise en donnant la priorité au cloud, en promouvant l’ouverture et le logiciel libre, et en modernisant les méthodes de management.
