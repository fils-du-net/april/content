---
site: InformatiqueNews.fr
title: "Quel est l'impact de l'open source en Europe?"
author: Loïc Duval
date: 2021-09-16
href: https://www.informatiquenews.fr/quel-est-limpact-de-lopen-source-en-europe-81286
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2021/09/shutterstock_334982726.jpg
tags:
- Économie
- Europe
series:
- 202137
series_weight: 0
---

> Un nouveau rapport sur l’impact de l’open source met en avant les bénéfices de ces technologies sur la souveraineté numérique européenne et la compétitivité des organisations européennes tout en soulignant le manque d’investissements dans ces technologies.
