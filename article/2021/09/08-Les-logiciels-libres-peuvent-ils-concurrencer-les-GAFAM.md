---
site: ouest-france.fr
title: "Les logiciels libres peuvent-ils concurrencer les GAFAM?"
date: 2021-09-08
href: https://ledrenche.ouest-france.fr/logiciels-libres-peuvent-ils-concurrencer-les-gafam
featured_image: https://ledrenche.ouest-france.fr/wp-content/uploads/Illustration_logiciellibre-1030x438.png
tags:
- Sensibilisation
- Informatique en nuage
series:
- 202136
series_weight: 0
---

> Récemment mis sur le devant de la scène politique française par Jean Castex, les logiciels libres peuvent-ils concurrencer les GAFAM?
