---
site: Acteurs Publics 
title: "L'État freine le “tout-Microsoft” dans ses administrations (€)"
author: Emile Marzolf
date: 2021-09-22
href: https://acteurspublics.fr/articles/letat-freine-le-tout-microsoft-dans-ses-administrations
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/36/342d46a31601a96a779ca6672c0b0685eaf36481.jpeg
tags:
- Administration
- Informatique en nuage
series:
- 202138
---

> Dans la ligne de la nouvelle doctrine “cloud au centre” de l’État, qui impose le recours à des solutions sécurisées et protégées des ingérences étrangères, le directeur interministériel du numérique de l’État interdit aux ministères de migrer vers la suite bureautique de Microsoft hébergée dans le cloud, Office 365, mais leur concède des marges de manœuvre.
