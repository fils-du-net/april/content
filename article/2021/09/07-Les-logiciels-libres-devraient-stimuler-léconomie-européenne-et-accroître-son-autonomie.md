---
site: ZDNet France
title: "Les logiciels libres devraient stimuler l'économie européenne et accroître son autonomie"
author: Thierry Noisette
date: 2021-09-07
href: https://www.zdnet.fr/blogs/l-esprit-libre/les-logiciels-libres-devraient-stimuler-l-economie-europeenne-et-accro-tre-son-autonomie-39928713.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/09/euro-force_Pixabay.jpg
tags:
- Économie
series:
- 202136
---

> Une augmentation de 10% des contributions aux logiciels libres créerait annuellement 0,4% à 0,6% de PIB dans l'UE, affirme une étude pour la Commission européenne. Elle préconise plusieurs mesures pour accélérer le recours au Libre.
