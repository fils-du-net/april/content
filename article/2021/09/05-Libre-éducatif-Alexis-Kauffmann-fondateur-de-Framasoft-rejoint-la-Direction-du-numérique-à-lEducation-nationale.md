---
site: ZDNet France
title: "Libre éducatif: Alexis Kauffmann, fondateur de Framasoft, rejoint la Direction du numérique à l'Education nationale"
author: Thierry Noisette
date: 2021-09-05
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-educatif-alexis-kauffmann-fondateur-de-framasoft-rejoint-la-direction-du-numerique-a-l-education-nationale-39928555.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/09/Alexis_Kauffmann_-_Lycee_francais_de_Taipei_-_2017.jpg
tags:
- Éducation
- Associations
series:
- 202135
series_weight: 0
---

> Professeur de mathématiques à l'origine de la très active association libriste Framasoft, Alexis Kauffmann devient 'chef de projet logiciels et ressources éducatives libres et mixité dans les filières du numérique' à la Direction du numérique de l'Education nationale.
