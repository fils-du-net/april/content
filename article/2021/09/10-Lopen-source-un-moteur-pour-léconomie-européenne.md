---
site: Le Monde Informatique
title: "L'open source, un moteur pour l'économie européenne"
author: Dominique Filippone
date: 2021-09-10
href: https://www.lemondeinformatique.fr/actualites/lire-l-open-source-un-moteur-pour-l-economie-europeenne-84116.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000080853.jpg
tags:
- Économie
series:
- 202136
---

> Dans un dernier rapport, la Commission européenne analyse les impacts des logiciels et des matériels open source sur la compétitivité, l'indépendance technologique et l'innovation en Europe. Près d'un milliard d'euros ont été investi dans des logiciels et matériels open source en 2018.
