---
site: Silicon
title: "L'Open Source Initiative nomme Stefano Maffulli directeur exécutif"
author: Ariane Beky
date: 2021-09-15
href: https://www.silicon.fr/open-source-initiative-stefano-maffulli-directeur-executif-416478.html
featured_image: https://www.silicon.fr/wp-content/uploads/2021/09/Stefano-Maffulli.jpg
tags:
- Licenses
- Associations
series:
- 202137
series_weight: 0
---

> Animateur et contributeur de l'écosystème, Stefano Maffulli devient le premier directeur exécutif de l'OSI, qui supervise les licences open source.
