---
site: Numerama
title: "La France va ouvrir le code source de FranceConnect, qui sert à se connecter aux services publics"
author: Julien Lausson
date: 2021-09-09
href: https://www.numerama.com/tech/737966-la-france-va-ouvrir-le-code-source-de-franceconnect-qui-sert-a-se-connecter-aux-services-publics.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2021/03/franceconnect.jpg?fit=1024,1024
tags:
- Administration
series:
- 202136
series_weight: 0
---

> Le  gouvernement ouvrira dans quelques semaines le code de FranceConnect, le dispositif qui permet de se connecter à des centaines de services publics grâce à des codes uniques.
