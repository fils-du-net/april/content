---
site: Journal du Net
title: "Ce qu'une décennie de travail hybride nous a appris sur l'avenir"
author: Lara Owen
date: 2021-09-08
href: https://www.journaldunet.com/management/ressources-humaines/1505101-ce-qu-une-decennie-de-travail-hybride-nous-a-appris-sur-l-avenir
tags:
- Innovation
series:
- 202136
series_weight: 0
---

> Les entreprises constatent aujourd'hui que leurs lieux d'implantation ne doivent pas nécessairement imposer des limites à ce que les gens peuvent réaliser. La flexibilité est notamment, et depuis longtemps, l'épine dorsale de la communauté open source. Du point de vue des logiciels, un travail efficace, "asynchrone", online et hybride, peut et va permettre la communication et la collaboration nécessaires à la création de logiciels qui - et ce n'est pas une hyperbole - peuvent réellement changer le monde.
