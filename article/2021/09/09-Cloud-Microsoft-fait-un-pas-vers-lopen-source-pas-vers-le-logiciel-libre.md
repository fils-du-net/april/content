---
site: Siècle Digital
title: "Cloud: Microsoft fait un pas vers l'open source… pas vers le logiciel libre"
author: Julia Guinamard
date: 2021-09-09
href: https://siecledigital.fr/2021/09/09/microsoft-openinfra
featured_image: https://siecledigital.fr/wp-content/uploads/2021/09/microsoft-open-source-940x550.jpeg
tags:
- Entreprise
series:
- 202136
---

> Microsoft rejoint la fondation OpenInfra pour soutenir l'open source, une notion très différente de celle du logiciel libre.
