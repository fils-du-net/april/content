---
site: The Conversation
title: "Souveraineté et numérique: maîtriser notre destin"
author: Annie Blandin-Obernesser
date: 2021-11-10
href: https://theconversation.com/souverainete-et-numerique-maitriser-notre-destin-171014
featured_image: https://images.theconversation.com/files/431063/original/file-20211109-27-ibogit.jpg?ixlib=rb-1.1.0&rect=35%2C449%2C4000%2C2197&q=45&auto=format&w=926&fit=clip
tags:
- Informatique en nuage
- Internet
- Vie privée
- Europe
series:
- 202145
series_weight: 0
---

> Comment se construit concrètement la souveraineté numérique, alors qu'elle semble menacée par les stratégies et ambitions d'entreprises étrangères et de certains États?
