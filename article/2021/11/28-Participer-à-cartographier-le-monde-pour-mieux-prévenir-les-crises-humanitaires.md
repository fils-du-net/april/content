---
site: RFI
title: "Participer à cartographier le monde pour mieux prévenir les crises humanitaires"
author: Aurore Lartigue
date: 2021-11-28
href: https://www.rfi.fr/fr/technologies/20211128-participer-%C3%A0-cartographier-le-monde-pour-mieux-pr%C3%A9venir-les-crises-humanitaires
featured_image: https://s.rfi.fr/media/display/e1a58a94-4f85-11ec-bfbc-005056a90284/w:1280/p:16x9/MSF175877%28High%29.webp
tags:
- Partage du savoir
series:
- 202148
series_weight: 0
---

> Cartographier les zones de la planète les plus vulnérables pour faciliter l’intervention humanitaire en cas de catastrophe. C’est la mission que s’est fixée depuis des années le projet Missing Maps. Le tout grâce à la carte participative OpenStreetMap et à une armée de contributeurs bénévoles.
