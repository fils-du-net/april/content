---
site: ZDNet France
title: "Libre et open source express: Europe, radio April, Vecam, agriculteurs, concours Acteurs du Libre"
author: Thierry Noisette
date: 2021-11-30
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-europe-radio-april-vecam-agriculteurs-concours-acteurs-du-libre-39933379.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/05/open-source-KeepCalmAndUse.jpg
tags:
- april
- Promotion
series:
- 202148
series_weight: 0
---

> Bouquet de brèves: tribune pour des politiques industrielles européennes. L'émission de l'April a son propre site. L'association Vecam s'arrête. Ekylibre conclut un partenariat pour ses logiciels de gestion agricole. Les lauréats de l'édition 2021 du concours Acteurs du Libre.
