---
site: RFI
title: "Face à l'immensité du Big Data, les stratégies des journalistes d'investigation"
author: Léopold Picot
date: 2021-11-07
href: https://www.rfi.fr/fr/technologies/20211107-face-%C3%A0-l-immensit%C3%A9-du-big-data-les-strat%C3%A9gies-des-journalistes-d-investigation
featured_image: https://s.rfi.fr/media/display/7fca2fa8-3e43-11ec-ad22-005056bfb2b6/w:1280/p:16x9/AP21278208956780.webp
tags:
- Partage du savoir
series:
- 202145
series_weight: 0
---

> Quantité de données stratégiques sont disponibles sur le web et noyées dans son immensité. Les fuites de millions de fichiers, comme les Pandora Papers, s’ajoutent à ce puits sans fond. Pour faire face à cet afflux de données exponentiel, les journalistes d’investigation coopèrent aux quatre coins du monde, développent de nouvelles stratégies et s’appuient sur des outils informatiques puissants.
