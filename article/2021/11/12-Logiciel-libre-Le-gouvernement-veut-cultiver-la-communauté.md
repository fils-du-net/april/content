---
site: ZDNet France
title: "Logiciel libre: Le gouvernement veut cultiver la communauté (€)"
author: Louis Adam
date: 2021-11-12
href: https://www.zdnet.fr/actualites/logiciel-libre-le-gouvernement-veut-cultiver-la-communaute-39932273.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/10/OpenSourceSoftware__w1200.jpg
tags:
- Administration
series:
- 202145
---

> Le gouvernement détaille aujourd'hui son plan d'action en soutien à l'adoption du logiciel libre dans les administrations. Le plan vise notamment à mieux répertorier et mettre en avant les logiciels libres portés par les ministères et les administrations.
