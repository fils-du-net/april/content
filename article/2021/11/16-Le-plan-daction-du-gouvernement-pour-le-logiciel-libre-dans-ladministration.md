---
site: LeMagIT
title: "Le plan d'action du gouvernement pour le logiciel libre dans l'administration"
author: François Cointe
date: 2021-11-16
href: https://www.lemagit.fr/dessin/Le-plan-daction-du-gouvernement-pour-le-logiciel-libre-dans-ladministration
featured_image: https://cdn.ttgtmedia.com/rms/LeMagIT%20-%20Cartoons/MagIT1954-catalogueGouvTech.jpg
tags:
- Administration
series:
- 202146
---

> Le gouvernement a exposé son plan d’action pour «accélérer» l’adoption des logiciels libres du partage des données et des algorithmes au sein de l’Administration. La feuille de route entend structurer des initiatives existantes sans imposer leur recours.
