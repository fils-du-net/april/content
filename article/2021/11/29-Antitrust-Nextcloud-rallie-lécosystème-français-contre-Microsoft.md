---
site: Silicon
title: "Antitrust: Nextcloud rallie l'écosystème français contre Microsoft"
author: Clément Bohic
date: 2021-11-29
href: https://www.silicon.fr/antitrust-nextcloud-ecosysteme-francais-microsoft-421150.html
featured_image: https://www.silicon.fr/wp-content/uploads/2021/11/Nextcloud-Microsoft-Commission-européenne-scaled.jpeg
tags:
- Entreprise
series:
- 202148
---

> Une dizaine d'entreprises françaises se liguent à la plainte que Nextcloud a déposée en début d'année contre Microsoft auprès de l'UE.
