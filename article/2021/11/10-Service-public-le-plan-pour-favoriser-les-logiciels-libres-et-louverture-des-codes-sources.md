---
site: Next INpact
title: "Service public: le plan pour favoriser les logiciels libres et l'ouverture des codes sources (€)"
description: "Et si on commençait par Parcoursup?"
author: Sébastien Gavois
date: 2021-11-10
href: https://www.nextinpact.com/article/48776/service-public-plan-pour-favoriser-logiciels-libres-et-louverture-codes-sources
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/1902.jpg
tags:
- Administration
- Open Data
series:
- 202145
---

> Afin de pousser les administrations à passer aux logiciels libres et à libérer le code source de leur application, Amélie de Montchalin présente un plan d’action en trois axes: faire connaitre, accompagner et renforcer l’attractivité. Cela passe notamment par la mise en ligne d’une plateforme dédiée: code.gouv.fr.
