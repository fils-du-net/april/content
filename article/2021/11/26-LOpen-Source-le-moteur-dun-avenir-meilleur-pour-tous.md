---
site: Siècle Digital
title: "L'Open Source: le moteur d'un avenir meilleur pour tous"
author: Bassem Asseh
date: 2021-11-26
href: https://siecledigital.fr/2021/11/26/lopen-source-le-moteur-dun-avenir-meilleur-pour-tous
featured_image: https://siecledigital.fr/wp-content/uploads/2021/11/Tribune-Github-Bassem-Asseh.jpg
tags:
- Innovation
- Internet
series:
- 202147
series_weight: 0
---

> Omniprésent dans nos applications quotidiennes et constituant le socle de notre monde moderne, l'open source réunit l'ensemble de la communauté des développeurs, engagée au service l'excellence numérique. Et si cette pratique, qui favorise une innovation rapide et accessible à tous, était la voie incontournable pour accélérer le progrès humain?
