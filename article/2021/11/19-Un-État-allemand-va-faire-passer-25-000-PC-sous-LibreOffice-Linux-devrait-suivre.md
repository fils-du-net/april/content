---
site: MacGeneration
title: "Un État allemand va faire passer 25 000 PC sous LibreOffice, Linux devrait suivre"
author: Félix Cattafesta
date: 2021-11-19
href: https://www.macg.co/ailleurs/2021/11/un-etat-allemand-va-faire-passer-25-000-pc-sous-libreoffice-linux-devrait-suivre-125485
featured_image: https://cdn.mgig.fr/2021/11/mg-7098e376-w1920.jpg
tags:
- Administration
- Logiciels privateurs
series:
- 202147
series_weight: 0
---

> L'État du Schleswig-Holstein (nord de l'Allemagne) envisage de se débarrasser de Windows et d'Office. La transition va commencer par un passage à LibreOffice dans les écoles et les administrations, mais à terme, le gouvernement ambitionne de remplacer complètement Windows par Linux. Cela serait l'occasion pour cet État de ne plus avoir à utiliser des solutions américaines et donc de regagner en souveraineté, mais aussi de mieux pouvoir sécuriser et protéger ses données.
