---
site: La Tribune
title: "Scaleway quitte Gaia-X: 'Je n'ai plus de temps à perdre avec un projet gangrené de l'intérieur par les Gafam' (Yann Lechelle, CEO)"
author: Sylvain Rolland
date: 2021-11-19
href: https://www.latribune.fr/technos-medias/innovation-et-start-up/scaleway-quitte-gaia-x-je-n-ai-plus-de-temps-a-perdre-avec-un-projet-gangrene-de-l-interieur-par-les-gafam-yann-lechelle-ceo-896783.html
featured_image: https://static.latribune.fr/full_width/1354613/cloud-act-h318-p-7-informatique-donnees-data-droits-gafa.jpg
tags:
- Informatique en nuage
- Entreprise
series:
- 202146
---

> ENTRETIEN. Alors qu'à Milan se déroule la deuxième conférence annuelle de Gaia-X (18-19 novembre), l'un de ses membres fondateurs, le français Scaleway, claque la porte. En cause: l'ouverture aux Américains et aux Chinois de cette initiative visant à fédérer les acteurs du cloud et des données en Europe. Yann Lechelle, CEO de Scaleway, explique à La Tribune pourquoi ce projet est devenu selon lui incompatible avec la nécessité d'une souveraineté numérique européenne dans le domaine du cloud.
