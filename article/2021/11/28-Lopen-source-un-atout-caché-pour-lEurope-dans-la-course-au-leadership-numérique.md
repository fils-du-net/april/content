---
site: La Tribune
title: "L'open source, un atout caché pour l'Europe dans la course au leadership numérique"
author: Stéfane Fermigier (CNLL, France), Peter Ganten (OSBA, Allemagne), Timo Väliharju (COSS, Finlande), Jonas Feist (Open Source Sweden, Suède), Gerardo Lisboa (ESOP, Portugal), Ronny Lam (NLUUG, Pays-Bas)
date: 2021-11-28
href: https://www.latribune.fr/opinions/tribunes/l-open-source-un-atout-cache-pour-l-europe-dans-la-course-au-leadership-numerique-897161.html
featured_image: https://static.latribune.fr/full_width/1815087/code-informatique.jpg
tags:
- Économie
- Europe
series:
- 202147
series_weight: 0
---

> OPINION. L'impact de la filière européenne du logiciel libre sur la croissance du PIB et de l'emploi, et sur la souveraineté numérique, a été récemment mis en lumière par plusieurs études, rapports et propositions politiques de haut niveau. Il devient dès lors impératif de mettre en oeuvre des politiques industrielles délibérées destinées à favoriser et accélérer son développement
