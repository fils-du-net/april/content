---
site: ZDNet France
title: "Bibliothèque d'Alexandrie des logiciels, Software Heritage fête ses cinq ans"
author: Thierry Noisette
date: 2021-11-28
href: https://www.zdnet.fr/blogs/l-esprit-libre/bibliotheque-d-alexandrie-des-logiciels-software-heritage-fete-ses-cinq-ans-39933219.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/11/Software-heritage-logo-title.jpg
tags:
- Partage du savoir
series:
- 202147
series_weight: 0
---

> L'organisme lancé par l'Inria en 2016 archive déjà des milliards de dossiers issus de 160 millions de projets. Il célèbre mardi avec l'Unesco cette étape.
