---
site: ICTjournal
title: "Les entreprises gèrent mal les dépendances open source de leurs applications"
author: Yannick Chavanne
date: 2021-11-04
href: https://www.ictjournal.ch/etudes/2021-11-04/les-entreprises-gerent-mal-les-dependances-open-source-de-leurs-applications
featured_image: https://www.ictjournal.ch/sites/default/files/styles/np8_full/public/media/2021/11/04/jan-canty--KOlStNqb1A-unsplash_web.jpg?itok=2rbAmnVk
tags:
- Entreprise
- Innovation
series:
- 202144
series_weight: 0
---

> La gestion des dépendances de composants open source au cœur d’applications en production est souvent négligée, selon une récente étude. Cette tâche est toutefois cruciale contre les attaques à la supply chain logicielle. Il convient de garantir l'installation des versions de composants non vulnérables.
