---
site: ZDNet France
title: "Blue Mind-Linagora: la Cour de cassation casse un arrêt d'appel"
author: Thierry Noisette
date: 2021-11-14
href: https://www.zdnet.fr/blogs/l-esprit-libre/blue-mind-linagora-la-cour-de-cassation-casse-un-arret-d-appel-39932371.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/06/marteau_justice_pxhere.jpeg
tags:
- Entreprise
- Institutions
series:
- 202146
series_weight: 0
---

> La condamnation des dirigeants de Blue Mind, par un arrêt d'appel de décembre 2020 est annulée: la juridiction suprême de l'ordre judiciaire casse l'arrêt qui était en faveur de Linagora.
