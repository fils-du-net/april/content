---
site: Clubic.com
title: "Pourquoi Mastodon demande la publication du code source du réseau social de Trump?"
author: Fanny Dufour
date: 2021-11-02
href: https://www.clubic.com/pro/blog-forum-reseaux-sociaux/actualite-390811-pourquoi-mastodon-demande-la-publication-du-code-source-du-reseau-social-de-trump.html
featured_image: https://pic.clubic.com/v1/images/1706418/raw.webp?fit=max&width=1200&hash=d5ea4e79d8dfdadeaf2eba4e307a6cce9a67a4e2
tags:
- Licenses
series:
- 202144
series_weight: 0
---

> Les avocats de Mastodon ont officiellement demandé à Truth Social, le réseau social de Donald Trump, de publier son code source.
