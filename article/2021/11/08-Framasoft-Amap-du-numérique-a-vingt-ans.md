---
site: ZDNet France
title: "Framasoft, «Amap du numérique», a vingt ans"
author: Thierry Noisette
date: 2021-11-08
href: https://www.zdnet.fr/blogs/l-esprit-libre/framasoft-amap-du-numerique-a-vingt-ans-39932127.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/11/Framasoft_Logo.jpg
tags:
- Associations
- Internet
- april
series:
- 202145
series_weight: 0
---

> Association «d'éducation populaire» dédiée aux logiciels libres, Framasoft a été créée en ligne le 9 novembre 2001. D'abord annuaire de logiciels, elle a bien grandi.
