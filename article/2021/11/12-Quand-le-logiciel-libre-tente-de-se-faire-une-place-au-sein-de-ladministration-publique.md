---
site: Les Numeriques
title: "Quand le logiciel libre tente de se faire une place au sein de l'administration publique"
author: Bastien Lion
date: 2021-11-12
href: https://www.lesnumeriques.com/vie-du-net/quand-le-logiciel-libre-tente-de-se-faire-une-place-au-sein-de-l-administration-publique-n171167.html
featured_image: https://dyw7ncnq1en5l.cloudfront.net/optim/news/17/171167/4aa9e9c7-quand-le-logiciel-libre-tente-de-se-faire-une-place-au-sein-de-l-administration-publique__w800.webp
tags:
- Administration
series:
- 202145
---

> Le logiciel libre peut-il se faire une place au soleil dans les administrations françaises? C'est, jusqu'à preuve du contraire, le sens indiqué par les annonces d'Amélie de Montchalin, ministre de la Transformation et de la Fonction publiques.
