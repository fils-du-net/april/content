---
site: ZDNet France
title: "Antitrust: Des entreprises européennes portent plainte contre Microsoft"
author: Steven J. Vaughan-Nichols
date: 2021-11-29
href: https://www.zdnet.fr/actualites/antitrust-des-entreprises-portent-plainte-contre-microsoft-39933227.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2021/11/OneDrive__w1200.jpg
tags:
- Entreprise
- Institutions
- Europe
- Informatique en nuage
- Interopérabilité
series:
- 202148
series_weight: 0
---

> Nextcloud et près de 30 autres entreprises européennes ont déposé une plainte concernant le comportement anticoncurrentiel de Microsoft avec son offre de stockage cloud OneDrive.
