---
site: Siècle Digital
title: "Le gouvernement souhaite accélérer le déploiement de l'open source"
author: Zacharie Tazrout
date: 2021-11-15
href: https://siecledigital.fr/2021/11/15/le-gouvernement-souhaite-accelerer-le-deploiement-de-lopen-source
featured_image: https://siecledigital.fr/wp-content/uploads/2021/11/47868795401_6eed8aebad_k-940x550.jpg
tags:
- Administration
series:
- 202146
---

> Amélie de Montchalin, ministre de la transformation et de la Fonction publique, a dévoilé son plan d'action sur les logiciels libres.
