---
site: LeMagIT
title: "Logiciels libres dans l'Administration: le gouvernement a son plan d'action (€)"
author: Gaétan Raoul
date: 2021-11-12
href: https://www.lemagit.fr/actualites/252509438/Logiciels-libres-dans-lAdministration-le-gouvernement-a-son-plan-daction
featured_image: https://cdn.ttgtmedia.com/visuals/LeMagIT/hero_article/Elections-France.jpg
tags:
- Administration
series:
- 202145
---

> Le gouvernement a exposé son plan d’action pour « accélérer » l’adoption des logiciels libres du partage des données et des algorithmes au sein de l’Administration. La feuille de route entend structurer des initiatives existantes sans imposer leur recours.
