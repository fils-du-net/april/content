---
site: Numerama
title: "Et si l'on ouvrait de force le code source des logiciels qui ne sont plus mis à jour?"
author: Julien Lausson
date: 2021-11-24
href: https://www.numerama.com/tech/757842-et-si-lon-ouvrait-de-force-le-code-source-des-logiciels-qui-ne-sont-plus-mis-a-jour.html
featured_image: https://www.numerama.com/wp-content/uploads/2021/11/code-source.jpg
tags:
- Droit d'auteur
- Institutions
series:
- 202147
series_weight: 0
---

> Un amendement propose de forcer l'ouverture du code source des logiciels, lorsque les mises à jour ne sont plus assurées. Une idée qui entend répondre au problème de l'obsolescence logicielle, mais qui fait face à des obstacles en nombre.
