---
site: EurActiv
title: "Le gouvernement veut accélérer le déploiement de l'open source"
author: Mathieu Pollet
date: 2021-11-11
href: https://www.euractiv.fr/section/economie/news/le-gouvernement-veut-accelerer-le-deploiement-de-lopen-source
featured_image: https://www.euractiv.fr/wp-content/uploads/sites/3/2021/11/IMG_20211110_171803-800x450.jpg
tags:
- Administration
- april
- Institutions
series:
- 202145
series_weight: 0
---

> La ministre de la Transformation et de la Fonction publiques, Amélie de Montchalin, a présenté mercredi 10 novembre la feuille de route du gouvernement pour développer le recours à l'open source, vecteur de souveraineté numérique et gage de «confiance démocratique».
