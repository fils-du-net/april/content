---
site: Le Monde Informatique
title: "Logiciels libres: le Gouvernement étoffe son plan d'action"
author: Jacques Cheminat
date: 2021-11-12
href: https://www.lemondeinformatique.fr/actualites/lire-logiciels-libres-le-gouvernement-etoffe-son-plan-d-action-84785.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000082112.jpg
tags:
- Administration
series:
- 202145
---

> A l'occasion de l'Open Source Expérience, Amélie de Montchalin, ministre de la transformation et de la fonction publique a dévoilé son plan d'action sur les logiciels libres dans le secteur public. Au menu, le lancement de la plateforme code.gouv.fr, des ouvertures de code, ainsi qu'un effort autour du recrutement.
