---
site: La Vie
title: "Laurent Chemla, premier cyberpirate de France"
author: Constance Vilanova
date: 2021-11-10
href: https://www.lavie.fr/actualite/societe/laurent-chemla-premier-cyberpirate-de-france-78611.php
featured_image: https://medias.lavie.fr/api/v1/images/view/618a939dd286c201583726b8/width_1000/image.jpg
tags:
- Sensibilisation
- Vie privée
series:
- 202145
series_weight: 0
---

> Il est le premier informaticien français à avoir été inculpé en mai 1986, puis relaxé pour piratage… à partir d’un Minitel. Portrait.
