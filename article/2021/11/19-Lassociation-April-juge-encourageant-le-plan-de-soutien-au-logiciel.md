---
site: aef info
title: "L’association April juge encourageant le plan de soutien au logiciel libre dans les administrations"
date: 2021-11-19
href: https://www.aefinfo.fr/depeche/662359-
featured_image: https://www.aefinfo.fr/assets/medias/documents/4/8/481085_prv.jpeg
seeAlso: "[Plan d'action logiciels libres et communs numériques: le Gouvernement avance, à son rythme](https://www.april.org/plan-d-action-logiciels-libres-et-communs-numeriques-le-gouvernement-avance-a-son-rythme)"
tags:
- Administration
- april
series:
- 202146
series_weight: 0
---

> Le plan d’action en faveur du logiciel libre annoncé par la ministre de la Fonction publique Amélie de Montchalin "pose des bases encourageantes pour une administration tournée vers les logiciels libres", estime l’association April, le 16 novembre 2021. Le pôle logiciels libres de la Dinum sera renforcé dans le budget 2022. Ce plan ne constitue toutefois pas un "changement de paradigme", regrette-t-elle, citant le cas des ministères qui restent "fortement dépendants" des solutions propriétaires de Microsoft.
