---
site: l'Humanité.fr
title: "Internet. Le Cloud, l'autre emprise des Gafam sur le monde (€)"
author: Pierric Marissal
href: https://www.humanite.fr/internet-le-cloud-lautre-emprise-des-gafam-sur-le-monde-728570
featured_image: https://www.humanite.fr/sites/default/files/styles/1048x350/public/images/91624.HR.jpg?itok=VSfU5HaC
tags:
- Informatique en nuage
- Entreprise
series:
- 202147
---

> Les activités du Cloud, soit en gros le stockage («dans les nuages») des données du Web, sont les plus rentables d'Amazon ou Microsoft. Les marges dépassant le niveau stratosphérique de 60 %. Le secteur et son oligopole se sont encore renforcés avec la pandémie. Explications.
