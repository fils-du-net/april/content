---
site: BFMtv
title: "Selon des notes internes, Amazon échoue à protéger les données personnelles de ses clients"
author: Victoria Beurnez
date: 2021-10-04
href: https://www.bfmtv.com/tech/selon-des-notes-internes-amazon-echoue-a-proteger-les-donnees-personnelles-de-ses-clients_AN-202111220328.html
featured_image: https://images.bfmtv.com/p-g9vyY-taSKXX6heNVwkc58RuY=/6x69:1254x771/1600x0/images/Amazon-le-top-10-des-offres-Black-Friday-a-decouvrir-en-avant-premiere-1171089.jpg
tags:
- Vie privée
- Entreprise
series:
- 202147
---

> Déjà épinglée en juillet pour non-respect du RGPD, l'entreprise Amazon est de nouveau sur le devant de la scène du fait de notes internes, révélant une politique trop laxiste avec les données personnelles de ses clients.
