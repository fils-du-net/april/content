---
site: Journal du Net
title: "Open Source: Un choix qui doit s'imposer dans le secteur public comme dans le privé"
author: Cédric Sibaud
date: 2021-11-25
href: https://www.journaldunet.com/solutions/dsi/1506945-open-source-un-choix-qui-doit-s-imposer-dans-le-secteur-public-comme-dans-le-prive
tags:
- Administration
series:
- 202147
---

> "Tout seul on va plus vite, ensemble on va plus loin", la philosophie de l'open source et le logiciel libre sont deux mouvements distincts mais s'érigent sur les mêmes bases d'une intelligence collective, de la liberté, du partage et de la contribution.
