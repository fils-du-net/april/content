---
site: The Conversation
title: "Quand l'open data et l'open source deviennent des leviers pour accélérer la transition écologique"
author: Martin Régner
date: 2021-11-22
href: https://theconversation.com/quand-lopen-data-et-lopen-source-deviennent-des-leviers-pour-accelerer-la-transition-ecologique-161393
featured_image: https://images.theconversation.com/files/431000/original/file-20211109-22-61aj2q.png?ixlib=rb-1.1.0&rect=6%2C4%2C1389%2C962&q=45&auto=format&w=926&fit=clip
tags:
- Open Data
- Innovation
series:
- 202147
series_weight: 0
---

> Construit grâce aux données de l'Ademe, Datagir est un outil destiné à tous les acteurs souhaitant guider les consommateurs dans leurs choix.
