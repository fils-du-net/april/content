---
site: Commentçamarche.net
title: "Administration numérique: l'État mise sur le logiciel libre"
author: Philippe Crouzillacq
date: 2021-11-17
href: https://www.commentcamarche.net/applis-sites/bureautique/1843-administration-numerique-le-gouvernement-mise-sur-le-logiciel-libre
featured_image: https://img-19.ccm2.net/BGVSm7vahAXOz_VK9ZW_FwdVZBo=/1000x420/smart/ebac8f90c2f1448baeb8bdf81f25681e/ccmcms-commentcamarche/29555247.jpg
tags:
- Administration
series:
- 202146
---

> La ministre de la Transformation et de la Fonction publique vient de présenter la nouvelle stratégie du Gouvernement pour favoriser l'adoption et l'utilisation des logiciels libres et open source dans l'administration française.
