---
site: Archimag
title: "Cloud: le gouvernement annonce une enveloppe de 1,8 milliard d'euros"
author: Bruno Texier
date: 2021-11-03
href: https://www.archimag.com/demat-cloud/2021/11/03/cloud-gouvernement-annonce-enveloppe-18-milliard-euros
featured_image: https://www.archimag.com/sites/archimag.com/files/styles/article/public/web_articles/image/cloud-financement_0.jpg?itok=ERNML3Q1
tags:
- Informatique en nuage
- Institutions
series:
- 202144
series_weight: 0
---

> La doctrine 'Cloud au centre' va bénéficier d'un financement public-privé auquel l'Europe va participer à hauteur de 444 millions d'euros. Objectif: faire émerger des champions industriels français et européens.
