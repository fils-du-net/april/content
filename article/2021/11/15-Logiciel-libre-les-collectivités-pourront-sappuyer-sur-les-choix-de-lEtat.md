---
site: La gazette.fr
title: "Logiciel libre: les collectivités pourront s'appuyer sur les choix de l'Etat (€)"
date: 2021-11-15
href: https://www.lagazettedescommunes.com/774210/774210
featured_image: https://www.lagazettedescommunes.com/wp-content/uploads/2019/09/logiciels.jpg
tags:
- Administration
series:
- 202146
---

> La ministre de la transformation et de la fonction publiques a présenté le plan d'action de l'Etat en faveur du logiciel libre. Si les collectivités ne sont pas directement concernées par les investissements nationaux, elles pourront s'appuyer sur les logiciels de l'Etat, notamment grâce au travail d'adaptation de l'Adullact.
