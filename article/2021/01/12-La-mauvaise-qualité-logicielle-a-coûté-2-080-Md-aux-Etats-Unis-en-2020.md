---
site: Le Monde Informatique
title: "La mauvaise qualité logicielle a coûté 2 080 Md$ aux Etats-Unis en 2020"
author: Maryse Gros
date: 2021-01-12
href: https://www.lemondeinformatique.fr/actualites/lire-la-mauvaise-qualite-logicielle-a-coute-2-080-mdhimBHsaux-etats-unis-en-2020-81614.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000076423.jpg
tags:
- Innovation
series:
- 202102
series_weight: 0
---

> Le Consortium for Information and Software Quality vient de publier son rapport sur le coût engendré par les défauts de qualité logicielle aux Etats-Unis l'an dernier. Il détaille les éléments qui lui ont permis de l'évaluer à 2 080 Md$ en 2020 et livre ses recommandations. Le CISQ expose aussi les tendances qui s'invitent dans le développement logiciel: edge, automatisation continue, poursuite de l'explosion des données, IA et cybersécurité.
