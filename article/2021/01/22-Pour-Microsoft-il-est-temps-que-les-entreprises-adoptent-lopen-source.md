---
site: BlogNT
title: "Pour Microsoft, il est temps que les entreprises adoptent l'open source"
author: Yohann Poiron
date: 2021-01-22
href: https://www.blog-nouvelles-technologies.fr/194243/microsoft-affirme-entreprises-peuvent-adopter-open-source
featured_image: https://www.blog-nouvelles-technologies.fr/wp-content/uploads/2021/01/microsoft-heart-open-source-840x500.jpg
tags:
- Entreprise
- Promotion
series:
- 202103
series_weight: 0
---

> Microsoft n'a pas seulement adopté l'open source, mais promeut aussi activement ce modèle, qui est essentiellement l'antithèse de la méthodologie commerciale propriétaire du géant du logiciel." 
