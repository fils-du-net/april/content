---
site: ZDNet France
title: "Pour lutter contre les épidémies, concevoir du matériel libre"
author: Thierry Noisette
date: 2021-01-30
href: https://www.zdnet.fr/blogs/l-esprit-libre/pour-lutter-contre-les-epidemies-concevoir-du-materiel-libre-39917169.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/01/microscope%20open%20source%20OpenFlexure.jpg
tags:
- Matériel libre
- Sciences
series:
- 202104
series_weight: 0
---

> Deux chercheurs britanniques soulignent les avantages et les limites des équipements open source, prenant le cas du respirateur et de leur microscope, et livrent exemples et recommandations.
