---
site: Sud Ouest
title: "Echapper à la domination de Google, c'est possible avec ces alternatives aux Gafam"
date: 2021-01-15
href: https://www.sudouest.fr/2021/01/13/savoir-rompre-avec-cette-dependance-8291162-10142.php
featured_image: https://images.sudouest.fr/2021/01/13/6001a6ec66a4bdd44e392e5c/widescreen/1000x500/pierre-yves-gosset-est.jpg?v1
tags:
- Vie privée
series:
- 202102
---

> Le directeur général de Framasoft, Pierre-Yves Gosset, qui est à l'origine d'un projet de «dégooglisation», nous donne les clés pour être moins «tenus» par les Gafam
