---
site: ZDNet France
title: "Open source: AWS riposte face à Elastic et crée un fork d'Elasticsearch"
author: Steven J. Vaughan-Nichols
date: 2021-01-25
href: https://www.zdnet.fr/actualites/open-source-aws-riposte-face-a-elastic-et-cree-un-fork-d-elasticsearch-39916773.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/01/open%20source__w1200.jpg
tags:
- Licenses
- Entreprise
series:
- 202104
---

> Amazon Web Services n'est pas le seul à ne pas apprécier la décision d'Elastic d'accorder une nouvelle licence à Elasticsearch sous la licence Server Side Public License.
