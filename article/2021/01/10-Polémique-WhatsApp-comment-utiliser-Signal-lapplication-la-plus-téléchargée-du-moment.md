---
site: RTL.fr
title: "Polémique WhatsApp: comment utiliser Signal, l'application la plus téléchargée du moment"
author: Benjamin Hue
date: 2021-01-10
href: https://www.rtl.fr/actu/sciences-tech/polemique-whatsapp-comment-utiliser-signal-l-application-la-plus-telechargee-du-moment-7800953035
featured_image: https://images.rtl.fr/~r/880v587/rtl/www/1375012-l-application-signal-est-presentee-par-les-experts-comme-la-plus-securisee-du-marche.png
tags:
- Vie privée
- Sensibilisation
series:
- 202101
series_weight: 0
---

> La messagerie sécurisée recommandée par les experts connaît un afflux inédit de nouveaux utilisateurs depuis l'annonce du changement des conditions d'utilisation de WhatsApp.

Vous recherchez une messagerie instantanée avec groupes et appels audio/vidéo à deux, libre, loyale, éthique et qui respecte vraiment votre vie privée? Notre Chapril vous propose un service XMPP utilisable https://chapril.org/XMPP
