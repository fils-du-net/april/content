---
site: ZDNet France
title: "Un spécialiste de l'open source nommé directeur de la technologie à la Maison blanche"
author: Steven J. Vaughan-Nichols
date: 2021-01-06
href: https://www.zdnet.fr/actualites/un-specialiste-de-l-open-source-nomme-directeur-de-la-technologie-a-la-maison-blanche-39915661.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/Pierre%20temp/Maison%20Blanche%20B__w1200.jpg
tags:
- Institutions
- Entreprise
- International
series:
- 202101
series_weight: 0
---

> Reconnu pour son expérience dans le domaine de l'open source, des normes ouvertes et de la sécurité, l'ancien directeur de l'ingénierie de Facebook, David Recordon, sera le prochain directeur de la technologie de la Maison blanche.
