---
site: Silicon
title: "Open Hardware: l'Open Source à l'assaut des processeurs"
author: David Feugey
date: 2021-01-18
href: https://www.silicon.fr/futur-de-lit-open-hardware-lopen-source-a-lassaut-des-processeurs-356853.html#
featured_image: https://www.silicon.fr/wp-content/uploads/2018/01/gemalto-etude-hardware-software.jpg
tags:
- Matériel libre
- Innovation
series:
- 202103
series_weight: 0
---

> Futur de l'IT - La rencontre entre Open Source et processeurs enregistre son premier succès d'envergure avec l'architecture RISC-V. Mais la technologie Power d'IBM est également en embuscade sur le front de l'Open Hardware.
