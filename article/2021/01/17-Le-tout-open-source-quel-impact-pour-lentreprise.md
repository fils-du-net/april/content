---
site: Le Monde Informatique
title: "Le tout open source: quel impact pour l'entreprise?"
author: Matt Asay
date: 2021-01-17
href: https://www.lemondeinformatique.fr/actualites/lire-le-tout-open-source-quel-impact-pour-l-entreprise-81651.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000076485.jpg
tags:
- Licenses
- Entreprise
series:
- 202102
series_weight: 0
---

> De nombreux éditeurs de logiciels open source ont adopté le modèle de développement «Open Core» pour augmenter leurs revenus. Ce n'est plus le cas de Yugabyte, qui, en abandonnant ce modèle économique, parvient à mieux valoriser sa production.
