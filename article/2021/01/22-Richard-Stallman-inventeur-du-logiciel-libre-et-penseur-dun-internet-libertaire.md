---
site: France Culture
title: "Richard Stallman, inventeur du logiciel libre et penseur d'un internet libertaire"
author: Yann Lagarde
date: 2021-01-22
href: https://www.franceculture.fr/numerique/richard-stallman-inventeur-du-logiciel-libre-et-penseur-dun-internet-libertaire
featured_image: https://s1.dmcdn.net/v/SisHg1W1jPReKmRQ1/x720
tags:
- Partage du savoir
- Internet
- Sensibilisation
series:
- 202103
series_weight: 0
---

> Wikipédia fête ses 20 ans en ce mois de janvier 2021. Mais l’encyclopédie collaborative n’existerait pas sans la vision de Richard Stallman, programmeur de génie, initiateur du logiciel libre et fervent défenseur d’un partage des savoirs.
