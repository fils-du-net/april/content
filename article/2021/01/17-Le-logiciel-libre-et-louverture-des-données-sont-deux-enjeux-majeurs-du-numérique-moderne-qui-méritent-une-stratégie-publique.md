---
site: Le Monde.fr
title: "«Le logiciel libre et l'ouverture des données sont deux enjeux majeurs du numérique moderne qui méritent une stratégie publique» (€)"
date: 2021-01-17
href: https://www.lemonde.fr/idees/article/2021/01/17/le-logiciel-libre-et-l-ouverture-des-donnees-sont-deux-enjeux-majeurs-du-numerique-moderne-qui-meritent-une-strategie-publique_6066551_3232.html
seeAlso: "[Logiciel libre: le Premier ministre se montrera-t-il à la hauteur du rapport Bothorel?](https://april.org/logiciel-libre-le-premier-ministre-se-montrera-t-il-a-la-hauteur-du-rapport-bothorel)"
tags:
- Open Data
- Institutions
- Europe
series:
- 202102
series_weight: 0
---

> Des acteurs du logiciel libre soulignent dans une tribune au «Monde» le rôle-clé du logiciel libre comme moyen de reconquête d'indépendance économique vis-à-vis d'acteurs extérieurs à l'Europe
