---
site: Sciences et avenir
title: "Une exposition pour décoder notre monde de codes"
author: Arnaud Devillard
date: 2021-01-31
href: https://www.sciencesetavenir.fr/decouvrir/expositions/une-expositon-pour-decoder-notre-monde-de-codes_150663
featured_image: https://www.sciencesetavenir.fr/assets/img/2021/01/08/cover-r4x3w1000-5ff883078b6c2-cil06.jpg
tags:
- Sensibilisation
- Licenses
series:
- 202104
series_weight: 0
---

> L'exposition "Code Is Law" à Paris exploite le potentiel artistique et poétique des technologies de l'information et du numérique. Et interroge notre monde technologique de manière souvent inattendue.
