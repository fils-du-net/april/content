---
site: Next INpact
title: "François Pellegrini, élu vice-président de la CNIL"
date: 2021-01-19
href: https://www.nextinpact.com/lebrief/45483/francois-pellegrini-elu-vice-president-cnil
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/square-linked-media/23.jpg
tags:
- Vie privée
- Institutions
series:
- 202103
series_weight: 0
---

> Publiée au Journal officiel ce matin, cette délibération consacre cette élection à la deuxième marche de l’autorité indépendante.
