---
site: Le Monde.fr
title: "A Bercy, une cellule d'informaticiens pour aider l'Etat à réguler les GAFA (€)"
author: Alexandre Piquard
date: 2021-01-22
href: https://www.lemonde.fr/economie/article/2021/01/22/a-bercy-une-cellule-d-informaticiens-pour-aider-l-etat-a-reguler-les-gafa_6067191_3234.html
tags:
- Entreprise
- Internet
- Institutions
- Europe
series:
- 202103
series_weight: 0
---

> Le ministère de l’économie accueille le pôle d’expertise et de régulation numérique, créé pour étudier les grandes plates-formes, comme celles de Google, Amazon, Facebook ou Apple.
