---
site: ZDNet France
title: "Après le rapport Bothorel, des acteurs des logiciels libres demandent une stratégie publique"
author: Thierry Noisette
date: 2021-01-23
href: https://www.zdnet.fr/blogs/l-esprit-libre/apres-le-rapport-bothorel-des-acteurs-des-logiciels-libres-demandent-une-strategie-publique-39916759.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/01/mascottes_GNU_Tux_Gnuliux.jpg
seeAlso: "[Logiciel libre: le Premier ministre se montrera-t-il à la hauteur du rapport Bothorel?](https://april.org/logiciel-libre-le-premier-ministre-se-montrera-t-il-a-la-hauteur-du-rapport-bothorel)  \n[«Libre à vous!» diffusée mardi 19 janvier 2021 sur radio Cause Commune](https://april.org/libre-a-vous-diffusee-mardi-19-janvier-2021-sur-radio-cause-commune)"
tags:
- Open Data
- Associations
series:
- 202103
series_weight: 0
---

> Les représentants d'associations, de clusters régionaux et d'entreprises du Libre demandent une 'mission logiciel libre' dépendant du Premier ministre, qui travaillerait avec les acteurs de l'écosystème du logiciel libre.
