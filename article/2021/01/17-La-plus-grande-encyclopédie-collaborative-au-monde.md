---
site: La Nouvelle République Algérie
title: "La plus grande encyclopédie collaborative au monde"
date: 2021-01-17
href: https://www.lnr-dz.com/2021/01/17/la-plus-grande-encyclopedie-collaborative-au-monde
featured_image: https://www.lnr-dz.com/wp-content/uploads/2021/01/Wikipedia.jpg
tags:
- Partage du savoir
series:
- 202102
---

> La plus grande encyclopédie libre du monde a été fondée le 15 janvier 2001. En croissance continue depuis, elle propose gratuitement aujourd’hui près de 55 millions d’articles dans plus de 300 langues différentes. Tous écrits, mis à jour et corrigés par des contributeurs anonymes et bénévoles.
