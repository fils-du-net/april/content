---
site: La Presse
title: "Pour un savoir libre et pluriel"
date: 2021-01-26
href: https://www.lapresse.ca/debats/opinions/2021-01-26/logiciels-proprietaires/pour-un-savoir-libre-et-pluriel.php
featured_image: https://mobile-img.lpcdn.ca/lpca/924x/r3996/c0428732-5f45-11eb-a88b-02fe89184577.jpg
tags:
- Éducation
- Logiciels privateurs
series:
- 202104
---

> Nous vous écrivons aujourd’hui pour vous faire part d’une inquiétude croissante sur l’avenir de nos universités, de la recherche et de l’enseignement. L’adoption massive de logiciels propriétaires par les universités a dénaturé le travail de nos communautés de recherche. Désormais, l’espace de notre pensée, de notre recherche et de notre enseignement est devenu l’espace privé de Microsoft, de Zoom, de Google, d’Apple et d’une poignée d’autres entreprises.
