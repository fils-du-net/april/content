---
site: AgoraVox
title: "Peace, love and Microsoft"
author: Sylvain
date: 2021-01-16
href: https://www.agoravox.fr/tribune-libre/article/peace-love-and-microsoft-230154
featured_image: https://www.agoravox.fr/local/cache-vignettes/L474xH296/external-content-duckduckgo-com-4-6fc1b.jpg
tags:
- Entreprise
series:
- 202102
---

> Microsoft a toujours été la bête noire du logiciel libre et de l'open source, le gargantua du software. Il a systématiquement racheté les petites pousses les plus prometteuses de l'informatique et les a assimilées dans son produit phare, windows et toutes ses applications
