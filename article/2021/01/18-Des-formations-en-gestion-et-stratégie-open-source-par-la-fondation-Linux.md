---
site: Le Monde Informatique
title: "Des formations en gestion et stratégie open source par la fondation Linux"
date: 2021-01-18
href: https://www.lemondeinformatique.fr/actualites/lire-des-formations-en-gestion-et-strategie-open-source-par-la-fondation-linux-81659.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000076497.jpg
tags:
- Sensibilisation
series:
- 202103
series_weight: 0
---

> La fondation Linux a lancé un programme de formations pour diffuser les meilleures pratiques en termes de gestion et de stratégie open source. 7 modules sont proposés pour un ticket d'entrée unique inférieur à 500 euros.
