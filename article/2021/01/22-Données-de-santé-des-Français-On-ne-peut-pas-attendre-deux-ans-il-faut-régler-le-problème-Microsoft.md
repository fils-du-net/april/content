---
site: L'OBS
title: "Données de santé des Français: «On ne peut pas attendre deux ans, il faut régler le problème Microsoft» (€)"
author: Timothée Vilars
date: 2021-01-22
href: https://www.nouvelobs.com/sante/20210122.OBS39190/donnees-de-sante-des-francais-on-ne-peut-pas-attendre-deux-ans-il-faut-regler-le-probleme-microsoft.html
featured_image: https://focus.nouvelobs.com/2021/01/18/0/0/1000/500/633/306/75/0/0fc6716_330342519-health.jpg
tags:
- Vie privée
series:
- 202103
---

> Ancien président de l'Institut des Données de Santé, Christian Babusiaux appelle Olivier Véran et le Health Data Hub à rompre au plus vite le contrat avec Microsoft.
