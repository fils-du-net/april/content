---
site: Les Numeriques
title: "Telegram, Signal, Olvid... Les messageries concurrentes de WhatsApp ont une carte à jouer"
author:  
date: 2021-01-16
href: https://www.lesnumeriques.com/telephone-portable/telegram-signal-olvid-les-messageries-concurrentes-de-whatsapp-ont-une-carte-a-jouer-a159259.html
featured_image: https://dyw7ncnq1en5l.cloudfront.net/optim/article/15/159259/f2293f69-telegram-signal-olvid-les-concurrentes-de-whatsapp-ont-une-carte-a-jouer__908_512__359-219-2054-1174.webp
tags:
- Vie privée
series:
- 202052
---

> Avec l'arrivée de nouvelles conditions d'utilisation chez WhatsApp, le marché des applications de messagerie sécurisée s'ouvre. Outre Telegram et Signal, deux autres géants du secteur, des concurrents plus discrets apparaissent.
