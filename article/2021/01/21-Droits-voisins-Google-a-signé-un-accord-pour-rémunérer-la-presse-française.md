---
site: RFI
title: "Droits voisins: Google a signé un accord pour rémunérer la presse française"
date: 2021-01-21
href: https://www.rfi.fr/fr/france/20210121-droits-voisins-google-a-sign%C3%A9-un-accord-pour-r%C3%A9mun%C3%A9rer-la-presse-fran%C3%A7aise
featured_image: https://s.rfi.fr/media/display/56dbd25c-5bbf-11eb-9864-005056bf87d6/w:310/p:16x9/google-news-editeurs-droit-voisin.jpg
tags:
- Droit d'auteur
- Institutions
- Europe
- Internet
- Entreprise
series:
- 202103
series_weight: 0
---

> Google et l'Alliance de la presse d'information générale (Apig), qui représente les quotidiens nationaux et régionaux français, ont annoncé ce jeudi 21 janvier la signature d'un accord ouvrant la voie à la rémunération de la presse hexagonale par le géant du Net au titre du «droit voisin», après des négociations mouvementées.
