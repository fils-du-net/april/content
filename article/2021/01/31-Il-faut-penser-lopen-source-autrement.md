---
site: Le Monde Informatique
title: "Il faut penser l'open source autrement"
author: Matt Asay
date: 2021-01-31
href: https://www.lemondeinformatique.fr/actualites/lire-il-faut-penser-l-open-source-autrement-81805.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000076752.jpg
tags:
- Licenses
- Entreprise
- Informatique en nuage
series:
- 202104
series_weight: 0
---

> Avec l'affaire Elastic et AWS, les éditeurs de logiciels libres et les fournisseurs de cloud sont à nouveau en guerre pour savoir qui peut tirer bénéfice des solutions open source. Pourtant, l'introduction de nouvelles catégories de licences pourraient résoudre le problème.
