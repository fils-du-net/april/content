---
site: ZDNet France
title: "L'Open Infrastructure Foundation annonce son premier conseil d'administration"
author: Steven J. Vaughan-Nichols
date: 2021-01-28
href: https://www.zdnet.fr/actualites/l-open-infrastructure-foundation-annonce-son-premier-conseil-d-administration-39917091.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/02/open-source-code-620__w630.jpg
tags:
- Associations
- Informatique en nuage
series:
- 202104
series_weight: 0
---

> Lorsque la Fondation OpenStack a changé d'orientation, passant du simple OpenStack à une variété de technologies dans le cloud en open source, elle a également changé d'organisation. Et voici que la Fondation Open Infrastructure a nommé son premier conseil d'administration.
