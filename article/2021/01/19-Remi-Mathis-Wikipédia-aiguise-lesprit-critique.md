---
site: leJDD.fr
title: "Rémi Mathis: 'Wikipédia aiguise l'esprit critique'"
author: Thomas Liabot
date: 2021-01-19
href: https://www.lejdd.fr/Medias/Internet/remi-mathis-wikipedia-aiguise-lesprit-critique-4019475
featured_image: https://resize-lejdd.lanmedia.fr/r/620,310,forcex,center-middle/img/var/europe1/storage/images/lejdd/medias/internet/remi-mathis-wikipedia-aiguise-lesprit-critique-4019475/56597926-1-fre-FR/Re-mi-Mathis-Wikipedia-aiguise-l-esprit-critique.jpg
tags:
- Partage du savoir
series:
- 202103
---

> INTERVIEW - Ancien président de Wikimédia France, Rémi Mathis publie un livre consacré à l'encyclopédie en ligne, 20 ans après sa création.
