---
site: Next INpact
title: "Le LABEL de la Dinum ne… labellisent pas ses «solutions»"
date: 2021-05-14
href: https://www.nextinpact.com/lebrief/47096/le-label-dinum-ne-labellisent-pas-ses-solutions
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/square-linked-media/9798.jpg
tags:
- Référentiel
- april
- Administration
series:
- 202119
---

> L'équipe LABEL de la direction interministérielle du numérique du Premier ministre vient de lancer son «Catalogue GouvTech» de solutions numériques pour les services publics. «L’évaluation de ces solutions ne repose toutefois que sur les déclarations des éditeurs», note toutefois Acteurs Publics.
