---
site: La Voix du Nord
title: "Montigny-en-Gohelle: des ordinateurs récupérés et redistribués pour lutter contre la fracture numérique (€)"
author: Youenn Martin
date: 2021-05-17
href: https://www.lavoixdunord.fr/1005912/article/2021-05-17/montigny-en-gohelle-des-ordinateurs-recuperes-et-redistribues-pour-lutter-contre
tags:
- Associations
series:
- 202120
series_weight: 0
---

> La crise sanitaire a mis en évidence l’inégalité entre les familles correctement équipées en ordinateurs et les autres. Une association d’Avion, Les Amis de Mandela, s’est décidé à réduire cette fracture numérique. Samedi, une dizaine d’ordinateurs récupérés et remis en état ont été distribués à des familles fréquentant le centre social de Montigny-en-Gohelle.
