---
site: cio-online.com
title: "La DINUM partage un catalogue d'applications recommandées"
author: Bertrand Lemaire
date: 2021-05-18
href: https://www.cio-online.com/actualites/lire-la-dinum-partage-un-catalogue-d-applications-recommandees-13192.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000017035.jpg
seeAlso: "[Catalogue de solutions de la DINUM: se proclame logiciel libre qui veut](https://april.org/catalogue-de-solutions-de-la-dinum-se-proclame-logiciel-libre-qui-veut)"
tags:
- Référentiel
- april
- Administration
series:
- 202120
series_weight: 0
---

> La catalogue GouvTech a été mis en ligne par la DINUM (Direction Interministérielle du Numérique) à l'attention des acteurs publics.
