---
site: Le Monde Informatique
title: "Le créateur de Python promet un langage plus rapide"
author: Serdar Yegulalp
date: 2021-05-21
href: https://www.lemondeinformatique.fr/actualites/lire-le-createur-de-python-promet-un-langage-plus-rapide-82998.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000078953.jpg
tags:
- Entreprise
- Internet
series:
- 202120
series_weight: 0
---

> Guido Van Rossum, créateur de Python et son équipe ont annoncé des améliorations progressives du langage et ont promis qu'il serait 2 à 5 fois plus rapide, tout en restant compatible avec le code existant.
