---
site: ouest-france.fr
title: "Le «Repair Café» numérique d’Auray aide les utilisateurs à réparer leur ordinateur"
date: 2021-05-27
href: https://www.ouest-france.fr/bretagne/auray-56400/le-repair-cafe-numerique-d-auray-aide-les-utilisateurs-a-reparer-leur-ordinateur-6ce20748-be4c-11eb-8393-c96418350d6a
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMTA1MmE2NjIzZjUyODAxM2RlNzZjNDlmNTVlZTU5ZTUxZWM?width=1260&focuspoint=50%2C50&cropresize=1&client_id=bpeditorial&sign=5af7e8b28bd309468a72e96c5253b309cca361af3fe5b9995c9d4f9305ee6e86
tags:
- Associations
series:
- 202121
series_weight: 0
---

> Avec l’Association Défis, on peut apprendre à réparer, entretenir et donc ne plus jeter ses appareils numériques. Chaque mercredi matin dans les locaux du Service citoyenneté d’Auray (Morbihan).
