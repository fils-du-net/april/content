---
site: Le Nouvelliste
title: "Logiciel libre: la coopération plutôt que la compétition"
author: Jean-Yves Proulx
date: 2021-05-25
href: https://www.lenouvelliste.ca/opinions/logiciel-libre-la-cooperation-plutot-que-la-competition-9bfee1822092c66a9b916449fdb36acf
featured_image: https://images.omerlocdn.com/resize?url=https%3A%2F%2Fgcm.omerlocdn.com%2Fproduction%2Fglobal%2Ffiles%2Fimage%2F6d00f327-5c0c-4763-8aa6-0daae073b508.jpg&width=1024&type=webp
tags:
- Sensibilisation
series:
- 202121
series_weight: 0
---

> Les environnements Windows et Mac OS sont bien connus, Microsoft Office et Google Chrome, tout autant. Par contre, lorsqu'on parle d'Ubuntu, de LinuxMint, de Fedora, d'Apache, de LibreOffice... il y a de fortes chances que des yeux s'écarquillent.
