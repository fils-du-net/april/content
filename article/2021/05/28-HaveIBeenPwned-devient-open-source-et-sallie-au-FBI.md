---
site: 01net.
title: "HaveIBeenPwned devient open source et s'allie au FBI"
author: Gilbert Kallenborn
date: 2021-05-28
href: https://www.01net.com/actualites/haveibeenpwned-devient-open-source-et-s-allie-au-fbi-2043441.html
featured_image: https://img.bfmtv.com/c/630/420/a9f/615e2edb03b27f035d3387693e83b.jpg
tags:
- Vie privée
- Internet
- Institutions
series:
- 202121
series_weight: 0
---

> Une première partie du travail de Troy Hunt est désormais en source ouverte, à savoir Pwned Passwords. Il en profite pour faire un premier appel à la communauté.
