---
site: ZDNet France
title: "Have I Been Pwned opte pour l'open source"
author: Steven J. Vaughan-Nichols
date: 2021-05-28
href: https://www.zdnet.fr/actualites/have-i-been-pwned-opte-pour-l-open-source-39923569.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/06/StrongWeakPasswords__w1200.jpg
tags:
- Vie privée
series:
- 202121
---

> Vous voulez savoir si quelqu'un a volé vos identifiants et mots de passe? Dans ce cas, vous pouvez utiliser 'Have I Been Pwned', dont le code est en cours de publication.
