---
site: Clubic.com
title: "Le logiciel libre Audacity \"acheté\" par le groupe Muse"
author: Thibaut Keutchayan
date: 2021-05-05
href: https://www.clubic.com/pro/legislation-loi-internet/propriete-intellectuelle/logiciel-libre-open-source/actualite-370562-le-logiciel-libre-audacity-achete-par-le-groupe-muse.html
featured_image: https://pic.clubic.com/v1/images/1674455/raw.webp?fit=max&width=1200&hash=112244d087db916205abb0a201867dc608adbb36
tags:
- Économie
series:
- 202118
series_weight: 0
---

> Muse Group a officialisé l'achat du célèbre logiciel libre d'édition musicale Audacity. Celui-ci rejoint notamment MuseScore, également consacré à la création musicale, le tout en open-source.
