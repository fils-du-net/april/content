---
site: Acteurs Publics
title: "La Dinum lance son catalogue de logiciels recommandés"
author: Emile Marzolf
date: 2021-05-12
href: https://www.acteurspublics.fr/articles/la-dinum-lance-son-catalogue-de-logiciels-recommandes
featured_image: https://www.acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/34/6b3423965fc62aaba8953d163387b3f064d9cc74.jpeg
tags:
- Référentiel
- Administration
series:
- 202119
---

> La DSI de l’État a mis en ligne un catalogue pour réunir les solutions numériques recommandées aux acteurs publics en fonction de critères d’écoconception, de sécurité, de transparence ou encore d’accessibilité aux personnes handicapées. L’évaluation de ces solutions ne repose toutefois que sur les déclarations des éditeurs.
