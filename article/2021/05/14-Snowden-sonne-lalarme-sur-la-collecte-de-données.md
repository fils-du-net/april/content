---
site: Le Journal de Montréal
title: "Snowden sonne l'alarme sur la collecte de données"
author: Nora T. Lamontagne
href: https://www.journaldemontreal.com/2021/05/13/snowden-sonne-lalarme-sur-la-collecte-de-donnees
featured_image: https://m1.quebecormedia.com/emp/emp/64242818_3542032f72c2e1-4893-4f76-b83e-28744390ee32_ORIGINAL.jpg
tags:
- Vie privée
- Internet
series:
- 202119
series_weight: 0
---

> Plus encore que les fuites de données personnelles, c’est la collecte massive d’informations en ligne à notre sujet qui devrait nous inquiéter, juge le lanceur d’alertes américain Edward Snowden.
