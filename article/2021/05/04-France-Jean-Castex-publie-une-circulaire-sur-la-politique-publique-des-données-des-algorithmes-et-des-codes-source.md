---
site: Developpez.com
title: "France: Jean Castex publie une circulaire sur la politique publique des données, des algorithmes et des codes source"
author: Stéphane le calme
date: 2021-05-04
href: https://www.developpez.com/actu/314820/France-Jean-Castex-publie-une-circulaire-sur-la-politique-publique-des-donnees-des-algorithmes-et-des-codes-source-dans-laquelle-il-plaide-pour-l-usage-du-logiciel-libre-et-ouvert/
featured_image: https://www.developpez.net/forums/attachments/p597176d1/a/a/a
tags:
- Institutions
- Open Data
series:
- 202118
---

> En décembre 2020, le député LaREM Eric Bothorel a remis au Premier ministre un rapport sur la politique publique de la donnée, des algorithmes et des codes source. Jean Castex, le Premier ministre avait assuré qu’il serait suivi de plusieurs «impulsions politiques» pour entrer dans un «deuxième âge de la politique de la donnée, des codes source et algorithmes».
