---
site: L'Echo
title: "Quel plan de relance pour l'enseignement à l'ère post-covid?"
author: Soufiane Amzur
date: 2021-05-12
href: https://www.lecho.be/economie-politique/belgique/general/quel-plan-de-relance-pour-l-enseignement-a-l-ere-post-covid/10305074.html
featured_image: https://images.lecho.be/view?iid=Elvis:6wmHZGUK4OiA1AzDjxuH-1&context=ONLINE&ratio=0/0&width=640&u=1620782461000
tags:
- Éducation
- Open Data
series:
- 202119
series_weight: 0
---

> Cinq priorités se dégagent pour assurer la transition entre l'école d'aujourd'hui et l'école de demain.
