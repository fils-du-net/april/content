---
site: LeMagIT
title: "Administrations: le catalogue GouvTech ne tient pas toutes ses promesses (€)"
author: Gaétan Raoul
date: 2021-05-19
href: https://www.lemagit.fr/actualites/252501061/Administrations-le-catalogue-GouvTech-ne-tient-pas-toutes-ses-promesses
featured_image: https://cdn.ttgtmedia.com/visuals/LeMagIT/hero_article/cybersecurite_France_AdobeStock_226588321.jpeg
tags:
- Référentiel
- april
series:
- 202120
---

> En marge des annonces consacrées à la stratégie cloud du gouvernement français, la direction interministérielle du numérique (DINUM) a dévoilé le 17 mai la disponibilité de son catalogue GouvTech. En gestation depuis deux ans, l’outil qui doit lister des solutions logicielles adressées aux administrations, accuse sa jeunesse et présente plusieurs défauts qui l’éloignent de la proposition originelle.
