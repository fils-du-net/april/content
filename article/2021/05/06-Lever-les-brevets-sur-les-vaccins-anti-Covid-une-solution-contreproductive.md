---
site: Le HuffPost
title: "Lever les brevets sur les vaccins anti-Covid, une solution contreproductive?"
author: Lucie Oriol
date: 2021-05-06
href: https://www.huffingtonpost.fr/entry/lever-brevets-vaccins-anti-covid-solution-contreproductive_fr_6093da2de4b05af50dcc9026
featured_image: https://img.huffingtonpost.com/asset/60940f8b2600002a5eb42651.jpg?cache=NwjQM5DoyV&ops=scalefit_630_noupscale
tags:
- Partage du savoir
- Sciences
- Institutions
- International
series:
- 202118
series_weight: 0
---

> Le vaccin contre le Covid-19 doit être accessible à tous, mais qu'en est-il de sa formule? Les enjeux autour de la levée des brevets mobilisent actuellement toute la communauté internationale.
