---
site: usine-digitale.fr
title: "La Cnil dit non aux outils collaboratifs américains pour l'enseignement supérieur et de la recherche"
author: Alice Vitard
date: 2021-05-27
href: https://www.usine-digitale.fr/editorial/la-cnil-dit-non-aux-outils-collaboratifs-americains-pour-l-enseignement-superieur-et-de-la-recherche.N1097114
featured_image: https://www.usine-digitale.fr/mediatheque/9/4/1/000965149_homePageUne/enseignement.jpg
tags:
- Éducation
- Europe
- Institutions
- Entreprise
series:
- 202121
series_weight: 0
---

> La Cnil veut mettre un terme à l'utilisation par l'enseignement supérieur et de la recherche d'outils collaboratifs proposés par des entreprises américaines car les données ainsi hébergées peuvent être transférées aux autorités américaines. Une période transitoire est prévue avant de basculer tous les services vers des solutions alternatives.
