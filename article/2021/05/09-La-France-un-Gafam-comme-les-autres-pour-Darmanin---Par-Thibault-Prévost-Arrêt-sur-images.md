---
site: Arrêt sur images
title: "La France, un Gafam comme les autres pour Darmanin"
author: Thibault Prévost
date: 2021-05-09
href: https://www.arretsurimages.net/chroniques/clic-gauche/la-france-un-gafam-comme-les-autres-pour-darmanin
featured_image: https://api.arretsurimages.net/api/public/media/le-texte-devoile-par-nextinpact/action/show?format=public&t=2021-05-08T05:51:39+02:00
tags:
- Institutions
- Vie privée
- Internet
series:
- 202119
series_weight: 0
---

> La critique média en toute indépendance. Enquêtes et chroniques chaque jour, émission hebdomadaire présentée par Daniel Schneidermann.
