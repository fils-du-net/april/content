---
site: ZDNet France
title: "Open source: les devs d'Ingenuity et de Perseverance s'associent à une entreprise de drones pour créer une architecture ouverte"
author: Greg Nichols
date: 2021-05-21
href: https://www.zdnet.fr/actualites/open-source-les-devs-d-ingenuity-et-de-perseverance-s-associent-a-une-entreprise-de-drones-pour-creer-une-architecture-ouverte-39923165.htm
featured_image: https://www.zdnet.com/a/hub/i/2021/05/19/6c534897-bc0e-4005-9cba-b2c0e901b4b1/ingenuity.jpg
tags:
- Sciences
- Entreprise
series:
- 202120
series_weight: 0
---

> L'avenir des drones est à code source ouvert, et la course mondiale à la suprématie fait que les entreprises américaines se serrent les coudes.
