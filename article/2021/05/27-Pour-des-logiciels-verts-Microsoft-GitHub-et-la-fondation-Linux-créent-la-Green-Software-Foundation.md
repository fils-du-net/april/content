---
site: ZDNet France
title: "Pour des logiciels «verts»: Microsoft, GitHub et la fondation Linux créent la Green Software Foundation"
author: Thierry Noisette
date: 2021-05-27
href: https://www.zdnet.fr/blogs/l-esprit-libre/pour-des-logiciels-verts-microsoft-github-et-la-fondation-linux-creent-la-green-software-foundation-39923501.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/05/informatique_vert_green-background-Pixabay.jpg
tags:
- Entreprise
series:
- 202121
series_weight: 0
---

> Réduire l'impact environnemental du fonctionnement des logiciels, en particulier les émissions de gaz à effet de serre, c'est l'ambition annoncée par la Fondation du logiciel vert.
