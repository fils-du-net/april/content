---
site: Le Monde Informatique
title: "Google et Microsoft rejoignent CNSF, framework de sécurité cloud open source"
author: Dominique Filippone
date: 2021-05-06
href: https://www.lemondeinformatique.fr/actualites/lire-google-et-microsoft-rejoignent-cnsf-framework-de-securite-cloud-open-source-82849.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000078672.jpg
tags:
- Informatique en nuage
- Entreprise
series:
- 202118
series_weight: 0
---

> Piloté par l'open network user group (ONUG), le groupe de travail Cloud Native Security Framework a pour ambition de proposer une architecture open source visant à standardiser la sécurité du cloud. Aux côtés de FedEx et GE Healthcare, des fournisseurs dont Google, IBM, Microsoft et VMware font partie de cette initiative.
