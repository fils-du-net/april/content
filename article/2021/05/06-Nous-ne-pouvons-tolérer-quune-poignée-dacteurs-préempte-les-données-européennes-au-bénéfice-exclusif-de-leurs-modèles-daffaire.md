---
site: Le Monde.fr
title: "«Nous ne pouvons tolérer qu'une poignée d'acteurs préempte les données européennes au bénéfice exclusif de leurs modèles d'affaire»"
author: Jean-Luc Beylat, Bernard Duverneuil, Gérard Roucairol
date: 2021-05-06
href: https://www.lemonde.fr/idees/article/2021/05/06/nous-ne-pouvons-tolerer-qu-une-poignee-d-acteurs-preempte-les-donnees-europeennes-au-benefice-exclusif-de-leurs-modeles-d-affaire_6079350_3232.html
tags:
- Informatique en nuage
- Europe
- Entreprise
series:
- 202118
series_weight: 0
---

> Les trois promoteurs pour la France de l'initiative européenne Gaia-X de gestion des données plaident, dans une tribune au «Monde», pour un «cloud de confiance» commun à l'Union européenne
