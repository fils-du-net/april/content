---
site: ZDNet France
title: "'Des logiciels libres pas libres': un catalogue de la DINUM taxé d'open source washing"
author: Thierry Noisette
date: 2021-05-13
href: https://www.zdnet.fr/blogs/l-esprit-libre/des-logiciels-libres-pas-libres-un-catalogue-de-la-dinum-taxe-d-open-source-washing-39922575.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/04/catalogue%20GouvTech%20distributions.jpg
tags:
- Référentiel
- april
- Administration
series:
- 202119
series_weight: 0
---

> Le catalogue de logiciels recommandés aux administrations que publie la DINUM suscite la critique: il présente comme libres des logiciels qui ne le sont pas.
