---
site: Journal du Net
title: "\"Un million de personnes se connectent tous les jours à Lichess et jouent plus de cinq millions de parties\""
author: Frantz Grenier
date: 2021-05-21
href: https://www.journaldunet.com/media/publishers/1501353-thibault-duplessis-lichess-org
featured_image: https://img-0.journaldunet.com/wC3OwloSk0QU31wS8zt2MEsysxo=/250x/smart/0d1063f9841d490cb8499e234ce21887/ccmcms-jdn/24796514.jpg
tags:
- Sensibilisation
- Internet
series:
- 202120
series_weight: 0
---

> Méconnue du grand public, la plateforme d'échecs en ligne Lichess.org est une des plus belles réussites du Web français à l'international. Gratuite et 100% open source, elle détonne de ses concurrents payants, à l'image de son créateur.
