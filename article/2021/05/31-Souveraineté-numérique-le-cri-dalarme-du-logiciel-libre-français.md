---
site: Les Echos
title: "Souveraineté numérique: le cri d'alarme du logiciel libre français (€)"
author: Florian Dèbes
date: 2021-05-31
href: https://www.lesechos.fr/tech-medias/hightech/souverainete-numerique-le-cri-dalarme-du-logiciel-libre-francais-1319474
featured_image: https://media.lesechos.com/api/v1/images/view/60b507963e454654902e6368/1280x720/0611120311426-web-tete.jpg
tags:
- Institutions
series:
- 202122
---

> Les professionnels du secteur regrettent l'absence d'une véritable politique économique en faveur de l'open source en France. Ils estiment que leurs logiciels constituent une alternative aux technologies américaines.
