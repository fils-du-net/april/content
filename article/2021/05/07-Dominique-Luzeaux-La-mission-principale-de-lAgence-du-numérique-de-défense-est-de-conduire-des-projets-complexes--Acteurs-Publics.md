---
site: Acteurs Publics 
title: "Dominique Luzeaux: “La mission principale de l'Agence du numérique de défense est de conduire des projets complexes” (€)"
author: Emile Marzolf
date: 2021-05-07
href: https://acteurspublics.fr/articles/dominique-luzeaux-la-mission-principale-de-lagence-du-numerique-de-defense-est-de-conduire-des-projets-complexe
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/34/786029f42f53ce2acc8116b278319cced51088f9.jpeg
seeAlso: "[Circulaire données et codes sources: un premier pas dans la bonne direction qui doit être confirmé](https://www.april.org/circulaire-donnees-et-codes-sources-un-premier-pas-dans-la-bonne-direction-qui-doit-etre-confirme)"
tags:
- Institutions
series:
- 202118
series_weight: 0
---

> Le directeur de l’Agence du numérique de défense, Dominique Luzeaux, qui a préfiguré sa mise en place, détaille à Acteurs publics les missions et le fonctionnement de ce nouvel organisme rattaché à la Délégation générale de l'armement (DGA), mais au service de l’ensemble du ministère. " 
