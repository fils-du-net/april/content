---
site: usine-digitale.fr
title: "Parasitisme: fondement judiciaire idéal pour sanctionner l'utilisation indue d'un logiciel libre?"
author: Pascal Agosti
date: 2021-05-04
href: https://www.usine-digitale.fr/article/parasitisme-fondement-judiciaire-ideal-pour-sanctionner-l-utilisation-indue-d-un-logiciel-libre.N1089519
featured_image: https://www.usine-digitale.fr/mediatheque/1/7/8/000876871_homePageUne/ordinateur.jpg
tags:
- Licenses
- Entreprise
- Institutions
series:
- 202118
series_weight: 0
---

> La donne est en train d’évoluer autour de l’utilisation des logiciels libres. Cette chronique de Pascal Agosti, avocat associé au sein du Cabinet Caprioli & Associés, vient préciser l’importance du parasitisme pour sanctionner certaines sociétés œuvrant directement ou indirectement dans le domaine du logiciel libre, peu scrupuleuses et réutilisant le code ouvert en se l’appropriant de manière indue. Au-delà du fondement contractuel ou de l’action en contrefaçon, le parasitisme peut être plus facilement mis en œuvre au regard des stratégies judiciaires.
