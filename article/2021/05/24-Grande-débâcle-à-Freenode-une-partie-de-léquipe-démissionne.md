---
site: Next INpact
title: "Grande débâcle à Freenode, une partie de l'équipe démissionne"
date: 2021-05-24
href: https://www.nextinpact.com/lebrief/47185/grande-debacle-a-freenode-partie-equipe-demissionne
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/wide-linked-media/2650.jpg
tags:
- Internet
series:
- 202121
---

> Le réseau IRC – créé en 1995 comme un canal de support pour Linux avant de s’élargir à de nombreux projets open source – traverse une période de crise, et une partie de son équipe (de volontaires) appelle désormais à le quitter pour des cieux plus cléments.
