---
site: ZDNet France
title: "Freenode est abandonné par plusieurs projets libres, dont Wikimedia et Ubuntu"
author: Thierry Noisette
date: 2021-05-31
href: https://www.zdnet.fr/blogs/l-esprit-libre/freenode-est-abandonne-par-plusieurs-projets-libres-dont-wikimedia-et-ubuntu-39923691.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/05/Freenode_logo.jpg
tags:
- Internet
series:
- 202122
series_weight: 0
---

> La prise de contrôle du réseau de chat populaire chez les libristes entraîne le départ de plusieurs organisations, au profit du nouvel outil Libera Chat, créé par des anciens de Freenode.
