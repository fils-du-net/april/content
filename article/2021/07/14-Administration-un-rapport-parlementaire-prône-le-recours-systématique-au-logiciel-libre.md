---
site: ZDNet France
title: "Administration: un rapport parlementaire prône le recours systématique au logiciel libre"
author: Thierry Noisette
date: 2021-07-14
href: https://www.zdnet.fr/blogs/l-esprit-libre/administration-un-rapport-parlementaire-prone-le-recours-systematique-au-logiciel-libre-39926175.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/07/numerique_monitor-Pixabay.jpg
tags:
- Institutions
- Administration
- Informatique en nuage
series:
- 202128
series_weight: 0
---

> Et si au lieu d'«encourager» l'utilisation des logiciels libres, l'administration y recourait systématiquement? Le rapport Latombe propose que l'usage des solutions propriétaires devienne «une exception».
