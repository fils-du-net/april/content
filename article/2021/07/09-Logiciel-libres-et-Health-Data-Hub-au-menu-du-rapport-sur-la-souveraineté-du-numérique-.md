---
site: Next INpact
title: "Logiciel libres et Health Data Hub au menu du rapport sur la souveraineté du numérique (€)"
description: "Avec fleurs et couronnes"
author: Marc Rees
date: 2021-07-09
href: https://www.nextinpact.com/article/47722/logiciel-libres-et-health-data-hub-au-menu-rapport-sur-souverainete-numerique
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/9762.jpg
tags:
- Informatique en nuage
- Institutions
series:
- 202127
series_weight: 0
---

> Le député Philippe Latombe publie son rapport d’information visant à «bâtir et promouvoir une souveraineté numérique nationale et européenne». L’épais document – 198 pages – est le fruit notamment de 83 auditions menées par le député Modem.
