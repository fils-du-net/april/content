---
site: Le Monde Informatique
title: "Audacity rétropédale sur sa politique de confidentialité"
author: Célia Seramour
date: 2021-07-27
href: https://www.lemondeinformatique.fr/actualites/lire-audacity-retropedale-sur-sa-politique-de-confidentialite-83709.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000080242.png
tags:
- Vie privée
series:
- 202130
series_weight: 0
---

> Après une mise à jour de sa politique de confidentialité, début juillet, perçue comme trop intrusive par ses utilisateurs, Audacity s'excuse. L'éditeur a révisé ses règles notamment sur la collecte de données et sur l'âge minimum pour se servir de l'utilitaire.
