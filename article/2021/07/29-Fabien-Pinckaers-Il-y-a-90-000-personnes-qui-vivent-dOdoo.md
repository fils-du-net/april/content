---
site: La Libre.be
title: "Fabien Pinckaers: 'Il y a 90 000 personnes qui vivent d'Odoo'"
author: Pierre-François Lovens
date: 2021-07-29
href: https://www.lalibre.be/archives-journal/2021/07/29/fabien-pinckaers-il-y-a-90-000-personnes-qui-vivent-dodoo-ZIUDONAAHBGHDF2YKDC2BPKWN4
tags:
- Entreprise
series:
- 202130
series_weight: 0
---

> Quel intérêt voyez-vous dans l’opération financière menée par Summit Partners dès lors qu’il n’y a pas eu d’augmentation de capital et que vous gardez plus de 50 % des parts d’Odoo?
