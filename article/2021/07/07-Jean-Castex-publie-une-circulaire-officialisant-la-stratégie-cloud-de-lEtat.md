---
site: cio-online.com
title: "Jean Castex publie une circulaire officialisant la stratégie cloud de l'Etat"
date: 2021-07-07
href: https://www.cio-online.com/actualites/lire-jean-castex-publie-une-circulaire-officialisant-la-strategie-cloud-de-l-etat-13343.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000017350.jpg
tags:
- Informatique en nuage
- Institutions
series:
- 202127
series_weight: 0
---

> La doctrine sur l'usage du cloud par l'État pour ses propres services est désormais officialisée par une circulaire du Premier Ministre.
