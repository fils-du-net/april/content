---
site: Le Monde.fr
title: "La doctrine de la France sur le cloud fait débat (€)"
author: Alexandre Piquard
date: 2021-07-01
href: https://www.lemonde.fr/economie/article/2021/07/01/la-doctrine-de-la-france-sur-le-cloud-fait-debat_6086512_3234.html
tags:
- Informatique en nuage
- Institutions
- Entreprise
series:
- 202126
series_weight: 0
---

> La promotion par l’Etat de solutions «hybrides» pour l’hébergement de données, associant logiciels américains et sociétés françaises, génère des inquiétudes sur la place des acteurs nationaux et la réalité de la souveraineté numérique de la France.
