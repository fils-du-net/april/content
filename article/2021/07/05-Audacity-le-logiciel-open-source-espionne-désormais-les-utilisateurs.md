---
site: PhonAndroid
title: "Audacity: le logiciel open source espionne désormais les utilisateurs"
author: Florian Bayard
date: 2021-07-05
href: https://www.phonandroid.com/audacity-open-source-espionne-utilisateurs.html
featured_image: https://img.phonandroid.com/2021/07/audacity-spyware-espion.jpg
tags:
- Vie privée
series:
- 202127
---

> Audacity, le célèbre logiciel open source de montage audio, est devenu un véritable spyware. Récemment acheté par la société russe Muse Group, le logiciel collecte une quantité astronomique de données sur ses utilisateurs.
