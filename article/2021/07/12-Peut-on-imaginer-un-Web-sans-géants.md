---
site: Numerama
title: "Peut-on imaginer un Web sans géants?"
author: Moran Kerinec
date: 2021-07-12
href: https://www.numerama.com/tech/725471-peut-on-imaginer-un-web-sans-geants.html
featured_image: https://www.numerama.com/wp-content/uploads/2021/07/gulliver.jpg
tags:
- Vie privée
- Internet
- april
- Associations
series:
- 202128
series_weight: 0
---

> Un monde sans GAFA est-il envisageable? Peut-être, mais sous condition d'agir sur les moyens techniques, de faire évoluer les pratiques, et d'être soutenu par une forte volonté politique.
