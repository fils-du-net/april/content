---
site: L'OBS
title: "Salaire unique, transparence totale… Dans ces entreprises, «on remet l'argent à sa place» (€)"
author: Agathe Ranc
date: 2021-07-05
href: https://www.nouvelobs.com/economie/20210705.OBS46120/salaire-unique-transparence-totale-dans-ces-entreprises-on-remet-l-argent-a-sa-place.html
featured_image: https://focus.nouvelobs.com/2021/06/29/337/0/1024/512/633/306/75/0/146b849_751543257-049-f0252649.jpg
tags:
- Économie
- Entreprise
series:
- 202127
series_weight: 0
---

> Des entreprises ont mis en place des modes de rémunération plus justes, plus transparents, ou carrément égalitaires. Les salariés nous racontent ce que cela change à leur vision de leur salaire et de leur travail.
