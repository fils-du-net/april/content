---
site: Numerama
title: "Audacity va-t-il vraiment vous espionner?"
author: Corentin Bechade
date: 2021-07-05
href: https://www.numerama.com/tech/723796-audacity-va-t-il-vraiment-vous-espionner.html
featured_image: https://www.numerama.com/wp-content/uploads/2021/07/espion-la-vie-des-autres.jpg
tags:
- Vie privée
series:
- 202127
series_weight: 0
---

> Audacity s'intéresserait d'un peu trop près à vos données personnelles. Voilà l'alarme qu'a lancée la communauté du logiciel libre, suite à l'évolution de la politique de confidentialité du logiciel de manipulation audio bien connu. La situation est évidemment un poil plus nuancée.
