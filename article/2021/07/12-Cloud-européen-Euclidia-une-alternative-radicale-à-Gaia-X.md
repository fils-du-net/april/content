---
site: Silicon
title: "Cloud européen: Euclidia, une alternative radicale à Gaia-X?"
date: 2021-07-12
href: https://www.silicon.fr/cloud-europeen-euclidia-alternative-gaia-x-412268.html
featured_image: https://www.silicon.fr/wp-content/uploads/2021/07/Euclidia.png
tags:
- Informatique en nuage
- Marchés publics
- Europe
series:
- 202128
series_weight: 0
---

> Influencer le financement et l'accès aux marchés publics de technologies cloud de pointe 'fabriquées en Europe' c'est l'objectif d'Euclidia.
