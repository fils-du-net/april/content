---
site: Heidi.news
title: "Comment Microsoft s'est imposé dans les écoles romandes"
author: Sophie Gaitzsch
date: 2021-07-02
href: https://www.heidi.news/education/comment-microsoft-s-est-impose-dans-les-ecoles-romandes
featured_image: https://heidi-17455.kxcdn.com/photos/166ad826-1d6e-4801-ac5f-f2bed954cbe8/large
tags:
- Éducation
- Logiciels privateurs
series:
- 202126
series_weight: 0
---

> La pandémie et le basculement inattendu vers l’école à distance au printemps 2020 ont poussé les écoles suisses à adopter de nouveaux outils numériques. Zoom, Google Classroom, Teams, Moodle… Les cantons romands ont parfois tâtonné. Mais à l’heure du bilan, le constat est clair: c’est Microsoft qui a raflé la mise. A part Genève, tous utilisent désormais la plateforme en ligne de solutions pour l’éducation du géant californien appelée «M365», qui va bien au-delà de la visioconférence.
