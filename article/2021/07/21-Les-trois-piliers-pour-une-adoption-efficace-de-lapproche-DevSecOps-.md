---
site: ZDNet France
title: "Les trois piliers pour une adoption efficace de l'approche DevSecOps"
author: Daniel Berman
date: 2021-07-21
href: https://www.zdnet.fr/actualites/les-trois-piliers-pour-une-adoption-efficace-de-l-approche-devsecops-39926267.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2016/05/automatisation-devops-300__w630.jpg
tags:
- Innovation
- Informatique en nuage
series:
- 202129
series_weight: 0
---

> L’approche DevOps est apparue il y a plus de dix ans avec pour objectif d’automatiser les processus de création, de test et de déploiement des applications. Elle consiste à la mise en place d’un lien renforcé entre les équipes de développement logiciel (Dev) et d’administration des infrastructures informatiques (Ops) et leurs activités nous explique Daniel Berman, de Snyk.
