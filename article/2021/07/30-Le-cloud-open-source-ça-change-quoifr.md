---
site: FrenchWeb
title: "Le cloud open source, ça change quoi?"
date: 2021-07-30
description: Interview de Thomas Dubus, directeur commercial Europe d’Aiven
href: https://www.frenchweb.fr/le-cloud-open-source-ca-change-quoi/426868
featured_image: https://www.frenchweb.fr/wp-content/uploads/2021/07/Aiven.jpg
tags:
- Informatique en nuage
- Entreprise
series:
- 202130
series_weight: 0
---

> Parmi les acteurs européens du cloud open source, la start-up finlandaise Aiven a levé 100 millions de dollars en mai dernier.
