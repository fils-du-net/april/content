---
site: Le Monde Informatique
title: "CBL-Mariner, la distribution Linux interne à Microsoft"
author: Jacques Cheminat
date: 2021-07-13
href: https://www.lemondeinformatique.fr/actualites/lire-cbl-mariner-la-distribution-linux-interne-a-microsoft-83603.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000080057.png
tags:
- Entreprise
series:
- 202128
series_weight: 0
---

> Microsoft dispose de sa propre distribution Linux. Elle se nomme CBL-Mariner et est utilisée par les ingénieurs de la firme pour élaborer des services et produits cloud et edge.
