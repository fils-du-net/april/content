---
site: Archimag
title: "DAM: les critères à retenir pour faire son choix"
author: Eric Le Ven
href: https://www.archimag.com/demat-cloud/2021/07/20/digital-asset-management-phototheque-multimedia-supplement
featured_image: https://www.archimag.com/sites/archimag.com/files/styles/article/public/web_articles/image/dam_choix.jpg?itok=UZa3r62W
tags:
- Informatique en nuage
- Logiciels privateurs
series:
- 202129
series_weight: 0
---

> Le choix d'une plateforme de DAM passe évidemment par une étude des différentes solutions disponibles sur le marché et par un benchmarking de celles qui semblent répondre à l'ensemble de vos besoins. Plusieurs critères entrent en compte lors de ce moment décisif. Quelles fonctionnalités faut-il rechercher, quels logiciels choisir et vers quel type d'architecture s'orienter?
