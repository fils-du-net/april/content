---
site: ActuaLitté.com
title: "Mort de Philippe Aigrain, des logiciels libres aux biens communs"
date: 2021-07-12
href: https://actualitte.com/article/101378/auteurs/mort-de-philippe-aigrain-des-logiciels-libres-aux-biens-communs
featured_image: https://actualitte.com/media/cache/width_944_webp/uploads/images/philippe-aigrain-ec24d731-fd32-445e-a471-b4a0f2d34b32-60ec2b4d6f3c5027377111.jpg
tags:
- Économie
series:
- 202128
---

> Présenté comme un spécialiste de la propriété intellectuelle, Philippe Aigrain était aussi bien poète que romancier, chercheur et épris de liberté. Ces dernières années, il s’était engagé en cofondant La Quadrature du net, organisation de défense des libertés des internautes, mais avait également pris la direction de Publie.net, la maison d’édition numérique. C’est la structure qui annonce la triste nouvelle.
