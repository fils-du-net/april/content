---
site: Slate.fr
title: "Le cofondateur de Wikipédia avoue lui-même ne plus avoir confiance en l'encyclopédie en ligne"
author: Emma Barrier
date: 2021-07-22
href: https://www.slate.fr/story/213075/cofondateur-wikipedia-plus-confiance-encyclopedie-en-ligne
featured_image: https://www.slate.fr/sites/default/files/styles/1060x523/public/luke-chesser-d8qbsyyifmw-unsplash.jpg
tags:
- Partage du savoir
series:
- 202129
series_weight: 0
---

> On y trouve les informations que l'on veut bien nous donner, estime Larry Sanger.
