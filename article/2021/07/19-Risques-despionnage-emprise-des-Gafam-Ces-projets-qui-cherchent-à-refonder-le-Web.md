---
site: LExpansion.com
title: "Risques d'espionnage, emprise des Gafam... Ces projets qui cherchent à refonder le Web"
author: Emmanuel Paquette
date: 2021-07-19
href: https://lexpansion.lexpress.fr/actualite-economique/risques-d-espionnage-emprise-des-gafam-ces-projets-qui-cherchent-a-refonder-le-web_2154970.html
featured_image: https://static.lexpress.fr/medias_11047/w_1966,h_1100,c_crop,x_14,y_106/w_2000,h_1125,c_fill,g_north/v1554891876/tim-berners-lee-1_5656371.jpg
tags:
- Internet
- Vie privée
series:
- 202129
series_weight: 0
---

> Plusieurs projets cherchent à rendre la Toile plus sécurisée et davantage respectueuse de la vie privée. Une riposte aux Gafam et à certains régimes autoritaires.
