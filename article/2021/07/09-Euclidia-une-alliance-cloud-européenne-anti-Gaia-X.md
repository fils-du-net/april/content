---
site: Le Monde Informatique
title: "Euclidia: une alliance cloud européenne anti Gaia-X"
author: Dominique Filippone
date: 2021-07-09
href: https://www.lemondeinformatique.fr/actualites/lire-euclidia-une-alliance-cloud-europeenne-anti-gaia-x-83572.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000080009.jpg
tags:
- Informatique en nuage
- Entreprise
series:
- 202127
series_weight: 0
---

> L'alliance Euclidia ambitionne de valoriser des solutions cloud uniquement européennes aussi bien open source que propriétaires pour les faire percer dans la sphère publique incluant ministères et agences gouvernementales. Elle est soutenue notamment par le CNLL, le fond de dotation du Libre et OW2.
