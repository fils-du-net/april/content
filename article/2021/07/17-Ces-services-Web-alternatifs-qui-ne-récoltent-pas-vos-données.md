---
site: Clubic.com
title: "Ces services Web alternatifs qui ne récoltent pas vos données"
date: 2021-07-17
href: https://www.clubic.com/application-mobile/dossier-377539-ces-services-web-alternatifs-qui-ne-recoltent-pas-vos-donnees.html
featured_image: https://pic.clubic.com/v1/images/1900988/raw.webp?fit=max&width=1200&hash=315a74508067a2fdf3edd4f84d90035feae39849
tags:
- Vie privée
- Internet
series:
- 202128
---

> Depuis le scandale de Cambridge Analytica, la question du droit à la vie privée à l'ère du numérique est sur toutes les lèvres. Des médias aux hautes sphères du pouvoir décisionnel européen, les GAFA et leurs homologues sont souvent interrogés, régulés et même contestés sur leurs politiques vis-à-vis des données qu'ils collectent. Mais tout cela est-il suffisant?
