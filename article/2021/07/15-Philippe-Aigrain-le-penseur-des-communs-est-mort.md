---
site: L'OBS
title: "Philippe Aigrain, le «penseur des communs», est mort"
date: 2021-07-15
href: https://www.nouvelobs.com/societe/20210715.OBS46565/philippe-aigrain-le-penseur-des-communs-est-mort.html
featured_image: https://focus.nouvelobs.com/2021/07/15/279/903/5812/2906/633/306/75/0/448f1b6_14198154-sipa-00672709-000016.jpg
tags:
- Économie
- Internet
series:
- 202128
series_weight: 0
---

> Informaticien et chercheur, Philippe Aigrain a été un défenseur ardent des libertés numériques. Il est décédé dimanche d'un accident de montagne à 71ans.
