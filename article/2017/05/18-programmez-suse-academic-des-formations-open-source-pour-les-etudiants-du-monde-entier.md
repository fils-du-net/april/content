---
site: Programmez!
title: "SUSE Academic: des formations open source pour les étudiants du monde entier"
author: fredericmazue
date: 2017-05-18
href: https://www.programmez.com/actualites/suse-academic-des-formations-open-source-pour-les-etudiants-du-monde-entier-25998
tags:
- Partage du savoir
- Éducation
---

> SUSE ouvre la porte de l’univers open source aux étudiants du monde entier par le biais de son nouveau programme de formation SUSE Academic. Moyennant un coût minime, voire nul, SUSE partage des logiciels open source, des programmes de formation, des outils pédagogiques et des services d’assistance dans le but d’aider les établissements scolaires, les universités, les centres hospitaliers universitaires (CHU) et autres établissements à utiliser, enseigner et développer des logiciels libres avec leurs étudiants.
