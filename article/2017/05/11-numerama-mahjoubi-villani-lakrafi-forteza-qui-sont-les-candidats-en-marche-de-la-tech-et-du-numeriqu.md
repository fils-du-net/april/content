---
site: Numerama
title: "Mahjoubi, Villani, Lakrafi, Forteza...: qui sont les candidats En Marche de la tech et du numérique?"
author: Corentin Durand
date: 2017-05-11
href: http://www.numerama.com/politique/257209-legislatives-qui-sont-les-candidats-en-marche-de-la-tech-des-sciences-et-du-numerique.html
tags:
- Institutions
---

> Jeudi 11 mai, En Marche a dévoilé quasiment tous les candidats qui porteront les couleurs du rassemblent La République en Marche pour les élections de juin. Parmi les 428 profils sélectionnés par le mouvement du Président Macron, on retrouve plusieurs personnalités issues de la science, de la tech et du numérique.
