---
site: toolinux
title: "Mieux comprendre les licences open source"
date: 2017-05-03
href: http://www.toolinux.com/Mieux-comprendre-les-licences-open
tags:
- Associations
- Licenses
---

> La Linux Fondation et la Free Software Fondation Europe publient de nouvelles ressources pour mieux comprendre les licences libres.
