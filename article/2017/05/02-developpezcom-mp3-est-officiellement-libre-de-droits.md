---
site: Developpez.com
title: "MP3 est officiellement libre de droits"
author: Michael Guilloux
date: 2017-05-02
href: https://www.developpez.com/actu/133688/MP3-est-officiellement-libre-de-droits-Fraunhofer-IIS-annonce-l-expiration-des-brevets-portant-sur-le-format-de-fichier-audio
tags:
- Brevets logiciels
---

> Après deux décennies d’existence, MP3 (MPEG Audio Layer 3) reste le format audio le plus populaire auprès des consommateurs, bien qu'il existe des codecs audio plus efficaces avec des fonctionnalités avancées disponibles aujourd'hui. Son développement a débuté à la fin des années 80 chez Fraunhofer IIS en Allemagne, sur la base des résultats de développement précédemment obtenus à l'Université Erlangen-Nuremberg.
