---
site: TV5MONDE
title: "Education nationale: les données scolaires bradées aux GAFAM?"
author: Pascal Hérard
date: 2017-05-20
href: http://information.tv5monde.com/info/education-nationale-les-donnees-scolaires-bradees-aux-gafam-170590
tags:
- Entreprise
- Internet
- Institutions
- Éducation
- Vie privée
---

> Polémique à l'Education nationale : un mail du ministère à destination des délégués académiques au numérique indique comment implanter les services des géants du net dans les écoles françaises. Explications.
