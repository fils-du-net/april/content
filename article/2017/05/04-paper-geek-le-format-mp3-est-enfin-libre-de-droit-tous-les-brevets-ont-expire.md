---
site: Paper Geek
title: "Le format mp3 est enfin libre de droit, tous les brevets ont expiré"
author: David
date: 2017-05-04
href: http://www.papergeek.fr/le-format-mp3-est-enfin-libre-de-droit-tous-les-brevets-ont-expire-25342
tags:
- Brevets logiciels
---

> Contrairement à ce que bon nombre d'utilisateurs pourraient penser, le format audio MP3 n'a jamais été libre de droit, même en cas d'utilisation par des applications open source. Il était couvert par plusieurs brevets dans le monde et faisait donc l'objet d'une licence commerciale. Tous ces brevets ont maintenant expiré, mettant le format dans le domaine public.
