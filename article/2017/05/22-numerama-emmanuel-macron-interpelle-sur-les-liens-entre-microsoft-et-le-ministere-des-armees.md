---
site: Numerama
title: "Emmanuel Macron interpellé sur les liens entre Microsoft et le ministère des Armées"
author: Julien Lausson
date: 2017-05-22
href: http://www.numerama.com/politique/260292-emmanuel-macron-interpelle-sur-les-liens-entre-microsoft-et-le-ministere-des-armees.html
tags:
- Entreprise
- Logiciels privateurs
- April
- Institutions
- Marchés publics
---

> L'association de promotion et de défense du logiciel libre interpelle le chef de l’État sur le renouvellement imminent du contrat entre Microsoft et le ministère des Armées. Elle demande à Emmanuel Macron d'y mettre un terme.
