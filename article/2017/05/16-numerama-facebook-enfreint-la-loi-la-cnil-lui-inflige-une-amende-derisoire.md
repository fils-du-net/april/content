---
site: Numerama
title: "Facebook enfreint la loi, la Cnil lui inflige une amende dérisoire"
author: Julien Lausson
date: 2017-05-16
href: http://www.numerama.com/politique/258401-facebook-enfreint-la-loi-la-cnil-lui-inflige-une-amende-derisoire.html
tags:
- Entreprise
- Internet
- Institutions
- Europe
- Vie privée
---

> 150 000 euros. C'est le montant de l'amende infligée par la Cnil à Facebook pour une série de manquements à la loi affectant 33 millions d'utilisateurs en France. Dérisoire au regard de l'argent gagné par le réseau social. Mais tout cela va bientôt changer.
