---
site: Numerama
title: "VideoLAN (VLC) se mobilise contre les codecs payants et propriétaires"
author: Julien Lausson
date: 2017-05-18
href: http://www.numerama.com/tech/259437-videolan-vlc-se-mobilise-contre-les-codecs-payants-et-proprietaires.html
tags:
- Associations
- Brevets logiciels
- Innovation
---

> L'association VideoLAN, qui s'occupe du développement du lecteur multimédia VLC, a décidé d'apporter son soutien au codec libre et gratuit AV1.
