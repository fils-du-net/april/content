---
site: Nouvelle République
title: "Les geeks reprennent d'assaut le château de Selles-sur-Cher"
author: Laurence Texier
date: 2017-05-30
href: http://www.lanouvellerepublique.fr/Loir-et-Cher/Loisirs/Fetes-festivals/n/Contenus/Articles/2017/05/30/Les-geeks-reprennent-d-assaut-le-chateau-de-Selles-sur-Cher-3116728
tags:
- Sensibilisation
- Associations
---

> Après une édition 2016 perturbée par les inondations, le festival Geek Faëries revient vendredi soir pour un week-end sous le signe du jeu et de la rencontre.
