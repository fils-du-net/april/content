---
site: ZDNet France
title: "L'ARCEP publie son premier rapport sur l'état de l'Internet en France"
author: Pierre Col
date: 2017-05-31
href: http://www.zdnet.fr/blogs/infra-net/l-arcep-publie-son-premier-rapport-sur-l-etat-de-l-internet-en-france-39853066.htm
tags:
- Internet
- Institutions
- Neutralité du Net
---

> Ce premier rapport de l’ARCEP fait un point exhaustif sur l’état de l’Internet en France, tant du point de vue du fonctionnement technique du réseau qu’en matière d’accessibilité pour la population et de neutralité des acteurs : un travail utile!
