---
site: Le café pédagogique
title: "Le ministère ouvre l'Ecole à Google?"
author: François Jarraud
date: 2017-05-16
href: http://www.cafepedagogique.net/lexpresso/Pages/2017/05/16052017Article636305160274839331.aspx
tags:
- Entreprise
- Institutions
- Éducation
---

> Rue de Grenelle le changement est déjà là. Après avoir soutenu durant des années les ENT (espaces numériques de travail) nationaux et prêché la sécurité face aux grands groupes étrangers, la Direction du numérique éducatif du ministère de l'éducation nationale (DNE) semble amorcer un virage. Dans un courrier que le Café pédagogique s'est procuré, Mathieu Jeandron, délégué au numérique éducatif, autorise clairement l'usage des services numériques des "GAFAM" (Google Apple Facebook Amazon Microsoft) par les établissements scolaires.
