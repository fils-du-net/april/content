---
site: L'usine Nouvelle
title: "Qui sont ces professionnels de l’industrie \"En marche\" pour devenir députés?"
author: Anne-Sophie Bellaiche
date: 2017-05-12
href: http://www.usinenouvelle.com/editorial/qui-sont-ces-professionnels-de-l-industrie-en-marche-pour-devenir-deputes.N539434
tags:
- Institutions
---

> Pour revivifier la politique, "La République en marche", le mouvement du président de la république Emmanuel Macron a investi une moitié de candidats issus de la société civile. Parmi eux des professionnels de l’industrie qui se lancent à l’assaut de l’Assemblée nationale et espèrent devenir députés. Inventaire non exhaustif
