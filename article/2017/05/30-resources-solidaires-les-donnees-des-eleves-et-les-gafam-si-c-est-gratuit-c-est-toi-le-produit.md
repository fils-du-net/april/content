---
site: Resources Solidaires
title: "Les données des élèves et les «GAFAM»: si c’est gratuit, c’est toi le produit!"
author: FCPE
date: 2017-05-30
href: http://www.ressources-solidaires.org/Les-donnees-des-eleves-et-les
tags:
- Entreprise
- Internet
- Associations
- Éducation
- Open Data
- Vie privée
---

> La FCPE a pris connaissance d’une communication de la direction nationale du numérique – DNE- indiquant qu’il était désormais possible de fournir aux «GAFAM», grands groupes fournisseurs de service web, les annuaires des établissements, ceux-ci ayant des conditions générales d’utilisation – CGU - «éducation» couvrants cette utilisation.
