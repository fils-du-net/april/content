---
site: "ouest-france.fr"
title: "Le pari de ces six étudiants allonnais a bien failli être perdu"
date: 2017-05-11
href: http://www.ouest-france.fr/pays-de-la-loire/allonnes-72700/le-pari-de-ces-six-etudiants-allonnais-bien-failli-etre-perdu-4981369
tags:
- Partage du savoir
- Promotion
---

> Six élèves du lycée Malraux, à Allonnes, offrent aux associations des ordinateurs reconditionnés et équipés de logiciels libres de droits. Le souci? Ils ont juste eu un peu de mal à les donner.
