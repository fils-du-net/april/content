---
site: clicanoo
title: "“Il faut faire évoluer la propriété intellectuelle pour faire face aux défis du numérique”"
author: iJohanne Chung
date: 2017-05-23
href: https://www.clicanoo.re/Culture-Loisirs/Article/2017/05/23/Il-faut-faire-evoluer-la-propriete-intellectuelle-pour-faire
tags:
- Entreprise
- Économie
- Institutions
- Droit d'auteur
- Europe
---

> À l’heure où internet permet la diffusion et le partage de toutes sortes de contenus, notamment culturels, qui se demande si ce qu’il publie est la propriété intellectuelle de quelqu’un ? Afin de mieux comprendre comment s’exerce la propriété intellectuelle sur le web, nous avons posé quelques questions à Sulliman Omarjee, juriste de propriété intellectuelle et des nouvelles technologies de l’information et de la communication.
