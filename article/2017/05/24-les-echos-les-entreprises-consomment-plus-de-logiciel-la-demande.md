---
site: Les Echos
title: "Les entreprises consomment plus de logiciel à la demande"
author: Erick Haehnsen
date: 2017-05-24
href: https://www.lesechos.fr/thema/030344851698-les-entreprises-consomment-plus-de-logiciel-a-la-demande-2089253.php
tags:
- Entreprise
- Économie
- Marchés publics
- Informatique en nuage
---

> A l'ère de la transition numérique, l'utilisation de logiciels en entreprise passe de plus en plus par le cloud. Bien que les bastions du logiciel installé sur les ordinateurs individuels ou les serveurs de l'entreprise résistent. Mais le logiciel libre et le SaaS bousculent le paysage des logiciels professionnels.
