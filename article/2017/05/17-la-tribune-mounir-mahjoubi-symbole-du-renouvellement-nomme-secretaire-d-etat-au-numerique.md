---
site: La Tribune
title: "Mounir Mahjoubi, symbole du renouvellement, nommé secrétaire d'Etat au Numérique"
author: Sylvain Rolland
date: 2017-05-17
href: http://www.latribune.fr/technos-medias/mounir-mahjoubi-symbole-du-renouvellement-nomme-secretaire-d-etat-au-numerique-715778.html
tags:
- Institutions
---

> Agé de 33 ans et symbole du renouvellement de la classe politique, Mounir Mahjoubi, ancien président du Conseil national du numérique (CNNum), a été nommé, mercredi 17 mai, secrétaire d'Etat au Numérique dans le gouvernement d'Edouard Philippe. Si son portefeuille ne comprend plus l'Innovation, rattachée à l'Enseignement supérieur et à la Recherche, il se place directement sous l'autorité directe du Premier ministre, et non plus de Bercy.
