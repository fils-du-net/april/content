---
site: infoDSI
title: "Avec Suse Academic, Suse ouvre sa base de connaissances aux universités et autres établissements scolaires"
date: 2017-05-18
href: http://www.infodsi.com/articles/168967/suse-academic-suse-ouvre-base-connaissances-universites-etablissements-scolaires.html
tags:
- Partage du savoir
- Éducation
---

> Suse s'adresse aux étudiants avec son nouveau programme de formation baptisé SUSE Academic. Au programme : le partage des logiciels open source, des cours pour Suse Linux Certified Administrator (SCA), des programmes de formation, des outils pédagogiques et des services d’assistance.
