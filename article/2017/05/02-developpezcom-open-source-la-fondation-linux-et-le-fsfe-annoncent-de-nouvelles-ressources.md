---
site: Developpez.com
title: "Open source: la fondation Linux et le FSFE annoncent de nouvelles ressources"
author: Malick Seck
date: 2017-05-02
href: https://www.developpez.com/actu/133558/Open-source-la-fondation-Linux-et-le-FSFE-annoncent-de-nouvelles-ressources-pour-une-meilleure-comprehension-des-licences
tags:
- Associations
- Licenses
---

> Aujourd'hui, force est de constater qu'il existe une multitude de licences qui régissent l'utilisation des différentes ressources mises à la disposition de la communauté, notamment comment les utilisateurs doivent les utiliser. Cela dit, il devient très facile pour les nouveaux arrivants dans le monde du libre et de l'open source de se perdre quant à la façon d'utiliser soit un document, soit un logiciel, etc. librement mis à leur disposition.
