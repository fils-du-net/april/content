---
site: Podcast Journal
title: "Réseaux alternatifs sur le web"
author: DAW
date: 2017-05-08
href: http://www.podcastjournal.net/Reseaux-alternatifs-sur-le-web_a24007.html
tags:
- Internet
- Sensibilisation
- Associations
- Vie privée
---

> Rechercher des informations sur Google, télécharger sur iTunes, communiquer sur le réseau social Facebook, tous ces logiciels largement utilisés, semblant incontournables, et détenus par les GAFAM (Google, Apple, Facebook, Amazon et Microsoft), ont des alternatives sur le web.
