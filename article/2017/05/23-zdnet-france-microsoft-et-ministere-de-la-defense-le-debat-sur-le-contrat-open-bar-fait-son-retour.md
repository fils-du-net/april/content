---
site: ZDNet France
title: "Microsoft et ministère de la Défense: le débat sur le contrat open bar fait son retour"
author: Louis Adam
date: 2017-05-23
href: http://www.zdnet.fr/actualites/microsoft-et-ministere-de-la-defense-le-debat-sur-le-contrat-open-bar-fait-son-retour-39852820.htm
tags:
- Entreprise
- April
- Institutions
- Éducation
- Marchés publics
---

> Alors que le contrat open bar liant les deux acteurs doit encore être renouvelé, les associations profitent de cette occasion pour rouvrir le débat.
