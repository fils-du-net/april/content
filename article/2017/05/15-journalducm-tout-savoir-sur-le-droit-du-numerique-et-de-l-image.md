---
site: journalducm
title: "Tout savoir sur le droit du numérique et de l’image"
author: Géraldine Gomaere
date: 2017-05-15
href: https://www.journalducm.com/2017/05/15/droit-du-numerique-et-des-images-16494
tags:
- Internet
- Institutions
- Open Data
- Vie privée
---

> Depuis de longues années, Internet demeure une zone de non-droit où chacun œuvrait un peu comme il le souhaite. Si c’est encore le cas aujourd’hui, de nombreuses mesures ont néanmoins été prises pour faire naitre un droit du numérique avec une loi qui encadre plusieurs sujets sensibles.
