---
site: ZDNet France
title: "Facebook condamné par la Cnil pour de ”nombreux manquements”"
author: Olivier Chicheportiche
date: 2017-05-17
href: http://www.zdnet.fr/actualites/facebook-condamne-par-la-cnil-pour-de-nombreux-manquements-maj-39852516.htm
tags:
- Entreprise
- Internet
- Institutions
- Europe
- Vie privée
---

> Suite à sa mise en demeure, le roi des réseaux sociaux n'a visiblement pas pris suffisamment de mesures concernant le croisement des données et écope de 150.000 euros d'amende.
