---
site: Monde qui bouge
title: "«Pair à pair»: l’essor des communs dans le champ numérique"
author: Guillaume Lohest
date: 2017-05-23
href: http://www.mondequibouge.be/index.php/2017/05/pair-a-pair-l%E2%80%99essor-des-communs-dans-le-champ-numerique
tags:
- Entreprise
- Économie
- Associations
---

> Les «communs» se définissent comme la gestion collective d’une ressource, par une communauté, selon des règles et pratiques sociales définies par celle-ci. Les communs sont le contraire de l’appropriation privée d’une ressource. On conçoit assez aisément ce fonctionnement avec une collection de semences, une parcelle de forêt, un lac ou un gisement de minerai. Mais comment cela s’applique-t-il à ce qu’on appelle les «communs de la connaissance», c’est-à-dire des ressources culturelles, des savoirs, des procédés d’invention?
