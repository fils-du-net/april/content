---
site: Ville Intelligente
title: "Présidentielle: la part du numérique chez les deux candidats en lice"
author: Yannick Sourisseau
date: 2017-05-02
href: http://www.villeintelligente-mag.fr/Presidentielle-la-part-du-numerique-chez-les-deux-candidats-en-lice_a143.html
tags:
- Institutions
---

> Renaissance Numérique, le think tank de la société numérique s’est fixé pour mission d’accompagner les acteurs publics, économiques et les citoyens dans la transition numérique de la société. Depuis le début de la présidentielle l’association dresse la vision numérique des cinq premiers candidats et notamment ceux d’Emmanuel Macron et de Marine Le Pen, qui s’affrontent pour la place de Président de la République française.
