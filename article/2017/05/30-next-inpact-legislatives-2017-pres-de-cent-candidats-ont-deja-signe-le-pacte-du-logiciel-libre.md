---
site: Next INpact
title: "Législatives 2017: près de cent candidats ont déjà signé le Pacte du logiciel libre"
author: Marc Rees
date: 2017-05-30
href: https://www.nextinpact.com/news/104414-legislatives-2017-pres-cent-candidats-ont-deja-signe-pacte-logiciel-libre.htm
tags:
- April
- Institutions
- Promotion
---

> À l’occasion des législatives pour 2017, l’association pour la promotion du libre, l’April, relance sa campagne autour du « Pacte du logiciel libre ». Une initiative qui fête cette année ses 10 ans.
