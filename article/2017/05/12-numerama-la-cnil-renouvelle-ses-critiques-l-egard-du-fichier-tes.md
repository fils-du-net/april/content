---
site: Numerama
title: "La Cnil renouvelle ses critiques à l'égard du fichier TES"
author: Julien Lausson
date: 2017-05-12
href: http://www.numerama.com/politique/257283-la-cnil-renouvelle-ses-critiques-a-legard-du-fichier-tes.html
tags:
- Institutions
- Vie privée
---

> À l'occasion de la publication d'un décret permettant de refuser la numérisation et l’enregistrement des empreintes digitales dans le fichier TES, la Cnil a publié une nouvelle délibération dans laquelle elle fait part de ses vives réserves à l'égard du fichier TES.
