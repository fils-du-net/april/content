---
site: Flottes Automobiles
title: "Aliapur: 335 000 t de pneus collectés en 2016"
author: Manon Lamoureux
date: 2017-05-09
href: https://www.flotauto.com/aliapur-collecte-2016-20170509.html
tags:
- Entreprise
- Matériel libre
- Innovation
---

> Aliapur a dépassé son record de l’année précédente, avec 1 000 à 1 200 t de pneus collectés chaque jour.
