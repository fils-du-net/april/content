---
site: Numerama
title: "Fichier TES: vous pouvez désormais refuser l'enregistrement de vos empreintes digitales"
author: Julien Lausson
date: 2017-05-10
href: http://www.numerama.com/politique/256650-fichier-tes-vous-pouvez-desormais-refuser-lenregistrement-de-vos-empreintes-digitales.html
tags:
- Institutions
- Vie privée
---

> Un décret publié au Journal officiel autorise désormais les Français et les Françaises à refuser la numérisation et l'enregistrement des empreintes digitales dans le fichier TES en cas de demande de renouvellement d'une carte d'identité.
