---
site: L'Informaticien
title: "Open Bar: peut-on encore empêcher la reconduction du contrat entre Microsoft et le ministère des Armées?"
author: Emilien Ercolani
date: 2017-05-23
href: http://www.linformaticien.com/actualites/id/44060/open-bar-peut-on-encore-empecher-la-reconduction-du-contrat-entre-microsoft-et-le-ministere-des-armees.aspx
tags:
- Entreprise
- April
- Institutions
- Marchés publics
---

> Très critiqué, le contrat entre Microsoft et le ministère des Armées (ex-Défense) doit être renouvelé pour la période 2017-2021. L’accord serait déjà scellé. L’April en appelle au président de la République.
