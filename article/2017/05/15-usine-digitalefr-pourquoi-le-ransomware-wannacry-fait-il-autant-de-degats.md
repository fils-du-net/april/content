---
site: "usine-digitale.fr"
title: "Pourquoi le ransomware WannaCry fait-il autant de dégâts?"
author: Julien Bergounhoux
date: 2017-05-15
href: http://www.usine-digitale.fr/article/pourquoi-le-ransomware-wannacry-fait-il-autant-de-degats.N540029
tags:
- Internet
---

> Une attaque informatique s'est répandue comme une trainée de poudre en fin de semaine dernière, s'attaquant à d'anciens systèmes informatiques, vulnérables car non mis à jour. Cette attaque au ransomware, a priori à simple vocation crapuleuse, a créé un vent de panique car elle a touché de nombreuses infrastructures industrielles dans plus de 100 pays dans le monde. Et pourtant cela aurait pu être bien pire. Tout laisse même à penser (hélas) que ce n'est qu'un début. Décryptage des tenants et aboutissants de la situation.
