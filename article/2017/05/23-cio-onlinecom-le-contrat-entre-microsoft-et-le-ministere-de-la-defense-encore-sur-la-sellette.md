---
site: "cio-online.com"
title: "Le contrat entre Microsoft et le Ministère de la Défense encore sur la sellette"
author: Bertrand Lemaire
date: 2017-05-23
href: http://www.cio-online.com/actualites/lire-le-contrat-entre-microsoft-et-le-ministere-de-la-defense-encore-sur-la-sellette-9399.html
tags:
- Entreprise
- April
- Institutions
- Marchés publics
---

> L'APRIL appelle à annuler l'Open Bar Microsoft au Ministère de la Défense (devenu Ministère des Armées), contrat très critiqué sur le plan juridique.
