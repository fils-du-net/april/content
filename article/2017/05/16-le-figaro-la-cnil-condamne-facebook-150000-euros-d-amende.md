---
site: Le Figaro
title: "La Cnil condamne Facebook à 150.000 euros d'amende"
author: Benjamin Ferran
date: 2017-05-16
href: http://www.lefigaro.fr/secteur/high-tech/2017/05/16/32001-20170516ARTFIG00118-la-cnil-condamne-facebook-a-150000-euros-d-amende.php
tags:
- Entreprise
- Internet
- Institutions
- Europe
- Vie privée
---

> L'autorité française a relevé de «nombreux manquements» à la loi Informatique et Libertés, notamment à des fins publicitaires.
