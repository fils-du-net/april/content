---
site: Developpez.com
title: "GNU GPL, une licence de logiciels libres largement utilisée, peut-elle faire office de contrat juridique?"
author: Stéphane le calme
date: 2017-05-14
href: https://www.developpez.com/actu/136533/GNU-GPL-une-licence-de-logiciels-libres-largement-utilisee-peut-elle-faire-office-de-contrat-juridique-Un-juge-estime-que-oui
tags:
- Entreprise
- Institutions
- Droit d'auteur
- Licenses
- Informatique en nuage
---

> Nombreux sont les projets open source qui sont couverts par la licence GNU GPL, qui est conçue pour garantir que le code logiciel reste libre afin de pouvoir être distribué gratuitement et pouvoir être utilisé par n'importe qui, à condition que l’entité respecte la licence. Une affaire impliquant l’utilisation de cette licence a été portée à la connaissance du juge Jacqueline Scott Corley du district de Californie.
