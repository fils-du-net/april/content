---
site: Le Monde Informatique
title: "Les chercheurs du libre et de l'open source ont rendez-vous à OSIS 2017"
author: Véronique Arène
date: 2017-05-12
href: http://www.lemondeinformatique.fr/actualites/lire-les-chercheurs-du-libre-et-de-l-open-source-ont-rendez-vous-a-osis-2017-68190.html
tags:
- Partage du savoir
- Innovation
- Promotion
- Sciences
---

> Du 11 mai au 26 juin 2017 à Paris se tiendra le printemps de l'innovation open source (OSIS), l'un des grands rendez-vous de la communauté scientifique du logiciel libre et de l'open source d'Ile-de-France. Lancé par Systematic Paris-Region, cet événement regroupe conférences et travaux en IoT, sécurité, blockchain, cloud, ou encore big data.
