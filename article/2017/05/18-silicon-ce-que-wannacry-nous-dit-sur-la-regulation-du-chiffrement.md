---
site: Silicon
title: "Ce que WannaCry nous dit sur la régulation du chiffrement"
author: Reynald Fléchaux
date: 2017-05-18
href: http://www.silicon.fr/wannacry-regulation-chiffrement-175267.html
tags:
- Entreprise
- Internet
- Institutions
- Europe
---

> Pour Tristan Nitot, la crise WannaCry montre que des gouvernements ne devraient pas détenir des secrets susceptibles de mettre en péril la sécurité globale des systèmes. A méditer alors que se profilent une régulation européenne sur le chiffrement.
