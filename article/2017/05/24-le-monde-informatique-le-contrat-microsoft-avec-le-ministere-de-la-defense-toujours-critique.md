---
site: Le Monde Informatique
title: "Le contrat Microsoft avec le ministère de la Défense toujours critiqué"
author: Bertrand Lemaire
date: 2017-05-24
href: http://www.lemondeinformatique.fr/actualites/lire-le-contrat-microsoft-avec-le-ministere-de-la-defense-toujours-critique-68308.html
tags:
- Entreprise
- Logiciels privateurs
- April
- Institutions
- Marchés publics
---

> L'accord signé entre Microsoft et le ministère de la Défense visant à forfaitiser ses contrats logiciels, baptisé Open Bar, reste sous le feu des critiques. L'April appelle à son annulation pour respecter la démarche normale des marchés publics.
