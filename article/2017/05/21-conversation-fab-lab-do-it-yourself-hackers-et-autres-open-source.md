---
site: The Conversation
title: "Fab lab: do it yourself, hackers et autres open source"
author: Clément Duhart
date: 2017-05-21
href: http://theconversation.com/fab-lab-do-it-yourself-hackers-et-autres-open-source-76881
tags:
- Économie
- Éducation
- Innovation
---

> Il serait réducteur de voir dans les fab lab une simple évolution 2.0 du garage californien des années 70. Car il s’agit avant tout de nouveaux espaces pour l’industrie de demain.
