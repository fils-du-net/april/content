---
site: La Tribune
title: "Numérique: les grands chantiers du président Macron"
author: Sylvain Rolland
date: 2017-05-17
href: http://www.latribune.fr/technos-medias/numerique-les-grands-chantiers-du-president-macron-714082.html
tags:
- Institutions
---

> L'ancien ministre de l'Économie s'est démené à Bercy en faveur du numérique, de l'innovation et des startups. Sera-t-il le président de la transformation digitale?
