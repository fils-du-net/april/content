---
site: Numerama
title: "Des informaticiens découvrent comment Volkswagen a trompé les tests environnementaux"
author: Antoine Boudet
date: 2017-05-26
href: http://www.numerama.com/tech/261700-des-informaticiens-decouvrent-comment-volkswagen-a-trompe-les-tests-environnementaux.html
tags:
- Entreprise
- Logiciels privateurs
- Partage du savoir
- Associations
---

> Volkswagen avait vu son image ternie par cette affaire de tricherie aux tests d'émission de CO2 de ses véhicules. Aujourd'hui, une équipe de chercheurs en informatique a trouvé le code qui a justement servi à truquer ces fameuses évaluations environnementales.
