---
site: ZDNet France
title: "Le 'Do Not Track' perd un allié majeur, et la vie privée une bataille de plus"
author: Steven J. Vaughan-Nichols
date: 2017-05-19
href: http://www.zdnet.fr/actualites/le-do-not-track-perd-un-allie-majeur-et-la-vie-privee-une-bataille-de-plus-39852702.htm
tags:
- Entreprise
- Internet
- Standards
- Vie privée
---

> Pensé comme un standard permettant aux internautes d'exprimer leur refus du suivi publicitaire, le 'Do Not Track' est abandonné par Twitter, un des premiers services en ligne à l'avoir déployé. Détricoté par les publicitaires, l'échec du DNT ne réconciliera pas les internautes avec la pub. Au contraire.
