---
site: Silicon
title: "Un expert en sécurité français trouve une parade à WannaCry… sous XP"
author: David Feugey
date: 2017-05-19
href: http://www.silicon.fr/parade-wannacry-xp-175311.html
tags:
- Logiciels privateurs
- Innovation
---

> WannaCry a été déjoué, mais uniquement sous Windows XP. Une faiblesse dans la Crypto API de Windows permet de trouver la clé de chiffrement.
