---
site: Silicon
title: "Plus de 1000 bugs Open Source révélés par l'initiative Google OSS-Fuzz"
author: Reynald Fléchaux
date: 2017-05-10
href: http://www.silicon.fr/open-source-oss-fuzz-google-paye-developpeurs-174263.html
tags:
- Entreprise
---

> Le programme OSS-Fuzz, qui vise à améliorer la sécurité et la stabilité de l’Open Source via des tests automatisés, a déjà débusqué plus de 1 000 défauts. Google veut accélérer en payant les développeurs pour identifier bugs et failles au sein de leurs projets.
