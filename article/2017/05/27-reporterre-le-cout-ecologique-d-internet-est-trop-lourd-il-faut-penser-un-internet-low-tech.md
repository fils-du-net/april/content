---
site: Reporterre
title: "Le coût écologique d’internet est trop lourd, il faut penser un internet low-tech"
author: Félix Tréguer et Gaël Trouvé
date: 2017-05-27
href: https://reporterre.net/Le-cout-ecologique-d-internet-est-trop-lourd-il-faut-penser-un-internet-low
tags:
- Internet
- Économie
- Associations
---

> Le combat pour une informatique émancipatrice échoue le plus souvent à expliquer les effroyables coûts écologiques et humains du numérique, expliquent les auteurs de cette tribune. Qui proposent des pistes pour un internet low-tech afin de nous émanciper des sphères technocratiques et industrielles.
