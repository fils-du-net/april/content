---
site: Acteurs publics
title: "Mathieu Jeandron: “Nous souhaitons privilégier l’hébergement en France des données scolaires”"
author: Soazig Le Nevé
date: 2017-05-18
href: https://www.acteurspublics.com/2017/05/18/mathieu-jeandron-nous-souhaitons-privilegier-l-hebergement-en-france-des-donnees-scolaires
tags:
- Entreprise
- Administration
- Institutions
- Éducation
- Marchés publics
---

> Rien ne s’oppose à ce que les enseignants recourent à des services numériques produits par Google ou Microsoft. C’est le message adressé par le directeur du numérique éducatif (DNE) du ministère de l’Éducation nationale, Mathieu Jeandron, dans un courrier aux services académiques qui a mis en émoi une partie de la communauté éducative et des cadres du ministère.
