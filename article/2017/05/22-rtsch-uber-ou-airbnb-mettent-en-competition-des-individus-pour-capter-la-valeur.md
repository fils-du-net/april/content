---
site: rts.ch
title: "”Uber ou Airbnb mettent en compétition des individus pour capter la valeur”"
author: dk
date: 2017-05-22
href: https://www.rts.ch/info/economie/8641100--uber-ou-airbnb-mettent-en-competition-des-individus-pour-capter-la-valeur-.html
tags:
- Entreprise
- Économie
---

> Uber et Airbnb ”mettent en compétition des individus” et ”captent la valeur”, dénonce le penseur belge Michael Bauwens. Pour lui, la ”véritable” économie collaborative, dont il est un théoricien, permet de lutter contre la désindustralisation.
