---
site: Le Figaro
title: "La cyberattaque a été stoppée, mais la menace n'est pas écartée"
author: Marie Simon
date: 2017-05-15
href: http://www.lefigaro.fr/secteur/high-tech/2017/05/15/32001-20170515ARTFIG00074-la-cyberattaque-a-ete-stoppee-mais-la-menace-n-est-pas-ecartee.php
tags:
- Internet
- Logiciels privateurs
---

> Les experts redoutent une recrudescence du virus ce lundi lorsque des millions d'ordinateurs sont rallumés, et l'apparition de nouvelles versions, plus résistantes.
