---
site: sputnik
title: "Parti pirate: la culture du secret, terreau idéal pour les cyberattaques"
date: 2017-05-16
href: https://fr.sputniknews.com/international/201705161031397456-parti-pirate-interview-wanatabe-vermorel
tags:
- Internet
- Institutions
---

> «Quand on cultive la culture du secret, on se met justement en position de faiblesse par rapport à ceux qui sont capables de découvrir ces secrets», a déclaré à Sputnik le porte-parole du Parti pirate français, qui prône la transparence et l'ouverture des données publiques.
