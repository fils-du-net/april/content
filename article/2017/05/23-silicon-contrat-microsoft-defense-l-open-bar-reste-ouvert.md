---
site: Silicon
title: "Contrat Microsoft – Défense: l’Open Bar reste ouvert"
author: Reynald Fléchaux
date: 2017-05-23
href: http://www.silicon.fr/contrat-microsoft-defense-open-bar-ouvert-175579.html
tags:
- Entreprise
- April
- Institutions
- Marchés publics
---

> Le contrat-cadre entre le ministère de la Défense, pardon des Armées, et Microsoft doit être renouvelé dans les jours prochains. Pour 120 M€. Et toujours sans mise en concurrence.
