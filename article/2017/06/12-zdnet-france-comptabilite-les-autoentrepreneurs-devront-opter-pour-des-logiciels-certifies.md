---
site: ZDNet France
title: "Comptabilité: Les autoentrepreneurs devront opter pour des logiciels certifiés"
date: 2017-06-12
href: http://www.zdnet.fr/actualites/comptabilite-les-autoentrepreneurs-devront-opter-pour-des-logiciels-certifies-39853532.htm
tags:
- Entreprise
- Administration
- Économie
---

> La nouvelle loi de Finances votée en 2016 prévoit une disposition nouvelle pour les autoentrepreneurs : afin d’éviter la fraude àa la TVA, ceux-ci seront tenus d’utiliser des logiciels répondant aux critères de l’administration. Même s'ils ne sont pas assujettis à cette taxe.
