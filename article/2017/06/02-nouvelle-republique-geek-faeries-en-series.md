---
site: Nouvelle République
title: "Geek Faëries en séries"
author: Laurence Texier
date: 2017-06-02
href: http://www.lanouvellerepublique.fr/France-Monde/Loisirs/Fetes-festivals/n/Contenus/Articles/2017/06/02/Geek-Faeries-en-series-3119668
tags:
- Sensibilisation
- Associations
---

> Le temps d’un week-end, Selles-sur-Cher (Loir-et-Cher) va basculer dans une sorte de monde parallèle, au gré des jeux de rôle et autres parties de quidditch.
