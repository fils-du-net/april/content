---
site: La Tribune
title: "Législatives: une Assemblée plus sensible aux enjeux de la nouvelle économie?"
author: Sylvain Rolland
date: 2017-06-20
href: http://www.latribune.fr/technos-medias/legislatives-une-assemblee-plus-sensible-aux-enjeux-de-la-nouvelle-economie-740739.html
tags:
- Institutions
---

> Malgré les échecs de certains des députés les plus actifs sur le numérique sous François Hollande (Axelle Lemaire, Lionel Tardy, Luc Belot, NKM...), le renouvellement générationnel et la présence de davantage d’élus issus de la société civile laissent espérer une Assemblée beaucoup plus en phase avec la révolution numérique.
