---
site: Numerama
title: "Droit d'auteur: l'incertitude règne en Europe sur le devenir de la directive Copyright"
author: Julien Lausson
date: 2017-06-06
href: http://www.numerama.com/politique/264303-droit-dauteur-lincertitude-regne-en-europe-sur-le-devenir-de-la-directive-copyright.html
tags:
- Internet
- Institutions
- Associations
- Droit d'auteur
- Europe
---

> La Commission européenne a présenté en septembre une directive sur le droit d'auteur dont certaines dispositions sont vivement critiquées. Alors que des commissions spécialisées au Parlement européen ont proposé d'arrondir les angles, le plus grand flou règne sur la position des eurodéputés.
