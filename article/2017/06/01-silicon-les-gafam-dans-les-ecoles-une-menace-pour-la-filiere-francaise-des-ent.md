---
site: Silicon
title: "Les Gafam dans les écoles: une menace pour la filière française des ENT?"
author: Reynald Fléchaux
date: 2017-06-01
href: http://www.silicon.fr/gafam-ecoles-menace-filiere-francaise-ent-176451.html
tags:
- Entreprise
- Éducation
- RGI
---

> L’Education nationale ouvre ses portes aux solutions des Apple, Google et autre Microsoft. Une volte-face qui fait bondir trois patrons d’éditeurs spécialisés, les ENT, qui critiquent une forme de deux poids, deux mesures.
