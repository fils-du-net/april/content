---
site: La Vie
title: "À Caen, un collectif pour lutter contre la fracture numérique"
author: Laurent Grzybowski
date: 2017-06-07
href: http://www.lavie.fr/solidarite/carnets-citoyens/a-caen-un-collectif-pour-lutter-contre-la-fracture-numerique-07-06-2017-82654_459.php
tags:
- Interopérabilité
- Partage du savoir
- Associations
---

> Mettre l’informatique au service des citoyens, quels que soient leurs moyens, leurs connaissances et leurs besoins. C’est l’objectif d’Artifaille, un collectif d’entraide qui propose aussi des supports informatiques libres et transparents.
