---
site: francetv info
title: "Alexandre Zapolsky, candidat En Marche! à Hyères et jugé en correctionnelle à Toulouse"
author: Fabrice Valery
date: 2017-06-14
href: http://france3-regions.francetvinfo.fr/occitanie/haute-garonne/toulouse/alexandre-zapolsky-candidat-marche-hyeres-juge-correctionnelle-toulouse-1275943.html
tags:
- Entreprise
- Institutions
---

> Le candidat dans la 3ème circonscription du Var comparaît mardi 20 juin, 48 heures après le second tour, dans une affaire d'injures publiques et de diffamation contre une société toulousaine. L'entreprise qu'il dirige vient aussi d'être condamnée en appel pour harcèlement moral.
