---
site: 20minutes.fr
title: "Ces makers montrent la voie aux esprits créatifs"
date: 2017-06-09
href: http://www.20minutes.fr/magazine/economie-collaborative/collaboratif-pratique/ces-makers-montrent-la-voie-aux-esprits-creatifs-381583
tags:
- Partage du savoir
- Matériel libre
- Innovation
---

> «On nous a appris à être passifs, alors qu’on est tous capables de créer», lance, dans un élan d’enthousiasme, Hortense Sauvard. La fondatrice de la plateforme Oui Are Makers (ouiaremakers.com) sera présente ce week-end à la Cité des sciences de Paris à l’occasion de la Maker Faire, qui a accueilli 65.000 visiteurs l’année dernière.
