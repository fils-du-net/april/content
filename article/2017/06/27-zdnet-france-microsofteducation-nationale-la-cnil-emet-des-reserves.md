---
site: ZDNet France
title: "Microsoft/Education Nationale: la CNIL émet des réserves"
author: Louis Adam
date: 2017-06-27
href: http://www.zdnet.fr/actualites/microsoft-education-nationale-la-cnil-emet-des-reserves-39854192.htm
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Éducation
---

> Dans une lettre obtenue et publiée aujourd’hui par NextInpact, la CNIL revient sur le projet de charte en cours de préparation entre l’Education nationale et Microsoft. La Commission y pointe plusieurs manques en matière de protection des données personnelles.
