---
site: Radio Prague
title: "La disparition de la vie privée n’est pas inévitable"
author: Lucie Drechselová
date: 2017-06-20
href: http://www.radio.cz/fr/rubrique/panorama/la-disparition-de-la-vie-privee-nest-pas-inevitable
tags:
- Internet
- Promotion
- Vie privée
---

> Pour la publicité ciblée comme pour les campagnes électorales, les informations concernant notre activité en ligne sont très prisées sur le marché des données personnelles. Pour beaucoup, la sécurité de l’information et la protection des données sur internet semblent certes être des sujets importants mais souvent trop techniques voire ennuyeux. Mais devons-nous nous résilier à laisser nos données personnelles à la portée de n’importe qui sur la toile? Informaticien français basé à Prague, Jerôme Poisson apporte une réponse: selon lui, la perte de la vie privée n’est pas inévitable.
