---
site: Developpez.com
title: "La NSA rend disponibles via Open Source Software certains des outils qu'elle a développés en interne"
author: Stéphane le calme
date: 2017-06-19
href: https://www.developpez.com/actu/144942/La-NSA-rend-disponibles-via-Open-Source-Software-certains-des-outils-qu-elle-a-developpes-en-interne-dans-le-cadre-de-son-programme-TTP
tags:
- Institutions
- Innovation
---

> La NSA a fourni une liste d’outils qu’elle a développés et qui sont désormais accessibles au public via Open Source Software (OSS) dans le cadre de son TTP (Technology Transfer Program). Pour rappel, c’est dans le cadre de son programme TTP que la NSA transfère la technologie qu’elle a développée en interne à l'industrie, au milieu universitaire et à d'autres organismes de recherche, des transferts qui vont «profiter à l'économie et à la mission de l'Agence».
