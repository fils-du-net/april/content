---
site: Industrie et Technologies
title: "Alexandre Zapolsky: un entrepreneur du logiciel libre en tête des législatives dans le Var"
author: Philippe Passebon
date: 2017-06-14
href: https://www.industrie-techno.com/alexandre-zapolsky-un-entrepreneur-du-logiciel-libre-en-tete-des-legislatives-dans-le-var.50182
tags:
- Entreprise
- Institutions
- Promotion
---

> Dans la 3e circonscription du Var, un promoteur invétéré du logiciel libre, vierge de tout mandat politique, est en passe de gagner son ticket à l'assemblée. Portait de Alexandre Zapolsky, dirigeant de Linagora, un champion des logiciels open source.
