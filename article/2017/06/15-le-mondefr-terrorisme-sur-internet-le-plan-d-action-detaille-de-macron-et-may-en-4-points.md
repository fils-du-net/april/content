---
site: Le Monde.fr
title: "Terrorisme sur Internet: le plan d’action détaillé de Macron et May en 4 points"
author: Damien Leloup
date: 2017-06-15
href: http://www.lemonde.fr/pixels/article/2017/06/15/emmanuel-macron-et-theresa-may-detaillent-leur-plan-d-action-contre-le-terrorisme-sur-internet_5145124_4408996.html
tags:
- Internet
- Institutions
- International
- Vie privée
---

> Modération automatique, stockage des données, chiffrement, traitement des comptes parodiques: les deux gouvernements ont détaillé leurs propositions mercredi 14 juin.
