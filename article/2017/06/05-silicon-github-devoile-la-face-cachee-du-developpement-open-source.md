---
site: Silicon
title: "GitHub dévoile la face cachée du développement Open Source"
author: Jacques Cheminat
date: 2017-06-05
href: http://www.silicon.fr/github-face-cachee-developpement-open-source-176919.html
tags:
- Sensibilisation
---

> GitHub a mené un sondage. La pauvreté de la documentation, des relations musclées et la faible mixité sont pointés du doigt.
