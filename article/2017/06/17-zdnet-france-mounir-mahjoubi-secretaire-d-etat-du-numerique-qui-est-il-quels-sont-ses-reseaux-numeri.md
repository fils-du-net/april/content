---
site: ZDNet France
title: "Mounir Mahjoubi, secrétaire d’État du Numérique: qui est-il, quels sont ses réseaux (numériques)?"
author: Guillaume Serries
date: 2017-06-17
href: http://www.zdnet.fr/actualites/mounir-mahjoubi-secretaire-d-tat-du-numerique-qui-est-il-quels-sont-ses-reseaux-numeriques-39852600.htm
tags:
- Institutions
---

> Il apostrophait publiquement le patron de Google sur la question fiscale quand il était président du CNNum. Mounir Mahjoubi, 33 ans, aura t-il la même fougue comme membre du gouvernement? Biographie rapide (mais concise) et liste des dossiers qui encombrent déjà son bureau.
