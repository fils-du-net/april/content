---
site: Nouvelle République
title: "Les \"Geek Faëries\" reviennent en force"
author: Laurence Texier
date: 2017-06-04
href: http://www.lanouvellerepublique.fr/Loir-et-Cher/Loisirs/Fetes-festivals/n/Contenus/Articles/2017/06/04/Les-Geek-Faeries-reviennent-en-force-3122061
tags:
- Sensibilisation
- Associations
---

> Malgré la pluie, les geeks ont afflué au château de Selles-sur-Cher. Le festival enfile ses plus beaux costumes pour une dernière partie ce dimanche.
