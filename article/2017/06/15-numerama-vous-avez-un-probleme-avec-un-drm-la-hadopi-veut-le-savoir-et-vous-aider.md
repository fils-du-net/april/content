---
site: Numerama
title: "Vous avez un problème avec un DRM? La Hadopi veut le savoir et vous aider"
author: Julien Lausson
date: 2017-06-15
href: http://www.numerama.com/politique/267281-vous-avez-un-probleme-avec-un-drm-la-hadopi-veut-le-savoir-et-vous-aider.html
tags:
- Internet
- Interopérabilité
- HADOPI
- DRM
- Droit d'auteur
---

> La Hadopi lance un service qui permet aux internautes de lui signaler un problème avec les DRM, ces mesures techniques de protection fixées aux œuvres au nom de la lutte contre le piratage.
