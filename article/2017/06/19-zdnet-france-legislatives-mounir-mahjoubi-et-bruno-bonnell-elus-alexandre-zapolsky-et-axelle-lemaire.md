---
site: ZDNet France
title: "Législatives: Mounir Mahjoubi et Bruno Bonnell élus, Alexandre Zapolsky et Axelle Lemaire battus"
author: Thierry Noisette
date: 2017-06-19
href: http://www.zdnet.fr/blogs/l-esprit-libre/legislatives-mounir-mahjoubi-et-bruno-bonnell-elus-alexandre-zapolsky-et-axelle-lemaire-battus-39853882.htm
tags:
- Institutions
---

> L'actuel secrétaire d’État au Numérique est élu, l'ancienne perd, Bruno Bonnell bat l'ex-ministre Najat Vallaud-Belkacem et le patron de Linagora échoue dans le Var.
