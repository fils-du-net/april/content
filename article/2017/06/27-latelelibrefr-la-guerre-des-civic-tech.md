---
site: LaTeleLibre.fr
title: "La Guerre des Civic Tech"
date: 2017-06-27
href: http://latelelibre.fr/reportages/guerre-civic-tech
tags:
- Internet
- April
- Institutions
- Innovation
---

> S’impliquer, voter, cliquer. Voilà la recette de la démocratie version 2.0. Simon est parti à la rencontre des Civic-tech, ces acteurs qui permettent à cette démocratie d’éclore. Mais, entre start-up, associations, partisans du logiciel libre et défenseurs du fermé, le monde des Civic Tech ne rime pas forcément avec participation citoyenne.
