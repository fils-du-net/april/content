---
site: ITespresso
title: "GitHub dresse un portrait des contributeurs open source"
author: Renald Boulestin
date: 2017-06-07
href: http://www.itespresso.fr/open-source-survey-github-160636.html?inf_by=580b8b482ad0a1eb557da66e
tags:
- Sensibilisation
---

> Github vient de publier les résultats de l’Open Source Survey, un sondage visant à recueillir des données sur la communauté du logiciel open source.
