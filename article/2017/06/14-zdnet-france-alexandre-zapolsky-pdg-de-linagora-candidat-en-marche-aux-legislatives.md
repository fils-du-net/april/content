---
site: ZDNet France
title: "Alexandre Zapolsky, PDG de Linagora, candidat En Marche aux législatives"
author: Thierry Noisette
date: 2017-06-14
href: http://www.zdnet.fr/blogs/l-esprit-libre/alexandre-zapolsky-pdg-de-linagora-candidat-en-marche-aux-legislatives-39852430.htm
tags:
- Entreprise
- Institutions
---

> Le fondateur de l'éditeur open source Linagora se présente aux élections législatives dans le Var sous l'étiquette du parti d'Emmanuel Macron.
