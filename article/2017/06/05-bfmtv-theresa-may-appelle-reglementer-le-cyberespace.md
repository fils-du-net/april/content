---
site: BFMtv
title: "Theresa May appelle à «réglementer le cyberespace»"
author: Olivier Laffargue
date: 2017-06-05
href: http://bfmbusiness.bfmtv.com/hightech/theresa-may-appelle-a-reglementer-le-cyberespace-1179811.html
tags:
- Internet
- Institutions
- International
- Vie privée
---

> La Première ministre britannique s'en est pris dimanche aux géants du Net, accusés de fournir aux terroristes des plateformes où ils peuvent librement propager leurs idéologies extrémistes. Elle appelle les gouvernements à travailler ensemble afin d'empêcher la diffusion de ces idées sur Internet.
