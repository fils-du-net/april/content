---
site: Numerama
title: "Une partie du code source de Windows 10 a été publiée sur le web"
author: Cassim Ketfi
date: 2017-06-24
href: http://www.numerama.com/tech/270247-une-partie-du-code-source-de-windows-10-a-ete-publiee-sur-le-web.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
---

> Windows est sans doute le logiciel au code source fermé le plus connu. Ce week-end, une partie du code source de Windows 10 a pourtant été publié sur le web.
