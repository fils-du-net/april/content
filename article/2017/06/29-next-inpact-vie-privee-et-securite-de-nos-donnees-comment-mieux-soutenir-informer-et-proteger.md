---
site: Next INpact
title: "Vie privée et sécurité de nos données: comment mieux soutenir, informer et protéger?"
author: David Legrand
date: 2017-06-29
href: https://www.nextinpact.com/news/104659-edito-vie-privee-et-securite-nos-donnees-comment-mieux-soutenir-informer-et-proteger.htm
tags:
- Internet
- Économie
- Associations
- Vie privée
---

> De nombreux outils qui assurent le fonctionnement du web et la sécurité de nos échanges, ou même la protection de notre vie privée, dépendent d'un mode de financement assez particulier: le don. Mais voilà, les efforts en la matière sont assez éclatés, ce qui mène parfois à une efficacité limitée. Et si l'on pensait les choses autrement?
