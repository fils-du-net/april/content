---
site: Numerama
title: "La NSA crée un compte sur Github et encourage les citoyens à améliorer ses projets"
author: Nelly Lesage
date: 2017-06-19
href: http://www.numerama.com/tech/268350-la-nsa-cree-un-compte-sur-github-et-encourage-les-citoyens-a-ameliorer-ses-projets.html
tags:
- Internet
- Partage du savoir
- Institutions
- Innovation
---

> La NSA vient de lancer officiellement son compte sur Github. En investissant la plateforme de développement de logiciels en open source, l'agence gouvernementale américaine souhaite encourager la coopération entre les citoyens et le gouvernement.
