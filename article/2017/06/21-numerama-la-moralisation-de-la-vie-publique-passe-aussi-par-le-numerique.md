---
site: Numerama
title: "La moralisation de la vie publique passe aussi par le numérique"
author: Julien Lausson
date: 2017-06-21
href: http://www.numerama.com/politique/268996-la-moralisation-de-la-vie-publique-passe-par-le-numerique-juge-le-cnnum.html
tags:
- Institutions
- Promotion
---

> Le Conseil national du numérique considère que la confiance dans la vie publique ne passe pas que par la moralisation de la politique. Elle a aussi besoin du numérique. L'instance entend donc apporter sa pierre à l'édifice.
