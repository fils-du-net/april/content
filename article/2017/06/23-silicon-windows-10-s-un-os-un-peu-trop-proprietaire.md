---
site: Silicon
title: "Windows 10 S, un OS un peu trop propriétaire"
author: Jacques Cheminat
date: 2017-06-23
href: http://www.silicon.fr/windows-10-s-un-os-un-peu-trop-proprietaire-178663.html
tags:
- Entreprise
- Logiciels privateurs
- Informatique en nuage
---

> A peine lancé, Windows 10 S se révèle très restrictif au point de rendre impossible l'accueil de logiciels tiers comme les antivirus.
