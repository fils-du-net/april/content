---
site: Numerama
title: "Contrairement à Macron, des élus européens plaident pour un chiffrement fort"
author: Julien Lausson
date: 2017-06-19
href: http://www.numerama.com/politique/268319-contrairement-a-macron-des-elus-europeens-plaident-pour-un-chiffrement-fort.html
tags:
- Internet
- Institutions
- Europe
- Vie privée
---

> Pendant que Theresa May et Emmanuel Macron poussent pour accéder d'une façon ou d'une autre aux contenus chiffrés, une commission du Parlement européen plaide au contraire pour un chiffrement fort. Et sans backdoor.
