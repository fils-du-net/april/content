---
site: ZDNet France
title: "Contenus illégaux: France et UK plaident pour une responsabilité légale des GAFA"
author: Louis Adam
date: 2017-06-14
href: http://www.zdnet.fr/actualites/contenus-illegaux-france-et-uk-plaident-pour-une-responsabilite-legale-des-gafa-39853608.htm
tags:
- Internet
- Institutions
- International
- Vie privée
---

> Le président de la République Emmanuel Macron a rencontré la 1ere ministre britannique Theresa May. Au programme des négociations, la question du terrorisme et comme bien souvent dans ces débats, celles de la régulation d’internet.
