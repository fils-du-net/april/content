---
site: ZDNet France
title: "Législatives: Mounir Mahjoubi, Alexandre Zapolsky, Bruno Bonnell, Parti pirate"
author: Thierry Noisette
date: 2017-06-12
href: http://www.zdnet.fr/blogs/l-esprit-libre/legislatives-mounir-mahjoubi-alexandre-zapolsky-bruno-bonnell-parti-pirate-39853554.htm
tags:
- Institutions
---

> Le tsunami du parti d'Emmanuel Macron a profité à ses candidats du numérique: l'ex-président du CNNum, le patron de Linagora et le dirigeant de Robopolis Bruno Bonnell sont en ballotages favorables. Le Parti pirate obtient quant à lui des résultats très faibles.
