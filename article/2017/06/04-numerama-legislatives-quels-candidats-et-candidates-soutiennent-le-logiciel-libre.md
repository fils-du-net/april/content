---
site: Numerama
title: "Législatives: quels candidats et candidates soutiennent le logiciel libre?"
author: Julien Lausson
date: 2017-06-04
href: http://www.numerama.com/politique/262991-le-pacte-du-logiciel-libre-sinvite-dans-les-elections-legislatives-de-2017.html
tags:
- April
- Institutions
- Promotion
---

> Depuis dix ans, une association s'efforce de sensibiliser les candidats et les candidates sollicitant le suffrage de la population française aux bienfaits du logiciel libre. Pour les élections législatives de 2017, elle remet le couvert.
