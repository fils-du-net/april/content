---
site: Numerama
title: "L'Assemblée nationale compte désormais 26 défenseurs du logiciel libre"
author: Julien Lausson
date: 2017-06-19
href: http://www.numerama.com/politique/268448-lassemblee-nationale-compte-desormais-26-defenseurs-du-logiciel-libre.html
tags:
- April
- Institutions
- Promotion
---

> L'association de promotion et de défense du logiciel libre a fait ses comptes: sur les 497 candidats et candidates en lice pour les législatives qui ont signé le pacte du logiciel libre, seule une toute petite portion a franchi le second tour avec succès.
