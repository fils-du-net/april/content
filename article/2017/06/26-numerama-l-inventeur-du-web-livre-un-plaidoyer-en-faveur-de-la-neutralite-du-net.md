---
site: Numerama
title: "L’inventeur du web livre un plaidoyer en faveur de la neutralité du net"
author: Julien Lausson
date: 2017-06-26
href: http://www.numerama.com/politique/270519-linventeur-du-web-livre-un-plaidoyer-en-faveur-de-la-neutralite-du-net.html
tags:
- Internet
- Neutralité du Net
---

> Tim Berners-Lee, le principal inventeur du web, signe une tribune pour appeler à la défense de la neutralité du net, dont le principe est gravement remis en cause aux États-Unis.
