---
site: L'Express.fr
title: "Wikipédia déboule à l'Assemblée pour tirer le portrait de nouveaux députés"
author: Geoffrey Bonnefoy
date: 2017-06-22
href: http://www.lexpress.fr/actualite/politique/assemblees/wikipedia-deboule-a-l-assemblee-pour-tirer-le-portrait-de-nouveaux-deputes_1920541.html
tags:
- Internet
- Partage du savoir
- Institutions
- Associations
---

> En cette semaine de rentrée des députés, Antoine mitraille les élus pour alimenter la photothèque de la célèbre encyclopédie en ligne Wikipédia.
