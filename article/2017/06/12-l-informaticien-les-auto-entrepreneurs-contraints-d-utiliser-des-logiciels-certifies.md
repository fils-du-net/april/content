---
site: L'Informaticien
title: "Les auto-entrepreneurs contraints d’utiliser des logiciels certifiés"
author: Guillaume Périssat
date: 2017-06-12
href: http://www.linformaticien.com/actualites/id/44219/les-auto-entrepreneurs-contraints-d-utiliser-des-logiciels-certifies.aspx
tags:
- Entreprise
- Administration
- Économie
- Informatique en nuage
---

> Les auto-entrepreneurs seront le 1er janvier prochain obligés de se doter de logiciels certifiés contre la fraude à la TVA. Peu importe qu’ils y soient assujettis ou non. Comme si les obligations comptables et fiscales n’étaient pas déjà assez prise-de-tête, s’y ajoute une nouvelle contrainte. Y faire défaut peut coûter 7500 euros.
