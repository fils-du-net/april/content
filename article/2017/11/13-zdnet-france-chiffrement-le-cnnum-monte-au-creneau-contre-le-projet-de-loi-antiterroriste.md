---
site: ZDNet France
title: "Chiffrement: le CNNum monte au créneau contre le projet de loi antiterroriste"
author: Guillaume Serries
date: 2017-11-13
href: http://www.zdnet.fr/actualites/chiffrement-le-cnnum-monte-au-creneau-contre-le-projet-de-loi-antiterroriste-39857234.htm
tags:
- Internet
- Institutions
- Vie privée
---

> Un avis du Conseil critique "la spirale infernale" dans laquelle semble engagés les pouvoirs publics dans leur lutte contre le chiffrement. "Il n'existe pas de technique d'affaiblissement systémique du chiffrement qui ne permettrait de viser que les activités criminelles" assure le CNNum.
