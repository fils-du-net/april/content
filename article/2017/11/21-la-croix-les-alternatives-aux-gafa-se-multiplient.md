---
site: la Croix
title: "Les alternatives aux Gafa se multiplient"
author: Xavier Renard
date: 2017-11-21
href: https://www.la-croix.com/Economie/Monde/alternatives-Gafa-multiplient-2017-11-21-1200893551
tags:
- Entreprise
- Internet
- Économie
- Vie privée
---

> Quelques pourfendeurs des géants d’Internet tentent de construire un écosystème alternatif, mais ils sont encore peu connus.
