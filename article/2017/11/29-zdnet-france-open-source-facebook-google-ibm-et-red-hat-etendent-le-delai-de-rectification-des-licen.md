---
site: ZDNet France
title: "Open source: Facebook, Google, IBM et Red Hat étendent le délai de rectification des licences GPL"
author: Thierry Noisette
date: 2017-11-29
href: http://www.zdnet.fr/blogs/l-esprit-libre/open-source-facebook-google-ibm-et-red-hat-etendent-le-delai-de-rectification-des-licences-gpl-39860830.htm
tags:
- Entreprise
- Licenses
---

> Droit à l'erreur et présomption de bonne foi: les 4 entreprises étendent à leur code source sous licence GPLv2 le délai de rectification de 60 jours qui existe déjà pour la GPLv3.
