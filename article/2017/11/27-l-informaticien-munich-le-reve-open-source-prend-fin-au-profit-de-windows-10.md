---
site: L'Informaticien
title: "A Munich, le rêve open source prend fin, au profit de Windows 10"
date: 2017-11-27
href: https://www.linformaticien.com/actualites/id/45694/a-munich-le-reve-open-source-prend-fin-au-profit-de-windows-10.aspx
tags:
- Logiciels privateurs
- Administration
- International
---

> La capitale de la Bavière a décidé de repasser sous pavillon Microsoft, et équipera tout son parc en Windows 10 d’ici 2020. Un budget de 49,3 millions d’euros a été voté par le conseil municipal.
