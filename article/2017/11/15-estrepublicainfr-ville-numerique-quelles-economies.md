---
site: estrepublicain.fr
title: "Ville numérique: quelles économies?"
author: Ghislain Utard
date: 2017-11-15
href: http://www.estrepublicain.fr/edition-de-nancy-ville/2017/11/15/ville-numerique-quelles-economies
tags:
- Administration
- Innovation
---

> La Ville de Nancy a amorcé le virage du logiciel libre et multiplie les services en ligne. Pour quelles économies? Avec quels bénéfices pour le citoyen? Éléments de réponse avant la semaine de l’innovation publique.
