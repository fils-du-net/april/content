---
site: Factor Today
title: "Richard Stallman and the Vanishing State of Privacy"
date: 2017-11-17
href: http://magazine.factor-tech.com/factor_winter_2017/richard_stallman_and_the_vanishing_state_of_privacy
tags:
- Internet
- Institutions
- Informatique-deloyale
- English
- Vie privée
---

> (Nous sommes à présent sujets à un plus grand niveau de surveillance qu'à n'importe quel autre point dans l'histoire, et la plus grande part vient de la révolution numérique des dernières décennies) We are now subject to a greater level of surveillance than any point in history, and most of it is thanks to the digital revolution of the last few decades. Lucy Ingham hears from the legend Richard Stallman about how the digital transformation has dramatically eroded our privacy, and what it means for our lives
