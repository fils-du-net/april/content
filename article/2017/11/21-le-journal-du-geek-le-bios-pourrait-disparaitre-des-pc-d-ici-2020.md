---
site: Le Journal du Geek
title: "Le BIOS pourrait disparaître des PC d'ici 2020"
author: Mathieu
date: 2017-11-21
href: http://www.journaldugeek.com/2017/11/21/bios-ordinateurs-disparaitre-2020
tags:
- Entreprise
- Informatique-deloyale
---

> Pour les novices, c’est parfois une véritable galère. Pour les utilisateurs plus expérimentés, c’est une obligation, mais aussi une potentielle perte de temps. Depuis toujours, les possesseurs d’ordinateurs portables ou fixes, doivent régulièrement veiller à mettre à jour le BIOS de la carte-mère.
