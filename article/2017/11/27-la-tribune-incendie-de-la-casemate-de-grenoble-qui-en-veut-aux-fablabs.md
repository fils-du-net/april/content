---
site: La Tribune
title: "Incendie de la Casemate de Grenoble: qui en veut aux fablabs?"
author: Alexis Poulin
date: 2017-11-27
href: https://acteursdeleconomie.latribune.fr/debats/opinion/2017-11-27/incendie-de-la-casemate-de-grenoble-qui-en-veut-aux-fablabs-759393.html
tags:
- Partage du savoir
- Matériel libre
---

> L'incendie qui a détruit le fablab historique de la Casemate à Grenoble relève d'un "acte de combat" par des "activistes de bac à sable", "bien désarmés dans le combat idéologique présent", assure Alexis Poulin. Pour l'ancien directeur d'EurActiv France et co-fondateur du média Le Monde Moderne, si "la critique d'une technologie coercitive de contrôle est plus que jamais nécessaire", en revanche, "l'action terroriste, ne peut que desservir la cause".
