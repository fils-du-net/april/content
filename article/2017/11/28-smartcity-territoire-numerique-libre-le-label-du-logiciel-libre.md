---
site: smartcity
title: "Territoire Numérique Libre: le label du logiciel libre"
author: Nelly Moussu
date: 2017-11-28
href: http://www.smartcitymag.fr/article/236/territoire-numerique-libre-le-label-du-logiciel-libre
tags:
- Administration
- Associations
- Promotion
---

> Le 5 décembre prochain, le label Territoire Numérique Libre récompensera des collectivités qui développent ou utilisent des logiciels libres. Des pratiques que l’association Adullact (Association des développeurs et utilisateurs de logiciels libres pour les administrations et les collectivités territoriales), à l’origine de l’initiative, souhaite encourager. Explications avec Pascal Kuczynski, son délégué général.
