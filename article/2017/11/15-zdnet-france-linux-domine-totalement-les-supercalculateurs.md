---
site: ZDNet France
title: "Linux domine totalement les supercalculateurs"
date: 2017-11-15
href: http://www.zdnet.fr/actualites/linux-domine-totalement-les-supercalculateurs-39859992.htm
tags:
- Innovation
---

> C'est finalement arrivé. Aujourd'hui, les 500 supercalculateurs les plus importants du monde utilisent Linux.
