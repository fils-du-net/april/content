---
site: Developpez.com
title: "Qu'advient-il du code open source après le décès du développeur?"
author: Olivier Famien
date: 2017-11-13
href: https://www.developpez.com/actu/172888/Qu-advient-il-du-code-open-source-apres-le-deces-du-developpeur-Quelles-solutions-adopter-pour-eviter-les-problemes-lies-a-l-abandon-du-code
tags:
- Sensibilisation
---

> Alors que l’on a assisté pendant longtemps à un combat entre les logiciels open source et les logiciels propriétaires, il faut reconnaître que depuis plusieurs années, ces deux mondes ne sont plus perçus comme opposés, mais plutôt complémentaires.
