---
site: francetv info
title: "Nouveau monde. Un YouTube \"libre\" à la française"
author: Jérôme Colombain
date: 2017-11-27
href: https://www.francetvinfo.fr/replay-radio/nouveau-monde/nouveau-monde-un-youtube-libre-a-la-francaise_2464908.html
tags:
- Internet
- Associations
- Innovation
- Vie privée
---

> Un concurrent de YouTube à base de logiciels libres et de peer-to-peer est mis au point par des Français ambitieux: en France, si les géants du web manquent, les idées, elles, fleurissent. C'est sans compter la méfiance naturelle des Français pour l'influence des grandes plateformes américaines. C’est le cas de l’association Framasoft qui a déjà fait parler d’elle il y a quelques temps avec une campagne pour "dégoogliser Internet". Framasoft propose des logiciels libres alternatifs pour remplacer Gmail, Chrome ou Google Doc.
