---
site: ina global
title: "Lutte fratricide dans les coulisses du web"
author: Guillaume Sire
date: 2017-11-30
href: http://www.inaglobal.fr/numerique/article/lutte-fratricide-dans-les-coulisses-du-web-10024
tags:
- Internet
- Associations
- DRM
- Standards
- English
---

> Qui connaît le W3C? Cet organe, qui décide ce qui peut être fait ou non sur le web, comment, et dans quelle mesure, traverse une crise sans précédent. La raison? L’implémentation des DRM au sein des standards du web, bien loin de l’esprit des pionniers d’Internet.
