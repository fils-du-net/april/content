---
site: Libération
title: "Surveillance: la première boîte noire est née"
author: Pierre Alonso et Amaelle Guiton
date: 2017-11-14
href: http://www.liberation.fr/france/2017/11/14/surveillance-la-premiere-boite-noire-est-nee_1609993
tags:
- Internet
- Institutions
- Vie privée
---

> Le président de la Commission de contrôle des techniques de renseignement a révélé que cet algorithme utilisé par les services de renseignement est mis en œuvre depuis plus d'un mois.
