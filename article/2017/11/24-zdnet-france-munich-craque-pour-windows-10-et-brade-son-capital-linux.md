---
site: ZDNet France
title: "Munich craque pour Windows 10 et brade son capital Linux"
author: Christophe Auffray
date: 2017-11-24
href: http://www.zdnet.fr/actualites/munich-craque-pour-windows-10-et-brade-son-capital-linux-39860596.htm
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Interopérabilité
---

> Le rêve open source de la ville de Munich est mort. La municipalité a voté le projet de migration du parc de PC vers Windows 10 et Office. Elle dépensera ainsi 49,3 millions d'euros pour revenir au tout-Windows.
