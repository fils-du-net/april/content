---
site: La Tribune
title: "Des logiciels libres pour renforcer la sécurité des TPE-PME"
author: Pauline Compan
date: 2017-11-23
href: http://objectif-languedoc-roussillon.latribune.fr/entreprises/tic/2017-11-21/des-logiciels-libres-pour-renforcer-la-securite-des-tpe-pme-758761.html
tags:
- Entreprise
- Associations
- Promotion
---

> La CCI Hérault recense les logiciels et les prestataires de services autour de l’open source au sein d'annuaires disponibles en ligne pour les TPE–PME. Objectif affiché sur 2018: accompagner leur mutation vers le logiciel libre, en partenariat avec Montpel’libre.
