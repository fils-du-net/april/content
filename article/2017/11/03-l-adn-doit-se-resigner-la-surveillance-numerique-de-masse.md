---
site: L’ADN
title: "Doit-on se résigner à la surveillance numérique de masse?"
author: Nastasia Hadjadji
date: 2017-11-03
href: http://www.ladn.eu/nouveaux-usages/doit-on-se-resigner-a-la-surveillance-numerique-de-masse
tags:
- Internet
- Vie privée
---

> Nous sommes allés voir le documentaire Nothing To Hide de Marc Meillassoux: il évoque la surveillance numérique de masse à travers les témoignages de lanceurs d’alerte, d’activistes et de défenseurs des libertés numériques.
