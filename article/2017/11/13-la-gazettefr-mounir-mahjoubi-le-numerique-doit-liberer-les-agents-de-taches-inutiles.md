---
site: La gazette.fr
title: "Mounir Mahjoubi: «Le numérique doit libérer les agents de tâches inutiles»"
author: Delphine Gerbeau et Romain Mazon
date: 2017-11-13
href: http://www.lagazettedescommunes.com/533016/mounir-mahjoubi-le-numerique-doit-liberer-les-agents-de-taches-inutiles
tags:
- Administration
- Institutions
- Innovation
- Open Data
---

> L’administration innove-t-elle assez ? Comment peut-on parvenir à 100 % de procédures dématérialisées à court terme? A quelles conditions resteront-elles accessibles au plus grand nombre? Quels seront les impacts de cette transformation sur les services publics, et la fonction publique, de l’Etat comme territoriale? A quelques jours du lancement de la semaine de l’innovation publique, interview de Mounir Mahjoubi, secrétaire d’Etat au numérique
