---
site: Journal du Net
title: "L'Île-de-France lance une plateforme d'open data et de données privées"
author: Jamal El Hassani
date: 2017-11-21
href: http://www.journaldunet.com/economie/services/1205129-ile-de-france-plateforme-open-data-3d
tags:
- Administration
- Open Data
---

> En plus de cette brique de partage, le service permettra à terme à toutes les villes de la région de disposer de visualisations 3D de leur territoire.
