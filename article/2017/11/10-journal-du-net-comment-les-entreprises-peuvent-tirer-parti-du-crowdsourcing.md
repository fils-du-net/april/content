---
site: Journal du Net
title: "Comment les entreprises peuvent tirer parti du crowdsourcing"
author: Jordan Elle
date: 2017-11-10
href: http://www.journaldunet.com/web-tech/expert/67926/comment-les-entreprises-peuvent-tirer-parti-du-crowdsourcing.shtml
tags:
- Entreprise
- Innovation
---

> Alors, qu'est-ce que le crowdsourcing au juste? Le crowdsourcing est l’externalisation d’une tâche à l'ensemble de la communauté des logiciels libres.
