---
site: ZDNet France
title: "Charly Berthet (CNNum): ”Quand Google vous recommande la mauvaise table basse, c'est pas très grave. Si la DGSI se trompe, ça a plus d'incidences”"
author: Guillaume Serries
date: 2017-11-13
href: http://www.zdnet.fr/actualites/charly-berthet-cnnum-quand-google-vous-recommande-la-mauvaise-table-basse-c-est-pas-tres-grave-si-la-dgsi-se-trompe-ca-a-plus-d-incidences-39857244.htm
tags:
- Internet
- Institutions
- Vie privée
---

> Les algorithmes doivent être domptés par les citoyens dit le responsable juridique du Conseil National du Numérique. Et une surveillance individualisée est possible sans tomber dans les travers de la surveillance de masse via la mise en place de backdoors dans les outils de chiffrement.
