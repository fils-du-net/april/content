---
site: Le Monde Informatique
title: "7 distributions Linux surprenantes"
author: Dave Taylor
date: 2017-11-03
href: http://www.lemondeinformatique.fr/actualites/lire-7-distributions-linux-surprenantes-69868.html
tags:
- Innovation
---

> En dehors des distributions Linux les plus connues, comme Ubuntu, Red Hat ou Fedora, il en existe d'autres moins renommées mais qui valent quand même le détour. Zoom sur les plus originales voire décalées.
