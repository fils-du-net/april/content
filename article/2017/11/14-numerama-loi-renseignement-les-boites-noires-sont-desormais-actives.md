---
site: Numerama
title: "Loi Renseignement: les boîtes noires sont désormais actives"
author: Julien Lausson
date: 2017-11-14
href: http://www.numerama.com/politique/305959-les-boites-noires-de-la-loi-renseignement-sont-desormais-actives.html
tags:
- Internet
- Institutions
- Vie privée
---

> Le président de la CNCTR, qui doit contrôler l'usage des boîtes noires par les services de renseignement, a déclaré que celles-ci étaient actives depuis un mois maintenant.
