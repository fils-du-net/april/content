---
site: Numerama
title: "Comment surveille-t-on et censure-t-on Internet?"
author: Victoria Castro
date: 2017-11-06
href: http://www.numerama.com/tech/303113-comment-surveille-t-on-et-censure-t-on-internet.html
tags:
- Internet
- Institutions
---

> Comment Internet est-il surveillé? Filtré? Censuré? Ou tout simplement, comment fonctionne-t-il, quand il fonctionne? Tentons de donner des réponses claires à ces questions complexes.
