---
site: The Conversation
title: "Les plates-formes d’innovation: des communautés porteuses de nouvelles relations de travail"
author: David W. Versailles
date: 2017-11-09
href: https://theconversation.com/les-plates-formes-dinnovation-des-communautes-porteuses-de-nouvelles-relations-de-travail-83121
tags:
- Partage du savoir
- Innovation
---

> La digitalisation de l’économie offre aujourd’hui de nouvelles opportunités pour l’innovation. Elle oblige aussi à une accélération du développement des projets, voire à une transformation des modes de travail. Ces opportunités émergent en mobilisant de nouveaux outils technologiques (data analytics, visualisation, imprimantes 3D…) ou se matérialisent en construisant de nouveaux business models.
