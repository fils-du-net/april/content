---
site: Le Monde.fr
title: "Aux Etats-Unis, la neutralité du Net menacée de disparition d’ici la fin de l’année"
author: Damien Leloup
date: 2017-11-21
href: http://www.lemonde.fr/pixels/article/2017/11/21/aux-etats-unis-la-neutralite-du-net-menacee-de-disparition-d-ici-la-fin-de-l-annee_5218312_4408996.html
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- International
---

> Le régulateur des télécoms va probablement adopter des mesures de dérégulation, laissant le champ libre aux fournisseurs d’accès à Internet.
