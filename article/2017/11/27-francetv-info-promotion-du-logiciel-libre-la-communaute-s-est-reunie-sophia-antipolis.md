---
site: francetv info
title: "Promotion du logiciel libre: la communauté s'est réunie à Sophia-Antipolis"
author: Coralie Becq
date: 2017-11-27
href: https://france3-regions.francetvinfo.fr/provence-alpes-cote-d-azur/promotion-du-logiciel-libre-communaute-s-est-reunie-sophia-antipolis-1373609.html
tags:
- Administration
- Associations
- Promotion
- Sciences
---

> Tous les deux ans, l'association Linux Azur organise la journée méditerranéenne du logiciel libre. Ces programmes gratuits et entièrement modifiables qui fédèrent toute une communauté. Un évènement pour sensibiliser et faire la promotion de ces logiciels.
