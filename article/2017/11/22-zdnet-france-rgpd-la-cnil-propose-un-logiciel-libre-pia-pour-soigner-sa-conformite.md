---
site: ZDNet France
title: "RGPD - La Cnil propose un logiciel libre PIA pour soigner sa conformité"
author: Christophe Auffray
date: 2017-11-22
href: http://www.zdnet.fr/actualites/rgpd-la-cnil-propose-un-logiciel-libre-pia-pour-soigner-sa-conformite-39860412.htm
tags:
- Entreprise
- Institutions
- Europe
- Vie privée
---

> Obligatoire dans bien des cas, et toujours fortement recommandée, l'analyse d'impact sur la protection des données (PIA) est un volet majeur du RGPD. Pour aider les entreprises à anticiper l'application de mai 2018 et se mettre en conformité, la Cnil publie un logiciel libre dédié à ces opérations.
