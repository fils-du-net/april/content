---
site: Developpez.com
title: "Munich a décidé d'abandonner LiMux pour Windows 10 à partir de 2020"
author: Stéphane le calme
date: 2017-11-25
href: https://www.developpez.com/actu/175493/Munich-a-decide-d-abandonner-LiMux-pour-Windows-10-a-partir-de-2020-une-migration-a-50-millions-d-euros
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Interopérabilité
---

> En 2003. Munich, troisième ville la plus importante d’Allemagne, dispose de plus de 16 000 PC utilisés par les employés de l’administration. La fin de la prise en charge de Windows NT est proche et celle de Windows XP suivra dans quelques années. La ville de Munich a besoin d’une alternative pour mettre fin aux migrations forcées des solutions propriétaires
