---
site: Le Monde.fr
title: "Justice: les enquêteurs pourront bientôt utiliser des logiciels espions"
author: Martin Untersinger (avec Elise Vincent et Jean-Baptiste Jacquin)
date: 2017-11-14
href: http://www.lemonde.fr/pixels/article/2017/11/14/justice-les-enqueteurs-pourront-bientot-utiliser-des-logiciels-espions_5214397_4408996.html?xtor=RSS-3208
tags:
- Institutions
- Vie privée
---

> Ces outils technologiques sont capables d’extraire les données des appareils informatiques d’une personne mise en cause dans une enquête judiciaire.
