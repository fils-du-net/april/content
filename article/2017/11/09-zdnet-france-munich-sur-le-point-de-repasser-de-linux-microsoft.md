---
site: ZDNet France
title: "Munich sur le point de repasser de Linux à Microsoft"
author: Thierry Noisette
date: 2017-11-09
href: http://www.zdnet.fr/blogs/l-esprit-libre/munich-sur-le-point-de-repasser-de-linux-a-microsoft-39859792.htm
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Europe
---

> Plus d'une décennie après son très remarqué passage au logiciel libre, la capitale de la Bavière est tout près de revenir à Microsoft. Un dernier vote, en principe acquis, a lieu le 23 novembre.
