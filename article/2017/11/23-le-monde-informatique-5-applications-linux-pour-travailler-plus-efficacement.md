---
site: Le Monde Informatique
title: "5 applications Linux pour travailler plus efficacement"
author: Mark Gibbs
date: 2017-11-23
href: http://www.lemondeinformatique.fr/actualites/lire-5-applications-linux-pour-travailler-plus-efficacement-70066.html
tags:
- Sensibilisation
- Promotion
---

> Voici cinq apps de productivité dans l'environnement Linux particulièrement performantes qui méritent d'être connues. La sélection est variée puisqu'elle comporte un outil de comptabilité GnuCash, un outil de mise en page Scribus, une app de gestion de projets ProjectLibre, une app de gestion du temps GnoTime et un outil de ligne de commande Xiki.
