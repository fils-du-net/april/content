---
site: Journal du Net
title: "Plaidoyer pour une stratégie open source ciblée au sein de l’industrie des transports"
author: Claire Lepelletier
date: 2017-11-07
href: http://www.journaldunet.com/solutions/expert/67911/plaidoyer-pour-une-strategie-open-source-ciblee-au-sein-de-l-industrie-des-transports.shtml
tags:
- Entreprise
- Partage du savoir
- Innovation
---

> Une stratégie open source ciblée crée les conditions d’un avantage concurrentiel décisif. Et ce, y compris pour les industriels des transports.
