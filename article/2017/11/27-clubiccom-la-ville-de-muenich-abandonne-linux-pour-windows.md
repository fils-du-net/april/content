---
site: Clubic.com
title: "La ville de Münich abandonne Linux pour Windows"
author: Alexandre Paulson
date: 2017-11-27
href: http://www.clubic.com/linux-os/actualite-839482-ville-munich-abandonne-linux-windows.html
tags:
- Logiciels privateurs
- Administration
- International
---

> Après 14 ans d'expérience du logiciel libre, la Ville de Münich a décidé de revenir en territoire payant en planifiant la bascule vers Windows à l'horizon 2020. L'équipe dirigeante dénonce le manque de fiabilité de Linux, mais les opposants à cette décision y voient un choix guidé par d'autres intérêts...
