---
site: Boursorama
title: "Savez-vous préserver votre vie privée sur internet?"
date: 2017-11-13
href: http://www.boursorama.com/actualites/savez-vous-preserver-votre-vie-privee-sur-internet-f93a01346333537331393f469f991b38
tags:
- Internet
- Vie privée
---

> Espace de liberté absolue, internet peut aussi vampiriser celui qui s'y égare. Photos, mails, localisation, évènements, nous laissons sur la toile quantité d'informations personnelles, avec parfois des conséquences fâcheuses. Il existe pourtant quelques astuces pour préserver (un peu) sa vie privée, sans renoncer à internet.
