---
site: Libération
title: "Les Etats-Unis vers l'Internet à deux vitesses"
author: Amaelle Guiton
date: 2017-11-22
href: http://www.liberation.fr/planete/2017/11/22/les-etats-unis-vers-l-internet-a-deux-vitesses_1611829
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Neutralité du Net
- International
---

> Comme attendu, l'autorité américaine de régulation des télécoms a précisé les contours de son offensive contre la «neutralité du Net». Seule une obligation de transparence sur leurs pratiques s'imposerait aux opérateurs.
