---
site: L'OBS
title: "Contre Google, Amazon &amp; Co: les 6 familles de résistants aux Gafam"
author: Dominique Nora
date: 2017-11-12
href: https://tempsreel.nouvelobs.com/economie/20171110.OBS7195/contre-google-amazon-co-les-6-familles-de-resistants-aux-gafam.html
tags:
- Entreprise
- Internet
- Vie privée
---

> Hier héros positifs de l'innovation, les Gafam [Google, Apple, Facebook, Amazon ou Microsoft] sont-ils en train de devenir les nouveaux vilains du capitalisme mondialisé? Séduisants et gratuits, leurs services sont plébiscités par des milliards d'usagers. Pourtant, en Europe et aux Etats-Unis, les attaques pleuvent à présent sur Larry Page (Alphabet/Google), Jeff Bezos (Amazon), Mark Zuckerberg (Facebook/Instagram/Whatsapp), Tim Cook (Apple) et Satya Nadella (Microsoft).
