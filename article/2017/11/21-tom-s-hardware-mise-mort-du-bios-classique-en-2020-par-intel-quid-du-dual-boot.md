---
site: Tom's Hardware
title: "Mise à mort du BIOS classique en 2020 par Intel, quid du dual boot?"
author: David Civera
date: 2017-11-21
href: http://www.tomshardware.fr/articles/bios-intel-csm-processeur-uefi,1-66007.html
tags:
- Entreprise
- Informatique-deloyale
---

> Intel va abandonner la gestion des BIOS classiques en 2020 au profit de l’UEFI Class 3. Concrètement, la décision sera avant tout un problème pour certains logiciels d’entreprises, les systèmes d’exploitation 32 bits, et le dual boot. Aujourd’hui les plateformes d’Intel pour le grand public et les serveurs disposent du Compatibility Support Module (CSM) qui permet de contourner le Secure Boot et les restrictions de l’UEFI des fabricants en assurant la gestion d’un BIOS 16 bits. Le père des Pentium annonce donc la fin de la prise en charge de cette fonctionnalité par ses composants.
