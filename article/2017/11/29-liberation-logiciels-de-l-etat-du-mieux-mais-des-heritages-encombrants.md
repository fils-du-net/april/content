---
site: Libération
title: "Logiciels de l'Etat: du mieux, mais des héritages encombrants"
author: Pierre Alonso et Amaelle Guiton
date: 2017-11-29
href: http://www.liberation.fr/france/2017/11/29/logiciels-de-l-etat-du-mieux-mais-des-heritages-encombrants_1613369
tags:
- Administration
- Économie
---

> Les dernières données publiques concernant les grands projets informatiques de l'Etat ont été mises en ligne ce mercredi.
