---
site: Rue89Lyon
title: "Eau, logiciels libres et monnaies locales: 5 choses à savoir sur les communs"
author:  Philippine Orefice
date: 2017-11-17
href: http://www.rue89lyon.fr/2017/11/17/logiciels-libres-monnaies-locales-5-choses-a-savoir-sur-les-communs
tags:
- Économie
- Partage du savoir
- Contenus libres
---

> L’eau, les logiciels libres, les jardins partagés, les monnaies locales… autant de réalités qui sont aujourd’hui englobées dans l’appellation «communs». Pourtant, alors même qu’on les croise tous les jours, il est parfois difficile de comprendre leurs spécificités.
