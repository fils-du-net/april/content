---
site: Le Monde.fr
title: "Objet de critiques sur sa composition, le Conseil national du numérique va être remanié"
author: Martin Untersinger
date: 2017-12-14
href: http://www.lemonde.fr/pixels/article/2017/12/14/objet-de-critiques-sur-sa-composition-le-conseil-national-du-numerique-va-etre-remanie_5229611_4408996.html
tags:
- Institutions
---

> Cette instance consultative, tout juste relancée, va voir sa composition modifiée à la suite d’une polémique autour de la militante Rokhaya Diallo, qui y faisait son entrée.
