---
site: Numerama
title: "Neutralité du net: l'Arcep souligne la «responsabilité mondiale des démocraties»"
author: Julien Lausson
date: 2017-12-13
href: http://www.numerama.com/politique/314206-neutralite-du-net-larcep-souligne-la-responsabilite-mondiale-des-democraties.html
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- International
---

> Le régulateur français des télécoms est intervenu dans le débat américain sur la neutralité du net. Son président, Sébastien Soriano, a livré dans la presse un plaidoyer pour ce principe.
