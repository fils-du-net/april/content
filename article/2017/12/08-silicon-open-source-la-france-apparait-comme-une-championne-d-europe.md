---
site: Silicon
title: "Open Source: la France apparaît comme une championne d’Europe"
author: Xavier Biseul
date: 2017-12-08
href: https://www.silicon.fr/open-source-france-championne-europe-193023.html?inf_by=576a48732ad0a13708c140f4
tags:
- Entreprise
- Économie
---

> Open Source Summit: le marché se porte bien en France. Entre secteurs public et privé, le recours aux solutions du libre se généralise. Ce qui crée des tensions au niveau de l’emploi.
