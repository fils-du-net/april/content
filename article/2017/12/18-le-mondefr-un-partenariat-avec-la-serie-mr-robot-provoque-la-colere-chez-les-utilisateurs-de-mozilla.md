---
site: Le Monde.fr
title: "Un partenariat avec la série «Mr. Robot» provoque la colère chez les utilisateurs de Mozilla Firefox"
date: 2017-12-18
href: http://www.lemonde.fr/pixels/article/2017/12/18/un-partenariat-avec-la-serie-mr-robot-provoque-la-colere-chez-les-utilisateurs-de-mozilla-firefox_5231411_4408996.html
tags:
- Internet
- Associations
- Vie privée
---

> Les utilisateurs du navigateur Internet ont vu une mystérieuse extension s’installer sans leur consentement.
