---
site: ZDNet France
title: "Le RGPD et ses impacts sur la fonction marketing"
author: Xavier Biseul
date: 2017-12-12
href: http://www.zdnet.fr/actualites/le-rgpd-et-ses-impacts-sur-la-fonction-marketing-39861228.htm
tags:
- Entreprise
- Internet
- Institutions
- Sensibilisation
- Associations
- Europe
- Vie privée
---

> Au-delà du recueil du consentement des personnes fichées, le règlement européen sur la protection des données personnelles introduit une opposition au profilage. Les directions marketing ont moins de six mois pour se mettre en règle.
