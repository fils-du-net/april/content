---
site: FIGARO
title: "Mounir Mahjoubi: «Le Conseil national du numérique doit être proche du gouvernement»"
author: Lucie Ronfaut
date: 2017-12-19
href: http://www.lefigaro.fr/secteur/high-tech/2017/12/19/32001-20171219ARTFIG00117-mounir-mahjoubi-le-conseil-national-du-numerique-doit-etre-proche-du-gouvernement.php
tags:
- Institutions
---

> Marie Ekeland a démissionné de la présidence du Conseil national du numérique, suite au renvoi de la militante féministe et antiraciste Rokhaya Diallo. Le gouvernement assume sa décision.
