---
site: Lyon Capitale.fr
title: "Le Rhône récompensé par le label \"Territoire numérique libre\""
author: Florent Deligia
date: 2017-12-27
href: http://www.lyoncapitale.fr/Journal/Communs/Univers/Technologies/High-tech/Le-Rhone-recompense-par-le-label-Territoire-numerique-libre
tags:
- Administration
- Économie
- Associations
- Promotion
---

> Les logiciels "open source", longtemps considérés comme chasse privée des férus d'informatique, séduisent davantage, et même jusque dans les collectivités. Le 21 décembre, le Rhône recevait une récompense du label "Territoire numérique libre" niveau 3, qui valorise l'investissement dans des logiciels libres.
