---
site: ZDNet France
title: "Le RGPD exige (enfin) un consentement éclairé"
author: Xavier Biseul
date: 2017-12-06
href: http://www.zdnet.fr/actualites/le-rgpd-exige-enfin-un-consentement-eclaire-39861032.htm
tags:
- Internet
- Institutions
- Europe
- Vie privée
---

> Le règlement européen sur la protection des données personnelles place le consentement de l’individu au cœur de son approche. Le responsable de traitement doit répondre à de nouvelles exigences notamment en matière d’informations. Check-list des mesures à prendre.
