---
site: infoDSI
title: "L’Open Source, une filière deux fois plus dynamique que l’ensemble du marché numérique français"
date: 2017-12-07
href: http://www.infodsi.com/articles/172267/open-source-filiere-deux-fois-plus-dynamique-ensemble-marche-numerique-francais.html
tags:
- Entreprise
- Économie
---

> Le cabinet d’études PAC et des chercheurs des IAE de Valenciennes et Lyon ont interrogé des entreprises, fournisseurs et utilisatrices de solutions Open Source, afin de mettre en évidence le poids économique de la filière et son impact sur l’ensemble de l’économie, notamment sur l’emploi et l’innovation. Et voici les principaux résultats de cette étude, menée à l’initiative de Syntec Numérique, du CNLL et de Systematic Paris Region. Des résultats communiqués à l’occasion du Paris Open Source Summit (les 6 et 7 décembre aux Docks de Paris).
