---
site: Le Monde.fr
title: "La France présente sa stratégie numérique"
author: Martin Untersinger
date: 2017-12-16
href: http://www.lemonde.fr/economie/article/2017/12/16/la-france-presente-sa-strategie-numerique_5230796_3234.html
tags:
- Entreprise
- Internet
- Institutions
---

> Le document du gouvernement français, qui souhaite toujours l’application du droit international au cyberespace, aborde aussi bien la cybersécurité, la fiscalité que les données personnelles.
