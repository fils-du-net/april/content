---
site: Libération
title: "L'Europe doit défendre la «neutralité du Net»"
author: Benjamin Bayart
date: 2017-12-14
href: http://www.liberation.fr/debats/2017/12/14/l-europe-doit-defendre-la-neutralite-du-net_1616667
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Neutralité du Net
- International
---

> Une loi datant de la présidence d'Obama obligeait les fournisseurs d'accès Internet à traiter tous les services de la même manière assurant ainsi le «neutralité du Net». L'administration Trump vient de renoncer à ce principe: une révolution pour les deux côtés de l'Atlantique. Décryptage.
