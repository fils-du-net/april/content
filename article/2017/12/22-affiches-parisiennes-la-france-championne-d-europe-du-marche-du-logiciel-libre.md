---
site: Affiches Parisiennes
title: "La France, championne d'Europe du marché du logiciel libre"
author: Anne Moreaux
date: 2017-12-22
href: http://www.affiches-parisiennes.com/la-france-championne-d-europe-du-marche-du-logiciel-libre-7605.html
tags:
- Entreprise
- Économie
- Sensibilisation
- Innovation
---

> La filière du logiciel libre et Open Source française s'est réuni récemment à l'occasion du Paris Open Source Summit, qui se tenaient aux Docks de Paris et accueillait aussi le Village de la LegalTech. Une restitution de l'Etude 2017-2022 sur l'évolution du marché du logiciel libre a placé l'Hexagone en tête de ses voisins européens en matière d'open source, au grand plaisir du secrétaire d'Etat chargé du Numérique.
