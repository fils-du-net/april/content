---
site: Numerama
title: "Cédric Villani entend imposer la transparence du successeur du système APB"
author: Julien Lausson
date: 2017-12-13
href: http://www.numerama.com/politique/313937-cedric-villani-entend-imposer-la-transparence-du-successeur-du-systeme-apb.html
tags:
- Administration
- Institutions
- Éducation
---

> Cédric Villani a fait adopter un amendement au projet de loi sur l’orientation et à la réussite des étudiants dans lequel il compte imposer explicitement la transparence de la plateforme succédant à APB... même si la loi prévoit déjà cette ouverture.
