---
site: Le Monde Informatique
title: "Les grands comptes s'unissent dans l'open-source"
author: Bertrand Lemaire
date: 2017-12-04
href: https://www.lemondeinformatique.fr/actualites/lire-les-grands-comptes-s-unissent-dans-l-open-source-70170.html
tags:
- Entreprise
- Associations
---

> En se regroupant au sein de l'association The Open-Source I-Trust (TOSIT), de grandes entreprises dont EDF, Enedis, Société Générale et Carrefour ont pour objectif de coordonner leurs actions dans le domaine de l'open source.
