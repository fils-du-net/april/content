---
site: Le Monde.fr
title: "Le Conseil national du numérique perd sa présidente et la quasi-totalité de ses membres"
author: Sandrine Cassini
date: 2017-12-19
href: http://www.lemonde.fr/pixels/article/2017/12/19/marie-ekeland-demissionne-du-conseil-national-du-numerique_5231925_4408996.html
tags:
- Institutions
---

> Marie Ekeland a démissionné après la décision du gouvernement d’écarter Rokhaya Diallo de l’institution ; la nomination de la militante féministe avait été très critiquée.
