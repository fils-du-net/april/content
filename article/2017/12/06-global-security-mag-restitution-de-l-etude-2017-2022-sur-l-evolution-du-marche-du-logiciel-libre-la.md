---
site: Global Security Mag
title: "Restitution de l'Etude 2017-2022 sur l'évolution du marché du logiciel libre: La France, championne d'Europe!"
date: 2017-12-06
href: http://www.globalsecuritymag.fr/Restitution-de-l-Etude-2017-2022,20171206,75575.html
tags:
- Entreprise
- Économie
- Associations
- Europe
---

> La filière du logiciel libre et Open Source française se réunit actuellement à l’occasion du Paris Open Source Summit, qui se tient jusqu’à demain soir, aux Docks de Paris.
