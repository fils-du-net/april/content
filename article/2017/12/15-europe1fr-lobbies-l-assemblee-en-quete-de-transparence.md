---
site: Europe1.fr
title: "Lobbies: l'Assemblée en quête de transparence"
date: 2017-12-15
href: http://www.europe1.fr/politique/lobbies-lassemblee-en-quete-de-transparence-3521963
tags:
- Institutions
- Open Data
---

> Les députés ont formulé cette semaine une proposition pour améliorer la transparence du travail des lobbyistes à l'Assemblée. Beaucoup d'efforts restent à faire.
