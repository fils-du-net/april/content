---
site: medi1radio
title: "Après 13 ans, Munich abandonne les logiciels libres"
author: Amaury Baradon
date: 2017-12-03
href: http://www.medi1.com/episode/apr%C3%A8s-13-ans--munich-abandonne-les-logiciels-libres-162232
tags:
- Logiciels privateurs
- Administration
- April
- International
---

> La municipalité de Munich, en 2004, choisissait de délaisser Windows pour un système d'exploitation sur mesure basé sur le noyau Linux...
