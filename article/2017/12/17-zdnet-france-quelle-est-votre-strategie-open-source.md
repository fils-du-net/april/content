---
site: ZDNet France
title: "Quelle est votre stratégie Open Source?"
author: Frédéric Charles
date: 2017-12-17
href: http://www.zdnet.fr/blogs/green-si/quelle-est-votre-strategie-open-source-39861310.htm
tags:
- Entreprise
- Économie
- Sensibilisation
- Innovation
---

> En 2017, l'open source a clairement quitté le cœur de Linux, le débat sur le système d'exploitation souverain ou sur la licence, pour devenir la plateforme scalable et ouverte de transformation digitale.
