---
site: Les Echos
title: "La France, championne d'Europe de l'open source, Open Source"
author: Florent Vairet
date: 2017-12-06
href: https://business.lesechos.fr/directions-numeriques/technologie/open-source/030980651430-la-france-championne-d-europe-de-l-open-source-316671.php
tags:
- Entreprise
- Économie
- Innovation
---

> Le Paris Open Source Summit ouvre ses portes aujourd'hui pour deux jours. La France fait figure de leader dans l'utilisation du numérique ouvert.
