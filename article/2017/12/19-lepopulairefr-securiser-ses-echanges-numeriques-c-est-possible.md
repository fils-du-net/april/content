---
site: lepopulaire.fr
title: "Sécuriser ses échanges numériques, c’est possible"
date: 2017-12-19
href: http://www.lepopulaire.fr/faux-la-montagne/internet-multimedia/2017/12/19/securiser-ses-echanges-numeriques-cest-possible_12674389.html
tags:
- Sensibilisation
- Associations
- Vie privée
---

> Système d’exploitation, navigateur internet ou application de messagerie instantanée figuraient à la carte du Volubilis, vendredi après-midi. L’association Ctrl-a a investi le salon de thé de Faux-la-Montagne pour évoquer la protection des données numériques.
