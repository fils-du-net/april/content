---
site: Clubic.com
title: "Linux, maître absolu des supercalculateurs"
author: Alexandre Paulson
date: 2017-12-25
href: http://www.clubic.com/linux-os/actualite-839888-8203-linux-maitre-absolu-supercalculateurs.html
tags:
- Innovation
- Promotion
---

> Le rêve des promoteurs de Linux ne s'est pas encore réalisé: le logiciel libre n'est encore qu'une niche face aux OS propriétaires sur les PC dans les entreprises, les collectivités publiques et chez les particuliers. Mais il est un domaine où il domine sans partage: les supercalculateurs.
