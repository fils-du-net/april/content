---
site: LeMagIT
title: "Open Source: en 2017, le modèle trouve sa place, les communautés s’ajustent"
author: Cyrille Chausson
date: 2017-12-26
href: http://www.lemagit.fr/actualites/450432326/Open-Source-en-2017-le-modele-trouve-sa-place-les-communautes-sajustent
tags:
- Entreprise
- Économie
- Sensibilisation
---

> Rares sont les technologies émergentes apparues ou arrivées à maturité en 2017 qui n’ont été Open Source. Cette année, le logiciel à code ouvert a concrétisé son statut dans l’innovation, trouvant sa place dans les entreprises. Cette montée en puissance met la pression sur les communautés et les fondations.
