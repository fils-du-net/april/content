---
site: "@ Brest"
title: "Les logiciels produits par les administrations sont passés en Open Source par défaut (et voici pourquoi)"
author: calimaq
date: 2017-12-08
href: http://www.a-brest.net/article21723.html
tags:
- Administration
- April
- Institutions
- Licenses
- Open Data
---

> La loi pour une République numérique adoptée l’année dernière nous réserve encore quelques surprises, plus d’un an après son entrée en vigueur. On en a eu une confirmation cette lors du Paris Open Source Summit 2017 pendant une session consacrée à la thématique «Administration publique».
