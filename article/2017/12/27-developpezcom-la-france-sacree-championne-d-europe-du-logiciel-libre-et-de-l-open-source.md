---
site: Developpez.com
title: "La France sacrée championne d'Europe du logiciel libre et de l'open source"
author: Stéphane le calme
date: 2017-12-27
href: https://www.developpez.com/actu/180512/La-France-sacree-championne-d-Europe-du-logiciel-libre-et-de-l-open-source-devant-l-Allemagne-et-le-Royaume-Uni
tags:
- Entreprise
- Économie
- Sensibilisation
- Innovation
---

> Chaque année, le CNLL (Conseil National du Logiciel Libre) réalise une étude visant à mettre en évidence le poids économique du libre et de l’open source, son impact sur l’ensemble de l’économie, et notamment sur l’emploi et l’innovation. Les statistiques de l’étude, qui a été réalisée par PAC-CXP et des chercheurs des universités de Valenciennes et Lyon 3 pour le compte du CNLL, ont été présentées à l’occasion du Paris Open Source Summit, organisé par le Pôle de Compétitivité Systematic Paris-Region.
