---
site: ITRnews.com
title: "Framasoft cherche des fonds pour finaliser le développement de PeerTube"
author: ITRnews.com
date: 2017-12-01
href: http://www.itrnews.com/articles/172165/framasoft-cherche-fonds-finaliser-developpement-peertube.html
tags:
- Internet
- Associations
- Innovation
---

> L’association française Framasoft finance actuellement le développement de PeerTube, un logiciel libre qui entend contrecarrer le monopole de YouTube.
