---
site: Numerama
title: "Le nouveau Conseil national du numérique explose au décollage"
author: Julien Lausson
date: 2017-12-19
href: http://www.numerama.com/politique/315915-le-nouveau-conseil-national-du-numerique-explose-au-decollage.html
tags:
- Institutions
---

> Le Conseil national du numérique, qui devait se relancer en cette fin d'année, vient de s'autodétruire. Après le départ de sa présidente, vingt-huit membres ont démissionné par solidarité. L'instance consultative n'est aujourd'hui composée plus qu'un membre.
