---
site: LeMagIT
title: "Rétropédalage de Munich sur LiMux: le début d'une explication"
author: Cyrille Chausson
date: 2017-12-07
href: http://www.lemagit.fr/actualites/450431477/Retropedalage-de-Munich-le-debut-de-quelques-explications
tags:
- Logiciels privateurs
- Administration
- International
---

> A l’occasion du Paris Open Source Summit, un membre de l’équipe de développement de la ville de Munich est venu (un peu) expliquer, en son nom propre, la situation qui a conduit la mairie à débrancher LiMux pour revenir à Windows.
