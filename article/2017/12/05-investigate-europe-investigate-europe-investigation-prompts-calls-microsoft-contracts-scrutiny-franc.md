---
site: Investigate Europe
title: "Investigate Europe investigation prompts calls for Microsoft contracts scrutiny in France"
date: 2017-12-05
href: http://www.investigate-europe.eu/en/investigate-europe-investigation-prompts-calls-for-microsoft-contracts-scrutiny-in-france
tags:
- Entreprise
- Administration
- April
- Institutions
- English
---

> Investigate Europe investigation into public IT contracting practices prompts French officials to call for a public inquiry into defence contracts handed to Microsoft.
