---
site: Sud Ouest
title: "Comment devenir un lanceur d’alerte? Une ONG publie un guide pratique"
date: 2017-12-14
href: http://www.sudouest.fr/2017/12/14/comment-devenir-un-lanceur-d-alerte-une-ong-publie-un-guide-pratique-4033952-4725.php
tags:
- Partage du savoir
---

> L’ONG veut inciter les citoyens à se mobiliser tout en leur apprenant à se prémunir contre d’éventuelles poursuites juridiques.
