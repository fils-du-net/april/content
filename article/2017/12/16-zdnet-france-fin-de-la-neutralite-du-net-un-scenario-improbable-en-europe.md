---
site: ZDNet France
title: "Fin de la neutralité du net: un scénario improbable en Europe?"
author: Louis Adam
date: 2017-12-16
href: http://www.zdnet.fr/actualites/fin-de-la-neutralite-du-net-un-scenario-improbable-en-europe-39861672.htm
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- Europe
- International
---

> Le régulateur américain des télécoms a mis fin au cadre réglementaire entérinant la neutralité du net aux États Unis. En Europe, ce principe est protégé par le droit européen, contrairement aux États Unis où il reste à la merci d’une simple décision de la FCC. Décryptage avec Oriane Piquer Louis de la Fédération des Fournisseurs d'Accès à Internet Associatifs, dite Fédération FDN.
