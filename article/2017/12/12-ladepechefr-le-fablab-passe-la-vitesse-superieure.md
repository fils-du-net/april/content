---
site: LaDepeche.fr
title: "Le Fablab passe à la vitesse supérieure"
date: 2017-12-12
href: https://www.ladepeche.fr/article/2017/12/12/2702645-le-fablab-passe-a-la-vitesse-superieure.html
tags:
- Promotion
---

> Lancé il y a quelques mois, le fablab vient d'être renforcé par un manager en service civique. Cette arrivée a permis de dynamiser le lieu et d'augmenter les créneaux d'ouverture. Les ateliers Fablab ont trouvé leur rythme de croisière et passent à la vitesse supérieure.
