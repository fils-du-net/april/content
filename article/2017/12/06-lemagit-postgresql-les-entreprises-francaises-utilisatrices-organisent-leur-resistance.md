---
site: LeMagIT
title: "PostgreSQL: les entreprises françaises utilisatrices organisent leur résistance"
author: Cyrille Chausson
date: 2017-12-06
href: http://www.lemagit.fr/actualites/450431397/PostgreSQL-les-entreprises-francaises-utilisatrices-organisent-leur-resistance
tags:
- Entreprise
---

> Le Groupe de Travail Inter-Entreprises (PGGTIE) de la communauté françaises de PostgreSQL compte désormais une vingtaine de membres et se structurent pour accompagner les entreprises vers la base de données Open Source. Deux outils ont aujourd’hui émergé au sein de la communauté pour faciliter la migration, hors d’Oracle.
