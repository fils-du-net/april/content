---
site: Le Monde.fr
title: "«Ce n’est pas la fin du combat», pour les défenseurs de la neutralité du Net"
date: 2017-12-15
href: http://www.lemonde.fr/pixels/article/2017/12/15/ce-n-est-pas-la-fin-du-combat-previennent-les-defenseurs-de-la-neutralite-du-net_5230248_4408996.html
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- International
---

> Alors que le régulateur américain des télécoms a entériné jeudi la fin de la neutralité du Net, les opposants ne baissent pas les bras et annoncent des actions en justice.
