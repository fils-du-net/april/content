---
site: Le Monde.fr
title: "Le Conseil national du numérique reprend forme"
author: Sandrine Cassini
date: 2017-12-11
href: http://www.lemonde.fr/economie/article/2017/12/11/le-conseil-national-du-numerique-reprend-forme_5227879_3234.html
tags:
- Institutions
---

> Le gouvernement a officialisé la désignation de la capital-risqueuse Marie Ekeland à sa tête. La nouvelle structure, comme la précédente, compte trente membres
