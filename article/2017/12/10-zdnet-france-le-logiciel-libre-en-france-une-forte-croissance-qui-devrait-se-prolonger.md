---
site: ZDNet France
title: "Le logiciel libre en France: une forte croissance, qui devrait se prolonger"
author:  Thierry Noisette
date: 2017-12-10
href: http://www.zdnet.fr/blogs/l-esprit-libre/le-logiciel-libre-en-france-une-forte-croissance-qui-devrait-se-prolonger-39861314.htm
tags:
- Entreprise
- Économie
---

> Deux fois plus dynamique que l’ensemble du marché numérique français, selon l'étude PAC, le marché des logiciels libres et open source devrait créer plus de 4.000 emplois nets par an d'ici 2020.
