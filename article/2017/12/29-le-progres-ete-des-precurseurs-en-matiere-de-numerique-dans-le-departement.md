---
site: Le progrès
title: "«On a été des précurseurs en matière de numérique dans le département»"
author: Sacha Martinez
date: 2017-12-29
href: http://www.leprogres.fr/loire-42-edition-gier/2017/12/29/on-a-ete-des-precurseurs-en-matiere-de-numerique-dans-le-departement
tags:
- Administration
- Associations
- Open Data
---

> Véronique Bochaton a le sourire. La directrice des systèmes d’information de la Ville tient dans ses mains le deuxième label numérique libre obtenu en deux ans. «On a été des précurseurs en matière de numérique dans le département. Nous avons obtenu une note de trois sur cinq pour ce label.»
