---
site: ZDNet France
title: "Intelligence artificielle: l'éthique est-elle soluble dans l'économie?"
author: Christophe Auffray
date: 2017-12-15
href: http://www.zdnet.fr/actualites/intelligence-artificielle-l-ethique-est-elle-soluble-dans-l-economie-39861666.htm
tags:
- Institutions
---

> Les américains et les chinois vendent, les européens régulent et réfléchissent, et l'intelligence artificielle ne fera pas exception. Une critique injuste? La Cnil et 60 partenaires se sont penchés sur les enjeux éthiques des algorithmes et de l'IA. Pour Mounir Mahjoubi et Cédric Villani, non l'éthique n'est pas un handicap.
