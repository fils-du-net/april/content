---
site: l'essor
title: "Label «Territoire numérique libre»: le Rhône récompensé"
author: lessor69.fr
date: 2017-12-21
href: http://lessor69.fr/label-territoire-numerique-libre-le-rhone-recompense-19884.html
tags:
- Administration
- Associations
- International
---

> À l'occasion de la soirée d'ouverture du Paris Open Source Summit (POSS), le Département du Rhône a été récompensé par le label «Territoire numérique libre» qui valorise l'investissement de la collectivité dans l'utilisation et le développement de solutions numériques ouvertes.
