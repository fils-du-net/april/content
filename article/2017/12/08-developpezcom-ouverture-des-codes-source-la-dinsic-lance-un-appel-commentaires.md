---
site: Developpez.com
title: "Ouverture des codes source: la DINSIC lance un appel à commentaires"
author: Michael Guilloux
date: 2017-12-08
href: https://www.developpez.com/actu/177783/Ouverture-des-codes-source-la-DINSIC-lance-un-appel-a-commentaires-sur-la-politique-de-contribution-aux-logiciels-libres-de-l-Etat-francais
tags:
- Administration
- Institutions
- Open Data
---

> En décembre 2016, à l’occasion du 4e Sommet mondial pour le Partenariat pour un gouvernement ouvert, une action collective sur les modalités d’ouverture des codes source détenus par l’administration a été initiée afin d’engager des collaborations internationales en la matière, et de mutualiser les meilleures pratiques.
