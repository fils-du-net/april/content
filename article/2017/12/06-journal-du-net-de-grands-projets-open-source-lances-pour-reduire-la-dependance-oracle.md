---
site: Journal du Net
title: "De grands projets open source lancés pour réduire la dépendance à Oracle"
author: Antoine Crochet-Damais
date: 2017-12-06
href: http://www.journaldunet.com/solutions/dsi/1205799-open-source-oracle
tags:
- Logiciels privateurs
- Économie
---

> L'Open CIO Summit a été l'occasion cette année de mettre en avant le rôle des logiciels libres pour s'émanciper des éditeurs aux pratiques commerciales considérées excessives.
