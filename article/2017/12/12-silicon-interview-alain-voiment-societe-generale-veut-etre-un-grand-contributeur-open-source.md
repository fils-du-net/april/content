---
site: Silicon
title: "Interview Alain Voiment: ”Société Générale veut être un grand contributeur open source”"
author: Xavier Biseul
date: 2017-12-12
href: https://www.silicon.fr/interview-alain-voiment-societe-generale-open-source-193343.html
tags:
- Entreprise
- Interopérabilité
- Sensibilisation
---

> Open Source Summit: Le groupe bancaire Société Générale a engagé un plan en trois ans pour se convertir à l’open source. Explications de son directeur technique adjoint.
