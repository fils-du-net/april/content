---
site: Le Soleil
title: "Le bien, le mal, le Web et nous"
author: Pierre Asselin
date: 2017-12-30
href: https://www.lesoleil.com/opinions/editoriaux/le-bien-le-mal-le-web-et-nous-da839ed9ae7276cf220aafd635c62596
tags:
- Internet
- Promotion
---

> L’année 2017 nous a fait prendre encore plus conscience, même si nous le savions déjà, que le Web ne fait aucune distinction entre le bien et le mal. Les outils qui ont permis l’élection du premier président de race noire aux États-Unis se sont ensuite retournés contre son parti pour permettre à Donald Trump de prendre le pouvoir et de démanteler, méthodiquement, l’héritage de son prédécesseur.
