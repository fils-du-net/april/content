---
site: Numerama
title: "Éric Ciotti appelle encore à modifier la Constitution pour pénaliser la lecture de sites terroristes"
author: Julien Lausson
date: 2017-12-20
href: http://www.numerama.com/politique/316187-eric-ciotti-appelle-encore-a-modifier-la-constitution-pour-penaliser-la-consultation-de-sites-terroristes.html
tags:
- Internet
- Institutions
---

> Le député Éric Ciotti a partagé ses regrets face aux décisions du Conseil constitutionnel, qui a notamment rejeté la création du délit de consultation habituelle de sites terroristes. Le parlementaire suggère de modifier la Constitution, une proposition qui n'est pas neuve.
