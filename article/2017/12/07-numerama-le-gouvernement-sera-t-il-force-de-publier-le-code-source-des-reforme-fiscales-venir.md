---
site: Numerama
title: "Le gouvernement sera-t-il forcé de publier le code source des réforme fiscales à venir?"
author: Julien Lausson
date: 2017-12-07
href: http://www.numerama.com/politique/312941-le-gouvernement-sera-t-il-force-de-publier-le-code-source-des-reforme-fiscales-a-venir.html
tags:
- Administration
- Institutions
- Open Data
---

> Sénateur et président de la commission des finances, Vincent Éblé propose un amendement pour le projet de loi de finances pour 2018 qui obligerait le gouvernement à publier le code source de toutes les réformes fiscales futures.
