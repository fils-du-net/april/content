---
site: Numerama
title: "Quels sont les groupes d'études sur le numérique à l'Assemblée nationale?"
author: Julien Lausson
date: 2017-12-22
href: http://www.numerama.com/politique/316968-quels-sont-les-groupes-detudes-sur-le-numerique-a-lassemblee-nationale.html
tags:
- Institutions
---

> Le Bureau de l'Assemblée nationale a approuvé un certain nombre de groupes d'études. Parmi eux, plusieurs concernent directement le numérique.
