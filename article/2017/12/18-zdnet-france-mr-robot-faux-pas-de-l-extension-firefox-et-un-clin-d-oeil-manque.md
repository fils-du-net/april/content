---
site: ZDNet France
title: "Mr. Robot: faux-pas de l'extension Firefox et un clin d'oeil manqué"
author: Christophe Auffray
date: 2017-12-18
href: http://www.zdnet.fr/actualites/mr-robot-faux-pas-de-l-extension-firefox-et-un-clin-d-oeil-manque-39861724.htm
tags:
- Internet
- Vie privée
---

> Dans le cadre d'un partenariat avec Mr. Robot, Mozilla a installé aux US une extension pour Firefox sans demander au préalable l'autorisation des utilisateurs. Si le plugin était inoffensif et une simple référence à la série télé, la démarche a suscité critiques et inquiétudes. Mozilla fait son mea culpa.
