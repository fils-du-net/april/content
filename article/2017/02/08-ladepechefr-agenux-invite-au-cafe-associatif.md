---
site: LaDepeche.fr
title: "aGeNux invité au café associatif"
date: 2017-02-08
href: http://www.ladepeche.fr/article/2017/02/08/2513325-agenux-invite-au-cafe-associatif.html
tags:
- Sensibilisation
- Associations
---

> L'association aGeNUx était invitée dans le cadre des rendez-vous au café associatif qui se déroule chaque dimanche matin à la salle de la mairie à Lusignan-Petit pour présenter l'installation de logiciels libres sous windows.
