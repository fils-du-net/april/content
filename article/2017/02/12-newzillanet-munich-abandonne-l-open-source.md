---
site: NewZilla.net
title: "Munich abandonne l’open-source"
author: Philippe Crouzillacq
date: 2017-02-12
href: https://www.newzilla.net/2017/02/12/munich-abandonne-lopen-source
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
---

> La capitale de la Bavière devrait décider la semaine prochaine d’abandonner les solutions logiciels en open-source déployées depuis 2004 auprès de ses 15 000 collaborateurs et de repasser sous Windows 10, au plus tard en 2021.
