---
site: France 24
title: "CHATONS, ces hébergeurs alternatifs qui ne collectent pas vos données personnelles"
date: 2017-02-04
href: http://mashable.france24.com/tech-business/20170204-chatons-collectif-hebergeurs-framasoft
tags:
- Internet
- Associations
- Informatique en nuage
- Vie privée
---

> Le réseau Framasoft a organisé jeudi à Paris un atelier pour présenter CHATONS, le Collectif des hébergeurs alternatifs, transparents, ouverts, neutres et solidaires. Mashable FR y était.
