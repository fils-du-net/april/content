---
site: Libération
title: "Justice prédictive, l’augure des procédures"
author: Julie Brafman
date: 2017-02-23
href: http://www.liberation.fr/france/2017/02/23/justice-predictive-l-augure-des-procedures_1550628
tags:
- Entreprise
- Institutions
- Innovation
- Open Data
---

> Pronostiquer les décisions de justice grâce à des algorithmes brassant les jurisprudences, c’est l’idée de plusieurs start-up, qui misent sur l’ouverture des données au public. La perspective fascine autant qu’elle inquiète.
