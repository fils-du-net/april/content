---
site: Journal du Net
title: "L'open source veut démocratiser l'intelligence artificielle"
author: Lélia De Matharel
date: 2017-02-02
href: http://www.journaldunet.com/economie/services/1191268-open-source-democratiser-intelligence-artificielle
tags:
- Entreprise
- Innovation
---

> Ces logiciels ouverts et gratuits peuvent être implémentés par de grands groupes mais aussi de petites entreprises qui veulent résoudre des problématiques business précises.
