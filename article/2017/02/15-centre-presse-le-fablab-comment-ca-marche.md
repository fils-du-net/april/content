---
site: Centre Presse
title: "Le FabLab, comment ça marche?"
author: Élisabeth Royez
date: 2017-02-15
href: http://www.centre-presse.fr/article-510363-le-fablab-comment-ca-marche.html
tags:
- Matériel libre
- Innovation
---

> Le FabLab des Usines nouvelles, à Ligugé, est ouvert à tous les publics, des simples curieux aux professionnels, autour de l'impression 3D, les drones, l'informatique, l'électronique, le bricolage...
