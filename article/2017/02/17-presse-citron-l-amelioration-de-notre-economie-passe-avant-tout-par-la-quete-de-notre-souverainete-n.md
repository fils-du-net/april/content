---
site: Presse Citron
title: "L’amélioration de notre économie passe avant tout par la quête de notre Souveraineté Numérique"
author: Gaël Duval
date: 2017-02-17
href: http://www.presse-citron.net/lamelioration-de-economie-passe-quete-de-souverainete-numerique
tags:
- Entreprise
- Économie
- Innovation
---

> Comment reprendre la main sur notre indépendance technologique et numérique, et quels en sont les enjeux?
