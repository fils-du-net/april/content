---
site: Programmez!
title: "Les éditeurs ont-ils tiré les leçons de la faille Heartbleed, ou sont-ils condamnés à répéter les mêmes erreurs?"
author: Christian Hindre
date: 2017-02-21
href: http://www.programmez.com/avis-experts/les-editeurs-ont-ils-tire-les-lecons-de-la-faille-heartbleed-ou-sont-ils-condamnes-repeter-les-memes-25556
tags:
- Entreprise
- Innovation
---

> Comment oublier Heartbleed: il y a quelques années, cette vulnérabilité présente dans la bibliothèque de cryptographie open source OpenSSL a soulevé un véritable vent de panique dans l'industrie des logiciels et au sein des entreprises du monde entier. À l'époque, les éditeurs (et donc leurs clients) en savaient trop peu sur les composants open source utilisés dans leurs propres produits pour déterminer si leurs logiciels étaient vulnérables.
