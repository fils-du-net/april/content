---
site: NewZilla.net
title: "Munich sur le point d’abandonner l’open-source"
author: Philippe Crouzillacq
date: 2017-02-15
href: https://www.newzilla.net/2017/02/15/munich-abandonne-lopen-source
tags:
- Logiciels privateurs
- Administration
- International
---

> Au cours d’une réunion qui s’est tenue mercredi 15 février, les actuels dirigeants politiques (CDU-SPD) de capitale de la Bavière ont fait un pas supplémentaire vers l’abandon de l’open-source au sein de la collectivité locale et vers son remplacement par Windows 10.
