---
site: Mediapart
title: " Continuons à Dégoogliser Internet avec Framasoft"
author: Jean-Pierre Favier
date: 2017-02-06
href: https://blogs.mediapart.fr/jean-pierre-favier/blog/060217/continuons-degoogliser-internet-avec-framasoft
tags:
- Institutions
- Associations
---

> En 2014, l'association Framasoft a lancé une initiative planifiée sur trois ans pour Dégoogliser Internet: proposer une trentaine d’alternatives «Libres, Éthiques, Décentralisées et Solidaires» aux services des multinationales GAFAM (Google, Apple, Facebook, Amazon et Microsoft). Certains candidats à la présidentielle ont intégré cette thématique dans leur projet et parlent de logiciels libres.
