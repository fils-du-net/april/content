---
site: Programmez!
title: "Munich: Microsoft Office vs The Document Foundation, match retour"
author: Francois Tonic
date: 2017-02-15
href: http://www.programmez.com/actualites/munich-microsoft-office-vs-document-foundation-match-retour-25534
tags:
- Logiciels privateurs
- Administration
- International
---

> Décidemment la saga bureautique de la ville de Munich n’en finit pas! Le projet de migration de Microsoft Office vers une solution open source de bureautique, OpenOffice, remonte à 2003. Le budget voté à l’époque n’était pas anodin : +30 millions € pour migrer la bureautique et de système d’exploitation. Le projet a duré 10 ans. Mais durant l’été 2014, et avec l’arrivée de nouveaux responsables politiques, le projet a été remis en question: sortir de Linux et revenir à Windows et MS Office… Ce rétropédalage serait dû aux plaintes d’utilisateurs et des problèmes de transitions.
