---
site: Libération
title: "Logiciel libre: à la conquête du grand public"
author: Amaelle Guiton
date: 2017-02-06
href: http://www.liberation.fr/futurs/2017/02/06/logiciel-libre-a-la-conquete-du-grand-public_1546749
tags:
- Institutions
- Innovation
- Promotion
- Informatique en nuage
---

> Comme chaque année depuis 2000, les «libristes» se sont retrouvés ce week-end à Bruxelles. Même si des géants comme Microsoft ou Google s’y sont partiellement convertis, beaucoup reste à faire pour sortir de leur logique purement commerciale.
