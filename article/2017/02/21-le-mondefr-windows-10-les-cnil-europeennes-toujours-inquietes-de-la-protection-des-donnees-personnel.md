---
site: Le Monde.fr
title: "Windows 10: les CNIL européennes toujours «inquiètes» de la protection des données personnelles"
date: 2017-02-21
href: http://www.lemonde.fr/pixels/article/2017/02/21/windows-10-les-cnil-europeennes-toujours-inquietes-de-la-protection-des-donnees-personnelles_5082963_4408996.html
tags:
- Logiciels privateurs
- Institutions
- Europe
- Vie privée
---

> Epinglé à plusieurs reprises pour l’étendue des données collectées et analysées par son système d’exploitation, Microsoft a annoncé des modifications en janvier. Qui n’ont pas suffi à convaincre entièrement les gendarmes de la vie privée.
