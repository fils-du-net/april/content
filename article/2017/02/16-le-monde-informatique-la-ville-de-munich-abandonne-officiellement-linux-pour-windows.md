---
site: Le Monde Informatique
title: "La ville de Munich abandonne officiellement Linux pour Windows"
author: Martin Bayer
date: 2017-02-16
href: http://www.lemondeinformatique.fr/actualites/lire-la-ville-de-munich-abandonne-effectivement-linux-pour-windows-67387.html
tags:
- Logiciels privateurs
- Administration
- International
---

> Le conseil municipal de Munich, en Bavière, a voté hier un retour à Windows sur les postes de travail de la ville. Depuis deux ans, des critiques s'élevaient sur l'utilisation de Linux et d'applications open source sur une grande partie du parc informatique municipal, alors que de nombreuses applications spécifiques utilisées par la ville fonctionnent sous Windows.
