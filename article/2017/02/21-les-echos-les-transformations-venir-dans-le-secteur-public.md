---
site: Les Echos
title: "Les transformations à venir dans le secteur public"
author: Stéphane Mahmoudi
date: 2017-02-21
href: http://www.lesechos.fr/idees-debats/cercle/cercle-166547-les-transformations-a-venir-dans-le-secteur-public-2066649.php
tags:
- Administration
- Innovation
- Open Data
---

> Les services informatiques du secteur public ont pour objectif de rendre plus efficaces les institutions, de mettre à niveau les organisations publiques avec les technologies d’aujourd’hui, mais aussi d’améliorer la transparence des données des citoyens en donnant accès à l’information résultante (Open Data) sans coût d’accès et en toute confidentialité pour les informations.
