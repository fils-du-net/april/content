---
site: "Génération-NT"
title: "L'utilisation des logiciels open source encouragée dans les services publics"
author: Christopher Potter
date: 2017-02-12
href: http://www.generation-nt.com/logiciels-open-source-services-publics-actualite-1939112.html
tags:
- Entreprise
- Administration
- Institutions
- Marchés publics
---

> Jusqu’à présent, les collectivités et les administrations françaises utilisaient en grande majorité des outils Microsoft, notamment Office et Sharepoint. Aujourd’hui, dans un souci d’économie et d’efficacité, elles sont sont invitées à passer à l’open source.
