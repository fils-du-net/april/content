---
site: ZDNet
title: "Munich: vers un retour du parc Linux sous Windows?"
author: Louis Adam
date: 2017-02-13
href: http://www.zdnet.fr/actualites/munich-vers-un-retour-du-parc-linux-sous-windows-39848464.htm#xtor%3DRSS-1
tags:
- Logiciels privateurs
- Administration
- Vente liée
- International
---

> La ville de Munich, connue pour avoir passé en 2004 l’ensemble de son parc de machines sous une distribution Linux, pourrait bien faire marche arrière. Un vote doit avoir lieu cette semaine pour déterminer l’avenir du projet.
