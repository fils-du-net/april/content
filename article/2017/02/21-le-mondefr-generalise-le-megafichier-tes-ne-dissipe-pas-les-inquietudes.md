---
site: Le Monde.fr
title: "Généralisé, le «mégafichier» TES ne dissipe pas les inquiétudes"
author: Martin Untersinger
date: 2017-02-21
href: http://www.lemonde.fr/pixels/article/2017/02/21/generalise-le-megafichier-tes-ne-dissipe-pas-les-inquietudes_5082696_4408996.html
tags:
- Institutions
- Vie privée
---

> Rassemblant à terme toutes les empreintes biométriques des Français, il est déployé mardi à Paris, puis le sera dans tout le pays d’ici à la fin du mois de mars.
