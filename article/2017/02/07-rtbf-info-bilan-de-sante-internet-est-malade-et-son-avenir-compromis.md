---
site: RTBF Info
title: "Bilan de santé: internet est malade et son avenir compromis"
author: Jean-Claude Verset
date: 2017-02-07
href: http://www.rtbf.be/info/medias/detail_bilan-de-sante-internet-est-malade-et-son-avenir-compromis?id=9523958
tags:
- Internet
- Institutions
- Droit d'auteur
- Neutralité du Net
- Vie privée
---

> Il parait qu’internet va mal. De mauvaises langues prétendent même qu’il va mourir. Réalité, jalousie, stratégie ou simple crise d’internet bashing? C’est Mozilla qui lance le débat en publiant cette sorte de bulletin de santé de la situation d'internet.
