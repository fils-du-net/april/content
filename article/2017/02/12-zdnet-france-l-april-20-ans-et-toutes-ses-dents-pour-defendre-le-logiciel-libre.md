---
site: ZDNet France
title: "L'April a 20 ans, et toutes ses dents pour défendre le logiciel libre"
author: Thierry Noisette
date: 2017-02-12
href: http://www.zdnet.fr/blogs/l-esprit-libre/l-april-a-20-ans-et-toutes-ses-dents-pour-defendre-le-logiciel-libre-39848430.htm
tags:
- April
---

> L'association libriste (4.090 adhérents, dont près de 400 personnes morales) fait le point avec nous à l'occasion de ses 20 ans: interview de Frédéric Couchet, délégué général de l'April.
