---
site: Métropolitain
title: "Découvrez les logiciels libres dans les médiathèques de la Métropole"
author: Cédric Nithard
date: 2017-02-04
href: http://e-metropolitain.fr/2017/02/04/decouvrez-les-logiciels-libres-dans-les-mediatheques-de-la-metropole
tags:
- Administration
- Promotion
---

> Cette année, le réseau des médiathèques de Montpellier Méditerranée Métropole propose un nouveau programme sur mesure dédié à la découverte des logiciels libres.
