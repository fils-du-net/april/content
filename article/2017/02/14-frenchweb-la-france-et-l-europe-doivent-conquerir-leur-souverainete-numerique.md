---
site: FrenchWeb
title: "La France et l’Europe doivent conquérir leur souveraineté numérique"
author: contributeur
date: 2017-02-14
href: http://www.frenchweb.fr/la-france-et-leurope-doivent-conquerir-leur-souverainete-numerique/279951
tags:
- Entreprise
- Économie
- Innovation
---

> La plupart de ces acteurs majeurs du numérique collaborent, en particulier avec le gouvernement américain ou la NSA.
