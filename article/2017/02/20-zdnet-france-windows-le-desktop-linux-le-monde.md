---
site: ZDNet France
title: "A Windows le desktop, à Linux le monde"
author: Steve Ranger
date: 2017-02-20
href: http://www.zdnet.fr/actualites/a-windows-le-desktop-a-linux-le-monde-39848762.htm
tags:
- Entreprise
- Logiciels privateurs
- Administration
---

> Ville symbole pour son projet de bureau Linux, Munich envisage désormais de revenir à Windows. Un revers pour Linux? Peut-être... sauf que le destin de Linux n'est plus lié au seul PC.
