---
site: Journal du Net
title: "L'évolution du modèle des logiciels libres, source d'inspiration pour la stratégie IT de l'entreprise"
author: Romain Le Merlus
date: 2017-02-07
href: http://www.journaldunet.com/solutions/expert/66315/l-evolution-du-modele-des-logiciels-libres--source-d-inspiration-pour-la-strategie-it-de-l-entreprise.shtml
tags:
- Entreprise
- Sensibilisation
- Innovation
---

> Des tout premiers ordinateurs livrés avec leurs sources à la révolution numérique portée par le cloud, la puissance d’innovation du logiciel libre a toujours été l’un des terreaux les plus fertiles pour la mise en œuvre de stratégies IT innovantes et pertinentes.
