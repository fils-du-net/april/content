---
site: Silicon
title: "Linus Torvalds remballe les discours sur l’innovation"
author: Jacques Cheminat
date: 2017-02-17
href: http://www.silicon.fr/linus-torvalds-remballe-les-discours-sur-linnovation-168971.html
tags:
- Innovation
---

> Connu pour son franc-parler, le fondateur de Linux considère les discours sur l’innovation technologique comme des foutaises.
