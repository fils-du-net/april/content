---
site: Alter1fo
title: "Interview du collectif CryptoRennes: «La notion de vie privée sur Internet existe(ra)-t-elle encore?»"
date: 2017-02-27
href: http://alter1fo.com/cryptoparty-rennes-cryptorennes-108066
tags:
- Internet
- Associations
- Vie privée
---

> A l’heure de l’hyperconnectivité et des outils 2.0, les technologies numériques se sont invitées dans tous les aspects de nos vies. Travailler, s’informer, jouer, vivre ses amitiés, se soigner, consommer, ou encore se révolter…. autant d’activités qui passent dorénavant par l’usage d’ordinateurs, de tablettes ou de smartphones. Mais quel contrôle avons-nous vraiment sur ces machines devenues omniprésentes?
