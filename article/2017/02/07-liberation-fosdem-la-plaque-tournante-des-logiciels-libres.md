---
site: Libération
title: "Fosdem: la plaque tournante des logiciels libres"
author: Sébastien Van Malleghem
date: 2017-02-07
href: http://www.liberation.fr/photographie/2017/02/07/fosdem-la-plaque-tournante-des-logiciels-libres_1546644
tags:
- Promotion
---

> Chaque année, les développeurs se retrouvent à Bruxelles pour la Réunion européenne des développeurs de logiciels libres et open source (Fosdem, en anglais). Si le partage du code s'est répandu à tous les niveaux, et si les géants du numérique s'y sont en partie convertis, beaucoup reste à faire pour toucher le grand public. Mais les scandales de surveillance, et les débats sur la transparence des algorithmes et la souveraineté technologique pourraient faire bouger les lignes.
