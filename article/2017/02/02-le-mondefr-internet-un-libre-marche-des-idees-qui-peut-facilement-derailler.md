---
site: Le Monde.fr
title: "«Internet, un libre marché des idées qui peut facilement dérailler»"
author: William Audureau
date: 2017-02-02
href: http://www.lemonde.fr/pixels/article/2017/02/02/internet-un-libre-marche-des-idees-qui-peut-facilement-derailler_5073445_4408996.html
tags:
- Internet
- Partage du savoir
- Innovation
- Sciences
---

> Pour Benjamin Loveluck, chercheur au CERSA et à Télécom ParisTech, le succès des «fake-news» dérive des inspirations libérales du Web et de ses modes d’organisation.
