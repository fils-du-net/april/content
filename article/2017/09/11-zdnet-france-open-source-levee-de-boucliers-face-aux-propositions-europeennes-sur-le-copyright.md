---
site: ZDNet France
title: "Open source: levée de boucliers face aux propositions européennes sur le copyright"
author: Louis Adam
date: 2017-09-11
href: http://www.zdnet.fr/actualites/open-source-levee-de-boucliers-face-aux-propositions-europeennes-sur-le-copyright-39857102.htm
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Le texte envisage d’obliger les hébergeurs à mettre en place un filtre automatique afin d’empêcher la mise en ligne de contenus protégés par le droit d’auteur.
