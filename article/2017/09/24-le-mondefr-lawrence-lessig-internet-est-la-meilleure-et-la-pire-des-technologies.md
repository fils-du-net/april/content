---
site: Le Monde.fr
title: "Lawrence Lessig: «Internet est la meilleure et la pire des technologies»"
author: Claire Legros
date: 2017-09-24
href: http://www.lemonde.fr/festival/article/2017/09/24/lawrence-lessig-internet-est-la-meilleure-et-la-pire-des-technologies_5190457_4415198.html
tags:
- Internet
- Institutions
---

> Peut-on réguler Internet? La question, devenue centrale dans les sociétés démocratiques connectées, était au cœur de la rencontre avec Lawrence Lessig au Monde Festival, dimanche 24 septembre.
