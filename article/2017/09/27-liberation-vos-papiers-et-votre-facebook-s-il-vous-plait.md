---
site: Libération
title: "Vos papiers et votre Facebook, s'il vous plaît!"
author: Amaelle Guiton
date: 2017-09-27
href: http://www.liberation.fr/france/2017/09/27/vos-papiers-et-votre-facebook-s-il-vous-plait_1599383
tags:
- Internet
- Institutions
- Vie privée
---

> Lors de l'examen du projet de loi antiterroriste, les députés ont adopté une obligation pour des personnes suspectées de déclarer l'ensemble de leurs identifiants électroniques. Une mesure que le Sénat avait pourtant écartée en juillet.
