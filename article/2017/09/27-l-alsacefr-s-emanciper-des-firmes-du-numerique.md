---
site: L'Alsace.fr
title: "S’émanciper des firmes du numérique"
author: Véronique Berkani
date: 2017-09-27
href: http://www.lalsace.fr/haut-rhin/2017/09/27/s-emanciper-des-firmes-du-numerique
tags:
- Administration
- Associations
- Promotion
---

> Si vous faites partie de ceux qui pensent que les logiciels libres sont la cour de récré de quelques mordus d’informatique, il se pourrait que vous changiez d’avis. Le luxe de s’affranchir de Microsoft ou d’Apple se démocratise et franchit les frontières des petits cercles de geeks. Exemple au Club Linux de Wintzenheim.
