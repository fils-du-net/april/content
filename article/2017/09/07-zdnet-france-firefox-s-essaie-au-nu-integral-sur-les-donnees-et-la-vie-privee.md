---
site: ZDNet France
title: "Firefox s'essaie au nu intégral sur les données et la vie privée"
author: Christophe Auffray
date: 2017-09-07
href: http://www.zdnet.fr/actualites/firefox-s-essaie-au-nu-integral-sur-les-donnees-et-la-vie-privee-39856962.htm
tags:
- Internet
- Vie privée
---

> La confidentialité, c'est un sujet sacré, voire une valeur fondamentale pour un navigateur comme Firefox. Mais le logiciel a aussi besoin des données des utilisateurs. Comment concilier les deux? Par la transparence et la clarté. S'agit-il d'une première étape avant l'activation par défaut de la télémétrie?
