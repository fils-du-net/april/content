---
site: Le Courrier
title: "Informatique: l’Etat client des multinationales"
author: Philippe Bach
date: 2017-09-28
href: https://www.lecourrier.ch/152983/informatique_l_etat_client_des_multinationales
tags:
- Entreprise
- Économie
- Marchés publics
- International
---

> Les appels d’offre des collectivités publiques sont réservés aux fabricants d’ordinateurs. Et ils favorisent Microsoft. L’ex-conseiller d’Etat François Marthaler s’y est cassé les dents.
