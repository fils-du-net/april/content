---
site: Les Echos
title: "Licences logicielles: la grogne monte"
author: Jean-Paul Argudo
date: 2017-09-21
href: https://www.lesechos.fr/idees-debats/cercle/cercle-173916-licences-logicielles-la-grogne-monte-2115994.php
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Licenses
---

> Tandis que les usages évoluent, les pratiques relatives aux licences logicielles héritées du passé restent de mise. À l’heure du "tout numérique", les modèles de tarification reposant sur des métriques amenées à évoluer rapidement et drastiquement font peser sur les entreprises des risques aussi sévères qu’inutiles, puisqu’il existe des alternatives.
