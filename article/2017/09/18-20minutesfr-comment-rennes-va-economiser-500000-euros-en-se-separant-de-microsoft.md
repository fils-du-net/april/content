---
site: 20minutes.fr
title: "Comment Rennes va économiser 500.000 euros en se séparant de Microsoft"
date: 2017-09-18
href: http://www.20minutes.fr/rennes/2135135-20170918-comment-rennes-va-economiser-400000-euros-separant-microsoft
tags:
- Logiciels privateurs
- Administration
- Économie
---

> C’est une petite révolution que vont connaître les agents de la ville et de Rennes Métropole. D’ici un mois, ils verront tous leur service de boîte mail changer de serveur et de logiciel. «Nous allons économiser 500.000 euros en boulant Microsoft», glisse Matthieu Theurier, chef de file du groupe écologiste.
