---
site: ZDNet France
title: "Oracle: vers une spin off de Java EE à la fondation Eclipse"
author: Steven J. Vaughan-Nichols
date: 2017-09-13
href: http://www.zdnet.fr/actualites/oracle-vers-une-spin-off-de-java-ee-a-la-fondation-eclipse-39857218.htm
tags:
- Entreprise
- Associations
---

> Oracle confirme qu'il se sépare de Java EE au profit d'une fondation open source appuyée par IBM et Red Hat. "Après un examen minutieux, nous avons choisi la Fondation Eclipse" affirme Oracle.
