---
site: KultureGeek
title: "Doucement mais sûrement, Linux voit sa part de marché augmenter"
author: Jean-Baptiste A.
date: 2017-09-01
href: http://kulturegeek.fr/news-119760/doucement-surement-linux-voit-part-marche-augmenter
tags:
- Internet
- Sensibilisation
---

> Linux était un système d’exploitation utilisé par peu de personnes pendant plusieurs années, mais il semblerait que l’alternative à Windows et macOS commence à avoir du succès. C’est encore léger par rapport aux systèmes de Microsoft et Apple, mais la part a bien augmenté au fil des années.
