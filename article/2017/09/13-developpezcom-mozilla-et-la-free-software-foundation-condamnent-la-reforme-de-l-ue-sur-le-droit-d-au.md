---
site: Developpez.com
title: "Mozilla et la Free Software Foundation condamnent la réforme de l'UE sur le droit d'auteur"
author: Michael Guilloux
date: 2017-09-13
href: https://www.developpez.com/actu/160091/Mozilla-et-la-Free-Software-Foundation-condamnent-la-reforme-de-l-UE-sur-le-droit-d-auteur-qui-propose-un-filtrage-massif-de-contenu-sur-Internet
tags:
- Internet
- Associations
- Droit d'auteur
- Europe
---

> Au début de cette année, des informations ont fuité sur la fameuse Directive du Parlement européen et du Conseil sur le copyright, un projet de réforme des droits d’auteur dans le cadre du marché numérique unique. Deux articles dans le texte de l’UE avaient particulièrement provoqué pas mal de remous. Le premier, l’article 11, traitait du droit de reproduction des publications de presse et de les rendre accessibles au public.
