---
site: Developpez.com
title: "Linux franchit la barre des 3 % sur les PC et se rapproche de macOS (5,84 %)"
author: Olivier Famien
date: 2017-09-01
href: https://www.developpez.com/actu/158200/Linux-franchit-la-barre-des-3-pourcent-sur-les-PC-et-se-rapproche-de-macOS-5-84-pourcent-les-Linuxiens-peuvent-ils-rever-a-la-seconde-place-apres-cet-exploit
tags:
- Internet
- Promotion
---

> L’an dernier, plus précisément au mois de juin 2016, la famille des systèmes d’exploitation Linux avait fait un bond remarquable en dépassant la barre des 2 %, selon les statistiques de Net Applications, un des cabinets de référence en matière d’édition de statistiques pour différentes technologies comme les navigateurs web, les moteurs de recherche, les médias sociaux et les systèmes d’exploitation.
