---
site: Numerama
title: "Une campagne veut rendre public le code source des logiciels financés par le contribuable"
author: Julien Lausson
date: 2017-09-15
href: http://www.numerama.com/politique/289690-une-campagne-appelle-a-rendre-public-le-code-source-des-logiciels-finances-par-le-contribuable.html
tags:
- Administration
- April
- Associations
- Marchés publics
---

> Des organisations proches du logiciel libre lancent une campagne de sensibilisation appelant à rendre public le code source financé par le contribuable.
