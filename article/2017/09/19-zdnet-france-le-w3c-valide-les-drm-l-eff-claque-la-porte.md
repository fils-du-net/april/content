---
site: ZDNet France
title: "Le W3C valide les DRM, l’EFF claque la porte"
author: Louis Adam
date: 2017-09-19
href: http://www.zdnet.fr/actualites/le-w3c-valide-les-drm-l-eff-claque-la-porte-39857488.htm#xtor
tags:
- Internet
- Associations
- DRM
- Standards
---

> Après une longue polémique, le W3C a finalement voté en faveur de l’implémentation des DRM au sein des standards web. Une décision qui ne fait pas l’unanimité. L’Electronic Frontier Foundation annonce ainsi son départ de l’organisme de standardisation du web suite à cette décision
