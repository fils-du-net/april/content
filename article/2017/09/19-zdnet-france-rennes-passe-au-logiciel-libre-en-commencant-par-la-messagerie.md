---
site: ZDNet France
title: "Rennes passe au logiciel libre, en commençant par la messagerie"
author: Thierry Noisette
date: 2017-09-19
href: http://www.zdnet.fr/blogs/l-esprit-libre/rennes-passe-au-logiciel-libre-en-commencant-par-la-messagerie-39857520.htm
tags:
- Logiciels privateurs
- Administration
- Économie
---

> La cité bretonne compte économiser un demi-million en passant d'Outlook à Zimbra, et va étudier le passage en logiciels bureautiques libres.
