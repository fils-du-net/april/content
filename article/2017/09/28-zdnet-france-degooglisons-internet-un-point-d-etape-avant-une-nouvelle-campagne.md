---
site: ZDNet France
title: "Degooglisons Internet: un point d’étape avant une nouvelle campagne"
author: Louis Adam
date: 2017-09-28
href: http://www.zdnet.fr/actualites/degooglisons-internet-un-point-d-etape-avant-une-nouvelle-campagne-39857954.htm
tags:
- Internet
- Associations
- Vie privée
---

> L’association Framasoft veut faire le point après trois dans de campagne Degooglisons Internet. L’association s’était déjà félicitée du succès surprise de cette initiative, qui avait notamment permis de remettre Framasoft à flot. Ses membres entendent maintenant consolider leurs appuis.
