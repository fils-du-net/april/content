---
site: Le Monde.fr
title: "Après Irma, des internautes mobilisés pour cartographier en urgence les zones dévastées"
date: 2017-09-08
href: http://www.lemonde.fr/pixels/article/2017/09/08/apres-irma-des-internautes-mobilises-pour-cartographier-en-urgence-les-zones-devastees_5182936_4408996.html
tags:
- Internet
- Partage du savoir
- Associations
---

> En quelques heures, les cartes des îles frappées par l’ouragan ont été actualisées par la communauté OpenStreetMap. Un travail précieux pour les secours.
