---
site: Les Inrocks
title: "”Nothing to Hide”: pourquoi ce docu sur la surveillance de masse nous concerne-t-il tous?"
author: Bruno Deruisseau
date: 2017-09-09
href: http://www.lesinrocks.com/2017/09/09/cinema/nothing-hide-pourquoi-ce-docu-sur-la-surveillance-de-masse-nous-concerne-t-il-tous-11983255
tags:
- Internet
- Institutions
- Contenus libres
- Vie privée
---

> Grâce à la collecte de nos données numériques, les agences de renseignement disposent aujourd'hui d'un accès quasi-total à notre intimité. Sorti cette semaine, "Nothing to Hide" propose, en évitant l'écueil de la paranoïa, une prise de conscience des enjeux de la surveillance de masse et des moyens dont chacun dispose pour s'y soustraire.
