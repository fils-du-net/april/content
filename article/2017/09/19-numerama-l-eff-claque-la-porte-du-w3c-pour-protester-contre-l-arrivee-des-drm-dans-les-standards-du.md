---
site: Numerama
title: "L'EFF claque la porte du W3C pour protester contre l'arrivée des DRM dans les standards du web"
author: Julien Lausson
date: 2017-09-19
href: http://www.numerama.com/tech/290706-leff-claque-la-porte-du-w3c-pour-protester-contre-larrivee-des-drm-dans-les-standards-du-web.html
tags:
- Internet
- Associations
- DRM
- Standards
---

> L'association américaine EFF, qui défend les libertés dans l'espace numérique, a pris la décision de quitter l'organisation du W3C, qui s'occupe de la standardisation du web, afin de protester contre l'arrivée des DRM dans les standards du web. Elle s'en explique dans une lettre ouverte.
