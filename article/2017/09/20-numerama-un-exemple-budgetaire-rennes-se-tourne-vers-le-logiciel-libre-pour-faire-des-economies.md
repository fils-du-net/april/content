---
site: Numerama
title: "Un exemple budgétaire? Rennes se tourne vers le logiciel libre pour faire des économies"
author: Julien Lausson
date: 2017-09-20
href: http://www.numerama.com/politique/290977-un-exemple-budgetaire-rennes-se-tourne-vers-le-logiciel-libre-pour-faire-des-economies.html
tags:
- Logiciels privateurs
- Administration
- Économie
---

> Rennes va entamer une transition vers le logiciel libre. La messagerie Outlook va être abandonnée au profit de Zimbra. La ville estime que cela lui fera des économies de 500 000 euros. Elle pourrait en outre poursuivre l'an prochain avec la suite bureautique.
