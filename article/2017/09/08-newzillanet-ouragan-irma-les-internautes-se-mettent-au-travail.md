---
site: NewZilla.net
title: "Ouragan Irma / Les internautes se mettent au travail"
date: 2017-09-08
href: https://www.newzilla.net/2017/09/08/ouragan-irma-les-internautes-se-mettent-au-travail
tags:
- Internet
- Partage du savoir
- Associations
---

> Depuis le passage d’Irma sur les Antilles, des volontaires bénévoles de la communauté de cartographie open-source, OpenStreetMap, se relaient pour recenser les routes coupées, les îles touchées, les infrastructures détruites par l’ouragan.
