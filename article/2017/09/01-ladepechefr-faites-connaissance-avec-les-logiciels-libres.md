---
site: LaDepeche.fr
title: "Faites connaissance avec les logiciels libres"
date: 2017-09-01
href: http://www.ladepeche.fr/article/2017/09/01/2636915-faites-connaissance-avec-les-logiciels-libres.html
tags:
- Sensibilisation
- Associations
---

> Les objectifs de L'association sont toujours les mêmes: faire découvrir les logiciels libres et offrir aux membres une alternative gratuite et fiable à Microsoft Windows.
