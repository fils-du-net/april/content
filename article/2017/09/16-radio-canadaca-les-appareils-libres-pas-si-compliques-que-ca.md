---
site: "Radio-Canada.ca"
title: "Les appareils libres, pas si compliqués que ça"
date: 2017-09-16
href: http://ici.radio-canada.ca/premiere/emissions/la-sphere/segments/entrevue/38612/appareils-libres-linux-purism-ordinateur-portable
tags:
- Innovation
- Promotion
---

> Le monde des appareils et des logiciels libres a souvent mauvaise presse, notamment en ce qui a trait à sa facilité d'utilisation. Mais pour Jean-François Fortin Tam, spécialiste de ces systèmes alternatifs, cette réputation n'est pas toujours méritée, et la population gagnerait à essayer les services libres.
