---
site: Libération
title: "Les libertés publiques cédées aux algorithmes"
author: Amaelle Guiton
date: 2017-09-29
href: http://www.liberation.fr/planete/2017/09/29/les-libertes-publiques-cedees-aux-algorithmes_1599911
tags:
- Entreprise
- Internet
- Associations
---

> Accusés de laisser diffuser des appels à la haine ou, au contraire, d’arbitrairement censurer des contenus, les géants du Web sortent timidement  de leur déni. Mais comment répondre à des injonctions contradictoires?
