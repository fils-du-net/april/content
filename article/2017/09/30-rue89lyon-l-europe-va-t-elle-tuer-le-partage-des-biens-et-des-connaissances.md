---
site: Rue89Lyon
title: "L’Europe va-t-elle tuer le partage des biens et des connaissances?"
date: 2017-09-30
href: http://www.rue89lyon.fr/2017/09/30/leurope-va-t-elle-tuer-le-partage-des-biens-et-des-connaissances
tags:
- Partage du savoir
- Europe
---

> Judith Rochfeld est agrégée des Facultés de droit et professeur de droit privé à l’École de droit de la Sorbonne, Université Panthéon-Sorbonne (Paris 1). Depuis 2007, elle s’intéresse particulièrement au renouvellement des figures de la propriété et à l’exploration des formes alternatives d’accès aux utilités des biens.
