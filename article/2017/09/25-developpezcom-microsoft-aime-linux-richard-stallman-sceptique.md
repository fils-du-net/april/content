---
site: Developpez.com
title: "Microsoft aime Linux: Richard Stallman sceptique"
author: Michael Guilloux
date: 2017-09-25
href: https://www.developpez.com/actu/162203/Microsoft-aime-Linux-Richard-Stallman-sceptique-contrairement-au-fondateur-de-Canonical-qui-a-deja-accepte-de-travailler-avec-Microsoft
tags:
- Entreprise
- Philosophie GNU
---

> Microsoft a emprunté une nouvelle direction avec l’arrivée de Satya Nadella à la tête du géant du logiciel. Avant lui, Steve Ballmer avait mené une lutte féroce contre Linux et le monde open source. Dans cette guerre, l’ancien PDG de Microsoft avait même comparé Linux à un «cancer qui se fixe, au sens de la propriété intellectuelle, à tout ce qu’il touche». Steve Ballmer disait cela en soutenant que «si vous utilisez un logiciel open source, vous devez rendre le reste de votre logiciel open source».
