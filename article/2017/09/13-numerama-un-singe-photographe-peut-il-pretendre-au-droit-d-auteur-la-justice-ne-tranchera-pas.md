---
site: Numerama
title: "Un singe photographe peut-il prétendre au droit d'auteur? La justice ne tranchera pas"
author: Julien Lausson
date: 2017-09-13
href: http://www.numerama.com/pop-culture/288913-un-singe-photographe-peut-il-pretendre-au-droit-dauteur-la-justice-ne-tranchera-pas.html
tags:
- Institutions
- Droit d'auteur
---

> On ne connaîtra pas le mot de la fin, sur le terrain juridique, de l'affaire du singe ayant fait un autoportrait avec un appareil photo. Alors que l'affaire devait aller en cour d'appel, un accord à l'amiable a été trouvé.
