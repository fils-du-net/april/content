---
site: "les-infostrateges.com"
title: "Sur quoi reposent nos infrastructures numériques? Le travail invisible des faiseurs du web"
author: Fabrice Molinaro
date: 2017-09-25
href: http://www.les-infostrateges.com/actu/17092434/sur-quoi-reposent-nos-infrastructures-numeriques-le-travail-invisible-des-faiseurs-du-web
tags:
- Internet
- Sensibilisation
---

> Le site OpenEdition Press propose de consulter une publication intitulée "Sur quoi reposent nos infrastructures numériques? Le travail invisible des faiseurs du web".
