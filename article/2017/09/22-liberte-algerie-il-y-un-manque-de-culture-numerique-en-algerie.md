---
site: Liberté Algérie
title: "«Il y a un manque de culture numérique en Algérie»"
author: Mohamed-Chérif Lachichi
date: 2017-09-22
href: http://www.liberte-algerie.com/magazine/il-y-a-un-manque-de-culture-numerique-en-algerie-277475
tags:
- Associations
- Promotion
- International
- Vie privée
---

> En prenant le parti de la vulgarisation et du partage informatique, la chaîne «Zenzla» s’intéresse aux logiciels libres et la protection de la vie privée. De même qu’elle aborde des sujets trés «Geek» comme les séries et autres jeux vidéo. Avec un soupçon d’accent annabi, la langue algérienne (Darija) sur la chaîne «Zenzla» est à l’honneur. Intrigués par cette démarche originale, mais aussi par le profil atypique du podcasteur, nous avons contacté l’animateur de «Zenzla» pour une interview qu’il a gentiment accepté.
