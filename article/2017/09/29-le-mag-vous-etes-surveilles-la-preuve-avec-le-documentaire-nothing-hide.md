---
site: UP le mag
title: "Vous êtes surveillés, la preuve avec le documentaire «Nothing to hide»"
author: Mounir Belhidaoui
date: 2017-09-29
href: http://www.up-inspirer.fr/37346-vous-etes-surveilles-la-preuve-avec-le-documentaire-nothing-to-hide
tags:
- Internet
- Partage du savoir
- Associations
- Vie privée
---

> Le documentaire «Nothing to hide» est diffusé en salles depuis quelques mois, avant une mise en ligne sous licence CC prévue pour le 30 septembre.
