---
site: Numerama
title: "Face aux menaces de Macron sur le chiffrement, le CNNum défend ce «rempart contre l’arbitraire des États»"
author: Julien Lausson
date: 2017-09-12
href: http://www.numerama.com/politique/288407-face-aux-menaces-de-macron-sur-le-chiffrement-le-cnnum-defend-ce-rempart-contre-larbitraire-des-etats.html
tags:
- Internet
- Institutions
- Vie privée
---

> Le Conseil national du numérique (CNNum) tente de freiner les ardeurs d'Emmanuel Macron sur le chiffrement. Ce mardi 12 septembre, l'instance consultative vient de remettre un avis dans lequel il présente le chiffrement comme un «rempart contre la surveillance de masse».
