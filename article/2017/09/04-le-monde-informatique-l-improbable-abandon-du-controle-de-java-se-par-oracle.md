---
site: Le Monde Informatique
title: "L'improbable abandon du contrôle de Java SE par Oracle"
author: Paul Krill
date: 2017-09-04
href: http://www.lemondeinformatique.fr/actualites/lire-l-improbable-abandon-du-controle-de-java-se-par-oracle-69239.html
tags:
- Entreprise
- Associations
---

> Si Oracle a indiqué étudié le transfert de Java EE vers une fondation open source, la question se pose également pour Java SE. Et la position de big red est ici très différente.
