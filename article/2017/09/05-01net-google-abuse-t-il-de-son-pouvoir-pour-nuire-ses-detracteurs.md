---
site: 01net.
title: "Google abuse-t-il de son pouvoir pour nuire à ses détracteurs?"
author: Gilbert Kallenborn
date: 2017-09-05
href: http://www.01net.com/actualites/google-abuse-t-il-de-son-pouvoir-pour-nuire-a-ses-detracteurs-1249695.html
tags:
- Entreprise
- Internet
- Vie privée
---

> La société Vivaldi a été éjectée du service Google AdWords après que son PDG a critiqué ouvertement Google pour sa boulimie de données personnelles. Les histoires de ce type se multiplient ces derniers temps.
