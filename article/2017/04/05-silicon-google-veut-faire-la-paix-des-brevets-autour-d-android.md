---
site: Silicon
title: "Google veut faire la paix des brevets autour d'Android"
author: Christophe Lagane
date: 2017-04-05
href: http://www.silicon.fr/google-paix-brevets-android-171489.html
tags:
- Entreprise
- Brevets logiciels
- Innovation
---

> AvecPax, Google invite les constructeurs d'appareils Android à partager leurs brevets entre eux. Pour mieux se protéger de Microsoft?
