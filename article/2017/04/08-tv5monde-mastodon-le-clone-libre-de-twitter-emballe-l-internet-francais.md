---
site: TV5MONDE
title: "Mastodon: le clone libre de Twitter emballe l'internet français"
author: Pascal Hérard
date: 2017-04-08
href: http://information.tv5monde.com/info/mastodon-le-clone-libre-de-twitter-emballe-l-internet-fran%C3%A7ais-163348
tags:
- Internet
- Innovation
---

> Un jeune informaticien allemand militant du logiciel libre, Eugene Rochko, a lancé  il y a quelques mois une application en ligne qui copie le principe de Twitter: Mastodon. En une semaine, par la magie d'Internet, une ruée des internautes a débuté vers ce réseau social qui renoue avec les origines libertaires du net. Explications.
