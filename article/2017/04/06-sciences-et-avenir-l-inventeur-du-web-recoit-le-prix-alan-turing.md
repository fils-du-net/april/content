---
site: Sciences et avenir
title: "L'inventeur du web reçoit le prix Alan Turing"
author: Joël Ignasse
date: 2017-04-06
href: https://www.sciencesetavenir.fr/fondamental/inventeur-du-web-tim-berners-lee-recoit-le-prix-turing-sorte-de-prix-nobel-de-l-informatique_112022
tags:
- Internet
- Innovation
- Neutralité du Net
---

> 28 ans après la naissance du premier site web, son créateur Tim Berners-Lee est enfin récompensé.
