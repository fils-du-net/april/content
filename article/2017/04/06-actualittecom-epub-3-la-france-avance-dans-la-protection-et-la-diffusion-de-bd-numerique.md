---
site: ActuaLitté.com
title: "EPUB 3: La France avance dans la protection et la diffusion de BD numérique"
author: Nicolas Gary
date: 2017-04-06
href: https://www.actualitte.com/article/lecture-numerique/epub-3-la-france-avance-dans-la-protection-et-la-diffusion-de-bd-numerique/87858
tags:
- Entreprise
- DRM
---

> Pour la lecture numérique, le développement des mesures techniques de protection reste au cœur des interrogations. Avec la création d’une DRM interopérable LCP, poussée par l’EDRLab, le format EPUB 3 devient plus simple à commercialiser. Et à ce titre, la bande dessinée vient de faire un pas de plus.
