---
site: "usine-digitale.fr"
title: "Quelle vision du numérique chez les \"petits candidats\"?"
date: 2017-04-03
href: http://www.usine-digitale.fr/article/presidentielle-2017-quelle-vision-du-numerique-chez-les-petits-candidats.N523059
tags:
- Institutions
---

> Comme chaque semaine, Renaissance Numérique décrypte les programmes des candidats à l’élection présidentielle 2017. Cette semaine, ce sont les "petits candidats", du moins selon les sondages, qui seront au cœur du sujet. Tour d’horizon des principales mesures par candidat.
