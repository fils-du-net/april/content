---
site: France 24
title: "Pourquoi Mastodon, le clone open-source de Twitter, attire-t-il autant?"
author: Romain Houeix
date: 2017-04-06
href: http://www.france24.com/fr/20170406-pourquoi-mastodon-clone-decentralise-open-source-twitter-reseaux-sociaux
tags:
- Internet
- Innovation
- Vie privée
---

> Mastodon, un clone de Twitter, libre, gratuit et décentralisé sorti il y a six mois, connaît un véritable engouement. Comme de nombreux réseaux sociaux créés ces dernières années, celui-ci prétend, à terme, enterrer son concurrent.
