---
site: Numerama
title: "L'Unesco craint pour les libertés si les DRM du web sont approuvés"
author: Julien Lausson
date: 2017-04-06
href: http://www.numerama.com/politique/247196-lunesco-craint-pour-les-libertes-si-les-drm-du-web-sont-approuves.html
tags:
- Internet
- April
- Associations
- DRM
- Standards
---

> L'Unesco a adressé un courrier au W3C, organisme chargé d'encadrer la conception des standards du web, pour lui faire part de ses craintes. L'agence onusienne ne voit pas d'un bon œil le fait de faire des DRM des standards du web.
