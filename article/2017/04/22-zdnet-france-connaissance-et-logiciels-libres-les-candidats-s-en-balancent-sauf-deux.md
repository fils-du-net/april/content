---
site: ZDNet France
title: "Connaissance et logiciels libres: les candidats s'en balancent, sauf deux?"
author: 	Thierry Noisette
date: 2017-04-22
href: http://www.zdnet.fr/blogs/l-esprit-libre/connaissance-et-logiciels-libres-les-candidats-s-en-balancent-sauf-deux-39851558.htm
tags:
- April
- Institutions
- Associations
---

> Le collectif de 10 associations Connaissance Libre a adressé 12 questions aux candidats à l'élection présidentielle sur la libre diffusion de la connaissance, notamment sur les logiciels libres. 2 des 11 seulement ont répondu.
