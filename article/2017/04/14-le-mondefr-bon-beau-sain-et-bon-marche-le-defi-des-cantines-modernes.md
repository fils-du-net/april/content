---
site: Le Monde.fr
title: "Bon, beau, sain et bon marché: le défi des cantines modernes"
author: Camille Labro
date: 2017-04-14
href: http://www.lemonde.fr/m-gastronomie/article/2017/04/14/des-cantines-ou-manger-bon-sain-et-bio-a-moins-de-10-euros_5111362_4497540.html
tags:
- Entreprise
- Économie
---

> Ce concept lumineux est dû à Augustin Legrand, cofondateur de l’association Les Enfants de Don Quichotte et pourfendeur de la malbouffe. «L’idée est de prendre le contre-pied des chaînes de fast-food, explique-t-il, en prouvant que l’on peut manger bon, sain et bio, pour le même tarif.» Soit un ticket moyen de 9 euros. Au modèle économique des franchises, il préfère la philosophie «open source» – libre à chacun de reproduire son exemple. Le Myrha et La Boétie se sont d’ailleurs faits sans lui. «Pour que cela fonctionne, poursuit-il, il faut qu’on ait du monde.
