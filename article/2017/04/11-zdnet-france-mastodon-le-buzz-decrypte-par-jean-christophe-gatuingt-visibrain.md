---
site: ZDNet France
title: "Mastodon: le buzz décrypté par Jean-Christophe Gatuingt (Visibrain)"
author: Benoit Darcy
date: 2017-04-11
href: http://www.zdnet.fr/blogs/marketing-reseaux-sociaux/mastodon-le-buzz-decrypte-par-jean-christophe-gatuingt-visibrain-39851044.htm
tags:
- Internet
- Innovation
---

> Retour analytique sur l'émergence de Mastodon et son incroyable percée médiatique.
