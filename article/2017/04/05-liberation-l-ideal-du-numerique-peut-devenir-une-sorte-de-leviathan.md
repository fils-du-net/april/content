---
site: Libération
title: "«L’idéal du numérique peut devenir une sorte de Léviathan»"
author: Amaelle Guiton
date: 2017-04-05
href: http://www.liberation.fr/futurs/2017/04/05/l-ideal-du-numerique-peut-devenir-une-sorte-de-leviathan_1560817
tags:
- Internet
- Institutions
- International
---

> La multiplication des cyberattaques interroge la capacité du droit international à encadrer ce type de conflit. A l’occasion du colloque dédié à la «cyberpaix», les chargés de la sécurité nationale et informatique auprès du Premier ministre, Louis Gautier et Guillaume Poupard, reviennent sur ces enjeux.
