---
site: NRJ.fr
title: "Pourquoi les créatifs doivent-ils apprendre à coder?"
date: 2017-04-13
href: http://www.nrj.fr/active/reussir-formation/pourquoi-les-creatifs-doivent-ils-apprendre-a-coder-31267642
tags:
- Sensibilisation
---

> Graphiste, designer, illustrateur… avec l’avènement du numérique, l’univers de la création est en pleine mutation et doit se réinventer. Voici 10 bonnes raisons d’apprendre le langage de programmation: bienvenue dans l’ère du coding.
