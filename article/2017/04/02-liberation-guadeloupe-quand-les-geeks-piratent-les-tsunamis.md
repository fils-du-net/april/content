---
site: Libération
title: "Guadeloupe: quand les geeks piratent les tsunamis"
author: Camille Gévaudan
date: 2017-04-02
href: http://www.liberation.fr/france/2017/04/02/guadeloupe-quand-les-geeks-piratent-les-tsunamis_1560089
tags:
- Associations
- Innovation
---

> Du 18 au 24 mars, pour le Caribe Wave, exercice annuel d’alerte de l’Unesco, des équipes de technophiles et d’experts ont pensé et fabriqué de nouveaux outils de gestion de crise.
