---
site: BFMtv
title: "Qui est Eugen Rochko, cet Allemand de 24 ans qui a créé Mastodon?"
author: Olivier Laffargue
date: 2017-04-11
href: http://bfmbusiness.bfmtv.com/entreprise/qui-est-eugen-rochko-cet-allemand-de-24-ans-qui-a-cree-mastodon-1139641.html
tags:
- Internet
- Innovation
---

> Mastodon est un réseau social Open Source qui se présente comme une alternative à Twitter. Il a été créé par Eugen Rochko, un programmeur allemand de 24 ans qui dit ne viser ni la gloire, ni la fortune.
