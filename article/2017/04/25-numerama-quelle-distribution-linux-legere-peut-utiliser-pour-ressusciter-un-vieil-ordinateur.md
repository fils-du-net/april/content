---
site: Numerama
title: "Quelle distribution Linux légère peut-on utiliser pour ressusciter un vieil ordinateur?"
author: Pierre-Olivier Chaput
date: 2017-04-25
href: http://www.numerama.com/tech/249275-quelle-distribution-linux-legere-peut-on-utiliser-pour-ressusciter-un-vieil-ordinateur.html
tags:
- Économie
- Sensibilisation
---

> Le souvenir de la lenteur. Vieilles tours qui ont trois ou quatre versions de Windows de retard, laptops de plus de cinq ans et netbooks qui étaient à la pointe de la mode entre 2006 et 2010 trainent désormais dans les caves et les greniers, prennent la poussière au fond d'un placard ou sont cachés sous une pile de papier. Ces appareils méritent pourtant une seconde vie. Grâce à Linux, vous pouvez réveiller leur potentiel.
