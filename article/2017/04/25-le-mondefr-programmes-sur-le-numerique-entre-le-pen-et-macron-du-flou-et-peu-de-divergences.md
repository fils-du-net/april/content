---
site: Le Monde.fr
title: "Programmes sur le numérique: entre Le Pen et Macron, du flou et peu de divergences"
author: Damien Leloup, Morgane Tual et Martin Untersinger
date: 2017-04-25
href: http://mobile.lemonde.fr/pixels/article/2017/04/25/entre-le-pen-et-macron-du-flou-et-peu-de-divergences-sur-le-numerique_5117032_4408996.html
tags:
- HADOPI
- Institutions
- Vie privée
---

> Les enjeux politiques et sociétaux liés aux nouvelles technologies tardent à se frayer un chemin dans les programmes des candidats à la présidentielle.
