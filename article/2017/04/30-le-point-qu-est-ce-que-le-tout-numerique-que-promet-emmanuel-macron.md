---
site: Le Point
title: "Qu'est-ce que le tout-numérique que promet Emmanuel Macron?"
author: Laurence Neuer
date: 2017-04-30
href: http://www.lepoint.fr/chroniqueurs-du-point/laurence-neuer/qu-est-ce-que-le-tout-numerique-que-promet-emmanuel-macron-29-04-2017-2123624_56.php
tags:
- Institutions
---

> Compte citoyen, accès dématérialisé à la justice, lutte contre la fracture numérique... Décryptage du programme d'Emmanuel Macron avec un avocat spécialisé.
