---
site: Numerama
title: "L'Union européenne et l'épineux problème de la dépendance aux logiciels Microsoft"
author: Alexis Orsini
date: 2017-04-25
href: http://www.numerama.com/tech/251971-lunion-europeenne-et-lepineux-probleme-de-la-dependance-aux-logiciels-microsoft.html
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Institutions
- Europe
---

> La Commission européenne reconnaît la dépendance de la communauté aux logiciels Microsoft (Office, Windows, Outlook). Elle envisage des alternatives libres, moins coûteuses, mais reste pour l'instant liée à la firme de Redmond, comme le révèle l'enquête des journalistes d'Investigate Europe.
