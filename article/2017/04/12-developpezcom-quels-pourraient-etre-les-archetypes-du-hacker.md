---
site: Developpez.com
title: "Quels pourraient être les archétypes du hacker?"
author: Stéphane le calme
date: 2017-04-12
href: https://www.developpez.com/actu/129646/Quels-pourraient-etre-les-archetypes-du-hacker-Eric-Raymond-un-gourou-de-l-open-source-suggere-une-liste-pour-motiver-les-debutants
tags:
- Sensibilisation
---

> S’inspirant du livre sur les arts martiaux intitulé On the Warrior’s path (Sur la voie du guerrier), dans lequel l’auteur essaie de comprendre les différentes psychologies des artistes martiaux en étudiant une demi-douzaine d'archétypes (ronin, guerrier tribal, etc.), Eric Steven Raymond (connu également sous les initiales ESR), un célèbre hacker américain à qui l'on doit notamment la popularisation du terme open source, s’est proposé d’en faire un parallèle en informatique.
