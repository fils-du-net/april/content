---
site: ZDNet France
title: "BDSM et Drupal: sentiments contrariés au sein de la communauté"
author: Louis Adam
date: 2017-04-14
href: http://www.zdnet.fr/actualites/bdsm-et-drupal-sentiments-contraries-au-sein-de-la-communaute-39851300.htm
tags:
- Internet
- Associations
---

> Une polémique peu commune secoue la communauté des contributeurs du CMS Drupal. Les organes de gouvernances du projet ont écarté un des principaux contributeurs du projet, lui reprochant ses goûts pour la culture Goréenne, une niche du BDSM jugée contraire à l’éthique du projet.
