---
site: El Watan
title: "Sécurité numérique: les logiciels propriétaires sont des terminaux d’espionnage"
author: APS
date: 2017-04-08
href: http://www.elwatan.com/hebdo/multimedia/securite-numerique-les-logiciels-proprietaires-sont-des-terminaux-d-espionnage-08-04-2017-342881_157.php
tags:
- Internet
- Institutions
- Informatique en nuage
- Vie privée
---

> Il n’y aura pas de sécurité dans le cyberespace tant qu’on utilise les logiciels propriétaires, a averti vendredi à Paris l’expert américain et militant pour les logiciels libres, Richard Stallman.
