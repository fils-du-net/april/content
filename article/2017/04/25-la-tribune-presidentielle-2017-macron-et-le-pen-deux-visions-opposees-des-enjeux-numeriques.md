---
site: La Tribune
title: "Présidentielle 2017: Macron et Le Pen, deux visions opposées des enjeux numériques"
author: Sylvain Rolland
date: 2017-04-25
href: http://www.latribune.fr/technos-medias/presidentielle-2017-macron-et-le-pen-deux-visions-opposees-des-enjeux-numeriques-696526.html
tags:
- HADOPI
- Institutions
- Open Data
- Vie privée
---

> Les deux finalistes de l’élection présidentielle présentent un programme diamétralement opposé concernant le numérique, l’innovation et les startups. La Tribune fait le point.
