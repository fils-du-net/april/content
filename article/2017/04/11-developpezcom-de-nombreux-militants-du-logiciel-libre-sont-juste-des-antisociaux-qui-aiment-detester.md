---
site: Developpez.com
title: "«De nombreux militants du logiciel libre sont juste des antisociaux qui aiment détester tout ce qui est populaire»"
author: Michael Guilloux
date: 2017-04-11
href: https://www.developpez.com/actu/129521/-De-nombreux-militants-du-logiciel-libre-sont-juste-des-antisociaux-qui-aiment-detester-tout-ce-qui-est-populaire-estime-le-patron-de-Canonical
tags:
- Entreprise
- Innovation
---

> Comme nous l’avons rapporté la semaine dernière, Canonical, la société qui assure le développement d’Ubuntu a décidé d’abandonner Unity pour GNOME comme environnement de bureau pour la distribution Linux. Cela marque donc, après plusieurs années, la fin du projet de convergence avec les smartphones. Le fondateur de Canonical, Mark Shuttleworth, a justifié cela par le fait que «les efforts de développement du projet de convergence n’ont pas été perçus par la communauté comme une innovation, mais plutôt comme une fragmentation».
