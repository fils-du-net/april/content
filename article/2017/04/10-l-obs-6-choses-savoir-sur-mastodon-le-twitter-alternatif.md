---
site: L'OBS
title: "6 choses à savoir sur Mastodon, le Twitter alternatif"
author: Thierry Noisette
date: 2017-04-10
href: http://tempsreel.nouvelobs.com/rue89/rue89-sur-les-reseaux/20170410.OBS7825/6-choses-a-savoir-sur-mastodon-le-twitter-alternatif.html
tags:
- Internet
- Innovation
---

> Coqueluche des geeks, Mastodon a quintuplé en quelques jours son nombre d'utilisateurs. Ce réseau social open source a des atouts (500 signes, réglages fins...).
