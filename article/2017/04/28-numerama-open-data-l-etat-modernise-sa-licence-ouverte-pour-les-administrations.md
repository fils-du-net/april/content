---
site: Numerama
title: "Open Data: l’État modernise sa Licence Ouverte pour les administrations"
author: Julien Lausson
date: 2017-04-28
href: http://www.numerama.com/politique/253280-open-data-letat-modernise-sa-licence-ouverte-pour-les-administrations.html
tags:
- Administration
- Licenses
- Open Data
---

> Au Journal officiel, un décret concernant la loi pour une République numérique, dite loi Lemaire, a été publié. Il précise la liste des licences Open Data homologuées. Par ailleurs, la mission Etalab a présenté la nouvelle version de sa Licence Ouverte destinée aux administrations.
