---
site: ZDNet France
title: "Mastodon: tabula rasa sur le microblogging"
author: Louis Adam
date: 2017-04-07
href: http://www.zdnet.fr/actualites/mastodon-tabula-rasa-sur-le-microblogging-39850966.htm
tags:
- Internet
- Innovation
---

> Le réseau social Mastodon fait beaucoup parler depuis le début de la semaine. Ce projet lancé en octobre 2016 est pourtant mal compris par les utilisateurs, qui restent trop souvent bloqués sur l’approche centralisée de Twitter. Mastodon entend en prendre le contre-pied. Son créateur, Eugen Rochko explique ses ambitions sur ZDNet.
