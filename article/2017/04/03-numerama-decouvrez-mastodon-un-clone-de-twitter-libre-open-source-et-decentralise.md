---
site: Numerama
title: "Découvrez Mastodon, un clone de Twitter libre, open source et décentralisé"
author: Nelly Lesage
date: 2017-04-03
href: http://www.numerama.com/tech/246010-decouvrez-mastodon-un-clone-de-twitter-libre-open-source-et-decentralise.html
tags:
- Internet
- Innovation
- Vote électronique
---

> Mastodon.social ressemble tant à Twitter que l'on pourrait se croire en train de naviguer sur le réseau social au petit oiseau bleu. Pourtant, des éléments de taille séparent ce nouveau réseau de son concurrent: il est totalement libre, open source et décentralisé.
