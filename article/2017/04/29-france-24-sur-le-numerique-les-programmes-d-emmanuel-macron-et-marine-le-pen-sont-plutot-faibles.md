---
site: France 24
title: "Sur le numérique, les programmes d'Emmanuel Macron et Marine Le Pen sont plutôt faibles"
author: Émilie Laystary
date: 2017-04-29
href: http://mashable.france24.com/tech-business/20170429-emmanuel-macron-marine-le-pen-propositions-numerique
tags:
- HADOPI
- Institutions
- Neutralité du Net
- Vie privée
---

> Emmanuel Macron et Marine Le Pen présentent tous les deux des programmes numériques assez parcellaires. On fait le point.
