---
site: Libération
title: "Terrorisme: Macron en marche contre la cryptographie"
author: Amaelle Guiton
date: 2017-04-10
href: http://www.liberation.fr/elections-presidentielle-legislatives-2017/2017/04/10/terrorisme-macron-en-marche-contre-la-cryptographie_1561864
tags:
- Internet
- Institutions
---

> Le candidat a annoncé vouloir contraindre les «messageries instantanées fortement cryptées» à fournir les clés de chiffrement aux autorités pour lutter contre le terrorisme. Impossible techniquement, sauf à s'engager vers un affaiblissement de la sécurité des communications pour tous.
