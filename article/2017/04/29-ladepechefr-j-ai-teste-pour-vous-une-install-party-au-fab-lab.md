---
site: LaDepeche.fr
title: "J'ai testé pour vous: une  «install party» au Fab-Lab"
date: 2017-04-29
href: http://www.ladepeche.fr/article/2017/04/29/2565253-ai-teste-install-party-fab-lab.html
tags:
- Administration
- Sensibilisation
- Associations
---

> Ce samedi, l'équipe du Fab-Lab de l'association Moissac Animation Jeunes proposait au centre culturel Henri Ena, une journée des plus originales. En toute convivialité et décontraction, on pouvait venir découvrir cet univers mal connu des logiciels libres, véritable alternative aux géants de l'informatique et de l'internet, qui proposent aujourd'hui des solutions gratuites et faciles d'utilisation, pour tous publics
