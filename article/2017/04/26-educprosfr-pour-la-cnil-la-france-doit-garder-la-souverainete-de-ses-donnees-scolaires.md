---
site: EducPros.fr
title: "Pour la CNIL, ”la France doit garder la souveraineté de ses données scolaires”"
author:  Céline Authemayou
date: 2017-04-26
href: http://www.letudiant.fr/educpros/entretiens/isabelle-falque-pierrotin-l-education-est-un-objectif-strategique-de-la-cnil.html
tags:
- Logiciels privateurs
- Administration
- Institutions
- Éducation
- Open Data
---

> Recours aux algorithmes, émergence de l'intelligence artificielle, gestion des données personnelles scolaires... La CNIL s'est saisie des problématiques du numérique dans l'éducation pour en faire l'une de ses priorités. Explications d'Isabelle Falque-Pierrotin, sa présidente.
