---
site: LeMonde.fr
title: "Les algorithmes: nouvelles formes de bureaucraties?"
author: Hubert Guillaud
date: 2017-04-08
href: http://internetactu.blog.lemonde.fr/2017/04/08/les-algorithmes-nouvelles-formes-de-bureaucraties
tags:
- Institutions
- Open Data
---

> Pour Real Life – dont on ne recommandera jamais assez la qualité (@_reallifemag) – l’essayiste Adam Clair (@awattobuildit) dresse une intéressante comparaison entre algorithme et bureaucratie.
