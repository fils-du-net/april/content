---
site: Les Inrocks
title: "Mélenchon, Le Pen et Asselineau, favorisés par l'algorithme de YouTube?"
date: 2017-04-13
href: http://www.lesinrocks.com/2017/04/news/melenchon-le-pen-et-asselineau-favorises-par-lalgorithme-de-youtube
tags:
- Internet
- Institutions
---

> Est-il normal que YouTube suggère à un internaute de regarder la vidéo intitulée “Pourquoi vais-je voter Asselineau?”, alors qu’il a lancé une recherche sur Jean-Luc Mélenchon? Ou qu’il lui suggère une vidéo sur les abstentionnistes, alors qu’il recherchait une vidéo concernant François Fillon? L’algorithme de YouTube serait-il en train de mettre en avant certains candidats à la présidentielles?
