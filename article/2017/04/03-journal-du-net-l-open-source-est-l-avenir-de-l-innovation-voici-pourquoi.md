---
site: Journal du Net
title: "L’open source est l’avenir de l’innovation: voici pourquoi"
author: Jim Whitehurst
date: 2017-04-03
href: http://www.journaldunet.com/solutions/expert/66693/l-open-source-est-l-avenir-de-l-innovation---voici-pourquoi.shtml
tags:
- Entreprise
- Innovation
---

> L'open source est fondé sur les contributions du plus grand nombre. De nombreux problèmes ont pu être résolus par ce mouvement, et la cadence des inventions au sein de la communauté est surprenante.
