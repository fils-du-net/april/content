---
site: Numerama
title: "Présidentielle: les algorithmes de YouTube préfèrent François Asselineau à Benoît Hamon"
author: Corentin Durand
date: 2017-04-13
href: http://www.numerama.com/politique/249358-presidentielle-les-algorithmes-de-youtube-preferent-francois-asselineau-a-benoit-hamon.html
tags:
- Internet
- Institutions
---

> AlgoTransparency est une initiative open source qui vise à mesurer la surreprésentation de certains candidats à la présidentielle sur YouTube. À l'heure où de nombreux votants préfèrent les chaînes des candidats aux messes télévisées, cette question a toute son importance.
