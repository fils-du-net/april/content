---
site: Numerama
title: "Contrat open bar de l'armée: la France doit «entamer sa cure de désintoxication» avec Microsoft"
author: Julien Lausson
date: 2017-10-20
href: http://www.numerama.com/politique/299527-contrat-open-bar-de-larmee-la-france-doit-entamer-sa-cure-de-desintoxication-avec-microsoft.html
tags:
- Entreprise
- Administration
- Interopérabilité
- April
- Institutions
- Marchés publics
---

> Au Sénat, une élue propose une résolution pour mettre en place une commission d’enquête sur les contrats passés entre Microsoft et le ministère des Armées.
