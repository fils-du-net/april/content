---
site: Rue89Lyon
title: "François Aubriot: «Le logiciel libre redonne de la valeur à l’individu»"
author: Bertrand Enjalbal
date: 2017-10-03
href: http://www.rue89lyon.fr/2017/10/03/francois-aubriot-le-logiciel-libre-redonne-de-la-valeur-a-lindividu
tags:
- Administration
- Sensibilisation
- Associations
- Europe
---

> Avec l’association Ploss-RA il prêche leur bonne parole du logiciel libre. Notamment auprès des administrations. Et ce n’est pas toujours évident.
