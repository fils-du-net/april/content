---
site: Le Monde.fr
title: "«Le pouvoir judiciaire devient le parent pauvre de l'antiterrorisme»"
author: Camille Bordenet
date: 2017-10-03
href: http://www.lemonde.fr/police-justice/article/2017/10/03/projet-de-loi-antiterroriste-l-etat-d-urgence-n-a-pas-d-efficacite-propre-contre-les-attentats-aujourd-hui_5195634_1653578.html
tags:
- Institutions
---

> Le texte permettant une sortie de l’état d’urgence vient d’être approuvé en première lecture par les députés. Retour sur la longue histoire de la justice d’exception.
