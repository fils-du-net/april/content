---
site: Public Sénat
title: "Accord Microsoft/Défense: «une question de souveraineté nationale» selon Joëlle Garriaud-Maylam"
date: 2017-10-30
href: https://www.publicsenat.fr/article/politique/accord-microsoftdefense-une-question-de-souverainete-nationale-selon-joelle
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Marchés publics
- Europe
---

> La sénatrice LR, Joëlle Garriaud-Maylam a fait part de ses inquiétudes au sujet du renouvellement d'un accord d'exclusivité entre Microsoft et le ministère des Armées. «C’est une question de souveraineté nationale», «Nous n’avons pas d’informations suffisantes» s’inquiète-elle.
