---
site: Numerama
title: "Après le vote du projet de loi antiterroriste, que va devenir l'obligation sur les identifiants?"
author: Julien Lausson
date: 2017-10-03
href: http://www.numerama.com/politique/294790-apres-le-vote-du-projet-de-loi-antiterroriste-que-va-devenir-lobligation-sur-les-identifiants.html
tags:
- Internet
- Institutions
---

> Les députés ont approuvé à une large majorité le projet de loi antiterroriste réclamé par le gouvernement. Une incertitude existe sur l'avenir de la mesure concernant l'obligation faite aux suspects de déclarer tous les identifiants qu'ils utilisent, du fait des différences d'appréciation entre le Sénat et l'Assemblée nationale.
