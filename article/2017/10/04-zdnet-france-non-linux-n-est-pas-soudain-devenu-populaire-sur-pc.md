---
site: ZDNet France
title: "Non, Linux n'est pas soudain devenu populaire sur PC"
author: Steven J. Vaughan-Nichols
date: 2017-10-04
href: http://www.zdnet.fr/actualites/non-linux-n-est-pas-soudain-devenu-populaire-sur-pc-39858210.htm
tags:
- Internet
---

> En août et septembre, les chiffres de NetMarketShare mesuraient un bond de la part de marché des PC Linux dans le monde. L'heure est-elle venue pour Linux sur PC ? En fait, non.
