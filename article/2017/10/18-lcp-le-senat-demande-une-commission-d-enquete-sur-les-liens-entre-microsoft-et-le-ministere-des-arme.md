---
site: LCP Assemblée nationale
title: "Le Sénat demande une commission d'enquête \"sur les liens \"entre Microsoft et le ministère des Armées"
author: AFP
date: 2017-10-18
href: http://www.lcp.fr/afp/le-senat-demande-une-commission-denquete-sur-les-liens-entre-microsoft-et-le-ministere-des
tags:
- Entreprise
- Administration
- Institutions
- Marchés publics
---

> La sénatrice Joëlle Garriaud-Maylam (LR) a demandé mercredi la création d'une commission d'enquête parlementaire pour faire la lumière ”sur les liens” entre le géant américain Microsoft et le ministère des Armées à la suite du renouvellement d'un accord d'exclusivité entre ces deux parties.
