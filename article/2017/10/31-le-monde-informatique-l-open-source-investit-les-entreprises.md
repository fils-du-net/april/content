---
site: Le Monde Informatique
title: "L'Open Source investit les entreprises"
author: Jean-philippe Levy
date: 2017-10-31
href: http://www.lemondeinformatique.fr/actualites/lire-l-open-source-investit-les-entreprises-69853.html
tags:
- Entreprise
- Économie
- Innovation
---

> Source d'innovation, évolution rapide, fonctionnalités adaptées aux besoins des métiers, l'Open Source entre de plus en plus dans les entreprises. Ces nombreux atouts séduisent non seulement les entreprises mais incitent aussi les grands éditeurs à libérer des parties de leurs codes sources.
