---
site: BFMtv
title: "Une sénatrice s'inquiète des «liens» entre Microsoft et le ministère des Armées"
author: Olivier Laffargue
date: 2017-10-18
href: http://bfmbusiness.bfmtv.com/france/une-senatrice-s-inquiete-des-liens-entre-microsoft-et-le-ministere-des-armees-1280902.html
tags:
- Entreprise
- Administration
- Institutions
- Marchés publics
---

> Joëlle Garriaud-Maylam, secrétaire de la commission de la Défense au Sénat, a demandé mercredi la création d'une commission d'enquête parlementaire à la suite de l'accord d'exclusivité conclu entre le géant américain et le ministère des Armées.
