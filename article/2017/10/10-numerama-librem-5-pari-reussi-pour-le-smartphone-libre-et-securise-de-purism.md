---
site: Numerama
title: "Librem 5: pari réussi pour le smartphone libre et sécurisé de Purism"
author: Julien Lausson
date: 2017-10-10
href: http://www.numerama.com/tech/296091-librem-5-pari-presque-reussi-pour-le-smartphone-libre-et-securise-de-purism.html
tags:
- Économie
- Innovation
- Vie privée
---

> Le projet de financement mis en place par la société américaine Purism pour donner naissance à Librem 5, un smartphone GNU/Linux, ouvert et sécurisé, est un succès. Le cap des 1,5 million de dollars a même été franchi.
