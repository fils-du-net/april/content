---
site: "L'Orient-Le Jour"
title: "Le droit de propriété intellectuelle au 21e siècle"
author: Joseph E. STIGLITZ, Dean BAKER et Arjun JAYADEV
date: 2017-10-19
href: https://www.lorientlejour.com/article/1079081/le-droit-de-propriete-intellectuelle-au-21e-siecle.html
tags:
- Entreprise
- Économie
- Institutions
- Innovation
- International
---

> En 1997, lorsque le gouvernement sud-africain a voulu amender sa législation de manière à pouvoir mettre des médicaments génériques à la disposition des malades du sida, l'industrie pharmaceutique mondiale a utilisé tout son arsenal juridique pour s'y opposer, ce qui a retardé la mise en œuvre de cette mesure et causé des dégâts considérables en terme de vies humaines.
