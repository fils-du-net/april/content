---
site: The Register
title: "French senator demands public inquiry into Microsoft military deal"
author: Gareth Corfield
date: 2017-10-31
href: https://www.theregister.co.uk/2017/10/31/microsoft_france_framework
tags:
- Entreprise
- April
- Institutions
- Marchés publics
---

> A French senator has put down a parliamentary motion demanding an investigation into Microsoft's framework deal with France's defence ministry.
