---
site: UP Magazine
title: "”L'innovation ascendante” et le rôle clé de l'utilisateur"
author: Fabienne Marion
date: 2017-10-10
href: http://up-magazine.info/index.php/decryptages/analyses/6977-qlinnovation-ascendanteq-a-travers-quelques-exemples-classiques-2
tags:
- Économie
- Innovation
---

> Les recherches en sciences sociales sur le processus d'innovation ont dû beaucoup s'employer pour faire reconnaître l'importance du rôle joué par les usagers. Ceux-ci sont en effet longtemps demeurés la figure marginale des travaux sur les sciences et les techniques. L'image d'un inventeur démiurge, puis celle de la toute puissance de la production, des laboratoires de R&amp;D et des services marketing, cantonnent toutes plus ou moins l'usager au rôle (certes décisif) de consommateur, ou de testeur. Pourtant, plusieurs études de cas sont venues fortement remettre en cause cette représentation.
