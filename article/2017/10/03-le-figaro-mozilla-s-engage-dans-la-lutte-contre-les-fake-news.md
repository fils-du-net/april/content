---
site: Le Figaro
title: "Mozilla s'engage dans la lutte contre les «fake news»"
author: Elsa Trujillo
date: 2017-10-03
href: http://www.lefigaro.fr/secteur/high-tech/2017/03/10/32001-20170310ARTFIG00016-mozilla-s-engage-dans-la-lutte-contre-les-fake-news.php
tags:
- Internet
- Associations
- Désinformation
- Vie privée
---

> L'irréductible créateur de Firefox, qui défend l'ouverture du Web, a racheté un service de lecture différée, Pocket. Il veut en faire un outil contre la désinformation.
