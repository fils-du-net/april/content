---
site: L'Express.fr
title: "Le Sénat demande une commission d'enquête"
author: AFP
date: 2017-10-18
href: http://www.lexpress.fr/actualites/1/politique/le-senat-demande-une-commission-d-enquete-sur-les-liens-entre-microsoft-et-le-ministere-des-armees_1953542.html
tags:
- Entreprise
- Administration
- Institutions
- Marchés publics
---

> Paris - La sénatrice Joëlle Garriaud-Maylam (LR) a demandé mercredi la création d'une commission d'enquête parlementaire pour faire la lumière "sur les liens" entre le géant américain Microsoft et le ministère des Armées à la suite du renouvellement d'un accord d'exclusivité entre ces deux parties.
