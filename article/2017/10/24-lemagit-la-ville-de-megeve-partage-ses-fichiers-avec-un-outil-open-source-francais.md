---
site: LeMagIT
title: "La ville de Megève partage ses fichiers avec un outil open source français"
author: Philippe Ducellier
date: 2017-10-24
href: http://www.lemagit.fr/etude/La-ville-de-Megeve-partage-ses-fichiers-avec-un-outil-open-source-francais
tags:
- Administration
- Sensibilisation
---

> Pydio, l’EFSS à déployer sur site, est utilisé depuis deux ans par la DSI de la station. Le contrôle des droits d’accès, l’ergonomie et la maitrise totale des documents ont emporté la décision face aux Dropbox-like américains en mode SaaS et aux alternatives sur site.
