---
site: Les Echos
title: "L’idylle entre la finance et l’open source"
author: Emmanuel Brochard
date: 2017-10-25
href: https://www.lesechos.fr/idees-debats/cercle/cercle-175250-lidylle-entre-la-finance-et-lopen-source-2125079.php
tags:
- Entreprise
- Économie
- Innovation
---

> L’idylle entre l’industrie financière et la technologie open source s’étend aujourd’hui bien au-delà de la gratuité initiale des licences open source. Elle est guidée par la volonté de s’affranchir des centaines de systèmes disparates qui exécutent les milliers d’applications de l’entreprise.
