---
site: "Alternatives-economiques"
title: "L’atelier paysan: agriculteurs, inventeurs et... collaborateurs"
author: Alexiane Lerouge
date: 2017-10-27
href: https://www.alternatives-economiques.fr/latelier-paysan-agriculteurs-inventeurs-collaborateurs/00081219
tags:
- Partage du savoir
- Matériel libre
- Associations
---

> L'atelier paysan met à disposition des agriculteurs des formations et des plans pour construire eux-mêmes leurs machines et leurs bâtiments. De quoi les rendre plus autonomes.
