---
site: Libération
title: "Microsoft: une élue veut la lumière sur les liens avec l’armée"
author: AFP
date: 2017-10-18
href: http://www.liberation.fr/futurs/2017/10/18/microsoft-une-elue-veut-la-lumiere-sur-les-liens-avec-l-armee_1603977
tags:
- Entreprise
- Administration
- Institutions
- Marchés publics
---

> La sénatrice Joëlle Garriaud-Maylam (LR) a demandé mercredi la création d’une commission d’enquête parlementaire pour faire la lumière «sur les liens» entre le géant américain Microsoft et le ministère des Armées à la suite du renouvellement d’un accord d’exclusivité entre ces deux parties.
