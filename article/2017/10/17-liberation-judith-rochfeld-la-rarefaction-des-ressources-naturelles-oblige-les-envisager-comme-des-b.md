---
site: Libération
title: "Judith Rochfeld: «La raréfaction des ressources naturelles a obligé à les envisager comme des biens communs»"
author: Amaelle Guiton
date: 2017-10-17
href: http://www.liberation.fr/debats/2017/10/17/judith-rochfeld-la-rarefaction-des-ressources-naturelles-a-oblige-a-les-envisager-comme-des-biens-co_1603790
tags:
- Économie
- Partage du savoir
- Contenus libres
---

> De la biodiversité, aux ressources numériques, comment définir les «communs»? Plus de 200 contributeurs (économistes, historiens, sociologues) relèvent le défi dans un dictionnaire. Pour la juriste Judith Rochfeld, une des coordinatrices de l’ouvrage, la notion remet en question le principe de propriété privée.
