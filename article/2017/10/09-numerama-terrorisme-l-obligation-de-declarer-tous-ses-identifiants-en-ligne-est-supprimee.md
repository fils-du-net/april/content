---
site: Numerama
title: "Terrorisme: l'obligation de déclarer tous ses identifiants en ligne est supprimée"
author: Julien Lausson
date: 2017-10-09
href: http://www.numerama.com/politique/296025-inutile-inconstitutionnelle-lavenir-incertain-de-lobligation-de-declarer-tous-ses-identifiants-en-ligne.html
tags:
- Internet
- Institutions
- Vie privée
---

> Lundi 9 octobre, les parlementaires se réunissent en commission mixte paritaire pour décider du sort à donner de l'obligation faite aux suspects de déclarer tous leurs identifiants. Une mesure jugée inutile et inconstitutionnelle par ses détracteurs.
