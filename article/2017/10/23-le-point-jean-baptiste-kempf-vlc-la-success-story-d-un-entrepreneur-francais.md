---
site: Le Point
title: "Jean-Baptiste Kempf: VLC, la success-story d'un entrepreneur français"
author: Marylou Magal
date: 2017-10-23
href: http://www.lepoint.fr/high-tech-internet/jean-baptiste-kempf-vlc-la-success-story-d-un-entrepreneur-francais-23-10-2017-2166586_47.php
tags:
- Innovation
---

> Créateur de VLC, l'ingénieur distribue le logiciel gratuitement, grâce aux fonds de son entreprise personnelle, et présente ses nouveaux projets.
