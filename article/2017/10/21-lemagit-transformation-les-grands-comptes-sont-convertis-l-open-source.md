---
site: LeMagIT
title: "Transformation: les grands comptes sont convertis à l’open source"
author: Yann Serra
date: 2017-10-21
href: http://www.lemagit.fr/actualites/450428378/Transformation-les-grands-comptes-choisissent-lOpen-source
tags:
- Entreprise
- Innovation
---

> Présents au Red Hat Forum à Paris, Orange, la Société Générale et autres Engie expliquent pourquoi l’Open Source leur permet d’innover plus facilement.
