---
site: Le progrès
title: "L’Aldi4 devient une association pour l’utilisation du logiciel libre"
date: 2017-10-25
href: http://www.leprogres.fr/rhone-69-edition-lyon-metropole/2017/10/25/l-aldi4-devient-une-association-pour-l-utilisation-du-logiciel-libre
tags:
- Associations
---

> En décembre 2016, sous l’égide du conseil de quartier Croix-Rousse Est et Rhône du 4e  arrondissement, naissait l’Aldi4, dont l’acronyme signifie “Atelier libre d‘Informatique du 4e”. Depuis le mois de juillet 2017, l’Aldi4 s’est muée en association, avec pour objet la défense et l’illustration des logiciels libres.
