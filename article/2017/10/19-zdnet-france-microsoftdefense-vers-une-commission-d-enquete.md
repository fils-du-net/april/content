---
site: ZDNet France
title: "Microsoft/Défense: vers une commission d’enquête?"
author: Louis Adam
date: 2017-10-19
href: http://www.zdnet.fr/actualites/microsoft-defense-vers-une-commission-d-enquete-39858840.htm
tags:
- Entreprise
- Administration
- April
- Institutions
- Marchés publics
---

> La sénatrice Joëlle Garriaud Maylam a déposé une proposition devant la commission des affaires étrangères et de la Défense du Senat. Elle entend faire la lumière sur le contrat passé entre Microsoft et le ministère de la Défense.
