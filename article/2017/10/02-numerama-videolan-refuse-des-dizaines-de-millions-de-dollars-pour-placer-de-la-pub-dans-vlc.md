---
site: Numerama
title: "VideoLAN a refusé des dizaines de millions de dollars pour placer de la pub dans VLC"
author: Julien Lausson
date: 2017-10-02
href: http://www.numerama.com/business/294426-videolan-a-refuse-des-dizaines-millions-de-dollars-pour-placer-des-contenus-publicitaires-dans-vlc.html
tags:
- Internet
- Institutions
- Associations
---

> Saviez-vous que le président de VideoLAN a refusé des dizaines de millions de dollars en échange de l'ajout de publicités et de bouts de code douteux dans VLC? Connaissez-vous l'histoire de l'icône du lecteur multimédia? Ou du risque que font peser la NSA et la CIA sur le projet? Non? Alors, il est temps de lire les réponses que le président de VideoLAN a données aux questions des internautes.
