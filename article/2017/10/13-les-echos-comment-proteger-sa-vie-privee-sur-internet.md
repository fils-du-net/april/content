---
site: Les Echos
title: "Comment protéger sa vie privée sur Internet"
author: Jessica Berthereau
date: 2017-10-13
href: https://www.lesechos.fr/week-end/perso/developpement-personnel/030695212540-comment-proteger-sa-vie-privee-sur-internet-2122002.php
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Europe
- Vie privée
---

> Rien de plus facile, pour une personne malveillante ou une entreprise intéressée par nos données personnelles, que de nous pister via nos machines numériques. Même si on n’a rien à cacher, mieux vaut ne pas laisser ouvertes les portes et les fenêtres digitales. Petit guide d’hygiène numérique.
