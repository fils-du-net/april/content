---
site: ZDNet France
title: "Contrat open bar Microsoft-Défense: l'April pointe l'armée \"victime du syndrome de Stockholm\""
author:  Thierry Noisette
date: 2017-10-02
href: http://www.zdnet.fr/blogs/l-esprit-libre/contrat-open-bar-microsoft-defense-l-april-pointe-l-armee-victime-du-syndrome-de-stockholm-39858078.htm
tags:
- Entreprise
- Administration
- Interopérabilité
- April
- Institutions
- Marchés publics
---

> L'association libriste publie les documents - passablement noircis - qui lui "confirment que l'administration est déterminée à s'enfermer toujours davantage" dans des solutions Microsoft.
