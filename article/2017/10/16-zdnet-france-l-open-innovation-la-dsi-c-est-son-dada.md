---
site: ZDNet France
title: "L'open innovation, la DSI c'est son dada"
author: Christophe Auffray
date: 2017-10-16
href: http://www.zdnet.fr/actualites/l-open-innovation-la-dsi-c-est-son-dada-39858730.htm
tags:
- Entreprise
- Innovation
---

> Pour innover grâce au numérique et le faire bien, mieux vaut se passer de la DSI et la cantonner à un rôle d'opérateur. En êtes-vous certains? Les DSI du Cigref estiment au contraire que la DSI a des atouts susceptibles d'aider tous les métiers de l'entreprise à innover de manière ouverte. Démonstration.
