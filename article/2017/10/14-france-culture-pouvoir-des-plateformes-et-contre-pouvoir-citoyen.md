---
site: France Culture
title: "Pouvoir des plateformes et contre-pouvoir citoyen"
author: Catherine Petillon
date: 2017-10-14
href: https://www.franceculture.fr/emissions/le-numerique-et-nous/donnees-algorithmes-comment-les-citoyens-peuvent-agir
tags:
- Entreprise
- Internet
- Associations
- Vie privée
---

> Airbnb, Deliveroo, Facebook.... les plateformes sont partout dans nos vies. Comment les réguler? Le conseil national du numérique sollicite l'avis de tous et lance une consultation publique sur la confiance à l'ère des plateformes.
