---
site: Joinup
title: "French senator: ‘Digital sovereignty at stake’"
author: Gijs Hillenius
date: 2017-10-24
href: https://joinup.ec.europa.eu/news/autonomy
tags:
- Entreprise
- April
- Institutions
- Marchés publics
---

> France’s Senate should start a commission of inquiry into a proprietary licence contract signed by the Ministry of Defence, says senator Joëlle Garriaud-Maylam. This summer, the ministry’s latest four-year framework contract came into force, following a so-called negotiated procedure, bypassing competition. Members of the parliament have criticised this licence agreement for years.
