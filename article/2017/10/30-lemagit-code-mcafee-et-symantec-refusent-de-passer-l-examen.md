---
site: LeMagIT
title: "Code: McAfee et Symantec refusent de passer l’examen"
author: Valéry Marchive
date: 2017-10-30
href: http://www.lemagit.fr/actualites/450429120/Code-McAfee-et-Symantec-refusent-de-passer-lexamen
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Sensibilisation
---

> Quitte à échouer à obtenir leur permis d’accès à certains marchés, les deux éditeurs ne souhaitent plus laisser certains gouvernements étudier le code source de produits.
