---
site: Numerama
title: "Bruxelles est contre les backdoors... mais invite les États à partager leur savoir-faire en déchiffrement"
author: Julien Lausson
date: 2017-10-19
href: http://www.numerama.com/politique/299000-bruxelles-est-contre-les-backdoors-mais-invite-les-etats-a-partager-leur-savoir-faire-en-dechiffrement.html
tags:
- Institutions
- Europe
---

> La Commission européenne n'est pas favorable aux portes dérobées, qui affaibliraient gravement la sécurité sur Internet. Par contre, elle estime qu'il faut développer des capacités en matière de déchiffrement.
