---
site: Developpez.com
title: "La Fondation Linux présente la Community Data License Agreement"
author: Stéphane le calme
date: 2017-10-29
href: https://www.developpez.com/actu/169274/La-Fondation-Linux-presente-la-Community-Data-License-Agreement-un-nouveau-framework-pour-le-partage-de-grands-ensembles-de-donnees
tags:
- Partage du savoir
- Licenses
- Contenus libres
- Open Data
---

> La fondation Linux a présenté la Community Data License Agreement, un nouveau framework pour le partage de grands ensembles de données nécessaires à la recherche, à l'apprentissage collaboratif et à d'autres fins. Dans un billet, la fondation s’est exprimée en ces termes
