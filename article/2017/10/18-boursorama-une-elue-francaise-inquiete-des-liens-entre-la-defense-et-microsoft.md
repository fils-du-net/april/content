---
site: Boursorama
title: "Une élue française inquiète des liens entre la Défense et Microsoft"
author: Reuters
date: 2017-10-18
href: http://www.boursorama.com/actualites/une-elue-francaise-inquiete-des-liens-entre-la-defense-et-microsoft-b9022ab62e55294d663a32db72ad359d
tags:
- Entreprise
- Administration
- Institutions
- Marchés publics
---

> PARIS (Reuters) - La sénatrice (Les Républicains) Joëlle Garriaud-Maylam, qui bataille contre le contrat liant la Défense française au groupe américain Microsoft, réclame désormais la création d'une commission d'enquête sur cet accord-cadre renouvelé jusqu'en 2021 "sans appel d'offres".
