---
site: Le Journal de Québec
title: "L’industrie veut être entendue"
author: Martin Lavoie
date: 2017-10-29
href: http://www.journaldequebec.com/2017/10/29/lindustrie-du-logiciel-libre-veut-etre-entendue
tags:
- Entreprise
- Économie
- Institutions
- Sensibilisation
- International
---

> Se disant mieux écouté que par le passé, l’industrie du logiciel libre réclame néanmoins une véritable ouverture du gouvernement du Québec.
