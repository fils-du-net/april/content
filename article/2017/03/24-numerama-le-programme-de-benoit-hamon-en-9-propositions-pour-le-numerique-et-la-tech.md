---
site: Numerama
title: "Le programme de Benoît Hamon en 9 propositions pour le numérique et la tech"
author: Alexis Orsini
date: 2017-03-24
href: http://www.numerama.com/politique/220443-le-programme-de-benoit-hamon-en-9-propositions-pour-le-numerique-et-la-tech.html
tags:
- Institutions
- Neutralité du Net
- Open Data
---

> Quelles sont les propositions de Benoît Hamon, candidat désigné par la Belle alliance populaire pour la présidentielle, en matière de tech et de numérique? Retrouvez-les ici, actualisées au fil de la campagne, en attendant le premier tour du scrutin, le 23 avril 2017.
