---
site: "Alternatives-economiques"
title: "Programmes - La souveraineté numérique en question"
date: 2017-03-31
href: http://www.alternatives-economiques.fr/souverainete-numerique-question/00078296
tags:
- Institutions
---

> Libertés numériques, souveraineté technologique, uberisation: les mesures envisagées varient beaucoup selon les programmes.
