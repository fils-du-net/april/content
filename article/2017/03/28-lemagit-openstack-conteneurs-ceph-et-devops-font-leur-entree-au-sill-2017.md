---
site: LeMagIT
title: "OpenStack, conteneurs, Ceph et DevOps font leur entrée au SILL 2017"
author: Cyrille Chausson
date: 2017-03-28
href: http://www.lemagit.fr/actualites/450415698/OpenStack-conteneurs-Ceph-et-DevOps-font-leur-entree-au-SILL-2017
tags:
- Administration
- RGI
---

> Ce référentiel 2017 liste quelque 139 logiciels libres, gonflé par l’arrivée du Cloud OpenStack et des technologies modernes, comme le stockage objet et en mode bloc, DevOps et les conteneurs dans les projets IT de l’Etat. Parmi les autres changements clés, Cassandra disparait, MariaDB est désormais recommandée, tout comme Pentaho.
