---
site: L'OBS
title: "Généraliser le vote électronique, la mauvaise idée d'Emmanuel Macron"
author: Thierry Noisette
date: 2017-03-03
href: http://tempsreel.nouvelobs.com/rue89/sur-le-radar/20170303.OBS6074/generaliser-le-vote-electronique-la-mauvaise-idee-d-emmanuel-macron.html
tags:
- Institutions
- Vote électronique
---

> L'Irlande, les Pays-Bas, l'Allemagne, entre autres, ont essayé puis abandonné le vote électronique - qui enlève tout contrôle à l'électeur et coûterait en fait plus cher que le vote papier.
