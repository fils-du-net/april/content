---
site: ZDNet France
title: "La vie privée n'est plus une obligation pour les FAI américains"
author: Tris Acatrinei
date: 2017-03-29
href: http://www.zdnet.fr/actualites/la-vie-privee-n-est-plus-une-obligation-pour-les-fai-americains-39850488.htm
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> N'oubliez jamais que si c'est gratuit, c'est vous le produit. Dans le monde de la fiction séries et romans comme Black Mirror ou Le Cercle nous alertent sur ces questions de transfert et de valorisation de données. D'autres considèrent que cela n'est pas un sujet. A tort.
