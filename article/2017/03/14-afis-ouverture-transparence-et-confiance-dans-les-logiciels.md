---
site: Afis
title: "Ouverture, transparence et confiance dans les logiciels"
author: Jean-Paul Delahaye
date: 2017-03-14
href: http://www.pseudo-sciences.org/spip.php?article2774
tags:
- Innovation
---

> Pour tenter d’éviter cela, on a conçu les logiciels «open source». Ce que fait un logiciel «open source» est public et compréhensible par quiconque dispose des compétences lui permettant d’analyser un programme. S’il y a réellement des gens intéressés par cette transparence et qui se donnent la peine d’analyser les programmes «open source» – c’est le cas le plus souvent – pour en contrôler le comportement annoncé et programmé, on a là une protection contre de nombreux abus des logiciels « propriétaires » dont les sources (le détail du fonctionnement sans masque) sont cachées.
