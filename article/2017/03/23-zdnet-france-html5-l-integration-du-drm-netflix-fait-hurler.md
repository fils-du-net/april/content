---
site: ZDNet France
title: "HTML5: l'intégration du DRM Netflix fait hurler"
author: Louis Adam
date: 2017-03-23
href: http://www.zdnet.fr/actualites/html5-l-integration-du-drm-netflix-fait-hurler-39850282.htm
tags:
- Entreprise
- Internet
- DRM
- Standards
---

> Au mois d’avril, le W3C devra se prononcer une fois pour toutes sur le plan de standardisation des EME (Encrypted Media Extension). Cette technologie, réclamée à grands cris par Netflix et consorts, est décrite par ses opposants comme une DRM au sein des standards web, traditionnellement ouverts.
