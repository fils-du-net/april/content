---
site: Finyear
title: "Civic Techs – 2 – Quel modèle économique?"
date: 2017-03-21
href: http://www.finyear.com/Civic-Techs-2-Quel-modele-economique_a38045.html
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Associations
- Innovation
---

> La démocratie est-elle un marché comme un autre? Peut-on bâtir un modèle économique pérenne sur des outils numériques dont l'objectif est de remettre le citoyen au centre du jeu démocratique? Si oui, comment? Ces structures doivent-elles être de nature associative ou peuvent-elle assumer une dimension commerciale tout en restant fidèles à leurs objectifs? Doivent-elles opter pour une logique open source ou propriétaire? Les interrogations autour du financement et du modèle économique sont très nombreuses pour cet écosystème en plein développement.
