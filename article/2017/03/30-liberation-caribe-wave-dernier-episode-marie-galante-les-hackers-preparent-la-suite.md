---
site: Libération
title: "Caribe Wave, dernier épisode: à Marie-Galante, les hackers préparent la suite"
author: Camille Gévaudan
date: 2017-03-30
href: http://www.liberation.fr/futurs/2017/03/30/caribe-wave-dernier-episode-a-marie-galante-les-hackers-preparent-la-suite_1559127
tags:
- Associations
- Innovation
---

> Alors que l'exercice d'alerte au tsunami se termine en Guadeloupe, les hackers de l'association Hand imaginent déjà leurs prochaines expériences et cherchent à pérenniser l'intégration des technologies dans la prévention des risques.
