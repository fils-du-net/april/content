---
site: 20minutes.fr
title: "Quand les fermiers américains sont obligés de pirater leurs propres tracteurs pour pouvoir les réparer"
author: Nicolas Raffin
date: 2017-03-23
href: http://www.20minutes.fr/high-tech/2036523-20170323-quand-fermiers-americains-obliges-pirater-propres-tracteurs-pouvoir-reparer
tags:
- Entreprise
- Logiciels privateurs
- Promotion
- Sciences
- International
---

> Imaginez un fermier en plein travail dans son champ. Soudain, une panne, le tracteur s’arrête. Mais même si le temps presse pour la récolte ou le semis, pas question de le réparer soi-même : une clause l’interdit expressément. Cette histoire, ils sont nombreux à la vivre aux Etats-Unis.
