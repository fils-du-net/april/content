---
site: Libération
title: "En Espagne, le «village global» bouge encore"
author: Amaelle Guiton
date: 2017-03-10
href: http://www.liberation.fr/futurs/2017/03/10/en-espagne-le-village-global-bouge-encore_1554915
tags:
- Internet
- Associations
- International
- Vie privée
---

> Près de 1 300 personnes étaient réunies cette semaine à Valence, pour parler surveillance du Net, harcèlement en ligne, blocages d’Internet, politiques numériques… Une édition qui a élargi son audience, avec des non-initiés, beaucoup d’intervenantes et 114 pays représentés.
