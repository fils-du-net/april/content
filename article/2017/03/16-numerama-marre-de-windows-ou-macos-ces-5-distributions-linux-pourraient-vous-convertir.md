---
site: Numerama
title: "Marre de Windows ou macOS? Ces 5 distributions Linux pourraient vous convertir"
author: Corentin Durand
date: 2017-03-16
href: http://www.numerama.com/tech/240961-marre-de-windows-ou-macos-ces-5-distributions-linux-pourraient-vous-convertir.html
tags:
- Partage du savoir
- Sensibilisation
---

> Numerama aime Linux. Toutefois le dire et l'écrire ne suffit pas: il faut également raconter les histoires du monde du libre et inviter nos lecteurs à découvrir de belles alternatives à Windows ou macOS. Ce guide vous propose 5 solutions prêtes pour l'avenir, belles et libres.
