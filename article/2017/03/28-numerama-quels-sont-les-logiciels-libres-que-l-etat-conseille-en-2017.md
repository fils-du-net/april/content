---
site: Numerama
title: "Quels sont les logiciels libres que l'État conseille en 2017?"
author: Julien Lausson
date: 2017-03-28
href: http://www.numerama.com/tech/244219-quels-sont-les-logiciels-libres-que-letat-conseille-en-2017.html
tags:
- Administration
- April
- Promotion
- RGI
---

> Le socle interministériel de logiciels libres a été mis à jour. Cette liste, publiée depuis 2012, regroupe les logiciels libres que l'État recommande. Elle inclut des programmes généralistes mais aussi des solutions bien plus pointues.
