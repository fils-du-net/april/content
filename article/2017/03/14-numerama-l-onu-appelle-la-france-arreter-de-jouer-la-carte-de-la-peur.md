---
site: Numerama
title: "L'ONU appelle la France à arrêter de jouer la «carte de la peur»"
author: Julien Lausson
date: 2017-03-14
href: http://www.numerama.com/politique/240376-le-rapporteur-special-de-lonu-sur-la-vie-privee-appelle-la-france-a-arreter-de-jouer-la-carte-de-la-peur.html
tags:
- Internet
- Institutions
- International
- Vie privée
---

> Dans un rapport remis le 8 mars, le rapporteur spécial de l'ONU sur la vie privée, Joseph Cannataci, appelle la France à arrêter de jouer la «carte de la peur» avec le terrorisme. Il dénonce le vote de lois liberticides et inefficaces.
