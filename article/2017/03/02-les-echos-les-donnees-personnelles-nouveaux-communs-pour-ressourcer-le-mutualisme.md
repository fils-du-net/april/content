---
site: Les Echos
title: "Les données personnelles, nouveaux ”communs” pour ressourcer le mutualisme"
author: Jean-Louis Davet
date: 2017-03-02
href: https://www.lesechos.fr/idees-debats/cercle/cercle-167009-les-donnees-personnelles-nouveaux-communs-pour-ressourcer-le-mutualisme-2069152.php
tags:
- Économie
- Innovation
- Vie privée
---

> En ce début de 21e siècle, nous assistons à un formidable renouveau des biens communs ou "communs", porté par la révolution numérique en cours.
