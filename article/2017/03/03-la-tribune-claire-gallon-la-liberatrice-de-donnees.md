---
site: La Tribune
title: "Claire Gallon, la libératrice de données"
author: Frédéric Thual
date: 2017-03-03
href: http://www.latribune.fr/technos-medias/innovation-et-start-up/claire-gallon-la-liberatrice-de-donnees-649773.html
tags:
- Administration
- Économie
- Institutions
- Associations
- Open Data
---

> Cofondatrice de l'association LiberTIC, la nantaise Claire Gallon incarne la promotion de l'ouverture des données publiques. Cette pionnière de la sensibilisation entend maintenant accompagner le changement d'échelle de l'open data.
