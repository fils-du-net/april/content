---
site: La Tribune
title: "Présidentielle 2017: ce que proposent les candidats pour le numérique"
author: Sylvain Rolland
date: 2017-03-28
href: http://www.latribune.fr/technos-medias/start-up/presidentielle-2017-ce-que-proposent-les-candidats-pour-le-numerique-672358.html
tags:
- Institutions
---

> Contrairement à l'élection présidentielle de 2012, les principaux candidats de 2017 - sauf Marine Le Pen, très imprécise sur le sujet - intègrent dans leur vision de l'avenir de la France les enjeux de la transformation numérique de l'économie et de la société. Si certains sujets font consensus, les diverses sensibilités politiques s'expriment sur les questions de souveraineté, de fiscalité, de financement de l'innovation, d'inclusion par le numérique ou encore dans le domaine de la culture. Décryptage.
