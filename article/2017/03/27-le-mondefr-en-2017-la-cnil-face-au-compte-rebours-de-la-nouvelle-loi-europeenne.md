---
site: Le Monde.fr
title: "En 2017, la CNIL face au compte à rebours de la nouvelle loi européenne"
author: Martin Untersinger
date: 2017-03-27
href: http://www.lemonde.fr/pixels/article/2017/03/27/en-2017-la-cnil-face-au-compte-a-rebours-de-la-nouvelle-loi-europeenne_5101598_4408996.html
tags:
- Institutions
- Promotion
- Europe
- Vie privée
---

> La Commission nationale de l’informatique et des libertés doit préparer l’application, en mai 2018, du nouveau règlement européen sur les données personnelles. Le temps presse.
