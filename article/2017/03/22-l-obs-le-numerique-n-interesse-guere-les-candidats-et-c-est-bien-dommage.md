---
site: L'OBS
title: "Le numérique n'intéresse guère les candidats... et c'est bien dommage"
author: Dominique Nora
date: 2017-03-22
href: http://tempsreel.nouvelobs.com/economie/20170322.OBS6997/le-numerique-n-interesse-guere-les-candidats-et-c-est-bien-dommage.html
tags:
- Entreprise
- Institutions
---

> La fondation Digital New Deal veut, avec son #Pacte Numérique, rééditer le coup de Nicolas Hulot sur l'écologie. Malheureusement, ce thème qui va pourtant décider de notre destin, n'attire pas plus les candidats qu'il n'excite les foules...
