---
site: Silicon
title: "Numérique et Présidentielle, un débat hélas sans candidats"
author: Jacques Cheminat
date: 2017-03-09
href: http://www.silicon.fr/numerique-et-presidentielle-un-debat-helas-sans-candidats-169958.html
tags:
- Institutions
- Associations
---

> Le collectif France Numérique 2017 avait invité certains prétendants à la présidentielle à venir débattre sur le numérique et avec l’écosystème. Souveraineté numérique, exception culturelle et emploi étaient au programme, mais sans les candidats.
