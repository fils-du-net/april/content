---
site: ZDNet France
title: "L'Anssi cherche à définir la cyberpaix"
author: Louis Adam
date: 2017-03-20
href: http://www.zdnet.fr/actualites/l-anssi-cherche-a-definir-la-cyberpaix-39850084.htm#xtor%3DRSS-1
tags:
- Institutions
- Innovation
---

> À l’occasion de la conférence internationale «Construire la paix et la sécurité internationales de la société numérique», l’Anssi et le SGDSN lancent une plateforme visant à recueillir les avis de la société civile sur la question cruciale «de l’utilisation non belliqueuse du monde numérique».
