---
site: Libération
title: "Quand le Congrès américain torpille la vie privée en ligne"
author: Amaelle Guiton
date: 2017-03-29
href: http://www.liberation.fr/futurs/2017/03/29/quand-le-congres-americain-torpille-la-vie-privee-en-ligne_1559176
tags:
- Entreprise
- Internet
- Institutions
- International
- Vie privée
---

> Après le Sénat, la Chambre des représentants a adopté une résolution pour abroger les nouvelles règles du régulateur des télécoms. Elles imposaient aux fournisseurs d'accès à Internet d'obtenir l'accord de leurs abonnés avant de vendre leurs données à des fins publicitaires.
