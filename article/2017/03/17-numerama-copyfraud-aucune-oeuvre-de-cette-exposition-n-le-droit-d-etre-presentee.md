---
site: Numerama
title: "Copyfraud: aucune œuvre de cette exposition n'a le droit d'être présentée"
author: Julien Lausson
date: 2017-03-17
href: http://www.numerama.com/pop-culture/241526-copyfraud-aucune-oeuvre-de-cette-exposition-na-le-droit-detre-presentee.html
tags:
- Partage du savoir
- Institutions
- Droit d'auteur
---

> Ce week-end, une exposition à Paris défie et tourne en dérision le copyfraud, terme qui regroupe les dérives de la propriété intellectuelle. Il s'agit pour les responsables de sensibiliser le grand public sur ce problème et d'inviter le personnel politique à y remédier.
