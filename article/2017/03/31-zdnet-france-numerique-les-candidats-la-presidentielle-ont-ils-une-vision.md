---
site: ZDNet France
title: "Numérique: les candidats à la présidentielle ont-ils une ”vision”?"
author: Laurent Calixte
date: 2017-03-31
href: http://www.zdnet.fr/actualites/numerique-les-candidats-a-la-presidentielle-ont-ils-une-vision-39850662.htm
tags:
- Institutions
- Open Data
---

> Jusqu'alors, le numérique était un gadget qui parsemait divers chapitres des programmes présidentiels. Depuis peu, les candidats semblent avoir compris que cet enjeu était structurant pour la politique et l'économie. Le point sur leurs principales prises de position.
