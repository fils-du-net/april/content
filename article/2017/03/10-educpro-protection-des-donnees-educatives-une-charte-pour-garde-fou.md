---
site: EducPro
title: "Protection des données éducatives: une charte pour garde-fou"
author:  Céline Authemayou
date: 2017-03-10
href: http://www.letudiant.fr/educpros/actualite/protection-des-donnees-educatives-une-charte-pour-garde-fou.html
tags:
- Institutions
- Associations
- Éducation
- Vie privée
---

> Annoncée en mars 2016, la "charte de confiance dans les services numériques éducatifs" doit être prochainement signée. Avec cette démarche, le ministère de l’Éducation nationale espère apporter plus de transparence dans la gestion des données personnelles produites par les élèves et les enseignants. Une question éthique sur fond d’enjeux économiques.
