---
site: ZDNet France
title: "Connaissance libre: questions aux candidats à l'élection présidentielle"
author: Thierry Noisette
date: 2017-03-05
href: http://www.zdnet.fr/blogs/l-esprit-libre/connaissance-libre-questions-aux-candidats-a-l-election-presidentielle-39849368.htm
tags:
- Institutions
- Associations
---

> Open data, logiciels libres, domaine public, sont parmi les points sur lesquels le collectif Connaissance libre interroge les candidats à l'élection présidentielle des 23 avril et 7 mai.
