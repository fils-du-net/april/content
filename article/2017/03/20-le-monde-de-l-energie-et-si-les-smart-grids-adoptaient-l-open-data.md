---
site: Le Monde de l'Energie
title: "Et si les smart grids adoptaient l’Open Data?"
author: Ghislain Delabie
date: 2017-03-20
href: http://www.lemondedelenergie.com/et-si-les-smart-grids-adoptaient-lopen-data/2017/03/20
tags:
- Institutions
- Innovation
- Open Data
---

> Vous êtes doctorant et modélisez l’impact européen à horizon 2040 de la politique de transition énergétique de la France, en tenant compte de l’évolution du climat. Vous construisez vos modèles à partir de l’état de l’art, et les nourrissez de données: capacités de production présentes et à venir, équilibre offre-demande des réseaux, corrélation entre météo et consommation électrique.
