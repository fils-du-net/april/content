---
site: Libération
title: "A Valence, l'Internet libre ne désarme pas"
author: Amaelle Guiton
date: 2017-03-07
href: http://www.liberation.fr/futurs/2017/03/07/a-valence-l-internet-libre-ne-desarme-pas_1553849
tags:
- Internet
- Associations
- International
- Vie privée
---

> La troisième édition de l'Internet Freedom Festival s'est ouverte lundi en Espagne: 1 300 personnes venues du monde entier s'y retrouvent pour débattre de liberté d'expression, de vie privée et de lutte contre les discriminations.
