---
site: L'OBS
title: "Loi République numérique: il va falloir s'armer de patience"
author: Thierry Noisette
date: 2017-03-17
href: http://tempsreel.nouvelobs.com/rue89/sur-le-radar/20170317.OBS6743/loi-republique-numerique-il-va-falloir-s-armer-de-patience.html
tags:
- Internet
- Institutions
- Open Data
---

> Goulot d'étranglement de fin de mandat, retours de Bruxelles à venir: l'open data passera sans problème, en revanche l'accessibilité ou la loyauté des plateformes dépendront du gouvernement suivant.
