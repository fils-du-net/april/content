---
site: ZDNet France
title: "La surveillance de masse fonctionne-t-elle? Rien ou presque ne le prouve"
author: Christophe Auffray et Zack Whittaker
date: 2017-03-13
href: http://www.zdnet.fr/actualites/la-surveillance-de-masse-fonctionne-t-elle-rien-ou-presque-ne-le-prouve-39849692.htm#xtor%3DRSS-1
tags:
- Internet
- Institutions
- International
- Vie privée
---

> Céder une partie de ses droits et libertés en acceptant la surveillance de masse est-il un compromis acceptable ? Ce n'est même pas sûr selon le rapporteur spécial de l'ONU, pour qui les lois sont avant tout "fondées sur la psychologie de la peur".
