---
site: L'Essor
title: "«L'Atelier paysan» favorise le génie paysan"
author: Sevim Sonmez
date: 2017-03-08
href: http://lessor.fr/-l-atelier-paysan-favorise-le-genie-paysan-18151.html
tags:
- Partage du savoir
- Matériel libre
- Associations
- Innovation
---

> Une rencontre entre un agronome et un maraîcher isérois utilisant des pratiques originales est à l'origine de cette coopérative d'auto-construction d'outils agricoles.
