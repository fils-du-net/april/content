---
site: La Tribune
title: "Pour redynamiser notre économie, nous devons conquérir notre souveraineté numérique"
author: Gaël Duval
date: 2017-03-13
href: http://www.latribune.fr/opinions/tribunes/pour-redynamiser-notre-economie-nous-devons-conquerir-notre-souverainete-numerique-658327.html
tags:
- Économie
- Institutions
---

> Quel est le rapport entre notre économie moribonde, notre Souveraineté Numérique, et les investissements massifs des USA dans l'armée et la conquête spatiale dans les années 40, 50 et 60? Aucun lien direct, en apparence... Par Gaël Duval, entrepreneur
