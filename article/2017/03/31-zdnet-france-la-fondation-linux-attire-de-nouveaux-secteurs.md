---
site: ZDNet France
title: "La fondation Linux attire de nouveaux secteurs"
author: Thierry Noisette
date: 2017-03-31
href: http://www.zdnet.fr/blogs/l-esprit-libre/la-fondation-linux-attire-de-nouveaux-secteurs-39850648.htm
tags:
- Entreprise
- Associations
---

> La Linux Foundation franchira bientôt le millier d'organisations adhérentes et compte notamment sur la formation pour rallier aux logiciels libres.
