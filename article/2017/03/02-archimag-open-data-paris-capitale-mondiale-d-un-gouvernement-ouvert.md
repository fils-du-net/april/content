---
site: Archimag
title: "Open data: Paris, capitale mondiale d'un gouvernement ouvert"
author: Bruno Texier
date: 2017-03-02
href: http://www.archimag.com/vie-numerique/2017/02/03/open-data-paris-capitale-mondiale-gouvernement-ouvert
tags:
- April
- Institutions
- Associations
- International
- Open Data
---

> La France a accueilli au mois de décembre dernier le 4e sommet mondial du Partenariat pour un gouvernement ouvert. L'occasion de promouvoir l'ouverture des données publiques et de nouveaux modes de gouvernance.
