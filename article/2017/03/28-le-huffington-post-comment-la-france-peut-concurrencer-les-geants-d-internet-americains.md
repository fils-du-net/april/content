---
site: Le Huffington Post
title: "Comment la France peut concurrencer les géants d'Internet américains"
author: Nicolas d'Hueppe et François Doux
date: 2017-03-28
href: http://www.huffingtonpost.fr/nicolas-d-hueppe/comment-la-france-peut-concurrencer-les-geants-dinternet-americ_a_22015065
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Innovation
---

> Les fameuses GAFA, Google, Apple, Facebook et Amazon, se sont transformées, en quelques années, en une impitoyable machine à rafler les parts de marché. A coups de rachats massifs de technologies, elles ont affiché une audace entrepreneuriale jamais égalée, quitte à ignorer les lois économiques.
