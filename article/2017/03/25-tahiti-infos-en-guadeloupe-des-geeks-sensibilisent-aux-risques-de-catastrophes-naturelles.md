---
site: TAHITI INFOS
title: "En Guadeloupe, des \"geeks\" sensibilisent aux risques de catastrophes naturelles"
date: 2017-03-25
href: http://www.tahiti-infos.com/En-Guadeloupe-des-geeks-sensibilisent-aux-risques-de-catastrophes-naturelles_a159217.html
tags:
- Institutions
- Associations
---

> Ils sont seize "geeks" à "s'être sentis concernés" par la question de la protection des populations en cas de tsunami ou de séisme: des passionnés d'informatique participent en Guadeloupe à un exercice international de simulation qui a démarré mardi.
