---
site: Le Courrier
title: "La Poste expose son e-voting"
author: Philippe Bach
date: 2017-03-02
href: https://www.lecourrier.ch/147264/la_poste_expose_son_e_voting
tags:
- Entreprise
- Internet
- Vote électronique
---

> Depuis lundi, il est possible de tester le système de vote par internet proposé par la Poste. Le Parti pirate est critique face aux options choisies.
