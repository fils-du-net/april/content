---
site: Numerama
title: "Linux: ces 5 applications dont nous ne pouvons plus nous passer"
author: Corentin Durand
date: 2017-03-12
href: http://www.numerama.com/tech/239644-linux-quelques-applications-dont-nous-ne-pouvons-plus-nous-passer.html
tags:
- Innovation
- Promotion
---

> En dehors des indispensables du libre que l'on connaît déjà, de VLC à The GIMP en passant par Firefox, il existe d'autres projets qui nous inspirent et nous facilitent la vie au quotidien. Compilation de nos applications préférées pour utiliser Linux au quotidien, au bureau comme à la maison.
