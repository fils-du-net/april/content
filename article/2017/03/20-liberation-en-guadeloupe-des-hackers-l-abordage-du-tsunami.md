---
site: Libération
title: "En Guadeloupe, des hackers à l'abordage du tsunami"
author: lundi 20 mars 2017
date: 2017-03-20
href: http://www.liberation.fr/futurs/2017/03/20/en-guadeloupe-des-hackers-a-l-abordage-du-tsunami_1556823
tags:
- Partage du savoir
- Institutions
- Associations
- Innovation
---

> Alors que les Nations unies organisent leur exercice annuel d'alerte au tsunami, des geeks antillais et métropolitains exploitent les nouvelles technologies pour gérer les situations de crise.
