---
site: Contexte
title: "Comparez les engagements numériques des candidats à la présidentielle"
author: Yann Guégan, Sabine Blanc
date: 2017-03-22
href: https://www.contexte.com/article/numerique/engagements-des-candidats-presidentielle-numerique_67135.html
tags:
- Internet
- Institutions
---

> Contexte et le think tank Renaissance numérique publient les premières réponses au questionnaire sur les grands enjeux numériques de la prochaine mandature adressé aux équipes de campagne, celles d’Emmanuel Macron, de Marine Le Pen, de Jean-Luc Mélenchon et de Benoît Hamon. Retrouvez l’ensemble de leurs positions sur une plateforme unique qui offre une vision claire de leurs engagements.
