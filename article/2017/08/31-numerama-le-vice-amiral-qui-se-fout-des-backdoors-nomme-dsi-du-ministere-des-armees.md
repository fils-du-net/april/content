---
site: Numerama
title: "Le vice-amiral qui «se fout» des backdoors nommé DSI du ministère des armées"
author: Julien Lausson
date: 2017-08-31
href: http://www.numerama.com/politique/285394-le-vice-amiral-qui-se-fout-des-backdoors-nomme-dsi-du-ministere-des-armees.html
tags:
- Logiciels privateurs
- Administration
- April
- Institutions
- Marchés publics
---

> Le ministère de la défense a un nouveau directeur général des systèmes d'information et de communication pour son administration centrale: le vice-amiral Coustillière. Celui-là même qui a déclaré n'avoir rien à faire des portes dérobées lors d'un reportage mené par Cash Investigation.
