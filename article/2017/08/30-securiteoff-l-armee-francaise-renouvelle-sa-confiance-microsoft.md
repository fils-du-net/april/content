---
site: SecuriteOff
title: "L’Armée française renouvelle sa confiance à Microsoft"
date: 2017-08-30
href: http://www.securiteoff.com/larmee-francaise-renouvelle-confiance-a-microsoft
tags:
- Logiciels privateurs
- Administration
- Institutions
---

> Les ministres ont beau changer, les (mauvaises) habitudes persistent! Critiqué par de nombreux experts, le contrat «Open Bar» entre Microsoft et le Ministère de la Défense est de nouveau renouvelé. Conclu pour la première fois en 2009, sans appel d’offres ni procédure publique, il a été reconduit en 2013 pour 4 ans. Rebelote jusqu’en 2021!
