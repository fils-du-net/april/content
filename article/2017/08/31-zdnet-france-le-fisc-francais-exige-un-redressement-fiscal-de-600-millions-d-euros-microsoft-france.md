---
site: ZDNet France
title: "Le fisc français exige un redressement fiscal de 600 millions d'euros à Microsoft France"
date: 2017-08-31
href: http://www.zdnet.fr/actualites/le-fisc-francais-exige-un-redressement-fiscal-de-600-millions-d-euros-a-microsoft-france-39856688.htm
tags:
- Entreprise
- Économie
- Institutions
---

> Microsoft France facturerait ses activités de régie publicitaire en ligne, le display ou encore l'achat de mots-clés, en passant par l'Irlande et non pas la France. De quoi questionner Bercy.
