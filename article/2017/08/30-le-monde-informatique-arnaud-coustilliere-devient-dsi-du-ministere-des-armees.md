---
site: Le Monde Informatique
title: "Arnaud Coustillière devient DSI du Ministère des Armées"
author: Didier Barathon
date: 2017-08-30
href: http://www.lemondeinformatique.fr/actualites/lire-arnaud-coustilliere-devient-dsi-du-ministere-des-armees-69178.html
tags:
- Institutions
---

> Le Ministère des Armées a nommé Arnaud Coustillière au poste de de directeur général des systèmes d'information et de communication. Il succède à ce poste à Marc Leclère.
