---
site: Numerama
title: "Évasion fiscale: Bercy veut faire payer Microsoft à hauteur de 600 millions d'euros"
author: Julien Lausson
date: 2017-08-31
href: http://www.numerama.com/politique/285340-evasion-fiscale-bercy-veut-faire-payer-microsoft-a-hauteur-de-600-millions-deuros.html
tags:
- Entreprise
- Économie
- Institutions
---

> Les services fiscaux suivent de près les pratiques d'évasion fiscale des géants de la tech. Microsoft est l'une des sociétés suivies par Bercy, qui lui réclame le paiement de 600 millions d'euros.
