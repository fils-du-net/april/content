---
site: Developpez.com
title: "Faut-il avoir recours à la publicité pour soutenir le financement des projets open source? Open Collective suggère d'aller au-delà des donations"
author: Michael Guilloux
date: 2017-08-29
href: https://www.developpez.com/actu/157457/Faut-il-avoir-recours-a-la-publicite-pour-soutenir-le-financement-des-projets-open-source-Open-Collective-suggere-d-aller-au-dela-des-donations
tags:
- Internet
- Économie
---

> Les logiciels open source ne sont pas gratuits. Mais si vous les utilisez gratuitement, c’est parce que quelqu’un d’autre les paye pour vous. Il peut s’agir d’entreprises sponsors et de simples particuliers qui soutiennent les projets open source par des dons; et c’est ainsi que l’open source fonctionne depuis toujours. La réalité peut toutefois être plus difficile. Si certains projets peuvent être parrainés par des entreprises, d'autres n’ont pas cette chance. Pour ces derniers, des flux de revenus alternatifs doivent donc être trouvés, mais comment?
