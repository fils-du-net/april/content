---
site: la Croix
title: "Les données des élèves français entre les mains des géants du Web"
author: Mégane De Amorim
date: 2017-08-29
href: http://www.la-croix.com/Famille/Education/donnees-eleves-francais-entre-mains-geants-Web-2017-08-29-1200872769
tags:
- Logiciels privateurs
- Institutions
- Éducation
- Vie privée
---

> L’école s’ouvre aux outils numériques proposés par Google, Apple, Facebook, Amazon ou Microsoft (Gafam), malgré des risques quant à l’utilisation des données scolaires des élèves.
