---
site: "cio-online.com"
title: "Florence Parly affirme la légalité du contrat du Ministère de la Défense avec Microsoft Irlande"
author: Bertrand Lemaire
date: 2017-08-25
href: http://www.cio-online.com/actualites/lire-florence-parly-affirme-la-legalite-du-contrat-du-ministere-de-la-defense-avec-microsoft-irlande-9650.html
tags:
- Entreprise
- April
- Institutions
---

> La sénatrice de droite Joëlle Garriaud-Maylam vient de publier la réponse de la ministre Florence Parly sur le contrat entre Microsoft Irlande et le Ministère de la Défense (devenu Ministère des Armées) à son interpellation parlementaire en mai dernier.
