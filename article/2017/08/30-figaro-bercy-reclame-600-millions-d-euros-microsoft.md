---
site: FIGARO
title: "Bercy réclame 600 millions d'euros à Microsoft"
author: Benjamin Ferran
date: 2017-08-30
href: http://www.lefigaro.fr/societes/2017/08/30/20005-20170830ARTFIG00319-bercy-reclame-600millions-d-euros-a-microsoft.php
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Institutions
---

> Selon L'Express, un redressement fiscal a été notifié à la filiale française du géant informatique pour des activités publicitaires facturées depuis l'Irlande.
