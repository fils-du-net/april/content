---
site: Le Monde Informatique
title: "La région Grand Est acquiert 245 000 licences Office mais à quel prix?"
author: Bertrand Lemaire
date: 2017-01-12
href: http://www.lemondeinformatique.fr/actualites/lire-la-region-grand-est-acquiert-245-000-licences-office-mais-a-quel-prix-67042.html
tags:
- Logiciels privateurs
- Administration
- Éducation
---

> Secteur public / privé: 28 000 enseignants et 217 000 lycéens de la région Grand Est vont être équipés de Microsoft Office 365 Pro Plus.
