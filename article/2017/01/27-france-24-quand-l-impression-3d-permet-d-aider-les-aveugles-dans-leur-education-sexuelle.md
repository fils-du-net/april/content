---
site: France 24
title: "Quand l'impression 3D permet d'aider les aveugles dans leur éducation sexuelle"
author: Katie Dupere
date: 2017-01-27
href: http://mashable.france24.com/tech-business/20170127-impression-3d-sexualite-aveugles
tags:
- Matériel libre
- Éducation
---

> En utilisant l’impression en 3D, des chercheurs américains ont élaboré des modèles d’organes sexuels à destination des écoles pour permettre aux adolescents aveugles d’appréhender la sexualité d’une manière inédite et utile.
