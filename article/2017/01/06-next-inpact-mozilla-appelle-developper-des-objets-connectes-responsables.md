---
site: Next INpact
title: "Mozilla appelle à développer des objets connectés responsables"
author: Vincent Hermann
date: 2017-01-06
href: https://www.nextinpact.com/news/102771-mozilla-appelle-a-developper-objets-connectes-responsables.htm
tags:
- Internet
- Innovation
- Vie privée
---

> Mozilla s’inquiète de l’explosion du nombre d’objets connectés et d'une conception oublieuse de certains principes. Si l’éditeur s’émerveille du potentiel d’Internet via des milliards de petits appareils du quotidien, il n’en reste pas moins très prudent, évoquant un véritable carrefour technologique.
