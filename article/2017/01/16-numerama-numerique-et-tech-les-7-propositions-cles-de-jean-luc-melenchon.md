---
site: Numerama
title: "Numérique et tech: les 7 propositions clés de Jean-Luc Mélenchon"
author: Alexis Orsini
date: 2017-01-16
href: http://www.numerama.com/politique/224878-numerique-et-tech-les-7-propositions-cle-de-jean-luc-melenchon.html
tags:
- Administration
- Institutions
- Vie privée
---

> Quelles sont les propositions principales de Jean-Luc Mélenchon, candidat de La France insoumise à la présidentielle 2017, en matière de numérique? Retrouvez-les ici, actualisées tout au long de la campagne, en attendant le premier tour de l'élection présidentielle, le 23 avril.
