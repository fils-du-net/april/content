---
site: Next INpact
title: "Les inquiétudes de Sergio Coronado, «seul député» à encore utiliser du libre à l’Assemblée"
author: Marc Rees
date: 2017-01-27
href: https://www.nextinpact.com/news/102993-les-inquietudes-sergio-coronado-seul-depute-a-utiliser-libre-a-l-assemblee.htm
tags:
- Logiciels privateurs
- Administration
- Institutions
---

> Sergio Coronado, l’un des députés des Français de l’étranger, vient d’écrire à Claude Bartolone, président de l’Assemblée nationale, pour lui faire part de ses «nombreuses inquiétudes» dans l’utilisation du libre sur son poste de travail.
