---
site: Le Monde Informatique
title: "La Free Software Foundation revoit sa liste de projets prioritaires"
author: Jean Elyan
date: 2017-01-18
href: http://www.lemondeinformatique.fr/actualites/lire-la-free-software-foundation-revoit-sa-liste-de-projets-prioritaires-67106.html
tags:
- Accessibilité
- Associations
- Innovation
---

> La Free Software Foundation, la «fondation du logiciel libre», a annoncé hier un remaniement majeur des projets logiciels qu'elle soutiendra en priorité. Parmi eux, un projet d'OS gratuit pour smartphones, un assistant personnel vocal intelligent, mais aussi l'accessibilité et le développement de pilotes.
