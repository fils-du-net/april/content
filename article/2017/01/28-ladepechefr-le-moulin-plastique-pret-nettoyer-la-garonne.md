---
site: LaDepeche.fr
title: "Le moulin à plastique prêt à nettoyer la Garonne"
author: Philippe Emery
date: 2017-01-28
href: http://www.ladepeche.fr/article/2017/01/28/2505957-le-moulin-a-plastique-pret-a-nettoyer-la-garonne.html
tags:
- Internet
- Matériel libre
- Associations
- Innovation
---

> Un groupe de jeunes Toulousains a imaginé, conçu et construit une machine à nettoyer la Garonne des bouteilles plastiques qui polluent l'océan atlantique. Vous ne savez pas ce qu'est un FabLab, un projet en open source, un maker ou un gyre océanique, et vous croyez que le 7e Continent est le titre d'une BD?
