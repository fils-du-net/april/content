---
site: Les Echos
title: "Mozilla dresse un bilan sombre de la santé d’Internet"
author: Nicolas Rauline
date: 2017-01-19
href: http://www.lesechos.fr/tech-medias/hightech/0211705386796-mozilla-dresse-un-bilan-sombre-de-la-sante-dinternet-2058427.php
tags:
- Internet
- Associations
- Promotion
---

> De moins en moins sûr, exposé à la censure et à la fragmentation, menacé par les intérêts privés, le réseau verrait sa qualité se dégrader. La fondation américaine va observer sa santé dans un rapport qu’il publiera régulièrement.
