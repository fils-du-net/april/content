---
site: Les Echos
title: "Thibault Verbiest: «Les ”open API” donnent l'occasion aux banques de rester au centre du jeu»"
author: Ninon Renaud
date: 2017-01-27
href: http://www.lesechos.fr/finance-marches/banque-assurances/0211733511808-thibault-verbiest-les-open-api-donnent-loccasion-aux-banques-de-rester-au-centre-du-jeu-2060570.php
tags:
- Entreprise
- Internet
- Économie
- Innovation
- Open Data
---

> Les interfaces de programmation ouvertes développées actuellement dans le monde de la finance sont-elles aussi simples d'accès que celles des géants de l'Internet?
