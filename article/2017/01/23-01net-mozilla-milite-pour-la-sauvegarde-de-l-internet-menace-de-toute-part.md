---
site: 01net.
title: "Mozilla milite pour la sauvegarde de l'Internet, menacé de toute part"
author: Gilbert Kallenborn
date: 2017-01-23
href: http://www.01net.com/actualites/mozilla-milite-pour-la-sauvegarde-de-l-internet-menace-de-toute-part-1087970.html
tags:
- Internet
- Associations
- Open Data
---

> La fondation vient de publier son premier "bilan de santé de l'Internet", un rapport censé alerter les Internautes sur les dangers qui guettent la Toile. C'est aussi un moyen pour redorer son blason de militant du Web.
