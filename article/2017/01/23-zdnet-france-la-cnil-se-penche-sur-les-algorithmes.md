---
site: ZDNet France
title: "La CNIL se penche sur les algorithmes"
date: 2017-01-23
href: http://www.zdnet.fr/actualites/la-cnil-se-penche-sur-les-algorithmes-39847530.htm
tags:
- Institutions
- Innovation
- Vie privée
---

> Conformément à la loi République numérique, la CNIL a été chargée de lancer une grande réflexion sur la place des algorithmes. Elle donne aujourd’hui le coup d’envoi à un cycle de débats autour de ce thème, qui devrait aboutir en septembre à un rapport sur le sujet.
