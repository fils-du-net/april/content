---
site: vousnousils
title: "EduNathon, qui regroupe des acteurs du \"numérique libre\", poursuit son action contre le partenariat entre Microsoft et l'Éducation Nationale."
author: Valentin Glo
date: 2017-01-16
href: http://www.vousnousils.fr/2017/01/16/edunathon-toujours-sceptique-sur-le-partenariat-microsoft-education-nationale-598394
tags:
- April
- Institutions
- Associations
- Éducation
---

> Le collectif EduNathon s’est toujours opposé au partenariat entre Microsoft et l’Éducation nationale. Ce partenariat noué en novembre 2015 a pour but de «renforcer l’accompagnement proposé par Microsoft dans le cadre du Plan Numérique à l’École», comme il est précisé dans le communiqué de presse de Najat Vallaud-Belkacem.
