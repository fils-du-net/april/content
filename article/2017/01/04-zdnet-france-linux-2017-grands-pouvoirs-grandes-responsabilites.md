---
site: ZDNet France
title: "Linux 2017: A grands pouvoirs, grandes responsabilités"
author: Steven J. Vaughan-Nichols
date: 2017-01-04
href: http://www.zdnet.fr/actualites/linux-2017-a-grands-pouvoirs-grandes-responsabilites-39846604.htm
tags:
- Entreprise
- Innovation
- Promotion
---

> Linux et le logiciel open source dirigent désormais le monde et cela signifie que nous devons travailler plus fort que jamais pour s'assurer qu'il soit fiable.
