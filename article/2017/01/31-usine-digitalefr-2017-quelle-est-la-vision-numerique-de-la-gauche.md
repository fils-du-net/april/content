---
site: "usine-digitale.fr"
title: "2017: quelle est la vision numérique de la gauche?"
date: 2017-01-31
href: http://www.usine-digitale.fr/article/2017-quelle-est-la-vision-numerique-de-la-gauche.N495339
tags:
- Institutions
- Innovation
- Neutralité du Net
---

> Que disent les candidats de gauche à la Présidentielle 2017 sur les sujets numériques? Quelques jours après les primaires de gauche, il est intéressant de faire un point sur les valeurs qu’ils incarnent, leurs similitudes et leurs différences. Y a-t-il une gauche du numérique qui se dessine pour les élections? Renaissance Numérique fait le point pour L'Usine Digitale.
