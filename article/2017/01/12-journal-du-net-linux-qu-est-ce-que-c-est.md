---
site: Journal du Net
title: "Linux: qu'est ce que c'est?"
author: Antoine Crochet- Damais
date: 2017-01-12
href: http://www.journaldunet.com/solutions/dsi/1091588-kernel-linux-open-source-8
tags:
- Sensibilisation
---

> De sa conception, sans ambition, à la conquête des plus grands clouds de la planète, l'OS créé par Linus Torvalds aura profondément marqué l'histoire de l'informatique.
