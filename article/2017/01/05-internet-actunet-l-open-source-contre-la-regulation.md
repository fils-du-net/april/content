---
site: internet ACTU.net
title: "L’open source contre la régulation?"
author: Hubert Guillaud
date: 2017-01-05
href: http://www.internetactu.net/a-lire-ailleurs/lopen-source-contre-la-regulation
tags:
- Entreprise
- Institutions
- Désinformation
- Innovation
---

> Il y a peu, nous évoquions rapidement Comma.ai, une startup lancée par un hacker de 26 ans, George Hotz, visant à fournir un kit pour rendre n’importe quelle voiture semi-autonome, depuis une simple caméra embarquée (une dashcam comme on appelle ces caméras de tableau de bord). Alors que les géants de l’automobile et de la technologie dépensent des milliards pour construire des voitures autonomes, Hotz annonçait à l’automne qu’il souhaitait vendre son kit logiciel et matériel pour moins de 1000 $.
