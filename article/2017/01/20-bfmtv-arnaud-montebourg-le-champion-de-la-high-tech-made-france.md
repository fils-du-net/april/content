---
site: BFMtv
title: "Arnaud Montebourg, le champion de la high-tech made in France"
author: Amélie Charnay
date: 2017-01-20
href: http://hightech.bfmtv.com/epoque/arnaud-montebourg-le-champion-de-la-high-tech-made-in-france-1085523.html
tags:
- Institutions
- Europe
---

> Pourfendeur des Gafa et volontariste, le candidat croit dur comme fer au concept de souveraineté numérique. Quitte à passer pour un irréaliste peu au fait des réalités technologiques.
