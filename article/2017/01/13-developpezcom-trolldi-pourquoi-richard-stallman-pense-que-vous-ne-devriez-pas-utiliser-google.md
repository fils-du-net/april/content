---
site: Developpez.com
title: "Trolldi: pourquoi Richard Stallman pense que vous ne devriez pas utiliser Google"
author: Michael Guilloux
date: 2017-01-13
href: http://www.developpez.com/actu/111877/Trolldi-pourquoi-Richard-Stallman-pense-que-vous-ne-devriez-pas-utiliser-Google
tags:
- Internet
- Sensibilisation
- Philosophie GNU
---

> Chaque fois qu’il en a l’occasion, Richard Stallman, le père du GNU et du mouvement du logiciel libre, ne manque pas de rappeler les causes qu’il défend et fustiger les entreprises, produits et services qui sont en violation de ses principes. Il s’en est déjà pris par exemple à Uber, qu’il accuse d’exploiter les chauffeurs, ou encore à Facebook qui «entrave les libertés». Il avait également qualifié Windows, OS X, iOS et Android de malwares en 2015, estimant que ces systèmes espionnent les utilisateurs, les emprisonnent et disposent de backdoor (porte dérobée) pour leur faire du mal.
