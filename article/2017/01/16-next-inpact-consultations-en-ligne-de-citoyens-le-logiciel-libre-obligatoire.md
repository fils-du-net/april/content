---
site: Next INpact
title: "Consultations en ligne de citoyens: le logiciel libre obligatoire?"
author: Xavier Berne
date: 2017-01-16
href: https://www.nextinpact.com/news/102895-consultations-en-ligne-citoyens-logiciel-libre-obligatoire.htm
tags:
- April
- Institutions
- Associations
- Promotion
- Open Data
---

> Alors que la question des consultations en ligne de citoyens est régulièrement évoquée en cette période de campagne présidentielle, plusieurs associations montent au créneau pour que les plateformes utilisées par les pouvoirs publics soient systématiquement basées sur du logiciel libre. Une question de transparence qui ne peut qu’aller de pair avec ces initiatives, estime notamment l’April.
