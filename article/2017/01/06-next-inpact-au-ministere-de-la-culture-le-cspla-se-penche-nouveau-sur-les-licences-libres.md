---
site: Next INpact
title: "Au ministère de la Culture, le CSPLA se penche à nouveau sur les licences libres"
author: Marc Rees
date: 2017-01-06
href: https://www.nextinpact.com/news/102774-au-ministere-culture-cspla-se-penche-a-nouveau-sur-licences-libres.htm
tags:
- Économie
- Institutions
- Licenses
---

> Le Conseil supérieur de la propriété littéraire et artistique, instance de réflexion abritée par le ministère de la Culture, va se pencher sur les licences libres. Une mission a été confiée à Joëlle Farchy, enseignante spécialiste des industries culturelles.
