---
site: vousnousils
title: "L’école numérique, c’est se livrer aux géants de l’informatique"
author: Fabien Soyez
date: 2017-01-03
href: http://www.vousnousils.fr/2017/01/03/lecole-numerique-cest-se-livrer-aux-geants-de-linformatique-karine-mauvilly-597693
tags:
- Entreprise
- Économie
- Éducation
---

> Prof en collège, Karine Mauvilly a démissionné face au "plan numérique pour l'éducation". Pour elle, il s'agit d'une intrusion du monde marchand à l'école.
