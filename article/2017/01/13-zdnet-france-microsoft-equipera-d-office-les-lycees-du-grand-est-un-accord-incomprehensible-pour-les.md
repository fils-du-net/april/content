---
site: ZDNet France
title: "Microsoft équipera ”d'Office ”les lycées du Grand-Est, un accord ”incompréhensible” pour les libristes"
author: Louis Adam
date: 2017-01-13
href: http://www.zdnet.fr/actualites/microsoft-equipera-d-office-les-lycees-du-grand-est-un-accord-incomprehensible-pour-les-libristes-39847124.htm
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Associations
- Éducation
- Marchés publics
---

> L’objectif: équiper l’ensemble des lycées de la région en licence Office. Un contrat passé par la région, mais qui pose évidemment des questions auprès des associations de défense du logiciel libre.
