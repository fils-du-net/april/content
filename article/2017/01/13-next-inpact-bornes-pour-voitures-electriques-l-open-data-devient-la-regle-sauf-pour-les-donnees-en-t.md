---
site: Next INpact
title: "Bornes pour voitures électriques: l’Open Data devient la règle, sauf pour les données en temps réel"
author: Xavier Berne
date: 2017-01-13
href: https://www.nextinpact.com/news/102870-bornes-pour-voitures-electriques-l-open-data-devient-regle-sauf-pour-donnees-en-temps-reel.htm
tags:
- Institutions
- Open Data
---

> Afin que le public puisse savoir en quelques clics où se trouvent les stations de recharge destinées aux voitures électriques, l’exécutif vient de publier un décret imposant la mise en ligne, sur le portail gouvernemental d’Open Data, des informations concernant leur localisation. La diffusion des données en temps réel ne sera toutefois pas impérative.
