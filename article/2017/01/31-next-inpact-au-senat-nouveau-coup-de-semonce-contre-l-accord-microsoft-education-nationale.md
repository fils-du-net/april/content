---
site: Next INpact
title: "Au Sénat, nouveau coup de semonce contre l’accord Microsoft-Éducation nationale"
author: Xavier Berne
date: 2017-01-31
href: https://www.nextinpact.com/news/103102-au-senat-nouveau-coup-semonce-contre-l-accord-microsoft-education-nationale.htm
tags:
- Institutions
- Éducation
- Marchés publics
---

> L'accord de «partenariat» entre Microsoft et le ministère de l'Éducation nationale continue de faire des vagues. Craignant de nombreuses «dérives», une sénatrice vient d'interpeller Axelle Lemaire, la secrétaire d'État au Numérique.
