---
site: Numerama
title: "Julian Assange ne se livrera pas aux États-Unis malgré la libération de Chelsea Manning"
author: Julien Lausson
date: 2017-01-19
href: http://www.numerama.com/politique/225893-julian-assange-ne-se-livrera-pas-aux-etats-unis-malgre-la-liberation-de-chelsea-manning.html
tags:
- Institutions
- International
- Vie privée
---

> Julian Assange en avait fait une condition de sa reddition: si Barack Obama libère Chelsea Manning, alors il se rendra. le président commué la peine de l'ex-analyste, mais le créateur de WikiLeaks a renoncé à son engagement.
