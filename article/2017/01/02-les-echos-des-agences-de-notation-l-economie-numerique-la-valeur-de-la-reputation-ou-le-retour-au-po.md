---
site: Les Echos
title: "Des agences de notation à l’économie numérique: la valeur de la réputation ou le retour au potlatch"
author: Luc-Marie Augagneur
date: 2017-01-02
href: http://www.lesechos.fr/idees-debats/cercle/cercle-164432-des-agences-de-notation-a-leconomie-numerique-la-valeur-de-la-reputation-ou-le-retour-au-potlatch-2053945.php
tags:
- Entreprise
- Économie
---

> Constater que certains modèles économiques reposent sur l'accumulation de la réputation doit conduire à mieux appréhender juridiquement les transactions réputationnelles.
