---
site: Next INpact
title: "On parle numérique avec Yannick Jadot, le candidat des Verts à la présidentielle"
author: Xavier Berne
date: 2017-01-11
href: https://www.nextinpact.com/news/102818-interview-on-parle-numerique-avec-yannick-jadot-candidat-verts-a-presidentielle.htm
tags:
- HADOPI
- Open Data
---

> Next INpact a pu interroger Yannick Jadot, le candidat d’Europe Écologie - Les Verts (EELV) à l’élection présidentielle. L’occasion d'en savoir plus sur les propositions de l’eurodéputé, ancien directeur de campagne de Greenpeace, en matière de numérique.
