---
site: Le Monde.fr
title: "Après la clémence pour Chelsea Manning, le sort d’Edward Snowden toujours en suspens"
date: 2017-01-18
href: http://www.lemonde.fr/surveillance-NSA-France/article/2017/01/18/apres-la-clemence-pour-chelsea-manning-le-sort-d-edward-snowden-toujours-en-suspens_5064563_4660509.html
tags:
- Institutions
- International
- Vie privée
---

> Le président américain a commué la peine de la lanceuse d’alerte. Mais la même clémence a peu de chances de s’appliquer à Snowden.
