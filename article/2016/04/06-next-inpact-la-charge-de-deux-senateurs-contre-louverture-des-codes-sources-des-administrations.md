---
site: Next INpact
title: "La charge de deux sénateurs contre l'ouverture des codes sources des administrations"
author: Xavier Berne
date: 2016-04-06
href: http://www.nextinpact.com/news/99359-la-charge-deux-senateurs-contre-l-ouverture-codes-sources-administrations.htm
tags:
- Administration
- April
- Institutions
- Open Data
- Vie privée
---

> Alors que Bercy a rendu public la semaine dernière le code source de son logiciel de calcul de l’impôt sur le revenu, deux sénateurs viennent de lancer une charge assez virulente contre ce mouvement en faveur de la transparence. Au risque de forcer un peu trop le trait.
