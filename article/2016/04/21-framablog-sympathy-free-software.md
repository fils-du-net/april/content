---
site: Framablog
title: "Sympathy for the Free Software"
author: Véronique Bonnet
date: 2016-04-21
href: http://framablog.org/2016/04/21/allumons-les-reverberes-du-libre
tags:
- Interopérabilité
- April
- HADOPI
- Institutions
- Associations
---

> Indésirable, le logiciel libre (free software)? Un amendement de la commission des lois du Sénat vient de faire disparaître, pour l’instant, avant l’examen en séance prévu fin avril, l’encouragement au logiciel libre. Certes, il s’agissait bien d’un amendement de repli, non juridiquement contraignant, que l’Assemblée avait voté comme pis-aller, vu les tirs de barrage contre la priorisation. Le simple encouragement est-il déjà tabou? Caillou dans la chaussure ? Loup dans la bergerie?
