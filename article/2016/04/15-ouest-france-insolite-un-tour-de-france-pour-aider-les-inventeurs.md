---
site: "ouest-france.fr"
title: "Insolite. Un tour de France pour aider les inventeurs"
author: Pierre Lemerle
date: 2016-04-15
href: http://www.entreprises.ouest-france.fr/article/insolite-tour-france-pour-aider-inventeurs-15-04-2016-264294
tags:
- Entreprise
- Partage du savoir
- Matériel libre
- Innovation
---

> Depuis le 2 mars, le Breton Xavier Coadic vient en aide à des porteurs de projets sur des nouvelles technologies, gratuitement. Il s'est arrêté à Concarneau pour travailler avec Explore.
