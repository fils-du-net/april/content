---
site: ZDNet France
title: "Avènement du PC Linux: Linus Torvalds y travaille toujours"
date: 2016-04-11
href: http://www.zdnet.fr/actualites/avenement-du-pc-linux-linus-torvalds-y-travaille-toujours-39835408.htm
tags:
- Innovation
---

> Linux dominera-t-il le PC un jour ? Pourquoi pas selon Linus Torvalds. Le créateur du kernel entend y travailler encore les 25 prochaines années. Pour autant, pas question de parler d'un échec de Linux.
