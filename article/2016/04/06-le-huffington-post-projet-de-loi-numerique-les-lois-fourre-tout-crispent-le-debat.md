---
site: Le Huffington Post
title: "Projet de loi numérique: les lois fourre-tout crispent le débat"
author: Nicolas Bouzou
date: 2016-04-06
href: http://www.huffingtonpost.fr/nicolas-bouzou/projet-de-loi-numerique-les-lois-fourre-tout-crispent-le-debat_b_9615942.html
tags:
- Institutions
- Open Data
---

> Loi El Khomri, loi Macron, loi Lemaire: le gouvernement s'est spécialisé dans la conception de lois fourre-tout (appelons-les par courtoisie "lois à large spectre") qui, généralement, crispent le débat parlementaire.
