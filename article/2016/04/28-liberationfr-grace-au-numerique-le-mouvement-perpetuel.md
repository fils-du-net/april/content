---
site: Libération.fr
title: "Grâce au numérique, le mouvement perpétuel"
author: Amaelle Guiton
date: 2016-04-28
href: http://www.liberation.fr/france/2016/04/28/grace-au-numerique-le-mouvement-perpetuel_1449290
tags:
- Internet
- Partage du savoir
---

> La création de plusieurs outils, dont la nouvelle version du site mise en ligne mercredi, vise à soutenir les rassemblements et à prolonger la lutte sur Internet.
