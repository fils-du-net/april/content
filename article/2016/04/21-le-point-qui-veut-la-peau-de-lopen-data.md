---
site: Le Point
title: "Qui veut la peau de l'open data?"
author: Mathieu Lehot
date: 2016-04-21
href: http://www.lepoint.fr/politique/qui-veut-la-peau-de-l-open-data-21-04-2016-2033949_20.php
tags:
- Institutions
- Associations
- Open Data
- Vie privée
---

> Tangui Morlier de Regards citoyens s'inquiète du coup de frein mis à la politique de transparence des données publiques. Interview.
