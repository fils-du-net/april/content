---
site: Silicon.fr
title: "Le logiciel libre repart à l'assaut de la loi Numérique"
author: Ariane Beky
date: 2016-04-26
href: http://www.silicon.fr/logiciel-libre-loi-numerique-senat-145889.html
tags:
- Administration
- April
- Institutions
- Marchés publics
- Open Data
---

> De la promotion du logiciel libre dans l’administration à la communication du code source aux citoyens, des sénateurs tentent de réintroduire dans le projet de loi pour une République numérique d’Axelle Lemaire des dispositions écartées en commission des lois.
