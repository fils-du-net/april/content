---
site: Developpez.com
title: "Sénat: un amendement réintroduit une disposition visant à promouvoir le logiciel libre et les formats ouverts"
author: Michael Guilloux
date: 2016-04-27
href: http://www.developpez.com/actu/98224/Senat-un-amendement-reintroduit-une-disposition-visant-a-promouvoir-le-logiciel-libre-et-les-formats-ouverts-au-sein-des-administrations-francaises
tags:
- Administration
- Institutions
---

> Le débat sur le choix entre logiciel libre et logiciel propriétaire est sans cesse relancé dans les discussions des politiques et parlementaires français. Il y a à peine quelques jours que la version 2 du Référentiel Général d’Interopérabilité (RGI) a été validée par un arrêté du Premier ministre français. Le RGI est un document qui décrit les normes et bonnes pratiques communes aux administrations publiques françaises dans le domaine informatique.
