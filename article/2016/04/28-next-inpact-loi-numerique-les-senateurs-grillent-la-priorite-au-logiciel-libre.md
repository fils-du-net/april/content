---
site: Next INpact
title: "Loi Numérique: les sénateurs grillent la priorité au logiciel libre"
author: Marc Rees
date: 2016-04-28
href: http://www.nextinpact.com/news/99643-loi-numerique-senateurs-grillent-priorite-au-logiciel-libre.htm
tags:
- Administration
- April
- Institutions
- Marchés publics
---

> Dans le cadre des débats autour du projet de loi Lemaire, les sénateurs ont finalement refusé d’accorder la priorité au logiciel libre dans la vie des administrations. En lieu et place, ils ont adopté un amendement du groupe socialiste se limitant à encourager ces licences.
