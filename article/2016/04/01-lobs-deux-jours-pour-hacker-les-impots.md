---
site: L'OBS
title: "Deux jours pour hacker les impôts"
author: Thierry Noisette
date: 2016-04-01
href: http://rue89.nouvelobs.com/2016/04/01/deux-jours-hacker-les-impots-263649
tags:
- Administration
- Institutions
- Innovation
- Open Data
---

> Bercy a ouvert le capot de son calculateur d’impôt sur le revenu, et propose aux codeurs de s’en servir jusqu’à samedi soir. Revenu universel, optimisateur fiscal... les bidouilleurs ne manquent pas d’envies.
