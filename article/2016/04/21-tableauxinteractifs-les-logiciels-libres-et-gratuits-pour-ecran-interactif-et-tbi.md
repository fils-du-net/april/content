---
site: TableauxInteractifs
title: "Les logiciels libres et gratuits pour ecran interactif et TBI"
date: 2016-04-21
href: http://www.tableauxinteractifs.fr/2016/04/liberez-les-logiciels
tags:
- Administration
- Interopérabilité
- RGI
---

> Le Socle interministériel des logiciels libres a édité en 2016 une liste d’une centaine de logiciels libres. Parmi ceux-ci, certains peuvent être utilisés dans les écoles, les administrations et les entreprises.
