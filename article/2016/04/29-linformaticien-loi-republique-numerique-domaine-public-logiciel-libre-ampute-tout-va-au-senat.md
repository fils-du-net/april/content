---
site: L'Informaticien
title: "Loi République numérique: domaine public, logiciel libre… on ampute à tout-va au Sénat"
author: Guillaume Périssat
date: 2016-04-29
href: http://www.linformaticien.com/actualites/id/40362/loi-republique-numerique-domaine-public-logiciel-libre-on-ampute-a-tout-va-au-senat.aspx
tags:
- Administration
- Institutions
- Droit d'auteur
- Marchés publics
- Open Data
---

> Trois jours de débats du matin au soir, 23 articles et plus de 400 amendements examinés, des joutes verbales dignes des plus célèbres «battles» de rap… Qui a dit qu’on ne sait pas s’amuser au Sénat? A mi-parcours, faisons un point sur certains points emblématiques du projet de loi République numérique.
