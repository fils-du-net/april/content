---
site: LaDepeche.fr
title: "Liberté et sécurité retrouvées avec l'installation de systèmes libres"
author: Frédéric Pascaud
date: 2016-04-10
href: http://www.ladepeche.fr/article/2016/04/10/2321965-liberte-et-securite-retrouvees-avec-l-installation-de-systemes-libres.html
tags:
- April
- Institutions
- Sensibilisation
- Associations
- Vie privée
---

> La semaine du logiciel libre s'est terminée hier, avec comme point d'orgue l'installation de systèmes d'exploitation libres dans la salle du bureau d'information jeunesse.
