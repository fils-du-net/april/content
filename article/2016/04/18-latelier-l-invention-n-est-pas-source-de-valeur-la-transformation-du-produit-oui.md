---
site: L'Atelier
title: "«L’invention n’est pas source de valeur, la transformation du produit, oui»"
author: Anna Bochu
date: 2016-04-18
href: http://www.atelier.net/trends/articles/invention-source-de-transformation-produit-oui_441237
tags:
- Brevets logiciels
- Innovation
---

> En quoi les brevets peuvent-ils parfois freiner la dynamique de création? Réponse avec Hacène Lahrèche, scientifique français faisant partie de l'équipe dirigeante de La Paillasse, premier laboratoire citoyen d'Europe.
