---
site: La gazette.frj
title: "Le numérique public: cache-sexe du déni des droits ou levier de solidarité?"
author: Valérie Peugeot et Michel Briand
date: 2016-04-13
href: http://www.lagazettedescommunes.com/437476/le-numerique-public-cache-sexe-du-deni-des-droits-ou-levier-de-solidarite
tags:
- Internet
- Économie
- Partage du savoir
- Institutions
- Accessibilité
- Associations
- Éducation
---

> Plus que jamais un enjeu majeur, la médiation numérique sous toutes ses formes doit être prise en main par les acteurs publics.
