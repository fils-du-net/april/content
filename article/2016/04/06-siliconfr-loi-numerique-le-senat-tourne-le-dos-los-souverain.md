---
site: Silicon.fr
title: "Loi Numérique: le Sénat tourne le dos à l'OS souverain"
author: Ariane Beky
date: 2016-04-06
href: http://www.silicon.fr/loi-numerique-senat-os-souverain-logiciels-libres-144052.html
tags:
- April
- Institutions
---

> Le projet de loi pour une République Numérique est amendé en commission des lois au Sénat. Le système d’exploitation souverain est écarté, l’encouragement aux logiciels libres oublié.
