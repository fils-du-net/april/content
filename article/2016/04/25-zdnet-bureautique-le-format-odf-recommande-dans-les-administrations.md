---
site: ZDNet
title: "Bureautique: le format ODF recommandé dans les administrations"
author: Thierry Noisette
date: 2016-04-25
href: http://www.zdnet.fr/actualites/bureautique-le-format-odf-recommande-dans-les-administrations-39836012.htm
tags:
- Administration
- Interopérabilité
- April
- HADOPI
- RGI
- Standards
---

> La nouvelle version du référentiel général d'interopérabilité recommande le format ODF dans les administrations et pointe les défauts de l'OOXML de Microsoft. Le RGI s'appuie par ailleurs sur Wikipedia.
