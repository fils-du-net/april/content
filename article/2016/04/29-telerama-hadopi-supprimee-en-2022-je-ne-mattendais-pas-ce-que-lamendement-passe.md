---
site: Télérama
title: "Hadopi supprimée en 2022: “Je ne m'attendais pas à ce que l'amendement passe”"
author: Olivier Tesquet
date: 2016-04-29
href: http://www.telerama.fr/medias/hadopi-supprimee-en-2022-je-ne-m-attendais-pas-a-ce-que-l-amendement-passe,141720.php
tags:
- HADOPI
- Institutions
---

> La député Isabelle Attard a réussi, à la surprise générale, a faire passer un amendement qui prévoit en 2022 la fin de la haute autorité chargée de la lutte contre le téléchargement illégal. Elle s'en explique.
