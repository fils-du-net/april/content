---
site: Silicon.fr
title: "Les États-Unis affûtent leur projet Open Source, la France aussi"
author: Ariane Beky
date: 2016-04-05
href: http://www.silicon.fr/etats-unis-affutent-projet-open-source-france-aussi-143895.html
tags:
- Administration
- Innovation
- Promotion
- RGI
- International
- Open Data
---

> Les États-Unis veulent accélérer le partage et la réutilisation du code source de logiciels développés par ou pour le gouvernement fédéral américain. En France, l’administration fiscale a mis à disposition le code source de son calculateur d’impôts lors d’un hackaton.
