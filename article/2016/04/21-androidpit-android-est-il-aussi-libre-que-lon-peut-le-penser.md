---
site: AndroidPIT
title: "Android est-il aussi libre que l'on peut le penser?"
author: Benoit Pepicq
date: 2016-04-21
href: http://www.androidpit.fr/android-est-il-aussi-libre-que-l-on-peut-le-penser
tags:
- Logiciels privateurs
- Licenses
---

> Vous avez entendu à de nombreuses reprises qu'Android appartient à Google et que ce dernier tire les ficelles. Pourtant, vous avez probablement entendu également qu'Android est un système ouvert dans lequel chacun peut faire ce qu'il veut. Au final, Android est-il un système ouvert ou non? Nous vous expliquons tout dans notre article.
