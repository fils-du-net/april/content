---
site: LeMagIT
title: "Logiciels libres et secteur public: le contrat de support étendu aux agences du gouvernement"
author: Cyrille Chausson
date: 2016-04-05
href: http://www.lemagit.fr/actualites/450280683/Logiciels-libres-et-secteur-public-le-contrat-de-support-etendu-aux-agences-du-gouvernement
tags:
- Administration
- Marchés publics
- RGI
---

> Publié la semaine dernière au BOAMP, l’avis de l’appel d’offres portant sur le support en logiciels libres des ministères se voit étendu aux agences du gouvernement comme l’ADEME ou l’Ecole polytechnique. L’accord-cadre, renouvelé, a aussi été remodelé et ne propose plus de barème dégressif.
