---
site: ActuaLitté.com
title: "Edito: l'ebook, c'est Cosette qui cire les pompe de Cendrillon"
author: Nicolas Gary
date: 2016-04-08
href: https://www.actualitte.com/article/edito/edito-l-ebook-c-est-cosette-qui-cire-les-pompe-de-cendrillon/64051
tags:
- Économie
- April
- Institutions
- DRM
- Europe
---

> Le livre est mort, vive le livre! Depuis des années, l’industrie du livre mène un combat pour faire comprendre à l’Europe qu’un livre reste un livre, qu’il soit de feuilles collées ou de bits codés. Depuis hier, c’est chose faite: la Commission européenne a accepté l’idée de modifier la TVA «pour les publications électroniques». Gloria in excelsis Deo! En fait, presque...
