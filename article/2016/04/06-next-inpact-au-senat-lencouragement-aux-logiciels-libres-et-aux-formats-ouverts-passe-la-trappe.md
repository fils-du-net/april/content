---
site: Next INpact
title: "Au Sénat, l'encouragement aux logiciels libres et aux formats ouverts passe à la trappe"
author: Xavier Berne
date: 2016-04-06
href: http://www.nextinpact.com/news/99367-au-senat-l-encouragement-aux-logiciels-libres-et-aux-formats-ouverts-passe-a-trappe.htm
tags:
- April
- Institutions
- Promotion
- Open Data
---

> La commission des lois du Sénat s’est opposée ce matin à ce que les administrations recourent prioritairement aux logiciels libres et aux formats ouverts. Les élus ont carrément fait disparaître le vague «encouragement» prévu jusqu’ici par le texte porté par Axelle Lemaire.
