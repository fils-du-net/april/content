---
site: Le Figaro
title: "Le gouvernement lance un hackathon autour des impôts"
author: Jean-Marc De Jaeger
date: 2016-04-01
href: http://www.lefigaro.fr/secteur/high-tech/2016/04/01/32001-20160401ARTFIG00248-le-gouvernement-lance-un-hackathon-autour-des-impots.php
tags:
- Administration
- Institutions
- Innovation
---

> L'administration fiscale a rendu public le code source du calculateur de l'impôt sur le revenu et de l'impôt sur la fortune. Les développeurs sont invités à imaginer de nouveaux services pour les contribuables.
