---
site: Developpez.com
title: "Europe: l'open source va-t-il être exclu des standards pour le Marché Numérique Unique?"
author: Michael Guilloux
date: 2016-04-21
href: http://www.developpez.com/actu/98036/Europe-l-open-source-va-t-il-etre-exclu-des-standards-pour-le-Marche-Numerique-Unique-L-UE-penche-vers-le-modele-de-licence-FRAND
tags:
- Économie
- Institutions
- Marchés publics
- Standards
- Informatique en nuage
- Europe
---

> Dans le cadre de sa nouvelle stratégie pour le Digital Single Market (Marché Numérique Unique) présentée l’année dernière, la Commission européenne (CE) a décidé de faire de la normalisation des technologies de l’information et de la communication (TIC) la pierre angulaire.
