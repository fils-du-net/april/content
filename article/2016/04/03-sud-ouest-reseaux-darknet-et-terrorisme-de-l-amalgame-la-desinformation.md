---
site: Sud Ouest
title: "Réseaux: Darknet et terrorisme, de l’amalgame à la désinformation"
author: Jean-Philippe Rennard
date: 2016-04-03
href: http://www.sudouest.fr/2016/04/03/reseaux-darknet-et-terrorisme-de-l-amalgame-a-la-desinformation-2319912-4725.php
tags:
- Internet
- Institutions
- Vie privée
---

> Si l’on en croit les autorités, en France comme ailleurs, l’interdiction des échanges anonymes et confidentiels permettrait de repérer et de neutraliser les groupes terroristes, analyse un expert
