---
site: Next INpact
title: "Au Sénat, l'impressionnant détricotage de la loi Numérique"
author: Xavier Berne
date: 2016-04-07
href: http://www.nextinpact.com/news/99369-au-senat-l-impressionnant-detricotage-loi-numerique.htm
tags:
- Internet
- Administration
- HADOPI
- Institutions
- Associations
- Open Data
- Vie privée
---

> La commission des lois du Sénat, qui a examiné hier plus de 400 amendements relatifs au projet de loi Numérique, s’est tout particulièrement employée à détricoter les mesures introduites en janvier par les députés: action collective en matière de données personnelles, amende de 20 millions d’euros pour la CNIL, obligations relatives à l’IPV6, rapport sur les consultations en ligne... Explications.
