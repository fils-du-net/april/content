---
site: La gazette.fr
title: "«L’open data est de plus en plus ancré dans les pratiques»"
author: Sabine Blanc
date: 2016-04-22
href: http://www.lagazettedescommunes.com/438262/lopen-data-est-de-plus-en-plus-ancre-dans-les-pratiques
tags:
- Administration
- Institutions
- Associations
- Open Data
---

> Laure Lucchiesi a pris la succession d'Henri Verdier à la tête d'Etalab, la mission en charge de l'ouverture des données publiques. Elle revient pour La Gazette sur les chantiers de l'open data en France, dans les administrations centrales et les territoires.
