---
site: "usine-digitale.fr"
title: "Quelle propriété pour les logiciels libres?"
author: Pascal Agosti
date: 2016-04-15
href: http://www.usine-digitale.fr/article/quelle-propriete-pour-les-logiciels-libres.N387317
tags:
- Entreprise
- Institutions
---

> Le numérique ne bouleverse pas que les business models. Pour le prendre en compte, les règles et les lois sont elles aussi en pleine mutation. Chaque semaine, les avocats Eric Caprioli, Pascal Agosti, Isabelle Cantero et Ilène Choukri se relaient pour nous fournir des clés pour déchiffrer les évolutions juridiques et judiciaires nées de la digitalisation: informatique, cybersécurité, protection des données, respect de la vie privée... Aujourd’hui, regard sur le Droit du logiciel libre... car il existe.
