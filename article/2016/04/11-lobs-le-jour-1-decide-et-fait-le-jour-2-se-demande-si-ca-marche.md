---
site: L'OBS
title: "«Le jour 1, on décide et on fait. Le jour 2, on se demande si ça a marché»"
author: Benoît Le Corre
date: 2016-04-11
href: http://rue89.nouvelobs.com/2016/04/11/nuitdebout-a-recherche-lapplication-parfaite-263729
tags:
- Internet
- Partage du savoir
- Institutions
- Associations
---

> Depuis le début du mouvement #NuitDebout, les manifestants sont en quête des meilleurs outils informatiques qui leur permettront de s’organiser, de ne pas être surveillés, mais qui devront surtout être en accord avec les visions politiques des participants.
