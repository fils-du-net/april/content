---
site: Numerama
title: "Priorité au logiciel libre dans l'administration: le Sénat vote un texte mou"
author: Guillaume Champeau
date: 2016-04-27
href: http://www.numerama.com/politique/166891-priorite-au-logiciel-libre-dans-ladministration-le-senat-vote-un-texte-mi-figues-mi-raisins.html
tags:
- Administration
- Institutions
- Marchés publics
---

> Les sénateurs ont adopté mercredi un amendement qui demande à l'administration d'«encourager l'utilisation des logiciels libres» dans ses choix informatiques, mais sans aller jusqu'à leur «donner la priorité», comme le souhaitaient certains élus.
