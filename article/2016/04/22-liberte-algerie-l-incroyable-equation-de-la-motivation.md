---
site: Liberté Algérie
title: "L’incroyable équation de la motivation"
author: Sadek Amrouche
date: 2016-04-22
href: http://www.liberte-algerie.com/cap-enp-liberte-algeriecom/lincroyable-equation-de-la-motivation-246245
tags:
- Économie
- Sciences
---

> Nous sommes en 1945, Karl Dunker, célèbre psychologue allemand, présente pour la première fois les résultats de son expérience intitulée «the candle problem» (le problème de la bougie). Cette expérience est un test rapide permettant de mesurer les capacités d’un individu à résoudre un problème. Il est très utilisé aujourd’hui dans une grande variété d’études en sciences du comportement. Si vous ne connaissez pas l’expérience, lisez ce qui suit et cherchez la solution avant de regarder la réponse.
