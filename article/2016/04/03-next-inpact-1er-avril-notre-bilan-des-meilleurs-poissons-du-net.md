---
site: Next INpact
title: "1er avril: notre bilan des meilleurs poissons du Net"
author: Sébastien Gavois
date: 2016-04-03
href: http://www.nextinpact.com/news/99306-recap-1er-avril-tour-dhorizon-meilleurs-poissons-net.htm
tags:
- Internet
- April
---

> Durant la journée d'hier, la pêche du 1er avril a été abondante. C'est en effet la profusion de poisson chez Google (parfois avec des couacs), tandis que la RATP en profite pour moderniser le nom de ses stations. Pour le reste, il est question de Pierre Gattaz, de Windows dans Linux, etc.
