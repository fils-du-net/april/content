---
site: Unidivers
title: "Imprimante 3D: panorama des débats et des ruptures"
author: Thibault Boixiere
date: 2016-04-22
href: http://www.unidivers.fr/imprimante-3d-technologie-debats-ruptures
tags:
- Économie
- Matériel libre
- Innovation
---

> L’imprimante 3D. Le nom lui-même fait penser à la dimension spectaculaire du cinéma en relief. On confond la lanterne magique avec la camera obscura. Pourtant, l’impression tridimensionnelle n’est pas un procédé technique récent. Ses implications restent néanmoins actuelles. Entre industrialisation et démocratisation, cette technologie suscite le débat et provoque la rupture. Your next stop, the Twilight Zone…
