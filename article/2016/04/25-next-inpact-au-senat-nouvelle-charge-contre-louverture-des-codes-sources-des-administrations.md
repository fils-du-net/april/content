---
site: Next INpact
title: "Au Sénat, nouvelle charge contre l'ouverture des codes sources des administrations"
author: Xavier Berne
date: 2016-04-25
href: http://www.nextinpact.com/news/99591-au-senat-nouvelle-charge-contre-l-ouverture-codes-sources-administrations.htm
tags:
- Administration
- April
- HADOPI
- Institutions
- Open Data
---

> Alors que les discussions relatives au projet de loi Numérique doivent débuter demain au Sénat, un parlementaire vient de déposer un amendement s’opposant à l’ouverture du code source des administrations – au travers d’une argumentation qui risque d'en laisser plus d'un pantois.
