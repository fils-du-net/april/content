---
site: CIO
title: "Le RGI 2.0 consacre les formats ouverts et les nouveaux standards"
author: Bertrand Lemaire
date: 2016-04-25
href: http://www.cio-online.com/actualites/lire-le-rgi-20-consacre-les-formats-ouverts-et-les-nouveaux-standards-8417.html
tags:
- Administration
- Interopérabilité
- RGI
- Standards
---

> La deuxième version du Référentiel Général d'Interopérabilité (RGI) a été officialisée le 22 avril 2016. Elle s'applique à tout la sphère publique.
