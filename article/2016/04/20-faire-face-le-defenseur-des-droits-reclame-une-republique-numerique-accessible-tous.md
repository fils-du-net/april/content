---
site: Faire Face
title: "Le Défenseur des droits réclame une République numérique accessible à tous"
author: Franck Seuret
date: 2016-04-20
href: http://www.faire-face.fr/2016/04/20/defenseur-droits-reclame-republique-numerique-accessible
tags:
- Institutions
- Accessibilité
---

> Le Défenseur des droits estime que le projet de loi pour une République numérique ne garantit pas suffisamment l’accessibilité des services numériques aux utilisateurs handicapés. Recommandations à la clé.
