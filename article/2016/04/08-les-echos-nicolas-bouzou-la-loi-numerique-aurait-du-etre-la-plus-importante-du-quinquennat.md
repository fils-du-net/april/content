---
site: Les Echos
title: "Nicolas Bouzou: «La loi numérique aurait dû être la plus importante du quinquennat»"
author: Nicolas Rauline
date: 2016-04-08
href: http://www.lesechos.fr/idees-debats/sciences-prospective/021822542516-nicolas-bouzou-la-loi-numerique-aurait-du-etre-la-plus-importante-du-quinquennat-1212471.php
tags:
- Institutions
- Marchés publics
- Open Data
---

> Il y a un écart terrible entre l'importance du sujet et son rendu final. On est devant une loi assez technique, qui manque d'un récit derrière. On a un peu l'impression d'avoir une juxtaposition de mesures, certaines très utiles d'ailleurs, mais il manque un souffle, une philosophie. On a peut-être voulu mettre trop de choses dans la loi et l'on a du mal à saisir la cohérence de l'ensemble. Au Royaume-Uni, par exemple, la société s'est emparée du sujet de l'« open data ».
