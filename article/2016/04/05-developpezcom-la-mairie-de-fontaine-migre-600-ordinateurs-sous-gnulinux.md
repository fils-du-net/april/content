---
site: Developpez.com
title: "La Mairie de Fontaine migre 600 ordinateurs sous GNU/Linux"
author: Grégory Époutierez
date: 2016-04-05
href: http://open-source.developpez.com/actu/97552/La-Mairie-de-Fontaine-migre-600-ordinateurs-sous-GNU-Linux-elle-nous-fait-partager-son-experience-des-logiciels-libres
tags:
- Administration
- Économie
- Sensibilisation
---

> Nicolas Vivant est directeur du système d’information à la mairie de Fontaine, en Isère. Depuis quelques années, il s’attelle à la migration du parc informatique vers des solutions logicielles libres.
