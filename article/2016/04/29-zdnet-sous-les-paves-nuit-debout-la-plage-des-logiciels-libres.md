---
site: ZDNet
title: "Sous les pavés Nuit Debout, la plage des logiciels libres"
author: Thierry Noisette
date: 2016-04-29
href: http://www.zdnet.fr/actualites/sous-les-paves-nuit-debout-la-plage-des-logiciels-libres-39836268.htm
tags:
- Internet
- Sensibilisation
- Associations
---

> Qui sont les geeks qui participent au mouvement né de l'opposition au projet de loi travail? Ils défendent le Libre, mais dur de convaincre le grand public.
