---
site: Numerama
title: "Richard Stallman: «Plus rien ne me fait rêver dans la technologie»"
author: Guillaume Champeau
date: 2016-04-19
href: http://www.numerama.com/tech/164017-richard-stallman-plus-rien-ne-me-fait-rever-dans-la-technologie.html
tags:
- Institutions
- Sensibilisation
- Philosophie GNU
- Vie privée
---

> Entretien avec Richard Stallman, l'inventeur du logiciel libre, résolument attaché aux libertés et au combat contre toutes les technologies qu'il juge «oppressives».
