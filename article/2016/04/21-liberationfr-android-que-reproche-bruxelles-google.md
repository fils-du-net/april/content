---
site: Libération.fr
title: "Android: que reproche Bruxelles à Google?"
author: Anaïs Cherif
date: 2016-04-21
href: http://www.liberation.fr/futurs/2016/04/21/android-que-reproche-bruxelles-a-google_1447667
tags:
- Entreprise
- Économie
- Institutions
- Europe
---

> La Commission européenne a adressé au géant américain une liste de griefs contre son système d’exploitation pour smartphones. Il est accusé de ne pas respecter les règles de la concurrence.
