---
site: LeMagIT
title: "OS, poste de travail, suite bureautique: osez les alternatives!"
author: Gene Demaitre
date: 2016-04-29
href: http://www.lemagit.fr/conseil/OS-poste-de-travail-et-suite-bureautique-oser-les-alternatives
tags:
- Entreprise
- Promotion
---

> Les administrateurs réfléchissant à des alternatives libres aux postes de travail Windows devraient savoir ce qu’offre déjà la communauté.
