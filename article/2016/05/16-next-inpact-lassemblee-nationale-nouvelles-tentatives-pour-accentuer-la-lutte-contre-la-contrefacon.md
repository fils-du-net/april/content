---
site: Next INpact
title: "À l'Assemblée nationale, nouvelles tentatives pour accentuer la lutte contre la contrefaçon"
author: Marc Rees
date: 2016-05-16
href: http://www.nextinpact.com/news/99850-a-l-assemblee-nationale-nouvelles-tentatives-pour-accentuer-lutte-contre-contrefacon.htm
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Droit d'auteur
- Promotion
- Vie privée
---

> Cent fois sur le métier remettez votre ouvrage! Ainsi pourrait se résumer une série d’amendements déposés par plusieurs élus LR dans le cadre du projet de loi sur l’action de groupe. L’objectif? Aiguiser la lutte contre la contrefaçon en ligne.
