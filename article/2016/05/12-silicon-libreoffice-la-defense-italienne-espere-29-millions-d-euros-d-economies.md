---
site: Silicon
title: "LibreOffice: la Défense italienne espère 29 millions d’euros d’économies"
author: Jacques Cheminat
date: 2016-05-12
href: http://www.silicon.fr/libreoffice-la-defense-italienne-attend-29-millions-deuros-deconomies-147291.html
tags:
- Administration
- Économie
- Associations
- International
---

> Annoncé à la fin de l’année dernière, le projet de migration de la Défense Italienne vers LibreOffice concerne 100 000 postes.
