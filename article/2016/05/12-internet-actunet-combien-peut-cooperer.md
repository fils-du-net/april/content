---
site: internet ACTU.net
title: "A combien peut-on coopérer?"
author: Jean-Michel Cornu
date: 2016-05-12
href: http://www.internetactu.net/2016/05/12/a-combien-peut-on-cooperer
tags:
- Sciences
---

> De même que Linux a montré dans les années 90 qu’il était possible de travailler à plusieurs centaines… sous certaines conditions, des succès des années 2000 comme Wikipédia ou Open Streetmap montrent qu’il est possible de faire une encyclopédie ou une cartographie à plusieurs dizaines de milliers de personnes voire plus. Peut-on comprendre comment développer des écosystèmes (d’équipes projets ou de communautés) de plusieurs dizaines de milliers de personnes?
