---
site: Libération
title: "56Kast #75: l'impossible combat contre la vente forcée"
author: Camille Gévaudan
date: 2016-05-20
href: http://www.liberation.fr/futurs/2016/05/20/56kast-75-l-impossible-combat-contre-la-vente-forcee_1453382
tags:
- April
- Vente liée
---

> Cette semaine, notre invité, Laurent Costy, raconte les cinq ans de procès qui l’ont opposé à Hewlett-Packard. Il cherchait simplement à se faire rembourser le système d’exploitation livré d’office sur son ordinateur…
