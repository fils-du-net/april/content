---
site: Sciences et avenir
title: "Le Sénat planche toujours sur la Loi numérique"
date: 2016-05-02
href: http://www.sciencesetavenir.fr/high-tech/web/20160502.OBS9634/le-senat-planche-toujours-sur-la-loi-numerique.html
tags:
- Internet
- Administration
- Institutions
- Droit d'auteur
---

> Les sénateurs reprennent lundi 2 mai 2016 l'examen en première lecture du projet de loi numérique. Il prévoit l'ouverture accrue des données publiques, une meilleure protection pour les internautes et un accès amélioré à internet.
