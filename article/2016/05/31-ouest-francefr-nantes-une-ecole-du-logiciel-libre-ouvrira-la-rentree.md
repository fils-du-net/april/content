---
site: "ouest-france.fr"
title: "Nantes: une école du logiciel libre ouvrira à la rentrée"
date: 2016-05-31
href: http://www.ouest-france.fr/pays-de-la-loire/nantes-44000/nantes-une-ecole-du-logiciel-libre-ouvrira-la-rentree-4265057
tags:
- Entreprise
- Éducation
---

> Elle a été lancée ce mardi matin dans les locaux du start-up palace. Une école du logiciel libre et des solutions open source ouvrira à la rentrée à Nantes.
