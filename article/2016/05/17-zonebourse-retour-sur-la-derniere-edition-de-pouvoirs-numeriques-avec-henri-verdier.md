---
site: zonebourse
title: "Retour sur la dernière édition de Pouvoirs Numériques avec Henri Verdier"
date: 2016-05-17
href: http://www.zonebourse.com/actualite-bourse/TECH-IN-France-Retour-sur-la-derniere-edition-de-Pouvoirs-Numeriques-avec-Henri-Verdier--22374265/
tags:
- Institutions
- Marchés publics
- Open Data
---

> Concernant la neutralité de la commande publique, Henri Verdier a précisé qu'il ne souhaitait pas faire du logiciel libre un sujet sectaire indiquant «apprécier» le premier alinéa de la nouvelle version de l'article 9 ter du projet de loi numérique,en ce qu'il clarifie les objectifs recherchés par l'administration lorsqu'elle privilégie un logiciel libre
