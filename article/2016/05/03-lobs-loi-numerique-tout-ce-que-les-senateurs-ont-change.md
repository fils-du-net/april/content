---
site: L'OBS
title: "Loi numérique: tout ce que les sénateurs ont changé"
author: Andréa Fradin
date: 2016-05-03
href: http://rue89.nouvelobs.com/2016/05/03/loi-numerique-tout-les-senateurs-ont-change-263930
tags:
- Internet
- Institutions
- Open Data
---

> Le Sénat vient de boucler ses discussions sur le texte d’Axelle Lemaire. Il le marque d’une certaine défiance vis-à-vis de l’open data et d’une vive offensive contre les plateformes comme Airbnb et leurs utilisateurs.
