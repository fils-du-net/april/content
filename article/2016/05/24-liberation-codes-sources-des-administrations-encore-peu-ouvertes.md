---
site: Libération
title: "Codes sources: des administrations encore peu ouvertes"
author: Amaelle Guiton
date: 2016-05-24
href: http://www.liberation.fr/france/2016/05/24/codes-sources-des-administrations-encore-peu-ouvertes_1454951
tags:
- Administration
- April
- Partage du savoir
- Institutions
---

> Le secrétaire d’Etat à l’Enseignement supérieur a promis de lever un peu le flou sur APB d’ici mardi, tandis que des initiatives d’usagers ont déjà permis de voir plus clair dans l’algorithme des impôts.
