---
site: Slate.fr
title: "Fabriquer des produits en open source, une révolution prometteuse"
author: Chisato Goya
date: 2016-05-26
href: http://www.slate.fr/story/101667/avenir-prometteur-open-source-hardware
tags:
- Entreprise
- Économie
- Matériel libre
- Innovation
---

> L'open source permet aussi de développer des produits physiques. Mais le matériel libre doit relever encore quelques défis avant de vraiment se démocratiser.
