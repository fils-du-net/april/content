---
site: Reporterre
title: "Halte à l’obsolescence! Les vieux ordinateurs reprennent vie avec le système Emmabuntüs"
author: Émilie Massemin
date: 2016-05-28
href: http://reporterre.net/Halte-a-l-obsolescence-Les-vieux-ordinateurs-reprennent-vie-avec-le-systeme
tags:
- Économie
- Associations
- Promotion
- Sciences
---

> L’obsolescence programmée n’est pas une fatalité. Telles est la conviction du collectif Emmabuntüs, qui remet en état de vieux ordinateurs destinés à la casse et les dote d’une distribution libre et facile d’utilisation au bénéfice des plus modestes. Très active dans les communautés Emmaüs, l’équipe envoie des machine jusqu’en Afrique. Mais l’un de ses militants actifs, d’origine guinéenne, est aujourd’hui menacé d’expulsion.
