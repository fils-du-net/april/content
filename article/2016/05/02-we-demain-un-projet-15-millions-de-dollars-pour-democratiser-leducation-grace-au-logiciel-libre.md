---
site: We Demain
title: "Un projet à 15 millions de dollars pour démocratiser l'éducation grâce au logiciel libre"
author: Lara Charmeil
date: 2016-05-02
href: http://www.wedemain.fr/Un-projet-a-15-millions-de-dollars-pour-democratiser-l-education-grace-au-logiciel-libre_a1822.html
tags:
- Institutions
- Éducation
- Innovation
- International
---

> Issues de 33 pays, 136 équipes vont développer des logiciels éducatifs à destination des enfants défavorisés. Les projets lauréats seront testés en Tanzanie. À l'origine de cette opération inédite, la fondation XPrize, l'UNESCO et le Programme alimentaire mondial.
