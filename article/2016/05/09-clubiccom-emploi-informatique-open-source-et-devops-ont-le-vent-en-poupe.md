---
site: Clubic.com
title: "Emploi informatique: open source et devops ont le vent en poupe"
author: Alexandre Laurent
date: 2016-05-09
href: http://www.clubic.com/pro/emploi-informatique.clubic.com/actualite-805324-emploi-informatique-open-source-devops-vent-poupe.html
tags:
- Entreprise
- Informatique en nuage
---

> Les profils qui gravitent autour des technologies open source seraient particulièrement recherchés par les entreprises, indique un rapport réalisé par le cabinet Dice pour la fondation Linux.
> * "[Silicon] Open Source: développeurs et devops, la cible des recruteurs":http://www.silicon.fr/open-source-developpeurs-devops-linux-fondation-dice-146860.html
