---
site: L'OBS
title: "L’open source, pierre angulaire de la démocratie"
author: Thomas Watanabe-Vermorel
date: 2016-05-30
href: http://rue89.nouvelobs.com/2016/05/30/lopen-source-pierre-angulaire-democratie-264184
tags:
- Administration
- Institutions
- Vote électronique
---

> Rue89 pointait récemment du doigt l’opacité du système informatique qui distribue les affectations post-bac. Au-delà du caractère arbitraire de la décision qui conditionne l’avenir de toute une génération, l’affaire illustre la problématique de la sûreté dans le monde numérique.
