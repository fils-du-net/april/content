---
site: Next INpact
title: "Une résolution européenne française pour bétonner le droit d'auteur"
author: Marc Rees
date: 2016-05-17
href: http://www.nextinpact.com/news/99866-une-resolution-europeenne-francaise-pour-betonner-droit-dauteur.htm
tags:
- Institutions
- Droit d'auteur
- Europe
---

> Comment court-circuiter les débats parlementaires sur le projet de loi Création? Simple: en faisant adopter une résolution européenne sur la protection du droit d’auteur dans l’Union européenne.
