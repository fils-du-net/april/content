---
site: DNA
title: "Bière «open source»"
author: Christian Bach
date: 2016-05-24
href: http://www.dna.fr/economie/2016/05/24/biere-open-source
tags:
- Partage du savoir
- Associations
---

> Quand les militants du logiciel libre se lient d’amitié avec un artisan brasseur, il en sort quoi? Une bière «open source», la Seeraiwer, née à Niederhausbergen dans les cuves de La Mercière. Une première du genre en Alsace, à notre connaissance…
