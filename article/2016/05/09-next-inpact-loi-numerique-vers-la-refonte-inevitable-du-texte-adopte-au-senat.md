---
site: Next INpact
title: "Loi Numérique: vers la refonte inévitable du texte adopté au Sénat"
author: Marc Rees
date: 2016-05-09
href: http://www.nextinpact.com/news/99751-loi-numerique-vers-refonte-inevitable-texte-adopte-au-senat.htm
tags:
- Internet
- Institutions
- Open Data
- Vie privée
---

> Le projet de loi sur la République numérique a été adopté la semaine dernière par les sénateurs. Un chiffre retiendra notre attention: le nombre d’amendements votés contre l’avis du gouvernement.
