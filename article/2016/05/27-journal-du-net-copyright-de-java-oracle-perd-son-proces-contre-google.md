---
site: Journal du Net
title: "Copyright de Java: Oracle perd son procès contre Google"
author: Antoine Crochet-Damais
date: 2016-05-27
href: http://www.journaldunet.com/solutions/dsi/1179162-copyright-de-java-oracle-perd-son-proces-contre-google
tags:
- Entreprise
- Institutions
- Droit d'auteur
- International
---

> Un jury vient de se prononcer aux États-Unis: Google ne viole pas les droits de propriété intellectuelle d'Oracle en utilisant ses API Java dans Android.
