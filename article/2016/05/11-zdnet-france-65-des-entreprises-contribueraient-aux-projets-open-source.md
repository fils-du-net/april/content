---
site: ZDNet France
title: "65% des entreprises contribueraient aux projets open source"
date: 2016-05-11
href: http://www.zdnet.fr/actualites/65-des-entreprises-contribueraient-aux-projets-open-source-39836732.htm
tags:
- Entreprise
---

> Une étude fait le point sur les contributions des entreprises aux projets open source et leur utilisation en environnement professionnel. Les containers open source ont le vent en poupe.
