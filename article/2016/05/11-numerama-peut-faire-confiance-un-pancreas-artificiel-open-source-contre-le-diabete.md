---
site: Numerama
title: "Peut-on faire confiance à un pancréas artificiel open source contre le diabète?"
author: Omar Belkaab
date: 2016-05-11
href: http://www.numerama.com/sciences/169614-pancreas-artificiel-open-source-vaincre-diabete.html
tags:
- Innovation
- Sciences
---

> Faute de solution industrielle homologuée par les autorités sanitaires, des malades atteints de diabète de type 1 s'en remettent à une solution open source pour réguler leur taux de glucose dans le sang, avec une pompe à insuline connectée. Mais est-ce raisonnable?
