---
site: Journal du Net
title: "La revanche des SourceForgeurs!"
author: Samir Amellal
date: 2016-05-13
href: http://www.journaldunet.com/solutions/expert/64308/la-revanche-des-sourceforgeurs.shtml
tags:
- Entreprise
- Innovation
- Promotion
---

> Qui aurait parié il y a quelques années sur ces illuminés, ces geeks qui semblaient plus bidouiller qu’autre chose et dont les choix technologiques semblaient plus être le résultat de choix par défaut que le fruit d’une stratégie!
