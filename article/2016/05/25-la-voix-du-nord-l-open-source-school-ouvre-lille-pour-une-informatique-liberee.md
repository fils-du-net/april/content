---
site: La Voix du Nord
title: "L’Open Source School ouvre à Lille pour une informatique libérée"
author: Jean-Marc Petit
date: 2016-05-25
href: http://www.lavoixdunord.fr/region/l-open-source-school-ouvre-a-lille-pour-une-informatique-ia19b0n3529716
tags:
- Entreprise
- Économie
- Éducation
---

> La première école française du logiciel libre (Open Source School) va ouvrir à Lille à la rentrée. Une formation répondant aux besoins croissant d’un marché où travaillent déjà 50 000 personnes en France.
