---
site: ZDNet
title: "Danese Cooper (PayPal): ”L'open source, c'est les gens”"
author: Thierry Noisette
date: 2016-05-21
href: http://www.zdnet.fr/actualites/danese-cooper-paypal-l-open-source-c-est-les-gens-39837160.htm
tags:
- Entreprise
- Institutions
- Brevets logiciels
---

> Responsable en chef de l'open source chez PayPal et de longue date liée au Libre (Sun, Drupal, Apache, Wikimedia...), Danese Cooper souligne des principes clés, transparence en tête.
