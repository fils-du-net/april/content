---
site: Next INpact
title: "Des députés veulent coupler vidéosurveillance et reconnaissance faciale"
author: Xavier Berne
date: 2016-05-17
href: http://www.nextinpact.com/news/99862-des-deputes-veulent-coupler-videosurveillance-et-reconnaissance-faciale.htm
tags:
- Institutions
- Vie privée
---

> Une dizaine de députés Les Républicains, menés par le très droitier Éric Ciotti, s’apprête à défendre un amendement autorisant les forces de l’ordre à recourir à des logiciels capables de reconnaître – en temps réel – le visage de certaines personnes à partir des images retransmises par des caméras de surveillance.
