---
site: ActuaLitté.com
title: "France: pour le livre, l'absence d'interopérabilité “est insupportable”"
author: Nicolas Gary
date: 2016-05-31
href: https://www.actualitte.com/article/lecture-numerique/france-pour-le-livre-l-absence-d-interoperabilite-est-insupportable/65225
tags:
- Interopérabilité
- Institutions
- DRM
- Europe
---

> En mai 2015, Andrus Ansip, vice-président de la Commission européenne, présentait la stratégie numérique de l’Europe pour le marché unique numérique. Parmi les grands piliers qui sous-tendaient les propositions, la volonté ferme de «définir les priorités en matière de normes et d’interopérabilité». L’ensemble s’articulait autour d’une facilitation d’accès aux biens et services numériques pour les consommateurs et les entreprises, à travers les États. La France a décidé de porter le combat plus loin.
