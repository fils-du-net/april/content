---
site: ITespresso
title: "«République numérique»: le Sénat a décortiqué l'open data"
author: Clément Bohic
date: 2016-05-03
href: http://www.itespresso.fr/republique-numerique-senat-decortique-open-data-128301.html
tags:
- Institutions
- Open Data
- Vie privée
---

> Le projet de loi numérique adopté ce 3 mai par le Sénat a fait l'objet, la semaine passée, de nombreux amendements sur le volet «économie de la donnée».
