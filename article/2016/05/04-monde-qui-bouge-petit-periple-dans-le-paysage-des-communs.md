---
site: Monde qui bouge
title: "Petit périple dans le paysage des Communs"
author: Dominique Nalpas
date: 2016-05-04
href: http://www.mondequibouge.be/index.php/2016/05/petit-periple-dans-le-paysage-des-communs
tags:
- Économie
- Droit d'auteur
---

> Un concept qui ne fait pas toujours l’unanimité et suscite même des inquiétudes mais qui se développe de plus en plus, à Bruxelles comme ailleurs. Essai de décryptage.
