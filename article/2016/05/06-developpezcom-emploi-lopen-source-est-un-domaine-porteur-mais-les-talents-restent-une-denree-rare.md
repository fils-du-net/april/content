---
site: Developpez.com
title: "Emploi: l'open source est un domaine porteur, mais les talents restent une denrée rare"
author: Michael Guilloux
date: 2016-05-06
href: http://www.developpez.com/actu/98431/Emploi-l-open-source-est-un-domaine-porteur-mais-les-talents-restent-une-denree-rare-revele-l-Open-Source-Jobs-Report-de-2016
tags:
- Entreprise
- Économie
---

> Lunivers des technologies reposant sur lopen source sagrandit et la demande de professionnels dans ce domaine suit la tendance. La fondation Linux en partenariat avec Dice, un site de recherche demploi pour les professionnels de la technologie, vient de publier son rapport annuel sur lemploi dans le monde open source, lOpen Source Jobs Report. À la fois recruteurs et professionnels de lopen source ont été interrogés pour avoir un aperçu du paysage de lemploi dans le domaine. Lenquête a ...
