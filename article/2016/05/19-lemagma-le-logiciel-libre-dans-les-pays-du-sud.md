---
site: Lemag.ma
title: "Le logiciel libre dans les pays du sud"
date: 2016-05-19
href: http://www.lemag.ma/Le-logiciel-libre-dans-les-pays-du-sud-theme-d-un-colloque-international-les-23-et-24-mai-a-Meknes_a100286.html
tags:
- Innovation
- Promotion
- International
---

> L’Ecole Nationale Supérieure d’Arts et Métiers (ENSAM) organise, les 23 et 24 mai à Meknès, un colloque international sur "Le logiciel libre dans les pays du sud: outil stratégique de formation, d'innovation et de développement durable".
