---
site: InformatiqueNews.fr
title: "Pourquoi pas un SGBD open source?"
author: La rédaction
date: 2016-05-03
href: http://www.informatiquenews.fr/sgbd-open-source-46861
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Standards
---

> Alors que le marché des bases de données s’est stabilisé autour du trio Oracle/IBM/Microsoft et plus récemment SAP, les bases de données open source ont gagné en maturité et deviennent un choix réaliste.
