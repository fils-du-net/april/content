---
site: Le Monde Informatique
title: "Oracle, grand perdant du procès pour contrefaçon contre Google"
author: James Niccolai
date: 2016-05-27
href: http://www.lemondeinformatique.fr/actualites/lire-oracle-grand-perdant-du-proces-pour-contrefacon-contre-google-64961.html
tags:
- Entreprise
- Institutions
- Droit d'auteur
- International
---

> Hier, le jury du tribunal de San Francisco a considéré que l'usage de Java dans Android était couvert par le « fair use » et que par conséquent Google ne violait pas le droit d'auteur de Java dans l'affaire l'opposant à Oracle. Mais tout n'est pas fini pour autant, puisqu'Oracle a annoncé qu'il ferait appel de cette décision.
