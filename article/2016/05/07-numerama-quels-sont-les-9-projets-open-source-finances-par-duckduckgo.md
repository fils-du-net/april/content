---
site: Numerama
title: "Quels sont les 9 projets open source financés par DuckDuckGo?"
author: Julien Lausson
date: 2016-05-07
href: http://www.numerama.com/tech/168426-quels-sont-les-9-projets-open-source-finances-par-duckduckgo.html
tags:
- Internet
- Économie
- Sciences
- Vie privée
---

> Le site DuckDuckGo dévoile le nom des neuf lauréats qui ont droit à une enveloppe de 25 000 dollars pour poursuivre le développement de leur projet open source.
