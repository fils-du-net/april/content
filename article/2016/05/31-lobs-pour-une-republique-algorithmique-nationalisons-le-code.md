---
site: L'OBS
title: "Pour une république algorithmique: nationalisons le code!"
author: Olivier Ertzscheid
date: 2016-05-31
href: http://rue89.nouvelobs.com/2016/05/31/republique-algorithmique-nationalisons-code-264193
tags:
- Entreprise
- Partage du savoir
- Innovation
---

> Derrière l’ouverture du code source de nombreux programmes d’intelligence artificielle par les géants du Net, demeure l’opacité d’algorithmes clés. Une question politique essentielle.
