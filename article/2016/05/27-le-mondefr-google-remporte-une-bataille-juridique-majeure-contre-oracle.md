---
site: Le Monde.fr
title: "Google remporte une bataille juridique majeure contre Oracle"
date: 2016-05-27
href: http://www.lemonde.fr/pixels/article/2016/05/27/google-remporte-une-bataille-juridique-majeure-contre-oracle_4927528_4408996.html
tags:
- Entreprise
- Institutions
- Droit d'auteur
- International
---

> C’était un procès à neuf milliards de dollars (huit milliards d’euros). La somme que réclamait Oracle, le géant des logiciels, à Google, qu’elle accusait d’avoir utilisé, sans payer de licence, des fonctionnalités de son programme Java pour concevoir Android. Google arguait de son côté que son utilisation de Java tombait sous la notion américaine de «fair use» – le droit d’utiliser de manière modérée une œuvre de l’esprit comme source d’inspiration ou pour un remix.
