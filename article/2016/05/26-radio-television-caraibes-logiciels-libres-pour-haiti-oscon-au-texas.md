---
site: Radio Television Caraibes
title: "Logiciels libres pour Haïti à OSCON au Texas"
author: Marc-Arthur Pierre-Louis
date: 2016-05-26
href: http://www.radiotelevisioncaraibes.com/nouvelles/high-tech/logiciels_libres_pour_ha_ti_oscon_au_texas.html
tags:
- Entreprise
- Promotion
- Standards
- International
---

> Un logiciel libre ou Open Source Software est un logiciel gratuit pouvant être légalement et techniquement étudié, modifié, dupliqué pour diffusion. L’ubiquité des technologies libres n’est plus à établir. À travers le monde on organise des conférences qui les mettent en relief. La maison d’édition des livres de technologie O’Reilly organise la sienne: OSCON. Cette année, du 17 au 19 mai, l’événement se tient à Austin, la capitale du Texas, là où j’ai passé huit ans de ma vie. Le mouvement des logiciels libres est une mouvance sociale.
