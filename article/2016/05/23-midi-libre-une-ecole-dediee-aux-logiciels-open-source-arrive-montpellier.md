---
site: Midi Libre
title: "Une école dédiée aux logiciels open source arrive à Montpellier"
date: 2016-05-23
href: http://www.midilibre.fr/2016/05/23/une-ecole-dediee-aux-logiciels-open-source-arrive-a-montpellier,1336780.php
tags:
- Entreprise
- Éducation
---

> L 'école est une première en France et veut lutter contre la pénurie de compétences dans le secteur.
