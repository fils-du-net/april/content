---
site: Developpez.com
title: "Procès API Java: la victoire de Google signifie la mort du logiciel libre et de la GPL"
author: Michael Guilloux
date: 2016-05-28
href: http://www.developpez.com/actu/99113/Proces-API-Java-la-victoire-de-Google-signifie-la-mort-du-logiciel-libre-et-de-la-GPL-estime-Annette-Hurst-avocate-d-Oracle
tags:
- Entreprise
- Institutions
- Droit d'auteur
- International
---

> Après quelques années de bataille entre Google et Oracle sur la copie des API Java dans Android, l’industrie du logiciel a accueilli hier la victoire du géant du Mountain View. Mais encore une fois, Oracle n’entend pas abandonner et envisage de faire appel pour remettre en cause cette décision.
