---
site: Silicon
title: "Open Source: développeurs et devops, la cible des recruteurs"
author: Ariane Beky
date: 2016-05-09
href: http://www.silicon.fr/open-source-developpeurs-devops-linux-fondation-dice-146860.html
tags:
- Entreprise
---

> 59 % des recruteurs interrogés par la Fondation Linux et le site Dice prévoient d'embaucher des professionnels Open Source d'ici la fin 2016.
