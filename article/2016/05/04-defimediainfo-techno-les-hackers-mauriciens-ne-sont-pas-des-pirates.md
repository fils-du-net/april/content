---
site: Defimedia.info
title: "Techno: les hackers mauriciens ne sont pas des pirates"
author: Patrice Donzelot
date: 2016-05-04
href: http://defimedia.info/techno-les-hackers-mauriciens-ne-sont-pas-des-pirates-27618
tags:
- Associations
- Promotion
- International
---

> Redonner aux «hackers» leurs lettres de noblesse et mettre en avant les talents des Mauriciens dans le codage, tels sont les objectifs de passionnés locaux regroupés dans le collectif ‘hackers.mu’. Le premier message que ces férus d’informatique expliquent, c’est qu’un ‘hacker’ n’est pas un pirate.
