---
site: Paris Match
title: "Frédéric Salat-Baroux: ”Juppé peut être l’homme de la situation”"
author: Bruno Jeudy
date: 2016-05-03
href: http://www.parismatch.com/Actu/Politique/Frederic-Salat-Baroux-Juppe-peut-etre-l-homme-de-la-situation-960412
tags:
- Administration
- Institutions
- Éducation
- Innovation
---

> Le fait marquant est le grand retour des organisations coopératives hier laminées par la révolution industrielle. Le logiciel libre est plus efficace que Microsoft. Toutes les entreprises cherchent à intégrer cette réalité avec l'Open Source. L'Etat doit le faire aussi. Il doit simplifier ses structures, non dans une logique ultra-libérale, mais pour avancer vers "l'administration participative". C'est ce qu'a commencé la ville de Paris avec son budget participatif d’investissements.
