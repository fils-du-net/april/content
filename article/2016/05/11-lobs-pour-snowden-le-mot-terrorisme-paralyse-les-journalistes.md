---
site: L'OBS
title: "Pour Snowden, le mot «terrorisme» paralyse les journalistes"
author: Thierry Noisette
date: 2016-05-11
href: http://rue89.nouvelobs.com/2016/05/11/snowden-mot-terrorisme-paralyse-les-journalistes-264003
tags:
- Partage du savoir
- Institutions
- Vie privée
---

> Des journalistes nécessaires mais souvent pas assez méfiants envers les pouvoirs publics, une concurrence qui amène les médias à ne pas traiter une info: comment Edward Snowden voit la presse et ses acteurs.
