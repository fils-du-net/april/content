---
site: La revue européenne des médias et du numérique
title: "Les communs: la théorie du milieu."
author: Françoise Laugée
date: 2016-05-26
href: http://la-rem.eu/2016/05/26/communs-theorie-milieu/
tags:
- Internet
- Économie
- Institutions
- Associations
---

> A la mesure de la place qu’elle occupe dans les médias, «l’économie de partage» (sharing economy) ou «économie collaborative» est devenue une expression courante. Pas un jour sans que soit publiée une chronique sur ce sujet, auquel est désormais associée la notion de «communs». Dans une société fragilisée par les bouleversements majeurs liés à l’impact de la numérisation sur l’ensemble des secteurs d’activité, on pourrait s’attendre à plus de pertinence. Or de nombreux discours d’experts, économistes ou non, sèment la confusion.
