---
site: Le Monde Informatique
title: "Les DSI redoutent les audits de licences"
author: Laurent Mavallet
date: 2016-12-21
href: http://www.lemondeinformatique.fr/actualites/lire-les-dsi-redoutent-les-audits-de-licences-66855.html
tags:
- Entreprise
- Logiciels privateurs
---

> Administration de système Informatique: Une étude de Snow Software pointe le risque ressenti par les DSI associé aux audits de licences réalisés par les éditeurs de logiciels.
