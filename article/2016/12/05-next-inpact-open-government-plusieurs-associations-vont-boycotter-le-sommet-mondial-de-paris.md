---
site: Next INpact
title: "Open Government: plusieurs associations vont boycotter le sommet mondial de Paris"
author: Xavier Berne
date: 2016-12-05
href: http://www.nextinpact.com/news/102375-open-government-plusieurs-associations-vont-boycotter-sommet-mondial-paris.htm
tags:
- Administration
- April
- Institutions
- Associations
- Open Data
---

> Alors que la France s’apprête à accueillir le sommet mondial du Partenariat pour un gouvernement ouvert, sorte de «COP21 de la démocratie», plusieurs associations ont annoncé aujourd’hui qu’elles boycotteraient l’événement. En cause : des choix «radicalement incompatibles» avec l’esprit du PGO, par exemple sur le fichier TES ou les logiciels libres.
