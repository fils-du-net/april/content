---
site: Next INpact
title: "L'April fête ses 20 ans et rappelle que «la servitude s’avance masquée»"
author: Sébastien Gavois
date: 2016-12-20
href: http://www.nextinpact.com/news/102596-lapril-fete-ses-20-ans-et-rappelle-que-servitude-s-avance-masquee.htm
tags:
- April
---

> L'April vient de souffler sa vingtième bougie, mais l'association ne compte pas en rester là et compte bien continuer son combat autour du logiciel libre. Le 11 janvier, une fête se déroulera dans les locaux où l'association est née en 1996, à l'université de Paris 8.
