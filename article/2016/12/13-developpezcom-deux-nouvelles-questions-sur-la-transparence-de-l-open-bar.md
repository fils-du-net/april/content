---
site: Developpez.com
title: "Deux nouvelles questions sur la transparence de l'«Open Bar»"
author: zoom61
date: 2016-12-13
href: http://open-source.developpez.com/actu/108288/Deux-nouvelles-questions-sur-la-transparence-de-l-Open-Bar-par-Mme-Isabelle-Attard-et-Mme-Joelle-Garriaud-Maylam-deux-parlementaires
tags:
- Entreprise
- Administration
- April
- Institutions
- Marchés publics
---

> Lors de l'émission Cash Investigation du 18 octobre 2016 qui a consacrée une large partie sur l'accord entre Microsoft et la Défense. Dans les semaines suivantes, l'April avait demandé la création d'une commission d'enquête parlementaire concernant les relations entre l'État et Microsoft.
