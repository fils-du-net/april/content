---
site: Numerama
title: "L'Union européenne investit près de 2 millions d'euros pour renforcer sa sécurité informatique"
author: Corentin Durand
date: 2016-12-05
href: http://www.numerama.com/politique/214305-lunion-europeenne-investit-pres-de-2-millions-deuros-pour-renforcer-sa-securite-informatique.html
tags:
- Institutions
- Europe
---

> Le budget 2017 de l'Union Européenne consacrera plus de 1,9 million d'euros au soutien des logiciels libres et à la création d'un programme de Bug Bounty.
