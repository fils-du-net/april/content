---
site: UP Le Mag
title: "Diaspora, un réseau social qui respecte la vie privée"
author: Héloïse Leussier
date: 2016-12-23
href: http://www.up-inspirer.fr/32254-diaspora-reseau-social-respecte-vie-privee
tags:
- Internet
- Vie privée
---

> Diaspora est un réseau social qui permet de publier messages et photos tout en protégeant les données des utilisateurs. Une alternative crédible au géant Facebook.
