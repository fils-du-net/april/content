---
site: Framablog
title: "L’April vient d’avoir 20 ans"
author: Véronique Bonnet
date: 2016-12-20
href: https://framablog.org/2016/12/20/l-april-a-20-ans
tags:
- April
- Promotion
---

> Ces jours-ci, l’April a eu 20 ans. Et toutes ses dents. Pas les dents de l’amer GAFAM, crocs avides des requins du Web et autres loups. Des dents, plutôt, qui ne mâchent pas leurs mots pour dénoncer l’inventivité souriante, glaçante, de firmes qui veulent continuer à dominer. Pour dire ce qu’un partenariat entre un ministère chargé d’éduquer à l’autonomie et Microsoft a de troublant. Pour s’étonner de l’open bar opaque de la Défense.
