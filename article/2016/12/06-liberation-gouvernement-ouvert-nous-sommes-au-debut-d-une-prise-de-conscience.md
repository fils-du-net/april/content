---
site: Libération
title: "Gouvernement ouvert: «Nous sommes au début d'une prise de conscience»"
author: Amaelle Guiton
date: 2016-12-06
href: http://www.liberation.fr/futurs/2016/12/06/gouvernement-ouvert-nous-sommes-au-debut-d-une-prise-de-conscience_1533419
tags:
- Administration
- Institutions
- Associations
- Open Data
---

> Alors que s'ouvre ce mercredi à Paris le quatrième sommet mondial du Partenariat pour un gouvernement ouvert (PGO), Laure Lucchesi, directrice d'Etalab et porte-parole de l'événement, en explique les enjeux.
