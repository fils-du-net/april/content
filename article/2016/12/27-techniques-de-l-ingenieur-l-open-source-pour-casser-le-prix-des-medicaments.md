---
site: Techniques de l'Ingénieur
title: "L’Open source pour casser le prix des médicaments"
author: Philippe Richard
date: 2016-12-27
href: http://www.techniques-ingenieur.fr/actualite/articles/lopen-source-pour-casser-le-prix-des-medicaments-39017
tags:
- Entreprise
- Économie
- Innovation
- Sciences
---

> Racheté par un fonds d’investissement, un médicament a vu son prix augmenter de 5.000%! Des lycéens australiens ont trouvé la parade pour le proposer à un prix correct.
