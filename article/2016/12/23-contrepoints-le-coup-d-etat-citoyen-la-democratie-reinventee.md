---
site: Contrepoints
title: "Le coup d’État citoyen: la démocratie réinventée"
author: Farid Gueham
date: 2016-12-23
href: https://www.contrepoints.org/2016/12/23/275830-coup-detat-citoyen-democratie-reinventee
tags:
- Institutions
- Innovation
---

> Une transition est en cours, une mutation qui s’appuie largement sur les technologies de l’open source. La renaissance démocratique est en marche.
