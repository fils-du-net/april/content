---
site: Silicon
title: "Sécurité: l'Europe inclut un bug bounty à son audit logiciel Open Source"
author: Ariane Beky
date: 2016-12-06
href: http://www.silicon.fr/securite-europe-bug-bounty-audit-logiciel-open-source-164362.html
tags:
- Institutions
- Europe
---

> Bug bounty oblige, un budget de 1,9 million d'euros accompagne l'extension du programme d'audit des logiciels Libres et Open Source EU-FOSSA.
