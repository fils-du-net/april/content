---
site: "cio-online.com"
title: "L'amortissement en douze mois des logiciels supprimé: SaaS et Open-Source favorisés"
author: Bertrand Lemaire
date: 2016-12-21
href: http://www.cio-online.com/actualites/lire-l-amortissement-en-douze-mois-des-logiciels-supprime-saas-et-open-source-favorises-8992.html
tags:
- Entreprise
- Économie
- Institutions
---

> Tech In France dénonce la suppression de l'amortissement rapide en douze mois des logiciels et craint que cela réduise les investissements en achats de logiciels par les entreprises. Mais les logiciels en mode locatif (SaaS ou non) et open-source seraient ainsi fiscalement favorisés. La Loi de Finances a été adoptée définitivement le 20 décembre 2016 et sera promulguée d'ici la fin de l'année 2016.
