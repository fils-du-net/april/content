---
site: Developpez.com
title: "L'Union européenne augmente le budget alloué à sa sécurité informatique"
author: Coriolan
date: 2016-12-06
href: http://www.developpez.com/actu/107425/L-Union-europeenne-augmente-le-budget-alloue-a-sa-securite-informatique-et-met-en-place-un-programme-de-chasse-aux-bogues
tags:
- Institutions
- Europe
---

> De nos jours, la question de la cybersécurité est devenue l’une des préoccupations clés des États et gouvernements partout dans le monde. En Europe notamment, les administrations ont été la cible de cyberattaques intensives, ce qui a poussé l’Union européenne à multiplier les efforts pour garder son autonomie et améliorer la sécurité informatique.
