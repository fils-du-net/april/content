---
site: Next INpact
title: "La France et l'Allemagne marquent leur attachement au logiciel libre"
author: Xavier Berne
date: 2016-12-14
href: http://www.nextinpact.com/news/102521-la-france-et-allemagne-marquent-leur-attachement-au-logiciel-libre.htm
tags:
- Administration
- April
- Institutions
- Innovation
- Promotion
---

> À l’issue de la seconde «conférence numérique franco-allemande», qui s’est tenue hier à Berlin, la France et l’Allemagne ont tenu à afficher leurs ambitions en matière de nouvelles technologies. Le logiciel libre y est même évoqué.
