---
site: La Tribune
title: "Démocratie ouverte: le sommet mondial cher à Hollande débute sur un gros clash"
author: Sylvain Rolland
date: 2016-12-07
href: http://www.latribune.fr/technos-medias/internet/democratie-ouverte-le-sommet-mondial-cher-a-hollande-debute-sur-un-gros-clash-622769.html
tags:
- Administration
- April
- Institutions
- Associations
- Open Data
---

> La tension monte alors que s’ouvre, à partir de mercredi 7 décembre, le sommet mondial annuel du Partenariat pour un gouvernement ouvert (PGO), qui s’apparente à la COP21 de la démocratie. Alors que la France se pose en exemple mondial en matière de transparence de la vie publique, onze associations dénoncent "l’affichage" du gouvernement. Certaines boycotteront même l’événement. Axelle Lemaire leur a vertement répondu.
