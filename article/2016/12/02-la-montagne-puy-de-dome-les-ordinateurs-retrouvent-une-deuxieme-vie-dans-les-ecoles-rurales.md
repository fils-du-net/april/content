---
site: la montagne
title: "Puy-de-Dôme: les ordinateurs retrouvent une deuxième vie dans les écoles rurales"
author: Pierre Peyret
date: 2016-12-02
href: http://www.lamontagne.fr/murol/ruralite/vie-associative/2016/12/02/puy-de-dome-les-ordinateurs-retrouvent-une-deuxieme-vie-dans-les-ecoles-rurales_12191456.html
tags:
- Administration
- Économie
- Associations
---

> Dans le Puy-de-Dôme, tous les ordinateurs jugés obsolètes ne finissent pas à la benne. L’association des maires ruraux en a déjà reconditionné et distribué plus de 400 à des écoles.
