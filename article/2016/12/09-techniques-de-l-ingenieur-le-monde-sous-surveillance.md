---
site: Techniques de l'Ingénieur
title: "Le monde sous surveillance"
author: Philippe Richard
date: 2016-12-09
href: http://www.techniques-ingenieur.fr/actualite/articles/le-monde-sous-surveillance-38752
tags:
- Entreprise
- Internet
- Vie privée
---

> Tous nos pas dans le cyberespace sont suivis, enregistrés, analysés, et nos profils se monnayent en permanence. Face à ce «tracking» de masse, un livre donne des solutions accessibles au grand public.
