---
site: Le Temps
title: "Les dérives managériales de la théorie des incitations"
author: Michel Ferrary
date: 2016-12-01
href: https://www.letemps.ch/economie/2016/12/01/derives-manageriales-theorie-incitations
tags:
- Entreprise
- Économie
---

> Comment amener le salarié à adopter le comportement attendu par l’employeur quand ce dernier ne peut pas le contrôler?
