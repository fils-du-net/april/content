---
site: Libération
title: "Pour des états généraux de l’Internet indépendant"
author: Olivier Ertzscheid
date: 2016-12-15
href: http://www.liberation.fr/debats/2016/12/15/pour-des-etats-generaux-de-l-internet-independant_1535520
tags:
- Entreprise
- Internet
- Institutions
---

> Alors que les géants du Net ont rencontré mercredi le futur président des Etats-Unis, il y a urgence à organiser au niveau international une régulation citoyenne et éthique du monde connecté. Avant que ce dernier ne se transforme en cauchemar.
