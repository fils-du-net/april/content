---
site: Silicon
title: "Loïc Rivière, Tech in France: «attention aux écarts populistes sur le numérique»"
author: Reynald Fl00e9chaux
date: 2016-12-09
href: http://www.silicon.fr/loic-riviere-tech-in-france-halte-populisme-numerique-164663.html
tags:
- Entreprise
- Économie
- Institutions
---

> Loïc Rivière, le délégué général de Tech in France, dresse un bilan des années Hollande, en particulier marquées par la sortie de la Loi République numérique. Et tance une certaine forme d’instrumentalisation politique des débats sur le sujet.
