---
site: Le Monde.fr
title: "Comment le numérique change la politique"
date: 2016-12-22
href: http://www.lemonde.fr/idees/article/2016/12/22/comment-le-numerique-change-la-politique_5053079_3232.html
tags:
- Internet
- Administration
- Institutions
- Innovation
- Open Data
---

> Le numérique nous fait entrer dans un nouvel âge du politique, avec les «civic tech», ces technologies qui doivent permettre aux citoyens de prendre part à la prise de décision publique et de se mobiliser pour l’intérêt général. Cette démocratie numérique demande à être maîtrisée.
