---
site: Digital Society Forum
title: "Vers une démocratie 2.0?"
author: Christine Treguier
date: 2016-12-15
href: http://digital-society-forum.orange.com/fr/les-forums/901-vers-une-democratie-20
tags:
- Internet
- Institutions
- Innovation
---

> L’expression «Démocratie 2.0» prête à sourire. Elle est caricaturale, car jamais les réseaux sociaux et l’interactivité sur la toile ne remplaceront totalement les modes et systèmes d’une démocratie, que celle-ci soit représentative, participative ou même directe. Elle pose en revanche la question d’un potentiel: les pratiques du numérique et surtout les rêves démocratiques que ses acteurs tentent de concrétiser ont-ils les moyens de renouveler la manière de gouverner et d’être gouverné, depuis la réfection du lampadaire de sa rue jusqu’à des décisions impliquant le monde entier?
