---
site: ParisTech Review
title: "Le pouvoir intellectuel à l'ère numérique"
author: Fred Turner
date: 2016-12-12
href: http://www.paristechreview.com/2016/12/12/pouvoir-intellectuel-numerique
tags:
- Économie
- Innovation
- Promotion
---

> La révolution numérique a redéfini notre culture littéraire et intellectuelle. Trois Américains illustrent ce processus: Norbert Wiener, le célèbre fondateur de la cybernétique; Stewart Brand, un hippie des années 70 reconverti en entrepreneur à succès; et, plus récemment, Tim O’Reilly, l’inventeur de formules comme «Web 2.0» et «open source» qui ont un profond impact sur notre façon de voir le monde. Ces intellectuels d’un nouveau genre travaillent à la façon d’entrepreneurs de réseau. Trois étapes permettent à ce nouveau type d’intellectuel d’asseoir sa position d’influence.
