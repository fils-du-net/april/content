---
site: Le Monde.fr
title: "Démocratie participative: le sommet de Paris s’ouvre sur fond de polémiques"
author: Sandrine Cassini et Claire Legros
date: 2016-12-05
href: http://www.lemonde.fr/pixels/article/2016/12/05/democratie-participative-le-sommet-de-paris-s-ouvre-sur-fond-de-polemiques_5043813_4408996.html
tags:
- Administration
- Institutions
- Associations
- Vie privée
---

> A la veille de l’ouverture à Paris du sommet mondial du Partenariat pour un gouvernement ouvert, des associations dénoncent les contradictions de la France en matière de démocratie numérique.
