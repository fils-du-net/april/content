---
site: Le Telegramme
title: "Install Party. Li-bé-rez les ordinateurs!"
date: 2016-12-05
href: http://www.letelegramme.fr/morbihan/auray/install-party-li-be-rez-les-ordinateurs-05-12-2016-11318892.php
tags:
- Logiciels privateurs
- Administration
- Sensibilisation
- Associations
---

> Les bénévoles de l'association Rhizome ont mis leur talent de surdoués de l'informatique samedi, au profit des utilisateurs d'ordinateurs qui souhaitaient installer un logiciel libre sur leur machine. Une démarche destinée à faire la nique aux géants du secteur, comme Windows ou Apple.
