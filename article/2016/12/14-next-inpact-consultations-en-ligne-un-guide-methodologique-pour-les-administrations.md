---
site: Next INpact
title: "Consultations en ligne: un guide méthodologique pour les administrations"
author: Xavier Berne
date: 2016-12-14
href: http://www.nextinpact.com/news/102501-consultations-en-ligne-guide-methodologique-pour-administrations.htm
tags:
- Internet
- Institutions
- Vie privée
---

> Afin d’épauler les administrations qui souhaiteraient organiser des consultations en ligne de citoyens, une sorte de guide de bonnes pratiques vient d’être dévoilé. Tout en incitant les responsables politiques à s’engager sur la voix de la «co-construction» des politiques publiques, ce rapport s’oppose fermement à une généralisation de ces initiatives.
