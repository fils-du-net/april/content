---
site: "cio-online.com"
title: "Un vent d'ouverture souffle sur la France et nos entreprises: l'Open Source s'impose!"
author: Véronique Torner
date: 2016-12-15
href: http://www.cio-online.com/actualites/lire--8978.html
tags:
- Entreprise
- Économie
- Promotion
---

> Un mois jour pour jour après l'Open CIO Summit, dont CIO était partenaire, sa présidente, Véronique Torner, tire un bilan du développement de l'Open Source en entreprises.
