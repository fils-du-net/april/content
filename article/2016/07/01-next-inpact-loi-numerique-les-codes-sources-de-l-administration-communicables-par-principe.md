---
site: Next INpact
title: "Loi Numérique: les codes sources de l'administration communicables par principe"
author: Xavier Berne
date: 2016-07-01
href: http://www.nextinpact.com/news/100492-loi-numerique-codes-sources-l-administration-communicables-par-principe.htm
tags:
- Internet
- Administration
- April
- Institutions
---

> Suivant la «jurisprudence» de la CADA et du juge administratif, le législateur s’apprête à faire entrer les codes sources de l’administration dans la liste des documents administratifs communicables de plein droit au citoyen (qui en fait la demande).
