---
site: Next INpact
title: "Windows 10: pourquoi la CNIL met en demeure Microsoft"
author: Marc Rees
date: 2016-07-21
href: http://www.nextinpact.com/news/100719-windows-10-pourquoi-cnil-met-en-demeure-microsoft.htm
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> De nombreux manquements à la loi Informatique et Libertés de 1978. Voilà le reproche qu’a adressé hier en fin de journée la CNIL à Microsoft, et son système d’exploitation Windows 10. L’éditeur a trois mois pour corriger le tir, avant une possible sanction.
