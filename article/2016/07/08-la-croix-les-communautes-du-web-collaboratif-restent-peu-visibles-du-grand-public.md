---
site: la Croix
title: "«Les communautés du Web collaboratif restent peu visibles du grand public»"
author: Julien Duriez
date: 2016-07-08
href: http://www.la-croix.com/Sciences/Numerique/Les-communautes-Web-collaboratif-restent-visibles-grand-public-2016-07-08-1200774524
tags:
- Internet
- Sensibilisation
---

> Cet été, La Croix fait le portrait des communautés des bénévoles de l’Internet libre et collaboratif. En guise d’introduction, entretien avec Dominique Cardon (1). Ce sociologue de la technique, spécialiste des rapports entre nouvelles technologies et mobilisation politique, suit les militants du Web collaboratif depuis leurs débuts.
