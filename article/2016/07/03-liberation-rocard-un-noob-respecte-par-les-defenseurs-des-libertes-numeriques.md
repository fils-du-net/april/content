---
site: Libération
title: "Rocard, un «noob» respecté par les défenseurs des libertés numériques"
author: Erwan Cario
date: 2016-07-03
href: http://www.liberation.fr/futurs/2016/07/03/rocard-un-noob-respecte-par-les-defenseurs-des-libertes-numeriques_1463715
tags:
- HADOPI
- Institutions
- Brevets logiciels
- Europe
---

> Le député européen d'alors, qui reconnaissait ne pas avoir «une pratique facile de l’ordinateur», s'est battu avec succès contre une directive qui menaçait les logiciels libres.
