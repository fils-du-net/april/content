---
site: Numerama
title: "Avec Software Heritage l'Inria crée un sanctuaire pour préserver les logiciels libres"
author: Guillaume Champeau
date: 2016-07-01
href: http://www.numerama.com/tech/179289-avec-software-heritage-linria-cree-un-sanctuaire-pour-preserver-les-logiciels-libres.html
tags:
- Internet
- Administration
- Sciences
- Informatique en nuage
---

> L'Inria dévoile Software Heritage, une plateforme qui ambitionne de réunir et de préserver l'ensemble des logiciels libres distribués sur Internet.
