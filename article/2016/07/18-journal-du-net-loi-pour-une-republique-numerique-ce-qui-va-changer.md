---
site: Journal du Net
title: "Loi pour une République numérique: ce qui va changer"
author: Gregoire Ducret
date: 2016-07-18
href: http://www.journaldunet.com/ebusiness/expert/64845/loi-pour-une-republique-numerique---ce-qui-va-changer.shtml
tags:
- Institutions
- Neutralité du Net
- Open Data
- Vie privée
---

> La version finale du projet de loi pour une République numérique a été décidée le 29 juin. Quels en seront les impacts concrets pour les entreprises? Réponses croisées de l'homme d’affaires et président de l'Acsel Cyril Zimmermann et de l'avocat Eric Barbry.
