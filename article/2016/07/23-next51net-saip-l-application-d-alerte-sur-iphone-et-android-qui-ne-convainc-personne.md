---
site: next51.net
title: "SAIP l'application d'alerte sur iPhone et Android qui ne convainc personne"
author: Next 51
date: 2016-07-23
href: http://www.next51.net/SAIP-l-application-d-alerte-sur-iPhone-et-Android-qui-ne-convainc-personne_a13685.html
tags:
- Internet
- Logiciels privateurs
- Institutions
- Vie privée
---

> Lancée à l'occasion de l'Euro 2016 début juin dernier, l'application d'alerte SAIP sert à compléter l’éventail des dispositifs d’alerte (signalétique urbaine, information diffusée oralement sur le terrain par les forces de sécurité ou via les médias, réseaux sociaux ...).
