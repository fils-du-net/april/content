---
site: Journal du Net
title: "Contribuer à l’open source, un levier de croissance et de dynamisme pour l’entreprise"
author: Arnaud Breton
date: 2016-07-07
href: http://www.journaldunet.com/solutions/expert/64809/contribuer-a-l-open-source--un-levier-de-croissance-et-de-dynamisme-pour-l-entreprise.shtml
tags:
- Entreprise
- Sensibilisation
---

> La production en open source est extrêmement riche et présente de nombreux avantages pour les entreprises dès lors qu’elles souhaitent s’y investir de manière concrète et durable.
