---
site: Localtis.info
title: "Territoire numérique libre, un label pour récompenser les collectivités impliquées dans le logiciel libre"
author: I.E. / EVS
date: 2016-07-13
href: http://www.localtis.info/cs/ContentServer?pagename=Localtis/LOCActu/ArticleActualite$urlcid=1250271104906
tags:
- Administration
- Associations
- Open Data
---

> L'Adullact lance sa première campagne de labellisation autour du logiciel libre dans les collectivités. Dans un contexte global de plus en plus porté vers le libre, le label "Territoire numérique libre" entend récompenser l'engagement des acteurs publics dans les territoires.
