---
site: LeDevoir.com
title: "Un autre monde numérique est possible"
author: Boris Proulx
date: 2016-07-06
href: http://www.ledevoir.com/societe/actualites-en-societe/475953/un-autre-monde-numerique-est-possible
tags:
- April
- Sensibilisation
- Associations
- Brevets logiciels
- Promotion
- International
---

> Le comité des «libristes» du Forum social mondial (FSM) de Montréal entend propager l’idée qu’il est possible d’émanciper la planète du joug de ces entreprises qui verrouillent leurs codes informatiques et qui génèrent un profit sur les informations personnelles de leurs usagers. À l’heure où l’information peut être copiée à l’infini et à coût pratiquement nul, les lois protégeant la propriété intellectuelle sont contestées par les militants, qui rêvent à l’émergence d’un monde où le savoir se transmet sans barrières.
