---
site: France Info
title: "Le Geek de A à Zeid: Open Source"
author: Jean Zeid
date: 2016-07-28
href: http://www.franceinfo.fr/emission/le-geek-de-zeid/2016-ete/le-geek-de-zeid-open-source-29-07-2016-07-41
tags:
- Sensibilisation
---

> Tout l'été avec Jean Zeid, France Info décrypte les expressions issues des nouvelles technologies et qui se sont peu à peu imposées, parfois jusque dans les pages des bons vieux dictionnaires. Le mot du jour: Open Source.
