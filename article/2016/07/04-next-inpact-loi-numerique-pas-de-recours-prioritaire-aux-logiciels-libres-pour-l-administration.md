---
site: Next INpact
title: "Loi Numérique: pas de recours «prioritaire» aux logiciels libres pour l'administration"
author: Xavier Berne
date: 2016-07-04
href: http://www.nextinpact.com/news/100503-loi-numerique-pas-recours-prioritaire-aux-logiciels-libres-pour-l-administration.htm
tags:
- Internet
- Administration
- April
- Institutions
- Marchés publics
- Promotion
---

> En dépit des avis émis par les participants à la consultation sur l'avant-projet de loi Numérique, le législateur n’a pas souhaité imposer aux administrations de recourir «en priorité» aux logiciels libres. Les fonctionnaires devront simplement s’en tenir à un vague «encouragement».
