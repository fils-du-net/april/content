---
site: Archimag
title: "Open source: lancement de Software Heritage, la plus grande bibliothèque de codes source de la planète"
author: Clémence Jost
date: 2016-07-01
href: http://www.archimag.com/demat-cloud/2016/07/01/open-source-software-heritage-archive-codes-source
tags:
- Administration
- Partage du savoir
- Sciences
---

> L'Institut national de recherche en numérique (Inria) vient de lancer la plateforme Software Heritage, qui collecte, organise, préserve et rend accessible le code source de tous les logiciels open source disponibles.
