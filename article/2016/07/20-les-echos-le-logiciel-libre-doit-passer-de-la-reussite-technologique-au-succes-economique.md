---
site: Les Echos
title: "Le logiciel libre doit passer de la réussite technologique au succès économique"
author: Sebastien Dumoulin
date: 2016-07-20
href: http://www.lesechos.fr/idees-debats/sciences-prospective/0211139278397-le-logiciel-libre-doit-passer-de-la-reussite-technologique-au-succes-economique-2015476.php
tags:
- Entreprise
- Économie
- Innovation
---

> Les Gafa ont popularisé l’open source, cependant les entreprises qui vivent du logiciel libre restent rares. Avec l’émergence du cloud et du Big Data, des start-up se sentent pousser des ailes.
