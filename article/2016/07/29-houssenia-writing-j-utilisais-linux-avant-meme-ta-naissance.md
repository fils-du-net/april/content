---
site: Houssenia Writing
title: "J’utilisais Linux avant même ta naissance"
author: Carla Schroder
date: 2016-07-29
href: https://actualite.housseniawriting.com/technologie/2016/07/29/jutilisais-linux-avant-meme-ta-naissance/17035
tags:
- Sensibilisation
---

> À une époque, il n’y avait pas de Linux. Il y avait plusieurs versions d’Unix, il y avait Apple et Microsoft Windows. En mode, c’était mieux avant, une utilisatrice de Linux nous raconte le chemin totalement pourri de la technologie actuelle.
