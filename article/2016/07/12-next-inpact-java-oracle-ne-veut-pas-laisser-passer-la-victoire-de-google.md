---
site: Next INpact
title: "Java: Oracle ne veut pas laisser passer la victoire de Google"
author: Vincent Hermann
date: 2016-07-12
href: http://www.nextinpact.com/news/100607-java-oracle-ne-veut-pas-laisser-passer-victoire-google.htm
tags:
- Entreprise
- Économie
- Institutions
- Droit d'auteur
---

> En mai, Google a infligé une défaite retentissante à Oracle. Ce dernier réclamait 9 milliards de dollars pour avoir violé le copyright de Java et s’en être servi pour assurer le succès d’Android. Un juge en a finalement décidé autrement: il s’agit bien d’un cas de fair use. Qu’Oracle conteste à présent.
