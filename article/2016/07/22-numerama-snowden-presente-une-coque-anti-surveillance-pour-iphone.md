---
site: Numerama
title: "Snowden présente une coque anti-surveillance pour iPhone"
author: Guillaume Champeau
date: 2016-07-22
href: http://www.numerama.com/tech/184460-snowden-presente-coque-anti-surveillance-iphone.html
tags:
- Innovation
- Vie privée
---

> Edward Snowden et le hacker Andrew ‘bunnie’ Huang ont coopéré pour mettre au point ensemble le principe d'une coque pour recharger son iPhone, qui a aussi l'intérêt de prévenir l'utilisateur d'activités suspectes typiques d'une surveillance à distance.
