---
site: Developpez.com
title: "Procès sur les API Java: Oracle ne veut pas s'avouer vaincu et conteste la décision du jury"
author: Stéphane le calme
date: 2016-07-12
href: http://www.developpez.com/actu/101242/Proces-sur-les-API-Java-Oracle-ne-veut-pas-s-avouer-vaincu-et-conteste-la-decision-du-jury-selon-lui-Google-n-en-a-pas-fait-un-usage-acceptable
tags:
- Entreprise
- Institutions
- Droit d'auteur
- Informatique en nuage
- International
---

> Le combat entre les deux entités que sont Oracle et Google semble ne pas être près de la ligne d’arrivée. Pour rappel, Oracle accusait Google d’avoir fait usage des API Java pour le développement de son système d’exploitation mobile Android sans lui avoir payé de licence. Pour ce préjudice, l’entreprise a réclamé 9,3 milliards de dollars au numéro un de la recherche.
