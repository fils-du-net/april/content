---
site: Le Monde.fr
title: "Snowden présente une coque d’iPhone antisurveillance"
date: 2016-07-22
href: http://www.lemonde.fr/pixels/article/2016/07/22/snowden-presente-une-coque-d-iphone-anti-surveillance_4973611_4408996.html
tags:
- Innovation
- Vie privée
---

> Le prototype, développé avec le hackeur Andrew Huang, doit permettre de savoir si un téléphone émet des ondes à l’insu de son utilisateur.
