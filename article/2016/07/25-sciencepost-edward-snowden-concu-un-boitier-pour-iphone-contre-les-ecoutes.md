---
site: SciencePost
title: "Edward Snowden a conçu un boitier pour iPhone contre les écoutes!"
author: Yohan Demeure
date: 2016-07-25
href: http://sciencepost.fr/2016/07/edward-snowden-a-concu-boitier-iphone-contre-ecoutes
tags:
- Innovation
- Vie privée
---

> L’informaticien lanceur d’alerte a conçu une coque pour iPhone destinée à protéger les journalistes et autres activistes de la surveillance des États. En exil en Russie depuis 2013, l’américain ne reste pas inactif et poursuit son combat.
