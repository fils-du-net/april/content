---
site: Le Monde.fr
title: "A peine adopté, l’accord Privacy Shield sur les données personnelles est déjà menacé"
author: Martin Untersinger
date: 2016-07-13
href: http://www.lemonde.fr/pixels/article/2016/07/13/a-peine-adopte-l-accord-privacy-shield-sur-les-donnees-personnelles-est-deja-menace_4969020_4408996.html
tags:
- Entreprise
- Internet
- Institutions
- International
- Vie privée
---

> Le nouveau cadre négocié entre l’Union européenne et les Etats-Unis pour remplacer Safe Harbour pourrait se retrouver bientôt devant la justice.
