---
site: ZDNet France
title: "L'Italie, l'autre pays du logiciel libre. Et la France?"
author: Christophe Auffray
date: 2016-07-01
href: http://www.zdnet.fr/actualites/l-italie-l-autre-pays-du-logiciel-libre-et-la-france-39839168.htm
tags:
- Administration
- April
- Marchés publics
- International
---

> Les services militaires italiens ont entamé leur migration de dizaines de milliers de postes vers LibreOffice, délaissant ainsi Microsoft Office. En France, l'armée a conclu elle un contrat Open Bar avec Microsoft, et les députés encouragent le libre sans lui donner la priorité.
