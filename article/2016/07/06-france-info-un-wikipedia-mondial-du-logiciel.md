---
site: France Info
title: "Un Wikipédia mondial du logiciel"
author: Jérôme Colombain
date: 2016-07-06
href: http://www.franceinfo.fr/emission/nouveau-monde/2015-2016/un-wikipedia-du-logiciel-06-07-2016-07-00
tags:
- Administration
- Partage du savoir
- Sciences
---

> Les logiciels constituent aujourd’hui une part importante de notre savoir collectif mais ils peuvent se perdre. Pour éviter cela, des Français ont décidé de créer une sorte de bibliothèque mondiale du logiciel.
