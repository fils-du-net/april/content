---
site: Numerama
title: "La Bulgarie impose l'open source dans l'administration"
author: Corentin Durand
date: 2016-07-08
href: http://www.numerama.com/politique/181184-la-bulgarie-impose-lopen-source-dans-ladministration.html
tags:
- Administration
- Institutions
- International
---

> La Bulgarie s'est dotée d'une loi déterminante pour sa gouvernance informatique. Désormais, toutes les réalisations et commandes de l'État devront être open source mais également réutilisables et publiées sur une plateforme gouvernementale.
