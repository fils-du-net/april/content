---
site: la Croix
title: "L'aventure de GNU/Linux"
author: Audrey Dufour
date: 2016-07-27
href: http://www.la-croix.com/Sciences/Numerique/L-aventure-GNU-Linux-2016-07-27-1200778499
tags:
- Sensibilisation
---

> CHRONOLOGIE – De 1969 à nos jours, retrouvez les étapes majeures de GNU et Linux.
