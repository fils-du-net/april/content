---
site: Next INpact
title: "Le partenariat entre Microsoft et l’Éducation nationale en piste pour les tribunaux"
author: Xavier Berne
date: 2016-07-28
href: http://www.nextinpact.com/news/100778-le-partenariat-microsoft-et-l-education-nationale-en-piste-pour-tribunaux.htm
tags:
- Entreprise
- Logiciels privateurs
- Vente liée
- Associations
- Éducation
- Marchés publics
---

> L’accord conclu l’année dernière entre Microsoft et le ministère de l’Éducation nationale continue de faire des vagues. Alors qu’un parlementaire interpelle l’exécutif, le dossier s'apprête à prendre un tournant judiciaire.
