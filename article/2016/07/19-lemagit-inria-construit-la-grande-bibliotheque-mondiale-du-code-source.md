---
site: LeMagIT
title: "Inria construit la grande bibliothèque mondiale du code source"
author: Sophy Caulier
date: 2016-07-19
href: http://www.lemagit.fr/actualites/450300582/Inria-construit-la-grande-bibliotheque-mondiale-du-code-source
tags:
- Entreprise
- Administration
- Partage du savoir
---

> Le projet Software Heritage a l'ambition de collecter et d'organiser le patrimoine logiciel mondial. Démarrée il y a déjà un an et demi, cette initiative veut être une gigantesque bibliothèque du code source. Le projet initié par Inria a déjà reçu plusieurs soutiens dont le premier de Microsoft.
