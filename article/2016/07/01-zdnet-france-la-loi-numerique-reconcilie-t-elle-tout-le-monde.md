---
site: ZDNet France
title: "La loi numérique réconcilie-t-elle tout le monde?"
author: Christophe Auffray
date: 2016-07-01
href: http://www.zdnet.fr/actualites/la-loi-numerique-reconcilie-t-elle-tout-le-monde-39839182.htm
tags:
- Administration
- April
- Institutions
- Promotion
---

> Sénateurs et députés n'auront pas obtenu tout ce qu'ils souhaitaient avec la loi République numérique, dont la localisation des données personnelles en Europe. Le gouvernement se déclare satisfait, tout comme les industriels du numérique.
