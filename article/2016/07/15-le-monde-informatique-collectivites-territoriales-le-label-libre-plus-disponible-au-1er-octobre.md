---
site: Le Monde Informatique
title: "Collectivités territoriales: Le label libre plus disponible au 1er octobre"
author: Didier Barathon
date: 2016-07-15
href: http://www.lemondeinformatique.fr/actualites/lire-collectivites-territoriales-le-label-libre-plus-disponible-au-1er-octobre-65421.html
tags:
- Administration
- April
- Associations
- RGI
---

> L'Adullact, avec le soutien de l'Etat et de la DINSIC, propose depuis le 1er juillet aux collectivités territoriales qui le souhaitent de se doter du label Territoire numérique libre pour signifier leur engagement envers les technologies ouvertes. La date limite de dépôt des dossiers est fixée au 30 septembre.
