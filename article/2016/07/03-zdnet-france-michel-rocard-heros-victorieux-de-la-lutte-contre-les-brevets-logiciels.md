---
site: ZDNet France
title: "Michel Rocard, héros victorieux de la lutte contre les brevets logiciels"
author: Thierry Noisette
date: 2016-07-03
href: http://www.zdnet.fr/actualites/michel-rocard-heros-victorieux-de-la-lutte-contre-les-brevets-logiciels-39839198.htm
tags:
- Institutions
- Brevets logiciels
- Droit d'auteur
- Innovation
- Europe
---

> Pendant plus de cinq ans, la Commission européenne a tenté d'introduire dans l'UE la brevetabilité des logiciels. Rapporteur du projet de directive, Michel Rocard a été un acteur clé de son rejet par les eurodéputés.
