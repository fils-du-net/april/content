---
site: Les Echos
title: "Pour une Équipe France du numérique"
author: Grégory Pascal
date: 2016-07-13
href: http://www.lesechos.fr/idees-debats/cercle/cercle-158896-pour-une-equipe-france-du-numerique-2014279.php
tags:
- Entreprise
- Internet
- Économie
- Innovation
---

> En politique, économie, pour un seul secteur professionnel, il n’existe pas d'"équipe". Une équipe suppose à sa tête un modèle de référence sur lequel on peut, si ce n’est, copier, du moins, compter. Avons-nous donc un leader, capable de créer l’"Équipe France" du numérique qui saura positionner le pays sur les devants de la scène internationale? La question est posée, mais la réponse est loin d’être facile.
