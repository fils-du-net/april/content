---
site: Developpez.com
title: "La ville de Grenoble compte faire migrer huit autres écoles sur Linux cette année"
author: Olivier Famien
date: 2016-07-11
href: http://www.developpez.com/actu/101131/La-ville-de-Grenoble-compte-faire-migrer-huit-autres-ecoles-sur-Linux-cette-annee-et-prevoit-de-basculer-plusieurs-autres-ecoles-d-ici-2018
tags:
- Administration
- April
- Éducation
- Informatique en nuage
---

> En juin 2015, les autorités de la ville de Grenoble ont présenté leur projet de remplacer les systèmes d’exploitation propriétaires équipant actuellement les appareils dans les écoles de la ville par le système d’exploitation Linux. Quelques mois plus tard, et plus précisément en décembre dernier, la ville de Grenoble a encore annoncé son adhésion à l’association April, ayant pour vocation de défendre et promouvoir les logiciels libres.
