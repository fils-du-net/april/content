---
site: Les Echos
title: "Le code open source pour les… enfants!"
author: Grégory Becue
date: 2016-07-21
href: http://www.lesechos.fr/idees-debats/cercle/cercle-159100-le-code-open-source-pour-les-enfants-2015962.php
tags:
- Économie
- Éducation
- Promotion
---

> Les nouvelles générations sont les plus connectées au monde. Élevées dès leur plus jeune âge par les nouvelles technologies, leur doudou n’est autre qu’Internet, Facebook ou Snapchat. Aujourd’hui, la question de l’apprentissage du code devient donc une évidence. Mais pour parvenir à démocratiser la programmation, tant pour les grands que pour les petits, l’open source est la condition sine qua non par laquelle passer.
