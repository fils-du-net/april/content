---
site: Global Security Mag
title: "Axelle Lemaire à l'heure du bilan (catastrophique) du gouvernement sur le numérique"
author: La Quadrature du Net
date: 2016-07-14
href: https://www.globalsecuritymag.fr/Axelle-Lemaire-a-l-heure-du-bilan,20160714,63748.html
tags:
- Internet
- Institutions
- Associations
- Neutralité du Net
- Vie privée
---

> Dans une interview donnée à Mediapart hier, Axelle Lemaire, secrétaire d’État au numérique, met en cause La Quadrature du Net à propos d’un communiqué publié par l’Observatoire des Libertés et du Numérique1 faisant le bilan de la Loi pour une République numérique. Devant les contre-vérités et les attaques injustifiées de la secrétaire d’État, il est nécessaire de rétablir une certaine part de vérité et de mettre madame Lemaire face à ses responsabilités.
