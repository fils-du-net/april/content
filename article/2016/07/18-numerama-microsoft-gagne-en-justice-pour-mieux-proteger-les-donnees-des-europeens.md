---
site: Numerama
title: "Microsoft gagne en justice pour mieux protéger les données des Européens"
author: Julien Lausson
date: 2016-07-18
href: http://www.numerama.com/politique/183225-microsoft-gagne-en-justice-pour-mieux-proteger-les-donnees-des-europeens.html
tags:
- Entreprise
- Institutions
- Europe
- International
- Vie privée
---

> Un simple mandat américain ne suffit pas pour transférer aux USA des données stockées en Europe. C'est en somme la décision rendue par une cour d'appel dans une affaire concernant les mails d'une personne soupçonnée d'un trafic de drogue.
