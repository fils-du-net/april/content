---
site: Prat!que
title: "La loi numérique fait débat"
author: Charlotte Jouss
date: 2016-07-12
href: http://www.pratique.fr/actu/la-loi-numerique-fait-debat-1563076.html
tags:
- Internet
- Administration
- April
- Institutions
- Promotion
- Open Data
---

> Dans le cadre de la loi Numérique, mercredi 29 juin dernier, la commission mixte paritaire (CMP) composée de 7 députés, 7 sénateurs ainsi que de nombreux membres suppléants, a réussi à mettre en place un accord pour que les administrations encouragent "l’utilisation des logiciels libres et des formats ouverts lors du développement". Celui-ci fait débat.
