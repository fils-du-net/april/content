---
site: L'OBS
title: "Chic, le gouvernement choisit le logiciel libre. Zut, c’est en Bulgarie"
author: Thierry Noisette
date: 2016-07-06
href: http://rue89.nouvelobs.com/2016/07/06/chic-gouvernement-choisit-logiciel-libre-zut-cest-bulgarie-264570
tags:
- Administration
- Institutions
- International
---

> Apprendre que le gouvernement choisit, pour tout développement logiciel sur mesure qu’il commandera, de privilégier les logiciels sous licences open source, voilà qui réjouit les partisans des logiciels libres et de leurs avantages (partage, économie, souveraineté...).
