---
site: ZDNet France
title: "AbulEdu, des logiciels libres pour l'enseignement sauvés par crowdfunding"
author: Thierry Noisette
date: 2016-07-14
href: http://www.zdnet.fr/actualites/abuledu-des-logiciels-libres-pour-l-enseignement-sauves-par-crowdfunding-39839724.htm
tags:
- Entreprise
- Économie
- Associations
- Éducation
---

> Créé en 2001 par des enseignants et des informaticiens libristes, cet ensemble d'outils logiciels devrait être pérennisé grâce à une campagne réussie.
