---
site: Next INpact
title: "Le projet Tor a élu un nouveau comité de direction"
author: Kevin Hottot
date: 2016-07-18
href: http://www.nextinpact.com/news/100658-le-projet-tor-a-elu-nouveau-comite-direction.htm
tags:
- Internet
- Associations
- Vie privée
---

> Le projet Tor a procédé cette semaine à l'élection d'un nouveau bureau. Six nouvelles têtes font leur apparition pour soutenir Roger Digledine et Nick Mathewson, les deux cofondateurs du projet. Un renouvellement qui fait suite au départ de Jacob Appelbaum, accusé notamment de harcèlement sexuel.
