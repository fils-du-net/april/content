---
site: MacGeneration
title: "La police lituanienne va augmenter ses salaires en virant Microsoft"
author: Nicolas Furno
date: 2016-07-18
href: http://www.macg.co/ailleurs/2016/07/la-police-lituanienne-va-augmenter-ses-salaires-en-virant-microsoft-94967
tags:
- Logiciels privateurs
- Administration
- Économie
- International
---

> La police lituanienne a abandonné les produits Microsoft dans ses commissariats. À la place de Windows, ses ordinateurs tournent désormais sous Ubuntu et à la place de la suite bureautique Office, c’est LibreOffice qui a succédé.
