---
site: Le Figaro
title: "Tristan Nitot: «Utiliser Internet, c'est accepter une forme de surveillance»"
author: Elsa Trujillo
date: 2016-07-10
href: http://www.lefigaro.fr/secteur/high-tech/2016/10/07/32001-20161007ARTFIG00001-tristan-nitot-utiliser-internet-c-est-accepter-une-forme-de-surveillance.php
tags:
- Internet
- Vie privée
---

> Tristan Nitot, ancien président de Mozilla Europe et membre du comité de prospective de la CNIL, entend redonner à ses lecteurs un pouvoir d'action face à la surveillance.
