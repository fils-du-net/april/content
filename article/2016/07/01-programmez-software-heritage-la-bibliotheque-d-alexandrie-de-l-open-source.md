---
site: Programmez!
title: "Software Heritage: la bibliothèque d'Alexandrie de l'open source!"
author: fredericmazue
date: 2016-07-01
href: http://www.programmez.com/actualites/software-heritage-la-bibliotheque-dalexandrie-de-lopen-source-24597
tags:
- Administration
- Partage du savoir
- Sciences
- Informatique en nuage
---

> Très belle initiative de Inria que la création de la plate-forme Software Heritage dont la vocation est de préserver, organiser et partager les codes sources de tous les logiciels libres.
