---
site: Le Monde.fr
title: "«Civic Tech»: des applis pour doper la démocratie en ville"
author: Claire Legros
date: 2016-07-14
href: http://www.lemonde.fr/smart-cities/article/2016/07/14/civic-tech-des-applis-pour-doper-la-democratie-participative_4969481_4811534.html
tags:
- Entreprise
- Internet
- Administration
- Sciences
---

> Les plateformes se multiplient pour proposer aux municipalités de renforcer le dialogue avec leurs habitants. Quels enjeux pour la démocratie locale? Premier volet de notre série sur les «Civic Techs».
