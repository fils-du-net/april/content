---
site: Next INpact
title: "Open Data: le gouvernement refuse d’augmenter davantage le budget de la mission Etalab"
author: Xavier Berne
date: 2016-11-15
href: http://www.nextinpact.com/news/102125-open-data-gouvernement-refuse-d-augmenter-davantage-budget-mission-etalab.htm
tags:
- Administration
- Économie
- Institutions
- Open Data
---

> Hier, à l’occasion des débats relatifs au projet de loi de finances pour 2017, le gouvernement s’est opposé à une augmentation des crédits budgétés pour la mission Etalab (en charge d’accompagner l’Open Data en France). Il faut dire que la députée Monique Rabin ne proposait ni plus ni moins que de doubler l’enveloppe allouée à l’institution.
