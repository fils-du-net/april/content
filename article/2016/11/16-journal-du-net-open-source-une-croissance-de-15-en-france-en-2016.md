---
site: Journal du Net
title: "Open Source: une croissance de 15% en France en 2016"
author: Antoine Crochet-Damais
date: 2016-11-16
href: http://www.journaldunet.com/solutions/reseau-social-d-entreprise/1188013-open-source-en-france/
tags:
- Entreprise
- Économie
---

> Le chiffre d'affaires des acteurs de l'open source progresse 6 fois plus vite que l'ensemble du secteur numérique en France. L'indicateur est publié par le Conseil National du Logiciel Libre à l'occasion du Paris Open Source Summit.
