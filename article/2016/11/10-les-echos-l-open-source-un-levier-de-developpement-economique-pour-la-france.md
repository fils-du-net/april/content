---
site: Les Echos
title: "L’Open Source, un levier de développement économique pour la France?"
author: Jean-Christophe Elineau
date: 2016-11-10
href: http://www.lesechos.fr/idees-debats/cercle/cercle-162463-lopen-source-un-levier-de-developpement-economique-pour-la-france-2042011.php
tags:
- Entreprise
- Économie
- Innovation
---

> Les nouvelles logiques numériques incontournables contribuent à construire les modèles économiques de demain.
