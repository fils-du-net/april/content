---
site: Le Monde.fr
title: "En 2016, le code informatique arrive à l’école"
date: 2016-11-02
href: http://www.lemonde.fr/sciences/article/2016/11/02/en-2016-le-code-informatique-arrive-a-l-ecole_5024344_1650684.html
tags:
- Éducation
---

> Pour faire face à la révolution numérique, il est impératif que les enfants, mais aussi les enseignants et les parents, comprennent l’enjeu de l’introduction de ce nouvel enseignement.
