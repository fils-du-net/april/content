---
site: Mediapart
title: "Pour le «numérique libre» avec le collectif EduNathon"
author: Jean-Pierre Favier
date: 2016-11-21
href: https://blogs.mediapart.fr/jean-pierre-favier/blog/211116/pour-le-numerique-libre-avec-le-collectif-edunathon
tags:
- Entreprise
- Administration
- Institutions
- Associations
- Éducation
- Marchés publics
- English
---

> Le collectif EduNathon a été créé début 2016 suite à la signature d’une convention, fin novembre 2015, entre le Ministère de l’Éducation Nationale et Microsoft France.
