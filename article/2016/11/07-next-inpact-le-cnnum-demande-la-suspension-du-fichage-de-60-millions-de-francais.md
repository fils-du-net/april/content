---
site: Next INpact
title: "Le CNNum demande la suspension du fichage de 60 millions de Français"
author: Marc Rees
date: 2016-11-07
href: http://www.nextinpact.com/news/102013-le-cnnum-demande-suspension-fichage-60-millions-francais.htm
tags:
- Institutions
- Vie privée
---

> La publication du décret baptisé Fichier Monstre sur les réseaux sociaux n'a pas laissé insensible le Conseil national du numérique. Dans un communiqué publié voilà quelques minutes, l'institution demande la suspension immédiate de ce texte administratif, passé sans l'ombre d'un débat.
