---
site: Silicon
title: "Munich envisagerait de revenir à Windows et Office"
author: Ariane Beky
date: 2016-11-13
href: http://www.silicon.fr/munich-envisagerait-revenir-windows-office-162444.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- International
---

> La ville allemande de Munich étudie la possibilité de migrer vers Windows et Office, pourtant abandonnés au profit de logiciels Open Source.
