---
site: Developpez.com
title: "Marché des PC: quels sont les problèmes qui freinent l'adoption de Linux?"
author: Michael Guilloux
date: 2016-11-07
href: http://www.developpez.com/actu/106312/Marche-des-PC-quels-sont-les-problemes-qui-freinent-l-adoption-de-Linux-Partagez-vos-avis
tags:
- Sensibilisation
---

> Dans le monde du développement, Linux est le plus grand projet communautaire. Autour du noyau, gravitent également de nombreux autres projets soutenus par des organisations et des développeurs dont une bonne partie apportent leur contribution de manière bénévole. Malgré la collaboration qu’il peut y avoir, dans le domaine des PC, Linux a beaucoup de retard sur ses concurrents Windows et macOS.
