---
site: Numerama
title: "Le fichage biométrique des Français en 7 questions"
author: Julien Lausson
date: 2016-11-02
href: http://www.numerama.com/politique/205589-le-fichage-biometrique-des-francais-en-questions.html
tags:
- Institutions
- Vie privée
---

> Ce fichier a un rôle-clé: rassembler dans une même base de données les données personnelles et biométriques des Français pour la gestion des cartes nationales d'identité et des passeports. Mais il suscite de vives inquiétudes.
