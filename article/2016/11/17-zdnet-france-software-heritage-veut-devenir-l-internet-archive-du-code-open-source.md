---
site: ZDNet France
title: "Software Heritage veut devenir l’internet Archive du code open source"
author: Louis Adam
date: 2016-11-17
href: http://www.zdnet.fr/actualites/software-heritage-veut-devenir-l-internet-archive-du-code-open-source-39844768.htm
tags:
- Internet
- Administration
- Partage du savoir
- Innovation
- Open Data
---

> L’Inria a profité du salon Paris Open Source Summit pour faire un point d’étape sur son projet Software Heritage. Un projet mené par 4 chercheurs, mais qui se fixe une ambition de taille: collecter tous les codes sources accessibles sur le Net.
