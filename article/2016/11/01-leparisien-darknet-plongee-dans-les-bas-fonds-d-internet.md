---
site: leParisien
title: "Darknet: plongée dans les bas-fonds d'Internet"
author: Aymeric Renou
date: 2016-11-01
href: http://www.leparisien.fr/societe/face-obscure-01-11-2016-6274275.php
tags:
- Internet
- Institutions
- Vie privée
---

> C'est la face cachée du Web, son côté obscur qui n'obéit qu'à la seule loi de l'anonymat. Voyage dans un monde parallèle où tout est possible, surtout le pire.
