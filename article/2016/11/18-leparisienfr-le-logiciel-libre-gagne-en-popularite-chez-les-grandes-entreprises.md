---
site: leParisien.fr
title: "Le logiciel libre gagne en popularité chez les grandes entreprises"
author: Josep Lago
date: 2016-11-18
href: http://www.leparisien.fr/high-tech/le-logiciel-libre-gagne-en-popularite-chez-les-grandes-entreprises-18-11-2016-6343878.php
tags:
- Entreprise
- Économie
---

> sation de logiciels libres, ou "open source", gagne du terrain au sein des grandes entreprises séduites par son aspect collaboratif et qui lui accordent désormais une place stratégique.
