---
site: Numerama
title: "Du «mulot» de Chirac au «Bon Coin» de Sarkozy, 6 gaffes de la droite sur le numérique"
author: Alexis Orsini
date: 2016-11-17
href: http://www.numerama.com/politique/209417-du-mulot-de-chirac-au-bon-coin-de-sarkozy-6-gaffes-de-la-droite-sur-le-numerique.html
tags:
- Institutions
---

> À quelques jours du premier tour de la primaire de la droite, retour sur les gaffes les plus marquantes de ses élus en matière de numérique.
