---
site: Next INpact
title: "La CNIL saisie sur l’accord entre Microsoft et l’Éducation nationale"
author: Marc Rees
date: 2016-11-10
href: http://www.nextinpact.com/news/102089-la-cnil-saisie-sur-l-accord-entre-microsoft-et-l-education-nationale.htm
tags:
- Économie
- Institutions
- Associations
- Éducation
- Marchés publics
- Vie privée
---

> Après l’échec d’un référé devant les juridictions civiles, une association traine l’accord de partenariat entre l’Éducation nationale et Microsoft devant la CNIL, mais aussi le ministère. La cible? La question des données personnelles.
