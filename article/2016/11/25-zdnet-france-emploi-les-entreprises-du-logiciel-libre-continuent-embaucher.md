---
site: ZDNet France
title: "Emploi: les entreprises du logiciel libre continuent à embaucher"
author: Thierry Noisette
date: 2016-11-25
href: http://www.zdnet.fr/actualites/emploi-les-entreprises-du-logiciel-libre-continuent-a-embaucher-39845228.htm
tags:
- Entreprise
- Économie
- Innovation
---

> Fortes d'une croissance confortable en 2015-2016 dont elles prévoient le maintien, les sociétés membres du CNLL comptent recruter 1.000 salariés en 2017.
