---
site: Next INpact
title: "Au ministère de la Culture, deux millions d’euros pour Microsoft Office?"
author: Xavier Berne
date: 2016-11-07
href: http://www.nextinpact.com/news/102021-au-ministere-culture-deux-millions-d-euros-pour-microsoft-office.htm
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Marchés publics
---

> Afin que les agents du ministère de la Culture travaillent «avec les mêmes logiciels», la Rue de Valois s’apprête à signer un contrat avec le géant américain Microsoft. Près de deux millions d’euros pourraient ainsi servir à l’installation d’Office, la célèbre suite bureautique.
