---
site: Numerama
title: "Microsoft Office: la facture à 2 millions d'euros du Ministère de la Culture"
author: Julien Lausson
date: 2016-11-07
href: http://www.numerama.com/politique/206716-microsoft-office-la-facture-a-2-millions-deuros-du-ministere-de-la-culture.html
tags:
- Entreprise
- Logiciels privateurs
- April
- Institutions
- Marchés publics
---

> Le ministère de la culture est décidé à basculer l'ensemble de son environnement informatique sous Microsoft Office, au détriment des suites bureautiques libres. C'est ce que révèle un courrier. Facture attendue: 2 millions d'euros.
