---
site: Le Temps
title: "La lutte contre l’obsolescence programmée s’organise"
author: Andrée-Marie Dussault
date: 2016-11-21
href: https://www.letemps.ch/sciences/2016/11/21/lutte-contre-lobsolescence-programmee-sorganise
tags:
- Économie
---

> Les stratégies pour combattre le fléau qui menace l’environnement et nos portefeuilles se multiplient. Qualité, réparabilité et compatibilité réémergent à l’ordre du jour
