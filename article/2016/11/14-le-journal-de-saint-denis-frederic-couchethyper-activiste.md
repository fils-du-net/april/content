---
site: "Le Journal de Saint-Denis"
title: "Frédéric Couchet/Hyper activiste"
author: Linda Maziz
date: 2016-11-14
href: http://www.lejsd.com/content/hyper-activiste
tags:
- April
- Sensibilisation
---

> Geek. Il y a vingt ans, il a fondé l’April, pour promouvoir et défendre le logiciel libre. Aujourd’hui, insatiable défenseur de la liberté, de l’égalité et de la fraternité, il s’est mis en quête d’une nouvelle cause dans laquelle s’investir.
