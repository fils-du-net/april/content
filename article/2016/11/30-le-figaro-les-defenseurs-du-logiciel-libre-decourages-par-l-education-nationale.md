---
site: Le Figaro
title: "Les défenseurs du logiciel libre découragés par l'Education nationale"
author: Elisa Braun
date: 2016-11-30
href: http://www.lefigaro.fr/secteur/high-tech/2016/11/30/32001-20161130ARTFIG00001-les-defenseurs-du-logiciel-libre-decourages-par-l-education-nationale.php
tags:
- Entreprise
- Administration
- April
- Institutions
- Associations
- Éducation
---

> Accusant l'Education nationale de privilégier des partenariats privés avec Microsoft au détriment de solutions alternatives gratuites, Framasoft ne répondra plus à ses sollicitations.
