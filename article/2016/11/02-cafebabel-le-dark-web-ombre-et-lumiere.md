---
site: Cafebabel
title: "Le Dark Web: ombre et lumière"
author: João Fernandes Silva
date: 2016-11-02
href: http://www.cafebabel.fr/style-de-vie/article/le-dark-web-ombre-et-lumiere.html
tags:
- Internet
- Institutions
- Vie privée
---

> Peu de gens connaissent le Dark Web, souvent listé comme un vivier de criminels en raison des sites qu'il héberge comme Silk Road, le marché en ligne de drogues illicites. Mais dans ces bas-fonds d'Internet, une lumière existe. Explications.
