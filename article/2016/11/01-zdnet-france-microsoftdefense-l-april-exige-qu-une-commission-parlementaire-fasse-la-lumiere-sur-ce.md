---
site: ZDNet France
title: "Microsoft/Défense: l'April exige qu'une commission parlementaire fasse la lumière sur ce contrat"
author: Louis Adam
date: 2016-11-01
href: http://www.zdnet.fr/actualites/microsoft-defense-l-april-exige-qu-une-commission-parlementaire-fasse-la-lumiere-sur-ce-contrat-39844024.htm
tags:
- Entreprise
- Administration
- April
- Institutions
- Marchés publics
---

> Le contrat passé entre Microsoft et le ministère de la Défense pour l’équipement de son parc de machine, révélé par l'émission Cash Investigation, fait bondir les défenseurs du logiciel libre. Debrief avec Frederic Couchet, délégué General de l’April et Étienne Gonnu, chargé de mission affaires publiques.
