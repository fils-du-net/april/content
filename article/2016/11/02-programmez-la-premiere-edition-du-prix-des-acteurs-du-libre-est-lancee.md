---
site: Programmez!
title: "La première édition du Prix des 'Acteurs du Libre' est lancée"
author: Frederic Mazue
date: 2016-11-02
href: http://www.programmez.com/actualites/la-premiere-edition-du-prix-des-acteurs-du-libre-est-lancee-25068
tags:
- Innovation
- Promotion
---

> Evénement international qui réunit l’ensemble des acteurs et clients de la filière Open Source, le Paris Open Source Summit 2016, sera cette année sous le parrainage de Thierry Mandon, secrétaire d’État en charge de l’Enseignement supérieur et de la Recherche et sous le marrainage d'Axelle Lemaire, secrétaire d'Etat chargée du Numérique et de l'Innovation.
