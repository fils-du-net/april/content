---
site: Numerama
title: "Fichier biométrique: le Conseil national du numérique réagit"
author: Julien Lausson
date: 2016-11-04
href: http://www.numerama.com/politique/206477-fichier-biometrique-le-conseil-national-du-numerique-reagit.html
tags:
- Institutions
- Vie privée
---

> Le Conseil national du numérique annonce ce vendredi qu'il s'auto-saisit pour évaluer la création du fichier TES, qui rassemblera les informations personnelles et biométriques de la quasi-totalité de la population dans un seul fichier.
