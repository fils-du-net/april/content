---
site: Next INpact
title: "Hadopi: comment Juppé et Fillon cèdent aux ayants droit"
author: Marc Rees
date: 2016-11-23
href: http://www.nextinpact.com/news/102236-hadopi-comment-juppe-et-fillon-cedent-aux-ayants-droit.htm
tags:
- Internet
- HADOPI
- Institutions
---

> L’échéance de la présidentielle est systématiquement un pont d’or pour les différents lobbys, tous secteurs confondus. Dans le domaine des industries culturelles, la règle est confirmée au regard des programmes portés par les deux candidats de la primaire de droite, spécialement sur le front de la Hadopi.
