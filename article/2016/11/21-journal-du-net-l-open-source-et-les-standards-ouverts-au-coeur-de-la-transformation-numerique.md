---
site: Journal du Net
title: "L’open source et les standards ouverts au cœur de la transformation numérique"
author: Fabien Cauchi
date: 2016-11-21
href: http://www.journaldunet.com/solutions/expert/65681/l-open-source-et-les-standards-ouverts-au-c-ur-de-la-transformation-numerique.shtml
tags:
- Entreprise
- Économie
- Innovation
---

> On ne compte plus les entreprises s’appuyant sur les approches ouvertes, que l’on parle d’open source, d’open data, d’open hardware, d’open innovation ou encore d’open API pour développer et accélérer leur stratégie de développement, de croissance et d’internationalisation.
