---
site: Next INpact
title: "L’État recherche dix «entrepreneurs d’intérêt général»"
author: Xavier Berne
date: 2016-11-07
href: http://www.nextinpact.com/news/102018-l-etat-recherche-dix-entrepreneurs-d-interet-general.htm
tags:
- Administration
- Innovation
---

> Relever en dix mois un «défi d’intérêt général» à Bercy, à la Cour des comptes ou à la Bibliothèque nationale de France vous motive-t-il? Les pouvoirs publics cherchent des spécialistes du numérique (développeurs, data-scientists...) pour des contrats censés débuter en janvier 2017.
