---
site: francetv info
title: "La ville de Fontaine se dote de logiciels libres gratuits pour ses écoles"
author: Gilles Ragris, Grégory Lespinasse, François Hubaud
date: 2016-11-10
href: http://france3-regions.francetvinfo.fr/alpes/isere/grenoble/ville-fontaine-se-dote-logiciels-libres-gratuits-ses-ecoles-1128249.html
tags:
- Administration
- Économie
- Éducation
---

> Le groupe scolaire Robespierre de Fontaine est en pleine migration informatique. Et cela ressemble à une révolution, car la ville a choisi d'équiper ses écoles de logiciels libres. Ces systèmes d'exploitation gratuits permettent de faire fonctionner votre ordinateur.
