---
site: ChannelNews
title: "Encore un ministère qui abandonne le logiciel libre pour Microsoft"
author: Dirk Basyn
date: 2016-11-09
href: http://www.channelnews.fr/ministere-abandonne-logiciel-libre-microsoft-67544
tags:
- Entreprise
- Logiciels privateurs
- April
- Institutions
- Marchés publics
---

> Ainsi donc, après le ministère de l’Education nationale, et celui de la Défense, le ministère de la Culture succombe également au charme des sirènes de Microsoft
