---
site: Le Monde.fr
title: "Le gouvernement va lancer un site web pour développer et valider des compétences numériques"
date: 2016-11-17
href: http://www.lemonde.fr/campus/article/2016/11/17/le-gouvernement-va-lancer-un-site-web-pour-developper-et-valider-des-competences-numeriques_5033050_4401467.html
tags:
- Internet
- Éducation
- Innovation
---

> D’ores et déjà disponible dans sa version test, la plate-forme Pix permettra, à compter de la rentrée 2017, de s’évaluer, de s’améliorer puis d’obtenir une certification.
