---
site: Le Monde Informatique
title: "L'open source devrait générer 1 000 emplois en France en 2017"
date: 2016-11-16
href: http://www.lemondeinformatique.fr/actualites/lire-l-open-source-devrait-generer-1-000-emplois-en-france-en-2017-66522.html
tags:
- Entreprise
---

> Le Conseil national du logiciel libre en France anticipe une dynamique de l'emploi positive dans les technologies liées au logiciel libre et à l'open source en 2017 avec des recrutements en hausse de 25%. En parallèle, la tension sur certains profils risque de s'accentuer.
