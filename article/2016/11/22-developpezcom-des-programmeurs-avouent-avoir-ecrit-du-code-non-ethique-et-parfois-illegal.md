---
site: Developpez.com
title: "Des programmeurs avouent avoir écrit du code non éthique et parfois illégal"
author: Coriolan
date: 2016-11-22
href: http://www.developpez.com/actu/106861/Des-programmeurs-avouent-avoir-ecrit-du-code-non-ethique-et-parfois-illegal-en-raison-des-requetes-de-plus-en-plus-contraignantes-de-leurs-employeurs
tags:
- Entreprise
---

> Avec la multiplication des scandales qui impliquent des programmeurs qui ont agi sans éthique de travail ou hors du champ de la loi, beaucoup commencent à se demander ce qu’il faudrait faire pour changer la donne. Que devraient faire les programmeurs quand on leur demande d’accomplir des tâches non éthiques ou illégales? Qu’est ce qu’il faudrait faire pour protéger le programmeur dans ces cas? Et faudrait-il prévoir des formations d’éthique pour éviter des agissements contraires à la loi dans le futur? Bref, une panoplie de questions qui hantent les programmeurs de tous les coins du globe.
