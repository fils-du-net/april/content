---
site: Localtis.info
title: "Un nouveau label pour encourager le logiciel libre dans les collectivités"
author: Pierre-Marie Langlois
date: 2016-11-18
href: http://www.localtis.info/cs/ContentServer?pagename=Localtis/LOCActu/ArticleActualite&jid=1250271802354&cid=1250271792874
tags:
- Administration
- April
- Associations
- Promotion
---

> A l'occasion de l'Open Source Summit 2016, une manifestation importante du monde du logiciel libre, plusieurs associations ont remis les premiers labels "Territoire numérique libre" en présence d'Estelle Grelier, secrétaire d'Etat chargée des collectivités locales. L'occasion de valoriser les initiatives des territoires qui sortent des sentiers battus en travaillant avec des applications open source.
