---
site: Le Figaro
title: "«3 % des femmes dans l'open, c'est trop peu»"
author: Sylvia di Pasquale
date: 2016-11-28
href: http://www.lefigaro.fr/emploi/2016/11/28/09005-20161128ARTFIG00260--3-des-femmes-dans-l-open-c-est-trop-peu.php
tags:
- Entreprise
- International
---

> L'invité RH, Charlotte de Broglie, fondatrice de l'AdaWeek, nous parle de la place des femmes dans les STEM, acronyme anglais pour définir les métieurs scientifiques, techniques, ingénierie et mathématiques. Pour cadremploi.fr et lefigaro.fr elle explique à quoi sert l'AdaWeek.
