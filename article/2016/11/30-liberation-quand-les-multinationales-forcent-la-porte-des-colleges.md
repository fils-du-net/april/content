---
site: Libération
title: "Quand les multinationales forcent la porte des collèges"
author: Valérie Brun, Patricia Combarel, Liêm-Khê Luguern, Pascal Pragnère et Laurent Rouzière
date: 2016-11-30
href: http://www.liberation.fr/debats/2016/11/30/quand-les-multinationales-forcent-la-porte-des-colleges_1531992
tags:
- Entreprise
- Économie
- Institutions
- Éducation
- Europe
---

> Des professeurs d'un collège du Tarn dénoncent les intrusions des entreprises telles que Nestlé, Total ou Microsoft sous la forme de «kits pédagogiques». Avec l'idée d'inculquer des comportements de consommateur aux individus dès leur plus jeune âge.
