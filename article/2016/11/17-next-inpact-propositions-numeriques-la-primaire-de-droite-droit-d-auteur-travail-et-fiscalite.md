---
site: Next INpact
title: "Propositions numériques à la primaire de droite: droit d'auteur, travail et fiscalité"
author: Xavier Berne
date: 2016-11-17
href: http://www.nextinpact.com/news/102176-propositions-numeriques-a-primaire-droite-droit-dauteur-travail-et-fiscalite.htm
tags:
- HADOPI
- Institutions
- Vie privée
---

> Le premier tour des primaires de la droite et du centre se tiendra ce dimanche. Et après deux débats, on ne peut pas dire que le numérique ait été au centre des préoccupations. Nous avons donc décidé de faire le point en détail avant la dernière confrontation de ce soir.
