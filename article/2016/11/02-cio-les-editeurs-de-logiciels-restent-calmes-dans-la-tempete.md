---
site: CIO
title: "Les éditeurs de logiciels restent calmes dans la tempête"
author: Bertrand Lemaire
date: 2016-11-02
href: http://www.cio-online.com/actualites/lire-edito-les-editeurs-de-logiciels-restent-calmes-dans-la-tempete-8841.html
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Sensibilisation
---

> Les entreprises utilisatrices sont mécontentes mais les éditeurs de logiciels réagissent bien peu. Pourquoi s'en feraient-ils, d'ailleurs?
