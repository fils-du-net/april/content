---
site: ZDNet France
title: "Microsoft fait une entrée remarquée à la Linux Foundation (c'est sérieux)"
author: Christophe Auffray
date: 2016-11-17
href: http://www.zdnet.fr/actualites/microsoft-fait-une-entree-remarquee-a-la-linux-foundation-c-est-serieux-39844730.htm
tags:
- Entreprise
- Informatique en nuage
---

> Trop tôt pour un premier avril. Microsoft rejoint donc très officiellement la fondation Linux en tant que membre Platinium. Un nouveau signe de l'engagement (intéressé) de Microsoft en faveur de Linux et de l'open source.
