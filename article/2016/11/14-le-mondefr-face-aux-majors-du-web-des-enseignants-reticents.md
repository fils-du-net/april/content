---
site: Le Monde.fr
title: "Face aux majors du Web, des enseignants réticents"
author: Luc Cédelle
date: 2016-11-14
href: http://www.lemonde.fr/education/article/2016/11/14/face-aux-majors-du-web-des-enseignants-reticents_5030875_1473685.html
tags:
- Entreprise
- Institutions
- Éducation
- Marchés publics
---

> Peut-on cautionner l’implication des entreprises du numérique dans l’école? C’est l’un des thèmes de la première conférence de consensus de la Ligue de l’enseignement, le 17 septembre, dans le cadre de la Semaine de l’éducation et du salon correspondant, porte de Versailles.
