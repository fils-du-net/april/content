---
site: Numerama
title: "Le décret du fichier biométrique TES attaqué en justice"
author: Julien Lausson
date: 2016-11-09
href: http://www.numerama.com/politique/207252-le-decret-du-fichier-biometrique-tes-attaque-en-justice.html
tags:
- Institutions
- Associations
- Vie privée
---

> Le collectif des Exégètes Amateurs annonce son intention d'attaquer devant le Conseil d'État le décret donnant naissance au controversé fichier TES.
