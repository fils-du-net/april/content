---
site: L'Informaticien
title: "Les administrations américaines en mode Open Source"
author: Guillaume Périssat
date: 2016-11-07
href: http://www.linformaticien.com/actualites/id/42280/les-administrations-americaines-en-mode-open-source.aspx
tags:
- Administration
- Institutions
- International
- Open Data
---

> Le DSI de la Maison Blanche a officialisé le lancement, jeudi 3 novembre, de la plateforme Code.gov. Celle-ci rassemblera les codes sources ouverts par l'administration fédérale américaine.
