---
site: Next INpact
title: "Blocage par erreur chez Orange: Lionel Tardy demande des comptes à l'Intérieur"
author: Marc Rees
date: 2016-11-02
href: http://www.nextinpact.com/news/101958-blocage-par-erreur-chez-orange-lionel-tardy-demande-comptes-a-interieur.htm
tags:
- Institutions
- Vie privée
---

> Le 17 octobre, Orange a bloqué Google, Wikipedia et d’autres sites pour apologie du terrorisme. Le député Lionel Tardy demande désormais des comptes au ministère de l’Intérieur.
