---
site: Next INpact
title: "Cazeneuve se justifie sur le fichage de 60 millions de Français mais rejette la puce sécurisée"
author: David Legrand
date: 2016-11-02
href: http://www.nextinpact.com/news/101972-cazeneuve-se-justifie-sur-fichage-60-millions-francais-mais-rejette-puce-securisee.htm
tags:
- Institutions
- Promotion
- Vie privée
---

> Après la création d'un fichier commun aux passeports et aux cartes nationales d'identité ce 31 octobre, viennent les premières interrogations. Ainsi, le député Lionel Tardy a profité de la séance de questions au Gouvernement afin d'obtenir des réponses, notamment sur la méthode retenue.
