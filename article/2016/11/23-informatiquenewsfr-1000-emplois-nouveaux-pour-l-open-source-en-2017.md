---
site: InformatiqueNews.fr
title: "1000 emplois nouveaux pour l’Open source en 2017"
date: 2016-11-23
href: http://www.informatiquenews.fr/1000-emplois-nouveaux-lopen-source-2017-49371
tags:
- Entreprise
- Économie
---

> A l’occasion du Paris Open Source Summit 2016 qui s’est tenu la semaine dernière, le CNLL (Union des entreprises du logiciel libre et de l’open source en France) a réalisé une enquête sur l’emploi du secteur selon laquelle 1000 emplois devraient être créés l’année prochaine.
