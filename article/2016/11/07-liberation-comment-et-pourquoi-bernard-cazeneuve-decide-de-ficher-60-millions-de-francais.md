---
site: Libération
title: "Comment (et pourquoi) Bernard Cazeneuve a décidé de ficher 60 millions de Français"
author: Jean-Marc Manach
date: 2016-11-07
href: http://www.liberation.fr/france/2016/11/07/comment-et-pourquoi-bernard-cazeneuve-a-decide-de-ficher-60-millions-de-francais_1526551
tags:
- Administration
- Économie
- Institutions
- Vie privée
---

> C'est d'abord pour des raisons budgétaires que le gouvernement a publié en toute discrétion un texte ouvrant la voie à la création d’un fichier massif. «Libération» a retracé la chronologie de cette décision dont la Cnil comme de nombreux acteurs s'alarment.
