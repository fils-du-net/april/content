---
site: Next INpact
title: "Regards Citoyens face à «l’extrême difficulté» d’analyser les consultations en ligne"
author: Xavier Berne
date: 2016-11-28
href: http://www.nextinpact.com/news/102293-regards-citoyens-face-a-l-extreme-difficulte-d-analyser-consultations-en-ligne.htm
tags:
- Internet
- Institutions
- Associations
- Open Data
---

> Consulter les internautes est une chose, en lire les commentaires en est une autre. Après avoir lancé un premier outil de crowdsourcing destiné à épauler les députés, l'association Regards Citoyens vient de publier une analyse de cette expérience.
