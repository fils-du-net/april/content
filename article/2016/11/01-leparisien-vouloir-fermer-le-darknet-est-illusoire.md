---
site: leParisien
title: "«Vouloir fermer le Darknet est illusoire»"
author: Aymeric Renou
date: 2016-11-01
href: http://www.leparisien.fr/societe/vouloir-fermer-le-darknet-est-illusoire-01-11-2016-6274134.php
tags:
- Internet
- Institutions
- Vie privée
---

> Auteur d'un des très rares ouvrages publiés en France sur le Darknet*, Jean-Philippe Rennard est professeur à Grenoble Ecole de management.
