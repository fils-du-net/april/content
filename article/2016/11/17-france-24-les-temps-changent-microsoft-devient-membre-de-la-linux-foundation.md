---
site: France 24
title: "Les temps changent: Microsoft devient membre de la Linux Foundation"
author: Marine Benoit
date: 2016-11-17
href: http://mashable.france24.com/tech-business/20161117-microsoft-membre-linux-foundation
tags:
- Entreprise
- Informatique en nuage
---

> Qui aurait cru qu'un jour, Microsoft et Linux, le roi des systèmes d'exploitation open source, marcheraient main dans la main? Pas grand monde, mais c'est pourtant bien vrai: les ennemis de toujours semblent (presque) réconciliés.
