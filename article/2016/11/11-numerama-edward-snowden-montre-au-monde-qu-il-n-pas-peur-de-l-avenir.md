---
site: Numerama
title: "Edward Snowden montre au monde qu'il n'a pas peur de l'avenir"
author: Gabriele Porrometo
date: 2016-11-11
href: http://www.numerama.com/politique/208009-edward-snowden-montre-au-monde-quil-na-pas-peur-de-lavenir.html
tags:
- Institutions
- Vie privée
---

> Edward Snowden a montré une tranquillité presque surréaliste pendant la conférence qu'il tenait hier soir. Il maintient néanmoins ses engagements.
