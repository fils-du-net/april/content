---
site: BFMtv
title: "Le ministère de la Culture cède aux sirènes de Microsoft"
author: Jamal Henni
date: 2016-11-07
href: http://bfmbusiness.bfmtv.com/entreprise/le-ministere-de-la-culture-cede-aux-sirenes-de-microsoft-1049695.html
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Marchés publics
---

> La rue de Valois va dépenser 2 millions d'euros l'an prochain pour installer la suite de logiciels bureautiques Office de l'éditeur américain sur les ordinateurs du personnel, aujourd'hui équipés de logiciels libres.
