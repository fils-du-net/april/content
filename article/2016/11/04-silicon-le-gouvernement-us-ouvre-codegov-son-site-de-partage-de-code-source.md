---
site: Silicon
title: "Le gouvernement US ouvre Code.gov, son site de partage de code source"
author: Ariane Beky
date: 2016-11-04
href: http://www.silicon.fr/gouvernement-us-code-gov-site-partage-code-source-161939.html
tags:
- Internet
- Administration
- Institutions
- International
---

> La plateforme Code.gov vient d’être lancée par l’administration Obama. Le site regroupe, à ce jour, près de 50 projets Open Source émanant d’agences fédérales américaines.
