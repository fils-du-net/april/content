---
site: ZDNet France
title: "Logiciel libre et l’Education nationale: la route est longue mais la coupe est pleine?"
author: Louis Adam
date: 2016-11-25
href: http://www.zdnet.fr/actualites/logiciel-libre-et-l-education-nationale-la-route-est-longue-mais-la-coupe-est-pleine-39845214.htm
tags:
- Entreprise
- Institutions
- Associations
- Éducation
---

> Sur son blog, l’association Framasoft a publié un long texte faisant état d’un changement de stratégie à l’égard de l’Education nationale. Face au manque de considération du ministère à l’égard du logiciel libre, Framasoft annonce son intention de prendre ses distances.
