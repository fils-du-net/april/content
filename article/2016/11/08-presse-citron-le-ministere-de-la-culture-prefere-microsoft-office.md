---
site: Presse Citron
title: "Le ministère de la Culture préfère Microsoft Office"
author: Thomas-Estimbre
date: 2016-11-08
href: http://www.presse-citron.net/le-ministere-de-la-culture-prefere-microsoft-office
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Marchés publics
- RGI
---

> Le ministère de la Culture a décidé de laisser tomber les logiciels libres et de se tourner vers la suite Office de Microsoft.
