---
site: Les Echos
title: "1.000 nouveaux emplois dans le secteur de l’Open Source en 2017"
author: Julia Lemarchand
date: 2016-11-21
href: http://start.lesechos.fr/rejoindre-une-entreprise/actu-recrutement/1-000-nouveaux-emplois-dans-le-secteur-de-l-open-source-en-2017-6564.php
tags:
- Entreprise
- Économie
---

> Les entreprises du logiciel libre et de l’open source se portent très bien. Au point de prévoir une croissance de 25% de leurs effectifs en France pour l’an prochain.
