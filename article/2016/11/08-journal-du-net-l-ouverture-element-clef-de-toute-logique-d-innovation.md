---
site: Journal du Net
title: "L'ouverture, élément clef de toute logique d'innovation"
author: Benjamin Jean
date: 2016-11-08
href: http://www.journaldunet.com/solutions/expert/65608/l-ouverture--element-clef-de-toute-logique-d-innovation.shtml
tags:
- Entreprise
- Brevets logiciels
- Innovation
---

> Open Source, Open Compute, Open Laws... Les nouvelles logiques numériques de l'"Open" sont devenues les clés de tout processus d’innovation.
