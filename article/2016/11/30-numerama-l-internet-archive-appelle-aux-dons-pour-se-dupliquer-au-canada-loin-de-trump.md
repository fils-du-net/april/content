---
site: Numerama
title: "L'Internet Archive appelle aux dons pour se dupliquer au Canada, loin de Trump"
author: Alexis Orsini
date: 2016-11-30
href: http://www.numerama.com/politique/212992-linternet-archive-appelle-aux-dons-pour-se-dupliquer-au-canada-loin-de-trump.html
tags:
- Internet
- Administration
- Partage du savoir
- Institutions
- Associations
- Neutralité du Net
- International
---

> La fondation américaine d'archivage du web, créée en 1996, appelle aux dons pour pouvoir dupliquer sa gigantesque base de données au Canada. Une décision motivée par ses inquiétudes sur l'avenir du web sous la présidence de Donald Trump.
