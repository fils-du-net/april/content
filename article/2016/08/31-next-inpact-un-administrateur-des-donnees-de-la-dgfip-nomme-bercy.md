---
site: Next INpact
title: "Un «administrateur des données de la DGFiP» nommé à Bercy"
author: Xavier Berne
date: 2016-08-31
href: http://www.nextinpact.com/news/101177-un-administrateur-donnees-dgfip-nomme-a-bercy.htm
tags:
- Administration
- Open Data
---

> Après le ministère de l’Environnement, c’est au tour de Bercy de se doter d’un « chief data officer ». Lionel Ploquin a été désigné voilà plusieurs jours administrateur des données de la Direction générale des finances publiques (DGFiP).
