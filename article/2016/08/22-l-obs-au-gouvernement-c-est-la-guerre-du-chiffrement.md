---
site: L'OBS
title: "Au gouvernement, c’est la guerre du chiffrement"
author: Andréa Fradin
date: 2016-08-22
href: http://rue89.nouvelobs.com/2016/08/22/gouvernement-cest-guerre-chiffrement-264957
tags:
- Internet
- Institutions
- Vie privée
---

> Protectrice pour les uns, outil des terroristes pour les autres, cette technologie qui permet de rendre illisibles les communications divise au sommet de l’Etat.
