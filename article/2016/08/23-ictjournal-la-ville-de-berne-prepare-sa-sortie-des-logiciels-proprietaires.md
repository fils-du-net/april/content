---
site: ICTjournal
title: "La Ville de Berne prépare sa sortie des logiciels propriétaires"
author: Rodolphe Koller
date: 2016-08-23
href: http://www.ictjournal.ch/fr-CH/News/2016/08/23/La-Ville-de-Berne-prepare-sa-sortie-des-logiciels-proprietaires.aspx
tags:
- Administration
---

> La Ville de Berne investit plus de 800'000 francs pour analyser la possibilité de remplacer ses logiciels commerciaux par des solutions open source.
