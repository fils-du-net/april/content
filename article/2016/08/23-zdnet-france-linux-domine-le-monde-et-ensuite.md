---
site: ZDNet France
title: "Linux domine le monde. Et ensuite?"
author: Steven J. Vaughan-Nichols
date: 2016-08-23
href: http://www.zdnet.fr/actualites/linux-domine-le-monde-et-ensuite-39840990.htm
tags:
- Sensibilisation
- Innovation
---

> Grâce à quelques améliorations significatives au cours de la dernière année et demie, Linux est maintenant le modèle de développement du logiciel.
