---
site: ZDNet France
title: "Des empreintes PGP frauduleuses révèlent une attaque contre des figures de l’hacktivisme"
author: Louis Adam
date: 2016-08-17
href: http://www.zdnet.fr/actualites/des-empreintes-pgp-frauduleuses-revelent-une-attaque-contre-des-figures-de-l-hacktivisme-39840792.htm
tags:
- Internet
- Vie privée
---

> Dans un mail publié en début de semaine sur la mailing-list du kernel Linux, un utilisateur alerte sur une tentative d’imposture visant notamment Linus Torvalds. De fausses clefs GPG ont été uploadées sur un serveur de clefs, et celles-ci partagent la même empreinte que les vraies clefs utilisées par des développeurs du kernel Linux et activistes des libertés numériques.
