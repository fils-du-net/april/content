---
site: Le Figaro.fr
title: "La Commission européenne ordonne à Apple de rembourser 13 milliards d'euros à l'Irlande"
date: 2016-08-30
href: http://www.lefigaro.fr/flash-eco/2016/08/30/97002-20160830FILWWW00107-amende-de-13-milliards-d-euros-pour-apple.php
tags:
- Entreprise
- Économie
- Institutions
- Europe
---

> La Commission européenne a frappé un grand coup mardi contre les "avantages fiscaux" accordés par l'Irlande à Apple, en ordonnant au géant américain de rembourser à Dublin un montant record de plus de 13 milliards d'euros.
