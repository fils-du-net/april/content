---
site: Le Monde.fr
title: "Bernard Cazeneuve veut «une initiative européenne» contre le chiffrement"
date: 2016-08-12
href: http://www.lemonde.fr/pixels/article/2016/08/12/bernard-cazeneuve-veut-une-initiative-europeenne-contre-le-chiffrement_4981741_4408996.html
tags:
- Internet
- Institutions
- Vie privée
---

> La France veut porter avec l’Allemagne une initiative européenne puis internationale sur le chiffrement des communications, qui complique la lutte contre le terrorisme, a annoncé Bernard Cazeneuve, jeudi 11 août. Le ministre de l’intérieur travaillera sur ce sujet avec son homologue allemand le 23 août, à Paris, «pour que nous puissions sur cette question lancer une initiative européenne destinée à préparer une initiative plus internationale permettant de faire face à ce nouveau défi», a-t-il dit.
