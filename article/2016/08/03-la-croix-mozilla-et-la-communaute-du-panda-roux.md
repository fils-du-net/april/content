---
site: la Croix
title: "Mozilla et la communauté du panda roux"
author: Paula Pinto Gomes
date: 2016-08-03
href: http://www.la-croix.com/Sciences-et-ethique/Numerique/Mozilla-et-la-communaute-du-panda-roux-2016-08-03-1200779865
tags:
- Internet
- Économie
- April
- Sensibilisation
- Associations
---

> La société Mozilla est surtout connue pour son célèbre navigateur Internet Firefox, dont la mascotte est un panda roux, que beaucoup confondent avec un renard, au grand dam des salariés. Mais Mozilla c’est aussi une Fondation qui milite pour un Web ouvert et accessible à tous, à travers notamment un site destiné aux développeurs.
