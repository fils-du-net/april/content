---
site: Next INpact
title: "Mozilla: une pétition pour actualiser le droit d’auteur aux besoins du 21e siècle"
author: Marc Rees
date: 2016-08-26
href: http://www.nextinpact.com/news/101121-mozilla-petition-pour-actualiser-droit-d-auteur-aux-besoins-21e-siecle.htm
tags:
- Économie
- Institutions
- Associations
- Droit d'auteur
- Europe
---

> Alors que la Commission européenne envisage d’ouvrir un nouveau round de réformes, Mozilla profite de l’instant pour lancer une pétition en faveur d’un futur régime du droit d'auteur plus en phase, selon elle, avec les attentes des utilisateurs.
