---
site: Le Monde.fr
title: "Des outils d’espionnage de l’unité d’élite de la NSA publiés par des pirates"
date: 2016-08-18
href: http://www.lemonde.fr/pixels/article/2016/08/18/des-outils-d-espionnage-de-l-unite-d-elite-de-la-nsa-publies-par-des-pirates_4984544_4408996.html
tags:
- Institutions
- Vie privée
---

> L’affaire, au parfum de guerre froide, intrigue les spécialistes des services de renseignement et de sécurité informatique depuis trois jours. Le 13 août, The Shadow Brokers, un groupe de pirates inconnu jusqu’alors, a publié sur Internet une série d’outils et de programmes d’espionnage informatique qu’il prétend avoir dérobé au groupe Equation, une référence transparente à la National Security Agency (NSA), responsable aux Etats-Unis de l’espionnage numérique.
