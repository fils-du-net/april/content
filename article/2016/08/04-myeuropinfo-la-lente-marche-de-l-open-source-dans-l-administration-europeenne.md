---
site: myeurop.info
title: "La lente marche de l'Open Source dans l'administration européenne"
date: 2016-08-04
href: http://fr.myeurop.info/2016/08/04/la-lente-marche-de-l-open-source-dans-l-administration-europ-enne-14686
tags:
- Administration
- Institutions
- International
---

> Les administrations des pays européens se convertissent lentement aux bienfaits des logiciels libres. A l'instar de la Bulgarie, qui vient de voter une loi obligeant ses services administratifs et gouvernementaux à proposer des sites en Open Source.
