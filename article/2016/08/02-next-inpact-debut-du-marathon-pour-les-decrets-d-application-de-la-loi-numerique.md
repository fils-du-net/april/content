---
site: Next INpact
title: "Début du marathon pour les décrets d’application de la loi Numérique"
author: Xavier Berne
date: 2016-08-02
href: http://www.nextinpact.com/news/100838-debut-marathon-pour-decrets-d-application-loi-numerique.htm
tags:
- Administration
- Institutions
- Open Data
---

> Selon nos informations, une réunion interministérielle a eu lieu il y a quelques jours afin d’évoquer les décrets d’application de la future loi Numérique. Certains devraient être publiés avant la fin de l’année.
