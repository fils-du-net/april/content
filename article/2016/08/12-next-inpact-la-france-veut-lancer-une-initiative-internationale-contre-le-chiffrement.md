---
site: Next INpact
title: "La France veut lancer une initiative internationale contre le chiffrement"
author: Xavier Berne
date: 2016-08-12
href: http://www.nextinpact.com/news/100969-la-france-veut-lancer-initiative-internationale-contre-chiffrement.htm
tags:
- Internet
- Institutions
- ACTA
- Vie privée
---

> Au nom de la lutte contre le terrorisme, le ministre de l’Intérieur Bernard Cazeneuve a annoncé hier que la France souhaitait impulser une initiative européenne à l'encontre du chiffrement des communications. L’exécutif espère que ses propositions auront un écho mondial. Il mise pour cela sur le soutien de l’Allemagne.
