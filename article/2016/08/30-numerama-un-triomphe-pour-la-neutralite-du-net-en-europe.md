---
site: Numerama
title: "Un «triomphe» pour la neutralité du Net en Europe"
author: Guillaume Champeau
date: 2016-08-30
href: http://www.numerama.com/politique/191818-triomphe-neutralite-net-europe.html
tags:
- Internet
- Institutions
- Neutralité du Net
- Europe
---

> En publiant ses lignes directrices sur la neutralité du net qui s'imposent à tous les régulateurs européens, le BEREC a donné pleinement satisfaction aux organisations qui plaidaient pour une obligation la plus ferme possible de respecter le principe par lequel Internet s'est développé.
