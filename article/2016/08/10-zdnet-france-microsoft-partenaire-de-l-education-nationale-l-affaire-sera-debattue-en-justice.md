---
site: ZDNet France
title: "Microsoft partenaire de l'Education nationale: l’affaire sera débattue en justice"
date: 2016-08-10
href: http://www.zdnet.fr/actualites/microsoft-partenaire-de-l-education-nationale-l-affaire-sera-debattue-en-justice-39840614.htm
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Associations
- Éducation
- Marchés publics
---

> Le collectif EduNathon met ses menaces à exécution et entreprend de saisir la justice pour qu’elle se prononce sur la légalité de l’accord passé entre Microsoft et l’Éducation nationale. Le collectif reproche à ce partenariat de ne pas respecter la règle des marchés publics.
