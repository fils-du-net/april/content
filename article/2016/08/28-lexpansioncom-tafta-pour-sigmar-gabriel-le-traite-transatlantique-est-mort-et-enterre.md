---
site: LExpansion.com
title: "Tafta: pour Sigmar Gabriel, le traité transatlantique est mort et enterré"
author: lexpress-fr-avec-afp
date: 2016-08-28
href: http://lexpansion.lexpress.fr/actualite-economique/tafta-pour-sigmar-gabriel-le-traite-transatlantique-est-mort-et-enterre_1825010.html
tags:
- Économie
- Institutions
- Europe
- ACTA
---

> Le ministre allemand de l'Économie a estimé ce dimanche que les discussions entre l'Union européenne et les Etats-Unis sur le traité transatlantique n'avaient pas abouti. Les négociations pourraient bien être suspendues à l'approche des élections.
