---
site: L'OBS
title: "Les algorithmes en prison!"
author: Olivier Ertzscheid
date: 2016-08-16
href: http://rue89.nouvelobs.com/2016/08/16/les-algorithmes-prison-264924
tags:
- Internet
- Institutions
---

> Depuis 10 ans, je répète souvent à mes étudiants que l’un des fondements essentiel de toute démocratie est la confidentialité de l’acte de lecture. Et tous ceux qui passent entre mes mains ont droit à la lecture commentée des «Dangers du livre électronique» de Richard Stallman, lequel texte me permet d’introduire ensuite les – heureusement nombreux – aspects positifs de (certains) livres électroniques.
