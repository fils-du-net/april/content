---
site: Next INpact
title: "TP-Link réouvre la voie aux firmwares open source sur ses routeurs"
author: Guénaël Pépin
date: 2016-08-02
href: http://www.nextinpact.com/news/100848-tp-link-reouvre-voie-aux-firmwares-open-source-sur-ses-routeurs.htm
tags:
- Entreprise
- Institutions
---

> Aux États-Unis, TP-Link a accepté de payer 200 000 dollars pour avoir enfreint les limites d'émission Wi-Fi sur certains routeurs. Dans le même temps, le fabricant doit aussi permettre à nouveau l'installation de firmwares tiers, en s'assurant de respecter la règlementation américaine.
