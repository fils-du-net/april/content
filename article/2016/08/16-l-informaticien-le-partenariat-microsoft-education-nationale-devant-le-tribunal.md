---
site: L'Informaticien
title: "Le partenariat Microsoft / Education nationale devant le tribunal"
author: Guillaume Périssat
date: 2016-08-16
href: http://www.linformaticien.com/actualites/id/41344/le-partenariat-microsoft-education-nationale-devant-le-tribunal.aspx
tags:
- Entreprise
- Institutions
- Associations
- Éducation
- Marchés publics
---

> Le partenariat signé en novembre 2015 entre l'Education nationale et Microsoft se retrouvera le 8 septembre devant le juge. Le collectif EduNathon considère en effet cet accord comme un marché public déguisé.
