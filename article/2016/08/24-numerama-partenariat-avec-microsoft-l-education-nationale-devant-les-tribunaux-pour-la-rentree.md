---
site: Numerama
title: "Partenariat avec Microsoft: l'Éducation nationale devant les tribunaux pour la rentrée"
author: Guillaume Champeau
date: 2016-08-24
href: http://www.numerama.com/business/190924-partenariat-avec-microsoft-leducation-nationale-devant-les-tribunaux-pour-la-rentree-scolaire.html
tags:
- Institutions
- Associations
- Éducation
- Marchés publics
---

> Microsoft et l'Éducation nationale auront rendez-vous le 8 septembre prochain au tribunal de grande instance de Paris, où ils sont assignés par le collectif Edunathon qui reproche à l'État d'avoir écarté abusivement des acteurs du logiciel libre d'un marché public dissimulé.
