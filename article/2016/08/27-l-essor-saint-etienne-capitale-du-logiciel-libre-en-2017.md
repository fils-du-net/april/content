---
site: L'Essor
title: "Saint-Etienne capitale du logiciel libre en 2017"
author: Denis Meynard
date: 2016-08-27
href: http://lessor.fr/saint-etienne-capitale-du-logiciel-libre-en-2017-15892.html
tags:
- Administration
- Associations
- Promotion
---

> Les organisateurs des Rencontres mondiales du logiciel libre visent 5 000 participants sur la première semaine de juillet prochain.
