---
site: Next INpact
title: "École et numérique: ce qui change en cette rentrée 2016"
author: Xavier Berne
date: 2016-08-30
href: http://www.nextinpact.com/news/101142-ecole-et-numerique-ce-qui-change-en-cette-rentree-2016.htm
tags:
- Institutions
- Éducation
---

> Alors que les écoles, collèges et lycées vont bientôt rouvrir leurs portes, cette rentrée 2016 semble plus que jamais placée sous le signe des nouvelles technologies. Le plan pour le numérique à l’école prend (enfin) son envol, en même temps que les nouveaux programmes – dans lesquels figure notamment l’apprentissage du code.
