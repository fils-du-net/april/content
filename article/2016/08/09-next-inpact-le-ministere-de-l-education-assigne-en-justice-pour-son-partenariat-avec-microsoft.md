---
site: Next INpact
title: "Le ministère de l’Éducation assigné en justice pour son partenariat avec Microsoft"
author: Xavier Berne
date: 2016-08-09
href: http://www.nextinpact.com/news/100933-le-ministere-l-education-assigne-en-justice-pour-son-partenariat-avec-microsoft.htm
tags:
- Entreprise
- Logiciels privateurs
- Associations
- Éducation
- Marchés publics
---

> La convention de « partenariat » liant le ministère de l’Éducation nationale et Microsoft sera bientôt débattue devant la justice. Le collectif EduNathon, pour qui il s’agit en réalité d’un marché public déguisé, a déposé aujourd’hui un référé auprès du tribunal de grande instance de Paris. Une audience est prévue pour le 8 septembre.
