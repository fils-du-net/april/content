---
site: NRJ.fr
title: "Comment se spécialiser dans l’informatique open source?"
date: 2016-08-09
href: http://www.nrj.fr/active/tendances/comment-se-specialiser-dans-l-informatique-open-source-31265741
tags:
- Entreprise
- Sensibilisation
- Éducation
---

> Le numérique est l’un des secteurs les plus porteurs pour les jeunes qui recherchent du travail. L’open source, soit la création de logiciels libres, est une des branches qui recrute le plus. Mais comment faire pour se spécialiser dans ce domaine particulier?
