---
site: ZDNet France
title: "Windows 10 - Microsoft ignore le choix des utilisateurs et leur vie privée"
author: Christophe Auffray
date: 2016-08-18
href: http://www.zdnet.fr/actualites/windows-10-microsoft-ignore-le-choix-des-utilisateurs-et-leur-vie-privee-39840818.htm
tags:
- Entreprise
- Logiciels privateurs
- Associations
- Vie privée
---

> Un an après le lancement de Windows 10, l'Electronic Frontier Foundation (EFF) recense toutes les pratiques qu'elle juge discutables employées par Microsoft pour favoriser l'adoption de Windows 10 et collecter des données personnelles.
