---
site: Le Monde.fr
title: "Pour Sigmar Gabriel, l’accord Tafta a «pratiquement échoué»"
author: Cécile Ducourtieux
date: 2016-08-29
href: http://www.lemonde.fr/europe/article/2016/08/29/pour-sigmar-gabriel-l-accortd-de-libre-echange-entre-l-ue-et-les-etats-unis-a-pratiquement-echoue_4989184_3214.html
tags:
- Économie
- Institutions
- Europe
- ACTA
---

> Selon le vice-chancelier allemand, les chances de conclure les négociations de l’accord de libre-échange entre l’UE et les Etats-Unis sont quasi nulles.
