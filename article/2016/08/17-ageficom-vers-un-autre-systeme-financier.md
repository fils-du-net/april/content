---
site: Agefi.com
title: "Vers un «autre» système financier"
author: Axel Lehmann
date: 2016-08-17
href: http://www.agefi.com/quotidien-agefi/une/detail/edition/2016-08-17/article/blockchain-pourquoi-et-comment-les-divers-potentiels-de-ce-corpus-technologique-saverent-veritablement-fascinants-433179.html
tags:
- Entreprise
- Économie
- Innovation
---

> Blockchain. Pourquoi et comment les divers potentiels de ce corpus technologique s’avèrent véritablement fascinants
