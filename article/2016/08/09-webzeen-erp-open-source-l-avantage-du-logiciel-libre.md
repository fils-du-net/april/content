---
site: WebZeen
title: "ERP open source: l'avantage du logiciel libre"
author: Andrea Bensaid
date: 2016-08-09
href: http://www.webzeen.fr/dossier/erp-open-source-lavantage-logiciel-libre/20873
tags:
- Entreprise
- Sensibilisation
---

> L’ERP open source ou Enterprise Ressource Planning open source, est une solution informatique incontournable pour les petites et les moyennes entreprises. Les spécificités de l’ERP open source sont en effet plus appropriées aux PME parce que, comme on peut voir sur ce site, ce type de logiciel s’adapte aux besoins particuliers des utilisateurs. Faisons un zoom sur les avantages de ce logiciel libre.
