---
site: Numerama
title: "Pourquoi l'Irlande ne veut pas des 13 milliards d'impôts d'Apple"
author: Guillaume Champeau
date: 2016-08-30
href: http://www.numerama.com/politique/191815-lirlande-ne-veut-13-milliards-dimpots-dapple.html
tags:
- Entreprise
- Économie
- Institutions
- Europe
---

> En faisant appel de la décision qui lui ordonne de récupérer 13 milliards d'euros d'impôts non perçus chez Apple, l'Irlande cherche à préserver une certaine souveraineté fiscale, dont elle use pour attirer les multinationales chez elle.
