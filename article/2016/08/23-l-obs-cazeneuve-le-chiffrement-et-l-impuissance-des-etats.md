---
site: L'OBS
title: "Cazeneuve, le chiffrement et l’impuissance des Etats"
author: Andréa Fradin
date: 2016-08-23
href: http://rue89.nouvelobs.com/2016/08/23/cazeneuve-chiffrement-limpuissance-etats-264969
tags:
- Internet
- Institutions
- Europe
- Vie privée
---

> Sur la question du chiffrement, les ministres de l’Intérieur français et allemand en appellent à la Commission européenne.
