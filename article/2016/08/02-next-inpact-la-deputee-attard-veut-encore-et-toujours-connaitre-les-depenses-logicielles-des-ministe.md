---
site: Next INpact
title: "La députée Attard veut (encore et toujours) connaître les dépenses logicielles des ministères"
author: Xavier Berne
date: 2016-08-02
href: http://www.nextinpact.com/news/100842-la-deputee-attard-veut-encore-et-toujours-connaitre-depenses-logicielles-ministeres.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
---

> Pour la quatrième année consécutive, la députée Isabelle Attard demande aux différents ministères de dévoiler leurs dépenses en logiciels, propriétaires et libres. Ses dernières initiatives ne s'étaient toutefois pas révélées très fructueuses...
