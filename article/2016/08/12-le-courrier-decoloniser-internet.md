---
site: Le Courrier
title: "Décoloniser internet"
author: Benito Perez
date: 2016-08-12
href: http://www.lecourrier.ch/141463/decoloniser_internet
tags:
- Internet
- Associations
- International
---

> La concentration des moyens de communication menace les libertés citoyennes. La riposte s'organise au Forum social mondial de Montréal
