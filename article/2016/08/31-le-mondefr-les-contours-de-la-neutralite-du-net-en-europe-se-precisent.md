---
site: Le Monde.fr
title: "Les contours de la neutralité du Net en Europe se précisent"
date: 2016-08-31
href: http://www.lemonde.fr/pixels/article/2016/08/31/les-contours-de-la-neutralite-du-net-en-europe-se-precisent_4990450_4408996.html
tags:
- Institutions
- Innovation
- Neutralité du Net
- Europe
---

> L’organe européen des régulateurs des télécoms a publié ses lignes directrices. Les associations de défense de la neutralité du Net se réjouissent.
