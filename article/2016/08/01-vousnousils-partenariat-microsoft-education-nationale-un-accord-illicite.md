---
site: vousnousils
title: "Partenariat Microsoft / Education nationale: un accord illicite?"
author: Fabien Soyez
date: 2016-08-01
href: http://www.vousnousils.fr/2016/08/01/partenariat-microsoft-education-nationale-un-accord-illicite-591249
tags:
- Entreprise
- Institutions
- Associations
- Éducation
---

> EduNathon, qui regroupe des acteurs du "numérique libre", poursuit son action contre le partenariat Education nationale / Microsoft... en justice. Il dénonce un accord "illicite".
