---
site: Next INpact
title: "Terrorisme: les nouveaux tours de vis proposés par les députés LR"
author: Marc Rees
date: 2016-08-18
href: http://www.nextinpact.com/news/101019-terrorisme-nouveaux-tours-vis-proposes-par-deputes-lr.htm
tags:
- Internet
- Institutions
- Vie privée
---

> Dans la course au sécuritaire, une ribambelle d’élus Les Républicains ont déposé voilà quelques jours une proposition de loi renforçant la lutte contre le terrorisme. Elle entend asséner de nouveaux tours de vis sur les nombreux textes en vigueur, tout en sacralisant l’état d’urgence dans le droit commun.
