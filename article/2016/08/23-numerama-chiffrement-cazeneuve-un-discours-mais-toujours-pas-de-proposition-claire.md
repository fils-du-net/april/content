---
site: Numerama
title: "Chiffrement: Cazeneuve a un discours mais toujours pas de proposition claire"
author: Guillaume Champeau
date: 2016-08-23
href: http://www.numerama.com/politique/190722-chiffrement-cazeneuve-a-un-discours-mais-toujours-pas-de-proposition-claire.html
tags:
- Internet
- Institutions
- Europe
- Vie privée
---

> Bernard Cazeneuve ne veut plus que les éventuels terroristes puissent communiquer entre eux sur des messageries chiffrées, et le fait savoir. Sans donner l'once d'une idée sur comment faire pour éviter que ça ne soit le cas.
