---
site: Silicon
title: "Document Foundation et Free Software Foundation font front commun"
author: David Feugey
date: 2016-08-22
href: http://www.silicon.fr/libreoffice-document-foundation-free-software-foundation-155533.html
tags:
- Associations
---

> La Document Foundation se rapproche de la Free Software Foundation Europe afin de favoriser l’adoption de LibreOffice.
