---
site: Le Point
title: "Contre le chiffrement, le dangereux projet de Cazeneuve"
author: Guerric Poncet
date: 2016-08-23
href: http://www.lepoint.fr/chroniqueurs-du-point/guerric-poncet/contre-le-chiffrement-le-dangereux-projet-de-cazeneuve-23-08-2016-2063292_506.php
tags:
- Internet
- Institutions
- Europe
- Vie privée
---

> Les experts craignent des conséquences catastrophiques alors que la France et l'Allemagne veulent limiter le chiffrement des télécommunications.
