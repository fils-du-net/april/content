---
site: Le Figaro
title: "Mort du Tafta: une victoire démocratique qui doit peu aux gouvernants"
author: Paul Zurkinden
date: 2016-08-30
href: http://www.lefigaro.fr/vox/politique/2016/08/30/31001-20160830ARTFIG00283-mort-du-tafta-une-victoire-democratique-qui-doit-peu-aux-gouvernants.php
tags:
- Économie
- Institutions
- ACTA
---

> Le secrétaire d'État au Commerce extérieur a annoncé la fin des négociations sur le traité transatlantique. Pour Paul Zurkinden, le Tafta est enterré pour des raisons économiques. Son caractère antidémocratique n'a pas motivé cette fin.
