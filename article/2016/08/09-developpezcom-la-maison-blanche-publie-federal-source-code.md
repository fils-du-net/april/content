---
site: Developpez.com
title: "La Maison Blanche publie Federal Source Code"
author: Stéphane le calme
date: 2016-08-09
href: http://www.developpez.com/actu/102403/La-Maison-Blanche-publie-Federal-Source-Code-un-ensemble-de-regles-qui-devront-aider-les-agences-a-mieux-embrasser-l-open-source
tags:
- Administration
---

> Après avoir vanté les bénéfices de l’open source au sein du gouvernement, en mars dernier, l’administration Obama a publié un Draft de son projet baptisé Federal Source Code, un ensemble de règles qui devraient permettre aux agences du gouvernement américain d’être plus efficaces sur le code qu’elles rédigent.
