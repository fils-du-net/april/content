---
site: ZDNet
title: "Des données libérées pour plus de citoyenneté?"
date: 2016-06-13
href: http://www.zdnet.fr/actualites/des-donnees-liberees-pour-plus-de-citoyennete-39838260.htm
tags:
- Entreprise
- Administration
- Associations
- Open Data
---

> Corollaire du Big Data, l’Open Data ou la mise à disposition de tous des données suscite des intérêts. Encore faut-il savoir dénicher les données et pouvoir les exploiter. En France après quelques pionniers, le mouvement reprend de la vigueur.
