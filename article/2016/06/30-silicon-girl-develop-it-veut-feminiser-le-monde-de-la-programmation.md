---
site: Silicon
title: "Girl Develop It veut féminiser le monde de la programmation"
author: David Feugey
date: 2016-06-30
href: http://www.silicon.fr/girl-develop-it-veut-feminiser-le-monde-de-la-programmation-151728.html
tags:
- Entreprise
- Open Data
---

> Apprendre aux femmes à développer du code. Voilà la mission que s’est fixée Girl Develop It, qui s’appuie pour l’essentiel sur des technologies Open Source.
