---
site: Libération
title: "Appli alerte attentats: «Il faut que la France respecte les standards internationaux»"
author: Camille Gévaudan
date: 2016-06-09
href: http://www.liberation.fr/futurs/2016/06/09/appli-alerte-attentats-il-faut-que-la-france-respecte-les-standards-internationaux_1458385
tags:
- Internet
- HADOPI
- Institutions
- Associations
- Standards
- Vie privée
---

> Alors que le gouvernement propose une appli pour les alertes aux attentats, Gaël Musquet, hacker et militant du logiciel libre, presse l'Etat d'adopter la diffusion cellulaire, plus efficace et respectueuse de la vie privée.
