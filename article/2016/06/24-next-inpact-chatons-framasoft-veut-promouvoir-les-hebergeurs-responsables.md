---
site: Next INpact
title: "CHATONS: Framasoft veut promouvoir les hébergeurs «responsables»"
author: Vincent Hermann
date: 2016-06-24
href: http://www.nextinpact.com/news/100393-chatons-framasoft-veut-promouvoir-hebergeurs-responsables.htm
tags:
- Internet
- Associations
- Promotion
- Informatique en nuage
---

> Framasoft, après avoir souhaité «degoogliser» Internet, tourne son regard vers les hébergeurs. Il prépare ainsi une nouvelle initiative, baptisée CHATONS, qui doit permettre à n’importe quel type de structure de stocker des données en respectant une stricte charte de transparence. L'idée reste la même: lutter contre les GAFAM.
