---
site: Next INpact
title: "Mozilla crée le fonds «SOS» pour renforcer la sécurité de l'open source"
author: Vincent Hermann
date: 2016-06-13
href: http://www.nextinpact.com/news/100225-mozilla-cree-fonds-sos-pour-renforcer-securite-open-source.htm
tags:
- Internet
- Associations
- Vie privée
---

> Mozilla ne veut plus d’un nouveau Heartbleed. Pour augmenter le niveau général de sécurité du logiciel libre, l’éditeur a décidé de créer un fonds de financement pour les audits, avec une mise de départ de 500 000 dollars. La société invite maintenant d’autres acteurs à participer.
