---
site: Les Echos
title: "La France, un pays discret, mais innovant"
author: Grégory Pascal
date: 2016-06-30
href: http://www.lesechos.fr/idees-debats/cercle/cercle-158533-la-france-un-pays-discret-mais-innovant-2011051.php
tags:
- Entreprise
- Économie
- Innovation
---

> Quand on parle "innovation" et "nouvelles technologies", on pense tout de suite aux États-Unis et plus particulièrement à la Silicon Valley, mais certainement pas à la France. La France, c’est plutôt le vin, la gastronomie, le luxe… et plus rarement la création technologique et les start-ups.
