---
site: ZDNet
title: "L’Open Source est en train de tuer Android"
author: Adrian Kingsley-Hughes
date: 2016-06-21
href: http://www.zdnet.fr/actualites/l-open-source-est-en-train-de-tuer-android-39838644.htm
tags:
- Entreprise
- Logiciels privateurs
- Innovation
---

> La plupart des problèmes d’Android proviennent du fait qu’il s’agit d’un système d’exploitation Open source.
