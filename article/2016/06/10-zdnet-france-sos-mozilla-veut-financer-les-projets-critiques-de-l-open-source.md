---
site: ZDNet France
title: "SOS: Mozilla veut financer les projets critiques de l’Open Source"
date: 2016-06-10
href: http://www.zdnet.fr/actualites/sos-mozilla-veut-financer-les-projets-critiques-de-l-open-source-39838188.htm
tags:
- Associations
---

> La Fondation annonce le lancement de son programme SOS, acronyme pour Secure open source. Celui-ci fait partie de l’effort plus global de Mozilla pour financer les projets open source, mais celui-ci se focalisera sur le financement de projets jugés critiques pour l’écosystème open source.
