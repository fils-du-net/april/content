---
site: Numerama
title: "En 2020, toutes les publications scientifiques européennes publiques seront libres d'accès"
author: Omar Belkaab
date: 2016-06-04
href: http://www.numerama.com/sciences/173928-2020-toutes-publications-scientifiques-europeennes-seront-libres-dacces.html
tags:
- Partage du savoir
- Institutions
- Sciences
- Europe
---

> Le Conseil Compétitivité de l'Union européenne a annoncé qu'à partir de 2020, les résultats de toutes les recherches scientifiques financées en partie ou en totalité par des fonds publics seraient disponibles en libre accès.
