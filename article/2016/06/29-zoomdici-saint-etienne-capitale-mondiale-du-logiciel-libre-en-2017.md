---
site: zoomdici
title: "Saint-Étienne capitale mondiale du Logiciel Libre en 2017"
date: 2016-06-29
href: http://www.zoomdici.fr/actualite/Saint-Etienne-capitale-mondiale-du-Logiciel-Libre-en-2017-id152417.html
tags:
- Administration
- Associations
- Promotion
---

> Après Strasbourg, Genève, Bruxelles, Montpellier et Beauvais, Saint-Étienne accueillera «Les Rencontres Mondiales du Logiciel Libre» (RMLL) du 1er au 7 juillet 2017.
