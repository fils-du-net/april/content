---
site: Sud Ouest
title: "Informatique: Open Source School arrive à Bordeaux pour pallier la pénurie de candidats"
author: Nicolas César
date: 2016-06-08
href: http://www.sudouest.fr/2016/06/08/informatique-oss-school-arrive-a-bordeaux-pour-pallier-la-penurie-d-emplois-2391609-705.php
tags:
- Entreprise
- Économie
- Éducation
---

> 4 000 emplois restent non pourvus chaque année en France dans les logiciels libres. La première école d’enseignement supérieur et de formation continue dans l’open source a pris mardi 6 juin ses quartiers aux Bassins à flot.
