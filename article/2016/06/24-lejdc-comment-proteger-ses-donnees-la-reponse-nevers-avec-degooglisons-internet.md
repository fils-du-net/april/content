---
site: lejdc
title: "Comment protéger ses données? La réponse à Nevers avec Dégooglisons Internet"
author: Lara Payet
date: 2016-06-24
href: http://www.lejdc.fr/nievre/actualite/pays/grand-nevers/2016/06/24/degooglisons-internet-a-nevers-ce-week-end-des-solutions-pour-surfer-plus-librement_11974072.html
tags:
- Entreprise
- Internet
- Informatique en nuage
- Vie privée
---

> La start-up Cozy cloud participera, ce week-end, du 25 et 26 juin, à l’opération Dégooglisons Internet. Tristant Nitot, est l’un des dirigeants de cette boîte qui offre plusieurs services pour protéger les données personnelles.
