---
site: Les Echos
title: "Panorama des dispositions du projet de loi pour une République numérique"
author: Dla Piper
date: 2016-06-03
href: http://business.lesechos.fr/directions-juridiques/partenaire/partenaire-656-panorama-des-dispositions-du-projet-de-loi-pour-une-republique-numerique-211033.php
tags:
- Administration
- Institutions
- Open Data
---

> A l'heure de l'innovation technologique et de l'utilisation du numérique tant dans le cadre personnel que professionnel, il semble que la loi de 1978 ne puisse plus faire face à la réalité des situations induites par les nouvelles technologies. Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés
