---
site: L'OBS
title: "Furieux de l’arrivée du «méchant Microsoft», ils fuient LinkedIn"
author: Thierry Noisette
date: 2016-06-14
href: http://rue89.nouvelobs.com/2016/06/14/furieux-larrivee-mechant-microsoft-ils-fuient-linkedin-264348
tags:
- Entreprise
- Internet
- Vie privée
---

> Ils n’aimaient déjà pas le poids de Microsoft dans les PC, ils n’apprécient donc pas plus son arrivée tonitruante dans les réseaux sociaux.
