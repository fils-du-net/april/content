---
site: L'Informaticien
title: "SOS: Mozilla finance la sécurité de l'Open Source"
author: Guillaume Périssat
date: 2016-06-13
href: http://www.linformaticien.com/actualites/id/40768/sos-mozilla-finance-la-securite-de-l-open-source.aspx
tags:
- Internet
- Associations
- Innovation
---

> Avec Secure Open Source, Mozilla concentre une partie des efforts de son programme Moss dans la détection et la correction de failles dans l’open source.
