---
site: The Good Life
title: "Intelligence Artificielle: nouvelle cyber-guerre économique?"
author: Kahina Meziant
date: 2016-06-29
href: http://thegoodlife.thegoodhub.com/2016/06/29/intelligence-artificielle-vers-nouvelle-cyber-guerre-economique
tags:
- Entreprise
- Partage du savoir
- Innovation
---

> Nous assistons à une véritable course à la recherche autour de l'IA. Qu’est-ce donc que cette forme d’intelligence et pourquoi cet engouement soudain?
