---
site: "@Sekurigi"
title: "La culture des hackers dans le monde de la politique"
date: 2016-06-17
href: http://www.sekurigi.com/2016/06/culture-hackers-monde-de-politique
tags:
- Internet
- Institutions
---

> A l’ère des luttes décentralisées et des mouvements d’occupation de type «Nuit debout», les hackers ont leur part d’influence dans les mouvements politiques actuels.
