---
site: Next INpact
title: "Parti de Tor, Jacob Appelbaum nie les allégations d'agressions sexuelles"
author: Vincent Hermann
date: 2016-06-06
href: http://www.nextinpact.com/news/100139-accuse-dagressions-sexuelles-jacob-appelbaum-quitte-projet-tor.htm
tags:
- Internet
- Associations
- Vie privée
---

> «Ce sera notre seul et unique communiqué public». Le projet Tor a annoncé samedi que le développeur Jacob Appelbaum était la cible d'allégations d'abus sexuels et qu'un cabinet spécialisé avait été engagé. Dans un tout récent communiqué, le développeur conteste ces accusations.
