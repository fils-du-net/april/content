---
site: Silicon
title: "Décrire un logiciel par le menu n'est pas saper le droit d'auteur"
author: Ariane Beky
date: 2016-06-07
href: http://www.silicon.fr/decrire-logiciel-droit-auteur-149416.html
tags:
- Entreprise
- Administration
- Institutions
- Droit d'auteur
---

> Faute d’apporter la preuve de l’originalité de son logiciel de gestion d’archives, la société Anaphore qui avait assigné le Conseil général de l’Eure en contrefaçon, n’a pas obtenu gain de cause.
