---
site: Silicon
title: "Loi Numérique: ce qu'a retenu la CMP rassure l'industrie"
author: Ariane Beky
date: 2016-06-30
href: http://www.silicon.fr/loi-republique-numerique-retenu-cmp-rassure-industrie-151827.html
tags:
- Administration
- Institutions
- Open Data
---

> L'équilibre trouvé en commission mixte paritaire rend au projet de loi République numérique «son ambition initiale», selon Tech In France.
