---
site: ZDNet France
title: "Les logiciels libres, ”seule alternative viable” pour la souveraineté numérique"
author: Thierry Noisette
date: 2016-06-26
href: http://www.zdnet.fr/actualites/les-logiciels-libres-seule-alternative-viable-pour-la-souverainete-numerique-39838886.htm
tags:
- Internet
- Administration
- Institutions
- Associations
- Marchés publics
- RGI
- Informatique en nuage
---

> Informaticien et commissaire à la Cnil, François Pellegrini plaide pour une priorité aux logiciels libres afin d'assurer une souveraineté numérique réelle.
