---
site: 01net.
title: "Intel embarque-t-il une porte dérobée dans toutes ses puces?"
author: Gilbert Kallenborn
date: 2016-06-17
href: http://www.01net.com/actualites/intel-embarque-t-il-une-porte-derobee-dans-ses-puces-985396.html
tags:
- Entreprise
- Logiciels privateurs
- Vie privée
---

> De plus en plus d’experts critiquent ouvertement le composant Management Engine qu’Intel embarque dans tous ses chipsets récents. C’est un ordinateur dans l’ordinateur, doté d’énormes privilèges d’accès et dont le code est complètement opaque.
