---
site: Le Monde Informatique
title: "Mozilla finance des audits de sécurité de code open source"
author: Stephen Lawson
date: 2016-06-10
href: http://www.lemondeinformatique.fr/actualites/lire-mozilla-finance-des-audits-de-securite-de-code-open-source-65108.html
tags:
- Entreprise
- Internet
- Associations
---

> La fondation Mozilla a créé Secure Open Source, un fonds qui va permettre de financer des audits de sécurité de code open source afin d'identifier des bugs de sécurité critiques. Ce dernier fait partie d'une initiative plus globale, Open Source Support, lancée en octobre dernier.
