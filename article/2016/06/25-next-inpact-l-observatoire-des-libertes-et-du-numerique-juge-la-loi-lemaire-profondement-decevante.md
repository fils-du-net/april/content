---
site: Next INpact
title: "L'Observatoire des libertés et du numérique juge la loi Lemaire «profondément décevante»"
author: Xavier Berne
date: 2016-06-25
href: http://www.nextinpact.com/news/100411-l-observatoire-libertes-et-numerique-juge-loi-lemaire-profondement-decevante.htm
tags:
- Administration
- Institutions
- Associations
---

> Même si le Parlement ne l’a pas encore adopté définitivement, l’Observatoire des libertés et du numérique (qui compte dans ses rangs la Ligue des droits de l'Homme ou La Quadrature du Net) juge d’ores et déjà que le projet de loi Numérique porté par Axelle Lemaire est «profondément» décevant et décourageant, tant sur sa méthode d’élaboration que sur son contenu.
