---
site: "ouest-france.fr"
title: "Une école du logiciel libre à Nantes"
date: 2016-06-15
href: http://jactiv.ouest-france.fr/job-formation/se-former/ecole-logiciel-libre-nantes-64247
tags:
- Entreprise
- Éducation
---

> Cloud, big data, digital... Une école du logiciel libre et des solutions open source (OSS) ouvrira à Nantes à la rentrée 2016. Soutenue financièrement par l'État, elle fait partie des six établissements d'enseignement supérieur de ce type à ouvrir en France, avec Paris, Lyon, Lille, Bordeaux, Montpellier. Lancé par Smile (expert en open source) et l'Epsi (école d'ingénierie informatique), l'établissement mise sur les besoins actuels dans le domaine du logiciel libre. Le monde numérique manque d'experts.
