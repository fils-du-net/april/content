---
site: Silicon
title: "Jim Whitehurst, Red Hat: «Linux a gagné»"
author: David Feugey
date: 2016-06-29
href: http://www.silicon.fr/whitehurst-red-hat-linux-gagne-151602.html
tags:
- Entreprise
- Innovation
---

> Le patron de Red Hat revient sur la révolution apportée par le modèle de développement participatif, utilisé par des projets comme Linux.
