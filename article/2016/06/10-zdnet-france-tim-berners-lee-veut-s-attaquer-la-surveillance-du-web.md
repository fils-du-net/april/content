---
site: ZDNet France
title: "Tim Berners Lee veut s’attaquer à la surveillance du Web"
date: 2016-06-10
href: http://www.zdnet.fr/actualites/tim-berners-lee-veut-s-attaquer-a-la-surveillance-du-web-39838174.htm
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- Vie privée
---

> Lors d’un sommet tenu à San Francisco, le père fondateur du web a exprimé ses inquiétudes à l’égard de son réseau, devenu aujourd’hui un «outil de surveillance» de la population. Le chercheur appelle a trouver des solutions techniques pour lutter contre ce phénomène.
