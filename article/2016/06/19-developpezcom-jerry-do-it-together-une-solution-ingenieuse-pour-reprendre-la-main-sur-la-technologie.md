---
site: Developpez.com
title: "Jerry Do-It-Together une solution ingénieuse pour reprendre la main sur la technologie"
author: ideefixe
date: 2016-06-19
href: http://hardware.developpez.com/actu/100150/Jerry-Do-It-Together-une-solution-ingenieuse-pour-reprendre-la-main-sur-la-technologie
tags:
- Associations
- Innovation
---

> Jerry Do-It-Together est un ordinateur PC fabriqué avec des composants informatiques de réemploi, carte mère, disque dur et bloc d'alimentation électrique, assemblés dans un bidon en plastique de vingt litres. Ce récipient, d'un usage courant en Afrique, permet d'installer un bloc d'alimentation électrique quelconque. Il est également possible d'attacher un disque dur d'ordinateur portable avec des cordons élastiques. L'ouverture du couvercle par fermeture éclair offre une vue sur les composants et démystifie le contenu de cet appareil électronique.
