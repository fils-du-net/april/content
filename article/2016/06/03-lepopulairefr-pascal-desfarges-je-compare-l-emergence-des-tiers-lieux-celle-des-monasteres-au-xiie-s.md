---
site: lepopulaire.fr
title: "Pascal Desfarges: «Je compare l’émergence des tiers-lieux à celle des monastères au XIIe siècle»"
author: Julien Rapegno
date: 2016-06-03
href: http://www.lepopulaire.fr/limousin/actualite/departement/creuse/2016/06/03/pascal-desfarges-je-compare-lemergence-des-tiers-lieux-a-celle-des-monasteres-au-xiie-siecle_11939823.html
tags:
- Économie
- Associations
---

> Je compare l’émergence des tiers-lieux à celle des monastères au XIIe siècle. Le tissu des monastères a irrigué le territoire en amenant le savoir. Aujourd’hui se tissent des réseaux de lieux de savoir, à l’échelle mondiale, porteurs de nouveaux modèles sociaux, économiques, politiques. De nouveaux médias. Et la France est particulièrement bien placée car on a, depuis la fin des années 1990, un réseau d’espaces publics numériques qui muent en tiers-lieux.
