---
site: Silicon
title: "La communauté renforce son soutien en faveur de LibreOffice"
author: David Feugey
date: 2016-06-20
href: http://www.silicon.fr/la-communaute-linux-renforce-son-soutien-en-faveur-de-libreoffice-150738.html
tags:
- Associations
- Innovation
---

> The Document Foundation, The GNOME Foundation et KDE e.V. font bloc autour de la suite bureautique Open Source LibreOffice.
