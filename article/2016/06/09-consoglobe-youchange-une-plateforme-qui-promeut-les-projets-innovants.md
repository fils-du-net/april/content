---
site: consoGlobe
title: "YouChange: une plateforme qui promeut les projets innovants"
date: 2016-06-09
href: http://www.consoglobe.com/youchange-plateforme-promeut-projets-innovants-cg
tags:
- Entreprise
- Internet
- Innovation
---

> La plateforme YouChange.org se propose d’être la première encyclopédie open-source des projets durables. Les particuliers, les entreprises ou les porteurs de projet peuvent, sur cette plateforme, suivre et proposer des projets innovants et écologiques.
