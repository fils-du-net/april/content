---
site: "usine-digitale.fr"
title: "La bibliothèque universelle de l'open source sera française"
author: Ridha Loukil
date: 2016-06-30
href: http://www.usine-digitale.fr/article/la-bibliotheque-universelle-de-l-open-source-sera-francaise.N400402
tags:
- Administration
- Partage du savoir
- Sciences
---

> Après un an et demi de travaux, l’Inria lance les fondations d’une bibliothèque d’Alexandrie du logiciel open source. Le projet Software Heritage a vocation à rassembler scientifiques, industriels et sponsors du monde entier. Microsoft est le premier grand éditeur à se joindre à l’initiative.
