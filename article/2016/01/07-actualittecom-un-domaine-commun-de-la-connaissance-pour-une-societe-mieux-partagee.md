---
site: ActuaLitté.com
title: "Un domaine commun de la connaissance, pour une société mieux partagée"
author: Clément Solym
date: 2016-01-07
href: https://www.actualitte.com/article/monde-edition/un-domaine-commun-de-la-connaissance-pour-une-societe-mieux-partagee/62858
tags:
- Institutions
- Associations
- Droit d'auteur
---

> «Suite à la non-prise en compte de leurs propositions d’amendements au projet de loi numérique, 10 acteurs majeurs du numérique et du Libre signent aujourd’hui une déclaration commune visant à présenter 5 amendements essentiels à la protection et à la valorisation d’un “domaine commun de la connaissance.»
