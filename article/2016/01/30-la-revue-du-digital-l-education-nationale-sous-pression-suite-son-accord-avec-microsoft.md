---
site: La Revue du Digital
title: "L’Education nationale sous pression suite à son accord avec Microsoft"
date: 2016-01-30
href: https://www.larevuedudigital.com/2016/01/30/leducation-nationale-sous-pression-suite-a-son-accord-avec-microsoft
tags:
- Entreprise
- Institutions
- Associations
- Éducation
- Marchés publics
---

> Trop c’est trop. L’offre en apparence généreuse de Microsoft de former les enseignants de l’Education nationale gratuitement au numérique, aura fait sortir de ses gonds plusieurs associations qui travaillent dans les logiciels libres.
