---
site: Traces Ecrites
title: "A Besançon, les «french makers» ont leur FabLab"
author: Monique Clémens
date: 2016-01-19
href: http://www.tracesecritesnews.fr/actualite/a-besancon-les-french-makers-ont-leur-fablab-69784
tags:
- Entreprise
- Matériel libre
- Innovation
---

> Imprimantes 3D, découpe laser, fraisage céramique, prototypage rapide, DAO et autres outils numériques…
> Le premier FabLab bisontin, privé, met à disposition de ses usagers - des particuliers, des étudiants, des entreprises - divers équipements et ateliers pour créer, inventer, fabriquer. Visite guidée.
