---
site: Le Monde.fr
title: "L’Assemblée fait le premier pas vers la création d’un «Windows» français"
date: 2016-01-21
href: http://www.lemonde.fr/pixels/article/2016/01/21/l-assemblee-vote-la-disposition-pour-un-systeme-d-exploitation-souverain_4851279_4408996.html
tags:
- Institutions
- Sciences
- Open Data
---

> D’autres mesures concernant le logiciel libre et l’ouverture des données publiques ont été adoptées lors de la discussion du projet de loi «pour une République numérique», dense et parfois aride.
