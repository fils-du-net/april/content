---
site: Silicon.fr
title: "Loi numérique: les amendements sur le logiciel libre divisent"
author: Ariane Beky
date: 2016-01-14
href: http://www.silicon.fr/amendements-loi-republique-numerique-logiciel-libre-135835.html
tags:
- Administration
- April
- Institutions
- Promotion
---

> Après 8500 contributions externes et 148 000 votes en ligne, plus de 700 amendements ont été portés par les députés au projet de loi numérique d’Axelle Lemaire, dont ceux sur le logiciel libre.
