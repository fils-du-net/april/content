---
site: Developpez.com
title: "Ian Murdock n'est plus: le fondateur du projet Debian est décédé"
author: Stéphane le Calme
date: 2016-01-01
href: http://www.developpez.com/actu/94416/Ian-Murdock-n-est-plus-le-fondateur-du-projet-Debian-est-decede-apres-une-arrestation-et-apres-avoir-profere-sur-twitter-des-menaces-de-sucide
tags:
- Philosophie GNU
---

> La communauté Linux en général et le projet Debian en particulier sont en deuil: Ian Murdock, le fondateur de la distribution Debian, est décédé lundi dernier aux États-Unis à l’âge de 42 ans dans des conditions qui n’ont pas encore été clairement définies. Pour la petite histoire, Ian Murdock épouse Debra Lynn, et la contraction de leurs deux prénoms a donné son nom à la distribution Debian.
