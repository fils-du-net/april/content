---
site: Next INpact
title: "Le projet de loi Lemaire accusé de fragiliser l'économie numérique française"
author: Xavier Berne
date: 2016-01-15
href: http://www.nextinpact.com/news/98098-le-projet-loi-lemaire-accuse-fragiliser-l-economie-numerique-francaise.htm
tags:
- Entreprise
- Internet
- Économie
- Institutions
---

> Le renforcement des droits des internautes, tel que prévu par le projet de loi numérique, n’est pas du goût de tous les acteurs de l’informatique et de l’internet. Quatre organisations de professionnels du secteur demandent au législateur de revoir sa copie, y compris sur la question de l’utilisation des logiciels libres au sein de l’administration.
