---
site: La gazette.fr
title: "Loi numérique: ce qu’il faut retenir du passage en commission des lois"
author: Sabine Blanc
date: 2016-01-16
href: http://www.lagazettedescommunes.com/425487/loi-numerique-la-donnee-et-les-savoirs-circulent-un-tout-petit-peu-plus
tags:
- Administration
- Institutions
- Open Data
---

> Le texte porté par la secrétaire d’Etat en charge du Numérique a été examiné entre mardi 11 et jeudi 14 janvier par plusieurs commissions: affaires culturelles et éducation, affaires sociales et affaires économiques, saisies pour avis, et commission des lois, saisie sur le fond. Récapitulatif des principaux amendements intéressant, de près ou de loin, les collectivités.
