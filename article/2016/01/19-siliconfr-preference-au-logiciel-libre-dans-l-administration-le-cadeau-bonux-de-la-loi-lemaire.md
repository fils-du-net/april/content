---
site: Silicon.fr
title: "Préférence au logiciel libre dans l’administration: le cadeau bonux de la loi Lemaire"
author: Reynald Fléchaux
date: 2016-01-19
href: http://www.silicon.fr/preference-logiciel-libre-administration-cadeau-loi-lemaire-136205.html
tags:
- Administration
- Institutions
- Marchés publics
- RGI
---

> Plusieurs amendements à la loi Lemaire prévoient de donner la priorité au logiciel libre dans les achats de l’administration. Le retour d’un marqueur idéologique de gauche?
