---
site: CB News
title: "La 'république numérique' demain à l’Assemblée"
author: Thierry Wojciak
date: 2016-01-17
href: http://www.cbnews.fr/digital/les-principaux-points-du-projet-de-loi-pour-une-republique-numerique-a1024999
tags:
- Administration
- Institutions
- Sciences
- Open Data
---

> C'est ce mardi à l'Assemblée nationale que débute l'examen en première lecture les discussions autour du projet de loi "pour une république numérique". Organisé autour de 3 grands chapitres, voici les principaux points de ce projet défendu par Axelle Lemaire, secrétaire d'Etat chargée du numérique.
