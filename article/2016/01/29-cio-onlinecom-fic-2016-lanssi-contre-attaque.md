---
site: "cio-online.com"
title: "FIC 2016: l'ANSSI contre-attaque"
author: Serge Leblal
date: 2016-01-29
href: http://www.cio-online.com/actualites/lire-fic-2016-l-anssi-contre-attaque-8192.html
tags:
- Administration
- Institutions
---

> A l'occasion de son point presse au dernier FIC (les 25 et 26 janvier à Lille), le directeur général de l'ANSSI Guillaume Poupard est revenu sur un certain nombre de sujets. Notamment les certifications demandées aux OIV, la directive NIS sur le devoir de transparence imposé aux entreprises du web et le retour de l'OS souverain ou la sécurité des ministères.
