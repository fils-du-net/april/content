---
site: ActuaLitté.com
title: "Le guide de l'Open Access pour Les Nuls, et au bénéfice du grand public"
author: Marie Lebert
date: 2016-01-19
href: https://www.actualitte.com/article/lecture-numerique/le-guide-de-l-open-access-pour-les-nuls-et-au-benefice-du-grand-public/63062
tags:
- Droit d'auteur
- Licenses
- Sciences
- Open Data
---

> Le mouvement promouvant l’accès ouvert (open access en anglais) à la recherche a été lancé avec une belle idée, celle de mettre les résultats de la recherche à la disposition de tous dans des archives ouvertes et des revues ouvertes. Ce mouvement conquiert maintenant le monde pour le plus grand bénéfice des auteurs, des chercheurs, des étudiants, des bibliothèques, des éditeurs, des universités et des centres de recherche. Et, tout aussi important peut-être, pour le bénéfice du grand public, quelle que soit sa formation et quel que soit son parcours professionnel.
