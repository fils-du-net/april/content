---
site: LeDevoir.com
title: "Les petits de l’économie collaborative s’organisent"
author: Jeanne Corriveau
date: 2016-01-22
href: http://www.ledevoir.com/societe/actualites-en-societe/460966/les-petits-de-l-economie-collaborative-s-organisent
tags:
- Entreprise
- Économie
- Innovation
---

> Devant les «prédateurs» Uber et Airbnb, un «think tank» s’installe à Montréal pour multiplier l’influence de 150 organismes à l’échelle du Québec
