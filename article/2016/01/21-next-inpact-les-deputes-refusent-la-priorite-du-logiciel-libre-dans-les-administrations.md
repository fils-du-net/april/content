---
site: Next INpact
title: "Les députés refusent la priorité du logiciel libre dans les administrations"
author: Xavier Berne
date: 2016-01-21
href: http://www.nextinpact.com/news/98171-les-deputes-refusent-priorite-logiciel-libre-dans-administrations.htm
tags:
- Administration
- Institutions
- Sensibilisation
- Marchés publics
- Vie privée
---

> Suivant l’avis du gouvernement, les députés ont refusé hier d’obliger les administrations à recourir «prioritairement» aux logiciels libres et aux formats ouverts. Une proposition qui avait pourtant été très largement soutenue lors de la consultation citoyenne relative au projet de loi numérique.
