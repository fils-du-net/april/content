---
site: ZDNet France
title: "Github: les utilisateurs déçus du manque de support"
date: 2016-01-19
href: http://www.zdnet.fr/actualites/github-les-utilisateurs-decus-du-manque-de-support-39831396.htm
tags:
- Entreprise
- Logiciels privateurs
- Innovation
---

> La plateforme de partage de code open source Github ne fait pas que des heureux. Dans une pétition signée par plusieurs centaines d’utilisateurs, les contributeurs de différents projets détaillent leurs griefs à l’égard de la plateforme, jugée trop peu transparente.
