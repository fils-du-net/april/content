---
site: ZDNet France
title: "Loi Numérique: les amendements sur le logiciel libre ne font pas que des heureux"
author: Louis Adam
date: 2016-01-15
href: http://www.zdnet.fr/actualites/loi-numerique-les-amendements-sur-le-logiciel-libre-ne-font-pas-que-des-heureux-39831208.htm
tags:
- Entreprise
- Administration
- April
- Institutions
---

> Le projet de loi numérique prend forme: après un passage en commission des lois, les députés ont achevé d’examiner les 600 amendements déposés par les parlementaires. Et les dispositions en faveur du logiciel libre suscitent des réactions parmi les éditeurs.
