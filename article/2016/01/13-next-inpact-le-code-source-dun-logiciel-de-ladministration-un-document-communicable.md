---
site: Next INpact
title: "Le code source d'un logiciel de l'administration, un document communicable"
author: Marc Rees
date: 2016-01-13
href: http://www.nextinpact.com/news/98062-le-code-source-dun-logiciel-administration-document-communicable.htm
tags:
- Administration
- April
- Institutions
---

> En commission des lois, les députés ont adopté un amendement visant à reconnaitre le code source comme un document communicable au titre des dispositions de la loi CADA.
