---
site: Numerama
title: "Un OS souverain? «Si c'est plein de saletés, je m'y opposerai»"
author: Guillaume Champeau
date: 2016-01-26
href: http://www.numerama.com/tech/140949-un-os-souverain-on-est-pas-en-coree-du-nord.html
tags:
- Administration
- Institutions
- Vie privée
---

> Le directeur général de l'Agence nationale de sécurité des systèmes d'information (Anssi) a fait savoir qu'il était farouchement opposé à l'idée de créer au niveau national un OS souverain conseillé aux Français.
