---
site: Numerama
title: "Microsoft France dit son amour du Libre et de la vie privée"
author: Guillaume Champeau
date: 2016-01-15
href: http://www.numerama.com/tech/139189-microsoft-france-dit-son-amour-du-libre-et-de-la-vie-privee.html
tags:
- Entreprise
- April
- Institutions
- Éducation
- Vie privée
---

> Dans un communiqué, Microsoft France a répondu à l'April sur la question des logiciels libres dans le cadre scolaire.
