---
site: Next INpact
title: "Loi Numérique: les principaux amendements adoptés ou rejetés en commission"
author: Xavier Berne
date: 2016-01-14
href: http://www.nextinpact.com/news/98075-loi-numerique-principaux-amendements-adoptes-ou-rejetes.htm
tags:
- Administration
- Institutions
- Open Data
---

> La commission des lois de l’Assemblée nationale poursuit jeudi 14 janvier son examen du projet de loi numérique d’Axelle Lemaire (à partir de 10h, puis à 15h). Vous pourrez suivre les débats en vidéo ou via notre synthèse des principaux amendements adoptés ou rejetés.
