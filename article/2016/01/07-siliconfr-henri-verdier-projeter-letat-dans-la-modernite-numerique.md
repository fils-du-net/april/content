---
site: Silicon.fr
title: "Henri Verdier: «projeter l'Etat dans la modernité numérique»"
author: Morgane Tual
date: 2016-01-07
href: http://www.silicon.fr/henri-verdier-dinsic-projeter-etat-modernite-numerique-135019.html
tags:
- Administration
- Open Data
---

> A la tête d’une DSI de l’Etat élargie, la Dinsic, Henri Verdier entend infuser la culture du numérique au cœur du fonctionnement de l’Etat. Entretien exclusif.
