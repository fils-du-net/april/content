---
site: Numerama
title: "David Bowie avait lancé son propre FAI et anticipait la mort du copyright"
author: Guillaume Champeau
date: 2016-01-11
href: http://www.numerama.com/pop-culture/138070-david-bowie-avait-lance-son-propre-fai-et-predit-la-mort-du-copyright.html
tags:
- Internet
- Droit d'auteur
- Innovation
---

> Fasciné par le numérique, David Bowie avait anticipé la mort du droit d'auteur tel qu'il l'avait connu au vingtième siècle, et lancé son propre fournisseur d'accès à internet à la fin des années 1990, pour accompagner les besoins de ses fans plutôt que de lutter contre la montée inexorable des réseaux P2P.
