---
site: Le Monde.fr
title: "Le gouvernement néerlandais défend le chiffrement des données"
author: Morgane Tual
date: 2016-01-07
href: http://www.lemonde.fr/pixels/article/2016/01/07/le-gouvernement-neerlandais-defend-le-chiffrement-des-donnees_4842993_4408996.html
tags:
- Internet
- Institutions
- International
- Vie privée
---

> A contre-courant, le ministre de la sécurité et de la justice affirme, dans un texte officiel, que le chiffrement protège les citoyens, les entreprises et le gouvernement.
