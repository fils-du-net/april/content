---
site: ZDNet France
title: "Linux et l'open source ont gagné, acceptez-le"
author: Steven J. Vaughan-Nichols
date: 2016-01-04
href: http://www.zdnet.fr/actualites/linux-et-l-open-source-ont-gagne-acceptez-le-39830530.htm
tags:
- Entreprise
- Vente liée
- Promotion
- Informatique en nuage
---

> 2015 a été l'année durant laquelle Linux et le logiciel Open Source ont pris le pouvoir dans le monde de l'IT. Mais de nombreux fans du logiciel open source et propriétaires n'en ont toujours pas pris conscience.
