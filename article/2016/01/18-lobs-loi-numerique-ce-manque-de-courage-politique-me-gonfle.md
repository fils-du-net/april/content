---
site: L'OBS
title: "Loi numérique: «Ce manque de courage politique me gonfle»"
author: Andréa Fradin
date: 2016-01-18
href: http://rue89.nouvelobs.com/2016/01/18/loi-numerique-manque-courage-politique-gonfle-262842
tags:
- Institutions
- Open Data
---

> Le projet de loi numérique est discuté mardi à l’Assemblée nationale. Isabelle Attard, députée écologiste qui a déposé 66 amendements, dénonce les lobbies à l’œuvre dans la discussion et l’absence de stratégie numérique du gouvernement. Entretien.
