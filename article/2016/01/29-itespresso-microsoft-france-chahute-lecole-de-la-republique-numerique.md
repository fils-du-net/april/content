---
site: ITespresso
title: "Microsoft France chahuté à l'école de la République numérique"
author: Clément Bohic
date: 2016-01-29
href: http://www.itespresso.fr/microsoft-france-chahute-ecole-republique-numerique-120158.html
tags:
- Entreprise
- Institutions
- Associations
- Éducation
- Marchés publics
---

> Le collectif Édunathon, qui défend le logiciel libre, veut faire annuler la convention signée en novembre dernier entre Microsoft et l’Éducation nationale.
