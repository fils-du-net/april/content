---
site: ZDNet France
title: "OS souverain: le choix Linux pour nos lecteurs"
date: 2016-01-25
href: http://www.zdnet.fr/actualites/os-souverain-le-choix-linux-pour-nos-lecteurs-39831642.htm
tags:
- Administration
- Institutions
- Vente liée
---

> Les ambitions françaises de plancher sur un éventuel OS souverain pourraient s'articuler autour de l'approche libre de Linux. D'ailleurs, une version dédiée et sécurisée de l'OS est déjà proposée aux opérateurs d'importance vitale par l'Anssi.
