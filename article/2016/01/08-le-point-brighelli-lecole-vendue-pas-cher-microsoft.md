---
site: Le Point
title: "Brighelli - L'école vendue (pas cher) à Microsoft"
author: Jean-Paul Brighelli
date: 2016-01-08
href: http://www.lepoint.fr/invites-du-point/jean-paul-brighelli/brighelli-l-ecole-vendue-pas-cher-a-microsoft-08-01-2016-2008088_1886.php
tags:
- Entreprise
- Institutions
- Éducation
---

> Sans le moindre appel d'offres, Microsoft vient de s'offrir 850 000 enseignants et plus de 12 millions d'élèves. "Une affaire!" juge Brighelli.
