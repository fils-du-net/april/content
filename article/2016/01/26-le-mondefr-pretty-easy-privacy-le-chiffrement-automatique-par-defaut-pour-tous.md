---
site: Le Monde.fr
title: "«Pretty Easy Privacy», le chiffrement automatique par défaut pour tous"
author: Yves Eudes
date: 2016-01-26
href: http://www.lemonde.fr/pixels/article/2016/01/26/pretty-easy-privacy-le-chiffrement-automatique-par-defaut-pour-tous_4853782_4408996.html
tags:
- Entreprise
- Internet
- Innovation
- Vie privée
---

> Vingt-cinq ans après le PGP (Pretty Good Privacy), une équipe européenne invente le PEP, qui chiffrera tous les messages électroniques, automatiquement.
