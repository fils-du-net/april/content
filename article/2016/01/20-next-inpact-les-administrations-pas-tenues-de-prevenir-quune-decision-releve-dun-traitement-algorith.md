---
site: Next INpact
title: "Les administrations pas tenues de prévenir qu'une décision relève d'un traitement algorithmique"
author: Xavier Berne
date: 2016-01-20
href: http://www.nextinpact.com/news/98157-les-administrations-pas-tenues-prevenir-qu-une-decision-releve-d-un-traitement-algorithmique.htm
tags:
- Administration
- Institutions
- Open Data
- Vie privée
---

> Les députés ont rejeté hier soir un amendement qui aurait contraint les administrations à indiquer qu’une décision individuelle a été prise sur le fondement d’un traitement algorithmique. Le débat n'est toutefois pas complètement clos.
