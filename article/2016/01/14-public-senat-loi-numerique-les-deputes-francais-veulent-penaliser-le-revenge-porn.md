---
site: Public Sénat
title: "Loi numérique: les députés français veulent pénaliser le \"revenge porn\""
date: 2016-01-14
href: http://www.publicsenat.fr/lcp/politique/loi-numerique-deputes-francais-veulent-penaliser-revenge-porn-1198423
tags:
- Internet
- Administration
- Institutions
- Europe
---

> Les députés ont adopté jeudi, contre l'avis du gouvernement, un amendement au projet de loi "pour une République numérique", qui vise à pénaliser les...
