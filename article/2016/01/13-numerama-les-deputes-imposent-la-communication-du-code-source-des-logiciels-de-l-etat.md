---
site: Numerama
title: "Les députés imposent la communication du code source des logiciels de l’État"
author: Guillaume Champeau
date: 2016-01-13
href: http://www.numerama.com/politique/138542-les-deputes-imposent-la-communication-du-code-source-des-logiciels-de-letat.html
tags:
- Administration
- Institutions
---

> La commission des lois a adopté un amendement qui impose à l'État et aux collectivités territoriales de communiquer le code source des logiciels qui sont produits dans le cadre de services publics.
