---
site: La gazette.fr
title: "Loi numérique: quelles conséquences pour les collectivités territoriales?"
author: Sabine Blanc
date: 2016-01-22
href: http://www.lagazettedescommunes.com/425972/loi-numerique-et-ouverture-des-donnees-quelles-consequences-pour-les-collectivites-territoriales
tags:
- Administration
- Institutions
- Europe
- International
- Open Data
---

> Le texte porté par la secrétaire d’Etat en charge du Numérique a été examiné en séance en première lecture à l’Assemblée nationale du 19 au 21 janvier. La Gazette vous livre un compte-rendu dans le détail des articles qui concernent, de près ou de loin les collectivités. Voici déjà le point sur le Titre I, sur “la circulation des données et du savoir”, et le Titre II, sur La protection des droits dans la société numérique, en attendant le Titre III sur "l'accès au numérique", ce lundi.
