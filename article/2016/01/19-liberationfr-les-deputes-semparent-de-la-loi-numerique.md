---
site: Libération.fr
title: "Les députés s'emparent de la loi numérique"
author: Amaelle Guiton
date: 2016-01-19
href: http://www.liberation.fr/futurs/2016/01/19/les-deputes-s-emparent-de-la-loi-numerique_1427437
tags:
- Administration
- Institutions
- Open Data
---

> Le texte est examiné à partir de cet après-midi. De l'étendue de l'ouverture des données publiques à la «portabilité» des données privées en passant par les «biens communs numériques», les points de débat sont nombreux.
