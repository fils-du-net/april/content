---
site: Bloomberg
title: "Get Ready for Patent Wars as IP Europe Consortium Is Unveiled"
author: Marie Mawad
date: 2016-01-26
href: http://www.bloomberg.com/news/articles/2016-01-26/get-ready-for-patent-wars-as-ip-europe-consortium-is-unveiled
tags:
- Entreprise
- Brevets logiciels
- Innovation
---

> (Certains des plus gros producteurs de propriété intellectuelle forment une équipe afin de battre en faveur de règles qui les aideront à mieux monétiser l'innovation) Some of Europe’s biggest producers of intellectual property are teaming up to battle for rules that’ll help them cash in better on innovation. Telecommunications network builder Ericsson AB, plane maker Airbus Group SE, French phone company Orange SA and train maker Alstom SA are among companies behind IP Europe, a consortium due to be unveiled today in Brussels.
