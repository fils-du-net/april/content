---
site: Le Monde Informatique
title: "Google défend sa licence open source OpenJDK pour Android"
author: John Ribeiro
date: 2016-01-27
href: http://www.lemondeinformatique.fr/actualites/lire-google-defend-sa-licence-open-source-openjdk-pour-android-63727.html
tags:
- Entreprise
- Institutions
- Licenses
- Vie privée
---

> Le 24 décembre dernier, Google a discrètement sorti une version d'Android basée sur le code OpenJDK, c'est-à-dire l'implémentation open source de la plate-forme Java Standard Edition, Java SE. Mais pour Oracle, cela ne permet pas à tous les composants d'Android de bénéficier d'une licence open source. L'éditeur a porté l'affaire devant la justice.
