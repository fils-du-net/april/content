---
site: Le Point
title: "L'accord Microsoft-Éducation nationale fait (encore) des vagues"
date: 2016-01-29
href: http://www.lepoint.fr/high-tech-internet/l-accord-microsoft-education-nationale-fait-encore-des-vagues-29-01-2016-2013909_47.php
tags:
- Entreprise
- Institutions
- Associations
- Éducation
- Marchés publics
---

> Des entreprises jugent illégal ce partenariat signé fin novembre et qui n'a pas donné lieu à un appel d'offres. Elles veulent porter l'affaire en justice.
