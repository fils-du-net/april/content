---
site: L'OBS
title: "Le Parti pirate a dix ans, et plus beaucoup de dents"
author: Nicolas Falempin
date: 2016-01-18
href: http://rue89.nouvelobs.com/2016/01/18/parti-pirate-a-dix-ans-plus-beaucoup-dents-262874
tags:
- Internet
- April
- Institutions
- Associations
---

> Le Parti pirate a fêté ses dix ans. Notre blogueur Nicolas Falempin en a été le secrétaire national. Nous lui avons demandé de raconter son expérience et dix ans de l’histoire d’un parti qui, malgré quelques réussites, n’est pas parvenu à faire de la politique autrement.
