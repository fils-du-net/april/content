---
site: JDN
title: "L’open source joue aujourd’hui à armes égales avec les approches propriétaires"
author: Olivier Buisson
date: 2016-01-19
href: http://www.journaldunet.com/solutions/expert/63335/l-open-source-joue-aujourd-hui-a-armes-egales-avec-les-approches-proprietaires.shtml
tags:
- Entreprise
---

> L’open source a toujours été un formidable champ d’expérimentation et d’innovation. Le point sur ses principaux avantages pour l'entreprise en 2016.
