---
site: Le Telegramme
title: "Ils veulent «dégoogliser» Internet"
date: 2016-01-26
href: http://www.letelegramme.fr/finistere/brest/forum/ils-veulent-degoogliser-internet-26-01-2016-10934027.php
tags:
- Internet
- Associations
- Vie privée
---

> Big Brother se cache-t-il derrière les géants du web? C'est l'objet des rencontres organisées jusqu'à ce soir à Brest, avec le soutien de la Ville. Parmi les invités, l'association Framasoft, qui souhaite «dégoogliser» Internet.
