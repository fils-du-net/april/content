---
site: Numerama
title: "Recours et menace de plainte après l'accord entre Microsoft et l'Éducation nationale"
author: Guillaume Champeau
date: 2016-01-29
href: http://www.numerama.com/politique/141873-recours-et-menace-de-plainte-apres-laccord-entre-microsoft-et-leducation-nationale.html
tags:
- Entreprise
- Institutions
- Associations
- Éducation
- Marchés publics
---

> Un collectif de défense de l'utilisation des logiciels libres dans l'éducation a déposé un recours contre le partenariat signé fin 2015 entre Microsoft France et l'Éducation nationale. Ses avocats menacent de déposer plainte au pénal pour favoritisme contre la ministre Najat Vallaud-Belkacem si l'accord n'est pas annulé.
