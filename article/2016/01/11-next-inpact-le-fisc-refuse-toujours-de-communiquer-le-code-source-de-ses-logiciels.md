---
site: Next INpact
title: "Le fisc refuse toujours de communiquer le code source de ses logiciels"
author: Marc Rees
date: 2016-01-11
href: http://www.nextinpact.com/news/98024-le-fisc-refuse-toujours-communiquer-code-source-ses-logiciels.htm
tags:
- Administration
- April
- Institutions
---

> En mars 2015, la CADA considérait que le code source d’un logiciel était un document communicable. Où en est ce dossier? Purgé du projet de loi Lemaire, il prend aujourd’hui la direction du tribunal administratif.
