---
site: Next INpact
title: "À l'Assemblée nationale, l'utilisation des logiciels libres simplement «encouragée»"
author: Xavier Berne
date: 2016-01-13
href: http://www.nextinpact.com/news/98057-des-deputes-veulent-accorder-priorite-aux-logiciels-libres.htm
tags:
- Administration
- April
- Institutions
---

> Les administrations doivent-elles utiliser des logiciels libres plutôt que des solutions propriétaires, développées notamment par des géants comme Microsoft ? Telle est la question à laquelle devront répondre aujourd’hui les députés de la commission des lois de l’Assemblée nationale.
