---
site: L'OBS
title: "Les Suisses, eux, vont peut-être annuler leur loi sur le renseignement"
author: Gael Cerez
date: 2016-01-25
href: http://rue89.nouvelobs.com/2016/01/25/les-suisses-vont-peut-etre-annuler-loi-renseignement-262952
tags:
- Internet
- Institutions
- International
- Vie privée
---

> En septembre, la Suisse a adopté une loi similaire à celle votée en France sur le renseignement. Mais grâce au succès d’une pétition d’opposants, un référendum devra être organisé en juin.
