---
site: L'Informaticien
title: "Projet de loi République numérique: OS français et backdoor obligatoires dans le matériel?"
author: Emilien Ercolani
date: 2016-01-12
href: http://www.linformaticien.com/actualites/id/39142/projet-de-loi-republique-numerique-os-francais-et-backdoor-obligatoires-dans-le-materiel.aspx
tags:
- Administration
- April
- Institutions
- Vie privée
---

> Plusieurs amendements attirent l’attention ces jours-ci: le premier pour l’intégration de backdoor dans les matériels pour faciliter le travail de la police, et un autre qui demande la création d’un OS «souverain». Parallèlement, certaines pistes veulent encourager l’utilisation de logiciels libres.
