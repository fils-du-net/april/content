---
site: Silicon.fr
title: "La facture salée de Munich pour quitter Windows"
author: Jacques Cheminat
date: 2016-01-21
href: http://www.silicon.fr/linux-munich-une-facture-salee-pour-quitter-windows-xp-et-2000-136564.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Promotion
---

> Connue pour son engagement Open Source, Munich va migrer les derniers PC sous Windows XP et 2000. Avec une facture de plus de 11 000 euros par salarié.
