---
site: "usine-digitale.fr"
title: "\"La société de la surveillance est-elle notre projet de société?\" interroge Tristan Nitot"
author: Emmanuelle Delsol
date: 2016-01-12
href: http://www.usine-digitale.fr/editorial/la-societe-de-la-surveillance-est-elle-notre-projet-de-societe-interroge-tristan-nitot.N373082
tags:
- Internet
- Institutions
- Vie privée
---

> Membre du CNNum de 2013 à 2016, Tristan Nitot est l’un des fondateurs de Mozilla Europe. Il l’a quitté début 2015 pour rejoindre l’équipe dirigeante de la jeune pousse CozyCloud, qui propose un cloud personnel destiné à redonner à l’utilisateur le contrôle de ses données. Depuis toujours, Tristan Nitot milite sur le thème de la protection de la vie privée et des données en ligne.
