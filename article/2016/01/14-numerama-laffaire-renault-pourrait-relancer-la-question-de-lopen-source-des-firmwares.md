---
site: Numerama
title: "L'affaire Renault pourrait relancer la question de l'open-source des firmwares"
author: Julien Lausson
date: 2016-01-14
href: http://www.numerama.com/tech/138876-laffaire-renault-pourrait-relancer-la-question-de-lopen-source-des-firmwares.html
tags:
- Entreprise
- Institutions
- Droit d'auteur
---

> Le groupe Renault a chuté en bourse à la suite de révélations sur une perquisition menée par la DGCCRF, ravivant le spectre du scandale Volkswagen. Une affaire qui pourrait au passage relancer la question de l'open-source des firmwares.
