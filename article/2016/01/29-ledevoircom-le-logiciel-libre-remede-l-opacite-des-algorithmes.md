---
site: LeDevoir.com
title: "Le logiciel libre, remède à l’opacité des algorithmes"
author: Mathieu Gauthier-Pilote
date: 2016-01-29
href: http://www.ledevoir.com/societe/science-et-technologie/461474/le-logiciel-libre-remede-pratique-a-l-opacite-des-algorithmes
tags:
- Logiciels privateurs
- Partage du savoir
- Sensibilisation
- Innovation
---

> Dans un texte publié dans Le Devoir du 12 janvier («Uber et le nécessaire contrôle social des algorithmes»), le chercheur Yves Gingras soulevait la question fort pertinente de l’opacité des algorithmes utilisés par les services numériques auxquels nous sommes de plus en plus habitués dans notre société (par exemple Uber). Le sujet était à nouveau discuté à l’émission de radio Médium large le 14 janvier. Cet enjeu nous interpelle en tant que citoyens et militants de l’informatique libre.
