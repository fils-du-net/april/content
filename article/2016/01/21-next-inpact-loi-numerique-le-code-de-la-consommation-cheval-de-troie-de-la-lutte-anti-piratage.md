---
site: Next INpact
title: "Loi Numérique: le Code de la consommation, cheval de Troie de la lutte anti-piratage"
author: Marc Rees
date: 2016-01-21
href: http://www.nextinpact.com/news/98132-loi-numerique-consommateur-escabeau-antipiratage-deputes-socialistes.htm
tags:
- Internet
- Institutions
---

> La loi Lemaire sur le numérique débutera son examen parlementaire cet après-midi. Plus de 800 amendements sont sur le tremplin. L’un est à retenir particulièrement, pas seulement parce qu’il a été signé par l’ensemble du groupe socialiste.
