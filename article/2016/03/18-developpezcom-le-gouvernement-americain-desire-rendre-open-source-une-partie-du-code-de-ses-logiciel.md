---
site: Developpez.com
title: "Le gouvernement américain désire rendre open source une partie du code de ses logiciels développés et partager ces logiciels entre ses agences"
author: Olivier Famien
date: 2016-03-18
href: http://www.developpez.com/actu/97025/Le-gouvernement-americain-desire-rendre-open-source-une-partie-du-code-de-ses-logiciels-developpes-et-partager-ces-logiciels-entre-ses-agences
tags:
- Administration
- International
---

> Il est de notoriété publique que le partage du code des applications constitue un avantage intéressant, car il permet à une communauté plus importante d’évaluer ce code et de détecter des erreurs éventuelles. Par ailleurs, la disponibilité du code pourrait donner d’autres idées insoupçonnées afin d’améliorer son implémentation.
