---
site: Next INpact
title: "École numérique: une convention avec la CNIL, une charte pour les fournisseurs de services"
author: Xavier Berne
date: 2016-03-11
href: http://www.nextinpact.com/news/98997-ecole-numerique-convention-avec-cnil-charte-pour-fournisseurs-services.htm
tags:
- Institutions
- Éducation
- Vie privée
---

> Alors que le grand plan pour le numérique à l’école doit prendre son envol à partir de la rentrée prochaine, la ministre de l’Éducation nationale a signé hier une convention avec la présidente de la CNIL, prévoyant des «actions communes» au sujet de la protection des données personnelles. Les fournisseurs de services numériques devront par ailleurs s’engager à respecter une charte traitant de cette problématique.
