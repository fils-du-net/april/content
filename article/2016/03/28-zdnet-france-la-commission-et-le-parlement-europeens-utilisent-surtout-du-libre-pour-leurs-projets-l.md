---
site: ZDNet France
title: "La Commission et le Parlement européens utilisent surtout du Libre pour leurs projets logiciels"
author: Thierry Noisette
date: 2016-03-28
href: http://www.zdnet.fr/actualites/la-commission-et-le-parlement-europeens-utilisent-surtout-du-libre-pour-leurs-projets-logiciels-39834734.htm
tags:
- Administration
- Institutions
- Europe
---

> L'audit de projets logiciels mené par la direction informatique de la Commission doit assurer la fiabilité des composants open source et la contribution des institutions de l'UE.
