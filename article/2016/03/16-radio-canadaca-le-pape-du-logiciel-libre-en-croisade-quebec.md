---
site: "Radio-Canada.ca"
title: "Le pape du logiciel libre en croisade à Québec"
date: 2016-03-16
href: http://ici.radio-canada.ca/regions/quebec/2016/03/16/008-richard-stallman-logiciel-libre-universite-laval.shtml
tags:
- Logiciels privateurs
- Institutions
- Vie privée
---

> Le programmeur américain Richard Stallman, surnommé «le pape du logiciel libre», est de passage à l'Université Laval mercredi pour encourager la population à cesser d'utiliser les logiciels dits «privateurs», comme Facebook, iOS et Windows.
