---
site: "ouest-france.fr"
title: "Le logiciel libre, loin d'être game over!"
date: 2016-03-28
href: http://jactiv.ouest-france.fr/actualites/multimedia/logiciel-libre-loin-detre-game-over-61181
tags:
- April
- Sensibilisation
- Associations
---

> La grande fête du numérique s'est déroulée samedi, aux Ursulines, à Lannion. Son succès démontre l'intérêt grandissant d'un large public pour les technologies collaboratives.
