---
site: Silicon.fr
title: "Le code source des logiciels publics librement accessible"
author: Ariane Beky
date: 2016-03-25
href: http://www.silicon.fr/code-source-logiciels-etat-bercy-calculateur-143069.html
tags:
- Administration
- April
- Institutions
---

> L’administration fiscale s’apprête à ouvrir à tous le code de son calculateur d’impôts. En amont, la justice a confirmé que le code source d’un logiciel de l’administration est bien communicable.
