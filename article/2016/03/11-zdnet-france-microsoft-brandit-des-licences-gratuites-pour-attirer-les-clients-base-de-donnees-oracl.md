---
site: ZDNet France
title: "Microsoft brandit des \"licences gratuites\" pour attirer les clients base de données Oracle"
date: 2016-03-11
href: http://www.zdnet.fr/actualites/microsoft-brandit-des-licences-gratuites-pour-attirer-les-clients-base-de-donnees-oracle-39833938.htm
tags:
- Entreprise
- Licenses
---

> Microsoft s'efforce d'attirer les clients de la base de données entreprise Oracle au travers d'une nouvelle offre de migration vers SQL Server 2016.
