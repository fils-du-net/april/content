---
site: Next INpact
title: "Justice: le code source d’un logiciel, document administratif communicable au citoyen"
author: Xavier Berne
date: 2016-03-14
href: http://www.nextinpact.com/news/99038-la-justice-confirme-qu-un-code-source-logiciel-est-document-administratif-communicable-au-citoyen.htm
tags:
- Administration
- April
- Institutions
- Open Data
---

> Le code source d’un logiciel développé par les services de l’État est-il un «document administratif» comme un autre, dès lors communicable par principe au citoyen qui en fait la demande? Oui, vient de répondre le tribunal administratif de Paris. Explications.
