---
site: ZDNet France
title: "Richard Stallman: \"il faut éliminer Facebook qui entrave les libertés\""
date: 2016-03-15
href: http://www.zdnet.fr/actualites/richard-stallman-il-faut-eliminer-facebook-qui-entrave-les-libertes-39834112.htm
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Institutions
- Vie privée
---

> Nouvelle saillie du pape du logiciel libre contre le roi des réseaux sociaux, accusé de tuer la démocratie...
