---
site: La Tribune
title: "Les Chatons toutes griffes dehors face aux géants du Net"
author: Sylvain Rolland
date: 2016-03-17
href: http://www.latribune.fr/technos-medias/internet/les-chatons-toutes-griffes-dehors-face-aux-geants-du-net-557084.html
tags:
- Internet
- Associations
---

> Dans le sillage de sa campagne "Dégooglisons Internet", qui vise à lutter contre l’hégémonie des Gafam (Google, Apple, Facebook, Amazon, Microsoft), l’association Framasoft travaille à la création d’un collectif d’hébergeurs alternatifs, les "Chatons", dont l’objectif est de populariser les services éthiques auprès du grand public.
