---
site: Next INpact
title: "Priorité aux logiciels libres: l'April exhorte le gouvernement à la transparence"
author: Xavier Berne
date: 2016-03-14
href: http://www.nextinpact.com/news/99024-priorite-aux-logiciels-libres-l-april-exhorte-gouvernement-a-transparence.htm
tags:
- Administration
- April
- Institutions
- Marchés publics
- Open Data
---

> Alors que les débats sur le projet de loi Numérique doivent reprendre au Sénat à partir du 6 avril, l’Association de promotion du logiciel libre (April) vient de demander au gouvernement qu’il publie la note juridique justifiant son opposition à toute priorisation des logiciels libres au sein de l’administration.
