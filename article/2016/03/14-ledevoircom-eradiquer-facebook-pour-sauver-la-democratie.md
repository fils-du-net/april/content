---
site: LeDevoir.com
title: "Éradiquer Facebook pour sauver la démocratie"
author: Fabien Deglise
date: 2016-03-14
href: http://www.ledevoir.com/societe/actualites-en-societe/465389/eradiquer-facebook-pour-sauver-la-democratie
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Institutions
- Éducation
- Vie privée
---

> Richard Stallman, le père des logiciels libres, appelle les citoyens à reprendre le contrôle de leur vie.
