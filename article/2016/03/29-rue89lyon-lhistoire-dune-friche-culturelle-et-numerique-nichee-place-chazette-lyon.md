---
site: Rue89Lyon
title: "L'histoire d'une friche culturelle et numérique nichée place Chazette, à Lyon"
author: Victor Guilbert
date: 2016-03-29
href: http://www.rue89lyon.fr/2016/03/29/originale-friche-culturelle-numerique-nichee-place-chazette-lyon
tags:
- Partage du savoir
- Institutions
- Associations
- Innovation
---

> Qui devinerait, en passant devant les sages devantures du 7 place Chazette (1er arrondissement de Lyon), qu’ici répétait le groupe punk mythique Starshooter, braillant sans doute dans un local de l’arrière-cour sa chanson controversée «Get Baque»?
