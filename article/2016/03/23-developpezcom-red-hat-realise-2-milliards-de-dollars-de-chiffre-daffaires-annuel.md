---
site: Developpez.com
title: "Red Hat réalise 2 milliards de dollars de chiffre d'affaires annuel"
author: Michael Guilloux
date: 2016-03-23
href: http://www.developpez.com/actu/97167/Red-Hat-realise-2-milliards-de-dollars-de-chiffre-d-affaires-annuel-et-devient-la-premiere-societe-de-l-open-source-a-franchir-ce-cap
tags:
- Entreprise
- Économie
---

> Dans l’esprit de bien de gens, les entreprises de logiciels propriétaires sont animées par la recherche accrue de profits. Et le monde propriétaire s’opposerait donc à celui de l’open source, où les acteurs ont décidé de faire vœu de pauvreté pour les intérêts de la communauté, en se contentant de simples dons d’entreprises et particuliers qui reposent sur leurs technologies. Eh bien, c’est loin d’être la réalité! L’open source peut également être une source de revenus aussi importante que les logiciels propriétaires.
