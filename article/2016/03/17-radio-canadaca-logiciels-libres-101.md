---
site: "Radio-Canada.ca"
title: "Logiciels libres 101"
author: Catherine Mathys
date: 2016-03-17
href: http://blogues.radio-canada.ca/triplex/2016/03/17/catherine-mathys-119
tags:
- Sensibilisation
- Philosophie GNU
- Vie privée
---

> À l'occasion du passage de Richard Stallman au Québec, voici un petit retour sur la genèse du mouvement du logiciel libre.
