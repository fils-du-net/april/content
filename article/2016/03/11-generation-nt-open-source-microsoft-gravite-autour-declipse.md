---
site: "Génération-NT"
title: "Open Source: Microsoft gravite autour d'Eclipse"
author: Jérôme G.
date: 2016-03-11
href: http://www.generation-nt.com/open-source-microsoft-fondation-eclipse-visual-studio-actualite-1925880.html
tags:
- Entreprise
- Associations
---

> La fondation Eclipse compte dans ses rangs un nouveau venu qui est nul autre que Microsoft. La firme de Redmond poursuit son rapprochement auprès de la communauté open source.
