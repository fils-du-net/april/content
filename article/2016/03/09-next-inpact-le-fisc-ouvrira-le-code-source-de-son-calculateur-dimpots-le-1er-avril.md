---
site: Next INpact
title: "Le fisc ouvrira le code source de son calculateur d'impôts le 1er avril"
author: Xavier Berne
date: 2016-03-09
href: http://www.nextinpact.com/news/98981-le-fisc-ouvrira-code-source-son-calculateur-d-impots-1er-avril.htm
tags:
- Administration
- Économie
- Innovation
---

> Le citoyen qui réclamait depuis près de deux ans que le fisc lui communique le code source de son logiciel de calcul de l’impôt sur les revenus a finalement obtenu gain de cause devant la justice. Le ministère des Finances a décidé de mettre en ligne ce fichier à compter du 1er avril.
