---
site: Libération.fr
title: "«Geeks contre tsunami», quand la technologie répond aux catastrophes naturelles"
author: Camille Gévaudan
date: 2016-03-07
href: http://www.liberation.fr/futurs/2016/03/07/geeks-contre-tsunami-quand-la-technologie-repond-aux-catastrophes-naturelles_1437985
tags:
- Internet
- Associations
---

> Une équipe de «geeks citoyens» veut améliorer les infrastructures de télécommunication dans les Antilles françaises pour la simulation d'alerte au tsunami qui sera organisée en mars.
