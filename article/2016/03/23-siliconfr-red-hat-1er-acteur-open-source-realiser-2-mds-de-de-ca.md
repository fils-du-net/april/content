---
site: Silicon.fr
title: "Red Hat, 1er acteur Open Source à réaliser 2 Mds de $ de CA"
author: David Feugey
date: 2016-03-23
href: http://www.silicon.fr/red-hat-premier-acteur-open-source-a-realiser-2-milliards-de-de-ca-annuel-142676.html
tags:
- Entreprise
- Économie
---

> Gros carton pour Red Hat sur l’exercice 2015-2016, avec un CA de plus de 2 milliards de $ et un bénéfice net de près de 200 millions de $.
