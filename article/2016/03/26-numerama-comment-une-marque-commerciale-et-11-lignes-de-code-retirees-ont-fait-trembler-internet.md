---
site: Numerama
title: "Comment une marque commerciale et 11 lignes de code retirées ont fait trembler Internet"
author: Alexis Piraina
date: 2016-03-26
href: http://www.numerama.com/tech/154759-comment-une-marque-commerciale-et-11-lignes-de-code-retirees-ont-fait-trembler-internet.html
tags:
- Entreprise
- Droit d'auteur
---

> Un désaccord entre un développeur de solutions open-source et une application de messagerie a eu d'énormes répercussions sur de nombreux projets.
