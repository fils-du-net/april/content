---
site: Developpez.com
title: "Comment vous allez pouvoir avec Ordi-Linux reconditionner des ordinateurs!"
author: ideefixe
date: 2016-03-01
href: http://open-source.developpez.com/actu/96577/Comment-vous-allez-pouvoir-avec-Ordi-Linux-reconditionner-des-ordinateurs
tags:
- Économie
- Associations
- Innovation
---

> Ordi-Linux.org est un espace d'entraide pour les reconditionneurs d’ordinateurs sous Linux. Il présente la problématique de la gestion des DEEE et des solutions logistiques et logicielles, notamment celle du reconditionnement sous Linux. Une carte interactive est dédiée au référencement des reconditionneurs informatiques sous Linux. Les aspects économique, législatif, sont exposés, des définitions et des propositions sont développées.
