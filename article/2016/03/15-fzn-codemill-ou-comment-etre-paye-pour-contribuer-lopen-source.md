---
site: FZN
title: "CodeMill, ou comment être payé pour contribuer à l'open-source"
author: Jérémy Heleine
date: 2016-03-15
href: http://www.fredzone.org/codemill-ou-comment-etre-paye-pour-contribuer-a-lopen-source-636
tags:
- Internet
- Économie
---

> L’open-source a plein d’avantages et c’est pourquoi il est autant apprécié. Cependant, il reste toujours une question délicate dans le domaine du code ouvert: celle de la monétisation. Comment peut-on gagner de l’argent alors que le code est disponible à tous et qu’il est donc parfaitement inutile de chercher à le vendre?
