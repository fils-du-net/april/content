---
site: 01net.
title: "Microsoft arrête de basher Linux et intègre ses outils dans Windows 10"
author: Adrien Banco
date: 2016-03-31
href: http://www.01net.com/actualites/microsoft-arrete-de-basher-linux-et-integre-ses-outils-dans-windows-10-963387.html
tags:
- Entreprise
- Logiciels privateurs
---

> Après des années de guerre, Microsoft fait du pied aux développeurs en intégrant en natif l’interpréteur Bash dans son système d’exploitation Windows 10.
