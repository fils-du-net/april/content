---
site: Developpez.com
title: "Steve Ballmer aime Linux, l'ancien PDG de Microsoft change d'opinion sur Linux"
author: Michael Guilloux
date: 2016-03-11
href: http://www.developpez.com/actu/96831/Steve-Ballmer-aime-Linux-l-ancien-PDG-de-Microsoft-change-d-opinion-sur-Linux-quinze-ans-apres-l-avoir-traite-de-cancer
tags:
- Entreprise
- Licenses
---

> Jusqu’il y a quelques années, la bataille faisait encore rage entre le monde open source et le monde propriétaire. Cette bataille de deux univers à idéologies différentes mettait surtout en opposition les deux systèmes les plus populaires de ces deux mondes, à savoir Linux pour le monde open source, et Windows pour le monde propriétaire. Si Windows dominait (et domine toujours) largement le marché des PC, il fallait maintenir cette position alors que Linux commençait à gagner du terrain.
