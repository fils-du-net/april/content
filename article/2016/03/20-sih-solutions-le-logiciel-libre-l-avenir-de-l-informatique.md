---
site: SIH Solutions
title: "Le logiciel libre, l’avenir de l’informatique?"
author: Albert Phelipot
date: 2016-03-20
href: http://www.sih-solutions.fr/le-logiciel-libre-l-avenir-de-l-informatique
tags:
- Administration
- Interopérabilité
- Sensibilisation
- Innovation
---

> Etant, comme Obélix, tombé dans le chaudron au siècle dernier après un échange avec Richard Stallman, le logiciel libre est porteur de potentialités qui nous amènent à penser que le logiciel propriétaire a vécu son âge d’or et qu’il va progressivement céder sa place.
