---
site: ZDNet France
title: "Hackathon - L'administration vous prête son jouet"
author: Louis Adam
date: 2016-03-11
href: http://www.zdnet.fr/actualites/hackathon-l-administration-vous-prete-son-jouet-39833960.htm
tags:
- Administration
- Open Data
---

> La Direction Générale des finances publiques a accepté d’ouvrir le code source de son programme utilisé pour calculer le montant des impôts et en profite pour organiser un hackathon autour de celui-ci.
