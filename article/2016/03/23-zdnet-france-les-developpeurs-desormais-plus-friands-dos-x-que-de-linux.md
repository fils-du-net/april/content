---
site: ZDNet France
title: "Les développeurs désormais plus friands d'OS X que de Linux"
date: 2016-03-23
href: http://www.zdnet.fr/actualites/les-developpeurs-desormais-plus-friands-d-os-x-que-de-linux-39834586.htm
tags:
- Logiciels privateurs
- Innovation
---

> Il n'y a pas de dogme chez les développeurs: rien que du pragmatisme. Une étude portant sur plus de 56 000 développeurs fait le point sur les usages d'un public très courtisé quand il est compétent.
