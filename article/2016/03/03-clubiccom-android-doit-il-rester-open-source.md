---
site: clubic.com
title: "Android doit-il rester open source?"
author: Guillaume Belfiore
date: 2016-03-03
href: http://www.clubic.com/os-mobile/android/actualite-798066-systeme-android-deviendra-proprietaire.html
tags:
- Entreprise
- Logiciels privateurs
- Innovation
---

> La nature open source du système Android a longuement été remise en cause, mais l'OS de Google pourrait-il finir par devenir complètement propriétaire? C'est en tout cas la réflexion d'un analyste spécialiste des technologies mobiles.
