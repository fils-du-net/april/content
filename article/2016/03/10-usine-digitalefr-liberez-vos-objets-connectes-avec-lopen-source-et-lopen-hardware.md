---
site: "usine-digitale.fr"
title: "Libérez vos objets connectés avec l'open source et l'open hardware"
author: Julien Bergounhoux
date: 2016-03-10
href: http://www.usine-digitale.fr/article/liberez-vos-objets-connectes-avec-l-open-source-et-l-open-hardware.N382895
tags:
- Entreprise
- Matériel libre
- Innovation
---

> L’open source n’est pas nouveau dans l’embarqué, mais il prend son essor. Conséquence de l’apparition de nouveaux usages et de contraintes, objets connectés en tête. Preuve que ses avantages sont bien réels. Enquête.
