---
site: Numerama
title: "Où en est le Libre face aux tablettes, smartphones et hybrides?"
author: Corentin Durand
date: 2016-03-05
href: http://www.numerama.com/tech/150130-le-monde-du-libre-face-a-une-nouvelle-ere-technologique.html
tags:
- Entreprise
- Innovation
---

> Du rififi dans le monde du logiciel libre. Alors que l'informatique grand public glisse vers les écrans mobiles et des interfaces unifiées, adaptées à tous les écrans, le monde du Libre peine à émerger avec une solution à la fois efficace et satisfaisante. Après le quasi-abandon de Firefox OS, où est la communauté GNU/Linux?
