---
site: GreenIT.fr
title: "Doubler son espérance de vie grâce à Linux"
author: Frédéric Bordage
date: 2016-03-22
href: http://www.greenit.fr/article/bonnes-pratiques/doubler-son-esperance-de-vie-grace-a-linux-5624
tags:
- Économie
- Sensibilisation
- Innovation
---

> Le site GreenIT.fr en a fait son cheval de bataille et le rappelle régulièrement: c’est la fabrication des équipements qui concentre les impacts environnementaux, bien plus que leur utilisation. De plus, une fois jeté, l’ordinateur représente un déchet polluant. L’allongement de la durée de vie des ordinateurs est donc la meilleure démarche possible du green IT.
