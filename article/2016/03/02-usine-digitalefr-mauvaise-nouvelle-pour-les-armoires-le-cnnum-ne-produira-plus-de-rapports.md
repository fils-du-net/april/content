---
site: "usine-digitale.fr"
title: "Mauvaise nouvelle pour les armoires: le CNNum ne produira plus de rapports"
author: Christophe Bys
date: 2016-03-02
href: http://www.usine-digitale.fr/editorial/mauvaise-nouvelle-pour-les-armoires-le-cnnum-ne-produira-plus-de-rapports.N382403
tags:
- Institutions
- Innovation
---

> Peu à peu, Mounir Mahjoubi imprime sa marque au Conseil national du numérique (CNNum). Invité de La grande table sur France Culture, il a considéré qu'il était temps d'en finir avec les rapports épais très généraux, pour passer à des documents plus pratiques.  Il s'est aussi prononcé sur le débat qui oppose Apple et la justice américaine ou sur le logiciel libre, considérant que la France devrait avoir une stratégie nationale.
