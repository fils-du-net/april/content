---
site: "paris-normandie.fr"
title: "Enquête sur la face sombre d’internet: comment se protéger d’une cyberdélinquance envahissante"
author: Joce Hue
date: 2016-03-26
href: http://www.paris-normandie.fr/detail_article/articles/5387167/enquete-sur-la-face-sombre-d-internet--comment-se-proteger-d-une-cyberdelinquance-envahissante
tags:
- Internet
- Administration
- Open Data
- Vie privée
---

> Les cyberattaques ont augmenté de 50 % l’année dernière en France. Peut-on se créer une bulle de sécurité numérique ou sommes-nous tous des victimes potentiellesde la cyberdélinquance? Illustrations régionales.
