---
site: La Liberté
title: "Cyberbataille autour des libertés civiles"
author: Amaelle Guiton
date: 2016-03-04
href: http://www.laliberte.ch/news/cyberbataille-autour-des-libertes-civiles-338032
tags:
- Internet
- Innovation
---

> Face au contrôle de masse des citoyens sur le réseau, les «hacktivistes» d’Anonymous ou de WikiLeaks font figure de dernier rempart pour la défense de la vie privée. Les explications de l’anthropologue Gabriella Coleman.
