---
site: ZDNet France
title: "LeftPad: un module vous manque et tout est dépeuplé"
author: Louis Adam
date: 2016-03-24
href: http://www.zdnet.fr/actualites/leftpad-un-module-vous-manque-et-tout-est-depeuple-39834650.htm
tags:
- Entreprise
- Droit d'auteur
---

> Suite à un conflit autour d’une question de copyright, le développeur Azer Koculu a retiré l’ensemble de ses projets proposés sur la plateforme de partage de paquet JavaScript npm. Conséquence directe: la disparition d’un bout de code utilisé par plusieurs milliers de projets open source.
