---
site: Sud Ouest
title: "Le père du logiciel libre veut éradiquer Facebook... pour sauver la démocratie"
date: 2016-03-15
href: http://www.sudouest.fr/2016/03/15/le-pere-du-logiciel-libre-veut-sauver-la-democratie-en-eradiquant-facebook-2301919-4725.php
tags:
- Institutions
- Éducation
- Promotion
- Sciences
- Vie privée
---

> De passage au Québec, Richard Stallman n'y est pas allé avec le dos de la cuillère au moment d'évoquer le réseau social Facebook. Interrogé par le quotidien Le Devoir, le célèbre programmateur américain, créateur notamment du système d'exploitation GNU/Linux, a lancé: "Il faut éliminer Facebook pour protéger la vie privée".
