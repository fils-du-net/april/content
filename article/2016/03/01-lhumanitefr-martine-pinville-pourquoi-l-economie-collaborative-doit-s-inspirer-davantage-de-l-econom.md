---
site: l'Humanité.fr
title: "Martine Pinville: pourquoi l’économie collaborative doit s’inspirer davantage de l’économie sociale et solidaire"
author: Martine Pinville
date: 2016-03-01
href: http://www.humanite.fr/martine-pinville-pourquoi-leconomie-collaborative-doit-sinspirer-davantage-de-leconomie-sociale-et
tags:
- Économie
- Institutions
- Innovation
---

> Par Martine Pinville, secrétaire d'Etat chargée du commerce, de l'artisanat, de la consommation et de l'économie sociale et solidaire. Alors que beaucoup de citoyens s’interrogent légitimement sur l’impact de l’économie collaborative sur les relations commerciales, sur l’organisation du travail et sur la distribution de la valeur-ajoutée, il faut rappeler que l’économie sociale et solidaire apporte une réponse différente et innovante.
