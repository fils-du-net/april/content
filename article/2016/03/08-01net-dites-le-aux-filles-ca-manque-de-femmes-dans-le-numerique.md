---
site: 01net.
title: "Dites-le aux filles: ça manque de femmes dans le numérique!"
author: Delphine Sabattier
date: 2016-03-08
href: http://www.01net.com/actualites/dites-le-aux-filles-ca-manque-de-femmes-dans-le-numerique-957534.html
tags:
- Sciences
---

> Pour la Journée internationale de la femme, on s'est posé la question de la présence féminine dans le secteur des technologies. Tout avait si bien commencé...
