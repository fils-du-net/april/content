---
site: LExpansion.com
title: "Richard Stallman: \"Il faut éliminer Facebook pour protéger la vie privée!\""
author: lexpress-fr
date: 2016-03-15
href: http://lexpansion.lexpress.fr/high-tech/richard-stallman-il-faut-eliminer-facebook-pour-proteger-la-vie-privee_1773569.html
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> Le père des logiciels libres accuse le réseau social de Mark Zuckerberg de tuer la démocratie. Il propose purement et simplement son éradication.
