---
site: Mediapart
title: "Pourquoi les profs vont boycotter l'accord Microsoft-Éducation Nationale"
author: Bruno Menan
date: 2016-03-18
href: https://blogs.mediapart.fr/ilian-amar/blog/180316/pourquoi-les-profs-vont-boycotter-laccord-microsoft-education-nationale
tags:
- Entreprise
- Logiciels privateurs
- April
- Institutions
- Éducation
---

> Au-delà des raisons éthiques que nous avons déjà exposées, c'est pour de simples raisons de bons sens que les enseignants vont massivement refuser d'utiliser les "outils" mis gracieusement à leur disposition par la multinationale.
