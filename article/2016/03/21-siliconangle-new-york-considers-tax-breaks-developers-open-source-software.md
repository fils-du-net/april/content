---
site: SiliconANGLE
title: "New York considers tax breaks for developers of open-source software"
author: Mike Wheatley
date: 2016-03-21
href: http://siliconangle.com/blog/2016/03/21/new-york-considers-tax-breaks-for-developers-of-open-source-software
tags:
- Économie
- Institutions
- International
- English
---

> (L'assemblée de l'état de New York va étudier une nouvelle proposition qui pourrait fournir des réductions d'impôts aux dévelopeurs open source) New York state’s assembly is set to consider a new bill that would provide tax breaks for open-source software developers.
