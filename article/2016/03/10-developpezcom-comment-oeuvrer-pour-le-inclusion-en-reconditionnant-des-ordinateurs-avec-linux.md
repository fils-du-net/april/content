---
site: Developpez.com
title: "Comment œuvrer pour l'E-inclusion en reconditionnant des ordinateurs avec Linux"
author: ideefixe
date: 2016-03-10
href: http://open-source.developpez.com/actu/96803/Comment-oeuvrer-pour-l-E-inclusion-en-reconditionnant-des-ordinateurs-avec-Linux-des-associations-reconditionnent-des-ordinateurs
tags:
- Administration
- Associations
---

> Dans le Grand Ouest, des associations reconditionnent des ordinateurs avec des logiciels libres pour en faire don à leurs adhérents.
