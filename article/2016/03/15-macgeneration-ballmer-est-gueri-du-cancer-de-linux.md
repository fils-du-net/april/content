---
site: MacGeneration
title: "Ballmer est guéri du «cancer» de Linux"
author: Florian Innocente
date: 2016-03-15
href: http://www.macg.co/logiciels/2016/03/ballmer-est-gueri-du-cancer-de-linux-93396
tags:
- Entreprise
- Promotion
---

> Steve Ballmer n’a jamais été homme à mâcher ses mots ou à tempérer ses propos à l’encontre de ses concurrents. Un caractère qui a fait de lui un chef d’entreprise atypique: mélange de commercial, de technicien et de troubadour. En 2001, Linux avait été la cible de cette faconde lorsque le PDG l’avait qualifié de «cancer».
