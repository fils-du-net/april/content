---
site: ITespresso
title: "Interview vidéo Axelle Lemaire: l'impulsion numérique avant la fin du quinquennat"
author: Philippe Guerrier
date: 2016-03-15
href: http://www.itespresso.fr/interview-video-axelle-lemaire-impulsion-numerique-fin-quinquennat-123847.html
tags:
- Institutions
- Promotion
---

> Hackathon Le Meilleur Dev’ de France: Axelle Lemaire, Secrétaire d’Etat au Numérique, détaille ses chantiers prioritaires alors que l’échéance de l’élection présidentielle se rapproche.
