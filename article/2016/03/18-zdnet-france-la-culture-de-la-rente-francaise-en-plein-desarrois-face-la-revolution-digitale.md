---
site: ZDNet France
title: "La \"culture de la rente\" française en plein désarrois face à la révolution digitale"
author: Guillaume Serries
date: 2016-03-18
href: http://www.zdnet.fr/actualites/la-culture-de-la-rente-francaise-en-plein-desarrois-face-a-la-revolution-digitale-39834322.htm
tags:
- Entreprise
- Économie
- Innovation
---

> Basée sur des rentes parfois ancestrales, la croissances des entreprises françaises se heurte au mur de la transformation numérique qui fissure un modèle à bout de souffle, explique Nicolas Mariotte, sociologue de Capgemini consulting.
