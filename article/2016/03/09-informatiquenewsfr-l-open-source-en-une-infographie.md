---
site: InformatiqueNews.fr
title: "L’Open source en une infographie"
date: 2016-03-09
href: http://www.informatiquenews.fr/lopen-source-infographie-45863
tags:
- Entreprise
- Associations
- Éducation
---

> L'open source school présente une infographie qui donne les chiffres de l'Open Source et montre le dynamisme de ce secteur.
