---
site: EconomieMatin
title: "L'open source: un secteur qui manque cruellement de candidats"
author: Grégory Becue
date: 2016-03-12
href: http://www.economiematin.fr/news-open-source-secteur-candidats-opportunites
tags:
- Entreprise
- Économie
- Innovation
---

> L’open source est présent dans tous les secteurs d’activités et particulièrement dans les domaines les plus innovants de l’informatique : cloud, big data, mobilité et Internet des objets. Des centaines de millions d’utilisateurs ont recours à l’open source tous les jours partout dans le monde. Il s’agit d’un domaine en forte croissance pour les années à venir et un extraordinaire vivier d’emplois.
