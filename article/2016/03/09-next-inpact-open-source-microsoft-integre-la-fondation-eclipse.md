---
site: Next INpact
title: "Open source: Microsoft intègre la fondation Eclipse"
author: Vincent Hermann
date: 2016-03-09
href: http://www.nextinpact.com/news/98972-open-source-microsoft-integre-fondation-eclipse.htm
tags:
- Entreprise
- Associations
---

> Les démarches multiplateformes de Microsoft continuent avec l’intégration à la fondation Eclipse, qui pilote l’environnement de développement du même nom. L’éditeur en profite pour annoncer un certain nombre de nouveaux outils dédiés à ceux qui auraient besoin de faire le lien avec certains services de l’entreprise.
