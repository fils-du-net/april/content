---
site: Silicon.fr
title: "Retrait d’un module JavaScript: des milliers de projets bloqués"
author: Jacques Cheminat
date: 2016-03-24
href: http://www.silicon.fr/le-rififi-autour-dun-module-javascript-bloque-des-milliers-de-projets-142952.html
tags:
- Entreprise
- Droit d'auteur
---

> Pour une querelle sur le nom d’un module JavaScript, plusieurs milliers de projets se sont retrouvés bloqués.
