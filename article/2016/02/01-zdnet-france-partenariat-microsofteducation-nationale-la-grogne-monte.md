---
site: ZDNet France
title: "Partenariat Microsoft/Education nationale: la grogne monte"
author: Louis Adam
date: 2016-02-01
href: http://www.zdnet.fr/actualites/partenariat-microsoft-education-nationale-la-grogne-monte-39831982.htm
tags:
- Entreprise
- April
- Institutions
- Associations
- Éducation
- Marchés publics
---

> Plusieurs associations et entreprises issues du secteur du logiciel libre se sont rassemblées pour déposer un premier recours attaquant le partenariat passé entre Microsoft et l’Éducation nationale. Pour eux, celui-ci est en contradiction avec le code des marchés publics.
