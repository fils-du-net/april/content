---
site: Programmez!
title: "Les femmes coderaient-elles mieux que les hommes?"
author: fredericmazue
date: 2016-02-15
href: http://www.programmez.com/actualites/les-femmes-coderaient-elles-mieux-que-les-hommes-23920
tags:
- Internet
- Sciences
---

> Ceci selon une étude, nommée Gender Bias in Open Source : Pull Request Acceptance of Women Versus Men, et réalisée par des chercheurs des universités d'Etat de Californie et de Caroline du Nord. L'étude a été réalisée à partir de profils GitHub. Normalement une inscription sur GitHub ne nécessite pas de mentionner son sexe. Mais les chercheurs ont retrouvé cette information en faisant des recoupements, avec des profils Google+ notamment.
