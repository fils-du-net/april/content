---
site: Le nouvel Economiste
title: "'Hacker' oui, 'cracker', non"
author: Maija Palmer
date: 2016-02-09
href: http://www.lenouveleconomiste.fr/financial-times/hacker-oui-cracker-non-29720
tags:
- Économie
- Sensibilisation
- Innovation
---

> 'Hacker', un terme exaspérant et imprécis, trop souvent synonyme de menace.
