---
site: Libération
title: "Que reste-t-il des utopies du Net?"
author: Amaelle Guiton
date: 2016-02-09
href: http://www.liberation.fr/futurs/2016/02/09/que-reste-t-il-des-utopies-du-net_1431942
tags:
- Internet
- Institutions
---

> En 1996, l'essayiste américain John Perry Barlow publiait sa «Déclaration d’indépendance du cyberespace», devenue emblématique des cyberutopies libertaires. Vingt ans après, Etats et entreprises ont repris la main, mais l'imaginaire de réinvention sociale n'a pas disparu.
