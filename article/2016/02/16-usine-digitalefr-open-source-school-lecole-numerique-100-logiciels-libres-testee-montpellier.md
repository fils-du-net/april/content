---
site: "usine-digitale.fr"
title: "Open Source School, l'école numérique 100 % logiciels libres testée à Montpellier"
author: Sylvie Brouillet
date: 2016-02-16
href: http://www.usine-digitale.fr/article/open-source-school-l-ecole-numerique-100-logiciels-libres-testee-a-montpellier.N379955
tags:
- Entreprise
- Éducation
---

> Intégrateur de solutions open source Smile lance avec l’école privée d’ingénierie informatique Epsi l’Open Source School (OSS), un double programme de formation initiale et continue entièrement dédié aux logiciels libres. Objectif: réduire la pénurie de compétences - des milliers de postes restent vacants chaque année - dans un domaine en pleine croissance.
