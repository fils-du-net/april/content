---
site: rts.ch
title: "\"L'iPhone d'Apple est un ordinateur-prison\", selon le père du logiciel libre"
author: Delphine Gendre
date: 2016-02-02
href: http://www.rts.ch/info/sciences-tech/7463886--l-iphone-d-apple-est-un-ordinateur-prison-selon-le-pere-du-logiciel-libre.html
tags:
- Droit d'auteur
- Promotion
- Vie privée
---

> Lors de son passage à Fribourg lundi, le pape du logiciel libre Richard Stallman a accordé un entretien à la RTS. L'Américain de 63 ans a notamment souligné le côté intrusif des logiciels standards.
