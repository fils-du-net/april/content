---
site: ChannelNews
title: "Smile lance une école supérieure dédiée aux technologies open source"
author: Johann Armand
date: 2016-02-03
href: http://www.channelnews.fr/smile-lance-une-ecole-superieure-dediee-aux-technologies-open-source-59789
tags:
- Entreprise
- Institutions
- Éducation
- Promotion
---

> Le Groupe Smile et l’école d’ingénieurs EPSI annoncent l’ouverture d’Open Source School (OSS), une école d’enseignement supérieur dédiée aux technologies open source. Cette école accessible en formation initiale aux Bac+2 propose un cursus Bac+5 en alternance sanctionné par un titre classé niveau 1 au Répertoire National des Certifications Professionnelles (RNCP). Il sera dispensé dans six campus (à Bordeaux, Lille, Lyon, Montpellier, Nantes et Paris). 25% de l’enseignement d’OSS sera porté par l’EPSI et les 75% restants par les professionnels de l’open source.
