---
site: leJDD.fr
title: "CARTE. Comment Paris veut devenir une capitale des makers"
author: Bertrand Gréco
date: 2016-02-21
href: http://www.lejdd.fr/JDD-Paris/Le-mouvement-des-makers-a-l-assaut-de-la-capitale-774027
tags:
- Administration
- Économie
- Matériel libre
- Innovation
---

> La mairie s’apprête à dévoiler un plan pour développer massivement le do it yourself à l’ère du numérique. Et faire émerger une communauté parisienne de bricoleurs 2.0.
