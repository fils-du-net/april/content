---
site: Silicon.fr
title: "Google, Apple et Windows dans le viseur du Kremlin"
author: Jacques Cheminat
date: 2016-02-10
href: http://www.silicon.fr/google-apple-et-windows-dans-le-viseur-du-kremlin-138661.html
tags:
- Logiciels privateurs
- Administration
- Institutions
- International
---

> Le conseiller Internet de Vladimir Poutine veut que Google et Apple payent plus d’impôts en Russie. Il est aussi partisan d’interdire Windows.
