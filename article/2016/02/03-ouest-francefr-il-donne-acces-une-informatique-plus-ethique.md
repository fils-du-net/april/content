---
site: "ouest-france.fr"
title: "Il donne accès à une informatique plus éthique"
author: Anne-Emmanuelle Lambert
date: 2016-02-03
href: http://www.ouest-france.fr/normandie/alencon-61000/il-donne-acces-une-informatique-plus-ethique-4017462
tags:
- Associations
- Promotion
---

> Depuis septembre, Nicolas Barteau, un Alençonnais de 36 ans, est entrepreneur salarié de la coopérative d'activités et d'emplois Crescendo, basée à Flers. Son boulot: animateur numérique spécialisé dans les logiciels libres.
