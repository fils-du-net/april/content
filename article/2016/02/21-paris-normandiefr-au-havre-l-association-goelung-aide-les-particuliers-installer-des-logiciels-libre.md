---
site: "paris-normandie.fr"
title: "Au Havre, l’association Goelung aide les particuliers à installer des logiciels libres"
date: 2016-02-21
href: http://www.paris-normandie.fr/detail_communes/articles/5150523/l-association-havraise-goelug-cherche-a-presenter-une-autre-vision-du-numerique-et-aider-les-particuliers-a-s-y-retrouver-dans-ce-monde-virtuel
tags:
- Internet
- Associations
- Promotion
---

> Association. Goelug cherche à présenter une autre vision du numérique et aider les particuliers à s’y retrouver dans ce monde virtuel.
