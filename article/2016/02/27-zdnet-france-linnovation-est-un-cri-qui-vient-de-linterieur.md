---
site: ZDNet France
title: "L'innovation est un cri qui vient de l'intérieur"
author: Frédéric Charles
date: 2016-02-27
href: http://www.zdnet.fr/actualites/l-innovation-est-un-cri-qui-vient-de-l-interieur-39833370.htm
tags:
- Entreprise
- Innovation
---

> En Europe on reconnait la théorie de Darwin et le rôle que l'évolution a pu jouer pour que des espèces s'adaptent à un environnement en mutation. Mais comment les mutations se font dans les entreprises confrontées aux disruptions et qui sont les mutants?
