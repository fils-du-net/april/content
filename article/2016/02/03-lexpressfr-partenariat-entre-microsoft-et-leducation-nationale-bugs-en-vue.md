---
site: L'Express.fr
title: "Partenariat entre Microsoft et l'Education nationale: bugs en vue?"
author: Sandrine Chesnel
date: 2016-02-03
href: http://www.lexpress.fr/education/partenariat-entre-microsoft-et-l-education-nationale-bugs-en-vue_1759758.html
tags:
- Entreprise
- April
- Institutions
- Associations
- Éducation
- Marchés publics
---

> Un collectif d'associations et entreprises du numérique a déposé un recours auprès du ministère de l'Education nationale pour demander l'annulation d'un partenariat signé en novembre avec Microsoft. Un accord accusé d'illégalité.
