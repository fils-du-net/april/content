---
site: "ouest-france.fr"
title: "Multimédia. Concrètement, à quoi sert Linux?"
author: Jasmine Saunier
date: 2016-02-08
href: http://jactiv.ouest-france.fr/actualites/multimedia/multimedia-concretement-quoi-sert-linux-59351
tags:
- Sensibilisation
---

> C’est une sorte de «super-logiciel». Le système d’exploitation est le chef d’orchestre qui fait le lien entre votre machine et les logiciels. Il gère la mémoire de l’ordinateur et la répartit entre les programmes. Windows, Mac OS et Linux sont les systèmes d’exploitation les plus connus. Pratiquement tous les PC sont livrés avec Windows. Les Macintosh, eux, fonctionnent avec MacOS. Aucun ordinateur ne propose Linux dès l’origine, il faut donc l’installer. Mais ce n’est pas du tout incompatible avec Windows ou Mac OS.
