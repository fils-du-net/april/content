---
site: JDN
title: "L'Open Source, enfin source de croissance!"
author: Mathieu Le Faucheur
date: 2016-02-25
href: http://www.journaldunet.com/solutions/expert/63657/l-open-source--enfin-source-de-croissance.shtml
tags:
- Entreprise
- Économie
- Institutions
- Sensibilisation
---

> Le digital transforme aussi bien les entreprises, que la vie quotidienne. En 2012, les technologies numériques ont généré 3 169 milliards d’euros dans le monde, et en 2016, ce marché devrait atteindre 3 663 milliards d’euros, soit une hausse de 16%!
