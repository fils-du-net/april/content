---
site: Le Monde Informatique
title: "L'Epsi et Smile ouvrent la 1ère école dédiée à l'open source"
author: Véronique Véronique Arène
date: 2016-02-02
href: http://www.lemondeinformatique.fr/actualites/lire-l-epsi-et-smile-ouvrent-la-1ere-ecole-dediee-a-l-open-source-63781.html
tags:
- Entreprise
- Éducation
- Promotion
---

> Créée par le groupe Smile et l'école d'informatique Epsi, l'Open Source School est la première école française spécialisée dans l'apprentissage des technologies open source. La formation initiale dispensée à 100% en alternance, conduit à à l'obtention d'un diplôme inscrit au répertoire national des certifications professionnelles de niveau 1 (Bac+5).
