---
site: Developpez.com
title: "Libre en Fête 2016"
author: zoom61
date: 2016-02-12
href: http://www.developpez.com/actu/95968/Libre-en-Fete-2016-decouvrez-les-logiciels-libres-du-samedi-5-mars-au-dimanche-3-avril-2016-inclus
tags:
- Associations
- Promotion
---

> Comme tous les ans, l'initiative Libre en Fête se déroulera pour la quinzième année consécutive partout en France. Les dates qui ont été retenues sont du samedi 5 mars au dimanche 3 avril 2016 inclus.
