---
site: ZDNet France
title: "Le logiciel libre perd presque tous ses représentants au Conseil national du numérique"
author: Thierry Noisette
date: 2016-02-11
href: http://www.zdnet.fr/actualites/le-logiciel-libre-perd-presque-tous-ses-representants-au-conseil-national-du-numerique-39832658.htm
tags:
- HADOPI
- Institutions
---

> Le nouveau CNNum voit les représentants des logiciels libres passer de quatre à un, relève un des sortants, Tristan Nitot.
