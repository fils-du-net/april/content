---
site: Silicon.fr
title: "Le logiciel libre se rebiffe contre l’accord Microsoft-Éducation Nationale"
date: 2016-02-01
href: http://www.silicon.fr/le-logiciel-libre-se-rebiffe-contre-accord-microsoft-education-nationale-137502.html
tags:
- Entreprise
- April
- Institutions
- Associations
- Éducation
- Marchés publics
---

> Fort marri de l’accord entre Microsoft et l’Education Nationale, plusieurs associations du logiciel libre montent au créneau pour faire annuler ce partenariat.
