---
site: france bleu
title: "Rencontre avec un hacker: sur internet, basculer du côté obscur de la force, c'est facile"
author: Arnaud Racapé
date: 2016-02-09
href: https://www.francebleu.fr/infos/faits-divers-justice/sur-internet-basculer-du-cote-obscur-de-la-force-c-est-facile-le-temoignage-d-un-jeune-hacker-1455041793
tags:
- Internet
- Sensibilisation
- Associations
---

> Qui se cache derrière les fausses alertes à la bombe dans les lycées parisiens? L'enquête de la sûreté territoriale devra le déterminer. Le seul personnage connu à l'heure actuelle, c'est Vincent, le jeune côte-d'orien interpellé au domicile de son père lundi matin à Marsannay-le-Bois.
