---
site: La Libre.be
title: "Comment s'adapter aux structures collaboratives?"
date: 2016-02-28
href: http://www.lalibre.be/economie/libre-entreprise/comment-s-adapter-aux-structures-collaboratives-56cc703d35700f74a703e58d
tags:
- Sensibilisation
- Innovation
- ACTA
---

> Qu’ont donc en commun l’économie pair-à-pair, la permaculture, les imprimantes 3D, le logiciel libre et l’improvisation théâtrale? Tous illustrent la "transition collaborative".
