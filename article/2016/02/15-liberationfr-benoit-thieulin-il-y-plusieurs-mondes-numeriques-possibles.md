---
site: Libération.fr
title: "Benoît Thieulin: «Il y a plusieurs mondes numériques possibles»"
author: Amaelle Guiton
date: 2016-02-15
href: http://www.liberation.fr/futurs/2016/02/15/benoit-thieulin-il-y-a-plusieurs-mondes-numeriques-possibles_1433554
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- Promotion
---

> Mounir Mahjoubi vient de succéder à Benoît Thieulin à la tête du Conseil national du numérique. L’un dresse son bilan, l’autre annonce les chantiers à venir.
