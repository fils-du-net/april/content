---
site: AgoraVox
title: "Logiciels libres et open source (Culture libre)"
author: Ludo Nymous
date: 2016-02-09
href: http://www.agoravox.fr/actualites/technologies/article/logiciels-libres-et-open-source-177425
tags:
- Sensibilisation
- Philosophie GNU
---

> Vous avez sûrement déjà entendu parler de logiciels libres et d'open source. Mais qu'est ce que c'est en fait? Et quelles sont les différences entre les deux?
