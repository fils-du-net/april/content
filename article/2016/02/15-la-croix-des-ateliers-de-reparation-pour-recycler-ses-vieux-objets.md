---
site: la Croix
title: "Des ateliers de réparation pour recycler ses vieux objets"
author: Anne-Marie Kaiser
date: 2016-02-15
href: http://www.la-croix.com/Economie/Social/Des-ateliers-reparation-pour-recycler-vieux-objets-2016-02-15-1200740108
tags:
- Partage du savoir
- Associations
---

> Consommer autrement. Les ateliers de réparation de vieux objets se multiplient, à l’initiative de personnes prêtes à partager leur savoir-faire.
