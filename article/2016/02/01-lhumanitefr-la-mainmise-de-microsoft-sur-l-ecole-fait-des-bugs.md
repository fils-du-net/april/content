---
site: l'Humanité.fr
title: "La mainmise de Microsoft sur l’école fait des bugs"
author: Laurent Mouloud
date: 2016-02-01
href: http://www.humanite.fr/la-mainmise-de-microsoft-sur-lecole-fait-des-bugs-597587
tags:
- Entreprise
- Institutions
- Associations
- Éducation
- Marchés publics
---

> Le ministère a signé un partenariat avec le géant de l’informatique pour développer le plan numérique dans les classes. Les acteurs du logiciel libre réclament son annulation.
