---
site: "cio-online.com"
title: "Edito: l'abus de propriété intellectuelle nuit à la propriété intellectuelle"
author: Bertrand Lemaire
date: 2016-02-05
href: http://www.cio-online.com/actualites/lire-edito%A0-l-abus-de-propriete-intellectuelle-nuit-a-la-propriete-intellectuelle-8210.html
tags:
- Entreprise
- Logiciels privateurs
- Licenses
---

> Les conflits se multiplient entre éditeurs de logiciels et entreprises utilisatrices, notamment autour des audits de licences. Mais, en fait, il n'y a rien de neuf sous le soleil: la rapacité justifiée par la propriété intellectuelle veut réduire à néant une autre propriété intellectuelle, celle des clients utilisateurs.
