---
site: "Branchez-Vous!"
title: "Les femmes seraient meilleures que les hommes en programmation selon une étude"
author: Laurent LaSalle 
date: 2016-02-12
href: http://branchez-vous.com/2016/02/12/les-femmes-seraient-meilleures-que-les-hommes-en-programmation-selon-etude
tags:
- Internet
---

> Le code informatique écrit par des femmes serait mieux accepté par la communauté que celui écrit par des hommes, mais seulement lorsqu’il est impossible de déterminer le genre de l’auteur en question.
