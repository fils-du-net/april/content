---
site: ClubicPro
title: "Partenariat Microsoft - Education: le logiciel libre passe à l’attaque"
author: Olivier Robillart
date: 2016-02-01
href: http://pro.clubic.com/entreprises/microsoft/actualite-794120-microsoft-education-nationale-edunathon.html
tags:
- Entreprise
- Institutions
- Associations
- Éducation
- Marchés publics
---

> Plusieurs associations représentantes des logiciels libres dénoncent un partenariat signé entre Microsoft et l'Education nationale. Elles menacent de porter l'affaire devant la justice.
