---
site: RTBF Culture
title: "Logiciel libre: \"la propriété c'est le vol\""
author: Flora Eveno
date: 2016-02-02
href: https://www.rtbf.be/culture/dossier/chroniques-culture/detail_logiciel-libre-la-propriete-c-est-le-vol?id=9202867
tags:
- Philosophie GNU
- Promotion
---

> Dans un monde informatique parallèle à Adobe, des alternatives gratuites fourmillent depuis la nuit des temps, tapies dans l’ombre. C’est la communauté du logiciel libre.
