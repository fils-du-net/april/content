---
site: Next INpact
title: "L'État ouvre un forum sur l'utilisation des logiciels libres au sein de l'administration"
author: Xavier Berne
date: 2016-02-12
href: http://www.nextinpact.com/news/98520-l-etat-ouvre-forum-sur-l-utilisation-logiciels-libres-au-sein-l-administration.htm
tags:
- Internet
- Administration
- April
- Institutions
- Promotion
---

> La Direction interministérielle au numérique (DINSIC) vient d’ouvrir un forum dédié à l’utilisation et au support des logiciels libres au sein de l’administration. Déployé en catimini depuis plusieurs semaines, cet espace contributif est censé permettre à l’État et ses agents de mieux tirer profit de l’open source.
