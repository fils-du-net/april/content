---
site: Madame Figaro
title: "Non, les femmes ne codent pas mieux que les hommes!"
author: Mylène Bertaux
date: 2016-02-22
href: http://madame.lefigaro.fr/societe/les-femmes-codent-mieux-que-les-hommes-220216-112834
tags:
- Sensibilisation
---

> Elles sont encore largement minoritaires en matière de programmation numérique. Pourtant, une récente étude américaine, copieusement reprise dans les médias, montrerait que les programmeuses seraient meilleures que leurs homologues masculins tout en étant victimes de sexisme en ligne. Seulement voilà, l'interprétation des résultats ne fait pas l’unanimité chez les scientifiques.
