---
site: LeMonde.fr
title: "Le Parlement européen veut reprendre la main sur le TiSA, l’autre grand traité qui effraie"
author: Maxime Vaudano
date: 2016-02-04
href: http://transatlantique.blog.lemonde.fr/2016/02/04/le-parlement-europeen-veut-reprendre-la-main-sur-le-tisa-lautre-grand-traite-qui-effraie
tags:
- Entreprise
- Économie
- Institutions
- International
- ACTA
---

> Dans l’univers très anxiogène des grands accords commerciaux négociés derrière des portes closes, le traité transatlantique Tafta/TTIP en préparation entre l’Europe et les Etats-Unis occupe depuis bientôt trois ans le devant de la scène de notre côté de l’Atlantique – quand les Américains s’intéressent davantage au traité transpacifique, officiellement signé le 4 février 2016.
