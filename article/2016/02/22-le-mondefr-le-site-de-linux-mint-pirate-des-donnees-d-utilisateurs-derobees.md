---
site: Le Monde.fr
title: "Le site de Linux Mint piraté, des données d’utilisateurs dérobées"
date: 2016-02-22
href: http://www.lemonde.fr/pixels/article/2016/02/22/le-site-de-linux-mint-pirate-des-donnees-d-utilisateurs-derobees_4869699_4408996.html
tags:
- Internet
- Économie
---

> C’est l’une des versions les plus utilisées du système d’exploitation Linux, et elle a été victime ce week-end d’un important piratage. Samedi 21 février, les personnes ayant téléchargé le système d’exploitation Linux Mint ont en fait obtenu une version modifiée par un pirate informatique, peut-on lire sur le blog officiel du projet. Il a installé dans le logiciel une «porte dérobée» (backdoor), une faille permettant à un tiers de s’introduire dans le système.
