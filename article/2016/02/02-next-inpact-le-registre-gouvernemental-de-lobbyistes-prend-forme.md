---
site: Next INpact
title: "Le registre gouvernemental de lobbyistes prend forme"
author: Xavier Berne
date: 2016-02-02
href: http://www.nextinpact.com/news/98331-le-registre-gouvernemental-lobbyistes-prend-forme.htm
tags:
- Entreprise
- HADOPI
- Institutions
- Open Data
---

> Annoncé il y a plus d’un an par François Hollande, le registre gouvernemental de lobbyistes devrait prendre forme dans le projet de loi qui sera porté dans quelques semaines devant le Parlement par Michel Sapin. Selon une première version du texte consultée par Mediapart, le dispositif serait cependant largement perfectible.
