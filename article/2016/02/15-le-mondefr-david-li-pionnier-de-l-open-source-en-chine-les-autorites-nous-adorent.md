---
site: Le Monde.fr
title: "David Li, pionnier de l’open-source en Chine: «Les autorités nous adorent!»"
author: Morgane Tual
date: 2016-02-15
href: http://www.lemonde.fr/pixels/article/2016/02/15/david-li-pionnier-de-l-open-source-en-chine-les-autorites-nous-adorent_4865535_4408996.html
tags:
- Matériel libre
- Institutions
- Innovation
- International
---

> Chantre du matériel libre, David Li a été le premier à ouvrir un «makerspace». Un mouvement qui a depuis essaimé en Chine.
