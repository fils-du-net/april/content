---
site: RTBF Culture
title: "Les Creative Commons profitent à tous"
author: Flora Eveno
date: 2016-02-11
href: http://www.rtbf.be/culture/dossier/chroniques-culture/detail_les-creative-commons-profitent-a-tous?id=9210802
tags:
- Internet
- Partage du savoir
- Contenus libres
---

> Les Creative Commons sont des licences de protection des œuvres en ligne, une protection qui est bénéfique tant à l’auteur qu’à l’utilisateur. Les CC ont été imaginés dans le pur esprit utilitariste du web.
