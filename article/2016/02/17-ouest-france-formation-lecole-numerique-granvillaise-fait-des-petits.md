---
site: "ouest-france"
title: "Formation. L'école numérique granvillaise fait des petits"
date: 2016-02-17
href: http://www.entreprises.ouest-france.fr/article/formation-lecole-numerique-granvillaise-fait-petits-17-02-2016-255273
tags:
- Entreprise
- Institutions
- Associations
- Éducation
---

> Après Granville, la Manche open school va ouvrir une formation à Saint-Lô en mars. À l'origine, deux trentenaires associés, atypiques et soutenus par les décideurs.
