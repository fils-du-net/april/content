---
site: ZDNet France
title: "Lancement de l'Open Source School"
author: Thierry Noisette
date: 2016-02-03
href: http://www.zdnet.fr/actualites/lancement-de-l-open-source-school-ecole-superieure-pour-le-logiciel-libre-soutenue-par-le-cnll-39832062.htm
tags:
- Entreprise
- Éducation
---

> Smile et l'EPSI lancent l'OSS, école d'enseignement supérieur et de formation continue dans l'open source, avec une aide d'Etat de 1,4 million comme investissement d'avenir.
