---
site: "ouest-france.fr"
title: "Microsoft - Education Nationale. La ministre menacée d’une plainte"
author: Serge Poirot
date: 2016-02-02
href: http://www.ouest-france.fr/societe/microsoft-education-nationale-la-ministre-menacee-dune-plainte-4010524
tags:
- Entreprise
- Institutions
- Associations
- Éducation
- Marchés publics
- Promotion
---

> Signé fin novembre, le partenariat qui ouvre au géant américain les portes des établissements scolaires suscite un tollé. Un collectif dénonce une opération illégale.
