---
site: La gazette.fr
title: "Vers des assemblées des communs dans plusieurs villes"
author: Stéphanie Stoll
date: 2016-02-26
href: http://www.lagazettedescommunes.com/431244/vers-des-assemblee-des-communs-dans-plusieurs-villes
tags:
- Administration
- Économie
- Institutions
- Associations
---

> Les communs n’ont pas trouvé place dans la loi numérique portée par Axelle Lemaire, pour le moment. Un revers qui n’empêche pas les initiatives dans les territoires. Les «assemblées des communs» élargissent au monde social un concept médiéval remis au goût du jour dans le monde numérique. Exemple à Brest.
