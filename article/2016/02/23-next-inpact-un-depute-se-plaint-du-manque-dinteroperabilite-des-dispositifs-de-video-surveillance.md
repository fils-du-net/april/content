---
site: Next INpact
title: "Un député se plaint du manque d'interopérabilité des dispositifs de vidéo-surveillance"
author: Xavier Berne
date: 2016-02-23
href: http://www.nextinpact.com/news/98711-un-depute-se-plaint-manque-dinteroperabilite-dispositifs-video-surveillance.htm
tags:
- Administration
- Interopérabilité
- Institutions
- Marchés publics
- Vie privée
---

> Dénonçant l’incompatibilité entre ordinateurs des forces de l'ordre et certains dispositifs de vidéo-surveillance, un député propose au gouvernement d’imposer aux fabricants plus d’interopérabilité lors des passations de marchés publics.
