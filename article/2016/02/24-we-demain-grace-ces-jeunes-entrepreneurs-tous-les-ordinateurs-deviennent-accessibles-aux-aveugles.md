---
site: We Demain
title: "Grâce à ces jeunes entrepreneurs, tous les ordinateurs deviennent accessibles aux aveugles"
author: Juliette Mauban
date: 2016-02-24
href: http://www.wedemain.fr/Grace-a-ces-jeunes-entrepreneurs-tous-les-ordinateurs-deviennent-accessibles-aux-aveugles_a1670.html
tags:
- Entreprise
- Innovation
---

> Avec des logiciels de reconnaissance vocale qui tiennent dans une clé USB, les deux jeunes fondateurs de Hypra veulent ouvrir l'informatique à tous les déficients visuel.
