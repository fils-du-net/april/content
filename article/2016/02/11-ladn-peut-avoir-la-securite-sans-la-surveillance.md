---
site: L'ADN
title: "Peut-on avoir la sécurité sans la surveillance?"
author: Béatrice Sutter
date: 2016-02-11
href: http://www.ladn.eu/actualites/peut-on-avoir-securite-sans-surveillance,article,30069.html
tags:
- Internet
- Institutions
- Innovation
- Informatique en nuage
- Vie privée
---

> À la tête du mouvement pour le logiciel libre, Richard Stallman est en guerre contre les abus dont se rendent responsables entreprises et États. Il en appelle à un projet de cloud computing alternatif. Interview.
