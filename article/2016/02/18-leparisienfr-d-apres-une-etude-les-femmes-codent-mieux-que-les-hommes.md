---
site: leParisien.fr
title: "D’après une étude, les femmes codent mieux que les hommes"
author: Sabrina Alili
date: 2016-02-18
href: http://www.leparisien.fr/economie/d-apres-une-etude-les-femmes-codent-mieux-que-les-hommes-17-02-2016-5554561.php
tags:
- Sciences
---

> Réalisée aux États-Unis et révélée dans le journal PeerJ, une étude démontre que les femmes sont meilleures pour écrire du code par rapport à leurs homologues masculins mais souffrent encore de sexisme.
