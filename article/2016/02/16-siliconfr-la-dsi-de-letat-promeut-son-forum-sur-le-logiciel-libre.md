---
site: Silicon.fr
title: "La DSI de l'État promeut son forum sur le logiciel libre"
author: Ariane Beky
date: 2016-02-16
href: http://www.silicon.fr/dinsic-dsi-etat-forum-discussion-logiciel-libre-139110.html
tags:
- Internet
- Administration
- Institutions
- Marchés publics
- Promotion
---

> Dialoguer avec l’écosystème tout en renforçant la capacité d’agir des utilisateurs et contributeurs du libre dans l’administration, c’est l’objectif du forum de discussion ouvert par la Dinsic.
