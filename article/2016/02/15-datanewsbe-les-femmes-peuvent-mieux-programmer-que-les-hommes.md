---
site: Datanews.be
title: "\"Les femmes peuvent mieux programmer que les hommes\""
date: 2016-02-15
href: http://datanews.levif.be/ict/actualite/les-femmes-peuvent-mieux-programmer-que-les-hommes/article-normal-467779.html
tags:
- Internet
- Sciences
---

> Le code écrit par une femme est plus souvent approuvé par des co-programmeurs que celui écrit par un homme, mais ce n'est le cas que si le reste ne sait pas que le code a été écrit par une femme.
