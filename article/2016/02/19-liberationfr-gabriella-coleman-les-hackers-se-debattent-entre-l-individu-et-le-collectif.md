---
site: Libération.fr
title: "Gabriella Coleman: «Les hackers se débattent entre l’individu et le collectif»"
author: Amaelle Guiton
date: 2016-02-19
href: http://www.liberation.fr/debats/2016/02/19/gabriella-coleman-les-hackers-se-debattent-entre-l-individu-et-le-collectif_1434607
tags:
- Internet
- Institutions
- Brevets logiciels
- Vie privée
---

> De WikiLeaks aux Anonymous, l’anthropologue américaine dépeint l’évolution des cybermilitants qui, venus d’horizons idéologiques très divers, mettent leur compétence technique au service de «la bataille pour les libertés civiles».
