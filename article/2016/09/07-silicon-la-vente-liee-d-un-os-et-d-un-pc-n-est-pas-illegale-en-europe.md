---
site: Silicon
title: "La vente liée d’un OS et d’un PC n’est pas illégale en Europe"
author: Reynald Fléchaux
date: 2016-09-07
href: http://www.silicon.fr/vente-liee-os-pc-pas-illegale-europe-156909.html
tags:
- Institutions
- Vente liée
- Europe
---

> La CJUE tranche un conflit opposant un consommateur à Sony dans la vente liée d’un PC et de Windows. Et valide cette pratique.
