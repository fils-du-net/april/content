---
site: Sciences et avenir
title: "Bruxelles propose de réformer le très sensible droit d'auteur"
date: 2016-09-16
href: http://www.sciencesetavenir.fr/high-tech/20160915.OBS8090/bruxelles-propose-de-reformer-le-tres-sensible-droit-d-auteur.html
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Droit d'auteur
- Europe
---

> La Commission européenne a présenté mercredi une ambitieuse réforme du droit d'auteur, qui mobilise une partie des artistes et professionnels de l'audiovisuel de l'UE, inquiets du financement futur de leurs créations.
