---
site: Next INpact
title: "François Hollande promeut le partage des «biens communs numériques»"
author: Xavier Berne
date: 2016-09-21
href: http://www.nextinpact.com/news/101475-francois-hollande-promeut-partage-biens-communs-numeriques.htm
tags:
- Partage du savoir
- Institutions
- Open Data
---

> Alors que la France s’apprête à prendre la présidence du Partenariat pour un gouvernement ouvert, qui réuni en son sein 70 pays, François Hollande a présenté hier les axes forts de son mandat. Le chef de l’État a tout particulièrement prôné le partage des «biens communs numériques».
