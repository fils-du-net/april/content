---
site: Next INpact
title: "Le projet de loi Numérique définitivement adopté par le Parlement"
author: Xavier Berne
date: 2016-09-28
href: http://www.nextinpact.com/news/101567-le-projet-loi-numerique-definitivement-adopte-par-parlement.htm
tags:
- Internet
- Administration
- Institutions
- Neutralité du Net
- Open Data
- Vie privée
---

> Après des années d’annonces et de débats, le projet de loi Numérique a été définitivement adopté cet après-midi par le Sénat. Il ne reste désormais plus qu’à attendre sa promulgation par François Hollande.
