---
site: Archimag
title: "Libre@Toi: bientôt une radio libre dédiée aux ”communs” sur la bande FM?"
author: Clémence Jost
date: 2016-09-16
href: http://www.archimag.com/bibliotheque-edition/2016/09/16/libre-toi-radio-libre-communs-bande-fm
tags:
- Partage du savoir
- Sensibilisation
- Associations
---

> L'association Libre@Toi candidate auprès du CSA pour l'obtention de la fréquence FM 93.1. Son objectif: lancer la première radio dédiée aux communs, rassemblant des acteurs du logiciel libre, du numérique, de la recherche, de l'éducation, du monde associatif et du monde du travail.
