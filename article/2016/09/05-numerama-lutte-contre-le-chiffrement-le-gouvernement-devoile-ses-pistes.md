---
site: Numerama
title: "Lutte contre le chiffrement: le gouvernement dévoile ses pistes"
author: Guillaume Champeau
date: 2016-09-05
href: http://www.numerama.com/politique/192473-lutte-contre-chiffrement-gouvernement-devoile-pistes.html
tags:
- Internet
- Institutions
- Vie privée
---

> Le gouvernement en dit davantage sur la manière dont il espère pouvoir lutter contre le recours au chiffrement de bout en bout, qui rend l'interception des messages quasi impossible pour les services de renseignement ou de police judiciaire. Deux options à l'étude: interdiction, ou backdoor.
