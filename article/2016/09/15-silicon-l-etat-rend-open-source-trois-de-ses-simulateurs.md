---
site: Silicon
title: "L'État rend Open Source trois de ses simulateurs"
author: Ariane Beky
date: 2016-09-15
href: http://www.silicon.fr/etat-open-source-trois-simulateurs-codegouv-157607.html
tags:
- Administration
- Open Data
---

> Un hackathon va accompagner l'ouverture des codes sources des simulateurs carte grise, gratification des stagiaires, intérêts moratoires.
