---
site: Métro
title: "Libérez-nous des logiciels"
author: Miriam Fahmy
date: 2016-09-19
href: http://journalmetro.com/opinions/miriam-fahmy/1024143/liberez-nous-des-logiciels
tags:
- Brevets logiciels
- Neutralité du Net
- Promotion
- International
- ACTA
---

> Le contraste était saisissant: d’un côté de la scène, Richard Stallman, le «pape» du logiciel libre, créateur du logiciel GNU, Américain parfaitement francophone, au charisme assumé, dégageant une forte aura de gourou, et de l’autre, la chercheuse québécoise Marianne Corvellec, jeune femme articulée et engagée, armée de l’assurance qu’on acquiert quand on connaît un sujet comme le fond de sa poche.
