---
site: Le Monde.fr
title: "Admission post-bac: le ministère poussé à dévoiler l’algorithme qui oriente les lycéens"
author: Séverin Graveleau
date: 2016-09-19
href: http://www.lemonde.fr/campus/article/2016/09/19/admission-post-bac-le-ministere-somme-d-apporter-transparence-et-legalite-a-la-plateforme_4999798_4401467.html
tags:
- Administration
- Éducation
---

> Qu’y a-t-il sous le capot de la «machine» Admission post-bac (APB), qui permet d’effectuer ses vœux d’inscription dans l’enseignement supérieur? Des irrégularités juridiques et une opacité de fonctionnement que le ministère va devoir lever rapidement au vu des décisions juridiques et administratives intervenues depuis le mois de juin.
