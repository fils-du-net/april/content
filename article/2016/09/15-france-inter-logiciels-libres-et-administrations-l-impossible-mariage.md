---
site: France Inter
title: "Logiciels libres et administrations: l'impossible mariage?"
author: Camille Magnard
date: 2016-09-15
href: https://www.franceinter.fr/info/la-bataille-des-logiciels-libres-reste-ca-mener-dans-l-administration
tags:
- Administration
- April
- Institutions
- Éducation
- Marchés publics
- Promotion
- International
---

> L'administration réaffirme régulièrement sa volonté d'utiliser les logiciels libres. Mais la mainmise des géants de l'informatique reste difficile à contourner.
