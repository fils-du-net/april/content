---
site: EconomieMatin
title: "La CJUE a décidé que Windows continuera de squatter vos PC neufs"
author: Paolo Garoscio
date: 2016-09-08
href: http://www.economiematin.fr/news-windows-ordinateur-cjue-installation-os-vente-marche-choix-linux
tags:
- Entreprise
- Institutions
- Vente liée
- International
---

> Bill Gates et Satya Nadella peuvent ouvrir le champagne: la CJUE (Cour de Justice de l'Union Européenne) leur a donné raison. A eux et à Sony qui était au centre de cette affaire un peu particulière. En l'occurrence, un acheteur d'un ordinateur Sony où était préinstallé l'OS Windows voulait se faire rembourser du prix de ce dernier car il n'avait pas eu le choix de l'OS lors de l'achat. Sony et Windows ont gagné, le consommateur partira bredouille.
