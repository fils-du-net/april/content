---
site: Numerama
title: "La Russie reste déterminée à se passer de Windows"
author: Julien Lausson
date: 2016-09-28
href: http://www.numerama.com/politique/197616-la-russie-reste-determinee-a-se-passer-de-windows.html
tags:
- Administration
- Institutions
- International
---

> Les autorités russes maintiennent leur plan consistant à remplacer tous les logiciels américains par des solutions locales ou, à défaut, open source pour en contrôler le code.
