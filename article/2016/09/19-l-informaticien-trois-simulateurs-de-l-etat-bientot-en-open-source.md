---
site: L'Informaticien
title: "Trois simulateurs de l’État bientôt en open source"
author: Guillaume Périssat
date: 2016-09-19
href: http://www.linformaticien.com/actualites/id/41707/trois-simulateurs-de-l-etat-bientot-en-open-source.aspx
tags:
- Administration
- Partage du savoir
---

> Gratification des stagiaires, immatriculations des véhicules, intérêts moratoires: trois nouvelles applications de l’administration rejoindront le 13 octobre le simulateur de calcul d’impôts.
