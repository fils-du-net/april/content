---
site: Commerce Monde
title: "Semaine québécoise de l'informatique libre 2016"
author: David Côté-Tremblay
date: 2016-09-06
href: https://www.commercemonde.com/2016/09/communaute-du-logiciel-libre
tags:
- Économie
- Associations
- Promotion
- International
---

> nuxDepuis 2009, l’organisme FACIL, avec l’appui de nombreux organismes de la communauté du logiciel libre au Québec, organise presque à chaque années l’événement SQIL (Semaine québécoise de l’informatique libre). Il s’agit d’une série d’activités ayant pour objectif de promouvoir l’utilisation du logiciel libre et des données ouvertes. Il y en a pour tous les niveaux: pour les débutants, comme les plus expérimentés.
