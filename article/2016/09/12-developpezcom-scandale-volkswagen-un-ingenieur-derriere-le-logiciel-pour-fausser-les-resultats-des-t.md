---
site: Developpez.com
title: "Scandale Volkswagen: un ingénieur derrière le logiciel pour fausser les résultats des tests antipollution plaide coupable et décide de coopérer"
author: Stéphane le calme
date: 2016-09-12
href: http://www.developpez.com/actu/103773/Scandale-Volkswagen-un-ingenieur-derriere-le-logiciel-pour-fausser-les-resultats-des-tests-antipollution-plaide-coupable-et-decide-de-cooperer
tags:
- Entreprise
- Institutions
- Standards
- International
---

> Vendredi dernier, un nouvel élément dans l’affaire de la fraude du test d’émission de gaz qui a frappé le constructeur automobile Volkswagen a fait surface : un ingénieur de l’entreprise a plaidé coupable dans son rôle pour tromper le gouvernement américain ainsi que les consommateurs pendant près d’une décennie sur la quantité réelle d’émission de gaz de certains modèles. James Robert Liang, l’homme de 62 ans qui a admis avoir développé un logiciel qui a permis de fausser les résultats des tests, a décidé de coopérer avec la justice.
