---
site: L'Informaticien
title: "Une plateforme libre et ouverte pour les consultations en ligne de l’État"
author: Guillaume Périssat
date: 2016-09-20
href: http://www.linformaticien.com/actualites/id/41726/une-plateforme-libre-et-ouverte-pour-les-consultations-en-ligne-de-l-etat.aspx
tags:
- Internet
- Institutions
---

> Comment reproduire l’expérience de la consultation en ligne du projet de loi République Numérique? Avec une plateforme basée sur un logiciel ouvert et libre, développé à coups de hackathons et d’ateliers, pardi!
