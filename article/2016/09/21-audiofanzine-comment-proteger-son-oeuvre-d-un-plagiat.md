---
site: Audiofanzine
title: "Comment protéger son œuvre d’un plagiat"
author: Doktor Sven
date: 2016-09-21
href: http://fr.audiofanzine.com/autoproduction-business/editorial/dossiers/droit-et-diffusion.html
tags:
- Internet
- Partage du savoir
- Droit d'auteur
- Licenses
---

> Après des heures et des heures de durs labeurs, seul ou à plusieurs, vous avez enfin finalisé votre première compo. L’ivresse de la création vous monte à la tête: ça y est, vous dites-vous, je suis un artiste, je suis un Créateur!
