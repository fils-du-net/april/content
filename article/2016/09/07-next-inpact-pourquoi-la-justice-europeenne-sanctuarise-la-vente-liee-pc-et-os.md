---
site: Next INpact
title: "Pourquoi la justice européenne a sanctuarisé la vente liée PC et OS"
author: Marc Rees
date: 2016-09-07
href: http://www.nextinpact.com/news/101268-la-justice-europeenne-sanctuarise-vente-liee-pc-et-os.htm
tags:
- Institutions
- Vente liée
- Europe
---

> La Cour de justice de l’Union européenne vient de considérer qu’une vente liée PC et OS n’est pas en soi une pratique commerciale déloyale, du moins sous certaines conditions. Elle estime par ailleurs que le prix des éléments de cette offre n’a pas à être ventilé.
