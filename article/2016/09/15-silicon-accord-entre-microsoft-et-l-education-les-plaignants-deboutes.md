---
site: Silicon
title: "Accord entre Microsoft et l’Éducation: les plaignants déboutés"
author: Ariane Beky
date: 2016-09-15
href: http://www.silicon.fr/accord-microsoft-education-edunathon-plaignants-deboutes-157661.html
tags:
- Institutions
- Associations
- Éducation
- Marchés publics
- Promotion
---

> Le collectif Edunathon n'a pas obtenu en référé la suspension de l'accord à 13 M€ entre Microsoft France et l'Éducation nationale.
