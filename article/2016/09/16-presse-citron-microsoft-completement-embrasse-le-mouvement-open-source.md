---
site: Presse Citron
title: "Microsoft a complètement embrassé le mouvement open source"
author: Setra
date: 2016-09-16
href: http://www.presse-citron.net/microsoft-a-completement-embrasse-le-mouvement-open-source
tags:
- Entreprise
---

> La firme de Redmond a plus de contributeurs open source que Google. La politique d’ouverture de Satya Nadella est en train de payer.
