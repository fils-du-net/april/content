---
site: Journal du Net
title: "Champion de la contribution open source!"
author: Antoine Crochet-Damais
date: 2016-09-16
href: http://www.journaldunet.com/web-tech/developpeur/1184759-microsoft-l-entreprise-avec-le-plus-grand-nombre-de-contributeurs-sur-github
tags:
- Entreprise
- Informatique en nuage
---

> Microsoft arrive en tête du classement des organisations avec le plus grande nombre de contributeurs open source sur GitHub. L'indicateur illustre le virage stratégique pris par le groupe dans ce domaine.
