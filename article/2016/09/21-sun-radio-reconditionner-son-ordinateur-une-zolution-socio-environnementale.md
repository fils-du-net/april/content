---
site: SUN Radio
title: "Reconditionner son ordinateur, une Zolution socio-environnementale"
author: Cerise Robin
date: 2016-09-21
href: http://www.lesonunique.com/content/reconditionner-son-ordinateur-une-zolution-socio-environnementale-65057
tags:
- Économie
- Associations
---

> A l'occasion de RéZolution numérique, Nâga animait un atelier le mardi 20 septembre au SoliLab à Nantes. l'association rezéenne explique comment diminuer l'impact socio-écologique de l'informatique.
