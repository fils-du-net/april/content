---
site: Next INpact
title: "Consultations en ligne de citoyens: l’État prépare une plateforme libre"
author: Xavier Berne
date: 2016-09-19
href: http://www.nextinpact.com/news/101420-consultations-en-ligne-citoyens-l-etat-prepare-plateforme-libre.htm
tags:
- Internet
- Institutions
---

> Avec l’aide de la société civile, l’État se prépare à fournir à ses administrations une plateforme de consultation libre (et gratuite). L’objectif est à la fois d’encourager ce type d’opération participative tout en favorisant leur transparence.
