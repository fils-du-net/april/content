---
site: Next INpact
title: "Les 15 mesures clés de la loi Numérique"
author: Xavier Berne
date: 2016-09-29
href: http://www.nextinpact.com/news/101526-les-15-mesures-cles-loi-numerique.htm
tags:
- Administration
- Institutions
- Neutralité du Net
- Open Data
- Vie privée
---

> Vous n'avez guère suivi les débats autour du projet de loi Numérique, qui vient tout juste d'être définitivement adopté par le Parlement? Voici un panorama de quinze mesures emblématiques.
