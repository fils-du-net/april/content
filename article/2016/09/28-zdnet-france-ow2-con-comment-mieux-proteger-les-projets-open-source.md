---
site: ZDNet France
title: "OW2 Con: comment mieux protéger les projets open-source?"
author: Louis Adam
date: 2016-09-28
href: http://www.zdnet.fr/actualites/ow2-con-comment-mieux-proteger-les-projets-open-source-39842564.htm
tags:
- Innovation
---

> À l’occasion de l’OW2Con qui se déroulait à Paris cette semaine, David Wheeler, responsable du programme de badge lancé par la Core Infrastructure Initiative, est revenu sur le succès de cette initiative.
