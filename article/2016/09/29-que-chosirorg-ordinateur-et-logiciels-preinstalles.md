---
site: Que Chosir.org
title: "Ordinateur et logiciels préinstallés"
author: Camille Gruhier
date: 2016-09-29
href: https://www.quechoisir.org/actualite-ordinateur-et-logiciels-preinstalles-la-justice-europeenne-ecarte-la-pratique-deloyale-n22691
tags:
- Institutions
- Vente liée
- Associations
- Europe
---

> Selon la Cour de justice de l’Union européenne, la vente d’un ordinateur équipé de logiciels préinstallés ne constitue pas une pratique commerciale déloyale. Tant pis si le consommateur ne peut ni choisir librement son système d’exploitation, ni contester la présence non sollicitée des multiples logiciels. Un revers pour les défenseurs du logiciel libre, une déception pour l’UFC-Que Choisir.
