---
site: Industrie et Technologies
title: "L’Industrie 4.0 sera open source... ou ne sera pas"
author: Nicolas du Manoir
date: 2016-09-14
href: http://www.industrie-techno.com/l-industrie-4-0-sera-open-source-ou-ne-sera-pas.45567
tags:
- Entreprise
- Internet
- Innovation
- Standards
---

> Selon Nicolas du Manoir, vice-président régional de Progress pour la France, la clé de l'industrie 4.0, intelligente et surtout connectée, réside dans l'interopérabilité. Et cette interopérabilité passe forcément par l'ouverture et le partage des plateformes, normes et protocoles. Fini les systèmes propriétaires. L'industrie du futur sera open source... ou ne sera pas.
