---
site: Numerama
title: "Vente liée d'un ordinateur et de Windows: la CJUE n'interdit pas le principe"
author: Guillaume Champeau
date: 2016-09-07
href: http://www.numerama.com/business/192961-vente-liee-dun-ordinateur-et-de-windows-la-cjue-ninterdit-pas-le-principe.html
tags:
- Institutions
- Vente liée
- Europe
---

> Dès lors que le client est prévenu et qu'il sait qu'il a la possibilité d'acheter un autre produit équivalent d'une autre marque sans logiciels préinstallés, le fait d'imposer une licence Windows à l'achat d'un ordinateur ne constitue pas une pratique commerciale déloyale qu'il faudrait interdire. Ainsi en a jugé la CJUE.
