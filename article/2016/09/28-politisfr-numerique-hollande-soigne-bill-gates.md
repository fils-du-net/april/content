---
site: Politis.fr
title: "Numérique: Hollande soigne Bill Gates"
date: 2016-09-28
href: http://www.politis.fr/articles/2016/09/numerique-hollande-soigne-bill-gates-35442
tags:
- Entreprise
- Économie
- Institutions
- Vente liée
- Associations
- Éducation
---

> Le gouvernement français a défendu la pratique de la vente liée d’ordinateurs et de systèmes d’exploitation.
