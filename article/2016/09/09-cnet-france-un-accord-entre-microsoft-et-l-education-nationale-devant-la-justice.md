---
site: cnet France
title: "Un accord entre Microsoft et l'éducation nationale devant la justice"
date: 2016-09-09
href: http://www.cnetfrance.fr/news/un-accord-entre-microsoft-et-l-education-nationale-devant-la-justice-39841722.htm
tags:
- Institutions
- Associations
- Innovation
- Marchés publics
---

> Un partenariat de formation aux outils Office a été passé sans accord financier mais sans appel d'offre, un collectif a porté l'affaire devant la justice.
