---
site: Le Journal du Geek
title: "Le grand plan numérique fait-il enfin sa rentrée?"
author: Elodie
date: 2016-09-06
href: http://www.journaldugeek.com/2016/09/06/grand-plan-numerique-rentree
tags:
- Institutions
- Associations
- Éducation
- Marchés publics
---

> Pour cette rentrée scolaire, le président François Hollande souhaite que l’ensemble des collégiens bénéficie d’un dispositif numérique d’ici 2018. Un vœu déjà formulé devenu depuis un véritable serpent de mer.
