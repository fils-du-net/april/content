---
site: L'OBS
title: "Microsoft et l’Education nationale: mécénat ou marché public dissimulé?"
author: Thierry Noisette
date: 2016-09-08
href: http://rue89.nouvelobs.com/2016/09/08/microsoft-leducation-nationale-mecenat-marche-public-dissimule-265104
tags:
- April
- Institutions
- Éducation
- Marchés publics
---

> La fourniture gratuite, pour une valeur de 13 millions d’euros de produits et services à l’Education nationale, est-elle un acte de mécénat, comme l’affirment le ministère et l’éditeur de logiciels? Ou faut-il en réalité y voir un marché public dissimulé?
