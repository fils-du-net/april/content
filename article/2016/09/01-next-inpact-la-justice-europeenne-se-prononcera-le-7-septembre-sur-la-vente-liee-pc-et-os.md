---
site: Next INpact
title: "La justice européenne se prononcera le 7 septembre sur la vente liée PC et OS"
author: Xavier Berne
date: 2016-09-01
href: http://www.nextinpact.com/news/101197-la-justice-europeenne-se-prononcera-7-septembre-sur-vente-liee-pc-et-os.htm
tags:
- Institutions
- Vente liée
- Europe
---

> La vente d’un ordinateur équipé de logiciels préinstallés constitue-t-elle une pratique commerciale déloyale au sens de la législation européenne? Voilà la question à laquelle va répondre la Cour de justice de l’UE, mercredi 7 septembre, à la demande des juridictions françaises.
