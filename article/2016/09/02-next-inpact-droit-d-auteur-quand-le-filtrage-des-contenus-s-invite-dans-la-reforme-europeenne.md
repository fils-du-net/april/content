---
site: Next INpact
title: "Droit d’auteur: quand le filtrage des contenus s’invite dans la réforme européenne"
author: Marc Rees
date: 2016-09-02
href: http://www.nextinpact.com/news/101211-droit-d-auteur-quand-filtrage-contenus-s-invite-dans-reforme-europeenne.htm
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
- Promotion
- Europe
---

> Le projet de réforme du droit d’auteur a été dévoilé depuis quelques jours. Portée par la Commission européenne, cette directive met à jour plusieurs normes dans l’optique du marché unique du numérique. Une belle occasion pour y injecter une savante dose de filtrage.
