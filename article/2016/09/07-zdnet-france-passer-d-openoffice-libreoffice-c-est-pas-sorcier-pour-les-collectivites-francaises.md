---
site: ZDNet France
title: "Passer d'OpenOffice à LibreOffice, c'est pas sorcier pour les collectivités françaises"
author: Christophe Auffray
date: 2016-09-07
href: http://www.zdnet.fr/actualites/passer-d-openoffice-a-libreoffice-c-est-pas-sorcier-pour-les-collectivites-francaises-39841604.htm
tags:
- Administration
- Associations
- Standards
---

> Et si le projet OpenOffice s'arrête, que deviennent les administrations françaises équipées de la suite libre? Elles doivent migrer et la complexité d'un tel projet n'est pas insurmontable. Pourquoi? Car le format de fichier, ODF, est inchangé et c'est l'essentiel.
