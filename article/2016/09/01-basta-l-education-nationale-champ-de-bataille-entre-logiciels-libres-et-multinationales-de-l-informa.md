---
site: Basta!
title: "L'Éducation nationale, champ de bataille entre logiciels libres et multinationales de l'informatique"
author: Eva Thiébaud
date: 2016-09-01
href: http://www.bastamag.net/Rentree-scolaire-education-numerique-logiciels-libres
tags:
- Logiciels privateurs
- Administration
- April
- Associations
- Éducation
- Marchés publics
---

> L’apprentissage de l’informatique et des outils numériques fait progressivement son entrée au sein des écoles. À l’enjeu éducatif – apprendre aux élèves à utiliser et comprendre les nouvelles technologies dont ils se servent – s’ajoute un enjeu commercial pour de grandes firmes, comme Microsoft, qui cherchent à faire connaître leurs produits auprès des 12,7 millions d’élèves.
