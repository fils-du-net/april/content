---
site: Libération
title: "La ville de demain nous appartient"
author: Patrick Cappelli et Xavier Colas
date: 2016-09-26
href: http://www.liberation.fr/evenements-libe/2016/09/26/la-ville-de-demain-nous-appartient_1511334
tags:
- Administration
- Économie
- Innovation
---

> «Citadins &amp; Citoyens»: échos de la journée de débats organisée ce dimanche par «Libération» au BHV Marais.
