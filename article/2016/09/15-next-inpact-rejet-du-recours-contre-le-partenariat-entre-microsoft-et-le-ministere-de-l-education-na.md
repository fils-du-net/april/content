---
site: Next INpact
title: "Rejet du recours contre le partenariat entre Microsoft et le ministère de l'Éducation nationale"
author: Marc Rees
date: 2016-09-15
href: http://www.nextinpact.com/news/101392-rejet-recours-contre-partenariat-entre-microsoft-et-ministere-education-nationale.htm
tags:
- Institutions
- Associations
- Éducation
- Marchés publics
- Vie privée
---

> Le juge des référés du tribunal de grande instance de Paris a rendu son ordonnance dans l’action lancée par plusieurs associations issues du milieu du libre contre l’accord Microsoft/Éducation nationale. Les plaignants sont tous déboutés.
