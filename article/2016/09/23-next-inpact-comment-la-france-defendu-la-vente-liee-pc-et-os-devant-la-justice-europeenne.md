---
site: Next INpact
title: "Comment la France a défendu la vente liée PC et OS devant la justice européenne"
author: Marc Rees
date: 2016-09-23
href: http://www.nextinpact.com/news/101489-comment-france-est-intervenue-pour-defendre-vente-liee-pc-et-os-en-europe.htm
tags:
- Économie
- Institutions
- Vente liée
- Europe
---

> Voilà peu, la Cour de justice de l’Union européenne a finalement considéré que la vente liée PC et OS n’était pas une pratique déloyale. L’avocat à l’origine de cette plainte vient de révéler les positions françaises sur ce dossier brûlant.
