---
site: Industrie et Technologies
title: "Quand Ray Kurzweil s’interroge sur la mort du brevet... mieux vaut quand même écouter"
author: Aurélie Barbaux
date: 2016-09-14
href: http://www.industrie-techno.com/quand-ray-kurzweil-s-interroge-sur-la-mort-du-brevet-mieux-vaut-quand-meme-ecouter.45576
tags:
- Entreprise
- Droit d'auteur
- Europe
---

> Quand Ray Kurzweil, l’auteur d'Humanité 2.0 qui annonce la dominance de l’intelligence artificielle sur celle des humains pour 2045, s’interroge sur la mort de la propriété intellectuelle, il vaut mieux tendre l’oreille. Pas tant pour ses arguments, pas inintéressants par ailleurs, que parce que la puissance de lobbying des géants de la Silicon Valley sur l’économie mondiale et la transformation de nos sociétés n’est plus à démontrer.
