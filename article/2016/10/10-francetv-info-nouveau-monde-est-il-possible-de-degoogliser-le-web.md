---
site: francetv info
title: "Nouveau monde. Est-il possible de \"Dégoogliser\" le Web?"
author: Jérôme Colombain
date: 2016-10-10
href: http://www.francetvinfo.fr/replay-radio/nouveau-monde/nouveau-monde-est-il-possible-de-degoogliser-le-web_1854529.html
tags:
- Internet
- Associations
- Vie privée
---

> Pour la troisième année, l’association française Framasoft lance une campagne contre la toute-puissance des géants du Web en proposant des logiciels alternatifs "libres".
