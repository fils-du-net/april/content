---
site: Numerama
title: "Apple attaqué en justice par une PME française qui en a marre de Safari"
author: Guillaume Champeau
date: 2016-10-07
href: http://www.numerama.com/tech/200167-apple-attaque-justice-pme-francaise-a-marre-de-safari.html
tags:
- Entreprise
- Internet
- Institutions
- Standards
---

> La société française Nexedi a annoncé qu'elle déposait plainte contre Apple, qu'il accuse d'imposer un rapport de force déséquilibré en refusant que d'autres navigateurs que Safari puissent être exécutés sur iOS, alors que son moteur WebKit est en retard dans la prise en charge des standards HTML5.
