---
site: francetv info
title: "VIDEO. \"Cash Investigation\". Comment Microsoft a conquis le réseau informatique du ministère de la Défense"
date: 2016-10-19
href: http://www.francetvinfo.fr/societe/debats/video-cash-investigation-comment-microsoft-a-conquis-le-reseau-informatique-du-ministere-de-la-defense_1878639.html
tags:
- Entreprise
- Administration
- Institutions
- Marchés publics
---

> Microsoft s'est progressivement implanté au sein du ministre de la Défense. Les militaires français se sont habitués à utiliser les produits du géant américain. Difficile après de changer de logiciels… Extrait du magazine "Cash Investigation" du 18 octobre.
