---
site: Developpez.com
title: "L'April demande la création d'une commission d'enquête parlementaire concernant les relations entre l'État et Microsoft"
author: zoom61
date: 2016-10-27
href: http://open-source.developpez.com/actu/105864/L-April-demande-la-creation-d-une-commission-d-enquete-parlementaire-concernant-les-relations-entre-l-Etat-et-Microsoft
tags:
- Entreprise
- Administration
- April
- Institutions
- Informatique en nuage
---

> suite à l'émission de Cash Investigation du 18 octobre 2016
