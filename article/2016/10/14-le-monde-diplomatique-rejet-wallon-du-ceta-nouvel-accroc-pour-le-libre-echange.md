---
site: Le Monde diplomatique
title: "Rejet wallon du CETA, nouvel accroc pour le libre-échange"
author: Pierre Kohler et Servaas Storm
date: 2016-10-14
href: http://www.monde-diplomatique.fr/carnet/2016-10-14-Rejet-du-Ceta-accroc-libre-echange
tags:
- Économie
- Institutions
- Europe
- International
- ACTA
---

> Deux parlements régionaux belges viennent d'opposer leur veto à l'accord de libre-échange entre l'Union européenne et le Canada, le CETA. Cette décision suffira-t-elle à enterrer ce projet? Rien n'est moins sûr. Une nouvelle étude affûte pourtant les arguments économiques des opposants en identifiant, chiffres à l'appui, ses uniques bénéficiaires: les investisseurs. Une manifestation contre le projet d'accord aura lieu le 15 octobre dans plusieurs villes européenne.
