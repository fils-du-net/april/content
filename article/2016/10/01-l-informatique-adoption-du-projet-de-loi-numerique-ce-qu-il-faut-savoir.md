---
site: L'informatique
title: "Adoption du projet de loi Numérique: ce qu’il faut savoir"
author: Victoria Manon
date: 2016-10-01
href: http://www.linformatique.org/adoption-du-projet-de-loi-numerique.html
tags:
- Administration
- Institutions
- Neutralité du Net
- Open Data
- Vie privée
---

> Le projet de loi Numérique vient d’être adopté par le Parlement. Qu’est-ce que les Français doivent savoir au sujet de cette nouvelle loi?
