---
site: LeMagIT
title: "Jim Zemlin aux opérateurs: \"Etes-vous prêts pour l'open source?\""
author: Christophe Bardy
date: 2016-10-07
href: http://www.lemagit.fr/actualites/450400668/Jim-Zemlin-aux-operateurs-Etes-vous-prets-pour-lopen-source
tags:
- Entreprise
- Innovation
---

> A l'occasion du forum sur l'Ultra haut débit organisé par Huawei à Francfort sous l'égide des Nations Unies, Jim Zemlin, le patron de la Linux Foundation a livré un véritable prêche en faveur du modèle open source.
