---
site: Next INpact
title: "Google.fr bloqué pour apologie du terrorisme suite à une «erreur humaine» d’Orange"
author: Marc Rees
date: 2016-10-17
href: http://www.nextinpact.com/news/101786-google-fr-bloque-pour-apologie-terrorisme-orange-invoque-erreur-humaine.htm
tags:
- Entreprise
- Internet
- Institutions
---

> L’incident risque de devenir l'un des plus beaux contre-exemples des procédures de blocage. Ce matin, Google.fr et fr.wikipedia.org notamment ont été qualifiés de sites faisant l’apologie du terrorisme sur les écrans des abonnés Orange. Et ils ont été bloqués pour ce motif. Contactée, Orange invoque l'erreur humaine.
