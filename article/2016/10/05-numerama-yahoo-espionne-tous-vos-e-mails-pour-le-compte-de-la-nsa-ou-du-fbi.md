---
site: Numerama
title: "Yahoo espionne tous vos e-mails pour le compte de la NSA ou du FBI"
author: Guillaume Champeau
date: 2016-10-05
href: http://www.numerama.com/tech/199281-yahoo-espionne-tous-vos-e-mails-pour-le-compte-de-la-nsa-ou-du-fbi.html
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> Yahoo a accepté sans combattre d'installer un logiciel sur ses serveurs, qui regarde le contenu des e-mails qui arrivent et transmet aux services de renseignement américains ceux qui peuvent les intéresser. Il est plus que temps de fermer son compte Yahoo.
