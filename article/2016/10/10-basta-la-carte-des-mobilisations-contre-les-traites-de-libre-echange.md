---
site: Basta!
title: "La carte des mobilisations contre les traités de libre-échange"
author: Collectif
date: 2016-10-10
href: http://www.bastamag.net/10-octobre-La-carte-des-mobilisations-contre-les-traites-de-libre-echange
tags:
- Économie
- Institutions
- Europe
- International
- ACTA
---

> Le projet de traité de libre échange entre l’Union européenne et le Canada (CETA) doit être soumis à l’approbation du Conseil européen le 18 octobre. Négocié dans une grande opacité, ce traité, s’il est adopté, aura des conséquences sur nos vies quotidiennes : l’emploi, la santé, l’alimentation, les services publics... Le 15 octobre, des manifestations sont prévues dans toute la France. Voici l’appel des organisations à l’initiative de l’événement et la carte des mobilisations.
