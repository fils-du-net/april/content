---
site: Finyear
title: "Cybersécurité en France: 47% de cyberattaques en moins en 2016"
author: Laurent Leloup
date: 2016-10-21
href: http://www.finyear.com/Cybersecurite-en-France-47-de-cyberattaques-en-moins-en-2016_a37215.html
tags:
- Entreprise
- Internet
---

> Les entreprises françaises auraient-elles trouvé un moyen de lutter efficacement contre les cyberattaques? C’est ce que laisse penser la dernière étude du cabinet d’audit et de conseil PwC «The Global State of Information Security® Survey 2017», réalisée en collaboration avec CIO et CSO, qui propose un tour d'horizon de la cybersécurité dans le monde.
