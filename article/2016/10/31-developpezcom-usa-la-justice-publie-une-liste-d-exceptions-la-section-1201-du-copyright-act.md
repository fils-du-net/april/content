---
site: Developpez.com
title: "USA: la justice a publié une liste d'exceptions à la section 1201 du Copyright Act"
author: Stéphane le calme
date: 2016-10-31
href: http://www.developpez.com/actu/106067/USA-la-justice-a-publie-une-liste-d-exceptions-a-la-section-1201-du-Copyright-Act-pour-proteger-les-chercheurs-qui-etudient-la-securite-des-DRM
tags:
- Institutions
- Associations
- DRM
- Droit d'auteur
- International
---

> La justice américaine a publié une liste d'exceptions au DMCA qui va permettre de protéger les professionnels en sécurité comme le réclamait l’EFF dans un combat lancé depuis des mois déjà. Pour les deux prochaines années, les chercheurs pourront contourner les contrôles d'accès numériques, faire de la rétro-ingénierie, avoir accès, copier et manipuler du contenu numérique qui est protégé par le droit d'auteur sans craindre de poursuite judiciaire dans la limite du raisonnable. La Federal Trade Commission parle d’actes qui doivent être perpétrés de «bonne foi».
