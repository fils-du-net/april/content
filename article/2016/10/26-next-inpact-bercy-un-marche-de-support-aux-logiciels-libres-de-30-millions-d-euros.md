---
site: Next INpact
title: "À Bercy, un marché de support aux logiciels libres de 30 millions d’euros"
author: Xavier Berne
date: 2016-10-26
href: http://www.nextinpact.com/news/101899-a-bercy-marche-support-aux-logiciels-libres-30-millions-d-euros.htm
tags:
- Administration
- Économie
---

> De fin 2008 à fin 2014, les ministères économiques et financiers ont injecté, via un marché dédié, 30 millions d’euros dans l’écosystème du logiciel libre. Cela a notamment permis d’apporter près de 200 correctifs. C'est en tout cas ce qui ressort d'une récente réponse à une question écrite.
