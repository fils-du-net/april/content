---
site: L'OBS
title: "Et d’un coup, j’ai pu me libérer d’Apple, Google et Microsoft"
author: Fabien Granier
date: 2016-10-10
href: http://rue89.nouvelobs.com/blog/rural-rules/2016/10/10/et-dun-coup-je-me-suis-libere-dapple-google-et-microsoft-235392
tags:
- Sensibilisation
---

> Le Bocage bourbonnais est un de ces nombreux coins de France qui, bien que très loin de l'émulation des villes, n'en est pas moins un lieu bouillonnant d'art, de rencontres, d'incongruités et de découvertes. Des générations s'y succèdent: poings dressés, pieds dans la boue, cœur vers le monde. Nos portes sont ouvertes et nos clés ne quittent pas le contact. Chroniques depuis la diagonale du vide.
