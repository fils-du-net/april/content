---
site: 20minutes.fr
title: "Comment une armée d'objets connectés infectés a cassé Internet"
author: Philippe Berry
date: 2016-10-22
href: http://www.20minutes.fr/high-tech/1947575-20161022-comment-armee-objets-connectes-infectes-casse-internet
tags:
- Entreprise
- Internet
---

> L'attaque contre un prestataire DNS a été menée avec un botnet d'un genre nouveau...
