---
site: ZDNet France
title: "L'Anssi sur Microsoft et la Défense: ni pour ni contre, bien au contraire"
author: Thierry Noisette
date: 2016-10-27
href: http://www.zdnet.fr/actualites/l-anssi-sur-microsoft-et-la-defense-ni-pour-ni-contre-bien-au-contraire-39843972.htm
tags:
- Entreprise
- Administration
- Institutions
---

> L'existence d'un accord confidentiel de l'agence avec Microsoft est implicitement admise par le directeur de l'Anssi. Qui affirme que l'agence est "favorable au logiciel libre", mais...
