---
site: Next INpact
title: "En 2015, 224 000 euros de logiciels libres pour le ministère de l’Agriculture"
author: Xavier Berne
date: 2016-10-19
href: http://www.nextinpact.com/news/101822-en-2015-224-000-euros-logiciels-libres-pour-ministere-l-agriculture.htm
tags:
- Administration
- Économie
---

> On l’oublie souvent, mais logiciel libre n’est pas toujours synonyme de gratuité. Le ministère de l’Agriculture a ainsi dépensé l’année dernière plus de 220 000 euros pour assurer le fonctionnement de programmes non propriétaires.
