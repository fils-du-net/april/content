---
site: L'OBS
title: "Voici le code source d’Admission post-bac qu’ont reçu les lycéens"
author: Emilie Brouze
date: 2016-10-18
href: http://rue89.nouvelobs.com/2016/10/18/voici-code-source-dapb-tenu-secret-jusqua-present-265443
tags:
- Institutions
- Associations
- Éducation
- Open Data
---

> Sur papier, le ministère a transmis à l’association Droit des lycéens le code source de l’algorithme du portail Admission post-bac (APB) pour les licences à capacité limitée.
