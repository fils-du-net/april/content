---
site: Les Nouvelles
title: "Pourquoi la gendarmerie de Sablé-sur-Sarthe est passée dans Cash Investigation"
author: Lucile Ageron
date: 2016-10-19
href: http://www.lesnouvellesdesable.fr/2016/10/19/television-pourquoi-la-gendarmerie-de-sable-sur-sarthe-est-passee-dans-cash-investigation
tags:
- Économie
- Institutions
- Marchés publics
---

> Mardi 18 octobre, sur France 2 dans l'émission Cash Investigation sur les marchés publics, une séquence est consacrée à la gendarmerie de Sablé-sur-Sarthe.
