---
site: Next INpact
title: "Le contrat Open Bar entre Microsoft et la Défense à l’honneur de Cash Investigation"
author: Marc Rees
date: 2016-10-19
href: http://www.nextinpact.com/news/101813-le-contrat-open-bar-entre-microsoft-et-defense-a-l-honneur-cash-investigation.htm
tags:
- Entreprise
- Administration
- April
- Institutions
- Marchés publics
---

> Hier l’émission Cash Investigation est revenue sur le contrat Open Bar signé entre Microsoft et le ministère de la Défense. Un contrat lourd de millions d'euros qui pose de sérieuses questions stratégiques et d'indépendance militaire.
