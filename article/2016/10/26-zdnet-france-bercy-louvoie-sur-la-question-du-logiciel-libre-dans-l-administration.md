---
site: ZDNet France
title: "Bercy louvoie sur la question du logiciel libre dans l’administration"
author: Louis Adam
date: 2016-10-26
href: http://www.zdnet.fr/actualites/bercy-louvoie-sur-la-question-du-logiciel-libre-dans-l-administration-39843878.htm
tags:
- Administration
- Économie
---

> Michel Sapin, le ministre de l’Économie et des Finances a répondu à une question de la députée Isabelle Attard sur la place du logiciel libre dans l’administration française.
