---
site: ZDNet France
title: "Préférence au logiciel libre - La Russie prépare sa loi pour écarter les américains"
author: Christophe Auffray
date: 2016-10-06
href: http://www.zdnet.fr/actualites/preference-au-logiciel-libre-la-russie-prepare-sa-loi-pour-ecarter-les-americains-39842970.htm
tags:
- Administration
- Économie
- Institutions
- Marchés publics
- International
---

> Guerre froide numérique. Le Parlement russe prépare une loi restreignant l'achat de licences propriétaires par les agences gouvernementales et établissant une priorité en faveur de l'open source.
