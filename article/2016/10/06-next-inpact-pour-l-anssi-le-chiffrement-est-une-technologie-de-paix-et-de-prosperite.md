---
site: Next INpact
title: "Pour l'ANSSI, le chiffrement est «une technologie de paix et de prospérité»"
author: Marc Rees
date: 2016-10-06
href: http://www.nextinpact.com/news/101661-pour-anssi-chiffrement-est-technologie-paix-et-prosperite.htm
tags:
- Institutions
- Vie privée
---

> En août dernier, Libération éventait une note de l’ANSSI où Guillaume Poupard dégommait sans nuance l’idée d’installer des backdoors (portes dérobées) dans les solutions de chiffrement. Le sujet est revenu sur la scène des Assises de Monaco.
