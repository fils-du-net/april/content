---
site: ITespresso
title: "Microsoft - Défense: polémique sur la portée de ”l'accord de sécurité”"
author: Philippe Guerrier
date: 2016-10-20
href: http://www.itespresso.fr/microsoft-defense-polemique-accord-securite-140959.html
tags:
- Entreprise
- Administration
- April
- Institutions
- Marchés publics
---

> Après l'émission Cash Investigation, l'APRIL veut en savoir plus sur l'accord renouvelé entre le ministère de la Défense et Microsoft pour l'usage des logiciels de l'éditeur américain.
