---
site: Le Courrier
title: "L’appel de l’e-démocratie"
author: Matthias Lecoq
date: 2016-10-24
href: http://www.lecourrier.ch/143592/l_appel_de_l_e_democratie
tags:
- Internet
- Institutions
- Open Data
---

> Au rang des nouveaux modes de gouvernance urbaine, la démocratie digitale – ou e-démocratie – participe à une revitalisation de la citoyenneté. Matthias Lecoq en recense les dernières évolutions et s’interroge sur les raisons de son inexistence en Suisse romande.
