---
site: Télérama.fr
title: "Yahoo!: mode d'emploi d'un suicide numérique"
author: Olivier Tesquet
date: 2016-10-05
href: http://www.telerama.fr/medias/yahoo-mode-d-emploi-d-un-suicide-numerique,148351.php
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> Yahoo! est un saumon. Dans un monde post-Snowden, les colosses du web ont eu une épiphanie: ils n’ont rien d’autre à vendre que de la confiance à leurs utilisateurs. Bien sûr, Google, Facebook, Amazon et consorts continuent de sucer nos données comme des nectarivores. Mais Yahoo! est un saumon.
