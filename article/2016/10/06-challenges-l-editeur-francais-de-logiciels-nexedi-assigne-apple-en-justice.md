---
site: Challenges
title: "L'éditeur français de logiciels Nexedi assigne Apple en justice"
author: Léa Lejeune
date: 2016-10-06
href: http://www.challenges.fr/challenges-soir/exclusif-l-editeur-francais-de-logiciels-nexedi-assigne-apple-en-justice_431647
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Standards
---

> L’éditeur de logiciels open source pour entreprises Nexedi, assigne Apple au tribunal de commerce de Paris aujourd'hui. Il reproche au géant du net de réclamer l’utilisation de ses éléments pour chaque application développée pour l’iPhone (iOS). Une pratique gênante en matière d'innovation et coûteuse.
