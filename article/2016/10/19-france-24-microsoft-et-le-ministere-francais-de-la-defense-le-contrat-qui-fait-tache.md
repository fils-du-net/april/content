---
site: France 24
title: "Microsoft et le ministère français de la Défense, le contrat qui fait tache"
author: Émilie Laystary
date: 2016-10-19
href: http://mashable.france24.com/tech-business/20161019-microsoft-ministere-defense-contrat
tags:
- Entreprise
- Administration
- April
- Institutions
- Marchés publics
---

> Cela fait plusieurs années que des associations s'égosillent à en dénoncer les risques: depuis 2008, un contrat lie l'armée française à Microsoft. Là où le bât blesse, c'est que celui-ci est onéreux et ne ferme pas la porte aux risques d’espionnage.
