---
site: Numerama
title: "Orange: pourquoi une «erreur humaine» paraît peu vraisemblable"
author: Guillaume Champeau
date: 2016-10-17
href: http://www.numerama.com/tech/202058-orange-pourquoi-une-erreur-humaine-parait-peu-vraisemblable.html
tags:
- Entreprise
- Internet
- Institutions
---

> Est-il possible qu'Orange ait bloqué les visites vers Google et Wikipedia et affiché qu'il s'agissait de sites terroristes, uniquement par «erreur humaine», comme l'assure l'opérateur?
