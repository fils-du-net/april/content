---
site: L’ADN
title: "Pour que GAFA ne soit plus une fatalité"
author: Sylvie le Roy
date: 2016-10-06
href: http://www.ladn.eu/innovation/datas-datas/pour-que-gafa-ne-soit-plus-une-fatalite
tags:
- Internet
- Associations
- Vie privée
---

> Dans l’immense toile des géants de la Silicon Valley, il existe de petits espaces d’irréductibles libertaires qui résistent encore et toujours à l’envahisseur. Leur sceau: Framasoft qui propose à ce jour six nouveaux services alternatifs.
