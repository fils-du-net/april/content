---
site: Next INpact
title: "Au Journal officiel, un fichier biométrique de 60 millions de «gens honnêtes»"
author: Marc Rees
date: 2016-10-31
href: http://www.nextinpact.com/news/101945-au-journal-officiel-fichier-biometrique-60-millions-gens-honnetes.htm
tags:
- Institutions
- Vie privée
---

> Hier, au Journal officiel, le gouvernement a publié un décret instituant un fichier monstre commun aux passeports et aux cartes nationales d'identité. Destiné à faciliter établissement et renouvellement de ces titres, en plus de prévenir les fraudes, il va ingurgiter des centaines de millions de données puisées dans toute la population française.
