---
site: Next INpact
title: "L'accord Défense-Microsoft? «Une logique d'achat économiquement plus performante»"
author: Marc Rees
date: 2016-10-25
href: http://www.nextinpact.com/news/101892-laccord-defense-microsoft-une-logique-dachat-economiquement-plus-performante-selon-le-drian.htm
tags:
- Entreprise
- Administration
- Économie
- April
- Institutions
---

> Questionné par la députée Isabelle Attard, le gouvernement a détaillé tant bien que mal la part des logiciels libres ou propriétaires utilisés par le ministère de la Défense et ses nombreuses administrations. Il est surtout revenu sur l'accord-cadre passé avec Microsoft.
