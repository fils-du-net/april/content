---
site: Developpez.com
title: "Fragmentation de l'open source: faut-il se concentrer sur un ou deux projets viables"
author: Michael Guilloux
date: 2016-10-17
href: http://www.developpez.com/actu/105447/Fragmentation-de-l-open-source-faut-il-se-concentrer-sur-un-ou-deux-projets-viables-ou-multiplier-les-alternatives-pour-la-competitivite
tags:
- Associations
- Innovation
---

> L’année passée, Christian Schaller, un ingénieur de Red Hat a attiré l’attention sur un problème qui pourrait disperser les efforts des contributeurs open source: celui de la fragmentation ou du développement parallèle de plusieurs solutions open source ayant le même but. Il a surtout exposé le cas des suites bureautiques OpenOffice et LibreOffice qui évoluent parallèlement depuis 2011, après que des développeurs ont quitté le projet initial OpenOffice.org pour créer un fork qu’ils ont alors baptisé LibreOffice.
