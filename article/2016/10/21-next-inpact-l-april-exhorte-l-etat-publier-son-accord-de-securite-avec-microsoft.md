---
site: Next INpact
title: "L’April exhorte l’État à publier son «accord de sécurité» avec Microsoft"
author: Xavier Berne
date: 2016-10-21
href: http://www.nextinpact.com/news/101851-l-april-exhorte-l-etat-a-publier-son-accord-securite-avec-microsoft.htm
tags:
- Entreprise
- April
- Institutions
- Marchés publics
- Open Data
---

> Le directeur des affaires publiques de Microsoft France, Marc Mossé, a annoncé devant les caméras de Cash Investigation qu’un «accord de sécurité» avait été noué entre le géant américain du logiciel propriétaire et le gouvernement français. L’April demande par conséquent la publication de ce document.
