---
site: Silicon
title: "Contrat Microsoft-Défense: l’Open Bar passe de 82 à 120 millions d’euros"
author: Reynald Fléchaux
date: 2016-10-19
href: http://www.silicon.fr/contrat-microsoft-defense-open-bar-120-millions-euros-160580.html
tags:
- Entreprise
- Administration
- April
- Institutions
- Marchés publics
---

> Renouvelé en 2013, le contrat «Open Bar» liant Microsoft à la Défense a vu ses coûts bondir de plus de 45 %.
