---
site: Les Echos
title: "L’Open Source School: consécration de la culture OS en France ou machine marketing trop huilée"
author: Sacha Labourey
date: 2016-10-04
href: http://www.lesechos.fr/idees-debats/cercle/cercle-161113-lopen-source-school-consecration-de-la-culture-os-en-france-ou-machine-marketing-trop-huilee-2032376.php
tags:
- Entreprise
- Éducation
---

> En février dernier, l’école EPSI et l’intégrateur SMILE, soutenus par le Conseil National du Logiciel Libre, lançaient à Paris la toute première formation diplômante 100 % Open Source, reconnue et financée par le gouvernement français à hauteur de 1,4 million d’euros.
