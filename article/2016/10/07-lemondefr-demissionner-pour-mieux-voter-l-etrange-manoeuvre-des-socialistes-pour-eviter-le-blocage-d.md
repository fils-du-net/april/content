---
site: LeMonde.fr
title: "Démissionner pour mieux voter: l’étrange manœuvre des socialistes pour éviter le blocage du CETA"
author: Maxime Vaudano
date: 2016-10-07
href: http://transatlantique.blog.lemonde.fr/2016/10/07/demissionner-pour-mieux-voter-letrange-manoeuvre-des-socialistes-pour-eviter-le-blocage-du-ceta
tags:
- Institutions
- Europe
- ACTA
---

> L’accord commercial entre l’Europe et le Canada, le CETA, devrait pouvoir entrer provisoirement en vigueur avant même le vote du Parlement français. C’est la commission des affaires européennes de l’Assemblée nationale qui en a décidé ainsi mercredi 5 octobre, en rejetant une résolution réclamant au gouvernement français qu’il s’oppose à la mise en oeuvre provisoire du CETA.
