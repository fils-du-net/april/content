---
site: ZDNet France
title: "Le logiciel libre contre la surveillance: ”nécessaire mais pas suffisant”"
author: Thierry Noisette
date: 2016-10-31
href: http://www.zdnet.fr/actualites/le-logiciel-libre-contre-la-surveillance-necessaire-mais-pas-suffisant-39844034.htm
tags:
- Internet
- Sensibilisation
- Innovation
- Vie privée
---

> Dans son livre ”Surveillance://”, Tristan Nitot pointe deux défis pour les logiciels libres: l'ergonomie et le modèle économique.
