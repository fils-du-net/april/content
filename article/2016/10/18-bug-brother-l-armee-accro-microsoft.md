---
site: Bug Brother
title: "L’ARMÉE “ACCRO” À MICROSOFT?"
author: Jean-Marc Manach
date: 2016-10-18
href: http://bugbrother.blog.lemonde.fr/2016/10/18/larmee-accro-a-microsoft
tags:
- Entreprise
- Administration
- April
- Institutions
- Marchés publics
---

> Cash Investigation diffusera ce soir une enquête sur le contrat, qualifié d’«open bar», passé entre Microsoft et le ministère de la défense, et basé sur des documents que j’avais rendu publics en 2013 sur le site du Vinvinteur, une émission de télévision quelque peu déjantée qui m’avait recruté, mais dont le site web a disparu. Je me permets donc de republier ladite enquête, consultable sur archive.org, qui archive le web, mais qui n’est pas indexé par Google (&amp; Cie).
