---
site: Journal du Net
title: "Open Management: 7 pistes pour adopter une philosophie d’ouverture"
author: Carine Braun-Heneault
date: 2016-10-06
href: http://www.journaldunet.com/management/expert/65342/open-management--7-pistes-pour-adopter-une-philosophie-d-ouverture.shtml
tags:
- Entreprise
- Économie
- Innovation
---

> Notre société est caractérisée par des exigences fortes de transparence, d’authenticité, d’accès et d’ouverture. Les entreprises doivent sortir de leur zone de confort et se départir de la posture traditionnelle de commandement et de contrôle pour embrasser les principes du management ouvert.
