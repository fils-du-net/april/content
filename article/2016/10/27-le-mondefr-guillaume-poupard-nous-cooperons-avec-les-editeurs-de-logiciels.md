---
site: Le Monde.fr
title: "Guillaume Poupard: «Nous coopérons avec les éditeurs de logiciels»"
author: Martin Untersinger
date: 2016-10-27
href: http://www.lemonde.fr/pixels/article/2016/10/27/guillaume-poupard-nous-cooperons-avec-les-editeurs-de-logiciels_5021403_4408996.html
tags:
- Entreprise
- Administration
- Économie
- Institutions
---

> Sans reconnaître l’existence d’un deal qui lie son administration à Microsoft, Guillaume Poupard, le directeur de l’Agence nationale de sécurité des systèmes d’information, a tenté de rassurer et d’expliquer le périmètre de ce type d’accord.
