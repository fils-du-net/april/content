---
site: Place Gre'net
title: "«Fontaine, la libérée»: le logiciel libre dans tous ses états"
author: Joël Kermabon
date: 2016-10-23
href: http://www.placegrenet.fr/2016/10/23/fontaine-la-liberee-le-logiciel-libre-dans-tous-ses-etats/106728
tags:
- Administration
- April
- Sensibilisation
- Associations
---

> La ville de Fontaine a organisé, ce samedi 22 octobre, à La Source la première édition de l’événement «Fontaine, la libérée». Une journée de rencontres, de présentations et de démonstrations autour du logiciel libre pour le faire découvrir au public et lui en expliquer les enjeux. Mais aussi l’occasion pour la Ville de démontrer, à travers son retour d’expérience, «comment conjuguer les valeurs du logiciel libre avec celles d’un service public communal».
