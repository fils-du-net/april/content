---
site: Le Monde Informatique
title: "La loi pour une République Numérique promulguée avec de nombreuses restrictions"
author: Peter Sayer
date: 2016-10-11
href: http://www.lemondeinformatique.fr/actualites/lire-la-loi-pour-une-republique-numerique-promulguee-avec-de-nombreuses-restrictions-66189.html
tags:
- Administration
- April
- Institutions
- Open Data
---

> La loi française sur la liberté de l'information traitera désormais le code source comme une donnée pouvant être divulguée, au même titre que d'autres informations administratives. Mais il y a une exception: ce droit ne s'appliquera pas si la divulgation du dit code source est susceptible de mettre en danger la sécurité des systèmes d'information du gouvernement.
