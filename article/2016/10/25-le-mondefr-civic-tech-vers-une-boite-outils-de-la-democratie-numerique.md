---
site: Le Monde.fr
title: "«Civic Tech»: vers une boîte à outils de la démocratie numérique"
author: Claire Legros
date: 2016-10-25
href: http://www.lemonde.fr/pixels/article/2016/10/25/civic-tech-vers-une-boite-a-outils-de-la-democratie-numerique_5019800_4408996.html
tags:
- Internet
- Administration
- Institutions
- Open Data
---

> Alors que la France accueillera du 7 au 9 décembre le sommet mondial du Partenariat pour un gouvernement ouvert, un portail va recenser les plateformes de consultation citoyenne. La transparence des outils est au cœur de la réflexion.
