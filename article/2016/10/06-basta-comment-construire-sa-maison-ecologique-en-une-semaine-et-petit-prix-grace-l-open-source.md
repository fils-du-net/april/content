---
site: Basta!
title: "Comment construire sa maison écologique en une semaine et à petit prix grâce à l'«open source»"
author: Rachel Knaebel
date: 2016-10-06
href: http://www.bastamag.net/Comment-construire-sa-maison-ecologique-en-une-semaine-et-a-petit-prix-grace-a
tags:
- Économie
- Matériel libre
- Associations
- Innovation
---

> Les géants du BTP ont-ils du souci à se faire? Après les logiciels libres et la fabrication de machines ou d'objets, la communauté mondiale des fab (...)
