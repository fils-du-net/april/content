---
site: Le Figaro
title: "Champignons: êtes-vous prêts à partager vos «coins»?"
author: Marc Mennessier
date: 2016-10-28
href: http://www.lefigaro.fr/jardin/2016/10/28/30008-20161028ARTFIG00333-champignons-etes-vous-prets-a-partager-vos-bons-coins.php
tags:
- Internet
- Partage du savoir
---

> Une carte interactive permet à tout un chacun de signaler ses zones de cueillette. Mais l'initiative n'est pas du goût de tout le monde...
