---
site: Next INpact
title: "Le calendrier des décrets d'application de la loi Numérique"
author: Xavier Berne
date: 2016-10-11
href: http://www.nextinpact.com/news/101712-le-calendrier-decrets-dapplication-loi-numerique.htm
tags:
- Administration
- Institutions
- Open Data
---

> En vigueur depuis le 9 octobre, la loi Numérique doit encore être complétée par plusieurs décrets d'application avant de pouvoir produire totalement ses effets.
