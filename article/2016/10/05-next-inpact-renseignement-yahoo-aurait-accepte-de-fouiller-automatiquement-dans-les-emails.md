---
site: Next INpact
title: "Renseignement: Yahoo aurait accepté de fouiller automatiquement dans les emails"
author: Vincent Hermann
date: 2016-10-05
href: http://www.nextinpact.com/news/101642-renseignement-yahoo-aurait-accepte-fouiller-automatiquement-dans-emails.htm
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Vie privée
---

> Yahoo aurait mis en place l’année dernière un logiciel spécifique lui permettant de rechercher dans des centaines de millions d’emails des séquences de caractères. Une «révélation» qui survient alors que l’éditeur fait toujours face aux récentes révélations sur l’ampleur de sa fuite de données.
