---
site: Numerama
title: "Android: Bruxelles avance vers des sanctions contre Google"
author: Julien Lausson
date: 2016-10-03
href: http://www.numerama.com/politique/198674-android-bruxelles-avance-vers-des-sanctions-contre-google.html
tags:
- Entreprise
- Institutions
- Europe
---

> La Commission européenne attend en fin de semaine la réponse de Google sur les griefs qu'elle lui a adressés sur Android. Bruxelles envisage de sanctionner le géant du net avec une très forte amende.
