---
site: SecuriteOff
title: "Éric Filiol: «Windows est pour nous une boite noire que connaît très bien la NSA!»"
author: Philippe Richard
date: 2016-10-24
href: http://www.securiteoff.com/eric-filiol-windows-boite-noire-connait-tres-bien-nsa
tags:
- Entreprise
- Administration
- Institutions
- Marchés publics
---

> Diffusée le 18 octobre sur France 2, l’émission Cash Investigation était intitulée «Marchés publics, le grand dérapage». Parmi les sujets abordés, il y avait le contrat entre Microsoft et le Ministère de la Défense signé initialement en 2009 puis renouvelé en 2013. Le contrat (Windows) est passé de 82 millions à 120 millions d’euros…
