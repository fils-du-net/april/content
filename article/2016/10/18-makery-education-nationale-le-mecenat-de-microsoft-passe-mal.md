---
site: Makery
title: "Education nationale: le «mécénat» de Microsoft passe mal"
date: 2016-10-18
href: http://www.makery.info/2016/10/18/education-nationale-le-mecenat-de-microsoft-passe-mal
tags:
- Entreprise
- April
- Institutions
- Associations
- Éducation
- Marchés publics
---

> Le partenariat Microsoft-Education nationale agite les défenseurs du logiciel libre. Un premier procès perdu, la Commission nationale de l’informatique et des libertés (Cnil) saisie, une pétition qui circule… Créé début 2016, le collectif d’associations libristes Edunathon a intenté une action en justice contre l’Etat et Microsoft pour contester la légalité du partenariat de 13 millions d’euros conclu en novembre 2015. Argument? L’Etat a signé cet accord avec Microsoft au mépris du code des marchés publics. Mi-septembre, le tribunal de grande instance de Paris en a jugé autrement.
