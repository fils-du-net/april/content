---
site: Le Temps
title: "La cyberattaque de vendredi révèle la faiblesse de l'Internet des objets"
author: Anouch Seydtaghia
date: 2016-10-23
href: https://www.letemps.ch/economie/2016/10/23/cyberattaque-vendredi-revele-faiblesse-linternet-objets
tags:
- Internet
---

> Des webcams, des enregistreurs vidéos et d'autres objets connectés à Internet ont été utilisés pour bloquer des sites web majeurs durant plusieurs heures
