---
site: Libération
title: "56kast #83: Microsoft et l'Education nationale, les liaisons dangereuses"
author: Camille Gévaudan
date: 2016-10-13
href: http://www.liberation.fr/futurs/2016/10/13/56kast-83-microsoft-et-l-education-nationale-les-liaisons-dangereuses_1521485
tags:
- April
- Institutions
- Éducation
- Marchés publics
- Informatique en nuage
---

> Cette semaine, Etienne Gonnu de l'April décrypte le partenariat qui lie Microsoft à l'Éducation nationale depuis un an: le géant américain de l'informatique fournit un «cloud» dans les écoles, forme les enseignants aux logiciels Microsoft... Est-ce bien normal?
