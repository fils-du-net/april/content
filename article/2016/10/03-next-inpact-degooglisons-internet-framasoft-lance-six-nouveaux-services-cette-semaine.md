---
site: Next INpact
title: "Dégooglisons Internet: Framasoft lance six nouveaux services cette semaine"
author: Vincent Hermann
date: 2016-10-03
href: http://www.nextinpact.com/news/101622-framasoft-troisieme-round-pour-degooglisons-internet-avec-30-services-desormais.htm
tags:
- Internet
- Associations
- Vie privée
---

> Framasoft, qui avait lancé en 2014 l’initiative «Dégooglisons Internet», revient pour la troisième vague de services. L’idée est toujours la même: proposer des alternatives à des solutions centralisées existantes afin que l’utilisateur récupère le pouvoir sur ses données.
