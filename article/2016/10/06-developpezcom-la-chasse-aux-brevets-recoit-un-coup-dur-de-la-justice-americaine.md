---
site: Developpez.com
title: "La chasse aux brevets reçoit un coup dur de la justice américaine"
author: Coriolan
date: 2016-10-06
href: http://www.developpez.com/actu/104883/La-chasse-aux-brevets-recoit-un-coup-dur-de-la-justice-americaine-bientot-la-fin-des-brevets-logiciels
tags:
- Économie
- Institutions
- Brevets logiciels
- International
---

> La question des brevets logiciels fait l’objet d’un débat politique et technique opposant diverses parties dans lesquelles les lobbys industriels jouent un rôle de premier plan. Aux États-Unis, la jurisprudence est traditionnellement favorable à la protection des logiciels par brevet; cependant, les décisions récentes paraissent relativiser cette position.
