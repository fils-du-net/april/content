---
site: Libération
title: "Framasoft accélère la dégooglisation du Web"
author: Camille Gévaudan
date: 2016-10-03
href: http://www.liberation.fr/futurs/2016/10/03/framasoft-accelere-la-degooglisation-du-web_1519262
tags:
- Internet
- Associations
- Vie privée
---

> L'association fête les deux ans de son projet en lançant six nouveaux services libres et éthiques en alternative à Skype, Evernote, Google groups et autres géants du Web.
