---
site: Le progrès
title: "Il partage ses coins à champignons sur le Web"
author: Fred Jimenez
date: 2016-10-10
href: http://www.leprogres.fr/actualite/2016/10/10/il-partage-ses-coins-a-champignons-sur-le-web
tags:
- Internet
- Partage du savoir
- Open Data
---

> Un Bisontin a créé une carte interactive en ligne pour géolocaliser ses coins à champignons.
