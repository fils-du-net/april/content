---
site: ZDNet France
title: "La place de Microsoft au sein de l'armée française encadrée par un accord secret"
author: Louis Adam
date: 2016-10-21
href: http://www.zdnet.fr/actualites/la-place-de-microsoft-au-sein-de-l-armee-francaise-encadree-par-un-accord-secret-39843658.htm
tags:
- Entreprise
- Administration
- April
- Institutions
- Marchés publics
---

> Lors de l’émission Cash Investigation diffusée mardi sur France 2, le directeur des affaires publiques de Microsoft France a reconnu devant les cameras l’existence d’un «accord de sécurité» entre Microsoft et l’État. L’April demande aujourd’hui à voir ce contrat pour juger de sa fiabilité.
