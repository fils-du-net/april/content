---
site: Programmez!
title: "L'IoT doit s'appuyer sur le Libre"
author: Pierre Ficheux
date: 2016-10-17
href: http://www.programmez.com/avis-experts/liot-doit-sappuyer-sur-le-libre-24981
tags:
- Entreprise
- Internet
- Innovation
- Standards
---

> Tout comme l'Internet a bouleversé notre quotidien au siècle dernier, la révolution de l'intégration d'objets physiques dans le réseau des réseaux est en marche. On peut raisonnablement se demander s'il s'agit  réellement d'une révolution ou tout simplement de l'exposition d'un ensemble de technologies autrefois ignorées du grand public (voire de certains industriels).
