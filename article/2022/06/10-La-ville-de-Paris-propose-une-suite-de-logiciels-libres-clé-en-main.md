---
site: La gazette.fr
title: "La ville de Paris propose une suite de logiciels libres «clé en main»"
author: Baptiste Cessieux
date: 2022-06-10
href: https://www.lagazettedescommunes.com/811389/la-ville-de-paris-propose-une-suite-de-logiciels-libres-cle-en-main
featured_image: https://www.lagazettedescommunes.com/wp-content/uploads/2019/09/logiciels.jpg
tags:
- Administration
- Sensibilisation
series:
- 202223
series_weight: 0
---

> Éditrice depuis plus de 20 ans de logiciels libres, la ville de Paris a annoncé le 8 juin la création d'une suite informatique prête à l'emploi: «CitéLibre».