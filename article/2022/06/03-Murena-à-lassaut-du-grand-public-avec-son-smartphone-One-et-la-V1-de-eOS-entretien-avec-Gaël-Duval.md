---
site: Next INpact
title: "Murena à l'assaut du grand public avec son smartphone One et la V1 de /e/OS, entretien avec Gaël Duval (€)"
author: Vincent Hermann
date: 2022-06-03
href: https://www.nextinpact.com/article/69289/murena-a-lassaut-grand-public-avec-son-smartphone-one-et-v1-eos-entretien-avec-gael-duval
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/12232.jpg
tags:
- Vie privée
- Innovation
series:
- 202222
series_weight: 0
---

> La société française Murena a annoncé récemment la «V1» de son système mobile /e/OS et le lancement de son premier smartphone, le Murena One. Nous nous sommes entretenus avec le fondateur et président de l’entreprise, Gaël Duval, sur les objectifs de ce nouveau venu.