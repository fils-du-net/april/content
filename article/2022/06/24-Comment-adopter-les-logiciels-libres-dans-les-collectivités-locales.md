---
site: cio-online.com
title: "Comment adopter les logiciels libres dans les collectivités locales"
author: Bertrand Lemaire
date: 2022-06-24
href: https://www.cio-online.com/actualites/lire-comment-adopter-les-logiciels-libres-dans-les-collectivites-locales-14284.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000018822.jpg
tags:
- Administration
- Promotion
series:
- 202225
series_weight: 0
---

> Ancienne DSI et élue locale, Claudine Chassagne publie aux éditions Territorial «Migrer son système d'information vers les logiciels libres».
