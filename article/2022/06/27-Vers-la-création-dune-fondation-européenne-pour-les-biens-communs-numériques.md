---
site: usine-digitale.fr
title: "Vers la création d'une fondation européenne pour les biens communs numériques?"
author: Alice Vitard
date: 2022-06-27
href: https://www.usine-digitale.fr/article/vers-la-creation-d-une-fondation-europeenne-pour-les-biens-communs-numeriques.N2020607
featured_image: https://www.usine-digitale.fr/mediatheque/7/8/8/001361887_homePageUne/europe.jpg
tags:
- Europe
- Open Data
- Contenus Libres
series:
- 202226
series_weight: 0
---

> 19 Etats membres, sous l'égide de la présidence française, et la Commission européenne ont publié un rapport sur les biens communs numériques, l'ensemble des ressources numériques produites et gérées par une communauté à l'instar de Wikipédia ou Linux. Ils proposent de créer une fondation dédiée, un guichet unique pour distribuer les financements...
