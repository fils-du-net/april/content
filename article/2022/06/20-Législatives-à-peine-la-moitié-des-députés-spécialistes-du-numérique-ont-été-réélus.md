---
site: La Tribune
title: "Législatives: à peine la moitié des députés, spécialistes du numérique, ont été réélus (€)"
author: Sylvain Rolland
date: 2022-06-20
href: https://www.latribune.fr/technos-medias/innovation-et-start-up/elections-legislatives-a-peine-la-moitie-des-deputes-specialistes-du-numerique-ont-ete-reelus-922518.html
featured_image: https://static.latribune.fr/full_width/710800/cedric-villani.jpg
tags:
- Institutions
series:
- 202225
series_weight: 0
---

> Si certaines têtes d'affiche du numérique à l'Assemblée nationale, comme Eric Bothorel (LREM) et Philippe Latombe (Modem), conservent leur siège, d'autres piliers comme Jean-Michel Mis (LREM) et Cédric Villani (Nupes) ont été battus. Le bilan est également mitigé pour les députés membres des diverses commissions et offices parlementaires dédiés au numérique. Etat des lieux.
