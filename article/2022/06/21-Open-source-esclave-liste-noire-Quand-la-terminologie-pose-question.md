---
site: Silicon
title: "Open source: esclave, liste noire... Quand la terminologie pose question"
author: Clément Bohic
date: 2022-06-21
href: https://www.silicon.fr/esclave-liste-noire-open-source-langage-inclusif-441644.html
featured_image: https://www.silicon.fr/wp-content/uploads/2022/06/open-source-langage-inclusif-scaled.jpg
tags:
- Sensibilisation
series:
- 202225
series_weight: 0
---

> «Esclave» et «liste noire» sont deux emblèmes de la chasse aux termes connotés que mènent certains projets open source. Le point sur quelques initiatives.
