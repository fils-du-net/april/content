---
site: ZDNet France
title: "La messagerie open source Thunderbird arrive sur Android!"
author: Liam Tung
date: 2022-06-15
href: https://www.zdnet.fr/actualites/la-messagerie-open-source-thunderbird-arrive-sur-android-39943314.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/Pierre%20temp/Thunderbird%20A__w1200.jpg
tags:
- Innovation
- Associations
series:
- 202224
series_weight: 0
---

> Bonne nouvelle pour les millions d'utilisateurs de Thunderbird: ces derniers peuvent s'attendre à voir apparaître très bientôt une application Thunderbird officielle pour Android.
