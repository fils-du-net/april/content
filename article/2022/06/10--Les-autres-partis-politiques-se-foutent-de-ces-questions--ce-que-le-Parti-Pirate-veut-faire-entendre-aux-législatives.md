---
site: Numerama
title: "«Les autres partis politiques se foutent de ces questions»: ce que le Parti Pirate veut faire entendre aux législatives"
description: "«Le plus gros problème du numérique, c’est la consommation de ressources et d’appareils»"
author: Aurore Gayte
date: 2022-06-10
href: https://www.numerama.com/politique/998153-les-autres-partis-politiques-se-foutent-de-ces-questions-ce-que-le-parti-pirate-veut-faire-entendre-aux-legislatives.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2022/06/florie-marie2-1024x576.jpg
tags:
- Institutions
series:
- 202223
series_weight: 0
---

> Le Parti pirate présente des candidats dans près d’une centaine de circonscriptions lors de ces élections législatives. Entretien avec Florie Marie, la porte-parole française et Vice-présidente du Parti pirate européen, pour discuter des engagements et des propositions de ce parti politique unique en son genre. Avec un thème-clé: le renforcement de l’éducation au numérique est indispensable dans la société de demain.