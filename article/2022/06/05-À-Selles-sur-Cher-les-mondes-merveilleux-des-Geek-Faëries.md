---
site: Nouvelle République
title: "À Selles-sur-Cher, les mondes merveilleux des Geek Faëries"
author: Pierre Calmeilles
date: 2022-06-05
href: https://www.lanouvellerepublique.fr/loir-et-cher/commune/selles-sur-cher/a-selles-sur-cher-les-mondes-merveilleux-des-geek-faeries
featured_image: https://images.lanouvellerepublique.fr/image/upload/t_1020w/f_auto/629bfc0aa756d8f16a8b4773.jpg
tags:
- Promotion
series:
- 202222
series_weight: 0
---

> Le festival dédié à Internet et aux mondes imaginaires se poursuit ce dimanche. Et il y en a pour tout le monde, du spécialiste au débutant.
