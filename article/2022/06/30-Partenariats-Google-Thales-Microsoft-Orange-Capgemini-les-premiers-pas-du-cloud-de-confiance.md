---
site: Le Monde.fr
title: "Partenariats Google-Thales, Microsoft-Orange-Capgemini: les premiers pas du «cloud de confiance» (€)"
author: Alexandre Piquard
date: 2022-06-30
href: https://www.lemonde.fr/economie/article/2022/06/30/partenariats-google-thales-microsoft-orange-capgemini-les-premiers-pas-du-cloud-de-confiance_6132711_3234.html#xtor=AL-32280270-%5Btwitter%5D-%5Bios%5D
featured_image: https://img.lemde.fr/2022/06/30/0/0/2681/1772/1328/0/45/0/f0bd1e3_1656574484147-y-colcanopa-20220630-image0.jpeg
tags:
- Informatique en nuage
series:
- 202226
---

> Malgré des avancées, l'essor de ces alliances américano-européennes dans l'hébergement en ligne de données et de logiciels à l'usage des professionnels suscite des critiques.
