---
site: Le Monde Informatique
title: "Les risques sur les logiciels open source persistent"
author: John P. Mello Jr
date: 2022-06-27
href: https://www.lemondeinformatique.fr/actualites/lire-les-risques-sur-les-logiciels-open-source-persistent-87206.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000086510.jpg
tags:
- Innovation
series:
- 202226
series_weight: 0
---

> Open Source: La sécurité reste une forte interrogation de la part des entreprises. Elles souhaitent de plus en plus prendre en compte cette problématique
