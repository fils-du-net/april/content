---
site: cio-online.com
title: "L'Adullact fête ses vingt ans de services numériques aux collectivités locales"
author: Bertrand Lemaire
date: 2022-06-17
href: https://www.cio-online.com/actualites/lire-l-adullact-fete-ses-vingt-ans-de-services-numeriques-aux-collectivites-locales-14269.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000018811.jpg
tags:
- Associations
- Administration
series:
- 202224
series_weight: 0
---

> L'Adullact a fêté ses vingt ans lors de son congrès annuel du 15 au 17 juin 2022 à Montpellier, entre révolution et continuité.
