---
site: Le Monde Informatique
title: "Investir dans la sécurité de l'open source n'est pas une fin en soi"
author: Matt Asay
date: 2022-06-05
href: https://www.lemondeinformatique.fr/actualites/lire-investir-dans-la-securite-de-l-open-source-n-est-pas-une-fin-en-soi-86894.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000085893.jpg
tags:
- Économie
series:
- 202222
series_weight: 0
---

> Les fonds promis pour sécuriser les logiciels open source constituent un début important, mais la créativité des pirates et la prolifération des cibles font que cela n'apporte aucune garantie.