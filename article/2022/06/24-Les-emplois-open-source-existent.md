---
site: ZDNet France
title: "Les emplois open source existent"
author: Steven Vaughan-Nichols
date: 2022-06-24
href: https://www.zdnet.fr/actualites/les-emplois-open-source-existent-39943852.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/01/dev_emploi__w1200.jpg
tags:
- Entreprise
- Économie
series:
- 202225
series_weight: 0
---

> La Fondation Linux constate que les emplois dans le domaine de l'open source sont de plus en plus populaires.
