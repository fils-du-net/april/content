---
site: Heidi.news
title: "L'ingénieur qui défie la propriété intellectuelle pour une meilleure santé"
author: Aylin Elci
date: 2022-06-13
href: https://www.heidi.news/sante-alimentation/l-ingenieur-qui-defie-la-propriete-intellectuelle-pour-une-meilleure-sante
featured_image: https://heidi-17455.kxcdn.com/photos/6e36cbac-8524-4704-92a1-657c382c1995/large
tags:
- Sciences
- Innovation
series:
- 202224
series_weight: 0
---

> La pandémie de Covid-19 a révélé les limites du secteur de la santé. L'ingénieur indien Vaibhav Chhabra, qui a participé au Geneva Health Forum, veut valoriser la capacité des gens à fabriquer, créer et innover collectivement pour dépasser les défis du secteur.
