---
site: Numerama
title: "Stanislas Guérini perd la gestion du cloud de l’État, car Google emploie son épouse"
description: La Première ministre prend la main
author: Julien Lausson
date: 2022-06-08
href: https://www.numerama.com/politique/994751-stanislas-guerini-perd-la-gestion-du-cloud-de-letat-car-google-emploie-son-epouse.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2022/06/stanislas-guerini.jpg?resize=1024,576
tags:
- Institutions
- Informatique en nuage
series:
- 202223
series_weight: 0
---

> Stanislas Guérini, le ministre de la Transformation et de la Fonction publiques, voit ses attributions réduites en tant que ministre: il n’a plus le droit de s’occuper ni de Google, ni d’Alphabet encore moins de cloud. Son épouse travaille en effet pour Google.