---
site: Journal du Net
title: "Le web3 va-t-il tuer le web?"
author: Guillaume Renouard
date: 2022-06-02
href: https://www.journaldunet.com/media/publishers/1512175-le-web3-va-t-il-tuer-le-web
featured_image: https://img-0.journaldunet.com/UoZmw3koGm7vUsWrRgare4Nna30=/1500x/smart/3360e2d93a0246179512522bd6573087/ccmcms-jdn/34985901.jpg
tags:
- Internet
series:
- 202222
---

> Le web3 va-t-il tuer le web ? Le web3 peut donner un nouveau souffle à la sphère numérique en brisant les monopoles et redonnant le pouvoir aux utilisateurs. Une vision qui fait toutefois de nombreux sceptiques.
