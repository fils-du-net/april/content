---
site: Le Monde.fr
title: "La transition des collectivités vers le logiciel libre, lente mais inéluctable? (€)"
author: Marius Rivière
date: 2022-06-18
href: https://www.lemonde.fr/pixels/article/2022/06/18/la-transition-des-collectivites-vers-le-logiciel-libre-lente-mais-ineluctable_6130924_4408996.html
featured_image: https://img.lemde.fr/2022/06/17/0/0/2100/1400/1328/0/45/0/bc0be98_1655474679430-logiciel-libre-open-source.jpg
tags:
- Administration
- Logiciels privateurs
series:
- 202224
series_weight: 0
---

> A l’aune du Covid-19 et de la guerre en Ukraine, le logiciel libre apparaît plus que jamais comme un outil de souveraineté numérique. Son adoption par l’administration ne se fait pourtant pas sans heurts.
