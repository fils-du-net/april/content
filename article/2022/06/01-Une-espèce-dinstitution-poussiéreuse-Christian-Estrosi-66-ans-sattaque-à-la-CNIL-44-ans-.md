---
site: Next INpact
title: "Une «espèce d'institution poussiéreuse»: Christian Estrosi (66 ans) s'attaque à la CNIL (44 ans) (€)"
author: Marc Rees
description: La baie des anges passe
date: 2022-06-01
href: https://www.nextinpact.com/article/69293/une-espece-dinstitution-poussiereuse-christian-estrosi-66-ans-sattaque-a-cnil-44-ans
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/12226.jpg
tags:
- Vie privée
- Institutions
series:
- 202222
series_weight: 0
---

> Sur Europe 1 hier, Christian Estrosi est revenu à la charge en faveur de la reconnaissance faciale. Une solution prônée en réaction aux incidents survenus au Stade de France samedi. Il s’en est pris une nouvelle fois à la CNIL, cette «espèce d’institution poussiéreuse». Une excellente occasion de réexpliquer au maire de Nice quelques fondamentaux.