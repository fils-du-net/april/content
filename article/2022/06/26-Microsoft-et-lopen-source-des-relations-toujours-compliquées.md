---
site: Le Monde Informatique
title: "Microsoft et l'open source: des relations toujours compliquées"
author: Matt Asay
date: 2022-06-26
href: https://www.lemondeinformatique.fr/actualites/lire-microsoft-et-l-open-source-des-relations-toujours-compliquees-87196.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000086490.jpg
tags:
- Entreprise
- Logiciels Privateurs
series:
- 202225
series_weight: 0
---

> La décision de rendre l'extension C# de Visual Studio Code propriétaire suscite des critiques, mais Microsoft reste un partisan résolu de l'open source.
