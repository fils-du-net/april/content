---
site: ZDNet France
title: "Collectivités locales et logiciel libre: l'Adullact a vingt ans"
author: Thierry Noisette
date: 2022-06-19
href: https://www.zdnet.fr/blogs/l-esprit-libre/collectivites-locales-et-logiciel-libre-l-adullact-a-vingt-ans-39943526.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/ADULLACT_bandeau.jpg
tags:
- Associations
series:
- 202224
---

> L'Association des Développeurs, Utilisateurs de Logiciels Libres pour les Administrations et les Collectivités Territoriales (Adullact) a fêté ses vingt ans. Des milliers de collectivités utilisent ses solutions.
