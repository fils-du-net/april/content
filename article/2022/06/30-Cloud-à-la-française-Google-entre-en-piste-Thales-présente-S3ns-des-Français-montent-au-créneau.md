---
site: Next INpact
title: "Cloud «à la française»: Google entre en piste, Thales présente «S3ns», des Français montent au créneau (€)"
author: Sébastien Gavois
date: 2022-06-30
href: https://www.nextinpact.com/article/69528/cloud-a-francaise-google-entre-en-piste-thales-presente-s3ns-francais-montent-au-creneau
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/12264.jpg
tags:
- Informatique en nuage
series:
- 202226
---

> C’est aujourd’hui que Google Cloud ouvre sa région en France. Pour l’occasion, Thales annonce le lancement de S3ns, son «cloud de confiance» en partenariat avec Google. Hier, trois hébergeurs français (Clever Cloud, OVHcloud et Scaleway) ont publié une tribune commune pour rappeler qu’il existe des acteurs français trop souvent oubliés.
