---
site: Journal du Net
title: "4 risques majeurs liés au passage au cloud et comment les gérer"
author: Thomas Dubus
date: 2022-06-30
href: https://www.journaldunet.com/web-tech/cloud/1512977-4-risques-majeurs-lies-au-passage-au-cloud-et-comment-les-gerer
tags:
- Informatique en nuage
series:
- 202226
series_weight: 0
---

> La migration vers un service de cloud managé est peut-être la transformation la plus importante qu'une entreprise ait à effectuer. Et comme tout grand projet, elle s'accompagne de risques!
