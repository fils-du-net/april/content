---
site: ZDNet France
title: "L'Europe veut promouvoir les communs numériques"
author: Thierry Noisette
date: 2022-06-23
href: https://www.zdnet.fr/blogs/l-esprit-libre/l-europe-veut-promouvoir-les-communs-numeriques-39943762.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2018/12/Bruxelles_Commission_UE_Berlaymont.jpg
tags:
- Europe
- Open Data
- Contenus Libres
series:
- 202225
series_weight: 0
---

> Wikipédia, Linux, OpenStreetMap et Open Food Facts sont cités en exemples par le rapport du groupe de travail sur les communs numériques de l'Assemblée numérique de l'Union européenne.
