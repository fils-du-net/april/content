---
site: Journal du Net
title: "Smartphone: quid de l'obsolescence logicielle?"
author: Agnès Crépet
date: 2022-06-13
href: https://www.journaldunet.com/ebusiness/telecoms-fai/1512419-smartphone-quid-de-l-obsolescence-logicielle
tags:
- Innovation
series:
- 202224
series_weight: 0
---

> Si l'obsolescence matérielle est à l'agenda des acteurs depuis longtemps, il serait temps qu'il en soit de même pour l'obsolescence logicielle.
