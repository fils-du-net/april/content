---
site: La gazette.fr
title: "Les logiciels libres, un choix engagé… et engageant!"
author: Baptiste Cessieux
date: 2022-06-30
href: https://www.lagazettedescommunes.com/813690/les-logiciels-libres-un-choix-engage-et-engageant%E2%80%89
featured_image: https://www.lagazettedescommunes.com/wp-content/uploads/2022/06/logicel-libre-code.jpg
tags:
- Administration
- Sensibilisation
series:
- 202226
series_weight: 0
---

> Les logiciels libres permet de bénéficier des avancées faites par d’autres acteurs et d’échanger sur les siennes. Toutes les collectivités peuvent se tourner vers les logiciels libres, et cette démarche est plus simple lorsqu’elle est mutualisée. Pour assurer leur bon fonctionnement, l’abonnement à un logiciel propriétaire est remplacé par un contrat de maintenance, interne ou externe.
