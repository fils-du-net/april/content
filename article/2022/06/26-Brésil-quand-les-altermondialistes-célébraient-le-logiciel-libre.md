---
site: ZDNet France
title: "Brésil: quand les altermondialistes célébraient le logiciel libre"
author: Thierry Noisette
date: 2022-06-26
href: https://www.zdnet.fr/blogs/l-esprit-libre/bresil-quand-les-altermondialistes-celebraient-le-logiciel-libre-39943886.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/Lula_da_Silva_and_GIlberto_Gil_WMC.jpg
tags:
- International
- Économie
series:
- 202225
series_weight: 0
---

> Archive. Gilberto Gil a 80 ans ce 26 juin, l'occasion d'un souvenir du musicien brésilien, alors ministre de la Culture, au Forum social mondial de Porto Alegre en 2005, louant les logiciels libres et les hackers, au côté de Lawrence Lessig et John Perry Barlow.
