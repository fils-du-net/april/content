---
site: Le Monde.fr
title: "«L'open data donne les moyens de s'adapter aux crises» (€)"
author: Jean-Marc Lazard, Akim Oural
date: 2022-04-14
href: https://www.lemonde.fr/idees/article/2022/04/14/l-open-data-donne-les-moyens-de-s-adapter-aux-crises_6122102_3232.html
tags:
- Open Data
series:
- 202216
series_weight: 0
---

> Jean-Marc Lazard et Akim Oural, promoteurs de l'open data, expliquent, dans une tribune au «Monde», pourquoi les collectivités locales doivent se saisir de ce sujet, afin d'éviter une fracture sociale entre grandes villes et monde rural.