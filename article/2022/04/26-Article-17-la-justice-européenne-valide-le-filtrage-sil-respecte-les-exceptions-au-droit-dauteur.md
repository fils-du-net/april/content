---
site: Next INpact
title: "Article 17: la justice européenne valide le filtrage, s'il respecte les exceptions au droit d'auteur (€)"
description: "#Boncourage"
author: Marc Rees
date: 2022-04-26
href: https://www.nextinpact.com/article/69015/article-17-justice-europeenne-valide-filtrage-sil-respecte-exceptions-au-droit-dauteur
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/277.jpg
tags:
- Droit d'auteur
- Internet
- Europe
series:
- 202217
series_weight: 0
---

> Au terme d’un arrêt de 31 pages, la Cour de justice de l’Union européenne rejette le recours de la Pologne contre l’article 17 de la directive droit d’auteur. L’article orchestre une obligation de filtrage sur l’ensemble des plateformes comme YouTube. Le juge européen a néanmoins multiplié les rappels aux garanties que devront respecter les États membres.