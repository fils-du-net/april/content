---
site: Acteurs Publics
title: "Les communs numériques: la réponse que l'on attendait pour la souveraineté digitale de l'UE"
author: Thomas Belarbi
date: 2022-04-13
href: https://acteurspublics.fr/articles/les-communs-numeriques-la-reponse-que-lon-attendait-pour-la-souverainete-digitale-de-lue
featured_image: https://acteurspublics.fr/upload/media/default/0001/39/662c2748dbae109be27f19679bb0d50942b4219c.jpeg
tags:
- Europe
- Économie
series:
- 202215
series_weight: 0
---

> Forte de sa présidence au Conseil de l’Union Européenne, la France a fait de la souveraineté numérique une de ses priorités. Au cœur de sa stratégie: les communs numériques. Une tribune de Thomas Belarbi, responsable stratégie et développement Secteur Public chez Red Hat.