---
site: Acteurs Publics 
title: "Aux Armées, le grand saut vers le logiciel libre n'est pas pour tout de suite (€)"
author: Emile Marzolf
date: 2022-04-14
href: https://acteurspublics.fr/articles/aux-armees-le-grand-saut-vers-le-logiciel-libre-nest-pas-pour-tout-de-suite
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/39/c22f6467801caa21aefabc0e58172f8b1552a956.jpeg
tags:
- Administration
series:
- 202215
---

> Afin de se défaire un peu de l’emprise de Microsoft, le ministère des Armées a mené une étude pour basculer le poste de travail d’une partie de son personnel sur des logiciels libres. Une migration “techniquement possible”, mais toujours pas à l’ordre du jour.