---
site: Clubic.com
title: "Sécurité: les systèmes Linux sont-ils aussi invulnérables qu'on le pense?"
author: Benoit Bayle
date: 2022-04-11
href: https://www.clubic.com/linux-os/debian/ubuntu/dossier-417288-linux-est-il-aussi-invulnerable-qu-on-le-pense.html
featured_image: https://pic.clubic.com/v1/images/1928736/raw.webp?hash=69296c0d97475639de11169bbf40935ac0261d49
tags:
- Sensibilisation
series:
- 202215
series_weight: 0
---

> On l'a souvent entendu: si l'on veut être en sécurité sur internet, il est beaucoup mieux d'utiliser Linux plutôt que Windows ou macOS. Si cette affirmation était globalement vraie et vérifiable il y a quelques années, l'évolution de la technologie et surtout l'évolution constante des menaces sur le web ont eu le don de modifier les faits. Pourtant, la perception reste aujourd'hui la même à propos du système créé par Linus Torvalds. Alors, Linux est-il aussi invulnérable qu'on le pense, ou s'agit-il d'un mythe?