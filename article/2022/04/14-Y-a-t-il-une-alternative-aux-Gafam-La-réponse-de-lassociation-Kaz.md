---
site: La Vie
title: "Y a-t-il une alternative aux Gafam? La réponse de l'association Kaz"
author: Manon Boquen
date: 2022-04-14
href: https://www.lavie.fr/actualite/societe/y-a-t-il-une-alternative-aux-gafam-la-reponse-de-lassociation-kaz-81845.php
featured_image: https://medias.lavie.fr/api/v1/images/view/6257d3af15d03e0cae2e48c7/width_1000/image.jpg
tags:
- Associations
- Internet
series:
- 202215
series_weight: 0
---

> Un groupe de défenseurs du logiciel libre a créé Kaz, un hébergeur local et éthique, en juin 2021, à Vannes (Morbihan). Les premiers résultats montrent qu’il est en effet possible de «dégoogliser» les usages numériques.