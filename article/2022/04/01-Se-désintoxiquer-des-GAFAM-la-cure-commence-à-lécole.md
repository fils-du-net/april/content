---
site: Opinion Internationale
title: "Se désintoxiquer des GAFAM: la cure commence à l'école!"
description: "Un lycée des Hauts-de-France s'évertue à libérer ses élèves de la dépendance aux GAFAM. Envers et contre tous?!"
author: Michel Taube
date: 2022-04-01
href: https://www.opinion-internationale.com/2022/04/01/se-desintoxiquer-des-gafam-la-cure-commence-a-lecole_105931.html
featured_image: https://www.opinion-internationale.com/wp-content/uploads/2022/04/linux2.png
tags:
- Éducation
series:
- 202213
series_weight: 0
---

> Préambule: pour écrire cet article, nous avons fait l’expérience du logiciel libre, en installant une «machine virtuelle» Linux (distribution Ubuntu) dans Windows, puis la suite bureautique «Libre Office», avec traitement de texte, tableur, outils internet et tutti quanti. Premier constat: c’est plutôt simple, agréable à utiliser, fonctionne parfaitement. Et bien sûr, c’est gratuit: pas de licence, pas de frais.