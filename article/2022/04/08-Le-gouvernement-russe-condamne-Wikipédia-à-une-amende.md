---
site: ActuaLitté.com
title: "Le gouvernement russe condamne Wikipédia à une amende"
author: Fasseur Barbara
date: 2022-04-08
href: https://actualitte.com/article/105527/acteurs-numeriques/le-gouvernement-russe-condamne-wikipedia-a-une-amende
featured_image: https://actualitte.com/uploads/images/photo-1646048033628-0265e23e1448-625010c9a4430978438323.jpeg
tags:
- Partage du savoir
- International
series:
- 202214
series_weight: 0
---

> Le Service fédéral de supervision des communications, des technologies de l’information et des médias de masse russe ou Roskomnadzor fait des siennes. Ce 31 mars, il a demandé à l’encyclopédie en ligne de supprimer une page contenant «des éléments non fiables d’importance sociale, ainsi que d’autres informations interdites» concernant ses opérations en Ukraine.