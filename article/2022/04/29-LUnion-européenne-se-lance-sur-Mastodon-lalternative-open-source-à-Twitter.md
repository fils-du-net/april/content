---
site: Clubic.com
title: "L'Union européenne se lance sur Mastodon, l'alternative open source à Twitter"
author: Alexandre Boero
date: 2022-04-29
href: https://www.clubic.com/pro/blog-forum-reseaux-sociaux/actualite-420261-l-union-europeenne-se-lance-sur-mastodon-l-alternative-open-source-a-twitter.html
featured_image: https://pic.clubic.com/v1/images/1999164/raw
tags:
- Internet
- Institutions
- Europe
series:
- 202217
series_weight: 0
---

> Le Contrôleur européen de la protection des données (CEPD) a annoncé ce jeudi s'être lancé sur les plateformes fondées sur les logiciels Mastodon et PeerTube.