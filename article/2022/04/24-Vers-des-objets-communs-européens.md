---
site: ZDNet France
title: "Vers des objets communs européens"
author: Frédéric Charles
date: 2022-04-24
href: https://www.zdnet.fr/blogs/green-si/vers-des-objets-communs-europeens-39940973.htm
featured_image: https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEiUgllPR_kh2s25_G2JS8YNYj3o8eOTF9t42kOiiXk-kY8YPIGNPTCNrVODGZY6yTWCSd-l1mlYSYOXQJg1sReDSePVtUPtkCJjHbshGU7U9TNbZRWlPmvc5OUzsl9a25waGKxjsY1aLlR1G61MNPgtqMsHm9W23I0e9pNXTP9gODkJ4zOC8nnlosWg2A/w640-h368/Open%20europe.jpg
tags:
- Interopérabilité
- Europe
series:
- 202216
series_weight: 0
---

> Passer d'un marché commun européen, à des objets communs européens, facilitant la libre circulation des données en Europe?