---
site: ZDNet France
title: "Le Libre, catalyseur de projets responsables"
author: Thierry Noisette
date: 2022-04-20
href: https://www.zdnet.fr/blogs/l-esprit-libre/le-libre-catalyseur-de-projets-responsables-39940829.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/Jerry-can..JPG
tags:
- Innovation
series:
- 202216
series_weight: 0
---

> Smartphone, prothèses, PC en éléments de réemploi, recyclage de plastique, équipements agricoles: à la Journée du Libre éducatif, des exemples parlants de démarche libriste au-delà du seul logiciel.