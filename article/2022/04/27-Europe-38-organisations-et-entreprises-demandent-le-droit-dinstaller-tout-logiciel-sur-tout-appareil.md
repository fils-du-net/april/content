---
site: ZDNet France
title: "Europe: 38 organisations et entreprises demandent le droit d'installer tout logiciel sur tout appareil"
author: Thierry Noisette
date: 2022-04-27
href: https://www.zdnet.fr/blogs/l-esprit-libre/europe-38-organisations-et-entreprises-demandent-le-droit-d-installer-tout-logiciel-sur-tout-appareil-39941183.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2018/04/copyright-UE_Pixabay_40632_960_720.png
tags:
- Vente liée
- Interopérabilité
- Europe
series:
- 202217
series_weight: 0
---

> La Free Software Foundation Europe (FSFE) et 37 organisations et entreprises demandent aux législateurs de l'UE, au nom de l'éco-conception, de permettre «une économie numérique plus durable».