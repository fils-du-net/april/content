---
site: L'usine Nouvelle
title: "La souveraineté numérique européenne n'est pas un frein à l'attractivité économique!"
author: Fabien Aufrechter
date: 2022-04-18
href: https://www.usinenouvelle.com/blogs/fabien-aufrechter/la-souverainete-numerique-europeenne-n-est-pas-un-frein-a-l-attractivite-economique.N1994582
featured_image: https://www.usinenouvelle.com/mediatheque/2/3/0/001084032_image_896x598.jpg
tags:
- Économie
- Europe
series:
- 202216
series_weight: 0
---

> Il y a encore quelques années, le mot souveraineté était un épouvantail marqué des extrêmes, synonyme de nationalisme voire d’isolationnisme. Mais au cours de ces dernières années, la sémantique a glissé, et la souveraineté est devenue une valeur étendard d’une Union européenne plus forte face à un contexte global toujours plus complexe. Au point que lors d’un discours crucial au milieu d’une guerre aux portes de l’Europe, le président Emmanuel Macron plaide pour une Europe «qui doit penser (...) entre autres choses évidemment, sa souveraineté technologique». La souveraineté technologique n’est plus vue comme un obstacle, mais bien comme une opportunité, voir une nécessité.