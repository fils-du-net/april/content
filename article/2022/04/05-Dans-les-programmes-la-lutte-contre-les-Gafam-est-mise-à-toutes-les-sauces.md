---
site: Mediapart
title: "Dans les programmes, la lutte contre les Gafam est mise à toutes les sauces"
author: Jérôme Hourdeaux
date: 2022-04-05
href: https://www.mediapart.fr/journal/france/050422/dans-les-programmes-la-lutte-contre-les-gafam-est-mise-toutes-les-sauces
featured_image: https://static.mediapart.fr/etmagine/default/files/2022/03/25/080-hl-dhimbert-1135057.jpg
tags:
- Institutions
series:
- 202214
series_weight: 0
---

> Malgré leurs différences d’approche du numérique, la quasi-totalité des candidates et candidats se retrouve sur la nécessité d’assurer la souveraineté numérique de la France et de limiter l’influence des géants du Net.