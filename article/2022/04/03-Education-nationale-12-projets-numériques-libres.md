---
site: ZDNet France
title: "Education nationale: 12 projets numériques libres"
author: Thierry Noisette
date: 2022-04-03
href: https://www.zdnet.fr/blogs/l-esprit-libre/education-nationale-12-projets-numeriques-libres-39939967.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/JDLE2022.jpg
tags:
- Éducation
series:
- 202214
series_weight: 0
---

> Lyon a accueilli près de 400 personnes à la première Journée du Libre éducatif, occasion de rencontres et de présentations de nombreux projets pédagogiques de logiciels libres et communs numériques.