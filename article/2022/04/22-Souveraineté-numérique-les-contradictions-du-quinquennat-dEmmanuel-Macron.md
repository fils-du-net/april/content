---
site: Marianne
title: "Souveraineté numérique: les contradictions du quinquennat d'Emmanuel Macron"
author: Axel Perru
date: 2022-04-22
href: https://www.marianne.net/politique/gouvernement/souverainete-numerique-les-contradictions-du-quinquennat-demmanuel-macron
featured_image: https://resize.marianne.net/r/1540,924/img/var/LQ9064425C/641458/045_ISC12429279.jpg
tags:
- Institutions
series:
- 202216
series_weight: 0
---

> Le président sortant a réaffirmé, dans une interview accordée ce jeudi 21 avril au média spécialisé The Big Whale, sa volonté de construire la souveraineté de la France et l'Europe dans le domaine numérique. Des cas pratiques montrent pourtant que son gouvernement a laissé le soin aux sociétés américaines de gérer les données des Français.