---
site: Numerama
title: "La France insoumise retente de taxer le domaine public"
author: Julien Lausson
date: 2022-10-03
href: https://www.numerama.com/politique/1132154-la-france-insoumise-retente-de-taxer-le-domaine-public.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2019/01/joconde-leonard-de-vinci-peinture-1024x576.jpg?webp=1&key=7bc97141
tags:
- Droit d'auteur
- Institutions
series:
- 202240
series_weight: 0
---

> Pour aider la création d’aujourd’hui, La France insoumise souhaite taxer les œuvres d’hier. Un amendement a été déposé pour taxer les bénéfices sur l’exploitation commerciale des œuvres du domaine public. Mais la proposition fait polémique.
