---
site: Next INpact
title: "Une partie de la gestion du «Zoom» de l'enseignement supérieur français sous-traitée (€)"
description: "BBBFac"
author: Martin Clavey
date: 2022-10-13
href: https://www.nextinpact.com/article/70129/une-partie-gestion-zoom-enseignement-superieur-francais-sous-traitee
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/9021.jpg
tags:
- Éducation
series:
- 202241
---

> Le ministère de l'Enseignement supérieur et la recherche préfère finalement passer par un prestataire privé, Arawa, pour gérer le Big Blue Button de ses classes virtuelles et reconcentrer le projet sur les webinaires à audience massive.
