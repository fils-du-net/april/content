---
site: Acteurs Publics 
title: "Les agents publics, une source de contribution pour Wikipédia? (€)"
author: Emile Marzolf
date: 2022-10-21
href: https://acteurspublics.fr/articles/les-agents-publics-une-source-de-contribution-pour-wikipedia
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/41/e820e0d4a6dde6cd5c6b7543a3dc24df3a268805.jpeg
tags:
- Partage du savoir
- Administration
series:
- 202242
series_weight: 0
---

> Le Conseil national du numérique (CNNum) a organisé le 20 octobre un atelier avec ses membres et des agents publics pour une initiation à la contribution à Wikipédia et donc aux communs numériques. Le CNNum montre l’exemple et troque ses outils internes contre des logiciels libres.
