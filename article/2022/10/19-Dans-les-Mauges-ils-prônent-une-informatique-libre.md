---
site: ouest-france.fr
title: "Dans les Mauges, ils prônent une informatique libre"
date: 2022-10-19
href: https://www.ouest-france.fr/pays-de-la-loire/chemille-en-anjou-49120/dans-les-mauges-ils-pronent-une-informatique-libre-a3847c8a-4975-11ed-ab19-608ef59c9fc6
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMjEwZDMxNGZlNTI5MzM1ZGEzNTk4ZmIzMzk0MDJhOTBmYzI?width=1260&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=fd33335efc86d74a9008b8ebe322796d6c1465675165fcbff964ccb42b27a5b7
tags:
- Associations
- Promotion
series:
- 202242
---

> À partir du jeudi 20 octobre et pendant dix jours, au centre social du Chemillois, des bénévoles présenteront les logiciels libres et aideront à les mettre en place sur votre ordinateur.
