---
site: "Chut!"
title: "L'Etat cultive un esprit libriste"
author: "Lec'hvien Julien"
date: 2022-10-14
href: https://chut.media/tech/letat-cultive-un-esprit-libriste
featured_image: https://chut.media/wp-content/uploads/2022/10/nec-jeudi-bd.marion-bornaz.13-1.jpg
tags:
- Institutions
- Éducation
series:
- 202242
series_weight: 0
---

> De nombreux·euse·s défenseur·euse·s des communs numériques étaient invité·e·s à s’exprimer lors de la cinquième édition Numérique En Commun[s] (NEC). Un signe que la culture du logiciel libre fait des émules au sein de l’Etat.
