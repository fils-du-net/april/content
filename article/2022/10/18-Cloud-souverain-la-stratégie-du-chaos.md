---
site: La Tribune
title: "Cloud souverain: la stratégie du chaos"
author: Sylvain Rolland
date: 2022-10-18
href: https://www.latribune.fr/technos-medias/internet/cloud-de-confiance-la-strategie-du-chaos-937067.html
featured_image: https://static.latribune.fr/full_width/1354613/cloud-act-h318-p-7-informatique-donnees-data-droits-gafa.jpg
tags:
- Informatique en nuage
- Entreprise
- Administration
series:
- 202242
---

> DÉCRYPTAGE. Le leader mondial du secteur, Amazon Web Services, et le champion tricolore déchu des services informatiques, Atos, s'allieraient pour proposer une nouvelle solution de Cloud de confiance, après Bleu (Microsoft avec Orange et Capgemini) et S3ns (Google avec Thales). Au-delà des graves problèmes actuels d'Atos, cette nouvelle solution s'inscrit dans une stratégie cloud nationale devenue presque illisible en raison des multiples revirements du gouvernement et du casse-tête juridique provoqué par l'intégration des Gafam. La Tribune fait le point.
