---
site: ouest-france.fr
title: "Bannalec. L’@ssourie lutte contre l’illectronisme"
date: 2022-10-21
href: https://www.ouest-france.fr/bretagne/bannalec-29380/bannalec-l-atssourie-lutte-contre-l-illectronisme-494c5202-4ded-11ed-97eb-183f9a152ae8
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMjEwN2QwOGM0M2NmY2ZmMzMxZWYzNzBjNWRiZWZlNWIzMDM?width=1260&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=b9e70592c4a466565ddd83d8e2facfcee5ba47ebc41c5741275e7745ed76ee5f
tags:
- Associations
series:
- 202242
---

> À Bannalec (Finistère), l’association L’@ssourie a répété son objectif de réduire la fracture numérique auprès des habitants, lors de son assemblée générale.
