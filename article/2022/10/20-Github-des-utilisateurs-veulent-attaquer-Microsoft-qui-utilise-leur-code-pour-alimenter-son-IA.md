---
site: Clubic.com
title: "Github: des utilisateurs veulent attaquer Microsoft qui utilise leur code pour alimenter son IA"
author: Robin Lamorlette
date: 2022-10-20
href: https://www.clubic.com/pro/entreprises/microsoft/actualite-442695-github-des-utilisateurs-veulent-attaquer-microsoft-qui-utilise-leur-code-pour-alimenter-leur-ia.html
featured_image: https://pic.clubic.com/v1/images/1927773/raw.webp?fit=max&width=1200&hash=0697595915659e6faf88b385c74c55151da194dd
tags:
- Logiciels privateurs
- Entreprise
- Licenses
series:
- 202242
series_weight: 0
---

> Copilot, un outil de Microsoft boosté par l'IA, fait face à une potentielle action de groupe de codeurs open-source sur Github.
