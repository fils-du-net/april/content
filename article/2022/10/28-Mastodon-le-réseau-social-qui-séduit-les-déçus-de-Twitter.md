---
site: TF1 INFO
title: "Mastodon, le réseau social qui séduit les déçus de Twitter"
author: Matthieu Delacharlery
date: 2022-10-28
href: https://www.tf1info.fr/high-tech/mastodon-le-reseau-social-qui-seduit-les-decus-de-twitter-rachate-par-elon-musk-2236932.html
featured_image: https://photos.tf1info.fr/images/1280/871/illustration-mastodon-1-aab91c-0@1x.png
tags:
- Internet
series:
- 202243
---

> De nombreux utilisateurs, effrayés par la prise de contrôle par Elon Musk de Twitter, annoncent vouloir quitter la plateforme à l'oiseau bleu.
