---
site: Le Monde Informatique
title: "Les modèles d'affaires open source: une liberté sous contrôle"
author: Nicolas Jullien
date: 2022-10-26
href: https://www.lemondeinformatique.fr/actualites/lire-les-modeles-d-affaires-open-source-une-liberte-sous-controle-88439.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000088569.jpg
tags:
- Economie
- Entreprise
- Innovation
- Sensibilisation
series:
- 202243
series_weight: 0
---

> Les offres commerciales basées sur des logiciels libres sont nombreuses et il n'est pas toujours facile de s'y retrouver, ni dans leur intérêt, ni dans les degrés de liberté qu'elles donnent aux clients.
