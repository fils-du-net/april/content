---
site: Le Monde Informatique
title: "En déclin, Mozilla en quête de bouc émissaire"
author: Matt Asay
date: 2022-10-05
href: https://www.lemondeinformatique.fr/actualites/lire-en-declin-mozilla-en-quete-de-bouc-emissaire-88198.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000088157.jpg
tags:
- Internet
series:
- 202240
series_weight: 0
---

> La fondation Mozilla éprouve des difficultés récurrentes à enrayer une inexorable chute de ses parts de marché dans les navigateurs. Un problème sans doute moins à chercher du côté des pratiques anticoncurrentielles que de la concurrence elle-même.
