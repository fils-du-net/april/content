---
site: Numerama
title: "Vouloir partir de Twitter à cause d'Elon Musk ne sert à rien"
description: "Twitter est déjà l'un des endroits les plus toxiques d'Internet"
author: Aurore Gayte
date: 2022-10-28
href: https://www.numerama.com/tech/1164616-vouloir-partir-de-twitter-a-cause-delon-musk-ne-sert-a-rien.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2022/10/elon-musk-3-1024x576.jpg?webp=1&key=0eef9e78
tags:
- Internet
series:
- 202243
---

> Le rachat du réseau social Twitter par Elon Musk suscite de nombreuses inquiétudes quant à la modération de la plateforme. Mais, Twitter est déjà infernal et l’on peut douter que beaucoup de choses changent.
