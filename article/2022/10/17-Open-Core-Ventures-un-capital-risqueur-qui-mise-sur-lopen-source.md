---
site: ZDNet France
title: "Open Core Ventures, un capital-risqueur qui mise sur l'open source"
author: Thierry Noisette
date: 2022-10-17
href: https://www.zdnet.fr/blogs/l-esprit-libre/open-core-ventures-un-capital-risqueur-qui-mise-sur-l-open-source-39948536.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/money-1090815_960_720_Pixabay.jpg
tags:
- Entreprise
series:
- 202242
series_weight: 0
---

> L'entreprise de capital-risque OCV, qui a notamment cofinancé Fleet DM, maintenant valorisée 100 millions de dollars, investit dans des compagnies qu'elle crée à partir de projets jugés suffisamment prometteurs.
