---
site: ActuaLitté.com
title: "Wikimédia crée un label pour soutenir la culture libre"
date: 2022-10-11
href: https://actualitte.com/article/108207/acteurs-numeriques/wikimedia-cree-un-label-pour-soutenir-la-culture-libre
featured_image: https://actualitte.com/media/cache/width_944_webp/uploads/images/wikimedia-lance-label-culture-libre-soutenir-open-content-634432a047d01993738814.jpg
tags:
- Open Data
- Partage du savoir
series:
- 202241
series_weight: 0
---

> Le 27 octobre 2022, Wikimédia France lancera l’appel à candidatures pour le Label Culture Libre, premier label dédié aux institutions culturelles qui intègrent l’open content [œuvre dont la diffusion et la modification sont libres] dans leurs pratiques numériques. Ce projet s'inscrit dans une démarche globale de sensibilisation à l’open content dont la première étape a été la publication du Rapport sur l’open content réalisé par l’agence Phare pour le compte et en partenariat avec Wikimédia France.
