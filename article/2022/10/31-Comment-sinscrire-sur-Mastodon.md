---
site: Les Numeriques
title: "Comment s'inscrire sur Mastodon?"
author: Corentin Bechade
date: 2022-10-31
href: https://www.lesnumeriques.com/divers/comment-s-inscrire-sur-mastodon-n194949.html
featured_image: https://cdn.lesnumeriques.com/optim/news/19/194949/3d81b391-comment-s-inscrire-sur-mastodon.webp
tags:
- Internet
- april
series:
- 202244
series_weight: 0
---

> Vous souhaitez quitter Twitter pour Mastodon, mais vous ne comprenez pas exactement comment marche le réseau social décentralisé et open source? Pas de craintes, voici les quelques trucs à savoir pour s'inscrire.
