---
site: ZDNet France
title: "Le Conseil national du numérique affirme son soutien au Libre"
author: Thierry Noisette
date: 2022-10-23
href: https://www.zdnet.fr/blogs/l-esprit-libre/le-conseil-national-du-numerique-affirme-son-soutien-au-libre-39948826.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/CNNum_logo.jpg
tags:
- Partage du savoir
series:
- 202242
---

> Le Conseil national du numérique (CNNum) promeut à travers l'usage de plusieurs outils les logiciels libres, 'source de convivialité, de partage, de coopération et de liberté'.
