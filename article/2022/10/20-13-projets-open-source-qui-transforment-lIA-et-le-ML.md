---
site: Le Monde Informatique
title: "13 projets open source qui transforment l'IA et le ML"
author: Peter Wayner
date: 2022-10-20
href: https://www.lemondeinformatique.fr/actualites/lire-13-projets-open-source-qui-transforment-l-ia-et-le-ml-88130.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000088036.jpg
tags:
- Innovation
series:
- 202242
series_weight: 0
---

> Des deepfakes au traitement du langage naturel et plus encore, le monde open source regorge de projets visant à soutenir le développement de logiciels aux frontières de l'intelligence artificielle et de l'apprentissage automatique.
