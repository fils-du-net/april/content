---
site: Le Telegramme
title: "À Vannes, le «chaton» Kaz, hébergeur web éthique, a séduit 90 associations en un an"
author: Caroline Lafargue
date: 2022-10-24
href: https://www.letelegramme.fr/morbihan/vannes/a-vannes-le-chaton-kaz-hebergeur-web-ethique-a-seduit-90-associations-en-un-an-24-10-2022-13206557.php
featured_image: https://www.letelegramme.fr/images/2022/10/24/francois-merciol-l-un-des-huit-samourais-de-kaz-lors-d-une_6979414_676x423p.jpg
tags:
- Associations
series:
- 202243
series_weight: 0
---

> Remettre de la chaleur humaine dans la froideur du numérique. Se libérer de l’emprise des grands exploitants de données que sont les Gafam. Tels sont les objectifs de Kaz, hébergeur web local, qui vient de fêter son 1er anniversaire.
