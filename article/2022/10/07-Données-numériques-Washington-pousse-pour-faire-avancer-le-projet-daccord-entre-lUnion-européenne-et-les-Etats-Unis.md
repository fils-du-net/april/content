---
site: Le Monde.fr
title: "Données numériques: Washington pousse pour faire avancer le projet d'accord entre l'Union européenne et les Etats-Unis (€)"
author: Alexandre Piquard
date: 2022-10-07
href: https://www.lemonde.fr/economie/article/2022/10/07/donnees-numeriques-washington-pousse-pour-faire-avancer-le-projet-d-accord-entre-l-union-europeenne-et-les-etats-unis_6144895_3234.html
tags:
- International
- Vie privée
series:
- 202240
---

> Dans un contexte polémique, le président américain, Joe Biden, a signé vendredi un décret proposant un nouveau cadre pour le transfert transatlantique des données.
