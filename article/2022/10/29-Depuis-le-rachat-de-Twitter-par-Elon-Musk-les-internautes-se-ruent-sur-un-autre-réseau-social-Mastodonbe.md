---
site: RTBF
title: "Depuis le rachat de Twitter par Elon Musk, les internautes se ruent sur un autre réseau social: Mastodon.be"
author: Pascale Bollekens
date: 2022-10-29
href: https://www.rtbf.be/article/depuis-le-rachat-de-twitter-par-elon-musk-les-internautes-se-ruent-sur-un-autre-reseau-social-mastodon-11094729
featured_image: https://ds.static.rtbf.be/article/image/1248x702/1/b/5/c275a60ead508a94c074632a1538a429-1666974521.jpg
tags:
- Internet
series:
- 202243
series_weight: 0
---

> Depuis que Twitter est désormais aux mains du milliardaire Elon Musk, nombreux sont les internautes à s'inquiéter de...
