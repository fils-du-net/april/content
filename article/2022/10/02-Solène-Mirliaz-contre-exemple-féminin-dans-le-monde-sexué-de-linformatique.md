---
site: RFI
title: "Solène Mirliaz, contre-exemple féminin dans le monde sexué de l'informatique"
author: Léopold Picot
date: 2022-10-02
href: https://www.rfi.fr/fr/science/20221002-sol%C3%A8ne-mirliaz-contre-exemple-f%C3%A9minin-dans-le-monde-sexu%C3%A9-de-l-informatique
featured_image: https://s.rfi.fr/media/display/2dcc134a-22c9-11ed-af0b-005056a97e36/w:1280/p:16x9/CV0_5261-2.webp
tags:
- Sensibilisation
series:
- 202239
series_weight: 0
---

> À seulement 26 ans, Solène Mirliaz vient d'arriver première ex æquo de l'agrégation d'informatique française. Malgré sa réussite, la désormais enseignante est plutôt pessimiste sur l'évolution à venir d'un secteur encore marqué par la sous-représentation des femmes.
