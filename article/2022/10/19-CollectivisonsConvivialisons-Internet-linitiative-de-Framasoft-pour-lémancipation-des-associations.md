---
site: Next INpact
title: "Collectivisons/Convivialisons Internet: l'initiative de Framasoft pour l'émancipation des associations (€)"
description: Projets laqués
author: Vincent Hermann
date: 2022-10-19
href: https://www.nextinpact.com/article/70180/collectivisonsconvivialisons-internet-initiative-framasoft-pour-emancipation-associations
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/12391.jpg
tags:
- Associations
- Internet
series:
- 202242
series_weight: 0
---

> L’association Framasoft revient avec un nouveau plan triennal et une série d’actions pensées cette fois pour les collectifs, en particulier les associations. Formation, hébergement, services, accompagnement, promotion: on vous détaille cette nouvelle campagne, baptisée «Collectivisons / Convivialisons Internet», ou… COIN / COIN.
