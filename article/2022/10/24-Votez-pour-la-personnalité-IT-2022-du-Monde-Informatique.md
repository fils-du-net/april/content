---
site: Le Monde Informatique
title: "Votez pour la personnalité IT 2022 du Monde Informatique"
date: 2022-10-24
href: https://www.lemondeinformatique.fr/actualites/lire-votez-pour-la-personnalite-it-2022-du-monde-informatique-88263.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000088479.png
tags:
- Institutions
- Informatique en nuage
series:
- 202243
series_weight: 0
---

> Pour la 9e année, Le Monde Informatique invite ses lecteurs à mettre à l'honneur une personnalité du secteur de l'IT et du numérique qui s'est distinguée au cours de l'année par son parcours ou ses projets, à la tête d'une DSI, dans le monde des start-up, au sein d'une association, à l'Assemblée Nationale ou comme fournisseur de technologies. Parmi les 10 noms que nous vous proposons cette année, quelle est votre personnalité IT 2022? Vous pouvez voter jusqu'au 20 décembre!
