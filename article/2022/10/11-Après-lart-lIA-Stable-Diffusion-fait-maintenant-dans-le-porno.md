---
site: Clubic.com
title: "Après l'art, l'IA Stable Diffusion fait maintenant dans le porno"
author: Thibaut Keutchayan
date: 2022-10-11
href: https://www.clubic.com/porno_industrie_sexe/actualite-441546-apres-l-art-l-ia-stable-diffusion-fait-maintenant-dans-le-porno.html
featured_image: https://pic.clubic.com/v1/images/1892968/raw.webp?fit=max&width=1200&hash=c2a3cdfe1b691779ae66838dede5fe6e3ce1115e
tags:
- Innovation
series:
- 202241
series_weight: 0
---

> C'était l'une des craintes principales des créateurs de Stable Diffusion. C'est désormais une réalité: l'IA de la plateforme sert désormais à générer du contenu pornographique.
