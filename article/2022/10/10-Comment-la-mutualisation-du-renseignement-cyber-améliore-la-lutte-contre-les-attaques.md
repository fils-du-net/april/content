---
site: ZDNet France
title: "Comment la mutualisation du renseignement cyber améliore la lutte contre les attaques"
author: Tristan Pinceaux
date: 2022-10-10
href: https://www.zdnet.fr/actualites/comment-la-mutualisation-du-renseignement-cyber-ameliore-la-lutte-contre-les-attaques-39948082.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2022/Cybersecurity0510__w1200.jpg
tags:
- Innovation
series:
- 202241
series_weight: 0
---

> Editeurs, experts, associations, mais aussi agences de renseignement travaillent de manière collaborative. Car la sécurité informatique est l'affaire de tous. Par Tristan PINCEAUX, lead CERT CWATCH Almond.
