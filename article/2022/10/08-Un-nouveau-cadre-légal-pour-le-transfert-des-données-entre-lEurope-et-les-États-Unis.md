---
site: RFI
title: "Un nouveau cadre légal pour le transfert des données entre l'Europe et les États-Unis"
author: Guillaume Naudin
date: 2022-10-08
href: https://www.rfi.fr/fr/am%C3%A9riques/20221008-un-nouveau-cadre-l%C3%A9gal-pour-le-transfert-des-donn%C3%A9es-entre-l-europe-et-les-%C3%A9tats-unis
featured_image: https://s.rfi.fr/media/display/ce7af9c8-0b0a-11ec-b762-005056a97e36/w:1280/p:16x9/2021-08-27T032218Z_708487992_RC2FDP961FPW_RTRMADP_3_CHINA-REGULATIONS-DATA.webp
tags:
- International
- Vie privée
series:
- 202240
series_weight: 0
---

>  Le président américain Joe Biden a signé un décret visant à fournir des gages aux Européens pour le transfert de leurs données personnelles de l'Union européenne vers les États-Unis dans un nouveau cadre légal, crucial pour l'économie numérique.
