---
site: Le Telegramme
title: "Le club Informatique L@ssourie partage ses connaissances, à Bannalec"
date: 2022-10-16
href: https://www.letelegramme.fr/finistere/bannalec/le-club-informatique-l-ssourie-partage-ses-connaissances-a-bannalec-16-10-2022-13200648.php
featured_image: https://www.letelegramme.fr/images/2022/10/16/lors-de-son-assemblee-generale-jeudi-13-octobre-2022-le_6955166_676x507p.jpg?v=1
tags:
- Associations
series:
- 202241
---

> Le programme s’annonce bien rempli, pour la saison 2022-2023, à l’association L@ssourie, à Bannalec. Son but: lutter contre l’illectronisme.
