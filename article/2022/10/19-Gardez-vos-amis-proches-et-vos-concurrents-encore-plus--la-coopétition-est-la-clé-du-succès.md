---
site: Journal du Net
title: "Gardez vos amis proches et vos concurrents encore plus: la coopétition est la clé du succès"
author: Oskari Saarenmaa
date: 2022-10-19
href: https://www.journaldunet.com/management/direction-generale/1515689-gardez-vos-amis-proches-et-vos-concurrents-encore-plus-la-coopetition-est-la-cle-du-succes
tags:
- Economie
series:
- 202242
series_weight: 0
---

> Pour faire face à un défi, le fait de se tourner vers un concurrent afin d'obtenir de l'aide est certainement la dernière chose à laquelle on pense. Mais s'en passer peut devenir une erreur stratégique majeure pour la croissance d'une entreprise.
