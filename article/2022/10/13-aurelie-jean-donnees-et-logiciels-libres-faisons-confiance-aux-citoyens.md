---
site: LEFIGARO
title: "Aurélie Jean: «Données et logiciels libres: faisons confiance aux citoyens!» (€)"
author: Aurélie Jean
date: 2022-10-13
href: https://www.lefigaro.fr/vox/societe/aurelie-jean-donnees-et-logiciels-libres-faisons-confiance-aux-citoyens-20221013
featured_image: https://i.f1g.fr/media/cms/704x396_cropupscale/2022/10/13/90aa6300eb8a4c221605db744c5b44daca97f07eb6db951936e42ca344fe669b.jpg
tags:
- Open Data
series:
- 202241
---

> Depuis le 7 octobre, les données sur la surveillance de la variole du singe sont mises à disposition du public. Une mesure tardive, regrette notre chroniqueuse, illustration selon elle d'une particularité culturelle de notre pays qui encourage peu les initiatives individuelles.
