---
site: Silicon
title: "Logiciels libres: 7 nouveaux entrants au SILL"
author: Clément Bohic
date: 2022-10-04
href: https://www.silicon.fr/logiciels-libres-7-nouveaux-entrants-au-sill-449180.html
featured_image: https://www.silicon.fr/wp-content/uploads/2022/10/SILL-septembre-2022.jpg
tags:
- Référentiel
- Administration
series:
- 202240
series_weight: 0
---

> Coup de projecteur sur quelques-uns des ajouts effectués en septembre au Socle interministériel des logiciels libres.
