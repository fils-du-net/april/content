---
site: Journal du Net
title: "Sécurité et innovation doivent aller de pair"
author: Nicolas Massé
date: 2022-10-06
href: https://www.journaldunet.com/solutions/dsi/1515343-securite-et-innovation-doivent-aller-de-pair
tags:
- Innovation
series:
- 202240
series_weight: 0
---

> En règle général, l'innovation n'est pas une dynamique qui plaît beaucoup aux experts de la sécurité. Est-il possible de conjuguer les deux, dans l'intérêt des entreprises?
