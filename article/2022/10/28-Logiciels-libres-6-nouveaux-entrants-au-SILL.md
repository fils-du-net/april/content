---
site: Silicon
title: "Logiciels libres: 6 nouveaux entrants au SILL"
author: Clément Bohic
date: 2022-10-28
href: https://www.silicon.fr/logiciels-libres-6-nouveaux-entrants-sill-451269.html
featured_image: https://www.silicon.fr/wp-content/uploads/2022/10/SILL-septembre-2022.jpg
tags:
- Réferentiel
series:
- 202243
series_weight: 0
---

> Coup de projecteur sur quelques-uns des ajouts effectués en octobre au Socle interministériel des logiciels libres.
