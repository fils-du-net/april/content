---
site: 01net.
title: "Microsoft change d'avis et laisse les développeurs vendre des programmes open source dans son magasin d'applications"
author: François Bedin
date: 2022-07-19
href: https://www.01net.com/actualites/microsoft-change-davis-et-laisse-les-developpeurs-vendre-des-programmes-open-source-dans-son-magasin-dapplications.html
featured_image: https://www.01net.com/app/uploads/2021/04/MEA-MIcrosoft-store.jpg
tags:
- Entreprise
series:
- 202229
series_weight: 0
---

> Microsoft avait initialement changé la politique de son Store pour lutter contre les clones payants de logiciels gratuits. Mais ce changement nuisait aux développeurs légitimes de programmes open source qui désiraient les vendre dans le magasin.
