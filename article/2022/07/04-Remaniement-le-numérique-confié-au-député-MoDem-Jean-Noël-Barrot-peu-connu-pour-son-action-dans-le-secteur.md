---
site: Le Monde.fr
title: "Remaniement: le numérique confié au député MoDem Jean-Noël Barrot, peu connu pour son action dans le secteur (€)"
author: Alexandre Piquard et Vincent Fagot
date: 2022-07-04
href: https://www.lemonde.fr/economie/article/2022/07/04/gouvernement-borne-2-le-numerique-confie-au-depute-modem-jean-noel-barrot-peu-connu-pour-son-action-dans-le-secteur_6133295_3234.html
featured_image: https://img.lemde.fr/2022/07/04/0/0/5025/3224/664/0/75/0/bda07a8_5625779-01-06.jpg
tags:
- Institutions
series:
- 202227
---

> Fort d'un titre de ministre délégué, le député des Yvelines devra reprendre des dossiers comme la régulation des grandes plates-formes du numérique, et mettre en musique quelques propositions du programme présidentiel d'Emmanuel Macron.
