---
site: Le Monde.fr
title: "Lettre aux nouveaux député.e.s: La souveraineté numérique citoyenne passera par les communs numériques, ou ne sera pas – binaire"
date: 2022-07-01
href: https://www.lemonde.fr/blog/binaire/2022/07/01/lettre-aux-nouveaux-depute-e-s-la-souverainete-numerique-citoyenne-passera-par-les-communs-numeriques-ou-ne-sera-pas/
featured_image: https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Flag_of_Ukraine.svg/512px-Flag_of_Ukraine.svg.png
tags:
- Institutions
- Économie
- Innovation
series:
- 202226
series_weight: 0
---

> Lors de l’ Assemblée numérique des 21 et 22 juin, les membres du groupe de travail sur les communs numériques de l’Union européenne, créé en février 2022, se sont réunis pour discuter de la création d’un incubateur européen, ainsi que des moyens ou d’ une structure permettant de fournir des orientations et une assistance aux États membres. En amont, seize acteurs du secteur ont signé une tribune dans Mediapart sur ce même sujet. Binaire a demandé à un des signataires, le Collectif pour une société des communs, de nous expliquer ces enjeux essentiels. Cet article est publié dans le cadre de la rubrique de binaire sur les Communs numériques.
