---
site: ActuaLitté.com
title: "Prolonger le droit d'auteur avec les NFT: le domaine public en danger?"
author: Clément Solym
date: 2022-07-13
href: https://actualitte.com/article/106934/legislation/prolonger-le-droit-d-auteur-avec-les-nft-le-domaine-public-en-danger
featured_image: https://actualitte.com/media/cache/width_944_webp/uploads/images/nft-jetons-non-fongibles-edition-domaine-public-62ce912f365d9208457606.jpg
tags:
- Droit d'auteur
series:
- 202229
series_weight: 0
---

> L’apparition des NFT dans le monde artistique provoque quelques mouvements circonspects. Au point que le Conseil Supérieur de la Propriété Littéraire et Artistique a mandaté Jean Martin, avocat à la Cour, pour une mission bien précise: évaluer ce que les jetons non fongibles impliqueraient, en regard du droit d’auteur. Et de confirmer que le domaine public devient alors une véritable manne…
