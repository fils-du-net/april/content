---
site: La Revue du Digital
title: "Les DSI veulent une clause d'immunité pour leurs données sensibles contre les lois extraterritoriales américaines"
author: La Revue du Digital
href: https://www.larevuedudigital.com/les-dsi-veulent-limmunite-contre-les-lois-extraterritoriales-americaines-dacces-a-leurs-donnees/
featured_image: https://www.larevuedudigital.com/wp-content/uploads/2018/02/Logo-LRDD-Transparent-Background.png
tags:
- Informatique en nuage
series:
- 202227
---

> Les DSI demandent aux autorités européennes de disposer d’une arme absolue – une immunité – afin de protéger si nécessaire leurs données sensibles d’un accès par une autorité étrangère lorsqu’elles sont placées dans un Cloud géré en Europe par un prestataire non européen, américain en l’occurrence.
