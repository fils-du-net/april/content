---
site: ZDNet France
title: "Remaniement: un MoDem au numérique"
author: Louis Adam
date: 2022-07-04
href: https://www.zdnet.fr/actualites/remaniement-un-modem-au-numerique-39944252.htm#Echobox=1656931200
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/Pierre%20temp/Presidentielle%20A__w1200.jpg
tags:
- Institutions
series:
- 202227
---

> Le gouvernement a annoncé le nom du nouveau ministre délégué au Numérique, chargé du numérique et des télécommunications: Jean-Noël Barrot, élu MoDem ayant notamment participé aux travaux de l'Assemblée nationale sur les cryptomonnaies.
