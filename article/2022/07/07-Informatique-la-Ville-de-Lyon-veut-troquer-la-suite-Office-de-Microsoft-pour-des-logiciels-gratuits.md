---
site: Lyon Mag
title: "Informatique: la Ville de Lyon veut troquer la suite Office de Microsoft pour des logiciels gratuits"
date: 2022-07-07
href: https://www.lyonmag.com/article/124375/informatique-la-ville-de-lyon-veut-troquer-la-suite-office-de-microsoft-pour-des-logiciels-gratuits
featured_image: https://www.lyonmag.com/media/images/thumb/870x489_62c67f1221747-27427119377-2702573ef0-o.webp
tags:
- Administration
series:
- 202227
series_weight: 0
---

> Voilà plus de 20 ans que la Ville de Lyon, à l'instar de nombreux foyers, entreprises et collectivités, s'appuie sur les technologies Microsoft.
