---
site: ZDNet France
title: "Les militaires américains veulent vérifier la sécurité des logiciels open source"
author: Thierry Noisette
date: 2022-07-17
href: https://www.zdnet.fr/blogs/l-esprit-libre/les-militaires-americains-veulent-verifier-la-securite-des-logiciels-open-source-39944824.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/12/cyber-security-3400657_960_720_Pixabay.jpg
tags:
- Innovation
- Informatique en nuage
series:
- 202228
series_weight: 0
---

> L'agence de R&D de l'armée américaine, la Darpa, veut mener «une analyse automatique à la fois du code et des dimensions sociales des logiciels open source» pour s'assurer de leur sécurité.
