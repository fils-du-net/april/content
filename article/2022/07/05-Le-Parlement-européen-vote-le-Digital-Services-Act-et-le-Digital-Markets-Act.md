---
site: ZDNet France
title: "Le Parlement européen vote le Digital Services Act et le Digital Markets Act"
author: Clarisse Treilles
date: 2022-07-05
href: https://www.zdnet.fr/actualites/le-parlement-europeen-vote-le-digital-services-act-et-le-digital-markets-act-39944312.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/09/europe_620__w1200.jpg
tags:
- Europe
- Entreprise
series:
- 202227
series_weight: 0
---

> Avec ces nouveaux textes, les entreprises sont passibles d'amendes pouvant atteindre 10 % de leur chiffre d'affaires annuel, en cas de violation du Digital Markets Act, et jusqu'à 6 %, en cas de violation du Digital Services Act.
