---
site: Acteurs Publics 
title: "Quand le logiciel libère des géants du numérique (€)"
author: Émile Marzolf
date: 2022-07-07
href: https://acteurspublics.fr/articles/quand-le-logiciel-libere-des-geants-du-numerique
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/40/c8e24cf44e164984c3636153e9206b478571e226.jpeg
tags:
- Entreprise
series:
- 202227
---

> L’État se laisse séduire par les logiciels libres et les communs numériques, par leur nature ouverte et partagée, un vrai contre-modèle démocratique aux Gafam. Troisième et dernier volet de notre dossier sur la souveraineté numérique.
