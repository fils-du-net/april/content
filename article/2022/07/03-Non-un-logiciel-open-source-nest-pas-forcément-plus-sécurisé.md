---
site: Clubic.com
title: "Non, un logiciel open source n'est pas forcément plus sécurisé"
author: Benoit Bayle
date: 2022-07-03
href: https://www.clubic.com/pro/legislation-loi-internet/propriete-intellectuelle/logiciel-libre-open-source/actualite-428889-non-un-logiciel-open-source-n-est-pas-forcement-plus-securise.html
featured_image: https://pic.clubic.com/v1/images/1665930/raw.webp?fit=max&width=1200&hash=703b8a0f30eaf614f3a4da5a3df9c03af8bedfe7
tags:
- Innovation
series:
- 202226
---

> On entend la phrase magique depuis un bon moment déjà: «un logiciel open source sera par définition plus sécurisé qu'un logiciel propriétaire». Et si cette maxime pouvait se vérifier il y a une dizaine d'années, ce n'est malheureusement plus forcément le cas aujourd'hui.
