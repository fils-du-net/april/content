---
site: Silicon
title: "Open source: Microsoft face à une impasse juridique?"
author: Clément Bohic
date: 2022-07-11
href: https://www.silicon.fr/open-source-microsoft-impasse-juridique-443490.html
featured_image: https://www.silicon.fr/wp-content/uploads/2022/07/Microsoft-Store-open-source.jpg
tags:
- Entreprise
series:
- 202229
---

> Une clause qui devait entrer en vigueur cette semaine sur le Microsoft Store va faire l'objet d'une révision après une levée de boucliers dans l'open source.
