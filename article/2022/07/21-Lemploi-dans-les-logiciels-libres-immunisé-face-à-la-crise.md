---
site: Le Monde Informatique
title: "L'emploi dans les logiciels libres immunisé face à la crise?"
author: Matt Asay
date: 2022-07-21
href: https://www.lemondeinformatique.fr/actualites/lire-l-emploi-dans-les-logiciels-libres-immunise-face-a-la-crise-87387.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000086827.jpg
tags:
- Internet
- Économie
series:
- 202229
series_weight: 0
---

> Rester proche du logiciel libre, soit par le biais d'entreprises qui soutiennent des projets open source, soit en contribuant directement aux communautés de projets, pourrait offrir une meilleure sécurité de l'emploi.
