---
site: RTBF
title: "Ne travailler qu'avec des logiciels libres au quotidien: on a testé. Pourquoi ne les utilise-t-on pas plus?"
author: Miguel Allo
date: 2022-07-10
href: https://www.rtbf.be/article/ne-travailler-quavec-des-logiciels-libres-au-quotidien-on-a-teste-pourquoi-ne-les-utilise-t-on-pas-plus-11027997
featured_image: https://ds.static.rtbf.be/article/image/1248x702/5/d/3/4f0cfe9c2789f27883a8a409d1d334ce-1657440405.jpg
tags:
- Sensibilisation
series:
- 202227
series_weight: 0
---

> Les plus à l’aise avec les outils informatiques ont certainement eu un petit sourire en voyant le titre de cet article. Si l’existence des logiciels libres nous est connue, passer le cap et donc abandonner des écosystèmes propriétaire avec lesquels nous nous débrouillons au quotidien, c’est encore autre chose.
