---
site: Usbek & Rica
title: "Matrix, le protocole décentralisé made in France qui cartonne en Ukraine"
author: Marie Roy
date: 2022-07-11
href: https://usbeketrica.com/fr/article/matrix-le-protocole-decentralise-made-in-france-qui-cartonne-en-ukraine
featured_image: https://usbeketrica.com/media/96265/download/matrix_ouverture.jpg?v=1&inline=1
tags:
- Vie privée
- Innovation
series:
- 202229
series_weight: 0
---

> Matrix et Element sont respectivement un protocole et une application sécurisés et décentralisés. Avec le conflit ukrainien, ces technologies ont connu un certain succès auprès des utilisateurs. Mais qu'ont-elles de spécifiques? On a rencontré Amandine Le Pape, la co-fondatrice de ces services, pour y voir plus clair.
