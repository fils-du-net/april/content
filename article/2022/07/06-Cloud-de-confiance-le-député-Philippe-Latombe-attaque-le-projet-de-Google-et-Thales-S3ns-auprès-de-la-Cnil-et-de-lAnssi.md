---
site: La Tribune
title: "«Cloud de confiance»: le député Philippe Latombe attaque le projet de Google et Thales (S3ns) auprès de la Cnil et de l'Anssi"
author: Sylvain Rolland
date: 2022-07-06
href: https://www.latribune.fr/technos-medias/internet/cloud-de-confiance-le-depute-philippe-latombe-attaque-le-projet-de-google-et-thales-s3ns-aupres-de-la-cnil-et-de-l-anssi-924750.html
featured_image: https://static.latribune.fr/full_width/1965689/philippe-latombe.jpg
tags:
- Informatique en nuage
- Institutions
series:
- 202227
series_weight: 0
---

> Le député Modem accuse le projet cloud S3ns (Google avec Thales) de communication prématurée et trompeuse sur son offre prévue en 2024, dans le but de «capter la clientèle» dès 2022 «avant qu'elle n'aille sur des offres cloud déjà existantes». L'expert demande à la Cnil et à l'Anssi de réaliser «une analyse approfondie» de la structure juridique de S3ns et de sa solidité contre les lois extraterritoriales américaines. Une question au gouvernement a aussi été déposée ce mercredi pour forcer Bruno Le Maire et Jean-Noël Barrot, le nouveau ministre du numérique, à réagir.
