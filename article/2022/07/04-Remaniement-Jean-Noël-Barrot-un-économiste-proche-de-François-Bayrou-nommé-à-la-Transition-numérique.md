---
site: LEFIGARO 
title: "Remaniement: Jean-Noël Barrot, un économiste proche de François Bayrou nommé à la Transition numérique"
author: Marius Bocquet
date: 2022-07-04
href: https://www.lefigaro.fr/politique/remaniement-jean-noel-barrot-un-economiste-proche-de-francois-bayrou-nomme-au-numerique-20220704
featured_image: https://i.f1g.fr/media/cms/704x396_cropupscale/2022/07/04/f0b2756fd9b78c407d3f6d964b169d484473573cce58942c3e47b8c86063aeca.jpg
tags:
- Institutions
series:
- 202227
series_weight: 0
---

> Le député MoDem des Yvelines, âgé de 39 ans, vient d'être nommé ministre délégué dans le gouvernement d'Élisabeth Borne. Il aura également la charge des Télécommunications.
