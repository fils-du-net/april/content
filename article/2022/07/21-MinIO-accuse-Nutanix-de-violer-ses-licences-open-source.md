---
site: InformatiqueNews.fr
title: "MinIO accuse Nutanix de violer ses licences open-source"
author: Loïc Duval
date: 2022-07-21
href: https://www.informatiquenews.fr/minio-accuse-nutanix-de-violer-ses-licences-open-source-avec-sa-solution-objects-88822
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2022/07/minio-nutanix-shutterstock_461891695.jpg
tags:
- Licenses
series:
- 202229
---

> Vraie violation de licences open-source ou simple gaffe de développeurs? Un conflit latent entre MinIO et Nutanix explose au grand jour.
