---
site: Le Monde Informatique
title: "Minio accuse Nutanix de violation de licence open source"
author: Serge Leblal
date: 2022-07-20
href: https://www.lemondeinformatique.fr/actualites/lire-minio-accuse-nutanix-de-violation-de-licence-open-source-87447.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000086916.png
tags:
- Licenses
series:
- 202229
series_weight: 0
---

> Accusé par Minio de violation de licence open source dans l'utilisation de sa solution objets, Nutanix plaide la bonne foi avec le simple oubli de code dans sa plateforme HCI.
