---
site: cio-online.com
title: "Le ministère du numérique confié à Jean-Noël Barrot"
author: Bertrand Lemaire
date: 2022-07-04
href: https://www.cio-online.com/actualites/lire-le-ministere-du-numerique-confie-a-jean-noel-barrot-14320.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000018898.jpg
tags:
- Institutions
series:
- 202227
---

> Jean-Noël Barrot a été nommé, le 4 juillet 2022, ministre délégué chargé de la transition numérique et des télécommunications.
