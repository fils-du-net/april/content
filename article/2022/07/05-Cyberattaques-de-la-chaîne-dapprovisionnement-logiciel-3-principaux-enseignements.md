---
site: Journal du Net
title: "Cyberattaques de la chaîne d'approvisionnement logiciel: 3 principaux enseignements"
author: Tony Hadfield
date: 2022-07-05
href: https://www.journaldunet.com/solutions/dsi/1513025-cyberattaques-de-la-chaine-d-approvisionnement-logiciel-3-principaux-enseignements-pour-les-organisations
tags:
- Sensibilisation
series:
- 202227
---

> 2021 est une année à part dans l'actualité des attaques ciblant la chaîne d'approvisionnement logiciel. Les organisations doivent tirer trois grands enseignements afin d'assurer leur sécurité.
