---
site: Educavox
title: "Comment collaborer à la communauté du libre"
author: Cauche Jean-François
date: 2022-07-13
href: https://www.educavox.fr/formation/analyse/comment-collaborer-a-la-communaute-du-libre
featured_image: https://www.educavox.fr/media/k2/items/cache/785288393d92a636d2a6c19d723786ef_L.jpg
tags:
- Sensibilisation
series:
- 202228
---

> Tout récemment, le développeur de Notepad++, un éditeur de textes et environnement de programmation bien connu sur Windows, a dédié la dernière version de son logiciel aux «utilisateurs insatisfaits». Cela fait suite à un mail insultant reçu d’un utilisateur mécontent, auquel il a répondu avec beaucoup d’humour en lui proposant de le rembourser.
