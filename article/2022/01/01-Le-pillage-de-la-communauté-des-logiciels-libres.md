---
site: Le Monde diplomatique
title: "Le pillage de la communauté des logiciels libres (€)"
description: "Que faire pour contrer la prédation des géants du numérique?"
author: Laure Muselli, Mathieu O’Neil, Fred Pailler et Stefano Zacchiroli
date: 2022-01-01
href: https://www.monde-diplomatique.fr/2022/01/MUSELLI_LAURE/64221
tags:
- Sensibilisation
- Entreprise
series:
- 202201
---

> Pendant que l'utopie numérique rêvée trente ans plus tôt enfantait un supermarché à partir de 1990, un groupe d'irréductibles maintenait envers et contre tout un projet fidèle aux origines: le logiciel libre. Coopté, récupéré et trahi par les mastodontes de l'industrie, le voici fragilisé.
