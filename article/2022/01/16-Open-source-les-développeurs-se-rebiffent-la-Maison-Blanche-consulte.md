---
site: Le Monde Informatique
title: "Open source: les développeurs se rebiffent, la Maison Blanche consulte"
author: Jacques Cheminat
date: 2022-01-16
href: https://www.lemondeinformatique.fr/actualites/lire-open-source-les-developpeurs-se-rebiffent-la-maison-blanche-consulte-85456.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000083331.jpg
tags:
- Sensibilisation
- Économie
series:
- 202202
series_weight: 0
---

> Les développeurs de projets open source veulent faire payer et contribuer les sociétés qui utilisent leurs initiatives. L'un d'entre eux menace même de jeter l'éponge et de ne plus maintenir sa solution. Un ras-le-bol qui intervient au moment où les acteurs IT étaient sollicités par la Maison Blanche pour réfléchir à la sécurité des projets open source.
