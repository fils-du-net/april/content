---
site: aef info
title: "Attractivité, open data, chantiers informatiques… L’ancien Dinum Nadi Bou Hanna défend son bilan sur LinkedIn"
author: Florianne Finet
date: 2022-01-19
href: https://www.aefinfo.fr/depeche/665954
featured_image: https://www.aefinfo.fr/assets/medias/documents/4/7/474468_prv.jpeg
tags:
- april
- Administration
series:
- 202203
series_weight: 0
---

> En trois ans, la Dinum a construit "un plan d’action pour développer l’attractivité de l’État employeur du numérique" couvrant les questions de rémunération, de marque employeur ou encore de soutien aux femmes du numérique", affirme Nadi Bou Hanna, le directeur interministériel du numérique, dans un long billet publié sur le réseau social professionnel LinkedIn, la veille de son départ du ministère, le 14 janvier 2022. Il reconnaît qu’il reste à mener une réflexion sur "les filières de fonctionnaires spécialistes du numérique".
