---
site: Numerama
title: "LibreOffice, Mastodon: l'UE offre 200 000 € pour sécuriser certains logiciels libres"
description: "C'est mieux que rien"
author: Julien Lausson
date: 2022-01-24
href: https://www.numerama.com/tech/830709-libreoffice-mastodon-lue-offre-200-000-e-pour-securiser-certains-logiciels-libres.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2020/10/libreoffice-1.jpg?resize=1024,576
tags:
- Économie
- Europe
series:
- 202204
series_weight: 0
---

> La Commission européenne met en place un programme de chasse aux bugs pour certains logiciels libres que ses services utilisent fortement.
