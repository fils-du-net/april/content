---
site: Nouvelle République
title: "Logiciels libres: Solix fête ses vingt ans à Romorantin"
date: 2022-01-26
href: https://www.lanouvellerepublique.fr/romorantin/logiciels-libres-solix-fete-ses-vingt-ans-a-romorantin
featured_image: https://images.lanouvellerepublique.fr/image/upload/t_1020w/f_auto/61f0a856e1545482038b458c.jpg
tags:
- Associations
- Sensibilisation
series:
- 202204
series_weight: 0
---

> Solix? Concentration de Sologne et de Linux. Linux, est un système d’exploitation qui rassemble tous les logiciels libres mais qui est également exploité par les plus grands groupes, pour ses serveurs et Google, Amazon, Facebook etc. La plupart des internautes l’utilisent à leur insu.
