---
site: korii.
title: "Au Texas, un vaccin open source et dix fois moins cher snobé par le gouvernement"
description: "Le Corbevax n'a presque pas été financé par l'État."
author: Barthélemy Dont
date: 2022-01-17
href: https://korii.slate.fr/biz/covid-19-texas-corbevax-vaccin-open-source-10-fois-moins-cher-snobe-gouvernement
featured_image: https://korii.slate.fr/sites/default/files/styles/1440x600/public/000_9vr3zn.jpg
tags:
- Sciences
series:
- 202203
series_weight: 0
---

> Malgré les supplications de pays comme l'Inde et l'Afrique du Sud et les promesses de bonne volonté de Joe Biden, les pays occidentaux ne sont pas parvenus à un accord pour lever les brevets des vaccins contre le Covid-19.
