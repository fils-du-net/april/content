---
site: ZDNet France
title: "Des 'guildes' d'ingénieurs à l'open source, comment Bloomberg innove"
author: Owen Hughes
date: 2022-01-19
href: https://www.zdnet.fr/actualites/des-guildes-d-ingenieurs-a-l-open-source-comment-bloomberg-innove-39935983.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/01/transfonum_strategie__w1200.jpg
tags:
- Entreprise
- Sensibilisation
series:
- 202203
---

> ZDNet s'est entretenu avec Adam Wolf, responsable de l'infrastructure logicielle chez Bloomberg, au sujet de la création et du déploiement de systèmes répondant aux besoins d'un secteur financier en constante évolution.
