---
site: LeBigData.fr
title: "GitHub restaure le compte du développeur militant"
author: Elina S.
date: 2022-01-19
href: https://www.lebigdata.fr/compte-developpeur-github-restaure
featured_image: https://www.lebigdata.fr/wp-content/uploads/2022/01/github-restaure-compte-developpeur1-660x330.jpg
tags:
- Sensibilisation
series:
- 202203
---

> Vers le début du mois de janvier, Bleeping Computer a découvert qu’une paire de bibliothèques populaires sur GitHub ont été intentionnellement compromises par un développeur. Le compte de ce dernier a d’abord été suspendu avant d’être restauré quelques jours plus tard.
