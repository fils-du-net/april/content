---
site: ZDNet France
title: "Le Health Data Hub retire temporairement sa demande d'autorisation auprès de la CNIL"
author: Clarisse Treilles
date: 2022-01-11
href: https://www.zdnet.fr/actualites/le-health-data-hub-retire-temporairement-sa-demande-d-autorisation-aupres-de-la-cnil-39935373.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/01/sante__w1200.jpg
tags:
- Vie privée
series:
- 202202
---

> Une pause dans la mise en œuvre de la mégaplateforme de santé s'impose.
