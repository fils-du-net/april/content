---
site: ZDNet France
title: "Messagerie instantanée: Olvid passe en open source"
author: Louis Adam
date: 2022-01-03
href: https://www.zdnet.fr/actualites/messagerie-instantanee-olvid-passe-en-open-source-39934843.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/02/open-source-code-620__w1200.jpg
tags:
- Sensibiliation
- Licenses
series:
- 202201
series_weight: 0
---

> La messagerie sécurisée proposée par l'éditeur français Olvid passe en open source sur Github. Cette solution de messagerie est l'une des premières du genre à avoir obtenu un visa de l'Anssi certifiant son respect des bonnes pratiques de sécurité.
