---
site: Acteurs Publics 
title: "La France mise sur l'“open source” pour protéger la souveraineté numérique européenne (€)"
author: Emile Marzolf
date: 2022-01-11
href: https://www.acteurspublics.fr/articles/la-france-mise-sur-l-open-source-pour-proteger-la-souverainete-numerique-europeenne
featured_image: https://www.acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/38/58ab3061ca20baa44f467e15facf075e195546cf.jpeg
tags:
- Administration
- Marchés publics
series:
- 202202
---

> La présidence française de l’Union européenne sera marquée par les dossiers numériques, avec l’examen de 2 textes de régulation des marchés et services numériques. Le gouvernement compte aussi sur la coopération européenne et le logiciel libre (open source) pour favoriser une transformation numérique du secteur public reposant sur la “qualité, l’ouverture et la souveraineté”.
