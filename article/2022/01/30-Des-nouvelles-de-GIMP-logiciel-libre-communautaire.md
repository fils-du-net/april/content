---
site: ZDNet France
title: "Des nouvelles de GIMP, «logiciel libre communautaire»"
author: Thierry Noisette
date: 2022-01-30
href: https://www.zdnet.fr/blogs/l-esprit-libre/des-nouvelles-de-gimp-logiciel-libre-communautaire-39936533.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/12/Gimp_ecran.jpg
tags:
- Sensibilisation
series:
- 202205
---

> Près d'une centaine de contributeurs, dont 41 développeurs, pour son dépôt principal, une équipe «communauté»: un instructif compte-rendu sur le logiciel de retouche d'images GIMP, par Jehan, un de ses principaux contributeurs.
