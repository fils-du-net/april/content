---
site: Le Monde Informatique
title: "Le Gouvernement met le Health Data Hub en pause"
author: Jacques Cheminat
date: 2022-01-10
href: https://www.lemondeinformatique.fr/actualites/lire-le-gouvernement-met-le-health-data-hub-en-pause-85375.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000083198.png
tags:
- Vie privée
- Institutions
- Informatique en nuage
series:
- 202202
series_weight: 0
---

> Le gouvernement français a retiré une demande d'autorisation auprès de la Cnil pour le Health Data Hub afin d'héberger la base principale du système national des données de santé. L'hébergement de ce projet sur Azure de Microsoft est critiqué par des associations et des parlementaires.
