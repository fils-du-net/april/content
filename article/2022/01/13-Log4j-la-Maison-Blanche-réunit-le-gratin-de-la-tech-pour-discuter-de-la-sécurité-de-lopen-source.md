---
site: Numerama
title: "Log4j: la Maison-Blanche réunit le gratin de la tech pour discuter de la sécurité de l'open source"
description: "La tech: assemble!"
date: 2022-01-13
href: https://www.numerama.com/tech/819773-log4j-la-maison-blanche-reunit-le-gratin-de-la-tech-pour-discuter-de-la-securite-de-lopen-source.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2022/01/log4j.png
tags:
- Sensibilisation
series:
- 202202
---

> Une réunion doit évoquer la manière de sécuriser l’open source, avec, en filigrane, la question du financement de projets cruciaux pour la tech.
