---
site: Numerama
title: "Mais que se passe-t-il avec le Health Data Hub?"
author: Alexandre Horn
date: 2022-01-13
href: https://www.numerama.com/politique/817057-mais-que-se-passe-t-il-avec-le-health-data-hub.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2021/02/chromosome_machinelearning.jpg?resize=1024,576
tags:
- Vie privée
series:
- 202202
---

> La demande d’autorisation formulée par le gouvernement auprès de la Cnil pour le Health Data Hub a été retirée. Un coup d’arrêt pour ce projet de centralisation des données de santé des Françaises et Français.
