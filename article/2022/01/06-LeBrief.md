---
site: Next INpact
title: "GnuPG a (enfin) un modèle économique durable (et rentable)"
date: 2022-01-06
href: https://www.nextinpact.com/lebrief/921
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/wide-linked-media/1956.jpg
tags:
- Économie
series:
- 202201
series_weight: 0
---

> «Pendant de nombreuses années, notre travail a été principalement financé par des dons et de petits projets», écrit Werner Koch, le créateur de GnuPG, pour expliquer ce pourquoi il a d'ores et déjà «annulé» les dons récurrents basés sur Paypal et Stripe, et appelle les autres donateurs à les «annuler et rediriger vos fonds vers d'autres projets qui ont davantage besoin d'un soutien financier».
