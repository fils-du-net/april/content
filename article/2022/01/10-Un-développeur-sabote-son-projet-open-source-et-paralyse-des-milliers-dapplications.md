---
site: Numerama
title: "Un développeur sabote son projet open source et paralyse des milliers d'applications"
description: "Un signal d'alarme sur la précarité de l'open source"
author: Alexandre Horn
date: 2022-01-10
href: https://www.numerama.com/cyberguerre/813825-un-developpeur-sabote-son-projet-open-source-et-paralyse-des-milliers-dapplications.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2021/12/hacker-dos.jpg?resize=1024,576
tags:
- Sensibilisation
series:
- 202202
---

> Un développeur bénévole a saboté deux projets open source auxquels il contribuait régulièrement, paralysant des milliers de projets qui en dépendaient. Au delà d’une blague ou d’un acte malveillant, cette grève 2.0 pointe la précarité du monde de l’open source.
