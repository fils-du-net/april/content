---
site: Clubic.com
title: "RGPD: InterHop saisit la CNIL pour que les acteurs e-santé (Alan, Keldoc, Maiia…) arrêtent d'utiliser Google Analytics"
author: Alexandre Boero
date: 2022-01-31
href: https://www.clubic.com/pro/legislation-loi-internet/donnees-personnelles/actualite-406680-rgpd-interhop-saisit-la-cnil-pour-que-les-acteurs-e-sante-alan-keldoc-maiia-arretent-d-utiliser-google-analytics.html
featured_image: https://pic.clubic.com/v1/images/1968447/raw.webp?fit=max&width=1200&hash=afbeaf037a4891526f47467bd0782fe08b0fbd19
tags:
- Vie privée
- Associations
- Institutions
series:
- 202205
series_weight: 0
---

> L'association InterHop, spécialisée dans le logiciel libre, peste contre le traitement des données collectées par la solution Google Analytics, hébergée aux États-Unis.
