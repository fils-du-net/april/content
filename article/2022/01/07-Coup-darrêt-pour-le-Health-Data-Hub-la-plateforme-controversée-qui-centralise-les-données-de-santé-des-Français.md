---
site: La Tribune
title: "Coup d'arrêt pour le Health Data Hub, la plateforme controversée qui centralise les données de santé des Français"
author: Sylvain Rolland
date: 2022-01-07
href: https://www.latribune.fr/technos-medias/informatique/coup-d-arret-pour-le-health-data-hub-la-plateforme-controversee-qui-centralise-les-donnees-de-sante-des-francais-899813.html
featured_image: https://static.latribune.fr/full_width/1727346/coronavirus-france-veran-previent-contre-le-risque-d-une-quatrieme-vague-fin-juillet.jpg
tags:
- Informatique en nuage
- Vie privée
- Institutions
series:
- 202201
series_weight: 0
---

> Le gouvernement a retiré auprès de la Cnil sa demande d'autorisation, qui était pourtant indispensable pour que le très controversé Health Data Hub (HDH) puisse fonctionner de manière opérationnelle. Traduction: l'Etat met un coup d'arrêt au HDH sous sa forme actuelle. Selon nos informations, il s'apprêterait à repenser son fonctionnement de fond en comble, pour "repartir sur de bonnes bases". Une manière de sortir, à quelques mois de l'élection présidentielle, des incessantes polémiques autour de sa gestion calamiteuse de ce dossier très sensible. Analyse.
