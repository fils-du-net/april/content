---
site: Le Monde Informatique
title: "Xavier Albouy va remplacer Nadi Bou Hana à la Dinum (MAJ)"
author: Bertrand Lemaire
date: 2022-01-12
href: https://www.lemondeinformatique.fr/actualites/lire-xavier-albouy-va-remplacer-nadi-bou-hana-a-la-dinum-maj-85424.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000083272.jpg
tags:
- Administration
series:
- 202202
---

> Dans le compte rendu du conseil des ministres de ce jour, on apprend qu'il est mis fin à sa demande aux fonctions de Nadi Bou Hana à la Dinum à compter du 17 janvier 2022. Xavier Albouy va assurer l'interim.
