---
site: Datanews.be
title: "L'Europe oriente les hackers vers les logiciels open source"
author: Pieterjan Van Leemputten
date: 2022-01-21
href: https://datanews.levif.be/ict/actualite/l-europe-oriente-les-hackers-vers-les-logiciels-open-source/article-news-1516113.html
featured_image: https://web.static-rmg.be/if/c_crop,w_512,h_341,x_0,y_0,g_center/c_fit,w_620,h_412/05e9b2ceaa3aeb51de5288193df15448.jpg
tags:
- Europe
- Innovation
series:
- 202203
series_weight: 0
---

> Quiconque découvre des bugs dans des logiciels open source populaires, pourra recevoir une récompense de la part de la Commission européenne. LibreOffice et le belge Odoo notamment disposeront ainsi de davantage d'attention pour gagner en sécurité.
