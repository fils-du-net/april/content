---
site: ZDNet France
title: "La Commission européenne lance un nouveau programme de chasse aux bugs dans cinq logiciels libres"
author: Thierry Noisette
date: 2022-01-23
href: https://www.zdnet.fr/blogs/l-esprit-libre/la-commission-europeenne-lance-un-nouveau-programme-de-chasse-aux-bugs-dans-cinq-logiciels-libres-39936139.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2018/12/Bruxelles_Commission_UE_Berlaymont.jpg
tags:
- Europe
series:
- 202203
---

> Des bug bounties allant jusqu'à 5.000 euros sont offerts pour détecter des failles dans des logiciels open source très utilisés par les services publics européens, comme LibreOffice ou Mastodon.
