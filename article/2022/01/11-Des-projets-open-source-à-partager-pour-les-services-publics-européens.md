---
site: ZDNet France
title: "Des projets open source à partager pour les services publics européens"
author: Thierry Noisette
date: 2022-01-11
href: https://www.zdnet.fr/blogs/l-esprit-libre/des-projets-open-source-a-partager-pour-les-services-publics-europeens-39935487.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/12/Amelie_de_Montchalin_WMC.jpg
tags:
- Administration
- Innovation
- Europe
series:
- 202202
series_weight: 0
---

> La DINUM a organisé un concours de pitchs de projets numériques de services publics, en majorité open source, de 11 pays, en vue de leur réplicabilité.
