---
site: Silicon
title: "Open source: les leviers pour un modèle durable"
date: 2022-01-17
href: https://www.silicon.fr/open-source-leviers-financiers-modele-durable-430070.html
featured_image: https://www.silicon.fr/wp-content/uploads/2022/01/COSSI-financement-projets-open-source-scaled.jpeg
tags:
- Économie
- Sensibilisation
- Informatique en nuage
- Licenses
series:
- 202203
series_weight: 0
---

> L'open source, modèle durable? À l'heure où la question (ré)attire l'attention, focus sur quelques leviers de financement.
