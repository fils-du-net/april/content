---
site: Le Monde.fr
title: "«La non-maîtrise des technologies affaiblit nos capacités de négociation avec les Big Tech» (€)"
author: Ophélie Coelho
date: 2022-01-17
href: https://www.lemonde.fr/idees/article/2022/01/17/la-non-maitrise-des-technologies-affaiblit-nos-capacites-de-negociation-avec-les-big-tech_6109750_3232.html
tags:
- Informatique en nuage
- Institutions
series:
- 202203
series_weight: 0
---

> Dans une tribune au «Monde», la chercheuse Ophélie Coelho défend l'idée d'une stratégie industrielle européenne pour le numérique capable de mieux organiser la résistance aux géants du secteur comme Facebook ou Google, qui cherchent à maintenir nos entreprises en situation de dépendance.
