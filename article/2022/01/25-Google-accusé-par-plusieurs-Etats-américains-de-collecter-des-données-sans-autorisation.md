---
site: Le Monde.fr
title: "Google accusé par plusieurs Etats américains de collecter des données sans autorisation"
date: 2022-01-25
href: https://www.lemonde.fr/pixels/article/2022/01/25/plusieurs-etats-americains-accusent-google-de-collecter-des-donnees-sans-autorisation_6110857_4408996.html
featured_image: https://img.lemde.fr/2022/01/24/0/0/4813/3208/1328/0/45/0/1477693_f1ab8d4199614242a0e0a16bc5dc5db8-f1ab8d4199614242a0e0a16bc5dc5db8-0.jpg
tags:
- Vie privée
- Institutions
- Entreprise
series:
- 202204
---

> Ils demandent notamment le remboursement des revenus générés grâce à la collecte et à l’utilisation des données de géolocalisation, ainsi que des amendes, d’un montant non précisé.
