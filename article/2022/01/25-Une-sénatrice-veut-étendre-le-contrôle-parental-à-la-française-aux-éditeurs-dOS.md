---
site: Next INpact
title: "Une sénatrice veut étendre le contrôle parental à la française aux éditeurs d'OS"
date: 2022-01-25
href: https://www.nextinpact.com/lebrief/49588/une-senatrice-veut-etendre-controle-parental-a-francaise-aux-editeurs-dos
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/wide-linked-media/5663.jpg
tags:
- Vente liée
- Institutions
series:
- 202204
series_weight: 0
---

> Au Sénat, la proposition de loi LREM déjà adoptée par les députés, prépare son parcours. En commission des affaires économiques, un amendement a été déposé par la rapporteure Sylviane Noël (LR) pour étendre le périmètre du contrôle parental.
