---
site: Archimag
title: "La science ouverte en France: chiffres et état des lieux de l'open access (€)"
author: Bruno Texier
date: 2022-01-26
href: https://www.archimag.com/bibliotheque-edition/2022/01/26/science-ouverte-france-chiffres-etat-lieux-open-access
featured_image: https://www.archimag.com/sites/archimag.com/files/styles/article/public/web_articles/image/science-ouverte-open-access-france-chiffres.jpg?itok=uzPQ4SuS
tags:
- Sciences
series:
- 202204
series_weight: 0
---

> La science ouverte (open access) entend favoriser la diffusion des publications et des données de la recherche. Elle est donc confrontée aux logiques propres au monde de l'édition scientifique et aux bases de données fermées. Son enjeu principal est de favoriser le travail des chercheurs et l'apprentissage des étudiants. Aux manettes, entre autres, les bibliothécaires des établissements universitaires. Via l'accès chez les éditeurs ou en archive ouverte, il leur revient de se poser en interface vers la connaissance scientifique, de travailler au bénéfice des chercheurs et des étudiants. Voici un état des lieux de la science ouverte, incluant la question du logiciel libre.
