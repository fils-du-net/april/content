---
site: Le Monde.fr
title: "Des logiciels libres très répandus sur Internet volontairement sabotés par leur créateur"
date: 2022-01-10
href: https://www.lemonde.fr/pixels/article/2022/01/10/des-logiciels-libres-tres-repandus-sur-internet-volontairement-sabotes-par-leur-createur_6108875_4408996.html
tags:
- Sensibilisation
series:
- 202202
---

> Le concepteur de deux librairies javascript très utilisées y a intentionnellement introduit des bugs. Ses motivations restent floues, mais cet épisode souligne à nouveau la dépendance des plus grands acteurs aux projets open source.
