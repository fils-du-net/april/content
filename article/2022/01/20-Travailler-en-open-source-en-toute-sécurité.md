---
site: Journal du Net
title: "Travailler en open source en toute sécurité"
author: Daniel Berman
date: 2022-01-20
href: https://www.journaldunet.com/solutions/dsi/1508251-travailler-en-open-source-en-toute-securite
tags:
- Sensibilisation
series:
- 202203
---

> Selon le Gartner, 96% des applications contiennent des packages open source qui représentent l'immense majorité des fondations invisibles qui soutiennent le code natif des développeurs. Toujours selon Gartner, 80% du code source des applications, toutes catégories confondues, est open source.
