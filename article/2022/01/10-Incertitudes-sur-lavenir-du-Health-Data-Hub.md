---
site: Acteurs Publics 
title: "Incertitudes sur l'avenir du Health Data Hub (€)"
author: Emile Marzolf
date: 2022-01-10
href: https://acteurspublics.fr/articles/incertitudes-sur-lavenir-du-health-data-hub
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/27/a6a7e44af5e436308536571557258291404f35fb.jpeg
tags:
- Vie privée
series:
- 202202
---

> La rumeur a enflé, toute la journée du vendredi 7 janvier, autour de la mise à l’arrêt de la plate-forme nationale des données de santé, après que sa direction a retiré une demande d’autorisation à la Cnil. L’organisme assure qu’il ne s’agit que d’un problème de “tuyauterie réglementaire”, et rien de plus, pour ce projet emblématique du quinquennat Macron.
