---
site: ZDNet France
title: "Brevets logiciels: comment Michel Rocard découvrit le logiciel libre"
author: Thierry Noisette
date: 2022-01-31
href: https://www.zdnet.fr/blogs/l-esprit-libre/brevets-logiciels-comment-michel-rocard-decouvrit-le-logiciel-libre-39936595.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/12/Michel_Rocard_2006_WMC_02.jpg
tags:
- Brevets logiciels
- Europe
series:
- 202205
series_weight: 0
---

> Michel Rocard, alors président de la commission culture au Parlement européen, découvrit en 2001 les logiciels libres... et les libristes. Un témoignage, et un retour sur la bataille contre les brevets logiciels dans l'Union européenne.
