---
site: EurActiv
title: "L'utilisation de Google Analytics viole le droit européen, selon l'autorité autrichienne de protection des données"
date: 2022-01-13
href: https://www.euractiv.fr/section/lactu-en-capitales/news/lutilisation-de-google-analytics-viole-le-droit-europeen-selon-lautorite-autrichienne-de-protection-des-donnees
featured_image: https://fr.euractiv.eu/wp-content/uploads/sites/3/2022/01/NEW-TEMPLATE-CAPITALS-ARTICLE-3-1536x864.png
tags:
- Europe
- Vie privée
series:
- 202202
series_weight: 0
---

> L’utilisation de Google Analytics viole le droit européen, juge l’autorité autrichienne de protection des données.
