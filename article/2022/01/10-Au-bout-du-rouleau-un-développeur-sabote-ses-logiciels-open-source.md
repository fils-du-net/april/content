---
site: 01net.
title: "Au bout du rouleau, un développeur sabote ses logiciels open source"
author: Gilbert Kallenborn
date: 2022-01-10
href: https://www.01net.com/actualites/au-bout-du-rouleau-un-developpeur-sabote-ses-logiciels-open-source-2053434.html
featured_image: https://img.bfmtv.com/c/630/420/0e39/d2b2fa6ad86a5e045fbdd0c4ce91.jpg
tags:
- Sensibilisation
- Économie
series:
- 202202
---

> Le développeur des librairies JavaScript «colors» et «faker», utilisées dans des dizaines de milliers de logiciels les a sabordées. Un événement qui pose, une nouvelle fois, la question de la sécurité de la chaîne logistique logicielle, et de la rémunération des développements open source.
