---
site: InformatiqueNews.fr
title: "Conseils et bonnes pratiques pour réussir son entrée dans la communauté open source"
author: Loïc Duval
date: 2022-01-20
href: https://www.informatiquenews.fr/conseils-et-bonnes-pratiques-pour-reussir-son-entree-dans-la-communaute-open-source-herve-lemaitre-red-hat-83835
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2022/01/shutterstock_397445059.jpg
tags:
- Sensibilisation
series:
- 202203
series_weight: 0
---

> On dit souvent que les débuts sont difficiles; un constat que peuvent partager les personnes qui souhaitent mettre à profit leurs aptitudes pour aider la communauté open source, et particulièrement les développeurs. Au carrefour entre enjeux économiques, de souveraineté, de sécurité et d’éthique, le monde de l’open source est en plein développement. De récentes recherches ont souligné l’influence du secteur à travers des chiffres clés:  il représente un impact économique de 65 à 95 milliards d’euros à l’échelle de l’Union européenne mais également un potentiel d’augmentation du PIB de 100 milliards d’euros à l’échelle mondiale. La production de code qui est à la fois réutilisable par les secteurs privé et public, équivaut à la production de 16 000 développeurs à temps plein. Enfin, l’augmentation de 10% des contributions du code open source contribuerait à créer plus de 600 start-ups technologiques par an.
