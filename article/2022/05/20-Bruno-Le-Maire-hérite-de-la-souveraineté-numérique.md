---
site: Le Monde Informatique
title: "Bruno Le Maire hérite de la souveraineté numérique"
author: Jacques Cheminat
date: 2022-05-20
href: https://www.lemondeinformatique.fr/actualites/lire-bruno-le-maire-herite-de-la-souverainete-numerique-86852.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000085811.png
tags:
- Institutions
series:
- 202220
---

> Nomination: Le ministre de l'Economie et des Finances est reconduit dans ses fonctions en y ajoutant la compétence sur la souveraineté industrielle et numérique.