---
site: Le Monde Informatique
title: "Anticor saisit le PNF sur l'accord Microsoft et Education nationale"
author: Jacques Cheminat
date: 2022-05-13
href: https://www.lemondeinformatique.fr/actualites/lire-anticor-saisit-le-pnf-sur-l-accord-microsoft-et-education-nationale-86771.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000085675.jpg
tags:
- Marchés publics
- Entreprise
- Éducation
series:
- 202219
series_weight: 0
---

> L'association Anticor a annoncé avoir saisi le parquet national financier sur des soupçons de favoritisme concernant le contrat passé entre Microsoft et l'Education nationale. La suite d'un feuilleton ancien.