---
site: Les Echos
title: "Un projet politique nommé open source (€)"
author: Gilles Babinet
date: 2022-05-31
href: https://www.lesechos.fr/idees-debats/editos-analyses/un-projet-politique-nomme-open-source-1410306
featured_image: https://media.lesechos.com/api/v1/images/view/6295db3a3870616b9d1480d6/1280x720/0701673293640-web-tete.jpg
tags:
- International
- Institutions
series:
- 202222
series_weight: 0
---

> L'open source représente une dynamique extraordinaire trop souvent sous-estimée en France, explique Gilles Babinet. Son utilisation accrue permettrait de démocratiser notre culture numérique et d'en faire un outil d'inclusion.
