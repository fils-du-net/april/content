---
site: La Provence
title: "Firefox puissance 100: pourquoi le navigateur demeure l'alternative la plus crédible à Chrome"
author: David Bénard
date: 2022-05-05
href: https://www.laprovence.com/article/sorties-loisirs/6753307/firefox-puissance-100-pourquoi-le-navigateur-demeure-lalternative-la-plus-credible-a-chrome.html
featured_image: https://images.laprovence.com/media/relaxnews/2022-05/2022-05-05/firefox_100.045d7143347.w768.jpg?twic=v1/focus=0x0/cover=1200x675
tags:
- Internet
- Vie privée
series:
- 202218
series_weight: 0
---

> Depuis bientôt une vingtaine d'années, Firefox joue les "troublions" dans le monde des navigateurs Web. Que ce soit sous l'hégémonie d'Internet Explorer ou maintenant sous celle de Chrome, Firefox se pose en alternative libre et respectueuse de la vie privée. Avec sa nouvelle version "100", son éditeur Mozilla confirme son côté avant-gardiste, malgré une popularité en berne depuis quelques années.