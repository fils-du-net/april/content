---
site: ZDNet France
title: "Bruno Lemaire oublie déjà qu'il est le nouveau ministre du Numérique"
author: Guillaume Serries
date: 2022-05-20
href: https://www.zdnet.fr/actualites/bruno-lemaire-oublie-deja-qu-il-est-le-nouveau-ministre-du-numerique-39942411.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/01/bruno%20le%20maire__w1200.jpg
tags:
- Institutions
series:
- 202220
---

> L'absence d'un secrétariat d'Etat dédié au Numérique dans le gouvernement Borne clôt une parenthèse de 16 années où le sujet aura été porté de manière spécifique. C'est désormais Bruno Lemaire qui est en charge de la question, entre autres choses.