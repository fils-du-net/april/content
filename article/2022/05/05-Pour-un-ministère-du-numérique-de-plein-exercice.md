---
site: La Tribune
title: "Pour un ministère du numérique de plein exercice"
date: 2022-05-05
href: https://www.latribune.fr/opinions/tribunes/pour-un-ministere-du-numerique-de-plein-exercice-916582.html
featured_image: https://static.latribune.fr/full_width/1921863/macron-investi-pour-un-second-mandat-samedi-lors-d-une-ceremonie-simple-et-sobre.jpg
tags:
- Institutions
- Économie
series:
- 202218
series_weight: 0
---

> Le numérique est profondément transformateur, d'une grande complexité, et exige la capacité à comprendre, déployer et défendre une vision technologique, sociale, écologique, économique, géostratégique et politique de long terme. Il requiert des moyens et des expertises que seul un ou une ministre de plein exercice peut convoquer et employer à la hauteur des enjeux, estime un collectif de plus de 80 élus, entrepreneurs, ingénieurs, développeurs, associatifs et acteurs du numérique.