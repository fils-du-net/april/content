---
site: ZDNet France
title: "À l'Education nationale, le projet d'un 'Wikipédia des ressources pédagogiques'"
author: Thierry Noisette
date: 2022-05-31
href: https://www.zdnet.fr/blogs/l-esprit-libre/l-education-nationale-le-projet-d-un-wikipedia-des-ressources-pedagogiques-39942730.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/numerique%20livres%20tablette_02.jpg
tags:
- april
- Éducation
- Partage du savoir
- Interopérabilité
series:
- 202222
series_weight: 0
---

> Bâtir le commun numérique des programmes scolaires dans les cinq ans, sur la base du logiciel d'apprentissage en ligne Moodle, c'est le 'rêve' ambitieux du directeur du numérique pour l'éducation, Audran Le Baron.