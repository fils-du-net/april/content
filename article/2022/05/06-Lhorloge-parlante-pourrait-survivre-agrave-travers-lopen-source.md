---
site: Numerama
title: "L'horloge parlante pourrait survivre à travers l'open source"
description: "«Au 4e top, il sera...»"
author: Julien Lausson
date: 2022-05-06
href: https://www.numerama.com/tech/952985-lhorloge-parlante-pourrait-survivre-a-travers-lopen-source.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2019/01/telephone-1024x682.jpg
tags:
- Open Data
series:
- 202218
series_weight: 0
---

> Orange a annoncé la fin de l’horloge parlante pour cet été. Mais, face à la mobilisation de certains internautes attachés au service et à la préservation du patrimoine technologie français, le 3699 pourrait survivre à travers l’open source.