---
site: Libération
title: "Rachat de Twitter: Elon Musk annonce la suspension puis se ravise, l'action s'effondre à Wall Street"
author: Elise Viniacourt
date: 2022-05-13
href: https://www.liberation.fr/economie/economie-numerique/elon-musk-annonce-la-suspension-du-rachat-de-twitter-laction-seffondre-a-wall-street-20220513_EYBTXXYVIBFVJCDUN7MB6LLGFU
featured_image: "https://www.liberation.fr/resizer/Y-cqOWks8t9KaXIcQzBNqrDiSmc=/800x0/filters:format(jpg):quality(70):focal(2056x1648:2066x1658)/cloudfront-eu-central-1.images.arcpublishing.com/liberation/RYJOUYOE5JAMHPGHXRX6I3XPDQ.jpg"
tags:
- Internet
series:
- 202219
---

> Le patron de Tesla et homme le plus riche de la planète a indiqué ce vendredi suspendre le rachat de Twitter. Avant de garantir que l'opération était toujours en cours.