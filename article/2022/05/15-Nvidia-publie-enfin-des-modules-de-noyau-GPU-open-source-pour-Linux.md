---
site: ZDNet France
title: "Nvidia publie enfin des modules de noyau GPU open source pour Linux"
author: Louis Adam
date: 2022-05-15
href: https://www.zdnet.fr/actualites/nvidia-publie-enfin-des-modules-de-noyau-gpu-open-source-pour-linux-39942039.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2021/08/Nvidia__w1200.jpg
tags:
- Logiciels privateurs
- Entreprise
series:
- 202219
series_weight: 0
---

> Les utilisateurs d'ordinateurs de bureau ne devraient pas se réjouir trop vite, mais les clients datacenters bénéficieront d'une fonctionnalité complète dès le départ.