---
site: Les Numeriques
title: "Les initiatives open source"
author: Renaud Labracherie
date: 2022-05-18
href: https://www.lesnumeriques.com/informatique/les-initiatives-open-source-e181097.html
tags:
- Sensibilisation
series:
- 202220
series_weight: 0
---

> L'open source, que l'on peut traduire par code source ouvert, est une véritable philosophie dans l'univers de la programmation. Avec un code accessible et redistribuable, les logiciels ouverts sont généralement le fruit du travail commun de plusieurs programmeurs à travers le monde.