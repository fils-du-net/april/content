---
site: Clubic.com
title: "Avis Firefox: performances au rabais, vie privée au sommet"
author: Chloé Claessens
date: 2022-05-16
href: https://www.clubic.com/navigateur-internet/mozilla-firefox/avis-422592-avis-firefox-performances-au-rabais-vie-privee-au-sommet.html
featured_image: https://pic.clubic.com/v1/images/2006016/raw.webp?fit=max&width=1200&hash=62ba04b34c4c15ce1bc67759f96728bc860d7a20
tags:
- Internet
- Vie privée
series:
- 202220
series_weight: 0
---

> Historiquement opposé à l'hégémonie Internet Explorer, actuel porte-étendard de la résistance contre Chrome, Firefox fait aujourd'hui partie des rares navigateurs grand public à ne pas avoir succombé à Chromium. À tort ou à raison?