---
site: ZDNet France
title: "Les institutions européennes arrivent sur Mastodon et PeerTube, alternatives à Twitter et YouTube"
author: Thierry Noisette
date: 2022-05-08
href: https://www.zdnet.fr/blogs/l-esprit-libre/les-institutions-europeennes-arrivent-sur-mastodon-et-peertube-alternatives-a-twitter-et-youtube-39941723.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2018/12/Bruxelles_Commission_UE_Berlaymont.jpg
tags:
- Europe
- Internet
series:
- 202218
series_weight: 0
---

> Pour diffuser messages, textes, vidéos etc., le Contrôleur européen des données lance deux plateformes, avec les réseaux sociaux libres et décentralisés Mastodon et PeerTube, ouvertes aux institutions de l'UE.