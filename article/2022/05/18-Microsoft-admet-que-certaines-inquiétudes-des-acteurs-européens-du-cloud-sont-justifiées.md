---
site: Le Monde.fr
title: "Microsoft admet que «certaines inquiétudes des acteurs européens du cloud sont justifiées» (€)"
date: 2022-05-18
href: https://www.lemonde.fr/economie/article/2022/05/18/cloud-certaines-inquietudes-des-acteurs-europeens-du-cloud-sont-justifiees-selon-microsoft_6126701_3234.html
tags:
- Entreprise
- Informatique en nuage
series:
- 202220
---

> Brad Smith, le président du géant américain, a annoncé à Bruxelles des concessions sur ses logiciels et ses services d'hébergement, en réponse à une plainte pour abus de position dominante.