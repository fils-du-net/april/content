---
site: Le Monde Informatique
title: "Nvidia publie en open source le driver des GPU pour Linux"
author: Dominique Filippone
date: 2022-05-13
href: https://www.lemondeinformatique.fr/actualites/lire-nvidia-publie-en-open-source-le-driver-des-gpu-pour-linux-86751.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000085631.jpg
tags:
- Logiciels privateurs
- Entreprise
series:
- 202219
---

> Pour travailler de manière plus étroite avec les distributions Linux, Nvidia a décidé de publier en open source le code du driver des GPU pour Linux. Cette ouverture concerne les modules de noyau pour les GPU Ampere et Turing.