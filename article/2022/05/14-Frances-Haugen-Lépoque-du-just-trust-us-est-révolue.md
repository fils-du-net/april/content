---
site: "L'Echo"
title: "Frances Haugen: \"L’époque du ‘just trust us’ est révolue\""
date: 2022-05-14
href: https://www.lecho.be/entreprises/tic/frances-haugen-l-epoque-du-just-trust-us-est-revolue/10388338.html
featured_image: https://images.lecho.be/view?iid=Elvis:8oFi_-vY460BFBoxxfmiZv&context=ONLINE&ratio=16/9&width=1280&u=1652516993000
tags:
- Internet
- Entreprise
series:
- 202219
series_weight: 0
---

> La lanceuse d'alerte sur Facebook s'inquiète de la sécurité dans le métavers alors que les plateformes internet actuelles éprouvent déjà toutes les peines de monde à contrôler les débordements. Mais elle reste optimiste. "De meilleurs médias sont possibles."
