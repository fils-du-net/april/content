---
site: Capital.fr
title: "Microsoft a-t-il été favorisé par l'Éducation nationale? Anticor saisit le PNF"
author: Xavier Martinage
date: 2022-05-12
href: https://www.capital.fr/economie-politique/microsoft-a-t-il-ete-favorise-par-leducation-nationale-anticor-saisit-le-pnf-1436300
featured_image: https://www.capital.fr/imgre/fit/https.3A.2F.2Fi.2Epmdstatic.2Enet.2Fcap.2F2022.2F05.2F12.2Fc7246ac8-380c-4f34-afdc-9e9977bbd8d5.2Ejpeg/790x395/background-color/ffffff/quality/70/microsoft-a-t-il-ete-favorise-par-l-education-nationale-anticor-saisit-le-pnf.jpg
tags:
- Marchés publics
series:
- 202219
---

> L'association anticorruption française dévoile avoir porté plainte le 2 mai après des soupçons de favoritisme dans l'attribution d'un marché public qui remonte à septembre 2020.