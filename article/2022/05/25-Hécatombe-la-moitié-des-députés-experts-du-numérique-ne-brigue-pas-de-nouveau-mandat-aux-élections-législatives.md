---
site: La Tribune
title: "Hécatombe: la moitié des députés experts du numérique ne brigue pas de nouveau mandat aux élections législatives (€)"
author: François Manens et Sylvain Rolland
date: 2022-05-25
href: https://www.latribune.fr/technos-medias/hecatombe-la-moitie-des-deputes-experts-du-numerique-ne-brigue-pas-de-nouveau-mandat-aux-elections-legislatives-919048.html#Echobox=1653471768
featured_image: https://static.latribune.fr/full_width/1711016/assemblee-nationale.jpg
tags:
- Institutions
series:
- 202221
series_weight: 0
---

> Si la maturité globale des députés sur les enjeux numériques a progressé, seuls une dizaine de députés de tous bords se sont illustrés à l'Assemblée nationale lors du premier quinquennat d'Emmanuel Macron. Pire: si des tauliers comme Philippe Latombe, Eric Bothorel, Cédric Villani ou encore Aurore Bergé souhaitent être réélus, la moitié des spécialistes du numérique ne rempile pas, à l'image de Paula Forteza, Mounir Mahjoubi, Laure de La Raudière ou Christine Hennion. La Tribune fait le bilan.