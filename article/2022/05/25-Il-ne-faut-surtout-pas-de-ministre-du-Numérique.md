---
site: "l'Opinion"
title: "«Il ne faut surtout pas de ministre du Numérique»"
author: Giuseppe de Martino
date: 2022-05-25
href: https://www-lopinion-fr.cdn.ampproject.org/c/s/www.lopinion.fr/politique/il-ne-faut-surtout-pas-de-ministre-du-numerique-la-tribune-de-giuseppe-de-martino
featured_image: https://beymedias-brightspotcdn-com.cdn.ampproject.org/i/s/beymedias.brightspotcdn.com/dims4/default/99a95db/2147483647/strip/true/crop/1000x498+0+0/resize/1680x836!/quality/90/?url=http%3A%2F%2Fl-opinion-brightspot.s3.amazonaws.com%2Fd9%2Fa0%2Ff730de8726b9f55ae53d9acd28c5%2Fgiuseppe-de-martino-dr.jpg
tags:
- Institutions
series:
- 202221
---

> Pour le président de l’Association des Services Communautaires Internet (Asic), «un ministère du numérique n’a pas de sens et peut même être contre-productif»
