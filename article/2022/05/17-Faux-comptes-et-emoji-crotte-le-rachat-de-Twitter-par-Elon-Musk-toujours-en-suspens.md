---
site: Paris Match
title: "Faux comptes et emoji crotte: le rachat de Twitter par Elon Musk toujours en suspens"
author: Kahina Sekkai
date: 2022-05-17
href: https://www.parismatch.com/Actu/International/Faux-comptes-et-emoji-crotte-le-rachat-de-Twitter-par-Elon-Musk-toujours-en-suspens-1805965
featured_image: https://resize-parismatch.lanmedia.fr/f/webp/r/1440,960,FFFFFF,forcex,center-middle/img/var/pm/public/media/image/2022/05/17/12/Faux-comptes-et-emoji-crotte-le-rachat-de-Twitter-par-Elon-Musk-toujours-en-suspens.jpg?VersionId=uYLLTf7jmvtkyMovvecCdYoHV45Ym_64
tags:
- Internet
- Entreprise
series:
- 202220
series_weight: 0
---

> Elon Musk a suspend toujours son offre de rachat de Twitter tant que la situation n'aura pas été clarifiée sur la proportion de faux comptes.
