---
site: Basta!
title: "Face à Twitter et compagnie, il existe des alternatives aux multinationales du net"
date: 2022-05-03
href: https://basta.media/face-a-twitter-et-compagnie-il-existe-des-alternatives-aux-multinationales-du
featured_image: https://basta.media/local/adapt-img/960/10x/IMG/logo/arton7473-2.jpg@.webp?1651562000
tags:
- Internet
- Entreprise
series:
- 202218
series_weight: 0
---

> Le milliardaire Elon Musk vient de racheter Twitter, quand Facebook a fait la fortune de Mark Zuckerberg. Peut-être est-il temps de passer à des réseaux sociaux libres des intérêts financiers de grands patrons. Ça existe.