---
site: La Presse
title: "Une action en justice vise la façon dont l'IA est conçue"
author: Cade Metz
date: 2022-12-02
href: https://www.lapresse.ca/affaires/techno/2022-12-02/droits-d-auteur/une-action-en-justice-vise-la-facon-dont-l-ia-est-concue.php
featured_image: https://mobile-img.lpcdn.ca/v2/924x/r3996/f94557913f2532ba8538c27faad4d861.webp
tags:
- Droit d'auteur
series:
- 202248
series_weight: 0
---

> Fin juin, Microsoft a présenté un nouveau type de technologie d'intelligence artificielle (IA) capable de générer son propre code informatique.
