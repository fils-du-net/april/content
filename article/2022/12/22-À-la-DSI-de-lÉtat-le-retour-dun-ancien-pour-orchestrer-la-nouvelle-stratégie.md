---
site: Acteurs Publics 
title: "À la DSI de l'État, le retour d'un ancien pour orchestrer la nouvelle stratégie (€)"
author: Emile Marzolf
date: 2022-12-22
href: https://acteurspublics.fr/articles/a-la-dsi-de-letat-un-ancien-de-retour-pour-orchestrer-la-nouvelle-strategie
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/43/01f7d56d17bde2a268f15fb1966795abd1a889a0.png
tags:
- Administration
series:
- 202251
series_weight: 0
---

> L’animation des travaux de conception de la prochaine feuille de route de la direction interministérielle du numérique (Dinum) a été confiée à Pierre Pezziardi, le co-inventeur des start-up d’État. Une première version de cette nouvelle stratégie centrée sur l’“impact” est déjà sur la table du ministre de la Transformation publique.
