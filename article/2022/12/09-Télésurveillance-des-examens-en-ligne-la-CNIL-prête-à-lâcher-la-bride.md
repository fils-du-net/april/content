---
site: Next INpact
title: "Télésurveillance des examens en ligne: la CNIL prête à lâcher la bride?"
author: Sarah Lepilleur
date: 2022-12-09
href: https://www.nextinpact.com/article/70573/telesurveillance-examens-en-ligne-cnil-prete-a-lacher-bride
featured_image: https://siecledigital.fr/wp-content/uploads/2022/12/mur-camera-surveillance-940x550.jpg
tags:
- Vie privée
series:
- 202249
series_weight: 0
---

> Eric Léandri, ancien patron du moteur de recherche Qwant, a fondé Altrnativ, une société d’espionnage en open-source.
