---
site: Veridik
title: "Monde: Framasoft propose de collectiviser Internet et de voler dans les plumes du capitalisme de surveillance"
author: Gregory Fiori
date: 2022-12-14
href: https://veridik.fr/2022/12/14/monde-framasoft-propose-de-collectiviser-internet-et-de-voler-dans-les-plumes-du-capitalisme-de-surveillance
featured_image: https://veridik.fr/wp-content/uploads/2022/12/Veridik-Monde-Framasoft-propose-de-collectiviser-Internet-et-de-voler-dans-les-plumes-du-capitalisme-de-surveillance-1140x570.jpg
tags:
- Associations
- Internet
series:
- 202250
series_weight: 0
---

> L'association Framasoft propose de s'émanciper des GAFAM et du capitalisme de surveillance en proposant des outils numériques éthiques pour construire un monde meilleur.
