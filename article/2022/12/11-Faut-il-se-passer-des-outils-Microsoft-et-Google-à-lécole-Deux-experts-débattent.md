---
site: L'Express.fr
title: "Faut-il se passer des outils Microsoft et Google à l'école? Deux experts débattent (€)"
author: Stéfane Fermigier
date: 2022-12-11
href: https://www.lexpress.fr/economie/high-tech/faut-il-se-passer-des-outils-microsoft-et-google-a-lecole-deux-experts-debattent-XVXKM53XYBHTPBD2S7HHTNBZTU
featured_image: https://www.lexpress.fr/resizer/JE1WuOynw4GwiKAPkNKBhmBuVt0=/970x548/cloudfront-eu-central-1.images.arcpublishing.com/lexpress/6LSQ23FELNHAVA5PCJT5ZZTNKU.jpg
tags:
- Éducation
series:
- 202249
series_weight: 0
---

> Le ministère de l’Education nationale a récemment appelé à cesser le déploiement des solutions éducatives et de bureautique américaines dans nos écoles. Une question de légalité, mais aussi de politique, à l’heure où la souveraineté numérique est sur toutes les lèvres.
