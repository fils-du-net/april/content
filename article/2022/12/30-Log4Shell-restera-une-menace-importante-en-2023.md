---
site: Le Monde Informatique
title: "Log4Shell restera une menace importante en 2023"
author: Lucas Constantin
date: 2022-12-30
href: https://www.lemondeinformatique.fr/actualites/lire-log4shell-restera-une-menace-importante-en-2023-89050.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000089707.png
tags:
- Sécurité
- Sensibilisation
series:
- 202252
series_weight: 0
---

> Il est probable que la vulnérabilité Log4Shell continuera à être exploitée, car les entreprises manquent de visibilité sur leur chaîne d'approvisionnement logicielle.
