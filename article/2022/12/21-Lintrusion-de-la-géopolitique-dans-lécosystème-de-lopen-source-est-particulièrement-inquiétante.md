---
site: Le Monde.fr
title: "«L'intrusion de la géopolitique dans l'écosystème de l'open source est particulièrement inquiétante» (€)"
author: Alice Pannier
date: 2022-12-21
href: https://www.lemonde.fr/idees/article/2022/12/21/l-intrusion-de-la-geopolitique-dans-l-ecosysteme-de-l-open-source-est-particulierement-inquietante_6155299_3232.html
tags:
- Innovation
series:
- 202251
series_weight: 0
---

> Un an après une faille détectée sur un logiciel open source, les gouvernements ont pris conscience de l'importance stratégique de ce type de programmes, observe, dans une tribune au «Monde», la chercheuse Alice Pannier.
