---
site: Next INpact
title: "Guillaume Poupard rejoindra en janvier Docaposte, la filiale de services numériques du groupe La Poste"
date: 2022-12-14
href: https://www.nextinpact.com/lebrief/70612/guillaume-poupard-rejoindra-docaposte-filiale-services-numeriques-groupe-la-poste
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/wide-linked-media/4971.jpg
tags:
- Institutions
- Informatique en nuage
series:
- 202250
---

> Guillaume Poupard, qui a annoncé début décembre quitter l’ANSSI, qu’il dirigeait depuis 8 ans, deviendra début janvier l'un des quatre directeurs généraux adjoints de Docaposte, la filiale de services numériques du groupe La Poste. L’annonce a été faite sur Twitter.
