---
site: Journal du Net
title: "3 prédictions qui façonneront le paysage de l'open source en 2023"
author: Idit Levine
href: https://www.journaldunet.com/web-tech/cloud/1517545-3-predictions-qui-faconneront-le-paysage-de-l-open-source-en-2023-par-idit-levine-fondateur-et-pdg-de-solo-io
tags:
- Entreprise
series:
- 202250
---

> Idit Levine, PDG de Solo.io, partage ses prévisions pour l'année 2023.
