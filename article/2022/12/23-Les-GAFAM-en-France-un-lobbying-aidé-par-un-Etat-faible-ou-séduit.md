---
site: ZDNet France
title: "Les GAFAM en France: un lobbying aidé par un Etat faible ou séduit"
author: Thierry Noisette
date: 2022-12-23
href: https://www.zdnet.fr/blogs/l-esprit-libre/les-gafam-en-france-un-lobbying-aide-par-un-etat-faible-ou-seduit-39951714.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/marionnettes_puppet-Pixabay.jpg
tags:
- Entreprise
- april
series:
- 202251
series_weight: 0
---

> «Dépenses de lobbying en augmentation rapide, débauchage de hauts fonctionnaires, contacts à l'Élysée, partenariats financiers avec des médias, des think tanks et des institutions de recherche...» L'Observatoire des multinationales a étudié les (grands) moyens du lobbying des GAFAM.
