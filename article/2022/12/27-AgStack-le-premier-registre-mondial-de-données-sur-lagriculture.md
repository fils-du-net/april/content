---
site: Siècle Digital
title: "AgStack: le premier registre mondial de données sur l'agriculture"
author: Valentin Cimino
date: 2022-12-27
href: https://siecledigital.fr/2022/12/27/agstack-le-premier-registre-de-donnees-mondiales-sur-lagriculture
featured_image: https://siecledigital.fr/wp-content/uploads/2022/12/linux-agstack-940x550.jpg
tags:
- Open Data
- Innovation
series:
- 202252
series_weight: 0
---

> La Fondation Linux a annoncé le lancement du projet AgStack pour héberger et gérer les données mondiales sur l'agriculture.
