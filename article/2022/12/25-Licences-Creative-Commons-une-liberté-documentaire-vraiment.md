---
site: Archimag
title: "Licences Creative Commons: une liberté documentaire, vraiment? (€)"
author: Christophe Dutheil
date: 2022-12-22
href: https://www.archimag.com/veille-documentation/2022/12/22/licences-creative-commons-liberte-documentaire-vraiment
featured_image: https://www.archimag.com/sites/archimag.com/files/styles/article/public/web_articles/image/licences-creative-commons-20-ans.jpg
tags:
- Partage du savoir
- Licenses
series:
- 202251
series_weight: 0
---

> Les licences Creative Commons fêtent leurs 20 ans: les 6 premières d'entre elles ont été publiées le 16 décembre 2002. Ce recul permet d'évaluer le système: en quoi a-t-il apporté une liberté de partage des connaissances et de l'information, mais aussi quelles sont ses failles?
