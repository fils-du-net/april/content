---
site: Next INpact
title: "Le lobbying des GAFAM en France est «au même niveau que les plus actifs des groupes du CAC40»"
date: 2022-12-15
href: https://www.nextinpact.com/lebrief/1151
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/wide-linked-media/1801.jpg
tags:
- Entreprise
- Internet
series:
- 202250
series_weight: 0
---

> «GAFAM Nation», un rapport d'une trentaine de pages de l'Observatoire des multinationales (une association qui milite pour l'encadrement des activités de lobbying), dénonce la «toile d'influence des géants du web en France», rapporte l'AFP.
