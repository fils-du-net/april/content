---
site: Next INpact
title: "Amazon, démarchage téléphonique, immobilier, montres fantaisie: la DGCCRF tire tous azimuts (€)"
description: "Les étrennes sont en avance cette année"
author: Sébastien Gavois
date: 2022-12-09
href: https://www.nextinpact.com/article/70575/amazon-demarchage-telephonique-immobilier-montres-fantaisie-dgccrf-tire-tous-azimuts
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/3776.jpg
tags:
- Vie privée
series:
- 202249
---

> Cette fin d’année est chargée du côté des régulateurs. Discord, EDF, Free… la CNIL a multiplié les sanctions pour violation du RGPD ces dernières semaines. La DGCCRF n’est pas en reste avec la mise en ligne de plusieurs enquêtes et une demande de règlement de 3,33 millions d’euros auprès d’Amazon.
