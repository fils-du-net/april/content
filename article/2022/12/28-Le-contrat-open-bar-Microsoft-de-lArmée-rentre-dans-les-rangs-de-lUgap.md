---
site: Le Monde Informatique
title: "Le contrat open bar Microsoft de l'Armée rentre dans les rangs de l'Ugap"
author: Jacques Cheminat
date: 2022-12-28
href: https://www.lemondeinformatique.fr/actualites/lire-le-contrat-open-bar-microsoft-de-l-armee-rentre-dans-les-rangs-de-l-ugap-89030.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000089681.jpg
tags:
- april
- Administration
series:
- 202252
series_weight: 0
---

> Après plusieurs demandes, l'April a obtenu confirmation de la fin du contrat liant Microsoft au ministère de l'Armée. Il a été remplacé par une convention signée en février 2021 où l'Ugap centralise les achats de licences de la Défense. Une petite avancée selon l'association.
