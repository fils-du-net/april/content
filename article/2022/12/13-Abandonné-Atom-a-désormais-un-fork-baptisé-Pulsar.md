---
site: Next INpact
title: "Abandonné, Atom a désormais un fork baptisé Pulsar"
date: 2022-12-13
href: https://www.nextinpact.com/lebrief/70605/abandonne-atom-a-desormais-fork-baptise-pulsar
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/wide-linked-media/6527.jpg
tags:
- Sensibilisation
series:
- 202250
---

> Atom a initialement été développé par GitHub. Avec le rachat de l’entreprise par Microsoft, le destin du projet était plus sombre, la firme de Redmond proposant son Visual Studio Code, basé sur Atom. Depuis, VSC a pris son envol et concentre les efforts de l’éditeur en matière d’IDE open source.
