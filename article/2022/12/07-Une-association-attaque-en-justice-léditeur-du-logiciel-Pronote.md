---
site: ouest-france.fr
title: "Une association attaque en justice l’éditeur du logiciel Pronote (€)"
author: Philippe Rubion
date: 2022-12-07
href: https://www.ouest-france.fr/pays-de-la-loire/angers-49000/angers-education-l-association-interet-a-agir-attaque-en-justice-l-editeur-du-logiciel-pronote-2c5c5f92-763e-11ed-a671-02732ecaf821
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMjEyOGFkMGIyODIwZDljYTA5ZjBiZThkM2JiNDMxZTYyODg?width=940&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=f2f150cda8da6966d16b3fc253f04ca0d3e623887b272a042c96f24ff9ed31d4
tags:
- Éducation
series:
- 202249
series_weight: 0
---

> Intérêt à Agir, dont le siège est à Trélazé, près d’Angers, se bat auprès du pouvoir judiciaire pour défendre l’intérêt général. Le collectif de juristes soutient cette fois l’association apiDV qui dénonce l’inaccessibilité numérique du site et de l’application de vie scolaire Pronote pour les déficients visuels.
