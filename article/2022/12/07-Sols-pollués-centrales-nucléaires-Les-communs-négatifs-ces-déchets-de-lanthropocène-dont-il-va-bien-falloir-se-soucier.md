---
site: Le Monde.fr
title: "Sols pollués, centrales nucléaires… Les «communs négatifs», ces «déchets de l'anthropocène» dont il va bien falloir se soucier (€)"
author: Claire Legros
date: 2022-12-07
href: https://www.lemonde.fr/idees/article/2022/12/07/sols-pollues-centrales-nucleaires-les-communs-negatifs-ces-dechets-de-l-anthropocene-dont-il-va-bien-falloir-se-soucier_6153302_3232.html
tags:
- Économie
series:
- 202249
series_weight: 0
---

> Cette notion récente s'inspire des travaux d'Elinor Ostrom sur les «communs» pour politiser la gouvernance des déchets industriels non recyclables. Trop souvent invisibilisés et relégués dans des territoires pauvres, ils nécessitent une communauté élargie et solidaire pour les prendre en charge.
