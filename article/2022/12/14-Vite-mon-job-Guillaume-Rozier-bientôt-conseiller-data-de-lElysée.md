---
site: Libération
title: "Vite mon job: Guillaume Rozier bientôt conseiller data de l'Elysée?"
author: Chez Pol
date: 2022-12-14
href: https://www.liberation.fr/politique/vite-mon-job-guillaume-rozier-bientot-conseiller-data-de-lelysee-20221214_4GVAO6VXRFFTNBSVZ3ST7PUUEM
featured_image: http://cloudfront-eu-central-1.images.arcpublishing.com/liberation/OPS3M63VZRDSDO33YNVQ5MRDJI.jpg
tags:
- Institutions
- Open Data
series:
- 202250
series_weight: 0
---

> Selon la lettre d'informations «Politico», le concepteur de Covid Tracker et Vite ma dose devrait intégrer l'équipe communication et stratégie de la présidence de la République. Son poste? Conseiller data.
