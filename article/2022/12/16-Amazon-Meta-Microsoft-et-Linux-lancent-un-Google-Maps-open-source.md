---
site: Presse Citron
title: "Amazon, Meta, Microsoft et Linux lancent un 'Google Maps' open source"
author: RPB
date: 2022-12-16
href: https://www.presse-citron.net/amazon-meta-microsoft-et-linux-lancent-un-google-maps-open-source
featured_image: https://www.presse-citron.net/app/uploads/2022/06/henry-perks-BJXAxQ1L7dI-unsplash.jpg
tags:
- Innovation
series:
- 202250
series_weight: 0
---

> Amazon (AWS), Meta, Microsoft et TomTom s'associent à la Linux Foundation pour lancer Overture Maps Foundation, un système de cartographie concurrent de Google Maps mais complètement Open Source.
