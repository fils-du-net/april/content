---
site: Next INpact
title: "À la découverte de Jupyter, un logiciel scientifique libre aux millions d'utilisateurs (€)"
description: "Un logiciel pas si gazeux"
author: Martin Clavey
date: 2022-12-21
href: https://www.nextinpact.com/article/70628/a-decouverte-jupyter-logiciel-scientifique-libre-aux-millions-dutilisateurs
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/8148.jpg
tags:
- Sciences
series:
- 202251
series_weight: 0
---

> Le logiciel Jupyter, dérivé en 2014 de iPython, a créé avec ses «carnets de notes» un standard du monde académique dans le partage des données et connaissances. Il a depuis été largement adopté par la communauté de la donnée. La conférence Open Science Days était l’occasion d’en apprendre davantage sur ses évolutions passées et à venir. 
