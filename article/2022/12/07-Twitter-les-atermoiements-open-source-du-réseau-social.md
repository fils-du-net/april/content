---
site: Silicon
title: "Twitter: les atermoiements open source du réseau social"
author: Ariane Beky
date: 2022-12-07
href: https://www.silicon.fr/twitter-atermoiements-open-source-454555.html
featured_image: https://www.silicon.fr/wp-content/uploads/2014/06/twitter.jpg
tags:
- Internet
series:
- 202249
---

> Contribuer activement à des projets open source ne serait plus d'actualité pour Twitter depuis son rachat par Elon Musk.
