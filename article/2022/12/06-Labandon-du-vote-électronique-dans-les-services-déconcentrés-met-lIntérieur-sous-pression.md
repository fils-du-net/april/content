---
site: Acteurs Publics 
title: "L'abandon du vote électronique dans les services déconcentrés met l'Intérieur sous pression (€)"
author: Bastien Scordia
date: 2022-12-06
href: https://acteurspublics.fr/articles/labandon-du-vote-electronique-dans-les-services-deconcentres-met-linterieur-sur-le-pied-de-guerre
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/42/6346242b39ad356b5e4e0184ad27a1d89a73be73.jpeg
tags:
- Vote électronique
series:
- 202249
---

> Dans une instruction adressée aux services déconcentrés, la Place Beauvau détaille les modalités d’organisation du vote à l’urne dans les directions départementales interministérielles (DDI). Un mode de scrutin décidé à la dernière minute après l’abandon du vote électronique. L’Intérieur appelle les services déconcentrés à tout faire pour favoriser la participation de leurs agents, dont le niveau de mobilisation reste incertain.
