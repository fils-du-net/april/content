---
site: Marianne
title: "GAFAM: 'La France est dans une forme de servitude volontaire' (€)"
author: Emilien Hertement
date: 2022-12-20
href: https://www.marianne.net/societe/big-brother/gafam-la-france-est-dans-une-forme-de-servitude-volontaire
featured_image: https://resize.marianne.net/r/1540,924/img/var/LQ11517036C/659735/080_HL_JCMILHET_1920764.jpg
tags:
- Entreprise
series:
- 202251
---

> Dans un rapport publié le 13 décembre 2022, l'Observatoire des multinationales dénonce l'influence des GAFAM (Google, Apple, Facebook, Amazon et Microsoft), et la toile tissée par ces géants du numérique dans tous les secteurs de la société. Chiara Pignatelli, corédactrice du rapport, appelle à un encadrement renforcé et davantage transparent de leur action en France. Entretien.
