---
site: Silicon
title: "Écarter les développeurs open source menace le fonctionnement des systèmes informatiques"
author: Lorna Mitchell
date: 2022-12-19
href: https://www.silicon.fr/avis-expert/ecarter-les-developpeurs-open-source-menace-le-fonctionnement-des-systemes-informatiques
featured_image: https://www.silicon.fr/wp-content/uploads/2012/10/Programmation-IDE-Orion-Eclipse-web-open-source-%C2%A9-Vicente-Barcelo-Varona-Shutterstock-684x513.jpg
tags:
- Sensibilisation
series:
- 202251
series_weight: 0
---

> Qu’il s’agisse de créer une nomenclature de logiciels pour les applications utilisées par une organisation ou de soutenir les projets et les développeurs de logiciels open source, les entreprises doivent redoubler d’efforts pour contribuer à réduire leurs risques.
