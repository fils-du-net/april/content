---
site: ZDNet France
title: "Libre et open source express: dons aux associations, alliance anti-Google Maps, manifeste pour un numérique sobre"
author: Thierry Noisette
date: 2022-12-26
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-dons-aux-associations-alliance-anti-google-maps-manifeste-pour-un-numerique-sobre-39951742.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/05/open-source-KeepCalmAndUse.jpg
tags:
- april
- Associations
series:
- 202252
series_weight: 0
---

> Fêtes et cadeaux: pensez à soutenir les associations libristes et proches. Une alliance pour une cartographie libre... anti-Google Maps. Manifeste néo-aquitain-francilien pour un numérique sobre grâce aux logiciels libres.
