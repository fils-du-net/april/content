---
site: Journal du Net
title: "Le logiciel libre, sujet marketing des GAFAM?"
author: Francois Caron
date: 2022-12-15
href: https://www.journaldunet.com/web-tech/cloud/1517419-logiciel-libre-un-marketing-des-gafam
featured_image: https://img-0.journaldunet.com/qngazzQIJBeWxs1eDtyP2K6bqEU=/450x/smart/32bfde52110d45bf973a615d8e6f027f/ccmcms-jdn/39439824.png
tags:
- Économie
- Entreprise
series:
- 202250
series_weight: 0
---

> Drôle de question penserez-vous. Mais observons surtout que les GAFAM surfent sur les termes "logiciel libre" et "open source". Mais pourquoi ces nouvelles pratiques marketing sont "inno-ventes"?
