---
site: ZDNet France
title: "Procès Linagora: la cour d'appel de Paris donne raison aux fondateurs de Blue Mind"
author: Thierry Noisette
date: 2022-12-04
href: https://www.zdnet.fr/blogs/l-esprit-libre/proces-linagora-la-cour-d-appel-de-paris-donne-raison-aux-fondateurs-de-blue-mind-39950730.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/06/marteau_justice_pxhere.jpeg
tags:
- Institutions
- Entreprise
series:
- 202248
series_weight: 0
---

> «Le respect du principe de la liberté du commerce et de l'industrie exige que l'interdiction de concurrence soit délimitée», juge la cour d'appel de Paris, qui conclut que les deux fondateurs de Blue Mind n'avaient pas manqué à leur obligation de non-concurrence.
