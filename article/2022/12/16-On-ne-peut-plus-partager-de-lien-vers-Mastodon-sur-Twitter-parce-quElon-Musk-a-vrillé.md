---
site: Numerama
title: "On ne peut plus partager de lien vers Mastodon sur Twitter, parce qu'Elon Musk a vrillé"
description: "Pas de concurrence chez Elon Musk"
author: Nicolas Lellouche
date: 2022-12-16
href: https://www.numerama.com/tech/1213728-on-ne-peut-plus-partager-de-lien-vers-mastodon-sur-twitter-parce-quelon-musk-a-vrille.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2022/12/sadmastodon-twitter-1-1024x576.jpg?webp=1&key=5dc99b42
tags:
- Internet
series:
- 202250
series_weight: 0
---

> Après s'être plusieurs fois moqué de Mastodon et avoir réduit la visibilité du réseau social open source, Elon Musk interdit désormais de partager sur Twitter un lien menant vers Mastodon, en public comme en privé. Elon Musk a aussi fait bannir le compte officiel de Mastodon. Qu'arrive-t-il à Elon Musk?
