---
site: cio-online.com
title: "L'open source répond aux attentes, mais décélère légèrement en production"
author: Aurélie Chandeze
date: 2022-12-29
href: https://www.cio-online.com/actualites/lire-l-open-source-repond-aux-attentes-mais-decelere-legerement-en-production-14666.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000019435.jpg
tags:
- Entreprise
series:
- 202252
series_weight: 0
---

> Selon une étude VMware, les solutions open source en entreprise sont en phase avec les attentes, mais quelques freins persistent autour de la sécurité.
