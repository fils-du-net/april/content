---
site: Numerama
title: "Elon Musk propose de démissionner de Twitter avec un sondage: le «Oui» l'emporte"
description: "Bannir Facebook et Instagram, mauvaise idée?"
author: Nicolas Lellouche
date: 2022-12-19
href: https://www.numerama.com/tech/1214520-elon-musk-propose-de-demissionner-de-twitter-avec-un-sondage.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2022/10/sans-titre-2-1024x576.jpg?webp=1&key=8b31ac78
tags:
- Internet
series:
- 202251
series_weight: 0
---

> Après avoir banni les liens vers Mastodon sans aucune raison valable, Twitter a annoncé qu’il était désormais interdit de promouvoir Facebook, Instagram, Truth et d’autres réseaux sociaux, au risque d’être banni. Une censure qui a contraint Elon Musk à s’excuser.
