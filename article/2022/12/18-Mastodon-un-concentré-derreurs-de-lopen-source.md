---
site: Le Monde Informatique
title: "Mastodon, un concentré d'erreurs de l'open source"
author: Adam Taylor
date: 2022-11-22
href: https://www.lemondeinformatique.fr/actualites/lire-mastodon-un-concentre-d-erreurs-de-l-open-source-88675.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000089006.png
tags:
- Internet
series:
- 202250
---

> Mastodon existe depuis des années et n'a pas connu le succès retentissant d'autres réseaux sociaux. Malgré ses valeurs mises en avant - open source, décentralisé, dépourvu de toute publicité - le service reste ce qu'il est: une alternative à Twitter bourrée de complexités
