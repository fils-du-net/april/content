---
site: Youmatter
title: "Mastodon, Twitter: le logiciel libre pour une société émancipée?"
author: Florentin Roy
date: 2022-12-05
href: https://youmatter.world/fr/mastodon-nouvelle-ere-logiciel-libre-twitter
featured_image: https://youmatter.world/app/uploads/sites/3/2022/12/Logiciel-libre.jpg
tags:
- Internet
series:
- 202249
series_weight: 0
---

> En réponse aux monopôles des géants du numérique, des mouvements politiques de logiciels libres bataillent pour offrir plus de liberté et plus de contrôle aux utilisateurs du numérique. Depuis que le rachat de Twitter par le fondateur de Tesla Elon Musk est acté, un nombre croissant d'utilisateurs quittent le réseau social pour se diriger vers un logiciel libre: Mastodon.
