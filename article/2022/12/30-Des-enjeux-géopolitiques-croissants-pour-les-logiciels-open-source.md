---
site: ZDNet France
title: "Des enjeux géopolitiques croissants pour les logiciels open source"
author: Thierry Noisette
date: 2022-12-30
href: https://www.zdnet.fr/blogs/l-esprit-libre/des-enjeux-geopolitiques-croissants-pour-les-logiciels-open-source-39951848.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/Ifri_pannier_opensource.jpg
tags:
- International
- Sécurité
series:
- 202252
series_weight: 0
---

> Une étude de l’Ifri sur les enjeux économiques et géopolitiques des logiciels open source s’intéresse particulièrement aux Etats-Unis, à la Chine et à l’Europe.
