---
site: Basta!
title: "Profs, parents d'élèves et activistes se bougent pour libérer l'école de l'emprise de Google et Microsoft"
author: Rachel Knaebel
date: 2022-09-15
href: https://basta.media/profs-parents-d-eleves-et-activistes-se-bougent-pour-liberer-l-ecole-des-Gafam
featured_image: https://basta.media/local/adapt-img/960/10x/IMG/logo/salledeclasse.jpg@.webp?1663152299
tags:
- Éducation
series:
- 202237
series_weight: 0
---

> Les Gafam, multinationales du numérique comme Google ou Microsoft, prennent toujours plus de place dans les écoles et mettent la main sur les données personnelles des élèves et de leurs parents. Des alternatives s'appuyant sur des logiciels libres émergent grâce à l'initiative d'enseignants, de parents et de hackers.
