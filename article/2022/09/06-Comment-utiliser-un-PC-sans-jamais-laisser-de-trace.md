---
site: 01net.
title: "Comment utiliser un PC sans jamais laisser de trace?"
author: Geoffroy Ondet
date: 2022-09-06
href: https://www.01net.com/astuces/comment-utiliser-un-pc-sans-jamais-laisser-de-trace.html
featured_image: https://www.01net.com/app/uploads/2022/09/incognito.jpg
tags:
- Sensibilisation
- Vie privée
series:
- 202236
series_weight: 0
---

> Utilisez n'importe quel ordinateur sans jamais laisser de trace de vos activités grâce à un système d'exploitation live entièrement sécurisé.
