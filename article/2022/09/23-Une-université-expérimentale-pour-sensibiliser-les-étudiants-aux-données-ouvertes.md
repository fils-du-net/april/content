---
site: Next INpact
title: "Une université «expérimentale» pour sensibiliser les étudiants aux «données ouvertes» (€)"
description: Open datattitude
author: Jean-Marc Manach
date: 2022-09-23
href: https://www.nextinpact.com/article/70023/une-universite-experimentale-pour-sensibiliser-etudiants-aux-donnees-ouvertes
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/12362.jpg
tags:
- Open Data
series:
- 202238
series_weight: 0
---

> Lancée à titre expérimental, l'Open Data University vise à «favoriser la création de services publics augmentés de manière systémique», ainsi qu'à «détecter les talents du numérique qui veulent œuvrer pour l’intérêt général», en aidant les étudiants à contribuer à des projets de réutilisations de données ouvertes.
