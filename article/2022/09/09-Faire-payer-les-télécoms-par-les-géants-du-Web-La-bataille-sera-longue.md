---
site: Le Monde.fr
title: "Faire payer les télécoms par les géants du Web? La bataille sera longue (€)"
author: Alexandre Piquard et Olivier Pinaud
date: 2022-09-09
href: https://www.lemonde.fr/economie/article/2022/09/09/faire-payer-les-telecoms-par-les-geants-du-web-la-bataille-sera-longue_6140954_3234.html
tags:
- Institutions
- Internet
series:
- 202236
---

> Alors que les opérateurs militent pour faire financer les infrastructures par les Google et autres Netflix, le commissaire européen Thierry Breton annonce une large consultation sur «la régulation des réseaux».
