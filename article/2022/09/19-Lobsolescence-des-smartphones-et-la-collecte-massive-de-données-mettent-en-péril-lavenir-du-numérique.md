---
site: Basta!
title: "L'obsolescence des smartphones et la collecte massive de données mettent en péril l'avenir du numérique"
author: Emma Bougerol
date: 2022-09-19
href: https://basta.media/l-obsolescence-des-smartphones-et-la-collecte-massive-de-donnees-impact-ecologique-du-numerique
featured_image: https://basta.media/local/adapt-img/960/10x/IMG/logo/carte-mere-impact-environnement-ecologie-digital-numerique-ordinateur.jpg@.webp?1661862926
tags:
- Innovation
series:
- 202238
series_weight: 0
---

> Le numérique représente 10% de la consommation d'électricité en France, et 2,5% de ses émissions de CO2. Des minerais nécessaires aux appareils à l'énergie consommée par les data centers, le numérique a des effets écologiques en augmentation constante. Mais ce n'est pas forcément réduire son usage d'Internet qui est décisif pour la sobriété énergétique.
