---
site: Le Monde.fr
title: "«Les communs numériques, une alternative au contrôle total des données par le privé ou le public» (€)"
author: Serge Abiteboul, François Bancilhon
date: 2022-09-16
href: https://www.lemonde.fr/idees/article/2022/09/16/les-communs-numeriques-une-alternative-au-controle-total-des-donnees-par-le-prive-ou-le-public_6141916_3232.html
tags:
- Vie privée
series:
- 202237
series_weight: 0
---

> TRIBUNE. Echapper à l'emprise des Etats ou des entreprises sur les données est possible, tous les outils existent pour cela, détaillent les deux informaticiens.
