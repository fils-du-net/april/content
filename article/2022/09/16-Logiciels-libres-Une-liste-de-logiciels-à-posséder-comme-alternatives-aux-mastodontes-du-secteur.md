---
site: Breizh Info
title: "Logiciels libres. Une liste de logiciels à posséder comme alternatives aux mastodontes du secteur"
date: 2022-09-16
href: https://www.breizh-info.com/2022/09/16/207988/logiciels-libres-une-liste-de-logiciels-a-posseder-comme-alternatives-aux-mastodontes-du-secteur
featured_image: https://www.breizh-info.com/wp-content/uploads/2022/09/open-source-g68e75ae0e_640.jpeg.webp
tags:
- Sensibilisation
series:
- 202237
series_weight: 0
---

> Dans un but de sécurisation de nos données personnelles puisque les entreprises commerciales peuvent en faire ce qu’elles en veulent, il est préférable d’utiliser des logiciels libres ou open source qui n’interceptent pas vos données et ne peuvent pas s’en servir tout simplement parce que leur code est open source (en libre accès) et que tout le monde peut l’auditer et vérifier que le logiciel est sécurisé.
