---
site: ZDNet France
title: "Comment installer LibreOffice sur macOS?"
author: Jack Wallen
date: 2022-09-07
href: https://www.zdnet.fr/pratique/comment-installer-libreoffice-sur-macos-39946840.htm
featured_image: https://www.zdnet.com/a/img/2020/04/22/60c3f42c-e91b-4aa9-9e6f-8debe201eb51/computeruserwindowistock-1127291554.jpg
tags:
- Sensibilisation
series:
- 202236
---

> Voici comment installer et utiliser LibreOffice sur macOS.
