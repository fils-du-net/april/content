---
site: ZDNet France
title: "La fondation Linux franchit l'Atlantique"
author: Thierry Noisette
date: 2022-09-29
href: https://www.zdnet.fr/blogs/l-esprit-libre/la-fondation-linux-franchit-l-atlantique-39947886.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2020/01/Linux%20Foundation%20logo.jpg
tags:
- Associations
- International
series:
- 202239
series_weight: 0
---

> La Linux Foundation Europe est créée à Bruxelles, avec pour DG Gabriele Columbro, également directeur exécutif de la Fintech Open Source Foundation (FINOS).
