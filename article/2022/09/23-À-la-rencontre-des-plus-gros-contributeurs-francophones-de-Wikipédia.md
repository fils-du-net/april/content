---
site: Slate.fr
title: "À la rencontre des plus gros contributeurs francophones de Wikipédia"
author: Vincent Bresson
date: 2022-09-23
href: https://www.slate.fr/tech-internet/la-libre-encyclopedie/episode-3-wikipedia-plus-gros-contributeurs-francophones
featured_image: https://www.slate.fr/sites/default/files/styles/1060x523/public/000_par1103606.jpeg
tags:
- Partage du savoir
series:
- 202238
series_weight: 0
---

> Wikipédia est beaucoup plus qu'une simple encyclopédie aux yeux des contributeurs francophones les plus actifs, dont certains ont déjà dépassé le million de modifications.
