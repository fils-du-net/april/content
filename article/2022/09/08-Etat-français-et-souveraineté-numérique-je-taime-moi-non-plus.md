---
site: Journal du Net
title: "Etat français et souveraineté numérique: je t'aime moi non plus"
author: Pierre Baudracco
date: 2022-09-08
href: https://www.journaldunet.com/solutions/dsi/1514231-etat-francais-et-souverainete-numerique-je-t-aime-moi-non-plus
tags:
- Institutions
series:
- 202236
series_weight: 0
---

> A en croire l'Etat français et les grandes entreprises qui pleurent sur la perte de leur souveraineté numérique, les solutions américaines sont une fatalité par manque d'alternative... Vraiment?
