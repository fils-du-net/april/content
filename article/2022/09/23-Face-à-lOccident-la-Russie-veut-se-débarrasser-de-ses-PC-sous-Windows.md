---
site: Clubic.com
title: "Face à l'Occident, la Russie veut se débarrasser de ses PC sous Windows"
author: Alexandre Boero
date: 2022-09-23
href: https://www.clubic.com/pro/technologie-et-politique/actualite-438804-face-a-l-occident-la-russie-veut-se-debarrasser-de-ses-pc-sous-windows.html
featured_image: https://pic.clubic.com/v1/images/2031021/raw.webp?fit=max&width=1200&hash=5d7339c204b9fa2903bf24794491776cc609e4c1
tags:
- International
- Logiciels privateurs
series:
- 202238
series_weight: 0
---

> En réponse aux sanctions des puissances occidentales, la Russie est en train d'écarter Windows du paysage. Moscou veut faire la part belle à Linux et à l'open source.
