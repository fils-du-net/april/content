---
site: Next INpact
title: "Contrôle parental sur les écrans connectés: ce qu'envisage le gouvernement (€)"
description: UN été studieux, une rentrée Studer
author: Marc Rees
date: 2022-09-01
href: https://www.nextinpact.com/article/69861/controle-parental-sur-ecrans-connectes-ce-quenvisage-gouvernement
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/5663.jpg
tags:
- Institutions
series:
- 202235
---

> Un contrôle parental sur chaque écran connecté. Tel est l’objet de la loi de mars 2022, portée par le député Bruno Studer (LREM). Next INpact vous révèle la dernière brique de l’édifice: son futur décret, qui devrait être soumis à consultation publique avant présentation en Conseil des ministres, après une réunion avec les acteurs concernés.
