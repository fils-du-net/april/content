---
site: ZDNet France
title: "Les vétérans de Linux s'unissent derrière CIQ, la société mère de Rocky Linux"
author: Steven Vaughan-Nichols
date: 2022-09-05
href: https://www.zdnet.fr/actualites/les-veterans-de-linux-s-unissent-derriere-ciq-la-societe-mere-de-rocky-linux-39946668.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/01/iStock-1212064060__w1200.jpg
tags:
- Entreprise
series:
- 202236
series_weight: 0
---

> Les nouveaux dirigeants de CIQ soutiendront Rocky Linux tout en l'utilisant comme base pour les offres de calcul haute performance traditionnelles et en cloud de la société.
