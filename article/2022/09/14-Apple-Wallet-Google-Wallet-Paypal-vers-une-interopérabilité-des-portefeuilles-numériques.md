---
site: Clubic.com
title: "Apple Wallet, Google Wallet, Paypal... vers une interopérabilité des portefeuilles numériques?"
author: Mathieu Grumiaux
date: 2022-09-14
href: https://www.clubic.com/paiement-mobile/actualite-437622-apple-wallet-google-wallet-paypal-vers-une-interoperabilite-des-portefeuilles-numeriques.html
featured_image: https://pic.clubic.com/v1/images/2040408/raw.webp?fit=max&width=1200&hash=2297b2122789c6084972491289aa375d31f31b62
tags:
- Interopérabilité
series:
- 202237
series_weight: 0
---

> Une solution open-source sera prochainement dévoilée afin de permettre aux éditeurs de rendre compatibles leurs portefeuilles numériques avec toutes les plateformes.
