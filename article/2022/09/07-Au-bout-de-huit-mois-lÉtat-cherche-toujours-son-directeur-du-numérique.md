---
site: Acteurs Publics 
title: "Au bout de huit mois, l'État cherche toujours son directeur du numérique"
author: Emile Marzolf
date: 2022-09-07
href: https://acteurspublics.fr/articles/au-bout-de-huit-mois-letat-cherche-toujours-son-directeur-du-numerique
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/18/e3e2755bdea9b1f2d35d9c6d2c0df4b1365b2fb2.jpeg
tags:
- Institutions
series:
- 202236
---

> La nomination du prochain directeur interministériel se fait attendre depuis le début de l’année. Plusieurs candidats ont été reçus par le ministère de la Transformation publique. L’arbitrage politique devrait être rendu d’ici fin septembre.
