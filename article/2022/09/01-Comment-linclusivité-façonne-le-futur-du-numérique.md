---
site: Usbek & Rica
title: "Comment l'inclusivité façonne le futur du numérique"
date: 2022-09-01
href: https://usbeketrica.com/fr/article/comment-l-inclusivite-faconne-le-futur-du-numerique
featured_image: https://usbeketrica.com/media/97622/download/john-schnobrich-FlPc9_VocJ4-unsplash.jpg?v=1&inline=1
tags:
- Sensibilisation
- Associations
series:
- 202235
---

> Faire rimer numérique avec émancipation et entraide, c'est l'objectif de nombreuses formations et associations qui embarquent tous ceux qui en sont encore trop souvent exclus. Focus sur quelques initiatives qui sèment les graines du numérique de demain.
