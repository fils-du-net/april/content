---
site: Le Monde Informatique
title: "L'IA Act de l'UE, un fardeau pour la communauté open source"
author: Célia Seramour
date: 2022-09-12
href: https://www.lemondeinformatique.fr/actualites/lire-l-ia-act-de-l-ue-un-fardeau-pour-la-communaute-open-source-87970.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000087719.jpg
tags:
- Europe
- Institutions
series:
- 202237
series_weight: 0
---

> Encore au stade de proposition, le futur règlement de l'UE sur l'intelligence artificielle fait déjà des étincelles. Son application telle que décrite par la Commission européenne pourrait nuire au développement de modèles open source créés par des développeurs indépendants.
