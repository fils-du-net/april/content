---
site: 01net.
title: "Le pape du web chiffré, Peter Eckersley, est mort"
date: 2022-09-05
href: https://www.01net.com/actualites/le-pape-du-web-chiffre-peter-eckersley-est-mort.html
featured_image: https://www.01net.com/wp/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2
tags:
- Internet
- Sciences
series:
- 202236
---

> Si le web est chiffré à plus de 80 %, c'est notamment grâce à ce chercheur en sécurité et son projet Let's Encrypt, qui a facilité le déploiement de certificats TLS.