---
site: Slate.fr
title: "L'un des cofondateurs de Wikipédia est devenu son critique le plus féroce, et ça arrange les haters"
author: Vincent Bresson
date: 2022-09-16
href: https://www.slate.fr/tech-internet/la-libre-encyclopedie/episode-2-larry-sanger-cofondateur-wikipedia-detracteur-nupedia-wiki
featured_image: https://www.slate.fr/sites/default/files/styles/1060x523/public/larry-sanger.jpg
tags:
- Partage du savoir
series:
- 202237
series_weight: 0
---

> L'histoire de Wikipédia est indissociable de celle de ses deux cofondateurs. Mais le discours très critique de l'un d'entre eux apporte de l'eau au moulin des détracteurs de l'encyclopédie en ligne... et notamment de ceux d'extrême droite.
