---
site: Le Monde Informatique
title: "La Fondation Linux accélère sur la modernisation des mainframes"
author: Michael Cooney
date: 2022-09-22
href: https://www.lemondeinformatique.fr/actualites/lire-la-fondation-linux-accelere-sur-la-modernisation-des-mainframes-88085.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000087942.jpg
tags:
- Promotion
series:
- 202238
---

> Le projet Open Mainframe de la Fondation Linux vise le développement d'applications et de compétences pour les mainframes, et dispose désormais de son propre système pour travailler.
