---
site: L'OBS
title: "Combien tu vaux? Benjamin, développeur, 1 800 euros par mois: «la réussite professionnelle? Je ne comprends pas la question» (€)"
author: Agathe Ranc
date: 2022-09-10
href: https://www.nouvelobs.com/rue89/20220910.OBS63026/benjamin-developpeur-1-800-euros-par-mois-la-reussite-professionnelle-je-ne-comprends-pas-la-question.html
featured_image: https://focus.nouvelobs.com/2022/05/02/0/17/1228/614/975/0/75/0/658060e_58572633-obs-salaires-web.jpg/webp
tags:
- Économie
- Entreprise
series:
- 202236
series_weight: 0
---

> COMBIEN TU VAUX? Que dit votre rémunération de vous et de votre profession? Benjamin et Noëmie ne font pas le même métier, mais ils gagnent le même salaire. Comme tous leurs collègues.
