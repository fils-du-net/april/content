---
site: Contrepoints
title: "Les réseaux sociaux vous espionnent, contrez-les!"
author: François Jolain
date: 2022-09-10
href: https://www.contrepoints.org/2022/09/10/438330-les-reseaux-sociaux-vous-espionnent-contrez-les
featured_image: https://www.contrepoints.org/wp-content/uploads/2019/07/VPN-and-internet-security-by-Mike-MacKenzieCC-BY-2.0--1200x800.jpg
tags:
- Vie privée
series:
- 202236
series_weight: 0
---

> Les réseaux sociaux sont devenus des armes dont nous sommes les munitions. Heureusement, nous pouvons arrêter les tirs.
