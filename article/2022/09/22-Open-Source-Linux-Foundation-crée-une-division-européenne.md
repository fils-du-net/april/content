---
site: Siècle Digital
title: "Open Source: Linux Foundation crée une division européenne"
author: Marc Premieux
date: 2022-09-22
href: https://siecledigital.fr/2022/09/22/open-source-linux-foundation-cree-une-division-europeenne
featured_image: https://siecledigital.fr/wp-content/uploads/2022/09/Linux-Foundation-linuxfoundation.org_-940x550.jpg
tags:
- Promotion
series:
- 202238
series_weight: 0
---

> Linux Foundation a annoncé le 16 septembre la création d'une branche en Europe, à Bruxelles, pour développer les projets open-source.
