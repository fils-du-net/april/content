---
site: Silicon
title: "7 projets open source made in Apple"
author: Clément Bohic
date: 2022-09-02
href: https://www.silicon.fr/7-projets-open-source-made-in-apple-446140.html
featured_image: https://www.silicon.fr/wp-content/uploads/2022/09/7-projets-open-source-Apple.jpg
tags:
- Entreprise
series:
- 202235
series_weight: 0
---

> Moins fourni que celui des autres GAFAM, le portefeuille open source d'Apple comprend tout de même quelques références.
