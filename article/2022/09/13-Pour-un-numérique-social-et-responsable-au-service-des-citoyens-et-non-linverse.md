---
site: Le Monde.fr
title: "«Pour un numérique social et responsable au service des citoyens, et non l'inverse» (€)"
date: 2022-09-13
href: https://www.lemonde.fr/idees/article/2022/09/13/pour-un-numerique-social-et-responsable-au-service-des-citoyens-et-non-l-inverse_6141426_3232.html
tags:
- Administration
series:
- 202237
series_weight: 0
---

> TRIBUNE. Il est urgent de penser un numérique sobre et de garantir une alternative humaine au numérique pour tous les services publics, plaident, dans une tribune au «Monde», dix représentants des collectivités territoriales membres du collectif Belle Alliance.
