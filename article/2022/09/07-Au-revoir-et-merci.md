---
site: Next INpact
title: "Au revoir et merci Next INpact"
description: "Next!"
author: Marc Rees
date: 2022-09-07
href: https://www.nextinpact.com/blog/69904/au-revoir-et-merci-next-inpact
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/4034.jpg
tags:
- Partage du savoir
series:
- 202236
series_weight: 0
---

> Ce mois de septembre sera un peu particulier. Ce sera mon dernier dans les colonnes de Next INpact. Après plus de 17 ans de bons et loyaux services, j’ai décidé de tourner la page pour écrire un nouveau chapitre.
