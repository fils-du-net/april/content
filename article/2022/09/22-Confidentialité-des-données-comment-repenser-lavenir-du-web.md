---
site: Journal du Net
title: "Confidentialité des données: comment repenser l'avenir du web?"
author: Andy Yen
date: 2022-09-22
href: https://www.journaldunet.com/solutions/dsi/1514625-confidentialite-des-donnees-comment-repenser-l-avenir-du-web
tags:
- Vie privée
- Internet
series:
- 202238
series_weight: 0
---

> Initialement fondé sur des valeurs démocratiques, force est de constater que l'internet que nous connaissons aujourd'hui s'éloigne chaque jour un peu plus de la vision de ses créateurs.
