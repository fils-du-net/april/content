---
site: Silicon
title: "Open source en Europe: le constat cru de la Fondation Linux"
author: Clément Bohic
date: 2022-09-15
href: https://www.silicon.fr/open-source-europe-constat-cru-fondation-linux-447366.html
featured_image: https://www.silicon.fr/wp-content/uploads/2022/09/open-source-Europe-Fondation-Linux.png
tags:
- Promotion
series:
- 202237
series_weight: 0
---

> Les Européens et l'open source, une relation de l'ordre du «romantique»? La Fondation Linux l'affirme. Sur quels fondements?
