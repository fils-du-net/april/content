---
site: Acteurs Publics
title: "La Première ministre place l'une de ses proches à la tête de la DSI de l'État (€)"
author: Emile Marzolf
date: 2022-09-26
href: https://acteurspublics.fr/articles/la-premiere-ministre-place-lune-de-ses-proches-a-la-tete-de-la-dsi-de-letat
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/41/e9ae70af559f174b0278b8991011809616933247.jpeg
tags:
- Institutions
series:
- 202239
series_weight: 0
---

> Après huit mois d’atermoiements, la direction interministérielle du numérique (Dinum) connaît enfin le nom de sa nouvelle patronne: Stéphanie Schaer. Une polytechnicienne adepte des start-up d’État, nommée en Conseil des ministres ce lundi 26 septembre.
