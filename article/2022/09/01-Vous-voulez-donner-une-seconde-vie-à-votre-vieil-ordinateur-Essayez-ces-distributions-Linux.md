---
site: ZDNet France
title: "Vous voulez donner une seconde vie à votre vieil ordinateur? Essayez ces distributions Linux"
author: Jack Wallen
date: 2022-09-01
href: https://www.zdnet.fr/pratique/vous-voulez-donner-une-seconde-vie-a-votre-vieil-ordinateur-essayez-ces-distributions-linux-39946572.htm
featured_image: https://www.zdnet.com/a/img/2022/08/29/63531447-fa6a-4263-9ddc-c567f8b1d41b/gettyimages-88622716.jpg
tags:
- Sensibilisation
series:
- 202235
series_weight: 0
---

> Voici cinq distributions Linux qui pourront redonner vie à du matériel informatique vieillissant.
