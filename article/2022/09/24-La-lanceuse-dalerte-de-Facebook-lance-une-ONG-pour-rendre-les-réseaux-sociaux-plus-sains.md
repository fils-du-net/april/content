---
site: 20minutes.fr
title: "La lanceuse d'alerte de Facebook lance une ONG pour rendre les réseaux sociaux plus sains"
date: 2022-09-24
href: https://www.20minutes.fr/by-the-web/4002203-20220924-lanceuse-alerte-facebook-lance-ong-rendre-reseaux-sociaux-plus-sains
featured_image: https://img.20mn.fr/R62Rut_-S5SLT0X3rWvE9Ck/1200x768
tags:
- Vie privée
- Internet
series:
- 202238
series_weight: 0
---

>  L'organisation va créer une base de données en open source pour documenter les manquements légaux et éthiques des grandes plateformes et identifier des solutions
