---
site: usine-digitale.fr
title: "Les États-Unis lancent leur projet de RGPD"
author: Raphaële Karayan
date: 2022-08-12
href: https://www.usine-digitale.fr/article/les-etats-unis-lancent-leur-projet-de-rgpd.N2033967
featured_image: https://www.usine-digitale.fr/mediatheque/2/7/9/000994972_homePageUne/federal-trade-commission.jpg
tags:
- Vie privée
- Institutions
series:
- 202232
- 202235
series_weight: 0
---

> La Federal Trade Commission (FTC) a lancé le 11 août une grande consultation, qui marque le point de départ d'un grand chantier réglementaire relatif à la protection des données personnelles.
