---
site: Le Monde Informatique
title: "FauxPilot prépare une alternative à Copilot hébergée localement"
author: Maryse Gros
date: 2022-08-10
href: https://www.lemondeinformatique.fr/actualites/lire-fauxpilot-prepare-une-alternative-a-copilot-hebergee-localement-87646.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000087231.jpg
tags:
- Licenses
- Innovation
series:
- 202232
- 202235
series_weight: 0
---

> Basé sur CodeGen de Salesforce Resarch, le projet d'assistance à l'écriture de code FauxPilot est hébergé localement. Contrairement à un outil comme Copilot, il ne renvoie donc pas de données de télémétrie à son fournisseur, en l'occurrence GitHub/Microsoft. Il s'agit pour l'instant d'un projet de recherche.
