---
site: Le Monde Informatique
title: "Sous pression, GitLab recule sur la suppression des dépôts inactifs"
author: Jacques Cheminat
date: 2022-08-05
href: https://www.lemondeinformatique.fr/actualites/lire-sous-pression-gitlab-recule-sur-la-suppression-des-depots-inactifs-87611.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000087167.png
tags:
- Entreprise
- Internet
series:
- 202231
series_weight: 0
---

> GitLab a fait machine arrière sur sa politique de suppression automatique des projets inactifs pour les utilisateurs d'un compte gratuit. La médiatisation de l'affaire et la colère de la communauté auront eu raison de cette stratégie.
