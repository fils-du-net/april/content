---
site: Le Monde Informatique
title: "L'histoire derrière gLinux, la discrète distribution de Google"
author: Steven J. Vaughan-Nichols
date: 2022-08-01
href: https://www.lemondeinformatique.fr/actualites/lire-l-histoire-derriere-glinux-la-discrete-distribution-de-google-87527.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000087049.jpg
tags:
- Innovation
- Entreprise
series:
- 202231
series_weight: 0
---

> Le système d'exploitation Google le plus connu est Chrome OS. Mais le géant américain utilise en son sein sa propre distribution Linux pour postes de travail : gLinux. Sa principale caractéristique : réduire le labeur et le stress des équipes en charge de son déploiement et de son exploitation.
