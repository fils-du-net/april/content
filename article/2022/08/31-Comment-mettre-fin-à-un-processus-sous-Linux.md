---
site: ZDNet France
title: "Comment mettre fin à un processus sous Linux?"
author: Jack Wallen
date: 2022-08-31
href: https://www.zdnet.fr/pratique/comment-mettre-fin-a-un-processus-sous-linux-39946498.htm
featured_image: https://www.zdnet.com/a/img/2020/06/09/6ac42f24-29a9-439d-83e4-86114f4ab122/istock-1159763172.jpg
tags:
- Sensibilisation
series:
- 202235
---

> Parfois, un programme ou une application peut causer des problèmes sur une machine Linux. Lorsque cela se produit, vous devez savoir comment mettre fin au processus en question.
