---
site: Le nouvel Economiste
title: "La ville de Paris propose une suite de logiciels libres aux collectivités"
date: 2022-08-24
href: https://www.lenouveleconomiste.fr/la-ville-de-paris-propose-une-suite-de-logiciels-libres-aux-collectivites-94066
featured_image: https://www.lenouveleconomiste.fr/wp-content/uploads/2022/08/paris-tennis.jpg
tags:
- Administration
- Internet
series:
- 202234
- 202235
series_weight: 0
---

> Une manière d'inciter à l'adoption de ces outils informatiques faciles d'accès et prêts à l'emploi
