---
site: Numerama
title: "Quels sont les logiciels libres que l'État conseille en 2022?"
description: "287 références!"
author: Julien Lausson
date: 2022-08-05
href: https://www.numerama.com/tech/1068894-quels-sont-les-logiciels-libres-que-letat-conseille-en-2022.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2021/08/manchots-animal.jpg?resize=1024,576
tags:
- Référentiel
- Administration
series:
- 202231
series_weight: 0
---

> La direction interministérielle du numérique et Etalab actualisent la liste des logiciels libres recommandés par État. Il y en a 287.
