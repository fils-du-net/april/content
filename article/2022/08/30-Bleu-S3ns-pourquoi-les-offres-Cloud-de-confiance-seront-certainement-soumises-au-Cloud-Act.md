---
site: La Tribune
title: "Bleu, S3ns: pourquoi les offres Cloud de confiance seront certainement soumises au Cloud Act (€)"
author: Sylvain Rolland
date: 2022-08-30
href: https://www.latribune.fr/technos-medias/internet/bleu-s3ns-pourquoi-les-offres-cloud-de-confiance-seront-certainement-soumises-au-cloud-act-928831.html
featured_image: https://static.latribune.fr/full_width/1354613/cloud-act-h318-p-7-informatique-donnees-data-droits-gafa.jpg
tags:
- Informatique en nuage
series:
- 202235
---

> Une étude commandée à un cabinet d'avocats américain par le ministère de la Justice des Pays-Bas sur le Cloud Act, conclut que les entités européennes peuvent êtres soumises à cette loi extraterritoriale même si leur siège social n'est pas aux Etats-Unis.
