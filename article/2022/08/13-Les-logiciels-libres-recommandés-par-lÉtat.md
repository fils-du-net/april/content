---
site: Génération-NT
title: "Les logiciels libres recommandés par l'État"
author: Jérôme G.
date: 2022-08-13
href: https://www.generation-nt.com/logiciels-libres-recommandes-etat-sill-actualite-2003915.html
featured_image: https://img.generation-nt.com/sill-2022-logiciels-libres-recommandes-etat_0096006401683357.webp
tags:
- Référentiel
- Administration
series:
- 202232
- 202235
series_weight: 0
---

> Il existe un catalogue des logiciels libres recommandés par l'État pour toute l'administration. C'est le SILL qui vient de bénéficier d'une mise à jour.
