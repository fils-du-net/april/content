---
site: Libération
title: "Aaron Swartz, il code de source (€)"
author: Amaelle Guiton
date: 2022-08-17
href: https://www.liberation.fr/portraits/aaron-swartz-il-code-de-source-20220817_3RTZXTNOIBGJ7C7XOPRNZMWFWI/
featured_image: https://cloudfront-eu-central-1.images.arcpublishing.com/liberation/IEQH7M7J5ZC4RBQE67ENWMHVC4.jpg
tags:
- Internet
- Sensibilisation
series:
- 202233
- 202235
series_weight: 0
---

> Si l'enfant prodige d'Internet ne s'était pas pendu en 2013, il soutiendrait les lanceurs d'alerte et tenterait de réguler les Gafa.
