---
site: Les Inrocks
title: "Pourquoi il faut se méfier des applications de suivi menstruel"
author: Milena Ill
date: 2022-08-29
href: https://www.lesinrocks.com/actu/pourquoi-il-faut-se-mefier-des-applications-de-suivi-menstruel-494216-29-08-2022
featured_image: https://www.lesinrocks.com/wp-content/uploads/2022/08/50041737516_fdd5fc8208_c.jpg
tags:
- Vie privée
series:
- 202235
series_weight: 0
---

> Alors que le droit à l'avortement est fortement remis en question aux États-Unis, l'ONG Mozilla a mené l'enquête sur les applications de suivi de règles et leur politique de confidentialité. Et soulève de réels problèmes.
