---
site: Le Monde Informatique
title: "Souveraineté numérique: L'UE se cherche un socle technologique commun"
author: Célia Seramour
date: 2022-02-08
href: https://www.lemondeinformatique.fr/actualites/lire-souverainete-numerique-l-ue-se-cherche-un-socle-technologique-commun-85734.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000083768.jpg
tags:
- Europe
- Vie privée
- Entreprise
- Internet
- Informatique en nuage
series:
- 202206
series_weight: 0
---

> L'Europe aurait-elle enfin trouvé sa voie face aux géants du numérique? Plusieurs initiatives et projets de réglementation se multiplient alors que la question de la protection des données est devenue cruciale. Le gouvernement français a notamment lancé une initiative ce 7 février 2022 pour soutenir ce socle technologique commun.
