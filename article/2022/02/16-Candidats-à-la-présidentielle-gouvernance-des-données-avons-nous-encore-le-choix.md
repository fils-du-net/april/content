---
site: Journal du Net
title: "Candidats à la présidentielle: gouvernance des données, avons-nous encore le choix?"
author: Emmanuel François
date: 2022-02-16
href: https://www.journaldunet.com/ebusiness/internet-mobile/1509217-interpellation-des-equipes-des-candidats-aux-elections-presidentielle-et-legislatives
tags:
- Institutions
- Open data
- Vie privée
series:
- 202207
---

> Cette tribune interpelle les candidats à la présidentielle sur l'enjeu de la gouvernance des données. 26 signataires se sont réunis dans un collectif baptisé ″Citizen-Nation″, convaincus que cette mutation ″civilisationnelle″ ne pourra que résulter d'une mobilisation citoyenne collective et inclusive, notamment de la nouvelle génération, visant à bâtir les fondations de cette nouvelle société pour commencer à construire pour la jeunesse et nos enfants.
