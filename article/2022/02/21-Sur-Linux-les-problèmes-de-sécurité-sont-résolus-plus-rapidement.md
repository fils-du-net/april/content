---
site: ZDNet France
title: "Sur Linux, les problèmes de sécurité sont résolus plus rapidement"
author: Steven Vaughan-Nichols
date: 2022-02-21
href: https://www.zdnet.fr/actualites/sur-linux-les-problemes-de-securite-sont-resolus-plus-rapidement-39937701.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2022/LinuxPhone__w1200.jpg
tags:
- Innovation
series:
- 202208
series_weight: 0
---

> Et c'est l'équipe de Google, Project Zero, qui le dit. Leurs analyses le prouvent: sur Linux, les failles de sécurité sont corrigées plus rapidement que sur Apple, Google et Microsoft.
