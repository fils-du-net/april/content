---
site: la Croix
title: "L'informatique libre, renfort de la démocratie (€)"
author: Stéphane Bataillon
date: 2022-02-02
href: https://www.la-croix.com/France/Linformatique-libre-renfort-democratie-2022-02-02-1201198114
featured_image: https://i.la-croix.com/729x0/smart/2022/02/02/1201198114/_0.jpg
tags:
- Sensibilisation
series:
- 202205
---

> Dans un monde numérisé où les risques de manipulation de l'opinion sont réels, les logiciels libres aident à remettre la technologie au service de l'action citoyenne. Dossier «Défendre la démocratie» : la bataille des fake news. (3/5)
