---
site: ZDNet France
title: "Vidéo: Donald Trump lance son réseau social Truth Social sur l'App Store d'Apple"
date: 2022-02-21
href: https://www.zdnet.fr/actualites/video-donald-trump-lance-son-reseau-social-truth-social-sur-l-app-store-d-apple-39937713.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/Pierre%20temp/Donald%20Trump%20B__w630.jpg
tags:
- Licenses
series:
- 202208
series_weight: 0
---

> Le réseau social de l'ancien président américain Donald Trump a été lancé ce dimanche soir sur l'App Store américain d'Apple après de nombreuses péripéties.
