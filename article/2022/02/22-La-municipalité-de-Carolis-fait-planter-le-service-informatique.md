---
site: l'Arlésienne
title: "La municipalité de Carolis fait planter le service informatique"
author: Eric Besatti
date: 2022-02-22
href: https://larlesienne.info/2022/02/22/la-municipalite-de-carolis-fait-planter-le-service-informatique
featured_image: https://larlesienne.info/wp-content/uploads/2022/02/de-carolis-fait-planter-le-service-informatique-arles-e1645536873121-1280x640.jpg
tags:
- Administration
series:
- 202208
---

> Le trésor du service informatique va être liquidé. Des logiciels Microsoft payants vont remplacer la culture maison tournée vers le libre. Celle-là même qui a fait d’Arles une ville d’excellence et d’innovation en la matière avec pas moins de 30 logiciels programmés par les agents dont certains utilisés par d’autres collectivités. Une nouvelle facture lourde à porter pour satisfaire le goût de la nouvelle administration, quand parallèlement, le service est au pain sec, placé en sous-effectif, incapable de répondre à toutes ses missions.
