---
site: ZDNet France
title: "Non, Linus Torvalds n'est pas Satoshi Nakamoto"
author: Steven Vaughan-Nichols
date: 2022-02-01
href: https://www.zdnet.fr/actualites/non-linus-torvalds-n-est-pas-satoshi-nakamoto-39936667.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2017/12/Bitcoin-steam-140__w1200.jpg
tags:
- Innovation
series:
- 202205
series_weight: 0
---

> Linus Torvalds a fait plusieurs choses étonnantes. Il est le créateur du système d'exploitation Linux et de Git, le système de contrôle de version le plus populaire. Mais il n'est pas l'inventeur du bitcoin.
