---
site: Acteurs publics
title: "La France fer de lance d'une stratégie Open Source européenne"
author: Thomas Belarbi
date: 2022-02-15
href: https://www.acteurspublics.fr/articles/la-france-fer-de-lance-dune-strategie-open-source-europeenne
featured_image: https://www.acteurspublics.fr/upload/media/default/0001/29/21aa8ebb46f76ecc47e14c86b96dd00e97d5e946.jpeg
tags:
- Europe
- Administration
series:
- 202207
series_weight: 0
---

> À l’occasion de la 8e conférence EU Open Source Policy Summit, début février, la France a défendu sa vision d’une collaboration renforcée entre les États membres sur l’adoption de l’Open Source. Thomas Belarbi, responsable stratégie et développement Secteur Public chez Red Hat France, souligne les potentialités de l’Open source pour les organisations publiques.
