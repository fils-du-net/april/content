---
site: Numerama
title: "Quelles sont les propositions de Jean-Luc Mélenchon sur le numérique?"
description: Cybersécurité, data centers, logiciels libres...
author: Aurore Gayte
date: 2022-02-17
href: https://www.numerama.com/politique/855013-quelles-sont-les-propositions-de-jean-luc-melenchon-sur-le-numerique.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2021/12/melenchon.jpg?resize=1024,576
tags:
- Institutions
series:
- 202207
---

> Le candidat de la France Insoumise, qui avait déjà fait des sujets du numérique et de la tech l’un de ses points forts lors de la campagne de 2017, a de nouveau beaucoup de propositions, qui touchent à de nombreux sujets.
