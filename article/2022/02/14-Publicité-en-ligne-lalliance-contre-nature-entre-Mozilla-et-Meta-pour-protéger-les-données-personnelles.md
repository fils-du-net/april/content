---
site: 01net.
title: "Publicité en ligne: l'alliance contre-nature entre Mozilla et Meta... pour protéger les données personnelles"
date: 2022-02-14
href: https://www.01net.com/actualites/publicite-en-ligne-l-alliance-contre-nature-entre-mozilla-et-meta-pour-proteger-les-donnees-personnelles-2054651.html
featured_image: https://img.bfmtv.com/c/0/708/cd5/6b7b9374f54933140fcf33802a081.jpg
tags:
- Vie privée
- Internet
series:
- 202207
series_weight: 0
---

> Les deux acteurs, qui n'ont pourtant jamais eu vraiment d'atomes crochus, travaillent ensemble pour créer une technologie de performance publicitaire qui protège l'identité des internautes.
