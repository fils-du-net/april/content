---
site: Silicon
title: "Logiciels libres: 5 outils made in France en observation au SILL"
author: Clément Bohic
date: 2022-02-09
href: https://www.silicon.fr/5-logiciels-libres-made-in-france-observation-sill-431775.html
featured_image: https://www.silicon.fr/wp-content/uploads/2022/02/logiciels-libres-observation-SILL.jpg
tags:
- Référentiel
series:
- 202206
series_weight: 0
---

> Coup d'œil sur cinq outils origine France actuellement en phase d'observation au SILL (Socle interministériel de logiciels libres).
