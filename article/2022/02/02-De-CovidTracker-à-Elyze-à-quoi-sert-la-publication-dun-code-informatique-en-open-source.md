---
site: 20minutes.fr
title: "De CovidTracker à Elyze, à quoi sert la publication d'un code informatique en «open source»?"
author: Laure Gamaury
date: 2022-02-02
href: https://www.20minutes.fr/high-tech/3225563-20220202-covidtracker-elyze-quoi-sert-publication-code-informatique-open-source
featured_image: https://img.20mn.fr/aRm5xINWQJCHl5ZhOqtlbQ/310x190_des-lignes-de-code-informatique-photo-d-illustration.jpg
tags:
- Sensibilisation
series:
- 202205
series_weight: 0
---

> Les récentes polémiques autour de l'application Elyze mettent en lumière la communauté des développeurs et le monde de l'«open source»
