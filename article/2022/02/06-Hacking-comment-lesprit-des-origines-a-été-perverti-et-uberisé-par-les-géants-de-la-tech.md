---
site: 01net.
title: "Hacking: comment l'esprit des origines a été perverti et uberisé par les géants de la tech"
author: Gilbert Kallenborn
date: 2022-02-06
href: https://www.01net.com/actualites/hacking-comment-l-esprit-des-origines-a-ete-perverti-et-uberise-par-les-geants-de-la-tech-2054127.html
featured_image: https://img.bfmtv.com/c/630/420/64c/e52ef0e0998d96d5a31bc8b737039.jpg
tags:
- Économie
series:
- 202205
series_weight: 0
---

> Un rapport de recherche revient aux origines de la recherche de failles de sécurité. On constate que l'esprit libertaire d'antan a été remplacé par une «uberisation» du hacking. Un autre monde est-il possible?
