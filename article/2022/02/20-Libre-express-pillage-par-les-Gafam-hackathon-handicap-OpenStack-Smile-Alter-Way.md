---
site: ZDNet France
title: "Libre express: pillage par les Gafam, hackathon handicap, OpenStack, Smile-Alter Way"
author: Thierry Noisette
date: 2022-02-20
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-express-pillage-par-les-gafam-hackathon-handicap-openstack-smile-alter-way-39937697.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/05/open-source-KeepCalmAndUse.jpg
tags:
- Sensibilisation
series:
- 202207
series_weight: 0
---

> Pillage du Libre par les Gafam. Cagnotte pour un hackathon 'défis du handicap'. Un Français à la tête de la fondation OpenInfra. Equipe de direction en majorité féminine à Alter Way.
