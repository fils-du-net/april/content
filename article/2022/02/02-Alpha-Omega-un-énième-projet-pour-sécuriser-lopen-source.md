---
site: Le Monde Informatique
title: "Alpha-Omega, un énième projet pour sécuriser l'open source"
author: Cynthia Brumfield
date: 2022-02-02
href: https://www.lemondeinformatique.fr/actualites/lire-alpha-omega-un-enieme-projet-pour-securiser-l-open-source-85670.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000083675.jpg
tags:
- Économie
- Entreprise
series:
- 202205
series_weight: 0
---

> Améliorer la sécurité de 10 000 programmes open source, tel est l'objectif du Projet Alpha-Omega initié par l'OpenSSF et la Fondation Linux, soutenu par Microsoft et Google. Un projet qui ressemble à la Core Initiative Infrastructure créée lors de l'affaire Heartbleed.
