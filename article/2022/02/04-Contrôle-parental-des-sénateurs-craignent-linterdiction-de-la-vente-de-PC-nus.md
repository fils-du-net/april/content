---
site: Next INpact
title: "Contrôle parental: des sénateurs craignent l'interdiction de la vente de PC nus (€)"
description: "Pour un naturisme informatique"
author: Marc Rees
date: 2022-02-04
href: https://www.nextinpact.com/article/49719/controle-parental-senateurs-craignent-linterdiction-vente-pc-nus
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/9901.jpg
tags:
- Vente liée
- Institutions
series:
- 202205
---

> La future loi veut imposer aux fabricants l’installation d’un logiciel de contrôle parental. Elle sera examinée au Sénat le 9 février. À l’approche de l’échéance, des sénateurs veulent colmater plusieurs risques dont l'interdiction de la vente de PC sans OS et l'exploitation des données des parents.
