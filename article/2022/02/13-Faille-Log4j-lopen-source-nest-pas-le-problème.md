---
site: Le Monde Informatique
title: "Faille Log4j: l'open source n'est pas le problème"
author: Michael Cooney
date: 2022-02-13
href: https://www.lemondeinformatique.fr/actualites/lire-faille-log4j-l-open-source-n-est-pas-le-probleme-85791.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000083852.jpg
tags:
- Sensibilisation
- Institutions
series:
- 202206
series_weight: 0
---

> Auditionnés par une commission du Sénat américain, les dirigeants de Cisco, Palo Alto et Apache sont revenus sur les réponses apportées par l'industrie pour remédier à la vulnérabilité Log4j et sur les potentiels problèmes à venir. D'une même voix, ils ont refusé de jeter l'opprobre sur l'open source.
