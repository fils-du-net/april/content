---
site: ZDNet France
title: "Google et Microsoft financent la sécurité de logiciels open source"
author: Thierry Noisette
date: 2022-02-13
href: https://www.zdnet.fr/blogs/l-esprit-libre/google-et-microsoft-financent-la-securite-de-logiciels-open-source-39937315.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/12/cyber-security-3400657_960_720_Pixabay.jpg
tags:
- Économie
series:
- 202206
series_weight: 0
---

> La fondation pour la sécurité de l'open source (Open Source Security Foundation, ou OpenSSF), lance une vaste campagne pour améliorer la sécurité, dont des tests automatisés pour 10.000 projets open source très employés.
