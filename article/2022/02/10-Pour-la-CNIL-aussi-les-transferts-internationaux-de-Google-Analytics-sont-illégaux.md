---
site: Next INpact
title: "Pour la CNIL aussi, les transferts internationaux de Google Analytics sont illégaux (€)"
description: "Audience, ô désespoir"
author: Marc Rees
date: 2022-02-10
href: https://www.nextinpact.com/article/49790/pour-cnil-aussi-transferts-internationaux-google-analytics-sont-illegaux
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/23.jpg
tags:
- Vie privée
- Internet
series:
- 202206
series_weight: 0
---

> Tremblement de terre dans l'univers de la mesure d'audience. Pour la CNIL, les transferts vers les États-Unis des données collectées par Google Analytics sont illégaux. L’autorité, qui suit ses homologues autrichiens et néerlandais, met en demeure un site. Il dispose d'un mois pour se conformer au RGPD et au besoin d’abandonner la solution Google.
