---
site: Silicon
title: "Office 365: l'Éducation nationale a-t-elle vraiment crevé l'abcès?"
author: Clément Bohic
date: 2022-11-17
href: https://www.silicon.fr/office-365-eudcation-nationale-vraiment-dit-stop-452688.html
featured_image: https://www.silicon.fr/wp-content/uploads/2021/09/Office-365-DINUM-684x513.jpeg
tags:
- Éducation
series:
- 202246
---

> Le ministère de l'Éducation nationale explique avoir appelé, il y a un an, les rectorats à «arrêter tout déploiement ou extension» d'Office 365. Pourquoi cette décision fait-elle surface maintenant?
