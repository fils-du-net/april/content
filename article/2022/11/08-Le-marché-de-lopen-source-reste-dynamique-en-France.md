---
site: Le Monde Informatique
title: "Le marché de l'open source reste dynamique en France"
author: Jacques Cheminat
date: 2022-11-08
href: https://www.lemondeinformatique.fr/actualites/lire-le-marche-de-l-open-source-reste-dynamique-en-france-88542.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000088738.jpg
tags:
- Économie
series:
- 202245
---

> Selon une étude, le marché de l'open source en France continue sa croissance. Il représentait un revenu de 5,9 Md€ en 2022 et près de 60 000 emplois directs. Et les perspectives sur les cinq prochaines années sont au beau fixe.
