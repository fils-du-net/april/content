---
site: "L'Est Républicain"
title: "Réseaux sociaux. Qu'est-ce que Mastodon, l'alternative à Twitter qui séduit de plus en plus?"
author: Juliette Mitoyen
date: 2022-11-07
href: https://www.estrepublicain.fr/science-et-technologie/2022/11/07/qu-est-ce-que-mastodon-l-alternative-a-twitter-qui-seduit-de-plus-en-plus
featured_image: https://cdn-s-www.estrepublicain.fr/images/783E14DF-1EE0-4290-89F6-0FA0CA704597/NW_detail/title-1667835446.jpg
tags:
- Internet
series:
- 202245
---

> Après le rachat de Twitter par Elon Musk, des centaines de milliers d'internautes se sont rabattus sur Mastodon, un réseau social se décrivant comme «pas à vendre», libre et en open source. On vous dit tout sur cette plateforme qui monte, qui monte.
