---
site: Siècle Digital
title: "Un rapport allemand estime que Microsoft 365 sur le cloud n'est pas conforme au RGPD"
author: Benjamin Terrasson
date: 2022-11-30
href: https://siecledigital.fr/2022/11/30/un-rapport-allemand-estime-que-microsoft-365-sur-le-cloud-nest-pas-conforme-au-rgpd
featured_image: https://siecledigital.fr/wp-content/uploads/2022/11/Microsoft-940x550.jpeg
tags:
- Éducation
- International
series:
- 202248
series_weight: 0
---

> L'autorité allemande de protection des données a dévoilé un rapport pointant un des insuffisances dans le respect du RGPD par Microsoft.
