---
site: l'Humanité.fr
title: "Délaisser Twitter pour aller sur Mastodon? Pourquoi pas!"
author: Nicolas Lambert
date: 2022-11-15
href: https://www.humanite.fr/en-debat/regard-de-cartographe/delaisser-twitter-pour-aller-sur-mastodon-pourquoi-pas-771000
featured_image: https://www.humanite.fr/sites/default/files/styles/1200x675_full/public/images/mastodon.jpg?itok=WVi01evJ
tags:
- Internet
series:
- 202246
---

> Depuis le rachat de Twitter par Elon Musk, de nombreux utilisateurs sont tentés par le refuge offert par Mastodon. Et ils ont bien raison, explique Nicolas Lambert, ingénieur de recherche au CNRS en sciences de l’information géographique. Et si c'était l'heure de la grande migration? Voici les avantages par rapport à Twitter de ce réseau social de micro blogging créé en octobre 2016.
