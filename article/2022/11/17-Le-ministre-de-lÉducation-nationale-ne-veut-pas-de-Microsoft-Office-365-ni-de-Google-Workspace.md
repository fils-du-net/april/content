---
site: Siècle Digital
title: "Le ministre de l'Éducation nationale ne veut pas de Microsoft Office 365 ni de Google Workspace"
author: Valentin Cimino
date: 2022-11-17
href: https://siecledigital.fr/2022/11/17/le-ministre-de-leducation-nationale-ne-veut-pas-de-microsoft-office-365-ni-de-google-workspace/
featured_image: https://siecledigital.fr/wp-content/uploads/2022/11/portaitpapndiaye-gouv-940x550.jpeg
tags:
- Éducation
series:
- 202246
---

> Le ministère de l'Éducation nationale confirme ne pas vouloir des offres de Microsoft Office 365 et de Google Workspace dans les écoles.
