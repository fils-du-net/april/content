---
site: Next INpact
title: "CERN: «nous devons réfléchir au véritable sens du mot \"libre\" et à ses limites»"
date: 2022-11-28
href: https://www.nextinpact.com/lebrief/70482/cern-nous-devons-reflechir-au-veritable-sens-mot-libre-et-a-ses-limites
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/wide-linked-media/8827.jpg
tags:
- Logicies privateurs
series:
- 202248
series_weight: 0
---

> Dans un article intitulé «Sécurité informatique: des logiciels pas si gratuits», l’Organisation européenne pour la recherche nucléaire rappelle que, dans un environnement de recherche ouvert, «l'utilisation d'outils et de logiciels commerciaux libres et open source (freemium and open source software – FOSS) n'a rien d'inhabituel».
