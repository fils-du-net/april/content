---
site: ZDNet France
title: "Libre et open source express: l'Education nationale sur Mastodon, Smile et l'Europe, Red Hat, Nonfiction"
author: Thierry Noisette
date: 2022-11-01
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-l-education-nationale-sur-mastodon-smile-et-l-europe-red-hat-nonfiction-39949148.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/05/open-source-KeepCalmAndUse.jpg
tags:
- Internet
series:
- 202244
---

> En bref. Une instance Mastodon pour les enseignants, Smile en «mariage d'amour» avec le Libre, la stratégie de Red Hat, Nonfiction et le pari des logiciels libres.
