---
site: RTBF
title: "Les réseaux sociaux éthiques grâce aux logiciels libres: l'exemple de Mastodon.be"
date: 2022-11-29
href: https://www.rtbf.be/article/les-reseaux-sociaux-ethiques-grace-aux-logiciels-libres-lexemple-de-mastodon-11113169
featured_image: https://ds.static.rtbf.be/article/image/770x433/e/5/2/f3d15cdb037d589902db84981e2fa7d5-1669715619.jpg
tags:
- Internet
- Sensibilisation
series:
- 202248
series_weight: 0
---

> Twitter ne vous satisfait plus depuis son rachat par Elon Musk? Vous estimez que certains réseaux sociaux manquent de valeurs humaines ? Nicolas Pettiaux enseignant et président de Educode asbl et Olivier Meunier, développeur et formateur art numérique nous parlent des logiciels libres. Découvrez le logiciel Mastodon, un réseau social éthique.
