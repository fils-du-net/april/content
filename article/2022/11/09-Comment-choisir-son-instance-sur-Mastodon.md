---
site: Numerama
title: "Comment choisir son instance sur Mastodon?"
description: "Pour envoyer des «pouet»"
author: Nelly Lesage
date: 2022-11-09
href: https://www.numerama.com/tech/1175038-comment-choisir-son-instance-sur-mastodon.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2022/11/mastodon-01-solo-1024x576.jpg?webp=1&key=a5bd7af9
tags:
- Internet
series:
- 202245
---

> S'inscrire sur Mastodon implique de le faire sur l'une des nombreuses instances de la plateforme. Qu'est-ce qu'une instance? Et comment en choisir une?
