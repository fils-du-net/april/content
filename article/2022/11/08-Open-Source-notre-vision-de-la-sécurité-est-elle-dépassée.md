---
site: Silicon
title: "Open Source: notre vision de la sécurité est-elle dépassée?"
author: Güray Turan
date: 2022-11-08
href: https://www.silicon.fr/avis-expert/open-source-notre-vision-de-la-securite-est-elle-depassee
featured_image: https://www.silicon.fr/wp-content/uploads/2017/06/open-source-obscur.jpg
tags:
- Sensibilisation
series:
- 202245
series_weight: 0
---

> Certains logiciels Open Source (OSS) se sont retrouvés sous le feu des projecteurs à cause de problématiques de sécurité. Alors que certains ne l'observent qu'à travers la lentille étroite de la sécurité, les opportunités sont nombreuses, et il serait dommage de les manquer.
