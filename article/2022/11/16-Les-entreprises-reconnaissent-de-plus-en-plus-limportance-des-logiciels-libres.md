---
site: IT SOCIAL
title: "Les entreprises reconnaissent de plus en plus l'importance des logiciels libres"
author: Philippe Richard
date: 2022-11-16
href: https://itsocial.fr/enjeux-it/enjeux-production/developpements/les-entreprises-reconnaissent-de-plus-en-plus-limportance-des-logiciels-libres
featured_image: https://itsocial.fr/wp-content/uploads/2022/11/logiciel-libre.jpg
tags:
- Entreprise
series:
- 202246
series_weight: 0
---

> Aujourd’hui, les logiciels libres sont à la base de très nombreux programmes: 97 % des applications utilisent du code libre et 90 % des entreprises l’appliquent ou l’utilisent d’une manière ou d’une autre.
