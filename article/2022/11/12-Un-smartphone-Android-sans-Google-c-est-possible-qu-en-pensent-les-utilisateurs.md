---
site: Le Monde.fr
title: "Un smartphone Android sans Google, c'est possible: qu'en pensent les utilisateurs?"
author: Nicolas Six
date: 2022-11-12
href: https://www.lemonde.fr/pixels/article/2022/11/12/un-smartphone-android-sans-google-c-est-possible-qu-en-pensent-les-utilisateurs_6149559_4408996.html
featured_image: https://img.lemde.fr/2020/10/15/0/0/3433/2669/1328/0/45/0/b509760_851799132-zoome.jpg
tags:
- Innovation
series:
- 202245
series_weight: 0
---

> Graphene, Lineage, Calyx, /e/ ou Iodé: les systèmes d’exploitation fondés sur Android, mais sans Google, séduisent les personnes avides de se défaire de l’emprise du géant américain. Ces smartphones fonctionnent-ils vraiment bien au quotidien?
