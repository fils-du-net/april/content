---
site: ZDNet France
title: "Concours des Acteurs du Libre: six lauréats distingués par le CNLL"
author: Thierry Noisette
date: 2022-11-20
href: https://www.zdnet.fr/blogs/l-esprit-libre/concours-des-acteurs-du-libre-six-laureats-distingues-par-le-cnll-39949996.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/ActeursduLibre_bandeau.jpg
tags:
- Promotion
series:
- 202247
series_weight: 0
---

> Six prix ont été décernés à des projets ou entreprises «contribuant activement aux avancées du logiciel libre».
