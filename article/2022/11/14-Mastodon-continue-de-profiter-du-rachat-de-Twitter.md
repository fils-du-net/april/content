---
site: 01net.
title: "Mastodon continue de profiter du rachat de Twitter"
author: Florian Bayard
date: 2022-11-14
href: https://www.01net.com/actualites/mastodon-continue-de-profiter-du-rachat-de-twitter.html
featured_image: https://www.01net.com/app/uploads/2022/11/mastodon-copie.jpg
tags:
- Internet
series:
- 202246
series_weight: 0
---

> Mastodon continue de rencontrer un succès colossal depuis le rachat de Twitter. Alors que sa plate-forme accumule de plus en plus d'utilisateurs, le créateur de Mastodon a évoqué la modération des contenus en ligne et l'idéologie d'Elon Musk dans une interview.
