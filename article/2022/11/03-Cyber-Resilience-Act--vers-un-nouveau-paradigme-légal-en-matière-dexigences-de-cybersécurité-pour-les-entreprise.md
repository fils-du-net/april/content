---
site: Journal du Net
title: "Cyber Resilience Act: vers un nouveau paradigme légal en matière d'exigences de cybersécurité pour les entreprise"
author: Valérie Chavanne
date: 2022-11-03
href: https://www.journaldunet.com/solutions/dsi/1516087-cyber-resilience-act-vers-un-nouveau-paradigme-legal-en-matiere-d-exigences-de-cybersecurite-pour-les-entreprises
tags:
- Europe
- Innovation
series:
- 202244
series_weight: 0
---

> Le Cyber Resilience Act vise à établir un cadre juridique cohérent et uniforme en matière d'exigences de cybersécurité. Un texte qui pourrait rebattre les cartes pour l'ensemble des organisations EU.
