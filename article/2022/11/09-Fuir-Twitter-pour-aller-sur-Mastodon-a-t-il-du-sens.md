---
site: Télérama.fr
title: "Fuir Twitter pour aller sur Mastodon a-t-il du sens?"
author: Olivier Tesquet
date: 2022-11-09
href: https://www.telerama.fr/ecrans/fuir-twitter-pour-aller-sur-mastodon-a-t-il-du-sens-7012881.php
featured_image: https://focus.telerama.fr/2022/11/08/0/0/5500/3547/1200/0/60/0/1e1a62d_1667921451143-043-457051906.jpg
tags:
- Internet
series:
- 202245
series_weight: 0
---

> Depuis qu’Elon Musk a racheté Twitter, Mastodon est présenté comme une alternative crédible. C’est oublier la facilité d’usage à laquelle les internautes sont habitués.
