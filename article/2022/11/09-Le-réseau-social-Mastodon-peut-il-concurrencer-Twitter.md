---
site: Le Telegramme
title: "Le réseau social Mastodon peut-il concurrencer Twitter?"
date: 2022-11-09
href: https://www.letelegramme.fr/soir/le-reseau-social-mastodon-peut-il-concurrencer-twitter-09-11-2022-13217083.php
featured_image: https://www.letelegramme.fr/images/2022/11/09/le-reseau-social-mastodon-est-une-alternative-a-twitter_7023837_676x443p.jpg?v=1
tags:
- Internet
series:
- 202245
---

> La prise de pouvoir d’Elon Musk sur Twitter va-t-elle faire déserter les utilisateurs? Et les alternatives au réseau social à l’oiseau bleu, comme Mastodon, sont-elles crédibles?
