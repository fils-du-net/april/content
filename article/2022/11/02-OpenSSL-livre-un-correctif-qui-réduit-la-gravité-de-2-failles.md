---
site: Le Monde Informatique
title: "OpenSSL livre un correctif qui réduit la gravité de 2 failles"
author: Lucian Constantin
date: 2022-11-02
href: https://www.lemondeinformatique.fr/actualites/lire-openssl-livre-un-correctif-qui-reduit-la-gravite-de-2-failles-88483.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000088636.png
tags:
- Innovation
series:
- 202244
series_weight: 0
---

> Après des tests supplémentaires, les deux vulnérabilités d'OpenSSL 3.0 corrigées par le projet éponyme sont désormais classées «gravité élevée» et non plus «gravité critique».
