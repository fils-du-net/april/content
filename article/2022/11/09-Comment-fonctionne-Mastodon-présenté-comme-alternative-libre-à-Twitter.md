---
site: Le Monde.fr
title: "Comment fonctionne Mastodon, présenté comme alternative «libre» à Twitter?"
author: Louis Adam
date: 2022-11-09
href: https://www.lemonde.fr/pixels/article/2022/11/09/comment-fonctionne-mastodon-presente-comme-alternative-libre-a-twitter_6149183_4408996.html
featured_image: https://img.lemde.fr/2022/11/03/0/0/3500/2333/180/0/95/0/c37136b_par01-engie-hydrogen-1103-11.jpg
tags:
- Internet
series:
- 202245
---

> Le rachat de Twitter par Elon Musk a généré un nouvel afflux de «mastonautes», mais mieux vaut ne pas se laisser berner par les apparentes similitudes entre les deux réseaux.
