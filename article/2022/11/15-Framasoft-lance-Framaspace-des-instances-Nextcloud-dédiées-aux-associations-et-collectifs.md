---
site: Next INpact
title: "Framasoft lance Frama.space, des instances Nextcloud dédiées aux associations et collectifs (€)"
description: "Vers le fini et pas au-delà"
author: Vincent Hermann
date: 2022-11-15
href: https://www.nextinpact.com/article/70361/framasoft-lance-frama-space-instances-nextcloud-dediees-aux-associations-et-collectifs
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/12420.jpg
tags:
- Associations
series:
- 202246
series_weight: 0
---

> Framasoft vient de lancer Frama.space, à destination des petites associations et des collectifs. Objectif, fournir un espace Nextcloud adapté à leurs besoins. L’association française souhaite, par cette initiative, redonner du pouvoir aux collectifs, malmenés selon elle depuis plusieurs années par les pouvoirs publics. Nous nous sommes entretenus avec Pierre-Yves Gosset, son délégué général.
