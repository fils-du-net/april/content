---
site: Slate.fr
title: "Le Brésil va remporter la Coupe du monde de foot 2022 (selon la science)"
author: Thomas Messias
date: 2022-11-21
href: https://www.slate.fr/story/236543/coupe-monde-qatar-predictions-vainqueur-bresil-pronostics-alan-turing-institute-football
featured_image: https://www.slate.fr/sites/default/files/styles/1060x523/public/bresil-champion-monde-2022.png
tags:
- Innovation
series:
- 202247
series_weight: 0
---

> La simulation de l'Alan Turing Institute donne à la Belgique 19% de chances de victoire, 13% à l'Argentine et 11% à la France.
