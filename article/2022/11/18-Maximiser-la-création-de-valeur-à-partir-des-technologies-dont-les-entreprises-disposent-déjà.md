---
site: Journal du Net
title: "Maximiser la création de valeur à partir des technologies dont les entreprises disposent déjà"
author: Hans Roth
date: 2022-11-18
href: https://www.journaldunet.com/solutions/dsi/1517011-maximiser-la-creation-de-valeur-a-partir-des-technologies-dont-les-entreprises-disposent-deja
tags:
- Entreprise
series:
- 202246
---

> Si enclencher le mode "survie" représente une solution de facilité dans le contexte actuel, les entreprises ont-elles pensé à tirer parti des technologies qu'elles possèdent?
