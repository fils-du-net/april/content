---
site: Reporterre, le média de l'écologie
title: "Fairphone: que vaut le téléphone durable et équitable?"
author: Fabienne Loiseau
date: 2022-11-24
href: https://reporterre.net/Fairphone-que-vaut-le-telephone-durable-et-equitable
featured_image: https://reporterre.net/local/cache-vignettes/L720xH480/arton26969-c2141.jpg?1669202097
tags:
- Innovation
series:
- 202247
series_weight: 0
---

> Réparable et plus écologique, le Fairphone garde encore l’image d’un téléphone peu performant et sujet aux pannes. Pourtant, il s’est amélioré. Des utilisateurs témoignent.
