---
site: ZDNet France
title: "Microsoft doit quitter les écoles, confirme le ministère de l'Education nationale"
date: 2022-11-18
href: https://www.zdnet.fr/actualites/microsoft-doit-quitter-les-ecoles-confirme-le-ministere-de-l-education-nationale-39949956.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2022/TechEducation__w1200.jpg
tags:
- Éducation
series:
- 202246
---

> La solution Microsotf Office 365 propose une suite bureautique entièrement hébergée sur le cloud de Microsoft, Azure, et donc potentiellement soumise au Cloud Act américain.
