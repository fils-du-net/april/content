---
site: Usbek & Rica
title: "Pour une accélération du numérique d'intérêt général en Europe"
date: 2022-11-03
href: https://usbeketrica.com/fr/article/pour-une-acceleration-du-numerique-d-interet-general-en-europe
featured_image: https://usbeketrica.com/media/99633/download/shutterstock_1667139886-2.jpg?v=1&inline=1
tags:
- Economie
- Sensibilisation
series:
- 202244
series_weight: 0
---

> Les Social Tech, ce sont ces entreprises du numérique à but non lucratif ou à lucrativité limitée qui tentent de régénérer l'esprit du web des origines. Comment y parviennent-elles? Plongez lors de la Social Good Week dans le numérique d'intérêt général, qui tente d'offrir une alternative juste, sobre et équitable aux mastodontes de la tech!
