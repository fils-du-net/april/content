---
site: Le Monde.fr
title: "A l’école, l’éducation nationale tente de limiter l’emprise des Gafam (€)"
author: Aurélien Defer
date: 2022-11-16
href: https://www.lemonde.fr/pixels/article/2022/11/16/a-l-ecole-l-education-nationale-tente-de-limiter-l-emprise-des-gafam_6150169_4408996.html
featured_image: https://img.lemde.fr/2021/11/29/0/0/1800/1198/1328/0/45/0/0fac4b8_833566223-e100769-920562563-000-1py42m.jpg
tags:
- Éducation
series:
- 202246
---

> Le gouvernement a affirmé mardi avoir «demandé [qu’on arrête] tout déploiement» des outils de Microsoft et Google, omniprésents dans les classes françaises. La préoccupation grandit en matière de souveraineté numérique et de protection des données personnelles.
