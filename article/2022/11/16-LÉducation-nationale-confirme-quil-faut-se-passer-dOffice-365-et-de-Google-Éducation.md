---
site: Numerama
title: "L'Éducation nationale confirme qu'il faut se passer d'Office 365 et de Google Éducation"
description: "Pas assez de garanties"
author: Julien Lausson
date: 2022-11-16
href: https://www.numerama.com/politique/1180864-leducation-nationale-confirme-quil-faut-se-passer-doffice-365-et-de-google-education.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2022/11/etudiants-polytechnique-1024x576.jpg?webp=1&key=127291a4
tags:
- Éducation
series:
- 202246
series_weight: 0
---

> Le ministère de l’Éducation nationale rappelle qu’en matière de suites collaboratives, les offres d’Office 365 et de Google Éducation ne sont pas adéquates. Leur mise en œuvre n’offre pas toutes les garanties au regard de la législation, de la doctrine de l’État dans le cloud et de la jurisprudence.
