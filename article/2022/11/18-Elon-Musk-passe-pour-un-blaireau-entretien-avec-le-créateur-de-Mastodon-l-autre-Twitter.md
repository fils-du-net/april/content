---
site: Vanity Fair
title: "«Elon Musk passe pour un blaireau»: entretien avec le créateur de Mastodon, l'autre Twitter"
author: Will Knight
date: 2022-11-18
href: https://www.vanityfair.fr/actualites/article/elon-musk-twitter-mastodon-blaireau
featured_image: https://media.vanityfair.fr/photos/63766c515042d62f46cf7857/16:9/w_2560%2Cc_limit/GettyImages-1244717296.jpg
tags:
- Internet
series:
- 202246
---

> Alors que se poursuit l'exode des utilisateurs de Twitter vers Mastodon, la plateforme alternative conçue par Eugen Rochko, ce dernier persiste et signe: les réseaux sociaux – même si personne ne les contrôle – peuvent générer un débat sain et apaisé.
