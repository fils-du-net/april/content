---
site: Le Monde.fr
title: "Qui est Eugen Rochko, le jeune créateur du réseau social Mastodon?"
author: Marine Bourrier
date: 2022-11-21
href: https://www.lemonde.fr/m-le-mag/article/2022/11/21/qui-est-vraiment-eugen-rochko-le-jeune-createur-de-mastodon_6150831_4500055.html
featured_image: https://img.lemde.fr/2022/11/15/0/0/1125/1500/1328/0/45/0/d476e62_241315-3297897.jpg
tags:
- Internet
series:
- 202247
series_weight: 0
---

> L'informaticien allemand de 29 ans a créé, en 2016, le réseau social alternatif et non lucratif Mastodon, dont le nombre d'utilisateurs connaît une folle ascension à la suite du rachat de Twitter par Elon Musk.
