---
site: ZDNet France
title: "Libre et open source express: Frama.space, Mastodon x 2, le Libre dans La Croix"
author: Thierry Noisette
date: 2022-11-29
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-framaspace-mastodon-x-2-le-libre-dans-la-croix-39950502.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/05/open-source-KeepCalmAndUse.jpg
tags:
- Internet
series:
- 202248
---

> Revue de web en bref. Framasoft offre du cloud aux associations. Interview du créateur de Mastodon. Le logiciel libre présenté dans le quotidien catholique.
