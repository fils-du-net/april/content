---
site: ZDNet France
title: "Adullact: Abbeville, Échirolles et les communes du Sitiv, prix d'excellence du logiciel libre"
author: Thierry Noisette
date: 2022-11-30
href: https://www.zdnet.fr/blogs/l-esprit-libre/adullact-abbeville-chirolles-et-les-communes-du-sitiv-prix-d-excellence-du-logiciel-libre-39950574.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/Territoire-Numerique-Libre_logo.jpg
tags:
- Promotion
- april
- Associations
series:
- 202248
series_weight: 0
---

> Trois collectivités ont obtenu le niveau 5 du label Territoire Numérique Libre: Abbeville, Échirolles et le Sitiv (Syndicat Intercommunal des Technologies de l'Information pour les Villes), qui associe huit villes des départements du Rhône et de la Loire.
