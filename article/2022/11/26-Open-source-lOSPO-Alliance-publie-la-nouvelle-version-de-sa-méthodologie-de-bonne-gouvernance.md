---
site: ZDNet France
title: "Open source: l'OSPO Alliance publie la nouvelle version de sa méthodologie de bonne gouvernance"
author: Thierry Noisette
date: 2022-11-26
href: https://www.zdnet.fr/blogs/l-esprit-libre/open-source-l-ospo-alliance-publie-la-nouvelle-version-de-sa-methodologie-de-bonne-gouvernance-39950360.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/pyramide%20bonne%20gouvernance%20open%20source.jpg
tags:
- Sensibilisation
series:
- 202247
series_weight: 0
---

> Le groupement né en 2021 sort la version 1.1 de son cadre méthodologique GGI (Good Governance Initiative), dont une traduction française.
