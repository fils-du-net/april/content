---
site: Clubic.com
title: "GitHub Copilot: Microsoft poursuivie pour vol de code open-source"
author: Robin Lamorlette
date: 2022-11-08
href: https://www.clubic.com/pro/entreprises/microsoft/actualite-445128-github-copilot-microsoft-poursuivie-pour-vol-de-code-open-source.html
featured_image: https://pic.clubic.com/v1/images/1927773/raw.webp?fit=max&width=1200&hash=0697595915659e6faf88b385c74c55151da194dd
tags:
- Droit d'auteur
- Licenses
series:
- 202245
series_weight: 0
---

> Microsoft, GitHub et OpenAI se retrouvent dans le viseur de la justice vis-à-vis de Copilot, un outil de programmation basé sur l'IA.
