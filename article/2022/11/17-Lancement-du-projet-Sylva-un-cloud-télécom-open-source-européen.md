---
site: Siècle Digital
title: "Lancement du projet Sylva: un cloud télécom open source européen"
author: Zacharie Tazrout
date: 2022-11-17
href: https://siecledigital.fr/2022/11/17/lancement-du-projet-sylva-un-cloud-telecom-open-source-europeen
featured_image: https://siecledigital.fr/wp-content/uploads/2022/11/telecom-940x550.jpg
tags:
- Informatique en nuage
series:
- 202246
series_weight: 0
---

> Le projet Sylva réunit plusieurs opérateurs et founisseurs de télécom européens dans le but de concevoir un cadre cloud homogène.
