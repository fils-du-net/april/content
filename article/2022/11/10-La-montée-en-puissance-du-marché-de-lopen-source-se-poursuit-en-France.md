---
site: ZDNet France
title: "La montée en puissance du marché de l'open source se poursuit en France"
date: 2022-11-10
href: https://www.zdnet.fr/actualites/la-montee-en-puissance-du-marche-de-l-open-source-se-poursuit-en-france-39949512.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/02/open-source-code-620__w1200.jpg
tags:
- Économie
series:
- 202245
series_weight: 0
---

> Le marché de l'open source en France a été multiplié par 40 en un peu moins de vingt ans, accéléré par l'essor du cloud et des technologies innovantes. 
