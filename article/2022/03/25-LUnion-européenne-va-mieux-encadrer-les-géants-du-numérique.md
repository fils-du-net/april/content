---
site: Le Monde.fr
title: "L'Union européenne va mieux encadrer les géants du numérique"
date: 2022-03-25
href: https://www.lemonde.fr/pixels/article/2022/03/25/l-union-europeenne-va-mieux-encadrer-les-geants-du-numerique_6119050_4408996.html
featured_image: https://img.lemde.fr/2022/03/25/0/0/5524/3682/664/0/75/0/4dabef4_1648181497433-000-8xc7vp.jpg
tags:
- Europe
- Entreprise
- Interopérabilité
- Internet
series:
- 202212
series_weight: 0
---

> La nouvelle législation qui doit entrer en vigueur en janvier 2023 a pour but de mettre fin aux abus de position dominante des Gafam.
