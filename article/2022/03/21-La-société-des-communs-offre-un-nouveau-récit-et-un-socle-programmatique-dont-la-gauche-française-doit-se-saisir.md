---
site: Le Monde.fr
title: "«La société des communs offre un nouveau récit et un socle programmatique dont la gauche française doit se saisir» (€)"
date: 2022-03-21
href: https://www.lemonde.fr/idees/article/2022/03/21/la-societe-des-communs-offre-un-nouveau-recit-et-un-socle-programmatique-dont-la-gauche-francaise-doit-se-saisir_6118468_3232.html
tags:
- Partage du savoir
- Institutions
- Économie
series:
- 202212
series_weight: 0
---

> TRIBUNE. Un collectif d'élus, de chercheurs, de militants associatifs, d'entrepreneurs et de décideurs publics, parmi lesquels Manon Aubry, Thomas Piketty, Axelle Lemaire ou Noël Mamère, lance un appel pour que la campagne électorale soit l'occasion de promouvoir un «nouveau contrat social», afin de «reconnecter les institutions publiques» à «la vitalité des acteurs engagés sur le terrain».
