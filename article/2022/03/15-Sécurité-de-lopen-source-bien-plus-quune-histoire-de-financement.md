---
site: LeMagIT
title: "Sécurité de l’open source: bien plus qu’une histoire de financement (€)"
author: Gaétan Raoul
date: 2022-03-15
href: https://www.lemagit.fr/actualites/252514603/Securite-de-lopen-source-bien-plus-quune-histoire-de-financement
featured_image: https://cdn.ttgtmedia.com/visuals/searchFinancialApplications/procurement/financialapplications_article_011.jpg
tags:
- Économie
series:
- 202211
---

> La vulnérabilité Log4Shell a non seulement mis sur le gril la sécurité de l’open source, mais également relancé le débat consacré à son financement, alors que les acteurs du secteur appellent de leurs vœux l’adoption du principe de responsabilité partagée.
