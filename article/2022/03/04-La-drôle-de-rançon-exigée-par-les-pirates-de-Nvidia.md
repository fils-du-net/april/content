---
site: Siècle Digital
title: "La drôle de rançon exigée par les pirates de Nvidia"
author: Benjamin Terrasson
date: 2022-03-04
href: https://siecledigital.fr/2022/03/04/la-drole-de-rancon-exigee-par-les-pirates-de-nvidia
featured_image: https://siecledigital.fr/wp-content/uploads/2022/03/nvidia-q4-2021-scaled-1-940x550.jpeg
tags:
- Entreprise
- Innovation
series:
- 202209
---

> Un pirate menace de publier des informations secrètes de Nvidia si l'entreprise ne débride pas ses cartes graphiques utilisées pour le minage.
