---
site: ITRnews.com
title: "Supply chain: stopper l’élan des cybercriminels"
date: 2022-03-15
href: https://itrnews.com/articles/193916/supply-chain-stopper-lelan-des-cybercriminels.html
tags:
- Sensibilisation
series:
- 202211
series_weight: 0
---

> Selon le dernier baromètre du CESIN sur la cybersécurité des entreprises en France, les attaques indirectes par rebond via un prestataire ont augmenté de 5% pour atteindre 21% en 2021. Pour les organisations, la principale difficulté réside dans le fait que la défense de la supply chain est une mission difficile à accomplir compte tenu des centaines, voire des milliers de points d’entrée qui doivent être surveillés tout au long de la chaîne.
