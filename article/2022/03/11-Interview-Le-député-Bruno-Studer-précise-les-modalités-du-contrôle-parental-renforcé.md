---
site: Next INpact
title: "[Interview] Le député Bruno Studer précise les modalités du contrôle parental renforcé (€)"
description: Une question, un clic
author: Vincent Hermann
date: 2022-03-11
href: https://www.nextinpact.com/article/49853/interview-le-depute-bruno-studer-precise-modalites-controle-parental-renforce
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/9703.jpg
tags:
- Internet
- Institutions
series:
- 202210
---

> La loi visant à renforcer le contrôle parental sur les moyens d'accès à internet a été publiée le 2 mars au Journal officiel. Il lui manque un décret d’application, ainsi que la réponse définitive de la Commission européenne. Bruno Studer, député du Bas-Rhin et rapporteur du texte, a répondu à nos questions.
