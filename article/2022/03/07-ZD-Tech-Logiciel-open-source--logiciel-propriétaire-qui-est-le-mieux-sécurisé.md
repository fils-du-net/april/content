---
site: ZDNet France
title: "ZD Tech: Logiciel open source / logiciel propriétaire: qui est le mieux sécurisé?"
author: Guillaume Serries
date: 2022-03-07
href: https://www.zdnet.fr/actualites/zd-tech-logiciel-open-source-logiciel-proprietaire-qui-est-le-mieux-securise-39938407.htm
tags:
- Logiciels privateurs
- Entreprise
- Économie
series:
- 202210
series_weight: 0
---

> 89 % des responsables informatiques pensent que les logiciels libres sont aussi sûrs que les logiciels propriétaires. Et pour quelle raison? Et bien parce qu'ils jugent qu'ils sont arrivés à maturité. Oui, je suis d'accord avec vous, cela demande d'aller un peu plus dans les détails.
