---
site: La Tribune
title: "L'Open Source est l'origine et l'avenir des technologies de l'information"
author: Antoine Thomas
date: 2022-03-30
href: https://www.latribune.fr/opinions/tribunes/l-open-source-est-l-origine-et-l-avenir-des-technologies-de-l-information-907231.html
featured_image: https://static.latribune.fr/full_width/1898145/open-source.jpg
tags:
- Sensibilisation
series:
- 202213
series_weight: 0
---

> OPINION. Défendre le modèle Open Source, c'est faire le choix de la modernité et de l'innovation permanente