---
site: Le Soir
title: "Soigner son hygiène numérique"
author: Pierre Dewitte
date: 2022-03-22
href: https://www.lesoir.be/431726/article/2022-03-23/soigner-son-hygiene-numerique
featured_image: https://www.lesoir.be/sites/default/files/dpistyles_v2/ls_16_9_856w/2022/03/23/node_431726/28964653/public/2022/03/23/B9730360034Z.1_20220323083132_000+GAHK5F14H.1-0.jpg?itok=Vs5fTsMj1648035002
tags:
- Vie privée
series:
- 202212
series_weight: 0
---

> Le grand public peine parfois à réaliser l’importance de la vie privée, ou à identifier des solutions concrètes pour la préserver. Voici quelques pistes de réflexion pour assainir les pratiques en matière d’empreinte numérique.
