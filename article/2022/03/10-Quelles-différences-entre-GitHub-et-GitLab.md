---
site: ZDNet France
title: "Quelles différences entre GitHub et GitLab?"
author: Steven Vaughan-Nichols
date: 2022-03-10
href: https://www.zdnet.fr/pratique/quelles-differences-entre-github-et-gitlab-39938757.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/Pierre%20temp/Ordinateur%20panne%20A__w630.jpg
tags:
- Sensibilisation
- Innovation
series:
- 202210
series_weight: 0
---

> GitHub et GitLab sont tous deux basés sur le système de contrôle de version distribué Git. Pour autant, les deux plateformes adoptent des approches de développement très différentes.
