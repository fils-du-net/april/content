---
site: Next INpact
title: "Libre en Fête 2022"
date: 2022-03-04
href: https://www.nextinpact.com/lebrief/50016/libre-en-fete-2022
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/square-linked-media/1902.jpg
tags:
- Promotion
- april
series:
- 202209
series_weight: 0
---

> Près de 50 événements sont déjà référencés dans le cadre de la 21e édition de Libre en Fête, qui aura lieu du samedi 5 mars au dimanche 3 avril: des ateliers d'initiation à un logiciel ou à un service libre, projections de films libres ou sur le Libre, des fêtes d'installation (install party en anglais), de la cartographie participative, des ateliers pour apprendre à mieux protéger son intimité sur Internet, des soirées d'échanges autour des enjeux de l'informatique libre…
