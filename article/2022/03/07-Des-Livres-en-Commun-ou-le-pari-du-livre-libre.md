---
site: ActuaLitté.com
title: "Des Livres en Commun, ou “le pari du livre libre”"
author: Hocine Bouhadjera
date: 2022-03-07
href: https://actualitte.com/article/104501/interviews/des-livres-en-commun-ou-le-pari-du-livre-libre
featured_image: https://actualitte.com/media/cache/width_944_webp/uploads/images/540509677-b55f312cb8-o-621f3615e4a15099166887.png
tags:
- Partage du savoir
- Associations
- Licenses
series:
- 202210
series_weight: 0
---

> Framabook, la maison d'édition de l’association Framasoft, tournée vers l’élaboration et la promotion du logiciel libre, change son nom en Des Livres en Commun, et par là-même de logique éditoriale. L’équipe qui conduit cette aventure a accepté de nous présenter les principes de la nouvelle structure associative, où l’ambition principale est «de rémunérer les auteurs à leur juste valeur», en dynamitant tous les principes du sacro-saint copyright.
