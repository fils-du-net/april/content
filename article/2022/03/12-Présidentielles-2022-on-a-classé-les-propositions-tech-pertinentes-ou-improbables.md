---
site: Numerama
title: "Présidentielles 2022: on a classé les propositions tech, pertinentes ou improbables"
description: De la cybersécurité au métavers en passant par la 5G
author: Aurore Gayte
date: 2022-03-12
href: https://www.numerama.com/politique/877057-presidentielles-2022-on-a-classe-les-propositions-tech-pertinentes-ou-improbables.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2022/03/pertinent-1024x576.jpg
tags:
- Institutions
series:
- 202210
series_weight: 0
---

> À un mois du premier tour de l’élection présidentielle, les candidats ayant reçu leurs 500 parrainages ont finalisé leur programme. Numerama fait le tour de leurs propositions sur la tech, le numérique et la cybersécurité.
