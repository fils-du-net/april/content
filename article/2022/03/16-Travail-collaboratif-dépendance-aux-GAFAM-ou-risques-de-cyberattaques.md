---
site: Acteurs Publics 
title: "Travail collaboratif, dépendance aux GAFAM ou risques de cyberattaques?"
date: 2022-03-16
href: https://www.acteurspublics.fr/articles/travail-collaboratif-dependance-aux-gafam-ou-risques-de-cyberattaques
featured_image: https://www.acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/39/5bbe182f97eb404c0847aa582e270966a92f243d.jpeg
tags:
- Informatique en nuage
- Entreprise
- Administration
series:
- 202211
series_weight: 0
---

> Nombre d’administrations et de collectivités ont multiplié les outils au plus fort de la crise COVID et se sont précipitées sur les solutions les plus connues, telles que Zoom, Teams et autres GAFAM. La généralisation du télétravail a bouleversé nos habitudes et nous oblige à jongler avec un grand nombre de technologies et de procédures
