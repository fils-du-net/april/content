---
site: rts.ch
title: "L'idée d'un cloud souverain réapparaît en Suisse"
author: Pascal Wassmer
date: 2022-03-08
href: https://www.rts.ch/info/sciences-tech/technologies/12917291-lidee-dun-cloud-souverain-reapparait-en-suisse.html
featured_image: https://www.rts.ch/2019/11/09/12/14/10851608.image?w=1280&h=720
tags:
- Informatique en nuage
- International
series:
- 202210
series_weight: 0
---

> Le télétravail puis la guerre en Ukraine ont éclairé sous un nouveau jour la problématique de la sécurité et de l'accès à nos données. Le concept d'un cloud suisse élaboré sous l'égide de la Confédération est réapparu au Parlement.
