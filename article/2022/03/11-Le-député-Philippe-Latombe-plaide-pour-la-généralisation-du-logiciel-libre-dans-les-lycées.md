---
site: Next INpact
title: "Le député Philippe Latombe plaide pour la généralisation du logiciel libre dans les lycées"
date: 2022-03-11
href: https://www.nextinpact.com/lebrief/68590/le-depute-philippe-latombe-plaide-pour-generalisation-logiciel-libre-dans-lycees
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/square-linked-media/1902.jpg
tags:
- Éducation
series:
- 202210
series_weight: 0
---

> Sous couvert de rationalisation et de limitation des coûts, les communes et autres collectivités locales finançant les équipements informatiques centralisent la gestion des équipements des établissements scolaires.
