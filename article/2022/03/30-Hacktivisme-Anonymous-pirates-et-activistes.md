---
site: Rotek
title: "Hacktivisme: Anonymous, pirates et activistes"
author: Hugo Bernard
date: 2022-03-30
href: https://rotek.fr/hacktivisme-anonymous-pirates-activistes
tags:
- Sensibilisation
series:
- 202213
series_weight: 0
---

> Les Anonymous sont liés à la notion d'”hacktivisme”. Il semble même impossible de parler d'Anonymous sans parler d'hacktivisme.