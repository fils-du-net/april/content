---
site: Le Monde Informatique
title: "Avec le sabotage de node-ipc, la protestation dans l'open source inquiète"
author: Lucian Constantin
date: 2022-03-21
href: https://www.lemondeinformatique.fr/actualites/lire-avec-le-sabotage-de-node-ipc-la-protestation-dans-l-open-source-inquiete-86188.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000084593.jpg
tags:
- Sensibilisation
series:
- 202212
series_weight: 0
---

> La méthode choisie par le développeur de node-ipc pour protester contre l'invasion de l'Ukraine par la Russie jette involontairement le doute sur l'intégrité de la chaîne d'approvisionnement des logiciels.
