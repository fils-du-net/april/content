---
site: ZDNet France
title: "Pourquoi une minorité de développeurs pourraient gâcher le potentiel de l'open-source?"
author: Steven Vaughan-Nichols
date: 2022-03-25
href: https://www.zdnet.fr/actualites/pourquoi-une-minorite-de-developpeurs-pourraient-gacher-le-potentiel-de-l-open-source-39939491.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/Pierre%20temp/Teletravail%20F__w1200.jpg
tags:
- Sensibilisation
series:
- 202212
---

> L'un des aspects les plus étonnants de l'open-source n'est pas qu'il produise d'excellents logiciels. C'est que tant de développeurs mettent leur ego de côté pour créer de grands programmes avec l'aide des autres. Reste qu'une poignée de programmeurs font désormais passer leurs propres préoccupations avant le bien du plus grand nombre et risquent de ruiner les logiciels à code source ouvert pour tous.
