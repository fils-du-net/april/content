---
site: Le Monde Informatique
title: "Harvard recense les paquets open source les plus utilisés"
author: Scott Carey
date: 2022-03-04
href: https://www.lemondeinformatique.fr/actualites/lire-harvard-recense-les-paquets-open-source-les-plus-utilises-86018.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000084237.jpg
tags:
- Économie
- Sensibilisation
series:
- 202209
series_weight: 0
---

> Les chercheurs de Harvard ont mené un travail de recensement des paquets open source les plus utilisés. L'objectif est de sensibiliser l'industrie sur les problématiques de sécurité et d'éviter un prochain exploit Log4j ou Heartbleed.
