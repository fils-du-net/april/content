---
site: Next INpact
title: "Le DMA et le casse-tête de l'interopérabilité des messageries (€)"
description: Téléphone maisons
author: Jean-Marc Manach
date: 2022-03-31
href: https://www.nextinpact.com/article/68791/le-dma-et-casse-tete-interoperabilite-messageries
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/561.jpg
tags:
- Interopérabilité
- Europe
series:
- 202213
---

> Le Digital Market Act prévoit d'obliger les principales plateformes de messagerie à l'interopérabilité. De nombreux experts et professionnels de la sécurité y voient une usine à gaz insurmontable. Les co-fondateurs de Matrix, un protocole promouvant précisément l'interopérabilité, y voient du pain béni pour le futur des messageries.