---
site: ZDNet France
title: "Atos met en avant son activité dans l'open source"
author: Thierry Noisette
date: 2022-03-28
href: https://www.zdnet.fr/blogs/l-esprit-libre/atos-met-en-avant-son-activite-dans-l-open-source-39939617.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/Atos_IT_Solutions_and_Services_GmbH_in_Wien.jpg
tags:
- Entreprise
- Marchés publics
- Administration
series:
- 202213
series_weight: 0
---

> L'entreprise présente en une page web ses experts et activités, précisant mener «une politique de 100% reversements».