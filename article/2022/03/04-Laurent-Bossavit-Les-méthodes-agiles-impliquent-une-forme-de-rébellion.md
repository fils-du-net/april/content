---
site: Acteurs Publics 
title: "Laurent Bossavit: “Les méthodes agiles impliquent une forme de rébellion”"
author: Laurent Bossavit
date: 2022-03-04
href: https://www.acteurspublics.fr/articles/laurent-bossavit-les-methodes-agiles-impliquent-une-forme-de-rebellion
featured_image: https://www.acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/38/84bfd23281398d5625cc3fc660f3ab0f6f9c123b.jpeg
tags:
- Administration
series:
- 202209
series_weight: 0
---

> L’ancien coach à Beta.gouv*, qui a contribué aux premières heures des “start-up d’État” appelle, dans cette tribune, à ne pas faire des méthodes agiles un simple outil de communication. Le prétendu “ritualisme” de la “méthode agile” est justement tout ce qui en fait la force et la rigueur.
