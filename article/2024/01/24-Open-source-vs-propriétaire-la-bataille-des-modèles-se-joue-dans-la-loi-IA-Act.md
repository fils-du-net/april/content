---
site: Numerama
title: "Open source vs propriétaire: la bataille des modèles se joue dans la loi IA Act"
description: Guerre de modèles
author: Julien Lausson
date: 2024-01-24
href: https://www.numerama.com/tech/1616102-une-bataille-de-lintelligence-artificielle-se-joue-dans-la-loi-ia-act.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2024/01/ai-glitch-1024x574.jpg?avif=1&key=19516efd
tags:
- Sciences
- Europe
series:
- 202404
series_weight: 0
---

> L'Union européenne devrait se montrer plus clémente pour les modèles d'IA open source, par rapport à leurs homologues propriétaires. C'est en tout cas ce qui transparait d'un document de travail qui a fuité sur le futur règlement de l'AI Act.
