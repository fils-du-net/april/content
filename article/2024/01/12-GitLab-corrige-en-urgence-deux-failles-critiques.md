---
site: Le Monde Informatique
title: "GitLab corrige en urgence deux failles critiques"
author: Dominique Filippone
date: 2024-01-12
href: https://www.lemondeinformatique.fr/actualites/lire-gitlab-corrige-en-urgence-deux-failles-critiques-92663.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000095554.png
tags:
- Innovation
series:
- 202402
---

> Intrusion, Hacking et Pare-feu: Le spécialiste en dépôt de code open source et solutions devops GitLab a publié des mises à jour de sécurité pour corriger deux vulnérabilités critiques. L'une d'elles au score CVSS de 10 pourrait être exploitée débouchant sur de la prise de contrôle de comptes sans interaction utilisateur.
