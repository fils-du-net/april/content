---
site: ZDNet France
title: "Libre et open source express: «Piège Microsoft» pour les armées, Versailles numérisé, Eiffel précurseur"
author: Thierry Noisette
date: 2024-01-29
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-piege-microsoft-pour-les-armees-versailles-numerise-eiffel-precurseur-39963896.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/05/open-source-KeepCalmAndUse.jpg
seeAlso: "[Un rapport parlementaire sur la cybersécurité alerte sur «le piège Microsoft»](https://april.org/un-rapport-parlementaire-sur-la-cybersecurite-alerte-sur-le-piege-microsoft)"
tags:
- Administration
series:
- 202405
series_weight: 0
---

> En bref. Un rapport s'inquiète des risques pour le ministère des Armées de la dépendance à Microsoft. 10.000 documents du château de Versailles bientôt numérisés. Gustave Eiffel partisan de l'open source.
