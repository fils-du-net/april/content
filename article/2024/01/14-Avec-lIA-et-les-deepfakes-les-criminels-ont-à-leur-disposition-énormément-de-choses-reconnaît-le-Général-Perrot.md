---
site: 20minutes.fr
title: "Avec l'IA et les deepfakes, «les criminels ont à leur disposition énormément de choses», reconnaît le Général Perrot"
author: Laure Beaudonnet
date: 2024-01-14
href: https://www.20minutes.fr/high-tech/4070143-20240114-ia-deepfakes-criminels-disposition-enormement-choses-reconnait-general-perrot
featured_image: https://img.20mn.fr/vzK-bo5sQZSvaTf0GX_X-ik/830x532_un-ordinateur-infecte-par-un-ransomware-illustration
tags:
- Sciences
series:
- 202402
series_weight: 0
---

> Le Général Patrick Perrot, coordinateur pour l'IA de la Gendarmerie nationale, décrit le nouveau visage de la criminalité à l'ère des intelligences artificielles dites génératives
