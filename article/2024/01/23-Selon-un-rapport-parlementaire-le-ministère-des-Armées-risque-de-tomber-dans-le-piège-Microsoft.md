---
site: Zone Militaire
title: "Selon un rapport parlementaire, le ministère des Armées risque de tomber dans le 'piège Microsoft'"
author: Laurent Lagneau
date: 2024-01-23
href: https://www.opex360.com/2024/01/23/selon-un-rapport-parlementaire-le-ministere-des-armees-risque-de-tomber-dans-le-piege-microsoft
featured_image: https://www.opex360.com/wp-content/uploads/influence-20221109.jpg
tags:
- april
- Administration
- Marchés publics
series:
- 202404
series_weight: 0
---

> «Avec des si, on mettrait Paris en bouteille», dit-on. Mais il n’en demeure pas moins que la France a eu en main tous les atouts pour favoriser l’essor de la micro-informatique dans les années 1970, avec le micro-ordinateur Micral, alors en avance sur les Altaïr 8800 et Apple II américains, ou encore avec le projet Cyclades qui, mené par l’ingénieur Louis Pouzin, annonçait l’Internet tel que nous le connaissons aujourd’hui.
