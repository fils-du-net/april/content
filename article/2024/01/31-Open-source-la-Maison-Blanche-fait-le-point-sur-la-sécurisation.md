---
site: ZDNet France
title: "Open source: la Maison Blanche fait le point sur la sécurisation"
author: Thierry Noisette
date: 2024-01-31
href: https://www.zdnet.fr/blogs/l-esprit-libre/open-source-la-maison-blanche-fait-le-point-sur-la-securisation-39963952.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/Pierre%20temp/Maison%20Blanche%20B__w630.jpg
tags:
- International
- Administration
series:
- 202405
series_weight: 0
---

> Pour l'administration Biden, "garantir la résilience des logiciels open source est une nécessité technique et un impératif stratégique".
