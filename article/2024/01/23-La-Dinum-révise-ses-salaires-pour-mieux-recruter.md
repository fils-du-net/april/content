---
site: Le Monde Informatique
title: "La Dinum révise ses salaires pour mieux recruter"
author: Emmanuelle Delsol
date: 2024-01-23
href: https://www.lemondeinformatique.fr/actualites/lire-la-dinum-revise-ses-salaires-pour-mieux-recruter-92737.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000095691.jpg
tags:
- Administration
series:
- 202404
---

> Le numérique de l'Etat a réévalué ses niveaux de rémunération à hauteur de 10% au dessus du marché pour s'aligner sur le secteur privé. Il a aussi opté pour une fourchette, plutôt qu'une grille de salaires, afin de donner toute souplesse aux recruteurs. Objectif : séduire des profils très courtisés.
