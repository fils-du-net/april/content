---
site: Le Monde Informatique
title: "L'IA et le cloud poussent à redéfinir les licences open source"
author: Matt Asay
date: 2024-01-02
href: https://www.lemondeinformatique.fr/actualites/lire-l-ia-et-le-cloud-poussent-a-redefinir-les-licences-open-source-92565.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000095414.jpg
tags:
- Licenses
- Informatique en nuage
series:
- 202401
series_weight: 0
---

> Le cloud et l'IA sont devenus aujourd'hui des technologies essentielles, et toutes deux ont bouleversé le cadre stricto sensu des licences open source. Pour le contributeur Matt Asay, 2024 pourrait être l'année propice à la redéfinition de ce modèle.
