---
site: IT SOCIAL
title: "L'open source plébiscité dans les entreprises et le secteur public"
author: Serge Escalé
date: 2024-01-05
href: https://itsocial.fr/enjeux-it/enjeux-applicatif/logiciels/lopen-source-plebiscite-dans-les-entreprises-et-le-secteur-public
featured_image: https://itsocial.fr/wp-content/uploads/2024/01/Article-346-1024x585.jpg
tags:
- Entreprise
- Administration
series:
- 202401
series_weight: 0
---

> Plus de 8 entreprises sur 10 utilisent de l’open source et neuf administrations sur 10 participent à son développement. Malgré leur bonne réputation, ces logiciels ouverts manquent d’actions de formations dans le privé et se heurtent aux craintes sur leur stabilité dans le secteur public.
