---
site: Siècle Digital
title: "Loi SREN: une régulation du numérique à la française très critiquée"
author: Grégoire Levy
date: 2024-01-18
href: https://siecledigital.fr/2024/01/18/loi-sren-une-regulation-du-numerique-a-la-francaise-tres-critiquee/
featured_image: https://siecledigital.fr/wp-content/uploads/2023/03/10437350944_8b11f4743f_k-1-940x550.jpg.webp
tags:
- Associations
- Internet
- Institutions
series:
- 202403
series_weight: 0
---

> Devant notamment protéger les mineurs contre la pornographie, le projet de loi SREN est jugé autoritaire par plusieurs associations.
