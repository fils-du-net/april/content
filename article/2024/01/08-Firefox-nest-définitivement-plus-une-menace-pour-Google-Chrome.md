---
site: ZDNet France
title: "Firefox n'est définitivement plus une menace pour Google Chrome"
author: Wuyong Kim
date: 2024-01-08
href: https://www.zdnet.fr/actualites/firefox-n-est-definitivement-plus-une-menace-pour-google-chrome-39963390.htm
featured_image: https://image.zdnet.co.kr/2020/11/18/801b6f0218f4b84d333a5c3394356744.png
tags:
- Internet
series:
- 202402
series_weight: 0
---

> Le navigateur de la fondation Mozilla ne cumule plus que 2,2 % de parts de marché. Retour sur la chute du célèbre navigateur open source.
