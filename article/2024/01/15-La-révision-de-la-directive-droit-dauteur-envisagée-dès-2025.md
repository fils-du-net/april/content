---
site: Electron Libre
title: "La révision de la directive droit d’auteur envisagée dès 2025 (€)"
author: Isabelle Szczepanski
date: 2024-01-15
href: https://electronlibre.info/2024/01/15/la-revision-de-la-directive-droit-dauteur-envisagee-des-2025
tags:
- Droit d'auteur
- Europe
series:
- 202403
series_weight: 0
---

> La révision de la directive droit d’auteur de 2019 n’est plus un tabou pour les services de la Commission européenne, qui envisagent d’y travailler dès le début du nouveau mandat.
