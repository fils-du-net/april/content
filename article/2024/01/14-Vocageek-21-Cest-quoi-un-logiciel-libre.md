---
site: Geek Junior
title: "Vocageek #21: C'est quoi un logiciel libre?"
author: Christophe Coquis
date: 2024-01-14
href: https://www.geekjunior.fr/vocageek-21-c-est-quoi-logiciel-libre-58650
featured_image: https://www.geekjunior.fr/wp-content/uploads/2024/01/illustration-open-source.jpeg
tags:
- Sensibilisation
series:
- 202402
series_weight: 0
---

> Le logiciel libre a bouleversé la façon de développer et de créer en informatique. Le logiciel libre n'est pas qu'un mode de développement et de distribution du logiciel.
