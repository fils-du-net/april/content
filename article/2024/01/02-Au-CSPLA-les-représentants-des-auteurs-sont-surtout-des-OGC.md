---
site: ActuaLitté.com
title: "Au CSPLA, les représentants des auteurs sont (surtout) des OGC"
author: Antoine Oury
date: 2024-01-02
href: https://actualitte.com/article/115029/auteurs/au-cspla-les-representants-des-auteurs-sont-surtout-des-ogc
featured_image: https://actualitte.com/media/cache/width_944_webp/uploads/images/au-cspla-les-representants-des-auteurs-sont-surtout-des-ogc-6593c2b1eb914509073275.jpg
tags:
- Droit d'auteur
series:
- 202401
series_weight: 0
---

> Plusieurs arrêtés du ministère de la Culture, signés début décembre 2023, mais publiés il y a quelques jours seulement, touchent à l'organisation du Conseil supérieur de la propriété littéraire et artistique (CSPLA). Sans modifier la tendance générale, pour les auteurs, surtout représentés par des organismes de gestion collective (OGC).
