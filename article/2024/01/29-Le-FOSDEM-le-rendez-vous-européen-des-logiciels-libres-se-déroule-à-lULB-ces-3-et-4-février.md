---
site: RTBF
title: "Le FOSDEM, le rendez-vous européen des logiciels libres, se déroule à l'ULB ces 3 et 4 février"
author: Samuel Camus
date: 2024-01-29
href: https://www.rtbf.be/article/le-fosdem-le-rendez-vous-europeen-des-logiciels-libres-se-deroule-a-lulb-ces-3-et-4-fevrier-11320687
featured_image: https://ds.static.rtbf.be/article/image/1248x702/e/1/0/5e74088a8d6e611a5cca9a05b9788496-1706547084.jpg
tags:
- Promotion
series:
- 202405
series_weight: 0
---

> Le "Free & Open Software Developers’European Meeting", ou plus simplement "FOSDEM", s’apprête à vivre cette année sa 24e édition. Ce rassemblement annuel se déroule à l’ULB et accueillera diverses conférences centrées autour du développement de logiciel, qu’il soit passé, futur ou présent.
