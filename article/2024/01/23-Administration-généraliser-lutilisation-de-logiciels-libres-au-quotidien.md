---
site: ZDNet France
title: "Administration: 'généraliser l'utilisation de logiciels libres' au quotidien"
author: Thierry Noisette
date: 2024-01-23
href: https://www.zdnet.fr/blogs/l-esprit-libre/administration-generaliser-l-utilisation-de-logiciels-libres-au-quotidien-39963768.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/Stephanie_Schaer_CMJN_HD.jpg
tags:
- Administration
series:
- 202404
series_weight: 0
---

> La directrice interministérielle du numérique (Dinum), Stéphanie Schaer, a exposé au Conseil national du numérique comment le logiciel libre participe à la stratégie de l'État.
