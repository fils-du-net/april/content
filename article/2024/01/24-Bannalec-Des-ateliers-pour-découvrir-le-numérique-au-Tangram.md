---
site: ouest-france.fr
title: "Bannalec. Des ateliers pour découvrir le numérique au Tangram"
date: 2024-01-24
href: https://www.ouest-france.fr/bretagne/bannalec-29380/bannalec-des-ateliers-pour-decouvrir-le-numerique-au-tangram-fe2ca79e-baa2-11ee-b9d9-64e72790227c
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyNDAxNDU1YWQ2MTcyYjE1M2MwZmNjOGM2MmUzOGM4MjZhZjk?width=940&focuspoint=50%2C42&cropresize=1&client_id=bpeditorial&sign=fccc3ac5c0465f5923c7db47b1a0992c41d09c95fd22fcdadf936f4e0aff6d08
tags:
- Associations
series:
- 202404
series_weight: 0
---

> Des ateliers dédiés à l'informatique sont organisés tous les mois à la médiathèque de Bannalec (Finistère), pour aider à réduire la fracture ou la facture numérique, avec des bénévoles de L'@ssourie et la conseillère numérique de Quimperlé communauté.
