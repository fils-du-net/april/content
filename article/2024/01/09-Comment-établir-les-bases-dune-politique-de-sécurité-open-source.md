---
site: LeMagIT
title: "Comment établir les bases d’une politique de sécurité open source"
author: George Lawton
date: 2024-01-09
href: https://www.lemagit.fr/conseil/Comment-etablir-les-bases-dune-politique-de-securite-open-source
featured_image: https://cdn.ttgtmedia.com/rms/onlineimages/security_a299192530.jpg
tags:
- Licenses
series:
- 202402
series_weight: 0
---

> L'utilisation de logiciels open source soulève des questions en matière de sécurité et de propriété intellectuelle. Voici comment prendre des décisions judicieuses et éviter des situations potentiellement regrettables.
