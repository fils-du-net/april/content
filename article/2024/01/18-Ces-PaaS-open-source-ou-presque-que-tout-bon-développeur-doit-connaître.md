---
site: LeMagIT
title: "Ces PaaS open source (ou presque) que tout bon développeur doit connaître"
author: Chris Tozzi, Zachary Flower, Gaétan Raoul
date: 2024-01-18
href: https://www.lemagit.fr/conseil/Ces-PaaS-open-source-ou-presque-que-les-developpeurs-devraient-connaitre
featured_image: https://cdn.ttgtmedia.com/visuals/German/article/cloud-management-automation-cogs-adobe.jpg
tags:
- Informatique en nuage
- Licenses
series:
- 202403
series_weight: 0
---

> Une PaaS open source est une bonne option pour les développeurs qui souhaitent contrôler l’hébergement de leurs applications et simplifier leur déploiement, mais il est important de connaître ses fondations techniques, ses principaux soutiens (commerciaux ou non) et la nature de sa licence.
