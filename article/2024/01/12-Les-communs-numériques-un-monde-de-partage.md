---
site: France Culture
title: "Les \"communs numériques\", un monde de partage"
date: 2024-01-12
href: https://www.radiofrance.fr/franceculture/podcasts/un-monde-connecte/les-communs-numeriques-un-monde-de-partage-1621980
featured_image: https://www.radiofrance.fr/s3/cruiser-production/2024/01/57dcc63d-f716-486f-9503-6b39e4978613/560x315_sc_gettyimages-1217664405-2620-x-2000-1.webp
tags:
- Économie
- Partage du savoir
series:
- 202402
series_weight: 0
---

> Les 'communs numériques' sont l'antithèse même des GAFAM. Ces ressources partagées — dont les éléments les plus visibles sont Linux, Wikipédia et les licences Creative Commons — sont toutes conçues avec la même philosophie: favoriser le partage culturel sans enrichissement individuel.
