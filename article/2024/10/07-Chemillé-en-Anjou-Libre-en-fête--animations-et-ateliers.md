---
site: "Courrier de l'Ouest"
title: "Chemillé-en-Anjou. Libre en fête: animations et ateliers"
date: 2024-10-07
href: https://cholet.maville.com/actu/actudet_-chemille-en-anjou.-libre-en-fete-animations-et-ateliers-_dep-6495815_actu.Htm
featured_image: https://maville.com/photosmvi/2024/10/07/P34642660D6495815G_crop_640-330_.jpg
tags:
- Associations
- Sensibilisation
series:
- 202441
series_weight: 0
---

> Qui n’a pas rêvé de pouvoir créer ses propres tableaux informatiques, présentations ou diaporamas sans y laisser une participation financière toujours assez coûteuse. Il y a des alternatives avec les logiciels libres.
