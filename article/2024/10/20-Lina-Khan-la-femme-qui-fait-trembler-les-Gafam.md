---
site: Le Monde.fr
title: "Lina Khan, la femme qui fait trembler les Gafam (€)"
author: Corine Lesnes
date: 2024-10-20
href: https://www.lemonde.fr/economie/article/2024/10/19/lina-khan-la-femme-qui-fait-trembler-les-gafam_6355658_3234.html
featured_image: https://img.lemde.fr/2024/10/18/0/0/3000/2001/800/0/75/0/22b08d5_1729263632817-017108.jpg
tags:
- Entreprise
- Économie
series:
- 202442
series_weight: 0
---

> «L'économie, enjeu d'une Amérique fracturée» (5/5). La présidente de la Federal Trade Commission, l'agence antitrust américaine, combat les grands monopoles. Elle irrite les républicains et participe aux efforts déployés par l'administration Biden pour défendre son action contre l'inflation.
