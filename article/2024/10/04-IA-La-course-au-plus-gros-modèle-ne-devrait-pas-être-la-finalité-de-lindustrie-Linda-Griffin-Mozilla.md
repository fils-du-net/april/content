---
site: La Tribune
title: "IA: «La course au plus gros modèle ne devrait pas être la finalité de l'industrie» (Linda Griffin, Mozilla)"
author: "Marine Protais"
date: 2024-10-04
href: https://www.latribune.fr/technos-medias/internet/ia-la-course-au-plus-gros-modele-ne-devrait-pas-etre-la-finalite-de-l-industrie-linda-griffin-mozilla-1008081.html
featured_image: https://static.latribune.fr/full_width/2455543/course-a-l-ia.jpg
tags:
- Sciences
series:
- 202440
series_weight: 0
---

> ENTRETIEN. Il y a trente ans, Mozilla luttait pour empêcher Microsoft d'imposer son navigateur comme unique porte d'entrée du Web. Aujourd'hui, la fondation renouvelle son engagement, cette fois-ci dans le domaine de l'intelligence artificielle. Linda Griffin, vice-présidente des politiques globales de Mozilla Corporation, explique que l'objectif est d'éviter que l'IA ne soit dominée par la vision des Big Tech, et sur leur course effrénée aux plus gros modèles, qui, selon elle, va à l'encontre de l'intérêt collectif.
