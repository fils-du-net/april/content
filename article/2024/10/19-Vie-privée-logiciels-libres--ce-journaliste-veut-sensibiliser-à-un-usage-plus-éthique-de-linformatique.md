---
site: La République
title: "Vie privée, logiciels libres: ce journaliste veut sensibiliser à un usage plus éthique de l'informatique"
date: 2024-10-19
href: https://actu.fr/ile-de-france/samois-sur-seine_77441/vie-privee-logiciels-libres-ce-journaliste-veut-sensibiliser-a-un-usage-plus-ethique-de-linformatique_61732546.html
featured_image: https://static.actu.fr/uploads/2024/10/db2f62c9e7076af2f62c9e707733f6v-960x640.jpg
tags:
- Vie privée
series:
- 202442
series_weight: 0
---

> Thierry Pigot, habitant de Samois-sur-Seine et journaliste spécialisé en technologies numériques vient de publier un livre pour simplifier l'informatique du quotidien. Rencontre.
