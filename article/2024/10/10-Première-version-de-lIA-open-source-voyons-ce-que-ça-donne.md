---
site: ZDNET
title: "Première version de l'IA open source, voyons ce que ça donne"
author: Steven Vaughan-Nichols
date: 2024-10-10
href: https://www.zdnet.fr/actualites/premiere-version-de-lia-open-source-voyons-ce-que-ca-donne-399289.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/02/Intelligence20artificielle20B__w630-630x410.jpg
tags:
- Sciences
series:
- 202441
series_weight: 0
---

> L'OSI et ses alliés se rapprochent d'une définition de l'intelligence artificielle open source. Mais les puristes ne sont pas les seuls à être mécontents.
