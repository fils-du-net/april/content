---
site: clubic.com
title: "NVIDIA veut faire de l'ombre à ses clients (et à ChatGPT) avec ses nouveaux modèles d'IA open source"
date: 2024-10-05
href: https://www.clubic.com/actualite-539525-nvidia-veut-faire-de-l-ombre-a-ses-clients-et-a-chatgpt-avec-ses-nouveaux-modeles-d-ia-open-source.html
featured_image: https://pic.clubic.com/062c07942216856/1600x1022/smart/jensen-huang.webp
tags:
- Sciences
- Entreprise
series:
- 202440
---

> Le géant des cartes graphiques vient de frapper un grand coup dans le monde de l'IA. En lançant NVLM 1.0, une famille de modèles de langage multimodaux open source, l'entreprise se positionne comme un acteur majeur de l'IA générative, au risque de faire de l'ombre à ses propres clients.
