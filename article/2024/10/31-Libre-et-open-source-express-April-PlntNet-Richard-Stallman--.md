---
site: ZDNET
title: "Libre et open source express: April, Pl@ntNet, Richard Stallman,  ..."
date: 2024-10-31
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-april-plntnet-richard-stallman-filigran-400488.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/02/arcom-365x200.jpg
tags:
- april
- Entreprise
- Internet
- Institutions
- Administration
- Promotion
- Innovation
- Sciences
- Vote électronique
- Europe
- International
series:
- 202444
---

> En bref. Appels aux dons de l'April et de PL@ntNet. Un rapport virulent sur RMS et son influence. Levée de fonds de Filigran.