---
site: ZDNET
title: "IBM redouble d'efforts en matière d'IA open source avec de nouveaux modèles Granite 3.0"
date: 2024-10-23
href: https://www.zdnet.fr/actualites/ibm-redouble-defforts-en-matiere-dia-open-source-avec-de-nouveaux-modeles-granite-3-0-399956.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/10/ibm_granite_llm-750x410.jpg
tags:
- Sciences
series:
- 202443
series_weight: 0
---

> Les grands modèles de langage Granite de Big Blue conçus pour les entreprises sont désormais disponibles sous licence Apache 2.0.
