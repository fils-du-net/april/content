---
site: ZDNET
title: "Wordpress: l'écosystème open source s'enfonce dans le conflit"
date: 2024-10-16
href: https://www.zdnet.fr/actualites/wordpress-lecosysteme-open-source-senfonce-dans-le-conflit-399588.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/02/wordpress-750x410.jpg
tags:
- Internet
series:
- 202442
series_weight: 0
---

> Une guerre fait rage depuis plusieurs semaines entre le fondateur du projet Matthew Mullenweg et l'un des principaux fournisseurs de site web Wordpress, WP Engine. 
