---
site: ZDNET
title: "Voici la définition officielle de l'IA open source, mais le combat continue"
author: Steven Vaughan-Nichols
date: 2024-10-31
href: https://www.zdnet.fr/actualites/voici-la-definition-officielle-de-lia-open-source-mais-le-combat-continue-400542.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/05/open-source_panneau.jpg
tags:
- Sciences
series:
- 202444
---

> Il a fallu près de deux ans à l'OSI pour créer et mettre en place l'OSAID. Mais sans aucun changement par rapport à la dernière version de l'OSAID, c'est enfin chose faite. Malheureusement, tout le monde n'en est pas satisfait. Et même ses créateurs admettent qu'il s'agit d'un travail en cours.
