---
site: ZDNET
title: "Un 'Guide pratique des logiciels libres' pour une logithèque idéale"
author: Thierry Noisette
date: 2024-10-21
href: https://www.zdnet.fr/blogs/l-esprit-libre/un-guide-pratique-des-logiciels-libres-pour-une-logitheque-ideale-399850.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/10/guide-pratique-des-logiciels-libres--750x410.jpg
tags:
- Sensibilisation
- Informatique en nuage
series:
- 202443
series_weight: 0
---

> Cet ouvrage présente les avantages et enjeux du Libre, et une vingtaine de logiciels libres de référence dans différents domaines.
