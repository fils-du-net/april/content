---
site: Next
title: "Du code propriétaire dans le projet GNU Boot, les difficultés du libre face au matériel courant"
description: Penser à se laver les mains régulièrement
author: Vincent Hermann
date: 2024-10-21
href: https://next.ink/154804/du-code-proprietaire-dans-le-projet-gnu-boot-les-difficultes-du-libre-face-au-materiel-courant
featured_image: https://next.ink/wp-content/uploads/2024/04/long_ma1_unsplash.webp
tags:
- Sensibilisation
series:
- 202443
series_weight: 0
---

> Le projet GNU Boot est bien embêté: pour la deuxième fois en moins d’un an, l’équipe de développement s’est retrouvée à pousser du code propriétaire dans leur logiciel. Problème, le projet se veut 100 % libre. Le cas illustre la difficulté du monde open source et libre à prévoir du support matériel avec les marques courantes.
