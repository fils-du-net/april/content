---
site: AirZen Radio
title: "Geotrek, un logiciel libre pour informer et sensibiliser les randonneurs"
author: Marie-Belle Parseghian
date: 2024-10-11
href: https://www.airzen.fr/geotrek-un-logiciel-libre-pour-informer-et-sensibiliser-les-randonneurs
featured_image: https://www.airzen.fr/wp-content/uploads/2024/09/Thierry-Maillet-Parc-national-des-Ecrins-3-e1727365646253.jpg
tags:
- Sensibilisation
series:
- 202441
series_weight: 0
---

> Les parcs nationaux des Écrins et du Mercantour ont développé Geotrek, un logiciel libre qui évolue en fonction des enrichissements de chacun.
