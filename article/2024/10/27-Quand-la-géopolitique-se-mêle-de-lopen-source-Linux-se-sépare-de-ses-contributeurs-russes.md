---
site: clubic.com
title: "Quand la géopolitique se mêle de l'open source: Linux se sépare de ses contributeurs russes"
author: Naïm Bada
date: 2024-10-27
href: https://www.clubic.com/actualite-541763-quand-la-geopolitique-se-mele-de-l-open-source-linux-se-separe-de-ses-contributeurs-russes.html
featured_image: https://pic.clubic.com/e31ba9652251757/1600x900/smart/russie-processeur.webp
tags:
- International
series:
- 202443
series_weight: 0
---

> La guerre en Ukraine et les sanctions internationales contre la Russie ont des répercussions inattendues. Le projet open source Linux vient en effet de retirer une douzaine de contributeurs russes de la liste officielle des maintainers du noyau, suscitant un vif débat au sein de la communauté.
