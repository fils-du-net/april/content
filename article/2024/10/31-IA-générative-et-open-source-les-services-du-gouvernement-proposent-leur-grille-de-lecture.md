---
site: Next
title: "IA générative et open source: les services du gouvernement proposent leur grille de lecture"
author: Martin Clavey
date: 2024-10-31
href: https://next.ink/156309/ia-generative-et-open-source-les-services-du-gouvernement-proposent-leur-grille-de-lecture
featured_image: https://next.ink/wp-content/uploads/2023/12/viktor-forgacs-LNwIJHUtED4-unsplash.jpg
tags:
- Sciences
series:
- 202444
series_weight: 0
---

> Alors que l’Open Source Initiative (OSI) a publié sa définition de l'IA open source, les services du gouvernement français publient un comparateur d'ouverture de ce genre de modèles pour aider à s'y retrouver et à choisir son modèle en fonction des différents critères d'ouverture.
