---
site: cio-online.com
title: "Supply chain logicielle: explosion des composants open source malveillants en 2024"
author: Lucian Constantin
date: 2024-10-23
href: https://www.cio-online.com/actualites/lire-supply-chain-logicielle-explosion-des-composants-open-source-malveillants-en-2024-15930.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000021296.jpg
tags:
- Innovation
series:
- 202443
series_weight: 0
---

> L'écosystème open source voit bondir le nombre de composants logiciels malveillants. Un signal d'alarme pour la supply chain logicielle.
