---
site: Silicon
title: "Définir l'IA open source: avec l'AI Act, ça presse?"
author: Clément Bohic
date: 2024-10-17
href: https://www.silicon.fr/Thematique/data-ia-1372/Breves/Definir-l-IA-open-source-avec-l-AI-Act-a-464314.htm
featured_image: https://cdn.edi-static.fr/image/upload/c_lfill,h_245,w_470/e_unsharp_mask:100,q_auto/f_auto/v1/Img/BREVE/2024/10/464314/Definir-open-source-Act--LE.jpg
tags:
- Sciences
series:
- 202442
---

> La Fondation Linux réaffirme le besoin d'arrêter une définition de l'IA open source au vu des opportunités que présente l'AI Act. Quelles sont-elles, d'ailleurs?
