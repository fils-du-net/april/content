---
site: ouest-france.fr
title: "Les informations de Wikipédia sont-elles vérifiées et sûres? On vous répond"
author: Antoine Masson
date: 2024-10-28
href: https://www.ouest-france.fr/culture/les-informations-de-wikipedia-sont-elles-verifiees-et-sures-on-vous-repond-30e4c774-8fba-11ef-9d74-cdf49a297048
tags:
- Partage du savoir
series:
- 202444
series_weight: 0
---

> Créé en 2001, Wikipédia est aujourd’hui un élément incontournable dans la recherche d’informations pour de nombreuses personnes. Aujourd’hui, le site regroupe plus de 58 millions d’articles et est traduit en près de 300 langues différentes. Cependant, les informations données par Wikipédia sont-elles toutes fiables? On vous explique.
