---
site: Le Figaro
title: "L'intelligence artificielle et la puissance des logiciels open source"
date: 2024-10-09
href: https://www.lefigaro.fr/secteur/high-tech/l-intelligence-artificielle-et-la-puissance-des-logiciels-open-source-20241009
featured_image: https://i.f1g.fr/media/cms/704x396_cropupscale/2024/09/27/86b3f04cf710e9d92d82ca18955121d9e096ca5d415a11fec0962f6e72e38fcb.jpg
tags:
- Sciences
series:
- 202441
series_weight: 0
---

> Les investissements dans l'intelligence artificielle sont en plein essor. La dernière enquête mondiale de McKinsey menée à ce propos a montré que 65 % des organisations utilisent régulièrement l'IA générative. Ce pourcentage a presque doublé par rapport à la même étude réalisée dix mois plus tôt.
