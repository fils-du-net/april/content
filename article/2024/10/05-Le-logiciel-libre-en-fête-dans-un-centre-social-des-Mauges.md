---
site: ouest-france.fr
title: "Le logiciel libre en fête dans un centre social des Mauges"
date: 2024-10-05
href: https://www.ouest-france.fr/pays-de-la-loire/chemille-en-anjou-49120/le-logiciel-libre-en-fete-dans-un-centre-social-des-mauges-ee826f76-822b-11ef-aa60-e9ecd5d09830
tags:
- Associations
series:
- 202440
series_weight: 0
---

> Du jeudi 10 au samedi 19 octobre 2024, au centre social de Chemillé-en-Anjou (Maine-et-Loire), des bénévoles et intervenants se mettent en quatre pour vous expliquer et vous accompagner dans la découverte des logiciels libres, une alternative aux gros systèmes qui envahissent les ordinateurs.
