---
site: Le Monde.fr
title: "Guerre de contrôle autour de Wordpress, l'un des principaux outils de création de sites Web"
date: 2024-10-14
href: https://www.lemonde.fr/pixels/article/2024/10/14/guerre-de-controle-autour-de-wordpress-l-un-des-principaux-outils-de-creation-de-sites-web_6351593_4408996.html
featured_image: https://img.lemde.fr/2024/10/14/0/0/1200/631/800/0/75/0/d497c8a_1728897896845-wp.png
tags:
- Internet
series:
- 202442
---

> Le populaire logiciel de gestion de contenus est au cœur d’une guerre juridique entre deux grandes entreprises-clés de l’écosystème Wordpress.
