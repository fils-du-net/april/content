---
site: Numerama
title: "L'avenir open source de Bitwarden a connu une frayeur"
descirption: Plus de peur que de mal
author: Julien Lausson
date: 2024-10-21
href: https://www.numerama.com/cyberguerre/1829954-lavenir-open-source-de-bitwarden-a-connu-une-frayeur.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2022/10/a8fdf78e-1308-4ca8-86da-8f18ba40634e-1024x576.jpg?avif=1&key=74aee241
tags:
- Licenses
- Vie privée
series:
- 202443
series_weight: 0
---

> Figurant parmi les gestionnaires de mots de passe les plus connus, Bitwarden a aussi pour particularité de reposer sur la philosophie de l'open source (code source ouvert). Mais ces jours-ci, des éléments ont suscité l'inquiétude. Avant que le fondateur ne prenne la parole.
