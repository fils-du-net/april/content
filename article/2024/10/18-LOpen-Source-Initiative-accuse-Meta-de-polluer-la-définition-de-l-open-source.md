---
site: Next
title: "L'Open Source Initiative accuse Meta de polluer la définition de l'IA open source"
description: Meta ou l'ouverture quantique
author: Martin Clavey
date: 2024-10-18
href: https://next.ink/154521/lopen-source-initiative-accuse-meta-de-polluer-la-definition-de-lia-open-source
featured_image: https://next.ink/wp-content/uploads/2024/06/Openwashing.webp
tags:
- Sciences
- Licenses
series:
- 202442
series_weight: 0
---

> Le directeur général de l'Open Source Initiative (OSI), Stefano Maffulli, critique vertement l'utilisation par Meta du terme «open source» pour qualifier ses modèles d'IA générative Llama. L'OSI attaque Meta alors qu'elle finalise justement sa définition de ce terme employé pour qualifier les modèles d'intelligence artificielle.
