---
site: Next
title: "L'IA open-source a sa définition 1.0 qui exclut les modèles Llama de Meta"
description: "Quand Llama fâché, lui toujours faire ainsi"
author: Martin Clavey
date: 2024-10-29
href: https://next.ink/155920/lia-open-source-a-sa-definition-1-0-qui-exclut-les-modeles-llama-de-meta
featured_image: https://next.ink/wp-content/uploads/2024/06/Openwashing.webp
tags:
- Sciences
series:
- 202444
---

> L'Open Source Initiative (OSI) a publié ce lundi la version finale de sa définition de l'IA open-source. Celle-ci diffère encore un peu de la release candidate diffusée mi-octobre. Comme on pouvait s'en douter aux vues des tensions entre l'OSI et Meta, la définition exclut les modèles de l'entreprise, qui se revendique pourtant leader de l'IA open source.
