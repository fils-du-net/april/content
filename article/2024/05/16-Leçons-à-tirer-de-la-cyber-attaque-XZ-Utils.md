---
site: InformatiqueNews.fr
title: "Leçons à tirer de la cyber attaque XZ Utils..."
author: Thomas Segura
date: 2024-05-16
href: https://www.informatiquenews.fr/lecons-a-tirer-de-la-cyber-attaque-xz-utils-thomas-segura-gitguardian-99125
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2024/05/xz-utils-lecons.jpg
tags:
- Innovation
- Sensibilisation
series:
- 202420
series_weight: 0
---

> L'incident XZ Utils est une sonnette d'alarme qui rappelle l'importance de pratiques robustes de surveillance et d'audit des dépôts.
