---
site: ZDNET
title: "Comena, plateforme des communs et ressources numériques en Nouvel ..."
author: Thierry Noisette
date: 2024-05-10
href: https://www.zdnet.fr/blogs/l-esprit-libre/comena-plateforme-des-communs-et-ressources-numeriques-en-nouvelle-aquitaine-391535.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/05/comena-logo_02.jpg
tags:
- Partage du savoir
series:
- 202419
series_weight: 0
---

> Initié par Naos (Nouvelle-Aquitaine Open Source), le projet Comena vise à référencer des communs et des ressources pouvant «servir l'intérêt général et aider au développement du territoire de la Nouvelle-Aquitaine».
