---
site: ZDNET
title: "Bureautique et collaboration: la Dinum déroule La Suite Numérique"
author: Louis Adam
date: 2024-05-27
href: https://www.zdnet.fr/actualites/bureautique-et-collaboration-la-dinum-deroule-la-suite-numerique-392157.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/02/Visioconference-3-750x410.jpg
tags:
- Administration
series:
- 202422
series_weight: 0
---

> Derrière ce terme, la direction interministérielle du numérique regroupe une suite d'outils bureautique et de collaboration à destination des agents publics. Voici ce qu'il contient.
