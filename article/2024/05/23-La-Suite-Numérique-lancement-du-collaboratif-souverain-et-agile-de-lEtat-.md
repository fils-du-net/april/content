---
site: Républik IT Le Média
title: "La Suite Numérique: lancement du collaboratif souverain et agile de l'Etat "
author: Bertrand Lemaire
date: 2024-05-23
href: https://www.republik-it.fr/decideurs-it/cas-usage/la-suite-numerique-lancement-du-collaboratif-souverain-et-agile-de-l-etat.html
featured_image: https://img.republiknews.fr/crop/none/191851716e4c9539e5bccef6a5290603/0/0/1500/844/465/262/samuel-paccoud-responsable-pole-suite-numerique-dinum.jpg
tags:
- Administration
series:
- 202421
series_weight: 0
---

> En déplacement sur le stand de l’État sur VivaTech, Stanislas Guerini, ministre chargé de la Transformation et de la Fonction Publiques, a officiellement lancé La Suite Numérique qui équipera prochainement tous les agents publics.
