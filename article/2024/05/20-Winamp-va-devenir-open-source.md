---
site: Next
title: "Winamp va devenir open source"
author: Vincent Hermann
date: 2024-05-20
href: https://next.ink/brief_article/winamp-va-devenir-open-source
featured_image: https://next.ink/wp-content/uploads/2024/05/Sans-titre-1536x820.jpg
tags:
- Logiciels privateurs
series:
- 202421
---

> Winamp, c’est le symbole d’une époque pratiquement révolue, où l’on organisait des centaines, voire des milliers de MP3 (ou autres codecs sonores) pour se constituer une vaste médiathèque musicale. Avec l’arrivée de YouTube et des plateformes de streaming, l’usage a changé et Winamp a été relégué à l’arrière-plan.
