---
site: France Culture
title: "Éloge du bug: quand le dysfonctionnement nous aide à repenser le numérique"
date: 2024-05-10
href: https://www.radiofrance.fr/franceculture/podcasts/le-meilleur-des-mondes/eloge-du-bug-quand-le-dysfonctionnement-nous-aide-a-repenser-le-numerique-4522554
featured_image: https://www.radiofrance.fr/s3/cruiser-production-eu3/2024/05/ab3916b2-bd03-4fc6-98e2-bece3d8eb377/560x315_sc_gettyimages-1347880311.webp
tags:
- Sensibilisation
series:
- 202419
series_weight: 0
---

> Si les GAFAMs affirment rendre la vie de leurs utilisateurs plus aisée, ils participent aussi à l'invisibilisation de la partialité technologique, que le bug permet de lever. Dans son Éloge du bug, Marcello Vitali Rosati propose de repenser le numérique, à l'aide du dysfonctionnement.
