---
site: Siècle Digital
title: "Comment Mistral AI part à la conquête de l'Amérique"
author: Benjamin Terrasson
date: 2024-05-30
href: https://siecledigital.fr/2024/05/30/comment-mistral-ai-part-a-la-conquete-de-lamerique
featured_image: https://siecledigital.fr/wp-content/uploads/2024/05/mistral-ai-1400x960.jpg
tags:
- Sciences
series:
- 202422
series_weight: 0
---

> Mistral AI commence a étoffé ses équipes aux États-Unis pour s'imposer sur place en alternative aux IA des géants du web.
