---
site: Les Echos
title: "Pour IBM, le logiciel libre est l'avenir de l'IA en entreprise (€)"
author: Benoît Georges
date: 2024-05-21
href: https://www.lesechos.fr/tech-medias/intelligence-artificielle/pour-ibm-le-logiciel-libre-est-lavenir-de-lia-en-entreprise-2096029
featured_image: https://media.lesechos.com/api/v1/images/view/664cb0198a60a97e5f39236a/1024x576-webp/01101218954825-web-tete.webp
tags:
- Sciences
series:
- 202421
---

> En ouverture de sa conférence Think, le géant des services informatiques a annoncé la mise à disposition de ses derniers modèles d'IA sous licence « open source » et un partenariat avec la start-up française Mistral.
