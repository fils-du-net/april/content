---
site: InformatiqueNews.fr
title: "De l'avenir de l'open source..."
author: Cédric Gegout
date: 2024-05-07
href: https://www.informatiquenews.fr/comment-preserver-les-fondamentaux-de-lopen-source-dans-les-annees-a-venir-cedric-gegout-canonical-99007
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2024/05/open-source-futur-shutterstock_531126358.jpg
tags:
- Sensibilisation
- Innovation
- Brevets logiciels
series:
- 202419
series_weight: 0
---

> Quel avenir pour un open source aujourd'hui menacé par le détournement de ses principes fondateurs par des acteurs majeurs de l'industrie.
