---
site: cio-online.com
title: "10 points à surveiller avec l'IA générative open source"
author: Maria Korolov
date: 2024-05-20
href: https://www.cio-online.com/actualites/lire-10-points-a-surveiller-avec-l-ia-generative-open-source-15653.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000020945.jpg
tags:
- Sciences
series:
- 202421
series_weight: 0
---

> Les modèles d'IA générative open source peuvent être téléchargés librement, utilisés à grande échelle sans coûts d'appels d'API et exécutés en toute sécurité derrière les pare-feu des entreprises. Mais ne baissez pas la garde. Car des risques sont bel et bien présents.
