---
site: Journal du Net
title: "Open source: le mentorat pour faire une place aux contributions occasionnelles"
author: Melanie Plageman
date: 2024-05-13
href: https://www.journaldunet.com/developpeur/1530269-open-source-le-mentorat-pour-faire-une-place-aux-contributions-occasionnelles
tags:
- Sensibilisation
series:
- 202420
series_weight: 0
---

> La gouvernance des projets de développement doit prendre en compte les contributions occasionnelles. Une démarche de mentorat permet de prendre de la distance avec la simple résolution de problèmes.
