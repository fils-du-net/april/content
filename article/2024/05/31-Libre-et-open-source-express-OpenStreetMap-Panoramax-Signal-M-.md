---
site: ZDNET
title: "Libre et open source express: OpenStreetMap, Panoramax, Signal, Murena, montorat"
author: Thierry Noisette
date: 2024-05-31
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-openstreetmap-panoramax-signal-murena-mentorat-392390.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/05/open-source_panneau.jpg
tags:
- Sensibilisation
series:
- 202422
series_weight: 0
---

> Journées OpenStreetMap. La FAQ de Panoramax. Meredith Whittaker (Signal) et la concentration des IA. Gaël Duval et les smartphones Murena. Plaidoyer pour le mentorat des contributeurs occasionnels.
