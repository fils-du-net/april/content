---
site: Numerama
title: "Quels sont les logiciels libres que l'État conseille en 2024?"
description: Le cap des 500 logiciels approche
author: Julien Lausson
date: 2024-05-24
href: https://www.numerama.com/tech/1748522-quels-sont-les-logiciels-libres-que-letat-conseille-en-2024.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2019/07/logo-ubuntu-1024x576.jpg?avif=1&key=b7f9f81b
tags:
- Administration
- Référentiel
series:
- 202421
series_weight: 0
---

> Les administrations publiques ont désormais accès à une liste de 473 logiciels libres recommandés par les services de l'État. On trouve des outils populaires comme LibreOffice, Firefox, 7zip, Gimp ou Debian, mais aussi des solutions plus confidentielles, comme mpv ou AgentJ, Scilab ou mpv. C'était il y a un peu plus
