---
site: Le Monde.fr
title: "Loi SREN: le Conseil constitutionnel valide l'essentiel, mais censure le délit d'outrage en ligne (€)"
author: Florian Reynaud, Aurélien Defer
date: 2024-05-17
href: https://www.lemonde.fr/pixels/article/2024/05/17/loi-sren-le-conseil-constitutionnel-valide-l-essentiel-mais-censure-le-delit-d-outrage-en-ligne_6233904_4408996.html
featured_image: https://img.lemde.fr/2023/11/13/0/0/5184/3456/800/0/75/0/ee6fbd9_1699878535418-pns-3264202.jpg
tags:
- Institutions
- Internet
series:
- 202421
---

> Au total, cinq articles du projet de loi visant à sécuriser et réguler l’espace numérique ont été censurés par les membres du Conseil, la plupart ayant été jugés sans lien avec le texte initial.
