---
site: L'OBS
title: "Projet de loi numérique: le délit d'outrage en ligne censuré par le Conseil constitutionnel"
date: 2024-05-17
href: https://www.nouvelobs.com/politique/20240517.OBS88538/le-delit-d-outrage-en-ligne-censure-par-le-conseil-constitutionnel.html
featured_image: https://focus.nouvelobs.com/2024/05/17/2/0/5307/3538/1020/680/50/0/ab33bf1_1715970560362-075-savoranineri-francepa240419-npqmy.jpg
tags:
- Institutions
- Internet
series:
- 202421
series_weight: 0
---

> Les membres du Conseil constitutionnel ont également censuré quatre autres articles considérés comme étant des «cavaliers législatifs», c'est-à-dire sans rapport direct ou indirect avec le texte initial.
