---
site: BFMtv
title: "Des joueurs de Pokémon Go inventent des plages pour attirer des pokémon en ville"
author: Salomé Ferraris
date: 2024-05-03
href: https://www.bfmtv.com/tech/gaming/des-joueurs-de-pokemon-go-inventent-des-plages-pour-attirer-des-pokemon-en-ville_AV-202405030331.html
tags:
- Partage du savoir
series:
- 202419
series_weight: 0
---

> Pour faciliter la capture de Pokémon rares, certains utilisateurs faussent les données d'Openstreetmap pour faire apparaître des plages plus accessibles.
