---
site: Les Echos
title: "Comment créer une communauté puissante et développer son business"
author: Noémie Kempf
date: 2024-05-28
href: https://entrepreneurs.lesechos.fr/developpement-entreprise/commercial-marketing/comment-creer-une-communaute-puissante-et-developper-son-business-2097548
featured_image: https://media.lesechos.com/api/v1/images/view/6655c7614ed6c33e5a6afeda/1024x576-webp/01101088925160-web-tete.webp
tags:
- Sensibilisation
series:
- 202422
series_weight: 0
---

> A partir de l'exemple de Linux, Noémie Kempf, autrice du «Pouvoir des communautés»*, explique comment l'approche communautaire peut favoriser le développement de votre entreprise.
