---
site: ZDNET
title: "Non, Winamp n'est pas en train de devenir un logiciel libre"
author: Steven Vaughan-Nichols
date: 2024-05-21
href: https://www.zdnet.fr/actualites/non-winamp-nest-pas-en-train-de-devenir-un-logiciel-libre-391886.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/05/winamp.jpg
tags:
- Logiciels privateurs
series:
- 202421
series_weight: 0
---

> Pour rafraîchir son lecteur multimédia Windows autrefois emblématique, Winamp ouvre son code source. Découvrez ce que cela signifie vraiment pour les utilisateurs et les développeurs.
