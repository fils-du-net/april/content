---
site: Next
title: "«Dans les algorithmes», nouvelle newsletter sur l’impact social de la tech"
author: Martin Clavey
date: 2024-05-13
href: https://next.ink/brief_article/dans-les-algorithmes-nouvelle-newsletter-sur-limpact-social-de-la-tech
tags:
- Sensibilisation
- Partage du savoir
series:
- 202420
---

> Une nouvelle newsletter parlant du numérique en français vient d'être lancée sous le nom de «Dans les algorithmes».
