---
site: Nouvelle République
title: "Au Blanc, des ateliers informatiques sur les logiciels libres pour être plus indépendant (€)"
author: Martine Tissier
date: 2024-05-19
href: https://www.lanouvellerepublique.fr/le-blanc/au-blanc-des-ateliers-informatiques-sur-les-logiciels-libres-pour-etre-plus-independant
featured_image: https://images.lanouvellerepublique.fr/image/upload/t_1020w/f_auto/664a12dd50de032f1f8b458b.jpg
tags:
- Associations
series:
- 202420
series_weight: 0
---

> Au Repar’Lab du Blanc, Bruno Gaudinat assure des ateliers informatiques dédiés aux logiciels libres, baptisés ateliers «libres». L’objectif est de faire découvrir aux participants d’autres pratiques sur ordinateur.
