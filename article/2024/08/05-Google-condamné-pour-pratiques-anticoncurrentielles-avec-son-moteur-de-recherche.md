---
site: Le Monde.fr
title: "Google condamné pour pratiques anticoncurrentielles avec son moteur de recherche"
date: 2024-08-05
href: https://www.lemonde.fr/pixels/article/2024/08/05/google-condamne-pour-pratiques-anticoncurrentielles-avec-son-moteur-de-recherche_6269129_4408996.html
tags:
- Entreprise
- Institutions
- International
series:
- 202432
series_weight: 0
---

> Le géant américain, reconnu coupable d’avoir imposé, par défaut, son logiciel de recherche sur des appareils, a annoncé sa volonté de faire appel.
