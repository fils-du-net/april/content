---
site: ouest-france.fr
title: "Quimper. La distribution d’ordinateurs continue tout l’été au Centre des Abeilles"
date: 2024-08-07
href: https://www.ouest-france.fr/bretagne/quimper-29000/quimper-une-distribution-dordinateurs-au-centre-des-abeilles-f7a3cde8-53cd-11ef-ac40-8f9eb1399b54
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyNDA4MWMyZmNmMWFiNWU1MGQ1ZGRlNzM5ZGRlZGFhOGJhNTk?width=940&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=a9c45117139d1dec61c5a6c80db39e4ecfd46861d2bda3fbda2df4b5cbddcbc4
tags:
- Associations
series:
- 202432
series_weight: 0
---

> Pas de pause en août 2024 pour les bénévoles du Centre des Abeilles et de Linux Quimper. La distribution d’ordinateurs de bureau sous Linux continue!
