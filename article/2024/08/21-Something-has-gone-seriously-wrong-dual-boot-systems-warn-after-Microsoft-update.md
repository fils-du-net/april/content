---
site: Ars Technica
title: "“Something has gone seriously wrong,” dual-boot systems warn after Microsoft update"
author: Dan Goodin
date: 2024-08-21
href: https://arstechnica.com/security/2024/08/a-patch-microsoft-spent-2-years-preparing-is-making-a-mess-for-some-linux-users
featured_image: https://cdn.arstechnica.net/wp-content/uploads/2024/08/power-on-button-800x451.jpg
tags:
- Logiciels privateurs
- English
series:
- 202434
series_weight: 0
---

> Microsoft said its update wouldn't install on Linux devices. It did anyway.
