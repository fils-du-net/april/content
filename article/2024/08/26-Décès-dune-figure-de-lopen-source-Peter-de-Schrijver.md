---
site: Goodtech
title: "Décès d'une figure de l'open source, Peter de Schrijver"
date: 2024-08-26
href: https://goodtech.info/deces-dune-figure-de-lopen-source-peter-de-schrijver/
featured_image: https://i0.wp.com/goodtech.info/wp-content/uploads/2024/08/images.jpg
tags:
- Sensibilisation
series:
- 202435
series_weight: 0
---

> La communauté du logiciel libre a perdu l’un des siens cet été. Peter de Schrijver est décédé dans un hôpital à Helsinki le 12 juillet. De Schrijver était un développeur Debian et un contributeur au noyau Linux.
