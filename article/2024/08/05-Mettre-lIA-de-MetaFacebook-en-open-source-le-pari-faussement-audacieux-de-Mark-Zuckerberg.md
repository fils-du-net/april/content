---
site: atlantico
title: "Mettre l'IA de Meta/Facebook en open source: le pari (faussement) audacieux de Mark Zuckerberg"
author: Atlantico
date: 2024-08-05
href: https://atlantico.fr/article/rdv/mettre-lia-de-meta-facebook-en-open-source-le-pari-faussement-audacieux-de-mark-zuckerberg-fabrice-epelboin
featured_image: https://atlantico.codexcdn.net/assets/asuCDYcjzOLNrXpLI.jpg
tags:
- Sciences
series:
- 202432
series_weight: 0
---

> Le 23 juillet, Mark Zuckerberg a publié un manifeste exposant les arguments commerciaux en faveur de l'IA open source.
