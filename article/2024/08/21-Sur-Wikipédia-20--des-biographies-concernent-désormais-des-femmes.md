---
site: Next
title: "Sur Wikipédia, 20 % des biographies concernent désormais des femmes"
author: Mathilde Saliou
date: 2024-08-21
href: https://next.ink/147182/sur-wikipedia-20-des-biographies-concernent-desormais-des-femmes
featured_image: https://next.ink/wp-content/uploads/2024/03/wiki.webp
tags:
- Partage du savoir
series:
- 202434
series_weight: 0
---

> Le Wikipédia francophone passe la barre des 20% de biographies consacrées à des femmes, notamment grâce au travail d'associations.
