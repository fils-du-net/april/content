---
site: Le Café du Geek
title: "Mais au fait, que signifie IA Open Source?"
author: Christiane
date: 2024-08-12
href: https://lecafedugeek.fr/mais-au-fait-que-signifie-ia-open-source
featured_image: https://lecafedugeek.fr/wp-content/uploads/2024/08/ia-open-source-780x470.jpg
tags:
- Sciences
series:
- 202434
series_weight: 0
---

> Vous avez probablement entendu parler de certaines technologies d'IA en Open Source. Mais qu'est-ce que c'est?
