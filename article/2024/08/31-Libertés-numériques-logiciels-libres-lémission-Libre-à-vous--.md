---
site: ZDNET
title: "Libertés numériques, logiciels libres: l'émission Libre à vous! fait sa rentrée"
author: Thierry Noisette
date: 2024-08-31
href: https://www.zdnet.fr/blogs/l-esprit-libre/libertes-numeriques-logiciels-libres-lemission-libre-a-vous-fait-sa-rentree-396273.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/08/libre-a-vous_cause-commune-750x410.jpg
tags:
- april
- Promotion
series:
- 202435
series_weight: 0
---

> L'émission de l'April sur la radio Cause commune commencera mardi sa 8e saison, et elle accueillera deux nouvelles chroniques.
