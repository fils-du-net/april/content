---
site: Solutions-Numeriques
title: "L’Open Source évolue: comment en préserver les fondements"
author: Cédric Gegout
date: 2024-08-27
href: https://www.solutions-numeriques.com/communiques/lopen-source-evolue-comment-en-preserver-les-fondements
featured_image: https://www.solutions-numeriques.com/wp-content/uploads/2024/08/cedric-gegout-300x300.jpg
tags:
- Sensibilisation
series:
- 202435
series_weight: 0
---

> Le concept de l’open source existe depuis des décennies, et il est actuellement en plein essor avec des  stratégies de commercialisation des solutions open source en constante évolution et de plus en plus efficaces. Sur les vingt dernières années, il est incontestable que l’open source a été clé dans le développement des logiciels toutes catégories confondues. L’open source n’est d’ailleurs plus seulement logiciel, mais a conquis les mondes du hardware et du design.
