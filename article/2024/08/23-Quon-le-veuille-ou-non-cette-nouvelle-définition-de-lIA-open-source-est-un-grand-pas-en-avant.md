---
site: ZDNET
title: "Qu'on le veuille ou non, cette nouvelle définition de l'IA open source est un grand pas en avant"
author: Steven Vaughan-Nichols
date: 2024-08-23
href: https://www.zdnet.fr/actualites/quon-le-veuille-ou-non-cette-nouvelle-definition-de-lia-open-source-est-un-grand-pas-en-avant-395967.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/05/open-source_panneau.jpg
tags:
- Sciences
series:
- 202434
---

> La dernière version du projet de l'Open Source Initiative est disponible. Évidemment les débats se poursuivent!
