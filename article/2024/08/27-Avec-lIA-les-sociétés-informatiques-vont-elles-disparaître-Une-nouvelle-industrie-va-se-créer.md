---
site: La Libre.be
title: "Avec l'IA, les sociétés informatiques vont-elles disparaître? 'Une nouvelle industrie va se créer' (€)"
author: Marie Debauche
date: 2024-08-27
href: https://www.lalibre.be/economie/digital/2024/08/27/avec-lia-les-societes-informatiques-vont-elles-disparaitre-une-nouvelle-industrie-va-se-creer-CYFQ274GLBBMRI74XTYTMD7QPE/
featured_image: https://www.lalibre.be/resizer/v2/G34W37DNBZA63HB3YYDHQMHB5Y.jpg?auth=e86d3e2adc60c6a95c90d3fbebb454a7cdd16cdacc5e36d12ed916fbc9b46633&width=1200&height=800&quality=85&focal=384%2C255
tags:
- Sciences
series:
- 202435
---

> Dans son travail pour Easi, Philippe Poupeleer est invité à encadrer des entreprises dans leur utilisation d'outils comme Copilot, l'IA générative de Microsoft.
