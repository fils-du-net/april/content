---
site: Next
title: "Vers des critères plus clairs pour l'«IA ouverte»?"
description: "Une ouverture fermée, ou une fermeture ouverte?"
author: Martin Clavey
date: 2024-08-29
href: https://next.ink/147650/vers-des-criteres-plus-clairs-pour-l-ia-ouverte
featured_image: https://next.ink/wp-content/uploads/2024/08/Ia-ouverte.webp
tags:
- Sciences
series:
- 202435
series_weight: 0
---

> Alors que les entreprises du secteur annoncent régulièrement la sortie de modèles «ouverts» le besoin d'une définition claire du terme est de plus en plus présent. L'Open Source Initiative a proposé la semaine dernière son dernier brouillon de définition.
