---
site: Acteurs Publics 
title: "Le projet de “Suite numérique” de l'État ne fait pas l'unanimité (€)"
author: Emile Marzolf
date: 2024-08-27
href: https://acteurspublics.fr/articles/le-projet-de-suite-numerique-de-letat-ne-fait-pas-lunanimite
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/50/454193ba48d1a9c283a0bca84f95e1d29afb6555.png
tags:
- Administration
series:
- 202435
series_weight: 0
---

> Après la Cour des comptes, c’est au tour du “Conseil national du logiciel libre” de s’en prendre au projet de suite d’outils numériques de l’agent public préparé par la direction du numérique de l’État, qu’il accuse de "jouer contre son camp" en faisant concurrence aux entreprises françaises.
