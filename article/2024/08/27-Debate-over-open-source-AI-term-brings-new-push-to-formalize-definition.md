---
site: Ars Technica
title: "Debate over “open source AI” term brings new push to formalize definition"
author: Benj Edwards
date: 2024-08-27
href: https://arstechnica.com/information-technology/2024/08/debate-over-open-source-ai-term-brings-new-push-to-formalize-definition/
featured_image: https://cdn.arstechnica.net/wp-content/uploads/2024/08/transparency_man-800x450.jpg
tags:
- Sciences
- Licenses
series:
- 202435
---

> Restrictive AI model licenses claimed as "open source" spark for clear standard.
