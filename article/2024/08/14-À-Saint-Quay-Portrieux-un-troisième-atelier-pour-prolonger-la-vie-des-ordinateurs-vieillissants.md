---
site: ouest-france.fr
title: "À Saint-Quay-Portrieux, un troisième atelier pour prolonger la vie des ordinateurs vieillissants"
date: 2024-08-14
href: https://www.ouest-france.fr/bretagne/saint-quay-portrieux-22410/a-saint-quay-portrieux-un-troisieme-atelier-pour-prolonger-la-vie-des-ordinateurs-vieillissants-d12ce8a4-5a24-11ef-98f9-a9eedbe59111
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyNDA4ZmVlNDk4M2Q1NzFlZDQ1NDA5NmEwOGFkMWIwNzFlNWM
tags:
- Associations
series:
- 202434
series_weight: 0
---

> La commune de Saint-Quay-Portrieux (Côtes-d’Armor) organise la troisième édition de l’Install Party, samedi 17 août 2024. Le but de cet atelier: revitaliser ou optimiser des ordinateurs aux systèmes d’exploitation vieillissants.
