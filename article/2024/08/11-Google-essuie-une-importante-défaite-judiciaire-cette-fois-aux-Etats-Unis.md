---
site: Le Monde.fr
title: "Google essuie une importante défaite judiciaire, cette fois aux Etats-Unis (€)"
author: Vincent Fagot
date: 2024-08-11
href: https://www.lemonde.fr/economie/article/2024/08/06/google-essuie-une-importante-defaite-judiciaire-cette-fois-aux-etats-unis_6269650_3234.html
tags:
- Entreprise
series:
- 202432
---

> Le géant américain du numérique a été reconnu coupable, lundi 5 août, par un juge de Washington, de pratiques anticoncurrentielles concernant son moteur de recherche.
