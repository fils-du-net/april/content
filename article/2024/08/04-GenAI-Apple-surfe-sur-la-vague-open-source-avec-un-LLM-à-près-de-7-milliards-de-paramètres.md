---
site: www.usine-digitale.fr
title: "GenAI: Apple surfe sur la vague open source avec un LLM à près de 7 milliards de paramètres"
author: Célia Séramour
date: 2024-08-04
href: https://www.usine-digitale.fr/article/genai-apple-surfe-sur-la-vague-open-source-avec-un-llm-a-7-milliards-de-parametres.N2216500
featured_image: https://www.usine-digitale.fr/mediatheque/7/8/0/001357087_896x598_c.jpg
tags:
- Sciences
series:
- 202432
---

> Avec pour objectif de démontrer l'importance des jeux de données et du travail de curation de ces dernières, Apple pousse une famille de LLM relativement compacts et totalement open source. Son modèle de fondation à 6,9 milliards de paramètres est notamment compétitif face à Mistral 7B et Llama 3.
