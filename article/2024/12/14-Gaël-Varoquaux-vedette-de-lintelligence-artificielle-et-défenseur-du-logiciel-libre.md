---
site: Le Monde.fr
title: "Gaël Varoquaux, vedette de l'intelligence artificielle et défenseur du logiciel libre (€)"
date: 2024-12-14
href: https://www.lemonde.fr/sciences/article/2024/12/14/gael-varoquaux-vedette-de-l-intelligence-artificielle-et-defenseur-du-logiciel-libre_6448689_1650684.html
featured_image: https://img.lemde.fr/2024/12/11/0/0/3840/2560/800/0/75/0/29549bd_1733934120322-inria-0429-007.jpg
tags:
- Sciences
series:
- 202451
series_weight: 0
---

> L'informaticien et chercheur à l'Inria est l'expert français le plus cité dans les publications scientifiques portant sur l'IA. Avec Scikit-learn, un programme de machine learning dont il est le cocréateur, il a permis à des millions d'utilisateurs de faire «parler» plus facilement les données.
