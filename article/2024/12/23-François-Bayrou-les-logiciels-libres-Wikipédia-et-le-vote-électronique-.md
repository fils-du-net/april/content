---
site: ZDNET
title: "François Bayrou, les logiciels libres, Wikipédia et le vote électronique"
author: Thierry Noisette
date: 2024-12-23
href: https://www.zdnet.fr/blogs/l-esprit-libre/francois-bayrou-les-logiciels-libres-wikipedia-et-le-vote-electronique-403501.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/02/GestionDocumentaire-365x200.jpg
tags:
- Institutions
- Vote électronique
series:
- 202501
series_weight: 0
---

> On sort les archives: quand l'actuel Premier ministre vantait le logiciel libre et Wikipédia, et s'opposait au vote électronique avant de le soutenir 13 ans plus tard.
