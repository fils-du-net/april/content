---
site: LaDepeche.fr
title: "Boé. Le niveau 5 du label Territoire Numérique Libre confirmé"
date: 2024-12-21
href: https://www.ladepeche.fr/2024/12/21/le-niveau-5-du-label-territoire-numerique-libre-confirme-12406352.php
featured_image: https://images.ladepeche.fr/api/v1/images/view/676643521f590a56ca1f0ae7/large/image.jpg?v=1
tags:
- Promotion
- Administration
series:
- 202451
series_weight: 0
---

> Boé a une nouvelle fois reçu le niveau 5 du label Territoire Numérique Libre à l’occasion de la remise des prix organisée à Paris le mercredi 4 décembre dernier. Cette distinction, la plus haute décernée par l’Association des développeurs et utilisateurs de logiciels libres pour les administrations et les collectivités territoriales (ADULLACT) a été remise à Françoise Lebeau, adjointe déléguée à l’administration générale, personnel et développement numérique et Fatima Houdaïbi, responsable des services Informatiques. C’est est une reconnaissance à l’échelle nationale du niveau très élevé de la ville de Boé dans le Libre, c’est-à-dire dans l’usage de logiciels n’étant pas sous licence donc libres de droits.
