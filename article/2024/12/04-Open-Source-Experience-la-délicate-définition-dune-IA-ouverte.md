---
site: Le Monde Informatique
title: "Open Source Experience: la délicate définition d'une IA ouverte"
author: Jacques Cheminat
date: 2024-12-04
href: https://www.lemondeinformatique.fr/actualites/lire-open-source-experience-la-delicate-definition-d-une-ia-ouverte-95439.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000100060.jpg
tags:
- Sciences
series:
- 202449
series_weight: 0
---

> Le salon Open Source Expérience vient de s'ouvrir à Paris autour de plusieurs sujets dont l'IA. La récente proposition de définition d'une IA open source par l'OSI a été l'occasion d'un débat entre plusieurs spécialistes.
