---
site: Les Petites Affiches
title: "OpenStreetMap, la carto NO-LIMIT (€)"
author: Leslie Taupenas
date: 2024-12-30
href: https://www.petitesaffiches64.com/toutes-les-actualites-64-bayonne-pays-basque-pau-bearn/openstreetmap-la-carto-no-limit
featured_image: https://www.petitesaffiches64.com/fileadmin/_processed_/6/d/csm_OpenStreetMap_c_seignanx.com_1f7964ecff.jpg
tags:
- Partage du savoir
series:
- 202504
series_weight: 0
---

> OpenStreetMap, ou OSM, est un projet collaboratif au service de tous, utilisé par un nombre croissant de personnes et acteurs à travers le monde. Cette gigantesque base de données cartographique contributive en open source (données ouvertes) a largement de quoi taquiner l’omnipotent Google Maps.
