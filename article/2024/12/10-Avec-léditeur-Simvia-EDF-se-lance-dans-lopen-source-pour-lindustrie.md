---
site: ZDNET
title: "Avec l'éditeur Simvia, EDF se lance dans l'open source pour l'industrie"
author: Christophe Auffray
date: 2024-12-10
href: https://www.zdnet.fr/actualites/avec-lediteur-simvia-edf-se-lance-dans-lopen-source-pour-lindustrie-402780.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/03/open-source-code-620__w1200-750x410.jpg
tags:
- Entreprise
- Innovation
series:
- 202451
series_weight: 0
---

> EDF crée Simvia, un éditeur de logiciel spécialisé dans l'open source pour le calcul et la simulation en ingénierie industrielle.
