---
site: ouest-france.fr
title: "Pour Noël, cette association de Quimper propose des ordinateurs reconditionnés sous Linux"
date: 2024-12-05
href: https://www.ouest-france.fr/bretagne/quimper-29000/pour-noel-cette-association-de-quimper-propose-des-ordinateurs-reconditionnes-sous-linux-4746da16-b19d-11ef-bea7-257efe97c77c
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyNDEyMjU4NTg4MTRkZDQxODU3N2E5MjgzN2YxYzQ5OGZlMGI?width=630&height=354&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=da4445420d1ac695b339f616e47bfe30b34aa2dc4cb2edf55d02b4d916220c12
tags:
- Associations
series:
- 202449
series_weight: 0
---

> À l'approche des fêtes de fin d'année, le centre des Abeilles et Linux Quimper (Finistère) proposent une solution engagée et responsable pour l'acquisition d'un ordinateur. Des machines reconditionnées sous Linux, accessibles pour une simple adhésion de 13 € au centre social.
