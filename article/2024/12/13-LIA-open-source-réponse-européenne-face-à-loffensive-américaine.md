---
site: Les Echos
title: "L'IA open source, réponse européenne face à l'offensive américaine (€)"
date: 2024-12-13
href: https://www.lesechos.fr/idees-debats/cercle/opinion-lia-open-source-reponse-europeenne-face-a-loffensive-americaine-2137810
featured_image: https://media.lesechos.com/api/v1/images/view/675c6f2554453d7cec13009a/1024x576-webp/01301868512404-web-tete.webp
tags:
- Sciences
- Europe
series:
- 202451
series_weight: 0
---

> Si elle sort du schéma classique où, tandis que l'Amérique innove et que la Chine imite, elle réglemente, l'Europe peut faire de gros progrès en IA. La solution est de favoriser l'IA open source, selon Alexandre Orhan, de Sia Partners.
