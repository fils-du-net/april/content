---
site: Next
title: "Entre fermer et partager le code, est-ce «fair»?"
author: Martin Clavey
date: 2024-09-24
href: https://next.ink/151204/entre-fermer-et-partager-le-code-est-ce-fair/
featured_image: https://next.ink/wp-content/uploads/2024/01/shahadat-rahman-BfrQnKBulYQ-unsplash.jpg
tags:
- Licenses
- april
series:
- 202439
series_weight: 0
---

> Dans le domaine du logiciel libre, la licence d'ouverture du code peut rapidement faire débat. Rejetées par ce milieu qui considère que leurs logiciels ne sont pas assez libres pour être qualifiés d'«open», des startups comme Sentry poussent un nouveau terme: «fair source».
