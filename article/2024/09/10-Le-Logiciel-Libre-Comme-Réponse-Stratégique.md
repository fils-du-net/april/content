---
site: ITRmobiles.com
title: "Le logiciel libre comme réponse stratégique à l’appel du rapport Draghi pour la compétitivité européenne"
date: 2024-09-10
href: https://itrmobiles.com/articles/203538/le-logiciel-libre-comme-reponse-strategique-a-lappel-du-rapport-draghi-pour-la-competitivite-europeenne.html
tags:
- Europe
series:
- 202437
series_weight: 0
---

> Le rapport Draghi, publié le 9 septembre 2024 et intitulé “L’avenir de la compétitivité européenne”, appelle à une transformation profonde de l’économie européenne pour relever des défis critiques: souveraineté numérique, stagnation de l’innovation, et dépendances stratégiques. En tant que porte-parole de la filière française du logiciel libre et du numérique ouvert, le CNLL* affirme que le logiciel libre (aussi appelé open source software ou OSS) est essentiel pour résoudre ces enjeux, en catalysant l’innovation, la croissance économique, et l’autonomie stratégique de l’Europe, explique Stefane Fermigier, co-chairman, qui nous livre son analyse.
