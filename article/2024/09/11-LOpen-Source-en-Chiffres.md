---
site: InformatiqueNews.fr
title: "L'Open Source en Chiffres"
date: 2024-09-11
href: https://www.informatiquenews.fr/open-source-en-chiffres-100850
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2024/09/open-source-en-chiffres.jpg
tags:
- Promotion
series:
- 202437
series_weight: 0
---

> De l'open source et des chiffres... Beaucoup de chiffres... Le nouveau rapport GitNux 2024 trace un paysage IT dominé par l'open source.
