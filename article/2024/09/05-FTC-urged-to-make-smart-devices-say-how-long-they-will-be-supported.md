---
site: Ars Technica
title: "FTC urged to make smart devices say how long they will be supported"
author: Scharon Harding
date: 2024-09-05
href: https://arstechnica.com/gadgets/2024/09/ftc-urged-to-make-smart-devices-say-how-long-theyll-be-supported
featured_image: https://cdn.arstechnica.net/wp-content/uploads/2022/07/car-thing-800x600.jpg
tags:
- Matériel libre
- English
series:
- 202436
series_weight: 0
---

> Sudden subscription fees, lost features causing users "death by a thousand cuts."
