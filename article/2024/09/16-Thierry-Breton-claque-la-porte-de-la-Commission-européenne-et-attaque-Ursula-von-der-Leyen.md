---
site: Numerama
title: "Thierry Breton claque la porte de la Commission européenne, et attaque Ursula von der Leyen"
description: "Retour à Paris?"
author: Julien Lausson
date: 2024-09-16
href: https://www.numerama.com/politique/1807996-thierry-breton-demissionne-de-la-commission-europeenne.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2022/04/thierry-breton-1024x576.jpg
tags:
- Institutions
- Europe
series:
- 202438
---

> Le Français Thierry Breton a annoncé à la surprise générale sa démission de la Commission européenne. Il avait pourtant été choisi par la France pour un autre mandat à Bruxelles.
