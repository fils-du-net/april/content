---
site: clubic.com
title: "La secrétaire d'État à l'IA et au Numérique est à la dernière place du gouvernement, un nouvel affront à la Tech"
author: Alexandre Boero
date: 2024-09-22
href: https://www.clubic.com/actualite-538199-la-secretaire-d-etat-a-l-ia-et-au-numerique-est-a-la-derniere-place-du-gouvernement-un-nouvel-affront-a-la-tech.html
featured_image: https://pic.clubic.com/c0c921352243066/1200x901/smart/clara-chappaz-ici-au-web-summit-2023.jpg
tags:
- Institutions
series:
- 202438
series_weight: 0
---

> Pour la première fois en France, l'intelligence artificielle est représentée au sein du gouvernement, avec le Numérique. Sauf qu'une fois de plus, les disciplines de la Tech n'ont pas droit à leur ministère.
