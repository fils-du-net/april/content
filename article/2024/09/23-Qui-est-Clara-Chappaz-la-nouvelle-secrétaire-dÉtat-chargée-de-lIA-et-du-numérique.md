---
site: Numerama
title: "Qui est Clara Chappaz, la nouvelle secrétaire d'État chargée de l'IA et du numérique?"
description: "Secrétaire d'État à l'IA"
author: Julien Lausson
date: 2024-09-23
href: https://www.numerama.com/politique/1812782-qui-est-clara-chappaz-la-nouvelle-secretaire-detat-chargee-de-lia-et-du-numerique.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2024/09/clara-chappaz-une-1024x576.jpg
tags:
- Institutions
series:
- 202439
series_weight: 0
---

> Âgée de 35 ans, Clara Chappaz est le nouveau visage de la politique du numérique en France. Elle prend possession du secrétariat d'État en charge du numérique, au sein du gouvernement de Michel Barnier. L'intelligence artificielle (IA) est particulièrement mise à l'honneur. Mais ce n'est pas la seule chose à noter.
