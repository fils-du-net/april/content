---
site: LeMagIT
title: "Open Source Summit: Proxmox se dévoile aux clients de VMware (€)"
author: Yann Serra
date: 2024-09-19
href: https://www.lemagit.fr/actualites/366611314/Open-Source-Summit-Proxmox-se-devoile-aux-clients-de-VMware
featured_image: https://cdn.ttgtmedia.com/visuals/LeMagIT/hero_article/Hero-ProxmoxOSS2024.jpg
tags:
- Logiciels privateurs
series:
- 202438
series_weight: 0
---

> Le petit éditeur autrichien a vu le nombre d’entreprises intéressées par sa solution de virtualisation exploser depuis que Broadcom a changé les conditions tarifaires de VMware, le leader du domaine.
