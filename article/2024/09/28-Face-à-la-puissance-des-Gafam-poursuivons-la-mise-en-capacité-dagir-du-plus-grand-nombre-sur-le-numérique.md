---
site: Le Monde.fr
title: "«Face à la puissance des Gafam, poursuivons la mise en capacité d'agir du plus grand nombre sur le numérique» (€)"
author: Charles de Laubier
date: 2024-09-28
href: https://www.lemonde.fr/economie/article/2024/09/24/face-a-la-puissance-des-gafam-poursuivons-la-mise-en-capacite-d-agir-du-plus-grand-nombre-sur-le-numerique_6330754_3234.html
featured_image: https://img.lemde.fr/2024/09/19/0/0/4000/2667/800/0/75/0/944de5f_1726736903068-campus-alpin-bourg-saint-maurice-18.jpg
tags:
- Institutions
series:
- 202439
---

> Pour Jean Cattan, secrétaire général du Conseil national du numérique, l'Etat doit promouvoir un développement du numérique au bénéfice de tous les citoyens et invite à élaborer une nouvelle politique du lien social."> <meta property="og:site_name" content="Le Monde.fr
