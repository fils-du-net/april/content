---
site: Le Temps
title: "Protection des données: les élèves genevois n'ont (presque) plus accès aux logiciels de Microsoft (€)"
author: Grégoire Barbey
date: 2024-09-25
href: https://www.letemps.ch/cyber/donnees-personnelles/protection-des-donnees-les-eleves-genevois-n-ont-presque-plus-acces-aux-logiciels-de-microsoft
featured_image: https://letemps-17455.kxcdn.com/photos/85fc5036-9d5a-4c9e-a454-e20ddec1c21f/giant.avif
tags:
- Logiciels privateurs
- Éducation
- Vie privée
series:
- 202439
series_weight: 0
---

> «Le Temps» révèle que le Département de l'instruction publique a renoncé aux services du géant américain depuis la rentrée pour les élèves du cycle d'orientation. Ceux du postobligatoire conservent un accès limité. En cause: un changement dans la politique de l'entreprise
