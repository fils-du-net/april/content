---
site: franceinfo
title: "Panoramax: le projet citoyen initié par l'Institut national de l'information géographique et forestière (IGN) pour ne plus dépendre de Google Street View"
author: Nicolas Arpagian
date: 2024-09-14
href: https://www.radiofrance.fr/franceinfo/podcasts/nouveau-monde/panoramax-le-projet-citoyen-initie-par-l-institut-national-de-l-information-geographique-et-forestiere-ign-pour-ne-plus-dependre-de-google-street-view-9690514
featured_image: https://www.radiofrance.fr/s3/cruiser-production-eu3/2024/09/93328d04-d3a5-4679-8b90-d661c6ab9e2c/640x340_sc_sans-titre-66e59f1ac8902361849707.jpg
tags:
- Open Data
series:
- 202437
series_weight: 0
---

> L'IGN fait appel au grand public pour constituer une base de données centralisée de photographies de notre environnement (rues, ronds-points, routes, berges, ponts...) qui soit en libre accès pour créer des services d'intérêt général.
