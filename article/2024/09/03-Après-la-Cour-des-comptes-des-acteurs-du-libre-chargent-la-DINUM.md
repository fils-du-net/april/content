---
site: Silicon
title: "Après la Cour des comptes, des acteurs du libre chargent la DINUM"
author: Clément Bohic
date: 2024-09-03
href: https://www.silicon.fr/dinum-logiciels-libres-481331.html
featured_image: https://www.silicon.fr/wp-content/uploads/2024/09/DINUM-CNLL-Cour-des-comptes.jpeg
tags:
- Administration
series:
- 202436
---

> Rebondissant sur les conclusions de la Cour des comptes à propos de La Suite Numérique, le CNLL accuse la DINUM de concurrencer le secteur privé... dont les acteurs du logiciel libre.
