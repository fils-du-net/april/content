---
site: Silicon
title: "Transformation numérique de l'État: la Cour des comptes critique la Dinum"
author: Clément Bohic
date: 2024-09-24
href: https://www.silicon.fr/Thematique/actualites-1367/Breves/Transformation-numerique-de-l-etat-la-Cour-des-comptes-critique-463883.htm
featured_image: https://cdn.edi-static.fr/image/upload/c_lfill,h_245,w_470/e_unsharp_mask:100,q_auto/f_auto/v1/Img/BREVE/2024/9/463883/Transformation-numerique-etat-Cour-comptes-critique-LE.jpg
tags:
- Administration
series:
- 202439
series_weight: 0
---

> Créée en 2019, la direction interministérielle du numérique (Dinum) développe les services numériques de l'État. La Cour des comptes s'est penchée sur son fonctionnement et ses réalisations. Et ne ménage pas ses critiques. Silicon vous propose son analyse sur les principaux points saillants.
