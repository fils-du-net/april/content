---
site: Journal de Millau
title: "Le collectif Open Source rend les collections du musée vivantes pour les Journées du patrimoine"
author: Chloé Azaïs
date: 2024-09-21
href: https://www.journaldemillau.fr/2024/09/21/le-collectif-open-source-rend-les-collections-du-musee-vivantes-pour-les-journees-du-patrimoine-12212309.php
featured_image: https://images.journaldemillau.fr/api/v1/images/view/66eed11889cb49085b05268e/original/image.jpg
tags:
- Partage du savoir
series:
- 202438
---

> Ce week-end des 21 et 22 septembre ont lieu les Journées européennes du patrimoine. Au musée de Millau, le collectif Open Source a fait vivre les collections ce samedi à travers des performances artistiques.
