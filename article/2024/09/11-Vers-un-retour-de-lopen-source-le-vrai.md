---
site: ITforBusiness
title: "Vers un retour de l'open source, le vrai?"
author: Laurent Delattre
date: 2024-09-11
href: https://www.itforbusiness.fr/vers-un-retour-de-l-open-source-le-vrai-81590
featured_image: https://www.itforbusiness.fr/wp-content/uploads/2024/09/Open-Source-Elastic-is-Back.jpg
tags:
- Licenses
series:
- 202437
series_weight: 0
---

> Le retour d'Elastic à l'open source marque-t-il le retour de l'open source ou l'arrivée d'un nouvel open source transformé?
