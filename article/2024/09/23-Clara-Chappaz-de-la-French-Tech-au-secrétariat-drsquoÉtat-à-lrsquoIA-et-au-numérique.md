---
site: Next
title: "Clara Chappaz, de la French Tech au secrétariat d&rsquo;État à l&rsquo;IA et au numérique"
author: Alexandre Laurent
date: 2024-09-23
href: https://next.ink/151194/clara-chappaz-de-la-french-tech-au-secretariat-detat-a-lia-et-au-numerique/
featured_image: https://next.ink/wp-content/uploads/2024/09/chappaz.webp
tags:
- Institutions
series:
- 202439
---

> Directrice de la mission French Tech depuis trois ans, Clara Chappaz a été nommée samedi secrétaire d’État à l’Intelligence artificielle et au Numérique. Traditionnellement sous tutelle de Bercy, son portefeuille est désormais rattaché au ministère de l’Enseignement supérieur et de la Recherche.
