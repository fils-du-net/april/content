---
site: ZDNET
title: "La documentation sur Linux et les logiciels libres est un véritable capharnaüm: voici la solution"
author: Steven Vaughan-Nichols
date: 2024-09-17
href: https://www.zdnet.fr/actualites/la-documentation-sur-linux-et-les-logiciels-libres-est-un-veritable-capharnaum-voici-la-solution-397210.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/09/documentation.webp
tags:
- Sensibilisation
series:
- 202438
series_weight: 0
---

> Il ne suffit pas de dire à quelqu'un de se débrouiller tout seul lorsque le manuel est obsolète, illisible ou inexistant. Nous devons améliorer la qualité de la documentation du code, et le moyen d'y parvenir est simple.
