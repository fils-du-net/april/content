---
site: Le Monde Informatique
title: "Avec Ada, la Dinum soutient la carrière des femmes dans l'IT de l'Etat"
author: Véronique Arène
date: 2024-09-06
href: https://www.lemondeinformatique.fr/actualites/lire-avec-ada-la-dinum-soutient-la-carriere-des-femmes-dans-l-it-de-l-etat-94642.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000098706.jpg
tags:
- Administration
series:
- 202436
---

> La direction interministérielle du numérique ouvre les candidatures de la promotion 2024 de son programme Ada d'accompagnement au développement professionnel des femmes du numérique de l'État. Cette année, ce dispositif de coaching est réservé aux techniciennes des catégories A et A+.
