---
site: ZDNET
title: "Nations unies: un engagement pour les logiciels libres"
author: Thierry Noisette
date: 2024-09-27
href: https://www.zdnet.fr/blogs/l-esprit-libre/nations-unies-un-engagement-pour-les-logiciels-libres-398419.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/09/un_members_flags2-750x410.jpg
tags:
- International
- Open Data
- Interopérabilité
series:
- 202439
series_weight: 0
---

> Le pacte numérique mondial, qui fait partie du Pacte pour l'avenir que vient d'approuver un sommet de l'ONU, s'engage à «développer, diffuser et maintenir» des logiciels open source.
