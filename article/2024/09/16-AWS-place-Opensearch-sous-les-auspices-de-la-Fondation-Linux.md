---
site: Le Monde Informatique
title: "AWS place Opensearch sous les auspices de la Fondation Linux"
author: Jacques Cheminat
date: 2024-09-16
href: https://www.lemondeinformatique.fr/actualites/lire-aws-place-opensearch-sous-les-auspices-de-la-fondation-linux-94723.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000098835.png
tags:
- Licenses
series:
- 202438
series_weight: 0
---

> Après avoir forké le moteur de recherche et d'analyse d'Elasticsearch, AWS a décidé de transférer ce projet à la Fondation Linux avec la création d'une entité dédiée la Fondation Opensearch.
