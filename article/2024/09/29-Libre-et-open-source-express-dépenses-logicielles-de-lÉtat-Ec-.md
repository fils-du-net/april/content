---
site: ZDNET
title: "Libre et open source express: dépenses logicielles de l'État, Eclipse, Echirolles, Windows et Linux"
author: Thierry Noisette
date: 2024-09-29
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-depenses-logicielles-de-letat-eclipse-echirolles-windows-et-linux-398432.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/05/open-source_panneau.jpg
tags:
- april
- Administration
series:
- 202439
series_weight: 0
---

> En bref. Une proposition de l'April à la Cour des comptes. Eclipse et l'UE. Echirolles, une ville iséroise libriste. Dix différences Windows-Linux.
