---
site: Le Monde Informatique
title: "Le retour d'Elastic à l'open source orthodoxe"
author: Matt Asay
date: 2024-09-03
href: https://www.lemondeinformatique.fr/actualites/lire-le-retour-d-elastic-a-l-open-source-orthodoxe-94602.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000098644.png
tags:
- Licenses
series:
- 202436
series_weight: 0
---

> Elastic a annoncé l'ajout d'une licence AGPL (validée par l'OSI) à son offre Elasticsearch et Kibana. Une décision qui marque un rapport pacifié avec AWS qui avait forké le projet.
