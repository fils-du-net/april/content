---
site: Le Monde Informatique
title: "Linus Torvalds incite les codeurs à miser sur les projets open source utiles (€)"
author: Sean Michael Kerner
date: 2024-09-18
href: https://www.lemondeinformatique.fr/actualites/lire-linus-torvalds-incite-les-codeurs-a-miser-sur-les-projets-open-source-utiles-94748.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000098882.jpg
tags:
- Innovation
series:
- 202438
series_weight: 0
---

> Lors de l'événement Open Source Summit Europe de la Fondation Linux, le créateur de Linux a fait le point sur les dernières mises à jour de Linux et sur l'open source.
