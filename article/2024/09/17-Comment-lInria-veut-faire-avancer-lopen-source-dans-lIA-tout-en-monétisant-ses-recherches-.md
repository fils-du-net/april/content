---
site: L'usine Nouvelle
title: "Comment l'Inria veut faire avancer l'open source dans l'IA… tout en monétisant ses recherches (€)"
author: Léna Corot
date: 2024-09-17
href: https://www.usinenouvelle.com/editorial/comment-l-inria-veut-faire-avancer-l-open-source-dans-l-ia-tout-en-monetisant-ses-recherches.N2218538
featured_image: https://www.usinenouvelle.com/mediatheque/6/9/6/001515696_896x598_c.jpg
tags:
- Sciences
series:
- 202438
series_weight: 0
---

> En présentant le 12 septembre le programme de recherche P16 et la start-up Probabl, l’Inria s'attaque à l’intégralité du cycle de la donnée. L’objectif est de développer et maintenir à l’état de l’art des bibliothèques de logiciels open source tout en parvenant à commercialiser ces ressources avec de nouveaux produits et services.
