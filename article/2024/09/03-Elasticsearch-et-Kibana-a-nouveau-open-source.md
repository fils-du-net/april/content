---
site: LeMagIT
title: "Elasticsearch et Kibana «à nouveau open source» (€)"
author: Gaétan Raoul
date: 2024-09-03
href: https://www.lemagit.fr/actualites/366609402/Elasticsearch-et-Kibana-a-nouveau-open-source
featured_image: https://cdn.ttgtmedia.com/visuals/ComputerWeekly/Hero%20Images/open-source-fotolia.jpg
tags:
- Licenses
series:
- 202436
---

> En annonçant le retour d’une licence open source pour les versions communautaires d’Elasticsearch et de Kibana, Elastic entend redorer son blason auprès des développeurs. Le choix de l’AGPLv3 montre toutefois que l’éditeur souhaite conserver le contrôle sur l’évolution de sa base de données.
