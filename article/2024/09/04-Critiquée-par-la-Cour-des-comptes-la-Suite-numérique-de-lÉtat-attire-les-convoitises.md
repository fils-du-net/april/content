---
site: Next
title: "Critiquée par la Cour des comptes, la «Suite numérique» de l'État attire les convoitises"
description: "Externaliser la souveraineté?"
author: Martin Clavey
date: 2024-09-04
href: https://next.ink/148528/critiquee-par-la-cour-des-comptes-la-suite-numerique-de-letat-attire-les-convoitises/
featured_image: https://next.ink/wp-content/uploads/2024/09/cours_des_comptes.webp
tags:
- Administration
- Institutions
series:
- 202436
series_weight: 0
---

> Dans son dernier rapport sur la Direction interministérielle du numérique (Dinum), la Cour des comptes pose la question de la pertinence de la « Suite numérique de l’agent public » proposée par les services du gouvernement et de sa gestion de projet. Certaines entreprises françaises du logiciel libre y voient une aubaine pour proposer leurs services en enfonçant le clou, qualifiant le projet de « concurrence irresponsable » et accusant la Dinum d'avoir une «vision restrictive du Libre».
