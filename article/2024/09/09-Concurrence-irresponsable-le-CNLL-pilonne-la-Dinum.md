---
site: ZDNET
title: "«Concurrence irresponsable»: le CNLL pilonne la Dinum"
author: Thierry Noisette
date: 2024-09-09
href: https://www.zdnet.fr/blogs/l-esprit-libre/concurrence-irresponsable-le-cnll-pilonne-la-dinum-396752.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/09/cour_des_comptes_paris_entree-wmc.jpg
tags:
- Institutions
- Administration
- april
series:
- 202437
series_weight: 0
---

> S'appuyant sur le rapport de la Cour des comptes sur la Direction interministérielle du numérique, le CNLL dresse un réquisitoire contre sa stratégie en matière de logiciels libres.
