---
site: Le Monde.fr
title: "Développement du numérique: à chaque ville sa stratégie (€)"
author: Fanny Hardy et Raphaëlle Lavorel
date: 2024-09-28
href: https://www.lemonde.fr/smart-cities/article/2024/09/24/developpement-du-numerique-a-chaque-ville-sa-strategie_6330685_4811534.html
featured_image: https://img.lemde.fr/2024/09/19/0/0/2000/1333/800/0/75/0/740c7be_1726736503735-fab-lab-chambery-25.jpg
tags:
- Administration
series:
- 202439
---

> A Echirolles, Chambéry et Bourg-Saint-Maurice, trois communes d'Auvergne-Rhône-Alpes, les équipes municipales ont mis en place des actions adaptées à leur territoire et aux besoins de leur population.
