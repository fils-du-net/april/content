---
site: L'Informaticien
title: "Une initiative pour soutenir les meilleures pratiques open source"
date: 2024-09-18
href: https://www.linformaticien.com/927-developpement/62441-ossummit-une-initiative-pour-soutenir-les-meilleures-pratiques-open-source.html
featured_image: https://www.linformaticien.com/images/00-articles/2024-09/Capture_de%CC%81cran_2024-09-18_a%CC%80_08.58.46_-_copie.jpg
tags:
- Innovation
series:
- 202438
series_weight: 0
---

> Baptisée Developer Relations Foundation (DRF), cette initiative, annoncée le lundi 16 septembre lors de l'OSSummit de Vienne, vise à soutenir et à unifier les meilleures pratiques au sein de la communauté des développeurs open source.
