---
site: Acteurs Publics 
title: “L'adoption des logiciels libres par les collectivités mérite d'être accompagnée par l'État (€)”
author: Emile Marzolf
date: 2024-09-03
href: https://acteurspublics.fr/articles/ladoption-des-logiciels-libres-par-les-collectivites-merite-detre-accompagnee-par-letat
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/52/9f40d1b4b2ba9d4348256d1212c87d29de907633.jpeg
tags:
- Administration
series:
- 202436
---

> Depuis quelques années, l’État finance le développement de logiciels libres, y compris pour les besoins des collectivités locales. Pour en tirer le plein potentiel, le directeur de la stratégie et de la culture numériques de la ville d’Échirolles, Nicolas Vivant, appelle à pousser un cran plus loin en n’accompagnant pas seulement la conception, mais aussi l’adoption de ces logiciels." 
