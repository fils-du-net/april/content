---
site: Silicon
title: "Elasticsearch redevient open source... mais pas sous sa licence d'origine"
author: Clément Bohic
date: 2024-09-04
href: https://www.silicon.fr/elasticsearch-redevient-open-source-481370.html
featured_image: https://www.silicon.fr/wp-content/uploads/2024/09/Elasticsearch-open-source.jpg
tags:
- Licenses
series:
- 202436
---

> Elasticsearch revient à l'open source non pas en réadoptant Apache 2.0, mais en proposant une option «compatible»: AGPLv3.
