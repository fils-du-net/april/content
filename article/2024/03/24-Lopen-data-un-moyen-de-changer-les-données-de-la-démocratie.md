---
site: ZDNet France
title: "L'open data, un moyen de 'changer les données' de la démocratie"
author: Thierry Noisette
date: 2024-03-24
href: https://www.zdnet.fr/blogs/l-esprit-libre/l-open-data-un-moyen-de-changer-les-donnees-de-la-democratie-39965132.htm
tags:
- Open Data
series:
- 202412
series_weight: 0
---

> Les données ouvertes «ne servent pas seulement à révéler des faits mais à transformer la réalité, 'à changer les données', expose Samuel Goëta dans 'Les données de la démocratie', un excellent ouvrage sur l'histoire, les enjeux et les perspectives de l'open data.
