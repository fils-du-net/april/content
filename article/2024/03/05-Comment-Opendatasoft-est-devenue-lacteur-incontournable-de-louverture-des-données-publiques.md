---
site: Le Monde.fr
title: "Comment Opendatasoft est devenue l'acteur incontournable de l'ouverture des données publiques (€)"
author: Raphaëlle Aubert et Léa Sanchez
date: 2024-03-05
href: https://www.lemonde.fr/les-decodeurs/article/2024/03/05/comment-opendatasoft-est-devenue-l-acteur-incontournable-de-l-ouverture-des-donnees-publiques_6220195_4355770.html
tags:
- Entreprise
- Open Data
series:
- 202410
---

> L’entreprise française a su répondre à un besoin nouveau des administrations, créé par l’obligation légale d’ouvrir leurs données.
