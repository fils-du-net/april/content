---
site: Le Monde Informatique
title: "Elon Musk porte plainte contre OpenAI et Sam Altman"
author: Jacques Cheminat
date: 2024-03-01
href: https://www.lemondeinformatique.fr/actualites/lire-elon-musk-porte-plainte-contre-openai-et-sam-altman-93111.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000096298.jpg
tags:
- Sciences
series:
- 202409
---

> Le président exécutif et directeur technique de X Elon Musk a déposé une plainte auprès d'un tribunal de San Francisco contre OpenAI et Sam Altman. Il estime que les récentes relations de la start-up avec Microsoft ont modifié son engagement initial en faveur d'une intelligence artificielle publique et open source.
