---
site: Numerama
title: "Elon Musk publie un torrent de 318 Go et rend Grok open source"
description: Le même coup que Mistral
author: Nicolas Lellouche
date: 2024-03-18
href: https://www.numerama.com/tech/1654490-elon-musk-publie-un-torrent-de-318-go-et-rend-grok-open-source.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2023/11/grok-1024x576.jpg?avif=1&key=1587c970
tags:
- Sciences
series:
- 202412
series_weight: 0
---

> En guerre contre OpenAI et ses modèles commerciaux, Elon Musk avait promis début mars qu'il rendrait le modèle de langage derrière Grok, son chatbot, open source. Promesse tenue.
