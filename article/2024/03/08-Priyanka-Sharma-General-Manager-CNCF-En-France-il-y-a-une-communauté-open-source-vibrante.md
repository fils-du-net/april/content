---
site: Le Monde Informatique
title: "Priyanka Sharma (General Manager CNCF): «En France il y a une communauté open source vibrante»"
author: Dominique Filippone
date: 2024-03-08
href: https://www.lemondeinformatique.fr/actualites/lire-priyanka-sharma-general-manager-cncf--en-france-il-y-a-une-communaute-open-source-vibrante-93009.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000096115.jpg
tags:
- Entreprise
- Informatique en nuage
series:
- 202410
series_weight: 0
---

> Un mois avant la première édition en France de la Kubecon Europe, la directrice exécutive de la Cloud Native Computing Foundation, qui gère Kubernetes, revient sur les projets et ambitions et apporte son regard sur l'évolution de la place de l'open source dans les entreprises.
