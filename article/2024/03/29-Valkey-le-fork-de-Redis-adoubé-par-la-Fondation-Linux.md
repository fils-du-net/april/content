---
site: Le Monde Informatique
title: "Valkey: le fork de Redis adoubé par la Fondation Linux"
author: Jacques Cheminat
date: 2024-03-29
href: https://www.lemondeinformatique.fr/actualites/lire-valkey-le-fork-de-redis-adoube-par-la-fondation-linux-93385.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000096741.png
tags:
- Licenses
series:
- 202413
---

> Après le changement de licence de Redis, la Fondation Linux a annoncé un projet alternatif baptisé Valkey. Ce dernier dispose déjà du soutien de plusieurs acteurs comme AWS, Google Cloud, Oracle et d'autres.
