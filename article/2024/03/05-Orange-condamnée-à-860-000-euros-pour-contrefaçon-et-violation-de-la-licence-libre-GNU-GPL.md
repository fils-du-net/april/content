---
site: Next
title: "Orange condamnée à 860 000 euros pour contrefaçon et violation de la licence libre GNU GPL"
date: 2024-03-05
href: https://next.ink/brief_article/orange-condamnee-a-860-000-euros-pour-contrefacon-et-violation-de-la-licence-libre-gnu-gpl/
featured_image: https://next.ink/wp-content/uploads/2024/03/Logo_gpl-violations.org_.png
tags:
- Licenses
- april
series:
- 202410
series_weight: 0
---

> Après plus de douze ans de procédure, rapporte l'association April, Orange vient d'être condamnée pour contrefaçon. Elle a violé les termes de la licence GNU GPL v2, et donc le droit d'auteur d'Entr'ouvert, société coopérative autrice de la bibliothèque libre de gestion d’identité LASSO (Liberty Alliance Single Sign On).
