---
site: l'Informé
title: "Offensive au Conseil d’État contre l’hébergement de nos données de santé par Microsoft (€)"
author: Marc Rees
date: 2024-03-13
href: https://www.linforme.com/tech-telecom/article/offensive-au-conseil-d-etat-contre-l-hebergement-de-nos-donnees-de-sante-par-microsoft_1544.html#Echobox=1710350522
featured_image: https://linforme-focus.sirius.press/2024/03/13/0/0/7771/5183/320/213/75/0/bb3f03c_1710342090774-afp-20220721-32ez9un-v1-highres-francehealthsocialhospitals.jpg
tags:
- Vie privée
series:
- 202411
series_weight: 0
---

> Plusieurs sociétés spécialisées dans l'hébergement sécurisé et des associations s'attaquent à la décision de la CNIL qui a autorisé ce stockage par le géant américain.
