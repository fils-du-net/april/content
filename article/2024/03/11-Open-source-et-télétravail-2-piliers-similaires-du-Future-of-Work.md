---
site: InformatiqueNews.fr
title: "Open source et télétravail: 2 piliers similaires du Future of Work"
author: Franz Karlsberger
date: 2024-03-11
href: https://www.informatiquenews.fr/open-source-et-teletravail-2-piliers-du-future-of-work-aux-similitudes-frappantes-franz-karlsberger-amazee-io-98259
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2024/03/Open-Source-Teletravail-Future-of-Work.jpg
tags:
- Innovation
series:
- 202411
series_weight: 0
---

> Dans un monde en pleine mutation, les entreprises font face à deux évolutions majeures: le travail à distance, d’une part, devenu la tendance dominante depuis la pandémie de Covid-19 et d’autre part, le développement vertigineux de l’open source. Deux transformations a priori indépendantes l’une de l’autre mais dont les similitudes sont frappantes, autant en termes de philosophie que de modes d’organisation.
