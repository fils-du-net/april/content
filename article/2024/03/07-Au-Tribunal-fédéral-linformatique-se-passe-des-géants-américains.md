---
site: Le Temps
title: "Au Tribunal fédéral, l'informatique se passe des géants américains (€)"
author: Grégoire Barbey
date: 2024-03-07
href: https://www.letemps.ch/cyber/au-tribunal-federal-l-informatique-se-passe-des-geants-americains
featured_image: https://letemps-17455.kxcdn.com/photos/f7559f87-6ebc-473d-aa37-d36e80deec04/giant.avif
tags:
- Administrations
- Standards
series:
- 202410
series_weight: 0
---

> La plus haute autorité judiciaire du pays a fait le choix de la souveraineté numérique pour son infrastructure informatique. Une approche qui tranche avec d'autres institutions publiques et privées
