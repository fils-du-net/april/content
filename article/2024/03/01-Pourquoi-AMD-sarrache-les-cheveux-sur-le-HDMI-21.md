---
site: 01net.
title: "Pourquoi AMD s'arrache les cheveux sur le HDMI 2.1?"
author: Mickaël Bazoge
date: 2024-03-01
href: https://www.01net.com/actualites/amd-face-a-un-mur-pour-le-hdmi-2-1-open-source.html
featured_image: https://www.01net.com/app/uploads/2024/02/HDMI-AMD-1360x907.jpg
tags:
- Standards
series:
- 202409
series_weight: 0
---

> AMD aimerait vraiment beaucoup être en mesure d'utiliser le HDMI 2.1 pour diffuser des contenus très haute définition à des fréquences d'affichage très élevées. Malheureusement, ça coince avec le HDMI Forum, qui a opposé une fin de non recevoir.
