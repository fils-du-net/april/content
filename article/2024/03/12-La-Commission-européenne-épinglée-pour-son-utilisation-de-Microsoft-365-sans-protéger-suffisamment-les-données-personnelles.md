---
site: Next
title: "La Commission européenne épinglée pour son utilisation de Microsoft 365 sans protéger suffisamment les données personnelles (€)"
description: Balayer devant son cloud
author: Martin Clavey
date: 2024-03-12
href: https://next.ink/130844/la-commission-europeenne-epinglee-pour-son-utilisation-de-microsoft-365-sans-proteger-suffisamment-les-donnees-personnelles/
featured_image: https://next.ink/wp-content/uploads/2023/11/guillaume-perigois-0NRkVddA2fw-unsplash-1536x1024.jpg
tags:
- Europe
series:
- 202411
---

> Selon le Contrôleur européen de la protection des données (CEPD), la Commission européenne a enfreint, dans son utilisation de Microsoft 365, certaines règles de protection clés du règlement sur le traitement des données à caractère personnel par les institutions (à ne pas confondre avec le RGPD). La commission a jusqu'au 9 décembre pour se mettre en règle.
