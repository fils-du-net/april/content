---
site: LeMagIT
title: "SGBD: Redis s’éloigne, lui aussi, de l’open source"
author: Gaétan Raoul
date: 2024-03-26
href: https://www.lemagit.fr/actualites/366575532/SGBD-Redis-seloigne-lui-aussi-de-lopen-source
featured_image: https://www.lemagit.fr/visuals/ComputerWeekly/Hero%20Images/open-source-fotolia.jpg
tags:
- Licenses
series:
- 202413
series_weight: 0
---

> La semaine dernière, l’éditeur a annoncé un changement de licence affectant le cœur de sa base de données qui devient propriétaire, mais est accessible de manière permissive. Une modification qui fait grincer des dents, mais qui n’a rien d’étonnant.
