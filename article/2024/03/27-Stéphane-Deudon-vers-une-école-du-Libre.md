---
site: Le Café pédagogique
title: "Stéphane Deudon: vers une école du Libre?"
author: Jean-Michel Le Baut
date: 2024-03-27
href: https://www.cafepedagogique.net/2024/03/27/stephane-deudon-vers-une-ecole-du-libre
featured_image: https://www.cafepedagogique.net/wp-content/uploads/2024/03/une-image-contenant-visage-humain-verres-personn-e1711461868513-600x597.jpeg
tags:
- Éducation
- Promotion
series:
- 202413
series_weight: 0
---

> L’École est-elle condamnée à obéir à la logique marchande des grandes entreprises du numérique? Pour Stéphane Deudon, professeur des écoles à Caudry dans le Nord, on peut s’en émanciper. Par exemple avec Primtux, un système d’exploitation libre et gratuit, basé sur Linux, installable sur des machines anciennes et reconditionnées. La solution est élaborée par une équipe d’enseignants, elle intègre des activités créées par des professeurs, elle offre une interface adaptée aux élèves de l’école primaire. Elle est présentée à la Journée Du Libre Educatif le 29 mars 2024 à Créteil, où sera aussi initié un «Appel à communs» dans l’éducation pour fédérer et mutualiser des projets souvent isolés et méconnus: l’enjeu sera de «proposer des services numériques de qualité, souverains et pérennes, à l’ensemble de la communauté scolaire tout en valorisant les compétences de nos enseignants et en offrant à l’Edtech des opportunités pour apporter de la valeur ajoutée et innover ensemble.»
