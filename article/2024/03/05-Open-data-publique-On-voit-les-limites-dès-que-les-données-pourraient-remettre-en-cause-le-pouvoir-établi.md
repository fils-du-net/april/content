---
site: Le Monde.fr
title: "Open data publique: «On voit les limites dès que les données pourraient remettre en cause le pouvoir établi» (€)"
author: Raphaëlle Aubert et Léa Sanchez
date: 2024-03-05
href: https://www.lemonde.fr/les-decodeurs/article/2024/03/05/open-data-publique-on-voit-les-limites-des-que-les-donnees-pourraient-remettre-en-cause-le-pouvoir-etabli_6220230_4355770.html
tags:
- Open Data
series:
- 202410
series_weight: 0
---

> Dans un entretien au «Monde», le sociologue Samuel Goëta dresse un bilan de l’ouverture des données publiques en France.
