---
site: Le Figaro
title: "Najat Vallaud-Belkacem: «Libérons-nous des écrans, rationnons internet!»"
date: 2024-03-18
href: https://www.lefigaro.fr/vox/societe/najat-vallaud-belkacem-liberons-nous-des-ecrans-rationnons-internet-20240318
featured_image: https://i.f1g.fr/media/cms/1194x804/2024/03/18/e0af2501acda65096da87dbb6e9e2e0557d69d55aeb7fcd43f946abf79d714a0.jpg
tags:
- Internet
series:
- 202412
---

> L'ancienne ministre de l'Éducation nationale propose de rationner internet, en accordant par exemple un nombre limité de gigas à utiliser quotidiennement. Face à la pollution numérique, une telle mesure serait profondément progressiste, argumente-t-elle.
