---
site: ZDNet France
title: "Canonical fête ses 20 ans: comment Ubuntu a changé le monde de Linux"
author: Steven Vaughan-Nichols
date: 2024-03-06
href: https://www.zdnet.fr/actualites/canonical-fete-ses-20-ans-comment-ubuntu-a-change-le-monde-de-linux-39964718.htm
featured_image: https://www.zdnet.com/a/img/2024/03/05/12ebda50-6aac-47ab-a01b-f5d8e9cd75f4/ubuntu-4-10.jpg
tags:
- sensibilisation
- Entreprise
series:
- 202410
series_weight: 0
---

> La société mère d'Ubuntu, qui équipe aujourd'hui des millions d'ordinateurs de bureau, de serveurs et d'instances cloud, continue de rechercher l'équilibre entre la fourniture de 'Linux pour les humains' et sa responsabilité croissante sur le marché mondial de la technologie.
