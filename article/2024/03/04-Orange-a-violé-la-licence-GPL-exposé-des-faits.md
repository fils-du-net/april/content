---
site: Silicon
title: "Orange a violé la licence GPL: exposé des faits"
author: Clément Bohic
date: 2024-03-04
href: https://www.silicon.fr/orange-viole-licence-gpl-476445.html
featured_image: https://www.silicon.fr/wp-content/uploads/2024/03/Orange-violation-licence-GPL-scaled.jpeg
tags:
- Licenses
series:
- 202410
---

> Un litige allé jusqu'en cassation voit Orange condamné pour avoir violé la licence GNU GPL v2 dans le cadre d'un projet d'État.
