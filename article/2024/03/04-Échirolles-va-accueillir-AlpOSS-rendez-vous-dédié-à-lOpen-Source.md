---
site: Place Gre'net
title: "Échirolles va accueillir AlpOSS, rendez-vous dédié à l'Open Source"
author: Florent Mathieu
date: 2024-03-04
href: https://www.placegrenet.fr/2024/03/04/echirolles-accueille-alposs-premiere-edition-dun-rendez-vous-dedie-aux-solutions-numeriques-open-source/624742
featured_image: https://www.placegrenet.fr/wp-content/uploads/2020/12/IMG_8543.jpg
tags:
- Promotion
series:
- 202410
series_weight: 0
---

> Échirolles accueille AlpOSS le 21 mars 2024, première édition d'un rendez-vous dédié aux solutions numériques Open Source.
