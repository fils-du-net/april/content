---
site: cio-online.com
title: "IA générative: davantage de modèles et davantage d'Open Source"
author: Reynald Fléchaux
date: 2024-03-29
href: https://www.cio-online.com/actualites/lire-ia-generative-davantage-de-modeles-et-davantage-d-open-source-15556.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000020798.jpg
tags:
- Sciences
series:
- 202413
series_weight: 0
---

> Selon le fonds Andreessen Horowitz, les grandes entreprises misent de plus en plus sur une diversité de LLM et privilégient les modèles Open Source.
