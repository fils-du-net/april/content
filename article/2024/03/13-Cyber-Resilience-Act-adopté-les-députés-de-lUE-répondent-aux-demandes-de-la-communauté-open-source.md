---
site: ICTjournal
title: "Cyber Resilience Act adopté: les députés de l'UE répondent aux demandes de la communauté open source"
author: Yannick Chavanne
date: 2024-03-13
href: https://www.ictjournal.ch/news/2024-03-13/cyber-resilience-act-adopte-les-deputes-de-lue-repondent-aux-demandes-de-la
featured_image: https://data.ictjournal.ch/styles/np8_full/s3/media/2024/03/13/adobestock_188054130.jpg?itok=rOBaftIF
tags:
- Europe
series:
- 202411
series_weight: 0
---

> Tout juste adopté, Cyber Resilience Act européen impose des contraintes de cybersécurité aux fournisseurs de produits numériques. Le texte final fait la distinction entre les acteurs de l’open source et les distributeurs de solutions commerciales.
