---
site: RFI
title: "Elon Musk lance la bagarre des IA en promettant une intelligence artificielle en accès libre"
author: Thomas Bourdeau
date: 2024-03-13
href: https://www.rfi.fr/fr/technologies/20240313-elon-musk-lance-la-bagarre-des-ia-en-promettant-une-intelligence-artificielle-en-acc%C3%A8s-libre
featured_image: https://s.rfi.fr/media/display/91850380-e143-11ee-bbad-005056a97e36/w:980/p:16x9/2024-03-11T170957Z_433069934_RC2SJ6A3AREO_RTRMADP_3_MUSK-AI.JPG
tags:
- Sciences
series:
- 202411
series_weight: 0
---

> Un dilemme réside en intelligence artificielle: les progrès dans le domaine sont-ils la propriété des entreprises ou doivent-ils être partagés à la communauté de chercheurs? En annonçant déployer en «open source» son «chatbot» conversationnel baptisé Grok, Elon Musk a non seulement relancé le débat, mais aussi taclé la société Open AI -créatrice de chat GPT- devenue une entreprise à but lucratif, au grand dam du milliardaire. Les motivations du patron de X sont-elles louables et un juste équilibre peut-il exister dans le monde de l'IA?
