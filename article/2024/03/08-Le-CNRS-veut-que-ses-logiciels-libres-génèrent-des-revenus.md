---
site: Next
title: "Le CNRS veut que ses logiciels libres génèrent des revenus"
description: Libres mais pas forcément gratuits
author: Martin Clavey
date: 2024-03-08
href: https://next.ink/130221/le-cnrs-veut-que-ses-logiciels-libres-generent-des-revenus/
featured_image: https://next.ink/wp-content/uploads/2024/01/shahadat-rahman-BfrQnKBulYQ-unsplash.jpg
tags:
- Économie
series:
- 202410
series_weight: 0
---

> Le logiciel libre a une place importante dans l'univers de la recherche publique. De nombreux chercheurs contribuent à des projets du libre ou en créent. Le CNRS veut maintenant aider ses chercheurs à intégrer leurs logiciels dans une «logique de satisfaction d’objectifs économiques viables» avec un programme de financement appelé «OPEN».
