---
site: Silicon
title: "Redis change de licence: le point sur la situation"
author: Clément Bohic
date: 2024-03-22
href: https://www.silicon.fr/redis-change-licence-477072.html
featured_image: https://www.silicon.fr/wp-content/uploads/2024/03/Redis-licence-open-source.jpg
tags:
- Licenses
- Entreprise
series:
- 202412
series_weight: 0
---

> Redis abandonne la licence permissive BSD au profit d'un système «à la carte» avec deux options. Quels en sont les motifs et les implications?
