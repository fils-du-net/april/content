---
site: ZDNet France
title: "Mais 3 Go par semaine, c'est sa peau contre ma peau..."
author: Guillaume Serries
date: 2024-03-18
href: https://www.zdnet.fr/actualites/mais-3-go-par-semaine-c-est-sa-peau-contre-ma-peau-39965006.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2022/03/fce_internet_1200__w630.jpg
tags:
- Internet
series:
- 202412
series_weight: 0
---

> Najat Vallaud-Belkacem, ancienne ministre de l'Education nationale, critique l'utilisation massive des écrans. Sa solution? 'La rareté oblige à une certaine sagesse' dit-elle. Et de mettre au débat le fait de 'rationner internet' par la loi.
