---
site: Silicon
title: "Elon Musk attaque OpenAI pour non respect du modèle Open Source"
author: Philippe Leroy
date: 2024-03-01
href: https://www.silicon.fr/elon-musk-attaque-openai-pour-non-respect-open-source-476432.html
featured_image: https://www.silicon.fr/wp-content/uploads/2023/03/AdobeStock_553614010_Editorial_Use_Only-scaled.jpeg
tags:
- Sciences
series:
- 202409
---

> Elon Musk reproche à OpenAI de s'être détourné de son statut associatif et du modèle Open Source.
