---
site: Echirolles
title: "Une première réussie pour AlpOSS!"
date: 2024-03-22
href: https://www.echirolles.fr/actualites/une-premiere-reussie-pour-alposs
featured_image: https://www.echirolles.fr/sites/default/files/styles/vignettes_echirolles_960x640/public/2024-03/P1050150.JPG
tags:
- Promotion
series:
- 202412
series_weight: 0
---

> Ce jeudi 21 mars s'est tenu la 1ère édition d'AlpOSS au sein de l'Hôtel de Ville. Coorganisé avec OW2 et Belledonne Communications, le nouvel événement isérois de l'écosystème open source local a réuni 250 personnes - entreprises, associations, collectivités locales ou passionné-es du libre - venu-es se rencontrer et échanger.
