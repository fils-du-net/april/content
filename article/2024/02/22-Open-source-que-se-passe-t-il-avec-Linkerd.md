---
site: Silicon
title: "Open source: que se passe-t-il avec Linkerd?"
author: Clément Bohic
date: 2024-02-22
href: https://www.silicon.fr/linkerd-open-source-476150.html
featured_image: https://www.silicon.fr/wp-content/uploads/2024/02/Linkerd-open-source-684x513.jpeg
tags:
- Licenses
- Entreprise
series:
- 202409
series_weight: 0
---

> L’entreprise porteuse de Linkerd n’en distribuera plus de versions stables en source ouverte et soumettra ses rétroportages à un paywall.
