---
site: ZDNet France
title: "Règlement pour une Europe interopérable: une avancée pour les logiciels libres, pour la FSFE"
author: Thierry Noisette
date: 2024-02-09
href: https://www.zdnet.fr/blogs/l-esprit-libre/reglement-pour-une-europe-interoperable-une-avancee-pour-les-logiciels-libres-pour-la-fsfe-39964154.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2018/12/Bruxelles_Commission_UE_Berlaymont.jpg
tags:
- Europe
- Licenses
- Marchés publics
- Interopérabilité
series:
- 202407
series_weight: 0
---

> Le texte que viennent de voter les eurodéputés est une «opportunité importante», estime la Free Software Foundation Europe.
