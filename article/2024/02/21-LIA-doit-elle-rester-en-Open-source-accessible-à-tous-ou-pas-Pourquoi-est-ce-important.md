---
site: euronews
title: "L'IA doit elle rester en 'Open source', accessible à tous, ou pas? Pourquoi est-ce important?"
author: Pascale Davies
date: 2024-02-21
href: https://fr.euronews.com/next/2024/02/21/lia-doit-elle-rester-en-open-source-accessible-a-tous-ou-pas-pourquoi-est-ce-important
featured_image: https://static.euronews.com/articles/stories/08/25/36/36/1200x675_cmsv2_d4f2de1a-f878-5096-9826-849392cf0cdf-8253636.jpg
tags:
- Sciences
- Sensibilisation
series:
- 202409
---

> Les régulateurs, les start-ups et les grandes entreprises technologiques sont divisés en deux camps dans le débat sur l'intelligence artificielle Open Source, accessible à tous, ou pas?
