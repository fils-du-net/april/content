---
site: Silicon
title: "Apple sacrifie les web apps progressives sur l'autel du DMA"
author: Clément Bohic
date: 2024-02-16
href: https://www.silicon.fr/apple-web-apps-progressives-dma-475977.html
featured_image: https://www.silicon.fr/wp-content/uploads/2024/02/Apple-DMA-iOS-PWA-scaled.jpeg
tags:
- Entreprise
- Europe
series:
- 202407
---

> iOS ne prendra bientôt plus en charge les PWA dans l'Union européenne. La conséquence de sa mise en conformité avec le DMA, explique Apple.
