---
site: Banque des Territoires
title: "La justice rappelle les obligations associées aux logiciels libres"
author: Olivier Devillers
date: 2024-02-28
href: https://www.banquedesterritoires.fr/la-justice-rappelle-les-obligations-associees-aux-logiciels-libres
featured_image: https://www.banquedesterritoires.fr/sites/default/files/styles/landscape_auto_crop/public/2024-02/AdobeStock_316627156.jpeg.jpg
tags:
- april
- Entreprise
- Insitutions
series:
- 202409
series_weight: 0
---

> Localtis: L'usage des logiciels libres est assorti d'obligations telles que le reversement des développements informatiques à la communauté. C'est ce que vient de rappeler un jugement de la cour d'appel de Paris du 24 février 2024 opposant la coopérative Entr'ouvert à Orange business services.
