---
site: ZDNet France
title: "Non-respect de la licence GPL: Orange condamné en appel"
author: Thierry Noisette
date: 2024-02-16
href: https://www.zdnet.fr/blogs/l-esprit-libre/non-respect-de-la-licence-gpl-orange-condamne-en-appel-39964312.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2019/01/justice-logo.jpg
tags:
- Licenses
- april
- Institutions
- Entreprise
series:
- 202407
series_weight: 0
---

> La cour d'appel de Paris a condamné Orange à payer 650.000 euros à la société coopérative Entr'Ouvert pour ne pas avoir respecté la licence GNU GPL v2.
