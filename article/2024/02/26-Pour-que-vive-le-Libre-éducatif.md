---
site: Le Café pédagogique
title: "Pour que vive le Libre éducatif"
author: Lilia Ben Hamouda
date: 2024-02-26
href: https://www.cafepedagogique.net/2024/02/26/260058/
featured_image: https://www.cafepedagogique.net/wp-content/uploads/2024/02/une-image-contenant-texte-capture-decran-graphi.png
tags:
- Éducation
- Promotion
series:
- 202409
---

> Après Lyon en 2022 et Rennes en 2023, la Journée du Libre Educatif 2024 déploiera sa 3ème édition le 29 mars 2024 à Créteil.
