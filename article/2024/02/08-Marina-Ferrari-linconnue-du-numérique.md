---
site: Le Monde.fr
title: "Marina Ferrari, l'inconnue du numérique (€)"
author: Olivier Pinaud et Vincent Fagot
date: 2024-02-08
href: https://www.lemonde.fr/economie/article/2024/02/08/marina-ferrari-l-inconnue-du-numerique_6215516_3234.html
featured_image: https://img.lemde.fr/2024/02/08/0/0/5215/3477/664/0/75/0/063a5bf_1707417175593-000-32cx3ch.jpg
tags:
- Institutions
series:
- 202407
---

> Cette députée MoDem de Savoie devient secrétaire d'Etat chargée du numérique et remplace Jean-Noël Barrot, qui devient ministre délégué chargé de l'Europe.
