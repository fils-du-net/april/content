---
site: ZDNet France
title: "Education nationale: des logiciels libres pour de nombreux usages, offerts aux enseignants"
author: Thierry Noisette
date: 2024-02-28
href: https://www.zdnet.fr/blogs/l-esprit-libre/education-nationale-des-logiciels-libres-pour-de-nombreux-usages-offerts-aux-enseignants-39964532.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/journee-libre-educatif-2024.jpeg
tags:
- Éducation
- Promotion
series:
- 202409
series_weight: 0
---

> Partage de fichiers, de vidéos, édition collaborative, création et partage de parcours pédagogiques... Les enseignants peuvent utiliser beaucoup d'alternatives aux Gafam. Le 29 mars, ces nombreux services seront parmi les programmes exposés à la Journée du Libre Educatif à Créteil
