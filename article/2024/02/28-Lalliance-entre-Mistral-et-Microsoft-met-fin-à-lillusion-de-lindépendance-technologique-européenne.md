---
site: La Tribune
title: "L'alliance entre Mistral et Microsoft met fin à l'illusion de l'indépendance technologique européenne"
author: François Manens
date: 2024-02-28
href: https://www.latribune.fr/technos-medias/informatique/l-alliance-entre-mistral-et-microsoft-met-fin-a-l-illusion-de-l-independance-technologique-europeenne-991558.html
featured_image: https://static.latribune.fr/full_width/2328296/mistral-ai-et-microsoft.jpg
tags:
- Sciences
series:
- 202409
series_weight: 0
---

> Alors qu'il défendait mordicus l'open source comme valeur cardinale pour s'imposer face aux géants américains, le champion français de l'intelligence artificielle, Mistral AI, a développé son plus puissant modèle de langage de manière fermée, et a conclu un partenariat pour l'heure exclusif avec Microsoft pour sa distribution, laissant même l'Américain entrer symboliquement à son capital. Si ce virage stratégique aux allures de séisme fait sens d'un point de vue économique et bénéficie du soutien de la France, il rend furieux à Bruxelles et parmi les défenseurs de l'IA européenne. Par ricochet, l'entrée de Mistral, comme OpenAI avant lui, dans le giron du géant Microsoft, est une claque pour la souveraineté numérique européenne, et acte la position déjà dominante des Américains dans la course à l'intelligence artificielle.
