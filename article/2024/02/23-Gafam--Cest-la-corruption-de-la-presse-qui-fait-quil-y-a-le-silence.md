---
site: Arrêt sur Images
title: "Gafam: \"C'est la corruption de la presse qui fait qu'il y a le silence\""
date: 2024-02-23
href: https://www.arretsurimages.net/emissions/arret-sur-images/gafam-cest-la-corruption-de-la-presse-qui-fait-quil-y-a-le-silence
featured_image: https://api.arretsurimages.net/api/public/media/2024-02-23-gafam/action/show
tags:
- Partage du savoir
- Internet
series:
- 202409
series_weight: 0
---

> Cinq petites lettres qui régissent en partie notre consommation de l'information: Gafam. Google, Amazon, Facebook, Apple et Microsoft. Ajoutons à la liste le nouveau X de Twitter et le T de Tiktok pour un panel complet.
