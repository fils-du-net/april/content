---
site: Le Monde.fr
title: "La France enterre son indice de durabilité des smartphones"
date: 2024-02-15
href: https://www.lemonde.fr/pixels/article/2024/02/15/la-france-enterre-son-indice-de-durabilite-des-smartphones_6216693_4408996.html
tags:
- Europe
- Institutions
series:
- 202407
series_weight: 0
---

> Tout en rappelant que son indice était plus ambitieux que celui prévu à l’heure actuelle par l’Union européenne, le ministère de l’écologie a fini par abandonner son projet, auquel la Commission européenne s’était dite défavorable.
