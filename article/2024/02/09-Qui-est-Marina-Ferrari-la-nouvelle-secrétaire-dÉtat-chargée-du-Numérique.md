---
site: Numerama
title: "Qui est Marina Ferrari, la nouvelle secrétaire d'État chargée du Numérique?"
author: Bogdan Bodnar
date: 2024-02-09
href: https://www.numerama.com/politique/1625732-remaniement-ministeriel-qui-est-marina-ferrari-la-nouvelle-secretaire-detat-chargee-du-numerique.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2024/02/ferrari-1024x598.jpg?avif=1&key=5498ac8c
tags:
- Institutions
series:
- 202407
series_weight: 0
---

> Marina Ferrari, députée Modem de Savoie, vient d’être nommée secrétaire d’État chargée du numérique. Elle remplace un collègue du MoDem, Jean-Noël Barrot, désormais ministre en charge de l’Europe.
