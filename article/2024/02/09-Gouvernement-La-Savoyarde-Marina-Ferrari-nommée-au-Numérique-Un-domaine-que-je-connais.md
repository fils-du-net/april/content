---
site: ledauphine.com
title: "Gouvernement. La Savoyarde Marina Ferrari nommée au Numérique: «Un domaine que je connais»"
author: Pierre-Eric Burdin
date: 2024-02-09
href: https://www.ledauphine.com/politique/2024/02/08/la-savoyarde-marina-ferrari-nommee-au-numerique-la-revanche-d-une-fidele-du-modem
featured_image: https://cdn-s-www.ledauphine.com/images/CFEC4A30-EF1C-4702-8A35-304C4D101927/NW_detail/photo-archives-le-dl-1707421585.jpg
tags:
- Institutions
series:
- 202407
---

> À Aix-les-Bains, le nom de Ferrari est un patronyme qui porte, et pas seulement depuis la nomination de Marina Ferrari, la députée MoDem de Savoie, au poste de secrétaire d'État chargée du Numérique ce jeudi 8 février.
