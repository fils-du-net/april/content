---
site: Le Monde.fr
title: "Intelligence artificielle: le partenariat de Mistral AI avec Microsoft fait grincer des dents à Bruxelles (€)"
author: Alexandre Piquard
date: 2024-02-29
href: https://www.lemonde.fr/economie/article/2024/02/29/intelligence-artificielle-le-partenariat-de-mistral-ai-avec-microsoft-fait-grincer-des-dents-a-bruxelles_6219316_3234.html
featured_image: https://img.lemde.fr/2024/02/29/0/0/3000/2000/800/0/75/0/728db69_1709224182949-lemonde-2024-02-20-cha-gonzalez-5217.jpg
tags:
- Sciences
series:
- 202409
---

> Malgré son accord avec le géant américain, la start-up française assure vouloir rester indépendante et ne pas renoncer à publier des modèles en open source.
