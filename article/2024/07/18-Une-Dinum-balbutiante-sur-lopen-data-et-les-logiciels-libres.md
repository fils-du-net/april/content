---
site: Silicon
title: "Une Dinum «balbutiante» sur l'open data et les logiciels libres"
date: 2024-07-18
href: https://www.silicon.fr/dinum-open-data-logiciels-libres-480592.html
featured_image: https://www.silicon.fr/wp-content/uploads/2024/07/Dinum-open-data-logiciels-libres.jpg
tags:
- Institutions
series:
- 202429
---

> Missions historiques de la Dinum, l'ouverture des données publiques et la promotion des logiciels libres sont «inéquitable» et «balbutiante», d'après la Cour des comptes.
