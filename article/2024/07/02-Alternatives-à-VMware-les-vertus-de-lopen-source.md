---
site: Le Monde Informatique
title: "Alternatives à VMware: les vertus de l'open source"
author: Enix
date: 2024-07-02
href: https://www.lemondeinformatique.fr/publi_info/lire-alternatives-a-vmware-les-vertus-de-l-open-source-1068.html
featured_image: https://images.itnewsinfo.com/commun/publi_info/grande/000000097970.png
tags:
- Logiciels privateurs
- Entreprise
series:
- 202427
series_weight: 0
---

> Au moment où les effets de son rachat par Broadcom se concrétisent, l'hégémonie de VMware pourrait être remise en cause par l'essor de solutions alternatives. Certaines entreprises décident de la remplacer à iso-fonctionnalités par d'autres solutions propriétaires, type Nutanix, Citrix ou Microsoft. D'autres, échaudées par le système du vendor lock-in et des tarifs souvent élevés, ne souhaitent pas reproduire le même modèle de dépendance technologique et financière, et se tournent vers des technologies open source comme Proxmox VE ou XCP-NG. De quels atouts l'open source peut-il se prévaloir sur le marché de la virtualisation? En quoi pourrait-il répondre à la nouvelle donne introduite par le rachat de VMware par Broadcom? Quelles alternatives open source considérer?
