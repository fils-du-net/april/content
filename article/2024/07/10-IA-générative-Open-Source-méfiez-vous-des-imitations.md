---
site: cio-online.com
title: "IA générative Open Source: méfiez-vous des imitations"
author: Reynald Fléchaux
date: 2024-07-10
href: https://www.cio-online.com/actualites/lire-ia-generative-open-source-mefiez-vous-des-imitations-15760.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000021082.jpg
tags:
- Sciences
- Europe
series:
- 202428
series_weight: 0
---

> Nombre de modèles d'IA se revendiquent ouverts. Mais ce qualificatif masque des réalités très différentes, souligne une étude.
