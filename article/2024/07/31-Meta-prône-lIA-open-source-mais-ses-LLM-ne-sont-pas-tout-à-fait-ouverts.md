---
site: ICTjournal
title: "Meta prône l'IA open source mais ses LLM ne sont pas tout à fait ouverts"
author: Yannick Chavanne
date: 2024-07-31
href: https://www.ictjournal.ch/articles/2024-07-31/meta-prone-lia-open-source-mais-ses-llm-ne-sont-pas-tout-a-fait-ouverts
featured_image: https://data.ictjournal.ch/styles/np8_full/s3/media/2024/07/31/oli-bekh-tltl7f9axqw-unsplash_web.jpg
tags:
- Sciences
series:
- 202432
---

> En parallèle à la publication de nouvelles versions du LLM Llama de Meta, Mark Zuckerberg a défendu les avantages de l'IA open source. Mais selon des chercheurs, Llama n’est que partiellement ouvert.
