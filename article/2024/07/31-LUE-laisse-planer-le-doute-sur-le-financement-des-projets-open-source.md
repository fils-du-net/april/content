---
site: LeMagIT
title: "L'UE laisse planer le doute sur le financement des projets open source"
author: Gaétan Raoul
date: 2024-07-31
href: https://www.lemagit.fr/actualites/366599554/LUE-laisse-planer-le-doute-sur-le-financement-des-projets-open-source
featured_image: https://www.lemagit.fr/visuals/ComputerWeekly/Hero%20Images/Weather-storm-wind-fotolia.jpg
tags:
- Europe
series:
- 202432
series_weight: 0
---

> Plus de 150 associations et organisations, dont le Conseil National du Logiciel Libre, OW2, OpenStreeMap France et Framasoft, s’inquiètent pour le devenir des projets open source financés par l’initiative Next Generation Internet (NGI) inscrit au programme de la Commission européenne, Horizon Europe.
