---
site: Silicon
title: "Les logiciels libres entrés au SILL au deuxième trimestre 2024"
author: Clément Bohic
date: 2024-07-03
href: https://www.silicon.fr/les-logiciels-libres-entres-au-sill-au-deuxieme-trimestre-2024-480171.html
featured_image: https://www.silicon.fr/wp-content/uploads/2022/10/SILL-septembre-2022.jpg
tags:
- Référentiel
- Administration
series:
- 202427
series_weight: 0
---

> D'AgentJ à YesWiki, voici les dernières entrées au SILL (Socle interministériel de logiciels libres).
