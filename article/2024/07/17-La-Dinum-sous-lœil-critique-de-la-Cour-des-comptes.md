---
site: Next
title: "La Dinum sous l'œil critique de la Cour des comptes"
author: Martin Clavey
date: 2024-07-17
href: https://next.ink/144004/la-dinum-sous-loeil-critique-de-la-cour-des-comptes/
featured_image: https://next.ink/wp-content/uploads/2023/12/Souverainete-echanges.jpg
tags:
- Institutions
- Open Data
series:
- 202429
series_weight: 0
---

> Cinq ans ou presque après sa création, la Cour des comptes se penche sur la Direction interministérielle du numérique. Elle demande la clarification de sa stratégie et de son budget pour évaluer ses dépenses convenablement. Elle pointe aussi le besoin d'une vigilance dans l’exercice de ses missions historiques que sont la promotion de la donnée publique, assurée par Etalab, et le réseau interministériel de l’État.
