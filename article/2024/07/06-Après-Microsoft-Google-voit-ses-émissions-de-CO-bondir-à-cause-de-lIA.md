---
site: Le Monde.fr
title: "Après Microsoft, Google voit ses émissions de CO₂ bondir à cause de l'IA (€)"
author: Alexandre Piquard
date: 2024-07-06
href: https://www.lemonde.fr/economie/article/2024/07/02/apres-microsoft-google-voit-ses-emissions-de-co2-bondir-a-cause-de-l-ia_6246155_3234.html
featured_image: https://img.lemde.fr/2024/07/02/0/0/4813/3208/800/0/75/0/38c3c5b_cd8a6e8ceafe4559817e4a0d16f14fcb-1-e39068c62d104a1e9325c57b2eb62456.jpg
tags:
- Sciences
- Entreprise
series:
- 202427
series_weight: 0
---

> En raison des besoins en calcul informatique de l'intelligence artificielle, le groupe a vu ses émissions de gaz à effet de serre augmenter de 13 % en un an et de 48 % en cinq ans.
