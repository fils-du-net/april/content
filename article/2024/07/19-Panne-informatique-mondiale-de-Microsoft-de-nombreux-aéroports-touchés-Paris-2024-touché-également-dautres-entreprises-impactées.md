---
site: L'OBS
title: "Panne informatique mondiale de Microsoft: de nombreux aéroports touchés, Paris 2024 touché également, d'autres entreprises impactées"
date: 2024-07-19
author: Hendrik Schmidt
href: https://www.nouvelobs.com/monde/20240719.OBS91322/une-panne-informatique-mondiale-provoque-d-importantes-perturbations-dans-les-aeroports.html
featured_image: https://focus.nouvelobs.com/2020/07/01/0/131/2727/1818/1020/680/50/0/fd797a7_107875333-Airbus.jpg
tags:
- Logiciels privateurs
- International
series:
- 202429
series_weight: 0
---

> Une panne informatique géante touche de nombreuses entreprises dans le monde, dont les aéroports. Le géant de la tech Microsoft a déclaré vendredi qu'il était en train de prendre des «mesures d'atténuation».
