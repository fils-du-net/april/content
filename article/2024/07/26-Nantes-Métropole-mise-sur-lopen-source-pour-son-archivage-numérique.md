---
site: Le Monde Informatique
title: "Nantes Métropole mise sur l'open source pour son archivage numérique"
author: Dominique Filippone
date: 2024-07-26
href: https://www.lemondeinformatique.fr/actualites/lire-nantes-metropole-mise-sur-l-open-source-pour-son-archivage-numerique-94373.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000098288.jpg
tags:
- Administration
series:
- 202430
series_weight: 0
---

> Externalisation: Regroupant 24 communes dans le département de Loire Atlantique, Nantes Métropole a confié à Atos le déploiement de son système d'archivage électronique reposant sur la solution open source Vitam conçue et développée par l'Etat.
