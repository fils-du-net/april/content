---
site: clubic.com
title: "Cette région française s'affranchit des GAFAM et opte pour des solutions maison"
author: Mélina Loupia
date: 2024-07-19
href: https://www.clubic.com/actualite-532983-cette-region-francaise-s-affranchit-des-gafam-et-opte-pour-des-solutions-maison.html
featured_image: https://pic.clubic.com/b0b3ee842182984/1600x1068/smart/ge-ants-de-la-tech-gafam.webp
tags:
- Administration
series:
- 202429
series_weight: 0
---

> Dans une démarche qui vise à regagner en autonomie et à réduire les coûts liés au fonctionnement de ses collectivités territoriales, une région française a décidé de remplacer les solutions des géants du numérique par des alternatives locales et open source.
