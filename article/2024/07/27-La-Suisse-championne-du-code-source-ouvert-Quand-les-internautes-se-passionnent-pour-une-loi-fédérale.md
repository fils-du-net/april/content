---
site: Le Temps
title: "La Suisse championne du code source ouvert? Quand les internautes se passionnent pour une loi fédérale"
date: 2024-07-27
href: https://www.letemps.ch/cyber/la-suisse-championne-du-code-source-ouvert-quand-les-internautes-se-passionnent-pour-une-loi-federale
featured_image: https://letemps-17455.kxcdn.com/photos/93b1b280-a6b4-4c77-98c9-98dd6a74b43f/giant.avif
tags:
- International
series:
- 202432
series_weight: 0
---

> Une disposition légale impose à la Confédération de publier le code source des logiciels qu'elle développe. Un média américain a surinterprété la mesure et a suscité des réactions enthousiastes sur les réseaux sociaux, y compris d'Elon Musk
