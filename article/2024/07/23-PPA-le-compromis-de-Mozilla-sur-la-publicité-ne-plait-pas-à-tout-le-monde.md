---
site: Next
title: "PPA: le compromis de Mozilla sur la publicité ne plait pas à tout le monde"
description: "Mal nécessaire?"
author: Vincent Hermann
date: 2024-07-23
href: https://next.ink/144565/ppa-le-compromis-de-mozilla-sur-la-publicite-ne-plait-pas-a-tout-le-monde
featured_image: https://next.ink/wp-content/uploads/2024/01/FX_Design_Blog_Header_1400x770.jpg
tags:
- Vie privée
series:
- 202430
series_weight: 0
---

> Avec Firefox 128, Mozilla a introduit une fonction baptisée PPA, pour Privacy-Preserving Attribution. Mais entre cette arrivée sans crier gare et le fonctionnement – pas toujours bien compris – du mécanisme, les critiques ont fusé.
