---
site: Alliancy
title: "Open source pour réduire l'obsolescence matérielle"
author: Rémy Marrone
date: 2024-07-08
href: https://www.alliancy.fr/open-source-reduire-obsolescence-materielle
featured_image: https://www.alliancy.fr/wp-content/uploads/2024/07/lopen-source-cle-pour-reduire-lobsolescence-materielle-740x494.jpg
tags:
- Sensibilisation
series:
- 202428
series_weight: 0
---

> Les entreprises et les utilisateurs finaux se saisissent encore peu ou mal de l'open source. Les règles sont parfois mal connues et les clichés encore nombreux
