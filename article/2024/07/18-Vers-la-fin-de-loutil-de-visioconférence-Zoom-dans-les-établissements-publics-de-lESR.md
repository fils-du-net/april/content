---
site: Campus Matin
title: "Vers la fin de l'outil de visioconférence Zoom dans les établissements publics de l'ESR?"
author: Isabelle Cormaty
date: 2024-07-18
href: https://www.campusmatin.com/numerique/equipements-systemes-informations/l-open-source-se-fait-une-place-parmi-les-outils-de-visioconference-utilises-par-les-etablissements.html
featured_image: https://geimage.newstank.fr/image/cms/3d55429a9dbb5402825d10b67d4bdef4/ministere-propose-depuis-mars-2023-suite-visioconference-bbb-etablissements.jpg?fm=browser&fill=auto&crop=157%2C94%2C1607%2C904&w=620&h=349&s=84a5b56bbc3e41a6fb3f4697ab3aca98
tags:
- Éducation
series:
- 202430
series_weight: 0
---

> En mars 2023, le ministère de l'enseignement supérieur a mis à disposition des établissements publics la solution de classe virtuelle BigBlueButton (BBB). Si l'offre a suscité de nombreuses critiques à son démarrage, elle est davantage appréciée aujourd'hui après l'augmentation des tarifs de Zoom.
