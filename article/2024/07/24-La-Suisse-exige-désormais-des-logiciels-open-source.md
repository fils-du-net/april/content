---
site: ZDNET
title: "La Suisse exige désormais des logiciels open source"
author: Steven Vaughan-Nichols
date: 2024-07-24
href: https://www.zdnet.fr/actualites/la-suisse-exige-desormais-des-logiciels-open-source-395030.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/02/drapeau-Suisse-WP-1-737x410.jpg
tags:
- Administration
- International
series:
- 202430
series_weight: 0
---

> La Suisse vient de faire un grand pas en avant avec une loi révolutionnaire qui impose l'utilisation de logiciels libres dans le secteur public du pays. Explications.
