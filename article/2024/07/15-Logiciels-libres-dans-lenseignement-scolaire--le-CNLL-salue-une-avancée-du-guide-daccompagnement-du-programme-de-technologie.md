---
site: ITRmanager.com
title: "Logiciels libres dans l’enseignement scolaire: le CNLL salue une avancée du guide d’accompagnement du programme de technologie"
date: 2024-07-15
href: https://itrmanager.com/articles/203140/logiciels-libres-dans-lenseignement-scolaire-le-cnll-salue-une-avancee-du-guide-daccompagnement-du-programme-de-technologie.html
tags:
- Éducation
series:
- 202429
series_weight: 0
---

> Le Guide d’accompagnement du programme de technologie au cycle 4, daté de février 2024, recommande de “privilégier” les logiciels libres dans de nombreuses activités pédagogiques et technologiques. Stefane Fermigier, du CNLL, Conseil National du Logiciel Libre, se réjouit de cette avancée significative dans l’intégration des logiciels libres au sein des programmes éducatifs français. Toutefois, le CNLL regrette que cette évolution ait nécessité 12 ans et que les recommandations concernant les logiciels libres demeurent principalement confinées à l’enseignement technologique.
