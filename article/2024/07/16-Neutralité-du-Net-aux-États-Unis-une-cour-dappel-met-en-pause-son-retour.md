---
site: Next
title: "Neutralité du Net: aux États-Unis, une cour d'appel met en pause son retour"
author: Sébastien Gavois
date: 2024-07-16
href: https://next.ink/brief_article/neutralite-du-net-aux-etats-unis-une-cour-dappel-met-en-pause-son-retour
tags:
- Neutralité du Net
- International
series:
- 202429
series_weight: 0
---

> La neutralité du Net aux États-Unis ressemble un peu au jeu du chat et de la souris avec la FCC. Elle a tout d’abord été instaurée sous la présidence de Barack Obama en 2015. Elle a ensuite été supprimée en 2018 lorsque Donald Trump était président et qu’il avait positionné Ajit Pai à la tête de la FCC.
