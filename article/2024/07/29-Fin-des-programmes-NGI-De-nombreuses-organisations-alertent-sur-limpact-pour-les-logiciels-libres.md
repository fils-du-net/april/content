---
site: ZDNET
title: "Fin des programmes NGI? De nombreuses organisations alertent sur l'impact pour les logiciels libres"
author: Thierry Noisette
date: 2024-07-29
href: https://www.zdnet.fr/blogs/fin-des-programmes-ngi-de-nombreuses-organisations-alertent-sur-limpact-pour-les-logiciels-libres-395258.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/07/europe-2021308_960_720.jpg
tags:
- Europe
series:
- 202432
---

> L'arrêt des programmes européens Next Generation Internet (NGI), qui financent depuis 2020 des communs numériques, est dénoncé par le CNLL, Petites Singularités, Framasoft, OW2, OpenStreetMap France, Linuxfr et beaucoup d'autres.
