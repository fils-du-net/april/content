---
site: Developpez.com
title: "La Suisse exige que tous les logiciels développés pour le gouvernement soient open source, avec divulgation du code source du logiciel"
description: Ce qui constitue une étape importante sur le plan juridique
author: Jade Emy
date: 2024-07-16
href: https://droit.developpez.com/actu/360338/La-Suisse-exige-que-tous-les-logiciels-developpes-pour-le-gouvernement-soient-open-source-avec-divulgation-du-code-source-du-logiciel-ce-qui-constitue-une-etape-importante-sur-le-plan-juridique
featured_image: https://www.developpez.com/images/logos/suisse.png
tags:
- Administration
- Institutions
- International
series:
- 202429
series_weight: 0
---

> La Suisse impose la divulgation du code source des logiciels pour le secteur public, ce qui est une étape importante sur le plan juridique
