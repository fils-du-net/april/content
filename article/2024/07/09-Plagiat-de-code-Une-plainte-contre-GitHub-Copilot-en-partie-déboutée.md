---
site: Le Monde Informatique
title: "Plagiat de code: Une plainte contre GitHub Copilot en partie déboutée"
author: Jacques Cheminat
date: 2024-07-09
href: https://www.lemondeinformatique.fr/actualites/lire-plagiat-de-code-une-plainte-contre-github-copilot-en-partie-deboutee-94234.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000098079.png
tags:
- Droit d'auteur
- Institutions
series:
- 202428
series_weight: 0
---

> Dans l'action collective menée par des développeurs contre l'assistant Copilot de GitHub, un juge a rejeté l'accusation principale de violation des termes des licences open source. Il laisse par contre la porte ouverte à deux autres charges à étayer.
