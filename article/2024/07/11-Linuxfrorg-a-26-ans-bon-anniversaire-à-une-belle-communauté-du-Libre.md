---
site: ZDNET
title: "Linuxfr.org a 26 ans: bon anniversaire à une belle communauté du Libre"
author: Thierry Noisette
date: 2024-07-11
href: https://www.zdnet.fr/blogs/l-esprit-libre/linuxfr-org-a-26-ans-bon-anniversaire-a-une-belle-communaute-du-libre-394572.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/07/colonie-de-manchots-de-crozet-_-plage-du-port-de-crozet-_-flickr-750x410.jpg
tags:
- Associations
- Partage du savoir
- April
series:
- 202428
series_weight: 0
---

> 117.000 contenus et 1,91 million de commentaires ont été mis en ligne dans le quart de siècle plus un an que vient de passer ce très utile site dédié aux logiciels libres et sujets proches.
