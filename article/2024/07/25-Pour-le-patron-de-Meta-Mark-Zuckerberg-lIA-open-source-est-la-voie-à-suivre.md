---
site: l'Opinion
title: "Pour le patron de Meta, Mark Zuckerberg, «l’IA open source est la voie à suivre»"
date: 2024-07-25
href: https://www.lopinion.fr/economie/pour-le-patron-de-meta-mark-zuckerberg-lia-open-source-est-la-voie-a-suivre
featured_image: https://beymedias.brightspotcdn.com/dims4/default/965b606/2147483647/strip/true/crop/1024x534+0+76/resize/1680x876!/format/webp/quality/90/?url=https%3A%2F%2Fassets.beymedias.fr%2Fafp%2Fce44f9345152fba9373a6c5de6b44d21dc75ea8a.jpeg
tags:
- Sciences
series:
- 202432
---

> Washington - Mark Zuckerberg, fondateur et patron de Meta (Facebook, Instagram), est devenu une figure de proue inattendue du développement de l’intelligence artificielle (IA) en open source, face aux modèles fermés d’OpenAI (ChatGPT) et de Google
