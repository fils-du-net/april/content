---
site: 01net.
title: "Peu utilisés, instables... les logiciels made in France de l'État coûtent un pognon de dingue"
author: Stéphanie Bascou
date: 2024-07-21
href: https://www.01net.com/actualites/peu-utilises-instables-les-logiciels-made-in-france-de-letat-coutent-un-pognon-de-dingue.html
featured_image: https://www.01net.com/app/uploads/2024/07/Design-sans-titre186-1360x907.jpg
tags:
- Administration
- Institutions
series:
- 202430
series_weight: 0
---

> La Cour des comptes, qui évalue les actions faites en matière de numérique de l'administration, livre un bilan plus que mitigé de «la suite numérique de l'agent public», un ensemble de services développés en interne par et pour l'administration qui vise à concurrencer Google doc ou Slack.
