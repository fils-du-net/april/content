---
site: www.usine-digitale.fr
title: "Cyberattaques sur la supply chain logicielle, talon d'Achille des grandes entreprises"
author: Yoann Bourgin
date: 2024-07-06
href: https://www.usine-digitale.fr/article/cyberattaques-sur-la-supply-chain-logicielle-talon-d-achille-des-grandes-entreprises.N2215388
featured_image: https://www.usine-digitale.fr/mediatheque/8/4/4/001491448_896x598_c.jpg
tags:
- Innovation
- Entreprise
series:
- 202427
series_weight: 0
---

> Une étude révèle une augmentation des attaques sur la supply chain logicielle auxquelles les entreprises sont très vulnérables. Cette "chaîne d'approvisionnement" numérique regroupe l’ensemble des composants nécessaires au développement, à la création et à la publication de logiciels. Au cours des deux dernières années, six grandes entreprises sur dix ont été visées par une cyberattaque de ce type.
