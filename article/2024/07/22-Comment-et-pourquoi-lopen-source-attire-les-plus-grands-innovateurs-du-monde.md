---
site: ZDNET
title: "Comment (et pourquoi) l'open source attire les plus grands innovateurs du monde"
author: Steven Vaughan-Nichols
date: 2024-07-22
href: https://www.zdnet.fr/actualites/comment-et-pourquoi-lopen-source-attire-les-plus-grands-innovateurs-du-monde-394904.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/03/OpenSourceSoftware__w630-1.jpg
tags:
- Innovation
series:
- 202430
series_weight: 0
---

> Certains des meilleurs profils IT travaillent dans le domaine de l'open source. Qui sont ces personnes? Comment sont-elles arrivées là? Quelle est leur vision de l'avenir de l'open source?
