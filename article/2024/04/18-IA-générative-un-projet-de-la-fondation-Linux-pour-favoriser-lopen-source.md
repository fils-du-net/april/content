---
site: Siècle Digital
title: "IA générative: un projet de la fondation Linux pour favoriser l'open source"
author: Julia Guinamard
date: 2024-04-18
href: https://siecledigital.fr/2024/04/18/ia-generative-un-projet-de-la-fondation-linux-pour-favoriser-lopen-source
featured_image: https://siecledigital.fr/wp-content/uploads/2024/04/intelligence-artificielle-1400x960.jpg
tags:
- Sciences
series:
- 202416
---

> La fondation Linux a lancé un projet pour favoriser le développement open source de l'IA générative, en partenariat avec des entreprises, dont Intel.
