---
site: 01net.
title: "VMWare: Pourquoi ces entreprises européennes sont-elles vent debout contre la société américaine Broadcom?"
author: Stéphanie Bascou
date: 2024-04-14
href: https://www.01net.com/actualites/vmware-pourquoi-ces-entreprises-europeennes-sont-elles-vent-debout-contre-la-societe-americaine-broadcom.html
featured_image: https://www.01net.com/app/uploads/2024/04/Design-sans-titre140-1360x907.jpg
tags:
- Informatique en nuage
- Logiciels privateurs
- Europe
series:
- 202415
series_weight: 0
---

> Les sociétés européennes appellent la Commission européenne à l'aide, depuis que la société américaine Broadcom, qui a racheté VMWare, a augmenté ses prix et rompu certains de ses contrats. Les sociétés européennes, grandes utilisatrices et dépendantes des logiciels VMWare pour faire tourner leurs infrastructures informatiques, se retrouvent contraintes d'accepter des prix et des conditions qu'elles estiment exorbitants et inacceptables.
