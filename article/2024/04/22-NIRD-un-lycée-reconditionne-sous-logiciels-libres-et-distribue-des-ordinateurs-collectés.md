---
site: ZDNET
title: "NIRD: un lycée reconditionne sous logiciels libres et distribue des ordinateurs collectés"
author: Thierry Noisette
date: 2024-04-22
href: https://www.zdnet.fr/blogs/nird-un-lycee-reconditionne-sous-logiciels-libres-et-distribue-des-ordinateurs-collectes-390954.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/04/pc_portable_don-1024x576-1-750x410.jpg
tags:
- Éducation
series:
- 202417
series_weight: 0
---

> Au lycée Carnot (62), des élèves mettent sous GNU/Linux des PC récupérés, pour équiper des familles et des écoles primaires.
