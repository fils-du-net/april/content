---
site: ZDNET
title: "Commun numérique: Panoramax, une base de photos de vues de terrain immersives"
author: Thierry Noisette
date: 2024-04-20
href: https://www.zdnet.fr/blogs/l-esprit-libre/commun-numerique-panoramax-une-base-de-photos-de-vues-de-terrain-immersives-390872.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/04/panoramax_france-712x410.jpg
tags:
- Partage du savoir
- Open Data
- Licenses
series:
- 202416
series_weight: 0
---

> Parrainée par l'IGN et OpenStreetMap France, cette alternative libre à des services comme Google Street View contient déjà près de 18 millions de photos.
