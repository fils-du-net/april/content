---
site: Next
title: "L'enseignement supérieur veut désengager ses services numériques des GAFAM"
description: Un guide un peu flou
author: Martin Clavey
date: 2024-04-11
href: https://next.ink/134063/lenseignement-superieur-veut-desengager-ses-services-numeriques-des-gafam
featured_image: https://next.ink/wp-content/uploads/2024/04/scott-graham-5fNmWej4tAA-unsplash-1536x1025.jpg
tags:
- Éducation
series:
- 202415
series_weight: 0
---

> Depuis la feuille de route 2009-2013, le ministère de l'Enseignement supérieur et de la recherche n'avait plus de plan de coordination du numérique. Un nouveau document fournit des lignes directrices pour la période 2023-2027.
