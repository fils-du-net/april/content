---
site: LeMagIT
title: "Selon les experts, le rachat d’HashiCorp par IBM pourrait modifier son équation open source"
author: Beth Pariseau, Gaétan Raoul
date: 2024-04-25
href: https://www.lemagit.fr/actualites/366582152/Selon-les-experts-le-rachat-dHashiCorp-par-IBM-pourrait-modifier-son-equation-open-source
featured_image: https://cdn.ttgtmedia.com/visuals/German/article/cloud-choice-decision-crossroad-adobe.jpg
tags:
- Entreprise
series:
- 202417
series_weight: 0
---

> IBM va acquérir HashiCorp pour 6,5 milliards de dollars, une opération qui ouvre un débat parmi les professionnels de l’informatique sur la façon dont la société mère de Red Hat pourrait aborder la gestion de l’open source dans sa future filiale.
