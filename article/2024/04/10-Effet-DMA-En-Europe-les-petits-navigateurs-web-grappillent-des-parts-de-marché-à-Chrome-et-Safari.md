---
site: Siècle Digital
title: "Effet DMA? En Europe, les petits navigateurs web grappillent des parts de marché à Chrome et Safari"
description: "Il se pourrait que le règlement sur les marchés numériques fasse déjà son effet."
author: Zacharie Tazrout
date: 2024-04-10
href: https://siecledigital.fr/2024/04/10/effet-dma-en-europe-les-petits-navigateurs-web-grappillent-des-parts-de-marche-a-chrome-et-safari
featured_image: https://siecledigital.fr/wp-content/uploads/2024/04/duckduckgo-1-1400x805.jpg.webp
tags:
- Europe
- Internet
- Entreprise
series:
- 202415
series_weight: 0
---

> Sur le vieux continent, les navigateurs à la part de marché modeste ont vu le nombre d'utilisateurs grimper le mois dernier.
