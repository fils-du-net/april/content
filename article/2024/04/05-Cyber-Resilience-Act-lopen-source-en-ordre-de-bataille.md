---
site: Silicon
title: "Cyber Resilience Act: l'open source en ordre de bataille"
author: Clément Bohic
date: 2024-04-05
href: https://www.silicon.fr/cyber-resilience-act-open-source-477487.html
featured_image: https://www.silicon.fr/wp-content/uploads/2024/04/Cyber-Resilience-Act-open-source.jpeg
tags:
- Europe
series:
- 202414
series_weight: 0
---

> Des fondations open source s'allient pour peser dans la définition des spécifications communes qui accompagneront la mise en application du Cyber Resilience Act.
