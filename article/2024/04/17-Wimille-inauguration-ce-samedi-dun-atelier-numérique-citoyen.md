---
site: La Voix du Nord
title: "Wimille: inauguration ce samedi d'un atelier numérique citoyen"
author: Olivier Roussel
date: 2024-04-17
href: https://www.lavoixdunord.fr/1452975/article/2024-04-17/wimille-inauguration-ce-samedi-d-un-atelier-numerique-citoyen
featured_image: https://lvdneng.rosselcdn.net/sites/default/files/dpistyles_v2/vdn_864w/2024/04/17/node_1452975/58425975/public/2024/04/17/15969117.jpeg?itok=1kLwaPer1713349268
tags:
- Promotion
- April
series:
- 202416
series_weight: 0
---

> Dans le cadre de la 1re édition dans le Boulonnais et le Pas-de-Calais de l’événement national «Libre en fête», organisé par l’APRIL association et promouvant la culture et le logiciel libres, la médiathèque de Wimille (espace Pilâtre de Rozier) ouvre les portes de son nouvel atelier numérique citoyen ce samedi à partir de 10h.
