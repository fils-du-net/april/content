---
site: Ars Technica
title: "German state gov. ditching Windows for Linux, 30K workers migrating"
description: Schleswig-Holstein looks to succeed where Munich failed.
author: Scharon Harding
date: 2024-04-05
href: https://arstechnica.com/information-technology/2024/04/german-state-gov-ditching-windows-for-linux-30k-workers-migrating
featured_image: https://cdn.arstechnica.net/wp-content/uploads/2024/04/GettyImages-169475683-800x450.jpg
tags:
- Administration
- International
- English
series:
- 202414
series_weight: 0
---

> Schleswig-Holstein, one of Germany’s 16 states, on Wednesday confirmed plans to move tens of thousands of systems from Microsoft Windows to Linux. The announcement follows previously established plans to migrate the state government off Microsoft Office in favor of open source LibreOffice.
