---
site: Next
title: "Des institutions internationales s'engagent pour l'ouverture des données sur la recherche (€)"
description: "Open as a business model?"
author: Martin Clavey
date: 2024-04-22
href: https://next.ink/134903/des-institutions-internationales-sengagent-pour-louverture-des-donnees-sur-la-recherche
featured_image: https://next.ink/wp-content/uploads/2024/03/karolina-grabowska-unsplash-1536x1024.jpg
tags:
- Sciences
- Partage du savoir
series:
- 202417
series_weight: 0
---

> Plus de 30 institutions internationales de recherche appellent dans une déclaration leur communauté à s'engager dans l'utilisation de plateformes ouvertes d'analyse de données sur la recherche et ainsi abandonner les outils d'analyse propriétaires comme ceux des multinationales Elsevier et Clarivate Analytics.
