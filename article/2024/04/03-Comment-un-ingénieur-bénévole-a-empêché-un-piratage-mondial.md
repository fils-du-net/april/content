---
site: BFMtv
title: "Comment un ingénieur bénévole a empêché un piratage mondial"
author: Kesso Diallo
date: 2024-04-03
href: https://www.bfmtv.com/tech/cybersecurite/comment-un-ingenieur-benevole-a-empeche-un-piratage-mondial_AV-202404030414.html
tags:
- Sensibilisation
series:
- 202414
---

> Andres Freund, développeur chez Microsoft a découvert une porte dérobée dans un ensemble de logiciels gratuits de compression de données très utilisé dans les systèmes d’exploitation Linux.
