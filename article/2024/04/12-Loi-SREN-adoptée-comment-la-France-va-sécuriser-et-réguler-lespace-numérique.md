---
site: Next
title: "Loi SREN adoptée: comment la France va sécuriser et réguler l'espace numérique"
description: Avec des tours de passe-passe
author: Sébastien Gavois
date: 2024-04-12
href: https://next.ink/134116/loi-sren-adoptee-comment-la-france-va-securiser-et-reguler-lespace-numerique
featured_image: https://next.ink/wp-content/uploads/2023/11/harcelementenligne9.png
tags:
- Internet
series:
- 202415
---

> Le projet de loi pour Sécuriser et réguler l'espace numérique (SREN) a été définitivement adopté cette semaine.
