---
site: Silicon
title: "GenAI: face à NVIDIA, Intel se tourne vers l'open source"
author: Clément Bohic
date: 2024-04-17
href: https://www.silicon.fr/genai-nvidia-intel-open-source-477866.html
featured_image: https://www.silicon.fr/wp-content/uploads/2024/04/Intel-OPEA-GenAI.jpg
tags:
- Sciences
- Entreprise
series:
- 202416
series_weight: 0
---

> Intel se greffe au projet OPEA (Open Enterprise Platform for AI) et y pousse des implémentations GenAI optimisées pour ses accélérateurs Gaudi.
