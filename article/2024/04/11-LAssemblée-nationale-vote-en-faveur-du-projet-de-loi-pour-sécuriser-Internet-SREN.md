---
site: Siècle Digital
title: "L'Assemblée nationale vote en faveur du projet de loi pour sécuriser Internet (SREN)"
description: "La loi a dû être réécrite afin de répondre aux exigences du droit européen."
author: Zacharie Tazrout
date: 2024-04-11
href: https://siecledigital.fr/2024/04/11/lassemblee-nationale-vote-en-faveur-du-projet-de-loi-pour-securiser-internet-sren
featured_image: https://siecledigital.fr/wp-content/uploads/2024/02/2863106566_5fca105862_o-1.jpg
tags:
- Internet
series:
- 202415
---

> Le projet de loi adopté par l'Assemblée nationale vise à lutter contre les dérives sur internet: escroqueries, harcèlement, etc.
