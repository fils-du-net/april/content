---
site: ZDNET
title: "Journée du Libre éducatif 2024: 14 projets libres pour la communauté scolaire"
author: Thierry Noisette
date: 2024-04-18
href: https://www.zdnet.fr/blogs/journee-du-libre-educatif-2024-14-projets-libres-pour-la-communaute-scolaire-390715.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/04/jdle24-750x410.jpg
tags:
- Éducation
- Promotion
series:
- 202416
series_weight: 0
---

> La nouvelle édition de la JDLE, créée en 2022 par le ministère de l'Education nationale, a présenté de nombreux projets utiles aux élèves, aux enseignants voire au-delà.
