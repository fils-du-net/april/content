---
site: LaDepeche.fr
title: "Foix. Ils ont appris à maîtriser les logiciels libres"
date: 2024-04-12
href: https://www.ladepeche.fr/2024/04/12/ils-ont-appris-a-maitriser-les-logiciels-libres-11886534.php
featured_image: https://images.ladepeche.fr/api/v1/images/view/6618a881c768ce488519693c/large/image.jpg
tags:
- Associations
- Promotion
series:
- 202415
series_weight: 0
---

> La Fédération départementale des foyers ruraux de l’Ariège organisait, samedi 6 avril, une journée consacrée aux logiciels libres. Faire durer les ordinateurs grâce aux logiciels libres, tel était l’objectif principal.
