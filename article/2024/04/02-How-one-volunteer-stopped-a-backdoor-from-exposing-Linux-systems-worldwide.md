---
site: The Verge
title: "How one volunteer stopped a backdoor from exposing Linux systems worldwide"
author: Amrita Khalid
date: 2024-04-02
href: https://www.theverge.com/2024/4/2/24119342/xz-utils-linux-backdoor-attempt
featured_image: https://duet-cdn.vox-cdn.com/thumbor/0x0:2040x1360/828x552/filters:focal(1020x680:1021x681):format(webp)/cdn.vox-cdn.com/uploads/chorus_asset/file/23318435/akrales_220309_4977_0232.jpg
tags:
- Sensibilisation
- English
series:
- 202414
---

> An off-the-clock Microsoft worker prevented malicious code from spreading into widely-used versions of Linux via a compression format called XZ Utils.
