---
site: LeMagIT
title: "Les fondations open source vues d'un bon œil par les investisseurs (Serena Capital)"
author: Gaétan Raoul
date: 2024-04-24
href: https://www.lemagit.fr/actualites/366582073/Les-fondations-open-source-vues-dun-bon-il-par-les-investisseurs-Serena-Capital
featured_image: https://cdn.ttgtmedia.com/visuals/German/article/open-source-1-fotolia.jpg
tags:
- Économie
- Licenses
series:
- 202417
series_weight: 0
---

> Outre une croissance forte des investissements en provenance de l’Europe, Serena Capital observe un cercle vertueux. Les startups dont les solutions commerciales s’appuient sur des projets open source soutenus par des fondations open source seraient plus susceptibles de bénéficier d’un large soutien financier.
