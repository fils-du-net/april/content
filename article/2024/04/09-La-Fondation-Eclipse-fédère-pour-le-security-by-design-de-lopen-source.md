---
site: Le Monde Informatique
title: "La Fondation Eclipse fédère pour le security by design de l'open source"
author: Paul Krill
date: 2024-04-09
href: https://www.lemondeinformatique.fr/actualites/lire-la-fondation-eclipse-federe-pour-le-security-by-design-de-l-open-source-93450.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000096830.jpg
tags:
- Sensibilisation
- Internet
- Innovation
- Europe
series:
- 202415
series_weight: 0
---

> La Fondation Eclipse veut établir des spécifications communes pour le développement de logiciels sécurisés sur la base des meilleures pratiques open source existantes.
