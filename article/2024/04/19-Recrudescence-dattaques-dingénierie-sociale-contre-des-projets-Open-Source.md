---
site: LeMagIT
title: "Recrudescence d’attaques d’ingénierie sociale contre des projets Open Source"
author: Alex Scroxton
date: 2024-04-19
href: https://www.lemagit.fr/actualites/366581494/Recrudescence-dattaques-dingenierie-sociale-contre-des-projets-Open-Source
featured_image: https://cdn.ttgtmedia.com/visuals/ComputerWeekly/HeroImages/code-metadata-programming-maciek905-adobe.jpg
tags:
- Sensibilisation
series:
- 202416
series_weight: 0
---

> À la suite de la récente alerte concernant XZ Utils, les responsables d’un autre projet open source se sont manifestés pour dire qu’ils avaient peut-être été victimes d’attaques similaires d’ingénierie sociale.
