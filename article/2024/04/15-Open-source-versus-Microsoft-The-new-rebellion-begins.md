---
site: The Register
title: "Open source versus Microsoft: The new rebellion begins"
author: Rupert Goodwins
date: 2024-04-15
href: https://www.theregister.com/2024/04/15/opinion_microsoft_sovereignty
tags:
- Administration
- International
- English
series:
- 202416
---

> Neither side can afford to lose, but one surely must
