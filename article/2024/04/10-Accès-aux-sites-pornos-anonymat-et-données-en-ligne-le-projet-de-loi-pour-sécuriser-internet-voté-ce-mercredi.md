---
site: Sud Ouest
title: "Accès aux sites pornos, anonymat et données en ligne: le projet de loi pour «sécuriser» internet voté ce mercredi"
date: 2024-04-10
href: https://www.sudouest.fr/sciences-et-technologie/acces-aux-sites-pornos-anonymat-et-donnees-en-ligne-le-projet-de-loi-pour-securiser-internet-vote-ce-mercredi-19277188.php
featured_image: https://media.sudouest.fr/19277188/1000x500/porno.jpg?v=1712728114
tags:
- Internet
- Associations
- International
series:
- 202415
series_weight: 0
---

> Le projet de loi pour sécuriser internet devrait être adopté largement même si certains se montrent réservés, entre craintes pour les libertés publiques et manque d’ambition sur l’encadrement des sites pornographiques
