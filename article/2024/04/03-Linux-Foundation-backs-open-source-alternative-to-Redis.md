---
site: The Register
title: "Linux Foundation backs open source alternative to Redis"
author: Lindsay Clark
date: 2024-04-03
href: https://www.theregister.com/2024/04/03/open_source_redis_alternative/
featured_image: https://www.theregister.com/design_picker/ae01b183a707a7db8cd5f2c947715ed56d335138/graphics/std/user_icon_white_extents_16x16.png
tags:
- Licenses
- English
series:
- 202414
---

> Follows the vendor's decision to overhaul licensing of the popular cache database
