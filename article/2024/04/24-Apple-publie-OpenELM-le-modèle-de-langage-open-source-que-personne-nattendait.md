---
site: Numerama
title: "Apple publie OpenELM, le modèle de langage open source que personne n'attendait"
description: "Un aperçu d'iOS 18"
author: Nicolas Lellouche
date: 2024-04-24
href: https://www.numerama.com/tech/1732480-apple-publie-openelm-le-modele-de-langage-open-source-que-personne-nattendait.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2023/06/siri-3d-1024x576.jpg?avif=1&key=ece8a2f1
tags:
- Sciences
- Entreprise
series:
- 202417
series_weight: 0
---

> Décliné en quatre versions, le modèle de langage OpenELM est la première approche open source d'Apple sur le terrain de l'intelligence artificielle générative. La marque, habituée au secret, va-t-elle aborder une stratégie différente face à OpenAI? L'avenir de l'intelligence artificielle générative sera-t-il open source? Deux visions cohabitent au sein de la Silicon Valley.
