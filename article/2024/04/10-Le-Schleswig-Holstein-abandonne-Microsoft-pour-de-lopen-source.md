---
site: Le Monde Informatique
title: "Le Schleswig-Holstein abandonne Microsoft pour de l'open source"
author: Manfred Bremmer
date: 2024-04-10
href: https://www.lemondeinformatique.fr/actualites/lire-le-schleswig-holstein-abandonne-microsoft-pour-de-l-open-source-93456.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000096836.jpg
tags:
- Administration
series:
- 202415
series_weight: 0
---

> Pour des raisons de souveraineté numérique, l'administration du Land allemand du Schleswig-Holstein va abandonner Windows et Microsoft Office. Les 30 000 employés vont progressivement travailler sur un environnement purement open source.
