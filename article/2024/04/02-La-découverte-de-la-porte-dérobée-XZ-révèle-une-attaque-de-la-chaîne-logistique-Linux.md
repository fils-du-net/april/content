---
site: LeMagIT
title: "La découverte de la porte dérobée XZ révèle une attaque de la chaîne logistique Linux"
author: Rob Wright, Alexander Culafi
date: 2024-04-02
href: https://www.lemagit.fr/actualites/366577818/La-decouverte-de-la-porte-derobee-XZ-revele-une-attaque-de-la-chaine-logistique-Linux
featured_image: https://cdn.ttgtmedia.com/visuals/German/article/weakest-link-adobe.jpg
tags:
- Sensibilisation
series:
- 202414
series_weight: 0
---

> Un mainteneur de XZ, une bibliothèque de compression open source très répandue pour les distributions Linux, a compromis le projet open source au cours des deux dernières années.
