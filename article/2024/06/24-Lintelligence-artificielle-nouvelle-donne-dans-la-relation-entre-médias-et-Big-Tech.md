---
site: La Revue des Médias
title: "L'intelligence artificielle, nouvelle donne dans la relation entre médias et Big Tech"
author: Franck Rebillard, Nikos Smyrnaios
date: 2024-06-24
href: https://larevuedesmedias.ina.fr/lintelligence-artificielle-nouvelle-donne-dans-la-relation-entre-medias-et-big-tech
featured_image: https://larevuedesmedias.ina.fr/sites/default/files/styles/landscape_1600x/public/2024-06/open%20ai.jpg?itok=sPYh5wTV
tags:
- Sciences
series:
- 202426
---

> Avec l’IA générative, les géants du numérique touchent désormais au cœur de métier des médias. Avec quelles contreparties et quelles conséquences?
