---
site: Télérama.fr
title: "WhatsApp, Signal... L'UE s'apprête à surveiller tous nos messages au nom de la lutte contre la pédocriminalité"
author: Olivier Tesquet
date: 2024-06-20
href: https://www.telerama.fr/debats-reportages/whatsapp-signal-l-ue-s-apprete-a-surveiller-tous-nos-messages-au-nom-de-la-lutte-contre-la-pedocriminalite-7020953.php
featured_image: https://focus.telerama.fr/2024/06/19/0/947/4200/2800/1200/0/60/0/9b05e71_1718806127197-gettyimages-1398294814.jpg/webp
tags:
- Europe
series:
- 202425
---

> Discuté ce jeudi 20 juin au Conseil de l’UE, un texte de lutte contre les pédocriminels prévoit le scan systématique des images et des liens envoyés par messagerie privée. Un projet dangereux, alertent les défenseurs des libertés publiques.
