---
site: actu.fr
title: "Une après-midi pour mieux connaître les logiciels libres à Marmande"
author: Marie-Pierre Caris
date: 2024-06-14
href: https://actu.fr/nouvelle-aquitaine/marmande_47157/une-apres-midi-pour-mieux-connaitre-les-logiciels-libres-a-marmande_61201447.html
featured_image: https://static.actu.fr/uploads/2024/06/f7f9ecc0276662cf9ecc02766300aev-960x640.jpg
tags:
- Associations
- Promotion
series:
- 202424
---

> L'association aGeNUx organise, samedi 15 juin, une après-midi de découverte des logiciels libres, à Marmande.
