---
site: Lyon Capitale.fr
title: "Logiciels libres: 'Internet ne fonctionnerait pas sans eux'"
author: Eloi Thiboud
date: 2024-06-02
href: https://www.lyoncapitale.fr/lctv/video/logiciels-libres-internet-ne-fonctionnerait-pas-sans-eux-video
featured_image: https://www.lyoncapitale.fr/wp-content/uploads/2024/05/Capture-decran-2024-05-31-a-15.52.38-e1717163616156.jpg
tags:
- Promotion
- Sensibilisation
series:
- 202423
series_weight: 0
---

> Philippe Scoffoni est président de l'association PLOSS-RA. Il était sur le plateau de l'émission '6 Minutes Chrono' de Lyon Capitale pour expliquer ce qu'est un logiciel libre à l'occasion des rencontres professionnelles du logiciel libre qui se tiendra le 10 juin à l'hôtel de la Métropole de Lyon.
