---
site: Le Monde.fr
title: "La souveraineté numérique avec le logiciel libre, grande absente de la campagne pour les Européennes en France"
author: Stéfane Fermigier
date: 2024-06-07
href: https://www.lemonde.fr/blog/binaire/2024/06/07/la-souverainete-numerique-avec-le-logiciel-libre-grande-absente-de-la-campagne-pour-les-europeennes-en-france
featured_image: https://asset.lemde.fr/prd-blogs/2024/05/ffa6248e-fermigier-800x800.jpg
seeAlso: "[Élections européennes 2024](https://www.april.org/elections-europeennes-2024)"
tags:
- Institutions
- Europe
series:
- 202423
series_weight: 0
---

> L’apport du logiciel libre pour la souveraineté numérique notamment en Europe commence à être compris. Pourtant, on peut s’inquiéter de l’absence de ce sujet dans la campagne pour les Européennes en France.
