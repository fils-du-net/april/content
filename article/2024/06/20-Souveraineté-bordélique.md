---
site: ZDNET
title: "Souveraineté bordélique"
author: Tris Acatrinei
date: 2024-06-20
href: https://www.zdnet.fr/blogs/souverainete-bordelique-393327.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/06/souverainete-750x410.jpg
tags:
- Sensibilisation
series:
- 202425
series_weight: 0
---

> Même la tech n'échappe pas la dissolution, comme le montre un article de Politico.
