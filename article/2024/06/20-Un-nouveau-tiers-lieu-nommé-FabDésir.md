---
site: Le Pays
title: "Un nouveau tiers-lieu nommé FabDésir"
date: 2024-06-20
href: https://www.le-pays.fr/saint-didier-sur-rochefort-42111/actualites/un-nouveau-tiers-lieu-nomme-fabdesir_14520811
featured_image: https://img.lamontagne.fr/ebETft_3LFAmIspHYOWXhmD3kCXl3EKL-xiYfYzXrx4/fit/657/438/sm/0/bG9jYWw6Ly8vMDAvMDAvMDcvMDcvODgvMjAwMDAwNzA3ODgxMg.jpg
tags:
- Associations
series:
- 202425
series_weight: 0
---

> Saint-Didier-sur-Rochefort. Inauguration des nouveaux locaux du FabDésir. L’inauguration des nouveaux locaux du FabDésir, un fablab novateur et un espace de collaboration communautaire situé sur la place du village, s’est déroulée le 15 juin. L’adresse se positionne comme «un tiers-lieu d’inspiration, d’innovation et d’intelligence collective».
