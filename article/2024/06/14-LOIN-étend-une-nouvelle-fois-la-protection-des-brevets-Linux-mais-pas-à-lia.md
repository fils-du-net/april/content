---
site: ZDNET
title: "L'OIN étend une nouvelle fois la protection des brevets Linux (mais pas à l'IA)"
author: Steven Vaughan-Nichols
date: 2024-06-14
href: https://www.zdnet.fr/actualites/loin-etend-une-nouvelle-fois-la-protection-des-brevets-linux-mais-pas-a-lia-393037.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/02/technologie-600.jpg
tags:
- Brevets logiciels
- Innovation
series:
- 202424
series_weight: 0
---

> L'Open Invention Network, la plus grande communauté de non-agression en matière de brevets, élargit sa définition du système Linux pour couvrir d'autres programmes de logiciels libres et de cloud-network.
