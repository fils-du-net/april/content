---
site: Les Echos
title: "L'open source au défi du modèle économique de l'intelligence artificielle"
author: Florian Dèbes
date: 2024-06-24
href: https://www.lesechos.fr/tech-medias/hightech/lopen-source-au-defi-du-modele-economique-de-lintelligence-artificielle-2103288
featured_image: https://media.lesechos.com/api/v1/images/view/667902fcb90d9c68e7080487/1024x576-webp/01102116143910-web-tete.webp
tags:
- Sciences
series:
- 202426
series_weight: 0
---

> Les investissements colossaux liés aux développements de l'IA s'accommodent mal d'une distribution gratuite des modèles ouverts les plus avancés. Mais Meta défend encore ce modèle sous-tendant une autre forme de monétisation et encourage les start-up à utiliser ses technologies.
