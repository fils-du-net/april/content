---
site: Next
title: "IA générative: quels modèles sont vraiment ouverts (€)"
author: Martin Clavey
date: 2024-06-07
href: https://next.ink/139420/ia-generative-quels-modeles-sont-vraiment-ouverts
featured_image: https://next.ink/wp-content/uploads/2024/06/Openwashing.webp
tags:
- Sciences
series:
- 202423
series_weight: 0
---

> Des chercheurs néerlandais ont classé 40 modèles de génération de textes et six modèles de génération d'images se prétendant «open» par degré d'ouverture réelle.
