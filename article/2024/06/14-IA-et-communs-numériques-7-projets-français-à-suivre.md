---
site: Silicon
title: "IA et communs numériques: 7 projets français à suivre"
author: Clément Bohic
date: 2024-06-14
href: https://www.silicon.fr/ia-communs-numriques-projets-francais-479487.html
featured_image: https://www.silicon.fr/wp-content/uploads/2024/06/communs-numeriques-IA-generative.png
tags:
- Sciences
series:
- 202425
series_weight: 0
---

> L'appel à projets «Communs numériques pour l'intelligence artificielle générative» a ses premiers lauréats. Qui sont-ils?
