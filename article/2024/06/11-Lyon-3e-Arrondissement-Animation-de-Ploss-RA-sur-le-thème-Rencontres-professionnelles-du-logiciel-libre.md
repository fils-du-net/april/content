---
site: Le progrès
title: "Lyon 3e Arrondissement. Animation de Ploss-RA sur le thème 'Rencontres professionnelles du logiciel libre'"
author: GTEC
date: 2024-06-11
href: https://www.leprogres.fr/culture-loisirs/2024/06/11/animation-de-ploss-ra-sur-le-theme-rencontres-professionnelles-du-logiciel-libre-gpso
featured_image: https://cdn-s-www.leprogres.fr/images/66648C00-E0EA-4F26-AB2F-CF8179BCE9E6/NW_raw/animation-de-ploss-ra-organisee-le-lundi-10-juin-a-l-hotel-de-la-metropole-a-lyon-3e-arrondissement-images-proposees-par-quot-gtec-(lyon-3e-arrondissement)-quot-1718103610.jpg
tags:
- Associations
- Promotion
series:
- 202424
---

> Animation de Ploss-RA organisée le lundi 10 juin à l'Hôtel de la Métropole à Lyon 3e arrondissement
