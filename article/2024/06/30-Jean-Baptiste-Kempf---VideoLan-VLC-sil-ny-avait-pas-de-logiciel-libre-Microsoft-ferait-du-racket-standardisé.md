---
site: 01net.
title: "Jean-Baptiste Kempf - VideoLan (VLC): «s'il n'y avait pas de logiciel libre, Microsoft ferait du racket standardisé»"
author: Stéphanie Bascou
date: 2024-06-30
href: https://www.01net.com/actualites/jean-baptiste-kempf-videolan-vlc-sil-ny-avait-pas-de-logiciel-libre-microsoft-ferait-du-racket-standardise.html
featured_image: https://www.01net.com/app/uploads/2024/06/Design-sans-titre173-1360x907.jpg
tags:
- International
series:
- 202426
series_weight: 0
---

> Le président de VideoLan, l'éditeur du lecteur multimédia VLC, était l'un des speakers de l'USI 2024. Pour 01net.com, le trentenaire est revenu sur l'open source et le logiciel libre, des moyens de rendre l'Europe plus souveraine numériquement.
