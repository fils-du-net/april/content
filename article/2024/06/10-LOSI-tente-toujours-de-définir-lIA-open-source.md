---
site: Silicon
title: "L'OSI tente toujours de définir l'«IA open source»"
author: Clément Bohic
date: 2024-06-10
href: https://www.silicon.fr/ia-open-source-definition-osi-479362.html
featured_image: https://www.silicon.fr/wp-content/uploads/2024/06/OSI-definition-IA-open-source.png
tags:
- Sciences
series:
- 202424
series_weight: 0
---

> Qu'entendre par «IA open source»? L'OSI entend finaliser une définition pour octobre. Aperçu de ses avancées.
