---
site: Sud Ouest
title: "Marmande: une rencontre pour découvrir les logiciels libres"
author: Jean-Christophe Wasner
date: 2024-06-12
href: https://www.sudouest.fr/lot-et-garonne/marmande/marmande-une-rencontre-pour-decouvrir-les-logiciels-libres-20100308.php
featured_image: https://media.sudouest.fr/20100308/1000x500/so-57ecd1b366a4bdbe429df5d5-ph0.jpg?v=1718353196
tags:
- Associations
- Promotion
series:
- 202424
series_weight: 0
---

> Didier Le Jallé et Sam Cramford, de l’association agenaise aGeNUx, veulent ouvrir une antenne à Marmande. Ils invitent à une présentation des logiciels libres samedi 15 juin, salle Damouran
