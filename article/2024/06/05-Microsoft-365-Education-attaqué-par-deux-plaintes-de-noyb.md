---
site: Next
title: "Microsoft 365 Education attaqué par deux plaintes de noyb"
description: "Pas responsable, pas coupable?"
author: Martin Clavey
date: 2024-06-05
href: https://next.ink/139487/microsoft-365-education-attaque-par-deux-plaintes-de-noyb
featured_image: https://next.ink/wp-content/uploads/2024/02/kenny-eliason-zFSo6bnZJTw-unsplash-1536x956.jpg
tags:
- Éducation
- Vie privée
- Logiciels privateurs
series:
- 202423
series_weight: 0
---

> L'association de Max Schrems a déposé deux plaintes contre Microsoft US auprès de la CNIL autrichienne à propos de la suite 365 Education utilisée dans certaines écoles autrichiennes. noyb accuse l'entreprise d'enfreindre le RGPD en manquant de transparence, en utilisant des cookies de tracking et en traitant des données des élèves sans leur consentement, tout en rejetant la responsabilité des traitements des données sur les écoles.
