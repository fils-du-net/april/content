---
site: ZDNET
title: "Framasoft lance de nouveaux services et invite à tester des logiciels"
author: Thierry Noisette
date: 2024-06-24
href: https://www.zdnet.fr/blogs/l-esprit-libre/framasoft-lance-de-nouveaux-services-et-invite-a-tester-des-logiciels-393536.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/06/framasoft-degooglisons-1024x392-1-750x392.png
tags:
- Associations
series:
- 202426
series_weight: 0
---

> L'association libriste propose un service de pétitions, une nouvelle version de celui de formulaires, et des essais tels qu'un logiciel de gestion de dépenses partagées.
