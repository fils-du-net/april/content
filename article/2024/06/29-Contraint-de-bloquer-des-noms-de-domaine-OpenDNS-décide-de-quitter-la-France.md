---
site: Next
title: "Contraint de bloquer des noms de domaine, OpenDNS décide de quitter la France"
description: Le DNS que vous avez demandé n'est plus disponible
author: Sébastien Gavois
date: 2024-06-29
href: https://next.ink/142507/contraint-de-bloquer-des-noms-de-domaine-opendns-decide-de-quitter-la-france/
featured_image: https://next.ink/wp-content/uploads/2023/12/photo-1546617885-4822125f891e-1536x1024.jpg
tags:
- Institutions
- Internet
series:
- 202426
series_weight: 0
---

> Une décision de justice demande à Cisco, Google et CloudFlare de bloquer l'accès à plus d'une centaine de sites. Canal+ en est à l'origine, dans sa guerre contre le piratage sportif.
