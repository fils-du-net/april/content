---
site: La Vie des idées
title: "Communs numériques et souveraineté: sauver les logiciels libres"
author: Christophe Masutti
href: https://laviedesidees.fr/Communs-numeriques-et-souverainete-sauver-les-logiciels-libres
featured_image: https://laviedesidees.fr/IMG/png/libre-bkg.png
tags:
- Économie
series:
- 202426
series_weight: 0
---

> Alors que la souveraineté numérique est de plus en plus associée au potentiel du logiciel libre ou open source, les multinationales et leurs plateformes ont réussi à marginaliser les alternatives communautaires, voire à en épuiser les ressources, avec la complaisance de l'État.
