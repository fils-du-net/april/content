---
site: ITforBusiness
title: "L'open source plaît aux DSI et aux investisseurs"
author: François Jeanne
date: 2024-06-14
href: https://www.itforbusiness.fr/open-source-plait-aux-dsi-et-aux-investisseurs-77742
featured_image: https://www.itforbusiness.fr/wp-content/uploads/2024/06/commercial-open-source.jpg
tags:
- Entreprise
series:
- 202424
series_weight: 0
---

> Un rapport Serena analyse la dynamique des éditeurs qui intègrent des composants open source dans leurs logiciels et de leurs investisseurs
