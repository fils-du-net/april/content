---
site: Next
title: "#ChatControl: 48 eurodéputés appellent au rejet du projet de surveillance des messageries"
author: Jean-Marc Manach
date: 2024-06-20
href: https://next.ink/141322/chatcontrol-48-eurodeputes-appellent-au-rejet-du-projet-de-surveillance-des-messageries
featured_image: https://next.ink/wp-content/uploads/2024/05/Espion.webp
tags:
- Europe
series:
- 202425
series_weight: 0
---

> Les gouvernements de l'UE doivent se prononcer ce jeudi sur le projet de règlement visant à «combattre les abus sexuels concernant les enfants». Il propose notamment d'obliger les utilisateurs de messageries privées à consentir à la surveillance des images et URL qu'ils voudraient partager.
