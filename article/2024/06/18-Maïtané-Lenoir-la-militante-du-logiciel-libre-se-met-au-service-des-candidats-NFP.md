---
site: l'Humanité.fr
title: "Maïtané Lenoir: la militante du logiciel libre se met au service des candidats NFP"
author: Pierric Marissal
date: 2024-06-18
href: https://www.humanite.fr/social-et-economie/legislatives-2024/maitane-lenoir-la-militante-du-logiciel-libre-se-met-au-service-des-candidats-nfp
featured_image: https://www.humanite.fr/wp-content/uploads/2024/06/p8-femme-jour_MER-edited.jpg
tags:
- Internet
series:
- 202425
series_weight: 0
---

> Maïtané Lenoir, militante du logiciel libre, propose aux candidats du Nouveau Front populaire (NFP) des sites Internet presque clés en main, avec un thème dédié aux législatives.
