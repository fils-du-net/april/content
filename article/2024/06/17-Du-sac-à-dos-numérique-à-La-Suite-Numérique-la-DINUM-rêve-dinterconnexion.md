---
site: Silicon
title: "Du «sac à dos numérique» à La Suite Numérique: la DINUM rêve d'interconnexion"
author: Clément Bohic
date: 2024-06-17
href: https://www.silicon.fr/la-suite-numerique-dinum-479597.html
featured_image: https://www.silicon.fr/wp-content/uploads/2024/06/DINUM-La-Suite-Numerique.jpeg
tags:
- Administration
series:
- 202425
series_weight: 0
---

> À travers La Suite Numérique, la DINUM a lancé un chantier d'interconnexion et d'extension du SNAP (sac à dos numérique de l'agent public).
