---
site: clubic.com
title: "Qu'est-ce que LibreChat, cette alternative à ChatGPT open source?"
author: Mélina Loupia
date: 2024-06-25
href: https://www.clubic.com/actualite-530646-qu-est-ce-que-librechat-cette-alternative-a-chatgpt-open-source.html
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/05/open-source_panneau.jpg
tags:
- Sciences
series:
- 202426
---

> LibreChat est annoncé comme un clone gratuit et open source de ChatGPT. Et qui dit open source dit flexibilité accrue aux utilisateurs. Cette plateforme permet non seulement de choisir parmi différents modèles d'IA, mais aussi d'en changer en cours de conversation. Un outil polyvalent qui bouscule les codes de l'IA conversationnelle.
