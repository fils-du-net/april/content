---
site: ZDNET
title: "Jean-Baptiste Kempf (VLC) tisse un lien entre souveraineté et logiciel libre"
author: Guillaume Serries
date: 2024-06-24
href: https://www.zdnet.fr/actualites/jean-baptiste-kempf-vlc-tisse-un-lien-entre-souverainete-et-logiciel-libre-393518.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/03/regulation_europe__w1200-365x200.jpg
tags:
- International
series:
- 202426
---

> Pourtant, la notion de souveraineté n'apparaît pas une évidence de prime abord pour celui qui édite VLC. 'VideoLAN, ce sont surtout des communautés anarchistes avec beaucoup d'organisation spontanée' dit-il.
