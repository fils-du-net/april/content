---
site: LaDepeche.fr
title: "Gourdon. Les logiciels libres présentés aux lycéens"
date: 2024-06-05
href: https://www.ladepeche.fr/2024/06/05/les-logiciels-libres-presentes-aux-lyceens-11995228.php
featured_image: https://static.milibris.com/thumbnail/title/3db3b795-5d91-4f89-8a02-68bdb46f61e2/front/catalog-cover.png
tags:
- Promotion
- Associations
- Éducation
series:
- 202423
series_weight: 0
---

> Jeudi 16 mai, le Pôle Numérique est intervenu à la cité scolaire Léo Ferré pour discuter avec les élèves de seconde sur la thématique du logiciel libre, des licences d’utilisation, des droits d’auteur, des conditions générales d’utilisation (CGU), des conditions générales de vente (CGV) et autres textes complexes.
