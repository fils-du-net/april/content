---
site: www.usine-digitale.fr
title: "Meta, Hugging Face et Scaleway renouvellent leur programme d'accélération de start-up IA"
author: Célia Séramour
date: 2024-06-27
href: https://www.usine-digitale.fr/article/meta-hugging-face-et-scaleway-renouvellent-leur-programme-d-acceleration-dedie-a-l-ia-open-source.N2214967
featured_image: https://www.usine-digitale.fr/mediatheque/6/1/4/001505416_896x598_c.jpg
tags:
- Sciences
series:
- 202426
---

> Après une première édition réussie pour l'AI Startup Program, Meta, Hugging Face et Scaleway annoncent ouvrir les inscriptions pour une nouvelle promotion. L'objectif est le même : accompagner les start-up européennes souhaitant intégrer des modèles de fondation en open source dans leurs produits et solutions.
