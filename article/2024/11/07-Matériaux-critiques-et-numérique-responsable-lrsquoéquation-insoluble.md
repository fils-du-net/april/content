---
site: Next
title: "Matériaux critiques et numérique responsable, l'équation insoluble? (€)"
description: "Miner pour construire quoi?"
author: Mathilde Saliou
date: 2024-11-07
href: https://next.ink/157115/materiaux-critiques-et-numerique-responsable-lequation-insoluble
featured_image: https://next.ink/wp-content/uploads/2024/04/anne-fehres_luke-conroy_ai4media_ai-models-built-from-fossils_2560x1440.jpg
tags:
- Économie
series:
- 202445
series_weight: 0
---

> À l’occasion du GreenTech Forum, l’ADEME a présenté sa dernière étude sur les besoins en métaux de l’industrie du numérique. Next a assisté à sa présentation et au débat qui a suivi. Le constat est amer et les solutions manquent, face à une nécessaire codépendance internationale.
