---
site: cio-online.com
title: "Avant de coder avec l'IA, il faut débuguer le risque juridique"
author: Grant Gross
date: 2024-11-15
href: https://www.cio-online.com/actualites/lire-avant-de-coder-avec-l-ia-il-faut-debuguer-le-risque-juridique-15981.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000021355.jpg
tags:
- Droit d'auteur
- Sciences
series:
- 202446
series_weight: 0
---

> Les développeurs qui utilisent l'IA pour écrire du code peuvent enfreindre les droits d'auteur ou les licences Open Source, exposant ainsi leurs employeurs.
