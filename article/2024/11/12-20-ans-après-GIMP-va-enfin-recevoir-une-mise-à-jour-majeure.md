---
site: Numerama
title: "20 ans après, GIMP va enfin recevoir une mise à jour majeure"
description: Une gestation bien longue
author: Julien Lausson
date: 2024-11-12
href: https://www.numerama.com/tech/1841518-20-ans-apres-gimp-va-enfin-recevoir-une-mise-a-jour-majeure.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2024/11/gimp-une-1024x576.jpg?webp=1&key=48c5c562
tags:
- Innovation
series:
- 202446
series_weight: 0
---

> Les libristes l'attendaient depuis longtemps: la nouvelle mise à jour majeure de GIMP arrive. Le logiciel libre va prochainement passer en version 3.0, vingt ans après la version 2.0. Entre temps, quelques moutures intermédiaires étaient sorties. C'est l'histoire d'un logiciel libre qui n'avait pas reçu de version
