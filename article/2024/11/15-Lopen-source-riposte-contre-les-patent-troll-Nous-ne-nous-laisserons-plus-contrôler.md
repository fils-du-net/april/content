---
site: ZDNET
title: "L'open source riposte contre les patent troll: 'Nous ne nous laisserons plus contrôler'"
author: Steven Vaughan-Nichols
date: 2024-11-15
href: https://www.zdnet.fr/actualites/lopen-source-riposte-contre-les-patent-troll-nous-ne-nous-laisserons-plus-controler-401437.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/11/cncf-musk-oxen-750x410.webp
tags:
- Brevets logiciels
series:
- 202446
series_weight: 0
---

> Les entreprises qui utilisent des projets open-source comme Kubernetes sont de plus en plus souvent la cible de patent trolls. La communauté open source lance une contre-offensive. Et recherche des volontaires.
