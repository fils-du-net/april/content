---
site: Next
title: "Fondation Mozilla: des licenciements… et la fin d'une ère?"
description: "Revoir ses fondations pour de solides bases?"
author: Sébastien Gavois
date: 2024-11-06
href: https://next.ink/156818/fondation-mozilla-des-licenciements-et-la-fin-dune-ere
featured_image: https://next.ink/wp-content/uploads/2024/01/FX_Design_Blog_Header_1400x770.jpg
tags:
- Internet
series:
- 202445
series_weight: 0
---

> La fondation Mozilla qui chapeaute les activités de Mozilla (et donc de Firefox) vient de confirmer un vaste plan de licenciement de 30 % de ses effectifs, avec la fermeture de deux divisions, dont le plaidoyer. Les dirigeants se veulent rassurants, affirmant qu’il s’agit d’une réorganisation, pas de la fin des combats.
