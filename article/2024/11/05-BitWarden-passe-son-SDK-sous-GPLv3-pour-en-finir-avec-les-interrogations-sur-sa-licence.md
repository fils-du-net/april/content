---
site: Next
title: "BitWarden passe son SDK sous GPLv3 pour en finir avec les interrogations sur sa licence"
author: Vincent Hermann
date: 2024-11-05
href: https://next.ink/brief_article/bitwarden-passe-son-sdk-sous-gplv3-pour-en-finir-avec-les-interrogations-sur-sa-licence
featured_image: https://next.ink/wp-content/uploads/2024/10/Bitwarden-multiple-devices-1024x532.webp
tags:
- Licence
series:
- 202445
series_weight: 0
---

> Le gestionnaire de mots de passe, connu pour son code open source, a été entouré d’une petite polémique ces derniers temps. En cause, une modification dans le SDK, qui ne permettait plus de reconstruire le client BitWarden en restant sur du code libre, du moins pas sans erreur.
