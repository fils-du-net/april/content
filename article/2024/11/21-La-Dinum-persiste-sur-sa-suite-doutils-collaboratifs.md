---
site: Le Monde Informatique
title: "La Dinum persiste sur sa suite d'outils collaboratifs"
author: Reynald Fléchaux
date: 2024-11-21
href: https://www.lemondeinformatique.fr/actualites/lire-la-dinum-persiste-sur-sa-suite-d-outils-collaboratifs-95306.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000099812.jpg
tags:
- Administration
series:
- 202447
series_weight: 0
---

> Autour d'une authentification unique, la DSI de l'État construit une suite d'outils collaboratifs open source. Une nouvelle entaille sans lendemain dans le monopole d'éditeurs comme Microsoft? Non, assure la Dinum, qui met en avant une démarche structurée et des formes de mutualisation avec l'Allemagne.
