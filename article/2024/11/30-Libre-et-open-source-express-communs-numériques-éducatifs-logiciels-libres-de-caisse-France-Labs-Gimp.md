---
site: ZDNET
title: "Libre et open source express: communs numériques éducatifs, logiciels libres de caisse, France Labs, Gimp"
author: Thierry Noisette
date: 2024-11-30
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-communs-numeriques-educatifs-logiciels-libres-de-caisse-france-labs-gimp-402274.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/05/open-source_panneau.jpg
tags:
- april
- Insitutions
series:
- 202449
series_weight: 0
---

> En bref. Une Gazette pour la communauté scolaire. L'April contre des amendements frappant les logiciels de caisse sous licences libres. Neural Datafari de France Labs, lauréat de NGI Search. Gimp en 3.0.
