---
site: Next
title: "HDH: le Conseil d'État valide l'hébergement des données de santé d'EMC2 chez Microsoft"
description: "Pas besoin de SecNumCloud"
author: Vincent Hermann
date: 2024-11-22
href: https://next.ink/159167/hdh-le-conseil-detat-valide-lhebergement-des-donnees-de-sante-demc2-chez-microsoft/
featured_image: https://next.ink/wp-content/uploads/2024/03/9356947429_75f411eefd_k.jpg
tags:
- Vie privée
series:
- 202449
series_weight: 0
---

> Le Conseil d’État a tranché: la validation par la CNIL de l’entrepôt de données de santé EMC2 n’était pas un excès de pouvoir. Le Health Data Hub, qui en est à l’origine, va donc pouvoir laisser les données dans Azure. C’est la deuxième fois que le Conseil d’État rejette une demande liée à l’hébergement des données par le HDH.
