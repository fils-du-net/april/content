---
site: Le Monde Informatique
title: "La fondation Mozilla licencie 30% de ses effectifs"
author: Jacques Cheminat
date: 2024-11-06
href: https://www.lemondeinformatique.fr/actualites/lire-la-fondation-mozilla-licencie-30-de-ses-effectifs-95186.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000099612.png
tags:
- Internet
series:
- 202445
---

> Sans préciser le nombre exact de postes concernés, la fondation Mozilla a annoncé la suppression de 30% de ses effectifs. Cette coupe entraîne la fermeture de la division liée à la défense des droits et celle sur les programmes mondiaux.
