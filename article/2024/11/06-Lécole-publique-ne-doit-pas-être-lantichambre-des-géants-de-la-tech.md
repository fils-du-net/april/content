---
site: Le Temps
title: "L'école publique ne doit pas être l'antichambre des géants de la tech"
author: Grégoire Barbey
date: 2024-11-06
href: https://www.letemps.ch/opinions/editoriaux/l-ecole-publique-ne-doit-pas-etre-l-antichambre-des-geants-de-la-tech
featured_image: https://letemps-17455.kxcdn.com/photos/690c5299-74e7-4bd5-a7d6-bf847522ae07/giant.avif
tags:
- Éducation
series:
- 202445
series_weight: 0
---

> ÉDITORIAL. La démarche privilégiée par le Département genevois de l'instruction publique basée sur des logiciels libres et des solutions ouvertes devrait questionner le reste du pays. Car elle s'avère pertinente du point de vue de la mission de l'école publique
