---
site: Silicon
title: "Data Privacy Framework: un an après, les États-Unis invités à corriger le tir"
author: Clément Bohic
date: 2024-11-19
href: https://www.silicon.fr/Thematique/data-ia-1372/Breves/Data-Privacy-Framework-un-an-apres-les-etats-Unis-invites-464999.htm
featured_image: https://cdn.edi-static.fr/image/upload/c_lfill,h_245,w_470/e_unsharp_mask:100,q_auto/f_auto/v1/Img/BREVE/2024/11/464999/Data-Privacy-Framework-apres-etats-Unis-invites-LE.jpg
tags:
- International
- Vie privée
series:
- 202447
series_weight: 0
---

> Le Comité européen de la protection des données pointe quantité d'insuffisances dans la mise en oeuvre du Data Privacy Framework, successeur du Privacy Shield.
