---
site: ZDNET
title: "Logiciel libre: dix leçons à tirer de sa communauté et qui n'ont rien à voir avec la technologie"
author: Jack Wallen
date: 2024-11-19
href: https://www.zdnet.fr/pratique/logiciel-libre-dix-lecons-a-tirer-de-sa-communaute-et-qui-nont-rien-a-voir-avec-la-technologie-401628.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/11/open-source-750x410.webp
tags:
- Sensibilisation
series:
- 202447
series_weight: 0
---

> On pourrait penser que les seuls enseignements à retenir de l'environnement open-source sont d'ordre technique. C'est une erreur. Voici pourquoi.
