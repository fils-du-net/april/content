---
site: LaDepeche.fr
title: "Livre. Un guide sur les logiciels libres pour rependre le contrôle de nos usages numériques"
author: Philippe Rioux
date: 2024-11-03
href: https://www.ladepeche.fr/2024/11/03/livre-un-guide-sur-les-logiciels-libres-pour-rependre-le-controle-de-nos-usages-numeriques-12300733.php
featured_image: https://images.ladepeche.fr/api/v1/images/view/67275f8e57c79e1e8e1cab54/large/image.png?v=1
tags:
- Sensibilisation
- Promotion
series:
- 202444
series_weight: 0
---

> Le journaliste Thierry Pigot publie un "Guide pratique des logiciels libres. L’informatique du quotidien pour le particulier et le professionnel" aux éditions du Puits Fleuri. Un ouvrage didactique qui montre que la vie numérique peut s’organiser hors des logiciels payants.
