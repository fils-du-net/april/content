---
site: ZDNET
title: "La NASA finance des logiciels open source pour soutenir l'innovation scientifique"
author: Thierry Noisette
date: 2024-11-09
href: https://www.zdnet.fr/blogs/l-esprit-libre/la-nasa-finance-des-logiciels-open-source-pour-soutenir-linnovation-scientifique-401056.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/11/nasa_logo-750x410.webp
tags:
- Économie
- Sciences
series:
- 202445
series_weight: 0
---

> L'agence spatiale américaine a accordé 15,6 millions de dollars de subventions à 15 projets de maintenance d'outils, de cadres et de bibliothèques open source. La NASA souligne "la nécessité d'un soutien et d'une maintenance durables des logiciels open source".
