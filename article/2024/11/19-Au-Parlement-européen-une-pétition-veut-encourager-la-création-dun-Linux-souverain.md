---
site: Next
title: "Au Parlement européen, une pétition veut encourager la création d'un Linux souverain"
description: "Souverain pour qui?"
author: Vincent Hermann
date: 2024-11-19
href: https://next.ink/158241/au-parlement-europeen-une-petition-veut-encourager-la-creation-dun-linux-souverain
featured_image: https://next.ink/wp-content/uploads/2024/11/Os-souverain1.webp
tags:
- Europe
series:
- 202447
series_weight: 0
---

> Un Autrichien anonyme a déposé au Parlement européen une pétition pour encourager le développement d’une distribution souveraine pour le Vieux continent. S’il y a peu de chances qu’elle récolte suffisamment de votes au vu de la complexité du sujet, elle vient mettre le doigt sur de nombreuses problématiques actuelles, dont la dépendance aux grandes sociétés américaines.
