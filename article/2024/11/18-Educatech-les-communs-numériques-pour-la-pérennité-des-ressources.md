---
site: Le Café pédagogique
title: "Educatech: les communs numériques pour la pérennité des ressources"
author: Julien Cabioch
date: 2024-11-18
href: https://cafepedagogique.net/2024/11/18/educatech-les-communs-numeriques-pour-la-perennite-des-ressources
featured_image: https://cafepedagogique.net/wp-content/uploads/2024/11/communs-numerique-cafe-pedagogique-e1731872580987-600x356.jpg
tags:
- Partage du savoir
- Éducation
series:
- 202447
series_weight: 0
---

> «Les communs numériques constituent l’une des priorités de la stratégie du numérique pour l’éducation». Au salon Educatech, il a aussi été question de l’intérêt des communs numériques pour l’action publique. Alexis Kauffmann (DNE) et Emma Ghariani (DINUM) ont eu un discours incisif sur la question. «De l’argent public pour financer des codes publics», clament-ils.
