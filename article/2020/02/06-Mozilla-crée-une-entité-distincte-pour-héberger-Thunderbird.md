---
site: Les Numeriques
title: "Mozilla crée une entité distincte pour héberger Thunderbird"
author:  
date: 2020-02-06
href: https://www.lesnumeriques.com/informatique/mozilla-cree-une-entite-distincte-pour-heberger-thunderbird-n146685.html
featured_image: https://dyw7ncnq1en5l.cloudfront.net/optim/news/14/146685/99f7b8a3-mozilla-cree-une-entite-distincte-pour-heberger-thunderbird__w800.webp
tags:
- Économie
series:
- 202006
series_weight: 0
---

> Après avoir cherché à se débarrasser de Thunderbird, puis changé son fusil d'épaule, la Fondation Mozilla a finalement décidé d'offrir un nid séparé au client de messagerie. Celui-ci sera désormais à la charge d'une filiale dédiée.
