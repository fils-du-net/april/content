---
site: Capital.fr
title: "VLC, l'histoire extraordinaire du logiciel français le plus téléchargé au monde"
author: Eric Wattez
date: 2020-02-10
href: https://www.capital.fr/entreprises-marches/vlc-lhistoire-extraordinaire-du-logiciel-francais-le-plus-telecharge-au-monde-1361858
tags:
- Sensibilisation
- Économie
series:
- 202007
series_weight: 0
---

> Reconnaissable à son logo en forme de cône de chantier, ce logiciel permet de lire tous les formats vidéo. Bien qu'il compte 400 millions d'utilisateurs dans le monde, ses créateurs en ont tiré très peu profit.
