---
site: Génération-NT
title: "La FSF envoie à Microsoft un disque dur pour libérer Windows 7"
author: Jérôme G.
date: 2020-02-22
href: https://www.generation-nt.com/windows-7-code-source-free-software-foundation-microsoft-disque-dur-actualite-1973315.html
featured_image: https://img.generation-nt.com/fsf-windows-7-disque-dur_0258000001665735.jpg
tags:
- Logiciels privateurs
series:
- 202008
series_weight: 0
---

> Dans sa quête de libération du code source de Windows 7, la Free Software Foundation a envoyé un disque dur à Microsoft.
