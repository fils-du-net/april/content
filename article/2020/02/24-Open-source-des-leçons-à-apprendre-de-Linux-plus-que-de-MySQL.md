---
site: Le Monde Informatique
title: "Open source: des leçons à apprendre de Linux, plus que de MySQL (€)"
author: Matt Asay
date: 2020-02-24
href: https://www.lemondeinformatique.fr/actualites/lire-open-source%C2%A0-des-lecons-a-apprendre-de-linux-plus-que-de-mysql-78204.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000070877.jpg
tags:
- Innovation
series:
- 202009
series_weight: 0
---

> Open Source: Quand un projet open source devient la propriété d'un vendeur unique, la communauté des développeurs recherche toujours des alternatives: les...
