---
site: Presse Citron
title: "DIY: Google vous aide à créer votre propre clé de sécurité"
author: Setra
date: 2020-02-01
href: https://www.presse-citron.net/diy-google-vous-aide-a-creer-votre-propre-cle-de-securite
featured_image: https://www.presse-citron.net/wordpress_prod/wp-content/uploads/2020/02/Clé-de-sécurité-DIY-à-laide-de-Google.jpg
tags:
- Innovation
series:
- 202005
---

> Google vient de lancer un projet open source qui permet de créer des clés de sécurité. Bien que celui-ci permette de se fabriquer une clé FIDO fonctionnelle, la firme souhaite surtout que les chercheurs, les fabricants de clés de sécurité et les particuliers l'utilisent pour développer de nouvelles fonctionnalités et pour accélérer l'adoption des clés de sécurité.
