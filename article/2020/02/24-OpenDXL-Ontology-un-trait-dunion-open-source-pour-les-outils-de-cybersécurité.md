---
site: Le Monde Informatique
title: "OpenDXL Ontology, un trait d'union open source pour les outils de cybersécurité"
author: Nicolas Certes
date: 2020-02-24
href: https://www.lemondeinformatique.fr/actualites/lire-opendxl-ontology-un-trait-d-union-open-source-pour-les-outils-de-cybersecurite-78209.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000070883.jpg
tags:
- Standards
series:
- 202009
---

> Annoncé au moment de la création de l'Alliance Open Cybersécurité, fin 2019, l'OpenDXL Ontology est accessible sur GitHub. Ce langage open source doit permettre aux plateformes de cybersécurité de communiquer entre elles pour signaler ou résoudre des problèmes.
