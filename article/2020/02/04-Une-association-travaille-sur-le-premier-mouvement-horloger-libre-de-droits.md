---
site: rts.ch
title: "Une association travaille sur le premier mouvement horloger libre de droits"
date: 2020-02-04
href: https://www.rts.ch/info/regions/neuchatel/11067644-une-association-travaille-sur-le-premier-mouvement-horloger-libre-de-droits.html
featured_image: https://www.rts.ch/2020/02/03/20/27/11066206.image?w=1200&h=675
tags:
- Matériel libre
series:
- 202006
series_weight: 0
---

> Un collectif de professionnels de l'horlogerie basé à La Chaux-de-Fonds est en train de mettre au point le premier mouvement de montre libre de droits. Tous les horlogers et les écoles pourraient reprendre ce mécanisme gratuitement.
