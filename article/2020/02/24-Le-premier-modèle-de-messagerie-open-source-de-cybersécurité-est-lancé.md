---
site: ZDNet France
title: "Le premier modèle de messagerie open source de cybersécurité est lancé"
author: Charlie Osborne
date: 2020-02-24
href: https://www.zdnet.fr/actualites/le-premier-modele-de-messagerie-open-source-de-cybersecurite-est-lance-39899571.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/03/article-620x400-papier2.png
tags:
- Standards
- Innovation
series:
- 202009
series_weight: 0
---

> OpenDXL Ontology est conçu pour le partage de données et de commandes entre différents logiciels de cybersécurité.
