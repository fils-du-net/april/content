---
site: Trax Magazine
title: "Deux Américains ont fait entrer 68,7 milliards de mélodies dans le domaine public"
author: Erwan Lecoup
date: 2020-02-27
href: https://www.traxmag.com/algorithme-687-milliards-de-melodies-domaine-public
featured_image: https://trxprds3.s3.amazonaws.com/uploads/2020/02/algorithme-midi-creativecommons-800x800.jpg
tags:
- Droit d'auteur
- Partage du savoir
series:
- 202009
series_weight: 0
---

> Deux musiciens et programamteurs ont enregistré, puis rendu publique, toutes les combinaisons de mélodies possibles sur une octave. Grâce à un algorithme créé par leur soin et au langage universel MIDI, les deux Américains souhaitent ainsi protéger les artistes d’être poursuivis pour violation de droits d’auteur devant les tribunaux, mais font aussi vaciller la question des droits de propriété intellectuelle.
