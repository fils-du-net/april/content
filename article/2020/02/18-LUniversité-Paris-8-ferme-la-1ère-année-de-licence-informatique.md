---
site: Le Monde Informatique
title: "L'Université Paris 8 ferme la 1ère année de licence informatique"
author: Véronique Arène
date: 2020-02-18
href: https://www.lemondeinformatique.fr/actualites/lire-l-universite-paris-8-ferme-la-1ere-annee-de-licence-informatique-78141.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000070794.jpg
tags:
- April
- Éducation
series:
- 202008
---

> L'équipe pédagogique du département d'informatique de l'Université Paris 8 s'est prononcée pour la non-ouverture de la première année de licence à la rentrée prochaine, faute de personnel. Une décision difficile qui a reçu dans le même temps le soutien unanime du Conseil national de l'Enseignement supérieur et de la Recherche.
