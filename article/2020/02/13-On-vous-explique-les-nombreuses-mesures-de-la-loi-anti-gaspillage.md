---
site: Next INpact
title: "On vous explique les nombreuses mesures de la loi «anti-gaspillage» (€)"
author: Xavier Berne
date: 2020-02-13
href: https://www.nextinpact.com/news/108690-on-vous-explique-nombreuses-mesures-loi-anti-gaspillage.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/23469.jpg
tags:
- Institutions
series:
- 202007
series_weight: 0
---

> Après des mois de débat, la loi «anti-gaspillage» a définitivement été adoptée par le Parlement puis publiée au Journal officiel, mardi 11 février. L’occasion de revenir en détail sur ce que va changer ce texte, notamment dans le domaine du numérique et plus largement des appareils électroniques qui peuplent notre quotidien.
