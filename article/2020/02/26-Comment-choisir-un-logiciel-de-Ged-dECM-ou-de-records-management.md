---
site: Archimag
title: "Comment choisir un logiciel de Ged, d'ECM ou de records management?"
author: C.Jost et M.Remize
href: https://www.archimag.com/content/choisir-logiciel-ged-ecm-records-management-dematerialisation
featured_image: https://www.archimag.com/sites/archimag.com/files/styles/article/public/couv_choisirlogiciel.jpg
tags:
- Sensibilisation
- Standards
series:
- 202009
series_weight: 0
---

> Gestion électronique de document (Ged), gestion de contenu (ECM) et records management (RM): il y a foule sur le marché des logiciels de gestion de l’information.
