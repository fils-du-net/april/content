---
site: Silicon
title: "Open source: des facteurs limitants pour les applications d'entreprise"
author: Clément Bohic
date: 2020-02-21
href: https://www.silicon.fr/open-source-applications-entreprise-334462.html
featured_image: https://www.silicon.fr/wp-content/uploads/2020/02/fondation-linux-open-source.jpg
tags:
- Innovation
series:
- 202008
---

> Un rapport de la Fondation Linux met en lumière plusieurs contraintes liées à l'intégration de l'open source dans les applications d'entreprise.
