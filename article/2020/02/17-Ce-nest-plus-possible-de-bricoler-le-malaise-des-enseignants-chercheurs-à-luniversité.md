---
site: francetv info
title: "'Ce n'est plus possible de bricoler': le malaise des enseignants-chercheurs à l'université"
author: Pauline Pennanec'h, Alexis Morel
date: 2020-02-17
href: https://www.francetvinfo.fr/societe/education/ce-n-est-plus-possible-de-bricoler-a-l-universite-paris-8-le-malaise-des-enseignants-chercheurs_3828997.html
featured_image: https://www.francetvinfo.fr/image/75run44e1-31c1/1500/843/20972991.jpg
tags:
- April
- Éducation
series:
- 202008
---

> Avec la future loi de programmation de la recherche, les universitaires craignent une précarisation des contrats, et plus de concurrence pour les financements. À l'université Paris 8, des enseignants-chercheurs se sont prononcés pour la fermeture de la première année de licence d'informatique, faute de moyens.
