---
site: Siècle Digital
title: "À quoi ressemblera l'avenir de la relation entre Google et Huawei?"
author: Valentin Cimino
date: 2020-02-02
href: https://siecledigital.fr/2020/02/02/quel-avenir-pour-la-relation-entre-google-et-huawei
featured_image: https://thumbor.sd-cdn.fr/RLQojR3GR1TnkBg_1Hose7DkLaw=/940x550/cdn.sd-cdn.fr/wp-content/uploads/2020/01/huawei-getty.jpg
tags:
- International
- Entreprise
series:
- 202005
---

> Si les États-Unis levaient l’interdiction qui empêche Huawei de travailler sur le territoire, la direction de l’entreprise chinoise s’estime prête à travailler de nouveau avec Google. Vraisemblablement, ce n’est pas l’avis de tous les directeurs du groupe. Pour cette raison et pour assurer ses arrières, Huawei travaille tout de même sur le développement de Huawei Mobile Services, un système d’exploitation open source qui semble recevoir un bon accueil, notamment en Europe.
