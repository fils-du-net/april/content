---
site: Silicon
title: "Open Source: GitHub déploie son projet Arctic Code Vault"
author: Ariane Beky
date: 2020-02-05
href: https://www.silicon.fr/github-deploie-arctic-code-vault-333768.html
featured_image: https://www.silicon.fr/wp-content/uploads/2019/01/GitHub.png
tags:
- Innovation
series:
- 202006
series_weight: 0
---

> GitHub a initié l'archivage à long terme du code source de logiciels libres. Le dépôt dans l'Arctic World Archive ets prévu au printemps 2020.
