---
site: cio-online.com
title: "L'adoption de solutions open source d'entreprise en hausse"
author: Aurélie Chandeze
date: 2020-02-27
href: https://www.cio-online.com/actualites/lire-l-adoption-de-solutions-open-source-d-entreprise-en-hausse-11978.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000014995.jpg
tags:
- Entreprise
series:
- 202009
series_weight: 0
---

> Une étude Red Hat montre que l'usage de logiciels open source d'entreprise progresse rapidement dans le monde, et pourrait bientôt dépasser celui des solutions propriétaires.
