---
site: Numerama
title: "La Commission européenne adopte Signal, sauf pour les discussions très sensibles"
author: Julien Lausson
date: 2020-02-25
href: https://www.numerama.com/tech/607720-la-commission-europeenne-adopte-signal-sauf-pour-les-discussions-tres-sensibles.html
featured_image: https://c2.lestechnophiles.com/www.numerama.com/content/uploads/2020/02/signal.jpg
tags:
- Europe
series:
- 202009
---

> Bruxelles recommande à son personnel d'utiliser la messagerie Signal pour discuter avec des personnes extérieures à l'institution, afin de relever le niveau de sécurité des communications. Les échanges très sensibles en revanche continuent de passer par des canaux dédiés.
