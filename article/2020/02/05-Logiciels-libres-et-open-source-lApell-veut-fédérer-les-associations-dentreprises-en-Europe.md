---
site: ZDNet France
title: "Logiciels libres et open source: l'Apell veut fédérer les associations d'entreprises en Europe"
author: Thierry Noisette
date: 2020-02-05
href: https://www.zdnet.fr/blogs/l-esprit-libre/logiciels-libres-et-open-source-l-apell-veut-federer-les-associations-d-entreprises-en-europe-39898645.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2018/04/copyright-UE_Pixabay_40632_960_720.png
tags:
- Associations
series:
- 202006
---

> Le CNLL pour la France et deux associations, allemande et finlandaise, sont les premiers membres de l'Apell (Association Professionnelle Européenne du Logiciel Libre).
