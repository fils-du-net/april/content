---
site: ZDNet France
title: "Voici les principaux composants des logiciels open source, et leurs problèmes"
author: Steven J. Vaughan-Nichols
date: 2020-02-19
href: https://www.zdnet.fr/actualites/voici-les-principaux-composants-des-logiciels-open-source-et-leurs-problemes-39899365.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2018/08/code-3337044_640__w630.jpg
tags:
- Innovation
- Standards
series:
- 202008
series_weight: 0
---

> Dans sa dernière étude, la Linux Foundation's Core Infrastructure Initiative montre à quel point les composants open source sont présents dans tous les logiciels. Ce qui pose des problèmes et des vulnérabilités communes.
