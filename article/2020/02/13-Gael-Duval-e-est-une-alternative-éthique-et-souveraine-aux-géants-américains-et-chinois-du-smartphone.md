---
site: Ecran Mobile 
title: "Gael Duval: «/e/ est une alternative éthique et souveraine aux géants américains et chinois du smartphone»"
date: 2020-02-13
href: https://www.ecranmobile.fr/%E2%80%8BGael-Duval-e-est-une-alternative-ethique-et-souveraine-aux-geants-americains-et-chinois-du-smartphone_a67582.html
featured_image: https://www.ecranmobile.fr/photo/art/default/42735104-35515955.jpg?v=1581583281
tags:
- Vie privée
- Innovation
series:
- 202007
series_weight: 0
---

> Créateur de la distribution Mandrake Linux, co-Fondateur de MandrakeSoft puis d’Ulteo, Gaël Duval est désormais aux commandes de E.foundation, une ambition projet consistant à créer des smartphones réellement indépendants des géants de la publicité.
