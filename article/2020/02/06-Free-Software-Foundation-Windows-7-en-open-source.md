---
site: 59Hardware
title: "Free Software Foundation, Windows 7 en open source?"
date: 2020-02-06
href: https://www.59hardware.net/actualit%C3%A9s/-logiciels-et-jeux-vid%C3%A9o/free-software-foundation,-windows-7-en-open-source-?-2020020620500.html
feature_image: https://www.59hardware.net/images/stories/Actus/2020/F%C3%A9vrier/Free-Software-Foundation-Windows-7-en-open-source.jpg
tags:
- Logiciels privateurs
series:
- 202006
---

> C'est la demande de la FSF auprès de Microsoft, une pétition est en cours avec près de 14 000 signatures pour 7777 souhaités par la fondation.
