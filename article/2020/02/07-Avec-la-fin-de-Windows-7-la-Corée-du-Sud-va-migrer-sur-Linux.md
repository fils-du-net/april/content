---
site: Le Monde Informatique
title: "Avec la fin de Windows 7, la Corée du Sud va migrer sur Linux"
author: Jacques Cheminat
date: 2020-02-07
href: https://www.lemondeinformatique.fr/actualites/lire-avec-la-fin-de-windows-7-la-coree-du-sud-va-migrer-sur-linux-78029.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000070644.jpg
tags:
- Logiciels privateurs
- International
series:
- 202006
series_weight: 0
---

> Système d'exploitation: Avec la fin du support de Windows 7, le gouvernement sud-coréen a élaboré une stratégie pour migrer à grande échelle sur un OS Linux.
