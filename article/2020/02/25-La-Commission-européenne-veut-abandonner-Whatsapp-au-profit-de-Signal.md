---
site: ICTjournal
title: "La Commission européenne veut abandonner Whatsapp au profit de Signal"
author: Yannick Chavanne
date: 2020-02-25
href: https://www.ictjournal.ch/news/2020-02-25/la-commission-europeenne-veut-abandonner-whatsapp-au-profit-de-signal
featured_image: https://www.ictjournal.ch/sites/default/files/styles/np8_full/public/media/2020/02/25/signal-ue.jpg
tags:
- Europe
- Logiciels privateurs
series:
- 202009
series_weight: 0
---

> Pour accroître la sécurité de ses communications publiques avec l'extérieur, la Commission européenne pousse ses collaborateurs à se passer de Whatsapp et d’opter pour l’alternative open source Signal.
