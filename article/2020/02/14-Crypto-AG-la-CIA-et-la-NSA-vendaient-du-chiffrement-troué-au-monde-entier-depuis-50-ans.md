---
site: Les Numeriques
title: "Crypto AG: la CIA et la NSA vendaient du chiffrement “troué” au monde entier depuis 50 ans"
author: Corentin Bechade
date: 2020-02-14
href: https://www.lesnumeriques.com/vie-du-net/crypto-ag-la-cia-et-la-nsa-vendaient-du-chiffrement-troue-au-monde-entier-depuis-50-ans-n147069.html
featured_image: https://dyw7ncnq1en5l.cloudfront.net/optim/news/14/147069/5e37211a-crypto-ag-une-porte-derobee-vieille-de-50-ans__w800.webp
tags:
- International
- Vie privée
series:
- 202007
series_weight: 0
---

> Depuis un demi-siècle environ, les services de renseignement des États-Unis vendaient des solutions de chiffrement obsolètes à des gouvernements étrangers pour écouter paisiblement les conversations du monde entier.
