---
site: Génération-NT
title: "Thunderbird placé sous l'égide d'une nouvelle filiale de la Fondation Mozilla"
author: Jérôme G.
href: https://www.generation-nt.com/thunderbird-mozilla-foundation-mzla-technologie-actualite-1972644.html
featured_image: https://img.generation-nt.com/01B0012001665371.jpg
tags:
- Économie
series:
- 202005
---

> MZLA Technologies Corporation est le nouveau propriétaire de Thunderbird. C'est une filiale de la Fondation Mozilla.
