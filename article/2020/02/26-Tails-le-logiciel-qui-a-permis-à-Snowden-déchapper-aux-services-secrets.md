---
site: Solidaire
title: "Tails: le logiciel qui a permis à Snowden d'échapper aux services secrets"
author: Tibor Van Cutsem
date: 2020-02-26
href: https://www.solidaire.org/articles/tails-le-logiciel-qui-permis-snowden-d-echapper-aux-services-secrets
featured_image: https://www.solidaire.org/sites/default/files/styles/solidair_news_main/public/images/2020/02/26/snowden-2.jpg
tags:
- Vie privée
- Institutions
series:
- 202009
series_weight: 0
---

> Comment échanger des documents confidentiels quand on est traqué par la NSA, les services de renseignement américains? Edward Snowden, le jeune lanceur d'alerte, a été confronté au problème. La solution? Tails, un logiciel qui garantit l'anonymat de ses utilisateurs. Solidaire les a rencontrés.
