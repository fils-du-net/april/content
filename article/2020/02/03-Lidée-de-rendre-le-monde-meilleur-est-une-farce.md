---
site: Usbek & Rica
title: "«L'idée de 'rendre le monde meilleur' est une farce»"
author: Fabien Benoit
date: 2020-02-03
href: https://usbeketrica.com/article/l-idee-de-rendre-le-monde-meilleur-est-une-farce
featured_image: https://static.usbeketrica.com/images/thumb_840xh/5e3804421b329.jpg
tags:
- Entreprise
series:
- 202006
---

> Joan Greenbaum, ex-programmeuse d'IBM ayant milité dès 1969 pour une technologie plus éthique, revient sur son parcours, et l'actualité des 'tech workers'.
