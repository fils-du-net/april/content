---
site: ZDNet France
title: "La Commission européenne recommande à ses employés de passer à Signal"
author: Thierry Noisette
date: 2020-02-29
href: https://www.zdnet.fr/blogs/l-esprit-libre/la-commission-europeenne-recommande-a-ses-employes-de-passer-a-signal-39899925.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2020/02/Commission_Europeenne_Charlemagne_WMC.jpg
tags:
- Europe
series:
- 202009
---

> La Commission recommande à ses employés l'usage comme messagerie instantanée du logiciel libre Signal, plus sûr.
