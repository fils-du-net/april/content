---
site: actu.fr
title: "Le groupe d'informatique libre aura son Fab Lab à Clisson"
author: Hervé Pavageau
date: 2020-02-03
href: https://actu.fr/pays-de-la-loire/clisson_44043/le-groupe-dinformatique-libre-aura-fab-lab-clisson_31200527.html
featured_image: https://static.actu.fr/uploads/2020/02/25256-200128100937552-001.jpg
tags:
- Promotion
- Associations
series:
- 202006
series_weight: 0
---

> Gullivigne, le groupe d'utilisateurs des logiciels libres du Vignoble nantais, va disposer d'un Fab Lab à Clisson fin février. L'outil sera situé dans la zone d'activités Tabari.
