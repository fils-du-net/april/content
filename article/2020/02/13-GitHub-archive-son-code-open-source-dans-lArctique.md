---
site: ICTjournal
title: "GitHub archive son code open source dans l’Arctique"
author: Steven Wagner
date: 2020-02-13
href: https://www.ictjournal.ch/news/2020-02-13/github-archive-son-code-open-source-dans-larctique
featured_image: https://www.ictjournal.ch/sites/default/files/styles/np8_full/public/media/2020/02/13/arctic-world-data-archive-global-seed-vault-8-970x647-c-970x576.jpg?itok=tH1QUcsA
tags:
- Innovation
series:
- 202007
series_weight: 0
---

> La plateforme de collaboration et d’hébergement de code GitHub a débuté la production de son projet Arctic Code Vault, dont l’ambition est d’archiver les logiciels libres et open source dans l’Arctique pendant 1000 ans.
