---
site: 3Dnatives
title: "Thingiverse: des fichiers 3D gratuits pour l'impression 3D"
author: Mélanie R.
date: 2020-02-18
href: https://www.3dnatives.com/thingiverse-plateforme-fichiers-3d-gratuit-18022020
featured_image: https://www.3dnatives.com/wp-content/uploads/Cover2-3.jpg
tags:
- Matériel libre
- Partage du savoir
series:
- 202008
series_weight: 0
---

> Thingiverse est une plate-forme de fichiers 3D gratuite où des millions d'utilisateurs peuvent trouver leurs modèles à imprimer en 3D
