---
site: Tribune de Genève
title: "Informatique: «L'être humain va devoir s'interroger sur son rapport au monde numérique»"
author: Olivier Wurlod
date: 2020-02-19
href: https://www.tdg.ch/economie/L-etre-humain-va-devoir-sinterroger-sur-son-rapport-au-monde-digital/story/13130908
featured_image: https://files.newsnetz.ch/story/1/3/1/13130908/5/topelement.jpg
tags:
- Vie privée
- Europe
series:
- 202008
series_weight: 0
---

> La protection des données revient sur le devant de la scène, alors qu'une affaire d'espionnage agite la Suisse. Le point avec Jean-Pierre Hubaux, du Center for Digital Trust de l'EPFL.
