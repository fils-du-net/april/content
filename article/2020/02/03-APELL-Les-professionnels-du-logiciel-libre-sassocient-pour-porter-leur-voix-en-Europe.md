---
site: Le Monde Informatique
title: "APELL: Les professionnels du logiciel libre s'associent pour porter leur voix en Europe"
author: Jacques Cheminat
date: 2020-02-03
href: https://www.lemondeinformatique.fr/actualites/lire-apell-les-professionnels-du-logiciel-libre-s-associent-pour-porter-leur-voix-en-europe-77944.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000070511.jpg
tags:
- Associations
- Entreprise
- International
series:
- 202006
series_weight: 0
---

> Plusieurs associations des professionnels du logiciel ont créé une structure, nommée APELL pour porter leur voix auprès des institutions européennes. La France, l'Allemagne et la Finlande sont parmi les fondateurs.
