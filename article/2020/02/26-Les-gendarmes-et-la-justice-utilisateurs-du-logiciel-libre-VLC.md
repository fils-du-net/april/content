---
site: ZDNet France
title: "Les gendarmes et la justice, utilisateurs du logiciel libre VLC"
author: Thierry Noisette
date: 2020-02-26
href: https://www.zdnet.fr/blogs/l-esprit-libre/les-gendarmes-et-la-justice-utilisateurs-du-logiciel-libre-vlc-39899783.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2020/02/VLC%20icone-3D_Flickr.jpg
tags:
- Administration
series:
- 202009
series_weight: 0
---

> Pour les gendarmes, comme d'autres parmi les millions d'utilisateurs du lecteur multimédia, la prochaine mise à jour permettra de 'lire des formats exotiques' sans avoir à repasser de Linux à Windows.
