---
site: Futura
title: "Les satellites pourraient-ils être piratés et transformés en armes?"
author: Louis Neveu
date: 2020-02-13
href: https://www.futura-sciences.com/tech/actualites/cybersecurite-satellites-pourraient-ils-etre-pirates-transformes-armes-79594
featured_image: https://cdn.futura-sciences.com/buildsv6/images/wide1920/1/4/b/14be2205a2_50160230_galileo-satellites.jpg
tags:
- Innovation
series:
- 202007
series_weight: 0
---

> Avec le lancement de vastes flottes de satellites de télécommunication, la problématique de la cybersécurité se pose. Faute d'une réglementation commune, les satellites ne sont pas sécurisés et pourraient être piratés pour modifier leur orbite ou les détruire. Explications.
