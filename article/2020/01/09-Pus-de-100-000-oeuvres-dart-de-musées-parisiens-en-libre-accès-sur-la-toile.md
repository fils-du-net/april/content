---
site: ZDNet France
title: "Pus de 100 000 oeuvres d'art de musées parisiens en libre accès sur la toile"
author: Clarisse Treilles
date: 2020-01-09
href: https://www.zdnet.fr/actualites/pus-de-100-000-oeuvres-d-art-de-musees-parisiens-en-libre-acces-sur-la-toile-39897049.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2020/01/bandeau_opencontent_620.jpg
tags:
- Contenus libres
series:
- 202002
series_weight: 0
---

> Et si l'art devenait demain un bien culturel dématérialisé libre d'accès pour tous? Un consortium de musées parisiens met à disposition du grand public ses oeuvres d'art numérisées, librement téléchargeables et réutilisables.
