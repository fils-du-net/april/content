---
site: Le Monde Informatique
title: "La Free Software Foundation réclame la mise en open source de Windows 7"
author: Maryse Gros
date: 2020-01-27
href: https://www.lemondeinformatique.fr/actualites/lire-la-free-software-foundation-reclame-la-mise-en-open-source-de-windows-7-77859.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000070383.png
tags:
- Logiciels privateurs
series:
- 202005
---

> En mettant Windows 7 dans l'open source, Microsoft permettrait à la communauté d'étudier le système d'exploitation et de l'améliorer, enjoint la Free Software Foundation à travers une pétition.
