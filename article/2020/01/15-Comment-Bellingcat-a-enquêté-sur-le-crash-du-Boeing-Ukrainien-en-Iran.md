---
site: L'ADN 
title: "Comment Bellingcat a enquêté sur le crash du Boeing Ukrainien en Iran?"
author: David-Julien Rahmil
date: 2020-01-15
href: https://www.ladn.eu/media-mutants/reseaux-sociaux/comment-bellingcat-enquete-crash-boeing-ukrainien-iran
featured_image: https://www.ladn.eu/wp-content/uploads/2020/01/guerre-1140x480.jpg
tags:
- Partage du savoir
series:
- 202003
series_weight: 0
---

> Peu connu du grand public, Bellingcat est le premier média à avoir popularisé le concept d'enquête open source. C'est grâce au travail de cette rédaction que l'Iran a reconnu sa responsabilité dans le récent crash du Boeing à Téhéran.
