---
site: Futura
title: "Email: Mozilla tente l'opération de la dernière chance pour sauver Thunderbird"
author: Fabrice Auclert
date: 2020-01-30
href: https://www.futura-sciences.com/tech/actualites/internet-email-mozilla-tente-operation-derniere-chance-sauver-thunderbird-12508
featured_image: https://cdn.futura-sciences.com/buildsv6/images/wide1920/f/c/e/fcef01cb42_50159729_thunderbird.jpg
tags:
- Économie
series:
- 202005
series_weight: 0
---

> Alors que Firefox demeure très populaire, son «petit frère» Thunderbird ne représente aujourd'hui que 0,5% du marché des clients de messagerie électronique. Malgré cette audience confidentielle, la fondation Mozilla a décidé de changer son mode de fonctionnement avec, désormais, un système combinant dons et partenariats pour tenter de le monétiser et ainsi maintenir son développement.
