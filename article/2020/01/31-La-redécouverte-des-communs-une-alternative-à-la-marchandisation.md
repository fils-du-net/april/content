---
site: Pressenza
title: "La redécouverte des «communs»: une alternative à la marchandisation?"
author: Olivier Flumian
date: 2020-01-31
href: https://www.pressenza.com/fr/2020/01/la-redecouverte-des-communs-une-alternative-a-la-marchandisation
featured_image: https://www.pressenza.com/wp-content/uploads/2020/01/xQuand-parle-Communs1-720x480.jpg.pagespeed.ic.nxnv5l595c.jpg
tags:
- Économie
series:
- 202005
---

> Alors que la marchandisation croissante des activités humaines se développe, approfondissant la crise sociale et écologique à l’échelle planétaire, la notion de «Communs» est de plus en plus présente dans les débats publics. Elle inspire des initiatives dans des domaines toujours plus nombreux. Que se cache-t-il derrière ce terme? En quoi «les communs» indiquent-ils une voie porteuse d’alternatives concrètes? Nous avons posé la question à Michel Bauwens, chercheur, entrepreneur et écrivain spécialiste de cette nouvelle approche.
