---
site: Breakingnews.fr 
title: Windows 7 devrait continuer de fonctionner comme une source ouverte, des demandes de pétition spectaculairement optimistes
date: 2020-01-26
href: https://www.breakingnews.fr/technologie/windows-7-devrait-continuer-de-fonctionner-comme-une-source-ouverte-des-demandes-de-petition-spectaculairement-optimistes-179139.html
featured_image: https://cdn.mos.cms.futurecdn.net/ZxQJX5tYUPg7aHFhyT5j2X.jpg
tags:
- Logiciels privateurs
series:
- 202004
series_weight: 0
---

> Windows 7 a peut-être atteint la fin de sa vie, le support officiel n’étant plus fourni (du moins pas gratuitement), mais le système d’exploitation vétéran pourrait perdurer si Microsoft le publiait en open source.
