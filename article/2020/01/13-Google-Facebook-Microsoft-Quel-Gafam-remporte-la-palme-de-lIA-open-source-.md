---
site: Journal du Net
title: "Google, Facebook, Microsoft... Quel Gafam remporte la palme de l'IA open source?"
author: Antoine Crochet-Damais
date: 2020-01-13
href: https://www.journaldunet.com/solutions/dsi/1487532-ces-ia-offertes-par-les-gafam-en-open-source
featured_image: https://img-0.journaldunet.com/9UB6aUnTaW839ppZ4S0VJlUztFU=/540x/smart/fe8d96c4666545d89c7d9c1f60b1950c/ccmcms-jdn/12943419.jpg
tags:
- Innovation
- Entreprise
series:
- 202003
---

> Tour d'horizon des projets open source les plus actifs sur GitHub lancés dans l'intelligence artificielle par les Gafam.
