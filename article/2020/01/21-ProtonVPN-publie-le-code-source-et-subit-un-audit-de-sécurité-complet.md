---
site: Breakingnews.fr 
title: "ProtonVPN publie le code source et subit un audit de sécurité complet"
date: 2020-01-21
href: https://www.breakingnews.fr/technologie/protonvpn-publie-le-code-source-et-subit-un-audit-de-securite-complet-162643.html
featured_image: https://cdn.mos.cms.futurecdn.net/fGFUeh5RAZcPtTw8Ej7Yc6.jpg
tags:
- Vie privée
- Innovation
series:
- 202004
series_weight: 0
---

> Depuis ses débuts, ProtonVPN est devenu le fournisseur de VPN le plus transparent et le plus responsable et maintenant la société a soutenu ces revendications en publiant son code source sur toutes les plateformes et en subissant un audit de sécurité indépendant.
