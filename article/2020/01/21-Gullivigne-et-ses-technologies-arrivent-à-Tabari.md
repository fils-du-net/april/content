---
site: ouest-france.fr
title: "Gullivigne et ses technologies arrivent à Tabari (€)"
date: 2020-01-21
href: https://www.ouest-france.fr/pays-de-la-loire/clisson-44190/clisson-gullivigne-et-ses-technologies-arrivent-tabari-6699424
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMDAxZDkwOTFiNjhkMGFkZTA3YjhkMDYwY2Y1ZjZjMzg0YmM?width=940&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=3c1c812c559f471ccdaa0eda73b29cb2c41a914a44a64aae7427c2632993bf73
tags:
- Associations
- Promotion
series:
- 202004
---

> L’association Gullivigne forme au logiciel libre et aux technologies informatiques innovantes. Elle s’installe en février dans un local permanent, zone industrielle de Tabari.
