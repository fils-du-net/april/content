---
site: Next INpact
title: "Qwant Mail: le #fail de Linagora (€)"
author: Jean-Marc Manach
date: 2020-01-10
href: https://www.nextinpact.com/news/108320-qwant-mail-fail-linagora.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/23996.jpg
tags:
- Entreprise
- Vie privée
series:
- 202002
series_weight: 0
---

> L'ex PDG de Qwant avait expliqué le retard de QwantMail par un «problème de sécurité qui nous a poussé à tout reprendre». Son équipe SSI avait en effet découvert de nombreuses failles sur une plateforme de Linagora, le «leader français du logiciel libre», destinée à tester QwantMail. Plusieurs ex-salariés déplorent une éthique «privacy by design» de façade.
