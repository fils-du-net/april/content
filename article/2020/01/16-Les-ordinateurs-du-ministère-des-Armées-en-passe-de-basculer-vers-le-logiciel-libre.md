---
site: Acteurs Publics 
title: "Les ordinateurs du ministère des Armées en passe de basculer vers le logiciel libre? (€)"
author: Emile Marzolf
date: 2020-01-16
href: https://www.acteurspublics.fr/articles/les-ordinateurs-du-ministere-des-armees-en-passe-de-basculer-vers-le-logiciel-libre
tags:
- Marchés publics
series:
- 202003
---

> Après la gendarmerie nationale, pionnière en la matière, le ministère des Armées pourrait lui aussi basculer vers le logiciel libre.
