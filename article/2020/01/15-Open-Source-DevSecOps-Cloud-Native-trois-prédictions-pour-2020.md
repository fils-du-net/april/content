---
site: Silicon
title: "Open Source, DevSecOps, Cloud-Native: trois prédictions pour 2020"
author: Nabil Bousselham
date: 2020-01-15
href: https://www.silicon.fr/avis-expert/open-source-devsecops-cloud-native-trois-predictions-pour-2020
featured_image: https://www.silicon.fr/wp-content/uploads/2012/01/Open-source-Code-%C2%A9-Zothen-Fotolia.com_-684x513.jpg
tags:
- Innovation
series:
- 202003
series_weight: 0
---

> Quelle sera la place de l'open source dans la création du code? Le DevSecOps sera-il incontournable? Le Cloud Native est certes tentant, mais rimera-t-il avec sécurité? Veracode livre quelques prédictions et tendances pour 2020.
