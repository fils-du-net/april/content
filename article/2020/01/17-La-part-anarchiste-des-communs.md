---
site: Ballast 
title: "La part anarchiste des communs"
author: Édouard Jourdain
date: 2020-01-17
href: https://www.revue-ballast.fr/la-part-anarchiste-des-communs
featured_image: https://www.revue-ballast.fr/wp-content/uploads/2020/01/Communs_vignette.jpg.webp
tags:
- Économie
series:
- 202003
series_weight: 0
---

> Face à la submersion néolibérale et à la privatisation généralisée du monde, la notion de «commun» est revenue en force. On ne compte plus les ouvrages et les discours louant la nécessité des «communs» ou du «bien commun», qu’il s’agisse de ressources naturelles (une forêt), matérielles (un musée) ou immatérielles (un logiciel).
