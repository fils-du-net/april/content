---
site: Génération-NT
title: "Qwant: le cofondateur Éric Leandri quitte la présidence du groupe"
author: Jérôme G.
href: https://www.generation-nt.com/qwant-moteur-recherche-leandri-actualite-1971977.html
featured_image: https://img.generation-nt.com/qwant-logo_0096006401665011.jpg
tags:
- Entreprise
series:
- 202002
---

> De la réorganisation au sein de la direction de Qwant. Cofondateur et patron historique, Éric Leandri quitte ce poste et n'aura plus de rôle exécutif.
