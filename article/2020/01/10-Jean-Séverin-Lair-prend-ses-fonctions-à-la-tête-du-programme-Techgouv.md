---
site: Le Monde Informatique
title: "Jean-Séverin Lair prend ses fonctions à la tête du programme Tech.gouv"
author: Bertrand Lemaire
date: 2020-01-10
href: https://www.lemondeinformatique.fr/actualites/lire-jean-severin-lair-prend-ses-fonctions-a-la-tete-du-programme-techgouv-77664.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000070121.jpg
tags:
- Institutions
series:
- 202002
series_weight: 0
---

> Nomination: Comme annoncé en Novembre 2019, Jean-Séverin Lair a pris la tête de Tech.gouv après Vitam, toujours à la DINUM.
