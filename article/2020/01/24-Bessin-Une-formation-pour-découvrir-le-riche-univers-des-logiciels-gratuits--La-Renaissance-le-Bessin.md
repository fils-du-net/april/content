---
site: actu.fr
title: "Bessin. Une formation pour découvrir le riche univers des logiciels gratuits"
date: 2020-01-24
href: https://actu.fr/normandie/ver-sur-mer_14739/bessin-une-formation-decouvrir-riche-univers-logiciels-gratuits_30981922.html
featured_image: https://static.actu.fr/uploads/2020/01/img-0409-854x569.jpg
tags:
- Associations
- Promotion
series:
- 202004
---

> Le domaine des logiciels libres est un univers riche en pépites, à trouver avec une bonne boussole. Le club informatique de Ver-sur-Mer (Calvados) propose l’aide de 6 formateurs pour faire les bons choix et déjouer les pièges tendus aux informaticiens en herbe. Une véritable offre d’utilité publique pour tous ceux, ils sont nombreux, qui souhaitent améliorer les performances de leur ordinateur tout en préservant leurs économies grâce à des logiciels offrant un niveau de qualité égal, et parfois supérieur, à des offres commerciales onéreuses.
