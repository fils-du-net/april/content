---
site: RTBF Info
title: "Clap de fin pour Windows 7, l'occasion de passer à l'open source Linux?"
author: M.A.
date: 2020-01-15
href: https://www.rtbf.be/info/medias/detail_clap-de-fin-pour-windows-7-l-occasion-de-passer-a-l-open-source-linux?id=10408348
featured_image: https://ds1.static.rtbf.be/article/image/370x208/a/1/1/64b67456c2dee9fc1406d63d8c8c6419-1579103507.jpg
seeAlso: "L'article concerne le [système d'exploitation GNU/Linux](https://www.gnu.org/gnu/linux-and-gnu.html) mais le nomme «Linux»"
tags:
- Sensibilisation
- Logiciels privateurs
series:
- 202003
series_weight: 0
---

> Depuis ce mardi 14 janvier, Microsoft a mis fin au support de Windows 7. Dans un article publié au mois de septembre de cette année et mis à jour ce 13 janvier nous abordions la nécessité ou pas de passer à Windows 10. Mais finalement est-ce là...
