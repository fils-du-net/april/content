---
site: ZDNet France
title: "La fondation Linux veut améliorer la sécurité de l'open source"
author: Thierry Noisette
date: 2020-01-28
href: https://www.zdnet.fr/blogs/l-esprit-libre/la-fondation-linux-veut-ameliorer-la-securite-de-l-open-source-39898177.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2020/01/Linux%20Foundation%20logo.jpg
tags:
- Innovation
series:
- 202005
series_weight: 0
---

> La Linux Foundation annonce un partenariat avec l'Open Source Technology Improvement Fund (OSTIF) pour développer ensemble des audits de sécurité.
