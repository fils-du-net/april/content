---
site: L'Echo
title: "Michel Bauwens: \"Plus une civilisation est inégalitaire et prédatrice, plus dure est sa chute\""
date: 2020-01-25
href: https://www.lecho.be/opinions/carte-blanche/michel-bauwens-plus-une-civilisation-est-inegalitaire-et-predatrice-plus-dure-est-sa-chute/10200919.html
featured_image: https://images.lecho.be/view?iid=dc:160173211&context=ONLINE&ratio=16/9&width=1280&u=1579894326000
tags:
- Économie
series:
- 202004
series_weight: 0
---

> Entretien avec Michel Bauwens, considéré comme l'une des 100 personnalités les plus influentes du 21e siècle.
