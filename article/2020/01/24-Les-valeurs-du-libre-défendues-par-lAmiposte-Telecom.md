---
site: Vosges matin
title: "Les valeurs du libre défendues par l'Amiposte Telecom"
date: 2020-01-24
href: https://www.vosgesmatin.fr/culture-loisirs/2020/01/24/les-valeurs-du-libre-defendues-par-l-amiposte-telecom
featured_image: https://cdn-s-www.vosgesmatin.fr/images/82EE2C57-7922-40ED-886D-DBD0526DAB99/NW_detail/title-1579799617.jpg
tags:
- Associations
- Promotion
series:
- 202004
series_weight: 0
---

> Il s’agit d’un des plus vieux clubs informatique des Vosges, l’Amiposte Telecom, fondé dans les années 80 est encore debout, malgré la démocratisation des nouvelles technologies. Ses adhérents découvrent chaque semaine ce qui a trait aux licences ou programmes libres.
