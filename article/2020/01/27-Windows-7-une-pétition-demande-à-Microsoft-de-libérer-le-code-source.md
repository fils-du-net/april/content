---
site: ZDNet France
title: "Windows 7: une pétition demande à Microsoft de libérer le code source"
date: 2020-01-27
href: https://www.zdnet.fr/actualites/windows-7-une-petition-demande-a-microsoft-de-liberer-le-code-source-39898055.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/12/windows7__w630.jpg
tags:
- Logiciels privateurs
series:
- 202005
---

> La Free Software Foundation a lancé cette initiative en appelant Microsoft à rendre Windows 7 open source pour que la communauté “l'étudie et l'améliore”.
