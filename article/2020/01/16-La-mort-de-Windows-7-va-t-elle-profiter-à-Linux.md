---
site: GinjFo
title: "La mort de Windows 7 va-t-elle profiter à Linux?"
author: Jérôme Gianoli
date: 2020-01-16
href: https://www.ginjfo.com/actualites/logiciels/linux/la-mort-de-windows-7-va-t-elle-profiter-a-linux-20200116
featured_image: https://ads.ginjfo.com/delivery/asyncjs.php
tags:
- Sensibilisation
- Logiciels privateurs
series:
- 202003
---

> La fin de vie de Windows 7 est un moment important de cette année. Microsoft laisse sur le «bord de la route» des centaines de millions d’ordinateurs à travers le monde. Désormais livrés à eux même, ils doivent prendre une décision pour assurer leur sécurité.
