---
site: cio-online.com
title: "La Défense Nationale va-t-elle basculer sur un poste de travail libre?"
author: Bertrand Lemaire
date: 2020-01-15
href: https://www.cio-online.com/actualites/lire-open-bar-11851.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000014782.jpg
seeAlso: "[Bientôt un poste de travail entièrement libre au ministère des Armées?](https://www.april.org/bientot-un-poste-de-travail-entierement-libre-au-ministere-des-armees)"
tags:
- Marchés publics
- Logiciels privateurs
series:
- 202003
series_weight: 0
---

> En répondant à la question d'une sénatrice sur le contrat dit «Open-Bar» au Ministère des Armées, le gouvernement envisage un PC open-source.
