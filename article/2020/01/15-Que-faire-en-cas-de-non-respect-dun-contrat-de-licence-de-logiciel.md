---
site: usine-digitale.fr
title: "Que faire en cas de non respect d'un contrat de licence de logiciel?"
author: Pascal Agosti
date: 2020-01-15
href: https://www.usine-digitale.fr/article/que-faire-en-cas-de-non-respect-d-un-contrat-de-licence-de-logiciel.N919709
featured_image: https://www.usine-digitale.fr/mediatheque/4/8/7/000828784_homePageUne/ordinateur.jpg
tags:
- Licenses
- Institutions
series:
- 202003
series_weight: 0
---

> Généralement, le droit d’utiliser un logiciel est concédé pour un usage spécifique via la conclusion d’un contrat de licence. Les éditeurs reprochent, à ce titre, fréquemment à leurs clients licenciés des actes constitutifs de contrefaçon en se fondant sur le non-respect du contrat. Dès lors, la nature de l'action - contractuelle ou délictuelle - permettant au titulaire des droits de propriété intellectuelle d'obtenir réparation en cas de non-respect des termes de la licence par un utilisateur est une question épineuse.
