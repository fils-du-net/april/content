---
site: Le Monde.fr
title: "Qwant par défaut dans les ordinateurs de l'Etat"
author: Martin Untersinger
date: 2020-01-09
href: https://www.lemonde.fr/pixels/article/2020/01/09/qwant-par-defaut-dans-les-ordinateurs-de-l-etat_6025332_4408996.html
tags:
- Entreprise
- Administration
series:
- 202002
---

> Alors que l’avenir managérial et financier de Qwant se clarifie, l’Etat vient de donner un coup de pouce à ce moteur de recherche français qui se veut être le rival de Google. Dans une note du mercredi 7 janvier, révélée par Libération et dont Le Monde a également obtenu copie, le directeur interministériel du numérique a requis de tous les directeurs d’administration chargés du numérique qu’ils installent «par défaut le moteur de recherche Qwant sur l’ensemble des terminaux, fixes et mobiles» dont ils ont la charge. Ces derniers ont jusqu’au 30 avril pour se conformer à cette consigne.
