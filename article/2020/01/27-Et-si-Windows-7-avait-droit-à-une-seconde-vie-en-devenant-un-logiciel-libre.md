---
site: Presse Citron
title: "Et si Windows 7 avait droit à une seconde vie en devenant un logiciel libre?"
author: Setra
date: 2020-01-27
href: https://www.presse-citron.net/et-si-windows-7-avait-droit-a-une-seconde-vie-en-devenant-un-logiciel-libre/
featured_image: https://www.presse-citron.net/wordpress_prod/wp-content/uploads/2019/10/Un-bâtiment-avec-le-logo-de-Microsoft.jpg
tags:
- Logiciels privateurs
series:
- 202005
---

> Le 14 janvier, Microsoft a débranché le système d'exploitation Windows 7 qui, aujourd'hui, n'est donc plus pris en charge. Mais pour la Free Software Foundation, la firme de Redmond pourrait donner une seconde vie à cet OS en en faisant un logiciel libre.
