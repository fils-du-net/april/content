---
site: La République du Centre
title: "Les candidats du Gâtinais invités à s'engager pour la transition énergétique"
date: 2020-01-24
href: https://www.larep.fr/montargis-45200/actualites/les-candidats-du-gatinais-invites-a-s-engager-pour-la-transition-energetique_13729671
featured_image: https://image1.larep.fr/photoSRC/VVNUJ1paUTgIBhVOCRAHHQ4zRSkXaldfVR5dW1sXVA49/collectif-citoyen-du-gatinais-pour-la-transition_4619655.jpeg
tags:
- Associations
series:
- 202004
---

> Le tout jeune collectif citoyen du Gâtinais pour la transition va aller voir les candidats aux élections municipales de mars et leur demander de s'engager pour l'environnement.
