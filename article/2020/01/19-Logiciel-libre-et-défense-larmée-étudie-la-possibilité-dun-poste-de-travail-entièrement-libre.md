---
site: ZDNet France
title: "Logiciel libre et défense: l'armée étudie la possibilité d'un poste de travail 'entièrement libre'"
author: Thierry Noisette
date: 2020-01-19
href: https://www.zdnet.fr/blogs/l-esprit-libre/logiciel-libre-et-defense-l-armee-etudie-la-possibilite-d-un-poste-de-travail-entierement-libre-39897653.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2020/01/hotel_de_brienne_Mre_Defense_WMC.jpg
tags:
- Marchés publics
series:
- 202003
---

> A une question parlementaire sur son contrat avec Microsoft, le ministère des Armées répond être engagé 'dans une dynamique de moindre dépendance aux grands éditeurs'.
