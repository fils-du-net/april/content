---
site: Datanews.be
title: "5G: l'éléphant qui débarque dans une boutique de porcelaine"
author: Bert Hubert
date: 2020-01-26
href: https://datanews.levif.be/ict/actualite/5g-l-elephant-qui-debarque-dans-une-boutique-de-porcelaine/article-opinion-1244103.html
featured_image: https://web.static-rmg.be/if/c_crop,w_2000,h_1277,x_0,y_0,g_center/c_fit,w_620,h_395/48aaa019864b907e53d10eed27f80450.jpg
tags:
- International
- Entreprise
series:
- 202005
series_weight: 0
---

> Les gouvernements se penchent sur le fait d'accorder ou non leur confiance à Huawei et ZTE, mais dans la pratique, cela ne fera guère de différence. Le problème réside en effet dans l'externalisation massive chez des partenaires, ce qui fait que les opérateurs ont depuis longtemps déjà perdu le contrôle, selon Bert Hubert.
