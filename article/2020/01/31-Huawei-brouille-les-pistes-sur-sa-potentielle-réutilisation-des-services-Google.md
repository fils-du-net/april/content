---
site: Clubic.com
title: "Huawei brouille les pistes sur sa potentielle réutilisation des services Google"
author: Pierre Crochart
date: 2020-01-31
href: https://www.clubic.com/pro/entreprises/huawei/actualite-884253-huawei-brouille-pistes-potentielle-reutilisation-services-google.html
featured_image: https://pic.clubic.com/v1/images/1712046/raw?width=1200&fit=max&hash=8381b41775eb69d897a41c7ada46f31ddece1cbf
tags:
- International
- Entreprise
series:
- 202005
---

> Les déclarations se suivent... et ne se ressemblent pas. Ces derniers jours, plusieurs huiles du géant Huawei ont pris la parole pour tenter de clarifier leur position par rapport à Google et aux sanctions américaines. De vaines tentatives, qui n'ont fait qu'embrouiller l'assistance.
