---
site: Alternatives-economiques
title: "Faites vous-même votre réforme des retraites!"
author: Sandrine Foulon
date: 2020-01-29
href: https://www.alternatives-economiques.fr/meme-reforme-retraites/00091704
featured_image: https://www.alternatives-economiques.fr/sites/default/files/public/styles/ae-169-custom_user_large_1x/public/field/image/gettyimages-1065824694_1.jpg?itok=nqvZMPUC&timestamp=1580407552
tags:
- Open Data
- Économie
series:
- 202005
series_weight: 0
---

> Le collectif Nos retraites propose une version «libérée» et en open source du simulateur du Conseil d'orientation des retraites pour faire soi-même sa réforme des retraites.
