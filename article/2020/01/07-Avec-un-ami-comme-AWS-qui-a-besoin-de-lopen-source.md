---
site: LeMagIT
title: "Avec un ami comme AWS, qui a besoin de l'open source?"
author: Cliff Saran
date: 2020-01-07
href: https://www.lemagit.fr/actualites/252476381/Avec-un-ami-comme-AWS-qui-a-besoin-de-lopen-source
featured_image: https://cdn.ttgtmedia.com/visuals/LeMagIT/hero_article/dog_listening.jpeg
tags:
- Licenses
- Économie
series:
- 202002
series_weight: 0
---

> Les commentaires de l'exécutif d'AWS concernant l'exploitation du code ouvert par le géant du cloud ont mis en doute la viabilité du marché open source.
