---
site: Silicon
title: "Le Bot de demain: vers un service citoyen universel?"
author:  Doriane Dupuy-Lenglet
date: 2020-01-09
href: https://www.silicon.fr/avis-expert/le-bot-de-demain-vers-un-service-citoyen-universel
featured_image: https://www.silicon.fr/wp-content/uploads/2018/09/Amazon-LEx-684x400.jpg
tags:
- Innovation
- Interopérabilité
series:
- 202002
series_weight: 0
---

> Quel est donc l'avenir de ces petits robots décrits comme le nouveau canal de communication entre une entreprise et ses clients?  Aujourd'hui orientés vers une logique «transactionnelle», ils seront demain «conversationnels», c'est à dire autonomes pour vous communiquer ce dont vous avez besoin à un moment donné.
