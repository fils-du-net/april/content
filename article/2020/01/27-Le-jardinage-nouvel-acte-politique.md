---
site: Le Point
title: "Le jardinage: nouvel acte politique?"
author: Nathalie Lamoureux
date: 2020-01-27
href: https://www.lepoint.fr/art-de-vivre/le-jardinage-nouvel-acte-politique-27-01-2020-2359833_4.php
featured_image: https://www.lepoint.fr/images/2020/01/27/19945928lpw-19945976-article-jpg_6882408_980x426.jpg
tags:
- Partage du savoir
series:
- 202005
---

> De la culture à Éden à la conquête du potager au pied de l'immeuble, en passant par les guérillas vertes, le jardinage nourrit, guérit, mobilise.
