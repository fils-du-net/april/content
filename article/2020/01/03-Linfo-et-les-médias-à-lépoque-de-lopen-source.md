---
site: Le Soleil
title: "L'info et les médias à l'époque de l'open source"
author: André Verville
date: 2020-01-03
href: https://www.lesoleil.com/opinions/point-de-vue/linfo-et-les-medias-a-lepoque-de-lopen-source-8c0c873d143040828a6bd692635dca1a
tags:
- Entreprise
series:
- 202001
---

> POINT DE VUE / Peu de gens l'ont remarqué, mais le monde de l'informatique a été le premier à vivre la crise qu'il a lui-même provoquée.
