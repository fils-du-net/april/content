---
site: Livres Hebdo
title: "Hachette reprend lelivrescolaire.fr"
author: Hervé Hugueny
date: 2020-01-22
href: https://www.livreshebdo.fr/article/hachette-reprend-lelivrescolairefr
featured_image: https://images4.livreshebdo.fr/sites/default/files/styles/image_full/public/assets/images/13612154_1181526305202444_9009586369968872536_n.png
tags:
- Contenus libres
- Éducation
series:
- 202004
series_weight: 0
---

> Le premier groupe d'édition français a pris le contrôle du dernier né des éditeurs scolaires, qui a su s’imposer à la faveur des réformes des programmes du collège et du lycée.
