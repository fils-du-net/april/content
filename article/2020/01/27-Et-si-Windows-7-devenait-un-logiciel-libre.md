---
site: Numerama
title: "Et si Windows 7 devenait un logiciel libre?"
author: Julien Lausson
date: 2020-01-27
href: https://www.numerama.com/tech/601893-et-si-windows-7-passait-open-source.html
featured_image: https://www.numerama.com/content/uploads/2019/01/windows-7.jpg
tags:
- Logiciels privateurs
series:
- 202005
series_weight: 0
---

> Le support étendu de Windows 7 a pris fin en janvier. La FSF, qui promeut le logiciel libre, tente de convaincre Microsoft de libérer Windows 7 en l'offrant aux libristes.
