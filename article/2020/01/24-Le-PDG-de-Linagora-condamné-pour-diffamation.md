---
site: Next INpact
title: "Le PDG de Linagora condamné pour diffamation"
date: 2020-01-24
href: https://www.nextinpact.com/brief/le-pdg-de-linagora-condamne-pour-diffamation-10973.htm
featured_image: https://cdn2.nextinpact.com/images/bd/square-linked-media/23678.jpg
tags:
- Entreprise
series:
- 202004
---

> La cour d'appel de Toulouse a confirmé, le 8 janvier dernier, la condamnation pour diffamation du PDG de Linagora, Alexandre Zapolsky.
