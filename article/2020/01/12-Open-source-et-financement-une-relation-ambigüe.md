---
site: Le Monde Informatique
title: "Open source et financement: une relation ambigüe"
author: Matt Asay
date: 2020-01-12
href: https://www.lemondeinformatique.fr/actualites/lire-open-source-et-financement-une-relation-ambigue-77676.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000070135.jpg
tags:
- Économie
- Entreprise
- Innovation
series:
- 202002
series_weight: 0
---

> Une étude s'est intéressé sur le rôle joué par l'argent dans le succès des frameworks JavaScript les plus populaires. Au-delà, la délicate question du rapport argent et développement des projets open source reste posée.
