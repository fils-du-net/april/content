---
site: Le Monde.fr
title: "Logiciels libres et école à distance: la sortie de la suite «Apps.education» précipitée par le confinement"
author: Bastien Lion
date: 2020-06-09
href: https://www.lemonde.fr/pixels/article/2020/06/09/logiciels-libres-et-ecole-a-distance-la-sortie-de-la-suite-apps-education-precipitee-par-le-confinement_6042279_4408996.html
featured_image: https://img.lemde.fr/2020/04/22/0/0/5568/3712/688/0/60/0/f401b89_HE-r3Dnzr9LlayafLLkGzrt5.jpg
seeAlso: "[#65 - Logiciels libres dans l’Éducation nationale - Oisux - Easter-Eggs - «Libre à vous!» diffusée mardi 5 mai 2020 sur radio Cause Commune](https://april.org/65-apps-education)"
tags:
- Éducation
- Associations
- Internet
series:
- 202024
series_weight: 0
---

> En pleine crise sanitaire, le ministère de l’éducation nationale a déployé la première version d’une plate-forme d’outils numériques pour les professeurs, composée de logiciels respectueux des réglementations sur les données des utilisateurs.
