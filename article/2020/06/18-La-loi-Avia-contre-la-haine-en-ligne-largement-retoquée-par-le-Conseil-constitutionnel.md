---
site: Le Monde.fr
title: "La loi Avia contre la haine en ligne largement retoquée par le Conseil constitutionnel"
author: Martin Untersinger et Alexandre Piquard
date: 2020-06-18
href: https://www.lemonde.fr/pixels/article/2020/06/18/le-conseil-constitutionnel-censure-la-disposition-phare-de-la-loi-avia-contre-la-haine-en-ligne_6043323_4408996.html
featured_image: https://img.lemde.fr/2019/03/08/0/0/3500/2337/688/0/60/0/e54d5fa_iNMumQ4TOvs1lBJd-6yIySmX.jpg
tags:
- Internet
- Institutions
series:
- 202025
series_weight: 0
---

> Les obligations pesant sur les réseaux sociaux de retirer en vingt-quatre heures les contenus illégaux n’ont pas été jugées compatibles avec la liberté d’expression.
