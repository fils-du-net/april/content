---
site: ZDNet France
title: "Health Data Hub: des organisations dénoncent un passage en force du projet dans le contexte d'urgence sanitaire"
author: Clarisse Treilles
date: 2020-06-11
href: https://www.zdnet.fr/actualites/health-data-hub-des-organisations-denoncent-un-passage-en-force-du-projet-dans-le-contexte-d-urgence-sanitaire-39905045.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2018/11/sante-digital-620__w630.jpg
tags:
- Vie privée
series:
- 202024
---

> Un collectif de personnalités et d’organisations mené par le Conseil national du logiciel libre a déposé un référé-liberté devant le Conseil d'Etat contre le déploiement accéléré du projet Health Data Hub pendant l'état d'urgence sanitaire. Les requérants contestent le fait de mettre la technologie au service de la santé... au détriment des règles de droit.
