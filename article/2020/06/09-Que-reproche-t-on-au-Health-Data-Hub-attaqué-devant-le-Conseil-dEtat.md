---
site: usine-digitale.fr
title: "Que reproche-t-on au Health Data Hub, attaqué devant le Conseil d'Etat?"
author: Alice Vitard
date: 2020-06-09
href: https://www.usine-digitale.fr/article/que-reproche-t-on-au-health-data-hub-attaque-devant-le-conseil-d-etat.N973541
featured_image: https://www.usine-digitale.fr/mediatheque/1/2/6/000876621_homePageUne/reunion-entre-medecins.jpg
seeAlso: "[InterHop - Les hôpitaux français pour l'interopérabilité et le partage libre des algorithmes](https://interhop.org)"
tags:
- Vie privée
- Entreprise
- International
- Institutions
series:
- 202024
series_weight: 0
---

> Le déploiement du Health Data Hub est attaqué devant le Conseil d'Etat. Les requérants estiment que cette base de données médicales portent atteinte à la vie privée des 67 millions de Français. Le choix de l'hébergeur, Microsoft, est au centre des plaintes.
