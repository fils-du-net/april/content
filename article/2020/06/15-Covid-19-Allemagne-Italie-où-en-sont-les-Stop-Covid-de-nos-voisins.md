---
site: Clubic.com
title: "Covid-19: Allemagne, Italie... où en sont les \"Stop-Covid\" de nos voisins?"
author: Alexandre Boero
date: 2020-06-15
href: https://www.clubic.com/coronavirus/actualite-3632-covid-19-allemagne-italie-ou-en-sont-les-stop-covid-de-nos-deux-voisins-.html
featured_image: https://pic.clubic.com/v1/images/1802928/raw-accept?width=1200&fit=max&hash=27f997873061aeead1db5421eca317f2b52b5ea3
tags:
- Innovation
- International
series:
- 202025
---

> L'application Corona Warn-App sera lancée cette semaine outre-Rhin, tandis que la version italienne est déjà un succès.
