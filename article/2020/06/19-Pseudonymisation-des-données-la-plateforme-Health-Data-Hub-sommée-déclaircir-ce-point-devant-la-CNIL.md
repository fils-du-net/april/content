---
site: ZDNet France
title: "Pseudonymisation des données: la plateforme Health Data Hub sommée d'éclaircir ce point devant la CNIL"
author: Clarisse Treilles
date: 2020-06-19
href: https://www.zdnet.fr/actualites/pseudonymisation-des-donnees-la-plateforme-health-data-hub-sommee-d-eclaircir-ce-point-devant-la-cnil-39905461.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/02/Jumeau%20numerique%20sante%20620__w630.jpg
seeAlso: "[InterHop - Les hôpitaux français pour l'interopérabilité et le partage libre des algorithmes](https://interhop.org)"
tags:
- Vie privée
- Associations
- Institutions
series:
- 202025
series_weight: 0
---

> La plus haute juridiction administrative estime globalement, dans une décision publiée ce vendredi, que le projet de plateforme centralisée des données santé France n'a pas lieu de porter atteinte au droit au respect de la vie privée et à la protection des données personnelles.
