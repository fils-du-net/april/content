---
site: RJB
title: "Le Grand Conseil en salle de classe"
date: 2020-06-10
href: https://www.rjb.ch/rjb/Actualite/Region/20200610-Le-Grand-Conseil-en-salle-de-classe.html
featured_image: https://bnj.blob.core.windows.net/assets/Htdocs/Images/Theme/rjb/loading-rjb.gif
tags:
- Éducation
series:
- 202024
---

> Le législatif bernois a avalisé et classé la possibilité de ne réaliser qu'une année d'école enfantine avant d'entrer à l'école obligatoire. L'utilisation de logiciels libres sera également favorisée au sein de l'instruction publique.
