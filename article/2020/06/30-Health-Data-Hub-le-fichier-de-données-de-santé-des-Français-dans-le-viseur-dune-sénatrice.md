---
site: BFMtv
title: "Health Data Hub: le fichier de données de santé des Français dans le viseur d'une sénatrice"
author: Elsa Trujillo
date: 2020-06-30
href: https://www.bfmtv.com/tech/health-data-hub-le-fichier-de-donnees-de-sante-des-francais-dans-le-viseur-d-une-senatrice_AN-202006300188.html
featured_image: https://images.bfmtv.com/rASlHeAMSTAcvoQ_MtXENQF-Uwo=/0x0:800x450/800x0/images/Microsoft-sera-charge-dheberger-les-donnees-de-sante-des-Francais-371043.jpg
seeAlso: "[InterHop - Les hôpitaux français pour l'interopérabilité et le partage libre des algorithmes](https://interhop.org)"
tags:
- Vie privée
- Institutions
- Entreprise
series:
- 202027
series_weight: 0
---

> L'hébergement d'un mégafichier de données de santé des Français a été confié à Microsoft. Vivement critiqué, ce projet devrait faire l'objet d'une commission d'enquête.
