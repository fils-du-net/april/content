---
site: Next INpact
title: "Cyberhaine: la loi Avia largement censurée par le Conseil constitutionnel"
author: Marc Rees
date: 2020-06-18
href: https://www.nextinpact.com/news/109096-cyberhaine-loi-avia-largement-censuree-par-conseil-constitutionnel.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/6653.jpg
tags:
- Internet
- Institutions
series:
- 202025
---

> Un mois jour pour jour après avoir été saisi par plus de 60 sénateurs, le Conseil constitutionnel vient de rendre sa décision. C’est peu de le dire: la loi Avia contre la Haine en ligne subit un énorme revers. Explications.
