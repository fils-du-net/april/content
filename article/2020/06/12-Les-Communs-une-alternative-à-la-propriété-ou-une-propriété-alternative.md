---
site: France Culture
title: "«Les Communs»: une alternative à la propriété ou une propriété alternative?"
date: 2020-06-12
href: https://www.franceculture.fr/conferences/radio-campus-france/les-communs-une-alternative-a-la-propriete-ou-une-propriete-alternative
featured_image: https://cdn.radiofrance.fr/s3/cruiser-production/2020/06/a9424e9f-5797-48c0-8f39-b8df0d33d241/838_les_communs1.webp
tags:
- Économie
series:
- 202024
series_weight: 0
---

> Jardins partagés, habitats participatifs, plateformes d'échanges de services, logiciels libres..., : « Les Communs » sont de plus en plus présents dans notre quotidien.
