---
site: Trust My Science 
title: "Premier modèle complet «open source» de la protéine de pointe du coronavirus SARS-CoV-2"
author: Fleur Brosseau
date: 2020-06-23
href: https://trustmyscience.com/premier-modele-open-source-proteine-pointe-sars-cov-2
featured_image: https://trustmyscience.com/wp-content/uploads/2020/06/modelisation-proteine-pointe-open-source-750x400.jpeg
tags:
- Sciences
series:
- 202026
series_weight: 0
---

> La protéine de pointe du SARS-CoV-2 permet au nouveau coronavirus de pénétrer dans les cellules humaines. Elle est donc au cœur des recherches dans le monde entier. Une équipe internationale de chercheurs propose aujourd’hui le tout premier modèle open source complet de cette protéine, ce qui permettra à toute la communauté scientifique d’effectuer diverses simulations dans le cadre de leurs recherches autour du COVID-19.
