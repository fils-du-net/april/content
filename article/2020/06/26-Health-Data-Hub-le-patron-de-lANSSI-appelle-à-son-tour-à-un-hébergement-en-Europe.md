---
site: Next INpact
title: "Health Data Hub: le patron de l'ANSSI appelle à son tour à un hébergement en Europe"
date: 2020-06-26
href: https://www.nextinpact.com/brief/health-data-hub---le-patron-de-l-anssi-appelle-a-son-tour-a-un-hebergement-en-europe-12858.htm
featured_image: https://cdn2.nextinpact.com/images/bd/square-linked-media/19112.jpg
seeAlso: "[InterHop - Les hôpitaux français pour l'interopérabilité et le partage libre des algorithmes](https://interhop.org)"
tags:
- Vie privée
- Institutions
- Entreprise
series:
- 202026
series_weight: 0
---

> Le sujet de la dépendance à Microsoft Azure est revenu sur le tapis à l'occasion de la conférence autour de StopCovid, cette dernière étant hébergée par Outscale de Dassault, une différence qui pose question.
