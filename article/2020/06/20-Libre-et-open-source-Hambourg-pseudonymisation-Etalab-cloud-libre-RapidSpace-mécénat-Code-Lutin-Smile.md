---
site: ZDNet France
title: "Libre et open source: Hambourg, pseudonymisation Etalab, cloud libre Rapid.Space, mécénat Code Lutin, Smile"
author: Thierry Noisette
date: 2020-06-20
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-hambourg-pseudonymisation-etalab-cloud-libre-rapidspace-mecenat-code-lutin-smile-39905491.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/05/open-source-KeepCalmAndUse.jpg
tags:
- Sensibilisation
- Le logiciel libre
series:
- 202025
series_weight: 0
---

> Express: l'Allemagne portée sur les logiciels libres, un outil d'IA de pseudonymisation, du cloud 100% en code libre, mécénat pour projets libres, un VP e-commerce chez Smile.
