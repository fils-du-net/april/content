---
site: The Conversation
title: "Universités: l'«open education», clé de la résilience post-Covid?"
author: Pierre Boulet
date: 2020-06-23
href: https://theconversation.com/universites-l-open-education-cle-de-la-resilience-post-covid-139602
featured_image: https://images.theconversation.com/files/342791/original/file-20200618-41230-1kjfok5.jpg
tags:
- Éducation
- Institutions
series:
- 202026
series_weight: 0
---

> Pour préparer une rentrée soumise aux nouvelles règles de distanciation physique, les établissements du supérieur peuvent mobiliser les ressources libres qui ont fait leurs preuves pendant la crise.
