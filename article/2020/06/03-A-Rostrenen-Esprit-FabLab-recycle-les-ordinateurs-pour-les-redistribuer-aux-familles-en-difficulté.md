---
site: Le Telegramme
title: "A Rostrenen, Esprit FabLab recycle les ordinateurs pour les redistribuer aux familles en difficulté (€)"
date: 2020-06-03
href: https://www.letelegramme.fr/cotes-darmor/rostrenen/a-rostrenen-esprit-fablab-recycle-les-ordinateurs-pour-les-redistribuer-aux-familles-en-difficulte-03-06-2020-12560726.php
featured_image: https://www.letelegramme.fr/images/2020/06/03/les-benevoles-d-esprit-fabl-lab-mardi-matin-dans-leur_5181724_676x449p.jpg
tags:
- Associations
- Sensibilisation
series:
- 202023
---

> L’association Esprit FabLab lance un appel aux dons d’ordinateurs usagés pour les redistribuer à des familles en difficulté. Les adhérents se chargent de les remettre en marche avec des logiciels libres.
