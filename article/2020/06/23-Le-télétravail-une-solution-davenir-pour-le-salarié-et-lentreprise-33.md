---
site: TV5MONDE
title: "Le télétravail, une solution d'avenir pour le salarié et l'entreprise? 3/3"
author: Elena Lionnet
date: 2020-06-23
href: https://information.tv5monde.com/info/le-teletravail-une-solution-d-avenir-pour-le-salarie-et-l-entreprise-33-364579
featured_image: https://information.tv5monde.com/sites/info.tv5monde.com/files/styles/large/public/assets/images/wood-people-desk-office-4041405.jpg
tags:
- Associations
- Entreprise
series:
- 202026
series_weight: 0
---

> Le confinement imposé pour lutter contre l’épidémie de coronavirus a forcé des millions de personnes à rester chez elles. Pour certaines il a été impossible de continuer leur activité, d’autres y ont été obligées même si cela signifiait mettre leur vie en danger. Et puis il y a eu toutes celles qui ont pu travailler chez elles. Avec le déconfinement progressif, la question du télétravail revient donc sur le devant de la scène. Mais qu’est-ce-que le télétravail exactement? Quels sont ses forces et ses faiblesses? Qui peut travailler en télétravail? Et si c’était une occasion pour repenser la façon dont nous travaillons?
