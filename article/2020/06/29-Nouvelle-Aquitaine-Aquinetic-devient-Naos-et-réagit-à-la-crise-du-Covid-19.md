---
site: ZDNet France
title: "Nouvelle-Aquitaine: Aquinetic devient Naos et réagit à la crise du Covid-19"
author: Thierry Noisette
date: 2020-06-29
href: https://www.zdnet.fr/blogs/l-esprit-libre/nouvelle-aquitaine-aquinetic-devient-naos-et-reagit-a-la-crise-du-covid-19-39905935.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2020/06/Nouvelle-Aquitaine_region.jpg
tags:
- Administration
- Économie
series:
- 202027
series_weight: 0
---

> Le cluster Naos, pour 'Nouvelle-Aquitaine Open Source', fédère 120 PME et ETI. Objectif: 'Que le numérique open source serve les enjeux de souveraineté nationale et soutienne la relance de l'économie'.
