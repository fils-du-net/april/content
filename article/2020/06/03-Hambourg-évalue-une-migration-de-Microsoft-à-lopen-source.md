---
site: Le Monde Informatique
title: "Hambourg évalue une migration de Microsoft à l'open source"
author: Jacques Cheminat
date: 2020-06-03
href: https://www.lemondeinformatique.fr/actualites/lire-hambourg-evalue-une-migration-de-microsoft-a-l-open-source-79292.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000072580.jpg
tags:
- Administration
- Logiciels privateurs
series:
- 202023
series_weight: 0
---

> Open Source: Suivant les traces de Munich, la ville de Hambourg a entamé une réflexion pour adopter des logiciels libres à la place des solutions Microsoft.
