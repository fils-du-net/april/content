---
site: Le Point
title: "«Nous ne sommes pas pieds et poings liés à Microsoft»"
author: Guillaume Grallet
date: 2020-06-07
href: https://www.lepoint.fr/technologie/nous-ne-sommes-pas-pieds-et-poings-lies-a-microsoft-07-06-2020-2378817_58.php
featured_image: https://static.lpnt.fr/images/2020/06/07/20421789lpw-20423174-article-jpg_7156500_660x281.jpg
tags:
- Vie privée
- Entreprise
- Institutions
- Informatique en nuage
series:
- 202023
series_weight: 0
---

> L'inquiétude grandit face à données qui ne sont pas hébergées par des acteurs français. La directrice du Health Data Hub Stéphanie Combes répond.
