---
site: ouest-france.fr
title: "Rostrenen. Esprit FabLab lutte contre l’exclusion numérique"
date: 2020-06-21
href: https://www.ouest-france.fr/bretagne/rostrenen-22110/rostrenen-esprit-fablab-lutte-contre-l-exclusion-numerique-6877223
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMDA2NTM4NTM2NDY2MGI4MzBmYTZiMDA4NGI2N2RiZTU2ZTU
tags:
- Associations
- Éducation
series:
- 202025
series_weight: 0
---

> Avec le confinement et la mise en place de la continuité pédagogique, l’association Esprit Fablab, à Rostrenen (Côtes-d’Armor) a été alertée sur l’exclusion numérique pour certaines familles.
