---
site: Clubic.com
title: "La base de données Health Data Hub traînée devant le Conseil d'État, mais pourquoi?"
author: Alexandre Boero
date: 2020-06-12
href: https://www.clubic.com/sante/actualite-3523-la-base-de-donnees-health-data-hub-trainee-devant-le-conseil-d-tat-mais-pourquoi-.html
tags:
- Vie privée
series:
- 202024
---

> Une coalition emmenée par le Conseil national du logiciel libre (CNLL) conteste le lancement de la plateforme française de données de santé ainsi que son hébergement sur le Cloud Microsoft Azure.
