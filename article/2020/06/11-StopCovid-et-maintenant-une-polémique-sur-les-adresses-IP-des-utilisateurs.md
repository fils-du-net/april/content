---
site: Numerama
title: "StopCovid: et maintenant, une polémique sur les adresses IP des utilisateurs"
author: Julien Lausson
date: 2020-06-11
href: https://www.numerama.com/tech/629727-stopcovid-et-maintenant-une-polemique-sur-les-adresses-ip-des-utilisateurs.html
featured_image: view-source:https://www.numerama.com/content/uploads/2020/06/stopcovid-gitlab.png
tags:
- Vie privée
series:
- 202024
---

> Lancée début juin, l'application StopCovid continue de faire l'objet de nombreuses critiques. Son fonctionnement fait aussi l'objet de controverses : la dernière en date porte sur les adresses IP des utilisateurs, qui seraient collectées en secret. La réalité est plus complexe.
