---
site: Alternatives-economiques
title: "Masques, valves, ventilateurs… L'impression 3D au secours des soins"
date: 2020-06-05
href: https://blogs.alternatives-economiques.fr/reseauinnovation/2020/06/05/masques-valves-ventilateurs-l-impression-3d-au-secours-des-soins
featured_image: https://blogs.alternatives-economiques.fr/system/files/styles/banniere_1500x290/private/2018-09/RRI.PNG?itok=UyTkqyjJ
tags:
- Matériel libre
- Innovation
series:
- 202023
series_weight: 0
---

> Des phases de croissance accélérée de la pandémie de Covid-19 ont débouché sur des crises sanitaires fulgurantes. Celles-ci ont conduit à des cas de saturation de l'infrastructure hospitalière et des dispositifs médicaux.
