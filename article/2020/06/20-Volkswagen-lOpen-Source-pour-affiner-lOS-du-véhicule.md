---
site: Leblogauto.com 
title: "Volkswagen: l'Open Source pour affiner l'OS du véhicule"
author: Elisabeth Studer
date: 2020-06-20
href: https://www.leblogauto.com/2020/06/volkswagen-approche-open-source-affiner-los-vehicule.html
featured_image: https://leblogauto.b-cdn.net/wp-content/uploads/2020/06/open-source-electric-vehicule.png
tags:
- Entreprise
- Innovation
series:
- 202025
series_weight: 0
---

> Volkswagen souhaite utiliser une approche Open Source pour affiner les éléments du système d’exploitation du véhicule basé sur un logiciel développé par le constructeur. C’est ce qu’indique Christian Senger, membre du conseil d’administration responsable des services et logiciels numériques.
