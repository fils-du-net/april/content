---
site: francetv info
title: "Le député breton Eric Bothorel missionné par l'Etat sur la circulation et le partage des données"
author: Lucas Hobé
date: 2020-06-24
href: https://france3-regions.francetvinfo.fr/bretagne/cotes-d-armor/depute-breton-eric-bothorel-missionne-etat-circulation-partage-donnees-1845478.html
featured_image: https://france3-regions.francetvinfo.fr/image/MlEs07Y1UElejE8wcnloJUgq06Q/930x620/regions/2020/06/09/5edf3421e90f5_eric_bothorel-3052249-3127101.jpg
tags:
- Open Data
- Institutions
series:
- 202026
series_weight: 0
---

> Le Premier ministre Edouard Philippe a confié au député des Côtes-d'Armor Eric Bothorel (LREM) une mission sur les moyens de favoriser la circulation et le partage des données, selon le Journal officiel.
