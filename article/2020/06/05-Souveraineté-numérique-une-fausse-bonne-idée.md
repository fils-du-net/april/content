---
site: Techniques de l'Ingénieur
title: "Souveraineté numérique: une fausse bonne idée?"
author: Philippe Richard
date: 2020-06-05
href: https://www.techniques-ingenieur.fr/actualite/articles/souverainete-numerique-une-fausse-bonne-idee-80130
featured_image: https://www.techniques-ingenieur.fr/actualite/wp-content/uploads/2020/06/bigdata.jpg
seeAlso: "[Rapport du Sénat sur la souveraineté numérique: il est urgent d'engager la réflexion sur le recours aux logiciels libres au sein de l'État](https://www.april.org/rapport-du-senat-sur-la-souverainete-numerique-il-est-urgent-d-engager-la-reflexion-sur-le-recours-a)"
tags:
- Institutions
- Innovation
- Informatique en nuage
- International
series:
- 202023
series_weight: 0
---

> Le lancement de l'application de traçage StopCovid a été l'occasion d'évoquer à nouveau l'idée de souveraineté numérique. Plusieurs projets ont été lancés en France pour relever ce défi. Sans grande réussite…
