---
site: Les Numeriques
title: "StopCovid: le Royaume-Uni fait défection et la France est plus isolée que jamais en Europe"
author: Romain Challand 
date: 2020-06-18
href: https://www.lesnumeriques.com/vie-du-net/stopcovid-le-royaume-uni-fait-defection-et-la-france-est-plus-isolee-que-jamais-en-europe-n151519.html
featured_image: https://dyw7ncnq1en5l.cloudfront.net/optim/news/15/150929/2d79c9ad-l-application-stopcovid-est-officiellement-disponible__w800.webp
tags:
- Innovation
- International
- Interopérabilité
- Vie privée
series:
- 202024
series_weight: 0
---

> Le Royaume-Uni change de philosophie concernant son app de traçage numérique et embrasse le protocole d'Apple et Google. La France devient ainsi le seul grand d'Europe à conserver une application qui ne fonctionne que sur son territoire.
