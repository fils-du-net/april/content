---
site: ZDNet France
title: "Les vulnérabilités des projets open source populaires ont doublé en 2019"
author: Catalin Cimpanu
date: 2020-06-08
href: https://www.zdnet.fr/actualites/les-vulnerabilites-des-projets-open-source-populaires-ont-double-en-2019-39904857.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2020/06/risksense-delays.png
tags:
- Innovation
series:
- 202024
series_weight: 0
---

> Jenkins et MySQL sont les projets dont les vulnérabilités ont le plus souvent été exploitées par des attaquants au cours des cinq dernières années.
