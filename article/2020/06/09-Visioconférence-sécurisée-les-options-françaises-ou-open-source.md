---
site: LeMagIT
title: "Visioconférence sécurisée: les options françaises ou open source"
date: 2020-06-09
href: https://www.lemagit.fr/essentialguide/Visioconference-securisee-les-options-francaises-ou-open-source
featured_image: https://cdn.ttgtmedia.com/visuals/German/article/work-from-home-3-adobe.jpg
seeAlso: "[#61 - Jitsi – Framasoft – Éducation – Le confinement - «Libre à vous!» diffusée mardi 7 avril 2020 sur radio Cause Commune](https://april.org/61-jitsi)"
tags:
- Innovation
- Internet
series:
- 202024
series_weight: 0
---

> Pour les fonctions et les conversations critiques, il existe de nombreuses alternatives simples, françaises ou open source, afin d’éviter un risque d’espionnage désormais inutile.
