---
site: Contrepoints
title: "Tirer profit du Big Data sans compromettre nos libertés (4/5)"
author: Philippe Mösching
date: 2020-06-01
href: https://www.contrepoints.org/2020/06/01/372580-tirer-profit-du-big-data-sans-compromettre-nos-libertes-4-5
featured_image: https://www.contrepoints.org/wp-content/uploads/2020/05/the-art-of-the-cash-register-by-joe-robertsonCC-BY-NC-ND-2.0-660x374.jpg
tags:
- Open Data
- Vie privée
- Licenses
series:
- 202023
series_weight: 0
---

> Analyse de la proposition de Génération Libre qui souhaite voir nos données rémunérées en échange de leur collecte. Une idée qui ne protège pas nos libertés..
