---
site: Next INpact
title: "Health Data Hub: le Conseil d'État exige des correctifs face au risque de surveillance américaine (€)"
author: Marc Rees
date: 2020-10-14
href: https://www.nextinpact.com/article/44169/health-data-hub-conseil-detat-exige-correctifs-face-au-risque-surveillance-americaine
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/5640.jpg
tags:
- Vie privée
series:
- 202042
---

> En attendant une solution plus pérenne, le Conseil d'État ordonne une mise à jour des contrats signés avec Microsoft pour réduire le risque d'une surveillance par les services du renseignement américains. Il refuse ceci dit de mettre un terme à cet accord. Les requérants annoncent poursuivre la bataille au fond.
