---
site: Mediapart
title: "La Cnil demande l'arrêt du stockage de nos données de santé par Microsoft (€)"
author: Jérôme Hourdeaux
date: 2020-10-09
href: https://www.mediapart.fr/journal/france/091020/la-cnil-demande-l-arret-du-stockage-de-nos-donnees-de-sante-par-microsoft
tags:
- Vie privée
- Institutions
series:
- 202041
series_weight: 0
---

> Dans le cadre d'un recours visant à obtenir la suspension du Health Data Hub, le projet de plateforme centralisant l'ensemble de nos données de santé, le gendarme de la vie privée a transmis au Conseil d'État un mémoire demandant à l'ensemble des acteurs de cesser de confier leur hébergement à Microsoft ou toute autre société soumise «au droit étatsunien». Mediapart le publie.
