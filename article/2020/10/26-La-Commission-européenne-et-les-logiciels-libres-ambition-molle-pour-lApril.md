---
site: ZDNet France
title: "La Commission européenne et les logiciels libres: \"ambition molle\" pour l'April"
author: Thierry Noisette
date: 2020-10-26
href: https://www.zdnet.fr/blogs/l-esprit-libre/la-commission-europeenne-et-les-logiciels-libres-ambition-molle-pour-l-april-39912003.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2020/10/Commission_UE_WMC_Berlaymontgebouw_rond_7_december_2005.JPG
seeAlso: "[Stratégie logiciel libre de la Commission européenne: un «esprit ouvert» qui manque de force](https://april.org/strategie-logiciel-libre-de-la-commission-europeenne-un-esprit-ouvert-qui-manque-de-force)"
tags:
- Europe
- april
- Marchés publics
- Institutions
- Entreprise
series:
- 202044
series_weight: 0
---

> La stratégie de la Commission manque d'actions concrètes, détaillées, par exemple sur les processus de passation des marchés publics ou encore, concernant sa dépendance à Microsoft, estime l'association libriste.
