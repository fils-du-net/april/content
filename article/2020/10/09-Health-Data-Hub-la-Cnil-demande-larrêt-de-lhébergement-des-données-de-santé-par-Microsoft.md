---
site: BFMtv
title: "Health Data Hub: la Cnil demande l'arrêt de l'hébergement des données de santé par Microsoft"
author: Elsa Trujillo
date: 2020-10-09
href: https://www.bfmtv.com/tech/health-data-hub-la-cnil-demande-l-arret-de-l-hebergement-des-donnees-de-sante-par-microsoft_AN-202010090163.html
featured_image: https://images.bfmtv.com/Ii6rgkYA10EIMwfYHcJUclmhLXE=/0x100:1920x1180/1600x0/images/-334681.jpg
tags:
- Vie privée
series:
- 202041
---

> Le gendarme des données personnelles demande à l'ensemble des acteurs appelés à stocker des données de santé de cesser "dans un délai aussi bref que possible" de confier leur hébergement à Microsoft.
