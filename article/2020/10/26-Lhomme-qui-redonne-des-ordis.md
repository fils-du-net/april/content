---
site: Le Droit
title: "L'homme qui redonne des ordis"
author: Mylène Moisan
date: 2020-10-26
href: https://www.ledroit.com/chroniques/mylene-moisan/lhomme-qui-redonne-des-ordis-c19d960f14b2ff34d8437a602926a8d3
featured_image: https://images.omerlocdn.com/resize?url=https%3A%2F%2Fgcm.omerlocdn.com%2Fproduction%2Fglobal%2Ffiles%2Fimage%2Ff20af732-ae0e-4545-a979-6ed53ea5a64c.jpg&stripmeta=true&width=1024&type=webp
tags:
- Associations
series:
- 202044
series_weight: 0
---

> Miguel Ross ne voit rien du tout et pourtant, il arrive avec ses mains à monter un ordinateur à partir d'un tas de pièces détachées.
