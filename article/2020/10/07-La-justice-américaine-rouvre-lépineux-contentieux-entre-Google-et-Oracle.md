---
site: ZDNet France
title: "La justice américaine rouvre l'épineux contentieux entre Google et Oracle"
date: 2020-10-07
href: https://www.zdnet.fr/actualites/la-justice-americaine-rouvre-l-epineux-contentieux-entre-google-et-oracle-39910903.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/Pierre%20temp/Programmation%20langage%20C.jpg
tags:
- Droit d'auteur
series:
- 202041
---

> La Cour suprême des Etats-Unis va rouvrir le contentieux entre Google et Oracle. Avec cette question sous-jacente: une API peut-elle et doit-elle faire l'objet d'un droit d'auteur?
