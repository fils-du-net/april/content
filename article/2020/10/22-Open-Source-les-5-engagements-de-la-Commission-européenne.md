---
site: Silicon
title: "Open Source: les 5 engagements de la Commission européenne"
author: Ariane Beky
date: 2020-10-22
href: https://www.silicon.fr/open-source-engagements-commission-europeenne-349799.html
featured_image: https://www.silicon.fr/wp-content/uploads/2012/10/Quiz-inventeurs-europeens-©-silver-tiger-shutterstock.png
tags:
- Europe
- Innovation
series:
- 202043
series_weight: 0
---

> La stratégie de la Commission européenne en matière de logiciels libres pour 2020-2023 a plusieurs objectifs. Tendre à «l’autonomie numérique» en est un.
