---
site: Numerama
title: "Apache OpenOffice ignore l'appel de LibreOffice qui l'invitait à abdiquer"
author: Julien Lausson
date: 2020-10-16
href: https://www.numerama.com/tech/658541-apache-openoffice-ignore-lappel-de-libreoffice-qui-linvitait-a-abdiquer.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/content/uploads/2020/10/openoffice.jpg?resize=1212,712
tags:
- Innovation
series:
- 202042
---

> Il n'y aura pas de rapprochement entre Apache OpenOffice et LibreOffice. Du moins, pour l'instant. Le premier n'a pas donné suite à l'invitation du second.
