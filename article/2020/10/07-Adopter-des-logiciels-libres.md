---
site: éduscol
title: "Adopter des logiciels libres"
date: 2020-10-07
href: https://eduscol.education.fr/numerique/tout-le-numerique/veille-education-numerique/octobre-2020/adopter-des-logiciels-libres
featured_image: https://eduscol.education.fr/numerique/tout-le-numerique/veille-education-numerique/octobre-2020/adopter-des-logiciels-libres/@@images/64e60712-73dc-41f4-9c04-6f93972db869.jpeg
tags:
- Éducation
- april
series:
- 202040
series_weight: 0
---

> Résolu est un framabook issu de la collaboration entre Framasoft et les CEMEA (Centres d'Entraînement aux Méthodes d'Éducation Active). Cette collection de fiches pratiques présente des pistes de réflexion et d'appropriation de solutions numériques basées sur des logiciels libres.
