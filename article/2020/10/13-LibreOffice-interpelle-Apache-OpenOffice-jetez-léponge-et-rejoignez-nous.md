---
site: Numerama
title: "LibreOffice interpelle Apache OpenOffice: jetez l'éponge et rejoignez-nous"
author: Julien Lausson
date: 2020-10-13
href: https://www.numerama.com/tech/657446-libreoffice-interpelle-apache-openoffice-jetez-leponge-et-rejoignez-nous.html
featured_image: https://c1.lestechnophiles.com/www.numerama.com/content/uploads/2020/10/libreoffice.jpg
tags:
- Innovation
series:
- 202042
series_weight: 0
---

> LibreOffice a pris la plume pour s'adresser à Apache OpenOffice. En substance, il lui est demandé de renoncer à sa suite bureautique et de rejoindre celle qui marche vraiment.
