---
site: La Tribune
title: "Propriété intellectuelle: la guerre Oracle-Google devant la Cour suprême"
date: 2020-10-06
href: https://www.latribune.fr/technos-medias/internet/propriete-intellectuelle-la-guerre-oracle-google-devant-la-cour-supreme-859050.html
featured_image: https://static.latribune.fr/full_width/1258965/oracle-depart-du-dg-chiffre-d-affaires-decevant.jpg
tags:
- Droit d'auteur
- Entreprise
series:
- 202041
series_weight: 0
---

> Le géant des logiciels Oracle accuse l'autre géant Google de lui avoir volé le code de programmation Java qui lui a permis de concevoir le système Android. Le groupe de Mountain View rétorque, lui, que l'exploitation du code Java était gratuite et en open source.
