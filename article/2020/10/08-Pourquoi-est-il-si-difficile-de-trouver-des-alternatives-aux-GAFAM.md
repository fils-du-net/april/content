---
site: ITforBusiness
title: "Pourquoi est-il si difficile de trouver des alternatives aux GAFAM?"
author: Patrick Brébion
date: 2020-10-08
href: https://www.itforbusiness.fr/pourquoi-est-il-si-difficile-de-trouver-des-alternatives-aux-gafam-41068
featured_image: https://www.itforbusiness.fr/wp-content/uploads/2020/10/shutterstock_572043892.jpg
tags:
- Informatique en nuage
- Entreprise
- Internet
series:
- 202041
series_weight: 0
---

> Pour la troisième année consécutive, le Cigref trace un portrait assez noir des relations clients-fournisseurs dans l’IT. Apparemment, les grands éditeurs de l’IT continuent de faire la sourde oreille aux mécontentements des DSI…
