---
site: Marianne
title: "Données de santé hébergées par Microsoft: \"Il y a un problème d'intégrité des responsables publics\""
author: Alexandra Saviana
date: 2020-10-12
href: https://www.marianne.net/societe/big-brother/health-data-hub-heberge-par-microsoft-il-y-a-un-probleme-de-competence-et-dintegrite-des-responsables-publics-qui-traitent-le-numerique
tags:
- Vie privée
- Entreprise
- Institutions
series:
- 202042
series_weight: 0
---

> Après un recours au Conseil d'Etat demandant la suspension du Health Data Hub, le projet de plate-forme qui centralise les données de santé des Français à des fins de recherche médicale, la CNIL a fait connaître ce 8 octobre sa position sur le sujet: le gendarme de la vie privée demande l'arrêt de leur hébergement à Microsoft.
