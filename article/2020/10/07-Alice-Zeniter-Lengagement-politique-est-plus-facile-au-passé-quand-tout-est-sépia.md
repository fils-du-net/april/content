---
site: Usbek & Rica
title: "Alice Zeniter: «L'engagement politique est plus facile au passé, quand tout est sépia»"
author: Fabien Benoit
date: 2020-10-07
href: https://usbeketrica.com/fr/article/l-engagement-politique-est-plus-facile-au-passe-quand-tout-est-sepia
featured_image: https://usbeketrica.com/media/68611/download/Capture%20d%E2%80%99e%CC%81cran%202020-10-05%20a%CC%80%2015.43.28.png?v=1&inline=1
tags:
- Sensibilisation
series:
- 202041
series_weight: 0
---

> La romancière et dramaturge, autrice du plébiscité L'art de perdre (Flammarion), récompensé en 2017 par le Prix Goncourt des lycéens, revient avec Comme un empire dans un empire (Flammarion, 2020), un livre qui convoque le mouvement des Gilets jaunes, Nuit debout et l'hacktivisme pour interroger les espoirs et limites des différentes formes contemporaines de l'engagement.
