---
site: Next INpact
title: "Droits voisins de la presse: condamnée à négocier, Google pourra parfois ne pas payer (€)"
author: Marc Rees
date: 2020-10-08
href: https://www.nextinpact.com/article/44079/droits-voisins-presse-condamnee-a-negocier-google-pourra-parfois-ne-pas-payer
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/1969.jpg
tags:
- Institutions
- Droit d'auteur
series:
- 202041
series_weight: 0
---

> La cour d'appel a rendu ce matin son arrêt. Saisie par Google, elle devait examiner la conformité de la décision de l'Autorité de la concurrence ayant condamné l'entreprise à négocier de bonne foi notamment avec les éditeurs de presse. Explications.
