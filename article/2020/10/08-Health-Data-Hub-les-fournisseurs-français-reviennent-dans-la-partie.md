---
site: ZDNet France
title: "Health Data Hub: les fournisseurs français reviennent dans la partie"
author: Clarisse Treilles
date: 2020-10-08
href: https://www.zdnet.fr/actualites/health-data-hub-les-fournisseurs-francais-reviennent-dans-la-partie-39910993.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2020/01/sante.jpg
tags:
- Vie privée
- Marchés publics
series:
- 202041
---

> Microsoft pourrait définitivement être écarté du dossier Health Data Hub à la suite de l'invalidation du Privacy Shield.
