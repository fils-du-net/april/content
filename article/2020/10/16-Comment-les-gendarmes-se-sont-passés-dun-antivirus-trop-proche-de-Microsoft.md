---
site: L'Essor 
title: "Comment les gendarmes se sont passés d'un antivirus trop proche de Microsoft"
author: Gabriel Thierry
date: 2020-10-16
href: https://lessor.org/a-la-une/le-bel-epilogue-du-refus-des-gendarmes-dun-antivirus-trop-lie-a-microsoft
featured_image: https://lessor.org/wp-content/uploads/2017/05/Gendbuntu_12.04_screenshot-e1500645224709-660x330.png
tags:
- Administration
- Logiciels privateurs
series:
- 202042
series_weight: 0
---

> 7 ans après leur refus de l'antivirus informatique McAfee, trop lié à Microsoft, les gendarmes ont trouvé leur bonheur avec un autre éditeur.
