---
site: 20minutes.fr
title: "Comment concurrencer Google, Amazon, Facebook et les autres géants du numérique?"
author: Nicolas Raffin
date: 2020-10-22
href: https://www.20minutes.fr/economie/2891247-20201022-comment-concurrencer-google-amazon-facebook-autres-geants-numerique
featured_image: https://img.20mn.fr/UawPbmCLRUqUSlQiTAUFDQ/310x190_mark-zuckerberg-is-watching-you.jpg
tags:
- Économie
- Entreprise
series:
- 202043
---

> Les auteurs proposent notamment de mieux réguler le contrôle des données des utilisateurs
