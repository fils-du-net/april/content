---
site: The Conversation
title: "La lutte pour une agriculture libre: bricoler et partager pour s'émanciper"
author: Morgan Meyer
date: 2020-10-13
href: https://theconversation.com/la-lutte-pour-une-agriculture-libre-bricoler-et-partager-pour-semanciper-147051
featured_image: https://images.theconversation.com/files/361032/original/file-20201001-22-v57jkw.jpg
tags:
- Partage du savoir
- Matériel libre
series:
- 202042
series_weight: 0
---

> Face au verrouillage des machines agricoles et des semences par des industriels, des agriculteurs utilisent les outils des communs pour promouvoir une agriculture libre.
