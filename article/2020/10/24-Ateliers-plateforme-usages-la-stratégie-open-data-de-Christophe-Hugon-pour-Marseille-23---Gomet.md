---
site: Gomet 
title: "Ateliers, plateforme, usages... la stratégie open data de Christophe Hugon pour Marseille (2/3)"
author: Lena Cardo
date: 2020-10-24
href: https://gomet.net/ateliers-plateforme-usages-la-strategie-open-data-de-christophe-hugon-pour-marseille-2-3
featured_image: https://mk0gomet3vhlwol4683.kinstacdn.com/wp-content/uploads/2020/10/hugon_2-750x375.jpg
tags:
- april
- Associations
- Open Data
series:
- 202043
series_weight: 0
---

> Christophe Hugon, conseiller municpal en charge de la transparence et de l'open data présente ses nouvelles missions auprès de la mairie de Marseille.
