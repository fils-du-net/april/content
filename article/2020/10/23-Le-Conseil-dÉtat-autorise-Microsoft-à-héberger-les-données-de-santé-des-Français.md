---
site: Developpez.com
title: "Le Conseil d'État autorise Microsoft à héberger les données de santé des Français"
date: 2020-10-20
description: Malgré l'invalidation de l'accord Privacy Shield et les recommandations de la CNIL
author: Stéphane le Calme
href: https://droit.developpez.com/actu/309820/Le-Conseil-d-Etat-autorise-Microsoft-a-heberger-les-donnees-de-sante-des-Francais-malgre-l-invalidation-de-l-accord-Privacy-Shield-et-les-recommandations-de-la-CNIL
featured_image: https://www.developpez.net/forums/attachments/p581854d1/a/a/a
seeAlso: "[Signez la pétition pour une commission d'enquête sénatoriale sur le Health Data Hub](https://april.org/signez-la-petition-pour-une-commission-d-enquete-senatoriale-sur-le-health-data-hub)"
tags:
- Vie privée
- Entreprise
- Institutions
- International
series:
- 202043
series_weight: 0
---

> La Plateforme pour centraliser les données de santé, organisme public également appelé «Health Data Hub», a été créée fin novembre 2019 afin de faciliter le partage des données de santé afin de favoriser la recherche.
