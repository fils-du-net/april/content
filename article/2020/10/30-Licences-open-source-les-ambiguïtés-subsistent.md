---
site: LeMagIT
title: "Licences open source: les ambiguïtés subsistent"
author: Gaétan Raoul
date: 2020-10-30
href: https://www.lemagit.fr/actualites/252491436/Licences-open-source-qui-dit-code-mal-documente-dit-ambiguite
featured_image: https://cdn.ttgtmedia.com/visuals/searchCompliance/compliance_operations/compliance_article_004.jpg
tags:
- Licenses
series:
- 202044
series_weight: 0
---

> Avec la multiplication des projets open source, la gestion des licences devient de plus en plus compliquée. Une étude réalisée par ClearlyDefined sur 5000 packages applicatifs sous licence open source démontre que seulement 5 % d’entre eux sont suffisamment documentés pour éviter les ambiguïtés de droits.
