---
site: Le Journal de Montréal
title: "L'accès universel à la science, plus que jamais nécessaire face à la COVID-19"
href: https://www.journaldemontreal.com/2020/10/27/lacces-universel-a-la-science-plus-que-jamais-necessaire-face-a-la-covid-19
date: 2020-10-27
featured_image: https://m1.quebecormedia.com/emp/emp/4d35fb30-e881-11ea-b04e-f7a5fe224d1b_ORIGINAL.jpg
tags:
- Sciences
- Partage du savoir
series:
- 202044
series_weight: 0
---

> La crise a révélé la solidarité dont peut faire preuve la communauté scientifique internationale, a indiqué mardi la directrice générale de l'UNESCO.
