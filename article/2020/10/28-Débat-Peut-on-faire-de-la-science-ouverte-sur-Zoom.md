---
site: The Conversation
title: "Débat: Peut-on faire de la science ouverte sur Zoom?"
author: Alexandre Hocquet
date: 2020-10-28
href: https://theconversation.com/debat-peut-on-faire-de-la-science-ouverte-sur-zoom-146491
featured_image: https://images.theconversation.com/files/364964/original/file-20201022-21-1r5g0h.jpg
tags:
- Sciences
- Éducation
- Partage du savoir
series:
- 202044
series_weight: 0
---

> Au plus fort de la pandémie de coronavirus, plusieurs grands éditeurs ont eu l’idée d’ouvrir l’accès à la littérature scientifique pour tou.te.s, un geste applaudi à la fois par la communauté et dans les médias.
