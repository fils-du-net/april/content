---
site: Silicon
title: "Open Source: des emplois par milliers... pour les DevOps?"
author: Ariane Beky
date: 2020-10-30
href: https://www.silicon.fr/open-source-emplois-milliers-devops-350343.html
featured_image: https://www.silicon.fr/wp-content/uploads/2020/02/open-source_photo-via-visualhunt.jpg
tags:
- Économie
series:
- 202044
---

> Malgré la crise économique et sanitaire, les compétences open source sont toujours très recherchées. Les profils orientés DevOps le sont en priorité.
