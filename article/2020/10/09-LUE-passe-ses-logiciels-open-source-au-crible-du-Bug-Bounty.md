---
site: ITforBusiness
title: "L'UE passe ses logiciels open source au crible du Bug Bounty"
author: Marie Varandat
date: 2020-10-09
href: https://www.itforbusiness.fr/lue-passe-ses-logiciels-open-source-au-crible-du-bug-bounty-41073
featured_image: https://www.itforbusiness.fr/wp-content/uploads/2020/10/shutterstock_1764951257.jpg
tags:
- Europe
- Innovation
series:
- 202041
series_weight: 0
---

> L’Union européenne a lancé deux vastes programmes pour vérifier la sécurité des principaux logiciels open source utilisés par ses institutions. Étonnants à bien des égards, ces programmes ont été conçus comme une démarche pour le bien commun et non pas pour garantir la sécurité des implémentations effectuées par l’UE.
