---
site: Le Monde.fr
title: "«Pour encadrer les plates-formes numériques, il faut des outils nouveaux, mais flexibles»"
author: Alexandre Piquard
date: 2020-10-27
href: https://www.lemonde.fr/economie/article/2020/10/27/pour-encadrer-les-plates-formes-numeriques-il-faut-des-outils-nouveaux-mais-flexibles_6057505_3234.html
featured_image: https://img.lemde.fr/2020/10/26/0/0/5760/3840/688/0/60/0/90fbfbc_117403934-rea-264830-022.jpg
tags:
- Entreprise
series:
- 202044
---

> Isabelle de Silva, présidente de l'Autorité de la concurrence, juge prometteuses les réformes en cours mises en route par Bruxelles.
