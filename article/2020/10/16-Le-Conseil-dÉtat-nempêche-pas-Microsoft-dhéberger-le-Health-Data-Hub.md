---
site: ChannelNews
title: "Le Conseil d'État n'empêche pas Microsoft d'héberger le Health Data Hub"
author: Dirk Basyn
date: 2020-10-16
href: https://www.channelnews.fr/le-conseil-detat-nempeche-pas-microsoft-dheberger-le-health-data-hub-99475
featured_image: https://www.channelnews.fr/wp-content/uploads/2020/10/Conseil_dEtat_Paris.jpg
tags:
- Vie privée
series:
- 202042
---

> Craignant de possibles transferts de données de santé personnelles vers les États-Unis, le collectif SantéNathon – qui regroupe des associations, des syndicats et des personnalités, notamment le Conseil national du logiciel libre, le Syndicat National des Journalistes, le Syndicat de la médecine générale et le professeur Didier Sicard de l’université Paris Descartes – a demandé au juge des référés du Conseil d’État de suspendre en urgence la plateforme Health Data Hub.
