---
site: Acteurs Publics 
title: "La Cnil demande de retirer à Microsoft l'hébergement des données de santé"
author: Emile Marzolf
date: 2020-10-09
href: https://www.acteurspublics.fr/articles/la-cnil-demande-de-retirer-a-microsoft-lhebergement-des-donnees-de-sante
tags:
- Vie privée
series:
- 202041
---

> Le gendarme des données personnelles demande à la plate-forme nationale des données de santé, le Health Data Hub, entre autres, de mettre un terme à l’hébergement des données de santé des Français par le géant informatique, et par tout autre acteur soumis aux lois extraterritoriales américaines. Ce revirement intervient après l’invalidation au niveau européen du “Privacy Shield”, le mécanisme permettant de sécuriser les transferts de données entre les deux rives de l’Atlantique.
