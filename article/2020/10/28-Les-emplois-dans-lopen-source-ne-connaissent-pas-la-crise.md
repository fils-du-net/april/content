---
site: Le Monde Informatique
title: "Les emplois dans l'open source ne connaissent pas la crise"
author: Véronique Arène
date: 2020-10-28
href: https://www.lemondeinformatique.fr/actualites/lire-les-emplois-dans-l-open-source-ne-connaissent-pas-la-crise-80848.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000075014.jpg
tags:
- Économie
series:
- 202044
---

> Dans son rapport 2020 sur l'emploi open source, la Linux Foundation relève une forte demande pour ce type de compétences par rapport à 2018. Pour 81% des DRH les recrutements de spécialistes du domaine sont considérés comme prioritaires, tandis que 56% prévoient des embauches dans les six prochains mois. Les DevOps sont les plus recherchés.
