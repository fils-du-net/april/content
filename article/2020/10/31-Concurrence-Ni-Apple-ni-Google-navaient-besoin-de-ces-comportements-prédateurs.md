---
site: Le Monde.fr
title: "Concurrence: «Ni Apple ni Google n'avaient besoin de ces comportements prédateurs» (€)"
date: 2020-10-31
href: https://www.lemonde.fr/idees/article/2020/10/31/concurrence-ni-apple-ni-google-n-avaient-besoin-de-ces-comportements-predateurs_6058000_3232.html
tags:
- Entreprise
- Internet
- Économie
series:
- 202044
series_weight: 0
---

> Le professeur de communication Charles Cuvelliez et le cryptographe Jean-Jacques Quisquater étudient, dans une tribune au «Monde», le rapport très complet de la commission antitrust du Congrès américain qui analyse le comportement anticoncurrentiel des GAFA.
