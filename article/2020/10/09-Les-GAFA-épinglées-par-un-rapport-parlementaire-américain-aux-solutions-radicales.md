---
site: Next INpact
title: "Les GAFA épinglées par un rapport parlementaire américain, aux solutions radicales (€)"
date: 2020-10-09
href: https://www.nextinpact.com/article/44060/les-gafa-epinglees-par-rapport-parlementaire-americain-aux-solutions-radicales
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/530.jpg
tags:
- Entreprise
- Institutions
series:
- 202041
series_weight: 0
---

> La Chambre des représentants a livré mardi un rapport conséquent sur les activités de Google, Amazon, Facebook et Apple. Les plateformes américaines y sont épinglées à de nombreuses reprises pour pratiques anticoncurrentielles. Les auteurs suggèrent des mesures musclées, dont le démantèlement.
