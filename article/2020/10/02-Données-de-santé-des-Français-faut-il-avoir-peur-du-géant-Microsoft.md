---
site: France Inter
title: "Données de santé des Français: faut-il avoir peur du géant Microsoft?"
date: 2020-10-02
href: https://www.franceinter.fr/donnees-de-sante-des-francais-faut-il-avoir-peur-du-geant-microsoft
featured_image: https://cdn.radiofrance.fr/s3/cruiser-production/2020/10/4ba16df6-277e-43ad-b114-7a422fcaa5b1/1136_photo_1_feuille_de_soin.webp
tags:
- Vie privée
- Institutions
series:
- 202040
series_weight: 0
---

> C'est un choix qui passe mal au moment où le gouvernement prône un retour à une souveraineté en matière de santé. L'américain Microsoft a été choisi pour héberger les données de santé des 67 millions de Français. Des voix s'élèvent jusque dans la majorité pour demander la suspension du contrat.
