---
site: Le Monde.fr
title: "La justice américaine ouvre une procédure contre Google pour abus de position dominante"
date: 2020-10-20
href: https://www.lemonde.fr/international/article/2020/10/20/la-justice-americaine-ouvre-une-procedure-contre-google-pour-abus-de-position-dominante_6056727_3210.html
tags:
- Économie
- Entreprise
- Institutions
- International
series:
- 202043
series_weight: 0
---

> Il pourrait s'agir du «procès pour abus de position dominante le plus important en une génération», selon un sénateur républicain très critique à l'encontre des GAFA.
