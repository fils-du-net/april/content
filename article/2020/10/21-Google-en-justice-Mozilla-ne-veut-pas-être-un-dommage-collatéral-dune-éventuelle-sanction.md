---
site: Clubic.com
title: "Google en justice: Mozilla ne veut pas être un dommage collatéral d'une éventuelle sanction"
author: Guillaume Belfiore
date: 2020-10-21
href: https://www.clubic.com/navigateur-internet/mozilla-firefox/actualite-18289-google-en-justice-mozilla-ne-veut-pas-etre-un-dommage-collateral-d-une-eventuelle-sanction.html
featured_image: https://pic.clubic.com/v1/images/1744732/raw-accept?width=1200&fit=max&format=webp&hash=8dc40fdb9b170b98fc3b2548a438fe021df8d39a
tags:
- Internet
- Économie
- Innovation
series:
- 202043
series_weight: 0
---

> Suite à la plainte déposée par le département américain de la justice à l'encontre de Google, la fondation Mozilla s'inquiète des éventuelles conséquences dont elle pourrait faire les frais.
