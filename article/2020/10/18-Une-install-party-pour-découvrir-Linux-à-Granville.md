---
site: ouest-france.fr
title: "Une install party pour découvrir Linux, à Granville"
date: 2020-10-18
href: https://www.ouest-france.fr/normandie/granville-50400/une-install-party-pour-decouvrir-linux-a-granville-7019634
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMDEwNjY0MzdhNDUyYTk2MGU3YTJlZmVjYmVhNzM1MjlhYjk
tags:
- Sensibilisation
series:
- 202042
series_weight: 0
---

> Une quinzaine de personnes s’est présentée à la médiathèque de Granville avec son ordinateur sous le bras, samedi 17 octobre 2020, pour installer sur ce dernier un système d’exploitation libre.
