---
site: InformatiqueNews.fr
title: "Microsoft n'abandonne ni Windows, ni Windows on ARM, bien au contraire…"
author: Loïc Duval
date: 2020-10-01
href: https://www.informatiquenews.fr/microsoft-nabandonne-ni-windows-ni-windows-on-arm-bien-au-contraire-73419
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2020/10/shutterstock_1082091851.jpg
tags:
- Logiciels privateurs
- Entreprise
series:
- 202040
series_weight: 0
---

> Alors que, suite à la publication d’un billet de blog de Eric S. Raymond beaucoup ont annoncé un manque d’intérêt de Microsoft pour Windows au profit de Linux, la réalité du moment s’avère l’exact opposé avec des investissements renforcés et l’arrivée prochaine de l’émulation x64 sous ARM.
