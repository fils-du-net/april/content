---
site: Le Figaro.fr
title: "Le Parlement européen appelle à une stricte régulation des géants du numérique"
date: 2020-10-21
href: https://www.lefigaro.fr/secteur/high-tech/le-parlement-europeen-appelle-a-une-stricte-regulation-des-geants-du-numerique-20201021
featured_image: https://i.f1g.fr/media/cms/767x431_crop/2020/10/21/5974348f2d15d7421e1ca59acfebdc420b1b463080891aae907310465d83e32d.jpeg
tags:
- Économie
series:
- 202043
---

> Trois textes ont rassemblé une majorité d'eurodéputés pour mieux superviser les grandes plateformes. Ils incitent ainsi la Commission à la fermeté dans son projet de nouvelle législation.
