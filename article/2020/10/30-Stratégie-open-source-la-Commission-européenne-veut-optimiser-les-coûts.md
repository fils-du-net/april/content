---
site: LeMagIT
title: "Stratégie open source: la Commission européenne veut optimiser les coûts"
author: Gaétan Raoul
date: 2020-10-30
href: https://www.lemagit.fr/actualites/252491372/Strategie-open-source-la-Commission-europeenne-veut-reduire-les-couts
featured_image: https://cdn.ttgtmedia.com/visuals/German/EU-european-flags-fotolia.jpg
tags:
- Europe
- april
- Marchés publics
series:
- 202044
---

> La Commission européenne a présenté sa stratégie open source pour l’horizon 2020-2023. La CE veut favoriser l’utilisation en interne de logiciels open source, et ouvrir le code de ceux qu’elle utilise déjà. Derrière, les grands mots, la transformation de l’administration doit se faire à moindre coût.
