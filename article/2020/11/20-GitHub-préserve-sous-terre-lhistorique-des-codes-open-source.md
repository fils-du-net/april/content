---
site: Le Monde Informatique
title: "GitHub préserve sous terre l'historique des codes open source"
author: Scott Carey
date: 2020-11-20
href: https://www.lemondeinformatique.fr/actualites/lire-github-preserve-sous-terre-l-historique-des-codes-open-source-81093.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000075498.png
tags:
- Partage du savoir
series:
- 202047
---

> Le programme d'archivage de Github vise à préserver des éléments historiquement pertinents de logiciels open source pour permettre aux futurs développeurs de logiciels de voir comment la communauté a construit et révisé le code.
