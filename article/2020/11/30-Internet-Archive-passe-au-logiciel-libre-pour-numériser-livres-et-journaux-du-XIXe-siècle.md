---
site: ZDNet France
title: "Internet Archive passe au logiciel libre pour numériser livres et journaux du XIXe siècle"
author: Thierry Noisette
date: 2020-11-30
href: https://www.zdnet.fr/blogs/l-esprit-libre/internet-archive-passe-au-logiciel-libre-pour-numeriser-livres-et-journaux-du-xixe-siecle-39913975.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2019/01/1850%20journal_Internet%20Archive.jpg
tags:
- Logiciels privateurs
- Partage du savoir
- Innovation
- Internet
series:
- 202049
series_weight: 0
---

> Le site d'archivage du Web abandonne les solutions propriétaires qu'il employait jusqu'à présent pour le logiciel libre Tesseract.
