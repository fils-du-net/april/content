---
site: Rue89 Strasbourg
title: "Professeur en informatique, Germain Forestier inspire scientifiques et gouvernants sur le Covid"
author: Jean-François Gérard
date: 2020-11-13
href: https://www.rue89strasbourg.com/portrait-germain-forestier-mulhouse-informaticien-chercheur-covid-193802
featured_image: https://www.rue89strasbourg.com/wp-content/uploads/2020/11/50593500706-10f830bba6-k-630x420.jpg
tags:
- Sciences
- Open Data
series:
- 202046
series_weight: 0
---

> Portrait de Germain Forestier, enseignant-chercheur en informatique à Mulhouse auteur de visualisations très partagées sur Twitter.
