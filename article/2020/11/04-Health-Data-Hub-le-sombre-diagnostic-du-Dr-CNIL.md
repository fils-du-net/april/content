---
site: Next INpact
title: "Health Data Hub: le sombre diagnostic du Dr CNIL (€)"
author: Marc Rees
date: 2020-11-04
href: https://www.nextinpact.com/article/44419/health-data-hub-sombre-diagnostic-dr-cnil
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/5640.jpg
tags:
- Vie privée
- Institutions
- Entreprise
series:
- 202045
series_weight: 0
---

> La plateforme des données de santé, hébergée par Microsoft, fut mise en œuvre de façon anticipée en avril 2020, urgence sanitaire oblige. Sa consécration dans le droit commun peine: sur la rampe de la CNIL, un avis très sec sur le futur texte d'application préparé par Olivier Véran. Next INpact révèle ces deux documents encore confidentiels.
