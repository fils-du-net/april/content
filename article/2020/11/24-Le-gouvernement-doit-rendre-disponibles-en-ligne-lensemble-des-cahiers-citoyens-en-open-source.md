---
site: Le Monde.fr
title: "«Le gouvernement doit rendre disponibles, en ligne, l'ensemble des “cahiers citoyens” en open source» (€)"
date: 2020-11-24
href: https://www.lemonde.fr/idees/article/2020/11/24/le-gouvernement-doit-rendre-disponibles-en-ligne-l-ensemble-des-cahiers-citoyens-en-open-source_6060947_3232.html
tags:
- Open Data
- Institutions
series:
- 202048
series_weight: 0
---

> TRIBUNE. Un demi-million de citoyens ont participé au grand débat national lancé par le chef de l'Etat après la crise des «gilets jaunes», remplissant quelque 630 000 pages de doléances. Cette cartographie inédite de la France depuis 1789 doit être numérisée afin de pouvoir être consultée, demande un collectif citoyen dans une tribune pour «Le Monde».
