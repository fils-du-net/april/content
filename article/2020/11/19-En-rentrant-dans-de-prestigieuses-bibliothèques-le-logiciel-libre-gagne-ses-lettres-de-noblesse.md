---
site: Le Monde.fr
title: "En rentrant dans de prestigieuses bibliothèques, le logiciel libre gagne ses lettres de noblesse"
author: Vincent Fagot
date: 2020-11-19
href: https://www.lemonde.fr/economie/article/2020/11/19/en-rentrant-dans-de-prestigieuses-bibliotheques-le-logiciel-libre-gagne-ses-lettres-de-noblesse_6060382_3234.html
featured_image: https://img.lemde.fr/2020/11/19/6/0/8256/5499/688/0/60/0/4b840b2_799185496-archiveprogram-github-bookshelf-jpg-glenn-wester.jpg
tags:
- Partage du savoir
- Entreprise
- Innovation
series:
- 202047
series_weight: 0
---

> L'entreprise américaine GitHub, propriété de Microsoft, sauvegarde des millions de lignes de langage informatique, qui vont être accueillies dans les bibliothèques d'Alexandrie, d'Oxford et de Stanford.
