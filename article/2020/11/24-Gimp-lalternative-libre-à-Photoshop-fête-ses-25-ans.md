---
site: Clubic.com
title: "Gimp, l'alternative libre à Photoshop, fête ses 25 ans"
author: Guillaume Belfiore
date: 2020-11-24
href: https://www.clubic.com/pro/legislation-loi-internet/propriete-intellectuelle/logiciel-libre-open-source/actualite-22345-gimp-l-alternative-libre-a-photoshop-fete-ses-25-ans.html
featured_image: https://pic.clubic.com/v1/images/1665919/raw.webp?fit=max&width=1200&hash=20b3116df138d3e15a5c4574c2437f6cd8acf6fc
tags:
- Sensibilisation
series:
- 202048
series_weight: 0
---

> Parmi les logiciels libres incontournables, GIMP fait figure de référence. L'application de manipulations graphiques fête ses 25 ans et continue sa promesse visant à offrir une alternative à Photoshop.
