---
site: Presse Citron
title: "Mobilizon: ce nouvel outil permet d'organiser des événements sans passer par Facebook"
description: Cette alternative libre est proposée par l'association Framasoft.
date: 2020-11-14
href: https://www.presse-citron.net/mobilizon-ce-nouvel-outil-permet-dorganiser-des-evenements-sans-passer-par-facebook
featured_image: https://www.presse-citron.net/wordpress_prod/wp-content/uploads/2020/11/Screenshot_2020-11-11-Mobilizon-org-Reprenons-le-contr%C3%B4le-de-nos-%C3%A9v%C3%A9nements.png
tags:
- Internet
- Associations
series:
- 202046
series_weight: 0
---

> «Un outil libre et fédéré pour libérer nos événements et nos groupes des griffes de Facebook». C’est ainsi que Framasoft décrit le nouveau service Mobilizon. Au terme d’un travail de deux ans, l’association a publié la première version du logiciel, assortie pour l’occasion de divers outils pour faciliter la prise en main.
