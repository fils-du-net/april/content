---
site: EurActiv
title: "Gaia-X: A trojan horse for Big Tech in Europe"
author: Stefane Fermigier, Sven Franck
date: 2020-11-23
href: https://www.euractiv.com/section/digital/opinion/gaia-x-a-trojan-horse-for-big-tech-in-europe
featured_image: https://en.euractiv.eu/wp-content/uploads/sites/2/2020/11/shutterstock_1348479257.jpg
tags:
- Informatique en nuage
- Europe
- English
series:
- 202048
series_weight: 0
---

> The EU's cloud infrastructure initiative, Gaia-X, poses a great risk of destroying the European ecosystem and hopes of sovereignty after the inclusion of some of the world's largest tech firms in the project
