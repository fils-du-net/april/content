---
site: Aquitaine Online
title: "Industrie du logiciel libre: un véritable atout pour la Nouvelle Aquitaine"
author:  Jean-Marc Blancherie
date: 2020-11-17
href: http://www.aquitaineonline.com/actualites-en-aquitaine/sud-ouest/8977-logiciel-libre-francois-pelligrini.html
featured_image: http://www.aquitaineonline.com/images/thumbnails/@2/images/stories/Economie_Industrie_2020/logiciel-libre-pixB-2341973_1920-fill-750x400.jpg
tags:
- Économie
- Sensibilisation
- Administration
series:
- 202047
series_weight: 0
---

> La Région Nouvelle Aquitaine ne s’est pas trompée en encourageant le développement de ce secteur très performant, et stratégique, de l’économie d’aujourd’hui. Les non-spécialistes ont du mal à comprendre de quoi il s’agit. Souvent, le logiciel libre est confondu avec le gratuit.
