---
site: ZDNet France
title: "Logiciel libre: disparition de Laurent Séguin, ancien président de l'AFUL, militant et musicien"
author: Thierry Noisette
date: 2020-11-04
href: https://www.zdnet.fr/blogs/l-esprit-libre/logiciel-libre-disparition-de-laurent-seguin-ancien-president-de-l-aful-militant-et-musicien-39912515.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2020/11/Laurent%20Seguin%20profil%20Twitter.jpg
tags:
- Associations
- april
series:
- 202045
series_weight: 0
---

> Eminent militant libriste, auteur de musiques électroniques, Laurent Séguin est mort à 44 ans. Personnalités et organisations des logiciels libres lui rendent hommage.
