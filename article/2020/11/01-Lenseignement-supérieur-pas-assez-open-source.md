---
site: lesoir.be
title: "L’enseignement supérieur pas assez «open source»? (€)"
author: Thomas Casavecchia
date: 2020-11-01
href: https://plus.lesoir.be/335240/article/2020-11-01/lenseignement-superieur-pas-assez-open-source
featured_image: https://plus.lesoir.be/sites/default/files/dpistyles_v2/ena_16_9_extra_big/2020/05/08/node_335240/27783694/public/2020/05/08/B9723410554Z.1_20200508091356_000+G23FVU9SS.1-0.jpg
tags:
- Éducation
series:
- 202044
series_weight: 0
---

> Un rapport de stage à rendre en «.doc» via Outlook, une réunion de TP sur Zoom; les étudiants sont nombreux à devoir composer quotidiennement avec les outils de Microsoft dans leur vie estudiantine. Trop?
