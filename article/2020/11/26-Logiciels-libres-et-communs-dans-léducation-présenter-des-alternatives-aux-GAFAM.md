---
site: ZDNet France
title: "Logiciels libres et communs dans l'éducation: présenter des alternatives aux GAFAM"
author: Thierry Noisette
date: 2020-11-26
href: https://www.zdnet.fr/blogs/l-esprit-libre/logiciels-libres-et-communs-dans-l-education-presenter-des-alternatives-aux-gafam-39913813.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2020/11/Ecole_173_rue_du_chateau-des-Rentiers_3_WMC.jpg
tags:
- Éducation
- Partage du savoir
series:
- 202048
series_weight: 0
---

> «Quelles alternatives aux GAFAM?» Après deux rencontres sur ce thème, les Etats généraux du numérique libre et des communs pédagogiques lancent un cycle, inauguré le 28 novembre.
