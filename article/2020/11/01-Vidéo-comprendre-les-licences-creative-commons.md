---
site: Archimag
title: "Vidéo: comprendre les licences creative commons"
author: Didier Frochot
href: https://www.archimag.com/content/vid%C3%A9o-comprendre-les-licences-creative-commons
tags:
- Licenses
series:
- 202044
---

> Première vidéo de notre série consacrée au droit de l'information: les licences creative commons. Découvrez en 5 minutes ce qu'il faut savoir sur ces licences nées du mouvement de l'open access, dédiées au partage de créations intellectuelles, et qui permettent à un auteur de céder à l'avance une partie de ses droits d'exploitation sur ses oeuvres.
