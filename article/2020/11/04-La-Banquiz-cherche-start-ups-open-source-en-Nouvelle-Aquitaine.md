---
site: Le Monde Informatique
title: "La Banquiz cherche start-ups open source en Nouvelle-Aquitaine"
author: Maryse Gros
date: 2020-11-04
href: https://www.lemondeinformatique.fr/actualites/lire-la-banquiz-cherche-start-ups-open-source-en-nouvelle-aquitaine-80927.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000075165.jpg
tags:
- Entreprise
series:
- 202045
series_weight: 0
---

> Avec à son actif, sur 6 ans, 31 entreprises enregistrées et 200 emplois créés, l'accélérateur La Banquiz poursuit son accompagnement des start-ups open source en région Nouvelle-Aquitaine avec un programme désormais régionalisé. Un nouvel appel à candidatures est ouvert.
