---
site: ZDNet France
title: "Guido van Rossum, le fondateur du langage Python, rejoint Microsoft"
author: Steven J. Vaughan-Nichols
date: 2020-11-13
href: https://www.zdnet.fr/actualites/guido-van-rossum-le-fondateur-du-langage-python-rejoint-microsoft-39912909.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/01/langage__w1200.jpg
tags:
- Entreprise
series:
- 202046
series_weight: 0
---

> Oui, vous avez bien lu. Le célèbre développeur open source est sorti de sa retraite pour rejoindre la division des développeurs de Microsoft et poursuivre son travail sur le langage Python.
