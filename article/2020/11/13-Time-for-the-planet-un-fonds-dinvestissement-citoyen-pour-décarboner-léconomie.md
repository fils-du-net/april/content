---
site: novethic
title: "Time for the planet: un fonds d'investissement citoyen pour décarboner l'économie"
author: Béatrice Héraud
date: 2020-11-13
href: https://www.novethic.fr/actualite/environnement/climat/isr-rse/les-raisons-d-y-croire-et-si-on-luttait-contre-le-changement-climatique-en-investissant-dans-les-entreprises-qui-ont-les-solutions-149190.html
featured_image: https://www.novethic.fr/fileadmin/_processed_/csm_finance-climat-planete-iStock-calypsoart_5b82598664.jpg
tags:
- Économie
- Entreprise
series:
- 202046
series_weight: 0
---

> Lutter contre la crise climatique depuis l’intérieur du système économique et financier, c’est l’ambition de "Time for the Planet". Ce nouveau fonds d’investissement à but non lucratif souhaite lever un milliard d’euros pour créer 100 entreprises innovantes en matière de décarbonation de l’économie. Un projet soutenu par le climatologue Jean Jouzel et qui a déjà séduit 5000 citoyens qui ont placé 1 million d’euros dans l’aventure.
