---
site: usine-digitale.fr
title: "Qui est derrière Unsplash, la banque d'images gratuites qui inonde le Web?"
date: 2020-11-16
href: https://www.usine-digitale.fr/article/qui-est-derriere-unsplash-la-banque-d-images-gratuites-qui-inonde-le-web.N1028554
featured_image: https://www.usine-digitale.fr/mediatheque/9/8/8/000915889_homePageUne/cofondateurs-d-unsplash.jpg
tags:
- Licenses
- Entreprise
- Internet
series:
- 202047
series_weight: 0
---

> Photo Reportage Médias, blogs, rapports, présentations… Souvent sans le savoir, les photos de la plus grande banque d’images gratuites se retrouvent sous nos yeux. Zoom sur Unsplash, une startup montréalaise méconnue et à l’histoire atypique.
