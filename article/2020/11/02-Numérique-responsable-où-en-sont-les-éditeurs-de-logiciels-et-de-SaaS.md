---
site: Siècle Digital
title: "Numérique responsable: où en sont les éditeurs de logiciels et de SaaS?"
author: Brice Schwartz
date: 2020-11-02
href: https://siecledigital.fr/2020/11/02/numerique-responsable-ou-en-sont-les-editeurs-de-logiciels-et-de-saas
featured_image: https://thumbor.sd-cdn.fr/tysOW8BdT6XQbemfOEaVFIgJDIE=/fit-in/939x891/cdn.sd-cdn.fr/wp-content/uploads/2020/10/Capture-de%CC%81cran-2020-10-30-a%CC%80-18.52.52.png
tags:
- Sensibilisation
- Économie
series:
- 202045
series_weight: 0
---

> Autrefois, le numérique (et ce qui l’accompagne: internet, les logiciels, les données, etc.) était censé être plus écologique, plus durable que le papier et les méthodes plus anciennes de communication. Désormais, nous savons que le digital a une empreinte carbone très forte et que les logiciels ne sont pas exempts de défauts, notamment en matière de durabilité.
