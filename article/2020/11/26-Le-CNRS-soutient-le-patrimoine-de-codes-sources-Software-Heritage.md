---
site: Le Monde Informatique
title: "Le CNRS soutient le patrimoine de codes sources Software Heritage"
author: Maryse Gros
date: 2020-11-26
href: https://www.lemondeinformatique.fr/actualites/lire-le-cnrs-soutient-le-patrimoine-de-codes-sources-software-heritage-81160.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000075616.jpg
tags:
- Partage du savoir
series:
- 202048
series_weight: 0
---

> Software Heritage constitue des archives universelles de code source de logiciels pour permettre leur réutilisation et préserver à long terme la capacité d'accéder aux informations numériques. Le CNRS devient sponsor de platine du projet en lui apportant 100 000 euros par an.
