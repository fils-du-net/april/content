---
site: Le Monde.fr
title: "Cryptomonnaies: «La Chine menace le cœur de l'économie mondiale: l'Internet de l'argent» (€)"
author: Vincent Lorphelin, Christian Saint-Etienne
date: 2020-11-02
href: https://www.lemonde.fr/idees/article/2020/11/02/cryptomonnaies-la-chine-menace-le-c-ur-de-l-economie-mondiale-l-internet-de-l-argent_6058149_3232.html
tags:
- Économie
- International
- Brevets logiciels
series:
- 202045
series_weight: 0
---

> L'expert du numérique Vincent Lorphelin et l'économiste Christian Saint-Etienne expliquent, dans une tribune au «Monde», que la Chine convoque le combat pour la suprématie technologique sur le terrain des brevets.
