---
site: ChannelNews
title: "Microsoft va continuer à héberger le Health Data Hub pendant près de deux ans"
author: Dirk Basyn
date: 2020-11-24
href: https://www.channelnews.fr/100122-100122
featured_image: https://www.channelnews.fr/wp-content/uploads/2020/11/Health-Data-Hub.jpg
tags:
- Vie privée
- Entreprise
- Institutions
series:
- 202048
series_weight: 0
---

> Le Health Data Hub restera chez Microsoft un certain temps encore. Craignant de possibles transferts de données de santé personnelles vers les États-Unis,
