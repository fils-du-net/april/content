---
site: ZDNet France
title: "Youtube-dl: l'EFF pousse Github à remettre le projet en ligne"
author: Catalin Cimpanu
date: 2020-11-17
href: https://www.zdnet.fr/actualites/youtube-dl-l-eff-pousse-github-a-remettre-le-projet-en-ligne-39913143.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2018/07/copyright_600__w1200.jpg
tags:
- Droit d'auteur
series:
- 202047
series_weight: 0
---

> GitHub met en place un 'fonds de défense des développeurs' d'un million de dollars pour aider les développeurs de logiciels libres à lutter contre les réclamations abusives basées sur la section 1201 du DMCA.
