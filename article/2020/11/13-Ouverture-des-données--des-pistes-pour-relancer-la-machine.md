---
site: Acteurs Publics 
title: "Ouverture des données: des pistes pour relancer la machine (€)"
author: Emile Marzolf
date: 2020-11-13
href: https://www.acteurspublics.fr/articles/ouverture-des-donnees-des-pistes-pour-relancer-la-machine
tags:
- Open Data
series:
- 202046
series_weight: 0
---

> Quelques mois après avoir démarré ses travaux, et avant de rendre son rapport final en décembre, la mission Bothorel sur l’ouverture des données et codes sources publics – mais aussi des données privées dites d’intérêt général – a invité le public à réagir aux 10 freins à cette ouverture identifiés et à proposer des solutions. Les propositions en faveur des logiciels libress, comme la création d’une agence spécifiquement chargée de la politique publique relative au logiciel libre, ressortent largement en tête.
