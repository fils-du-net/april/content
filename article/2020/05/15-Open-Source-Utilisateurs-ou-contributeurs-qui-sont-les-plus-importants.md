---
site: Le Monde Informatique
title: "Open Source: Utilisateurs ou contributeurs qui sont les plus importants?"
author: Matt Asay
date: 2020-05-15
href: https://www.lemondeinformatique.fr/actualites/lire-open-source-utilisateurs-ou-contributeurs-qui-sont-les-plus-importants-79085.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000072275.jpg
tags:
- Sensibilisation
- Innovation
series:
- 202020
series_weight: 0
---

> Open Source: On pourrait penser que les projets open source cherchent surtout à attirer des contributeurs au code. Mais la réalité est plus compliquée.
