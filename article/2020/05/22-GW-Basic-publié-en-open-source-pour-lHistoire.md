---
site: InformatiqueNews.fr
title: "GW-Basic, publié en open source pour l'Histoire…"
author: Loïc Duval
date: 2020-05-22
href: https://www.informatiquenews.fr/gw-basic-publie-en-open-source-pour-lhistoire-70374
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2020/05/shutterstock_1174882978.jpg
tags:
- Entreprise
series:
- 202021
---

> Dérivant du Basica de l’ancestral IBM PC XT, GW-Basic est l’interpréteur Basic livré en standard avec MS-DOS et sur lequel bien des étudiants des années 80 ont fait leurs premiers pas en programmation. Microsoft vient d’en ouvrir l’accès au code source.
