---
site: Numerama
title: "Quels sont les logiciels libres que l'État conseille en 2020?"
author: Julien Lausson
date: 2020-05-06
href: https://www.numerama.com/tech/622711-quels-sont-les-logiciels-libres-que-letat-conseille-en-2020.html
featured_image: https://c1.lestechnophiles.com/www.numerama.com/content/uploads/2020/05/open-source-linux.jpg
tags:
- Référentiel
- Institutions
series:
- 202019
---

> La nouvelle liste des logiciels libres que l’État recommande pour ses administrations est publiée. Une édition qui marque l'arrivée de plusieurs nouveautés.
