---
site: ITforBusiness
title: "Etalab publie sa liste 2020 des logiciels libres recommandés aux organismes publics"
author: Laurent Delattre
date: 2020-05-14
href: https://www.itforbusiness.fr/etalab-publie-sa-liste-2020-des-logiciels-libres-recommandes-aux-organismes-publics-38400
featured_image: https://www.itforbusiness.fr/wp-content/uploads/2020/05/shutterstock_539542870.jpg
tags:
- Administration
- Référentiel
series:
- 202020
series_weight: 0
---

> Le très officiel Etalab vient de mette à jour son socle interministériel des logiciels libres et inaugure un nouveau portail interactif pour rechercher et accéder aux solutions open source recommandées.
