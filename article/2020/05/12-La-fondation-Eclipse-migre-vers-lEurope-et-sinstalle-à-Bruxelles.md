---
site: ZDNet France
title: "La fondation Eclipse migre vers l'Europe et s'installe à Bruxelles"
author: Thierry Noisette
date: 2020-05-12
href: https://www.zdnet.fr/blogs/l-esprit-libre/la-fondation-eclipse-migre-vers-l-europe-et-s-installe-a-bruxelles-39903537.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2020/05/Eclipse_Foundation_Logo.jpg
tags:
- Associations
- International
series:
- 202020
series_weight: 0
---

> Américaine depuis sa création en 2004, la fondation Eclipse déménage et affiche son ambition de devenir «la fondation open source européenne de référence».
