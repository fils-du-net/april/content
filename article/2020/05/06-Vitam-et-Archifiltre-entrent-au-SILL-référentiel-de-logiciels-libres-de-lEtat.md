---
site: Le Monde Informatique
title: "Vitam et Archifiltre entrent au SILL, référentiel de logiciels libres de l'Etat"
author: Maryse Gros
date: 2020-05-06
href: https://www.lemondeinformatique.fr/actualites/lire-vitam-et-archifiltre-entrent-au-sill-referentiel-de-logiciels-libres-de-l-etat-79010.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000072142.jpg
tags:
- Référentiel
- Institutions
series:
- 202019
---

> Auparavant mis à jour une fois par an, le socle interministériel de logiciels libres - SILL - est maintenant régulièrement enrichi. Sa version 2020 compte 34 logiciels de plus, soit 190 au total. Parmi les nouveautés, la base de données NoSQL Redis et l'IDE RStudio, ainsi que des logiciels conçus par secteur public comme les solutions d'archivage Vitam et Archifiltre.
