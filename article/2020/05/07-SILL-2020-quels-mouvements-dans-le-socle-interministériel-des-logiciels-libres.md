---
site: Silicon
title: "SILL 2020: quels mouvements dans le socle interministériel des logiciels libres?"
author: Clément Bohic
date: 2020-05-07
href: https://www.silicon.fr/sill-2020-quels-mouvements-dans-le-socle-interministeriel-de-logiciels-libres-339273.html
featured_image: https://www.silicon.fr/wp-content/uploads/2020/05/SILL-2020.jpg
tags:
- Référentiel
- Institutions
- Administration
series:
- 202019
series_weight: 0
---

> L'État a mis à jour son socle de logiciels libres recommandés aux administrations. Le point sur les entrants et les sortants.
