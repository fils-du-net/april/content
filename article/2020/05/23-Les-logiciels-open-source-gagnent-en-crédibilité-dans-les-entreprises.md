---
site: L'usine Nouvelle
title: "Les logiciels open source gagnent en crédibilité dans les entreprises"
author: Nathan Mann
date: 2020-05-23
href: https://www.usinenouvelle.com/editorial/les-logiciels-open-source-gagnent-en-credibilite-dans-les-entreprises.N947301
featured_image: https://www.usinenouvelle.com/mediatheque/1/5/5/000857551_image_896x598/image.png
tags:
- Entreprise
- Innovation
series:
- 202021
series_weight: 0
---

> L'open source se consolide dans les entreprises. Installés dans les couches systèmes, les logiciels cherchent à convaincre les utilisateurs.
