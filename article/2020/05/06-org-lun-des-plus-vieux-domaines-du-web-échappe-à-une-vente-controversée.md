---
site: Numerama
title: "«.org»: l'un des plus vieux domaines du web échappe à une vente controversée"
author: Julien Lausson
date: 2020-05-06
href: https://www.numerama.com/business/622648-org-lun-des-plus-vieux-domaines-du-web-echappe-a-une-vente-controversee.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/content/uploads/2019/11/org-impact-award.jpg
seeAlso: "[Suite à la mobilisation internationale l'ICANN rejette la vente de PIR qui gère le domaine .org](https://april.org/icann-stoppe-vente-point-org)"
tags:
- Internet
- april
series:
- 202019
series_weight: 0
---

> Annoncée en novembre, la vente du gestionnaire en charge du «.org» a une société à but lucratif a finalement échoué. Le régulateur des noms de domaine a fini par poser son véto, sous la pression publique.
