---
site: Le Monde Informatique
title: "Covid-19: les développeurs sur GitHub très actifs mais épuisés"
author: Paul Krill
date: 2020-05-11
href: https://www.lemondeinformatique.fr/actualites/lire-covid-19-les-developpeurs-sur-github-tres-actifs-mais-epuises-79038.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000072194.jpg
tags:
- Innovation
series:
- 202020
---

> Selon l'analyse des usages effectuée par GitHub, l'activité des développeurs a été «résiliente» pendant la pandémie. Cependant, la plateforme met en garde contre un risque potentiel de burn-out.
