---
site: The Conversation
title: "Travail à distance: cinq bonnes pratiques à emprunter au développement «open source»"
author: Kiane Goudarzi, Véronique Sanguinetti, Vincent Chauvet
date: 2020-05-27
href: https://theconversation.com/travail-a-distance-cinq-bonnes-pratiques-a-emprunter-au-developpement-open-source-139348
featured_image: https://images.theconversation.com/files/337373/original/file-20200525-106853-1b4nzzk.png
tags:
- Licenses
- Innovation
series:
- 202022
series_weight: 0
---

> La crise sanitaire a démontré la nécessité de mettre en place des processus de développement ouverts et distribués déjà à l'œuvre depuis 25 ans dans le monde du logiciel.
