---
site: Acteurs Publics
title: "Éducation: une boîte à outils numériques partagés à l'avenir incertain"
author: Émile Marzolf
date: 2020-05-20
href: https://www.acteurspublics.fr/evenement/education-une-boite-a-outils-numeriques-partages-a-lavenir-incertain
tags:
- Éducation
series:
- 202021
series_weight: 0
---

> Face à la crise sanitaire qui a plongé le monde de l’éducation, comme bien d’autres, dans un fonctionnement entièrement à distance, le ministère de l’Éducation nationale a lancé dans l’urgence une plate-forme pour réunir différents outils numériques répondant aux besoins de base. Mais ces services sont hébergés gracieusement par des entreprises privées, ce qui pose la question de leur pérennisation. 
