---
site: Journal du Net
title: "L'innersource: quand l'open source inspire l'excellence en matière de transformation numérique"
author: Alain Helaïli
date: 2020-05-11
href: https://www.journaldunet.com/solutions/dsi/1491205-l-innersource-quand-l-open-source-inspire-les-entreprises-en-quete-d-excellence-en-matiere-de-transformation-numerique
tags:
- Innovation
series:
- 202020
series_weight: 0
---

> Les entreprises en quête de transformation numérique doivent concilier agilité et productivité dans un environnement complexe, où les talents numériques sont rares. Convaincues par l'open source, elles sont de plus en plus nombreuses à se convertir à l'innersource.
