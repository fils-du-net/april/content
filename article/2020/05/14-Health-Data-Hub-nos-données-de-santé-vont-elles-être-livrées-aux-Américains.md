---
site: 01net.
title: "Health Data Hub: nos données de santé vont-elles être livrées aux Américains?"
author: Marion Simon-Rainaud
date: 2020-05-14
href: https://www.01net.com/actualites/health-data-hub-nos-donnees-de-sante-vont-elles-etre-livrees-aux-americains-1913351.html
featured_image: https://img.bfmtv.com/c/630/420/117/8a69ec5d6c10b8d1ee2313f952de0.jpeg
seeAlso: "[InterHop - Les hôpitaux français pour l'interopérabilité et le partage libre des algorithmes](https://interhop.org)  \n
[[Mediapart] La Cnil s’inquiète d’un possible transfert de nos données de santé aux Etats-Unis (¤)](https://www.mediapart.fr/journal/france/080520/la-cnil-s-inquiete-d-un-possible-transfert-de-nos-donnees-de-sante-aux-etats-unis)"
tags:
- Vie privée
- Institutions
series:
- 202020
series_weight: 0
---

> Comme le rapporte Mediapart, le gouvernement passe en force son projet de plate-forme sanitaire géante au nom de l'état d'urgence. Problème: les données seraient hébergées par Microsoft qui pourrait sur seul ordre des États-Unis... les transférer outre-Atlantique.
