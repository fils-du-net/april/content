---
site: Silicon
title: "Open Source: pourquoi la Fondation Eclipse s'installe en Europe?"
author: Ariane Beky
date: 2020-05-13
href: https://www.silicon.fr/open-source-fondation-eclipse-europe-339623.html
featured_image: https://www.silicon.fr/wp-content/uploads/2012/10/Quiz-inventeurs-europeens-%C2%A9-silver-tiger-shutterstock.png
tags:
- Associations
- International
series:
- 202020
---

> La Fondation Eclipse migre en Europe, où la majorité de ses membres sont installés, pour mieux soutenir ses ambitions mondiales dans l'open source.
