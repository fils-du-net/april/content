---
site: Sciencepost 
title: "La médecine open source peut-elle nous aider à nous préparer aux prochaines pandémies?"
author: Yohan Demeure
date: 2020-05-17
href: https://sciencepost.fr/la-medecine-open-source-peut-elle-nous-aider-a-nous-preparer-aux-prochaines-pandemies
featured_image: https://sciencepost.fr/wp-content/uploads/2020/05/iStock-1051617250.jpg
tags:
- Sciences
series:
- 202020
series_weight: 0
---

> Mettre en place un système efficace de partage open source pour la création de médicaments est une idée séduisante. Mais est-elle utopique?
