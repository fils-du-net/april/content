---
site: The Conversation
title: "Données de santé: l'arbre StopCovid qui cache la forêt Health Data Hub"
author: Bernard Fallery
date: 2020-05-25
href: https://theconversation.com/donnees-de-sante-larbre-stopcovid-qui-cache-la-foret-health-data-hub-138852
featured_image: https://images.theconversation.com/files/336197/original/file-20200519-152338-8watz2.jpg
tags:
- Vie privée
- Économie
series:
- 202022
series_weight: 0
---

> Coup de projecteur sur Health Data Hub, un projet national français fondé sur le recours à l'intelligence artificielle… et à certains géants mondiaux du numérique, ce qui pose divers problèmes.
