---
site: LeMagIT
title: "Les 4outils Open source essentiels pour automatiser l'IT"
author: Yann Serra,
date: 2020-05-19
href: https://www.lemagit.fr/conseil/Les-4-outils-Open-source-essentiels-pour-automatiser-lIT
featured_image: https://cdn.ttgtmedia.com/visuals/ComputerWeekly/Hero%20Images/datacentre-management-5-fotolia.jpg
tags:
- Entreprise
series:
- 202021
---

> Jenkins, Cockpit, le pipeline GitOps de Kubernetes et Ansible sont les quatre environnements que tout administrateur système devrait maîtriser. Cet article explique leur principe et liste leurs accessoires.
