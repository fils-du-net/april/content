---
site: ZDNet France
title: "Microsoft fait son mea culpa sur l'open source"
author: Liam Tung
date: 2020-05-18
href: https://www.zdnet.fr/actualites/microsoft-fait-son-mea-culpa-sur-l-open-source-39903881.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/02/Microsoft%20Illustration%20A%20620.jpg
tags:
- Entreprise
series:
- 202021
---

> Le président de Microsoft, Brad Smith, a expliqué que les positions du géant américain sur l'open source l'avait placé du «mauvais côté de l'Histoire».
