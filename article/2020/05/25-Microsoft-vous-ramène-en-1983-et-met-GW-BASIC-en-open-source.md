---
site: ZDNet France
title: "Microsoft vous ramène en 1983, et met GW-BASIC en open-source"
author: Liam Tung
date: 2020-05-25
href: https://www.zdnet.fr/actualites/microsoft-vous-ramene-en-1983-et-met-gw-basic-en-open-source-39904139.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2020/02/gw_basic.jpg
tags:
- Entreprise
- Logiciels privateurs
series:
- 202022
---

> Microsoft met en open source un élément clé de l'histoire des logiciels, qui a aidé la société à prendre la main sur la marché des ordinateurs personnels.
