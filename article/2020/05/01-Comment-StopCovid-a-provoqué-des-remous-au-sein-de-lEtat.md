---
site: iGeneration
title: "Comment StopCovid a provoqué des remous au sein de l'Etat"
author: Christophe Laporte
date: 2020-05-03
href: https://www.igen.fr/ailleurs/2020/05/comment-stopcovid-provoque-des-remous-au-sein-de-letat-114709
featured_image: https://cdn.mgig.fr/2020/05/mg-45524124-443a-4175-b772-w1000h839-sc.jpg
tags:
- Innovation
- Vie privée
series:
- 202018
---

> Le projet StopCovid a fait couler beaucoup d’encre depuis sa présentation. Cet article publié par Acteurs Publics révèle que cette application a également suscité beaucoup de débats et de tensions au sein de l’Etat.
