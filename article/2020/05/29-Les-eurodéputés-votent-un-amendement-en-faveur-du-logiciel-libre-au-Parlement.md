---
site: ZDNet France
title: "Les eurodéputés votent un amendement en faveur du logiciel libre au Parlement"
author: Thierry Noisette
date: 2020-05-29
href: https://www.zdnet.fr/blogs/l-esprit-libre/les-eurodeputes-votent-un-amendement-en-faveur-du-logiciel-libre-au-parlement-39904477.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2020/05/Bruxelles_parlement%20UE_WMC.JPG
seeAlso: "[Le Parlement européen appelle l'Union européenne à migrer vers le logiciel libre](https://www.april.org/le-parlement-europeen-appelle-lunion-europeenne-migrer-vers-le-logiciel-libre)"
tags:
- Europe
- Institutions
- Open Data
series:
- 202022
series_weight: 0
---

> Le Parlement européen recommande que tout logiciel développé pour lui soit mis sous licence libre.

## Commentaire de l'april

La formulation n'est en effet pas contraignante et est en recul notamment avec l'article 47 de la résolution sur la surveillance électronique de masse adoptée par le Parlement européen en 2015 et qui appelait clairement l'Union européenne à migrer vers le logiciel libre, et à ajouter le logiciel libre comme critère de sélection obligatoire dans les passations de marchés publics dans le domaine de l'informatique.
