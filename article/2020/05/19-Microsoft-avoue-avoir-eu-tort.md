---
site: Le Journal du Geek
title: "Microsoft avoue avoir eu tort à propos de l’open-source"
author: Felix Gouty
date: 2020-05-19
href: https://www.journaldugeek.com/2020/05/19/microsoft-open-source
featured_image: https://www.journaldugeek.com/content/uploads/2020/02/code-cryptage-piratage-640x426.jpg
tags:
- Entreprise
series:
- 202021
---

> 'Microsoft était du mauvais côté de l'Histoire en matière d'open-source' a avoué le président actuel de la firme de Redmond, après des décennies de lutte.
