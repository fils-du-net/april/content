---
site: Le Monde Informatique
title: "Le développeur open source indépendant survivra-t-il au Covid-19?"
author: Matt Asay
date: 2020-05-20
href: https://www.lemondeinformatique.fr/actualites/lire-le-developpeur-open-source-independant-survivra-t-il-au-covid-19-79142.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000072354.png
tags:
- Sensibilisation
series:
- 202021
series_weight: 0
---

> Beaucoup de projets de développement open source importants sont maintenus par des développeurs bénévoles. Mais, aujourd'hui, ces codeurs ont des besoins plus urgents.
