---
site: Politis.fr
title: "La licence libre, remède miracle contre les pénuries?"
author: Erwan Manac'h
date: 2020-05-06
href: https://www.politis.fr/articles/2020/05/la-licence-libre-remede-miracle-contre-les-penuries-41780
featured_image: https://static.politis.fr/medias/articles/2020/05/la-licence-libre-remede-miracle-contre-les-penuries-41780/thumbnail_large-41780.jpg
tags:
- Licenses
- Sciences
series:
- 202019
series_weight: 0
---

> Aujourd'hui dans la série #DéconfinonsLesIdées, on s'interroge sur l'histoire singulière du gel hydroalcoolique qui se diffuse grâce aux principes de l'open source. Et si la licence libre était le remède de demain?
