---
site: Next INpact
title: "Le Conseil constitutionnel censure un bout de la loi Hadopi, mais rien ne changera (€)"
author: Marc Rees
date: 2020-05-20
href: https://www.nextinpact.com/news/108994-le-conseil-constitutionnel-censure-bout-loi-hadopi-mais-rien-ne-changera.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/14270.jpg
tags:
- HADOPI
- Institutions
series:
- 202021
series_weight: 0
---

> Le Conseil constitutionnel vient de déclarer contraire à la Constitution une partie du droit de communication de la Hadopi. Celle qui lui permet d’identifier les abonnés derrière les adresses IP. Seulement, l’annulation est à effet différé. Entretemps, le gouvernement pourra donc corriger le tir. Une victoire à la Pyrrhus pour les requérants.
