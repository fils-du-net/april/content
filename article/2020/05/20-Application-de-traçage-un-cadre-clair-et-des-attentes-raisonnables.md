---
site: Le Temps
title: "Application de traçage: un cadre clair et des attentes raisonnables"
author: Damien Cottier
date: 2020-05-20
href: https://www.letemps.ch/opinions/application-tracage-un-cadre-clair-attentes-raisonnables
featured_image: https://assets.letemps.ch/sites/default/files/styles/article_detail_desktop/public/media/2020/05/08/file7aenre423dehhg973xp.jpg
tags:
- Vie privée
- Innovation
series:
- 202021
---

> L’app est à voir comme une chance, à introduire dans un cadre légal clair, en protégeant les droits et les libertés des citoyens et dont il ne faut ni sous-estimer ni exagérer l’utilité, écrit Damien Cottier, conseiller national
