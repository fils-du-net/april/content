---
site: korii.
title: "La médecine en open source pourrait nous préparer pour la prochaine pandémie"
date: 2020-05-09
href: https://korii.slate.fr/biz/medecine-open-source-recherche-medicaments-partage-outils-donnees
featured_image: https://korii.slate.fr/sites/default/files/styles/1440x600/public/sharon-mccutcheon-vglttwpesg-unsplash.jpg
tags:
- Sciences
- Partage du savoir
series:
- 202019
series_weight: 0
---

> Devrait-il exister un système de partage open source pour la création de médicaments? Sur un plan éthique, difficile de défendre la thèse du «non». Sur celui de l'économie en revanche, la situation parle d'elle-même: les laboratoires pharmaceutiques ont la mainmise sur les recherches menées par leurs équipes, sur la production de médicaments et sur leur distribution au grand public. Et ce souvent bien au-delà de l'expiration des brevets puisque les résultats des recherches sont jalousement conservés entre les quatre murs de l'entreprise.
