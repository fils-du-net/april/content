---
site: Usbek & Rica
title: "Oeuvrer à l'émergence d'un «autre numérique» est-il une impasse?"
author: Hubert Guillaud
date: 2020-05-31
href: https://usbeketrica.com/article/oeuvrer-emergence-autre-numerique-est-il-impasse
featured_image: https://static.usbeketrica.com/images/thumb_840xh/5ece284435f89.jpg
tags:
- Sensibilisation
- Open Data
series:
- 202022
series_weight: 0
---

> Le livre «Contre l'alternumérisme», de Julia Laïnae et Nicolas Alep, interroge les arguments de ceux qui oeuvrent à faire émerger «un autre numérique».
