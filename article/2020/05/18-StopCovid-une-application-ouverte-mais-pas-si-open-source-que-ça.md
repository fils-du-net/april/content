---
site: LeMagIT
title: "StopCovid: une application ouverte, mais pas si open source que ça"
author: Gaétan Raoul
date: 2020-05-18
href: https://www.lemagit.fr/actualites/252483302/StopCovid-un-projet-aux-contours-incertains
featured_image: https://cdn.ttgtmedia.com/visuals/ComputerWeekly/Hero%20Images/mobile-smartphone-message-3G-4G-fotolia.jpeg
tags:
- Vie privée
- Innovation
series:
- 202021
series_weight: 0
---

> L’application de traçage de contacts StopCovid bénéficie du soutien de nombreux acteurs IT, mais les doutes subsistent quant à son déploiement le 2 juin prochain.
