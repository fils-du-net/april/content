---
site: Les Numeriques
title: "StopCovid, une application pas si open source qu'il n'y paraît"
author: Corentin Bechade
date: 2020-05-16
href: https://www.lesnumeriques.com/vie-du-net/stopcovid-une-application-pas-si-open-source-qu-il-n-y-parait-n150327.html
featured_image: https://dyw7ncnq1en5l.cloudfront.net/optim/news/15/150327/1969a2f6-stopcovid-une-application-pas-si-open-source-que-ca__w800.webp
tags:
- Innovation
series:
- 202020
---

> Les premiers morceaux de l'application StopCovid se dévoilent… et sont déjà sous le feu des critiques. La raison? Les promesses de transparence et d'open source faites par Cédric O semblent déjà avoir été oubliées.
