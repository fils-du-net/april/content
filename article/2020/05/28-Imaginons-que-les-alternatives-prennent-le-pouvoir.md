---
site: Reporterre
title: "Imaginons que les alternatives prennent le pouvoir"
author: Lorène Lavocat
date: 2020-05-28
href: https://reporterre.net/Imaginons-que-les-alternatives-prennent-le-pouvoir
featured_image: https://reporterre.net/local/cache-vignettes/L709xH500/arton20432-72e1c.png
tags:
- Économie
series:
- 202022
series_weight: 0
---

> La pandémie du Covid-19 a multiplié les initiatives pour le «monde d’après». Parmi elles, «Et si…?», du mouvement Alternatiba, à laquelle ont contribué notamment Alain Damasio, Fatima Ouassak, Cédric Herrou… Une exploration jubilatoire et vivifiante de l’avenir.
