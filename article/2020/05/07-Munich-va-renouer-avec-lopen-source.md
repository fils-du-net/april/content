---
site: Le Monde Informatique
title: "Munich va renouer avec l'open source"
author: Jacques Cheminat
date: 2020-05-07
href: https://www.lemondeinformatique.fr/actualites/lire-munich-va-renouer-avec-l-open-source-79023.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000072175.jpg
tags:
- Administration
series:
- 202019
series_weight: 0
---

> En changeant de coalition politique, la ville de Munich va encore réorienter son IT et renouer avec l'open source. Si le slogan «argent public, code public» doit devenir la règle, des incertitudes planent sur la partie bureautique et OS.
