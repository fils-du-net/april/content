---
site: Mediapart
title: "La Cnil s'inquiète d'un possible transfert de nos données de santé aux Etats-Unis (€)"
date: 2020-05-08
href: https://www.mediapart.fr/journal/france/080520/la-cnil-s-inquiete-d-un-possible-transfert-de-nos-donnees-de-sante-aux-etats-unis
tags:
- Vie privée
- Institutions
series:
- 202019
---

> Au nom de l'état d'urgence, le gouvernement a accéléré la mise en place du Health Data Hub, une plateforme devant centraliser la quasi-totalité de nos données de santé. Dans un avis, la Cnil relève, notamment, que le contrat «mentionne l'existence de transferts de données en dehors de l'Union européenne». La directrice du projet dément.
