---
site: francetv info
title: "VIDEO. Le monde face au virus: le traçage numérique en Israël, en Espagne, au Royaume-Uni et en Allemagne"
date: 2020-05-02
href: https://www.francetvinfo.fr/sante/maladie/coronavirus/video-le-monde-face-au-virus-le-tracage-numerique-en-israel-en-espagne-aux-royaume-uni-et-en-allemagne_3943737.html
featured_image: https://www.francetvinfo.fr/image/75rzcqyen-a3be/1500/843/21440303.jpg
tags:
- Innovation
- International
series:
- 202018
---

> Chaque semaine, quatre de nos correspondants nous racontent comment la crise du coronavirus est gérée dans le pays où ils se trouvent. Direction l'Israël, l'Espagne, le Royaume-Uni et l'Allemagne pour aborder la question du traçage numérique.
