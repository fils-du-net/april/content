---
site: ZDNet
title: "Linux not Windows: Why Munich is shifting back from Microsoft to open source – again"
author: Cathrin Schaer
date: 2020-05-14
href: https://www.zdnet.com/article/linux-not-windows-why-munich-is-shifting-back-from-microsoft-to-open-source-again
featured_image: https://zdnet4.cbsistatic.com/hub/i/r/2019/07/12/3ba7b71a-f24c-4d1e-99f8-43f90f596247/thumbnail/600x400/5e45c21f20ad5e907e5e3ada4dee4c6d/istock-922023956.jpg
tags:
- Administration
- Logiciels privateurs
- English
series:
- 202020
series_weight: 0
---

> Munich's flip-flop back to open source is the latest sign of Germany's political sea change over proprietary software.
