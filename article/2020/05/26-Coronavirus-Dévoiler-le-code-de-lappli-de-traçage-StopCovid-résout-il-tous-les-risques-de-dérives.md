---
site: 20minutes.fr
title: "Coronavirus: Dévoiler le code de l'appli de traçage StopCovid résout-il tous les risques de dérives?"
author: Laure Beaudonnet
date: 2020-05-26
href: https://www.20minutes.fr/arts-stars/web/2786491-20200526-coronavirus-devoiler-code-appli-tracage-stopcovid-resout-tous-risques-derives
featured_image: https://img.20mn.fr/IxQT5SroQh6a9i4fs-06xw/310x190_application-stopcovid-pourrait-etre-disponible-week-end-illustration.jpg
tags:
- Innovation
- Institutions
- Vie privée
series:
- 202022
series_weight: 0
---

> L'application de traçage StopCovid, destinée à repérer la propagation du coronavirus, pourrait être disponible dès le week-end prochain
