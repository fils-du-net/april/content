---
site: Daily Geek Show
title: "Le saviez-vous? Il existe un sigle en opposition au copyright... le copyleft"
author: Manon Fraschini
date: 2020-05-17
href: https://dailygeekshow.com/sigle-copyleft
featured_image: https://dailygeekshow.com/wp-content/uploads/2020/05/une-copyleft.jpg
tags:
- Droit d'auteur
series:
- 202020
---

> Le terme anglais «copyright» est connu de tous. Représenté par le sigle ©, il confère une certaine protection à une oeuvre ou un travail. Il est défini comme étant un «droit que se réserve un auteur ou son cessionnaire pour protéger l’exploitation, pendant un certain nombre d’années, d’une oeuvre». Mais saviez-vous que le contraire du copyright existe ? Il s’agit du copyleft, aussi appelé «gauche d’auteur».
