---
site: LeMagIT
title: "Alternatives open source et sécurisées de visioconférence: Jitsi"
author: Gaétan Raoul,
date: 2020-05-26
href: https://www.lemagit.fr/conseil/Alternatives-open-source-et-securisees-de-visioconference-Jitsi
featured_image: https://cdn.ttgtmedia.com/visuals/German/article/video-call-Unified-communications-adobe.jpg.jpg
seeAlso: "[#61 - Jitsi – Framasoft – Éducation – Le confinement - «Libre à vous!» diffusée mardi 7 avril 2020 sur radio Cause Commune](https://april.org/61-jitsi)"
tags:
- Sensibilisation
series:
- 202022
series_weight: 0
---

> Jitsi paraît bien petit à côté de Zoom, le service de visioconférence qui rassemble plus de 300 millions de participants au quotidien. Pourtant, ce projet open source est utilisé par environ 10 millions de personnes mensuellement. Il propose des fonctionnalités intéressantes pour celui qui veut héberger sa propre application de communication.
