---
site: Le Monde Informatique
title: "Véronique Bonnet prend la présidence de l'April"
author: Dominique Filippone
date: 2020-07-01
href: https://www.lemondeinformatique.fr/actualites/lire-veronique-bonnet-prend-la-presidence-de-l-april-79611.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000073085.jpg
tags:
- april
series:
- 202027
series_weight: 0
---

> Succédant à Jean-Christophe Becquet, Véronique Bonnet a été élue présidente de l'association pour la promotion et la recherche en informatique libre. Parmi ses préoccupations: faire de l'apprentissage du code à l'école un atout pour l'autonomie des individus.

Note de l'April: les propos de Véronique ont été mal compris dans l'article de presse qui parle d'apprentissage du code à l'école. Ce qui ne correspond pas aux propos de Véronique. Elle a précisé cela dans [une réponse d'une dépêche](https://linuxfr.org/nodes/121007/comments/1817180) sur LinuxFr.org.
