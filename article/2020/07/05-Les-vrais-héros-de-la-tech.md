---
site: GQ France
title: "Les vrais héros de la tech"
author: Loïc Hecht
date: 2020-07-05
href: https://www.gqmagazine.fr/lifestyle/article/les-vrais-heros-de-la-tech
featured_image: https://media.gqmagazine.fr/photos/5ef0b4fbcaa5a09c5304aeda/16:9/w_1920%2cc_limit/Sans-titre-1.jpg
tags:
- Matériel libre
- Innovation
series:
- 202028
series_weight: 0
---

> On les appelle les «Makers», et pendant la crise du Covid-19, ils ont mis leur ingéniosité au service du personnel soignant en confectionnant des équipements précieux imprimés en 3D. Loin des gourous de la Silicon Valley qui s’offrent une bonne conscience à coups de millions de dollars, ces MacGyver du quotidien prouvent que l’intelligence collective peut faire des miracles. Maker... avec les doigts!
