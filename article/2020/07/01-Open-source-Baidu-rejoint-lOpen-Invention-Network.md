---
site: Clubic.com
title: "Open source: Baidu rejoint l'Open Invention Network"
author: Alicia Muñoz
date: 2020-07-01
href: https://www.clubic.com/pro/entreprises/baidu/actualite-5007-open-source-baidu-rejoint-l-open-invention-network.html
featured_image: https://pic.clubic.com/v1/images/1806749/raw-accept?width=1200&fit=max&hash=84a99b34fe0f4ec8a65d2bbae1abbb452504bf78
tags:
- Brevets logiciels
- Entreprise
series:
- 202027
series_weight: 0
---

> Le «Google chinois», entreprise leader dans le domaine de l'intelligence artificielle (IA) rejoint le groupe de protection des logiciels libres et de Linux, l'Open Invention Network. 
