---
site: La Lettre du Cadre
title: "Méthode FunWork: Les Chatons à la rescousse (€)"
author: Franck Plasse
date: 2020-07-10
href: http://www.lettreducadre.fr/20167/methode-funwork-les-chatons-a-la-rescousse
featured_image: http://www.lettreducadre.fr/wp-content/uploads/2020/07/Me%CC%81thode-FunWor_92528670-320x213.jpg
tags:
- Internet
- Associations
series:
- 202028
series_weight: 0
---

> La crise sanitaire liée au Covid-19 a clairement modifié nos habitudes de travail, nous contraignant à nous organiser pour un travail à distance. Le collectif des hébergeurs alternatifs, transparents, ouverts, neutres et solidaires nous propose des services en ligne éthiques. En voici quelques exemples gratuits pour faciliter nos réunions en ligne.
