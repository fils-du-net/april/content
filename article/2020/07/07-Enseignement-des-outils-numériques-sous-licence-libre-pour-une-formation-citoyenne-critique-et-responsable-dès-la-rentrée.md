---
site: RTBF Info
title: "Enseignement: des outils numériques sous licence libre pour une formation citoyenne, critique et responsable dès la rentrée"
date: 2020-07-07
href: https://www.rtbf.be/info/opinions/detail_enseignement-des-outils-numeriques-sous-licence-libre-pour-une-formation-citoyenne-critique-et-responsable-des-la-rentree?id=10537655
featured_image: https://ds1.static.rtbf.be/article/image/370x208/9/8/7/7a246a0907f924fba999dc0023916994-1594123031.png
tags:
- Éducation
- Promotion
series:
- 202028
series_weight: 0
---

> La période que nous venons de vivre a mis la société et les services publics à rude épreuve. Il a été une nouvelle fois évident, et c’est sans doute une des conclusions heureuses, que le rôle de l’État est essentiel au bon fonctionnement d’une société qui souhaite protéger ses membres. Pour assurer la continuité pédagogique en confinement, les enseignants ont majoritairement eu recours au numérique à marche forcée. L’ordinateur, la tablette et le smartphone, formidables outils de communication, ont permis de récréer du lien entre les professeurs, les élèves et leurs parents, en mettant fortement en évidence les besoins de chacun en formation et accompagnement.
