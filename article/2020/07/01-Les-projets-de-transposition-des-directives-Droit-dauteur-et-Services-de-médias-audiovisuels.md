---
site: Next INpact
title: "Les projets de transposition des directives Droit d'auteur et Services de médias audiovisuels"
author: Marc Rees
date: 2020-07-01
href: https://www.nextinpact.com/news/109129-les-projets-transposition-directives-droit-dauteur-et-services-medias-audiovisuels.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/21524.jpg
tags:
- Droit d'auteur
- Institutions
- Europe
series:
- 202027
series_weight: 0
---

> Ce matin au Sénat, Franck Riester a indiqué le plan de bataille du gouvernement pour sauver le projet de loi sur l’audiovisuel. Le texte, englouti par la pandémie du Covid-19 et un calendrier surchargé, va être saucissonné notamment dans une ordonnance. Next INpact révèle les documents afférents.
