---
site: L'Economiste
title: "Les biens communs, ça sert à quoi?"
date: 2020-07-16
href: https://www.leconomiste.com/article/1064817-les-biens-communs-ca-sert-quoi
featured_image: https://www.leconomiste.com/sites/default/files/eco7/public/thumbnails/image/othman-el-ferdaous-05.jpg
tags:
- Économie
series:
- 202029
series_weight: 0
---

> Quel est le point commun entre le programme d'aide aux médias marocains, Wikipédia et les gestes de précaution anti-Covid-19?
