---
site: Clubic.com
title: "PeerTube, le projet d'alternative à YouTube décentralisée, avance bien!"
author: Benjamin Bruel
date: 2020-07-27
href: https://www.clubic.com/television-tv/video-streaming/youtube/actualite-7294-sr-peertube-le-projet-d-alternative-decentralisee-a-youtube-avance-bien-.html
featured_image: https://pic.clubic.com/v1/images/1671678/raw-accept?hash=b2e2b090b63feb3938eea8b0660900a9e2409d3a
tags:
- Associations
- Internet
- Innovation
series:
- 202031
series_weight: 0
---

> Deux ans après le financement participatif qui avait conduit à sa naissance, PeerTube, l'alternative à YouTube française et libre, continue gentiment de rouler sa bosse. Framasoft, l'association pour le développement de logiciels libres à l'origine du projet, dévoile la version 2.3 de PeerTube.
