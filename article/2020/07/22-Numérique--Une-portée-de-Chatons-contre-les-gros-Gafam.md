---
site: Politis.fr
title: "Numérique: Une portée de Chatons contre les gros Gafam (€)"
author: Romain Haillard
date: 2020-07-22
href: https://www.politis.fr/articles/2020/07/numerique-une-portee-de-chatons-contre-les-gros-gafam-42198
featured_image: https://static.politis.fr/medias/articles/2020/07/numerique-une-portee-de-chatons-contre-les-gros-gafam-42198/thumbnail_large-42198.jpg
tags:
- Internet
- Associations
series:
- 202030
series_weight: 0
---

> Le confinement a fait apparaître avec clarté notre dépendance aux géants du numérique. Mais des services associatifs et locaux offrent une alternative.
