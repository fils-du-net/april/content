---
site: La Tribune
title: "Le cluster Naos passe le logiciel libre à l'échelle de la Nouvelle-Aquitaine"
date: 2020-07-22
href: https://objectifaquitaine.latribune.fr/business/2020-07-22/le-cluster-naos-passe-le-logiciel-libre-a-l-echelle-de-la-nouvelle-aquitaine-853216.html
featured_image: https://static.latribune.fr/full_width/1482527/naos-open-source.jpg
tags:
- Entreprise
series:
- 202030
series_weight: 0
---

> Ne dites plus Aquinetic mais Naos, pour Nouvelle-Aquitaine open source. Ce cluster régional dédié au logiciel libre vient de changer de nom et d'échelle pour regrouper désormais 120 PME régionales. La souveraineté numérique et la formation des entreprises sont au cœur de ses priorités comme l'explique à La Tribune, son co-président François Pellegrini.
