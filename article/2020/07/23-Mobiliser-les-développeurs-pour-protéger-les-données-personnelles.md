---
site: Les Echos
title: "Mobiliser les développeurs pour protéger les données personnelles"
author: Damien Clochard
date: 2020-07-23
href: https://www.lesechos.fr/idees-debats/cercle/opinion-comprendre-et-proteger-les-donnees-personnelles-1225831
featured_image: https://media.lesechos.com/api/v1/images/view/5f196c513e45466dd8040e26/1280x720/209996-1.jpg
tags:
- Institutions
- Vie privée
series:
- 202030
---

> Pour protéger les données, les démarches de pseudonymisation et d'anonymisation s’accélèrent dans les entreprises. Mais pour Damien Clochard, ces outils sont limités.
