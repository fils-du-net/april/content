---
site: Numerama
title: "Notepad++ prend parti pour Hong Kong, au risque de subir un nouveau raid de militants pro-Chinois"
date: 2020-07-17
href: https://www.numerama.com/tech/637316-notepad-prend-parti-pour-hong-kong-au-risque-de-subir-un-nouveau-raid-de-militants-pro-chinois.html
featured_image: https://www.numerama.com/content/uploads/2020/07/hong-kong-free-hk.jpg
tags:
- International
series:
- 202029
series_weight: 0
---

> Notepad++ dédie sa dernière mise à jour à Hong Kong, qui fait face à une ferme reprise en main de la Chine.
