---
site: Nextgen-Auto.com
title: "Les pièces de F1 en open-source: révolution ou poudre aux yeux?"
author: A. Combralier
date: 2020-07-13
href: https://motorsport.nextgen-auto.com/fr/formule-1/les-pieces-de-f1-en-open-source-revolution-ou-poudre-aux-yeux,149403.html
featured_image: https://motorsport.nextgen-auto.com/IMG/arton149403.jpg?1593290877
tags:
- Matériel libre
- Partage du savoir
series:
- 202029
series_weight: 0
---

> Dans le but de rapprocher les performances entre équipes comme de réduire les coûts, la F1 a décidé d’une mesure encore peu commentée: mettre en place un système open-source, pour que les plans de certaines pièces soient accessibles à toutes les écuries. Ce système pourra être testé l’an prochain, mais sera surtout obligatoire en 2022, pour l’arrivée du nouveau règlement.
