---
site: Le Monde.fr
title: "L’accord sur le transfert de données personnelles entre l’UE et les Etats-Unis annulé par la justice européenne"
author: Damien Leloup et Grégor Brandy
date: 2020-07-16
href: https://www.lemonde.fr/pixels/article/2020/07/16/la-justice-europeenne-annule-l-accord-sur-le-transfert-de-donnees-personnelles-ue-etats-unis_6046344_4408996.html
tags:
- Vie privée
- Europe
- International
series:
- 202029
series_weight: 0
---

> Ce dispositif adopté en 2016 est utilisé par la quasi-totalité des grandes entreprises américaines pour traiter les données personnelles (identité, comportement en ligne, géolocalisation…) de leurs utilisateurs européens.
