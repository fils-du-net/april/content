---
site: ouest-france.fr
title: "La démocratie participative doit être à la hauteur des enjeux qu’elle veut défendre"
author: Axel Dauchez
date: 2020-07-11
href: https://www.ouest-france.fr/societe/point-de-vue-la-democratie-participative-doit-etre-a-la-hauteur-des-enjeux-qu-elle-veut-defendre-6903159
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMDA3NmUxNDlhNDhjNjYyNjU2MTgyOGE1ZWZlN2YxYTAwZTY
tags:
- Institutions
series:
- 202028
series_weight: 0
---

> «La survie de notre modèle de société nécessite une réappropriation citoyenne urgente de l’action publique», selon Axel Dauchez, président et fondateur de Make.org,
