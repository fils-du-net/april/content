---
site: Next INpact
title: "La Cour de justice de l'Union européenne annule le Privacy Shield (€)"
author: Sébastien Gavois
date: 2020-07-16
href: https://www.nextinpact.com/news/109172-la-cour-justice-lunion-europeenneannule-privacy-shield.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/22603.jpg
tags:
- Vie privée
- Europe
series:
- 202029
---

> Second coup de tonnerre européen en deux jours: la CJUE vient d’annuler le Privacy Shield. Elle valide par contre les clauses types pour le transfert de données à caractère personnel vers des sous-traitants établis dans des pays tiers.
