---
site: Sciencepost 
title: "21 téraoctets de données open-source ont été mis à l'abri en Arctique (juste au cas où)"
author: Brice Louvet
date: 2020-07-25
href: https://sciencepost.fr/donnees-open-source-arctique
featured_image: https://sciencepost.fr/wp-content/uploads/2020/07/010-github-vault-0_1024.jpg
tags:
- Partage du savoir
series:
- 202030
---

> 21 téraoctets de données open-source se trouvent maintenant sous le pergélisol norvégien pour être protégés et préservés en cas d'apocalypse.
