---
site: GinjFo
title: "Ordinateurs de bureau, Windows continue de reculer au profit de Linux!"
author: Jérôme Gianoli
date: 2020-07-15
href: https://www.ginjfo.com/actualites/logiciels/linux/ordinateurs-de-bureau-windows-continue-de-reculer-au-profit-de-linux-20200715
featured_image: https://www.ginjfo.com/wp-content/uploads/2016/10/Windows10_Chute-1.jpg
tags:
- Sensibilisation
- Logiciels privateurs
series:
- 202029
series_weight: 0
---

> Les derniers chiffres de NetMarketShare confirment une tendance inquiétante pour Microsoft. Windows recule sur le marché des ordinateurs de bureau.
