---
site: The Conversation
title: "Réconcilier l'Économie sociale et solidaire avec la tech, l'autre enjeu de la transition numérique"
author: Arthur Gautier
date: 2020-07-21
href: https://theconversation.com/reconcilier-leconomie-sociale-et-solidaire-avec-la-tech-lautre-enjeu-de-la-transition-numerique-142827
featured_image: https://images.theconversation.com/files/347888/original/file-20200716-29-1910b34.jpg
tags:
- Économie
series:
- 202030
series_weight: 0
---

> Des questions de financement, de formation ou encore de valeurs expliquent le fossé entre deux mondes qui auraient pourtant tout intérêt à travailler ensemble.
