---
site: Infoguerre
title: "Logiciel Libre ou Open Source: de l'idéologie à l'influence"
author: Cédric Joly
date: 2020-07-03
href: https://infoguerre.fr/2020/07/logiciel-libre-open-source-de-lideologie-a-linfluence
featured_image: https://cdn.pixabay.com/photo/2016/09/23/08/28/code-1689066_960_720.jpg
tags:
- Sensibilisation
series:
- 202027
series_weight: 0
---

> Le 20 août 2011, Marc Andreessen – pionnier du web et co-fondateur du cabinet de capital-risque Andreessen Horowitz, investissant massivement dans les start-ups numériques – publie dans le Walt Street Journal un article expliquant que «le logiciel est en train de dévorer le monde»
