---
site: LeMagIT
title: "OpenCore: le créateur de Redis laisse sa place à un modèle de gouvernance plus ouvert"
author: Gaétan Raoul
date: 2020-07-06
href: https://www.lemagit.fr/actualites/252485705/OpenCore-le-createur-de-Redis-laisse-sa-place-a-un-modele-de-gouvernance-plus-ouvert
featured_image: https://cdn.ttgtmedia.com/visuals/searchAWS/cloud_development/aws_article_006.jpg
tags:
- Innovation
series:
- 202028
series_weight: 0
---

> Salvatore Sanfilippo, le créateur de Redis, quitte son poste de mainteneur du cœur open source. Redis Labs profite de ce départ pour établir une gouvernance «plus ouverte» pour régir les évolutions de la base de données.
