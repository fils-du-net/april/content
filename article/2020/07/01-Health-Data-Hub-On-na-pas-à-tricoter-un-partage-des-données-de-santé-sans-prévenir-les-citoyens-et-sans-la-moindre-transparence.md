---
site: TV5MONDE
title: "Health Data Hub: \"On n'a pas à tricoter un partage des données de santé sans prévenir les citoyens et sans la moindre transparence\""
author: Pascal Hérard
date: 2020-07-01
href: https://information.tv5monde.com/info/health-data-hub-n-pas-tricoter-un-partage-des-donnees-de-sante-sans-prevenir-les-citoyens-et
featured_image: https://information.tv5monde.com/sites/info.tv5monde.com/files/styles/large/public/assets/images/Helath-Data-Hub-France.jpg
tags:
- Vie privée
- Entreprise
series:
- 202027
---

> La crise du Covid-19 soulève des questions sur la protection des données de santé en France. C'est dans ce contexte que la sénatrice Nathalie Goulet s'inquiète que le futur Health Data Hub soit géré par l'entreprise américaine Microsoft, sans concertation ni débat. Sa demande d'enquête parlementaire vient d'aboutir, l'attribution à Microsoft du marché du Health Data Hub fera partie de la mission d'investigation de la Commission parlementaire consacrée au Covid19.
