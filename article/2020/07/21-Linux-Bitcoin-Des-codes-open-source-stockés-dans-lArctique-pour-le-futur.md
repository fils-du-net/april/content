---
site: Le Figaro.fr
title: "Linux, Bitcoin... Des codes open source stockés dans l'Arctique pour le futur"
author: Ingrid Vergara
date: 2020-07-21
href: https://www.lefigaro.fr/secteur/high-tech/linux-bitcoin-des-codes-open-source-stockes-dans-l-arctique-pour-le-futur-20200721
featured_image: https://i.f1g.fr/media/eidos/767x431_crop/2020/07/21/XVM9ead93a4-cb69-11ea-bba7-62e28879044c.jpg
tags:
- Partage du savoir
series:
- 202030
series_weight: 0
---

> À la manière d’une Bibliothèque d’Alexandrie version digitale, la plateforme de développement GitHub a déposé des codes de logiciels open source emblématiques dans un coffre-fort sous le permafrost de la région norvégienne du Svalbard.
