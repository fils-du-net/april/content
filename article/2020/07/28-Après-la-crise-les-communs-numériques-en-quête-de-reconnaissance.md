---
site: Le Monde.fr
title: "Après la crise, les communs numériques en quête de reconnaissance (€)"
author: Claire Legros
date: 2020-07-28
href: https://www.lemonde.fr/series-d-ete/article/2020/07/28/apres-la-crise-les-communs-numeriques-en-quete-de-reconnaissance_6047455_3451060.html
featured_image: https://img.lemde.fr/2020/07/16/48/0/4204/2102/2048/1024/30/0/aeadbe6_99245183-Numerique.jpg
tags:
- Économie
- Open Data
series:
- 202031
series_weight: 0
---

> «Le retour des communs» (2/6). Pendant le confinement, les données et outils ouverts ont joué un rôle vital et prouvé leur pertinence en temps de crise. La séquence a aussi mis en lumière la nécessaire articulation entre ces communs numériques et les secteurs public et privé.
