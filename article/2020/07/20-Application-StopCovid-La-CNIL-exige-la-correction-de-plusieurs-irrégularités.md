---
site: Next INpact
title: "Application StopCovid: La CNIL exige la correction de plusieurs irrégularités (€)"
author: Marc Rees
date: 2020-07-20
href: https://www.nextinpact.com/news/109181-application-stopcovid-la-cnil-exige-correction-plusieurs-irregularites.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/25282.jpg
tags:
- Institutions
- Vie privée
series:
- 202030
series_weight: 0
---

> Comme promis, la Commission chargée de veiller au respect des données personnelles a mis son nez dans StopCovid. Ses conclusions? L’application de suivi de contact souffre de plusieurs problèmes au regard du RGPD. «Le ministère de la Santé est mis en demeure d’y remédier» expose l’autorité.
