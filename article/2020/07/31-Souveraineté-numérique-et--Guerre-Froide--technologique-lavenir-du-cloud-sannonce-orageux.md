---
site: LeMagIT
title: "Souveraineté numérique et «Guerre Froide» technologique: l’avenir du cloud s’annonce orageux"
author: Philippe Ducellier
date: 2020-07-31
href: https://www.lemagit.fr/actualites/252487001/Souverainete-numerique-et-Guerre-Froide-technologique-lavenir-du-cloud-sannonce-orageux
featured_image: https://cdn.ttgtmedia.com/visuals/LeMagIT/hero_article/Storm_AdobeStock_213467508.jpeg
tags:
- Informatique en nuage
- Institutions
series:
- 202031
series_weight: 0
---

> Pour la banque d’investissement Klecha & Co, au regard du contexte de guerre économique où l’Europe est un terrain de jeu possible pour les États-Unis et la Chine, la cloudification massive ira de pair avec la question de la souveraineté des données et de l’indépendance technologique.
