---
site: Next INpact
title: "C'est une «belle journée»: la Cour de justice de l'Union européenne consacre la neutralité du Net"
date: 2020-09-16
href: https://www.nextinpact.com/lebrief/43732/cest-belle-journee-cour-justice-lunion-europeenne-consacre-neutralite-net
featured_image: https://cdnx.nextinpact.com/data-next/images/bd/wide-linked-media/1234.jpg
tags:
- Neutralité du Net
series:
- 202038
---

> La CJUE explique que, pour la toute première fois, elle interprète «le règlement de l’Union consacrant la "neutralité d’Internet"». La question de fond était de savoir si un fournisseur de service pouvait proposer des «offres groupées d’accès préférentiel (dites à «tarif nul») ayant pour particularité que le trafic de données généré par certains services et applications spécifiques n’est pas décompté dans la consommation du volume de données acheté par les clients».
