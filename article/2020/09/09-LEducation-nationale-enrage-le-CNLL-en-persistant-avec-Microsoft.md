---
site: Le Monde Informatique
title: "L'Education nationale enrage le CNLL en persistant avec Microsoft"
author: Dominique Filippone
date: 2020-09-09
href: https://www.lemondeinformatique.fr/actualites/lire-l-education-nationale-enrage-le-cnll-en-persistant-avec-microsoft-80322.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000074127.jpg
tags:
- Marchés publics
- Éducation
series:
- 202037
---

> Un article du Canard Enchâiné révèle un appel d'offres lancé en août par le ministère de l'Education Nationale pour s'équiper d'une centaine de solutions Microsoft. Après un précédent remontant à 2016, le Conseil National du Logiciel Libre repart au front pour dénoncer ce choix.
