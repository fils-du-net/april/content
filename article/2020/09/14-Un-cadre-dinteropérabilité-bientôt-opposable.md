---
site: DSIH
title: "Un cadre d'interopérabilité bientôt opposable"
author: Pierre Derrouch
date: 2020-09-14
href: https://www.dsih.fr/article/3887/un-cadre-d-interoperabilite-bientot-opposable.html
featured_image: https://www.dsih.fr/images/2020-09/L200_masante2022_logo.jpg
tags:
- Interopérabilité
series:
- 202038
---

> Il a fallu plusieurs années avant de pouvoir changer d'opérateur téléphonique sans perdre son numéro d'appel. Une (r)évolution est en marche pour les industriels de l'e-santé. Dans quelques semaines doit être publié le décret rendant opposable le cadre d'interopérabilité des systèmes d'information de santé (CI-SIS) défini par l'Agence du numérique en santé (ANS). À la clef, partage de données et portabilité.
