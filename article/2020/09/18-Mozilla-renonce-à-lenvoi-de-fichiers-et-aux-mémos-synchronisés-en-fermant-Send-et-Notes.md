---
site: Numerama
title: "Mozilla renonce à l'envoi de fichiers et aux mémos synchronisés en fermant Send et Notes"
author: Julien Lausson
date: 2020-09-18
href: https://www.numerama.com/tech/649009-mozilla-renonce-a-lenvoi-de-fichiers-et-aux-memos-synchronises-en-fermant-send-et-notes.html
featured_image: https://c2.lestechnophiles.com/www.numerama.com/content/uploads/2020/07/firefox-send.jpg
tags:
- Internet
series:
- 202038
series_weight: 0
---

> L'outil de partage de fichiers Firefox Send ne sera pas relancé. Quant au service Notes, qui permet de prendre des mémos et de les synchroniser entre plusieurs appareils, il sera désactivé en novembre.
