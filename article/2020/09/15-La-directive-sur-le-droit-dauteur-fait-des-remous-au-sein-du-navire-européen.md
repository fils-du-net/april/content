---
site: EurActiv
title: "La directive sur le droit d'auteur fait des remous au sein du navire européen"
date: 2020-09-15
href: https://www.euractiv.fr/section/economie/news/eu-civil-society-says-commissions-copyright-guidance-violates-fundamental-rights/
featured_image: https://www.euractiv.fr/wp-content/uploads/sites/3/2020/09/w_55025389-800x450.jpg
tags:
- Institutions
- Droit d'auteur
- Europe
series:
- 202038
series_weight: 0
---

> Plusieurs sociétés civiles européennes ont écrit une lettre à Thierry Breton, commissaire européen au marché intérieur, mettant en lumière de «profondes inquiétudes» liées au document d'orientations de la Commission européenne sur la directive relative au droit d'auteur.
