---
site: la Croix
title: "Numérique: le guide pour échapper à la mainmise des GAFAM (€)"
author: Stéphane Bataillon
date: 2020-09-26
href: https://www.la-croix.com/Sciences-et-ethique/Numerique-guide-echapper-mainmise-GAFAM-2020-09-26-1201116151
featured_image: https://img.aws.la-croix.com/2020/09/26/1201116151/Sur-Facebook-pouvez-desactiver-lactivite-future-dehors-Facebook-votre-navigation-Internet-publicites-ciblees_4_729_486.jpg
tags:
- Sensibilisation
- Associations
- Internet
- Vie privée
series:
- 202039
series_weight: 0
---

> L'exploitation de nos données personnelles et de notre vie privée par les géants du Net ne cesse d'augmenter. Mais un autre numérique est possible, qui propose des solutions respectueuses de nos données personnelles, créées dans un esprit collaboratif et soucieuses de leur impact sur la planète. Ce guide vous invite à les découvrir.
