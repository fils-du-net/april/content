---
site: Le Figaro.fr
title: "Santé/Recherche: le Conseil d'Etat rejette un recours contre les transferts de données vers les USA"
date: 2020-09-21
href: https://www.lefigaro.fr/flash-eco/sante/recherche-le-conseil-d-etat-rejette-un-recours-contre-les-transferts-de-donnees-vers-les-usa-20200921
tags:
- Vie privée
- Entreprise
series:
- 202039
series_weight: 0
---

> Le Conseil d'Etat a rejeté ce lundi le recours en référé d'un collectif qui souhaitait suspendre le transfert de données de santé de la plateforme française destinée à la recherche Health Data Hub vers les serveurs du géant américain Microsoft.
