---
site: ZDNet France
title: "L'Inria veut faciliter l'accès aux technologies numériques open source"
author: Clarisse Treilles
date: 2020-09-08
href: https://www.zdnet.fr/actualites/l-inria-veut-faciliter-l-acces-aux-technologies-numeriques-open-source-39909225.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2020/01/Formation.jpg
tags:
- Entreprise
- Internet
- Accessibilité
- Innovation
- Sciences
- Europe
- International
series:
- 202037
---

> Inria Academy, c'est le nom du nouveau dispositif de formation continue pour aider les entreprises à utiliser certains logiciels open source.