---
site: ouest-france.fr
title: "Voile. Arthur Le Vaillant: «On ne doit pas être des sportifs comme les autres»"
date: 2020-09-10
href: https://saint-brieuc.maville.com/sport/detail_-voile.-arthur-le-vaillant-on-ne-doit-pas-etre-des-sportifs-comme-les-autres-_54135-4272241_actu.Htm
featured_image: https://mvistatic.com/photosmvi/2020/09/10/P23580449D4272241G_px_640_.jpg
tags:
- Innovation
series:
- 202037
series_weight: 0
---

> A 32 ans le skipper Rochelais a déjà un beau palmarès (2e de la transat Jacques Vabre en class 40 2017, 4e de la Route du Rhum 2018 en Class 40), mais pour lui qui est également musicien, la compétition n’est pas tout. Après sa victoire lors du Grand Prix de Brest Multi 50 dimanche dernier, celui qui passe beaucoup de temps dans la Baie de Morlaix est revenu pour Ouest-France sur ce qui l’anime en dehors de la compétition : l’impact environnemental de son sport. Entretien.
