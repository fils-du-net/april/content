---
site: Association mode d'emploi
title: "Les Chatons concurrencent les Gafam"
author: Laurent Costy, Angie Gaudion
date: 2020-09-15
href: https://www.associationmodeemploi.fr/article/les-chatons-concurrencent-les-gafam.71529
featured_image: https://www.associationmodeemploi.fr/mediatheque/4/5/6/000013654_600x400_c.jpg
tags:
- Associations
- April
series:
- 202038
series_weight: 0
---

> Après une phase importante de déploiement d’outils numériques par les associations (1), on assiste à une rationalisation et à un intérêt marqué pour les logiciels libres. Ceux-ci permettent aujourd’hui de répondre aux principaux besoins des acteurs associatifs. Des besoins renforcés depuis la crise sanitaire du Covid-19 qui obligent à repenser nos modes de fonctionnement. Le collectif Chatons les aide à les découvrir et les adopter.
