---
site: Journal du Net
title: "Une digital workplace 100% free se révèle dans le sillage du Covid"
author: Antoine Crochet-Damais
date: 2020-09-29
href: https://www.journaldunet.com/solutions/dsi/1494223-une-digital-workplace-100-free-se-revele-dans-le-sillage-du-covid
featured_image: https://img-0.journaldunet.com/xMYxuDRwSqTJ1KGxOnRleJDEa_E=/540x/smart/87e8fb4b3fb742268a9bf463360bebd0/ccmcms-jdn/18990269.jpg
tags:
- Innovation
- Entreprise
series:
- 202040
series_weight: 0
---

> Une plateforme open source désormais très complète se dessine pour outiller le travail à distance. Au programme: intranet social, suite de productivité, team messaging, visioconférence...
