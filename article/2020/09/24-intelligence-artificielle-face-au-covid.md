---
site: France Culture
title: "Pierre-Antoine Gourraud: l'intelligence artificielle face au Covid"
author: Maxime Tellier
date: 2020-09-24
href: https://www.franceculture.fr/emissions/comme-personne/pierre-antoine-gourraud-lintelligence-artificielle-face-au-covid
featured_image: https://cdn.radiofrance.fr/s3/cruiser-production/2020/09/d802c589-b6f9-46b6-99e8-e249e5620e24/838_pierre_antoine_gourraud.webp
tags:
- Sciences
- Matériel libre
series:
- 202039
series_weight: 0
---

> Pierre-Antoine Gourraud est professeur de médecine au CHU de Nantes mais il n'a pas été au contact des patients pendant le pic de la crise sanitaire au printemps. Cet enseignant-chercheur passé par la Californie utilise la donnée et le numérique pour enrayer la pandémie.
