---
site: Le Vent Se Lève
title: "«La surveillance est un mode du capitalisme» - Entretien avec Christophe Masutti"
author: Maud Barret Bertelloni
date: 2020-09-25
href: https://lvsl.fr/aux-sources-du-capitalisme-de-surveillance-entretien-avec-christophe-masutti
featured_image: https://lvsl.fr/wp-content/uploads/2020/09/photo_2020-09-25_14-33-20.jpg
tags:
- Économie
- Vie privée
series:
- 202039
series_weight: 0
---

> La surveillance et le marché ne cessent de s'immiscer dans notre milieu de vie et nos rapports sociaux. Entretien d'analyse et de recherche de pistes d'émancipation collective avec Christophe Masutti, auteur d'Affaires privées, Aux sources du capitalisme de surveillance (2020).
