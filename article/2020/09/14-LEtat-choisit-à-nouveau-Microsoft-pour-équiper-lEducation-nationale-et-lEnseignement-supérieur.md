---
site: usine-digitale.fr
title: "L'Etat choisit à nouveau Microsoft pour équiper l'Education nationale et l'Enseignement supérieur"
author: Alice Vitard
date: 2020-09-14
href: https://www.usine-digitale.fr/article/l-etat-choisit-a-nouveau-microsoft-pour-equiper-l-education-nationale-et-l-enseignement-superieur.N1003774
featured_image: https://www.usine-digitale.fr/mediatheque/4/0/5/000897504_homePageUne/enfants.jpg
tags:
- Marchés publics
- Éducation
series:
- 202038
---

> L'Etat a lancé un appel d'offres de 8,3 millions d'euros pour équiper de "solutions Microsoft" les services de l'Education nationale et de l'Enseignement supérieur. Pour le Conseil national du logiciel libre, le choix d'un acteur américain est contestable et écarte la concurrence française et européenne. Un raisonnement qui rappelle les critiques formulées à l'encontre du Health Data Hub.
