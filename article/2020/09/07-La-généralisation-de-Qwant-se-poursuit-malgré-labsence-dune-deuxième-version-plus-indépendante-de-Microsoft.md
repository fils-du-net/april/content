---
site: Acteurs Publics 
title: "La généralisation de Qwant se poursuit malgré l'absence d'une deuxième version plus indépendante de Microsoft (€)"
author: Emile Marzolf
date: 2020-09-07
href: https://www.acteurspublics.fr/articles/la-generalisation-de-qwant-se-poursuit-malgre-labsence-dune-deuxieme-version-plus-independante-de-microsoft
tags:
- Entreprise
series:
- 202037
---

> Le moteur de recherche français est devenu, cette année, le moteur par défaut des ordinateurs des agents publics. Comme l’avait révélé Acteurs publics dans la foulée de cette décision, la DSI de l’État avait alors conditionné son feu vert à la mise en route, début 2020, d’une deuxième version plus indépendante de Microsoft. Mais celle-ci n’a toujours pas été déployée.
