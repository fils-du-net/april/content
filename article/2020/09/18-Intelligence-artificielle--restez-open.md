---
site: Journal du Net
title: "Intelligence artificielle: restez open!"
author: Christophe Boitiaux
date: 2020-09-18
href: https://www.journaldunet.com/solutions/dsi/1494023-intelligence-artificielle-restez-open
tags:
- Informatique en nuage
- Innovation
series:
- 202038
series_weight: 0
---

> Aujourd'hui dans 90% des cas, le cloud public classique est suffisant pour gérer le volume de data nécessaire au machine learning. Mais demain, il est fort probable qu'on ne puisse se passer de l'informatique quantique.
