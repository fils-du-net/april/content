---
site: Le Point
title: "Alice Zeniter: «J'interroge les différentes possibilités de l'engagement»"
date: 2020-09-12
href: https://www.lepoint.fr/afrique/alice-zeniter-j-interroge-les-differentes-possibilites-de-l-engagement-12-09-2020-2391591_3826.php
featured_image: https://static.lpnt.fr/images/2020/09/12/20749622lpw-20749675-article-jpg_7343473_660x281.jpg
tags:
- Économie
- Sensibilisation
series:
- 202037
series_weight: 0
---

> ENTRETIEN. Avec son roman «Comme un empire dans un empire», Alice Zeniter interroge le dépassement des «passions tristes» et peut-être au fond, aussi, le sens du retrait et de l'absence de réponse définitive comme ultime refus.
