---
site: Acteurs Publics
title: "1 milliard d'euros pour accélérer la transformation numérique des services publics (€)"
author: Emile Marzolf
date: 2020-09-03
href: https://www.acteurspublics.fr/articles/un-milliard-deuros-pour-accelerer-la-transformation-numerique-des-services-publics
tags:
- Administration
series:
- 202036
series_weight: 0
---

> Dans le cadre du plan de relance présenté ce 3 septembre par le gouvernement, une enveloppe spécifique d’1 milliard d’euros est prévue pour financer l’innovation et la transformation numérique des administrations d’État et locales sur les deux prochaines années. Cinq cents millions d’euros sont également alloués à l’inclusion numérique et à la couverture du territoire en haut débit.
