---
site: Le Monde.fr
title: "Données de santé: le Conseil d’Etat rejette un nouveau recours contre le Health Data Hub"
date: 2020-09-22
href: https://www.lemonde.fr/pixels/article/2020/09/22/donnees-de-sante-le-conseil-d-etat-rejette-un-nouveau-recours-contre-le-health-data-hub_6053180_4408996.html
tags:
- Vie privée
- Entreprise
- Institutions
series:
- 202039
---

> Cette action avait été lancée par un collectif s’inquiétant de voir des données de santé de Français hébergées par l’entreprise américaine Microsoft.
