---
site: Acteurs Publics 
title: "Jean-Luc Nevache: \"La culture des administrations est, encore aujourd'hui, davantage celle du secret que de l'ouverture\" (€)"
date: 2020-09-30
href: https://www.acteurspublics.fr/articles/jean-luc-nevache-la-crise-sanitaire-a-permis-a-la-cada-depuiser-le-stock-de-dossiers-en-attente-de-traitement
tags:
- Institutions
- Open Data
series:
- 202040
series_weight: 0
---

> Dans un entretien à Acteurs publics, le nouveau président de la Commission d’accès aux documents administratifs (Cada), Jean-Luc Nevache, rappelle “aux fonctionnaires et élus que la Cada est le dernier recours avant le tribunal administratif, et non la première étape avant de communiquer le document”. La Cada a affiché en 2019 des résultats encore une fois bien en-deça des attentes des requérants et de la loi. Un audit interne devrait toutefois être lancé en octobre afin d’identifier des axes d’amélioration de la productivité des agents de la commission.
