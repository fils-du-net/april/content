---
site: Le Monde Informatique
title: "La Chine bannit Scratch, le langage d'apprentissage à la programmation"
author: Jacques Cheminat
date: 2020-09-08
href: https://www.lemondeinformatique.fr/actualites/lire-la-chine-bannit-scratch-le-langage-d-apprentissage-a-la-programmation-80299.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000074101.jpg
tags:
- International
series:
- 202037
series_weight: 0
---

> La guerre commerciale entre les Etats-Unis et la Chine fait une nouvelle victime: Scratch. Le langage d'apprentissage à la programmation développé par le MIT est interdit d'accès aux jeunes Chinois.
