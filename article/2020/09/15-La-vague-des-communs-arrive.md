---
site: La gazette.fr
title: "La vague des communs arrive"
author: Delphine Gerbeau
date: 2020-09-15
href: https://www.lagazettedescommunes.com/695697/la-vague-des-communs-arrive
featured_image: https://www.lagazettedescommunes.com/wp-content/uploads/2020/09/dos-les-communs-web-002.jpg
tags:
- Économie
- Administration
series:
- 202038
series_weight: 0
---

> Notion très ancienne, les communs connaissent depuis quelques années un regain d'intérêt, à l'heure de la recherche d'économies et d'autres façons de mener les politiques publiques. Plusieurs pays européens sont bien en avance sur le sujet, en France quelques collectivités tentent de donner forme à cette gestion "en commun".
