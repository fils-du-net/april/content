---
site: ZDNet France
title: "Comment évaluer les projets open source?"
author: Steven J. Vaughan-Nichols
date: 2020-09-08
href: https://www.zdnet.fr/pratique/comment-evaluer-les-projets-open-source-39909227.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/02/open-source-code-620.jpg
tags:
- Sensibilisation
series:
- 202037
---

> Bien sûr, nous aimerions tous avoir un moyen facile de juger les programmes open source. C'est possible. Mais facilement? Non. Le point sur la bonne manière de faire.
