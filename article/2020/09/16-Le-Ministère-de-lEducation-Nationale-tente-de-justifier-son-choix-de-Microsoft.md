---
site: cio-online.com
title: "Le Ministère de l'Education Nationale tente de justifier son choix de Microsoft"
date: 2020-09-16
href: https://www.cio-online.com/actualites/lire-le-ministere-de-l-education-nationale-tente-de-justifier-son-choix-de-microsoft-12539.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000015947.jpg
tags:
- Marchés publics
- Éducation
series:
- 202038
series_weight: 0
---

> Après l'article dans Le Canard Enchaîné et la plainte du CNLL, le Ministère de l'Education Nationale a transmis ses arguments en faveur de Microsoft.
