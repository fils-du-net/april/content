---
site: Le Monde Informatique
title: "Open source: préserver les mainteneurs du code"
author: Matt Asay
date: 2020-09-03
href: https://www.lemondeinformatique.fr/actualites/lire-open-source-preserver-les-mainteneurs-du-code-80253.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000074016.jpg
tags:
- Économie
- Sensibilisation
series:
- 202036
series_weight: 0
---

> Si les logiciels open source sont gratuits et reproductibles à l'infini, les personnes qui prennent en charge le maintien du code sont rares et précieuses.
