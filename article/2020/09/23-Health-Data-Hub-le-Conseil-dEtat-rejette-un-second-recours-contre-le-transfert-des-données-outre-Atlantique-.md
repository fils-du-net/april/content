---
site: ZDNet France
title: "Health Data Hub: le Conseil d'Etat rejette un second recours contre le transfert des données outre-Atlantique"
author: Clarisse Treilles
date: 2020-09-23
href: https://www.zdnet.fr/actualites/health-data-hub-le-conseil-d-etat-rejette-un-second-recours-contre-le-transfert-des-donnees-outre-atlantique-39910027.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2020/01/sante.jpg
tags:
- Vie privée
series:
- 202039
---

> Les requérants ont mis en avant l'argument Privacy Shield. Microsoft France recentre le débat sur le système des clauses contractuelles types.
