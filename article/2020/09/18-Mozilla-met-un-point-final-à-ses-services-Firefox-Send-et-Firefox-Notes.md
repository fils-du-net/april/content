---
site: ZDNet France
title: "Mozilla met un point final à ses services Firefox Send et Firefox Notes"
author: Catalin Cimpanu
date: 2020-09-18
href: https://www.zdnet.fr/actualites/mozilla-met-un-point-final-a-ses-services-firefox-send-et-firefox-notes-39909827.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/Pierre%20temp/Firefox%20Send%20A.jpg
tags:
- Internet
series:
- 202038
---

> Mozilla vient d'annoncer la fermeture définitive de ses deux services Firefox Send et Firefox Notes, deux services choyés par des groupes de cybercriminels.
