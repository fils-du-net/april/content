---
site: ZDNet France
title: "Logiciel libre à l'Education nationale: une question, une non-réponse et des éléments"
author: Thierry Noisette
date: 2020-09-28
href: https://www.zdnet.fr/blogs/l-esprit-libre/logiciel-libre-a-l-education-nationale-une-question-une-non-reponse-et-des-elements-39910289.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2020/09/Ecole_Salle_de_Classe_WMC.jpg
tags:
- Éducation
- Institutions
- april
series:
- 202040
series_weight: 0
---

> Quand une députée interroge sur la place faite au logiciel libre dans l'Education nationale, elle reçoit une réponse du plus beau flou. Dommage, car il y avait à dire.
