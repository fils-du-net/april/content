---
site: cio-online.com
title: "Stéphane Rousseau (VP, CIGREF): «les logiciels libres sont des choix par défaut... dans les infrastructures»"
date: 2020-09-28
href: https://www.cio-online.com/actualites/lire-stephane-rousseau-vp-cigref-%C2%A0-%C2%A0les-logiciels-libres-sont-des-choix-par-defaut-dans-les-infrastructures%C2%A0-12562.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000015983.jpg
tags:
- Entreprise
series:
- 202040
series_weight: 0
---

> A l'occasion de la webconférence «Le DSI, entre acheteur IT et FinOps» le 22 septembre 2020, CIO a interrogé Stéphane Rousseau, VP du Cigref.
