---
site: BlogNT
title: "Les codes sources de Windows XP et Office 2003 ont fait l'objet d'une fuite en ligne"
author: Yohann Poiron
date: 2020-09-26
href: https://www.blog-nouvelles-technologies.fr/186310/codes-sources-windows-xp-et-office-2003-ont-fait-objet-fuite-en-ligne
featured_image: https://www.blog-nouvelles-technologies.fr/wp-content/uploads/2020/09/RE4Cd9a-840x500.jpg
tags:
- Logiciels privateurs
series:
- 202039
series_weight: 0
---

> Les codes sources de Windows XP, de Office 2003 et d'un certain nombre d'autres propriétés de Microsoft auraient fait l'objet de fuites sur la toile.
