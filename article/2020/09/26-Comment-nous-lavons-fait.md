---
site: la Croix
title: "Comment nous l'avons fait? (€)"
date: 2020-09-26
href: https://www.la-croix.com/JournalV2/Comment-nous-lavons-fait-2020-09-26-1101116073
featured_image: https://img.aws.la-croix.com/2020/09/26/1101116073/framasoft_0_729_729.jpg
tags:
- Sensibilisation
series:
- 202039
---

> La très grande majorité des conseils et solutions présentés ont fait l'objet de tests de notre part ces trois derniers mois. Pour faire ces tests, nous disposions de trois ordinateurs, équipés de Windows 10, de MacOS, et de Linux Mint, ainsi que d'une tablette iPad (Apple), et d'un smartphone Samsung S7 sous Android. Nous avons croisé ces expériences avec les avis de nos amis et contacts informaticiens, très au fait de ces questions (y compris au sein de Bayard, merci à Jérôme Genneviève et Sylvain Dehe!).
