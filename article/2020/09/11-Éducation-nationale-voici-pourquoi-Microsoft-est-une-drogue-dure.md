---
site: ZDNet France
title: "Éducation nationale: voici pourquoi Microsoft est une drogue dure"
author: Louis Adam
date: 2020-09-11
href: https://www.zdnet.fr/actualites/ducation-nationale-voici-pourquoi-microsoft-est-une-drogue-dure-39909447.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2016/11/5009895583_0dbc6bc27e_b.jpg
tags:
- Marchés publics
- Éducation
series:
- 202037
series_weight: 0
---

> L'Union des entreprises du logiciel libre et du numérique ouvert déplore l'ouverture d'un nouveau marché public par l'éducation nationale, qui vise à fournir des licences et services Microsoft aux employés du ministère. La conséquence d'une politique initiée depuis longtemps par les gouvernements successifs.
