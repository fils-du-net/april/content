---
site: Le Monde.fr
title: "La justice européenne consacre «la neutralité du Net» dans l'UE"
date: 2020-09-15
href: https://www.lemonde.fr/pixels/article/2020/09/15/la-justice-europeenne-consacre-la-neutralite-du-net-dans-l-ue_6052269_4408996.html
tags:
- Neutralité du Net
- Europe
series:
- 202038
series_weight: 0
---

> L’arrêt de la Cour de justice de l’Union européenne consacre mardi le principe d’égalité de traitement et d’accès des contenus en ligne.
