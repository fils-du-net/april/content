---
site: Journal du Net
title: "Je ne suis pas une licorne!"
date: 2020-09-03
href: https://www.journaldunet.com/web-tech/start-up/1493655-je-ne-suis-pas-une-licorne
tags:
- Institutions
- Entreprise
- Informatique en nuage
series:
- 202036
series_weight: 0
---

> Le rapport du Sénat est clair: pour préserver notre souveraineté numérique, nous avons besoin d'une licorne! Vraiment? Et si on n'est pas une bête curieuse alors?
