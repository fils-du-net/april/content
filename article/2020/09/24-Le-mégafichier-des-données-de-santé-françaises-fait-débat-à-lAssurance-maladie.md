---
site: NotreTemps.com
title: "Le mégafichier des données de santé françaises fait débat à l'Assurance maladie"
date: 2020-09-24
href: https://www.notretemps.com/high-tech/actualites/le-megafichier-des-donnees-de-sante-afp-202009,i228165
tags:
- Vie privée
series:
- 202039
---

> La controverse sur le mégafichier Health Data Hub s'invite à l'Assurance maladie: saisi pour avis sur un projet de décret, son conseil d'administration a décidé jeudi d'attendre le verdict de la Cnil, non sans appeler à "une solution souveraine" pour l'hébergement des données actuellement confiées à l'américain Microsoft.
