---
site: la Croix
title: "Numérique, le guide pour reprendre le contrôle (€)"
author: Stéphane Bataillon
date: 2020-09-26
href: https://www.la-croix.com/JournalV2/Numerique-guide-reprendre-controle-2020-09-26-1101116086
featured_image: https://img.aws.la-croix.com/2020/09/26/1101116086/Numerique-guidereprendre-controle_0_730_890.jpg
tags:
- Sensibilisation
series:
- 202039
---

> L'exploitation de nos données personnelles et de notre vie privée par les géants du Net ne cesse d'augmenter. Mais un autre numérique est possible, qui propose des solutions respectueuses de nos données personnelles, créées dans un esprit collaboratif et soucieuses de leur impact sur la planète. Ce guide vous invite à les découvrir et à reprendre le contrôle sur ce pan désormais incontournable de notre vie.
