---
site: ZDNet France
title: "Le Health Data Hub de nouveau attaqué devant le Conseil d'Etat"
author: Clarisse Treilles
date: 2020-09-17
href: https://www.zdnet.fr/actualites/le-health-data-hub-de-nouveau-attaque-devant-le-conseil-d-etat-39909723.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2020/01/e-sante.jpg
tags:
- Vie privée
- Entreprise
series:
- 202038
series_weight: 0
---

> Un collectif a déposé un recours en urgence au Conseil d'Etat, qui avait déjà été appelé à trancher une première fois sur le sujet. Les requérants espèrent cette fois-ci faire peser l'invalidité du Privacy Shield.
