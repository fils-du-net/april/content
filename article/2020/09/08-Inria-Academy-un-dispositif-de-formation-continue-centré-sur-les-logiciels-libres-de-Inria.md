---
site: Programmez!
title: "Inria Academy: un dispositif de formation continue centré sur les logiciels libres de Inria"
author: fredericmazue
date: 2020-09-08
href: https://www.programmez.com/actualites/inria-academy-un-dispositif-de-formation-continue-centre-sur-les-logiciels-libres-de-inria-30884
featured_image: https://www.programmez.com/sites/all/themes/programmez/images/logo.png
tags:
- Sensibilisation
series:
- 202037
---

> L'Institut national de recherche en informatique et en automatique (Inria) a lancé un nouveau programme de formation continue, baptisé Inria Academy, La vocation de ce programme de formation est d'aider les développeurs, les ingénieurs et les scientifiques à prendre en main certains des logiciels open source développés par l'Institut.
