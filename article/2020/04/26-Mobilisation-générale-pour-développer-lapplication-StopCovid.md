---
site: Le Figaro.fr
title: "Mobilisation générale pour développer l'application StopCovid"
author: Elsa Bembaron
date: 2020-04-26
href: https://www.lefigaro.fr/secteur/high-tech/mobilisation-generale-pour-developper-l-application-stopcovid-20200426
featured_image: https://i.f1g.fr/media/eidos/767x431_crop/2020/04/26/XVMda0ead4c-87db-11ea-8ae5-850715b5edce.jpg
tags:
- Innovation
- Institutions
- Vie privée
series:
- 202017
series_weight: 0
---

> Les récents avis rendus par la Cnil, le CNNum et le Conseil scientifique ont sonné le top départ pour les équipes de développement d’une application de «contact tracing».
