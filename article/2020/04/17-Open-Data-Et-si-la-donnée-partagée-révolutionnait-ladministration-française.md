---
site: Decideo.fr
title: "Open Data: Et si la donnée partagée révolutionnait l'administration française?"
author: Raphaël Allègre
href: https://www.decideo.fr/Open-Data-Et-si-la-donnee-partagee-revolutionnait-l-administration-francaise_a11837.html
featured_image: https://www.decideo.fr/photo/art/default/44939580-36511733.jpg
tags:
- Open Data
- Administration
series:
- 202016
series_weight: 0
---

> Le partage collaboratif des données en temps réel a permis de comprendre avec plus de précision le monde dans lequel nous évoluons. À l’instar de nombreux acteurs du privé, les institutions françaises se sont elles aussi lancées dans le grand bain de la transformation digitale en utilisant notamment l’Open Data. Malgré son potentiel (accessibilité, utilisation, analyses etc.), ce mouvement soulève également des problématiques d’éthique, d’intégrité et de sécurité.
