---
site: Usbek & Rica
title: "Covid-19: «Les initiatives ouvertes sont en train de se construire une visibilité nationale»"
author: Lila Meghraoua
date: 2020-04-19
href: https://usbeketrica.com/article/covid-19-les-initiatives-ouvertes-sont-en-train-de-se-construire-une-visibilite-nationale
featured_image: https://static.usbeketrica.com/images/thumb_840xh/5e9b7b95e00b4.jpg
tags:
- Matériel libre
- Économie
- Licenses
series:
- 202016
---

> Dans la lutte contre la pandémie, le mouvement maker passe son épreuve du feu avec les industriels. On en parle avec la sociologue Isabelle Berrebi-Hoffmann.
