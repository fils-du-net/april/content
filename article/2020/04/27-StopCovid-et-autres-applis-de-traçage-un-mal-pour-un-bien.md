---
site: Contrepoints
title: "StopCovid et autres applis de traçage: un mal pour un bien?"
author: Marcel Moritz et Audrey Dequesnes
date: 2020-04-27
href: https://www.contrepoints.org/2020/04/27/370065-stopcovid-et-autres-applis-de-tracage-un-mal-pour-un-bien
featured_image: https://www.contrepoints.org/wp-content/themes/cp2016/css/static/CP-logo-desktop-tablette.png
tags:
- Innovation
- Vie privée
series:
- 202018
---

> Nous devons rester vigilants face aux dispositifs de traçage, qui sont loin d'avoir été l'outil le plus efficace pour lutter contre l'épidémie en Asie.
