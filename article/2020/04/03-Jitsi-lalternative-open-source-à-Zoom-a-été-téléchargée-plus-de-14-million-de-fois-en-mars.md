---
site: Clubic.com
title: "Jitsi, l'alternative open-source à Zoom, a été téléchargée plus de 1,4 million de fois en mars"
author: Pierre Crochart
date: 2020-04-03
href: https://www.clubic.com/telecharger/logiciel-messagerie-instantanee/actualite-378-jitsi-l-alternative-open-source-zoom-a-t-t-l-charg-e-plus-de-1-4-million-de-fois-en-mars.html
featured_image: https://pic.clubic.com/v1/images/1786903/raw
tags:
- Sensibilisation
- Vie privée
series:
- 202014
series_weight: 0
---

> Le confinement généralisé de la moitié de la population mondiale ne fait pas les choux gras que de ZOOM. Jitsi Meet, son alternative open-source (et respectueuse de la vie privée) a elle aussi fait un bond de géant le mois dernier.
