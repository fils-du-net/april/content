---
site: InformatiqueNews.fr
title: "ERP et Open Source une équation génératrice de valeur"
author: Mehdi Nagati
date: 2020-04-09
href: https://www.informatiquenews.fr/erp-et-open-source-une-equation-generatrice-de-valeur-mehdi-nagati-smile-69079
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2020/04/photo-Mehdi-CV.png
tags:
- Entreprise
- Logiciels privateurs
- Sensibilisation
series:
- 202015
series_weight: 0
---

> Longtemps l’apanage des PME, l’open source séduit de plus en plus les grands groupes. Plus flexible et paramétrable? Moins coûteux? La réponse n’est pas toujours évidente tant les situations diffèrent. Voici quelques questions qu’il est utile de se poser avant de choisir l’open source pour son projet ERP.
