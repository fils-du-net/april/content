---
site: France Inter
title: "Traçage numérique: Un débat sans vote à l'Assemblée?"
author: Thomas Legrand
date: 2020-04-21
href: https://www.franceinter.fr/emissions/l-edito-politique/l-edito-politique-21-avril-2020
featured_image: https://cdn.radiofrance.fr/s3/cruiser-production/2016/06/23a11d0c-c0ea-497a-8f8b-bb22ab8ff552/1200x680_legrand_thomas.jpg
tags:
- Innovation
- Institutions
- Vie privée
series:
- 202017
---

> Faut-il un vote du parlement lors du débat sur le traçage numérique à l’étude pour aider au dé-confinement?
