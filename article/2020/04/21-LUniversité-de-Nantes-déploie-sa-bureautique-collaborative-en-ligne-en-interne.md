---
site: cio-online.com
title: "L'Université de Nantes déploie sa bureautique collaborative en ligne en interne (€)"
author: Bertrand Lemaire
date: 2020-04-21
href: https://www.cio-online.com/actualites/lire-l-universite-de-nantes-deploie-sa-bureautique-collaborative-en-ligne-en-interne-12136.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000015266.jpg
tags:
- Éducation
series:
- 202017
series_weight: 0
---

> Etudiants, enseignants et administratifs de l'Université de Nantes bénéficient désormais d'un service à base de NextCloud et OnlyOffice dans un environnement totalement en logiciels libres. 16 000 comptes et 110 000 partages sont aujourd'hui actifs. L'université finance des améliorations de la plate-forme en vue des prochaines étapes du projet.
