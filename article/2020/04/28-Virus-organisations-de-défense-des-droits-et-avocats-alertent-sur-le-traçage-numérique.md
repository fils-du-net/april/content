---
site: NotreTemps.com
title: "Virus: organisations de défense des droits et avocats alertent sur le traçage numérique"
date: 2020-04-28
href: https://www.notretemps.com/high-tech/actualites/virus-organisations-de-defense-des-afp-202004,i218233
tags:
- Innovation
- Vie privée
series:
- 202018
series_weight: 0
---

> Le projet controversé de traçage numérique, envisagé par le gouvernement pour lutter contre la pandémie de coronavirus après le confinement, représente une atteinte à la vie privée et aux libertés, alertent mardi des organisations de défense des droits, syndicats et avocats.
