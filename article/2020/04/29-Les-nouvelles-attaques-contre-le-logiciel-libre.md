---
site: LeMagIT
title: "Les nouvelles attaques contre le logiciel libre"
author: Thierry Carrez
date: 2020-04-29
href: https://www.lemagit.fr/tribune/Les-nouvelles-attaques-contre-le-logiciel-libre
featured_image: https://cdn.ttgtmedia.com/visuals/German/article/hidden-costs.jpg
tags:
- Licenses
- Entreprise
- Informatique en nuage
series:
- 202018
series_weight: 0
---

> Le logiciel libre est à nouveau attaqué. Ses adversaires cherchent à le redéfinir à leur profit, notamment à grand renfort de licences restrictives. Ils disposent de beaucoup de moyens. Une communication claire, forte et coordonnée du monde du libre est nécessaire pour le préserver.
