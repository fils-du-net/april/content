---
site: 01net.
title: "Coronavirus: des chercheurs tunisiens conçoivent un respirateur open source à imprimer en 3D"
date: 2020-04-17
href: https://www.01net.com/actualites/coronavirus-des-chercheurs-tunisiens-concoivent-un-respirateur-open-source-a-imprimer-en-3d-1894986.html
featured_image: https://img.bfmtv.com/c/630/420/fba/e826be0d57ba0ac9f24dbe96f03c0.jpeg
tags:
- Matériel libre
- International
series:
- 202016
---

> Face au manque criant de matériel sur le continent africain, un groupe de scientifiques a fabriqué une machine d'assistance respiratoire à partir d'une imprimante 3D. Le mode de fabrication sera disponible en ligne. 
