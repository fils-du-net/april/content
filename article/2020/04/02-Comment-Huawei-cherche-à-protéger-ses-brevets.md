---
site: ZDNet France
title: "Comment Huawei cherche à protéger ses brevets"
author: Steven J. Vaughan-Nichols
date: 2020-04-02
href: https://www.zdnet.fr/actualites/comment-huawei-cherche-a-proteger-ses-brevets-39901685.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/02/Huawei%20Illustration%20D%20620.jpg
tags:
- Brevets logiciels
- Entreprise
series:
- 202014
series_weight: 0
---

> Si certains pourraient trouver cela surprenant, Huawei a fait le choix de rejoindre le Linux and Open Invention Network, le principal groupe américain de défense des brevets et de la protection intellectuelle.
