---
site: LaProvence.com
title: "Coronavirus: feu vert à un essai clinique avec du sang de ver marin pour malades"
date: 2020-04-05
href: https://www.laprovence.com/actu/en-direct/5952989/coronavirus-feu-vert-a-un-essai-clinique-avec-du-sang-de-ver-marin-pour-malades.html
featured_image: https://medias.laprovence.com/4gOmH613DVRckqGwuDb9g6v0fCQ=/0x91:1512x925/850x575/top/smart/3c9e140191f948e18607610755e3cb82/1583949803_vrel1918.jpg
tags:
- Science
series:
- 202014
---

> Un essai clinique consistant à administrer à dix malades du Covid-19 une solution issue du sang d'un ver marin aux propriétés oxygénantes va pouvoir démarrer après l'accord obtenu du Comité de protection des personnes (CPP), ont annoncé samedi les porteurs du projet. Après l'accord de l'ANSM (Agence nationale du médicament et des produits de santé) il y a une semaine, la société bretonne Hemarina, à l'origine du produit, a annoncé à l'AFP avoir obtenu l'indispensable feu vert du CPP pour démarrer ses recherches.
