---
site: L'usine Nouvelle
title: "Les premiers détails de \"Stop Covid\", le projet d'application de tracking du gouvernement"
author: Marion Garreau
date: 2020-04-08
href: https://www.usinenouvelle.com/editorial/covid-19-les-premiers-details-de-stop-covid-le-projet-d-application-de-tracking-du-gouvernement.N951436
featured_image: https://www.usinenouvelle.com/mediatheque/6/0/5/000860506_image_896x598/foule-la-defense.jpg
tags:
- Vie privée
- Innovation
- Institutions
series:
- 202015
series_weight: 0
---

> Le cabinet du secrétaire d‘Etat en charge du numérique Cédric O a dévoilé, mercredi 8 avril, les contours d’un projet baptisé Stop Covid. Piloté en France par l’Inria, il vise à développer une application mobile qui alerte les individus précédemment croisés par une personne infectée.
