---
site: francetv info
title: "“On est en pleine guerre informatique”: après le coronavirus, faut-il s'attendre à une autre crise?"
author: Antoine Marquet
date: 2020-04-26
href: https://france3-regions.francetvinfo.fr/bourgogne-franche-comte/on-est-pleine-guerre-informatique-apres-coronavirus-faut-il-s-attendre-autre-crise-1820354.html
featured_image: https://france3-regions.francetvinfo.fr/bourgogne-franche-comte/sites/regions_france3/files/styles/top_big/public/assets/images/2020/04/24/maxstockworld396674_1-4780604.jpg
tags:
- Internet
- Entreprise
- Logiciels privateurs
series:
- 202017
series_weight: 0
---

> La 'guerre' contre le coronavirus Covid-19 met le système de santé et les institutions à l'épreuve et paralyse l'économie. Mais au-delà de la crise sanitaire, doit-on également craindre une crise informatique? Réponses et réflexions, avec des spécialistes.
