---
site: UP' Magazine
title: "Les communs, pour un autre futur du travail (€)"
author: Grégoire Epitalon
date: 2020-04-21
href: https://up-magazine.info/economie-de-linnovation-4/47894-les-communs-pour-un-autre-futur-du-travail
featured_image: https://up-magazine.info/wp-content/uploads/2020/04/travail-futur-generique1.jpg
tags:
- Économie
series:
- 202017
series_weight: 0
---

> «Rien ne sera plus comme avant.» Cette phrase, elle résonne dans le web depuis quelques semaines. Avec le confinement, un raz-de-marée d’interrogations sur le monde de demain déferle sur nos écrans. Et le futur du travail occupe une place centrale dans ce questionnement (ex. ici et là). En fait, ce «rien ne sera plus comme avant» n’est pas tant une question qu’une affirmation. Affirmation où l’on perçoit à la fois un profond malaise et le souhait d’un avenir un peu meilleur. Et si on pouvait construire un autre futur du travail, grâce aux communs?
