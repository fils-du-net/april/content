---
site: francetv info
title: "Nouveau monde. Bientôt une application pour traquer le coronavirus tout en respectant la vie privée"
author: Jérôme Colombain
date: 2020-04-07
href: https://www.francetvinfo.fr/sante/maladie/coronavirus/nouveau-monde-bientot-une-application-pour-traquer-le-coronavirus-tout-en-respectant-la-vie-privee_3881607.html
featured_image: https://www.francetvinfo.fr/image/75rxss0hu-d421/1500/843/21296959.jpg
tags:
- Vie privée
series:
- 202015
---

> Un groupe international d'experts lance cette semaine une plateforme visant à développer des applications mobiles pour éviter la propagation du virus sans nuire à la vie privée.
