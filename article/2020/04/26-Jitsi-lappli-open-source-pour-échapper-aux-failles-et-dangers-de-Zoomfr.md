---
site: atlantico
title: "Jitsi, l'appli open source pour échapper aux failles et dangers de Zoom"
date: 2020-04-26
href: https://www.atlantico.fr/decryptage/3589064/jitsi-l-appli-open-source-pour-echapper-aux-failles-et-dangers-de-zoom-visioconference-teletravail-confinement-coronavirus-piratage-donnees-personnelles-telegram-whatsapp-serveurs-adrien-serres
featured_image: https://www.atlantico.fr/sites/default/files/styles/media_image_960x533/public/jitsi.JPG
seeAlso: "[Jitsi – Framasoft – Éducation – Le confinement - «Libre à vous!»](https://april.org/61-jitsi)"
tags:
- Sensibilisation
- Innovation
- Logiciels privateurs
series:
- 202017
series_weight: 0
---

> Jitsi est un service de visioconférence gratuit et open source, lancé en 2013. Il rencontre un franc succès alors que l'application Zoom a été récemment confrontée à des difficultés liées à des piratages et sur la gestion des données personnelles.
