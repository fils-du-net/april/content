---
site: Numerama
title: "Le confinement pousse l'Éducation nationale à regarder vers le logiciel libre"
author: Julien Lausson
date: 2020-04-30
href: https://www.numerama.com/tech/621660-le-confinement-pousse-leducation-nationale-a-regarder-vers-le-logiciel-libre.html
featured_image: https://c2.lestechnophiles.com/www.numerama.com/content/uploads/2020/04/apps-gouv-fr.jpg?resize=1212,712
tags:
- Éducation
- Logiciels privateurs
series:
- 202018
series_weight: 0
---

> C'est l'un des effets inattendus de la crise sanitaire. Alors que le confinement dure depuis six semaines, le ministère de l'Éducation nationale explore l'utilisation de logiciels libres pour aider son personnel.
