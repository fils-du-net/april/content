---
site: Le Figaro.fr
title: "«Le traçage numérique nous permettra de donner de la visibilité à un ennemi invisible»"
author: Philippe Latombe
date: 2020-04-09
href: https://www.lefigaro.fr/vox/politique/le-tracage-numerique-nous-permettra-de-donner-de-la-visibilite-a-un-ennemi-invisible-20200409
featured_image: https://i.f1g.fr/media/eidos/767x431_crop/2020/04/09/XVMd9b05882-7a43-11ea-a7cf-1493f1be4cb1.jpg
tags:
- Vie privée
series:
- 202015
---

> Le traçage numérique sur la base du volontariat proposé par le gouvernement est une idée séduisante pour organiser un déconfinement réussi, argumente le député Philippe Latombe. Il rappelle néanmoins que la protection des données personnelles n’est pas négociable et invite à la collaboration intracommunautaire.
