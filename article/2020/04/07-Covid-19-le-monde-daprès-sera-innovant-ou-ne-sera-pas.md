---
site: La Tribune
title: "Covid-19: le monde d'après sera innovant ou ne sera pas"
author: David Menga et Xavier Dalloz
date: 2020-04-07
href: https://www.latribune.fr/opinions/tribunes/covid-19-le-monde-d-apres-sera-innovant-ou-ne-sera-pas-844559.html
featured_image: https://static.latribune.fr/full_width/1407420/3d.jpg
tags:
- Innovation
series:
- 202015
series_weight: 0
---

> Alors que le COVID-19 se propage dans le monde entier, elle oblige l'humanité à innover, c'est-à-dire à faire plus, mieux, à moindre coûts, de meilleure qualité avec les mêmes ressources.
