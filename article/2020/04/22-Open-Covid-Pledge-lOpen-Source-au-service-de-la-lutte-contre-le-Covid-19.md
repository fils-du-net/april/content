---
site: FZN
title: "Open Covid Pledge: l'Open Source au service de la lutte contre le Covid-19"
author: Micka
date: 2020-04-22
href: https://www.fredzone.org/open-covid-pledge-lopen-source-au-service-de-la-lutte-contre-le-covid-19-255
featured_image: https://www.fredzone.org/wp-content/uploads/2020/03/bing-coronavirus.png
tags:
- Licenses
- Innovation
- Entreprise
series:
- 202017
series_weight: 0
---

> Le monde entier se mobilise pour faire face au Covid-19. De nombreuses initiatives émergent à travers le globe pour aider la population mondiale à lutter contre ce fléau. Il y a quelques jours, le site Motherboard a rapporté l’existence d’un nouveau projet lancé par un groupe de scientifiques, de juristes et d’entrepreneurs venus des quatre coins du monde.
