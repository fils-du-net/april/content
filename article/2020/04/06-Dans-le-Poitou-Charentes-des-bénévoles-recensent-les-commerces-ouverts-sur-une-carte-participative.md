---
site: france bleu
title: "Dans le Poitou-Charentes, des bénévoles recensent les commerces ouverts sur une carte participative"
date: 2020-04-06
href: https://www.francebleu.fr/infos/societe/dans-le-poitou-charentes-des-benevoles-recensent-la-liste-des-commerces-ouverts-sur-une-carte-1585674031
featured_image: https://cdn.radiofrance.fr/s3/cruiser-production/2020/03/f3afde49-7416-4784-a8f5-7196bffae29e/870x489_carte_niort.webp
tags:
- Associations
- Partage du savoir
- Internet
series:
- 202015
series_weight: 0
---

> Pour éviter les déplacements inutiles, des cartographes bénévoles ont créé 'Ça reste ouvert', une carte collaborative où tout le monde peut indiquer l'ouverture ou la fermeture d'un commerce, ainsi que ses horaires. Des passionnés dans le Poitou-Charentes nous expliquent.
