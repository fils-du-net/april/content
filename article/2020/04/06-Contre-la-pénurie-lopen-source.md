---
site: Le Courrier
title: "Contre la pénurie, l'open source"
author: Julie Jeannet
date: 2020-04-06
href: https://lecourrier.ch/2020/04/06/contre-la-penurie-lopen-source
featured_image: https://lecourrier.ch/app/uploads/2020/04/contre-la-penurie-lopen-source-936x546.jpg
tags:
- Matériel libre
series:
- 202015
---

> Pour pallier les problèmes d’approvisionnement, divers dispositifs médicaux sont produits avec des imprimantes 3D, sur la base de plans de conception libres partagés sur Internet.
