---
site: usine-digitale.fr
title: "Covid-19: Bluetooth, serveur central, logiciel libre… On en sait un peu plus sur StopCovid"
author: Alice Vitard
date: 2020-04-20
href: https://www.usine-digitale.fr/article/covid-19-bluetooth-serveur-central-logiciel-libre-on-en-sait-un-peu-plus-sur-stopcovid.N955196
featured_image: https://www.usine-digitale.fr/mediatheque/1/2/3/000863321_homePageUne/carte-contaminations-covid-19.jpg
tags:
- Innovation
series:
- 202017
---

> Dans une note publiée le 18 avril, le PDG de l'Inria, Bruno Sportisse, dévoile de nouvelles informations sur le protocole qui sera à l'origine de l'application StopCovid. Il reposera bien sur le volontariat et sur l'utilisation du Bluetooth plutôt que les données de géolocalisation. Mais le dirigeant veut être clair : rien n'est encore définitif.
