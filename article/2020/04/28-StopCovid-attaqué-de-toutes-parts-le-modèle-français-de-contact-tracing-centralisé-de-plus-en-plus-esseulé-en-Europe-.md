---
site: Industrie et Technologies
title: "StopCovid: attaqué de toutes parts, le modèle français de contact tracing centralisé de plus en plus esseulé en Europe "
author: Kevin Poireault
date: 2020-04-28
href: https://www.industrie-techno.com/article/stopcovid-attaque-de-toutes-parts-le-modele-francais-de-contact-tracing-centralise-de-plus-en-plus-esseule-en-europe.60261
featured_image: https://www.industrie-techno.com/mediatheque/1/8/3/000047381_600x400_c.png
tags:
- Innovation
- Vie privée
series:
- 202018
---

> En France, les contours de StopCovid, la future application de suivi des personnes diagnostiquées positives au Covid-19, commencent à se dessiner. Et font pleuvoir les critiques. A l’heure où le Premier ministre Edouard Philippe s’apprête à dévoiler son plan de déconfinement, la France est le seul pays de l’UE à se diriger vers un modèle centralisé.
