---
site: Next INpact
title: "StopCovid: Cédric O saisit le Conseil national du Numérique"
author: Marc Rees
date: 2020-04-17
href: https://www.nextinpact.com/news/108910-stopcovid-cedric-o-saisit-conseil-national-numerique.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/25173.jpg
tags:
- Innovation
- Institutions
- Vie privée
series:
- 202016
series_weight: 0
---

> Cédric O a saisi ce 14 avril le Conseil national du numérique sur le projet StopCovid, l’application de suivi de contact. Next INpact diffuse la lettre de saisine.
