---
site: Next INpact
title: "Parcoursup: consécration du droit constitutionnel d'accès aux documents administratifs"
author: Marc Rees
date: 2020-04-03
href: https://www.nextinpact.com/news/108861-parcoursup-consecration-droit-constitutionnel-dacces-aux-documents-administratifs.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/6634.jpg
tags:
- Open Data
- Institutions
- Éducation
series:
- 202014
series_weight: 0
---

> Le Conseil constitutionnel consacre l'existence d'un droit constitutionnel à l'accès aux documents administratifs. Il considère en outre que les établissements d'enseignement supérieur sont tenus de rendre compte des critères en fonction desquels ont été examinées les candidatures Parcoursup. Explications.
