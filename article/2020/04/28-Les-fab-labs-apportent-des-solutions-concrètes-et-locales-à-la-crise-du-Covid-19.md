---
site: The Conversation
title: "Les fab labs apportent des solutions concrètes et locales à la crise du Covid-19"
author: Kevin Lhoste
date: 2020-04-28
href: https://theconversation.com/les-fab-labs-apportent-des-solutions-concretes-et-locales-a-la-crise-du-covid-19-136277
featured_image: https://images.theconversation.com/files/330793/original/file-20200427-145513-g981zz.jpg
tags:
- Matériel libre
- Innovation
series:
- 202018
series_weight: 0
---

> Les fab lab sont équipés pour concevoir de nouvelles technologies et les prototyper rapidement – en attendant le passage à l'échelle industrielle, ils équipent soignants et patients.
