---
site: francetv info
title: "'Internet peut redevenir un bien commun si nous nous emparons des outils alternatifs aux Gafam': entretien avec Maxime Guedj, co-auteur de 'Déclic'"
author: Laure Narlian
date: 2020-04-18
href: https://www.francetvinfo.fr/culture/livres/internet-peut-redevenir-un-bien-commun-si-nous-nous-emparons-des-outils-alternatifs-aux-gafam-entretien-avec-maxime-guedj-co-auteur-de-declic_3919083.html
featured_image: https://www.francetvinfo.fr/image/75rxysn52-9ec1/1500/843/21352751.jpg
tags:
- Internet
- Associations
series:
- 202016
series_weight: 0
---

> Les Gafam, ces géants d'internet, n'ont jamais eu autant de pouvoir. Comment résister à leur emprise? Une profusion d'outils alternatifs et de solutions respectueuses existent. Le confinement est le bon moment pour les découvrir.
