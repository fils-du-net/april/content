---
site: Medias24
title: "Covid-19: L'application de tracking des Marocains doit être open source"
author: Omar El Hyani
date: 2020-04-16
href: https://www.medias24.com/covid-19-l-application-de-tracking-des-marocains-doit-etre-open-source-9498.html
featured_image: https://www.medias24.com//photos_articles/big/16-04-2020/controlecoronavirusmap.jpg
tags:
- Innovation
- Open Data
series:
- 202016
---

> TRIBUNE. L'élu FGD et par ailleurs ingénieur informatique Omar El Hyani appelle à publier le code source de la future application marocaine de tracking des contaminations, faute de quoi elle susciterait la méfiance au lieu de l'adhésion du public.
