---
site: La Libre
title: "“L'application de tracking du gouvernement doit être Open Source ou ne sera pas!”"
date: 2020-04-23
href: https://www.lalibre.be/economie/decideurs-chroniqueurs/l-application-de-tracking-du-gouvernement-doit-etre-open-source-ou-ne-sera-pas-5ea16ee19978e21833bf171f
featured_image: https://t2.llb.be/RS4hth5cBaHcFJ-FT2kLs7glBsQ=/0x136:2560x1416/1280x640/5ea172f29978e21833bf5131.jpg
tags:
- Innovation
series:
- 202017
---

> Une chronique de Stoomlink, start-up bruxelloise spécialisée dans la mobilité et l’open-source. Avec le soutien de La Mouette, association francophone promouvant LibreOffice (projet de suite bureautique libre) et le format ODF (Open Document Format, seul format de document bureautique ouvert, libre et stable de version en version), et Open Knowledge Belgium, association à but non lucratif promouvant la culture libre, en particulier les contenus libres et l'open data.
