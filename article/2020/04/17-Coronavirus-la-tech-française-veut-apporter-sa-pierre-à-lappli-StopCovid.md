---
site: Les Echos
title: "Coronavirus: la tech française veut apporter sa pierre à l'appli StopCovid"
author: Florian Dèbes, Raphaël Balenieri
date: 2020-04-17
href: https://www.lesechos.fr/tech-medias/hightech/coronavirus-la-tech-francaise-veut-apporter-sa-pierre-a-lappli-stopcovid-1195199
featured_image: https://media.lesechos.com/api/v1/images/view/5e9738768fe56f60e3141167/1280x720/0603097055628-web-tete.jpg
tags:
- Innovation
series:
- 202016
---

> Parallèlement au projet officiel porté par l'Inria, six grands groupes français dont Orange, Dassault et Capgemini sont prêts à offrir leurs briques technologiques. De Londres à San Francisco, des start-up françaises ne sont pas en reste.
