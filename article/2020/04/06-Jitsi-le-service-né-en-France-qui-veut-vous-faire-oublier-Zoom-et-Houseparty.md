---
site: Le Point
title: "Jitsi, le service né en France qui veut vous faire oublier Zoom et Houseparty"
author: Guillaume Grallet
date: 2020-04-06
href: https://www.lepoint.fr/high-tech-internet/jitsi-le-service-ne-en-france-qui-veut-vous-faire-oublier-zoom-et-houseparty-06-04-2020-2370280_47.php
featured_image: https://static.lpnt.fr/images/2020/04/06/20224578lpw-20224656-article-jpg_7028011_660x281.jpg
seeAlso: "[Jitsi – Framasoft – Éducation – Le confinement - «Libre à vous!»](https://april.org/61-jitsi)"
tags:
- Sensibilisation
series:
- 202015
series_weight: 0
---

> Confinement oblige, ce service de visioconférence gratuit et open source, lancé en 2013, rencontre un vif succès, avec la bénédiction d'Edward Snowden.
