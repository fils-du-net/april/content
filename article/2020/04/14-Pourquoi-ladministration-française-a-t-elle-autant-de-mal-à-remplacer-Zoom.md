---
site: Numerama
title: "Pourquoi l'administration française a-t-elle autant de mal à remplacer Zoom?"
author: François Manens
date: 2020-04-14
href: https://cyberguerre.numerama.com/4405-pourquoi-ladministration-francaise-a-t-elle-autant-de-mal-a-remplacer-zoom.html
tags:
- Logiciels privateurs
- Sensibilisation
- Administration
series:
- 202016
series_weight: 0
---

> Zoom a tellement de problèmes de sécurité que les institutions déconseillent son usage. Mais la qualité de son expérience utilisateur le rend très compliqué à remplacer.
