---
site: The Conversation
title: "Comment le coronavirus a réveillé l'intelligence collective mondiale"
author: Marc Santolini
date: 2020-04-05
href: https://theconversation.com/comment-le-coronavirus-a-reveille-lintelligence-collective-mondiale-135465
featured_image: https://images.theconversation.com/files/324961/original/file-20200402-74900-45jt1r.png
tags:
- Science
series:
- 202014
series_weight: 0
---

> Individuellement, nous sommes tous démunis devant la crise du coronavirus. Un boom collaboratif mondial est en train de changer la manière dont la science se fait.
