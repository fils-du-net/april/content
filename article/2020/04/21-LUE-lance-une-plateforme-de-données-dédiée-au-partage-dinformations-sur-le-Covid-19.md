---
site: Siècle Digital
title: "L'UE lance une plateforme de données dédiée au partage d'informations sur le Covid-19"
author: Valentin Cimino
date: 2020-04-21
href: https://siecledigital.fr/2020/04/21/lue-lance-une-plateforme-de-donnees-pour-soutenir-la-recherche-sur-le-covid-19
featured_image: https://thumbor.sd-cdn.fr/19mKaONvmpkrWHVKI-YNjTx60Vc=/940x550/cdn.sd-cdn.fr/wp-content/uploads/2020/03/epidemie-covid-19-IBM.jpg
tags:
- Sciences
- Europe
- Open Data
series:
- 202017
series_weight: 0
---

> La Commission européenne a annoncé le 20 avril qu'une plateforme dédiée au partage des données sur le Covid-19 était disponible.
