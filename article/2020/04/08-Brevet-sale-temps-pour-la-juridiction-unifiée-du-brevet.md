---
site: Dalloz
title: "Brevet: sale temps pour la juridiction unifiée du brevet"
author: Cédric Meiller et Vincent Chapuis
date: 2020-04-08
href: https://www.dalloz-actualite.fr/flash/brevet-sale-temps-pour-juridiction-unifiee-du-brevet
seeAlso: "[Brevet unitaire, quand le château de cartes fini enfin de s'écrouler](https://www.april.org/brevet-unitaire-quand-le-chateau-de-cartes-fini-enfin-de-s-ecrouler)"
tags:
- Brevets logiciels
- Institutions
- Europe
series:
- 202015
series_weight: 0
---

> Après l’annulation, le 20 mars, par la Cour constitutionnelle fédérale allemande de la loi ratifiant l’Accord sur la juridiction unifiée du brevet (JUB), un retour sur la construction de cette juridiction ainsi qu’un point sur ses perspectives s’imposent.
