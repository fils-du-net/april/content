---
site: Marianne
title: "\"Il faut d'abord regarder si ça marche\": la Macronie en plein brouillard sur le traçage numérique du coronavirus"
author: Louis Hausalter
date: 2020-04-08
href: https://www.marianne.net/politique/il-faut-d-abord-regarder-si-ca-marche-la-macronie-en-plein-brouillard-sur-le-tracage
featured_image: https://media.marianne.net/sites/default/files/styles/mrn_article_large/public/macron-tracking-coronavirus.jpg
tags:
- Vie privée
series:
- 202015
---

> D'abord opposé au traçage des contaminations via une application mobile, le pouvoir a fait volte-face et prépare les esprits. Mais sur ce sujet touchant aux libertés publiques, Emmanuel Macron marche sur des œufs.
