---
site: Usbek & Rica
title: "Pourquoi le Covid-19 va (aussi) propager le mouvement maker"
author: Bastien Marchand
date: 2020-04-09
href: https://usbeketrica.com/article/pourquoi-le-covid-19-va-aussi-propager-le-mouvement-maker
featured_image: https://static.usbeketrica.com/images/thumb_840xh/5e8ef97f80711.jpg
tags:
- Matériel libre
- Économie
series:
- 202015
series_weight: 0
---

> Face à la crise sanitaire, nos dirigeants, même les plus libéraux, brandissent des solutions économiques qui dans «le monde d'avant» auraient été écartées d'un revers de main ou bien n’avaient pas tenu leurs promesses: open source, impressions 3D, revenu universel, relocalisation de la production… Passage en revue des solutions maker à qui la situation offre une seconde chance.
