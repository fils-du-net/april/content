---
site: Alternatives Economiques
title: "Appli Stop Covid: une question de confiance"
author: David Chopin, Alvaro Pina Stranger, Aurélien Romano et Julien Mendoza
date: 2020-04-16
href: https://www.alternatives-economiques.fr/appli-stop-covid-une-question-de-confiance/00092461
featured_image: https://www.alternatives-economiques.fr/sites/default/files/public/styles/ae-169-custom_user_large_1x/public/field/image/rea_285928_202.jpg
tags:
- Innovation
- Open Data
series:
- 202020
---

> Pour respecter les libertés individuelles, les applications mobiles permettant d'identifier les personnes infectées doivent répondre à plusieurs conditions, expliquent les auteurs de cette tribune.
