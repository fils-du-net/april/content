---
site: Arcinfo
title: "L'Université de Neuchâtel souhaite donner des armes juridiques à l’open source matériel"
author: Loïc Marchand
date: 2020-04-01
href: https://www.arcinfo.ch/articles/regions/canton/l-universite-de-neuchatel-souhaite-donner-des-armes-juridiques-a-l-open-source-materiel-925158
featured_image: https://d1ck6dlp32n1hb.cloudfront.net/media/image/95/normal_16_9/fablab-1.jpg
tags:
- Matériel libre
- Licenses
series:
- 202014
series_weight: 0
---

> Des chercheurs de l’Université de Neuchâtel lancent une étude afin de poser les bases juridiques profitables au développement de l’open source matériel (hardware) en Suisse. Ce projet de trois ans est soutenu par le Fonds national suisse à hauteur de 210 000 francs.
