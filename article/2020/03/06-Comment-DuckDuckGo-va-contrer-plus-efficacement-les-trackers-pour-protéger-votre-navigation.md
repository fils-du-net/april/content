---
site: Numerama
title: "Comment DuckDuckGo va contrer plus efficacement les trackers pour protéger votre navigation"
author: Julien Lausson
date: 2020-03-06
href: https://www.numerama.com/tech/610087-comment-duckduckgo-va-contrer-plus-efficacement-les-trackers-pour-proteger-votre-navigation.html
featured_image: https://c2.lestechnophiles.com/www.numerama.com/content/uploads/2020/03/duckduckgo-tracker-radar.jpg
tags:
- Vie privée
- Entreprise
series:
- 202010
series_weight: 0
---

> DuckDuckGo annonce Tracker Radar, un projet qui vise à améliorer le suivi et la lutte des outils de pistage sur le web. L'outil, gratuit et open source, doit corriger certaines insuffisances des listes anti-suivi déjà existantes.
