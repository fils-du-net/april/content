---
site: Heidi.news
title: "Les biohackers se mobilisent contre le coronavirus"
author: Fabrice Delaye
date: 2020-03-16
href: https://www.heidi.news/articles/les-biohackers-se-mobilisent-contre-le-coronavirus
featured_image: https://heidi-f385.kxcdn.com/photos/ab074f9f-f985-44c4-9b81-6aecd82fa7fd/large
tags:
- Sciences
- Partage du savoir
series:
- 202012
series_weight: 0
---

> En 13 jours, l’OpenCovid19 Initiative, lancée par des militants de l’open science est parvenue à fédérer plus de mille partenaires tels que des biohackers (des biologistes travaillant en dehors des institutions) mais aussi des académiques, des start-up et des laboratoires communautaires autour d’un projet de science citoyenne contre l’épidémie. Porté par la plateforme en ligne Just One Giant Lab (JOGL), le projet vise à développer des protocoles de diagnostic de Covid-19 en open source afin que les pays pauvres et les petits laboratoires puissent y avoir accès. Il s’accompagne d’un projet de science participative (ouvert à tous) destiné à suivre la propagation du virus dans l’environnement.
