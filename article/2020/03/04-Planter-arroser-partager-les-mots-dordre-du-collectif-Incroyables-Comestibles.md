---
site: Ville Intelligente
title: "Planter, arroser, partager, les mots d'ordre du collectif «Incroyables Comestibles»"
author: Yannick Sourisseau
date: 2020-03-04
href: https://www.villeintelligente-mag.fr/Planter-arroser-partager-les-mots-d-ordre-du-collectif-Incroyables-Comestibles_a848.html
featured_image: https://www.villeintelligente-mag.fr/photo/art/default/43348630-35802189.jpg?v=1583337440
tags:
- Partage du savoir
series:
- 202010
series_weight: 0
---

> Investir les terrains inutilisés en ville, de la pelouse abandonnée, au rond-poind, en passant par les friches urbaines, l’objectif des membres des Incroyables Comestibles est de transformer l’espace urbain en potager géant et totalement gratuit. Et ça marche …
