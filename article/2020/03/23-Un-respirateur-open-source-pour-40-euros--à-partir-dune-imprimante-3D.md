---
site: European Scientist
title: "Un respirateur open-source pour 40 euros …. à partir d'une imprimante 3D"
date: 2020-03-23
href: https://www.europeanscientist.com/fr/non-classifiee/un-respirateur-open-source-pour-40-euros-a-partir-dune-imprimante-3d
featured_image: https://www.europeanscientist.com/wp-content/uploads/thumbs/vent-3aki7lej3usc3a2pwjoqo0.png
tags:
- Matériel libre
series:
- 202013
---

> Ventilaid est un projet insolite venu tout droit de Pologne et qui tombe à pic au moment de la crise du COVID19: une équipe d’ingénieurs polonais a développé et mis à disposition gratuitement sur Internet un projet de ventilation pouvant être imprimé sur une imprimante 3D pour la modique somme de 40 euros
