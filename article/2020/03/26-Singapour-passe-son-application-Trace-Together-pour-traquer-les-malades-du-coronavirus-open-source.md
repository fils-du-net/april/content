---
site: 01net.
author: Geoffroy Ondet
title: "Singapour passe son application Trace Together, pour traquer les malades du coronavirus, open source"
date: 2020-03-26
href: https://www.01net.com/actualites/singapour-passe-son-application-trace-together-pour-traquer-les-malades-du-coronavirus-open-source-1882479.html
featured_image: https://img.bfmtv.com/c/630/420/e49/39ecda52d45e3e4c70b98001eb8b6.jpg
tags:
- Innovation
series:
- 202013
---

> Le gouvernement singapourien met à disposition le code source de son application permettant de pister les malades du coronavirus. Elle espère aider les autres pays dans leur combat contre la pandémie.
