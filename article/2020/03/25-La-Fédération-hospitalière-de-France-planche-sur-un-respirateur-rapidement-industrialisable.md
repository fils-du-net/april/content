---
site: Industrie et Technologies
title: "La Fédération hospitalière de France planche sur un respirateur rapidement industrialisable"
description: "annonce Enguerrand Habran, son directeur de l'innovation"
author: Kevin Poireault
date: 2020-03-25
href: https://www.industrie-techno.com/article/la-federation-hospitaliere-de-france-planche-sur-un-respirateur-rapidement-industrialisable-annonce-enguerrand-habran-son-directeur-de-l-innovation.59701
featured_image: https://www.industrie-techno.com/mediatheque/6/8/5/000046586_600x400_c.jpg
tags:
- Matériel libre
- Innovation
series:
- 202013
series_weight: 0
---

> Face à la pénurie de respirateurs artificiels qui guette les hôpitaux français, le Fonds de dotation de la Fédération hospitalière de France pour la recherche et l'innovation élabore un modèle open source à partir de systèmes de traitement de l'air utilisé dans les salles blanches. Selon Enguerrand Habran, son directeur de l'innovation, un prototype devrait voir le jour cette semaine et Airbus est intéressé pour commencer rapidement l'industrialisation.
