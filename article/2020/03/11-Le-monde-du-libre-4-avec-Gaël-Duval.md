---
site: Numericatous
title: "Le monde du libre 4, avec Gaël Duval"
author: Numericatous
date: 2020-03-11
href: https://numericatous.fr/2020/03/le-monde-du-libre-4
featured_image: https://numericatous.fr/wp-content/uploads/2020/03/home-main-phone.png
tags:
- Sensibilisation
- Vie privée
series:
- 202011
---

> Nouvelle interview pour cette fois-ci découvrir le projet /e/ pour libérer nos smartphones.
