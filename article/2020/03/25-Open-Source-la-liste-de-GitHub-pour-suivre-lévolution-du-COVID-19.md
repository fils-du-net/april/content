---
site: Silicon
title: "Open Source: la liste de GitHub pour suivre l'évolution du COVID-19"
author: Ariane Beky
date: 2020-03-25
href: https://www.silicon.fr/open-source-github-suivre-evolution-covid-19-336904.html
featured_image: https://www.silicon.fr/wp-content/uploads/2019/01/GitHub.png
tags:
- Innovation
series:
- 202013
series_weight: 0
---

> D'une carte de la JHU au calcul distribué de Folding@home, GitHub recense des projets open source de suivi du coronavirus COVID-19.
