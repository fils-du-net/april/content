---
site: InformatiqueNews.fr
title: "Les chercheurs du MIT publient en open source le plan d'un respirateur artificiel"
date: 2020-03-26
href: https://www.informatiquenews.fr/les-chercheurs-du-mit-publient-en-open-source-le-plan-dun-respirateur-artificiel-68595
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2020/03/mit-respirateur-proto-768x607.jpg
tags:
- Matériel libre
series:
- 202013
---

> Pour aider à lutter contre la pandémie de coronavirus et le manque d’équipements des hôpitaux américains comme à l’échelon mondial, les chercheurs de la célèbre université technologique du Massachusetts (MIT) ont publié les plans en open source d’un respirateur qu’ils viennent également de soumettre à l’approbation rapide de la FDA (Food & Drug Administration).
