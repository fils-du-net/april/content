---
site: FHH Journal
title: "Un mouvement en «open source» pour redynamiser l'horlogerie"
author: Fabrice Eschmann
date: 2020-03-04
href: https://journal.hautehorlogerie.org/fr/un-mouvement-en-open-source-pour-redynamiser-lhorlogerie
featured_image: https://journal.hautehorlogerie.org/wp-content/uploads/2020/03/Calibre-OM10-c-430x290.jpg
tags:
- Matériel libre
series:
- 202010
series_weight: 0
---

> L’association Openmovement travaille au développement d’un mouvement libre de droits. Pour la première fois dans l’horlogerie, les plans du calibre seront accessibles par les acquéreurs. Qui pourront librement le modifier ou l’améliorer.
