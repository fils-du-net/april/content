---
site: La Gazette de la Défense 
title: "À l'Electrolab, les membres s'occupent aussi de satellites"
date: 2020-03-12
href: https://lagazette-ladefense.fr/2020/03/12/a-lelectrolab-les-membres-soccupent-aussi-de-satellites
featured_image: https://lagazette-ladefense.fr/wp-content/uploads/2020/03/1280-Station-Radioamateur-Electrolab.jpg
tags:
- Matériel libre
- Associations
- Sciences
series:
- 202011
series_weight: 0
---

> Le «hackerspace» nanterrien a développé une branche radioamateur qui lui permet de fabriquer du matériel adapté à la construction et la maintenance de satellites amateurs.
