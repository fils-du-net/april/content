---
site: Next INpact
title: "Health Data Hub: un collectif critique le choix Microsoft (€)"
author: Marc Rees
date: 2020-03-13
href: https://www.nextinpact.com/news/108781-health-data-hub-collectif-critique-choix-microsoft.htm
featured_image: https://cdn2.nextinpact.com/compress/900-435/images/bd/wide-linked-media/23340.jpg
tags:
- Marchés publics
- Vie privée
series:
- 202011
series_weight: 0
---

> Un collectif d’entreprises et d’associations «œuvrant dans le monde des logiciels libres de l’open source et des données ouvertes» annonce avoir adressé une lettre au ministre de la Santé. En ligne de mire? Le Health Data Hub et ses liens avec Microsoft.
