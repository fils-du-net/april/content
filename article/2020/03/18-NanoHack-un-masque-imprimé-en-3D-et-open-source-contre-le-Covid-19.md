---
site: 3Dnatives
title: "NanoHack, un masque imprimé en 3D et open-source contre le Covid-19"
author: Mélanie R.
date: 2020-03-18
href: https://www.3dnatives.com/nanohack-masque-covid-19-180320203
featured_image: https://www.3dnatives.com/wp-content/uploads/masque_cover.jpg
tags:
- Science
- Matériel libre
series:
- 202012
---

> Copper3D a conçu NanoHack, un masque open-source imprimé en 3D pour se protéger contre le Covid-19. Découvrez les caractéristiques de NanoHack!
