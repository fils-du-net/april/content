---
site: usine-digitale.fr
title: "L'impression 3D au secours d'un hôpital italien en pénurie de valves respiratoires"
author: Alice Vitard 
date: 2020-03-19
href: https://www.usine-digitale.fr/article/l-impression-3d-au-secours-d-un-hopital-italien-en-penurie-de-valves-respiratoires.N942861
featured_image: https://www.usine-digitale.fr/mediatheque/1/6/1/000854161_homePageUne/valves-respiratoires-imprimees-3d.jpg
tags:
- Science
- Matériel libre
series:
- 202012
---

> La start-up italienne Isinnova a imprimé des valves respiratoires pour aider l'hôpital de Chiari, dans le Nord de l'Italie. Sans cet élément, impossible de connecter les masques à oxygène aux respirateurs qui servent à maintenir en vie les patients les plus gravement atteints de Covid-19. La jeune pousse en a imprimé une centaine en 24 heures pour moins d'un euro pièce.
