---
site: ZDNet France
title: "Linux/Open source: le point sur les événements annulés"
author: Steven J. Vaughan-Nichols
date: 2020-03-10
href: https://www.zdnet.fr/actualites/linux-open-source-le-point-sur-les-evenements-annules-39900371.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2020/03/Linux.jpg
tags:
- Associations
series:
- 202011
series_weight: 0
---

> L'épidémie de coronavirus a décimé la plupart des événements tech de ces dernières semaines, et ceux à venir sont généralement annulés. Le point sur les événements concernant Linux et l'open source.
