---
site: korii.
title: "Wikipédia gagne la guerre contre la désinformation sur le Covid-19"
author: Antoine Hasday
date: 2020-03-17
href: https://korii.slate.fr/et-caetera/moderation-comment-wikipedia-gagne-guerre-desinformation-covid-19-coronavirus
featured_image: https://korii.slate.fr/sites/default/files/styles/1440x600/public/taras-chernus-2ugptycirpo-unsplash.jpg
tags:
- Internet
- Partage du savoir
- Désinformation
series:
- 202012
series_weight: 0
---

> Des spécialistes de la santé veillent au grain. Les GAFAM pourraient prendre exemple.
