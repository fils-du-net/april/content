---
site: leParisien.fr
title: "Coronavirus: quand les inventeurs viennent à la rescousse des hôpitaux"
author: David Opoczynski
date: 2020-03-24
href: http://www.leparisien.fr/societe/coronavirus-quand-les-inventeurs-viennent-a-la-rescousse-des-hopitaux-23-03-2020-8286186.php
featured_image: http://www.leparisien.fr/resizer/Ssh_uFvP1DLuNz9efTWKgeTNBP8=/300x190/arc-anglerfish-eu-central-1-prod-leparisien.s3.amazonaws.com/public/SNOMYSOGEKBENPUZGIAUWCTN7I.jpg
tags:
- Matériel libre
series:
- 202013
---

> Pour aider les soignants souvent en manque d'équipements, des start-ups en tout genre multiplient les solutions innovantes à l'aide des nouvelles technologies, notamment des imprimantes 3D.
