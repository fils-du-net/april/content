---
site: Wired
title: "Open Source Fonts Are Love Letters to the Design Community"
date: 2020-03-28
href: https://www.wired.com/story/open-source-fonts-love-letters-design-community
tags:
- Internet
- Partage du savoir
series:
- 202013
series_weight: 0
---

> Typefaces that can be freely used and modified give others a chance to hone their craft—and share valuable feedback.
