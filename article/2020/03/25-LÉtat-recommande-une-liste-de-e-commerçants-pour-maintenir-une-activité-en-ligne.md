---
site: Clubic.com
title: "L'État recommande une liste de e-commerçants pour maintenir une activité en ligne"
author: Alexandre Boero
date: 2020-03-25
href: https://www.clubic.com/pro/e-commerce/actualite-889696-tat-recommande-liste-commercants-maintenir-activite-ligne.html
featured_image: https://pic.clubic.com/v1/images/1784168/raw?width=1200&fit=max&hash=b618a15405d66419bc1b0f3bc6bf3b6aef8d1bfa
tags:
- Économie
- Institutions
- Promotion
series:
- 202013
---

> Le gouvernement promeut auprès du grand public des solutions et marketplaces qui favorisent le commerce local. Et on y retrouve quelques grands noms.
