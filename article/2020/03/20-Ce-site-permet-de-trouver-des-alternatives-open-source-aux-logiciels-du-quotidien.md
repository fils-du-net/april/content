---
site: Siècle Digital
title: "Ce site permet de trouver des alternatives open source aux logiciels du quotidien"
author: Éléonore Lefaix
date: 2020-03-20
href: https://siecledigital.fr/2020/03/20/ce-site-permet-de-trouver-des-alternatives-open-source-aux-logiciels-du-quotidien
featured_image: https://thumbor.sd-cdn.fr/Erq9X-xHj8CU7kKuTTdpQaBXr40=/940x550/cdn.sd-cdn.fr/wp-content/uploads/2020/03/openbuilders.jpg
tags:
- Innovation
series:
- 202012
---

> Un site en open source qui regroupe les meilleures et plus récentes alternatives aux logiciels les plus populaires.
