---
site: Cryptonews.com
title: "C'est quoi un fork?"
author: Boris Nedeltchev
date: 2020-03-06
href: https://fr.cryptonews.com/exclusives/c-est-quoi-un-fork-5803.htm
featured_image: https://cimg.co/w/articles-attachments/0/5e6/19a81394ff.jpg
tags:
- Sensibilisation
series:
- 202010
series_weight: 0
---

> Dans le domaine de l’ingénierie, un fork (ou «fourche» en français) se produit lorsque les développeurs d’un logiciel, à partir d’une copie du code source, commencent un développement indépendant sur celui-ci. Ainsi, une version distincte du logiciel est séparée et créée.
