---
site: 01net.
title: "Coronavirus: ces ingénieurs ont créé un ventilateur médical open source et facile à fabriquer"
author: Gilbert Kallenborn
date: 2020-03-25
href: https://www.01net.com/actualites/coronavirus-ces-ingenieurs-ont-cree-un-ventilateur-medical-open-source-et-facile-a-fabriquer-1881644.html
featured_image: https://img.bfmtv.com/c/630/420/ec4/1be46822e77b5b2d8647bb5307c32.jpg
tags:
- Matériel libre
series:
- 202013
---

> Pour faire face à la pénurie qui risque de surgir bientôt, une entreprise barcelonaise a imaginé une machine de ventilation artificielle à partir de composants faciles à trouver en temps de crise.
