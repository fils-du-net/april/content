---
site: Le Monde Informatique
title: "GitHub archive les codes des logiciels open source en Arctique"
date: 2020-03-17
href: https://www.lemondeinformatique.fr/actualites/lire-github-archive-les-codes-des-logiciels-open-source-en-arctique-78462.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000071336.jpg
tags:
- Entreprise
series:
- 202012
---

> La plateforme de partage de code a lancé l'initiative Arctic Code Vault qui vise à sauvegarder les codes des logiciels open source. Le lieu de stockage est basé dans l'Arctique pour un archivage à très long terme.
