---
site: korii.
title: "Face à la pandémie de Covid-19, le DIY à la rescousse"
author: Antoine Hasday
date: 2020-03-19
href: https://korii.slate.fr/tech/covid-19-pandemie-penurie-materiel-diy-rescousse-impression-3d-masques-ventilateurs
featured_image: https://korii.slate.fr/sites/default/files/styles/1440x600/public/000_1pz5xr.jpg
tags:
- Science
- Matériel libre
series:
- 202012
---

> Les «makers» tentent de bidouiller des équipements médicaux hors des circuits classiques.
