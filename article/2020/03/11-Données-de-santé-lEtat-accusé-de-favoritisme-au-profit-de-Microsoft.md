---
site: Mediapart
title: "Données de santé: l'Etat accusé de favoritisme au profit de Microsoft (€)"
date: 2020-03-11
href: https://www.mediapart.fr/journal/france/110320/donnees-de-sante-l-etat-accuse-de-favoritisme-au-profit-de-microsoft
featured_image: https://static.mediapart.fr/images/picto_mkt/1euro.svg
tags:
- Marchés publics
series:
- 202011
---

> Le géant américain s'est vu confier l'hébergement de la plateforme devant centraliser les données de santé des Français. Mediapart révèle que plusieurs sociétés du numérique demandent au ministre de la santé de saisir le parquet afin qu'une enquête soit ouverte.
