---
site: ZDNet France
title: "#JeTravailleChezMoi: caresser son chat, se balader en peignoir, Linus Torvalds et le télétravail"
author: Steven J. Vaughan-Nichols
date: 2020-03-20
href: https://www.zdnet.fr/actualites/jetravaillechezmoi-caresser-son-chat-se-balader-en-peignoir-linus-torvalds-et-le-teletravail-39900989.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2020/02/torvald_620.jpg
tags:
- Innovation
series:
- 202012
---

> Linus Torvalds a créé Linux et Git depuis son domicile. Voici comment (attention à ceux qui commencent le télétravail, ça va vous choquer légèrement).
