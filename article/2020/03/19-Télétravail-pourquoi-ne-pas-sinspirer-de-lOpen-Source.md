---
site: Le Monde Informatique
title: "Télétravail: pourquoi ne pas s'inspirer de l'Open Source"
author: Matt Asay
date: 2020-03-19
href: https://www.lemondeinformatique.fr/actualites/lire-teletravail-pourquoi-ne-pas-s-inspirer-de-l-open-source-78486.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000071363.jpg
tags:
- Innovation
- Sensibilisation
series:
- 202012
series_weight: 0
---

> Si des équipes distribuées pouvaient construire quelque chose d'aussi génial que Linux, alors il est peut être temps de vraiment encourager le télétravail.
