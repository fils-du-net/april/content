---
site: Science-et-vie.com 
title: "Observez la diffusion du coronavirus sur la planète dans ces animations"
author: Marie Atteia
date: 2020-03-26
href: https://www.science-et-vie.com/corps-et-sante/observez-la-diffusion-du-virus-sur-la-planete-dans-ces-graphes-55048
featured_image: https://file1.science-et-vie.com/var/scienceetvie/storage/images/1/0/9/109821/observez-diffusion-coronavirus-sur-planete-dans-ces-animations.png
tags:
- Science
series:
- 202013
series_weight: 0
---

> Un site open-source illustre l'ensemble des séquences génétiques du coronavirus obtenues par les chercheurs. Une manière de visualiser la pandémie et de comprendre son évolution...
