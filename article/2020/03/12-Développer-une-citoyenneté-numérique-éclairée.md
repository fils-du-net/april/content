---
title: "Développer une citoyenneté numérique éclairée"
date: 2020-03-13
href: https://eduscol.education.fr/numerique/tout-le-numerique/veille-education-numerique/mars_2020/developper-une-citoyennete-numerique-eclairee
featured_image: https://eduscol.education.fr/numerique/tout-le-numerique/veille-education-numerique/mars_2020/developper-une-citoyennete-numerique-eclairee/@@images/8824c62a-4fc9-4206-a30c-8ffc7e280d56.png
tags:
- Internet
- Éducation
series:
- 202008
---

> Une formation en ligne ouverte à tous visant à apporter un regard critique sur les technologies numériques afin d'amener les apprenants à énoncer les critères d'un internet citoyen et d'un web décentralisé" 
