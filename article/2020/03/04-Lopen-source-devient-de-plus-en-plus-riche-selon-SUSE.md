---
site: ZDNet France
title: "L'open source devient de plus en plus riche, selon SUSE"
author: Daphne Leprince-Ringuet
date: 2020-03-04
href: https://www.zdnet.fr/actualites/l-open-source-devient-de-plus-en-plus-riche-selon-suse-39900137.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2020/02/SUSE_620.jpg
tags:
- Économie
- Entreprise
series:
- 202010
series_weight: 0
---

> Le logiciel libre est l'avenir, et il est gratuit, en supposant qu'il puisse survivre au capitalisme. Mais le modèle de développement emprunté par SUSE fait polémique.
