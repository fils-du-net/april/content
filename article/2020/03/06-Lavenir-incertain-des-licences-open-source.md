---
site: LeMagIT
title: "L'avenir incertain des licences open source"
author: Cliff Saran, Adrian Bridgwater
date: 2020-03-06
href: https://www.lemagit.fr/actualites/252479646/Lavenir-incertain-des-licences-open-source
featured_image: https://cdn.ttgtmedia.com/visuals/ComputerWeekly/Hero%20Images/Penguin-fotolia.jpg
tags:
- Licenses
- Informatique en Nuage
- Économie
series:
- 202010
series_weight: 0
---

> Adrian Bridgwater, contributeur de Computer Weekly (propriété de TechTarget également propriétaire du MagIT), a récemment interrogé des experts de l’industrie concernant le futur des licences open source.
