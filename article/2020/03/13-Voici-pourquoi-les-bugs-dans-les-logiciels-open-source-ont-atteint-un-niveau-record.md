---
site: ZDNet France
title: "Voici pourquoi les bugs dans les logiciels open source ont atteint un niveau record"
author: Liam Tung
date: 2020-03-13
href: https://www.zdnet.fr/actualites/voici-pourquoi-les-bugs-dans-les-logiciels-open-source-ont-atteint-un-niveau-record-39900639.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2019/02/open-source-code-620.jpg
tags:
- Innovation
series:
- 202011
series_weight: 0
---

> Les efforts déployés pour améliorer la sécurité des logiciels open source ont permis de découvrir 6 100 vulnérabilités l'année dernière, soit dix fois plus qu'il y a dix ans.
