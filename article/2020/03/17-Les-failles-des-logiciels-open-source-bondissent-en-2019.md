---
site: Le Monde Informatique
title: "Les failles des logiciels open source bondissent en 2019"
author: Jacques Cheminat
date: 2020-03-17
href: https://www.lemondeinformatique.fr/actualites/lire-les-failles-des-logiciels-open-source-bondissent-en-2019-78467.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000071343.jpg
tags:
- Innovation
series:
- 202012
series_weight: 0
---

> Selon une étude, les vulnérabilités dans les projets open source ont bondi de près de 50% en 2019. Mauvaise nouvelle? Au contraire, la sécurité de ces initiatives est de plus en plus prise en compte.
