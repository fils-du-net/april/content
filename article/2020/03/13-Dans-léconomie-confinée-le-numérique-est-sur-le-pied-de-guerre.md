---
site: Maddyness
title: "Dans l'économie confinée, le numérique est sur le pied de guerre"
date: 2020-03-13
href: https://www.maddyness.com/2020/03/13/economie-confinee-coronavirus
featured_image: https://www.maddyness.com/wp-content/uploads/2020/03/coronavirus-1-924x462.jpg
tags:
- Internet
- Associations
series:
- 202011
series_weight: 0
---

> Hébergeurs de centres de données, fournisseurs de solutions de travail collaboratif ou de vidéoconférences, certaines entreprises du numérique se retrouvent en position stratégique en période de confinement et de recours massif au télétravail.
