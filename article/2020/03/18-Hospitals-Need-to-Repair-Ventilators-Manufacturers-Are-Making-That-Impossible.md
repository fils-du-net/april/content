---
title: "Hospitals Need to Repair Ventilators. Manufacturers Are Making That Impossible"
author: Jason Koebler
date: 2020-03-18
href: https://www.vice.com/en_us/article/wxekgx/hospitals-need-to-repair-ventilators-manufacturers-are-making-that-impossible
featured_image: https://video-images.vice.com/test-uploads/articles/5e722a3214263d009d051766/lede/1584540630428-GettyImages-1207348129.jpeg
tags:
- Science
- English
series:
- 202012
---

> We are seeing how the monopolistic repair and lobbying practices of medical device companies are making our response to the coronavirus pandemic harder.
