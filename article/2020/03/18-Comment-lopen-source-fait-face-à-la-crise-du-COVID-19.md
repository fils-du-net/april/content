---
site: ZDNet France
title: "Comment l'open source fait face à la crise du COVID-19"
author: Steven J. Vaughan-Nichols
date: 2020-03-18
href: https://www.zdnet.fr/actualites/comment-l-open-source-fait-face-a-la-crise-du-covid-19-39900867.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/02/open-source-code-620__w630.jpg
tags:
- Science
series:
- 202012
series_weight: 0
---

> De nombreux projets open source relèvent le défi du COVID-19 et proposent des initiatives pour soutenir la lutte contre le nouveau virus.
