---
site: ZDNet France
title: "«Ça reste ouvert», une carte collaborative créée par des contributeurs d'OpenStreetMap"
author: Thierry Noisette
date: 2020-03-31
href: https://www.zdnet.fr/blogs/l-esprit-libre/a-reste-ouvert-une-carte-collaborative-creee-par-des-contributeurs-d-openstreetmap-39901573.htm
featured_image: http://www.zdnet.fr/i/edit/ne/2020/03/Ca%20Reste%20Ouvert%20carte.jpg
tags:
- Partage du savoir
- Internet
series:
- 202014
series_weight: 0
---

> Quels lieux, commerces et services restent ouverts, et avec quels horaires, pendant le confinement anti-coronavirus? Le site caresteouvert.fr, développé par des bénévoles et auxquels les internautes peuvent contribuer, permet de le savoir.
