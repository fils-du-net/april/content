---
site: Presse Citron
title: "L'équipe d'Elizabeth Warren rend ses outils de campagne accessibles en open source"
author: Jean-Yves Alric
date: 2020-03-31
href: https://www.presse-citron.net/lequipe-delizabeth-warren-rend-ses-outils-de-campagne-accessibles-en-open-source
featured_image: https://www.presse-citron.net/wordpress_prod/wp-content/uploads/2018/10/github.jpg
tags:
- Innovation
- Internet
series:
- 202014
series_weight: 0
---

> L'idée est de permettre à d'autres causes de bénéficier de ces services gratuits et faciles à prendre en main.
