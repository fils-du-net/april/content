---
site: ICTjournal
title: "Les experts suisses de la data se mobilisent dans la lutte contre le Covid-19"
author: Yannick Chavanne
date: 2020-03-24
href: https://www.ictjournal.ch/news/2020-03-24/les-experts-suisses-de-la-data-se-mobilisent-dans-la-lutte-contre-le-covid-19
featured_image: https://www.ictjournal.ch/sites/default/files/styles/np8_full/public/media/2020/03/24/corona-data.jpg?itok=BHaEN-Ml
tags:
- Science
series:
- 202013
---

> Hackathon, outil de reporting ou infographie interactive… plusieurs initiatives impliquant des spécialistes suisses de la data se focalisent sur des solutions digitales pour lutter contre la pandémie en puisant dans les données.
