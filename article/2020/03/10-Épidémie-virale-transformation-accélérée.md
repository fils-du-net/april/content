---
site: L'Opinion
title: "«Épidémie virale, transformation accélérée»"
author: David Lacombled
date: 2020-03-10
href: https://www.lopinion.fr/edition/economie/epidemie-virale-transformation-acceleree-chronique-david-lacombled-214082
featured_image: https://www.lopinion.fr/sites/nb.com/files/bloc-promo/images/elections_americaines_wsj_390_325_1_0.png
tags:
- Internet
- Économie
series:
- 202011
---

> «La perspective d’un passage en stade 3 de l’épidémie de Coronavirus fait évoluer les consciences et les pratiques à vitesse grand V»
