---
site: Silicon
title: "Open Source: Facebook rejoint la Fondation Linux"
author: Ariane Beky
date: 2020-08-14
href: https://www.silicon.fr/open-source-facebook-fondation-linux-345167.html
featured_image: https://www.silicon.fr/wp-content/uploads/2018/04/facebook_pexels-photo-267399.jpeg
tags:
- Entreprise
series:
- 202033
series_weight: 0
---

> Facebook rejoint les membres les plus influents de la Fondation Linux et place sa responsable open source au conseil d'administration de l'organisation.
