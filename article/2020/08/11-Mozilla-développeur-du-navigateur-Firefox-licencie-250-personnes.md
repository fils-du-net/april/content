---
site: usine-digitale.fr
title: "Mozilla, développeur du navigateur Firefox, licencie 250 personnes"
author: Julien Bergounhoux
date: 2020-08-11
href: https://www.usine-digitale.fr/article/mozilla-developpeur-du-navigateur-firefox-licencie-250-personnes.N993564
featured_image: https://www.usine-digitale.fr/mediatheque/2/4/3/000191342_homePageUne/mozilla.jpg
tags:
- Internet
series:
- 202033
series_weight: 0
---

> Mozilla Corporation, filiale de la fondation Mozilla en charge du navigateur Firefox, a annoncé ce 11 août son intention de licencier environ 250 employés. L'entreprise met en cause la crise économique liée à la pandémie de Covid-19, ses plans d'avant crise n'étant plus tenables.
