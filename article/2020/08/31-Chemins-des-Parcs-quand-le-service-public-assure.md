---
site: Etourisme.info
title: "Chemins des Parcs: quand le service public assure!"
author: Jean-Luc Boulin
date: 2020-08-31
href: http://www.etourisme.info/chemins-des-parcs-quand-le-service-public-assure
featured_image: http://www.etourisme.info/wp-content/uploads/2020/08/Capture-d%E2%80%99e%CC%81cran-2020-08-30-a%CC%80-16.57.23-1-1536x647.png
tags:
- Administration
- Partage du savoir
series:
- 202036
series_weight: 0
---

> J’avais envie d’écrire ce billet de rentrée en soulignant une réussite du service public. Il s’agît de “Chemins des Parcs“, un site web et une appli développés par les parcs naturels régionaux de la région Provence-Alpes-Côte d’Azur.
