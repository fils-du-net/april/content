---
site: Libération
title: "Mort du philosophe Bernard Stiegler, technicien de la pensée et penseur de la technique"
author: Sonya Faure et Simon Blin
date: 2020-08-07
href: https://www.liberation.fr/debats/2020/08/07/mort-du-philosophe-bernard-stiegler-technicien-de-la-pensee_1796272
featured_image: https://medias.liberation.fr/photo/1329124-prodlibe-2015-0641.jpg
tags:
- Économie
- Sciences
series:
- 202032
series_weight: 0
---

> Le philosophe engagé à gauche est mort à l’âge de 68 ans. Condamné en 1978 pour plusieurs braquages de banques, il avait étudié en prison. Il est l'auteur d'une œuvre hybride et pionnière sur la technique et le numérique.
