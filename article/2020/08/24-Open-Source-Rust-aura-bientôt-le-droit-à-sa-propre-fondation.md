---
site: LeMagIT
title: "Open Source: Rust aura bientôt le droit à sa propre fondation"
author: Gaétan Raoul
date: 2020-08-24
href: https://www.lemagit.fr/actualites/252488003/Open-Source-Rust-aura-bientot-le-droit-a-sa-propre-fondation
featured_image: https://cdn.ttgtmedia.com/visuals/searchDomino/domino_coding/domino_article_005.jpg
tags:
- Associations
series:
- 202035
series_weight: 0
---

> En retour de flammes de la restructuration de Mozilla, l'équipe en charge du projet Rust précipite la création d'une fondation open source dédiée au langage de programmation dont les portes ouvriront idéalement à la fin de l'année. Une décision qui ne doit pas remettre en question l'attrait de Rust auprès des développeurs.
