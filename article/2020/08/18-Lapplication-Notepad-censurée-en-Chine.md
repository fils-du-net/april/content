---
site: Le Monde.fr
title: "L'application Notepad++ censurée en Chine"
date: 2020-08-18
href: https://www.lemonde.fr/pixels/article/2020/08/18/l-application-notepad-censuree-en-chine_6049233_4408996.html
tags:
- International
- Sensibilisation
- Institutions
series:
- 202034
series_weight: 0
---

> Le site du logiciel libre, très populaire chez les développeurs, a été bloqué par le pouvoir chinois à la suite de prises de position en faveur des Ouïgours.
