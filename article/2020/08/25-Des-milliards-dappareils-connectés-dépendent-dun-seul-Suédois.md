---
site: korii.
title: "Des milliards d'appareils connectés dépendent d'un seul Suédois"
date: 2020-08-25
href: https://korii.slate.fr/tech/milliards-appareils-connectes-iphones-voitures-tv-dependent-un-seul-suedois-benevoles-open-source
featured_image: https://korii.slate.fr/sites/default/files/styles/1440x600/public/bruno-figueiredo-rbnp_odmtee-unsplash.jpg
tags:
- Sensibilisation
series:
- 202035
series_weight: 0
---

> D'innombrables infrastructures informatiques reposent sur des bénévoles travaillant en open source.
