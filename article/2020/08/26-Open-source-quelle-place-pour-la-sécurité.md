---
site: Silicon
title: "Open source: quelle place pour la sécurité?"
author: Clément Bohic
date: 2020-08-26
href: https://www.silicon.fr/open-source-securite-345669.html
featured_image: https://www.silicon.fr/wp-content/uploads/2020/08/Sonatype-securite-open-source.jpg
tags:
- Entreprise
- Innovation
series:
- 202035
series_weight: 0
---

> Comment appréhender la sécurité de l'open source? Sonatype donne des éclairages à la fois quant au développement de projets et à leur réutilisation.
