---
site: ZDNet France
title: "Accord Google/Mozilla: vers une prolongation"
author: Catalin Cimpanu
date: 2020-08-13
href: https://www.zdnet.fr/actualites/accord-google-mozilla-vers-une-prolongation-39908095.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2018/08/mozilla-firefox-icon-620__w630.jpg
tags:
- Entreprise
- Internet
series:
- 202033
series_weight: 0
---

> Selon plusieurs sources, Google devrait rester le moteur de recherche par défaut de Firefox pour les trois prochaines années.
