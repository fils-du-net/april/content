---
site: Clubic.com
title: "Des grands noms de la tech créent l'Open Source Security Foundation (OpenSSF)"
author: Arnaud Marchal
date: 2020-08-05
href: https://www.clubic.com/linux-os/actualite-8155-des-grands-noms-de-la-tech-creent-l-open-source-security-foundation-openssf-.html
featured_image: https://pic.clubic.com/v1/images/1813599/raw-accept?hash=ef5c34387ea5bae720bdddf543e14d6215f71b69
tags:
- Associations
series:
- 202032
---

> Dans l'objectif d'améliorer la sécurité des logiciels Open Source, la Linux Foundation annonce la création d'OpenSSF (Open Source Security Foundation) regroupant des acteurs majeurs du logiciel libre.
