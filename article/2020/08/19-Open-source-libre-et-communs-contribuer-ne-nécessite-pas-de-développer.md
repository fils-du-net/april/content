---
site: Next INpact
title: "Open source, libre et communs: contribuer ne nécessite pas de développer (€)"
author: David Legrand
date: 2020-08-19
href: https://www.nextinpact.com/article/43375/open-source-libre-et-communs-contribuer-ne-necessite-pas-developper
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/5553.jpg
tags:
- Sensibilisation
series:
- 202034
series_weight: 0
---

> Si l'open source et le logiciel libre gagnent du terrain dans notre quotidien, nous ne sommes en général que des bénéficiaires de ces communs. Habitués à une position de consommateur dans un écosystème numérique ouvert, pourtant pensé avec une certaine symétrie. Participer est donc possible, même si vous n'êtes pas développeur.
