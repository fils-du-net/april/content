---
site: Next INpact
title: "Fork et pull request: la participation open source par l'exemple (€)"
author: David Legrand
date: 2020-08-21
href: https://www.nextinpact.com/article/43385/fork-et-pull-request-participation-open-source-par-exemple
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/3548.jpg
tags:
- Sensibilisation
series:
- 202034
---

> La participation aux projets open source est en général perçue comme complexe, souvent à cause de l'outil de gestion de versions Git. Mais lorsqu'il s'agit de proposer de petites modifications, tout peut se faire simplement en ligne via des plateformes comme GitHub.
