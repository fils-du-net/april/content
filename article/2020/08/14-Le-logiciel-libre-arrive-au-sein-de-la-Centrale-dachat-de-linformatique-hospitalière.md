---
site: Next INpact
title: "Le logiciel libre arrive au sein de la Centrale d'achat de l'informatique hospitalière (€)"
author: Marc Rees
date: 2020-08-14
href: https://www.nextinpact.com/article/43372/le-logiciel-libre-arrive-au-sein-centrale-dachat-informatique-hospitaliere
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/5274.jpg
tags:
- Administration
- Marchés publics
- Institutions
series:
- 202033
series_weight: 0
---

> La CAIH vient de lancer un appel d’offres. Objet ? Proposer des logiciels sous licence libre à ses 1 200 membres établissements de santé. Un mouvement qu’avait réclamé Olivier Véran, du moins lorsqu’il était député.
