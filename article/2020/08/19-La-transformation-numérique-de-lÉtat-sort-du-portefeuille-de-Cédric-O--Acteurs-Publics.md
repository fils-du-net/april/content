---
site: Acteurs Publics 
title: "La transformation numérique de l'État sort du portefeuille de Cédric O"
author: Emile Marzolf
date: 2020-08-19
href: https://www.acteurspublics.fr/articles/la-transformation-numerique-de-letat-sort-du-portefeuille-de-cedric-o
tags:
- Institutions
series:
- 202034
---

> C’est après plus de deux semaines d’attente que le gouvernement Castex a finalement été complété par la nomination des secrétaires d’État, le 27 juillet
