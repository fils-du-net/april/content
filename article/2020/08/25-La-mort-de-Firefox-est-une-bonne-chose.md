---
site: Houssenia Writing
title: "La mort de Firefox est une bonne chose"
author: Houssen Moshinaly
date: 2020-08-25
href: https://housseniawriting.com/web/la-mort-de-firefox-est-une-bonne-chose
featured_image: https://housseniawriting.com/wp-content/uploads/2020/08/mort-firefox-720x340.jpg
tags:
- Internet
series:
- 202035
series_weight: 0
---

> Certains annoncent la mort de Firefox à cause de licenciements massifs chez Mozilla et la concentration sur des services payants. Mais c'est sans doute la meilleure chose qui puisse lui arriver.
