---
site: lesoir.be
title: "Du code informatique stocké bien au frais dans l’Arctique (€)"
author: Thomas Casavecchia
date: 2020-08-09
href: https://plus.lesoir.be/318055/article/2020-08-09/du-code-informatique-stocke-bien-au-frais-dans-larctique
featured_image: https://plus.lesoir.be/sites/default/files/dpistyles_v2/ena_16_9_extra_big/2020/08/09/node_318055/27663609/public/2020/08/09/B9724237589Z.1_20200809155342_000+GE5GFAVJ1.1-0.jpg
tags:
- Partage du savoir
series:
- 202032
series_weight: 0
---

> Il s’agit d’une véritable bibliothèque d’Alexandrie : les codes sources du système d’exploitation Linux, le code du programme qui a permis de capturer la première image d’un trou noir, les codes permettant de faire fonctionner le bitcoin, ou encore ceux de MS-DOS, l’ancêtre de Windows qui permettait de faire fonctionner les disquettes et que les moins de vingt ans ne peuvent pas connaître. Tous font partie des 21 téraoctets de codes qui sont stockés sur 186 bobines de microfilm. Github promet que ces derniers resteront accessibles durant les 1.000 prochaines années. De quoi enthousiasmer les futurs archéologues.
