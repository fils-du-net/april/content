---
site: Le Monde Informatique
title: "430% de cyberattaques en plus sur les composants Open Source"
author: Maryse Gros
date: 2020-08-13
href: https://www.lemondeinformatique.fr/actualites/lire-430-de-cyberattaques-en-plus-sur-les-composants-open-source-80025.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000073691.jpg
tags:
- Innovation
series:
- 202033
series_weight: 0
---

> La chaîne logicielle des entreprises est de plus en plus dépendante des composants open source qui lui permettent d'innover plus rapidement. Mais les projets intègrent aussi des centaines de dépendances avec d'autres projets open source qui peuvent contenir des failles. L'édition 2020 du State of the Software Supply Chain indique une hausse de 430% des cyberattaques de nouvelle génération visant à infiltrer ces composants.
