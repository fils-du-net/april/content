---
site: ZDNet France
title: "Notepad++, dans le viseur de la censure chinoise"
author: Louis Adam
date: 2020-08-18
href: https://www.zdnet.fr/actualites/notepad-dans-le-viseur-de-la-censure-chinoise-39908285.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/08/1200px-Notepad++_Logo.svg__w630.png
tags:
- International
series:
- 202034
---

> L'application Notepad++, éditeur de texte bien connu des développeurs, fait les frais de ses prises de position: le gouvernement chinois a en effet bloqué les pages de téléchargement proposées par l'éditeur.
