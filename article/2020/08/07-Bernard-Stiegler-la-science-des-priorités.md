---
site: Libération
title: "Bernard Stiegler: la science des priorités"
author: Sonya Faure et Simon Blin
date: 2020-08-07
href: https://next.liberation.fr/livres/2020/08/07/bernard-stiegler-la-science-des-priorites_1796328
featured_image: https://medias.liberation.fr/photo/1329186-portrait-industrie-robotique.jpg
tags:
- Économie
- Sciences
series:
- 202032
---

> Le philosophe engagé à gauche est mort jeudi, à 68 ans. Condamné en 1978 pour plusieurs braquages de banques, il avait étudié en prison. Il est l'auteur d'une œuvre hybride et pionnière sur la place de la technique dans la société.
