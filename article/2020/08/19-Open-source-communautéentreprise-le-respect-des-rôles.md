---
site: Le Monde Informatique
title: "Open source: communauté/entreprise, le respect des rôles"
author: Matt Asay
date: 2020-08-19
href: https://www.lemondeinformatique.fr/actualites/lire-open-source-communaute-entreprise-le-respect-des-roles-80081.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000073751.png
tags:
- Entreprise
- Sensibilisation
series:
- 202034
series_weight: 0
---

> Open Source: Pour Lili Cosic, responsable de kube-state-metrics et employée de Red Hat, la question n'est pas de savoir ce que Red Hat attend du projet, mais de...
