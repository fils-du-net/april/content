---
site: ZDNet France
title: "Open Source Security Foundation: regrouper pour mieux sécuriser"
author: Steven J. Vaughan-Nichols
date: 2020-08-04
href: https://www.zdnet.fr/actualites/open-source-security-foundation-regrouper-pour-mieux-securiser-39907689.htm
featured_image: https://www.zdnet.fr/i/edit/ne/2019/02/open-source-code-620.jpg
tags:
- Associations
- Innovation
series:
- 202032
series_weight: 0
---

> Une nouvelle fondation vient rassembler les différents efforts de la communauté visant à mieux sécuriser les programmes Open Source.
