---
site: LaDepeche.fr
title: "Panne de Google: ce que l'on sait sur l'incident qui a paralysé internet ce lundi"
date: 2020-12-14
href: https://www.ladepeche.fr/2020/12/14/internet-google-gmail-ou-encore-youtube-confrontes-a-une-panne-mondiale-9257083.php
featured_image: https://images.ladepeche.fr/api/v1/images/view/5ef4628ed286c250867d57e8/large/image.jpg?v=1
tags:
- Internet
- Entreprise
series:
- 202051
series_weight: 0
---

> Les services du géant américain, de son moteur de recherche à YouTube en passant par Gmail ou encore le service Google Maps, ont subi une panne de près d'une heure lundi 14 décembre. Voici ce que l'on sait.
