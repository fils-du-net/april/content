---
site: Contexte
title: "Régulation des plateformes: tout ce qu'on sait et tout ce qu'on ne sait pas"
author: Cécile Frangne, Guénaël Pépin
date: 2020-12-09
href: https://www.contexte.com/article/numerique/regulation-des-plateformes-tout-ce-quon-sait-et-tout-ce-quon-ne-sait-pas_124177.html
featured_image: https://m.contexte.com/medias-images/2020/12/8657345891_9232376c40_kjpg.jpg.1024x320_q85_box-218%2C204%2C1791%2C695_crop_detail_gradient_fx-10.png
tags:
- Europe
series:
- 202050
series_weight: 0
---

> Le 15 décembre, la Commission doit présenter sa refonte de la régulation du numérique, le Digital Services Act et le Digital Markets Act. Pour vous aider à vous repérer dans les grands plans de l’exécutif européen le jour J, Contexte vous résume ses informations collectées depuis trois mois et les zones d’ombre des deux textes.
