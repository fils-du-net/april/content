---
site: la-rem
title: "Le grand écart entre Gaia-X et le Health Data Hub"
date: 2020-12-07
author: Jacques-André Fines Schlumberger
href: https://la-rem.eu/2020/12/le-grand-ecart-entre-gaia-x-et-le-health-data-hub/
tags:
- Informatique en nuage
- Vie privée
- Europe
- Marchés publics
series:
- 202050
series_weight: 0
---

> Alors que Gaia-X vise à donner à l'Europe une souveraineté numérique, une annonce en totale contradiction avec le choix du gouvernement français de confier à l'américain Microsoft l'hébergement du projet Health Data Hub.
