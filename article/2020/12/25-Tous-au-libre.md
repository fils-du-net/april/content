---
site: Le Monde.fr
title: "Tous au libre!"
date: 2020-12-25
href: https://www.lemonde.fr/blog/binaire/2020/12/25/tous-au-libre
featured_image: https://asset.lemde.fr/prd-blogs/2020/10/fe344d7e-petit-binaire.png
tags:
- Sensibilisation
series:
- 202052
series_weight: 0
---

> Tu as faim? J’ai une pomme. Partageons là. Du coup, je n’ai mangé qu’une demi-pomme. Mais j’ai gagné ton amitié. Tu as une idée? Partageons là. Du coup, nous voilà toi et moi avec une idée. Mieux encore: ton idée vient de m’en susciter une autre que je te repartage, en retour. Pour te permettre d’en trouver une troisième peut-être.
