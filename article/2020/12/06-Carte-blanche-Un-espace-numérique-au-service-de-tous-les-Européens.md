---
site: lesoir.be
title: "Carte blanche: «Un espace numérique au service de tous les Européens»"
date: 2020-12-06
author: Thierry Breton, Margrethe Vestager
href: https://plus.lesoir.be/342027/article/2020-12-06/carte-blanche-un-espace-numerique-au-service-de-tous-les-europeens
featured_image: https://plus.lesoir.be/sites/default/files/dpistyles_v2/ena_16_9_extra_big/2020/12/06/node_342027/27829998/public/2020/12/06/B9725455218Z.1_20201206172334_000+GVLH6HJVQ.1-0.jpg?itok=Q5339kTt1607274526
tags:
- Europe
- Internet
- Institutions
series:
- 202049
series_weight: 0
---

> Deux membres de l’exécutif européen, Margrethe Vestager et Thierry Breton, soulignent l’importance, pour «nous, Européens, d’organiser notre monde numérique» via le Digital Services Act.
