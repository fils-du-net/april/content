---
site: Le Monde.fr
title: "GAFA: Bruxelles dévoile son plan pour mieux lutter contre les abus des géants du numérique (€)"
author: Virginie Malingre
date: 2020-12-15
href: https://www.lemonde.fr/economie/article/2020/12/15/gafa-bruxelles-devoile-son-plan-pour-mieux-lutter-contre-les-abus-des-geants-du-numerique_6063487_3234.html
featured_image: https://img.lemde.fr/2020/12/15/0/0/4500/3000/1328/0/45/0/9be6697_88051653-398954.jpg
tags:
- Europe
series:
- 202051
---

> La Commission européenne a présenté mardi les deux textes, DSA et DMA, qui doivent permettre de remettre de l'ordre sur Internet, que ce soit en matière de contenus en ligne ou de concurrence.
