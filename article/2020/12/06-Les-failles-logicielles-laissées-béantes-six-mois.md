---
site: cio-online.com
title: "Les failles logicielles laissées béantes six mois"
author: Laurent Mavallet
date: 2020-12-06
href: https://www.cio-online.com/actualites/lire-les-failles-logicielles-laissees-beantes-six-mois-12745.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000016280.jpg
tags:
- Innovation
series:
- 202049
---

> Selon une étude de Veracode, la moitié des failles repérées dans les logiciels ne sont pas corrigées six mois plus tard.
