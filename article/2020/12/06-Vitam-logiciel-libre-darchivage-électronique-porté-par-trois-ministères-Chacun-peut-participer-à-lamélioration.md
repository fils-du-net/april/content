---
site: ZDNet France
title: "Vitam, logiciel libre d'archivage électronique porté par trois ministères: 'Chacun peut participer à l'amélioration'"
author: Thierry Noisette
date: 2020-12-06
href: https://www.zdnet.fr/blogs/l-esprit-libre/vitam-logiciel-libre-d-archivage-electronique-porte-par-trois-ministeres-chacun-peut-participer-a-l-amelioration-39914383.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2020/12/Vitam_logo.jpg
tags:
- Administration
- Innovation
series:
- 202050
series_weight: 0
---

> Logiciel libre permettant de gérer de très gros volumes, Vitam est un programme interministériel d'archivage électronique, passé en 2020 en phase produit. Le point avec deux de ses responsables, Thierry Devillechabrolle et Alice Grippon.
