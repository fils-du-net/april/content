---
site: Le Monde.fr
title: "Gafam: une mise au pas légitime au niveau européen"
date: 2020-12-17
href: https://www.lemonde.fr/idees/article/2020/12/17/gafam-une-mise-au-pas-legitime-au-niveau-europeen_6063698_3232.html
featured_image: https://img.lemde.fr/2020/12/17/0/0/4500/2999/1328/0/45/0/46a3bb2_937505606-260044.jpg
tags:
- Europe
- Internet
- Institutions
series:
- 202051
series_weight: 0
---

> Editorial. Pour respecter les nouvelles contraintes imposée par la Commission européenne le 15 décembre, les géants du numérique devront adapter leur modèle. Si leur puissance leur octroie beaucoup de droits, il est temps qu'ils en assument maintenant les devoirs.
