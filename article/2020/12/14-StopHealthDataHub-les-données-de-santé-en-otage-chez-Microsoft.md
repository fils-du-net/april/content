---
site: Libération
title: "#StopHealthDataHub: les données de santé en otage chez Microsoft"
date: 2020-12-14
href: https://www.liberation.fr/amphtml/debats/2020/12/14/stophealthdatahub-les-donnees-de-sante-en-otage-chez-microsoft_1808434
featured_image: https://medias.liberation.fr/photo/1352957-000_8vd8z4.jpg
seeAlso: "[Pour des logiciels libres au service de la santé, répondre à l'appel à don de Interhop](https://www.april.org/pour-des-logiciels-libres-au-service-de-la-sante-repondre-a-l-appel-a-don-de-interhop)"
tags:
- Vie privée
- Associations
- Informatique en nuage
series:
- 202051
series_weight: 0
---

> Contestable en matière de sécurité, le projet de centralisation des données de santé des Français est inconciliable avec le respect des droits à la protection des informations personnelles, alertent des associations, personnalités publiques, syndicats des secteurs de la santé et de la défense des libertés.
