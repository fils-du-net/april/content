---
site: Le Monde.fr
title: "«L'Europe a besoin de lois qui limitent le pouvoir des grandes entreprises du numérique sur nos vies» (€)"
author: Jan Penfrat
date: 2020-12-15
href: https://www.lemonde.fr/idees/article/2020/12/15/l-europe-a-besoin-de-lois-qui-limitent-le-pouvoir-des-grandes-entreprises-du-numerique-sur-nos-vies_6063412_3232.html
tags:
- Europe
series:
- 202051
---

> Jan Penfrat, conseiller d'une coalition européenne de quarante-quatre ONG de protection des droits numériques, appelle dans une tribune au «Monde» à aller au-delà des Digital Services Act et Digital Market Act présentés mardi 15 décembre par la Commission européenne.
