---
site: Silicon
title: "Open source: OVHcloud rallie l'Open Invention Network"
author: Clément Bohic
date: 2020-12-21
href: https://www.silicon.fr/open-source-ovhcloud-open-invention-network-355076.html
featured_image: https://www.silicon.fr/wp-content/uploads/2020/12/OVHcloud-Open-Invention-Network.jpg
tags:
- Brevets logiciels
- Entreprise
series:
- 202052
series_weight: 0
---

> OVHcloud affirme sa stratégie open source en rejoignant l'Open Invention Network, fonds de propriété intellectuelle qui vient de fêter ses 15 ans.
