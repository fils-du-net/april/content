---
site: ZDNet France
title: "Le RGPD et l'Education nationale, un vaste chantier inachevé"
author: Clarisse Treilles
date: 2020-12-01
href: https://www.zdnet.fr/actualites/le-rgpd-et-l-education-nationale-un-vaste-chantier-inacheve-39914007.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/01/ecole%20numerique__w1200.jpg
tags:
- Éducation
- Vie privée
series:
- 202049
series_weight: 0
---

> Si cela fait un peu plus de deux ans que le RGPD est entré en vigueur, la crise sanitaire de la Covid-19 a mis en exergue le fossé entre la théorie et la réalité dans l'enseignement secondaire.
