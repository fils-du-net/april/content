---
site: Le Monde Informatique
title: "La Fondation Linux crée une certification pour débutant"
author: Jacques Cheminat
date: 2020-12-02
href: https://www.lemondeinformatique.fr/actualites/lire-la-fondation-linux-cree-une-certification-pour-debutant-81226.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000075742.jpg
tags:
- Partage du savoir
- Entreprise
series:
- 202049
series_weight: 0
---

> Il faut bien démarrer un jour et la Fondation Linux veut mettre le pied à l'étrier des débutants qui envisagent une carrière dans l'open source. Elle a donc lancé une certification accessible à ces personnes-là.
