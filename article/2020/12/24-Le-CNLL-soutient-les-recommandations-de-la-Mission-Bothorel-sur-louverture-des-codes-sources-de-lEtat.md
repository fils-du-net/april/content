---
site: Programmez!
title: "Le CNLL soutient les recommandations de la Mission Bothorel sur l'ouverture des codes sources de l'Etat"
author: fredericmazue
date: 2020-12-24
href: https://www.programmez.com/actualites/le-cnll-soutient-les-recommandations-de-la-mission-bothorel-sur-louverture-des-codes-sources-de-31283
tags:
- Open data
- Institutions
series:
- 202052
series_weight: 0
---

> La Mission Bothorel “ayant pour objet la politique de la donnée et des codes sources de l'Etat”, lancée en juin dernier, vient de rendre son rapport au Premier
