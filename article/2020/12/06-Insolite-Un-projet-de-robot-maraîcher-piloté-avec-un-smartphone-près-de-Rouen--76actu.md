---
site: 76actu
title: "Insolite. Un projet de robot maraîcher, piloté avec un smartphone, près de Rouen"
author: Fabien Massin
date: 2020-12-06
href: https://actu.fr/normandie/le-petit-quevilly_76498/insolite-un-projet-de-robot-maraicher-pilote-avec-un-smartphone-pres-de-rouen_37873759.html
featured_image: https://static.actu.fr/uploads/2020/12/kaleidoscope-petit-quevilly-farmbot-rouen-robot-potager.jpg
tags:
- Matériel libre
- Innovation
series:
- 202049
series_weight: 0
---

> Un projet de robot maraîcher est en cours d'élaboration au Kaléidoscope, tiers-lieu du Petit-Quevilly, près de Rouen, qui produira ses premiers légumes l'année prochaine.
