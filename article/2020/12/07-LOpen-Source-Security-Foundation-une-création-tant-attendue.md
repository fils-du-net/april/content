---
site: Le Monde Informatique
title: "L'Open Source Security Foundation, une création tant attendue"
author: Matt Asay
date: 2020-12-07
href: https://www.lemondeinformatique.fr/actualites/lire-l-open-source-security-foundation-une-creation-tant-attendue-81250.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000075781.jpg
tags:
- Innovation
series:
- 202050
series_weight: 0
---

> Le processus open source qui permet de repérer et de corriger les bogues est aussi la meilleure option pour améliorer la sécurité des logiciels. L'Open Source Security Foundation (OpenSSF) offre une chance de coordonner les efforts.
