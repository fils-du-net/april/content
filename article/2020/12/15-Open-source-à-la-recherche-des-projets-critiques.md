---
site: Silicon
title: "Open source: à la recherche des projets critiques"
author: Clément Bohic
date: 2020-12-15
href: https://www.silicon.fr/open-source-projets-critiques-354455.html
featured_image: https://www.silicon.fr/wp-content/uploads/2020/12/Open-source-score-criticit%C3%A9.jpg
tags:
- Promotion
series:
- 202051
series_weight: 0
---

> Comment estimer le degré d'importance d'un projet open source? L'OSSF tente d'établir la bonne formule algorithmique.
