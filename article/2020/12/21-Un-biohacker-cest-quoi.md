---
site: RTBF Info
title: "Un biohacker, c'est quoi?"
author: Olivier Marchal
date: 2020-12-21
href: https://www.rtbf.be/lapremiere/emissions/detail_week-end-premiere/accueil/article_un-biohacker-c-est-quoi?id=10658909&programId=12394
featured_image: https://ds1.static.rtbf.be/article/image/770x433/a/9/b/2bd7f907b7f5b6bbd91822c0c7b835f6-1608545702.jpg
tags:
- Sciences
- Innovation
series:
- 202052
series_weight: 0
---

> Si l’on dit souvent qu’on a tous un peu de sang rebelle qui coule dans nos veines et si parfois, nos petits brins d’ADN se déguisent en Robin des bois, c’est qu’il reste, çà et là, sans même qu’on ne les voit: des tas de forêts de Sherwood dans lesquelles en marge des secteurs professionnels bien visibles, s’inventent et se profilent des métiers nouveaux et fascinants ! Chaque mois, en compagnie d’Olivier Marchal, sociologue et directeur de la Cité des Métiers de Charleroi, on explore le futur des métiers.
