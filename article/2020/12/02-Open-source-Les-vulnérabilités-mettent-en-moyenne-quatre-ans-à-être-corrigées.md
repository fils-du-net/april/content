---
site: ZDNet France
title: "Open source: Les vulnérabilités mettent en moyenne quatre ans à être corrigées"
author: Charlie Osborne
date: 2020-12-02
href: https://www.zdnet.fr/actualites/open-source-les-vulnerabilites-mettent-en-moyenne-quatre-ans-a-etre-corrigees-39914125.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2020/12/open%20source__w1200.jpg
tags:
- Innovation
series:
- 202049
series_weight: 0
---

> Les recherches menées par GitHub suggèrent qu'il est nécessaire de réduire le temps entre la détection et la correction des bugs.
