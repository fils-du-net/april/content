---
site: Le Monde.fr
title: "«Un consensus international inédit émerge pour couper les ailes des GAFA» (€)"
author: Philippe Escande
date: 2020-12-15
href: https://www.lemonde.fr/economie/article/2020/12/15/un-consensus-international-inedit-emerge-pour-couper-les-ailes-des-gafa_6063463_3234.html
featured_image: 
tags:
- Europe
series:
- 202051
---

> L'Union européenne, les Etats-Unis et même la Chine commencent à agir, chacun avec ses armes, contre des géants de l'Internet dont la puissance financière et stratégique, fondée sur un modèle de recueil des données personnelles, commence à menacer les Etats trop sérieusement, explique Philippe Escande, éditorialiste économique au «Monde».
