---
site: Le Monde.fr
title: "La CNIL inflige de lourdes amendes à Google et Amazon pour non-respect de la législation sur les cookies"
date: 2020-12-10
href: https://www.lemonde.fr/pixels/article/2020/12/10/la-cnil-inflige-des-amendes-a-google-et-amazon-pour-non-respect-de-la-legislation-sur-les-cookies_6062860_4408996.html
featured_image: https://img.lemde.fr/2019/06/14/0/0/4256/2832/1328/0/45/0/d7025d9_HoixpPXb0L2Itld1AAkHb7E-.jpg
tags:
- Vie privée
- Institutions
- Entreprise
series:
- 202050
series_weight: 0
---

> Le «gendarme» de la vie privée reproche aux entreprises d’avoir pisté leurs utilisateurs pour des fins publicitaires sans leur consentement.
