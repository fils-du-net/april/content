---
site: LeMagIT
title: "Open Data: l'avance prise par la France ne tient qu'à un fil"
author: Gaétan Raoul
date: 2020-12-30
href: https://www.lemagit.fr/actualites/252494218/Open-Data-lavance-prise-par-la-France-ne-tient-qua-un-fil
featured_image: https://cdn.ttgtmedia.com/visuals/LeMagIT/hero_article/France.jpg
tags:
- Open Data
- Institutions
- Administration
series:
- 202053
- 202101
series_weight: 0
---

> La mission Bothorel a remis son rapport sur la politique publique de la donnée, des algorithmes et des codes sources au Premier ministre Jean Castex le 23 décembre. Les auteurs signent un document sans fard pour l’État français et les administrations dont les tergiversations pourraient ralentir les bienfaits de l’open data.
