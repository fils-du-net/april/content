---
site: 20minutes.fr
title: "Comment marche Time for the Planet, le fonds d'investissement citoyen qui oeuvre pour le climat"
author: Fabrice Pouliquen
date: 2020-12-22
href: https://www.20minutes.fr/planete/2936563-20201222-comment-marche-time-for-the-planet-fonds-investissement-citoyen-oeuvre-climat
featured_image: https://img.20mn.fr/6rgOjES4QLaUIqfUI4uYeA/310x190_lance-six-entrepreneurs-lyonnais-peu-plus-an-time-for-planet-reunit-aujourdhui-7574-actionnaires-recolte-1247832-euros-recoltes.jpg
tags:
- Économie
- Innovation
series:
- 202052
series_weight: 0
---

> «Jouer sur le terrain économique, mais avec nos règles». C’est le credo de Time for the Planet, fonds d’investissement citoyen qui, en un an, a déjà levé un peu plus d’un million d’euros
