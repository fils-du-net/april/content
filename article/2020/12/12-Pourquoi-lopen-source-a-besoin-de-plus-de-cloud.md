---
site: Le Monde Informatique
title: "Pourquoi l'open source a besoin de plus de cloud"
author: Matt Asay
date: 2020-12-12
href: https://www.lemondeinformatique.fr/actualites/lire-pourquoi-l-open-source-a-besoin-de-plus-de-cloud-81333.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000075917.jpg
tags:
- Informatique en nuage
- Entreprise
- Économie
- Licenses
series:
- 202050
series_weight: 0
---

> Pour le contributeur Matt Asay, les projets open source doivent se souvenir des erreurs passées de Microsoft vis-à-vis d'eux. A l'heure du cloud, le succès dépend de la croissance du marché dans son ensemble, même si cela doit se faire au détriment du contrôle.
