---
site: ZDNET
title: "Décès de Jean-Pierre Archambault, pionnier du logiciel libre dans les écoles"
author: Thierry Noisette
date: 2025-03-02
href: https://www.zdnet.fr/blogs/l-esprit-libre/deces-de-jean-pierre-archambault-pionnier-du-logiciel-libre-dans-les-ecoles-407279.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2025/03/jean-pierre_archambault_wp.jpg
tags:
- april
- Sensibilisation
- Éducation
series:
- 202509
series_weight: 0
---

> Cet ancien enseignant, professeur agrégé de mathématiques, a été un grand acteur de l'enseignement de l'informatique et du développement du Libre dans le système éducatif.
