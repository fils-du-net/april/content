---
site: La Tribune
title: "Open source (d'inspiration)"
author: Philippe Boyer
date: 2025-03-01
href: https://www.latribune.fr/opinions/blogs/homo-numericus/open-source-d-inspiration-1019423.html
featured_image: https://pictures.latribune.fr/cdn-cgi/image/width=2048,format=auto,quality=80/145/1898145.jpg
tags:
- Sciences
series:
- 202509
series_weight: 0
---

> HOMO NUMERICUS. De plus en plus de modèles d'intelligence artificielle sont élaborés en «open source», c'est-à-dire un accès gratuit à un logiciel dont le code est public. Ce modèle s'imposera-t-il comme la référence de la future IA? Par Philippe Boyer, directeur relations institutionnelles et innovation à Covivio.
