---
site: ZDNET
title: "Prix Science ouverte pour le logiciel libre d'archéologie archeoViz"
author: Thierry Noisette
date: 2025-03-08
href: https://www.zdnet.fr/blogs/l-esprit-libre/prix-science-ouverte-pour-le-logiciel-libre-darcheologie-archeoviz-407751.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2025/03/archeoviz-portail-750x410.jpg
tags:
- Sciences
series:
- 202510
series_weight: 0
---

> C'est un des huit logiciels distingués par les Prix Science ouverte du logiciel libre et de la recherche. Cette appli incite les archéologues à partager leurs données avec la communauté scientifique et avec le grand public.
