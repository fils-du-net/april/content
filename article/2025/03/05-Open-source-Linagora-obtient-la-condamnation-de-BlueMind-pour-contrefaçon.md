---
site: ZDNET
title: "Open source: Linagora obtient la condamnation de BlueMind pour contrefaçon"
author: Louis Adam
date: 2025-03-05
href: https://www.zdnet.fr/actualites/open-source-linagora-obtient-la-condamnation-de-bluemind-pour-contrefacon-407487.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/02/Justice20informatique-750x410.jpg
tags:
- Droit d'auteur
series:
- 202510
series_weight: 0
---

> C'est un combat judiciaire qui oppose les deux sociétés depuis plus de dix ans. Linagora vient d'obtenir une victoire en appel.
