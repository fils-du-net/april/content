---
site: Silicon
title: "Un refus d'interopérabilité peut constituer un abus de position dominante"
author: Clément Bohic
date: 2025-03-05
href: https://www.silicon.fr/Thematique/business-1367/Breves/refus-d-interoperabilite-abus-position-dominante-468437.htm
featured_image: https://www.silicon.fr/Assets/Img/BREVE/2025/3/468437/refus-interoperabilite-peut-constituer-abus-position-dominante-LE.jpg
tags:
- Interopérabilité
- Europe
series:
- 202510
series_weight: 0
---

> Dans un dossier opposant Google à une entreprise italienne autour d'Android Auto, la CJUE a clarifié la question du refus de fourniture d'interopérabilité.
