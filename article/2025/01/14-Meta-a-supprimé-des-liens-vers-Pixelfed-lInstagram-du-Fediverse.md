---
site: Next
title: "Meta a supprimé des liens vers Pixelfed, l’Instagram du Fediverse"
description: "Pratique, le filtre de spam"
author: Martin Clavey 
date: 2025-01-14
href: https://next.ink/165362/meta-a-supprime-des-liens-vers-pixelfed-linstagram-du-fediverse
featured_image: https://next.ink/wp-content/uploads/2025/01/pixeldroid_mascot2.webp
tags:
- Internet
series:
- 202503
---

> Alors que Meta voit certains de ses utilisateurs chercher des solutions de repli après son changement de politique de modération, l'entreprise a supprimé des messages partageant des liens vers l'instance principale de Pixelfed, l'Instagram du Fediverse.
