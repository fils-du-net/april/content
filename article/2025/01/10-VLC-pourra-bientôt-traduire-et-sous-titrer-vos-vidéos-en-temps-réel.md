---
site: 01net.
title: "VLC pourra bientôt traduire et sous-titrer vos vidéos en temps réel"
author: Geoffroy Ondet
date: 2025-01-10
href: https://www.01net.com/actualites/vlc-traduire-sous-titrer-videos-temps-reel.html
featured_image: https://www.01net.com/app/uploads/2018/02/vlc-media-player-logo-1360x907.jpg
tags:
- Innovation
series:
- 202502
---

> Le lecteur multimédia de VideoLAN va s'appuyer sur des modèles d'IA exécutés localement sur votre ordinateur pour générer des sous-titres dans de nombreuses langues, sans connexion à Internet.
