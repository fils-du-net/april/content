---
site: Numerama
title: "Lucie: que s'est-il passé avec l'IA française qui répond n'importe quoi?"
description: Pas fameux comme début
author: Julien Lausson
date: 2025-01-27
href: https://www.numerama.com/tech/1891180-lucie-que-sest-il-passe-avec-lia-francaise-qui-repond-nimporte-quoi.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2025/01/lucie-une-1024x576.jpg?webp=1&key=f8f21204
tags:
- Sciences
series:
- 202505
---

> Mauvaise en mathématiques, en histoire ou en logique: les débuts de Lucie, une IA générative française, sont laborieux. Ce week-end, le chatbot a rapidement montré ses limites à cause de ses fausses réponses, et suscité bien des moqueries. L'accès à Lucie a depuis été désactivé, et les concepteurs sont intervenus pour calmer le jeu.
