---
site: La Tribune
title: "La définition de l'IA open source résoudra un million de casse-tête"
author: Sylvain Rolland
date: 2025-01-13
href: https://www.latribune.fr/opinions/tribunes/la-definition-de-l-ia-open-source-resoudra-un-million-de-casse-tete-1015597.html
featured_image: https://pictures.latribune.fr/cdn-cgi/image/width=2048,format=auto,quality=80/145/1898145.jpg
tags:
- Sciences
series:
- 202503
series_weight: 0
---

> OPINION. Une définition étroite et peu rigoureuse de l'IA open source laisserait la porte ouverte à des entreprises comme Meta, qui pourraient changer de cap et cesser de publier des parties de leurs modèles d'IA si ceux-ci ne servent plus leurs intérêts. Un sujet crucial à l'approche du Sommet pour l'action sur l'IA, qui se tiendra en février. Par Mark Surman, président de Mozilla.
