---
site: Basta! 
title: "Quitter X, mais pour Mastodon ou BlueSky?"
description: "«Mastodon, c’est un bien commun, surtout vu l’environnement médiatique et politique»"
author: Rachel Knaebel
date: 2025-01-20
href: https://basta.media/mastodon-bien-commun-surtout-vu-environnement-mediatique-politique-quitter-X
featured_image: https://basta.media/local/adapt-img/960/10x/IMG/logo/mastodon_vs_elon_musk_et_mark_zuckerberg.jpg@.webp?1737387110
tags:
- Internet
series:
- 202504
series_weight: 0
---

> Avec le soutien de Musk à Trump et aux extrêmes droites européennes, un mouvement se forme pour quitter X/Twitter. Mastodon, réseau social non lucratif et respectueux des données, est la première alternative. Rencontre avec son responsable technique.
