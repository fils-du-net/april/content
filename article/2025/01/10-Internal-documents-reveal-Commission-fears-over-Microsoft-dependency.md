---
site: EurActiv
title: "Internal documents reveal Commission fears over Microsoft dependency"
author: Jacob Wulff Wold
date: 2025-01-10
href: https://www.euractiv.com/section/tech/news/internal-documents-reveal-commission-fears-over-microsoft-dependency
featured_image: https://en.euractiv.eu/wp-content/uploads/sites/2/2025/01/GettyImages-2100326324.jpg
tags:
- Europe
- Logiciels privateurs
- English
series:
- 202503
series_weight: 0
---

> European Commission officials fear that their heavy reliance on Microsoft constitutes a clear breach of EU data rules, according to internal Commission documents seen by Euractiv, which contradict the executive’s public statements on the matter.
