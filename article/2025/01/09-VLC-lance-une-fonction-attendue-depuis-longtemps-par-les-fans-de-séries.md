---
site: Numerama
title: "VLC lance une fonction attendue depuis longtemps par les fans de séries"
description: "La fin des .srt"
author: "Hugo Bernard & Nicolas Lellouche"
date: 2025-01-09
href: https://www.numerama.com/tech/1879712-vlc-lance-les-sous-titres-automatiques-dans-toutes-les-langues-grace-a-lia.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2025/01/vlc-ai-1024x576.jpg?webp=1&key=f1f1b0c3
tags:
- Innovation
series:
- 202502
series_weight: 0
---

> VLC va intégrer de l'IA générative dans son lecteur vidéo open-source. Le plus célèbre des logiciels français prépare un générateur automatique de sous-titres capable de parler plusieurs dizaines de langues.
