---
site: 01net.
title: "Lucie, la catastrophique IA française ferme sous une pluie de moqueries"
author: Florian Bayard
date: 2025-01-27
href: https://www.01net.com/actualites/lucie-lia-francaise-catastrophe-ferme-pluie-moqueries.html
featured_image: https://www.01net.com/app/uploads/2025/01/lucie-ia-fiasco-ferme-1360x907.jpg
tags:
- Sciences
series:
- 202505
series_weight: 0
---

> Lucie, l'IA générative open source censée améliorer la compétitivité de la France, n'a pas convaincu les internautes. Suite à ses nombreuses erreurs, l'IA a essuyé une vague de moqueries sur les réseaux sociaux. La plateforme a été fermée dans l'urgence au cours du week-end. Les concepteurs plaident une erreur de communication.
