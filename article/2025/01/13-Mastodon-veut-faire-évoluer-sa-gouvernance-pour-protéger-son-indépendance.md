---
site: Next
title: "Mastodon veut faire évoluer sa gouvernance pour protéger son indépendance"
author: Alexandre Laurent
date: 2025-01-13
href: https://next.ink/165145/mastodon-veut-faire-evoluer-sa-gouvernance-pour-proteger-son-independance/
featured_image: https://next.ink/wp-content/uploads/2024/04/mastodon-1986.jpg
tags:
- Internet
series:
- 202503
series_weight: 0
---

> Mastodon annonce une évolution sous six mois de sa gouvernance, avec la volonté de se placer sous la protection d'une nouvelle organisation à but non lucratif localisée en Europe. Le réseau social décentralisé affiche dans le même temps la volonté de muscler ses opérations et lance un appel à soutien pour boucler un budget prévisionnel 2025 fixé à 5 millions d'euros.
