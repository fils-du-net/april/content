---
site: Journal du Net
title: "Le fair source: nouvelle alternative pour éviter les pièges de l'open source"
date: 2025-01-15
href: https://www.journaldunet.com/cloud/1538071-le-fair-source-nouvelle-alternative-pour-eviter-les-pieges-de-l-open-source
featured_image: https://img-0.journaldunet.com/yRNwxfQXZeC3GGulQSn9SoJznbo=/1500x/smart/a3ebd7d14ae5488986a636c86013e2b2/ccmcms-jdn/39515034.jpeg
tags:
- Licenses
series:
- 202503
series_weight: 0
---

> Le fair source: nouvelle alternative pour éviter les pièges de l'open source Lancé par l'éditeur américain Sentry cet été, le fair source a été adopté par toute une myriade de start-up. Son principal bénéfice est de clarifier la position des entreprises mixant propriétaire et open source.
