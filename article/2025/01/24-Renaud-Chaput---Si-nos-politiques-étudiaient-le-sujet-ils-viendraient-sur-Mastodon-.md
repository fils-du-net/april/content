---
site: Next
title: "Renaud Chaput: «Si nos politiques étudiaient le sujet, ils viendraient sur Mastodon» (€)"
description: Confiance décentralisée
author: Alexandre Laurent
date: 2025-01-24
href: https://next.ink/166950/renaud-chaput-si-nos-politiques-etudiaient-le-sujet-ils-viendraient-sur-mastodon
featured_image: https://next.ink/wp-content/uploads/2025/01/portrait-mastodon.webp
tags:
- Internet
series:
- 202504
---

> Alors que le débat fait rage autour des politiques de modération des réseaux sociaux américains, le projet open source Mastodon a annoncé l'évolution prochaine de sa gouvernance. Il affiche l'ambition de s'imposer comme une alternative durable et éthique aux grandes plateformes privées, de X à Meta en passant par l'étoile montante Bluesky. Comment compte-t-il y parvenir? Next fait le point avec Renaud Chaput, directeur technique du projet.
