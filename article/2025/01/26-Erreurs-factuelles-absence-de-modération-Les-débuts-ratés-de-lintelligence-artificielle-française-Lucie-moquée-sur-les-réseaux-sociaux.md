---
site: francetv info
title: "Erreurs factuelles, absence de modération... Les débuts ratés de l'intelligence artificielle française Lucie, moquée sur les réseaux sociaux"
author: Linh-Lan Dao
date: 2025-01-26
href: https://www.francetvinfo.fr/internet/intelligence-artificielle/erreurs-factuelles-absence-de-moderation-les-debuts-rates-de-l-intelligence-artificielle-francaise-lucie-moquee-sur-les-reseaux-sociaux_7037849.html
featured_image: https://www.francetvinfo.fr/pictures/mIxkKExSDb3MmTd0Y6yimdeRwgY/0x25:1024x600/1328x747/filters:format(avif):quality(50)/2025/01/26/23417493b5be7ff1bc426c1a0a696fea699286ce1521523e5293bc56fe0569f6-67967674ed922682013718.webp
tags:
- Sciences
series:
- 202505
---

> L'IA générative tricolore est fortement critiquée par les internautes. Les cofondateurs plaident une erreur de communication.
