---
site: Silicon
title: "La Chine veut livrer un processeur Open Source cette année"
author: Matthieu Broersma
date: 2025-01-08
href: https://www.silicon.fr/Thematique/open-source-1375/Breves/chine-processeur-open-source-annee-466336.htm
featured_image: https://cdn.edi-static.fr/image/upload/c_lfill,h_245,w_470/e_unsharp_mask:100,q_auto/f_auto/v1/Img/BREVE/2025/1/466336/chine-veut-livrer-processeur-open-source-cette-annee-LE.jpg
tags:
- International
- Matériel libre
series:
- 202502
series_weight: 0
---

> Le directeur de l'académie de recherche du gouvernement chinois promet un processeur haute performance basée sur la norme open source RISC-V.
