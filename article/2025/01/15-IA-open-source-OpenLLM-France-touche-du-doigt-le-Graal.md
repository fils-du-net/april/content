---
site: LeMagIT
title: "IA open source: OpenLLM-France touche du doigt le Graal (€)"
author: Gaétan Raoul
date: 2025-01-15
href: https://www.lemagit.fr/actualites/366617846/IA-open-source-OpenLLM-France-touche-du-doigt-le-Graal
featured_image: https://www.lemagit.fr/visuals/German/Ai-KI-Human-hand-touching-robot-finger-Xv-Adobe.jpg
tags:
- Sciences
series:
- 202503
---

> Le consortium OpenLLM France dit avoir réussi à entraîner une collection de LLM respectueuse de la définition de l’IA open source par l’Open Source Initiative, tout en tentant de dépasser ce cadre. Un défi de taille quand il faut réunir des milliers de milliards de mots (tokens).
