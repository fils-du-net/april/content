---
site: ZDNET
title: "Expliquer le Libre: le dessin animé «Ada et Zangemann» en version française"
author: Thierry Noisette
date: 2025-01-25
href: https://www.zdnet.fr/blogs/l-esprit-libre/expliquer-le-libre-le-dessin-anime-ada-et-zangemann-en-version-francaise-405164.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2025/01/ada-et-zangemann-film.png
tags:
- Sensibilisation
series:
- 202504
series_weight: 0
---

> Le dessin animé adapté de l'album jeunesse existe à présent en VF, une belle ressource pour initier aux principes des libertés informatiques.
