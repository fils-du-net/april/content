---
site: Next
title: "La définition de l'IA ouverte divise encore le milieu"
description: "La bataille se rejoue"
author: Martin Clavey
date: 2025-01-06
href: https://next.ink/163941/la-definition-de-lia-ouverte-divise-encore-le-milieu
featured_image: https://next.ink/wp-content/uploads/2024/06/Openwashing.webp
tags:
- Sciences
- april
series:
- 202502
series_weight: 0
---

> En octobre dernier, l'Open Source Initiative a publié la version 1.0 de sa définition de l'IA ouverte, mais des acteurs du milieu du logiciel libre n'en sont pas satisfaits. Ils s'organisent autour de Sam Johnston pour peser sur la définition qui sera utilisée en Europe lors du AI Action Summit organisé par l'Élysée les 10 et 11 février.
