---
site: Le Figaro
title: "Luc Ferry: «Le danger mortel de l'open source et des deepfakes» (€)"
author: Luc Ferry
date: 2025-01-29
href: https://www.lefigaro.fr/vox/societe/luc-ferry-le-danger-mortel-de-l-open-source-et-des-deepfakes-20250129
featured_image: https://i.f1g.fr/media/cms/704x396_cropupscale/2025/01/29/4df12fcbc2cdbbdff185f32d8309adadfdb32967c5f20fcf7fa728c065d83325.jpg
tags:
- Sciences
series:
- 202505
---

> CHRONIQUE - Mettre les avancées de l'IA en accès libre laisse l'opportunité aux terroristes, comme au premier imbécile venu, de détourner le meilleur des projets pour fabriquer des armes de destruction massive.
