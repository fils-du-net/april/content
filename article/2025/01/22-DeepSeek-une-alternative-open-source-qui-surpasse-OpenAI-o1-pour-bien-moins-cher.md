---
site: ZDNET
title: "DeepSeek, une alternative open source qui surpasse OpenAI o1 pour bien moins cher"
author: Radhika Rajkumar
date: 2025-01-22
href: https://www.zdnet.fr/actualites/deepseek-une-alternative-open-source-qui-surpasse-openai-o1-pour-une-fraction-du-cout-404967.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2025/01/deepseek.jpg
tags:
- Sciences
series:
- 202504
series_weight: 0
---

> Une victoire pour l'open source? Avec son nouveau modèle R1, DeeSeek parvient à concurrence OpenAI o1 tout en proposant un tarif plus accessible.
