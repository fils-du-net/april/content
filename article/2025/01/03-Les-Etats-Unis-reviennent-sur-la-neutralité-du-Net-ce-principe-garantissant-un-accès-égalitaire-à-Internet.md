---
site: Le Monde.fr
title: "Les Etats-Unis reviennent sur la neutralité du Net, ce principe garantissant un accès égalitaire à Internet"
date: 2025-01-03
href: https://www.lemonde.fr/pixels/article/2025/01/03/les-etats-unis-reviennent-en-arriere-sur-le-principe-de-la-neutralite-du-net_6479575_4408996.html
tags:
- Neutralité du Net
series:
- 202501
---

> Une cour d’appel américaine a jugé infondée une décision prise par l’agence américaine des télécoms, qui garantissait que les fournisseurs d’accès n’ont pas le droit de moduler la vitesse de débit en fonction des contenus.
