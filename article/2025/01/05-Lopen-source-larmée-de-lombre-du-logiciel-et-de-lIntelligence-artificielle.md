---
site: Le Monde.fr
title: "L'open source, l'armée de l'ombre du logiciel… et de l'Intelligence artificielle (€)"
author: Sophy Caulier
date: 2025-01-05
href: https://www.lemonde.fr/economie/article/2025/01/05/l-open-source-l-armee-de-l-ombre-du-logiciel-et-de-l-intelligence-artificielle_6482931_3234.html
featured_image: https://img.lemde.fr/2025/01/03/115/0/5631/2815/2048/1024/45/0/f84e113_1735918522375-000-34kf3tm.jpg
tags:
- Sciences
series:
- 202501
series_weight: 0
---

> Alors que le raz de marée de l'IA repose largement sur eux, les «logiciels libres» souffrent d'un manque de visibilité et de reconnaissance en dehors du cercle des initiés. Malgré l'attrait qu'ils suscitent chez les géants du Web, leur avenir dépend en grande partie d'une communauté engagée, mais fragilisée.
