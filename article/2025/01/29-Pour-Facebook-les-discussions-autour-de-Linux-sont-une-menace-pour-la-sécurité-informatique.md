---
site: clubic.com
title: "Pour Facebook, les discussions autour de Linux sont une menace pour la sécurité informatique"
author: Mélina Loupia
date: 2025-01-29
href: https://www.clubic.com/actualite-551843-pour-facebook-les-discussions-autour-de-linux-sont-une-menace-pour-la-securite-informatique.html
featured_image: https://pic.clubic.com/c9d217472274536/1280x854/smart/linux.webp
tags:
- Promotion
- Internet
series:
- 202505
series_weight: 0
---

> Depuis le 19 janvier 2025, Facebook bloque les publications et les groupes mentionnant Linux. La plateforme considère ces contenus comme des menaces pour la cybersécurité. En conséquence, certains comptes subissent des restrictions, et des messages disparaissent. Cette politique affecte notamment DistroWatch, un site spécialisé dans l'actualité des systèmes d'exploitation open source.
