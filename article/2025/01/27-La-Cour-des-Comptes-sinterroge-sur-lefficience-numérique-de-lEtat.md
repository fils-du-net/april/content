---
site: Républik IT Le Média
title: "La Cour des Comptes s'interroge sur l'efficience numérique de l'Etat"
author: Bertrand Lemaire
date: 2025-01-27
href: https://www.republik-it.fr/decideurs-it/gouvernance/la-cour-des-comptes-s-interroge-sur-l-efficience-numerique-de-l-etat.html
featured_image: https://geimage.republiknews.fr/image/cms/585cc5e55b8a6fb17f587dfdf40f3fa9/cour-comptes-publie-rapport-numerique-etat.jpg?fm=browser&fill=auto&crop=0%2C0%2C1500%2C844&w=620&h=349&s=e7afab738a9f3c51833e8ce5d6c11ab0
tags:
- april
- Entreprise
- Administration
series:
- 202505
---

> Le 21 janvier 2025, la Cour des Comptes a publié un rapport critique sur les gains de productivité de l'État issus du numérique.
