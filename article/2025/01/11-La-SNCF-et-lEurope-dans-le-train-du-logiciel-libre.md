---
site: ZDNET
title: "La SNCF et l'Europe dans le train du logiciel libre"
author: Thierry Noisette
date: 2025-01-11
href: https://www.zdnet.fr/blogs/l-esprit-libre/la-sncf-et-leurope-dans-le-train-du-logiciel-libre-404319.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2025/01/tgv_-_panoramio.jpg
tags:
- Europe
- Entreprise
series:
- 202502
series_weight: 0
---

> Le groupe SNCF, qui réalise et collabore à plusieurs logiciels libres dont son jumeau numérique Open Source Railway Designer (OSRD), présente l'association Open Rail, née en 2024.
