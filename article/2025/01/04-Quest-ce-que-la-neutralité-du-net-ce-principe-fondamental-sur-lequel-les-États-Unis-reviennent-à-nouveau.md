---
site: RFI
title: "Qu'est-ce que la neutralité du net, ce principe fondamental sur lequel les États-Unis reviennent à nouveau?"
author: Aurore Lartigue
date: 2025-01-04
href: https://www.rfi.fr/fr/monde/20250104-qu-est-ce-que-la-neutralit%C3%A9-du-net-ce-principe-fondamental-sur-lequel-les-%C3%A9tats-unis-reviennent-%C3%A0-nouveau
featured_image: https://s.rfi.fr/media/display/f1e0635e-0386-11ef-a7ca-005056bfb2b6/w:980/p:16x9/2024-04-25T165325Z_1320117352_RC2RD7AZEB9N_RTRMADP_3_USA-INTERNET-NET-NEUTRALITY.JPG
tags:
- Neutralité du Net
series:
- 202501
series_weight: 0
---

> Une cour d'appel américaine a mis fin, jeudi 2 janvier, au principe de «neutralité du net», censé garantir un accès égalitaire à internet. RFI fait le point sur les enjeux de cette notion au cœur du monde numérique.
