---
site: Mac4Ever
title: "Facebook a mené une courte guerre contre Linux"
author: Vincent Lautier
date: 2025-01-30
href: https://www.mac4ever.com/web/186830-facebook-a-mene-une-courte-guerre-contre-linux
featured_image: https://img-api.mac4ever.com/800/0/ef533ccb18_facebook-a-mene-une-courte-guerre-contre-linux.webp
tags:
- Internet
series:
- 202505
---

> Mauvais délire pour les utilisateurs de Linux: Facebook a récemment bloqué des publications qui évoquent le système d'exploitation, les classant…
