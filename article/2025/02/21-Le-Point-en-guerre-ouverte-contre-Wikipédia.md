---
site: Le Monde.fr
title: "«Le Point» en guerre ouverte contre Wikipédia (€)"
author: Michaël Szadkowski, Brice Laemle
date: 2025-02-21
href: https://www.lemonde.fr/pixels/article/2025/02/21/le-point-en-guerre-ouverte-contre-wikipedia_6557619_4408996.html
featured_image: https://img.lemde.fr/2025/02/21/0/0/3000/2000/800/0/75/0/15519ab_sirius-fs-upload-1-m0qcrgfp7rrw-1740136310297-wiki-lepoint.jpg
tags:
- Partage du savoir
series:
- 202508
---

> L’hebdomadaire, mécontent de sa présentation sur l’encyclopédie en ligne, a adressé une mise en demeure et lancé une pétition. Les contributeurs francophones de Wikipédia protestent, de leur côté, contre certaines méthodes éditoriales de la rédaction.
