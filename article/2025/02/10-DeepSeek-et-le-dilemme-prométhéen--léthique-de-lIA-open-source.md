---
site: Les Echos
title: "DeepSeek et le dilemme prométhéen: l'éthique de l'IA open source"
author: Bianca Nobilo
date: 2025-02-10
href: https://www.lesechos.fr/idees-debats/cercle/opinion-deepseek-et-le-dilemme-prometheen-lethique-de-lia-open-source-2147783
featured_image: https://media.lesechos.com/api/v1/images/view/67aa136eb9b34851f3712f5c/1024x576-webp/0140704048490-web-tete.webp
tags:
- Sciences
series:
- 202507
---

> Si elle démocratise l'accès aux outils d'intelligence artificielle, l'IA open source pose aussi la question des mécanismes de contrôle, de la responsabilité juridique et des risques éthiques, prévient Bianca Nobilo chez IFS.
