---
site: Journal du Net
title: "IA et retour sur investissement : pourquoi l'open source accélère la valeur pour les entreprises"
author: Alexandra Ruez
date: 2025-02-18
href: https://www.journaldunet.com/intelligence-artificielle/1539261-ia-et-retour-sur-investissement-pourquoi-l-open-source-accelere-la-valeur-pour-les-entreprises/
tags:
- Sciences
series:
- 202508
---

> Alors que le déploiement de l'IA d'entreprise s'accélère en 2025, une étude révèle que les entreprises européennes privilégient une vision à long terme, misant notamment sur l'IA open source pour optimiser leurs investissements et stimuler l'innovation.
