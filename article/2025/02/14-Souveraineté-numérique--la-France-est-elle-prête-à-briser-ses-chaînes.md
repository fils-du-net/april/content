---
site: Silicon
title: "Souveraineté numérique: la France est-elle prête à briser ses chaînes?"
author: Antoine Brenner
date: 2025-02-14
href: https://www.silicon.fr/Thematique/open-source-1375/Breves/tribune-expert-souverainete-numerique-france-est-prete-chaines-467715.htm
featured_image: https://cdn.edi-static.fr/image/upload/c_lfill,h_245,w_470/e_unsharp_mask:100,q_auto/f_auto/v1/Img/BREVE/2025/2/467715/tribune-expert-souverainete-numerique-france-est-prete-briser-chaines-LE.jpg
tags:
- International
series:
- 202507
series_weight: 0
---

> Plutôt que de satisfaire d'une indépendance d'apparat, la France doit faire de l'open source un axe stratégique, conditionner ses investissements publics à l'usage de technologies ouvertes et inscrire l'indépendance numérique au coeur de l'éducation. Faute de choix clairs et volontaristes, toute ambition de souveraineté restera illusoire.
