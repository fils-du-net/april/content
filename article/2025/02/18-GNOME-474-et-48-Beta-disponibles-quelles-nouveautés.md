---
site: Next
title: "GNOME 47.4 et 48 Beta disponibles: quelles nouveautés?"
description: "X11 s'éloigne encore un peu"
author: Vincent Hermann
date: 2025-02-18
href: https://next.ink/171082/gnome-47-4-et-48-beta-disponibles-quelles-nouveautes
featured_image: https://next.ink/wp-content/uploads/2025/02/GNOME.png
tags:
- Sensibilisation
series:
- 202508
series_weight: 0
---

> Durant le week-end, l’équipe de développement de GNOME a publié coup sur coup deux versions. La première, stable, contient des améliorations pour l’actuelle branche 47 et est diffusée sur l’ensemble des distributions Linux l’utilisant. L’autre, en bêta, représente la prochaine branche majeure et contient des nouveautés plus importantes.
