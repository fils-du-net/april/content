---
site: clubic.com
title: "DeepSeek continue de bousculer l'IA en partageant plus largement ses modèles"
author: Samir Rahmoune
date: 2025-02-21
href: https://www.clubic.com/actualite-554550-deepseek-continue-de-bousculer-l-ia-en-partageant-plus-largement-ses-modeles.html
featured_image: https://pic.clubic.com/4bf22ea82273678/1200x800/smart/deepseek-ia-chinoise-chatbot.jpg
tags:
- Sciences
series:
- 202508
series_weight: 0
---

> La start-up chinoise continue de faire son trou dans le monde des grandes IA. Et elle se distingue encore par son engagement pour l'open source.
