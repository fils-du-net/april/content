---
site: Le Figaro.fr
title: "Débat IA/open source: le patron d'OpenAI (ChatGPT) pense être du «mauvais côté de l'histoire»"
date: 2025-02-01
href: https://www.lefigaro.fr/flash-eco/debat-ia-open-source-le-patron-d-openai-chatgpt-pense-etre-du-mauvais-cote-de-l-histoire-20250201
featured_image: https://i.f1g.fr/media/cms/704x396_cropupscale/2025/02/01/6f98c04bcd8ef50c33bc7adeb20c3abf2fb8a16e0693cf74ba580d9c3c02fd90.jpg
tags:
- Innovation
- Sciences
series:
- 202505
series_weight: 0
---

> Le patron d'OpenAI (ChatGPT) a estimé vendredi que son entreprise est du «mauvais côté de l'Histoire» parce que son modèle d'intelligence artificielle (IA) n'est pas «open source» (ouvert), un débat relancé cette semaine par le succès du nouvel agent conversationnel de la start-up chinoise DeepSeek. «Personnellement, je pense que nous avons été du mauvais côté de l'Histoire et que nous devons trouver une stratégie différente en matière d'open source», a déclaré Sam Altman lors d'une séance de questions-réponses sur un forum Reddit.
