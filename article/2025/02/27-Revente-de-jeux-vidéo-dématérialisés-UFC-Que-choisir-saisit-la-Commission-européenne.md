---
site: Le Monde.fr
title: "Revente de jeux vidéo dématérialisés: UFC-Que choisir saisit la Commission européenne"
date: 2025-02-27
href: https://www.lemonde.fr/pixels/article/2025/02/27/revente-de-jeux-video-dematerialises-ufc-que-choisir-saisit-la-commission-europeenne_6567231_4408996.html
featured_image: https://img.lemde.fr/2024/07/26/0/0/4928/3280/800/0/75/0/b05430a_1722004887633-pns-4811785.jpg
tags:
- Droit d'auteur
series:
- 202509
---

> L’association de consommateur reproche aux magistrats français de ne pas avoir saisi la Cour de justice de l’Union européenne. La procédure vise à contraindre l’américain Valve à autoriser la revente de jeux d’occasion dématérialisés sur sa boutique en ligne.
