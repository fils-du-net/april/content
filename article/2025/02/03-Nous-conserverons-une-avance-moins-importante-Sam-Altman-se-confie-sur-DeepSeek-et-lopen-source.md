---
site: clubic.com
title: "'Nous conserverons une avance moins importante': Sam Altman se confie sur DeepSeek et l'open source"
author: Samir Rahmoune
date: 2025-02-03
href: https://www.clubic.com/actualite-552212-nous-conserverons-une-avance-moins-importante-sam-altman-se-confie-sur-deepseek-et-l-open-source.html
featured_image: https://pic.clubic.com/a422abf92164204/1600x1068/smart/sam-altman-et-openai-c-a-matche-de-nouveau-messrro-shutterstock-com.webp
tags:
- Sciences
series:
- 202506
---

> Sam Altman s'est confié sur le défi représenté par DeepSeek. Et il reste confiant sur le rôle de leader que pourra assurer OpenAI à l'avenir.
