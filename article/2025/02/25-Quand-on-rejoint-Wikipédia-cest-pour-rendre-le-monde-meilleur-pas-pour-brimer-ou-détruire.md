---
site: L'OBS
title: "“Quand on rejoint Wikipédia, c'est pour rendre le monde meilleur, pas pour brimer ou détruire”"
author: Samuel Le Goff
date: 2025-02-25
href: https://www.nouvelobs.com/economie/20250225.OBS100775/quand-on-rejoint-wikipedia-c-est-pour-rendre-le-monde-meilleur-pas-pour-brimer-ou-detruire.html
tags:
- Partage du savoir
- Internet
series:
- 202509
series_weight: 0
---

> Sous le feu des critiques d'Elon Musk, l'encyclopédie en ligne est maintenant attaquée en France. Des critiques qui devraient inquiéter tous les défenseurs des libertés, estime Samuel Le Goff, ancien président de Wikimédia France.
