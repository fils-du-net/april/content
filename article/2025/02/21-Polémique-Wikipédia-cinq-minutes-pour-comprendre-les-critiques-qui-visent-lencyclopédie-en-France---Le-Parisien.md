---
site: leParisien.fr
title: "Polémique Wikipédia: cinq minutes pour comprendre les critiques qui visent l'encyclopédie en France - Le Parisien"
date: 2025-02-21
href: https://www.leparisien.fr/societe/polemique-wikipedia-cinq-minutes-pour-comprendre-les-critiques-qui-visent-lencyclopedie-en-france-21-02-2025-I2PKHUSHEREY7OIDBCK727O3X4.php
featured_image: https://www.leparisien.fr/resizer/l_7fPspaUQQ4N1alcP3Keu59vvY=/932x582/cloudfront-eu-central-1.images.arcpublishing.com/leparisien/HU4PIYU55BL4JVJLP6Y4EZS7LI.jpg
tags:
- Partage du savoir
series:
- 202508
series_weight: 0
---

> Le Point, magazine hebdomadaire d'actualité français, a déclenché une guerre médiatique contre Wikipédia. Ils s'accusent mutuellement, l'un d'intimidation, l'autre de désinformation.
