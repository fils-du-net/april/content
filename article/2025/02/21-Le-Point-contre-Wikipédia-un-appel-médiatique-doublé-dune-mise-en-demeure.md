---
site: Next
title: "Le Point contre Wikipédia: un appel médiatique doublé d'une mise en demeure"
description: "Vox populi"
author: Alexandre Laurent
date: 2025-02-21
href: https://next.ink/172030/le-point-contre-wikipedia-un-appel-mediatique-double-dune-mise-en-demeure
featured_image: https://next.ink/wp-content/uploads/2024/01/Liberte-expression-responsabilite1.webp
tags:
- Partage du savoir
series:
- 202508
---

> Dénonçant une campagne de dénigrement «méticuleusement coordonnée» à son encontre, l'hebdomadaire Le Point sonne la charge contre Wikipédia, avec un appel à plus de neutralité, cosigné par 70 personnalités. Le magazine a dans le même temps adressé une mise en demeure à la Fondation Wikimedia, arguant de l'absence de moyen d'action permettant à une personne visée par un contenu malveillant d’en demander la suppression.
