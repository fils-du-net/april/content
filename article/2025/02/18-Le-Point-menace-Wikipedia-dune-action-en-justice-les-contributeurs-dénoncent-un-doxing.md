---
site: Next
title: "Le Point menace Wikipedia d'une action en justice, les contributeurs dénoncent un doxing"
description: "Affaire.dox"
author: Alexandre Laurent
date: 2025-02-18
href: https://next.ink/171340/le-point-menace-wikipedia-dune-action-en-justice-les-contributeurs-denoncent-un-doxing
featured_image: https://next.ink/wp-content/uploads/2024/03/wiki.webp
tags:
- Partage du savoir
series:
- 202508
---

> Près de 600 contributeurs Wikipedia dénoncent dans une lettre ouverte les «menaces» adressées à l'un d'entre eux par un journaliste du Point. Auteur d'une récente enquête à charge contre l'encyclopédie, il n'avait pas apprécié certaines modifications apportées à la page consacrée au magazine qui l'emploie. Mardi, il a publiquement exposé le contributeur en question dans un nouvel article et affirme que le Point prépare une action en justice.
