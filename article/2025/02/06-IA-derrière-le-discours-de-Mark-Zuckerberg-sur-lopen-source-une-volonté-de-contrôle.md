---
site: ZDNET
title: "IA: derrière le discours de Mark Zuckerberg sur l'open source, une volonté de contrôle?"
author: Steven Vaughan-Nichols
date: 2025-02-06
href: https://www.zdnet.fr/actualites/ia-derriere-le-discours-de-mark-zuckerberg-sur-lopen-source-une-volonte-de-controle-405834.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2025/02/zuckerberg-meta-750x410.jpg
tags:
- Entreprise
- Sciences
series:
- 202506
series_weight: 0
---

> Pour les experts de l'open source, l'enjeu repose sur le respect des normes. Pour Meta, il s'agit avant tout d'une question d'argent.
