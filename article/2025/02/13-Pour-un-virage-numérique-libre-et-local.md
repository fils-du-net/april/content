---
site: LeDevoir.com
title: "Pour un virage numérique libre et local"
author: Jacques Berger
date: 2025-02-13
href: https://www.ledevoir.com/opinion/idees/843586/idees-virage-numerique-libre-local
featured_image: https://media1.ledevoir.com/images_galerie/nwd_1898779_1468723/image.jpg
tags:
- Institutions
- International
series:
- 202507
series_weight: 0
---

> Nous avons tout le talent nécessaire au Québec pour réaliser un tel projet.
