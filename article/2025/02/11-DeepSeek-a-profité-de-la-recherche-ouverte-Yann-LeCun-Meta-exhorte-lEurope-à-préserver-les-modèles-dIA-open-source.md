---
site: clubic.com
title: "'DeepSeek a profité de la recherche ouverte': Yann LeCun (Meta) exhorte l'Europe à préserver les modèles d'IA open source"
author: Samir Rahmoune
date: 2025-02-11
href: https://www.clubic.com/actualite-553188-deepseek-a-profite-de-la-recherche-ouverte-yann-lecun-meta-exhorte-l-europe-a-preserver-les-modeles-d-ia-open-source.html
featured_image: https://pic.clubic.com/0512ce562118145/1280x720/smart/yann-lecun.webp
tags:
- Sciences
series:
- 202507
---

> Le directeur scientifique de Meta conseille à l'Europe de ne pas mettre de bâtons dans les roues de l'open source. À moins de vouloir se faire distancier par les États-Unis et la Chine.
