---
site: Le Monde.fr
title: "Intelligence artificielle: la nécessité d'une troisième voie"
date: 2025-02-14
href: https://www.lemonde.fr/idees/article/2025/02/12/intelligence-artificielle-la-necessite-d-une-troisieme-voie_6543393_3232.html
featured_image: https://img.lemde.fr/2025/02/12/0/0/7552/5035/180/0/95/0/2b2adf4_sirius-fs-upload-1-gaepn31dfvxg-1739356506924-lesvictoiresdelamusique2025-la40c3a8mecc3a9rc3a9monie-lesrc3a9vc3a9lations.jpg
tags:
- Sciences
series:
- 202507
---

> ÉDITORIAL. Comme cela a été esquissé lors du sommet organisé lundi et mardi à Paris, l'Union européenne doit proposer des solutions alternatives au modèle américain, en misant sur l'IA en source ouverte, sur la frugalité énergétique, la sécurité et la transparence des systèmes.
