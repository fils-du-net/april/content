---
site: Les Echos
title: "Pourquoi l'open source est devenu un enjeu majeur dans l'IA (€)"
date: 2025-02-22
href: https://www.lesechos.fr/tech-medias/intelligence-artificielle/pourquoi-lopen-source-est-devenu-un-enjeu-majeur-dans-lia-2150300
featured_image: https://media.lesechos.com/api/v1/images/view/67b9bfd2831ed907d946a44a/1024x576-webp/01401099047380-web-tete.webp
tags:
- Sciences
series:
- 202508
---

> Le qualificatif d'IA «open source» est sur toutes les lèvres, mais demeure très débattu dans la communauté. D'immenses enjeux financiers et concurrentiels sont sur le tapis.
