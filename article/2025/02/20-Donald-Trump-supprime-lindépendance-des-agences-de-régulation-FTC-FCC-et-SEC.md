---
site: Next
title: "Donald Trump supprime l'indépendance des agences de régulation FTC, FCC et SEC"
description: "King Donald the 1st"
author: Martin Clavey
date: 2025-02-20
href: https://next.ink/171860/donald-trump-supprime-lindependance-des-agences-de-regulation-americaines-ftc-fcc-et-sec
featured_image: https://next.ink/wp-content/uploads/2024/11/Trump-usa.webp
tags:
- International
series:
- 202508
series_weight: 0
---

> Les agences FTC, FCC et SEC régulent les marchés américains et surveillent notamment les entreprises américaines du numérique. Elles opéraient jusque ici de manière indépendante du pouvoir exécutif fédéral américain. Un décret de Donald Trump publié le 18 février dernier veut leur imposer une supervision présidentielle.
