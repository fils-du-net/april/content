---
site: ZDNET
title: "Elon Musk prône l'IA open source, alors pourquoi son modèle Grok reste-t-il fermé?"
author: Tiernan Ray
date: 2025-02-19
href: https://www.zdnet.fr/actualites/elon-musk-prone-lia-open-source-alors-pourquoi-son-modele-grok-reste-t-il-ferme-406618.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/02/gettyimages-1238365889-elon-musk-space2-scaled.jpg
tags:
- Sciences
series:
- 202508
---

> Alors qu'il tente une prise de contrôle hostile sur OpenAI, au nom de l'humanité, Elon Musk ne s'est jusqu'à présent pas engagé à ouvrir le code source des propres modèles d'IA.
