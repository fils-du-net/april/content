---
site: ZDNET
title: "ZD Tech: ce qui est crucial dans l'IA open source ce sont les poids!"
author: Guillaume Serries
date: 2025-02-07
href: https://www.zdnet.fr/actualites/zd-tech-ce-qui-est-crucial-dans-lia-open-source-ce-sont-les-poids-405925.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2024/09/zdnet-podcast-bo.jpg
tags:
- Sciences
series:
- 202506
---

> L'Open Weight Definition veut clarifier ce que signifie vraiment une IA open source. Vous avez peut-être entendu parler des efforts pour définir l'IA open source, Mais des désaccords persistent.
