---
site: La Tribune
title: "Face à la menace DeepSeek, le patron d'OpenIA veut revenir à l'«open source»"
author: Carlos Barria
date: 2025-02-01
href: https://www.latribune.fr/technos-medias/internet/face-a-la-menace-deepseek-le-patron-d-openia-veut-revenir-a-l-open-source-1017293.html
featured_image: https://pictures.latribune.fr/cdn-cgi/image/width=2048,format=auto,quality=80/493/2270493.jpg
tags:
- Innovation
- Sciences
series:
- 202505
---

> Sam Altman a affirmé, vendredi, que son entreprise est du «mauvais côté de l'histoire» parce que ChatGPT n'est pas «open source». Une remise en question due à l'essor du chinois DeepSeek.
