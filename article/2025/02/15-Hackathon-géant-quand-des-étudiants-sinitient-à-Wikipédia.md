---
site: L'OBS
title: "Hackathon géant: quand des étudiants s'initient à Wikipédia"
author: Thierry Noisette
date: 2025-02-15
href: https://www.nouvelobs.com/societe/20250215.OBS100339/hackathon-geant-quand-des-etudiants-s-initient-a-wikipedia.html
featured_image: https://focus.nouvelobs.com/2025/02/13/384/0/4608/3072/1020/680/50/0/5f37f54_sirius-fs-upload-1-mpay65otskgb-1739466394744-salle-hackathon-20250127.jpg
tags:
- Partage du savoir
- Éducation
series:
- 202507
series_weight: 0
---

> Plus de 300 étudiants de l'école d'ingénieurs ENTPE ont planché pendant deux jours en créant ou enrichissant des articles de l'encyclopédie collaborative.
