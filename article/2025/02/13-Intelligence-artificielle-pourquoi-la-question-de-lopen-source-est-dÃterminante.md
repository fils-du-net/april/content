---
site: BFMtv
title: "Intelligence artificielle: pourquoi la question de l'open source est déterminante"
date: 2025-02-13
href: https://www.bfmtv.com/tech/intelligence-artificielle/intelligence-artificielle-pourquoi-la-question-de-l-open-source-est-determinante_AV-202502130583.html
featured_image: https://www.bfmtv.com/assets/v11/images/placeholder_lcp_video.da751f0f91185e0dec99b857d185b74b.webp
tags:
- Sciences
series:
- 202507
---

> Le développement d'IA open source, soit ouvertes et modifiables par tous, a été un des thèmes abordés lors du sommet sur l'IA. Une approche qui a déjà été adoptée par nombre d'entreprises et qui présente plusieurs avantages.
