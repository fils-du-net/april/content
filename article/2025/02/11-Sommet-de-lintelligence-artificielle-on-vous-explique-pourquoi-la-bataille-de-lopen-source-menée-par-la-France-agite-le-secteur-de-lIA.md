---
site: francetv info
title: "Sommet de l'intelligence artificielle: on vous explique pourquoi la bataille de 'l'open source' menée par la France agite le secteur de l'IA"
author: Luc Chagnon
date: 2025-02-11
href: https://www.francetvinfo.fr/internet/intelligence-artificielle/sommet-de-l-intelligence-artificielle-on-vous-explique-pourquoi-la-bataille-de-l-open-source-menee-par-la-france-agite-le-secteur-de-l-ia_7066532.html
featured_image: https://www.francetvinfo.fr/pictures/Pe0axACzzU1L-PgkDcj5ClTtBJA/1200x1200/2025/02/11/000-36xg9k4-67ab23c59311f119869160.jpg
tags:
- Sciences
series:
- 202507
series_weight: 0
---

> Contre les géants de l'intelligence artificielle comme OpenAI, Emmanuel Macron et les entreprises françaises soutiennent le développement d'IA ouvertes et modifiables par tous. De quoi partager plus largement les bénéfices liés à ces technologies, mais aussi les risques.
