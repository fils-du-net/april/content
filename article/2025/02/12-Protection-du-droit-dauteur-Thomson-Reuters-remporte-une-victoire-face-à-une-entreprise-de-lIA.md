---
site: Le Monde.fr
title: "Protection du droit d'auteur: Thomson Reuters remporte une victoire face à une entreprise de l'IA"
date: 2025-02-12
href: https://www.lemonde.fr/pixels/article/2025/02/12/protection-du-droit-d-auteur-thomson-reuters-remporte-une-victoire-face-a-une-entreprise-de-l-ia_6543611_4408996.html
featured_image: https://img.lemde.fr/2025/02/06/0/0/5760/3840/800/0/75/0/ac938d0_ftp-import-images-1-joav09i9mdmp-2025-02-06t095123z-407476072-rc2woca8tlzt-rtrmadp-3-thomsonreuters-results.JPG
tags:
- Droit d'auteur
- Sciences
series:
- 202507
series_weight: 0
---

> Le conglomérat de médias américain remporte une première victoire contre Ross Intelligence, une start-up spécialisée dans l’IA juridique. Le juge a rejeté l’argument d’usage loyal (« fair use ») avancé par l’entreprise.
