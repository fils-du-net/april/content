---
site: ZDNET
title: "Ces pays avec qui la collaboration open source est interdite aux Etats-Unis"
author: Thierry Noisette
date: 2025-02-02
href: https://www.zdnet.fr/blogs/l-esprit-libre/ces-pays-avec-qui-la-collaboration-open-source-est-interdite-aux-etats-unis-405589.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2025/02/barbeles_keep_out_11167455416_wmc-750x410.jpg
tags:
- International
series:
- 202506
---

> Chez les Américains, échanger avec un développeur d'un pays banni par les USA comme la Russie ou la Corée du Nord peut valoir une forte amende ou une peine de prison, avertit la fondation Linux.
