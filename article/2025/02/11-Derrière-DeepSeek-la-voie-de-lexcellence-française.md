---
site: Les Echos
title: "Derrière DeepSeek, la voie de l'excellence française"
author: Rémi Bourgeot
date: 2025-02-11
href: https://www.lesechos.fr/idees-debats/cercle/opinion-derriere-deepseek-la-voie-de-lexcellence-francaise-2148083
featured_image: https://media.lesechos.com/api/v1/images/view/67ab64e79c05ec0414185af4/1024x576-webp/0140800207041-web-tete.webp
tags:
- Sciences
series:
- 202507
---

> En s'appuyant sur sa culture mathématique et en tablant sur l'open source, dont DeepSeek a révélé le potentiel, l'Europe peut espérer atteindre les résultats obtenus en Chine ou aux Etats-Unis, plaide l'économiste et ingénieur Rémi Bourgeot.
