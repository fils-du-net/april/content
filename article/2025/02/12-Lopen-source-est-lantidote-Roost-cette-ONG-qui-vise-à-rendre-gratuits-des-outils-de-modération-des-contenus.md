---
site: Le Figaro
title: "«L'open source est l'antidote»: Roost, cette ONG qui vise à rendre gratuits des outils de modération des contenus"
author: Chloé Woitier
date: 2025-02-12
href: https://www.lefigaro.fr/secteur/high-tech/l-open-source-est-l-antidote-roost-cette-ong-qui-vise-a-rendre-gratuits-des-outils-de-moderation-des-contenus-20250211
featured_image: https://i.f1g.fr/media/cms/1194x804/2025/02/11/27dc25ffa3be1593480d4f59d0bcecc4866459d16dc115705347a18ce30c83d0.jpg
tags:
- Internet
series:
- 202507
series_weight: 0
---

> Soutenue par Google, OpenAI ou Discord et présidée par la chercheuse Camille François, cette fondation bâtit des outils de modération en open source dont pourront se servir gratuitement tous les éditeurs de services en ligne.
