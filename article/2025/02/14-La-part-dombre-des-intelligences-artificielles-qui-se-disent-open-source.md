---
site: Le Monde.fr
title: "La part d'ombre des intelligences artificielles qui se disent «open source» (€)"
author: Nicolas Six
date: 2025-02-14
href: https://www.lemonde.fr/pixels/article/2025/02/14/la-part-d-ombre-des-intelligences-artificielles-qui-se-disent-open-source_6546078_4408996.html
featured_image: https://img.lemde.fr/2025/02/10/0/0/7488/4994/800/0/75/0/a012d78_ftp-import-images-1-rsno8aaitfde-5996004-01-06.jpg
tags:
- Sciences
series:
- 202507
---

> Nombre d’éditeurs, de DeepSeek à Mistral, en passant par Meta, s’accordent sur l’importance de l’ouverture des intelligences artificielles. Mais le degré de transparence de leurs modèles laisse à désirer, illustrant la tension entre une approche réellement ouverte et le développement d’un produit commercial.
