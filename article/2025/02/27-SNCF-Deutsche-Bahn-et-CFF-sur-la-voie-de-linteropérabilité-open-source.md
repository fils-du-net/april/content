---
site: cio-online.com
title: "SNCF, Deutsche Bahn et CFF sur la voie de l'interopérabilité open source"
author: Jurgen Hill
date: 2025-02-27
href: https://www.cio-online.com/actualites/lire-sncf-deutsche-bahn-et-cff-sur-la-voie-de-l-interoperabilite-open-source-16184.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000021615.jpg
tags:
- Interopérabilité
- International
series:
- 202510
series_weight: 0
---

> Les chemins de fer européens cherchent des voies communes sur une plateforme open source, via une initiative appelée Openrail.
