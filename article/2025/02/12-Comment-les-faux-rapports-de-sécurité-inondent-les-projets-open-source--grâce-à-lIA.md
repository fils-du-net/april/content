---
site: ZDNET
title: "Comment les faux rapports de sécurité inondent les projets open-source, grâce à l'IA"
author: Steven Vaughan-Nichols
date: 2025-02-12
href: https://www.zdnet.fr/actualites/comment-les-faux-rapports-de-securite-inondent-les-projets-open-source-grace-a-lia-406296.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2025/02/ai-fakes-open-source-program-security-patches-and-feature-requests-750x410.webp
tags:
- Innovation
series:
- 202507
series_weight: 0
---

> Les spams de patch contiennent du code qui est carrément erroné et non fonctionnel. Pire encore: il peut introduire des vulnérabilités ou des portes dérobées. Alors, que doivent faire les développeurs?
