---
site: ZDNET
title: "Red Hat et l'intelligence artificielle open source: le pragmatisme plutôt que les rêves utopiques"
author: Steven Vaughan-Nichols
date: 2025-02-04
href: https://www.zdnet.fr/actualites/red-hat-et-lintelligence-artificielle-open-source-le-pragmatisme-plutot-que-les-reves-utopiques-405701.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2025/02/red-hat-ia.jpg
tags:
- Sciences
series:
- 202506
series_weight: 0
---

> Le géant de Linux envisage un développement de l'intelligence artificielle qui reflète l'éthique collaborative des logiciels open source. Ce ne sera pas facile.
