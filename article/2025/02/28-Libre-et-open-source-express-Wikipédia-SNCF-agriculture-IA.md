---
site: ZDNET
title: "Libre et open source express: Wikipédia, SNCF, agriculture, IA"
author: Thierry Noisette
date: 2025-02-28
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-wikipedia-sncf-agriculture-ia-407252.htm
featured_image: https://www.zdnet.fr/rw-placeholder/365/200/image.svg
tags:
- Partage du savoir
series:
- 202509
---

> Revue de presse et de web: Wikipédia ciblé par le magazine Le Point. Wikitok, une idée géniale. Alliance dans le rail. Convention entre l'INRAE et La Ferme digitale. IA libre?
