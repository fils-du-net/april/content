---
site: ZDNET
title: "Après son départ de X, les conseillers parisiens proposent que Paris crée un serveur mastodon"
author: Thierry Noisette
date: 2025-02-25
href: https://www.zdnet.fr/blogs/l-esprit-libre/apres-son-depart-de-x-les-conseillers-parisiens-proposent-que-paris-cree-un-serveur-mastodon-406984.htm
featured_image: https://www.zdnet.fr/wp-content/uploads/zdnet/2025/02/statue_liberte_ile_aux_cygnes_paris_2008-750x410.jpg
tags:
- Internet
series:
- 202509
series_weight: 0
---

> Soulignant l'évolution toxique de l'ex-Twitter et de Meta, les élus parisiens ont adopté un vœu des écologistes pour des alternatives aux réseaux sociaux de Musk et Zuckerberg.
