---
site: Next
title: "Rust dans le noyau Linux: nouvelles bisbilles, Linus Torvalds s'en mêle"
author: Vincent Hermann
date: 2025-02-10
href: https://next.ink/169944/rust-dans-le-noyau-linux-nouvelles-bisbilles-linus-torvalds-sen-mele/
featured_image: https://next.ink/wp-content/uploads/2024/02/Rust_programming_language-Logo.wine_.png
tags:
- Sensibilisation
series:
- 202507
series_weight: 0
---

> Le noyau Linux contient du code Rust depuis bientôt trois ans. De petits ajouts, qui ont surtout consisté pendant un temps à permettre à du code Rust d’être ajouté, notamment dans les pilotes. Mais la progression du langage dans le noyau reste complexe.
