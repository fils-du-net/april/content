---
site: Le Monde Informatique
title: "Un guide des pays sous sanctions internationales pour les développeurs open source"
author: Paul Krill
date: 2025-02-04
href: https://www.lemondeinformatique.fr/actualites/lire-un-guide-des-pays-sous-sanctions-internationales-pour-les-developpeurs-open-source-95960.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000100840.jpg
tags:
- International
series:
- 202506
series_weight: 0
---

> Plusieurs réglementations internationales et en particulier l'Ofac aux Etats-Unis imposent aux développeurs open source de ne pas travailler avec des pays touchés par des sanctions. La Fondation Linux a publié un guide pour détailler ces différents cadres et les risques.
