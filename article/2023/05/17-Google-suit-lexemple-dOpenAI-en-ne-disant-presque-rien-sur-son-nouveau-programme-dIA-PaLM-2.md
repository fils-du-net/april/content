---
site: ZDNet France
title: "Google suit l'exemple d'OpenAI en ne disant presque rien sur son nouveau programme d'IA PaLM 2"
author: Tiernan Ray
date: 2023-05-17
href: https://www.zdnet.fr/actualites/google-suit-l-exemple-d-openai-en-ne-disant-presque-rien-sur-son-nouveau-programme-d-ia-palm-2-39958394.htm
featured_image: https://www.zdnet.com/a/img/2023/05/16/e92293c9-d60a-486a-82a7-b8bfd22259ef/google-palm-2-graphic.jpg
tags:
- Sciences
series:
- 202320
---

> À l'instar d'OpenAI, le géant des moteurs de recherche revient sur des décennies de publication ouverte dans le domaine de la recherche sur l'IA.
