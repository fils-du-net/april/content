---
site: Numerama
title: "Quels sont les logiciels libres que l'État préconise en 2023?"
description: De nouveaux logiciels sont recensés
author: Julien Lausson
date: 2023-05-20
href: https://www.numerama.com/tech/1375274-quels-sont-les-logiciels-libres-que-letat-preconise-en-2023.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2017/03/open-source-linux-1024x686.jpg?webp=1&key=2a85d3ee
tags:
- Administration
series:
- 202320
series_weight: 0
---

> La liste des logiciels libres recommandés par État continue de grandir. Ils sont désormais 359 à être conseillés. En parallèle, le site web dédié a bénéficié d’une refonte bienvenue.
