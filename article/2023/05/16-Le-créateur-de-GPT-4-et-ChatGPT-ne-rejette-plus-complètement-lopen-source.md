---
site: Numerama
title: "Le créateur de GPT-4 et ChatGPT ne rejette plus complètement l'open source"
description: "Un revirement timide?"
author: Julien Lausson
date: 2023-05-16
href: https://www.numerama.com/tech/1376432-le-createur-de-gpt-4-et-chatgpt-ne-rejette-plus-completement-lopen-source.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2023/05/openai-1024x574.jpg?webp=1&key=fd2c60d9
tags:
- Sciences
- Licenses
series:
- 202320
---

> L’open source de retour en grâce chez OpenAI, la société derrière GPT-4 et ChatGPT? L’entreprise est attendue sur ce terrain avec l’ouverture d’un de ses modèles de langage les plus récents. Un revirement qui serait justement lié à la pression du secteur de l’open source.
