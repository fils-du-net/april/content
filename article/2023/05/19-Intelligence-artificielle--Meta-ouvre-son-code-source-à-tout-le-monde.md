---
site: La Presse
title: "Intelligence artificielle | Meta ouvre son code source à tout le monde"
author: Cade Metz et Mike Isaac
date: 2023-05-19
href: https://www.lapresse.ca/affaires/entreprises/2023-05-19/intelligence-artificielle/meta-ouvre-son-code-source-a-tout-le-monde.php
featured_image: https://mobile-img.lpcdn.ca/v2/924x/r3996/50d1057988263866b5cf329627dde4c2.webp
tags:
- Sciences
series:
- 202320
series_weight: 0
---

> En février, Meta a pris une décision inhabituelle dans le monde en pleine effervescence de l'intelligence artificielle (IA): elle a donné les joyaux de la couronne.
