---
site: Le Monde.fr
title: "La recherche au défi de la sobriété énergétique du numérique (€)"
author: David Larousserie
date: 2023-05-01
href: https://www.lemonde.fr/sciences/article/2023/05/01/la-recherche-au-defi-de-la-sobriete-energetique-du-numerique_6171677_1650684.html
featured_image: https://img.lemde.fr/2023/04/27/0/0/4919/4919/664/0/75/0/2c2ebb5_1682582467814-hup9973-08-15-cropped.jpg
tags:
- Sciences
series:
- 202318
series_weight: 0
---

> La quantité de gaz à effet de serre émise par le secteur du numérique augmente de 6 % par an. Une évolution inquiétante du bilan carbone de ce secteur qui fait débat au sein des laboratoires d'informatique, où les tenants de la recherche d'efficacité des systèmes s'opposent aux partisans d'une plus grande sobriété, donc d'une réduction des usages.
