---
site: Numerama
title: "HuggingChat: 4 questions sur l'alternative open source à ChatGPT"
description: HuggingChat, l'IA qui vous veut du bien
author: Benjamin Polge
date: 2023-05-04
href: https://www.numerama.com/tech/1365280-huggingchat-4-questions-sur-lalternative-open-source-a-chatgpt.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2023/05/robot-ia-fenetre-1024x576.jpg?webp=1&key=ef536b5a
tags:
- Sciences
series:
- 202318
series_weight: 0
---

> HuggingChat, un petit cousin franco-américain de ChatGPT, a de quoi surprendre. Développé par Hugging Face, le chatbot a la particularité de se baser sur un modèle open source. Une petite révolution dans le domaine.
