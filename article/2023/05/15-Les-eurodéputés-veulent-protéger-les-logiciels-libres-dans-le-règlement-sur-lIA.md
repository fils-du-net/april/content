---
site: ZDNet France
title: "Les eurodéputés veulent protéger les logiciels libres dans le règlement sur l'IA"
author: Thierry Noisette
date: 2023-05-15
href: https://www.zdnet.fr/blogs/l-esprit-libre/les-eurodeputes-veulent-proteger-les-logiciels-libres-dans-le-reglement-sur-l-ia-39958336.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/europe-cube_Pixabay.jpg
tags:
- Europe
- Innovation
series:
- 202320
series_weight: 0
---

> De bon augure avant l'examen  d'autres projets touchant aux logiciels libres? Deux commissions du Parlement européen ont voté un rapport préliminaire, qui prévoit des dérogations «pour les composants d'IA fournis sous des licences open source».
