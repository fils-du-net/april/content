---
site: Silicon
title: "Public Money Public Code: où en est cette campagne européenne pour le logiciel libre"
author: Clément Bohic
date: 2023-05-05
href: https://www.silicon.fr/public-money-public-code-campagne-europeenne-logiciel-libre-464585.html
featured_image: https://www.silicon.fr/wp-content/uploads/2023/05/public-money-public-code.jpg
tags:
- april
- Administration
- Europe
series:
- 202318
series_weight: 0
---

> À l'automne 2017, la FSFE lançait la campagne Public Money Public Code. Toujours active, qu'est-elle devenue?
