---
site: Next INpact
title: "«Dorlotons Dégooglisons»: Framasoft veut prendre soin de ses outils numériques (€)"
description: Mettre en lumière le travail invisible de soin informatique
author: Mathilde Saliou
date: 2023-05-24
href: https://www.nextinpact.com/article/71731/dorlotons-degooglisons-framasoft-veut-prendre-soin-ses-outils-numeriques
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/12674.jpg
tags:
- Associations
- Internet
series:
- 202321
series_weight: 0
---

> Le 23 mai, Framasoft a lancé une campagne de financement pour son projet « Dorlotons Dégooglisons », une initiative qui vise aussi bien à mettre à jour et améliorer ses outils phares qu’à rendre plus visibles les tâches essentielles et méconnues de maintien des produits informatiques.
