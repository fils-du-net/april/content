---
site: ouest-france.fr
title: "Le club informatique de Bannalec multiplie ses activités et ses formations"
date: 2023-05-25
href: https://www.ouest-france.fr/bretagne/bannalec-29380/le-club-informatique-de-bannalec-multiplie-ses-activites-et-ses-formations-b7d61eb4-fa46-11ed-848c-839499ee950f
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMzA1NWRlNTM4YzQ0NDllN2EyNDkyYWM5M2RhOGQ2MjAxMTY?width=940&focuspoint=48%2C24&cropresize=1&client_id=bpeditorial&sign=4a9e11abc69ff1def19d7ab9118e1ac392d36cafabf0faf79043753f246a0672
tags:
- Promotion
- Associations
series:
- 202321
series_weight: 0
---

> Les deux rendez-vous proposés par le club informatique L'@ssourie, le jour du marché, à Bannalec, dans le Finistère, n'ont pas attiré beaucoup d'amateurs, mais le club poursuit ses activités d'initiation et de formation tout au long de l'année.
