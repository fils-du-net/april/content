---
site: Clubic.com
title: "Mastodon simplifie son système d'inscription et améliore ses fonctionnalités"
author: Antoine Roche
date: 2023-05-02
href: https://www.clubic.com/pro/blog-forum-reseaux-sociaux/actualite-467847-mastodon-simplifie-son-systeme-d-inscription-et-ameliore-ses-fonctionnalites.html
featured_image: https://pic.clubic.com/v1/images/2102310/raw.webp?fit=max&width=1200&hash=9210d972562fe7b2f12e7eb4b6eb746c9328327c
tags:
- Internet
series:
- 202318
series_weight: 0
---

> Vous n'avez jamais rien compris à Mastodon dès l'étape d'inscription? Le réseau social décentralisé a décidé de simplifier un peu cela, mais aussi d'ajouter quelques fonctionnalités.
