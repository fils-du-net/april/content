---
site: Numerama
title: "Les moins de 15 ans exclus de Wikipédia? L'absurde faille d'une proposition de loi"
description: Il faut sauver le soldat Wikipédia
author: Julien Lausson
date: 2023-05-22
href: https://www.numerama.com/politique/1385006-les-moins-de-15-ans-exclus-de-wikipedia-labsurde-faille-dune-proposition-de-loi.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2023/05/senat-1024x576.jpg?avif=1&key=bba3d9e6
tags:
- Institutions
- Internet
series:
- 202321
series_weight: 0
---

> Le Parlement discute d’une proposition de loi qui fixe à 15 ans la majorité numérique. En dessous, les mineurs devront avoir l’accord de leurs parents pour s’inscrire sur un réseau social. Problème: la définition est si générale qu’elle touche aussi… Wikipédia.
