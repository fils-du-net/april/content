---
site: Presse Citron
title: "Pourquoi Google a bien plus peur de ces IA que de ChatGPT"
author: RPB
date: 2023-05-16
href: https://www.presse-citron.net/pourquoi-google-a-bien-plus-peur-de-ces-ia-que-de-chatgpt
featured_image: https://www.presse-citron.net/app/uploads/2023/05/bard-google-io.jpg
tags:
- Sciences
series:
- 202320
---

> Google pense que la guerre de l'IA est déjà perdue pour Alphabet... mais aussi OpenAI!
