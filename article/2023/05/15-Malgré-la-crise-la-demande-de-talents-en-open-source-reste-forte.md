---
site: Le Monde Informatique
title: "Malgré la crise, la demande de talents en open source reste forte"
author: Jacques Cheminat
date: 2023-05-15
href: https://www.lemondeinformatique.fr/actualites/lire-malgre-la-crise-la-demande-de-talents-en-open-source-reste-forte-90426.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000091893.jpg
tags:
- Entreprise
series:
- 202320
series_weight: 0
---

> Selon une étude menée par la Fondation Linux, l'emploi dans l'open source est soumis aux incertitudes économiques. Cependant, la recherche de certains profils demeure élevée. Face à constat, les entreprises investissent dans la formation et les certifications pour séduire et retenir les talents.
