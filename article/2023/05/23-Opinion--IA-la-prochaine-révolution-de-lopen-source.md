---
site: Les Echos
title: "Opinion | IA: la prochaine révolution de l'open source (€)"
author: Georges Nahon
date: 2023-05-23
href: https://www.lesechos.fr/idees-debats/cercle/opinion-ia-la-prochaine-revolution-de-lopen-source-1945848
featured_image: https://media.lesechos.com/api/v1/images/view/646ce651d1694f37e3292aeb/1280x720/090233834810-web-tete.jpg
tags:
- Sciences
series:
- 202321
series_weight: 0
---

> L'open source a modifié les priorités et apporté de nouvelles idées dans le domaine de l'intelligence artificielle, souligne Georges Nahon. Cette nouvelle révolution va profondément changer le visage du secteur.
