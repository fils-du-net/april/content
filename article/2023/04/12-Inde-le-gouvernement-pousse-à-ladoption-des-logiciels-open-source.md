---
site: ZDNet France
title: "Inde: le gouvernement pousse à l'adoption des logiciels open source"
author: Thierry Noisette
date: 2023-04-12
href: https://www.zdnet.fr/blogs/l-esprit-libre/inde-le-gouvernement-pousse-a-l-adoption-des-logiciels-open-source-39957002.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/Inde%20OSS%20Flickr(1).jpg
tags:
- International
- Administration
series:
- 202315
series_weight: 0
---

> La priorité à l'open source devrait devenir obligatoire pour les administrations indiennes.
