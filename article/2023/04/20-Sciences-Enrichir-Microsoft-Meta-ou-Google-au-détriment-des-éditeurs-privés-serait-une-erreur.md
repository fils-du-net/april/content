---
site: Le Monde.fr
title: "Sciences: «Enrichir Microsoft, Meta ou Google au détriment des éditeurs privés serait une erreur» (€)"
author: Vincent Montagne
date: 2023-04-20
href: https://www.lemonde.fr/idees/article/2023/04/20/sciences-enrichir-microsoft-meta-ou-google-au-detriment-des-editeurs-prives-serait-une-erreur_6170254_3232.html
tags:
- Droit d'auteur
- Sciences
series:
- 202316
---

> Avec l'envol des intelligences artificielles génératives, qui absorbe tous les contenus, la diffusion en «open access» de publications scientifiques, sans droits d'auteur, fragilise l'édition et la recherche, alerte, dans une tribune au «Monde», Vincent Montagne, président du Syndicat national de l'édition.
