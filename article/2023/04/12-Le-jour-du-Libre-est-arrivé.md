---
site: Le Café pédagogique
title: "Le jour du Libre est arrivé?"
author: Jean-Michel Le Baut
date: 2023-04-12
href: https://www.cafepedagogique.net/2023/04/12/le-jour-du-libre-est-arrive
featured_image: https://www.cafepedagogique.net/wp-content/uploads/2023/04/JDLE-partie-4-Conclusion-Table-ronde-768x576.jpg.webp
tags:
- Éducation
series:
- 202315
series_weight: 0
---

> Peut-on toujours penser que l’éducation n’est pas une marchandise mais un bien commun? Peut-on encore considérer que le numérique appartient à tous et toutes? Le 7 avril 2023 à Rennes, le couvent des Jacobins a été transformé en cathédrale du Libre éducatif. 400 participant·es ont partagé outils, pratiques et réflexions. L’événement a permis de démontrer et valoriser le dynamisme de  celles et ceux qui créent et diffusent des logiciels et ressources éducatives libres. Il a aussi ouvert des horizons pour favoriser dans l’Ecole la culture des communs, de la coopération, de la publication ouverte.
