---
site: Républik IT Le Média
title: "La DINUM ajuste son organisation"
author: Bertrand Lemaire
date: 2023-04-26
href: https://www.republik-it.fr/decideurs-it/gouvernance/la-dinum-ajuste-son-organisation.html
featured_image: https://img.republiknews.fr/crop/none/275b677fcb57bfc38c5201cf377751b0/0/0/750/422/465/262/stephanie-schaer-directrice-interministerielle-numerique.jpg
tags:
- Institutions
series:
- 202317
---

> Suite à la publication de sa nouvelle feuille de route, la DINUM (Direction Interministérielle du Numérique) est légèrement réorganisée par un décret.
