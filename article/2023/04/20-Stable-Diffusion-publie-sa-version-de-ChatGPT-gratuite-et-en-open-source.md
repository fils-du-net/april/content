---
site: BFMtv
title: "Stable Diffusion publie sa version de ChatGPT gratuite et en open source"
author: Thomas Leroy
date: 2023-04-20
href: https://www.bfmtv.com/tech/intelligence-artificielle/stable-diffusion-publie-sa-version-de-chat-gpt-gratuite-et-en-open-source_AV-202304200369.html
featured_image: https://bcboltnexti1-a.akamaihd.net/image/v1/static/6306058915001/213d0c7f-9513-44b9-848b-f8eb93372e85/477c7046-bea2-446a-8f7d-26b1b54fc648/1280x720/match/image.jpg
tags:
- Sciences
series:
- 202316
series_weight: 0
---

> Déjà connu pour ses générateurs d’images, Stability AI va désormais proposer une version texte sur le même modèle que ChatGPT.
