---
site: ZDNet France
title: "Etats-Unis: la CISA expose son programme de sécurisation des logiciels open source"
author: Thierry Noisette
date: 2023-04-10
href: https://www.zdnet.fr/blogs/l-esprit-libre/etats-unis-la-cisa-expose-son-programme-de-securisation-des-logiciels-open-source-39956856.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/12/cyber-security-3400657_960_720_Pixabay.jpg
tags:
- Innovation
series:
- 202315
series_weight: 0
---

> L'agence fédérale américaine de cyberdéfense embauche un responsable de la sécurité open source et veut améliorer la sécurité d'un 'écosystème parmi les plus importants pour le fonctionnement du gouvernement fédéral et des infrastructures critiques'.
