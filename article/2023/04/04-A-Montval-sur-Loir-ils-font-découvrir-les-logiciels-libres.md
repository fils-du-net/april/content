---
site: actu.fr
title: "A Montval-sur-Loir, ils font découvrir les logiciels libres"
author: Chloé Morin
date: 2023-04-04
href: https://actu.fr/pays-de-la-loire/montval-sur-loir_72071/a-montval-sur-loir-ils-font-decouvrir-les-logiciels-libres_58662480.html
featured_image: https://static.actu.fr/uploads/2023/04/803640d671246843640d6712468364v-960x640.jpg
tags:
- Associations
- april
- Promotion
series:
- 202314
series_weight: 0
---

> Dans le cadre "Libre en fête", l'entreprise de Montval-sur-Loir (Sarthe) PMB Services organise une après-midi gratuite de découverte des logiciels libres le samedi 8 avril 2023.
