---
site: www.usine-digitale.fr
title: "Le gouvernement sélectionne 3 projets pour rivaliser avec Microsoft Office et Google Docs"
author: Jérôme Marin
date: 2023-04-06
href: https://www.usine-digitale.fr/article/le-gouvernement-selectionne-3-projets-pour-rivaliser-avec-microsoft-office-et-google-docs.N2119426#xtor=EPR-4
featured_image: https://www.usine-digitale.fr/mediatheque/2/5/1/001398152_896x598_c.png
tags:
- Institutions
- Informatique en nuage
series:
- 202314
---

> Une enveloppe de 23 millions d’euros sera distribuée à trois consortiums, regroupant 18 partenaires. Objectif: concevoir des offres proposant tous les outils et les fonctionnalités nécessaires pour représenter une alternative crédible aux suites bureautiques de deux géants américains.
