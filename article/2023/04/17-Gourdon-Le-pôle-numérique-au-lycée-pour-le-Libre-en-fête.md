---
site: LaDepeche.fr
title: "Gourdon. Le pôle numérique au lycée pour le \"Libre en fête\""
date: 2023-04-17
href: https://www.ladepeche.fr/2023/04/17/le-pole-numerique-au-lycee-pour-le-libre-en-fete-11139202.php
featured_image: https://images.ladepeche.fr/api/v1/images/view/643cba37eaafa5495f01df76/large/image.png
tags:
- Promotion
- Éducation
series:
- 202316
series_weight: 0
---

> Le pôle numérique de Gourdon est intervenu au sein des classes de 2de d’Édith Lagarde, professeure de technologie de la cité scolaire, afin de sensibiliser les élèves aux questions de droits d’auteur, de licences, de logiciels libres et de droit à l’image.
