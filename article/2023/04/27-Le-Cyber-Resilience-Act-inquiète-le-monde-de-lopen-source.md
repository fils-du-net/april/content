---
site: ZDNet France
title: "Le Cyber Resilience Act inquiète le monde de l'open-source"
author: Gabriel Thierry
date: 2023-04-27
href: https://www.zdnet.fr/actualites/le-cyber-resilience-act-inquiete-le-monde-de-l-open-source-39957426.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/02/open-source-code-620__w1200.jpg
tags:
- Europe
- Institutions
series:
- 202317
series_weight: 0
---

> Après la présentation de ce projet de texte réglementaire par la Commission européenne, treize organisations européennes de l'open-source regrettent de ne pas être davantage associées aux travaux législatifs en cours.
