---
site: Journal du Net
title: "Intelligence artificielle: quels sont les enjeux pour les employeurs?"
author: Thierry Fabre
date: 2023-04-18
href: https://www.journaldunet.com/solutions/dsi/1521669-intelligence-artificielle-quels-sont-les-enjeux-pour-les-employeurs
tags:
- Sciences
series:
- 202316
---

> Avec l'apparition de ChatGPT, de nombreuses entreprises à travers le monde prennent des mesures pour encadrer l'utilisation de tels outils.
