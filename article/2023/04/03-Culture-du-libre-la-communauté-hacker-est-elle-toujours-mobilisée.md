---
site: Usbek & Rica
title: "Culture du libre: la communauté hacker est-elle toujours mobilisée?"
author: Juliette Devaux
date: 2023-04-03
href: https://usbeketrica.com/fr/article/culture-du-libre-la-communaute-hacker-est-elle-toujours-mobilisee
featured_image: https://usbeketrica.com/media/108846/download/hacker%2C%20AlejandroCarnicero%2C%20Shutterstock.jpg?v=1&inline=1
tags:
- Sensibilisation
- Partage du savoir
series:
- 202314
series_weight: 0
---

> Une nouvelle génération d'hacktivistes tente aujourd'hui de perpétuer l'engagement et la mémoire de figures tutélaires de la culture libre comme Richard Stallman et Aaron Swartz, notamment pour faciliter l'accès au savoir scientifique.
