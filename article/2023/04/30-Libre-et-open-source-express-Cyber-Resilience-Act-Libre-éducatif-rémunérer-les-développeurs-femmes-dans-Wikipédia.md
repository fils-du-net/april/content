---
site: ZDNet France
title: "Libre et open source express: Cyber Resilience Act, Libre éducatif, rémunérer les développeurs, femmes dans Wikipédia"
author: Thierry Noisette
date: 2023-04-30
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-cyber-resilience-act-libre-educatif-remunerer-les-developpeurs-femmes-dans-wikipedia-39957842.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/05/open-source-KeepCalmAndUse.jpg
tags:
- april
- Europe
series:
- 202317
---

> L'April met en cause l'approche de la Commission européenne avec le CRA. Trois outils du Libre éducatif. Un nouveau système de rémunération des développeurs d'open source. Une créatrice d'articles sur les femmes scientifiques dans Wikipédia.
