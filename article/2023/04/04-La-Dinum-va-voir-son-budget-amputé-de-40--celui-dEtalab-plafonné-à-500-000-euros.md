---
site: Next INpact
title: "La Dinum va voir son budget amputé de 40 %, celui d'Etalab plafonné à 500 000 euros"
date: 2023-04-04
href: https://www.nextinpact.com/lebrief/71389/la-dinum-va-voir-son-budget-ampute-40-celui-detalab-plafonne-a-500-000
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/square-linked-media/347.jpg
tags:
- Administration
series:
- 202314
---

> La Direction interministérielle du numérique (Dinum) va voir son budget amputé de 40 %, a appris La Lettre A.
