---
site: www.usine-digitale.fr
title: "Google accuse Microsoft de pratiques anticoncurrentielles dans le cloud"
author: Jérôme Marin
date: 2023-04-03
href: https://www.usine-digitale.fr/article/google-accuse-microsoft-de-pratiques-anticoncurrentielles-dans-le-cloud.N2116481
featured_image: https://www.usine-digitale.fr/mediatheque/2/7/2/001364272_896x598_c.jpg
tags:
- Institutions
- Informatique en nuage
series:
- 202314
---

> Google demande à Bruxelles d'agir contre les pratiques commerciales de Microsoft sur le marché du cloud, malgré l'accord à l'amiable qui se dessine entre Microsoft et trois acteurs européens, dont OVHcloud.
