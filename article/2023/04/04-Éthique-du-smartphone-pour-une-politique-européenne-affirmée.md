---
site: Journal du Net
title: "Éthique du smartphone: pour une politique européenne affirmée"
author: Gaël Duval
date: 2023-04-04
href: https://www.journaldunet.com/ebusiness/telecoms-fai/1521217-ethique-du-smartphone-pour-une-politique-europeenne-affirmee
tags:
- Innovation
- Vie privée
series:
- 202314
series_weight: 0
---

> Les smartphones dont nous faisons l'usage quotidien ont un impact sur l'environnement et notre vie privée. Comment construire une politique européenne d'éthique et de souveraineté numérique?
