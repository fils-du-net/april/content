---
site: ouest-france.fr
title: "À Geneston, initiation à l’installation des logiciels libres en une séance"
date: 2023-04-17
href: https://www.ouest-france.fr/pays-de-la-loire/geneston-44140/a-geneston-initiation-a-linstallation-des-logiciels-libres-en-une-seance-ff98990e-dd02-11ed-b444-90931e1ae31e
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMzA0YTI1MGRiOTA0ODBhZjdkNDIxMGNlMWEzNjZhZjhjNDg?width=940&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=eefc73a03e911ba883deba22c7593a9dda32aafee29e44b1e750a687a30f08f7
tags:
- Associations
series:
- 202316
series_weight: 0
---

> L’association Lichen, collectif citoyen de Geneston pour la transition écologique, propose vendredi 21 avril, à Geneston, une séance animée par Emmanuel Blanchard, formateur en informatique, pour s’initier aux logiciels libres, les installer sur son ordinateur et les utiliser.

