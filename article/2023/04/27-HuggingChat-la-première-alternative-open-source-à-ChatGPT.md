---
site: Next INpact
title: "HuggingChat, «la première alternative open source à ChatGPT»"
author: Jean-Marc Manach
date: 2023-04-27
href: https://www.nextinpact.com/article/71562/huggingchat-premiere-alternative-open-source-a-chatgpt
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/12630.jpg
tags:
- Sciences
series:
- 202317
series_weight: 0
---

> La start-up Hugging Face, créée en 2016 par trois Français à New York, vient de lancer HuggingChat, «la première alternative open source à ChatGPT».
