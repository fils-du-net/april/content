---
site: ITforBusiness
title: "Le Cyber Resilience Act met en danger l'open source"
author: Laurent Delattre
date: 2023-04-19
href: https://www.itforbusiness.fr/le-cyber-resilience-act-cra-met-en-danger-l-open-source-61695
featured_image: https://www.itforbusiness.fr/wp-content/uploads/2023/04/Cyber-resilience-act-CRA-et-Open-source-shutterstock_758084194.jpg
tags:
- Europe
series:
- 202316
---

> Le Cyber Resilience Act (CRA) impose de nouvelles responsabilités aux éditeurs incompatibles avec l'open source selon les associations.
