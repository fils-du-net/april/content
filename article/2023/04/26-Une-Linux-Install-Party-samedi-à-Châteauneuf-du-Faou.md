---
site: ouest-france.fr
title: "Une Linux Install Party, samedi, à Châteauneuf-du-Faou"
date: 2023-04-26
href: https://www.ouest-france.fr/bretagne/chateauneuf-du-faou-29520/une-linux-install-party-samedi-a-chateauneuf-du-faou-e58fa1bc-e444-11ed-9871-5b64b4f08ca0
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMzA0ZDM2M2M1ZjE4YzgzZmNhOTZjNWM5ZmRmMmQ1ZWRiOTA?width=940&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=a4c0aedac8e5aeffb7ba7416d949a4a4a69fd3173f1159d0c1849aae5c7d45a0
tags:
- Sensibilisation
- Associations
series:
- 202317
series_weight: 0
---

> La communauté de communes de Haute Cornouaille (CCHC) organise une Linux Install Party ce samedi 29 avril 2023, de 13 h 30 à 18 h 30, dans ses locaux au 6, rue de Morlaix, à Châteauneuf-du-Faou (Finistère) en partenariat avec les associations Linux Quimper et Ordis Libres de l’Arrée.
