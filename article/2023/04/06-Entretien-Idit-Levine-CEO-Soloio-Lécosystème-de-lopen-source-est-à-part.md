---
site: Le Monde Informatique
title: "Entretien Idit Levine, CEO Solo.io: «L'écosystème de l'open source est à part»"
author: Célia Seramour
date: 2023-04-06
href: https://www.lemondeinformatique.fr/actualites/lire-entretien-idit-levine-ceo-soloio--l-ecosysteme-de-l-open-source-est-a-part-89409.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000090346.jpg
tags:
- Entreprise
series:
- 202314
series_weight: 0
---

> Développement et Tests: Après avoir évolué au sein de différentes entreprises IT, Idit Levine a lancé sa start-up reposant sur du code open source, Solo.io. Rapidement devenue une licorne, la société est aujourd'hui une référence en matière de microservices et solutions cloud natives.
