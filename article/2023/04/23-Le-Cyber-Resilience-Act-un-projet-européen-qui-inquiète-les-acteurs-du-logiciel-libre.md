---
site: ZDNet France
title: "Le Cyber Resilience Act, un projet européen qui inquiète les acteurs du logiciel libre"
author: Thierry Noisette
date: 2023-04-23
href: https://www.zdnet.fr/blogs/l-esprit-libre/le-cyber-resilience-act-un-projet-europeen-qui-inquiete-les-acteurs-du-logiciel-libre-39957466.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/Europe%20ordi%20portable.jpeg
tags:
- Europe
- Institutions
- Associations
series:
- 202316
series_weight: 0
---

> Le CRA, projet de directive européenne, est «une menace existentielle pour la filière européenne du logiciel libre», selon le CNLL, qui avec plusieurs organisations interpelle les eurodéputés et le Conseil de l'UE.
