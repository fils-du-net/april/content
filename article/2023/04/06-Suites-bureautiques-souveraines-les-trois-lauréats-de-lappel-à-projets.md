---
site: Silicon
title: "Suites bureautiques souveraines: les trois lauréats de l'appel à projets"
author: Clément Bohic
date: 2023-04-06
href: https://www.silicon.fr/suites-bureautiques-souveraines-trois-laureats-appel-projets-462389.html
featured_image: https://www.silicon.fr/wp-content/uploads/2023/04/AAP-suites-bureautiques-cloud-souveraines.jpg
tags:
- Institutions
- Informatique en nuage
series:
- 202314
series_weight: 0
---

> Trois projets sont lauréats de l'appel à projets sur les suites bureautiques cloud souveraines. Qui en sont les porteurs?
