---
site: Acteurs Publics
title: "La direction du numérique de l'État amorce un contrôle sur les RH (€)"
author: Emile Marzolf
date: 2023-04-25
href: https://acteurspublics.fr/articles/la-direction-du-numerique-de-letat-amorce-un-controle-sur-les-rh
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/43/e7ce36dcb497c85b56cbaa01e08af95c6111a25e.jpeg
tags:
- Institutions
series:
- 202317
---

> Trois ans après une première restructuration, la direction interministérielle du numérique voit ses services réorganisés, avec un renforcement de son rôle de DRH des métiers du numérique et de fournisseur de services numériques mutualisés pour l’ensemble des ministères.
