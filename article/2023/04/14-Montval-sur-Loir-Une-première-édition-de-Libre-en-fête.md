---
site: ouest-france.fr
title: "Montval-sur-Loir. Une première édition de Libre en fête (€)"
date: 2023-04-14
href: https://www.ouest-france.fr/pays-de-la-loire/montval-sur-loir-72500/une-premiere-edition-de-libre-en-fete-877a9ed9-739a-4c25-a06f-d69f2dd79518
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMzA0MWIzY2YxNDNhOGE0ZTY0MjA1ZDAwMGMwYjQxZTQyYjA?width=940&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=dadb57657b3faff2a8fc7c21de2f8480dbf7bf95e7bc609a333bfcff79ee0389
tags:
- Promotion
series:
- 202315
series_weight: 0
---

> Samedi à la bibliothèque-ludothèque, l’entreprise castélorienne PMB services organisait un après-midi de découverte des logiciels libres à destination du grand public. Cet évènement s’inscrit dans le cadre de l’initiative nationale «Libre en fête» qui vise à promouvoir les avantages du numérique libre.
