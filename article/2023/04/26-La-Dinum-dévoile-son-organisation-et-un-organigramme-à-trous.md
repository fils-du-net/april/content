---
site: cio-online.com
title: "La Dinum dévoile son organisation et un organigramme... à trous"
author: Reynald Fléchaux
date: 2023-04-26
href: https://www.cio-online.com/actualites/lire-la-dinum-devoile-son-organisation-et-un-organigramme-a-trous-14902.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000019878.jpg
tags:
- Institutions
series:
- 202317
series_weight: 0
---

> L'organisation de la Dinum repose sur 6 départements, dont une DRH interministérielle du numérique. Mais 11 des 14 postes à responsabilités sont vacants.
