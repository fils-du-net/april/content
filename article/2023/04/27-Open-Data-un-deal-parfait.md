---
site: Silicon
title: "Open Data: un deal parfait?"
author: N'Da Joël Kouassi & Alexis Trentesaux
date: 2023-04-27
href: https://www.silicon.fr/avis-expert/open-data-un-deal-parfait
featured_image: https://www.silicon.fr/wp-content/uploads/2014/10/Open-Data-sticker.jpg
tags:
- Open Data
series:
- 202317
series_weight: 0
---

> L'Open Data peut s'agir comme un véritable levier de performance pour les entreprises et ceci devient encore plus vrai quand elles acceptent de jouer le jeu de l'ouverture de leurs données.
