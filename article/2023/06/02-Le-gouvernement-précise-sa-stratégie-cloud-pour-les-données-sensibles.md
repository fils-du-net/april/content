---
site: Le Monde Informatique
title: "Le gouvernement précise sa stratégie cloud pour les données sensibles"
author: Jacques Cheminat
date: 2023-06-02
href: https://www.lemondeinformatique.fr/actualites/lire-le-gouvernement-precise-sa-strategie-cloud-pour-les-donnees-sensibles-90599.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000092161.jpeg
tags:
- Administration
- Informatique en nuage
series:
- 202322
series_weight: 0
---

> L'exécutif vient de publier une circulaire pour préciser la doctrine cloud de l'Etat en particulier pour les données sensibles. Elle vient spécifier le périmètre des informations concernées en imposant le recours à un fournisseur labelisé SecNumCloud, sauf dérogation.
