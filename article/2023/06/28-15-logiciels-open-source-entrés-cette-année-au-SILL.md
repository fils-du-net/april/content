---
site: Silicon
title: "15 logiciels open source entrés cette année au SILL"
author: Clément Bohic
date: 2023-06-28
href: https://www.silicon.fr/15-logiciels-open-source-sill-468648.html
featured_image: https://www.silicon.fr/wp-content/uploads/2022/10/SILL-septembre-2022.jpg
tags:
- Administration
- Référentiel
series:
- 202326
series_weight: 0
---

> Backup, monitoring, automatisation, bureau à distance... Voici quelques-uns des logiciels entrés cette année au SILL.
