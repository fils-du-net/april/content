---
site: EconomieMatin
title: "SaaS et open source: deux notions clés d'une transformation numérique réussie"
author: Benjamin Planque
date: 2023-06-06
href: https://www.economiematin.fr/saas-open-source-transformation-numerique-reussie-planque
featured_image: https://www.economiematin.fr/wp-content/uploads/2023/03/internet-gafam-plateforme-regulation-chatgpt-contenu-1200x780.jpg
tags:
- Informatique en nuage
series:
- 202323
series_weight: 0
---

> Si l’open source est une technologie plutôt destinée à des utilisateurs aux compétences techniques confirmées de l’accès libre au code, le SaaS prône davantage le «prêt à l’emploi», avec sa capacité à s’adapter à toute infrastructure informatique. En offrant un accès libre au code source du logiciel, permettant à chacun de l’enrichir, l’open source repose au départ sur le principe que chacun l’installe sur ses propres systèmes.
