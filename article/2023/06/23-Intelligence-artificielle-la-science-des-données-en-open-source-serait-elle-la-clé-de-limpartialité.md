---
site: Journal du Net
title: "Intelligence artificielle: la science des données en open source serait-elle la clé de l'impartialité?"
author: Guray Turan
date: 2023-06-23
href: https://www.journaldunet.com/solutions/dsi/1523213-intelligence-artificielle-la-science-des-donnees-en-open-source-serait-elle-la-cle-de-l-impartialite
tags:
- Sciences
series:
- 202325
series_weight: 0
---

> À l'instar des humains qui effectuaient certaines tâches bien avant l'introduction de la technologie, l'IA peut également les réaliser avec la même vision tronquée. C'est là où l'open source intervient.
