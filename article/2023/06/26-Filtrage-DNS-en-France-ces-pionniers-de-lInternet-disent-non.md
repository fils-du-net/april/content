---
site: Clubic.com
title: "Filtrage DNS en France: ces pionniers de l'Internet disent non!"
author: Alexandre Boero
date: 2023-06-26
href: https://www.clubic.com/internet/actualite-475516-filtrage-dns-en-france-ces-pionniers-de-l-internet-disent-non.html
featured_image: https://pic.clubic.com/v1/images/2120590/raw.webp?fit=max&width=1200&hash=b69eb9f9573e991f24a13e0c001973d6aecf3ac5
tags:
- Institutions
- Internet
series:
- 202326
series_weight: 0
---

> Plusieurs grandes figures de l'Internet, parmi lesquelles Vint Cerf et Steve Crocker, entendent sensibiliser le Parlement français sur les risques du filtrage DNS (Domain Name System), cette technique qui permet de bloquer l'accès à un site web.
