---
site: L'actualité
title: "Les logiciels libres pour gérer le diabète de type 1 sont sécuritaires"
author: Jean-Benoit Legault
date: 2023-06-29
href: https://lactualite.com/actualites/les-logiciels-libres-pour-gerer-le-diabete-de-type-1-sont-securitaires
featured_image: https://media.lactualite.com/2023/06/2023062911068-649d9f09ba9038b56f85a13djpeg.jpg
tags:
- Sensibilisation
- Logiciels privateurs
series:
- 202326
series_weight: 0
---

> MONTRÉAL — Les logiciels libres disponibles en ligne pour faire fonctionner les systèmes automatisés d’administration d’insuline sont comparables et tout aussi sécuritaires que les logiciels commerciaux, conclut la toute première étude à s’être penchée sur la question.
