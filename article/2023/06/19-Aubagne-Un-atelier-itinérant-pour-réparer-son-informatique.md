---
site: LaProvence.com
title: "Aubagne: Un atelier itinérant pour réparer son informatique"
author: I.D.
date: 2023-06-19
href: https://www.laprovence.com/article/region/420421429191515/aubagne-un-atelier-itinerant-pour-reparer-son-informatique
featured_image: https://images.laprovence.com/media/hermes/20230619/20230619_1_2_5_1_1_obj28227033_1.jpg?twic=v1/crop=1800x1013@0x0/cover=1670x939
tags:
- Associations
- Promotion
series:
- 202325
series_weight: 0
---

> Chaque 3e samedi du mois, le collectif Garlatek propose d'apprendre à réparer son matériel informatique.
