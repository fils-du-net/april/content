---
site: EurActiv
title: "Cyberrésilience: les eurodéputés définissent le champ d'application et les obligations des fabricants"
author: Luca Bertuzzi
date: 2023-06-27
href: https://www.euractiv.fr/section/cybercriminalite/news/cyberresilience-les-eurodeputes-definissent-le-champ-dapplication-et-les-obligations-des-fabricants
featured_image: https://www.euractiv.fr/wp-content/uploads/sites/3/2023/06/1679929324104_20230327_EP-147828A_9O6_592_DOWNLOAD_LARGE2-800x450.jpg
tags:
- Europe
- Institutions
series:
- 202326
series_weight: 0
---

> Les eurodéputés peaufinent les obligations que la nouvelle législation sur la cybersécurité imposera aux fabricants de produits et la manière dont elle s'appliquera aux logiciels libres.
