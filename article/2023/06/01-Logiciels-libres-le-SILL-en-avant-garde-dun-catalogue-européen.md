---
site: Silicon
title: "Logiciels libres: le SILL en avant-garde d'un catalogue européen"
author: Clément Bohic
date: 2023-06-01
href: https://www.silicon.fr/logiciels-libres-sill-catalogue-europeen-466761.html
featured_image: https://www.silicon.fr/wp-content/uploads/2023/06/SILL-catalogue-europeen-logiciels-libres-scaled.jpeg
tags:
- Administration
- Europe
series:
- 202322
series_weight: 0
---

> Avec son SILL, la France peut peser dans le projet de constitution d'un catalogue européen de logiciels libres pour le secteur public.
