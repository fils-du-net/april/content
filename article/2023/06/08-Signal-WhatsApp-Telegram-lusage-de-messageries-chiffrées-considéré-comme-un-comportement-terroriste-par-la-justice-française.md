---
site: Les Numeriques
title: "Signal, WhatsApp, Telegram…: l'usage de messageries chiffrées considéré comme un comportement terroriste par la justice française?"
author: Maxence Fabrion
date: 2021-10-14
href: https://www.lesnumeriques.com/societe-numerique/signal-whatsapp-telegram-l-usage-de-messageries-chiffrees-considere-comme-un-comportement-terroriste-par-la-justice-francaise-n210415.html
featured_image: https://cdn.lesnumeriques.com/optim/news/21/210374/a1e548cd-signal-whatsapp-telegram-l-usage-de-messageries-criffrees-considere-comme-un-comportement-terroriste-par-la-justice-francaise__w800.webp
tags:
- Vie privée
- Institutions
series:
- 202323
series_weight: 0
---

> Dans le cadre d'une affaire impliquant un groupuscule désigné comme appartenant à l’"ultragauche", le chiffrement des communications est dans le viseur de la justice française. La Quadrature du Net dénonce un "amalgame entre protection de la vie privée et terrorisme".
