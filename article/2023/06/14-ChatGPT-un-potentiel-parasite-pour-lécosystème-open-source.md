---
site: Le Monde Informatique
title: "ChatGPT, un potentiel parasite pour l'écosystème open source"
author: Matt Asay
date: 2023-05-30
href: https://www.lemondeinformatique.fr/actualites/lire-chatgpt-un-potentiel-parasite-pour-l-ecosysteme-open-source-90559.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000092095.jpg
tags:
- Sciences
- Droit d'auteur
series:
- 202325
series_weight: 0
---

> Que doivent ChatGPT et d'autres grands modèles linguistiques aux créateurs humains qui fournissent les données sur lesquelles ils s'entraînent? Que se passe-t-il si ces créateurs cessent de rendre leurs connaissances accessibles au public? Le chroniqueur Matt Asay, revient sur l'engouement autour des LLM et les risques qu'ils représentent pour l'écosystème open source.
