---
site: Nouvelle République
title: "Loir-et-Cher: la culture internet dans toutes ses dimensions aux Geek Faëries (€)"
author: Pierre Calmeilles
date: 2023-06-03
href: https://www.lanouvellerepublique.fr/loir-et-cher/commune/selles-sur-cher/loir-et-cher-la-culture-internet-dans-toutes-ses-dimensions-aux-geek-faeries
featured_image: https://images.lanouvellerepublique.fr/image/upload/t_1020w/f_auto/647b845700f66d621e8b4610.jpg
tags:
- Sensibilisation
series:
- 202322
series_weight: 0
---

> Décalé ou plus sérieux, le festival des cultures internet Geek Faëries donne à voir toutes les facettes du web. Jusqu’à ce dimanche 4 juin 2023 au soir, au château de Selles-sur-Cher.
