---
site: Le Monde.fr
title: "«L'intelligence artificielle pourrait renforcer les géants du numérique, mais aussi les affaiblir» (€)"
author: Alexandre Piquard
date: 2023-06-01
href: https://www.lemonde.fr/idees/article/2023/06/01/l-ia-pourrait-renforcer-les-geants-du-numerique-mais-aussi-les-affaiblir_6175649_3232.html
tags:
- Sciences
series:
- 202322
series_weight: 0
---

> CHRONIQUE. L'IA en open source serait susceptible de rebattre les cartes du secteur dans un sens moins favorable aux Big Tech, explique Alexandre Piquard, journaliste au «Monde», dans sa chronique.
