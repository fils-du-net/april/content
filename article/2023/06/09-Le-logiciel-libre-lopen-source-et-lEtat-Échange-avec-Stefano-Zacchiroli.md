---
site: Le Monde.fr
title: "Le logiciel libre, l'open source et l'Etat. Échange avec Stefano Zacchiroli"
date: 2023-06-09
href: https://www.lemonde.fr/blog/binaire/2023/06/09/le-logiciel-libre-lopen-source-et-letat-echange-avec-stefano-zacchiroli
tags:
- Sensibilisation
series:
- 202323
---

> Professeur en informatique à l’école Télécom Paris de l’Institut Polytechnique de Paris, Stefano Zacchiroli revient dans cet entretien sur les caractéristiques du logiciel libre qui en font un mouvement social de promotion des libertés numériques des utilisateurs. Nous publions cet entretien dans le cadre de notre collaboration avec le  Conseil national du numérique qui l’a réalisé.
