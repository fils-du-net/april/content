---
site: Le Point
title: "L'Open Source, chance unique de créer une IA de confiance européenne"
author: Alexandre Zapolsky, Michel Levy Provençal, Arno Pons, Leïla Mörch, Quentin Adam et Jean-Marie Cavada
date: 2023-06-14
href: https://www.lepoint.fr/debats/l-open-source-chance-unique-de-creer-une-ia-de-confiance-europeenne-14-06-2023-2524434_2.php
featured_image: https://static.lpnt.fr/images/2023/06/14/24620865lpw-24621178-article-jpg_9585820_660x287.jpg
tags:
- Sciences
- Entreprise
series:
- 202325
series_weight: 0
---

> TRIBUNE. Alors qu'Emmanuel Macron s'apprête à annoncer un plan pour soutenir l'intelligence artificielle française, six entrepreneurs partagent leur point de vue au «Point».
