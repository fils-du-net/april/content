---
site: ICTjournal
title: "Les «mainteneurs» open source peuvent-ils être tenus responsables de la sécurité des composants?"
author: Rodolphe Koller
date: 2023-06-09
href: https://www.ictjournal.ch/articles/2023-06-09/les-mainteneurs-open-source-peuvent-ils-etre-tenus-responsables-de-la-securite
featured_image: https://data.ictjournal.ch/styles/np8_full/s3/media/2018/12/10/developer_development_computer_gilaxia-istock-607969272_preview.jpg?itok=3_zHVbY8
tags:
- Europe
series:
- 202323
series_weight: 0
---

> Face à la recrudescence des cyberattaques, les Etats légifèrent pour que les fournisseurs de logiciels prennent leurs responsabilités. Peut-on exiger de même d’une communauté open source peu ou pas rémunérée dont les développements profitent gratuitement à tous?
