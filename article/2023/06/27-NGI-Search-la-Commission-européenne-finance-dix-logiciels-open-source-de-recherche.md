---
site: ZDNet France
title: "NGI Search: la Commission européenne finance dix logiciels open source de recherche"
author: Thierry Noisette
date: 2023-06-27
href: https://www.zdnet.fr/blogs/l-esprit-libre/ngi-search-la-commission-europeenne-finance-dix-logiciels-open-source-de-recherche-39960000.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/NGISearch_Sticler.png
tags:
- Promotion
- Innovation
- Europe
series:
- 202326
series_weight: 0
---

> Accompagnés par les cinq partenaires du projet NGI Search, les logiciels sélectionnés doivent innover «pour chercher, découvrir et exploiter les données et les ressources en général sur internet et sur le Web».
