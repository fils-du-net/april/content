---
site: 01net.
title: "Gabriel, 16 ans, créateur du téléphone à 30 euros: «tout le monde a un smartphone, mais personne ne sait comment il fonctionne»"
author: Stéphanie Bascou
date: 2023-08-06
href: https://www.01net.com/actualites/gabriel-16-ans-createur-du-telephone-a-30-euros-tout-le-monde-a-un-smartphone-mais-personne-ne-sait-comment-il-fonctionne.html
featured_image: https://www.01net.com/app/uploads/2023/08/Design-sans-titre25-1360x905.jpg
tags:
- Sensibilisation
series:
- 202332
series_weight: 0
---

> Gabriel Rochet, que nous avons rencontré, s'échine depuis trois ans à fabriquer, dans sa chambre, un téléphone low cost avec les moyens du bord. Cela a donné Paxo, un téléphone qui téléphone et envoie des messages. Son aventure est en partie racontée sur son site, sur lequel code et consignes sont en libre accès. Comment en est-il venu à fabriquer son propre téléphone? Pourquoi avoir choisi l'open source? Éléments de réponse ici.
