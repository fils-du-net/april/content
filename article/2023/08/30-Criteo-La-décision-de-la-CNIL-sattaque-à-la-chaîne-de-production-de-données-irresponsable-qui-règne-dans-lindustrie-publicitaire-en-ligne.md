---
site: Le Monde.fr
title: "Criteo: «La décision de la CNIL s'attaque à la chaîne de production de données irresponsable qui règne dans l'industrie publicitaire en ligne» (€)"
author: Lucie Audibert
date: 2023-08-30
href: https://www.lemonde.fr/idees/article/2023/08/30/criteo-la-decision-de-la-cnil-s-attaque-a-la-chaine-de-production-de-donnees-irresponsable-qui-regne-dans-l-industrie-publicitaire-en-ligne_6187094_3232.html
tags:
- Vie privée
- Internet
series:
- 202336
---

> TRIBUNE. Pendant des années, l'industrie de la publicité et du pistage en ligne s'est autorisée à espionner les usagers du Web sans limite. La sanction infligée par la Commission nationale de l'informatique et des libertés à l'entreprise Criteo annonce un vent de changement au bénéfice des utilisateurs, estiment Lucie Audibert et Eliot Bendinelli de Privacy International, dans une tribune au «Monde».
