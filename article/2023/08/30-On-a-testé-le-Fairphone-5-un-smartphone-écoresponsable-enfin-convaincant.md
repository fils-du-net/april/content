---
site: Le Monde.fr
title: "On a testé… le Fairphone 5, un smartphone écoresponsable enfin convaincant"
author: Nicolas Six
date: 2023-08-30
href: https://www.lemonde.fr/pixels/article/2023/08/30/on-a-teste-le-fairphone-5-un-smartphone-ecoresponsable-enfin-convaincant_6187064_4408996.html
featured_image: https://img.lemde.fr/2023/08/28/0/0/6720/4480/664/0/75/0/598b0ba_1693229785378-1k2a3282.JPG
tags:
- Innovation
series:
- 202335
series_weight: 0
---

> Ce téléphone assez volumineux embarque un très honnête équipement de mobile moyen de gamme, aisément démontable. Un saut qualitatif notable en comparaison de son prédécesseur, mais qui se fait au prix d’une facture plus salée.
