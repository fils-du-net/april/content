---
site: www.usine-digitale.fr
title: "Aux Etats-Unis, la Défense lance un concours pour mettre l'IA au service de la cybersécurité"
author: Aurélien Defer
date: 2023-08-10
href: https://www.usine-digitale.fr/article/aux-etats-unis-la-defense-lance-un-concours-pour-mettre-l-ia-au-service-de-la-cybersecurite.N2160137
featured_image: https://www.usine-digitale.fr/mediatheque/4/0/6/000815604_896x598_c.jpg
tags:
- Sciences
- Innovation
series:
- 202332
series_weight: 0
---

> OpenAi, Anthropic, Microsoft et Google sont partenaires d'un challenge lancé par la Defense Advanced Research Projects Agency (DARPA). Les participants devront identifier et résoudre des failles de cybersécurité grâce à l'intelligence artificielle.
