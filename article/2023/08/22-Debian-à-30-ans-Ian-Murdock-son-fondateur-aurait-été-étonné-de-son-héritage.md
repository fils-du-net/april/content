---
site: ZDNet France
title: "Debian à 30 ans: Ian Murdock, son fondateur, aurait été étonné de son héritage"
author: Steven Vaughan-Nichols
date: 2023-08-22
href: https://www.zdnet.fr/actualites/debian-a-30-ans-ian-murdock-son-fondateur-aurait-ete-etonne-de-son-heritage-39960948.htm
featured_image: https://www.zdnet.com/a/img/2023/08/21/f8ab7691-2a31-4a74-988a-fa2283907acf/ian-murdock-2008.jpg
tags:
- Sensibilisation
series:
- 202335
series_weight: 0
---

> Cette distribution Linux reste l'une des plus dominantes du marché. Voici comment elle a commencé et où son impact se fait encore sentir aujourd'hui. 
