---
site: 01net.
title: "La nouvelle boîte à outils des hackers: le FBI blâme l'explosion de l'IA open source"
author: Florian Bayard
date: 2023-08-01
href: https://www.01net.com/actualites/nouvelle-boite-outils-hackers-fbi-blame-explosion-ia-open-source.html
featured_image: https://www.01net.com/app/uploads/2023/08/hackers-ia-fbi-open-source-1360x905.jpg
tags:
- Sciences
series:
- 202332
series_weight: 0
---

> Les cybercriminels se servent de plus en plus de l'intelligence artificielle. D'après le FBI, les hackers exploitent les IA pour améliorer leurs outils et piéger les internautes. Ce sont surtout les modèles open source qui ont attiré l'attention des pirates…
