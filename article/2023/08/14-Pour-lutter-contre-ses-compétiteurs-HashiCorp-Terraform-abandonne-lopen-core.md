---
site: LeMagIT
title: "Pour lutter contre ses compétiteurs, HashiCorp (Terraform) abandonne l'open core"
author: Beth Pariseau
date: 2023-08-14
href: https://www.lemagit.fr/actualites/366548214/Pour-lutter-contre-ses-competiteurs-HashiCorp-Terraform-abandonne-lopen-core
featured_image: https://cdn.ttgtmedia.com/visuals/German/article/gate-lock-chain-secure-adobe.jpg
tags:
- Licenses
- Entreprise
series:
- 202335
series_weight: 0
---

> HashiCorp, le principal contributeur de Terraform, adopte la licence BSL pour toutes ses futures versions de produits. Permissive pour la plupart des usagers, celle-ci interdit l’utilisation des logiciels à des fins commerciales, ce qui relance les questions relatives au modèle open core.
