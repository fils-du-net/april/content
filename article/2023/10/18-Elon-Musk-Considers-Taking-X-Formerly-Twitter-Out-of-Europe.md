---
title: "Elon Musk Considers Taking X, Formerly Twitter, Out of Europe"
author: Kali Hays
date: 2023-10-18
href: https://www.businessinsider.com/elon-musk-considering-taking-twitter-x-out-of-europe-dsa-2023-10?r=US&IR=T
featured_image: https://www.businessinsider.com/public/assets/subscription/marketing/banner-overlay/top-left.svg
tags:
- Europe
- Internet
- Anglais
series:
- 202342
---

> In recent weeks Elon Musk has suggested X could stop being accessible in Europe to avoid new regulation enacted by the European Commission.
