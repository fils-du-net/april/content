---
site: Le Monde.fr
title: "Face aux Gafam, les Chatons sortent les griffes (€)"
author: Bastien Lion
date: 2023-10-14
href: https://www.lemonde.fr/pixels/article/2023/10/14/face-aux-gafam-les-chatons-sortent-les-griffes_6194362_4408996.html
featured_image: https://img.lemde.fr/2023/10/13/0/0/2268/1411/800/0/75/0/0458419_1697189227515-commundouble-copie.jpg
tags:
- Associations
- Internet
series:
- 202341
series_weight: 0
---

> Le Collectif des hébergeurs alternatifs, transparents, ouverts, neutres et solidaires (Chatons) regroupe 96 associations un peu partout en France. Ces militants des logiciels libres développent des outils pour éviter le recours aux applications fournies par les géants du numérique.
