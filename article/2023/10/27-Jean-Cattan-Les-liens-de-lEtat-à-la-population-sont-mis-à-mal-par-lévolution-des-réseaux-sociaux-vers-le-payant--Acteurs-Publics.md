---
site: Acteurs Publics 
title: "Jean Cattan: “Les liens de l'Etat à la population sont mis à mal par l'évolution des réseaux sociaux vers le payant\" (€)"
author: Emile Marzolf
date: 2023-10-27
href: https://acteurspublics.fr/articles/les-liens-de-letat-a-la-population-sont-mis-a-mal-par-levolution-des-reseaux-sociaux-vers-le-payant
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/47/86de517b3810e075d1f892f47ce4eec384edb498.jpeg
tags:
- Entreprise
series:
- 202344
---

> Dans cette tribune, le secrétaire général du Conseil national du Numérique (CNNum) et co-auteur d'un ouvrage sur les réseaux sociaux*, Jean Cattan, s'interroge sur l'avenir de l'Etat sur les réseaux sociaux, et en particulier sur Twitter/X, alors que leur fonctionnement bascule vers un modèle payant qui met à mal le principe de non-discrimination de l'accès aux communications des organisations publiques. Une bascule qui fait également peser le risque d'une “remonopolisation” autour d'une poignée d'acteurs des réseaux sociaux.
