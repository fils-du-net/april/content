---
site: ZDNet France
title: "Libre et open source express: Smile, smartphone Murena, conversions aux logiciels propriétaires"
author: Thierry Noisette
date: 2023-10-30
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-smile-smartphone-murena-conversions-aux-logiciels-proprietaires-39962154.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/05/open-source-KeepCalmAndUse.jpg
tags:
- Licenses
series:
- 202344
series_weight: 0
---

> En bref. Smile adhère au projet collaboratif Yocto. Crowdfunding pour le smartphone Murena, 100% libre. L'abandon des licences libres dans l'espoir de gagner plus ne date pas d'hier.
