---
site: Le Monde.fr
title: "Avec Pyronear, un système d'intelligence artificielle sous licence libre détecte les feux de forêt dès les premières fumées (€)"
author: Claire Legros
date: 2023-10-17
href: https://www.lemonde.fr/pixels/article/2023/10/17/avec-pyronear-un-systeme-d-intelligence-artificielle-sous-licence-libre-detecte-les-feux-de-foret-des-les-premieres-fumees_6194994_4408996.html
featured_image: https://img.lemde.fr/2023/10/11/0/0/5500/3667/800/0/75/0/da1ff3e_1697033896458-000-32fk6b6.jpg
tags:
- Sciences
- Open Data
series:
- 202342
---

> En partenariat avec des sapeurs-pompiers ardéchois, de jeunes ingénieurs bénévoles développent, au sein de l’association Pyronear, un outil low-tech et ouvert, associant des caméras à un logiciel entraîné à repérer les départs de feux de forêt le plus précocement possible.
