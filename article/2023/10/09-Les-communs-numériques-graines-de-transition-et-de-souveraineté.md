---
site: Le Monde.fr
title: "Les communs numériques, graines de transition et de souveraineté (€)"
author: Claire Legros et Isabelle Hennebelle
date: 2023-10-09
href: https://www.lemonde.fr/pixels/article/2023/10/09/les-communs-numeriques-graines-de-transition-et-de-souverainete_6193334_4408996.html
featured_image: https://img.lemde.fr/2023/10/13/0/0/4488/3228/800/0/75/0/97b76f6_1697189442992-communune.jpg
tags:
- Économie
series:
- 202341
series_weight: 0
---

> Ces ressources citoyennes, coproduites et ouvertes, ont fait la preuve de leur pertinence, y compris en temps de crise. Elles sont aujourd’hui sollicitées pour leur sobriété ou leurs capacités collaboratives. Si les pouvoirs publics peuvent les soutenir, chacun doit respecter l’équilibre d’une gouvernance partagée.
