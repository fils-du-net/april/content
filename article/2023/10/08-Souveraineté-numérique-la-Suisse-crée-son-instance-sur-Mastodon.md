---
site: ZDNet France
title: "Souveraineté numérique: la Suisse crée son instance sur Mastodon"
author: Thierry Noisette
date: 2023-10-08
href: https://www.zdnet.fr/blogs/l-esprit-libre/souverainete-numerique-la-suisse-cree-son-instance-sur-mastodon-39961706.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/12/Mastodon_mascot.png
tags:
- Internet
- International
series:
- 202341
series_weight: 0
---

> Après les institutions européennes et d'autres comme, en France, l'Education nationale ou la ville d'Echirolles, la Confédération helvétique arrive sur le réseau social décentralisé, en quête d'indépendance envers les géants.
