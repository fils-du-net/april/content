---
site: Les Echos
title: "Soluris, le cybergendarme des petites communes de Charente-Maritime (€)"
author: Lea Delpont
date: 2023-10-02
href: https://www.lesechos.fr/pme-regions/nouvelle-aquitaine/soluris-le-cybergendarme-des-petites-communes-de-charente-maritime-1983714
featured_image: https://media.lesechos.com/api/v1/images/view/651acf7fa7117f6dd43b3768/1024x576-webp/0902174712412-web-tete.webp
tags:
- Administration
- Vie privée
series:
- 202340
series_weight: 0
---

> Le syndicat mixte informatique créé par 40 mairies en 1985 compte désormais 580 collectivités ou établissements publics adhérents, soulagés de toute la gestion numérique. Mais ses logiciels libres sur le RGPD et désormais sur la cybersécurité sont utilisés bien au-delà de ses frontières.
