---
site: Acteurs Publics 
title: "Entre le “pack Office” et les logiciels libres, les positions contrastées des agents publics (€)"
author: Emile Marzolf
date: 2023-10-20
href: https://acteurspublics.fr/articles/entre-le-pack-office-et-les-logiciels-libres-les-positions-contrastees-des-agents-publics
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/47/a072baf910e5cd7e61701e042afafb5c88ce15ed.jpeg
tags:
- Administration
series:
- 202342
series_weight: 0
---

> La consultation en ligne “Fonction publique+”, à laquelle plus de 110 000 agents ont contribué, a donné lieu à une passe d’armes sur la question, toujours sensible, des outils numériques de travail, entre d’un côté, les promoteurs des logiciels libres, et de l’autre, ceux qui préfèrent les outils déjà éprouvés des géants du numérique.
