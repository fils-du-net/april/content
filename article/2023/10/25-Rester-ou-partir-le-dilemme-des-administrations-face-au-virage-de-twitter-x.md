---
site: Acteurs Publics
title: "Rester ou partir, le dilemme des administrations face au virage de Twitter/X (€)"
author: Emile Marzolf
date: 2023-10-25
href: https://acteurspublics.fr/articles/rester-ou-partir-le-dilemme-des-administrations-face-aux-derives-de-twitter-x
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/47/4f48f8fadf1ca241191f600c22fc6472a3ebf4c4.jpeg
tags:
- Administration
series:
- 202343
---

> Face aux dérives du réseau social depuis son rachat par Elon Musk, les administrations doivent-elles prendre leurs distances comme l'ont fait plusieurs universités, quitte à se priver d'un important relais de communication ? Le Service d'information du gouvernement n'a pas pris position, tandis que la direction interministérielle du numérique a déjà reposé ses valises sur le réseau Mastodon.
