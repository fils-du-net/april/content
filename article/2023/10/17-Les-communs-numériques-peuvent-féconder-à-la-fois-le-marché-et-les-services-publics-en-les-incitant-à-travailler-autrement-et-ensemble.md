---
site: Le Monde.fr
title: "«Les communs numériques peuvent féconder à la fois le marché et les services publics en les incitant à travailler autrement et ensemble» (€)"
author: Claire Legros et Isabelle Hennebelle
date: 2023-10-17
href: https://www.lemonde.fr/pixels/article/2023/10/17/les-communs-numeriques-peuvent-feconder-a-la-fois-le-marche-et-les-services-publics-en-les-incitant-a-travailler-autrement-et-ensemble_6194945_4408996.html
featured_image: https://img.lemde.fr/2023/10/13/0/0/3150/2583/800/0/75/0/a015d80_1697189613609-communder.jpg
tags:
- International
- Administration
- Économie
series:
- 202342
series_weight: 0
---

> Valérie Peugeot, chercheuse, membre de la CNIL, et Henri Verdier, ambassadeur pour le numérique, en appellent, dans un entretien croisé, aux Etats pour soutenir ces ressources ouvertes, gérées par une communauté de citoyens.
