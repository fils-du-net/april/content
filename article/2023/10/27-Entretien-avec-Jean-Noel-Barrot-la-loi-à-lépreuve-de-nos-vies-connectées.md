---
site: France Culture
title: "Entretien avec Jean-Noël Barrot: la loi à l'épreuve de nos vies connectées"
date: 2023-10-27
href: https://www.radiofrance.fr/franceculture/podcasts/le-meilleur-des-mondes/entretien-avec-jean-noel-barrot-la-loi-a-l-epreuve-de-nos-vies-connectees-1046718
featured_image: https://www.radiofrance.fr/s3/cruiser-production/2023/10/463399d3-709d-4bf8-9914-ba36663de388/560x315_sc_sc_gettyimages-1493380206.webp
tags:
- Europe
series:
- 202344
---

> Alors que la loi SREN a été adoptée par l'Assemblée nationale le 17 octobre, Le Meilleur des mondes s'entretient avec celui qui porte ce projet de régulation depuis des mois: le ministre délégué au numérique, Jean-Noël Barrot.
