---
site: Le Monde.fr
title: "Mobicoop, le covoiturage gratuit pour les citoyens, accélère le rythme (€)"
author: Hélène Seingier
date: 2023-10-13
href: https://www.lemonde.fr/economie/article/2023/10/13/mobicoop-le-covoiturage-gratuit-pour-les-citoyens-accelere-le-rythme_6194109_3234.html
tags:
- Économie
series:
- 202341
---

> Plus de 600 000 personnes sont inscrites sur cette plate-forme coopérative de covoiturage, où la mise en contact est gratuite et qui fonctionne selon les principes du logiciel libre
