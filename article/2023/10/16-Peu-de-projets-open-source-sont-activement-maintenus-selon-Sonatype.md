---
site: Le Monde Informatique
title: "Peu de projets open source sont activement maintenus selon Sonatype"
author: Paul Krill
date: 2023-10-16
href: https://www.lemondeinformatique.fr/actualites/lire-peu-de-projets-open-source-sont-activement-maintenus-selon-sonatype%C2%A0-91854.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000094195.png
tags:
- Innovation
series:
- 202342
series_weight: 0
---

> L'étude annuelle de la chaîne d'approvisionnement des logiciels de Sonatype indique que la maintenance des projets open source est en déclin, tandis qu'un téléchargement sur huit présente un risque connu.
