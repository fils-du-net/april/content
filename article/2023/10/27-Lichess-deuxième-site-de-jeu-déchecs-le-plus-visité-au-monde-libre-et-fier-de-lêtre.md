---
site: ZDNet France
title: "Lichess, deuxième site de jeu d'échecs le plus visité au monde, libre et fier de l'être"
author: Thierry Noisette
date: 2023-10-27
href: https://www.zdnet.fr/blogs/l-esprit-libre/lichess-deuxieme-site-de-jeu-d-echecs-le-plus-visite-au-monde-libre-et-fier-de-l-etre-39962122.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/03/echecs_tournoi_800px-Czech_Open_2013.jpeg
tags:
- Entreprise
- Sensibilisation
series:
- 202343
series_weight: 0
---

> Créé en 2010 par un développeur français, Lichess compte des millions de parties d'échecs jouées par jour. Associatif et libriste, Lichess proteste contre un partenariat annoncé par la Fédération française des échecs avec une entreprise utilisant des NFT.
