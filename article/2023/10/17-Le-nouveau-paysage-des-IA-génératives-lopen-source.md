---
site: ZDNet France
title: "Le nouveau paysage des IA génératives: l'open source"
author: Frédéric Charles
date: 2023-10-17
href: https://www.zdnet.fr/blogs/green-si/le-nouveau-paysage-des-ia-generatives-l-open-source-39961848.htm
featured_image: https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhreRAD5a9z5qNMaMDE_Q0IDow8EbQDcqNQAxuqqAf9bQ1AsCJFJBF-cVD-wK1Jbk0EeJL4oPOCOebsyvXBg6zOtEUxljWOm5weIWGIymFYdPc8xgZ3viqRcKwreH5UeGIVOzJQZOlwDk7uzg7tmXKPjwA-b2p_qEppdBTwKFvIcbbWnv_-HGm73vZqktk3/w640-h380/image_2023-10-16_222925956.png
tags:
- Sciences
series:
- 202342
series_weight: 0
---

> Nous sommes passés en 1 an du modèle universitaire des LLM, au modèle concurrentiel, avec des méthodes de financement massif. L'open source reste-t-il une option devant les modèles propriétaires?
