---
site: EurActiv
title: "Loi cyberrésilience: accord en vue sur les logiciels open source et le maintien en conditions opérationnelles"
author: Luca Bertuzzi
date: 2023-10-30
href: https://www.euractiv.fr/section/cybersecurite/news/loi-cyberresilience-accord-en-vue-sur-les-logiciels-open-source-et-le-maintien-en-conditions-operationnelles
featured_image: https://fr.euractiv.eu/wp-content/uploads/sites/3/2023/10/shutterstock_675903388.jpg
tags:
- Europe
series:
- 202344
series_weight: 0
---

> Les législateurs européens se rapprochent d'un accord sur le règlement sur la cyberrésilience (Cyber Resilience Act) en ce qui concerne les logiciels open source et la période de maintien en conditions opérationnelles.
