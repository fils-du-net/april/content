---
site: Le Monde.fr
title: "Open Food Facts, l'addition de bonnes volontés contre les additifs de l'industrie alimentaire (€)"
author: Marius Rivière
date: 2023-10-11
href: https://www.lemonde.fr/pixels/article/2023/10/11/open-food-facts-l-addition-de-bonnes-volontes-contre-les-additifs-de-l-industrie-alimentaire_6193745_4408996.html
featured_image: https://img.lemde.fr/2023/10/11/0/0/1024/768/800/0/75/0/f2c2c60_1697013414374-openfoodfacts-formation-sur-une-aire-d-autoroute.jpg
tags:
- Partage du savoir
series:
- 202341
series_weight: 0
---

> Créée il y a plus de dix ans par trois ingénieurs militants, la base de données libre Open Food Facts recense désormais plus de 2 millions de produits. Une initiative citoyenne qui permet aux consommateurs de mieux choisir leurs aliments.
