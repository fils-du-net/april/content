---
site: l'Informé
title: "Majorité numérique, influenceurs… la lettre incendiaire de Thierry Breton au gouvernement (€)"
author: Marc Rees
date: 2023-10-05
href: https://www.linforme.com/tech-telecom/article/majorite-numerique-influenceurs-la-lettre-incendiaire-de-thierry-breton-au-gouvernement_1056.html
featured_image: https://linforme-focus.sirius.press/2023/10/05/0/0/4077/2944/1920/1280/75/0/82a24e5_1696515954702-9l1bc-highres.jpg
tags:
- Europe
- Internet
series:
- 202340
series_weight: 0
---

> L’Informé a consulté un courrier adressé à la France par le commissaire européen au marché intérieur. Il pilonne la loi sur les influenceurs et celle imposant une majorité numérique sur les réseaux sociaux.
