---
site: Next INpact
title: "La DINUM met en ligne une instance Mastodon"
date: 2023-10-24
href: https://www.nextinpact.com/lebrief/72731/la-dinum-met-en-ligne-instance-mastodon
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/square-linked-media/1986.jpg
tags:
- Internet
- Institutions
series:
- 202343
series_weight: 0
---

> Installée sur un sous-domaine (social.numerique) du très officiel gouv.fr, la toute nouvelle instance Mastodon mise en ligne par la Direction interministérielle du numérique (DINUM) est prévue pour héberger «uniquement des comptes institutionnels et certifiés».
