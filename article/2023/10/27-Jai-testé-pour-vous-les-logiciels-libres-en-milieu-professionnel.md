---
site: curseurs
title: "J'ai testé pour vous: les logiciels libres en milieu professionnel"
author: Aeryn
date: 2023-10-27
href: https://www.curseurs.be/numeros/numero-1/article/j-ai-teste-pour-vous-les-logiciels-libres-en-milieu-professionnel
featured_image: https://www.curseurs.be/plugins/auto/licence/v1.0.0/img_pack/cc-by-nc-sa.png
tags:
- Sensibilisation
series:
- 202343
series_weight: 0
---

> Cela fait maintenant plus de 15 ans que j’utilise des logiciels libres. J’y suis venue seule, par goût, par curiosité. J’y suis restée par conviction. Si ce n’est pas toujours facile d’en faire la promotion, il est un domaine où je fais face à une résistance très forte à ces outils: le milieu professionnel.
