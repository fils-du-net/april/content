---
site: Contexte
title: "La Commission tire son premier boulet contre le projet de loi sur l'espace numérique"
author: Guénaël Pépin, Tiphaine Saliou
date: 2023-10-26
href: https://www.contexte.com/article/numerique/info-contexte-la-commission-tire-son-premier-boulet-contre-le-projet-de-loi-sur-lespace-numerique_177122.html
featured_image: https://contexte-journal-production.s3.amazonaws.com/medias-images/2023/10/19983571694_ca8d41cd3a_kjpg.jpg.1024x320_q85_box-0%2C341%2C2048%2C980_crop_detail_gradient_fx-3.png
tags:
- Europe
series:
- 202344
---

> Contexte a pu consulter l’avis circonstancié que la Commission a envoyé à la France, signe de son fort mécontentement.
