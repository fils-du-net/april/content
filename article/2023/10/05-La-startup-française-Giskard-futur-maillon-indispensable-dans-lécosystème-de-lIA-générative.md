---
site: La Tribune
title: "La startup française Giskard, futur maillon indispensable dans l'écosystème de l'IA générative?"
author: François Manens
date: 2023-10-05
href: https://www.latribune.fr/technos-medias/informatique/la-startup-francaise-giskard-futur-maillon-indispensable-dans-l-ecosysteme-de-l-ia-generative-978373.html
featured_image: https://static.latribune.fr/full_width/2221461/intelligence-artificielle-ia-chat-gpt.jpg
tags:
- Licenses
- Innovation
- Entreprise
series:
- 202340
series_weight: 0
---

> Alors que le sujet des risques liés aux hallucinations et aux biais de ChatGPT et ses concurrents prend de plus en plus de place, la startup française Giskard se positionne comme garante de la qualité des intelligences artificielles génératives. Elle espère imposer son service de certification des IA comme un tiers de confiance indispensable au développement du secteur... Un potentiel jackpot en perspective.
