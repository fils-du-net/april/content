---
site: Le Monde.fr
title: "Casser le chiffrement des messageries, un serpent de mer politique inapplicable (€)"
author: Florian Reynaud, Damien Leloup, Pauline Croquet
date: 2023-10-20
href: https://www.lemonde.fr/pixels/article/2023/10/20/casser-le-chiffrement-des-messageries-un-serpent-de-mer-politique-inapplicable_6195665_4408996.html
tags:
- Internet
- Institutions
series:
- 202343
---

> Le ministre de l’intérieur, Gérald Darmanin, a déclaré, jeudi, souhaiter obliger les messageries chiffrées à introduire des «portes dérobées» pour les mettre à disposition des autorités. Une proposition qui, même si elle revient régulièrement dans les discours politiques, reste impossible à mettre en place.
