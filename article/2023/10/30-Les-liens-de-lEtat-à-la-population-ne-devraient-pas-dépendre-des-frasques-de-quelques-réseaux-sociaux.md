---
site: CNNum 
title: "Tribune - «Les liens de l'Etat à la population ne devraient pas dépendre des frasques de quelques réseaux sociaux.»"
author: Jean Cattan
date: 2023-10-30
href: https://cnnumerique.fr/dans-les-medias/tribune-les-liens-de-letat-la-population-ne-devraient-pas-dependre-des-frasques-de
tags:
- Entreprise
- Internet
- Institutions
series:
- 202344
series_weight: 0
---

> Le Conseil national du numérique est une institution dotée d'une mission claire: assurer la circulation des idées sur notre relation au numérique. Ce qui exige de maximiser nos canaux d'échanges pour échanger avec toutes les personnes intéressées. En l'état, nous faisons le choix, que nous discutons régulièrement, de rester sur X (ex-Twitter) où sont encore présents beaucoup de relais d'information, de partenaires et d'interlocuteurs.
