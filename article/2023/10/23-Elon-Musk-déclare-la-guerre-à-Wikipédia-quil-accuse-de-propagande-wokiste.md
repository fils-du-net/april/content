---
site: Numerama
title: "Elon Musk déclare la guerre à Wikipédia, qu'il accuse de propagande wokiste"
author: Nicolas Lellouche
date: 2023-10-23
href: https://www.numerama.com/politique/1539034-elon-musk-declare-la-guerre-a-wikipedia-quil-accuse-de-propagande-wokiste.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2023/05/wario-1024x576.jpg?avif=1&key=11bc262c
tags:
- Partage du savoir
- Désinformation
- Internet
series:
- 202343
series_weight: 0
---

> Accusé de laisser la désinformation se propager sur X (ex-Twitter), Elon Musk a décidé de s’en prendre à Wikipédia, qu’il accuse de servir un agenda politique. Le milliardaire alimente des théories du complot sur le site communautaire depuis plusieurs jours.
