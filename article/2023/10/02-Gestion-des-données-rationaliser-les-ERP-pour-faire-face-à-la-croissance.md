---
site: Les Echos
title: "Gestion des données: rationaliser les ERP pour faire face à la croissance"
author: Erick Haehnsen
date: 2023-10-02
href: https://www.lesechos.fr/thema/articles/gestion-des-donnees-rationaliser-les-erp-pour-faire-face-a-la-croissance-1983774
featured_image: https://media.lesechos.com/api/v1/images/view/651ae95015137e3f6029437b/1024x576-webp/0902478869296-web-tete.webp
tags:
- Entreprise
series:
- 202340
series_weight: 0
---

> Après avoir acquis treize sociétés en moins de quatre ans, le groupe Barkene, spécialisé dans l'intégration et la maintenance de systèmes de sécurité électronique et anti-incendie, déploie un progiciel de gestion intégré à code source ouvert qui va unifier la comptabilité et la gestion de ses différentes entités. Objectif : maîtriser les marges et accroître l'efficacité opérationnelle.
