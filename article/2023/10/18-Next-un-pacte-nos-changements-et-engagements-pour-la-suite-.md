---
site: Next INpact
title: "Next, un pacte: nos changements et engagements pour la suite "
description: "nextunpacte.fr et .com sont disponibles!"
author: Sébastien Gavois
date: 2023-10-18
href: https://www.nextinpact.com/blog/72679/next-pacte-nos-changements-et-engagements-pour-suite
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/12860.jpg
tags:
- Partage du savoir
series:
- 202342
series_weight: 0
---

> What’s et who’s Next ? On vous explique ce que doit être, pour nous, un média tech en 2023. Compte rendu et résumé de notre conférence de lundi soir. Des questions, un avis (constructif), des craintes, des encouragements ? Les commentaires sont ouverts à tous et toutes. (Merci à DantonQ-Robespierre pour nous avoir cédé l’intégralité de ses droits sur le titre de cette actualité).
