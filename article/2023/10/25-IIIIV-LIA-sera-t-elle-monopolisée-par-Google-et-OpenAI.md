---
site: Contrepoints
title: "(III/IV) L'IA sera-t-elle monopolisée par Google et OpenAI?"
author: Thomas Jestin
date: 2023-10-25
href: https://www.contrepoints.org/2023/10/25/465587-iii-iv-lia-sera-t-elle-monopolisee-par-google-et-openai
featured_image: https://www.contrepoints.org/wp-content/uploads/2023/10/contrepoints_An_image_showing_a_factory_with_robots_driven_by_a_81c22cd7-c80a-4452-9630-ffd1773629fc-1200x800.png
tags:
- Sciences
series:
- 202343
series_weight: 0
---

> Alors que Google et OpenAI semblent dominer le domaine de l'intelligence artificielle, une myriade de startups et d'initiatives open-source émergent. Est-ce l'aube d'une nouvelle ère collaborative ou le prélude à un monopole inattendu?
