---
site: ZDNet France
title: "BlueSky, le réseau social refuge?"
author: Tris Acatrinei
date: 2023-10-12
href: https://www.zdnet.fr/blogs/zapping-decrypte/bluesky-le-reseau-social-refuge-39961806.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2023/bluesky__w1200.png
tags:
- Internet
series:
- 202341
series_weight: 0
---

> Parfois, tout ce dont on a besoin est un ciel sans nuages et sans oiseau de malheur.
