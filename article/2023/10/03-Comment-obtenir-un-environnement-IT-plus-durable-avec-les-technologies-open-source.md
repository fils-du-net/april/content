---
site: Journal du Net
title: "Comment obtenir un environnement IT plus durable avec les technologies open source?"
author: David Szegedi
date: 2023-10-03
href: https://www.journaldunet.com/solutions/dsi/1525199-comment-obtenir-un-environnement-it-plus-durable-avec-les-technologies-d-open-source
tags:
- Informatique en nuage
series:
- 202340
series_weight: 0
---

> Comprendre comment l'open source et le cloud peuvent aider les entreprises dans leur démarche de durabilité liée à leur système informatique. 
