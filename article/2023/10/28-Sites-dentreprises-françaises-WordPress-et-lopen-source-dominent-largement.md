---
site: ZDNet France
title: "Sites d'entreprises françaises: WordPress et l'open source dominent largement"
author: Thierry Noisette
date: 2023-10-28
href: https://www.zdnet.fr/blogs/l-esprit-libre/sites-d-entreprises-francaises-wordpress-et-l-open-source-dominent-largement-39962124.htm
featured_image: https://upload.wikimedia.org/wikipedia/commons/4/40/Que_es_un_CMS.jpg
tags:
- Entreprise
series:
- 202343
---

> L'étude «observatoire du Web» de l'agence Kernix montre la forte prédominance de WordPress parmi les sites web d'entreprise en France.
