---
site: Numerama
title: "L'open source en IA gagne du terrain face aux modèles propriétaires"
description: "Open source vs propriétaire"
author: Julien Lausson
date: 2023-12-16
href: https://www.numerama.com/tech/1590098-lopen-source-en-ia-gagne-du-terrain-face-aux-modeles-proprietaires.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2023/12/glitch-art-design-1024x574.jpg
tags:
- Sciences
series:
- 202350
series_weight: 0
---

> Les modèles de langage open source se rapprochent de leurs rivaux fermés et propriétaires, selon une étude comparant les performances des uns et des autres. Si les seconds sont toujours devant, l'écart s'est réduit en 2023.
