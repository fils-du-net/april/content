---
site: Le Télégramme
title: "Avec Linux Quimper et le Centre des abeilles, c'est Noël toute l'année"
date: 2023-12-27
href: https://www.letelegramme.fr/finistere/quimper-29000/penhars/avec-linux-quimper-et-le-centre-des-abeilles-cest-noel-toute-lannee-6496422.php
featured_image: https://media.letelegramme.fr/api/v1/images/view/658c6ac3bec17021962abd88/web_golden_xxl/658c6ac3bec17021962abd88.1
tags:
- Associations
series:
- 202352
series_weight: 0
---

> Depuis 2009, le Centre des abeilles et Linux Quimper récupèrent des ordinateurs et les remettent en état de fonctionner pour les distribuer. Cette année, ils ont atteint leur 600e don.
