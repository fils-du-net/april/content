---
site: Next
title: "Un roman graphique explique les logiciels libres aux enfants"
description: Hacking 4 freedom
author: Jean-Marc Manach
date: 2023-12-08
href: https://next.ink/119505/un-roman-graphique-explique-les-logiciels-libres-aux-enfants/
featured_image: https://next.ink/wp-content/uploads/2023/12/ADACF1.png
tags:
- Sensibilisation
series:
- 202349
series_weight: 0
---

> Ada & Zangemann, un conte graphique créé pour expliquer l'intérêt des logiciels libres aux enfants mais qui peut intéresser les adultes.
