---
site: ZDNet France
title: "«Ada & Zangemann», un beau livre jeunesse sur les logiciels libres"
author: Thierry Noisette
date: 2023-12-14
href: https://www.zdnet.fr/blogs/l-esprit-libre/ada-zangemann-un-beau-livre-jeunesse-sur-les-logiciels-libres-39963026.htm#xtor%3D123456
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/couv_ada_grand.jpg
tags:
- Sensibilisation
series:
- 202350
series_weight: 0
---

> Un album jeunesse sous licence libre, traduit en français par une centaine d'élèves, présente un conte moderne: skateboards, glace à la framboise, informatique libre et bidouillage. Un cadeau de Noël très recommandé.
