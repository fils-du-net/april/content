---
site: ZDNet France
title: "OpenStack: quand l'open source propulse le cloud souverain"
date: 2023-12-05
href: https://www.zdnet.fr/actualites-partenaires/openstack-quand-l-open-source-propulse-le-cloud-souverain-39962818.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2021/shutterstock_2197939687__w630.jpg
tags:
- Informatique en nuage
series:
- 202349
series_weight: 0
---

> Face à l'enjeu de souveraineté des données et d'indépendance par rapport aux solutions propriétaires des Gafam, l'alternative s'appelle OpenStack. Infomaniak contribue et propulse ses infrastructures cloud avec cette technologie libre dès 2013. Olivier Chaze et Kévin Allioli d'Infomaniak expliquent pourquoi.
