---
site: ZDNet France
title: "Tribune: l'Europe s'appuie sur l'open source via les fondations, par Pierre-Yves Gibello (OW2)"
author: Thierry Noisette
date: 2023-12-10
href: https://www.zdnet.fr/blogs/l-esprit-libre/tribune-l-europe-s-appuie-sur-l-open-source-via-les-fondations-par-pierre-yves-gibello-ow2-39962906.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/Pierre-Yves_Gibello.jpg
tags:
- Économie
- Europe
series:
- 202349
series_weight: 0
---

> Alors que 55 projets de logiciels libres viennent de recevoir le soutien des fonds européens, le DG d'OW2, Pierre-Yves Gibello, rappelle les principes du Libre et souligne l'importance d'un 'récit alternatif' orienté vers la démocratie et les libertés civiles. 
