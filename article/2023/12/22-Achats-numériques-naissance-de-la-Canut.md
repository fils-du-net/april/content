---
site: Républik IT Le Média
title: "Achats numériques: naissance de la Canut"
author: Bertrand Lemaire
date: 2023-12-22
href: https://www.republik-it.fr/solutions-techno/applicatif-metier/achats-numeriques-naissance-de-la-canut.html
featured_image: https://img.republiknews.fr/crop/none/f86e49d074976257417b6c1c80182e36/0/0/1500/844/465/262/association-canut-bien-basee-lyon.jpg
tags:
- Marchés publics
- Administration
series:
- 202351
series_weight: 0
---

> Les collectivités locales et établissements associés vont pouvoir bénéficier d'une nouvelle centrale d'achats numériques, la Canut.
