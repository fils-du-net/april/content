---
site: cio-online.com
title: "L'Open Source s'est immiscé dans la quasi-totalité de l'économie française"
author: Reynald Fléchaux
date: 2023-12-18
href: https://www.cio-online.com/actualites/lire-l-open-source-s-est-immisce-dans-la-quasi-totalite-de-l-economie-francaise-15357.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000020537.jpg
tags:
- Entreprise
series:
- 202351
series_weight: 0
---

> Le secteur public reste en pointe dans l'usage de l'Open Source. Mais ce type de logiciel s'est généralisé également dans les entreprises.
