---
site: ZDNet France
title: "Quatre prix BlueHats pour des mainteneurs de logiciels libres utilisés par l'administration"
author: Thierry Noisette
date: 2023-12-28
href: https://www.zdnet.fr/blogs/l-esprit-libre/quatre-prix-bluehats-pour-des-mainteneurs-de-logiciels-libres-utilises-par-l-administration-39963246.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2014/03/Bluehats%20lancement.jpg
tags:
- Promotion
- Administration
series:
- 202352
series_weight: 0
---

> La mission logiciels libres de la Dinum et la fondation NLnet veulent récompenser des mainteneurs de logiciels libres «fortement utilisés dans l'administration».
