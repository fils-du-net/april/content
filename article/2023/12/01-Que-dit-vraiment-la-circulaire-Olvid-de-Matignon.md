---
site: Silicon
title: "Que dit vraiment la «circulaire Olvid» de Matignon?"
date: 2023-12-01
href: https://www.silicon.fr/circulaire-olvid-matignon-473891.html
featured_image: https://www.silicon.fr/wp-content/uploads/2022/01/Olvid-open-source.jpg
tags:
- Institutions
- Logiciels privateurs
series:
- 202348
series_weight: 0
---

> Les membres du Gouvernement et des cabinets ministériels ont jusqu'au 8 décembre 2023 pour installer Olvid. Que contient la circulaire qui le leur intime?
