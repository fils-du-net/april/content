---
site: InformatiqueNews.fr
title: "L'open source au service d'un environnement IT plus durable?"
author: David Szegedi
date: 2023-12-21
href: https://www.informatiquenews.fr/l-open-source-au-service-dun-environnement-it-plus-durable-david-szegedi-red-hat-96807
featured_image: https://www.informatiquenews.fr/wp-content/uploads/2023/12/open-source-et-I8T-durable.jpg
tags:
- Informatique en nuage
series:
- 202351
series_weight: 0
---

> La sobriété numérique est à l'agenda de toutes DSI. Dans leur quête d'une IT plus durable, elles peuvent se tourner vers l'open source.
