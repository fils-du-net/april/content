---
site: Silicon
title: "Open source: LXD change à son tour de licence"
author: Clément Bohic
date: 2023-12-14
href: https://www.silicon.fr/open-source-lxd-change-licence-474266.html
featured_image: https://www.silicon.fr/wp-content/uploads/2023/12/LXD-AGPLv3.jpg
tags:
- Licenses
series:
- 202350
series_weight: 0
---

> Canonical opère à son tour la bascule d'Apache 2.0 vers AGPLv3, pour le projet LXD. Les porteurs du fork Incus haussent le ton.
