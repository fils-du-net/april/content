---
site: ZDNet France
title: "Libre et open source express: dons aux associations, April, Eclipse, journalisme"
author: Thierry Noisette
date: 2023-12-31
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-dons-aux-associations-april-eclipse-journalisme-39963264.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/2024%20sable.jpeg
tags:
- april
- Associations
series:
- 202401
series_weight: 0
---

> En bref. Dernières minutes pour les dons 2023. L'April arrête Twitter. À la découverte de la fondation Eclipse. Pourquoi les journalistes ne parlent pas du logiciel libre.
