---
site: Clubic.com
title: "Vous êtes francophone et voulez participer à l'essor de l'open source? Mozilla a besoin de votre voix"
author: Mallory Delicourt
date: 2023-12-19
href: https://www.clubic.com/actualite-513058-vous-etes-francophone-et-voulez-participer-a-l-essor-de-l-open-source-mozilla-a-besoin-de-votre-voix.html
featured_image: https://pic.clubic.com/v1/images/2175034/raw.webp?hash=23a367d219b33409df1f81aa1597b096eb7b970c
tags:
- Innovation
series:
- 202351
series_weight: 0
---

> Le projet Common Voice de Mozilla, dont l'objectif est de créer une base de données vocable accessible à tous, a récemment lancé un appel pour avancer sur certaines langues représentatives des minorités, mais également sur le français.
