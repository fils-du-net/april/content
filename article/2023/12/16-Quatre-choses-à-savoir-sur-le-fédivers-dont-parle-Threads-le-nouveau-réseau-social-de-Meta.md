---
site: Libération
title: "Quatre choses à savoir sur le «fédivers» dont parle Threads, le nouveau réseau social de Meta"
author: Elise Viniacourt
date: 2023-12-16
href: https://www.liberation.fr/economie/economie-numerique/quatre-choses-a-savoir-sur-le-fedivers-dont-parle-threads-le-nouveau-reseau-social-de-meta-20231216_4GVVK2DIVZADRLBWZENZWJ4JPY
featured_image: https://cloudfront-eu-central-1.images.arcpublishing.com/liberation/LCP4W677TNCGHL33W6EWWIEXYU.jpg
tags:
- Internet
- Interopérabilité
series:
- 202350
series_weight: 0
---

> Depuis sa création en juillet, le nouveau réseau social Threads, lancé jeudi 14 décembre dans l'UE par Meta, dit vouloir rejoindre le «fédivers». On éclaire vos lanternes sur ce mot-valise, en passe de gagner en popularité.
