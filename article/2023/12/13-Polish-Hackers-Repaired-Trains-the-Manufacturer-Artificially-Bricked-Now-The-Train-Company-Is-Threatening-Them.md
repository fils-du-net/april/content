---
site: 404 Media
title: "Polish Hackers Repaired Trains the Manufacturer Artificially Bricked. Now The Train Company Is Threatening Them"
author: Jason Koebler
date: 2023-12-13
href: https://www.404media.co/polish-hackers-repaired-trains-the-manufacturer-artificially-bricked-now-the-train-company-is-threatening-them
featured_image: https://www.404media.co/content/images/size/w2000/2023/12/Screenshot-2023-12-12-at-11.27.26-AM.png
tags:
- DRM
- Droit d'auteur
- English
series:
- 202350
series_weight: 0
---

> After breaking trains simply because an independent repair shop had worked on them, NEWAG is now demanding that trains fixed by hackers be removed from service.
