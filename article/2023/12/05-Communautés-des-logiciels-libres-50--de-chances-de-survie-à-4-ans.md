---
site: ZDNet France
title: "Communautés des logiciels libres: 50 % de chances de survie à 4 ans"
date: 2023-12-05
href: https://www.zdnet.fr/actualites/communaute-des-logiciels-libres-50-de-chances-de-survie-a-4-ans-39962800.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2021/06/OpenSource__w630.jpg
tags:
- Sensibilisation
series:
- 202349
series_weight: 0
---

> NTT et l'université de Kyushu (Japon) ont analysé 'six mythes' autour de la communauté des logiciels libres à partir de plus de 40 000 données sur des dépôts sur GitHub.
