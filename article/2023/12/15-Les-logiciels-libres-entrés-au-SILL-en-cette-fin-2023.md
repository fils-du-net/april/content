---
site: Silicon
title: "Les logiciels libres entrés au SILL en cette fin 2023"
author: Clément Bohic
date: 2023-12-15
href: https://www.silicon.fr/logiciels-libres-sill-fin-2023-474304.html
featured_image: https://www.silicon.fr/wp-content/uploads/2022/01/entreprises-françaises-logiciel-libre-SILL.jpg
tags:
- Référentiel
series:
- 202350
series_weight: 0
---

> Depuis la rentrée, le SILL (Socle interministériel de logiciels libres) s'est enrichi d'une trentaine d'entrées. Les voici.
