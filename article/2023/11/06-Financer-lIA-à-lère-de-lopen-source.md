---
site: Les Echos
title: "Financer l'IA à l'ère de l'open source (€)"
author: Sylvain Duranton
date: 2023-11-06
href: https://www.lesechos.fr/idees-debats/editos-analyses/financer-lia-a-lere-de-lopen-source-2027040
featured_image: https://media.lesechos.com/api/v1/images/view/65491bd7cc02477150743abb/1024x576-webp/0903310084325-web-tete.webp
tags:
- Sciences
- Économie
series:
- 202345
---

> Un nouveau match de titans s'est engagé dans l'arène de l'IA générative, où se défient les modèles commerciaux et les modèles gratuits.
