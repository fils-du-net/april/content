---
site: l'Humanité.fr
title: "Olivier Grieco, la liberté mise en ondes"
author: Eugénie Barbezat
date: 2023-11-10
href: https://www.humanite.fr/medias/dons-aux-associations/olivier-grieco-la-liberte-mise-en-ondes
featured_image: https://www.humanite.fr/wp-content/uploads/2023/11/der-Olivier-Grieco_VEN-edited.jpg
tags:
- Associations
- Promotion
series:
- 202345
series_weight: 0
---

> Le fondateur de la plus jeune radio associative francilienne lance un appel aux auditeurs de Cause commune (93.1 FM) et aux défenseurs du tiers-secteur audiovisuel pour que l’aventure commencée en 2018 perdure et se déploie.
