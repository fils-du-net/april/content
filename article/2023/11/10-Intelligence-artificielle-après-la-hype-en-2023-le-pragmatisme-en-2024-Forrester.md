---
site: LeMagIT
title: "Intelligence artificielle: après la «hype» en 2023, le pragmatisme en 2024? (Forrester)"
author: Philippe Ducellier,
date: 2023-11-10
href: https://www.lemagit.fr/actualites/366559076/Intelligence-artificielle-apres-la-hype-en-2023-le-pragmatisme-en-2024-Forrester
featured_image: https://cdn.ttgtmedia.com/visuals/German/article/ai-robot-macine-learning-blackboard-adobe.jpg
tags:
- Sciences
- Entreprise
series:
- 202345
series_weight: 0
---

> D’après Forrester, 2024 devrait être l’année où les technologies d'IA continueront à se démocratiser, mais aussi celles où les différentes contraintes –comme le Shadow AI et la gouvernance– vont être sérieusement prises en compte.
