---
site: Next INpact
title: "Mozilla propose un dépôt Debian pour Firefox Nightly, un changement plus important qu'il n'y paraît"
author: Vincent Hermann
date: 2023-11-02
href: https://www.nextinpact.com/article/72793/mozilla-propose-depot-debian-pour-firefox-nightly-changement-plus-important-quil-ny-parait
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/9912.jpg
tags:
- Sensibilisation
series:
- 202344
series_weight: 0
---

> Mozilla propose désormais sa branche Nightly au format de paquet Debian pour les personnes intéressées par des tests. Ce qui n’a l’air de rien en apparence devrait marquer un important changement dans la manière dont la fondation distribue son navigateur sur les distributions Linux.
