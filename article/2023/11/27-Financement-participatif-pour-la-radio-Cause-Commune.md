---
site: ZDNet France
title: "Financement participatif pour la radio Cause Commune"
author: Thierry Noisette
date: 2023-11-27
href: https://www.zdnet.fr/blogs/l-esprit-libre/financement-participatif-pour-la-radio-cause-commune-39962644.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/Cause%20commune%20radio%20logo.png
tags:
- april
- Promotion
series:
- 202348
series_weight: 0
---

> «Radio associative et citoyenne», en proie à de gros ennuis de trésorerie, Cause Commune diffuse notamment l'émission hebdomadaire «Libre à vous» de l'association libriste April.
