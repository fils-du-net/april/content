---
site: Le Monde Informatique
title: "GPT-4 Turbo et Assistants, une menace pour les solutions open source"
author: Anirban Ghoshal
date: 2023-11-16
href: https://www.lemondeinformatique.fr/actualites/lire-gpt-4-turbo-et-assistants-une-menace-pour-les-solutions-open-source-92119.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000094687.png
tags:
- Économie
- Sciences
series:
- 202346
series_weight: 0
---

> OpenAI tente de se positionner comme une alternative viable au développement open source de type «build-it-yourself» avec des produits moins chers et des capacités avancées. L'arrivée de GPT-4 Turbo et de l'API Assistant montre clairement cette voie selon les analystes et les spécialistes du marché de l'IA générative.
