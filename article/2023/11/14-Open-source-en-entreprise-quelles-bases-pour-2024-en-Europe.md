---
site: Silicon
title: "Open source en entreprise: quelles bases pour 2024 en Europe"
author: Clément Bohic
date: 2023-11-14
href: https://www.silicon.fr/open-source-entreprises-europe-2024-473347.html
featured_image: https://www.silicon.fr/wp-content/uploads/2023/11/AdobeStock_364761614-scaled.jpeg
tags:
- Entreprise
- Europe
series:
- 202346
series_weight: 0
---

> Où en sont l’usage et la contribution à l’open source dans les entreprises européennes? Aperçu à l’appui de rapports de la Fondation Linux.
