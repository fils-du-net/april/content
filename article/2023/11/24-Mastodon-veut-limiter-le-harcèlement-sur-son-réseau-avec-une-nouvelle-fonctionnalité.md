---
site: Clubic.com
title: "Mastodon veut limiter le harcèlement sur son réseau avec une nouvelle fonctionnalité"
author: Maxence Glineur
date: 2023-11-24
href: https://www.clubic.com/actualite-510169-mastodon-veut-limiter-le-harcelement-sur-son-reseau-avec-une-nouvelle-fonctionnalite.html
featured_image: https://pic.clubic.com/v1/images/2165527/raw.webp?fit=max&width=1200&hash=c17dee0eae582032724a43357319fb53fe751d09
tags:
- Internet
series:
- 202347
series_weight: 0
---

> Le concurrent de X.com (anciennement Twitter) affichera des messages d'avertissement pour dissuader certains comportements désagréables.
