---
site: ZDNet France
title: "Open source en Europe: BlueHats, l'initiative française du secteur public, lauréate"
author: Thierry Noisette
date: 2023-11-05
href: https://www.zdnet.fr/blogs/l-esprit-libre/open-source-en-europe-bluehats-l-initiative-francaise-du-secteur-public-laureate-39962242.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2014/03/Bluehats%20lancement.jpg
tags:
- Promotion
- Europe
- Administration
series:
- 202345
series_weight: 0
---

> Cette initiative de la Dinum née en 2018 a gagné le Prix de la Communauté de l'Observatoire du logiciel libre de la Commission européenne (OSOR).
