---
site: atlantico
title: "Le législateur français rattrapé par Bruxelles"
author: Samuel Le Goff
date: 2023-11-02
href: https://atlantico.fr/article/rdv/le-legislateur-francais-rattrape-par-bruxelles-regulation-de-l-espace-numerique-reseaux-sociaux-thierry-breton-assemblee-nationale-deputes-legislation-europeenne-opposition-souverainete-nationale-samuel-le-goff
featured_image: https://atlantico-media.s3.eu-west-3.amazonaws.com/33_CM_4_GN_highres_93d4cab32e.jpg
tags:
- Europe
series:
- 202344
---

> Un projet de loi visant à sécuriser et réguler l'espace numérique est en cours d'examen à l'Assemblée Nationale. Le commissaire européen Thierry Breton a envoyé à la France une mise en demeure imposant un gel d'au moins trois mois du processus.
