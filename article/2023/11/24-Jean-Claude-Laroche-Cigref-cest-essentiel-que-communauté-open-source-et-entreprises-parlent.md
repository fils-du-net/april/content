---
site: Républik IT Le Média
title: "Jean-Claude Laroche (Cigref): «c'est essentiel que communauté open-source et entreprises parlent»"
author: Bertrand Lemaire
date: 2023-11-24
href: https://www.republik-it.fr/decideurs-it/gouvernance/jean-claude-laroche-cigref-c-est-essentiel-que-communaute-open-source-et-entreprises-parlent.html
featured_image: https://img.republiknews.fr/crop/none/082fcc17552d951294a697fbd8e9e636/0/0/1500/844/465/262/jean-claude-laroche-president-cigref.jpg
tags:
- Entreprise
series:
- 202347
series_weight: 0
---

> Parrain de l'Open CIO Summit 2023, Jean-Claude Laroche, président du Cigref, nous explique le sens de son engagement.
