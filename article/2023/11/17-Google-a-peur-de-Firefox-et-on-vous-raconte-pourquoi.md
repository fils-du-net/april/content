---
site: Clubic.com
title: "Google a peur de Firefox, et on vous raconte pourquoi"
author: Maxence Glineur
date: 2023-11-17
href: https://www.clubic.com/actualite-509287-google-a-peur-de-firefox-et-on-vous-raconte-pourquoi.html
featured_image: https://pic.clubic.com/v1/images/2162620/raw.webp?fit=max&width=1200&hash=6a84ed7c21af6fa24772f91405ac00008e467c1e
tags:
- Vie privée
- Internet
series:
- 202346
series_weight: 0
---

> Face aux critiques, le géant américain doit faire machine arrière sur la question des bloqueurs de publicité.
