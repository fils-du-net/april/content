---
site: Journal du Net
title: "Achats logiciels: le TCO incontournable pour comparer l'incomparable"
author: Eryk Markiewicz
date: 2023-11-27
href: https://www.journaldunet.com/solutions/dsi/1526503-achats-logiciels-le-tco-incontournable-pour-comparer-l-incomparable
tags:
- Économie
series:
- 202348
series_weight: 0
---

> Avec la complexification des SI, le total cost of ownership demeure le seul outil indiscutable pour permettre aux DSI de faire les meilleurs choix logiciels, tout en optimisant leur budget.
