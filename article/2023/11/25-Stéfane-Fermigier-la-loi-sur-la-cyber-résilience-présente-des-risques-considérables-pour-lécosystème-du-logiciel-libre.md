---
site: Archimag
title: "Stéfane Fermigier: 'la loi sur la cyber-résilience présente des risques considérables pour l'écosystème du logiciel libre'"
author: Bruno Texier
href: https://www.archimag.com/vie-numerique/2023/11/24/loi-cyber-resilience-presente-risques-considerables-ecosysteme-logiciel
featured_image: https://www.archimag.com/sites/archimag.com/files/styles/article/public/web_articles/image/stephane-fermigier-entreprises-logiciels-libres.jpg
tags:
- Institutions
- Europe
series:
- 202347
series_weight: 0
---

> Stéfane Fermigier est cofondateur et coprésident du CNLL (Union des entreprises du logiciel libre et du numérique ouvert), il revient sur les risques que la loi sur la cyberrésilience (Cyber Resilience Act) fait peser sur la filière du logiciel libre en France et en Europe.
