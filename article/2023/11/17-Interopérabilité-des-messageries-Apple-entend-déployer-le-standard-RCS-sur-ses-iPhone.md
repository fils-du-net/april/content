---
site: Le Monde.fr
title: "Interopérabilité des messageries: Apple entend déployer le standard RCS sur ses iPhone"
date: 2023-11-17
href: https://www.lemonde.fr/pixels/article/2023/11/17/interoperabilite-des-messageries-apple-entend-deployer-le-standard-rcs-sur-ses-iphone_6200706_4408996.html
tags:
- Interopérabilité
series:
- 202346
series_weight: 0
---

> Mise sous pression par l’UE, Apple adoptera en 2024 le standard de messages courts enrichis (RCS) sur ses smartphones, en plus de sa solution maison, iMessages. Une décision qui ouvre la voie à plusieurs améliorations lorsque des SMS sont échangés entre les mobiles Android et iOS.
