---
site: EurActiv
title: "L'UE est-elle prête à faire face à la concentration du marché de l'IA?"
author: Luca Bertuzzi 
date: 2023-11-06
href: https://www.euractiv.fr/section/intelligence-artificielle/news/lue-est-elle-prete-a-faire-face-a-la-concentration-du-marche-de-lia
featured_image: https://www.euractiv.fr/wp-content/uploads/sites/3/2023/11/shutterstock_2273556625-800x450.jpg
tags:
- Sciences
- Europe
series:
- 202345
---

> Alors que le marché de l'intelligence artificielle est voué à se concentrer, les experts interrogés pas Euractiv estiment que même les règlementations les plus récentes de l'UE pourraient ne pas être suffisantes pour empêcher certaines entreprises d'acquérir une position dominante.
