---
site: La Presse
title: "Diabète | «J'ai des patients qui prient pour que leur pompe ne brise pas»"
author: Mathieu Perreault
date: 2023-11-14
href: https://www.lapresse.ca/actualites/sante/2023-11-14/diabete/j-ai-des-patients-qui-prient-pour-que-leur-pompe-ne-brise-pas.php
featured_image: https://mobile-img.lpcdn.ca/v2/924x/r3996/f1409b4b96213425a246bbe0bd3a432f.webp
tags:
- Sciences
series:
- 202346
series_weight: 0
---

> Des milliers de Québécois souffrant de diabète de type 1 doivent gérer leur maladie avec les moyens du bord, faute de financement public des pompes à insuline. Ces appareils très coûteux sont remboursés par l'assurance maladie dans plusieurs autres provinces, dont l'Ontario.
