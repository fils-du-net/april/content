---
site: BFMtv
title: "Wikipédia bloque l'académie d'Aix-Marseille pour 'vandalisme'"
author: Louis Mbembe
date: 2023-11-17
href: https://www.bfmtv.com/tech/vie-numerique/wikipedia-bloque-l-academie-d-aix-marseille-pour-vandalisme_AV-202311170689.html
tags:
- Partage du savoir
series:
- 202346
series_weight: 0
---

> L'académie d'Aix-Marseille n'a plus le droit de rédiger de contributions sur Wikipédia. Motif: à travers celles-ci, les étudiants de l'institution "vandalisent" l'encyclopédie en ligne estiment ses administrateurs.
