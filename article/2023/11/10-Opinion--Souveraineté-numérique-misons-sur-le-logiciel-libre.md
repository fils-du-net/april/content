---
site: Les Echos
title: "Souveraineté numérique: misons sur le logiciel libre (€)"
author: Alain Issarni
date: 2023-11-10
href: https://www.lesechos.fr/idees-debats/cercle/opinion-souverainete-numerique-misons-sur-le-logiciel-libre-2028214
featured_image: https://media.lesechos.com/api/v1/images/view/654e525de7e4bb2a1659c727/1024x576-webp/0903550924669-web-tete.webp
tags:
- International
- Sensibilisation
series:
- 202345
series_weight: 0
---

> La souveraineté numérique est devenue un enjeu majeur dans un monde de plus en plus interconnecté. Le logiciel libre peut ainsi être un outil pour mieux maîtriser les dépendances numériques, estime Alain Issarni.
