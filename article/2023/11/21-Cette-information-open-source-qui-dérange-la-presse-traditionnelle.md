---
site: atlantico
title: "Cette information open source qui dérange la presse traditionnelle"
date: 2023-11-21
href: https://atlantico.fr/article/decryptage/cette-information-open-source-qui-derange-la-presse-traditionnelle-hashtable-h16
featured_image: https://atlantico-media.s3.eu-west-3.amazonaws.com/pexels_brotin_biswas_518543_1_e9d603d1b2.jpg
tags:
- Partage du savoir
series:
- 202347
series_weight: 0
---

> Il faut le dire simplement: la presse traditionnelle ne fait plus de journalisme, ni d'information. Elle est à peine capable de rapporter les potins.
