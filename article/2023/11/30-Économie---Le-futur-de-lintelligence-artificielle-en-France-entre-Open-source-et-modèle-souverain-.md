---
site: LaProvence.com
title: "Économie - Le futur de l'intelligence artificielle en France: entre Open source et modèle souverain"
author: Gwendeline Jerad
date: 2023-11-30
href: https://www.laprovence.com/article/economie/7985241360454/le-futur-de-l-intelligence-artificielle-en-france-entre-open-source-et-modele-souverain
featured_image: https://images.laprovence.com/media/bpi/20231130/606166ecb3932778a733420e53f76e435c2fd97fba6f0a7c4301228ede15e46e.png?twic=v1/cover=1000x563/output=preview
tags:
- Sciences
- Économie
series:
- 202348
series_weight: 0
---

> Une IA construite grâce à des données en libre accès, pour que chacun d’entre nous puisse profiter d’un système personnel tenant compte de ses besoins particulier. C'est l’avenir de l’intelligence artificielle annoncé à ai-Pulse.
