---
site: Le Monde.fr
title: "Inquiétudes autour d'un règlement européen sur la sécurité des navigateurs Web"
author: Damien Leloup
date: 2023-11-02
href: https://www.lemonde.fr/pixels/article/2023/11/02/inquietudes-autour-d-un-reglement-europeen-sur-la-securite-des-navigateurs-web_6197816_4408996.html
tags:
- Internet
- Europe
series:
- 202344
series_weight: 0
---

> Dans une lettre ouverte publiée jeudi, plusieurs centaines de spécialistes de la sécurité et de la cryptographie contestent un texte bientôt soumis au vote au Parlement européen, parce qu’il impose aux navigateurs Web d’utiliser certains certificats de sécurité sélectionnés par les Etats membres.
