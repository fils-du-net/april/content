---
site: Le Monde Informatique
title: "Des auteurs poursuivent OpenAI et Microsoft pour violation du droit d'auteur"
author: Elizabeth Montalbano
date: 2023-11-23
href: https://www.lemondeinformatique.fr/actualites/lire-des-auteurs-poursuivent-openai-et-microsoft-pour-violation-du-droit-d-auteur-92227.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000094860.jpg
tags:
- Droit d'auteur
- Sciences
series:
- 202347
series_weight: 0
---

> La plainte, l'une des nombreuses déposées pour des motifs similaires, intervient dans un contexte tumultueux pour OpenAI, après la mise à l'écart surprise de son fondateur Sam Altman, la semaine dernière, et son retour il y a un jour.
