---
site: Le Monde.fr
title: "IA: le comité d'éthique propose de davantage réguler les services comme ChatGPT que les modèles «open source» (€)"
author: Alexandre Piquard
date: 2023-07-05
href: https://www.lemonde.fr/economie/article/2023/07/05/ia-le-comite-d-ethique-propose-de-davantage-reguler-les-services-comme-chatgpt-que-les-modeles-open-source_6180674_3234.html
tags:
- Sciences
- Institutions
series:
- 202327
series_weight: 0
---

> Dans un avis, le Comité national pilote d'éthique du numérique souhaite imposer des obligations plus contraignantes aux modèles d'intelligence artificielle «mis sur le marché».
