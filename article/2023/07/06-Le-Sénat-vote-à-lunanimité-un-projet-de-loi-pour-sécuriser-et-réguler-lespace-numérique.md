---
site: Le Monde.fr
title: "Le Sénat vote à l'unanimité un projet de loi pour «sécuriser et réguler l'espace numérique»"
date: 2023-07-06
href: https://www.lemonde.fr/pixels/article/2023/07/06/le-senat-vote-a-l-unanimite-un-projet-de-loi-pour-securiser-et-reguler-l-espace-numerique_6180758_4408996.html
tags:
- Internet
- Institutions
series:
- 202327
series_weight: 0
---

> Le texte comprend un arsenal de mesures: blocage des sites pornographiques sans passer par un juge, création d’un «filtre anti-arnaques», lutte contre les «deepfakes», «bannissement» des réseaux sociaux… Il sera examiné par les députés à la rentrée.
