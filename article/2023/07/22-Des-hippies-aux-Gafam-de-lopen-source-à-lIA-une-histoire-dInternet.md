---
site: ZDNet France
title: "Des hippies aux Gafam, de l'open source à l'IA, une histoire d'Internet"
author: Thierry Noisette
date: 2023-07-22
href: https://www.zdnet.fr/blogs/l-esprit-libre/des-hippies-aux-gafam-de-l-open-source-a-l-ia-une-histoire-d-internet-39960768.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/livre%20Comment%20les%20hippies.jpg
tags:
- Sensibilisation
- Internet
series:
- 202330
series_weight: 0
---

> Gilles Babinet, coprésident du Conseil national du numérique, dresse un tableau éloquent de l'histoire du Net, de ses grandes étapes et figures, dont Stallman et Torvalds.
