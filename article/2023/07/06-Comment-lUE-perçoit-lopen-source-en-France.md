---
site: Silicon
title: "Comment l'UE perçoit l'open source en France"
author: Clément Bohic
date: 2023-07-06
href: https://www.silicon.fr/comment-ue-percoit-open-source-france-469390.html
featured_image: https://www.silicon.fr/wp-content/uploads/2023/07/AdobeStock_525262191-scaled.jpeg
tags:
- Administration
- Promotion
- april
series:
- 202327
series_weight: 0
---

> Le rapport de la DIGIT sur l'état de l'open source en France a été mis à jour. Entre stratégies, cadre juridique et réalisations concrètes, que dépeint-il?
