---
site: ZDNet France
title: "Proton: un gestionnaire de mots de passe avec chiffrement de bout en bout"
author: Lance Whitney
date: 2023-07-03
href: https://www.zdnet.fr/actualites/proton-un-gestionnaire-de-mots-de-passe-avec-chiffrement-de-bout-en-bout-39960190.htm
featured_image: https://www.zdnet.com/a/img/2023/06/29/34d59b7e-eeaf-4d39-84d2-dca4fc34dce0/figure-top-proton-officially-launches-password-manager-with-end-to-end-encryption.jpg
tags:
- Vie privée
- Innovation
series:
- 202327
---

> Conçu pour protéger tous vos identifiants et détails de compte, le nouveau gestionnaire de mots de passe Proton Pass est disponible en version gratuite et payante.
