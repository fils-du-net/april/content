---
site: leParisien.fr
title: "Pourquoi Threads, l'alternative à Twitter imaginée par Meta, ne sera pas disponible en France"
date: 2023-07-05
href: https://www.leparisien.fr/high-tech/pourquoi-threads-lalternative-a-twitter-imaginee-par-meta-ne-sera-pas-disponible-en-france-05-07-2023-6KPIWKNRX5HM3GKCZ36KKSFQ44.php
featured_image: https://www.leparisien.fr/resizer/2nYtcNB8EZVlagdZgPcCbwz3De8=/932x582/cloudfront-eu-central-1.images.arcpublishing.com/leparisien/224M3RWIEZGK3NMODJX5RKMFJQ.jpg
tags:
- Internet
- International
series:
- 202327
---

> Le groupe californien a annoncé pour jeudi le lancement du nouveau réseau social Threads, une occasion pour lui de profiter des déboires de Twitter. Les Français comme l'ensemble des Européens ne pourront toutefois pas y accéder.
