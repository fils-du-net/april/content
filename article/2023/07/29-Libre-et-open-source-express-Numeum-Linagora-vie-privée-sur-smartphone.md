---
site: ZDNet France
title: "Libre et open source express: Numeum, Linagora, vie privée sur smartphone"
author: Thierry Noisette
date: 2023-07-29
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-numeum-linagora-vie-privee-sur-smartphone-39960770.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/05/open-source-KeepCalmAndUse.jpg
tags:
- Licenses
- Vie privée
series:
- 202330
series_weight: 0
---

> Véronique Torner à la présidence de Numeum. Linagora revient à la licence Affero GPL v3 simple. /e/OS équipe un des Fairphone.
