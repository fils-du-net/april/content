---
site: 01net.
title: "Google ne veut pas se faire manger dans la course à l'intelligence artificielle"
author: Mickael Bazoge
date: 2023-07-12
href: https://www.01net.com/actualites/google-sure-de-sa-position-dans-la-course-a-lintelligence-artificielle.html
featured_image: https://www.01net.com/app/uploads/2023/07/Sans-titre-5-9-1360x905.jpg
tags:
- Sciences
series:
- 202330
series_weight: 0
---

> Google a bien l'intention de faire la course à l'intelligence artificielle, même si le moteur de recherche a pris en marche le train de l'IA générative, lancé à toute allure par ChatGPT et Microsoft.
