---
site: Next INpact
title: "Cyber Resilience Act: une commission du Parlement européen exempte les logiciels libres non commerciaux"
description: Le garage VS le commerce
author: Jean-Marc Manach
date: 2023-07-03
href: https://www.nextinpact.com/article/72006/cyber-resilience-act-commission-parlement-europeen-exempte-logiciels-libres-non-commerciaux
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/5878.jpg
tags:
- Institutions
- Europe
- April
series:
- 202327
series_weight: 0
---

> Après avoir initialement été ignorés, les défenseurs des logiciels libres et open source ont obtenu de ne pas avoir à respecter les mêmes règles et normes que ceux susceptibles de devoir estampiller leurs produits commerciaux de la «norme CE».
