---
site: ZDNet France
title: "Red Hat soupçonné de trahir l'open source"
author: Steven Vaughan-Nichols
date: 2023-07-05
href: https://www.zdnet.fr/actualites/red-hat-soupconne-de-trahir-l-open-source-39960282.htm
featured_image: https://www.zdnet.com/a/img/2023/07/03/34aca7d0-84b1-4fc7-ad37-32cc3123a003/gettyimages-1212755749.jpg
tags:
- Entreprise
series:
- 202327
series_weight: 0
---

> Les rivaux de Red Hat et de nombreux développeurs de logiciels libres sont furieux de la nouvelle politique de l'entreprise en matière de code source. Ce différend se terminera-t-il devant les tribunaux?
