---
site: Libération
title: "Microsoft: l'UE ouvre une enquête pour abus de position dominante visant Teams"
date: 2023-07-27
href: https://www.liberation.fr/economie/economie-numerique/microsoft-lue-ouvre-une-enquete-pour-abus-de-position-dominante-visant-teams-20230727_5IUZ76PYQJAKRFZ4Y5PDQCWNZ4/
featured_image: https://cloudfront-eu-central-1.images.arcpublishing.com/liberation/J7SZEYI2XVAGLODE62PHFPUNJI.jpg
tags:
- Entreprise
- Europe
series:
- 202330
---

> La Commission européenne a lancé des investigations ce jeudi 27 juillet pour déterminer si le géant américain a enfreint les règles de concurrence en liant son appli de visioconférence avec sa suite de logiciels Microsoft 365.
