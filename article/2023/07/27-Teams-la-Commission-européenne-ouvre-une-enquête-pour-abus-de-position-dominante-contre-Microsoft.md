---
site: Next INpact
title: "Teams: la Commission européenne ouvre une enquête pour abus de position dominante contre Microsoft"
description: Le nouvel Internet Explorer
author: Vincent Hermann
date: 2023-07-27
href: https://www.nextinpact.com/article/72147/teams-commission-europeenne-ouvre-enquete-pour-abus-position-dominante-contre-microsoft
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/175.jpg
tags:
- Entreprise
- Europe
series:
- 202330
series_weight: 0
---

> La rumeur circulait depuis quelque temps, elle est désormais confirmée: la Commission européenne vient d’annoncer officiellement l'ouverture d'une enquête contre Teams, la messagerie de Microsoft pour les entreprises. L’éditeur pourrait y avoir abusé de sa position dominante.
