---
site: Libération
title: "Cosmolocalisme: et si on se fabriquait un tracteur, une prothèse, un satellite… (€)"
date: 2023-01-23
href: https://www.liberation.fr/idees-et-debats/cosmolocalisme-linternationale-du-do-it-yourself-20230123_GSXQNTJHCVFIRCIXZY5GRSFNGM
featured_image: https://cloudfront-eu-central-1.images.arcpublishing.com/liberation/UJ3AUYYX3BECZOUOPVUWO55UN4.JPG
tags:
- Matériel libre
series:
- 202304
series_weight: 0
---

> Refusant un modèle où les produits high-tech, sous brevets, sont chers et impossibles à réparer, ce mouvement s’appuie sur la philosophie de l’open source pour promouvoir des technologies alternatives. Fédérant des collectifs du monde entier, il a déjà permis de produire un satellite, des machines agricoles ou une prothèse de la main.
