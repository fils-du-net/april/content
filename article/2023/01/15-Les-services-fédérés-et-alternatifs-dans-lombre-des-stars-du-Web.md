---
site: Le Monde Informatique
title: "Les services fédérés et alternatifs: dans l'ombre des stars du Web"
author: Adam Taylor
date: 2023-01-15
href: https://www.lemondeinformatique.fr/actualites/lire-les-services-federes-et-alternatifs-dans-l-ombre-des-stars-du-web-88821.html
featured_image: https://images.itnewsinfo.com/lmi/articles/grande/000000089613.png
tags:
- Promotion
- Internet
series:
- 202302
series_weight: 0
---

> Les réseaux sociaux ont explosé ces dernières années et la publicité également. Récemment, la vague d'utilisateurs Twitter ayant décidé de migrer vers Mastodon a relancé le débat sur les réseaux fédérés et alternatifs comme ce dernier. L'occasion de faire le point sur ceux qui existent dans l'ombre des géants du Web.
