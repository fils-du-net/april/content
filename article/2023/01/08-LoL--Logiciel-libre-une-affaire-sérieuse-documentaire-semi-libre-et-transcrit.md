---
site: ZDNet France
title: "«LoL – Logiciel libre, une affaire sérieuse», documentaire semi-libre et transcrit"
author: Thierry Noisette
date: 2023-01-08
href: https://www.zdnet.fr/blogs/l-esprit-libre/lol-logiciel-libre-une-affaire-serieuse-documentaire-semi-libre-et-transcrit-39952222.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/lol-logiciel-libre-une-affaire-serieuse-300x169.jpg
tags:
- Sensibilisation
- april
- Licenses
series:
- 202301
series_weight: 0
---

> Ce film, qui présente clairement en une petite heure les logiciels libres et leurs enjeux, est à présent sous licence CC non commerciale, et l'April a transcrit son contenu.
