---
site: 20minutes.fr
title: "«Le monde ne se privera jamais d'un journaliste qui s'y connaît dans un domaine», pour Iona Manolescu"
author: Lina Fourneau
date: 2023-01-09
href: https://www.20minutes.fr/medias/4011610-20230109-monde-privera-jamais-journaliste-connait-domaine-iona-manolescu
featured_image: https://img.20mn.fr/VP5flsF6TQ69Pn6KprtAiSk/830x532_les-donnees-peuvent-aides-au-traitement-de-l-information-mais-sont-souvent-ignorees
tags:
- Partage du savoir
- Open Data
series:
- 202302
series_weight: 0
---

> Ioana Manolescu, chercheuse en informatique à l'Inria, travaille aux côtés des rédactions pour les aider à mieux se servir des données
