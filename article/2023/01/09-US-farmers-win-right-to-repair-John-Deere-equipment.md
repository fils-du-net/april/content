---
site: BBC News
title: "US farmers win right to repair John Deere equipment"
author: Monica Miller
date: 2023-01-09
href: https://www.bbc.com/news/business-64206913
featured_image: https://ichef.bbci.co.uk/news/976/cpsprodpb/2F15/production/_128235021_capture.jpg.webp
tags:
- Innovation
series:
- 202302
---

> Tractor maker John Deere has agreed to give its US customers the right to fix their own equipment.
