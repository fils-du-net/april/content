---
site: Next INpact
title: "Qu'implique la construction d'un smartphone équitable? (€)"
description: "Du temps, principalement"
author: Mathilde Saliou
date: 2023-01-25
href: https://www.nextinpact.com/article/70871/quimplique-construction-dun-smartphone-equitable
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/12517.jpg
tags:
- Innovation
series:
- 202304
---

> Vous connaissez peut-être Fairphone, le smartphone qui met l’accent sur la réparabilité. Mais qu’est-ce que ça implique, au juste, de construire un téléphone respectueux de l’environnement et des personnes qui le construisent ? On a posé la question à Agnès Crépet, directrice informatique chez le constructeur.
