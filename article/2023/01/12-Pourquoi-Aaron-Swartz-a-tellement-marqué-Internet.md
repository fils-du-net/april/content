---
site: Numerama
title: "Pourquoi Aaron Swartz a tellement marqué Internet"
description: Un héritage qui vit encore aujourd'hui
author: Julien Lausson
date: 2023-01-12
href: https://www.numerama.com/tech/1236034-pourquoi-aaron-swartz-a-tellement-marque-internet.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2023/01/aaron-swartz-une-1024x576.jpg?webp=1&key=4bd3feb5
tags:
- Internet
series:
- 202302
---

> 10 ans après sa disparition, Aaron Swartz est devenu une figure du militantisme sur Internet. Son engagement en faveur de la liberté marque encore le web aujourd’hui.
