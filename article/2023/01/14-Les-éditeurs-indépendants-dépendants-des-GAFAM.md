---
site: ActuaLitté.com
title: "Les éditeurs indépendants... dépendants des GAFAM?"
date: 2023-01-14
href: https://actualitte.com/article/109541/salons-festivals/les-editeurs-independants-dependants-des-gafam
featured_image: https://actualitte.com/media/cache/width_944_webp/uploads/images/les-editeurs-independants-dependants-des-gafam-63c2c31d3b9a8165637593.jpg
tags:
- Entreprise
series:
- 202302
series_weight: 0
---

> «Tout petit déjà nous rêvons d'indépendance, mais rapidement nous comprenons que tout système est constitué de liens et d'interdépendances. Que l'indépendance totale n'est qu'une illusion.» Par Albert de Pétigny - Éditions Pourpenser.
