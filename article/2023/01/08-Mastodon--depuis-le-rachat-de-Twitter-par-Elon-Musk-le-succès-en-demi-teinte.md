---
site: Capital.fr
title: "Mastodon: depuis le rachat de Twitter par Elon Musk, le succès en demi-teinte"
author: Stéphanie Bascou
date: 2023-01-08
href: https://www.capital.fr/entreprises-marches/mastodon-depuis-le-rachat-de-twitter-par-elon-musk-le-succes-en-demi-teinte-1456738
featured_image: https://www.capital.fr/imgre/fit/https.3A.2F.2Fi.2Epmdstatic.2Enet.2Fcap.2F2023.2F01.2F08.2F887265e0-17fd-4e89-85a0-3e04875de130.2Epng/790x395/background-color/ffffff/quality/70/mastodon-depuis-le-rachat-de-twitter-par-elon-musk-le-succes-en-demi-teinte.jpg
tags:
- Internet
series:
- 202301
series_weight: 0
---

> À chaque frasque d'Elon Musk, Mastodon a connu un afflux de nouveaux utilisateurs. Mais certains en sont partis: voici pourquoi.
