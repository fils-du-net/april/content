---
site: Libération
title: "L'héritage d'Aaron Swartz, l'activiste qui a renversé le code"
description: Internet libre
author: Amaelle Guiton
date: 2023-01-10
href: https://www.liberation.fr/idees-et-debats/lheritage-daaron-swartz-lactiviste-qui-a-renverse-le-code-20230110_MZWK7ITVOFAP3HIPTN5QIYQVWM/
featured_image: https://cloudfront-eu-central-1.images.arcpublishing.com/liberation/LBIBQMANSZDLRLYNTVLXXEZVHM.jpg
tags:
- Internet
series:
- 202302
series_weight: 0
---

> A rebours de la mainmise d’Elon Musk sur Twitter ou de l’essor des technologies sécuritaires, le jeune militant, mort il y a tout juste dix ans, voulait construire un Internet libre. Des Creative Commons au logiciel de transmission de documents confidentiels SecureDrop, il continue d’inspirer les combats pour un numérique éthique.
