---
site: ZDNet France
title: "Communs numériques et logiciels libres dans la stratégie de l'Education nationale"
author: Thierry Noisette
date: 2023-01-30
href: https://www.zdnet.fr/blogs/l-esprit-libre/communs-numeriques-et-logiciels-libres-dans-la-strategie-de-l-education-nationale-39953390.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/Salle_multimedia_CDI_WMC.JPG
tags:
- Éducation
series:
- 202305
series_weight: 0
---

> Dans la «Stratégie numérique pour l'éducation 2023-2027» que publie le ministère de l'Education nationale, «soutenir le développement des communs numériques» est mis en avant, avec mention de logiciels libres.
