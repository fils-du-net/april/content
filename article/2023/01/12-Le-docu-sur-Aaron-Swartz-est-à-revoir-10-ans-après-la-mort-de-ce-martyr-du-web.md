---
site: Numerama
title: "Le docu sur Aaron Swartz est à (re)voir, 10 ans après la mort de ce martyr du web"
description: Un activiste du libre accès
author: Julien Lausson
date: 2023-01-12
href: https://www.numerama.com/tech/1235758-le-docu-sur-aaron-swartz-est-a-revoir-10-ans-apres-la-mort-de-ce-martyr-du-web.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2023/01/aaron-swartz-1024x576.jpg?webp=1&key=2889cff5
tags:
- Internet
series:
- 202302
---

> Figure de la liberté d’accès sur Internet et du partage du savoir sans contrainte, Aaron Swartz s’est suicidé il y a dix ans en amont d’un procès aux États-Unis. Perçu comme le rejeton et martyr d’Internet, il a été l’objet d’un documentaire revenant sur son rôle décisif en faveur des internautes.
