---
site: usine-digitale.fr
title: "Quel bilan pour le Fonds \"Transformation numérique des collectivités territoriales\"?"
author: Mélicia Poitiers
date: 2023-01-18
href: https://www.usine-digitale.fr/article/fonds-transformation-numerique-des-collectivites-territoriales-l-heure-du-bilan.N2087506
featured_image: https://www.usine-digitale.fr/mediatheque/1/3/9/001416931_896x598_c.jpeg
tags:
- Administration
series:
- 202304
series_weight: 0
---

> Deux ans après le lancement du fonds "Transformation numérique des collectivités territoriales", l'enveloppe de 88 millions d’euros a été entièrement dépensée. L'objectif est désormais qu'un maximum de collectivités se saisissent des projets à disposition en open source pour les déployer dans leur territoire.
