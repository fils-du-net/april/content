---
site: Silicon
title: "Open source d'entreprise: quelles bases pour 2023 en Europe"
author: Clément Bohic
date: 2023-01-02
href: https://www.silicon.fr/open-source-europe-bases-2023-455424.html
featured_image: https://www.silicon.fr/wp-content/uploads/2023/01/open-source-entreprise-Europe-2023.png
tags:
- Entreprise
series:
- 202301
series_weight: 0
---

> Où en sont les entreprises européennes avec l’open source? L’OSPO Survey fournit quelques indicateurs.
