---
site: Le Telegramme
title: "Adopter Linux, c'est simple avec l'association Libre à Quimperlé"
date: 2023-01-10
href: https://www.letelegramme.fr/finistere/quimperle/adopter-linux-c-est-simple-avec-l-association-libre-a-quimperle-10-01-2023-13255786.php
featured_image: https://www.letelegramme.fr/images/2023/01/10/lors-de-la-session-de-decembre-les-specialistes-du_7181652_676x418p.jpg
tags:
- Associations
- Promotion
series:
- 202302
series_weight: 0
---

> L’association Libre, à Quimperlé, organise des ateliers pour prendre en main le système d’exploitation informatique Linux.
