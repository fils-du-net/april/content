---
site: Next INpact
title: "Boum publie la 6e édition de son guide d'autodéfense numérique (€)"
description: OS, sex, drug & rock'n roll
author: Jean-Marc Manach
date: 2023-01-18
href: https://www.nextinpact.com/article/70831/boum-publie-6e-edition-son-guide-dautodefense-numerique
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/12504.jpg
tags:
- Innovation
- Vie privée
series:
- 202304
series_weight: 0
---

> Il existe des dizaines de guides et modes d'emploi en matière de sécurité informatique (cf cette blogroll). Si celui de l'Electronic Frontier Foundation, Surveillance Self Defense (SSD), met en avant les modèles de menace, celui du collectif apparenté à l'OS sécurisé Tails opte pour une approche pédagogique de la réduction des risques.
