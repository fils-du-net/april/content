---
site: Usbek & Rica
title: "Le design peut-il rendre la sobriété (vraiment) désirable?"
author: EDF
date: 2023-01-04
href: https://usbeketrica.com/fr/article/le-design-peut-il-rendre-la-sobriete-vraiment-desirable
featured_image: https://usbeketrica.com/media/103192/download/apple.jpeg?v=1&inline=1
tags:
- Économie
series:
- 202301
---

> La sobriété est une vertu dotée d'un défaut de taille: elle est porteuse d'une forme de résignation. Difficile par conséquent d'en faire un élément de transformation désirable de nos modes de vie et de consommation. A moins, peut-être, d'en changer la perception? Fruit d'une époque industrielle triomphante et instigateur de cette désirabilité, le design pourrait précisément apporter des réponses concrètes pour changer l'imaginaire autour de la sobriété.
