---
site: Le Pays
title: "Quand le logiciel se libère"
date: 2023-01-26
href: https://www.le-pays.fr/tarare-69170/actualites/quand-le-logiciel-se-libere_14251899
featured_image: https://img.lamontagne.fr/4W2xw7RVuxDo9gRV3kxKXJPfymB8Osm_JaMxHi0gqv4/fit/657/438/sm/0/bG9jYWw6Ly8vMDAvMDAvMDYvMzkvMDYvMjAwMDAwNjM5MDY1Nw.jpg
tags:
- Associations
series:
- 202304
---

> Le club informatique Tarare Micro a ouvert ses portes au centre municipal de loisirs pour communiquer sur les logiciels libres, samedi 21 janvier.
