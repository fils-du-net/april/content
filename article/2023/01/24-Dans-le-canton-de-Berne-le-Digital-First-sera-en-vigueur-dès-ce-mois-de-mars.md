---
site: ICTjournal
title: "Dans le canton de Berne, le «Digital First» sera en vigueur dès ce mois de mars"
author: Joël Orizet
date: 2023-01-24
href: https://www.ictjournal.ch/news/2023-01-24/dans-le-canton-de-berne-le-digital-first-sera-en-vigueur-des-ce-mois-de-mars
featured_image: https://www.ictjournal.ch/sites/default/files/styles/np8_full/public/media/2023/01/24/berne-flag.jpg
tags:
- Administration
series:
- 202304
series_weight: 0
---

> A partir du 1er mars 2023, le principe du «Digital First» s'appliquera aux administrations du canton de Berne.
