---
site: RTBF
title: "Les données personnelles de nos écoliers accessibles aux États-Unis?"
author: Bénédicte Beauloye
date: 2023-01-11
href: https://www.rtbf.be/article/les-donnees-personnelles-de-nos-ecoliers-accessibles-aux-etats-unis-11133432
featured_image: https://ds.static.rtbf.be/article/image/1248x702/7/f/c/972ededf6c4d7c1405ef53f27d961eda-1673376154.jpg
tags:
- Éducation
series:
- 202302
series_weight: 0
---

> Depuis l’introduction du RGPD en 2018, l’enseignement en Europe se tourne de plus en plus vers l’emploi de logiciels libres à l’école. L’Allemagne, la France et le Danemark ont passé le cap de la souveraineté numérique. Ce n’est pas encore le cas en Belgique. Or, l’enjeu est majeur: il s’agit de garantir la protection des données privées de nos enfants.
