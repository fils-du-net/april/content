---
site: ZDNet France
title: "Libre et open source express: Framasoft, cosmolocalisme, Open Metaverse"
author: Thierry Noisette
date: 2023-01-24
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-framasoft-cosmolocalisme-open-metaverse-39953130.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/05/open-source-KeepCalmAndUse.jpg
tags:
- Associations
series:
- 202304
series_weight: 0
---

> En bref. Collecte réussie pour Framasoft, qui participe au réseau européen ECHO Network. Satellite, prothèse... des technologies alternatives et libres. Alliance pour un métavers ouvert à la fondation Linux.
