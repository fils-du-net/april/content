---
site: Techniques de l'Ingénieur
title: "Les enjeux géopolitiques et économiques de l'Open source"
author: Philippe Richard
date: 2023-01-11
href: https://www.techniques-ingenieur.fr/actualite/articles/les-enjeux-geopolitiques-et-economiques-de-lopen-source-119211
featured_image: https://www.techniques-ingenieur.fr/actualite/wp-content/uploads/2023/01/securite-open-source.jpg
tags:
- Économie
series:
- 202302
series_weight: 0
---

> L'open source est devenu un élément déterminant dans les processus d'innovation des entreprises du numérique. Bien que parfois victime de son succès, il souffre d'un manque de moyens dédiés à sa maintenance. La principale menace vient toutefois de l'implication de certains États et des Big Tech dans l'open source pour renforcer leur influence.
