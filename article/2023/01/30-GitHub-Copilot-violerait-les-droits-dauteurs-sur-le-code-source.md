---
site: Siècle Digital
title: "GitHub Copilot violerait les droits d'auteurs sur le code source"
author: Zacharie Tazrout
date: 2023-01-30
href: https://siecledigital.fr/2023/01/30/github-copilot-violerait-les-droits-dauteurs-sur-le-code-source
featured_image: https://siecledigital.fr/wp-content/uploads/2023/01/GitHub-940x550.jpg
tags:
- Droit d'auteur
series:
- 202305
series_weight: 0
---

> GitHub, OpenAI et Microsoft s'associent pour essayer de rejeter un recours les accusant de ne pas respecter les droits d'auteur sur l'IA.
