---
site: Presse Citron
title: "100 millions de développeurs utilisent GitHub, voici pourquoi"
author: Setra
date: 2023-01-27
href: https://www.presse-citron.net/100-millions-de-developpeurs-utilisent-github-voici-pourquoi
featured_image: https://www.presse-citron.net/app/uploads/2023/01/GITHUB-min.jpg
tags:
- Innovation
series:
- 202304
---

> GitHub vient de franchir les 100 millions de développeurs.
