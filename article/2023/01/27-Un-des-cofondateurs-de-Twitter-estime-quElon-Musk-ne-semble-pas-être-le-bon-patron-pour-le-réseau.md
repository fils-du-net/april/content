---
site: Vanity Fair
title: "Un des cofondateurs de Twitter estime qu'Elon Musk «ne semble pas être» le bon patron pour le réseau"
author: Valentine Servant-Ulgu
date: 2023-01-27
href: https://www.vanityfair.fr/business/article/un-des-cofondateurs-de-twitter-estime-elon-musk-ne-semble-pas-etre-le-bon-patron
featured_image: https://media.vanityfair.fr/photos/63d3e17d33163ec1d7525064/16:9/w_2560%2Cc_limit/GettyImages-930534188.jpg
tags:
- Internet
series:
- 202304
series_weight: 0
---

> Interviewé par le Guardian, Biz Stone, un des quatre créateurs de Twitter, fustige les premières mesures du nouveau patron, Elon Musk.
