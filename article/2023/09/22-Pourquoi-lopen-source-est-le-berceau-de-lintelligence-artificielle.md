---
site: ZDNet France
title: "Pourquoi l'open source est le berceau de l'intelligence artificielle"
author: Steven Vaughan-Nichols
date: 2023-09-22
href: https://www.zdnet.fr/actualites/pourquoi-l-open-source-est-le-berceau-de-l-intelligence-artificielle-39961438.htm
featured_image: https://www.zdnet.com/a/img/2023/05/25/1ff6a155-2b9e-49c7-9dfb-8c4ea6c8ba04/keep-ai-open-source-smaller-may-2023.jpg
tags:
- Sciences
- Licenses
series:
- 202338
series_weight: 0
---

> Dans le secteur extrêmement concurrentiel de l'IA, l'open source est-il condamné à être la bonne fille, utilisée à souhait mais jamais récompensée? Détrompez-vous. 
