---
site: ZDNet France
title: "Libre express: responsabilité sociale, Richard Stallman, témoignages libristes"
author: Thierry Noisette
date: 2023-09-30
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-express-responsabilite-sociale-richard-stallman-temoignages-libristes-39961578.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/05/open-source-KeepCalmAndUse.jpg
tags:
- Sensibilisation
- april
series:
- 202339
series_weight: 0
---

> En bref: des exemples de bonnes pratiques RSE. Richard Stallman a un cancer. Des militants, à Framasoft et LFI, racontent leur engagement.
