---
site: Basta!
title: "Un projet de loi pour renforcer la censure du web: «Une perspective inquiétante pour la liberté d'expression»"
author: Emma Bougerol
date: 2023-09-07
href: https://basta.media/Un-projet-de-loi-pour-renforcer-la-censure-du-web-Une-perspective-inquietante-pour-la-liberte-d-expression
featured_image: https://basta.media/local/adapt-img/960/10x/IMG/logo/navigateur-firefox-loi-espace-nume_rique-internet-censure-quadrature-du-net-mozilla.jpg@.webp?1693491995
tags:
- Institutions
- Internet
series:
- 202336
series_weight: 0
---

> Dans son projet de loi pour «sécuriser et réguler l'espace numérique», le gouvernement veut imposer aux navigateurs web (Chrome, Safari, Mozilla...) de censurer des sites internet. Mozilla et la Quadrature du Net craignent que cela fixe un précédent dangereux.
