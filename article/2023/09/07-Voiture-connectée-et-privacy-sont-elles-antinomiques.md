---
site: Silicon
title: "Voiture connectée et privacy sont-elles antinomiques?"
author: Clément Bohic
date: 2023-09-07
href: https://www.silicon.fr/voiture-connectee-privacy-constructeurs-471105.html
featured_image: https://www.silicon.fr/wp-content/uploads/2023/09/AdobeStock_621790420-scaled.jpeg
tags:
- Vie privée
series:
- 202336
---

> En matière de confidentialité et de sécurité des données, le constat de Mozilla au sujet des constructeurs automobiles est particulièrement sevère.
