---
site: ZDNet France
title: "Le support long terme au noyau Linux passe à 2 ans (au lieu de 6)"
author: Steven Vaughan-Nichols
date: 2023-09-20
href: https://www.zdnet.fr/actualites/le-support-long-terme-au-noyau-linux-passe-a-2-ans-au-lieu-de-6-39961400.htm
featured_image: https://www.zdnet.com/a/img/2023/09/19/b954962e-89a8-44bc-9cf5-3ad42f7f14ee/underwater-gettyimages-1131691962.jpg
tags:
- Entreprise
series:
- 202338
series_weight: 0
---

> L'Open Source Summit fait le point sur les nouveautés du noyau Linux et ses perspectives d'évolution. Et ce n'est pas rose.
