---
site: Clubic.com
title: "Crise Unity: le studio Re-Logic (Terraria) s'engage à soutenir financièrement des moteurs concurrents"
author: Antoine Roche
date: 2023-09-20
href: https://www.clubic.com/actualite-485335-crise-unity-le-studio-re-logic-terraria-s-engage-a-soutenir-financierement-des-moteurs-concurrents.html
featured_image: https://pic.clubic.com/v1/images/2143054/raw.webp?fit=max&width=1200&hash=b0ef346529442fa9100f26cbd106ce986fe49c28
tags:
- Économie
- Entreprise
series:
- 202338
series_weight: 0
---

> Depuis la sortie en 2011 de son carton Terraria, le studio indépendant Re-Logic s'est montré assez discret. Mais pas inactif. En témoigne sa dernière décision, prise durant la crise créée par Unity: il va activement soutenir les moteurs concurrents Godot et FNA.
