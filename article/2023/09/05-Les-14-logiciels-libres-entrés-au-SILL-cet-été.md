---
site: Silicon
title: "Les 14 logiciels libres entrés au SILL cet été"
author: Clément Bohic
date: 2023-09-05
href: https://www.silicon.fr/logiciels-libres-sill-ete-2023-470983.html
featured_image: https://www.silicon.fr/wp-content/uploads/2022/10/SILL-septembre-2022.jpg
tags:
- Référentiel
series:
- 202336
series_weight: 0
---

> Une quinzaine d'entrées ont été ajoutées cet été au SILL (Socle interministériel de logiciels libres). Les voici.
