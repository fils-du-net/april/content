---
site: Next INpact
title: "DMA: la Commission européenne nomme 6 contrôleurs d'accès et 22 «services de plateforme essentiels» (€)"
author: Mathilde Saliou
date: 2023-09-06
href: https://www.nextinpact.com/article/72365/dma-commission-europeenne-nomme-six-controleurs-dacces
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/774.jpg
tags:
- Europe
- Internet
series:
- 202336
---

> La Commission européenne publie aujourd’hui la liste des contrôleurs d’accès concernés par le Digital Markets Act. Ils sont six et la liste ne réserve pas de surprise particulière.
