---
site: Républik IT Le Média
title: "Audran Le Baron (Education Nationale): «notre cap pour les quatre prochaines années est fixé»"
author: Bertrand Lemaire
date: 2023-09-01
href: https://www.republik-it.fr/decideurs-it/gouvernance/audran-le-baron-education-nationale-notre-cap-pour-les-quatre-prochaines-annees-est-fixe.html
featured_image: https://img.republiknews.fr/crop/none/05310bf6b6bd91ced9fb5acde8de6cdb/0/0/1500/844/465/262/audran-baron-directeur-numerique-education-ministere-education-nationale.jpg
tags:
- Éducation
series:
- 202335
series_weight: 0
---

> Directeur du Numérique pour l’Education, Audran Le Baron n’est pas seulement DSI du Ministère de l’Education Nationale. Il détaille ici ses missions et sa stratégie ainsi que l’organisation informatique des administrations de l’éducation nationale, des établissements scolaires et universitaires ainsi que des organisations connexes.
