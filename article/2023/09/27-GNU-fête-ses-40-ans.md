---
site: Next INpact
title: "GNU fête ses 40 ans"
description: "Mais alors... GNU/Linux ou Linux?"
author: Vincent Hermann
date: 2023-09-27
href: https://www.nextinpact.com/article/72538/gnu-fete-ses-40-ans
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/471.jpg
tags:
- Sensibilisation
series:
- 202339
---

> Le système d’exploitation GNU fête en ce moment ses 40 ans. Le projet, initié par Richard Stallman, a donné naissance à un courant de pensée, qui s’est plus tard matérialisé dans la Free Software Foundation. On lui doit également la notion de copyleft.
