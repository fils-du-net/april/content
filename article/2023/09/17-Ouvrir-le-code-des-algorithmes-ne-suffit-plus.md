---
site: AOC media
title: "Ouvrir le code des algorithmes ne suffit plus"
author: Olivier Ertzscheid
date: 2023-09-17
href: https://aoc.media/opinion/2023/09/17/ouvrir-le-code-des-algorithmes-ne-suffit-plus
tags:
- Internet
series:
- 202337
series_weight: 0
---

> Depuis son arrivée à la tête de Twitter rebaptisé X, Elon Musk a bien tenu sa promesse d'ouvrir certaines parties du code source de la plateforme. Cette réponse à de vieilles revendications militantes pour plus de transparence des plateformes ne manque, en vérité, pas de cynisme. Il est devenu transparent que le code de Twitter favorisait certains contenus et en défavorisait d'autres, au bon vouloir de Musk. L'ouverture du code ne suffit plus, aujourd'hui, pour donner des outils de contrôle à ce que les grandes plateformes font de nos espaces d'expression et de nos démocraties.
