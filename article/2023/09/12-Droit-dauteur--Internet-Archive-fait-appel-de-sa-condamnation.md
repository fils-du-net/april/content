---
site: ActuaLitté.com
title: "Droit d'auteur: Internet Archive fait appel de sa condamnation"
author: Antoine Oury
date: 2023-09-12
href: https://actualitte.com/article/113369/droit-justice/droit-d-auteur-internet-archive-fait-appel-de-sa-condamnation
featured_image: https://actualitte.com/media/cache/width_944_webp/uploads/images/droit-d-auteur-internet-archive-fait-appel-de-sa-condamnation-650066c7f22e9557363175.jpg
tags:
- Droit d'auteur
- Partage du savoir
series:
- 202337
series_weight: 0
---

> Condamnée en mars dernier pour violation du copyright après avoir diffusé sans autorisation les œuvres publiées par quatre groupes d'édition américains, l'ONG Internet Archive avait anticipé son recours en appel. Celui-ci a été déposé auprès d'une cour new-yorkaise, ce 11 septembre 2023.
