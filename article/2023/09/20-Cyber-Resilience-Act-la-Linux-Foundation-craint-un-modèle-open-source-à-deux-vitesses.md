---
site: LeMagIT
title: "Cyber Resilience Act: la Linux Foundation craint un modèle open source à deux vitesses (€)"
author: Gaétan Raoul
date: 2023-09-20
href: https://www.lemagit.fr/actualites/366552712/Cyber-Resilience-Act-la-Linux-Foundation-craint-un-open-source-a-deux-vitesses
featured_image: https://www.lemagit.fr/visuals/LeMagIT/hero_article/cra%20lf%20europe.jpg
tags:
- Europe
- Institutions
series:
- 202338
series_weight: 0
---

> L’open source gagne en popularité… Et pourtant le principe même de technologies sous licence libre semble menacé. C’est en tout cas ce que laisse entendre la Linux Foundation Europe au regard du futur Cyber Resilience Act européen.
