---
site: ZDNet France
title: "Nous sommes en 2030 et les portefeuilles numériques ont remplacé... vos portefeuilles"
author: Steven Vaughan-Nichols
date: 2023-09-22
href: https://www.zdnet.fr/actualites/nous-sommes-en-2030-et-les-portefeuilles-numeriques-ont-remplace-vos-portefeuilles-39961440.htm
featured_image: https://www.zdnet.com/a/img/2023/09/20/d4b20589-06d1-4e08-b6db-321f8c6a816b/walletygettyimages-1461360968.jpg
tags:
- Standards
- Innovation
series:
- 202338
series_weight: 0
---

> OpenWallet, désormais rejoint par Microsoft, envisage un avenir proche où les portefeuilles numériques remplaceront les portefeuilles traditionnels, de la même manière que les cartes de débit ont remplacé les chéquiers.
