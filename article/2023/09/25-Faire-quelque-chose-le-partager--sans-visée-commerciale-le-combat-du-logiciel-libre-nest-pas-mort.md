---
site: Basta!
title: "«Faire quelque chose, le partager», sans visée commerciale: le combat du logiciel libre n'est pas mort"
author: Nils Hollenstein
date: 2023-09-25
href: https://basta.media/faire-quelque-chose-le-partager-sans-visee-commerciale-le-combat-du-logiciel-libre
featured_image: https://basta.media/local/adapt-img/960/10x/IMG/logo/2022-05-23_lets-leave-planet-gafam-natu-batx_by-david-revoy.jpg@.webp?1692782836
tags:
- Associations
- Sensibilisation
series:
- 202339
series_weight: 0
---

> Souvent bénévoles, les développeuses et développeurs de logiciels libres contribuent largement au monde numérique actuel. Deux libristes trentenaires témoignent d'un secteur en recomposition face au poids écrasants des géants du numérique, de Google et Microsoft à Twitter et Facebook.
