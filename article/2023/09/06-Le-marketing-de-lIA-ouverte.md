---
site: Next INpact
title: "Le marketing de l'IA «ouverte»"
description: "La porte ouverte à toutes les fenêtres de l'intelligence (€)"
author: Martin Clavey
date: 2023-09-06
href: https://www.nextinpact.com/article/72321/le-marketing-ia-ouverte
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/1902.jpg
tags:
- Sciences
series:
- 202336
series_weight: 0
---

> L'utilisation du mot «open» concernant les intelligences artificielles génératives est-elle pertinente? Constatant que les termes «open» et «open source» sont souvent utilisés de façons diverses et confuses pour parler des nouvelles solutions d'IA, le sociologue David Gray Widder et les deux chercheuses Sarah Myers West et Meredith Whittaker ont analysé son emploi par les différents acteurs de ces nouvelles technologies.
