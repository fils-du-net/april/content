---
site: ouest-france.fr
title: "Les ateliers informatiques démarrent en octobre avec L’@ssourie"
date: 2023-09-20
href: https://www.ouest-france.fr/bretagne/bannalec-29380/les-ateliers-informatiques-demarrent-en-octobre-avec-l-ssourie-487a88aa-5781-11ee-a390-be4049371535
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMzA5NmY2MDNlNTRkMzdkM2UyNGEwNDRjOTJiNGNiOWZhMTY?width=940&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=6f1d0d473f705c6cc0104969b99c0785c6a3d8ce7b920ea7591ca94eb54da6a0
tags:
- Associations
series:
- 202338
series_weight: 0
---

> Samedi 16 septembre 2023, à Bannalec (Finistère), les responsables et bénévoles de L’@ssourie accueillaient le public pour une opération portes ouvertes. Ce qui leur a permis de rencontrer les Bannalécois et présenter l’association et les formations proposées tout au long de l’année à partir du mois d’octobre.
