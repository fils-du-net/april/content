---
site: ZDNet France
title: "Risques du Cyber Resilience Act: 'Le logiciel libre est une source de souveraineté' (Philippe Latombe)"
author: Thierry Noisette
date: 2023-09-10
href: https://www.zdnet.fr/blogs/l-esprit-libre/risques-du-cyber-resilience-act-le-logiciel-libre-est-une-source-de-souverainete-philippe-latombe-39961212.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/Europe%20ordi%20portable.jpeg
tags:
- april
- Europe
series:
- 202336
series_weight: 0
---

> Le député, le CNLL et les libristes de France et d'Europe mettent à nouveau en garde contre les dangers d'un CRA mal ficelé par la Commission européenne.
