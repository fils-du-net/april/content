---
site: Silicon
title: "Le support LTS des noyaux Linux réduit de 6 à 2 ans"
date: 2023-09-21
href: https://www.silicon.fr/lts-noyaux-linux-2-ans-471566.html
featured_image: https://www.silicon.fr/wp-content/uploads/2023/09/AdobeStock_140914398-scaled.jpeg
tags:
- Entreprise
series:
- 202338
---

> Retour à l'ancienne politique de support pour les noyaux Linux LTS. Google, qui avait impulsé une extension, a depuis lors adapté Android pour moins en dépendre.
