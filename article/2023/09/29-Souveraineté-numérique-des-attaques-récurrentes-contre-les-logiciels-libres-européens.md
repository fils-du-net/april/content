---
site: ZDNet France
title: "Souveraineté numérique: des attaques récurrentes contre les logiciels libres européens"
author: Thierry Noisette
date: 2023-09-29
href: https://www.zdnet.fr/blogs/l-esprit-libre/souverainete-numerique-des-attaques-recurrentes-contre-les-logiciels-libres-europeens-39961570.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/europe-cube_Pixabay.jpg
tags:
- Informatique en nuage
- Entreprise
- International
series:
- 202339
series_weight: 0
---

> Dans la revue des «Annales des Mines», qui consacre un numéro à la souveraineté numérique, Jean-Paul Smets, entrepreneur et libriste, dresse un réquisitoire contre le système français du «cloud de confiance» et pointe d'autres discriminations contre les solutions libres.
