---
site: ZDNet France
title: "Qu'est-ce que HuggingChat? Tout ce qu'il faut savoir sur ce chatbot IA open-source"
author: Maria Diaz
date: 2023-09-01
href: https://www.zdnet.fr/pratique/qu-est-ce-que-huggingchat-tout-ce-qu-il-faut-savoir-sur-ce-chatbot-ia-open-source-39961090.htm
featured_image: https://www.zdnet.com/a/img/2023/04/26/0f31419d-6125-466e-b00c-c8237d2886ef/huggingchat1.jpg
tags:
- Sciences
series:
- 202335
series_weight: 0
---

> La réponse de Hugging Face à ChatGPT doit encore être améliorée. Mais elle est encore en cours d'élaboration. Vous pouvez vous inscrire pour participer à sa formation. 
