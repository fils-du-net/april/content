---
site: 01net.
title: "Ce modèle d'IA open source vient défier ChatGPT, Google Bard et Llama 2 de Meta"
author: Florian Bayard
date: 2023-09-08
href: https://www.01net.com/actualites/modele-dia-open-source-vient-defier-chatgpt-google-bard-llama-2-meta.html
featured_image: https://www.01net.com/app/uploads/2023/07/chatgpt-bard-1360x905.jpg
tags:
- Sciences
series:
- 202336
---

> Un modèle d'IA surpuissant a été mis en ligne par la communauté open source. Ce modèle linguistique s'avère être aussi performant que PaLM 2, qui anime Google Bard, et plus puissant que la version standard de ChatGPT…
