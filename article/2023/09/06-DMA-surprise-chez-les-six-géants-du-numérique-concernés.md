---
site: ZDNet France
title: "DMA: surprise chez les six géants du numérique concernés"
author: Xavier Biseul
date: 2023-09-06
href: https://www.zdnet.fr/actualites/dma-surprise-chez-les-six-geants-du-numerique-concernes-39961154.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2019/02/Commission%20europeenne%20620__w1200.jpg
tags:
- Europe
- Internet
series:
- 202336
series_weight: 0
---

> La Commission européenne a publié la liste des plateformes qui devront se conformer à la nouvelle réglementation favorisant l'ouverture des marchés numériques. Si les GAFAM sont, sans surprise, en première ligne, certains de leurs services sont étonnement exclus.
