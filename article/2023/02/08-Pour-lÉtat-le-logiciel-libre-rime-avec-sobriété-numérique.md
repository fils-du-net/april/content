---
site: Acteurs Publics 
title: "Pour l'État, le logiciel libre rime avec sobriété numérique (€)"
author:  Emile Marzolf
date: 2023-02-08
href: https://acteurspublics.fr/articles/pour-letat-le-logiciel-libre-rime-avec-sobriete-numerique
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/41/474962aef470a33ead52d82c23bf1cac75aa3270.jpeg
tags:
- Administration
series:
- 202306
---

> Le conseil “des logiciels libres” institué par la direction interministérielle du numérique a tranché: il ne fait aucun doute que l’utilisation des logiciels libres augmente la durabilité des ordinateurs, smartphones et autres objets connectés.
