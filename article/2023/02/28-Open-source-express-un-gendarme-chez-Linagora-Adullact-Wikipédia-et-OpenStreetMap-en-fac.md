---
site: ZDNet France
title: "Open source express: un gendarme chez Linagora, Adullact, Wikipédia et OpenStreetMap en fac"
author: Thierry Noisette
date: 2023-02-28
href: https://www.zdnet.fr/blogs/l-esprit-libre/open-source-express-un-gendarme-chez-linagora-adullact-wikipedia-et-openstreetmap-en-fac-39954812.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/05/open-source-KeepCalmAndUse.jpg
tags:
- Sensibilisation
series:
- 202309
series_weight: 0
---

> Xavier Guimard, artisan du passage de la gendarmerie au Libre, chez Linagora. Une interview de François Elie (Adullact). Un wikimédien en résidence à l'Urfist de Bordeaux.
