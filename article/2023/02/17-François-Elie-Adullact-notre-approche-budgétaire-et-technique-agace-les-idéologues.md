---
site: Républik IT Le Média
title: "François Elie (Adullact): «notre approche budgétaire et technique agace les idéologues» "
author: Bertrand Lemaire
date: 2023-02-17
href: https://www.republik-it.fr/decideurs-it/achat-it/francois-elie-adullact-notre-approche-budgetaire-et-technique-agace-les-ideologues.html
featured_image: https://img.republiknews.fr/crop/none/0a31dfee1ff66898ee1162aba8c8d30a/0/0/1499,5556872038/843,75/465/262/francois-elie-president-adullact-elu-local-angouleme-ici-20-ans-association.jpg
tags:
- Associations
- Administration
series:
- 202307
series_weight: 0
---

> Organisant l’emploi du logiciel libre dans les collectivités locales, l’Adullact (Association des Développeurs, Utilisateurs de Logiciels Libres pour les Administrations et les Collectivités Territoriales) vise surtout au bon usage des deniers publics comme l’explique François Elie, son président et élu local d’Angoulême. Avec 300 adhérents directs, l’Adullact accompagne en fait environ 15 000 collectivités et établissements.
