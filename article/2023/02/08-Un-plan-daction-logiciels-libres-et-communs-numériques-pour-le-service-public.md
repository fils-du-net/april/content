---
site: ZDNet France
title: "Un plan d'action «logiciels libres et communs numériques» pour le service public"
author: Thierry Noisette
date: 2023-02-08
href: https://www.zdnet.fr/blogs/l-esprit-libre/un-plan-d-action-logiciels-libres-et-communs-numeriques-pour-le-service-public-39953840.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2020/12/Marianne_Debre_Collection_WMC.jpg
tags:
- Administration
- april
series:
- 202306
series_weight: 0
---

> Mieux connaître et utiliser les logiciels libres, développer la libération et l'ouverture des codes sources, sont parmi les objectifs annoncés du plan d'action publié par une branche de la Dinum.
