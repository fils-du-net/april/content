---
site: La gazette.fr
title: "\"Si la ville intelligente devient automatique et privatisée, on aura tout perdu\" (€)"
author: Laura Fernandez Rodriguez, Romain Mazon
date: 2023-02-16
href: https://www.lagazettedescommunes.com/849077/si-la-ville-intelligente-devient-automatique-et-privatisee-on-aura-tout-perdu
featured_image: https://www.lagazettedescommunes.com/wp-content/uploads/2020/05/jacques-priol-google-toronto.jpg
tags:
- Open Data
- Administration
series:
- 202307
series_weight: 0
---

> Que ce soit l'appropriation de la donnée, les modèles de smart city, la maturité des technologies ou leur souveraineté, Jacques Priol, président de l’Observatoire Data Publica, également consultant et président et fondateur du cabinet Civiteo, passe en revue de nombreux chantiers numériques dans un entretien accordé à la Gazette.
