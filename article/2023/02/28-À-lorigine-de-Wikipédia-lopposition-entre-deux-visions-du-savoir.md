---
site: France Culture
title: "À l'origine de Wikipédia, l'opposition entre deux visions du savoir"
author: Anna Mexis, Camille Renard
date: 2023-02-28
href: https://www.radiofrance.fr/franceculture/a-l-origine-de-wikipedia-l-opposition-entre-deux-visions-du-savoir-1154389
featured_image: https://www.radiofrance.fr/s3/cruiser-production/2023/02/832ca691-0d48-4b1e-b3da-18064610626e/870x489_reencodedfatimage_vignette-wiki-16-9-sans-texte.jpg
tags:
- Partage du savoir
series:
- 202309
series_weight: 0
---

> A L'ORIGINE DES GEANTS DU WEB - À l'origine de Wikipédia en 2001, ses deux fondateurs partagent la volonté de rendre disponible tout le savoir du monde. Mais leur opposition va révéler deux utopies divergentes - un conflit idéologique dont est issue la success story de Wikipédia.
