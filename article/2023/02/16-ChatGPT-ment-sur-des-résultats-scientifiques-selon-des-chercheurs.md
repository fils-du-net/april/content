---
site: ZDNet France
title: "ChatGPT ment sur des résultats scientifiques selon des chercheurs"
author: Tiernan Ray
date: 2023-02-16
href: https://www.zdnet.fr/actualites/chatgpt-ment-sur-des-resultats-scientifiques-disent-des-chercheurs-39954258.htm
featured_image: https://www.zdnet.com/a/img/2023/02/15/fc9b055d-9bd0-4616-8fd4-55b39b2179bb/gettyimages-1247068124.jpg
tags:
- Logiciels privateurs
- Innovation
series:
- 202307
series_weight: 0
---

> La transparence du code est essentielle pour lutter contre les nombreux dangers des informations trompeuses. Ces chercheurs plaident pour des alternatives open source.
