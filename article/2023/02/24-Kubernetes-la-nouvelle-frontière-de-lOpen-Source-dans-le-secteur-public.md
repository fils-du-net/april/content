---
site: cio-online.com
title: "Kubernetes, la nouvelle frontière de l'Open Source dans le secteur public"
author: Reynald Fléchaux
date: 2023-02-24
href: https://www.cio-online.com/actualites/lire-kubernetes-la-nouvelle-frontiere-de-l-open-source-dans-le-secteur-public-14768.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000019625.jpg
tags:
- Administration
- Informatique en nuage
series:
- 202308
series_weight: 0
---

> L'Open Source comme garantie d'indépendance de l'Etat vis-à-vis des fournisseurs IT. A l'heure de la généralisation des architectures cloud, Kubernetes perpétue cette tradition. L'Urssaf et l'Intérieur lancent de premiers services reposant sur l'orchestrateur de conteneurs.
