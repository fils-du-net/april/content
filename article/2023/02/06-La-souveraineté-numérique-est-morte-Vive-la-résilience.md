---
site: Le Point
title: "«La souveraineté numérique est morte... Vive la résilience!»"
author: Tariq Krim
date: 2023-02-06
href: https://www.lepoint.fr/debats/la-souverainete-numerique-est-morte-vive-la-resilience-06-02-2023-2507586_2.php
featured_image: https://static.lpnt.fr/images/2023/02/06/24121557lpw-24133204-article-jpg_9319214_660x287.jpg
tags:
- International
series:
- 202306
series_weight: 0
---

> TRIBUNE. Face à la militarisation croissante de l'Internet, il est urgent d'investir dans la résilience de nos infrastructures numériques.
