---
site: Next INpact
title: "Assises de la féminisation du numérique: «l'urgence» de diversifier l'industrie de la tech (€)"
author: Mathilde Saliou
date: 2023-02-20
href: https://www.nextinpact.com/article/71045/assises-feminisation-numerique-urgence-diversifier-industrie-tech
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/12552.jpg
tags:
- Partage du savoir
series:
- 202308
---

> Aux premières Assises nationales de la féminisation des métiers et filières du numérique, l'alarme est sonnée sur le manque de diversité de l'industrie technologique. Si l'on se prive de «50 % des talents», comme le pointe l'association Femmes@Numérique, comment espérer remplir les nombreux postes vacants?
