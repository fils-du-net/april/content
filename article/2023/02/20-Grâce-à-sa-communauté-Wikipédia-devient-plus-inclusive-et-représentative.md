---
site: Slate.fr
title: "Grâce à sa communauté, Wikipédia devient plus inclusive et représentative"
author: Vincent Bresson
date: 2023-02-20
href: https://www.slate.fr/tech-internet/la-libre-encyclopedie/wikipedia-inclusion-representativite-internet-genre-femmes-feminisation-racisation
featured_image: https://www.slate.fr/sites/default/files/styles/1060x523/public/desola-lanre-ologun-igur1ix0mqm-unsplash_0.jpg
tags:
- Partage du savoir
series:
- 202308
series_weight: 0
---

> Miroir d'une société inégalitaire, Wikipédia peine à se féminiser, mettre en avant des personnalités racisées ou même intégrer des personnalités africaines. Un déficit que des wikipédiens tentent de corriger en se regroupant dans des collectifs.
