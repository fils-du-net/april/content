---
site: ouest-france.fr
title: "Bannalec. Informatique: apprendre à utiliser Linux grâce à L’@ssourie, c’est ce vendredi"
date: 2023-02-23
href: https://www.ouest-france.fr/bretagne/bannalec-29380/bannalec-informatique-apprendre-a-utiliser-linux-grace-a-l-atssourie-c-est-ce-vendredi-e9824c5e-b1bb-11ed-840b-f006cb1e69bd
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMzAyMGM2MDZkNDc1NWFhZGJhMzc3NTYzYWJiNWYyODE5Y2I?width=1260&focuspoint=50%2C29&cropresize=1&client_id=bpeditorial&sign=b9bd3af3355271da539931b688a47698f2f6ae7fb05cd46a7d40d7cf9fcecf76
tags:
- Associations
series:
- 202308
series_weight: 0
---

> Grâce à une collaboration entre la médiathèque Le Tangram et l’association L’@ssourie, les Bannalécois (Finistère) peuvent découvrir le système d’exploitation Linux, lors d’un atelier dédié à cet environnement qui fait de plus en plus d’émules.
