---
site: lindependant.fr
title: "Narbonne: quand la communauté des \"libertariens\" change notre logiciel"
author: Samuel Mourier
date: 2023-02-09
href: https://www.lindependant.fr/2023/02/06/narbonne-lupn-a-defini-la-liberte-des-logiciels-libres-a-la-mjc-10953161.php
featured_image: https://images.lindependant.fr/api/v1/images/view/63d4307ed0b0a9116b59693e/large/image.jpg?v=1
tags:
- Sensibilisation
series:
- 202306
series_weight: 0
---

> L'Université Populaire de la Narbonnaise avait invité la Libre Fabrique Electro-Informatique pour traiter des "Logiciels Libres" à l'Annexe de la MJC. Explications sur cette petite révolution informatique... et sociétale.
