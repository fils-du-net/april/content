---
site: 01net.
title: "Mastodon: l'anti-Twitter est massivement déserté par ses utilisateurs, quelle surprise!"
author: Florian Bayard
date: 2023-02-08
href: https://www.01net.com/actualites/mastodon-lanti-twitter-est-massivement-deserte-par-ses-utilisateurs-quelle-surprise.html
featured_image: https://www.01net.com/app/uploads/2022/11/mastodon-copie-1-1024x683.jpg
tags:
- Internet
series:
- 202306
series_weight: 0
---

> Mastodon, l'une des alternatives au Twitter d'Elon Musk, perd des utilisateurs. Après le succès éclair de fin 2022, la plate-forme open source est progressivement désertée par les internautes... et il fallait s'y attendre.
