---
site: Capital.fr
title: "\"Bloom, l’intelligence artificielle made in France n’a rien à envier à ChatGPT\""
author: Stéphane Barge
date: 2023-02-10
href: https://www.capital.fr/entreprises-marches/bloom-lintelligence-artificielle-made-in-france-na-rien-a-envier-a-chatgpt-1459927
featured_image: https://www.capital.fr/imgre/fit/https.3A.2F.2Fi.2Epmdstatic.2Enet.2Fcap.2F2023.2F02.2F10.2F7ba690cd-090a-4938-9bf3-46538ed45e30.2Ejpeg/790x395/background-color/ffffff/quality/70/bloom-l-intelligence-artificielle-made-in-france-n-a-rien-a-envier-a-chatgpt.jpg
tags:
- Sciences
series:
- 202306
series_weight: 0
---

> ENTRETIEN - Dans une interview à Capital, Laurence Devillers, professeur d’intelligence artificielle à la Sorbonne, chercheur au CNRS et auteure de l’essai Les robots émotionnels (Editions de l’Observatoire), livre sa vision de cette nouvelle révolution numérique qui se déroule sous nos yeux.
