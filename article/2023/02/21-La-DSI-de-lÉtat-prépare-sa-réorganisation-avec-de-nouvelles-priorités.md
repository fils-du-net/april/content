---
site: Acteurs Publics
title: "La DSI de l'État prépare sa réorganisation, avec de nouvelles priorités (€)"
author: Emile Marzolf
date: 2023-02-21
href: https://acteurspublics.fr/articles/la-dsi-de-letat-prepare-sa-reorganisation-avec-de-nouvelles-priorites
featured_image: https://acteurspublics.fr/media/cache/default_news_big/upload/media/default/0001/29/746cc2d45cb48af6200c8290fa03c5a8e5b00a2a.jpeg
tags:
- Administration
series:
- 202308
---

> Alors qu’une réorganisation se prépare à la direction interministérielle du numérique, Acteurs publics a pu consulter un projet de décret confiant de nouvelles priorités à cette direction stratégique, notamment en matière de promotion d’un “numérique agile et à impact”, ou encore de contrôle des trajectoires d’emplois des ministères et des règles d’externalisation.
