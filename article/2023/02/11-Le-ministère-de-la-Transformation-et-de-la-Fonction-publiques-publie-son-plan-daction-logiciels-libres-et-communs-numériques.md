---
site: Archimag
title: "Le ministère de la Transformation et de la Fonction publiques publie son plan d'action logiciels libres et communs numériques"
author: Claire Martinez
href: https://www.archimag.com/vie-numerique/2023/02/10/ministere-transformation-fonction-publiques-publie-plan-action
featured_image: https://www.archimag.com/sites/archimag.com/files/styles/article/public/web_articles/image/plan-action-ministere-fonctions-publiques.jpg?itok=hdJcm_Ql
tags:
- Administration
series:
- 202306
---

> Après un an de travail, le ministère de la Transformation et de la Fonction publiques à publié le 8 février 2023 son plan d'action logiciels libres et communs numériques qui vise à soutenir la transformation numérique du service public.
