---
site: ouest-france.fr
title: "Quimper. Les Abeilles numériques bourdonnent à plein régime"
date: 2023-02-14
href: https://www.ouest-france.fr/bretagne/quimper-29000/quimper-les-abeilles-numeriques-bourdonnent-a-plein-regime-945cb2cc-a93d-11ed-9e48-2edafa48dd40
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMzAyODJjZDQ4YzBkODQwOGRhNDg1NDdiNjhlMWRjNzNjOWU?width=940&focuspoint=50%2C56&cropresize=1&client_id=bpeditorial&sign=720cdb6a2756c2029d9e0a449ea2635b111553454ced7ac07e5908b222995f2f
tags:
- Associations
series:
- 202307
series_weight: 0
---

> En 2022, 766 ordinateurs reconditionnés sous Linux ont été distribués gratuitement par le centre des Abeilles et Linux Quimper aux habitants et associations de l’agglomération quimpéroise qui sont écartés du numérique pour des raisons financières.
