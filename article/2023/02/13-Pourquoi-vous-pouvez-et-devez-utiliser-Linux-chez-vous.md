---
site: ZDNet France
title: "Pourquoi vous pouvez et devez utiliser Linux chez vous"
author: Jack Wallen
date: 2023-02-13
href: https://www.zdnet.fr/actualites/pourquoi-vous-pouvez-et-devez-utiliser-linux-chez-vous-39954016.htm
featured_image: https://www.zdnet.com/a/img/2023/02/09/d9624b9e-12c3-472c-82a2-342fadc9f53c/gettyimages-1371828696.jpg
tags:
- Sensibilisation
series:
- 202307
series_weight: 0
---

> Je suis fermement convaincu que Linux est le système d'exploitation idéal pour être productif à domicile. Et je plaide pour que tout le monde puisse envisager cette plate-forme gratuite.
