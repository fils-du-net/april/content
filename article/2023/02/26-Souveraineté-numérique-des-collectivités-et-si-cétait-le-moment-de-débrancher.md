---
site: Le courrier des maires
title: "Souveraineté numérique des collectivités: et si c'était le moment de débrancher? (€)"
author: Emilie Denetre
date: 2023-02-26
href: https://www.courrierdesmaires.fr/article/souverainete-numerique-des-collectivites-et-si-c-etait-le-moment-de-debrancher.53676
featured_image: https://www.courrierdesmaires.fr/mediatheque/6/9/1/000026196_600x400_c.jpeg
tags:
- Administration
- Open Data
series:
- 202308
---

> Face à la hausse (conséquente) de l’abonnement Microsoft 365, certaines collectivités locales s’interrogent sur l’opportunité de quitter le géant américain afin de regagner leur souveraineté numérique. D'autant que l’«enfermement» avec un même éditeur peut s’avérer délétère en cas de cyberattaque. Mais changer de système n’est pas toujours simple, notamment pour les petites communes qui auraient besoin d’un coup de pouce de l’Etat.
