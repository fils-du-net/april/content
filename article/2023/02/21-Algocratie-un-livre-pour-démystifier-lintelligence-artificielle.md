---
site: Le Monde.fr
title: "«Algocratie», un livre pour démystifier l'intelligence artificielle (€)"
author: Antoine Reverchon
date: 2023-02-21
href: https://www.lemonde.fr/idees/article/2023/02/21/algocratie-un-livre-pour-demystifier-l-intelligence-artificielle_6162724_3232.html
tags:
- Sciences
series:
- 202308
series_weight: 0
---

> «Allons-nous donner le pouvoir aux algorithmes?», s'interroge dans son ouvrage Hugues Bersini, professeur d'informatique à l'Université libre de Bruxelles, qui décortique le fonctionnement de ces outils et réclame une nouvelle démocratie numérique.
