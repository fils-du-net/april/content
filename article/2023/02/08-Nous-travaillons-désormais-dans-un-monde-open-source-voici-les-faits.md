---
site: ZDNet France
title: "Nous travaillons désormais dans un monde open source: voici les faits"
author: Joe McKendrick
date: 2023-02-08
href: https://www.zdnet.fr/actualites/nous-travaillons-desormais-dans-un-monde-open-source-voici-les-faits-39953688.htm
featured_image: https://www.zdnet.com/a/img/2023/02/06/d02b2ebc-1073-49b8-8569-0ffbd2ac2c96/gettyimages-1350230158.jpg
tags:
- Entreprise
series:
- 202306
series_weight: 0
---

> Reste que près de 40 % des équipes utilisant des logiciels libres ne disposent pas des compétences internes nécessaires pour tester, utiliser ou intégrer ces logiciels.
