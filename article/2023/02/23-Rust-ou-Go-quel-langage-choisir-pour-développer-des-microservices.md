---
site: LeMagIT
title: "Rust ou Go: quel langage choisir pour développer des microservices?"
author: Kerry Doyle
date: 2023-02-23
href: https://www.lemagit.fr/conseil/Rust-ou-Go-quel-langage-choisir-pour-developper-des-microservices
featured_image: https://www.lemagit.fr/visuals/ComputerWeekly/Hero%20Images/programming-language-code-script-adobe.jpg
tags:
- Sensibilisation
series:
- 202308
series_weight: 0
---

> Les langages de programmation Rust et Go offrent tous deux des fonctionnalités adaptées au développement de microservices, mais leurs capacités respectives les rendent plus adaptés à certains scénarios qu’à d’autres.
