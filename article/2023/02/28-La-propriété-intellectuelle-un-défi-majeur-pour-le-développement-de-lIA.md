---
site: Les Echos
title: "La propriété intellectuelle, un défi majeur pour le développement de l'IA (€)"
author: Hortense Goulard
date: 2023-02-28
href: https://www.lesechos.fr/tech-medias/intelligence-artificielle/la-propriete-intellectuelle-un-defi-majeur-pour-le-developpement-de-lia-1910697
featured_image: https://media.lesechos.com/api/v1/images/view/63fdeab230d8a6646015301d/1280x720/0703459174117-web-tete.jpg
tags:
- Droit d'auteur
- Sciences
series:
- 202309
---

> Les entreprises en pointe en matière d'intelligence artificielle générative - dont OpenAI et Stability AI - ne se sont pas beaucoup souciées, pour l'instant, du droit de la propriété intellectuelle. Plusieurs procédures judiciaires espèrent protéger les droits des artistes et des codeurs face aux machines.
