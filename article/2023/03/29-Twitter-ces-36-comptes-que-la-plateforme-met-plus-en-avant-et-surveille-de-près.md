---
site: Clubic.com
title: "Twitter: ces 36 comptes que la plateforme met plus en avant... et surveille de près"
author: Maxence Glineur
date: 2023-03-29
href: https://www.clubic.com/internet/twitter/actualite-463035-twitter-ces-36-comptes-que-la-plateforme-met-plus-en-avant-et-surveille-de-pres.html
featured_image: https://pic.clubic.com/v1/images/2059164/raw.webp?fit=max&width=1200&hash=0d9573c3c5bca034cb6b37e105753e5318fe543f
tags:
- Internet
- Entreprise
series:
- 202313
series_weight: 0
---

> Il s'agit d'une liste arbitraire à première vue, contenant entre autres le compte de… Elon Musk.
