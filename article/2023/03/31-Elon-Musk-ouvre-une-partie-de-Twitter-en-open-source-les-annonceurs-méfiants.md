---
site: Les Echos
title: "Elon Musk ouvre une partie de Twitter en open source, les annonceurs méfiants (€)"
author: Samir Touzani
date: 2023-03-31
href: https://www.lesechos.fr/tech-medias/medias/elon-musk-ouvre-une-partie-de-twitter-en-open-source-les-annonceurs-mefiants-1921111
featured_image: https://media.lesechos.com/api/v1/images/view/6426d525e334d02c842424bd/1280x720/0703698643766-web-tete.jpg
tags:
- Internet
series:
- 202313
---

> Le patron de Twitter va rendre public le fonctionnement des algorithmes de recommandation des tweets de la plateforme ce vendredi. Des algorithmes qui font polémique et font fuir des annonceurs, avec une baisse de 50 % des revenus depuis la prise de contrôle par Elon Musk.
