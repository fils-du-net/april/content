---
site: ZDNet France
title: "Mozilla crée deux nouvelles compagnies"
author: Thierry Noisette
date: 2023-03-15
href: https://www.zdnet.fr/blogs/l-esprit-libre/mozilla-cree-deux-nouvelles-compagnies-39955546.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2022/03/Mozilla_logo.svg.png
tags:
- Associations
- Sciences
series:
- 202311
series_weight: 0
---

> La fondation éditrice de Firefox, créée en 1998, souligne son but d’une IA «de confiance» et recherche trois nouveaux administrateurs.
