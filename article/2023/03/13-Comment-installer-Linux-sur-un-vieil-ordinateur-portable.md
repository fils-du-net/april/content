---
site: ZDNet France
title: "Comment installer Linux sur un vieil ordinateur portable?"
author: Jack Wallen
date: 2023-03-13
href: https://www.zdnet.fr/pratique/comment-installer-linux-sur-un-vieil-ordinateur-portable-39955326.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2023/Laptops__w1200.jpg
tags:
- Sensibilisation
series:
- 202311
series_weight: 0
---

> Si vous souhaitez redonner vie à un ordinateur portable vieillissant, Linux pourrait être le meilleur choix. Voyez comme c'est facile à faire.
