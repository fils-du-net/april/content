---
site: Next INpact
title: "La DINUM tape du poing et veut restructurer le développement numérique de l'État (€)"
author: Vincent Hermann
date: 2023-03-15
href: https://www.nextinpact.com/article/71226/la-dinum-tape-poing-et-veut-restructurer-developpement-numerique-letat
featured_image: https://cdnx.nextinpact.com/compress/1023-496/data-next/images/bd/wide-linked-media/347.jpg
tags:
- Administration
series:
- 202311
series_weight: 0
---

> En dépit de nombreux projets, la transformation numérique de l’appareil étatique se fait attendre. La nouvelle feuille de route de la DINUM est là pour mettre des points sur les i, d’autant que ses moyens vont être renforcés et son nombre d’agents accru.
