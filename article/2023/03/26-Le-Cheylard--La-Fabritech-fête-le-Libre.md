---
site: hebdo-ardeche.fr
title: "Le Cheylard- La Fabritech fête le 'Libre'"
author: Cyril Lehembre
date: 2023-03-26
href: https://www.hebdo-ardeche.fr/actualite-13507-le-cheylard-la-fabritech-fete-le-libre
featured_image: https://www.hebdo-ardeche.fr/photos/moyen/13507.jpg
tags:
- April
series:
- 202312
---

> En plein essor, le laboratoire de fabrication numérique organise deux journées exceptionnelles autour de la découverte du logiciel libre.
