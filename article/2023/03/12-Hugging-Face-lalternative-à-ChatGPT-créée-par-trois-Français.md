---
site: LEFIGARO
title: "Hugging Face, l’alternative à ChatGPT créée par trois Français (€)"
author: Chloé Woitier
date: 2023-03-12
href: https://www.lefigaro.fr/secteur/high-tech/hugging-face-l-alternative-a-chatgpt-creee-par-trois-francais-20230312
featured_image: https://i.f1g.fr/media/eidos/704x396_cropupscale/2023/03/12/XVMede1b3d2-c0ea-11ed-975e-30998a6b03d4.jpg
tags:
- Sciences
series:
- 202310
---

> DÉCRYPTAGE - Valorisée 2 milliards de dollars, cette plateforme open source prend le contrepied du modèle fermé de ChatGPT.
