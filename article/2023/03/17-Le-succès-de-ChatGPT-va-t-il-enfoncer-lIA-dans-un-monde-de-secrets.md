---
site: ZDNet France
title: "Le succès de ChatGPT va-t-il enfoncer l'IA dans un monde de secrets?"
author: Tiernan Ray
date: 2023-03-17
href: https://www.zdnet.fr/actualites/le-succes-de-chatgpt-va-t-il-enfoncer-l-ia-dans-un-monde-de-secrets-39955578.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2023/OpenAIeye__w1200.jpg
tags:
- Sciences
series:
- 202311
series_weight: 0
---

> Les pressions du marché peuvent pousser l'industrie de l'intelligence artificielle à moins divulguer, ce qui pourrait entraver le progrès scientifique et causer d'autres problèmes.
