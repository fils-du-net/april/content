---
site: Siècle Digital
title: "Vers un État numérique «plus efficace, plus simple et plus souverain»"
author: Antoine Messina
date: 2023-03-10
href: https://siecledigital.fr/2023/03/10/vers-un-etat-numerique-plus-efficace-plus-simple-et-plus-souverain
featured_image: https://siecledigital.fr/wp-content/uploads/2023/03/la-france-4e-au-classement-mondial-du-bien-etre-numerique-une-940x550.jpg.webp
tags:
- Administration
- Open Data
series:
- 202310
series_weight: 0
---

> La direction interministérielle du numérique (DINUM) a partagé sa feuille de route pour la stratégie numérique de l'État.
