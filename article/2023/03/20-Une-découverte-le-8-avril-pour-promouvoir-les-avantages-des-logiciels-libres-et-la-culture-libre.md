---
site: ouest-france.fr
title: "Une découverte le 8 avril pour «promouvoir les avantages des logiciels libres et la culture libre»"
date: 2023-03-20
href: https://www.ouest-france.fr/pays-de-la-loire/montval-sur-loir-72500/une-decouverte-le-8-avril-pour-promouvoir-les-avantages-des-logiciels-libres-et-la-culture-libre-96fd813c-c66b-11ed-8242-6ead8a575c42
featured_image: https://media.ouest-france.fr/v1/pictures/MjAyMzAzOWUyMGQ4NzZlMWEwMmZjOWEwZGFiMmIyZmFkMjk2YTM?width=1260&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=83d4894fcba03452359b27923d39a8888d6f7d1c744afb5b21a16cb8541d2ef5
tags:
- April
- Promotion
series:
- 202312
series_weight: 0
---

> Samedi 8 avril 2023, dans le cadre de l’événement national Libre en fête, la bibliothèque de Montval-sur-Loir proposera au grand public de découvrir des logiciels libres.
