---
site: 20 minutes online
title: "Mozilla investit dans une IA digne de confiance"
date: 2023-03-24
href: https://www.20min.ch/fr/story/lediteur-de-firefox-investit-dans-une-ia-digne-de-confiance-526295879338
featured_image: https://cdn.unitycms.io/images/A4v6jOpvqx69giBbo8QFLE.jpg?op=ocroped&val=2001,2000,1000,1000,0,0&sum=ZBID_nRCJCI
tags:
- Sciences
series:
- 202312
---

> La fondation Mozilla a annoncé un investissement de 30 millions de dollars dans la création de Mozilla.ai, start-up dédiée au développement d’une future intelligence artificielle open source.
