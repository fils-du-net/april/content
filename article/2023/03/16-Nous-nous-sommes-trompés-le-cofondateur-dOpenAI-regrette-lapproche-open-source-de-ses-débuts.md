---
site: BFMtv
title: "'Nous nous sommes trompés': le cofondateur d'OpenAI regrette l'approche open source de ses débuts"
author: Marius Bocquet
date: 2023-03-16
href: https://www.bfmtv.com/tech/nous-nous-sommes-trompes-le-cofondateur-d-open-ai-regrette-l-approche-open-source-de-ses-debuts_AD-202303160353.html
tags:
- Sciences
series:
- 202311
---

> OpenAI, qui à ses débuts disait vouloir 'collaborer librement' avec d'autres, n'a dévoilé quasiment aucune information sur les données utilisées pour former GPT-4, la nouvelle version de ChatGPT.
