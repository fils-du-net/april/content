---
site: La gazette.fr
title: "Label Territoire Numérique Libre: les collectivités territoriales passent aux logiciels libres "
date: 2023-03-06
href: https://www.lagazettedescommunes.com/854195/label-territoire-numerique-libre-les-collectivites-territoriales-passent-aux-logiciels-libres/
featured_image: https://www.lagazettedescommunes.com/wp-content/uploads/2023/02/visuel-article.jpg
tags:
- april
- Administration
- Sensibilisation
series:
- 202310
series_weight: 0
---

> En 2022, trois collectivités se sont démarquées de par leur engagement envers le logiciel libre et ont obtenu le plus au niveau de labellisation du label Territoire Numérique Libre. Qui sont-elles?
