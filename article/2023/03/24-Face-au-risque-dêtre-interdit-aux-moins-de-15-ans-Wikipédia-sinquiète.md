---
site: BFMtv
title: "Face au risque d'être interdit aux moins de 15 ans, Wikipédia s’inquiète"
author: Lucie Lequier
date: 2023-03-24
href: https://www.bfmtv.com/tech/actualites/face-au-risque-d-etre-interdit-aux-moins-de-15-ans-wikipedia-s-inquiete_AV-202303240409.html
tags:
- Internet
- Institutions
- Partage du savoir
series:
- 202312
series_weight: 0
---

> Une proposition de loi restreignant l’utilisation des réseaux sociaux aux plus de 15 ans pourrait toucher TikTok, Instagram et Facebook…mais aussi des sites comme Wikipédia. L’association Wikimédia France a fait part de ses inquiétudes à Tech&Co.
