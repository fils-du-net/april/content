---
site: Le Telegramme
title: "L'@ssourie forme aux logiciels libres à Bannalec"
date: 2023-03-05
href: https://www.letelegramme.fr/finistere/bannalec/l-ssourie-forme-aux-logiciels-libres-a-bannalec-05-03-2023-13290272.php
featured_image: https://www.letelegramme.fr/images/2023/03/05/l-association-l-ssourie-avec-michel-cheron-et-gilles-david_7313824_676x507p.jpg?v=1
tags:
- Associations
- Sensibilisation
series:
- 202309
series_weight: 0
---

> L’association l'@ssourie anime désormais un atelier Linux à la médiathèque Le Tangram, à Bannalec, tous les derniers vendredis du mois. «Linux esr facile d’installation, d’utilisation, la gratuité, la possibilité d’apprentissage personnel et la cohabitation avec Windows car on peut avoir les deux sur son ordinateur. Linux prend très peu de place et est compatible avec les machines très anciennes. Les mises à jour régulières sont sécurisées, il n’y a pas besoin d’antivirus. Il n’y a pas besoin d’être expert pour l’installer tout seul, c’est simple et très guidé, la documentation de l’utilisation est en français», explique Michel Chéron.
