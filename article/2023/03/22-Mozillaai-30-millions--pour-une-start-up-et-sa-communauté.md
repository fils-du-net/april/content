---
site: Silicon
title: "Mozilla.ai: 30 millions $ pour une start-up et sa communauté"
author: Ariane Beky
date: 2023-03-22
href: https://www.silicon.fr/mozilla-ai-30-millions-start-up-communaute-461199.html
featured_image: https://www.silicon.fr/wp-content/uploads/2023/03/Mozilla.ai_.png
tags:
- Sciences
series:
- 202312
series_weight: 0
---

> L'ambition de Mozilla.ai: développer des produits d'IA open source fiables et constituer une communauté de contributeurs, hors des Big Tech.
