---
site: Siècle Digital
title: "LLaMA, le modèle de langage de Meta, a fuité"
author: Antoine Messina
date: 2023-03-10
href: https://siecledigital.fr/2023/03/10/llama-le-modele-de-langage-de-meta-a-fuite
featured_image: https://siecledigital.fr/wp-content/uploads/2023/03/meta-logo-940x550.jpg.webp
tags:
- Sciences
series:
- 202310
series_weight: 0
---

> La fuite de LLaMA a entraîné un débat sur la nécessité de rendre ces technologies open source, malgré les risques.
