---
site: BFMtv
title: "Comment trois Français exilés aux Etats-Unis sont devenus des incontournables de l'IA"
date: 2023-03-17
href: https://www.bfmtv.com/tech/intelligence-artificielle/l-ia-devrait-etre-une-sorte-de-bien-commun-hugging-face-les-francais-au-coeur-de-la-revolution_AV-202303170022.html
tags:
- Sciences
series:
- 202311
---

> Fondée par trois français, la plateforme Hugging Face se pense comme la boîte à outil de la révolution de l'intelligence artificielle. Elle héberge déjà des milliers de modèles, et défend une conception "open source" à l'opposé de celle de ChatGPT et son créateur OpenAI.
