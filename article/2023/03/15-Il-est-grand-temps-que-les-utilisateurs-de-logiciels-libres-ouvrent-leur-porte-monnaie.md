---
site: ZDNet France
title: "Il est grand temps que les utilisateurs de logiciels libres ouvrent leur porte-monnaie"
author: Jack Wallen
date: 2023-03-15
href: https://www.zdnet.fr/actualites/il-est-grand-temps-que-les-utilisateurs-de-logiciels-libres-ouvrent-leur-porte-monnaie-39955480.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/optim/i/edit/ne/2023/Wallet__w1200.jpg
tags:
- Économie
series:
- 202311
series_weight: 0
---

> L'histoire a montré que les utilisateurs de logiciels libres préfèrent que leurs logiciels soient gratuits. Mais c'est un problème pour beaucoup d'entreprises qui tentent de survivre dans un contexte économique difficile. 
