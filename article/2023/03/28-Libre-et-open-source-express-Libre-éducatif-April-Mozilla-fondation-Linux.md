---
site: ZDNet France
title: "Libre et open source express: Libre éducatif, April, Mozilla, fondation Linux"
author: Thierry Noisette
date: 2023-03-28
href: https://www.zdnet.fr/blogs/l-esprit-libre/libre-et-open-source-express-libre-educatif-april-mozilla-fondation-linux-39956268.htm
featured_image: https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2017/05/open-source-KeepCalmAndUse.jpg
tags:
- april
- Éducation
series:
- 202313
series_weight: 0
---

> Journée du Libre éducatif le 7 avril. Les travaux de l'April, par sa présidente. 30 millions de dollars pour Mozilla.ai. Une étude de la Linux Foundation sur les bénéfices de l'open source.
