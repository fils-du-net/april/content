---
site: Numerama
title: "Seul ChatGPT a le droit d'utiliser Bing, pas les autres chatbots, prévient Microsoft"
description: Microsoft veut garder son index pour lui
author: Julien Lausson
date: 2023-03-27
href: https://www.numerama.com/tech/1318614-seul-chatgpt-a-le-droit-dutiliser-bing-pas-les-autres-chatbots-previent-microsoft.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2023/01/chatgpt-numerama-12-evil-1024x576.jpg
tags:
- Sciences
series:
- 202313
series_weight: 0
---

> L'index de Bing peut-il être utilisé par des chabots concurrents de ChatGPT? Pour Microsoft, c'est clair: non.
