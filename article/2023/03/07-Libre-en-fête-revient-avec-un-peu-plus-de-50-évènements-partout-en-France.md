---
site: Next INpact
title: "«Libre en fête» revient avec un peu plus de 50 évènements partout en France"
date: 2023-03-07
href: https://www.nextinpact.com/lebrief/71158/libre-en-fete-revient-avec-peu-plus-50-evenements-partout-en-france
featured_image: https://cdnx.nextinpact.com/compress/1003-485/data-next/images/bd/square-linked-media/1902.jpg
tags:
- april
- Promotion
series:
- 202310
series_weight: 0
---

> Pour la 22e année consécutive, à l'occasion de l’arrivée du printemps, l'association April coordonne «Libre en fête», qui propose un peu plus de 50 évènements partout en France afin de découvrir et promouvoir la culture du logiciel libre, du 4 mars au 2 avril.
