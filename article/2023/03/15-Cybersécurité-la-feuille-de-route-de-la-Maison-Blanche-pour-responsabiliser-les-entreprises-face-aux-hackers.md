---
site: La Tribune
title: "Cybersécurité: la feuille de route de la Maison Blanche pour responsabiliser les entreprises face aux hackers"
author: Guillaume Renouard
date: 2023-03-15
href: https://www.latribune.fr/economie/international/cybersecurite-la-feuille-de-route-de-la-maison-blanche-pour-responsabiliser-les-entreprises-face-aux-hackers-955275.html
featured_image: https://static.latribune.fr/full_width/2093708/photo-du-president-americain-joe-biden.jpg
tags:
- International
- Institutions
series:
- 202311
series_weight: 0
---

> Au programme de l'administration Biden, la volonté de mieux protéger les infrastructures critiques, des actions renforcées contre les échanges illégaux de cryptomonnaies et, surtout, la perspective de sanctions pour les fournisseurs de logiciel qui manqueraient à leurs obligations en matière de cybersécurité.

(article lisible avec le mode lecture de firefox)
