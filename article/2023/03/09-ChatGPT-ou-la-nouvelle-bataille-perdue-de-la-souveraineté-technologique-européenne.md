---
site: cio-online.com
title: "ChatGPT ou la nouvelle bataille perdue de la souveraineté technologique européenne?"
author: Reynald Fléchaux
date: 2023-03-09
href: https://www.cio-online.com/actualites/lire-chatgpt-ou-la-nouvelle-bataille-perdue-de-la-souverainete-technologique-europeenne-14793.html
featured_image: https://images.itnewsinfo.com/cio/articles/grande/000000019654.jpg
tags:
- Sciences
series:
- 202310
---

> En quelques semaines, ChatGPT a saturé l'imaginaire technologique. Laissant, pour l'instant, la France et l'Europe sans réponse.
