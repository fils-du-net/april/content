---
site: Numerama
title: "«Nous avions tort»: en dévoilant GPT-4, OpenAI dit rejeter l'open source désormais"
description: CloseAI
author: Julien Lausson
date: 2023-03-17
href: https://www.numerama.com/tech/1307322-nous-avions-tort-en-devoilant-gpt-4-openai-dit-rejeter-lopen-source-desormais.html
featured_image: https://c0.lestechnophiles.com/www.numerama.com/wp-content/uploads/2023/01/chatgpt-3-1024x576.jpg?webp=1&key=41505124
tags:
- Sciences
series:
- 202311
---

> À l’origine, OpenAI avait embrassé le mouvement de l’open source pour ses travaux sur l’intelligence artificielle. Mais ça, c’était avant.
