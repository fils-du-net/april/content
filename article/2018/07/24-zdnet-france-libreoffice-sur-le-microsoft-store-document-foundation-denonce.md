---
site: ZDNet France
title: "LibreOffice sur le Microsoft Store: The Document Foundation dénonce"
date: 2018-07-24
href: https://www.zdnet.fr/actualites/libreoffice-sur-le-microsoft-store-the-document-foundation-denonce-39871641.htm
tags:
- Économie
- Associations
---

> La suite bureautique libre sur la plate-forme logicielle de Windows 10? Pendant quelques jours un tiers à utilisé ce stratagème pour faire payer les internautes. Sans que The Document Foundation n'approuve l'initiative.
