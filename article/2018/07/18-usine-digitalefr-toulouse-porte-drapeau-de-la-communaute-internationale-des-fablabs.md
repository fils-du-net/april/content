---
site: "usine-digitale.fr"
title: "Toulouse, porte-drapeau de la communauté internationale des fablabs"
author: Marina Angel
date: 2018-07-18
href: https://www.usine-digitale.fr/editorial/toulouse-porte-drapeau-de-la-communaute-internationale-des-fablabs.N721059
tags:
- Entreprise
- Partage du savoir
- Matériel libre
- Innovation
---

> La rencontre mondiale des fablabs organisée cette semaine à Toulouse est une nouvelle occasion de mettre en lumière de nouveaux modes de travail plus collaboratifs et d'innovation plus ouverts. Des méthodes qui font chaque jour de plus en plus d'adaptes, jusqu'au cœur de grands groupes industriels.
