---
site: Yahoo!
title: "L'UE va infliger mercredi une énorme amende à Google"
author: Céline le Prioux
date: 2018-07-17
href: https://fr.news.yahoo.com/lue-va-infliger-mercredi-%C3%A9norme-amende-%C3%A0-google-133734739--finance.html
tags:
- Entreprise
- Internet
- Institutions
- Europe
---

> Bruxelles s'apprête à infliger mercredi à Google une nouvelle amende de plusieurs milliards d'euros, cette fois dans le dossier antitrust Android, selon plusieurs sources, une décision qui va peser sur les relations entre les Etats-Unis et l'Union européenne.
