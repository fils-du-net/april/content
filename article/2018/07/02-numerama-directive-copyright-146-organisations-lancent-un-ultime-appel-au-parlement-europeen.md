---
site: Numerama
title: "Directive Copyright: 146 organisations lancent un ultime appel au Parlement européen"
author: Julien Lausson
date: 2018-07-02
href: https://www.numerama.com/politique/391982-directive-copyright-146-organisations-lancent-un-ultime-appel-au-parlement-europeen.html
tags:
- Internet
- April
- Institutions
- Associations
- Droit d'auteur
- Promotion
- Sciences
- Europe
---

> Le Parlement européen est invité à s'opposer à la proposition de directive sur le droit d’auteur dans le marché unique numérique. 146 organisations viennent de signer une lettre ouverte contre la directive Copyright.
