---
site: ZDNet France
title: "Quand les grands éditeurs irritent et abusent de leur force"
author: Christophe Auffray
date: 2018-07-02
href: https://www.zdnet.fr/actualites/quand-les-grands-editeurs-irritent-et-abusent-de-leur-force-39870570.htm
tags:
- Entreprise
- Logiciels privateurs
---

> Géants de l'IT et entreprises clientes, des partenaires? Pas sûr que toutes les pratiques des éditeurs (Microsoft, SAP, Oracle et Salesforce en tête) puissent être jugées acceptables d'acteurs qui se présentent volontiers comme des partenaires. Le Cigref plaide pour une relation ”équilibrée”.
