---
site: Usbek & Rica
title: "Neutralité du Net: l’Inde vient d’adopter les «règles les plus strictes au monde»"
author: Annabelle Laurent
date: 2018-07-12
href: https://usbeketrica.com/article/neutralite-du-net-l-inde-vient-d-adopter-les-regles-les-plus-strictes-au-monde
tags:
- Internet
- Institutions
- Neutralité du Net
- International
---

> Mercredi 12 juillet, le gouvernement indien a adopté les règles « les plus strictes au monde » en matière de neutralité du Net. La bataille sur cette question avait commencé dans ce pays dès 2015, quand Facebook avait souhaité, avec son programme FreeBasics, donner un accès gratuit à son site et à une poignée d'autres, tout en facturant le reste de l'accès à Internet. Cette initiative avait déclenché plusieurs années d'affrontement entre la plateforme et les défenseurs de ce principe fondateur d'Internet.
