---
site: Developpez.com
title: "Explosion des prix de Google Maps: le département de Maine-et-Loire abandonne Google"
author: Michael Guilloux
date: 2018-07-23
href: https://www.developpez.com/actu/215961/Explosion-des-prix-de-Google-Maps-le-departement-de-Maine-et-Loire-abandonne-Google-pour-OpenStreetMaps-un-service-de-cartographie-open-source
tags:
- Entreprise
- Internet
- Administration
- Économie
---

> Début mai, Google a annoncé une refonte complète de son offre cartographique à destination des professionnels. Ces changements, qui impliquent de renseigner obligatoirement un code de carte bleue, se traduisent par une explosion des prix pour les usages professionnels avec une réduction drastique du volume d’affichages gratuits autorisés.
