---
site: Agoravox TV
title: "PeerTube: L'alternative à Youtube"
author: matthius
date: 2018-07-04
href: https://www.agoravox.tv/actualites/economie/article/peertube-l-alternative-a-youtube-77640
tags:
- Internet
- Associations
- Innovation
---

> Nous avons reçu Pouhiou pour nous parler de son alternative à Youtube.
