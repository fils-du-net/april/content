---
site: Silicon
title: "Droits d’auteur: Wikipédia proteste contre les nouvelles directives de l’UE"
author: Rénald Boulestin
date: 2018-07-05
href: https://www.silicon.fr/droits-dauteurs-wikipedia-proteste-contre-les-nouvelles-directives-de-lue-213429.html/?inf_by=5a2a92c2671db8ea4c8b4774
tags:
- Internet
- Institutions
- Associations
- Droit d'auteur
- Europe
---

> Wikipédia a bloqué ses versions italienne, espagnole et polonaise pour protester contre les propositions de réforme sur le droit d’auteur dans l’UE qui vont restreindre la liberté de l’internet.
