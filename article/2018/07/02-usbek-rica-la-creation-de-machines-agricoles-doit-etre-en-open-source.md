---
site: Usbek & Rica
title: "«La création de machines agricoles doit être en open source»"
author: Vincent Tardieu
date: 2018-07-02
href: https://usbeketrica.com/article/la-creation-de-machines-agricoles-doit-etre-en-open-source
tags:
- Partage du savoir
- Matériel libre
- Associations
- Innovation
---

> Joseph Templier est maraîcher en bio et cofondateur, en 2011, de l’Adabio Autoconstruction, devenu en 2014 l’Atelier Paysan. Cette SCIC (société coopérative d’intérêt collectif, à but non lucratif), installée dans une ancienne papeterie de la petite ville de Renage, au nord-ouest de Grenoble (Isère), est un modèle de coopération, de solidarité et du Do It Ourselves («faire ensemble») entre agriculteurs. Mais aussi de diffusion des innovations paysannes pour en faire un bien commun.
