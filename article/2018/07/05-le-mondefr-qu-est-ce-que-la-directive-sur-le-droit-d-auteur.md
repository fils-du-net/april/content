---
site: Le Monde.fr
title: "Qu'est-ce que la directive sur le droit d'auteur?"
author: Martin Untersinger
date: 2018-07-05
href: https://www.lemonde.fr/pixels/article/2018/07/05/qu-est-ce-que-la-directive-sur-le-droit-d-auteur-et-pourquoi-fait-elle-si-peur_5326080_4408996.html
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Le Parlement européen devait donner son aval à un texte très contesté. Certains craignaient que le filtrage a priori des contenus ne se transforme en mécanisme de censure privée.
