---
site: Next INpact
title: "Le gouvernement lance des «états généraux des nouvelles régulations numériques»"
author: Xavier Berne
date: 2018-07-26
href: https://www.nextinpact.com/news/106898-le-gouvernement-lance-etats-generaux-nouvelles-regulations-numeriques.htm
tags:
- Administration
- Institutions
- Europe
---

> Le gouvernement vient d'annoncer le lancement d’une série de consultations destinées à mûrir la position de la France en matière de régulation du numérique (sous les angles économiques, sociétaux, etc.). Ces «états généraux» sont censés aboutir «début 2019».
