---
site: Next INpact
title: "Directive Droit d'auteur: vers une pluie de millions pour les sociétés de gestion collective"
author: Marc Rees
date: 2018-07-03
href: https://www.nextinpact.com/news/106807-directive-droit-dauteur-vers-pluie-millions-pour-societes-gestion-collective.htm
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Droit d'auteur
- Europe
- ACTA
---

> Jeudi, les eurodéputés examineront en séance plénière la révision de la directive sur le droit d’auteur. Au-delà du droit voisin pour les éditeurs de presse, elle enclenche le filtrage chez les intermédiaires techniques. Le levier permettra de juteuses retombées pour les sociétés de perception, et pas depuis le seul porte-monnaie des GAFAM.
