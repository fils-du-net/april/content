---
site: Miroir Mag
title: "Six clichés qui vous empêchent de vous mettre au logiciel libre"
date: 2018-07-23
href: https://www.miroir-mag.fr/technologie/six-cliches-qui-vous-empechent-de-vous-mettre-au-logiciel-libre
tags:
- Sensibilisation
- Associations
---

> Encore mal connus, les logiciels libres continuent de faire peur aux utilisateurs. Doutes sur la fiabilité, nécessité de compétences techniques, habitude des autres systèmes… Autant de freins à leur généralisation. Pourtant, ces logiciels que chacun est libre d’utiliser, modifier ou partager pourraient amener de nombreux bienfaits, s’ils étaient utilisés de manière globale.
