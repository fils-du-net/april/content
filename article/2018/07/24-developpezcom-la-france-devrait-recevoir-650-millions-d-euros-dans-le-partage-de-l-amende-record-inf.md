---
site: Developpez.com
title: "La France devrait recevoir 650 millions d'euros dans le partage de l'amende record infligée à Google"
author: Michael Guilloux
date: 2018-07-24
href: https://www.developpez.com/actu/216382/La-France-devrait-recevoir-650-millions-d-euros-dans-le-partage-de-l-amende-record-infligee-a-Google-mais-qui-seront-les-beneficiaires
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Europe
---

> La semaine passée, la Commission européenne a infligé à Google une amende de 4,34 milliards d'euros (soit 5 milliards de dollars) pour violation des règles de concurrence de l'UE avec Android. Si l'on devait encore une fois présenter ces violations, on dirait qu'il s'agit de «trois types de restrictions imposées par Google aux fabricants d'appareils Android et aux opérateurs de réseaux», pour reprendre les mots de Margrethe Vestager, commissaire chargée de la politique de concurrence.
