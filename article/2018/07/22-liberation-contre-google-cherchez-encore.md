---
site: Libération
title: "Contre Google, cherchez encore"
author: Christophe Alix
date: 2018-07-22
href: http://www.liberation.fr/france/2018/07/22/contre-google-cherchez-encore_1668138
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Europe
---

> Au regard de la surface financière acquise par Google, l’amende que vient de lui infliger l’Union européenne, pour record qu’elle soit, a tout d’une goutte d’eau dans un océan de dollars. Que représentent en effet 4,3 milliards d’euros au regard d’une capitalisation boursière qui dépasse les 700 milliards d’euros et d’une trésorerie qui avoisine les 100 milliards ?
