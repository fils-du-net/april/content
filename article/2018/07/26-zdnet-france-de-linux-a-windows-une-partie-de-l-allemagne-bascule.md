---
site: ZDNet France
title: "De Linux à Windows: une partie de l'Allemagne bascule"
author: David Meyer
date: 2018-07-26
href: https://www.zdnet.fr/actualites/de-linux-a-windows-une-partie-de-l-allemagne-bascule-39871733.htm
tags:
- Entreprise
- Logiciels privateurs
- Administration
- International
---

> Après être passé en 2006 de Solaris à Linux, le land allemand de Basse-Saxe veut à présent migrer 13.000 postes de travail d'OpenSuse vers Windows, vraisemblablement Windows 10. Justification avancée: la standardisation des OS.
