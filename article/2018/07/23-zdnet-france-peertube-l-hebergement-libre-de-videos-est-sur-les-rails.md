---
site: ZDNet France
title: "PeerTube: l'hébergement libre de vidéos est sur les rails"
author: Thierry Noisette
date: 2018-07-23
href: https://www.zdnet.fr/blogs/l-esprit-libre/peertube-l-hebergement-libre-de-videos-est-sur-les-rails-39871579.htm
tags:
- Économie
- Associations
- Innovation
---

> Soutenu par Framasoft, PeerTube a réussi haut la main son appel à crowdfunding. Cet hébergement de vidéo décentralisé devrait se doter dans quelques mois de fonctionnalités améliorées.
