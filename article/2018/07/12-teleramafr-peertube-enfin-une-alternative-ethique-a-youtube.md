---
site: Télérama.fr
title: "PeerTube, enfin une alternative éthique à YouTube"
author: Annabelle Chauvet
date: 2018-07-12
href: https://www.telerama.fr/medias/peertube,-la-plateforme-qui-defie-youtube-avec-lethique,n5715829.php
tags:
- Internet
- Économie
- Associations
- Innovation
---

> Pas d’exploitation des données personnelles et pas de financement publicitaire. La plateforme d’échange de vidéos devrait voir le jour à l’automne 2018. Elle a été lancée par l’association Framasoft, déjà connue pour ses logiciels libres.
