---
site: FIGARO
title: "«Avec les réseaux sociaux, chacun est devenu le surveillant de l'autre»"
author: Etienne Campion
date: 2018-07-06
href: http://www.lefigaro.fr/vox/societe/2018/07/06/31003-20180706ARTFIG00153-avec-les-reseaux-sociaux-chacun-est-devenu-le-surveillant-de-l-autre.php
tags:
- Entreprise
- Internet
- Vie privée
---

> Laurent Gayard a étudié en profondeur l'univers d'Internet: celui des réseaux sociaux, des GAFAM, du darknet... Voyage dans ce monde troublant.
