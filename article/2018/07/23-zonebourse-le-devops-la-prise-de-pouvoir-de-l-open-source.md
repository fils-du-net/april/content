---
site: zonebourse
title: "Le DevOps – la prise de pouvoir de l’Open Source"
author: Jan Gabriel
date: 2018-07-23
href: https://www.zonebourse.com/ITS-GROUP-5096/actualite/ITS-Le-DevOps-ndash-la-prise-de-pouvoir-de-l-rsquo-Open-Source--26978275/
tags:
- Économie
- Innovation
---

> S'il y a un débat public sur le fait qu'un outil puisse ou ne puisse pas être un «outil DevOps», nous pouvons néanmoins constater que les thèmes phares du DevOps - Agilité, Automatisation, Collaboration - se recoupent fort bien avec ceux du mouvement Open Source.
