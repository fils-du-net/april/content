---
site: ZDNet France
title: "5 alternatives libres à Skype"
author: Stéphanie Chaptal
date: 2018-07-09
href: https://www.zdnet.fr/actualites/5-alternatives-libres-a-skype-39870892.htm
tags:
- Internet
- Promotion
---

> Longtemps présenté comme l’outil indispensable pour faire des vidéoconférences ou de la messagerie instantanée, Skype a été détrôné pour certaines fonctions, mais pas toutes. Voici cinq solutions issues du monde open source pour s’en débarrasser totalement.
