---
site: ActuaLitté.com
title: "En Italie, Wikipedia ferme pour dénoncer la directive droit d'auteur"
author: Clément Solym
date: 2018-07-03
href: https://www.actualitte.com/article/patrimoine-education/en-italie-wikipedia-ferme-pour-denoncer-la-directive-droit-d-auteur/89716
tags:
- Internet
- Institutions
- Associations
- Droit d'auteur
- Europe
---

> La communauté Wikipedia d’Italie a décidé de bloquer l’accès à ses pages et ses résultats de recherche, protestation contre la directive européenne sur le droit d’auteur. Débattue au Parlement européen, elle «menace la liberté du en ligne et crée des obstacles dans l’accès au Net». Juste cela.
