---
site: ChannelNews
title: "Le Cigref dénonce les pratiques abusives des grands éditeurs en position dominante"
author: Dirk Basyn
date: 2018-07-02
href: https://www.channelnews.fr/le-cigref-denonce-les-pratiques-abusives-dediteurs-en-position-dominante-82550
tags:
- Entreprise
- Logiciels privateurs
- Informatique en nuage
---

> Les relations se sont considérablement détériorées entre les grandes entreprises et les administrations d’une part et les grands fournisseurs et éditeurs de logiciels d’autre part à cause de l’irruption de certaines technologies, dont le cloud, estime le Cigref qui appelle à «un comportement commercial plus mesuré des grands éditeurs, à une relation équilibrée et transparente (gagnant-gagnant) et à des partenariats d’innovation».
