---
site: Le Monde.fr
title: "PeerTube, le «YouTube décentralisé», réussit son financement participatif"
author: Bastien Lion
date: 2018-07-09
href: https://www.lemonde.fr/pixels/article/2018/07/09/peertube-le-youtube-decentralise-reussit-son-financement-participatif_5328501_4408996.html
tags:
- Internet
- Économie
- Associations
- Innovation
- Promotion
- Sciences
---

> L’association française pour le développement de logiciels libres Framasoft lance une plate-forme de vidéos, qui doit encore faire ses preuves si elle veut, à terme, attirer le grand public.
