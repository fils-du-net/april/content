---
site: Batiactu
title: "Logiciels anti-fraude à la TVA: l'administration fiscale apporte des précisions"
author: G.N.
date: 2018-07-10
href: https://www.batiactu.com/edito/logiciels-anti-fraude-a-tva-administration-fiscale-53559.php
tags:
- Entreprise
- Économie
- Institutions
---

> REGLEMENTATION. Les contours de l'obligation d'utilisation d'un logiciel de caisse certifié pour les professionnels sont détaillés dans un bulletin officiel des Finances publiques, paru ce 4 juillet 2018, soit six mois après l'entrée en vigueur de cette mesure. Analyse.
