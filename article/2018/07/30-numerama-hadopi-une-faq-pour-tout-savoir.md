---
site: Numerama
title: "Hadopi: une FAQ pour tout savoir"
author: Julien Lausson
date: 2018-07-30
href: https://www.numerama.com/politique/129728-hadopi-faq-savoir.html
tags:
- Internet
- Administration
- HADOPI
---

> La Hadopi est toujours dans l'actualité plusieurs années après la création de la riposte graduée. Les envois de mails Hadopi n'ont jamais été aussi nombreux et l'on ne sait pas toujours comment réagir. Voici donc de quoi répondre à toutes vos questions.
