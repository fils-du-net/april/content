---
site: PCInpact
title: "La présidente d’Hadopi a du mal à oublier la Cour de cassation"
author: Marc Rees
date: 2018-07-07
href: http://www.pcinpact.com/actu/news/58129-lcen-mariefrancoise-marais-hadopi-cassation.htm
tags:
- Internet
- HADOPI
- Institutions
---

> Marie Françoise Marais, présidente de l'Hadopi, l'avait dit lors des deux conférences de presse organisées Rue du Texel: elle reste magistrate à la Cour de cassation, mais elle ne touchera plus aux dossiers liés à l’univers numérique.
