---
site: FZN
title: "PeerTube a réussi sa campagne de financement participatif"
author: Andy
date: 2018-07-12
href: http://www.fredzone.org/peertube-a-reussi-sa-campagne-de-financement-participatif-887
tags:
- Internet
- Associations
- Innovation
---

> Récemment, Framasoft, une association de développement de services et logiciels a lancé sa propre version de YouTube nommée PeerTube.
