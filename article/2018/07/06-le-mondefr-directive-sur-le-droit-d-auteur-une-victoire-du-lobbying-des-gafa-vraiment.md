---
site: Le Monde.fr
title: "Directive sur le droit d'auteur: une victoire du lobbying des GAFA, vraiment?"
author: Pixels
date: 2018-07-06
href: https://www.lemonde.fr/pixels/article/2018/07/06/directive-sur-le-droit-d-auteur-une-victoire-du-lobbying-des-gafa-vraiment_5326914_4408996.html
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Droit d'auteur
- Europe
---

> Les défenseurs du texte, rejeté en l’état jeudi par le Parlement, accusent les géants du Web d’avoir eu recours à un lobbying massif. Mais la réalité est nettement plus nuancée.
