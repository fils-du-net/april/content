---
site: Normandinamik
title: "Logiciels de caisse certifiés: l’administration fiscale précise le dispositif"
author: Jacques-Olivier Gasly
date: 2018-07-11
href: https://www.normandinamik.cci.fr/logiciels-de-caisse-certifies-ladministration-fiscale-precise-le-dispositif
tags:
- Entreprise
- Économie
- Institutions
---

> Les commentaires qui précisent la définition du logiciel ou système de caisse et détaillent les contours de l’obligation d’utiliser un logiciel certifié, viennent d’être publiés par la Direction générale des Finances publiques, en concertation avec les différents partenaires et les professionnels, dans un Bofip du 4 juillet 2018.
