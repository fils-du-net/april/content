---
site: Podcast Journal
title: "Contre le monopole des GAFAM, le logiciel libre"
date: 2018-07-24
href: https://www.podcastjournal.net/Contre-le-monopole-des-GAFAM-le-logiciel-libre_a25566.html
tags:
- April
- Sensibilisation
---

> Google, Apple, Facebook, Amazon, Microsoft sont souvent épinglés pour leur utilisation de nos données personnelles ou encore sur les bénéfices énormes qu'ils font dans les pays sans y reverser d'impôts. Difficile pourtant de lutter contre leur omniprésence sur le web et dans le monde de l'informatique. L'utilisation de logiciels libres apparaît peu à peu comme leur meilleure alternative.
