---
site: Developpez.com
title: "Après Munich, la Basse-Saxe, un État fédéré d'Allemagne, annonce l'abandon de Linux pour Windows"
author: Bill Fassinou
date: 2018-07-27
href: https://www.developpez.com/actu/216837/Apres-Munich-la-Basse-Saxe-un-Etat-federe-d-Allemagne-annonce-l-abandon-de-Linux-pour-Windows-afin-de-standardiser-ses-systemes-informatiques
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Marchés publics
- International
---

> En 2017, le conseil municipal de Munich a pris la décision d’abandonner LiMux, la distribution de Linux créée spécialement pour la ville, afin d’adopter pleinement Windows 10 d’ici 2020. Les élus de la ville avaient, à l’époque, voté à une majorité de 50 contre 25 la migration de l’ensemble des postes alors sous Linux vers Windows 10. Après Munich, une importante migration de Linux vers Windows vient d’être annoncée en Basse-Saxe, un État fédéré d'Allemagne (ou Land).
