---
site: "cio-online.com"
title: "Le Maine-et-Loire trouve une alternative Open Source à Google Maps"
author: Jacques Cheminat
date: 2018-07-25
href: https://www.cio-online.com/actualites/lire-le-maine-et-loire-trouve-une-alternative-open-source-a-google-maps-10505.html
tags:
- Entreprise
- Internet
- Administration
- Économie
---

> Face au passage à un modèle payant, le département du Maine-et-Loire a décidé de se passer des services de cartographie de Google. La collectivité a choisi la solution Open Source, Openstreet.
