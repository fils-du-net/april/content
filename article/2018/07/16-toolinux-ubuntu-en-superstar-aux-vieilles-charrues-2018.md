---
site: toolinux
title: "Ubuntu en superstar aux Vieilles Charrues 2018"
date: 2018-07-16
href: https://www.toolinux.com/?Ubuntu-en-superstar-aux-Vieilles-Charrues-2018
tags:
- Promotion
---

> Déjà plus de 20 ans pour les Vieilles Charrues à Carhaix. Ce que vous ignorez peut-être, c’est que cette année encore, le festival va permettre de promouvoir Linux et Ubuntu.
