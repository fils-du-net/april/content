---
site: Numerama
title: "Directive droit d'auteur: le vote du Parlement européen fixé au 12 septembre"
author: Julien Lausson
date: 2018-07-10
href: https://www.numerama.com/politique/393962-directive-droit-dauteur-le-vote-du-parlement-europeen-fixe-au-12-septembre.html
tags:
- Entreprise
- Internet
- Institutions
- Désinformation
- Droit d'auteur
- Europe
---

> Le Parlement européen sera invité à prendre position sur la future directive copyright, dont deux articles sont au cœur d'une vive controverse.
