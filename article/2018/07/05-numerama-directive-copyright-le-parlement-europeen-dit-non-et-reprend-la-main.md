---
site: Numerama
title: "Directive Copyright: le Parlement européen dit non et reprend la main"
author: Julien Lausson
date: 2018-07-05
href: https://www.numerama.com/politique/392882-directive-copyright-le-parlement-europeen-desapprouve.html
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
- Sciences
- Europe
- Vie privée
---

> Le mandat de négociations de la commission des affaires juridiques pour la directive sur le droit d'auteur a été rejeté par le Parlement européen en séance plénière, jeudi 5 juillet.
