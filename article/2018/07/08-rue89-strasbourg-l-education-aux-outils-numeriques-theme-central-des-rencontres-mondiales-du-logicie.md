---
site: Rue89 Strasbourg
title: "L'éducation aux outils numériques, thème central des Rencontres mondiales du logiciel libre"
author: Simon Adolf
date: 2018-07-08
href: https://www.rue89strasbourg.com/leducation-aux-outils-numeriques-theme-central-des-rencontres-mondiales-du-logiciel-libre-138982
tags:
- Associations
- Éducation
- Promotion
---

> L’université de Strasbourg accueille du 7 au 12 juillet les 18e Rencontres mondiales du logiciel libre (RMLL). Organisé en partenariat avec l’association Hackstub, le plus grand évènement francophone sur ce thème attend 5 000 participants et des intervenants de top niveau. Cette année, les débats portent sur la place de plus en plus importante que prennent les grandes entreprises dans l’éducation au numérique.
