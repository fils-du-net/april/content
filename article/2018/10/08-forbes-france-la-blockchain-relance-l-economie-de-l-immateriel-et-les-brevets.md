---
site: Forbes France
title: "La Blockchain Relance L’Économie De L’Immatériel Et Les Brevets"
author: Vincent Lorphelin et Pierre Ollivier
date: 2018-10-08
href: https://www.forbes.fr/finance/la-blockchain-relance-leconomie-de-limmateriel-et-les-brevets
tags:
- Économie
- Innovation
---

> L’économie de l’immatériel, qui s’est toujours heurtée à la difficulté de mesurer les actifs immatériels, est restée théorique. Pourtant, les évolutions liées aux usages, à l’intelligence artificielle et à la blockchain révèlent aujourd’hui de nouveaux instruments de mesure pour valoriser l’immatériel et la propriété industrielle.
