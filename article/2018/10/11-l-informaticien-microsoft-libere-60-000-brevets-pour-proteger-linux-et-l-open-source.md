---
site: L'Informaticien
title: "Microsoft libère 60 000 brevets pour protéger Linux et l’open source"
author: Guillaume Périssat
date: 2018-10-11
href: https://www.linformaticien.com/actualites/id/50493/microsoft-libere-60-000-brevets-pour-proteger-linux-et-l-open-source.aspx
tags:
- Entreprise
- Brevets logiciels
---

> L’éditeur rejoint l’Open Invention Network, une organisation destinée à protéger Linux et l’open source au moyen d’un portefeuille de brevets et de licences croisées. Et Microsoft ne vient pas les mains vides puisque il offre à la plateforme 60 000 de ses brevets.
