---
site: Le Monde Informatique
title: "La feuille de route numérique de l'Etat se redessine"
author: Bertrand Lemaire
date: 2018-10-25
href: https://www.lemondeinformatique.fr/actualites/lire-la-feuille-de-route-numerique-de-l-etat-se-redessine-73241.html
tags:
- Administration
- Institutions
- Open Data
---

> Du réseau social privé Tech.Gouv au renouvellement des dirigeants en passant par des missions claires, les ambitions numériques du gouvernement sont réaffirmées.
