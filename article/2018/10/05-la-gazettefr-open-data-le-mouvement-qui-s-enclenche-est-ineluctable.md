---
site: La gazette.fr
title: "Open data: «Le mouvement qui s'enclenche est inéluctable»"
author: Romain Mazon
date: 2018-10-05
href: http://www.lagazettedescommunes.com/585196/open-data-le-mouvement-qui-senclenche-est-ineluctable-axelle-lemaire
tags:
- Administration
- Open Data
---

> Adoptée dans la loi pour une République numérique d'octobre 2016, l'obligation d'open data par défaut pour les collectivités territoriales entre en vigueur le 7 octobre. Si le mouvement a incontestablement progressé, on reste encore loin du compte, puisque à peine 10 % des collectivités concernées ont ouvert des données. Dans les administrations de l'Etat, le résultat est aussi mitigé.
