---
site: "Génération-NT"
title: "Rachat de GitHub par Microsoft: feu vert de la Commission européenne"
author: Jérôme G.
date: 2018-10-19
href: https://www.generation-nt.com/github-rachat-microsoft-commission-europeenne-actualite-1958566.html
tags:
- Entreprise
- Internet
- Institutions
- Innovation
- Europe
---

> La Commission autorise sans condition l'acquisition de GitHub par Microsoft pour 7,5 milliards de dollars.
