---
site: L'Informaticien
title: "Chaises musicales à la DINSIC, la DSI de l'État"
author: Guillaume Périssat
date: 2018-10-30
href: https://www.linformaticien.com/actualites/id/50635/chaises-musicales-a-la-dinsic-la-dsi-de-l-etat.aspx
tags:
- Administration
- Institutions
---

> Revoilà Mounir Mahjoubi conforté dans sa mission de grand patron du numérique au gouvernement. Et quoique passé de Matignon à Bercy, il a emmené dans ses valises rien de moins que la DINSIC, en profitant pour remplacer Henri Verdier par Nadi Bou Hanna, un ancien haut-fonctionnaire devenu entrepreneur.
