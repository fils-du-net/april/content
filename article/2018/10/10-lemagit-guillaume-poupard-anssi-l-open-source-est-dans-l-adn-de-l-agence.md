---
site: LeMagIT
title: "Guillaume Poupard, Anssi: «l'open source est dans l'ADN de l'agence»"
author: Valéry Marchive
date: 2018-10-10
href: https://www.lemagit.fr/actualites/252450411/Guillaume-Poupard-Anssi-lopen-source-est-dans-lADN-de-lagence
tags:
- Administration
- Institutions
- Sensibilisation
- Informatique en nuage
---

> Aux Assises de la Sécurité, Le directeur général de l’Agence nationale pour la sécurité des systèmes d’information revient avec nous sur ces projets internes que l’Anssi a versé à l’open source. Mais également sur ces start-ups qui ont émergé de ses rangs.
