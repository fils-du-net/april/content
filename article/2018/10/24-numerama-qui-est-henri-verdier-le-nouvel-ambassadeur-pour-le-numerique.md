---
site: Numerama
title: "Qui est Henri Verdier, le nouvel ambassadeur pour le numérique?"
author: Julien Lausson
date: 2018-10-24
href: https://www.numerama.com/politique/434380-qui-est-henri-verdier-le-nouvel-ambassadeur-pour-le-numerique.html
tags:
- Administration
- Institutions
- Open Data
---

> Le gouvernement a nommé un nouvel ambassadeur pour le numérique. Il s'agit de Henri Verdier. Il remplace David Martinon à ce poste.
