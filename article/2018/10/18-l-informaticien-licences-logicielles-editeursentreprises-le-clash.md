---
site: L'Informaticien
title: "Licences logicielles - Éditeurs/entreprises: le clash!"
author: Alain Clapaud
date: 2018-10-18
href: https://www.linformaticien.com/dossiers/licences-logicielles-le-clash.aspx
tags:
- Entreprise
- Logiciels privateurs
- English
---

> Contrats de licence illisibles, chasseurs de primes, méthodes de voyous, racket, les noms d’oiseaux fusent lorsqu’on évoque la problématique des licences logicielles auprès des DSI. Alors que les migrations vers le Cloud battent leur plein, le torchon brûle entre DSI et grands éditeurs.
