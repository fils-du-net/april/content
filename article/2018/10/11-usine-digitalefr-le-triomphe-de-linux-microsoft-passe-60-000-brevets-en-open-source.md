---
site: "usine-digitale.fr"
title: "Le triomphe de Linux: Microsoft passe 60 000 brevets en open source"
author: Julien Bergounhoux
date: 2018-10-11
href: https://www.usine-digitale.fr/article/le-triomphe-de-linux-microsoft-passe-60-000-brevets-en-open-source.N753734
tags:
- Entreprise
- Brevets logiciels
---

> Non, vous ne rêvez pas: Microsoft est un champion de l'open source. L'entreprise vient d'annoncer qu'elle ouvre plus de 60 000 de ses brevets au sein du consortium Open Invention Network, afin de protéger ses partenaires et clients contre des procès abusifs. Mais en vérité, le soutien de Microsoft à l'open source n'est pas nouveau, et cette annonce est le résultat d'une lente transformation ces 10 dernières années.
