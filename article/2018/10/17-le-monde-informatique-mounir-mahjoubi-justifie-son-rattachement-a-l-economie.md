---
site: Le Monde Informatique
title: "Mounir Mahjoubi justifie son rattachement à l'Economie"
author: Bertrand Lemaire
date: 2018-10-17
href: https://www.lemondeinformatique.fr/actualites/lire-mounir-mahjoubi-justifie-son-rattachement-a-l-economie-73172.html
tags:
- Institutions
---

> Jusque-là secrétaire d'Etat auprès du Premier ministre, chargé du Numérique, Mounir Mahjoubi est désormais secrétaire d'Etat auprès du ministre de l'Economie et des Finances et du ministre de l'Action et des Comptes publics. Il garde le périmètre du Numérique et récupère au passage une double tutelle.
