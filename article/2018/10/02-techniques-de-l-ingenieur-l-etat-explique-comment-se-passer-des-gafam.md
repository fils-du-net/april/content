---
site: Techniques de l'Ingénieur
title: "L’État explique comment se passer des GAFAM"
author: Philippe Richard
date: 2018-10-02
href: https://www.techniques-ingenieur.fr/actualite/articles/letat-explique-comment-se-passer-des-gafam-58573
tags:
- Administration
- Promotion
- Vie privée
---

> Peut-on travailler sans utiliser des outils et des plateformes des poids lourds américains? L’État répond positivement à cette question en publiant une liste de solutions alternatives.
