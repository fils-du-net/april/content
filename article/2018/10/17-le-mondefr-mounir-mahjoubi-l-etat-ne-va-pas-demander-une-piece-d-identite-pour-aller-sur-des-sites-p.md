---
site: Le Monde.fr
title: "Mounir Mahjoubi: «L’Etat ne va pas demander une pièce d’identité pour aller sur des sites pornos»"
author: Damien Leloup et Martin Untersinger
date: 2018-10-17
href: https://www.lemonde.fr/pixels/article/2018/10/17/mounir-mahjoubi-l-etat-ne-va-pas-demander-des-pieces-d-identite-pour-aller-sur-des-sites-pornos_5370605_4408996.html
tags:
- Internet
- Institutions
---

> Pornographie en ligne, RGPD, YouTube, lutte contre les contenus haineux sur les réseaux sociaux… entretien avec le secrétaire d’Etat au numérique.
