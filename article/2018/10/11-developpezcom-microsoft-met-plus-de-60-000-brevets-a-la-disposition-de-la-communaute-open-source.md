---
site: Developpez.com
title: "Microsoft met plus de 60 000 brevets à la disposition de la communauté open source"
author: Michael Guilloux
date: 2018-10-11
href: https://www.developpez.com/actu/228380/Microsoft-met-plus-de-60-000-brevets-a-la-disposition-de-la-communaute-open-source-la-firme-renonce-a-des-milliards-de-dollars-pour-proteger-Linux
tags:
- Entreprise
- Brevets logiciels
---

> Microsoft qui, pendant longtemps, a généré des milliards de dollars grâce à son large portefeuille de brevets logiciels, pourrait maintenant renoncer à une source importante de revenus. Rappelons en effet qu'un certain nombre de fournisseurs Android versent à Microsoft une redevance sur chaque téléphone qu’ils vendent en échange du droit d'exploitation de brevets appartenant à la firme de Redmond.
