---
site: Le Temps
title: "Les résistants du logiciel libre"
author: Anouch Seydtaghia
date: 2018-10-11
href: https://www.letemps.ch/economie/resistants-logiciel-libre
tags:
- Internet
- Associations
- Vie privée
---

> L’association française Framasoft se bat pour proposer un maximum de variations «libres» des principaux services commerciaux de Microsoft, Google et même Facebook. Son but: se libérer du joug de ces géants de la technologie
