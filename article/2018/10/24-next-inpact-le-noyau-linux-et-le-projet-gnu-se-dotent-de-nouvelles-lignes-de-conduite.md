---
site: Next INpact
title: "Le noyau Linux et le projet GNU se dotent de nouvelles lignes de conduite"
date: 2018-10-24
href: https://www.nextinpact.com/brief/le-noyau-linux-et-le-projet-gnu-se-dotent-de-nouvelles-lignes-de-conduite-6159.htm
tags:
- Associations
- Innovation
---

> Il y a du mouvement dans le monde du logiciel libre: le comportement des développeurs est actuellement au centre des attentions. On note ainsi deux publications importantes.
