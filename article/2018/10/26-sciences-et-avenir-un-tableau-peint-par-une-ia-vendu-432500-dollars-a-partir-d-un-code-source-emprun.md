---
site: Sciences et avenir
title: "Un tableau peint par une IA vendu 432.500 dollars... à partir d'un code source emprunté sur le web"
author: Sarah Sermondadaz
date: 2018-10-26
href: https://www.sciencesetavenir.fr/high-tech/intelligence-artificielle/un-tableau-peint-par-une-ia-vendu-a-plus-de-400-000-dollars_128993
tags:
- Licenses
---

> Une oeuvre peinte par une intelligence artificielle a été vendue aux enchères à plus de 435.000 dollars à New York. Une vente qui nourrit la polémique: l'algorithme utilisé avait au préalable été divulgué en open source par un autre artiste.
