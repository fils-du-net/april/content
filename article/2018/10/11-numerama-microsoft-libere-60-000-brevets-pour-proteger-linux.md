---
site: Numerama
title: "Microsoft libère 60 000 brevets pour protéger Linux"
author: Julien Lausson
date: 2018-10-11
href: https://www.numerama.com/tech/426979-microsoft-libere-60-000-brevets-pour-proteger-linux.html
tags:
- Entreprise
- Brevets logiciels
---

> Microsoft ne se contente pas d'aimer Linux: le groupe veut aussi le protéger. Le géant des logiciels rejoint le fonds de propriété intellectuelle OIN, qui propose de mettre en commun des brevets au profit de Linux. Et il n'est pas venu les mains vides: il offre 60 000 de ses brevets.
