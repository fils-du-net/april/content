---
site: Les Frontaliers
title: "Fab lab: qu'est-ce que c'est?"
date: 2018-10-03
href: https://www.lesfrontaliers.lu/societe/fab-lab-quest-ce-que-cest
tags:
- Partage du savoir
- Innovation
---

> Fab Lab...Bien sûr, ça vient des États-Unis ! A quoi ça sert? Pour les artistes, bricoleurs, entrepreneurs....jeunes, vieux? Tout savoir....
