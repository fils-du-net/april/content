---
site: Developpez.com
title: "La Commission européenne donne son feu vert pour le rachat de GitHub par Microsoft, sans condition"
author: Michael Guilloux
date: 2018-10-20
href: https://www.developpez.com/actu/229829/La-Commission-europeenne-donne-son-feu-vert-pour-le-rachat-de-GitHub-par-Microsoft-sans-condition
tags:
- Entreprise
- Internet
- Institutions
- Europe
---

> Après une rumeur de courte durée, Microsoft a annoncé début juin l'acquisition de la plateforme web d'hébergement et de gestion de développement de logiciels GitHub, pour le montant de 7,5 milliards de dollars en actions. Mais comme il est coutume pour ce genre de transaction, la firme de Redmond devait se soumettre à un examen règlementaire de différentes autorités de régulation dans le monde afin de finaliser le rachat de GitHub.
