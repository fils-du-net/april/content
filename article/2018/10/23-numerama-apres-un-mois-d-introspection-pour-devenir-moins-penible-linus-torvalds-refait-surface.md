---
site: Numerama
title: "Après un mois d'introspection pour devenir moins pénible, Linus Torvalds refait surface"
author: Julien Lausson
date: 2018-10-23
href: https://www.numerama.com/tech/433946-apres-un-mois-dintrospection-pour-devenir-moins-penible-linus-torvalds-refait-surface.html
tags:
- Associations
- Innovation
---

> Linus Torvalds a annoncé en septembre prendre ses distances avec le noyau Linux, qu'il développe depuis des années, pour corriger son mauvais caractère.
