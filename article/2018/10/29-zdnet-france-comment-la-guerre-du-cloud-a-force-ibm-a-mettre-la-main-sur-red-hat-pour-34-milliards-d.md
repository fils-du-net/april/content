---
site: ZDNet France
title: "Comment la guerre du cloud a forcé IBM à mettre la main sur Red Hat (pour 34 milliards de dollars)"
author: Larry Dignan
date: 2018-10-29
href: https://www.zdnet.fr/actualites/comment-la-guerre-du-cloud-a-force-ibm-a-mettre-la-main-sur-red-hat-pour-34-milliards-de-dollars-39875715.htm
tags:
- Entreprise
- Informatique en nuage
---

> L'achat de Red Hat par IBM est un pari important sur le cloud hybride et privé et la capacité de Big Blue à gérer plusieurs fournisseurs de cloud public. Voici quelques dynamiques à méditer.
