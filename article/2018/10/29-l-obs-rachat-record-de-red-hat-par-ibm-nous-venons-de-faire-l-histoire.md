---
site: L'OBS
title: "Rachat record de Red Hat par IBM: \"Nous venons de faire l’histoire\""
author: Thierry Noisette
date: 2018-10-29
href: https://www.nouvelobs.com/economie/20181029.OBS4642/rachat-record-de-red-hat-par-ibm-nous-venons-de-faire-l-histoire.html
tags:
- Entreprise
- Informatique en nuage
---

> Big Blue fait la plus grosse acquisition de son histoire centenaire, en reprenant le leader mondial des logiciels open source. L’époque où les géants tech regardaient de haut le logiciel libre est bien de la préhistoire.
