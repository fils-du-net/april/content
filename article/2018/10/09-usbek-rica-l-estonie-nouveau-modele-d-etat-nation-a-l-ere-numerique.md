---
site: Usbek & Rica
title: "L’Estonie, nouveau modèle d’Etat-nation à l’ère numérique?"
author: Pablo Maillé
date: 2018-10-09
href: https://usbeketrica.com/article/en-france-l-administration-sert-l-etat-en-estonie-elle-sert-le-citoyen
tags:
- Internet
- Administration
- Institutions
- International
---

> Violaine Champetier de Ribes et Jean Spiri dressent le portrait de l’Estonie, «modèle d’Etat-nation à l’ère numérique».
