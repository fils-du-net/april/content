---
site: Les Numeriques
title: "Pour aider Linux, Microsoft rend 60 000 brevets open source"
author:  Mathieu Chartier
date: 2018-10-11
href: https://www.lesnumeriques.com/vie-du-net/pour-aider-linux-microsoft-rend-60-000-brevets-open-source-n79151.html
tags:
- Entreprise
- Brevets logiciels
---

> Joli geste que celui de Microsoft qui fait une nouvelle fois un pas en direction des développeurs, rejoignant l'Open Invention Network et rendant 60 000 de ses brevets Open Source.
