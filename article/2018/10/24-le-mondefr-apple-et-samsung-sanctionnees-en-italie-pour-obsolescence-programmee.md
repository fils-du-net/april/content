---
site: Le Monde.fr
title: "Apple et Samsung sanctionnées en Italie pour obsolescence programmée"
date: 2018-10-24
href: https://www.lemonde.fr/entreprises/article/2018/10/24/apple-et-samsung-sanctionnees-en-italie-pour-obsolescence-programmee_5373931_1656994.html
tags:
- Entreprise
- Institutions
- International
---

> Ces sociétés «ont mis en œuvre des pratiques commerciales malhonnêtes», a conclu l’autorité italienne à l’issue de son enquête.
