---
site: La Tribune
title: "PeerTube: ”Le logiciel libre est une alternative crédible à l'hyperpuissance des GAFA”"
author: Anaïs Cherif
date: 2018-10-15
href: https://www.latribune.fr/technos-medias/peertube-le-logiciel-libre-est-une-alternative-credible-a-l-hyperpuissance-des-gafa-793324.html
tags:
- Internet
- Associations
- Innovation
- Promotion
---

> L'association française Framasoft, qui milite pour le développement de logiciels libres, lance ce lundi PeerTube, un "Youtube décentralisé". Un financement participatif de 53.100 euros, bouclé cet été, lui a permis de voir le jour. Pierre-Yves Gosset, directeur de Framasoft, revient sur la genèse du projet.
