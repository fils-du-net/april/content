---
site: Numerama
title: "Directive sur le droit d'auteur: vers un accord d'ici la fin décembre 2018?"
author: Julien Lausson
date: 2018-10-09
href: https://www.numerama.com/politique/426439-directive-sur-le-droit-dauteur-vers-un-accord-dici-la-fin-decembre-2018.html
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
- Vie privée
---

> Approuvée par le Parlement européen mi-septembre, la directive sur le droit d'auteur doit maintenant faire l'objet d'un accord avec le Conseil et la Commission. Officiellement, l'objectif est de trouver un terrain d'entente avant la fin de l'année.
