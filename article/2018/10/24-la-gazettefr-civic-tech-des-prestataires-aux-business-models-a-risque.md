---
site: La gazette.fr
title: "Civic tech: des prestataires aux business models à risque"
author: Gabriel Zignani
date: 2018-10-24
href: https://www.lagazettedescommunes.com/587720/civic-tech-des-prestataires-aux-business-models-a-risque
tags:
- Entreprise
- Institutions
- Innovation
- Open Data
---

> Les modèles économiques des acteurs de la civic tech sont multiples. Mais ce marché reste fragile, et aucun modèle ne présente de garantie absolue.
