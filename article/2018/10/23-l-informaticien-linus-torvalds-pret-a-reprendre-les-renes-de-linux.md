---
site: L'Informaticien
title: "Linus Torvalds prêt à reprendre les rênes de Linux?"
author: Guillaume Périssat
date: 2018-10-23
href: https://www.linformaticien.com/actualites/id/50578/linus-torvalds-pret-a-reprendre-les-renes-de-linux.aspx
tags:
- Associations
- Innovation
---

> A en croire Greg Kroah-Hartman, le créateur du kernel reprendrait prochainement du service. Son numéro deux annonce, un peu plus d’un mois après que Linus Torvalds se soit mis en retrait, qu’il rend le «kernel tree» à son «patron». La pause introspective n’aura pas duré bien longtemps.
