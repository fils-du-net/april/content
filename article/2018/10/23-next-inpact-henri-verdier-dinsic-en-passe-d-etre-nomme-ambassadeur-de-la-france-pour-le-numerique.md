---
site: Next INpact
title: "Henri Verdier (DINSIC) en passe d’être nommé ambassadeur de la France pour le numérique"
author: Xavier Berne
date: 2018-10-23
href: https://www.nextinpact.com/news/107207-henri-verdier-dinsic-en-passe-detre-nomme-ambassadeur-france-pour-numerique.htm
tags:
- Administration
- Institutions
- Open Data
---

> Selon nos informations, Henri Verdier, le numéro un de la Direction interministérielle au numérique, est en passe de devenir le nouvel ambassadeur de la France pour le numérique. Cette désignation pourrait être officialisée demain en Conseil des ministres.
