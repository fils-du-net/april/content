---
site: Next INpact
title: "Aux États-Unis, on peut contourner les DRM pour réparer des produits"
date: 2018-10-29
href: https://www.nextinpact.com/brief/aux-etat-unis--on-peut-contourner-les-drm-pour-reparer-des-produits-6245.htm
tags:
- Institutions
- Associations
- DRM
- International
---

> Dans une décision entrée en vigueur dimanche 28 octobre, la Librarian of Congress et l’U.S. Copyright Office ont élargi la liste des exemptions autorisant les utilisateurs à contourner les verrous numériques pour réparer une série de produits.
