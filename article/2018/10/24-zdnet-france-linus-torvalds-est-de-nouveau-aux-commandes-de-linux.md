---
site: ZDNet France
title: "Linus Torvalds est de nouveau aux commandes de Linux"
author: Steven J. Vaughan-Nichols
date: 2018-10-24
href: https://www.zdnet.fr/actualites/linus-torvalds-est-de-nouveau-aux-commandes-de-linux-39875531.htm
tags:
- Associations
- Innovation
---

> Après quelques semaines passées à l’écart afin de prendre du recul sur son rôle au sein de la communauté, Linus Torvalds est de retour.
