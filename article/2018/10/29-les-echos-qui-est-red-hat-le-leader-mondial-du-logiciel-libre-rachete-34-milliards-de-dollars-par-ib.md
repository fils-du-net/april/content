---
site: Les Echos
title: "Qui est Red Hat, le leader mondial du logiciel libre racheté 34 milliards de dollars par IBM?"
author: Nicolas Richaud
date: 2018-10-29
href: https://www.lesechos.fr/tech-medias/hightech/0600053291719-qui-est-red-hat-le-leader-mondial-du-logiciel-libre-rachete-34-milliards-de-dollars-par-ibm-2217501.php
tags:
- Entreprise
- Informatique en nuage
---

> Lancée au milieu des années 1990, la société a traversé la bulle Internet puis a su devenir la référence mondiale de l'open source. Son chiffre d'affaires augmente continuellement depuis 66 trimestres consécutifs.
