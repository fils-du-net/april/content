---
site: Télérama.fr
title: "L'Union européenne renonce à durcir l'exportation de technologies de surveillance"
author: Olivier Tesquet
date: 2018-10-29
href: https://www.telerama.fr/medias/lunion-europeenne-renonce-a-durcir-lexportation-de-technologies-de-surveillance,n5869532.php
tags:
- Entreprise
- Institutions
- Europe
- Vie privée
---

> Alors que l’Union européenne tente de renforcer le contrôle de l’exportation de technologies de surveillance depuis 2016, des documents internes au Conseil de l’UE transmis en exclusivité à Netzpolitik et Télérama par Reporters sans frontières montrent que plusieurs pays, au premier rang desquels la Suède et la Finlande, sont déterminés à saboter ces efforts. A sept mois des élections européennes, l’Allemagne et – surtout – la France semblent prêtes à accepter n’importe quel compromis, même s’il faut sacrifier les droits de l’homme sur l’autel des intérêts industriels.
