---
site: L'ADN
title: "L'économie collaborative: état des lieux"
author: Nastasia Hadjadji
date: 2018-10-22
href: https://www.ladn.eu/entreprises-innovantes/genie-collectif-edf/economie-collaborative-etat-des-lieux
tags:
- Économie
- Associations
- Innovation
- Sciences
---

> Où en est-on du modèle de société libre, ouvert et collaboratif promis par l’économie collaborative (sharing economy en anglais)?
