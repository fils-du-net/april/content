---
site: LeMagIT
title: "Microsoft partage 60 000 de ses brevets avec l'open source"
author: Cyrille Chausson
date: 2018-10-11
href: https://www.lemagit.fr/actualites/252450471/Microsoft-partage-60-000-de-ses-brevets-avec-lopen-source
tags:
- Entreprise
- Brevets logiciels
---

> L’éditeur de Redmond a rejoint l’Open Innovation Network et met dans la balance quelque 60 000 de ses brevets, qu’il partage avec la communauté open source. A travers ce geste, il prend part à l’évolution du modèle open source et de Linux. Finis les patents trolls.
