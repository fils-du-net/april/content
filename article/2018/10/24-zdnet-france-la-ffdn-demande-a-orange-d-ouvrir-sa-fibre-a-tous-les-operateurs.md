---
site: ZDNet France
title: "La FFDN demande à Orange d’ouvrir sa fibre à tous les opérateurs"
author: APM
date: 2018-10-24
href: https://www.zdnet.fr/actualites/la-ffdn-demande-a-orange-d-ouvrir-sa-fibre-a-tous-les-operateurs-39875515.htm
tags:
- Entreprise
- Internet
- Institutions
- Associations
---

> La Fédération des fournisseurs d'accès Internet associatifs s’inquiète de ne pas pouvoir accéder à la fibre des opérateurs, et suggère à Orange d’ouvrir largement son réseau FFTH via la mise en place d’une offre activée.
