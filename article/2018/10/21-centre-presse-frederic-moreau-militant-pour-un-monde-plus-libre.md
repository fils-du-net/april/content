---
site: Centre Presse
title: "Frédéric Moreau, militant pour un monde plus libre"
author: Bastien Lion
date: 2018-10-21
href: https://www.centre-presse.fr/article-634315-frederic-moreau-militant-pour-un-monde-plus-libre.html
tags:
- Administration
- Associations
- Promotion
---

> Secrétaire de l'Association poitevine pour la promotion des logiciels libres et de Linux, Frédéric Moreau défend un rapport différent aux outils numériques.
