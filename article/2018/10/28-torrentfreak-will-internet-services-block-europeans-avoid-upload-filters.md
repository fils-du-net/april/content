---
site: TorrentFreak
title: "Will Internet Services Block Europeans to Avoid ”Upload Filters”?"
author: Ernesto
date: 2018-10-28
href: https://torrentfreak.com/will-internet-services-block-europeans-to-avoid-upload-filters-181028
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> The EU's plans to modernize copyright law in Europe are moving forward, including the controversial Article 13. While supporters and opponents remain diametrically opposed, we take a look ahead. If Article 13 is implemented, will large websites block European visitors fearing potential liability for pirated content? (Le plan de modernisation du droit d'auteur en europe avance, incluant le controversé article 13)
