---
site: Developpez.com
title: "Richard Stallman adopte une alternative aux codes de conduite pour le projet GNU"
author: Michael Guilloux
date: 2018-10-23
href: https://www.developpez.com/actu/230329/Richard-Stallman-adopte-une-alternative-aux-codes-de-conduite-pour-le-projet-GNU-les-GNU-Kind-Communications-Guidelines
tags:
- Associations
- Innovation
---

> La saison des codes de conduite se confirme. Outre la communauté du noyau Linux et celle de SQLite, le projet GNU a également adopté des règles ou principes qui doivent régir les interactions de la communauté (mainteneurs et contributeurs) au quotidien. Mais Richard Stallman, initiateur du projet et président de la Free Software Foundation (FSF), a opté pour une approche qui vise à prévenir les écarts de conduite plutôt qu'à punir ceux qui en seront responsables.
