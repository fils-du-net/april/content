---
site: Le Monde.fr
title: "IBM achète l’éditeur de logiciels Red Hat pour 34 milliards de dollars"
date: 2018-10-28
href: https://www.lemonde.fr/entreprises/article/2018/10/28/ibm-achete-l-editeur-de-logiciels-red-hat-pour-34-milliards-de-dollars_5375838_1656994.html
tags:
- Entreprise
- Informatique en nuage
---

> Cette transaction, la plus importante acquisition jamais réalisée par le groupe américain, doit accélérer la présence du géant informatique sur le marché, très lucratif, du «cloud».
