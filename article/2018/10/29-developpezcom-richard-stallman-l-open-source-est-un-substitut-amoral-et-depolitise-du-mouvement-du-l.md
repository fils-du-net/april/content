---
site: Developpez.com
title: "Richard Stallman: «l'open source est un substitut amoral et dépolitisé du mouvement du logiciel libre», qui n'ose pas défendre la liberté"
author: Michael Guilloux
date: 2018-10-29
href: https://www.developpez.com/actu/231162/Richard-Stallman-l-open-source-est-un-substitut-amoral-et-depolitise-du-mouvement-du-logiciel-libre-qui-n-ose-pas-defendre-la-liberte
tags:
- Philosophie GNU
---

> Quelle est la différence entre l'open source et le mouvement du logiciel libre? Il y a trois ans, nous avons sur ce site essayé d'apporter des éclaircissements sur ces deux mouvements. L'open source est en effet toujours associé au logiciel libre et tous les deux sont opposés aux logiciels propriétaires. Ces termes sont même utilisés de manière interchangeable par bon nombre de personnes, mais ils restent deux concepts distincts.
