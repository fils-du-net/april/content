---
site: Le Monde.fr
title: "La Californie réinstaure la neutralité du Net, l’Etat fédéral l’attaque en justice"
date: 2018-10-01
href: https://www.lemonde.fr/pixels/article/2018/10/01/la-californie-retablit-la-neutralite-du-net-la-justice-poursuit_5362497_4408996.html
tags:
- Internet
- Institutions
- Neutralité du Net
- International
---

> L’administration de Donald Trump a lancé des poursuites judiciaires contre l’Etat californien après que celui-ci a fait passer une loi pour rétablir ce principe.
