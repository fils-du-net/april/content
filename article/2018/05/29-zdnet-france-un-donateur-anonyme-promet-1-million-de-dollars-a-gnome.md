---
site: ZDNet France
title: "Un donateur anonyme promet 1 million de dollars à Gnome"
author: Thierry Noisette
date: 2018-05-29
href: http://www.zdnet.fr/blogs/l-esprit-libre/un-donateur-anonyme-promet-1-million-de-dollars-a-gnome-39868817.htm
tags:
- Économie
- Associations
---

> Cette somme rondelette sera versée en deux ans à la fondation qui soutient l'environnement de bureau, notamment adopté pour les distributions Linux Ubuntu.
