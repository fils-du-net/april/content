---
site: ZDNet France
title: "Outils publics: la Dinsic publie son guide en matière d’ouverture du code"
author: Louis Adam
date: 2018-05-15
href: http://www.zdnet.fr/actualites/outils-publics-la-dinsic-publie-son-guide-en-matiere-d-ouverture-du-code-39868220.htm
tags:
- Administration
- Licenses
---

> Dans un document en ligne, la direction interministérielle des systèmes d’information et de communication de l’Etat détaille ses bonnes pratiques et recommandations en matière d’ouverture du code source des outils développés par et pour les administrations. Une mesure prévue par la loi République Numérique en 2016.
