---
site: Le Monde.fr
title: "De l’utopie au désenchantement, les vingt-cinq ans contrariés du Web"
author: William Audureau
date: 2018-05-09
href: http://www.lemonde.fr/pixels/article/2018/05/09/de-l-utopie-au-desenchantement-les-vingt-cinq-ans-contraries-du-web_5296713_4408996.html
tags:
- Entreprise
- Internet
- Partage du savoir
- Innovation
- Vie privée
---

> A l’occasion de la Web Conf, les pionniers de la Toile sont revenus avec émotion sur la genèse et l’essor d’Internet, cette révolution qui a fini par leur échapper.
