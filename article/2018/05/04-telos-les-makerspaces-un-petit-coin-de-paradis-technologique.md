---
site: telos
title: "Les makerspaces: un petit coin de paradis technologique?"
author: Monique Dagnaud
date: 2018-05-04
href: https://www.telos-eu.com/fr/societe/les-makerspaces-un-petit-coin-de-paradis-technolog.html
tags:
- Partage du savoir
- Innovation
---

> Dans un livre très documenté, Isabelle Berrebi-Hoffmann, Marie Christine Bureau et Michel Lallement nous convient à un voyage ethnographique dans les hackerspaces, les makerspaces et les fablabs, ces temples du bidouillage qui prolifèrent aujourd’hui en France. À l’issue de la lecture, une hésitation est permise: s’agit-il de laboratoires de l’innovation, leviers utiles à l’économie nouvelle, ou bien de pôles d’éducation populaire dédiés à l’art de la main et à finalité intégratrice?
