---
site: Le Journal de Montréal
title: "Montréal: une nouvelle politique sur les logiciels libres"
author: Sarah Daoust-Braun
date: 2018-05-15
href: http://www.journaldemontreal.com/2018/05/15/montreal--une-nouvelle-politique-sur-les-logiciels-libres
tags:
- Administration
- International
---

> La Ville de Montréal se dote d’une nouvelle politique sur l’utilisation et le développement de logiciels et de matériels libres sur 10 ans, pour se libérer à terme des licences et du menottage informatique.
