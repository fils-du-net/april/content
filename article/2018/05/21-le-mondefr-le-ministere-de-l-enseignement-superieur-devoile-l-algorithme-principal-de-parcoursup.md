---
site: Le Monde.fr
title: "Le ministère de l’enseignement supérieur dévoile l’algorithme principal de Parcoursup"
author: Soazig Le Nevé
date: 2018-05-21
href: https://www.lemonde.fr/campus/article/2018/05/21/le-ministere-de-l-enseignement-superieur-devoile-l-algorithme-principal-de-parcoursup_5302387_4401467.html
tags:
- Internet
- Administration
- Économie
- Éducation
---

> Le code informatique de la plate-forme d’admission a été rendu public, lundi, mais les algorithmes locaux élaborés par les universités restent, eux, secrets.
