---
site: Le Journal du CM
title: "Lumière sur la licence Creative Commons"
author: Guillaume Guersan
date: 2018-05-23
href: https://www.journalducm.com/licence-creative-commons
tags:
- Droit d'auteur
- Licenses
---

> La licence Creative Commons vise à simplifier l'utilisation des œuvres diffusées sur Internet et à développer l'enrichissement du patrimoine commun. Je vais vous expliquer dans cet article, tout ce que vous devez savoir sur la licence Creative Commons.
