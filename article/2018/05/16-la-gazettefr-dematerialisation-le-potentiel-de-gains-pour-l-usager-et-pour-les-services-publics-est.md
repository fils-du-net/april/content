---
site: La gazette.fr
title: "Dématérialisation: «Le potentiel de gains pour l’usager, et pour les services publics est extraordinaire»"
author: Gabriel Zignani Romain Mazon
date: 2018-05-16
href: http://www.lagazettedescommunes.com/563650/dematerialisation-le-potentiel-de-gains-pour-lusager-et-pour-les-services-publics-est-extraordinaire
tags:
- Administration
- Institutions
- Sensibilisation
- Marchés publics
- Open Data
---

> Henri Verdier, directeur interministériel du numérique et du système d'information de l'Etat français est au coeur de la transformation numérique du secteur public, jusqu'à l'opendata, puisqu'il est également administrateur général des données (AGD). La Gazette l'a longuement interviewé, sur l'ensemble des dossiers à l'ordre du jour des collectivités. Premier volet d'une série qui en compte trois: la dématérialisation des services publics, et l'administration numérique.
