---
site: Le Monde Informatique
title: "La DINSIC clarifie sa politique de contribution vis-à-vis du logiciel libre"
author: Bertrand Lemaire
date: 2018-05-17
href: https://www.lemondeinformatique.fr/actualites/lire-la-dinsic-clarifie-sa-politique-de-contribution-vis-a-vis-du-logiciel-libre-71761.html
tags:
- Administration
- Licenses
---

> La direction interministérielle du numérique et du système d'information et de communication (DINSIC) a précisé les règles de l'ouverture du code des logiciels conçus pour l'Etat. Les agents publics pourront contribuer aux logiciels libres sur leur temps de travail.
