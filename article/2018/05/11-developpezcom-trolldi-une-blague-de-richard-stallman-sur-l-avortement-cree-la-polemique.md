---
site: Developpez.com
title: "Trolldi: une blague de Richard Stallman sur l'avortement crée la polémique"
author: Michael Guilloux
date: 2018-05-11
href: https://www.developpez.com/actu/203261/Trolldi-une-blague-de-Richard-Stallman-sur-l-avortement-cree-la-polemique-26-ans-apres-avoir-ete-ecrite-dans-la-documentation-du-projet-glibc
tags:
- Sensibilisation
---

> Le saviez-vous? Richard Matthew Stallman (RMS), s'il est souvent qualifié d'extrémiste avec sa position et ses déclarations radicales à l'égard de tout ce qu'il considère comme privateur, a aussi un sens de l'humour. Et il le cultive depuis des décennies. Mais ici en 2018, certains contributeurs du projet GNU ne semblent pas bien accueillir le sens de l'humour dont fait preuve l'initiateur du mouvement du logiciel libre et chef suprême du projet.
