---
site: Programmez!
title: "Mozilla publie un rapport pour faciliter la mise en place de projets"
author: Frederic Mazue
date: 2018-05-16
href: https://www.programmez.com/actualites/open-source-mozilla-publie-un-rapport-pour-faciliter-la-mise-en-place-de-projets-27561
tags:
- Sensibilisation
- Associations
---

> Mozilla et l’Open Tech Strategies annoncent la publication d'un rapport destiné à mieux comprendre le fonctionnement d’un projet Open Source, depuis la stratégie jusqu’à la gouvernance, en passant par leur fonctionnement au quotidien. Le rapport repose sur la stratégie “Open by Design” de Mozilla, qui vise à optimiser le développement et l’impact de projets technologiques collaboratifs, à l’aide des communautés et autres parties prenantes extérieures.
