---
site: ZDNet France
title: "Montréal précise son engagement pour le logiciel libre"
author: Thierry Noisette
date: 2018-05-21
href: http://www.zdnet.fr/blogs/l-esprit-libre/montreal-precise-son-engagement-pour-le-logiciel-libre-39868458.htm
tags:
- Administration
- International
---

> La métropole canadienne précise son choix de modernisation avec les logiciels libres, "plus adaptables aux besoins réels d’une organisation comme la nôtre".
