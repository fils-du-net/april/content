---
site: Developpez.com
title: "L'IoT est un danger pour la vie privée, une technologie à risque accordant peu de place aux logiciels libres"
author: Christian Olivier
date: 2018-05-07
href: https://www.developpez.com/actu/201842/L-IoT-est-un-danger-pour-la-vie-privee-une-technologie-a-risque-accordant-peu-de-place-aux-logiciels-libres-d-apres-Richard-Stallman
tags:
- Internet
- Vie privée
---

> Le citoyen américain Richard Matthew Stallman, président de la Free Software Foundation (FSF) et pionnier du GNU, est un fervent partisan du logiciel libre et des valeurs qui s’y rattachent. C’est aussi un critique acerbe, à la limite extrémiste, de l’industrie technologique profitant de chaque occasion qui lui est offerte pour donner son point de vue sur les nouveautés, les tendances et les autres faits marquants qui s’y rapportent.
