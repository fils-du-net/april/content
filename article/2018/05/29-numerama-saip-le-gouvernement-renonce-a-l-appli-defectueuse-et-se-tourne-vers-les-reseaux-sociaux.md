---
site: Numerama
title: "SAIP: le gouvernement renonce à l'appli défectueuse et se tourne vers les réseaux sociaux"
author: Julien Lausson
date: 2018-05-29
href: https://www.numerama.com/politique/380494-saip-le-gouvernement-renonce-a-lappli-defectueuse-et-se-tourne-vers-les-reseaux-sociaux.html
tags:
- Internet
- Logiciels privateurs
- Institutions
- Vie privée
---

> Faute pour l'application SAIP d'avoir pu faire ses preuves, le gouvernement change d'approche. Désormais, sa communication va se fonder sur les réseaux sociaux. Facebook, Google et Twitter sont dans la boucle.
