---
site: Developpez.com
title: "Google et Facebook sous le coup de 4 accusations dans 4 pays pour avoir enfreint le RGPD"
author: Stéphane le Calme
date: 2018-05-26
href: https://www.developpez.com/actu/205840/Google-et-Facebook-sous-le-coup-de-4-accusations-dans-4-pays-pour-avoir-enfreint-le-RGPD-quelques-heures-seulement-apres-son-entree-en-vigueur
tags:
- Entreprise
- Internet
- Institutions
- Informatique en nuage
- Europe
- Vie privée
---

> Google, Facebook, WhatsApp et Instagram sont accusées de forcer les utilisateurs à consentir à une publicité ciblée afin de pouvoir utiliser leurs services.
