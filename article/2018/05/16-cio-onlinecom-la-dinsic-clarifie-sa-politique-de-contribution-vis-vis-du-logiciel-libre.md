---
site: "cio-online.com"
title: "La DINSIC clarifie sa politique de contribution vis-à-vis du logiciel libre"
author: Bertrand Lemaire
date: 2018-05-16
href: https://www.cio-online.com/actualites/lire-la-dinsic-clarifie-sa-politique-de-contribution-vis-a-vis-du-logiciel-libre-10331.html
tags:
- Administration
---

> La DINSIC (Direction interministérielle du numérique et du système d'information et de communication), la « DSI groupe » de l'Etat, vient de publier des textes de référence sur la politique de contribution open-source de l'Etat. Si, depuis la Loi pour une République Numérique du 7 octobre 2016, les codes sources conçus par des agents publics sont des documents administratifs communicables et réutilisables, les modalités pratiques restaient grandement à définir. C'était notamment le cas pour le travail des agents publics amenés à contribuer à des logiciels libres pré-existants.
