---
site: Studyrama.com
title: "Parcoursup: publication du code informatique des algorithmes"
date: 2018-05-22
href: http://www.studyrama.com/parcoursup/parcoursup-publication-du-code-informatique-des-104656
tags:
- Internet
- Administration
- Éducation
---

> Comme ils s’y étaient engagés, Frédérique Vidal, Ministre de l’enseignement supérieur, de la recherche et de l’innovation et Mounir Mahjoubi, Secrétaire d’Etat en charge du Numérique, ont souhaité que soit rendu public ce jour le code informatique du cœur algorithmique de la plateforme Parcoursup, utilisé pour déterminer quotidiennement les propositions d’admission à adresser aux candidats.
