---
site: Les Yeux du Monde
title: "La bataille pour le logiciel libre en Afrique"
author: Loup Viallet
date: 2018-05-25
href: http://les-yeux-du-monde.fr/ressources/35564-la-bataille-pour-le-logiciel-libre-en-afrique-par-loup-viallet
tags:
- Entreprise
- Économie
- Institutions
- Associations
- Promotion
- International
---

> Aujourd’hui l’Afrique demeure sous-équipée dans presque tous les domaines et les nouvelles technologies ne font pas exception à la règle: elles sont d’abord développées dans les pays industrialisés. Aussi le logiciel libre représente en Afrique une aide au développement, pour les entreprises comme pour les particuliers. C’est un transfert de technologie à la fois très économique et très rentable, peu coûteux, très adaptable et très utile pour démarrer une activité ou améliorer des services. Il intéresse beaucoup de monde.
