---
site: World Socialist Web Site
title: "Il y a 25 ans: Le code source du World Wide Web était rendu public"
date: 2018-05-02
href: https://www.wsws.org/fr/articles/2018/05/02/3w25-m02.html
tags:
- Internet
- Sensibilisation
- Philosophie GNU
- Standards
---

> Le 30 avril 1993, le CERN (l’Organisation européenne pour la recherche nucléaire) rendait le code source du World Wide Web domaine public et libre de droits de licence. Tim Berners-Lee, qui créa le web en 1989, exhorta le CERN à distribuer son invention en code ouvert (open source). On considère que cette décision marque la véritable date de naissance du World Wide Web et est la raison fondamentale qui a permis son rapide développement international.
