---
site: 20 minutes online
title: "Cambridge Analytica annonce son sabordage"
date: 2018-05-02
href: http://www.20min.ch/ro/news/monde/story/Cambridge-Analytica-cesse-ses-activites-21915914
tags:
- Entreprise
- Internet
- Vie privée
---

> La société britannique, au coeur du scandale sur les données des utilisateurs de Facebook, a entamé une procédure d'insolvabilité.
