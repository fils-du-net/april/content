---
site: Association mode d'emploi
title: "L’éducation populaire se réinvente"
author: Michel Lulek
date: 2018-05-15
href: https://www.associationmodeemploi.fr/article/l-education-populaire-se-reinvente.64234
tags:
- Associations
- Éducation
---

> Traditionnellement ancrée dans la culture, les sports et la jeunesse, l’éducation populaire a également investi de nouvelles thématiques. Elle est en particulier très ­présente dans le champ du numérique autour du mouvement des logiciels libres, des données personnelles et plus globalement de ce qu’on appelle les communs.
