---
site: Commentçamarche.net
title: "Des ateliers pour préparer la fête des libertés numériques"
author: Perrine Tiberghien
date: 2018-05-23
href: https://www.commentcamarche.net/news/5871478-ateliers-fete-libertes-numeriques
tags:
- Internet
- Associations
- Europe
- Vie privée
---

> Ces dernières semaines, l’info fleurit dans les boîtes mail. Les entreprises se préparent à la mise en place du règlement général sur la protection des données personnelles, le RGPD.
> Le sujet semble obscur, mais il est néanmoins important. Et pour bien s’y retrouver, le site dédié à la Fête des Libertés numériques propose outils, infos pratiques et ateliers.
