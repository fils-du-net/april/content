---
site: nonfiction.fr
title: "Entretien à propos de «Makers», les laborantins du changement social"
author: Jean Bastien
date: 2018-05-29
href: https://www.nonfiction.fr/article-9406-entretien-a-propos-de-makers-les-laborantins-du-changement-social.htm
tags:
- Matériel libre
- Institutions
- Associations
- Innovation
---

> Les fablabs et autres hackerspaces retiennent de plus en plus l’attention: seraient-ils des laboratoires du changement social?
