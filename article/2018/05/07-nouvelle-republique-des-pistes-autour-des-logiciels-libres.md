---
site: Nouvelle République
title: "Des pistes autour des logiciels libres"
date: 2018-05-07
href: https://www.lanouvellerepublique.fr/indre-et-loire/commune/saint-paterne-racan/des-pistes-autour-des-logiciels-libres
tags:
- Administration
- Associations
- Promotion
---

> Le centre multimédia de Saint-Paterne-Racan était en effervescence lors de la foire du 1er mai
