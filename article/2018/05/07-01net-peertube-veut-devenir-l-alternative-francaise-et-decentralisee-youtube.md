---
site: 01net.
title: "PeerTube veut devenir l'alternative française et décentralisée à YouTube"
author: Camille Suard
date: 2018-05-07
href: http://www.01net.com/actualites/peertube-veut-devenir-l-alternative-francaise-et-decentralisee-a-youtube-1439372.html
tags:
- Entreprise
- Internet
- Partage du savoir
- Associations
- Vie privée
---

> Le YouTube français et décentralisé propulsé par Framasoft entend s'attaquer au géant Google. PeerTube est un logiciel libre actuellement disponible en version bêta ouverte au public.
