---
site: La gazette.fr
title: "Logiciel libre: conseils pratiques et bonnes conduites"
author: Gabriel Zignani
date: 2018-05-31
href: http://www.lagazettedescommunes.com/566715/logiciel-libre-conseils-pratiques-et-bonnes-conduites
tags:
- Administration
- Sensibilisation
---

> La Dinsic a publié un document relatif à la politique de contribution aux logiciels libres de l'Etat, visant les agents publics. Les collectivités sont incitées à s'y référer librement et à l'adapter à leurs besoins.
