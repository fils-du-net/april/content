---
site: ZDNet France
title: "Directive droit d'auteur vs logiciels libres: une députée interroge la ministre de la Culture"
author: Thierry Noisette
date: 2018-02-06
href: http://www.zdnet.fr/blogs/l-esprit-libre/directive-droit-d-auteur-vs-logiciels-libres-une-deputee-interroge-la-ministre-de-la-culture-39863766.htm
tags:
- April
- Institutions
- Droit d'auteur
- Europe
---

> La députée LFI Sabine Rubin pointe les dangers pour le développement des logiciels libres du projet de réforme de la directive droit d'auteur, et demande à Françoise Nyssen comment seront protégées les forges logicielles.
