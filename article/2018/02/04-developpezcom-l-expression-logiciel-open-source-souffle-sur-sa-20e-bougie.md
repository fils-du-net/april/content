---
site: Developpez.com
title: "L'expression «logiciel open source» souffle sur sa 20e bougie"
author: Stéphane le calme
date: 2018-02-04
href: https://www.developpez.com/actu/186243/L-expression-logiciel-open-source-souffle-sur-sa-20e-bougie-la-cofondatrice-du-Foresight-Institute-en-raconte-la-genese
tags:
- Philosophie GNU
- Promotion
---

> Le 3 février a sonné le 20e anniversaire de l'expression «logiciel open source», qui a été adoptée par le directeur exécutif du Foresight Institute, un groupe de réflexion à but non lucratif axé sur la nanotechnologie et l'intelligence artificielle.
