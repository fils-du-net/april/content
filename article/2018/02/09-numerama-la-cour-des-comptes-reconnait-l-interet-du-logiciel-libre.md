---
site: Numerama
title: "La Cour des comptes reconnaît l'intérêt du logiciel libre"
author: Julien Lausson
date: 2018-02-09
href: https://www.numerama.com/politique/328216-la-cour-des-comptes-reconnait-linteret-du-logiciel-libre.html
tags:
- Administration
- April
- Institutions
- Promotion
---

> Dans son rapport public annuel 2018, la Cour des comptes a reconnu l'intérêt du logiciel libre, décrit comme un «puissant facteur d’efficience et d’influence» mais aussi une façon de répondre à un «enjeu de sécurité et de souveraineté».
