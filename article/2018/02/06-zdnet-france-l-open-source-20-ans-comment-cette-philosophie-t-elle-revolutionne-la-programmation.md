---
site: ZDNet France
title: "L’Open Source a 20 ans: comment cette philosophie a-t-elle révolutionné la programmation?"
author: Steven J. Vaughan-Nichols
date: 2018-02-06
href: http://www.zdnet.fr/actualites/l-open-source-a-20-ans-comment-cette-philosophie-a-t-elle-revolutionne-la-programmation-39863738.htm
tags:
- Sensibilisation
- Associations
- Philosophie GNU
---

> «La récompense d’une victoire est souvent une nouvelle série de batailles.»
