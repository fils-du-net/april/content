---
site: France Inter
title: "Que reste-t-il de l'internet libre?"
author: Christine Siméone
date: 2018-02-17
href: https://www.franceinter.fr/culture/l-internet-libre-est-mort-vive-l-internet-libre-barlow-independance
tags:
- Entreprise
- Internet
- Institutions
---

> John Perry Barlow a défendu l'idée d'un internet libertaire, utopique. A l'heure des fake news, du dark net, et de la censure en Chine ou ailleurs, sa déclaration d'indépendance du cyberespace est réécrite par Olivier Ertzscheid, chercheur français en sciences de l'information.
