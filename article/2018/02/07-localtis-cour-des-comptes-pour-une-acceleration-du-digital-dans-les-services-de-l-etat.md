---
site: Localtis
title: "Cour des comptes - Pour une accélération du digital dans les services de l'Etat"
author: Edgar Brault
date: 2018-02-07
href: https://www.caissedesdepotsdesterritoires.fr/cs/ContentServer?pagename=Territoires/Articles/Articles$urlcid=1250280542054
tags:
- Administration
- Économie
- Institutions
- Innovation
- Promotion
- Open Data
---

> Dans son rapport annuel publié ce 7 février, la Cour des comptes reconnaît les acquis de la stratégie innovante “d'Etat-plateforme” menée par la direction interministérielle du numérique et du système d'information et de communication (Dinsic) en matière de mutualisations, d’optimisation des ressources et de décloisonnement. Elle se montre plus réservée sur la diffusion des compétences digitales dans l’administration, “au-delà des sphères spécialisées”.
