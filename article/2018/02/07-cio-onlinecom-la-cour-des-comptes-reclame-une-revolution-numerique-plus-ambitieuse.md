---
site: "cio-online.com"
title: "La Cour des Comptes réclame une révolution numérique plus ambitieuse"
author: Bertrand Lemaire
date: 2018-02-07
href: https://www.cio-online.com/actualites/lire-la-cour-des-comptes-reclame-une-revolution-numerique-plus-ambitieuse-10079.html
tags:
- Administration
- Économie
- April
- Institutions
- Innovation
- Promotion
- Open Data
---

> Qu'il s'agisse du compteur communiquant Linky ou de l'audit de la DINSIC, le rapport annuel de la Cour des Comptes s'affiche critique tout en réclamant de l'ambition dans le domaine du numérique.
