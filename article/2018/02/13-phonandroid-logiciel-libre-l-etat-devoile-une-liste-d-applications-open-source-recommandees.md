---
site: PhonAndroid
title: "Logiciel libre: l’Etat dévoile une liste d’applications open source recommandées"
author: Christelle Perret
date: 2018-02-13
href: http://www.phonandroid.com/firefox-vlc-qwant-liste-logiciels-libres-recommandes-par-etat-2018.html
tags:
- Administration
- Promotion
- RGI
---

> L’Etat dévoile la liste des logiciels libres et applications open sources recommandés en 2018. Cette liste est principalement destinée aux services administratifs. Mais de nombreuses recommandations qui y figurent peuvent convenir au grand public. On y trouve ainsi des services de messagerie, navigation internet, multimédia ou encore bureautique.
