---
site: "cio-online.com"
title: "SILL 2018: l'Etat met à jour son référentiel de logiciels libres"
author: Bertrand Lemaire
date: 2018-02-13
href: https://www.cio-online.com/actualites/lire-sill-2018-l-etat-met-a-jour-son-referentiel-de-logiciels-libres-10091.html
tags:
- Administration
- Promotion
- RGI
---

> Le SILL 2018 (Socle Interministériel de Logiciels Libres) vient de paraître pour uniformiser au mieux les SI des administrations.
