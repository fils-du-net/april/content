---
site: Nouvelle République
title: "Ubuntu accessible à tous"
date: 2018-02-27
href: https://www.lanouvellerepublique.fr/indre-et-loire/commune/la-riche/ubuntu-accessible-a-tous
tags:
- Administration
- Associations
- Promotion
---

> Voilà un logiciel libre qui a séduit son public! Samedi 17 février à Équinoxe, la troisième édition de Unbuntu Party, organisée par la municipalité, a attiré un nombreux public, toutes générations confondues, intéressées par l’utilisation de ce logiciel.
