---
site: "Alternatives-economiques"
title: "Isabelle Delannoy: «L’économie symbiotique produit une autre croissance»"
author: Catherine André
date: 2018-02-06
href: https://www.alternatives-economiques.fr/isabelle-delannoy-leconomie-symbiotique-produit-une-croissance/00082940
tags:
- Internet
- Économie
---

> Théoricienne du concept d’«économie symbiotique», Isabelle Delannoy décrypte les voies d’une autre croissance, qui mise sur la qualité plus que sur la quantité
