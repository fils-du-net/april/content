---
site: ZDNet France
title: "Pourquoi l’open source est si important pour Microsoft"
author: Louis Adam
date: 2018-02-28
href: http://www.zdnet.fr/actualites/pourquoi-l-open-source-est-si-important-pour-microsoft-39864770.htm
tags:
- Entreprise
---

> Le CTO Data de Microsoft nous a parlé de l’importance croissante de l’open source, sachant que Microsoft est aujourd’hui l’un de ses plus gros contributeurs.
