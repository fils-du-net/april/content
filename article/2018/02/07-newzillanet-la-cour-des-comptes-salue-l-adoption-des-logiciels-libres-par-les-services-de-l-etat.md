---
site: NewZilla.net
title: "La Cour des comptes salue l’adoption des logiciels libres par les services de l’Etat"
author: Philippe Crouzillacq
date: 2018-02-07
href: https://www.newzilla.net/2018/02/07/la-cour-des-comptes-salue-ladoption-des-logiciels-libres-par-les-services-de-letat
tags:
- Administration
- April
- Institutions
- Promotion
---

> Le logiciel libre comme “élément moteur de la modernisation des administrations publiques”? Pour la Cour des comptes, c’est une réalité.
