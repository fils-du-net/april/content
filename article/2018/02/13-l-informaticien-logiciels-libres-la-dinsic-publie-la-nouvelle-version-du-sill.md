---
site: L'Informaticien
title: "Logiciels libres: la DINSIC publie la nouvelle version du SILL"
author: Bertrand Garé
date: 2018-02-13
href: https://www.linformaticien.com/actualites/id/48376/logiciels-libres-la-dinsic-publie-la-nouvelle-version-du-sill.aspx
tags:
- Administration
- Promotion
- RGI
---

> La DINSIC (Direction Interministérielle des Systèmes d’Information et de communication  de l’Etat) vient de publier la nouvelle version du SILL (Socle Interministériel des logiciels libres). 
