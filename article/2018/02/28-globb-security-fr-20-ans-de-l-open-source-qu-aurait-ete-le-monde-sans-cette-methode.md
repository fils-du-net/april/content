---
site: Globb Security FR
title: "20 ans de l'Open Source: qu'aurait été le monde sans cette méthode?"
author: Gerald Pfeifer
date: 2018-02-28
href: http://globbsecurity.fr/20-ans-de-lopen-source-quaurait-ete-monde-cette-methode-43610
tags:
- Entreprise
- Innovation
- Standards
---

> Le terme «open source» est souvent associé à des logiciels accessibles à tous en tant que code source, gratuitement, à tout moment, et pratiquement sans restrictions. Sous sa forme originale, c’était vrai. Mais le concept d’open source est désormais bien plus vaste. L’open source fait partie de notre quotidien, sans que l’on s’en rende compte: des sites collaboratifs tels que Wikipedia, des plans de hardware, mais il est aussi à la base des systèmes d’exploitation Android ou iOS.
