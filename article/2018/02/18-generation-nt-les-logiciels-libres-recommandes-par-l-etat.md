---
site: "Génération-NT"
title: "Les logiciels libres recommandés par l'État"
author: Jérôme G.
date: 2018-02-18
href: https://www.generation-nt.com/logiciel-libre-etat-administration-sill-2018-actualite-1950978.html
tags:
- Administration
- Promotion
- RGI
---

> La mise à jour annuelle du SILL a été publiée. Les logiciels libres recommandés par l'État pour les administrations.
