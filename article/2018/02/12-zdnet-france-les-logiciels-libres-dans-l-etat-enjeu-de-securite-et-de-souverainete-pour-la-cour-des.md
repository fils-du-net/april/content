---
site: ZDNet France
title: "Les logiciels libres dans l'Etat, \"enjeu de sécurité et de souveraineté\" pour la Cour des comptes"
author: Thierry Noisette
date: 2018-02-12
href: http://www.zdnet.fr/blogs/l-esprit-libre/les-logiciels-libres-dans-l-etat-enjeu-de-securite-et-de-souverainete-pour-la-cour-des-comptes-39864024.htm
tags:
- Administration
- April
- Institutions
- Promotion
- Open Data
---

> Le rapport annuel de la Cour des comptes souligne que "le partage de développements libres permet d'étendre la portée des mutualisations" et pointe "un facteur d'efficience et d'influence".
