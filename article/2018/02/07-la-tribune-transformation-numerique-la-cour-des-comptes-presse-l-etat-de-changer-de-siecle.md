---
site: La Tribune
title: "Transformation numérique: la Cour des Comptes presse l’Etat de «changer de siècle»"
author: Sylvain Rolland
date: 2018-02-07
href: https://www.latribune.fr/technos-medias/transformation-numerique-la-cour-des-comptes-presse-l-etat-de-changer-de-siecle-767483.html
tags:
- Administration
- Économie
- Institutions
- Innovation
- Promotion
- Open Data
---

> Dans son rapport public annuel publié ce mercredi, la Cour des comptes encourage l'État à accélérer le déploiement de la stratégie d'État plateforme et de former massivement l'ensemble des agents publics aux compétences numériques. La condition sine qua non pour acter le "changement de siècle" voulu par Emmanuel Macron.
