---
site: LeMagIT
title: "Modernisation de l’Etat: la Cour des comptes ré-affirme le rôle clé des logiciels libres"
author: Cyrille Chausson
date: 2018-02-15
href: http://www.lemagit.fr/actualites/252435154/Modernisation-de-lEtat-la-Cour-des-comptes-re-affirme-le-role-cle-des-logiciels-libres
tags:
- Administration
- Institutions
- Promotion
---

> Moteurs de la mutualisation souhaitée pour l’Etat Plateforme, enjeu de sécurité et de souveraineté, la Cour des comptes confirme le rôle de pilier du Libre dans la numérisation de l’Etat.
