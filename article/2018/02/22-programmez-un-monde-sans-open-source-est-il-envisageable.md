---
site: Programmez!
title: "Un monde sans open source est-il envisageable?"
author: Gerald Pfeifer
date: 2018-02-22
href: https://www.programmez.com/avis-experts/un-monde-sans-open-source-est-il-envisageable-27171
tags:
- Sensibilisation
- Innovation
- Sciences
- Informatique en nuage
---

> Le terme «open source» est souvent associé à des logiciels accessibles à tous en tant que code source, gratuitement, à tout moment, et pratiquement sans restrictions. Sous sa forme originale, c’était vrai. Mais le concept d’open source est désormais bien plus vaste. L’open source fait partie de notre quotidien, sans que l’on s’en rende compte: des sites collaboratifs tels que Wikipedia, des plans de hardware, mais il est aussi à la base des systèmes d’exploitation Android ou iOS.
