---
site: Numerama
title: "Une députée s'alarme des effets néfastes de la réforme du droit d'auteur sur le logiciel libre"
author: Julien Lausson
date: 2018-02-07
href: https://www.numerama.com/politique/327492-une-deputee-salarme-des-effets-nefastes-de-la-reforme-du-droit-dauteur-sur-le-logiciel-libre.html
tags:
- April
- Institutions
- Droit d'auteur
- Europe
---

> Une députée de la France Insoumise interpelle la ministre de la culture Françoise Nyssen sur un article de la proposition de directive européenne sur le droit d'auteur. Elle estime que les dispositions de ce texte vont nuire à la communauté du logiciel libre. Et donc à la sécurité et au développement du numérique.
