---
site: ZDNet France
title: "Le marché mondial de l'open source triplera de 2017 à 2022"
author: Thierry Noisette
date: 2018-02-28
href: http://www.zdnet.fr/blogs/l-esprit-libre/le-marche-mondial-de-l-open-source-triplera-de-2017-a-2022-39864810.htm
tags:
- Entreprise
- Économie
---

> C’est l’estimation d’une étude, qui prévoit une croissance annuelle de près de 24%, avec l’Amérique du Nord en tête.
