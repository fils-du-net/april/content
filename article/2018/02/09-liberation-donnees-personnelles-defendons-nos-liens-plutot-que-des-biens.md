---
site: Libération
title: "Données personnelles: défendons nos liens plutôt que des «biens»!"
author: Lionel Maurel
date: 2018-02-09
href: http://www.liberation.fr/debats/2018/02/09/donnees-personnelles-defendons-nos-liens-plutot-que-des-biens_1628598
tags:
- Internet
- Économie
- Vie privée
---

> Mettre en place un droit de propriété sur les données produites reviendrait à établir un rapport féodal entre les utilisateurs et les plateformes. Il faut au contraire privilégier une approche collective qui puiserait dans le riche héritage des droits sociaux.
