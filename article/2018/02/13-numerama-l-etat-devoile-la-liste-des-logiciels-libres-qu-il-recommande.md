---
site: Numerama
title: "L'État dévoile la liste des logiciels libres qu'il recommande"
author: Julien Lausson
date: 2018-02-13
href: https://www.numerama.com/tech/328962-letat-devoile-la-liste-des-logiciels-libres-quil-recommande.html
tags:
- Administration
- Promotion
- RGI
---

> Nouvelle année, nouvelle édition des logiciels libres recommandés par les services de l'État. Une liste globalement similaire à celle publiée l'année dernière, mais avec quelques changements.
