---
site: Siècle Digital
title: "Barcelone délaisse Microsoft pour les logiciels open source"
author: Arnaud Verchère
date: 2018-01-16
href: https://siecledigital.fr/2018/01/16/barcelone-delaisse-microsoft-logiciels-open-source
tags:
- Administration
- Innovation
- International
---

> La ville de Barcelone souhaite retrouver sa souveraineté technologique en rompant les contrats avec les logiciels propriétaires comme Microsoft pour développer les siens et les rendre accessibles à d'autres villes comme aux citoyens.
