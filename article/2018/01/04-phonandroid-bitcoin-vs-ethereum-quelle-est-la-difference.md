---
site: PhonAndroid
title: "Bitcoin vs Ethereum: quelle est la différence?"
author: Romain Pomian-Bonnemaison
date: 2018-01-04
href: http://www.phonandroid.com/bitcoin-vs-ethereum-difference.html
tags:
- Économie
- Innovation
---

> Le Bitcoin fait beaucoup parler de lui depuis quelques temps, et dans son sillage d’autres cryptomonnaies, parfois appelées altcoins comme l’Ethereum. La multiplicité de ces monnaies virtuelles peut créer de la confusion, surtout lorsque l’on ne regarde pas de près ce qui les différencie. Le Bitcoin créé en 2009 a démontré la puissance du système du blockchain pour gérer un système complexe de transactions sécurisées. L’Ethereum, créé en 2015, s’est construit sur le succès du blockchain, mais n’exploite plus uniquement sa puissance pour réaliser des transactions financières.
