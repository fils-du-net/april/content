---
site: "ouest-france.fr"
title: "Gaël Duval lance un smartphone libre respectueux de nos données"
author: Bastien Bocquel
date: 2018-01-08
href: http://jactiv.ouest-france.fr/actualites/multimedia/gael-duval-lance-smartphone-libre-respectueux-nos-donnees-82745
tags:
- Économie
- Innovation
---

> Pionnier de Linux, ce Caennais poursuit sa lutte pour protéger les données numériques personnelles. Il espère commercialiser son smartphone libre Eelo en 2019. Entretien.
