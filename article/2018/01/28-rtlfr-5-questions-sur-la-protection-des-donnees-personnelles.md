---
site: RTL.fr
title: "5 questions sur la protection des données personnelles"
author: Benjamin Hue
date: 2018-01-28
href: http://www.rtl.fr/actu/futur/5-questions-sur-la-protection-des-donnees-personnelles-7792001103
tags:
- Entreprise
- Internet
- Institutions
- Europe
- Vie privée
---

> Célébrée tous les 28 janvier, la journée mondiale de la protection des données personnelles vise à sensibiliser les internautes à la nécessité de protéger leur vie privée en ligne. Cette année revêt une importance particulière. Une nouvelle législation européenne entre en vigueur le 25 mai.
