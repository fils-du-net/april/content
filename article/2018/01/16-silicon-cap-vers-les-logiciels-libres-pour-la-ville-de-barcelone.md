---
site: Silicon
title: "Cap vers les logiciels libres pour la ville de Barcelone"
author: La rédaction
date: 2018-01-16
href: https://www.silicon.fr/logiciels-libres-barcelone-196379.html
tags:
- Administration
- International
---

> Un chantier IT d'envergure est enclenché à Barcelone. Objectif: remplacer les outils Microsoft par des logiciels libres: Ubuntu, Open-Xchange, Firefox et LibreOffice.
