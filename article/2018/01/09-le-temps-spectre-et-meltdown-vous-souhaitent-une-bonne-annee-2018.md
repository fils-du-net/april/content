---
site: Le Temps
title: "Spectre et Meltdown vous souhaitent une bonne année 2018!"
author: Solange Ghernaouti
date: 2018-01-09
href: https://www.letemps.ch/opinions/2018/01/09/spectre-meltdown-souhaitent-une-bonne-annee-2018
tags:
- Entreprise
- Innovation
---

> Les vulnérabilités informatiques avouées récemment par les producteurs de microprocesseurs représentent une révélation capitale. Elle met en évidence la fragilité du monde numérique sur lequel nous bâtissons notre société, estime Solange Ghernaouti, experte en cybersécurité
