---
site: PhonAndroid
title: "Android: comment ne plus être pisté par Google?"
author: Romain Pomian-Bonnemaison
date: 2018-01-12
href: http://www.phonandroid.com/android-comment-piste-google.html
tags:
- Entreprise
- Vie privée
---

> Vous vous méfiez du pistage sur votre smartphone Android et souhaitez réduire votre dépendance aux services de Google? Nous allons voir dans ce guide pourquoi des données sont transmises sur les serveurs de Google (et les serveurs de votre constructeur), comment désactiver une partie du pistage via votre compte Google, et vous montrer des alternatives, aussi bien en termes de ROM que d’applications et services Google!
