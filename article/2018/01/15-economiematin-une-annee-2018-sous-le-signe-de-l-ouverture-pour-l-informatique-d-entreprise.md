---
site: EconomieMatin
title: "Une année 2018 sous le signe de l'ouverture pour l'informatique d'entreprise"
author: Thomas Di Giacomo
date: 2018-01-15
href: http://www.economiematin.fr/news-une-annee-2018-sous-le-signe-de-l-ouverture-pour-l-informatique-d-entreprise
tags:
- Entreprise
- Économie
- Innovation
- Promotion
---

> L’année 2018 sera marquée par d’importants événements déjà prévus, notamment le mariage princier au Royaume-Uni, la Coupe du monde de football en Russie et l’entrée en vigueur du Règlement européen général sur la protection des données (RGPD), pour n’en citer que quelques-uns.
