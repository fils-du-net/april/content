---
site: La Presse
title: "Dix ans pour rattraper le retard informatique de la Ville"
author: Pierre-André Normandin
date: 2018-01-16
href: http://www.lapresse.ca/actualites/grand-montreal/201801/15/01-5150125-dix-ans-pour-rattraper-le-retard-informatique-de-la-ville.php
tags:
- Administration
- International
---

> Utilisant encore des logiciels datant des années 70, la Ville de Montréal évalue qu'il lui faudra 10 ans pour rattraper son retard en informatique.
