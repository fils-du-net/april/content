---
site: Developpez.com
title: "Reporty: la ville de Nice teste une application polémique qui contient des trackers"
author: Coriolan
date: 2018-01-17
href: https://www.developpez.com/actu/183166/Reporty-la-ville-de-Nice-teste-une-application-polemique-qui-contient-des-trackers-le-but-etant-de-renforcer-la-securite
tags:
- Logiciels privateurs
- Administration
- Associations
- Vie privée
---

> Nice est la ville la plus surveillée de France, mais elle veut aller plus loin et faire contribuer les citoyens afin d’accentuer cette surveillance. Pour cette raison, rien de mieux que d’exploiter leurs smartphones pour devenir témoins. C’est l’idée de l’application Reporty que Nice a commencé de tester à partir de ce lundi, cette application a été développée en Israël par une startup dirigée par l’ancien Premier ministre Ehud Barack. L’application permet d’alerter la police en cas d'incivilité, de filmer l’incident en temps réel et le transmettre en direct à la police.
