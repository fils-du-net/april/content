---
site: canoe
title: "Linus Torvalds: les correctifs Spectre et Meltdown, de la foutaise"
author: André Boily
date: 2018-01-23
href: http://fr.canoe.ca/techno/materiel/archives/2018/01/20180123-183000.html
tags:
- Entreprise
---

> Jusqu'ici, les impacts des vulnérabilités Spectre et Meltdown des puces Intel ont été couverts pour les systèmes Windows et macOS, mais du côté des systèmes libres Linux, ça ne tourne pas rond là non plus.
