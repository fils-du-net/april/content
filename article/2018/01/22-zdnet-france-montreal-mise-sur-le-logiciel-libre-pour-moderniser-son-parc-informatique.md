---
site: ZDNet France
title: "Montréal mise sur le logiciel libre pour moderniser son parc informatique"
author: Thierry Noisette
date: 2018-01-22
href: http://www.zdnet.fr/blogs/l-esprit-libre/montreal-mise-sur-le-logiciel-libre-pour-moderniser-son-parc-informatique-39863044.htm
tags:
- Administration
- Associations
- International
---

> "On le prend parce que c'est meilleur dans bien des domaines", déclare le directeur du STI de Montréal: la ville, qui accuse un fort retard informatique, table sur le Libre et des partenariats avec d'autres villes.
