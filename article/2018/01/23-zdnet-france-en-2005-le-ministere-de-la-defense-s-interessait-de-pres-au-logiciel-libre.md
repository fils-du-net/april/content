---
site: ZDNet France
title: "En 2005, le ministère de la Défense s’intéressait de près au logiciel libre"
author: Louis Adam
date: 2018-01-23
href: http://www.zdnet.fr/actualites/en-2005-le-ministere-de-la-defense-s-interessait-de-pres-au-logiciel-libre-39863042.htm
tags:
- Administration
- April
- Institutions
- Marchés publics
---

> Comme le révèle NextInpact, le ministère de la Défense était prêt à envisager une transition vers le logiciel libre en 2005, soit deux ans avant la signature du contrat dit «Open Bar» avec Microsoft.
