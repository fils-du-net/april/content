---
site: "Génération-NT"
title: "La ville de Barcelone abandonne les produits Microsoft et adopte l'Open Source"
author: Mathieu M.
date: 2018-01-15
href: https://www.generation-nt.com/barcelone-abandonne-produits-microsoft-adopte-open-source-actualite-1949892.html
tags:
- Administration
- International
---

> De plus en plus de grandes villes européennes font le choix de se séparer des outils logiciels américains de Microsoft pour adopter des alternatives libres et gratuites. Barcelone a ainsi entamé la bascule vers l'open source.
