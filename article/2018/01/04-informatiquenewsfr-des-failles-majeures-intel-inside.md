---
site: InformatiqueNews.fr
title: "Des failles majeures Intel Inside"
date: 2018-01-04
href: https://www.informatiquenews.fr/failles-majeures-intel-inside-55206
tags:
- Entreprise
- Vie privée
---

> Les experts en sécurité informatique ont découvert deux failles de sécurité majeures dans les microprocesseurs Intel. Les deux problèmes, appelés Meltdown et Specter, pourraient permettre aux pirates informatiques de voler tout le contenu de la mémoire des ordinateurs, quel que soit le type de matériels, terminaux mobiles, ordinateurs personnels et serveurs.
