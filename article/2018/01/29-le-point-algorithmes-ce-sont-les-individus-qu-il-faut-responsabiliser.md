---
site: Le Point
title: "Algorithmes: «Ce sont les individus qu'il faut responsabiliser»"
author: Laurence Neuer
date: 2018-01-29
href: http://www.lepoint.fr/chroniqueurs-du-point/laurence-neuer/algorithmes-ce-sont-les-individus-qu-il-faut-responsabiliser-29-01-2018-2190382_56.php
tags:
- Institutions
- Vie privée
---

> Pour la présidente de la Cnil Isabelle Falque Perrotin, la «déviance» des algorithmes est inéluctable, d'où l'importance de les tester régulièrement.
