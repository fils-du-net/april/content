---
site: Boursorama
title: "Bitcoin mania: bulle spéculative ou révolution technologique?"
date: 2018-01-04
href: http://www.boursorama.com/actualites/bitcoin-mania-bulle-speculative-ou-revolution-technologique-19eb708072d1488ad84f86f7303096ae
tags:
- Économie
- Innovation
---

> De moins de 1 000 dollars début 2017, le cours du bitcoin a entamé une course effrénée jusqu'à tutoyer la barre des 20 000 dollars le 17 décembre dernier. Une ascension pour le moins spectaculaire qui se solde par une hausse de 1400 % et qui a braqué tous les projecteurs sur ce placement sulfureux. Avec une multiplication de son cours par 20 en un an, la désormais célèbre crypto-monnaie attire les foules tout autant qu'elle attise les foudres. Le bitcoin est un sujet clivant.
