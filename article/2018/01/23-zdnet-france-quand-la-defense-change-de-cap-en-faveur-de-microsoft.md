---
site: ZDNet France
title: "Quand la Défense a changé de cap en faveur de Microsoft"
author: Thierry Noisette
date: 2018-01-23
href: http://www.zdnet.fr/blogs/l-esprit-libre/quand-la-defense-a-change-de-cap-en-faveur-de-microsoft-39863050.htm
tags:
- Administration
- April
- Institutions
---

> Des présidences Chirac à Sarkozy, et des ministères Alliot-Marie à Morin, la Défense a d'abord penché pour les logiciels libres, avant de se jeter dans les bras de l'éditeur, montrent des documents révélés par Next INpact et pointés par l'April.
