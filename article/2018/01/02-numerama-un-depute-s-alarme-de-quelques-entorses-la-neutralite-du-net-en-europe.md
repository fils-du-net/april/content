---
site: Numerama
title: "Un député s'alarme de «quelques entorses» à la neutralité du net en Europe"
author: Julien Lausson
date: 2018-01-02
href: https://www.numerama.com/politique/317977-un-depute-salarme-de-quelques-entorses-a-la-neutralite-du-net-en-europe.html
tags:
- Internet
- Institutions
- Neutralité du Net
---

> Un député de la majorité présidentielle interpelle le gouvernement sur la neutralité du net. Il s'inquiète de la persistance de «quelques entorses» à ce principe en Europe.
