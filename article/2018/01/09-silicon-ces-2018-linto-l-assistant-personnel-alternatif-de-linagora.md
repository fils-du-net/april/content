---
site: Silicon
title: "CES 2018: LinTO, l'assistant personnel alternatif de Linagora"
author: Rénald Boulestin
date: 2018-01-09
href: https://www.silicon.fr/ces-2018-linto-assistant-personnel-alternatif-linagora-195631.html
tags:
- Entreprise
- Innovation
- Vie privée
---

> Linagora, fournisseur français de solutions open source pour les entreprises, déballe un assistant personnel open source LinTO annoncé comme ”GAFAM free”.
