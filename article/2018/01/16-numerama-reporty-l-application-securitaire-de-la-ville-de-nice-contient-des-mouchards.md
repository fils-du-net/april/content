---
site: Numerama
title: "Reporty: l'application sécuritaire de la ville de Nice contient des mouchards"
author: Corentin Durand
date: 2018-01-16
href: https://www.numerama.com/politique/321656-reporty-lapplication-securitaire-de-la-ville-de-nice-contient-des-mouchards.html
tags:
- Logiciels privateurs
- Administration
- Associations
- Vie privée
---

> Présentée par Christian Estrosi, Reporty est une application de la Ville de Nice. En test pendant deux mois, Reporty permet aux habitants de filmer des actes d'incivilité. Mais selon deux associations, l'application cache des mouchards et collecte des données sur ses utilisateurs.
