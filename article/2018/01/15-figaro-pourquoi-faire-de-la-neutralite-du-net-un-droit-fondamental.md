---
site: FIGARO
title: "Pourquoi faire de la neutralité du Net un droit fondamental?"
author: Pauline Verge
date: 2018-01-15
href: http://www.lefigaro.fr/secteur/high-tech/2018/01/15/32001-20180115ARTFIG00307-pourquoi-faire-de-la-neutralite-du-net-un-droit-fondamental.php
tags:
- Institutions
- Associations
- Neutralité du Net
---

> La proposition du président de l'Assemblée nationale, François de Rugy, d'inscrire la neutralité du Net dans la Constitution a fait l'objet d'un groupe de travail. Pour son rapporteur, la députée Paula Forteza, il ne s'agit pas seulement d'une mesure symbolique.
