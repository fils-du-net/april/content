---
site: Datanews.be
title: "FundRequest présente ses solutions financières open source"
author: Matthieu Van Steenkiste
date: 2018-01-02
href: http://datanews.levif.be/ict/start-ups/fundrequest-presente-ses-solutions-financieres-open-source/article-normal-777705.html
tags:
- Entreprise
- Économie
- Innovation
---

> La progression des logiciels open source persiste, ce qui ne va pas sans poser de problèmes. Pour garantir des solutions fiables, la startup belge FundRequest recourt à présent à la technologie de la chaîne de blocs.
