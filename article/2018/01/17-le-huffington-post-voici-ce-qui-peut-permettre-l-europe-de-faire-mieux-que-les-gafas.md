---
site: Le Huffington Post
title: "Voici ce qui peut permettre à l'Europe de faire mieux que les GAFAs"
author: Michel Lévy-Provençal
date: 2018-01-17
href: http://www.huffingtonpost.fr/michel-levyprovencal/voici-ce-qui-peut-permettre-a-leurope-de-faire-mieux-que-les-gafas_a_23335759
tags:
- Entreprise
- Économie
- Europe
---

> Il est possible d'attaquer leur modèle en proposant un nouveau système dans lequel les utilisateurs sont propriétaires de leurs données et bénéficient de la valeur créée.
