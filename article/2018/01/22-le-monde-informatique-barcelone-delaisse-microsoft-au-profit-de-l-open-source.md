---
site: Le Monde Informatique
title: "Barcelone délaisse Microsoft au profit de l'open source"
author: Serge Leblal
date: 2018-01-22
href: https://www.lemondeinformatique.fr/actualites/lire-barcelone-delaisse-microsoft-au-profit-de-l-open-source-70627.html
tags:
- Administration
- Associations
- Promotion
---

> Dans le cadre de la campagne européenne «Public Money, Public Code» lancée par la Free Software Foundation Europe, Barcelone a décidé de se passer des services de Microsoft au profit de solutions open source. Les logiciels Exchange Server, Internet Explorer et Office vont être progressivement délaissés au profit d'Open-Xchange, Firefox et la suite LibreOffice.
