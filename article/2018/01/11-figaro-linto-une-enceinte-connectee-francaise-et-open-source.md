---
site: FIGARO
title: "LinTO, une enceinte connectée française et open source"
author: Lucie Ronfaut
date: 2018-01-11
href: http://www.lefigaro.fr/secteur/high-tech/start-up/2018/01/10/32004-20180110ARTFIG00143-linto-une-enceinte-connectee-francaise-et-open-source.php
tags:
- Entreprise
- Innovation
---

> Une entreprise française a imaginé un assistant personnel destiné aux professionnels, et dont le code est libre d'accès, en opposition au Google Home ou à l'Amazon Echo.
