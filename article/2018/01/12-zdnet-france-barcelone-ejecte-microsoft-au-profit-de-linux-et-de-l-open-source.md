---
site: ZDNet France
title: "Barcelone éjecte Microsoft au profit de Linux et de l'Open Source"
date: 2018-01-12
href: http://www.zdnet.fr/actualites/barcelone-ejecte-microsoft-au-profit-de-linux-et-de-l-open-source-39862672.htm
tags:
- Logiciels privateurs
- Administration
- Associations
- Marchés publics
---

> Adieu Outlook, adieu Exchange Server, bienvenue à Open-Xchange. Adieu IE, Adieu Office, Firefox et LibreOffice bienvenue. Barcelone devient avec cette initiative la première commune à rejoindre la campagne européenne "Public Money, Public Code".
