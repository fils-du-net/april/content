---
site: Le Monde Informatique
title: "Les 6 prévisions technologiques 2018 de GitHub"
author: Jason Warner
date: 2018-01-02
href: https://www.lemondeinformatique.fr/actualites/lire-les-6-previsions-technologiques-2018-de-github-70406.html
tags:
- Entreprise
- Internet
- Innovation
- Neutralité du Net
- Vie privée
---

> 2017 a été l'année de l'intelligence artificielle et de l'apprentissage automatique, deux domaines dont les avancées vont se poursuivre pendant encore de longues années. Que pouvons-nous attendre en 2018? Les données occupent une place de plus en plus importante dans notre vie et imposent des exigences croissantes sur la sécurité, le cloud et la technologie open source Voici les principales tendances qui animeront l'année à venir du point de vue technologique.
