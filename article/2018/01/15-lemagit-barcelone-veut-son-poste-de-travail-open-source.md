---
site: LeMagIT
title: "Barcelone veut son poste de travail Open Source"
author: Cyrille Chausson
date: 2018-01-15
href: http://www.lemagit.fr/actualites/450433173/Barcelone-veut-son-poste-de-travail-Open-Source
tags:
- Administration
- April
- Associations
- Marchés publics
- International
---

> La municipalité Catalane souhaite remplacer les applications Windows de bureautique et de messagerie par des alternatives Open Source et prévoit de troquer Windows pour Ubuntu. L’Open Source prend place au cœur du plan numérique de la ville qui promet la réutilisation des développements.
