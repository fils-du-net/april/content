---
site: Le Monde.fr
title: "Failles Meltdown et Spectre: l'incertaine mise à jour des microprocesseurs"
author: Martin Untersinger
date: 2018-01-10
href: http://www.lemonde.fr/pixels/article/2018/01/10/failles-meltdown-et-spectre-l-incertaine-mise-a-jour-des-microprocesseurs_5239929_4408996.html
tags:
- Entreprise
- Innovation
---

> Les correctifs protégeant de défauts importants récemment découverts dans la plupart des microprocesseurs sont compliqués à installer et font craindre un ralentissement des ordinateurs.
