---
site: L'ADN
title: "Tristan Nitot: «Les internautes sont devenus le bétail de Google et Facebook»"
author: Olivier Robillart
date: 2018-01-24
href: http://www.ladn.eu/tech-a-suivre/data-big-et-smart/tristan-nitot-les-internautes-sont-devenus-le-betail-de-google-et-facebook
tags:
- Entreprise
- Internet
- Vie privée
---

> Ne plus laisser Facebook, Apple, Google ou Microsoft avoir accès à votre vie numérique, c’est possible. Tristan Nitot de Cozy Cloud livre les clés pour reprendre le pouvoir sur vos informations personnelles.
