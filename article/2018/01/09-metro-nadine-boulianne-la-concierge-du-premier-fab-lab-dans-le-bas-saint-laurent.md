---
site: Métro
title: "Nadine Boulianne, la concierge du premier fab lab dans le Bas-Saint-Laurent"
author: Chloé Freslon
date: 2018-01-09
href: http://journalmetro.com/opinions/urelles/1335842/nadine-boulianne-la-concierge-du-premier-fab-lab-dans-le-bas-saint-laurent
tags:
- Matériel libre
- Innovation
- International
---

> Nadine Boulianne est fab manager, ou concierge en français, ce qui consiste à coordonner le lieu, le seul FabLab à Rivière-du-Loup et le seul dans la région du Bas-Saint-Laurent.
