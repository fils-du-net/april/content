---
site: Le Monde.fr
title: "La bibliothèque numérique Open Library inquiète des associations d’auteurs"
date: 2018-01-29
href: http://www.lemonde.fr/pixels/article/2018/01/29/la-bibliotheque-numerique-open-library-inquiete-des-associations-d-auteurs_5248746_4408996.html
tags:
- Internet
- Partage du savoir
- Associations
- Droit d'auteur
---

> Le projet Open Library, qui veut donner libre accès à des milliers de livres, va trop loin dans sa démarche, estiment plusieurs associations d’auteurs qui dénoncent une violation de leurs droits.
