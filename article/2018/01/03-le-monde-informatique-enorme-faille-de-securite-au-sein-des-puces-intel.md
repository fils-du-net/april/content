---
site: Le Monde Informatique
title: "Enorme faille de sécurité au sein des puces Intel"
author: Serge Leblal
date: 2018-01-03
href: https://www.lemondeinformatique.fr/actualites/lire-enorme-faille-de-securite-au-sein-des-puces-intel-70419.html
tags:
- Entreprise
- Vie privée
---

> Une erreur de conception dans la gestion de la mémoire protégée des puces Intel vient d'être dévoilée alors que les éditeurs de systèmes d'exploitation et d'hyperviseurs travaillent depuis plusieurs mois sur les moyens de combler cette vulnérabilité.
