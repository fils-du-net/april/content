---
site: LeMagIT
title: "Examen de code source par les autorités russes: des airs de faux procès"
author: Valéry Marchive
date: 2018-01-26
href: http://www.lemagit.fr/actualites/252433877/Examen-de-code-source-par-les-autorites-russes-des-airs-de-faux-proces
tags:
- Entreprise
- Institutions
- International
---

> De nombreux éditeurs ont, au moins par le passé, autorisé l'examen du code source de leurs produits afin d'accéder à certains marchés en Russie. Une source de vulnérabilité? Pas tant que cela, soulignent certains.
