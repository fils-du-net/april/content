---
site: NewZilla.net
title: "Barcelone choisit Linux et l’open-source"
author: Philippe Crouzillacq
date: 2018-01-13
href: https://www.newzilla.net/2018/01/13/barcelone-choisit-linux-et-lopen-source
tags:
- Logiciels privateurs
- Administration
---

> La grande migration a commencé! Selon le quotidien espagnol El Pais, la ville de Barcelone serait en train de remplacer progressivement toutes les applications propriétaires de son parc informatique par des applications open-source. Objectif : faire fonctionner le tout sous une distribution Linux à l’horizon 2019.
