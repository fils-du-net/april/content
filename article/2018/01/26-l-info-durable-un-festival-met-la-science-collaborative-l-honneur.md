---
site: L'info durable
title: "Un festival met la ”science collaborative” à l'honneur"
author: Sarah Diep
date: 2018-01-26
href: http://www.linfodurable.fr/culture/un-festival-met-la-science-collaborative-lhonneur-1783
tags:
- Partage du savoir
- Matériel libre
- Innovation
---

> Laboratoires associatifs, microscopes low-cost et performances queer gynécologiques? Bienvenue dans la santé du futur!
