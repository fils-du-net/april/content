---
site: Numerama
title: "Open source: qui sont les bons élèves et les cancres parmi les géants de la tech?"
author: Victoria Castro
date: 2018-06-12
href: https://www.numerama.com/politique/328556-open-source-qui-sont-les-bons-eleves-et-les-cancres-parmi-les-geants-de-la-tech.html
tags:
- Entreprise
---

> Tout le monde dit aimer l'open source, mais qu'en est-il vraiment? Nous avons dressé un classement de 6 géants de la tech, du plus grand sympathisant de l'ouverture au plus propriétaire d'entre eux.
