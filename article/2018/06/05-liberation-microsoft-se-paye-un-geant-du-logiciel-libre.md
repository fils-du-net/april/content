---
site: Libération
title: "Microsoft se paye un géant du logiciel libre"
author: Christophe Alix
date: 2018-06-05
href: http://www.liberation.fr/futurs/2018/06/05/microsoft-se-paye-un-geant-du-logiciel-libre_1656800
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Informatique en nuage
---

> En s'emparant, pour 7,5 milliards de dollars, de la plate-forme leader de l'open source GitHub, le gÃ©ant du logiciel confirme son changement radical d'approche.
