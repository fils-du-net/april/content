---
site: Usbek & Rica
title: "Depuis son exil russe, Edward Snowden estime que «tout a changé»"
author: Mathilde Simon
date: 2018-06-05
href: https://usbeketrica.com/article/edward-snowden-exil-interview-tout-a-change
tags:
- Institutions
- Europe
- Vie privée
---

> Cela fait maintenant cinq ans qu’Edward Snowden a quitté son confort hawaiien pour une vie d'exil en Russie. Pour marquer l’anniversaire de la fuite de documents confidentiels la plus importante de l’histoire, l’ancien membre de la NSA a accordé une interview au quotidien britannique The Guardian, qui révélait alors l’affaire en 2013, dans laquelle il tire le bilan de son sacrifice.
