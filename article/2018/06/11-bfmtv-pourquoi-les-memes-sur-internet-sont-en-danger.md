---
site: BFMtv
title: "Pourquoi les mèmes sur Internet sont en danger"
author: Elsa Trujillo
date: 2018-06-11
href: https://www.bfmtv.com/tech/pourquoi-les-memes-sur-internet-sont-en-danger-1468454.html
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Droit d'auteur
- Promotion
- Europe
---

> L'article 13 d'un projet de directive européenne sur le droit d'auteur entend limiter drastiquement la réutilisation de contenus protégés.
