---
site: FIGARO
title: "Un projet de loi européen pourrait menacer les mèmes sur Internet"
author: Marius François
date: 2018-06-08
href: http://www.lefigaro.fr/secteur/high-tech/2018/06/08/32001-20180608ARTFIG00290-un-projet-de-loi-europeen-pourrait-menacer-les-memes-sur-internet.php
tags:
- Internet
- Institutions
- Associations
- Droit d'auteur
- Europe
---

> Les images parodiques et les remix pourraient disparaître des réseaux sociaux. Des experts dénoncent un projet de loi européen sur le droit d'auteur qui risque d'être un coup dur pour la culture «lol».
