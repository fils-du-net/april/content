---
site: LeMagIT
title: "Le Microsoft de 2018 n’a aucun intérêt à corrompre GitHub"
author: Thierry Carrez
date: 2018-06-23
href: https://www.lemagit.fr/tribune/Le-Microsoft-de-2018-na-aucun-interet-a-corrompre-GitHub-Thierry-Carrez-OpenStack-Foundation
tags:
- Entreprise
- Internet
- Innovation
---

> Pour Thierry Carrez, vice-président de l’OpenStack Foundation, le rachat de GitHub par Microsoft n’est pas le fond du problème. Il invite en revanche à avoir une réflexion sur les plateformes libres pour héberger ses projets libres.
