---
site: "les-infostrateges.com"
title: "Software Heritage: une bibliothèque au service de l'archive universelle des logiciels"
author: Fabrice Molinaro
date: 2018-06-14
href: http://www.les-infostrateges.com/actu/18062573/software-heritage-une-bibliotheque-au-service-de-l-archive-universelle-des-logiciels
tags:
- Partage du savoir
- Institutions
---

> Le 7 juin dernier, L'Unesco et L'Inria ont annoncé le lancement de Softwareheritage.org, une initiative mondiale visant à créer une bibliothèque universelle rassemblant les codes sources des programmes informatiques depuis les débuts de l'ère numérique.
