---
site: The Conversation
title: "Quand les chercheurs se libèrent des revues scientifiques au coût exorbitant"
author: Corinne Leyval
date: 2018-06-17
href: https://theconversation.com/quand-les-chercheurs-se-liberent-des-revues-scientifiques-au-cout-exorbitant-97355
tags:
- Internet
- Partage du savoir
- Licenses
- Sciences
---

> Pour obtenir des financements, les chercheurs doivent publier, pour publier ils doivent payer des éditeurs, un problème?
