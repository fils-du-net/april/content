---
site: MacGeneration
title: "Google avait des vues sur GitHub avant son acquisition par Microsoft"
author: Stéphane Moussie
date: 2018-06-28
href: https://www.macg.co/ailleurs/2018/06/google-avait-des-vues-sur-github-avant-son-acquisition-par-microsoft-102837
tags:
- Entreprise
- Internet
- Innovation
---

> Si GitHub n’avait pas fini dans l’escarcelle de Microsoft, la plateforme d’hébergement de code aurait peut-être fini dans celle de Google. La dirigeante de Google Cloud, Diane Greene, a en effet révélé que son entreprise avait eu des vues sur GitHub, sans préciser si une offre formelle avait été faite.
