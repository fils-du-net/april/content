---
site: We Demain
title: "Pas de communs sans gouvernance démocratique"
author: Myriam Bouré
date: 2018-06-26
href: https://www.wedemain.fr/Pas-de-communs-sans-gouvernance-democratique_a3406.html
tags:
- Partage du savoir
- Associations
---

> Je m’appelle Myriam, j’ai cofondé Open Food France, une plateforme basée sur un logiciel libre où paysans, mangeurs, restaurateurs, peuvent organiser et opérer des circuits de distribution courts et indépendants.
