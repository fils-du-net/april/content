---
site: Numéro Magazine
title: "Portrait de l'artiste en hackeur qui détourne les nouvelles technologies"
author: Ingrid Luquet-Gad
date: 2018-06-14
href: http://www.numero.com/fr/art/japonais-ryoji-ikeda-portrait-hacker-artiste-invite-honneur-centre-pompidou-nouvelles-technologies
tags:
- Institutions
---

> Algorithmes, codage, datas, hardware… comment les artistes détournent-ils les nouvelles technologies? C'est la question vertigineuse à laquelle répond une double exposition au centre pompidou, avec l'artiste japonais Ryoji Ikeda en invité d'honneur.
