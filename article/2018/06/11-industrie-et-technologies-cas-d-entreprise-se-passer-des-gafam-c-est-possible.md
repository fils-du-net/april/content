---
site: Industrie et Technologies
title: "Se passer des GAFAM, c'est possible!"
author: Anaïs Marechal
date: 2018-06-11
href: https://www.industrie-techno.com/mail-serveurs-bureautique-les-alternatives-aux-gafam-fleurissent-pour-les-professionnels.54145
tags:
- Entreprise
- Sensibilisation
- Associations
---

> vec un marché en forte croissance, les logiciels libres - alternatives aux logiciels propriétaires largement fournis par les GAFAM - se font une place dans les entreprises. Les professionnels cherchent avant tout à maitriser leur système d’information, et nombre de solutions se présentent aujourd’hui.
