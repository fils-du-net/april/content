---
site: Blog Qwant
title: "Protéger le droit d'auteur avec des robots: un risque pour les droits et libertés fondamentaux"
author: Guillaume Champeau
date: 2018-06-19
href: https://blog.qwant.com/fr/proteger-le-droit-dauteur-avec-des-robots-un-risque-pour-les-droits-et-libertes-fondamentaux
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Cette semaine, la commission des affaires juridiques (JURI) du Parlement européen votera sur la proposition de la Commission européenne d’une Directive sur le droit d’auteur dans le Marché Unique du Numérique (“Directive copyright”). Qwant reconnaît l’importance de protéger les droits d’auteur et le besoin de le faire correctement respecter sur Internet.
