---
site: 01net.
title: "Les meilleurs logiciels libres et gratuits pour Windows, Mac et Linux"
author: Geoffroy Ondet
date: 2018-06-07
href: https://www.01net.com/astuces/les-meilleurs-logiciels-libres-et-gratuits-pour-windows-mac-et-linux-1466170.html
tags:
- Sensibilisation
---

> Abandonnez les logiciels propriétaires au profit de logiciels libres et gratuits.
