---
site: Le Monde.fr
title: "La réforme européenne du droit d’auteur franchit une étape décisive"
date: 2018-06-20
href: https://www.lemonde.fr/pixels/article/2018/06/20/la-reforme-europeenne-du-droit-d-auteur-franchit-une-etape-decisive_5318485_4408996.html
tags:
- Internet
- Institutions
- Droit d'auteur
- Promotion
- Europe
---

> La directive aprouvée par la commission juridique du Parlement européen prévoit une obligation de filtrage des contenus en ligne. Le texte alarme les défenseurs des libertés numériques.
