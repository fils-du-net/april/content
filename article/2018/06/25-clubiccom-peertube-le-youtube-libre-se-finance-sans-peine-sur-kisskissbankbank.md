---
site: Clubic.com
title: "PeerTube, le YouTube libre, se finance sans peine sur Kisskissbankbank"
author: Mathieu Grumiaux
date: 2018-06-25
href: https://www.clubic.com/television-tv/video-streaming/youtube/actualite-844252-peertube-youtube-libre-finance-peine-kisskissbankbank.html
tags:
- Internet
- Associations
- Innovation
- Promotion
- Vie privée
---

> La plateforme alternative de vidéos en ligne a récolté plus de 22 000€ en moins d'une semaine. Le lancement d'une première version est prévu pour le mois d'octobre.
