---
site: RTBF Info
title: "Des logiciels pour vous aider à contrôler vos données sur internet"
date: 2018-06-07
href: https://www.rtbf.be/info/societe/onpdp/detail_des-logiciels-pour-vous-aider-a-controler-vos-donnees-sur-internet?id=9939008
tags:
- Internet
- Sensibilisation
- Vie privée
---

> Le règlement européen de protection des données est entré en vigueur fin du mois dernier et on peut désormais connaître auprès des entreprises mais également des fournisseurs de services du web, les informations nous concernant qui ont pu être collectées.
