---
site: Les Numeriques
title: "Google aurait bien voulu racheter GitHub"
date: 2018-06-30
href: https://www.lesnumeriques.com/vie-du-net/google-aurait-bien-voulu-racheter-github-n75755.html
tags:
- Entreprise
- Internet
- Innovation
---

> En rachetant GitHub pour 7,5 milliards de dollars en juin, Microsoft a grillé la priorité à Apple, mais pas seulement… Selon Bloomberg, Diane Green, la responsable de Google Cloud, a laissé entendre que la firme de Mountain View s'était intéressée au rachat de la plateforme de codage.
