---
site: ActuaLitté.com
title: "Droit d'auteur: la proposition de directive est adoptée"
author: Antoine Oury
date: 2018-06-21
href: https://www.actualitte.com/article/monde-edition/droit-d-auteur-la-proposition-de-directive-est-adoptee/89499
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> La commission des affaires juridiques du Parlement européen a voté ce matin le projet de réforme du droit d'auteur au sein de l'Union européenne, marquant ainsi une première étape parlementaire pour sa mise en œuvre. La réforme entend améliorer la rémunération des artistes-interprètes dont les œuvres sont diffusées sur les plateformes en ligne, mais institue aussi des outils contestés pour limiter les possibilités de violation du droit d'auteur.
