---
site: RTL.fr
title: "Un projet de loi européen voté le 20 juin va-t-il changer la face du Web?"
author: Benjamin Hue
date: 2018-06-12
href: http://www.rtl.fr/actu/futur/un-projet-de-loi-europeen-vote-le-20-juin-va-t-il-changer-la-face-du-web-7793710077
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Droit d'auteur
- Europe
---

> Experts et associations dénoncent la révision de la directive européenne sur le droit d'auteur qui entend limiter la réutilisation de contenus protégés.
