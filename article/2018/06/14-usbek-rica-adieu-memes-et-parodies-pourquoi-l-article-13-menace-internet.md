---
site: Usbek & Rica
title: "Adieu mèmes et parodies? Pourquoi «l’article 13» menace Internet"
author: Guillaume Ledit
date: 2018-06-14
href: https://usbeketrica.com/article/adieu-memes-et-parodies-pourquoi-l-article-13-menace-internet
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Droit d'auteur
- Europe
---

> L'article 13 d'une directive européenne est accusé de mettre en péril la création sur Internet en s'attaquant notamment à la culture du remix en encourageant la mise en place d’une censure automatique par les plateformes numériques de tout type d’oeuvre protégées par le droit d’auteur. Ses opposants se mobilisent pour peser sur le vote à venir des parlementaires européens, prévu pour le 20 juin.
