---
site: Le Monde.fr
title: "Messageries, moteurs de recherche... comment se passer de Google, Facebook ou Twitter"
author: Yves Eudes
date: 2018-06-17
href: https://www.lemonde.fr/m-perso/article/2018/06/17/moteurs-de-recherche-messageries-comment-se-passer-de-google-facebook-ou-twitter_5316649_4497916.html
tags:
- Internet
- Associations
- Promotion
- Vie privée
---

> L’association lyonnaise Framasoft veut lutter contre l’hégémonie des Gafam et promeut des services Internet libres et respectueux de la vie privée.
