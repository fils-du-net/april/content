---
site: "CNCD-11.11.11"
title: "Des semences open source pour libérer les paysans"
author: Jean-François Pollet
date: 2018-06-26
href: https://www.cncd.be/agriculture-brevetage-vivant-semences-open-source-paysans
tags:
- Associations
- Innovation
- Licenses
- Europe
---

> A l'origine des récoltes, il y a les semences. Qui les détient, tient le monde. Le projet Semences open source encourage la production de semences collaboratives qui appartiennent à tout le monde.
