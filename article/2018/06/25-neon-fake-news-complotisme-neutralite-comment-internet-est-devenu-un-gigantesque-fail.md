---
site: neon
title: "Fake news, complotisme, neutralité…: Comment internet est devenu un gigantesque fail"
author: Charles Faugeron
date: 2018-06-25
href: https://www.neonmag.fr/fake-news-complotisme-streaming-neutralite-comment-internet-est-devenu-un-gigantesque-fail-508169.html
tags:
- Entreprise
- Internet
- Institutions
- Désinformation
- Neutralité du Net
- Vie privée
---

> Liberté, égalité, praticité : quand on nous l’a présenté, internet devait nous sauver. Depuis, il semble qu’on a tout foiré.
