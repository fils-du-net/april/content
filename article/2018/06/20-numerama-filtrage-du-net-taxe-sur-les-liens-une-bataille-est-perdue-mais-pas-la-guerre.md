---
site: Numerama
title: "Filtrage du net, taxe sur les liens: une bataille est perdue, mais pas la guerre"
author: Julien Lausson
date: 2018-06-20
href: https://www.numerama.com/politique/387644-filtrage-du-net-taxe-sur-les-liens-une-bataille-est-perdue-mais-pas-la-guerre.html
tags:
- Internet
- Institutions
- Désinformation
- Droit d'auteur
- Europe
- ACTA
---

> Au Parlement européen, une commission a approuvé la réforme du droit d'auteur, qui ouvre la voie à une taxation sur les liens et au filtrage automatique des contenus avant leur mise en ligne. Mais tout n'est pas perdu.
