---
site: France Culture
title: "Que reste-t-il du logiciel libre?"
author: Hervé Gardette
date: 2018-06-13
href: https://www.franceculture.fr/emissions/du-grain-a-moudre/que-reste-t-il-du-logiciel-libre
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Promotion
---

> Microsoft vient de racheter la plateforme de création collaborative de logiciels Github. Est-ce vraiment une bonne nouvelle pour le logiciel libre? Et quelles conséquences pour les utilisateurs? La philosophie du libre a-t-elle gagné ou s’est-elle fait manger?
