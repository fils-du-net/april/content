---
site: Le Monde.fr
title: "Comment des parlementaires veulent inscrire la neutralité du Net dans la Constitution"
author: Manon Rescan et Martin Untersinger
date: 2018-06-22
href: https://www.lemonde.fr/pixels/article/2018/06/22/comment-des-parlementaires-veulent-inscrire-la-neutralite-du-net-dans-la-constitution-francaise_5319402_4408996.html
tags:
- Internet
- Institutions
- Neutralité du Net
- Open Data
---

> Données personnelles, accès à Internet… députés et sénateurs proposent l’adoption d’une «charte du numérique » lors de la révision constitutionnelle.
