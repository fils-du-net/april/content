---
site: Konbini France
title: "Les gifs et les mèmes sont-ils en danger de mort?"
author: Pierre Schneidermann
date: 2018-06-15
href: http://www.konbini.com/fr/tendances-2/memes-danger-de-mort
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Un projet de directive européenne soumis au vote le 20 juin prochain pourrait modifier profondément la face du Web.
