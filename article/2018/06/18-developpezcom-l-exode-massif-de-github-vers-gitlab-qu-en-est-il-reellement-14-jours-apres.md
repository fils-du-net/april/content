---
site: Developpez.com
title: "L'exode «massif» de GitHub vers GitLab, qu'en est-il réellement 14 jours après?"
author: Michael Guilloux
date: 2018-06-18
href: https://www.developpez.com/actu/210004/L-exode-massif-de-GitHub-vers-GitLab-qu-en-est-il-reellement-14-jours-apres-Une-analyse-basee-sur-un-tableau-de-bord-de-GitLab
tags:
- Entreprise
- Internet
- Informatique en nuage
---

> Juste avant l’annonce officielle du rachat de GitHub par Microsoft, les rumeurs qui ont filtré sur cette acquisition ont provoqué une activité inhabituelle sur les plateformes d’hébergement de code source concurrentes, et en particulier GitLab. GitLab a en effet annoncé une multiplication par 10 des créations de dépôts sur sa plateforme, en passant de moins de 250 dépôts importés en moyenne par heure à près de 2500 dépôts d’un coup.
