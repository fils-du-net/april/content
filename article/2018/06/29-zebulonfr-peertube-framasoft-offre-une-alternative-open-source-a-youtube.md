---
site: Zebulon.fr
title: "PeerTube: Framasoft offre une alternative open-source à YouTube"
author: Ruby Charpentier
date: 2018-06-29
href: https://www.zebulon.fr/actualites/17501-peertube-framasoft-offre-une-alternative-open-source-a-youtube.html
tags:
- Internet
- Associations
- Innovation
---

> Après quelques déboires, la nouvelle plateforme de vidéo en ligne PeerTube devrait voir le jour en octobre prochain. Initié par le réseau Framasoft depuis peu, ce projet français a en effet connu certaines difficultés, notamment lorsqu'il a fallu trouver des fonds pour le développement du service.
