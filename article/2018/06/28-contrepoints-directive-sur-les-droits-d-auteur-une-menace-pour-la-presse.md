---
site: Contrepoints
title: "Directive sur les droits d’auteur: une menace pour la presse"
author: Pierre Bentata
date: 2018-06-28
href: https://www.contrepoints.org/2018/06/28/319121-directive-sur-les-droits-dauteur-une-menace-pour-la-presse
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Le projet de réforme du droit d’auteur au niveau européen comporte des effets pervers pour les éditeurs de presse.
