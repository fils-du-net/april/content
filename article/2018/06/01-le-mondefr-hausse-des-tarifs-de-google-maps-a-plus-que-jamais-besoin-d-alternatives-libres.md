---
site: Le Monde.fr
title: "Hausse des tarifs de Google Maps: «On a plus que jamais besoin d'alternatives libres»"
author: Claire Legros
date: 2018-06-01
href: https://www.lemonde.fr/chronique-des-communs/article/2018/06/01/hausse-des-tarifs-de-google-maps-on-a-plus-que-jamais-besoin-d-alternatives-libres_5307968_5049504.html
tags:
- Entreprise
- Internet
- Économie
- Open Data
---

> La conférence annuelle d’OpenStreetMap France, le projet de carte libre et collaborative, ouvre ses portes vendredi 1er juin, dix jours seulement avant la hausse annoncée des tarifs de Google Maps. Entretien avec son porte-parole.
