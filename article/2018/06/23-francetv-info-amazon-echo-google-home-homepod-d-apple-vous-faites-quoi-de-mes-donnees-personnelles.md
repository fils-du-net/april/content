---
site: francetv info
title: "Amazon Echo, Google Home, HomePod d'Apple: vous faites quoi de mes données personnelles?"
author: Robin Prudent et Vincent Matalon
date: 2018-06-23
href: https://www.francetvinfo.fr/internet/securite-sur-internet/amazon-echo-google-home-homepod-d-apple-vous-faites-quoi-de-mes-donnees-personnelles_2810683.html
tags:
- Entreprise
- Innovation
- Vie privée
---

> Alors que la marque à la pomme vient de sortir en France son enceinte HomePod, franceinfo revient sur les enjeux soulevés par les assistants vocaux en matière de confidentialité.
