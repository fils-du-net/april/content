---
site: Developpez.com
title: "Rachat de GitHub par Microsoft: la fondation Linux livre son analyse de la situation"
author: Stéphane le calme
date: 2018-06-08
href: https://www.developpez.com/actu/208325/Rachat-de-GitHub-par-Microsoft-la-fondation-Linux-livre-son-analyse-de-la-situation-et-affirme-avoir-hate-de-voir-les-ameliorations-sur-GitHub
tags:
- Entreprise
- Internet
- Associations
- Innovation
- Informatique en nuage
---

> Cette semaine, Microsoft a annoncé le rachat de GitHub pour 7,5 milliards de dollars en stock. Par le biais de Jim Zemlin, son Directeur Exécutif, la fondation Linux a décidé de partager son ressenti.
