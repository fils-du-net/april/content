---
site: L'OBS
title: "La bibliothèque d'Alexandrie des logiciels ouvre ses portes (virtuelles) au public"
author: Thierry Noisette
date: 2018-06-07
href: https://www.nouvelobs.com/sciences/20180607.OBS7902/la-bibliotheque-d-alexandrie-des-logiciels-ouvre-ses-portes-virtuelles-au-public.html
tags:
- Partage du savoir
- Institutions
- Sciences
---

> Software Heritage vise à préserver la mémoire de tous les codes sources. Elle ouvre à tous l'accès à son dépôt : 83 millions de projets logiciels.
