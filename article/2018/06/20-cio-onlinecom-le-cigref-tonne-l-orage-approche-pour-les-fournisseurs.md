---
site: "cio-online.com"
title: "Le Cigref tonne, l'orage approche pour les fournisseurs"
author: Bertrand Lemaire
date: 2018-06-20
href: https://www.cio-online.com/actualites/lire-le-cigref-tonne-l-orage-approche-pour-les-fournisseurs-10418.html
tags:
- Entreprise
- Logiciels privateurs
- Associations
- Licenses
---

> En y mettant du poids et de la solennité, le Cigref a adressé un nouvel avertissement sans frais aux grands fournisseurs IT. Le dernier? A l'heure des promesses non-tenues du cloud et des pressions insupportables sur les budgets, les grandes entreprises semblent avoir pris le virage des alternatives. Oracle, Microfocus, SAP et les autres devraient sérieusement se méfier ou ils se réveilleront quand les entreprises cesseront brutalement leurs contrats, une fois les migrations en cours achevées.
