---
site: Industrie et Technologies
title: "Software Heritage: les logiciels ont leur bibliothèque d'Alexandrie"
author: Anaïs Marechal
date: 2018-06-13
href: https://www.industrie-techno.com/lancement-de-software-heritage-la-premiere-archive-universelle-du-logiciel.53341
tags:
- Partage du savoir
- Institutions
---

> La première bibliothèque universelle du code source du logiciel vient d’être mise en ligne, fruit d’un partenariat entre l’INRIA et l’Unesco. Avec l’objectif de réunir tous les logiciels (libres ou payants) sur une même plateforme, elle préservera ce patrimoine culturel et scientifique de façon uniforme.
