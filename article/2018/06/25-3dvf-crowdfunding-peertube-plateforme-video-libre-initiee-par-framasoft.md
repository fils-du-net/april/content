---
site: 3DVF
title: "Crowdfunding: PeerTube, plateforme vidéo libre initiée par Framasoft"
author: shadows44
date: 2018-06-25
href: http://www.3dvf.com/actualite-23699-crowdfunding-peertube-plateforme-video-libre-initiee-framasoft.html
tags:
- Internet
- Associations
- Innovation
---

> Framasoft, réseau dédié à la promotion du libre et des logiciels libres, a récemment lancé une campagne de financement participatif autour de son nouveau projet, PeerTube.
