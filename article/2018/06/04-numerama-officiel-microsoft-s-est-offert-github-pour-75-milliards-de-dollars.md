---
site: Numerama
title: "Officiel: Microsoft s'est offert GitHub pour 7,5 milliards de dollars"
author: Julien Lausson
date: 2018-06-04
href: https://www.numerama.com/business/382125-microsoft-se-serait-offert-github-de-quoi-alarmer-les-partisans-du-logiciel-libre.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
---

> Microsoft a confirmé qu'il s'est offert GitHub, une plateforme web pour héberger et gérer des logiciels en coopération. De quoi inquiéter les défenseurs du logiciel libre? Pas forcément.
