---
site: ZDNet France
title: "Les emplois Linux et open source sont plus demandés que jamais"
author: Steven J. Vaughan-Nichols
date: 2018-06-21
href: https://www.zdnet.fr/actualites/les-emplois-linux-et-open-source-sont-plus-demandes-que-jamais-39870032.htm
tags:
- Entreprise
- Sensibilisation
- Désinformation
---

> En quête d'un emploi dans les technologies? Alors, il est temps de quitter Windows et de se tourner vers Linux et l'open source. Selon la fondation Linux et le rapport Open Source 2018 de Dice, 87% des responsables du recrutement éprouvent des difficultés à trouver des talents de l'open source. Or, ces embauches sont désormais une priorité pour 83% des employeurs.
