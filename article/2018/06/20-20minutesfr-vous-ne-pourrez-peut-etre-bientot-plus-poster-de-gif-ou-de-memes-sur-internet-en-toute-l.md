---
site: 20minutes.fr
title: "Vous ne pourrez peut-être bientôt plus poster de GIF ou de mèmes sur Internet en toute liberté"
author: M.F.
date: 2018-06-20
href: https://www.20minutes.fr/high-tech/2292047-20180620-pourrez-peut-etre-bientot-plus-poster-gif-memes-internet-toute-liberte
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Un projet de directive européenne sur le droit d’auteur fait craindre aux défenseurs des droits numériques une atteinte à la liberté d’expression sur Internet…
