---
site: ZDNet France
title: "Rachat de GitHub par Microsoft: une victoire pour la fondation Linux"
author: Liam Tung
date: 2018-06-11
href: https://www.zdnet.fr/actualites/rachat-de-github-par-microsoft-une-victoire-pour-la-fondation-linux-39869471.htm
tags:
- Entreprise
- Internet
---

> Tous aux abris après l'annonce du rachat de GitHub par Microsoft? Pour le président de la Linux Foundation, c'est au contraire une victoire pour l'open source. Les temps ont changé, nous avons tous grandi, juge-t-il.
