---
site: Libération
title: "Est-il vrai qu'on ne pourra bientôt plus poster de GIF ou de mèmes sur Internet?"
author: Fabien Leboucq
date: 2018-06-29
href: http://www.liberation.fr/checknews/2018/06/29/est-il-vrai-qu-on-ne-pourra-bientot-plus-poster-de-gif-ou-de-memes-sur-internet_1662604
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Droit d'auteur
- Europe
---

> La directive n'est pas encore passée devant le Parlement européen, mais c'est bien un risque, selon les détracteurs du projet.
