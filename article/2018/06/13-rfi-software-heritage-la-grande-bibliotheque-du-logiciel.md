---
site: RFI
title: "Software Heritage, la grande bibliothèque du logiciel"
author: Dominique Desaunay
date: 2018-06-13
href: http://www.rfi.fr/technologies/20180613-internet-informatique-software-heritage-patrimoine-bibliotheque-logiciel
tags:
- Internet
- Partage du savoir
- Institutions
---

> La plupart des activités humaines dépendent exclusivement des programmes informatiques qui permettent, par exemple, aux internautes de consulter leurs réseaux sociaux ainsi que de surfer sur n’importe quelle page web. Des logiciels fragiles qui contrairement aux hiéroglyphes gravés dans la pierre peuvent s’altérer avec le temps et disparaître à jamais. C’est la raison pour laquelle les informaticiens de l'Institut national de recherche en informatique et en automatique ont développé une immense bibliothèque en ligne dénommée Software Heritage.
