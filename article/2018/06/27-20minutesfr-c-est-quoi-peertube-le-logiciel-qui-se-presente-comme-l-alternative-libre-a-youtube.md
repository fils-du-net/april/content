---
site: 20minutes.fr
title: "C'est quoi PeerTube, le logiciel qui se présente comme l’alternative libre à YouTube?"
author: Marie De Fournas
date: 2018-06-27
href: https://www.20minutes.fr/high-tech/2296959-20180627-quoi-peertube-logiciel-presente-comme-alternative-libre-youtube
tags:
- Internet
- Associations
- Innovation
- Promotion
---

> Tout comme Youtube, Dailymotion ou Vimeo, PeerTube permet de visionner, commenter et publier des vidéos sur des plateformes où les administrateurs choisissent leurs propres règles d’utilisations…
