---
site: 01net.
title: "Europe: la réforme contestée du droit d'auteur votée en commission"
author: Gilbert Kallenborn
date: 2018-06-20
href: https://www.01net.com/actualites/europe-la-reforme-contestee-du-droit-d-auteur-votee-en-commission-1474954.html
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> En dépit des inquiétudes formulées par les pionniers du Web et les organisations de défense des droits citoyens, les parlementaires européens ont adopté un texte qui renforce considérablement le droit d’auteur.
