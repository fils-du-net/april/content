---
site: ZDNet France
title: "L’académie des oscars déroule le tapis rouge à l’open source"
author: Louis Adam
date: 2018-08-13
href: https://www.zdnet.fr/actualites/l-academie-des-oscars-deroule-le-tapis-rouge-a-l-open-source-39872359.htm
tags:
- Entreprise
- Associations
- Innovation
---

> Plusieurs acteurs de l’industrie du cinéma se sont rassemblés afin de créer l’Academy Software Foundation, une fondation destinée à chapeauter la gouvernance des nombreux projets open source utilisés par les studios.
