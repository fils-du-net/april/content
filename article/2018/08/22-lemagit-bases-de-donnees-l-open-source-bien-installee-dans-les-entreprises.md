---
site: LeMagIT
title: "Bases de données: l’open source bien installée dans les entreprises"
author: Cyrille Chausson
date: 2018-08-22
href: https://www.lemagit.fr/conseil/Bases-de-donnees-lopen-source-bien-installee-dans-les-entreprises
tags:
- Entreprise
- Administration
- Économie
- Informatique en nuage
---

> Open source et bases de données: les technologies open source ont gagné en performance face aux bases de données relationnelles classiques - et le cloud en facilite l’accès. Pour qui est la question des coûts est primordiale, elles constituent une véritable option.
