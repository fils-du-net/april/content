---
site: Developpez.com
title: "Le projet OpenStreetMap accueille de nombreux contributeurs dont Microsoft, Apple et Facebook"
author: Olivier Famien
date: 2018-08-28
href: https://www.developpez.com/actu/221036/Le-projet-OpenStreetMap-accueille-de-nombreux-contributeurs-dont-Microsoft-Apple-et-Facebook-Google-Maps-doit-il-craindre-cette-collaboration
tags:
- Entreprise
- Internet
- Associations
- Open Data
---

> Google Maps doit-il craindre cette collaboration?
