---
site: Programmez!
title: "Le DevOps – la prise de pouvoir de l’Open Source?"
author: Jan Gabriel
date: 2018-08-28
href: https://www.programmez.com/avis-experts/le-devops-la-prise-de-pouvoir-de-lopen-source-27916
tags:
- Entreprise
- Innovation
- Informatique en nuage
---

> S’il y a un débat public sur le fait qu’un outil puisse ou ne puisse pas être un « outil DevOps », nous pouvons néanmoins constater que les thèmes phares du DevOps – Agilité, Automatisation, Collaboration – se recoupent fort bien avec ceux du mouvement Open Source.
