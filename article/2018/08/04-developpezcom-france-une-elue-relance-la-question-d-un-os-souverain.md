---
site: Developpez.com
title: "France: une élue relance la question d'un OS souverain"
author: Stéphane le calme
date: 2018-08-04
href: https://www.developpez.com/actu/217730/France-une-elue-relance-la-question-d-un-OS-souverain-apres-avoir-interpelle-le-gouvernement-au-sujet-de-l-omnipresence-des-GAFAM-dans-le-pays
tags:
- Entreprise
- April
- Institutions
- Informatique en nuage
---

> En janvier 2016, après examen à l’Assemblée nationale, les députés ont approuvé l’idée de mettre en place un organe qui sera chargé de piloter la création d’un système d’exploitation souverain made in France. Par souverain, il faut surtout comprendre que l’OS devra permettre au pays d’avoir plus de contrôle sur ses données et d’être à l’abri de tout espionnage étranger. Autrement dit, un OS axé sur la sécurité pourrait faire l’affaire. Le gouvernement a également exprimé un avis favorable sur la création d’un tel système d’exploitation.
