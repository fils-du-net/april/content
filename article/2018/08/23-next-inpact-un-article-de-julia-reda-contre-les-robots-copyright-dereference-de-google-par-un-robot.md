---
site: Next INpact
title: "Un article de Julia Reda contre les robots-copyright déréférencé de Google par un robot-copyright"
author: Marc Rees
date: 2018-08-23
href: https://www.nextinpact.com/news/106950-un-article-julia-reda-contre-robots-copyright-dereference-google-par-robot-copyright.htm
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Qu’un article de l’eurodéputée Julia Reda soit déréférencé de Google à la demande d’une société de défense de l’industrie culturelle, c’est déjà fort. Lorsque ce même article traite des dangers des robots-copyrights prônés dans la réforme sur le droit d’auteur, cela en devient un superbe avant-goût.
