---
site: ZDNet France
title: "Sur Linux, Steam poursuit sur sa lancée"
author: Louis Adam
date: 2018-08-23
href: https://www.zdnet.fr/actualites/sur-linux-steam-poursuit-sur-sa-lancee-39872659.htm
tags:
- Entreprise
- DRM
---

> Développé en interne depuis deux ans, l’initiative Steam Play vise à permettre aux utilisateurs de Linux de profiter des jeux disponibles sur Steam. Valve annonce aujourd’hui l’arrivée en version beta de son système.
