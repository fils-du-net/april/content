---
site: BFMtv
title: "Bornes Autolib': la faille logicielle"
author: Jean-Baptiste Huet
date: 2018-08-30
href: https://bfmbusiness.bfmtv.com/entreprise/bornes-autolib-la-faille-logicielle-1513259.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
---

> Après l’arrêt brutal du service d’autopartage il y a quelques semaines... La mairie de Paris a récupéré les bornes afin de proposer un service de recharge pour les véhicules électriques. Seulement ces bornes, en l'état, sont inutilisables.En cause, un problème de logiciel.
