---
site: Developpez.com
title: "Directive Copyright: les eurodéputés vont-ils retirer les articles litigieux? Des manifestations sont prévues le 26 août avant le prochain vote"
author: Michael Guilloux
date: 2018-08-22
href: https://www.developpez.com/actu/220192/Directive-Copyright-les-eurodeputes-vont-ils-retirer-les-articles-litigieux-Des-manifestations-sont-prevues-le-26-aout-avant-le-prochain-vote
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Le 5 juillet, contre toute attente, la réforme controversée sur le droit d'auteur a été rejetée par le Parlement européen en session plénière. Comme prévu, tous les eurodéputés se sont prononcés sur le texte élaboré par la Commission des affaires juridiques du Parlement. Ledit texte a été adopté le 20 juin par la Commission juridique avec les articles 11 et 13 qui sont énormément controversés.
