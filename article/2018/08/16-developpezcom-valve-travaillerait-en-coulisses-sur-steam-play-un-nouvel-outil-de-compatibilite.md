---
site: Developpez.com
title: "Valve travaillerait en coulisses sur Steam Play, un nouvel outil de compatibilité"
author: Christian Olivier
date: 2018-08-16
href: https://www.developpez.com/actu/219200/Valve-travaillerait-en-coulisses-sur-Steam-Play-un-nouvel-outil-de-compatibilite-permettant-aux-jeux-concus-pour-Windows-de-fonctionner-sous-Linux
tags:
- Entreprise
- Logiciels privateurs
---

> Valve semble travailler en coulisses sur un ensemble «d’outils de compatibilité», appelé Steam Play, qui à terme devrait permettre à des jeux développés pour Windows de fonctionner sous Linux. C’est du moins ce que laissent supposer de récentes découvertes faites par SteamDB dans la base de données de Steam.
