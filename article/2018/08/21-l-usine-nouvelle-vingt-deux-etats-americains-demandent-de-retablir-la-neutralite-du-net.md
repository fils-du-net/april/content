---
site: L'usine Nouvelle
title: "Vingt-deux Etats américains demandent de rétablir la neutralité du Net"
author: David Shepardson et Claude Chendjou
date: 2018-08-21
href: https://www.usinenouvelle.com/article/vingt-deux-etats-americains-demandent-de-retablir-la-neutralite-du-net.N731864
tags:
- Internet
- Institutions
- Neutralité du Net
- International
---

> Vingt-deux Etats américains et le district de Columbia ont demandé le 20 août à une cour d'appel de rétablir le principe de la neutralité du net, édicté en 2015 par l'administration Obama pour garantir un traitement égal des flux de données par les opérateurs.
