---
site: Clubic.com
title: "Open source: l'industrie américaine du cinéma s'associe avec la Fondation Linux"
author: Alistair Servet
date: 2018-08-13
href: https://www.clubic.com/humour-informatique-geek/actualite-844899-open-source-industrie-americaine-cinema-associe-fondation-linux.html
tags:
- Entreprise
- Associations
- Innovation
- Sciences
---

> L'académie américaine du cinéma s'est récemment associée à la fondation Linux pour faciliter et réglementer l'utilisation des logiciels open source dans les industries du cinéma et des médias.
