---
site: Usbek & Rica
title: "La Californie possède désormais sa propre version du RGPD"
date: 2018-08-09
href: https://usbeketrica.com/article/californie-possede-version-rgpd
tags:
- Entreprise
- Internet
- Institutions
- International
- Vie privée
---

> Le Golden State s’est doté d’une loi relative à la protection des données des internautes. Une initiative qui s’inscrit dans un contexte d’inquiétudes croissantes autour de la vie privée sur Internet. Et qui pourrait bien servir de modèle pour le reste du pays.
