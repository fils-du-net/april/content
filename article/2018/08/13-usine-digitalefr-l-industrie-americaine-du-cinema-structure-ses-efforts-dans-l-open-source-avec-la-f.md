---
site: "usine-digitale.fr"
title: "L'industrie américaine du cinéma structure ses efforts dans l'open source avec la fondation Linux"
author: Julien Bergounhoux
date: 2018-08-13
href: https://www.usine-digitale.fr/article/l-industrie-americaine-du-cinema-structure-ses-efforts-dans-l-open-source-avec-la-fondation-linux.N730094
tags:
- Entreprise
- Associations
- Innovation
---

> L'académie américaine des arts et sciences du cinéma, qui organise entre autres la cérémonie des Oscars, a fait alliance avec la fondation Linux pour structurer les efforts de l'industrie du film en matière de développement d'outils logiciels.
