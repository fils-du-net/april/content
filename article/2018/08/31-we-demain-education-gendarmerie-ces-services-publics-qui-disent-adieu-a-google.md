---
site: We Demain
title: "Éducation, gendarmerie... Ces services publics qui disent adieu à Google"
author: Anaïs Marechal
date: 2018-08-31
href: https://www.wedemain.fr/Education-gendarmerie-Ces-services-publics-qui-disent-adieu-a-Google_a3544.html
tags:
- Entreprise
- Internet
- Administration
- Institutions
---

> Petit à petit, le service public s'organise contre le monopole de Google. Des outils associatifs font leur apparition sur les sites internet des collectivités comme Framasoft ou OpenStreetMap, et certaines entreprises comme Qwant investissent l'éducation nationale.
