---
site: Contrepoints
title: "Blockchain: une menace technologique sur nos libertés?"
author: Gérard Dréan
date: 2018-08-07
href: https://www.contrepoints.org/2018/08/07/321917-blockchain-une-menace-technologique-sur-nos-libertes
tags:
- Internet
- Institutions
- Vie privée
---

> La Blockchain va-t-elle servir pour porter atteinte aux libertés individuelles?
