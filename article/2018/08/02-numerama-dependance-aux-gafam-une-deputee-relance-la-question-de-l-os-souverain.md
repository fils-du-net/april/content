---
site: Numerama
title: "Dépendance aux «GAFAM»: une députée relance la question de l'OS souverain"
author: Julien Lausson
date: 2018-08-02
href: https://www.numerama.com/politique/403029-dependance-aux-gafam-une-deputee-relance-la-question-de-los-souverain.html
tags:
- Entreprise
- April
- Institutions
---

> Une députée interpelle le gouvernement sur l'omniprésence croissante des géants américains de la tech en France. Elle souhaite la mise en place d'un commissariat à la souveraineté numérique et pose la question de l'OS made in France.
