---
site: Le Monde Informatique
title: "Taxation des GAFA: pas de consensus européen en vue"
author: Nicolas Certes
date: 2018-04-30
href: https://www.lemondeinformatique.fr/actualites/lire-taxation-des-gafa-pas-de-consensus-europeen-en-vue-71620.html
tags:
- Entreprise
- Économie
- Institutions
- Europe
---

> Réunis à Sofia (Bulgarie), le 28 avril, les ministres des Finances de l'UE débattaient de la taxation des géants du web. Le Royaume-Uni, qui s'était prononcé pour, a changé de position. L'Allemagne se désolidarise. La France, en la personne de Bruno Le Maire, porteur du projet, voit rouge quant à la proposition d'un consensus mondial sur le sujet.
