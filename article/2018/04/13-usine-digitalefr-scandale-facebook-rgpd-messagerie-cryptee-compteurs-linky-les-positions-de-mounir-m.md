---
site: "usine-digitale.fr"
title: "Scandale Facebook, RGPD, messagerie cryptée, compteurs Linky… Les positions de Mounir Mahjoubi sur la protection des données personnelles"
author: Juliette Raynal
date: 2018-04-13
href: https://www.usine-digitale.fr/article/scandale-facebook-rgpd-messagerie-cryptee-compteurs-linky-les-positions-et-annonces-de-mounir-mahjoubi-sur-la-protection-des-donnees-personnelles.N679859
tags:
- Internet
- Administration
- Institutions
- Vie privée
---

> Interviewé sur France Inter, le secrétaire d'Etat en charge du Numérique Mounir Mahjoubi a balayé de nombreux sujets concernant la question épineuse de la protection des données personnelles. Voici ce qu'il faut en retenir.
