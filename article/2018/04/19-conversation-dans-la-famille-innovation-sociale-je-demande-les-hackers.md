---
site: The Conversation
title: "Dans la famille «innovation sociale», je demande… les hackers!"
author: Yannick Chatelain
date: 2018-04-19
href: https://theconversation.com/dans-la-famille-innovation-sociale-je-demande-les-hackers-94267
tags:
- Internet
- Partage du savoir
- Innovation
---

> L’éthique hacker: une innovation sociale!
