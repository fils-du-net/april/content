---
site: La Tribune
title: "Qui est Kat Borlongan, la (probable) nouvelle patronne de la Mission French Tech?"
author: Sylvain Rolland
date: 2018-04-17
href: https://www.latribune.fr/technos-medias/qui-est-kat-borlongan-la-probable-nouvelle-patronne-de-la-mission-french-tech-775564.html
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Innovation
- Promotion
- Open Data
---

> Sauf rebondissement, le secrétaire d’Etat au Numérique, Mounir Mahjoubi, devrait annoncer dans la deuxième quinzaine de mai la nomination de l’entrepreneure Kat Borlongan en tant que nouvelle directrice de la Mission French Tech. Une vraie surprise, autant pour la Mission French Tech que pour l'écosystème d'innovation français.
