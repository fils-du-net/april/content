---
site: Numerama
title: "Messagerie anonyme et sécurisée: pourquoi Tor Messenger jette l'éponge"
author: Julien Lausson
date: 2018-04-03
href: https://www.numerama.com/tech/340070-messagerie-anonyme-et-securisee-pourquoi-tor-messenger-jette-leponge.html
tags:
- Internet
- Vie privée
---

> Annoncée en 2014 et sortie en 2015, la messagerie Tor tire sa révérence. L'équipe en charge de son développement a décidé de jeter l'éponge, à cause de problèmes jugés insurmontables. Le projet était né dans la foulée des révélations d'Edward Snowden.
