---
site: Usbek & Rica
title: "Comment Facebook vous contraint à accepter ses nouvelles conditions d'utilisation"
author: Lionel Maurel
date: 2018-04-26
href: https://usbeketrica.com/article/comment-facebook-contraint-accepter-nouvelles-conditions-utilisation
tags:
- Entreprise
- Internet
- Institutions
- Europe
- Vie privée
---

> Le réseau social vous pousse à accepter de nouveaux paramètres de confidentialité, procédant ainsi à un «chantage au service».
