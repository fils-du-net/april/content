---
site: ZDNet France
title: "Le RGPD, un atout concurrentiel pour le cloud français"
author: Xavier Biseul
date: 2018-04-26
href: http://www.zdnet.fr/actualites/le-rgpd-un-atout-concurrentiel-pour-le-cloud-francais-39867506.htm
tags:
- Entreprise
- Économie
- Institutions
- Informatique en nuage
- Europe
- Vie privée
---

> Un temps rétifs au nouveau règlement européen, hébergeurs, providers et éditeurs français font aujourd’hui du RGPD un argument marketing, face à la concurrence étrangère.
