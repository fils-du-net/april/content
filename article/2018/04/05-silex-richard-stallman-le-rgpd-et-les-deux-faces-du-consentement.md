---
site: "- S.I.Lex -"
title: "Richard Stallman, le RGPD et les deux faces du consentement"
author: calimaq
date: 2018-04-05
href: https://scinfolex.com/2018/04/05/richard-stallman-le-rgpd-et-les-deux-faces-du-consentement
tags:
- Entreprise
- Institutions
- Europe
- Vie privée
---

> Richard Stallman, figure emblématique du mouvement du logiciel libre, a publié cette semaine dans The Guardian une tribune dans laquelle il réagit au scandale Facebook/Cambridge Analytica, en élargissant la perspective à la problématique de la surveillance.
