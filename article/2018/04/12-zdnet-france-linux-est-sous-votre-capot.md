---
site: ZDNet France
title: "Linux est sous votre capot"
author: Steven J. Vaughan-Nichols
date: 2018-04-12
href: http://www.zdnet.fr/actualites/linux-est-sous-votre-capot-39866888.htm
tags:
- Entreprise
---

> BMW, Chevrolet, Honda, Mercedes et Tesla ont toutes une chose en commun en plus de faire des voitures: leurs véhicules fonctionnent tous sous Linux.
