---
site: Next INpact
title: "Le 31 mars, Mozilla fêtait ses 20 ans"
date: 2018-04-03
href: https://www.nextinpact.com/brief/le-31-mars--mozilla-fetait-ses-20-ans-3377.htm
tags:
- Entreprise
- Internet
- Associations
- Licenses
---

> Le projet est né le 31 mars 1998, quand le code source du navigateur Netscape a été ouvert à tout un chacun.
