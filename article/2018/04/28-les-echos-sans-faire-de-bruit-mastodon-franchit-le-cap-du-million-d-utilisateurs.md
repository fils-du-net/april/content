---
site: Les Echos
title: "Sans faire de bruit, Mastodon franchit le cap du million d'utilisateurs"
author: Etienne Combier
date: 2018-04-28
href: https://www.lesechos.fr/tech-medias/hightech/0301603995730-sans-faire-de-bruit-mastodon-franchit-le-cap-du-million-dutilisateurs-2172564.php
tags:
- Internet
- Vie privée
---

> Le réseau social décentralisé, opposé à toute collecte de données sur ses utilisateurs, trace son chemin tranquillement.
