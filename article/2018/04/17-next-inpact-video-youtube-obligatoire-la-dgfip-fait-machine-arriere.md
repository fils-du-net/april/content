---
site: Next INpact
title: "Vidéo YouTube obligatoire: la DGFiP fait machine arrière"
author: David Legrand
date: 2018-04-17
href: https://www.nextinpact.com/news/106465-le-site-impots-force-ses-visiteurs-a-regarder-video-youtube-pour-entrer.htm
tags:
- Internet
- Administration
- Institutions
- Vie privée
---

> Si vous avez tenté de déclarer vos revenus pour 2017 sur le site des impôts, vous avez sans doute constaté qu'il était impossible de rentrer à moins de regarder une vidéo.
