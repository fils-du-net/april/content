---
site: Developpez.com
title: "Richard Stallman remet en cause l'efficacité du RGPD"
author: Michael Guilloux
date: 2018-04-04
href: https://www.developpez.com/actu/196326/Richard-Stallman-remet-en-cause-l-efficacite-du-RGPD-il-veut-plutot-une-loi-qui-empeche-les-systemes-de-collecter-des-donnees-personnelles
tags:
- Entreprise
- Institutions
- Europe
- Vie privée
---

> Richard Matthew Stallman (RMS) est pour beaucoup quelqu'un d'un peu trop extrémiste pour que ses opinions soient prises au sérieux. Mais pour ceux qui ont l'habitude de l'écouter ou de le lire, il dit parfois de grandes vérités et bon nombre de ses avertissements sont aujourd'hui une réalité. Les années ne l'ont pas rendu plus flexible, au contraire, le président de la Free Software Foundation (FSF) est resté et s'est affermi dans sa position de militant du logiciel libre et des valeurs qui vont de pair, comme le respect de la vie privée.
