---
site: Numerama
title: "Réforme du droit d'auteur: 147 organisations chargent la directive européenne"
author: Julien Lausson
date: 2018-04-26
href: https://www.numerama.com/politique/350957-reforme-du-droit-dauteur-147-organisations-chargent-la-directive-europeenne.html
tags:
- April
- Institutions
- Associations
- Droit d'auteur
- Europe
- International
---

> Alors que le projet de réforme du droit d'auteur au niveau européen poursuit son cheminement, une coalition de 147 organisations appelle les États membres de l'Union à lui mettre un coup de frein.
