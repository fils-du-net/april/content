---
site: L'Opinion
title: "Directive “droits d’auteur”: un pas de plus vers le contrôle du Net?"
author: Nicolas Mazzucchi
date: 2018-04-26
href: https://www.lopinion.fr/edition/economie/nicolas-mazzucchi-directive-droits-d-auteur-pas-plus-vers-controle-net-148348
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> La proposition d’une nouvelle directive de l’UE sur les droits d’auteurs, actuellement en débat à l’Union européenne (UE), vise notamment à renforcer le cadre légal de la protection de ces droits dans l’optique de l’établissement d’un marché numérique commun.
