---
site: Next INpact
title: "L'ex-directrice juridique d'Hadopi quitte la Commission européenne pour le ministère de la Culture"
author: Marc Rees
date: 2018-04-24
href: https://www.nextinpact.com/news/106509-lex-directrice-juridique-dhadopi-quitte-commission-europeenne-pour-ministere-culture.htm
tags:
- HADOPI
- Institutions
- Droit d'auteur
- Europe
---

> Sarah Jacquier, directrice des affaires juridiques de la Hadopi, détachée à la Commission européenne depuis novembre 2014, rejoint selon nos informations le ministère de la Culture. Elle y poursuivra ses travaux sur la réforme de la directive relative au droit d'auteur.
