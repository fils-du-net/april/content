---
site: Numerama
title: "5 questions sur l'action de la Quadrature du Net contre les géants du web"
author: Julien Lausson
date: 2018-04-17
href: https://www.numerama.com/politique/345803-5-questions-laction-de-groupe-de-quadrature-net-contre-geants-web.html
tags:
- Entreprise
- Institutions
- Associations
- Vie privée
---

> La Quadrature du Net annonce le lancement d'une action de groupe contre Google, Apple, Facebook, Amazon et Microsoft. Elle est fondée sur le Règlement général sur la protection des données, qui entre en application le 25 mai. L'association juge que ces entreprises obtiennent incorrectement le consentement des internautes.
