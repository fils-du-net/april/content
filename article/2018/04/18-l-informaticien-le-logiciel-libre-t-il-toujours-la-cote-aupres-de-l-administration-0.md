---
site: L'Informaticien
title: "Le logiciel libre a-t-il toujours la cote auprès de l’Administration?"
author: Christophe Guillemin
date: 2018-04-18
href: https://www.linformaticien.com/dossiers/logiciel-libre-administration.aspx
tags:
- Administration
- International
---

> Alors que Barcelone et Montréal engagent de vastes plans de migration vers le logiciel libre, les collectivités françaises et l’administration d’État sont-elles toujours aussi férues d’open-source? Il semble en effet, selon les acteurs du secteur, que le logiciel libre n’a jamais autant suscité d’intérêt dans les territoires, les ministères et les autres structures publiques, qu’en cette période de restrictions budgétaires. Près d’un tiers des collectivités en serait désormais équipé!
