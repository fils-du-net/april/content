---
site: The Conversation
title: "Facebook et le «paradoxe de la vie privée»"
author: David Glance
date: 2018-04-10
href: https://theconversation.com/facebook-et-le-paradoxe-de-la-vie-privee-94684
tags:
- Entreprise
- Internet
- Associations
- Vie privée
---

> Désirons-nous réellement protéger notre vie privée alors que nous l’exposons sur les réseaux sociaux?
