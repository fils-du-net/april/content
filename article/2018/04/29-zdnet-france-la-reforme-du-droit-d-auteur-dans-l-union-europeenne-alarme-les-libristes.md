---
site: ZDNet France
title: "La réforme du droit d'auteur dans l'Union européenne alarme les libristes"
author: Thierry Noisette
date: 2018-04-29
href: http://www.zdnet.fr/blogs/l-esprit-libre/la-reforme-du-droit-d-auteur-dans-l-union-europeenne-alarme-les-libristes-39867668.htm
tags:
- April
- Institutions
- Associations
- Droit d'auteur
- Europe
---

> L’impact potentiellement nuisible de ce projet de réforme, pointé depuis des mois, a suscité une lettre ouverte de 147 organisations au Conseil de l’Union européenne. Qui n’est pour l’instant pas parvenu à s’accorder.
