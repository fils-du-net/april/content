---
site: Next INpact
title: "Collecte de données via les sites de l'État: avant les impôts, le précédent de l’Élysée"
author: David Legrand
date: 2018-04-17
href: https://www.nextinpact.com/news/106478-collecte-donnees-via-sites-etat-avant-impots-precedent-lelysee.htm
tags:
- Internet
- Administration
- Institutions
- ACTA
- Vie privée
---

> Si le site de la DGFiP est critiqué pour l'utilisation d'une vidéo YouTube, imposée à ses visiteurs en pleine campagne de déclaration des revenus, qu'en est-il des autres sites publics? Recours à des plateformes de vidéos tierces, présence de nombreux traceurs et consentement non explicite sont au programme.
