---
site: Paperjam News
title: "Cinq raisons de choisir (ou pas) les logiciels libres"
date: 2018-04-20
href: http://paperjam.lu/news/cinq-raisons-de-choisir-ou-pas-les-logiciels-libres
tags:
- Sensibilisation
- Associations
---

> L’open source, un choix économique mais aussi une volonté de s’affranchir de la dépendance vis-à-vis de solutions commerciales propriétaires.
