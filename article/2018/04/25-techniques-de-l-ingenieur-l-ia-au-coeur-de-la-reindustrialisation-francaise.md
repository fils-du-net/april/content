---
site: Techniques de l'Ingénieur
title: "L’IA, au coeur de la réindustrialisation française"
author: Matthieu Combe
date: 2018-04-25
href: https://www.techniques-ingenieur.fr/actualite/articles/lia-au-coeur-de-la-reindustrialisation-francaise-54338
tags:
- Institutions
- Sciences
---

> Le rapport Villani sur l'intelligence artificielle a été publié fin mars. Dans la foulée, Emmanuel Macron a repris une large série de préconisations en faveur de cette technologie pour lancer un plan complet de développement.
