---
site: L'Informaticien
title: "Après YouTube… PeerTube? Déjà des centaines d’instances!"
author: Charlie Braume
date: 2018-04-19
href: https://www.linformaticien.com/actualites/id/49046/apres-youtube-peertube-deja-des-centaines-d-instances.aspx
tags:
- Internet
- Institutions
- Associations
---

> Soutenu par Framasoft, le projet de solution de vidéo en streaming peer to peer open source arrive en version bêta.
