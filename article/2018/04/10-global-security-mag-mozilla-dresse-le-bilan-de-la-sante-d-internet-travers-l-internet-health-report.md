---
site: Global Security Mag
title: "Mozilla dresse le bilan de la santé d’Internet à travers l’Internet Health Report 2018"
author: Mozilla
date: 2018-04-10
href: https://www.globalsecuritymag.fr/Mozilla-dresse-le-bilan-de-la,20180410,77978.html
tags:
- Internet
- Associations
- Désinformation
- Vie privée
---

> Mozilla lance la deuxième édition de l’Internet Health Report, son initiative open source qui vise à explorer la santé d’Internet.
