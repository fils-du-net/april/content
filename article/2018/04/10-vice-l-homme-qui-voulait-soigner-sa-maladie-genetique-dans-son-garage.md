---
site: VICE
title: "L'homme qui voulait soigner sa maladie génétique dans son garage"
author: Sébastien Wesolowski
date: 2018-04-10
href: https://www.vice.com/fr_ca/article/8xk5jp/lhomme-qui-voulait-soigner-sa-maladie-genetique-dans-son-garage
tags:
- Internet
- Sciences
---

> Chris souffre de rétinite pigmentaire, une maladie génétique incurable. Dans cinq à dix ans, il sera aveugle. Sans formation médicale, et avec le seul aide d'Internet, il a décidé de créer son propre traitement à coups de manipulations génétiques.
