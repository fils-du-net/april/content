---
site: Contexte
title: "À Bruxelles, la guerre de la responsabilité des plateformes se prépare déjà"
author: Camille Fortin, Laura Kayali
date: 2018-04-18
href: https://www.contexte.com/article/numerique/a-bruxelles-la-guerre-de-la-responsabilite-des-plateformes-se-prepare-deja_85959.html
tags:
- Entreprise
- Internet
- Institutions
- Europe
---

> La Commission Juncker a promis de ne pas rouvrir la directive e-commerce. Mais après 2019, tout est possible. Alors que Paris, Londres et Berlin semblent en faveur d’une redéfinition du cadre, l’industrie numérique anticipe déjà les débats à venir.
