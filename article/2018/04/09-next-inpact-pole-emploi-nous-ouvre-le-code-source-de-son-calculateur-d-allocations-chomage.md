---
site: Next INpact
title: "Pôle emploi nous ouvre le code source de son calculateur d’allocations chômage"
author: Xavier Berne
date: 2018-04-09
href: https://www.nextinpact.com/news/106435-pole-emploi-nous-ouvre-code-source-son-calculateur-dallocations-chomage.htm
tags:
- Administration
- Open Data
- Vie privée
---

> Suite à une demande «CADA» de Next INpact, Pôle emploi vient d’ouvrir le code source de son calculateur d’allocations d’aide au retour à l’emploi (ARE).
