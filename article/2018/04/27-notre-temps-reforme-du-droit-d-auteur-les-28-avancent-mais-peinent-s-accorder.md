---
site: Notre temps
title: "Réforme du droit d'auteur: les 28 avancent mais peinent à s'accorder"
date: 2018-04-27
href: https://www.notretemps.com/high-tech/actualites/reforme-du-droit-d-auteur-les-28-afp-201804,i167844
tags:
- Institutions
- Droit d'auteur
- Europe
---

> Les 28 de l'UE avancent mais peinent à s'accorder sur la très attendue réforme du droit d'auteur, qui ambitionne de pousser les plateformes à mieux rémunérer éditeurs de presse et artistes pour leurs productions accessibles en ligne.
