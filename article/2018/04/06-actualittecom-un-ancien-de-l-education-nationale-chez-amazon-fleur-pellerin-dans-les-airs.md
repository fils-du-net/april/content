---
site: ActuaLitté.com
title: "Un ancien de l'Éducation nationale chez Amazon, Fleur Pellerin dans les airs"
author: Antoine Oury
date: 2018-04-06
href: https://www.actualitte.com/article/monde-edition/un-ancien-de-l-education-nationale-chez-amazon-fleur-pellerin-dans-les-airs/88233
tags:
- Entreprise
- Éducation
---

> Dans le secteur de la culture aussi, les déplacements entre le secteur public et le secteur privé font partie intégrante d'une carrière de politique qui se respecte. À quelques jours d'intervalle, on apprend ainsi que Fleur Pellerin, ancienne ministre de la Culture du gouvernement Valls, siègera au conseil de surveillance de la compagnie aérienne néerlandaise KLM, tandis qu'un ancien du ministère de l'Éducation nationale pointe chez Amazon.
