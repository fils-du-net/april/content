---
site: Telquel.ma
title: "Quelles alternatives face à Google, Facebook et autres géants de l'Internet?"
author: Thibault de Seilhac
date: 2018-04-25
href: http://telquel.ma/2018/04/25/quelles-alternatives-face-aux-services-proposes-google-facebook-autres-geants-linternet_1589865
tags:
- Internet
- Promotion
- Vie privée
---

> Après le déluge qu'a suscité l'affaire Cambridge Analytica et toutes les problématiques qu'elle a soulevée, de nombreux internautes sont désormais soucieux quant à la protection des données personnelles sur leurs appareils connectés. Telquel.ma vous propose quelques alternatives pour une meilleure hygiène numérique.
