---
site: francetv info
title: "Des sorties de classe au musée? Non, dans un magasin Apple!"
date: 2018-04-03
href: https://www.francetvinfo.fr/societe/education/numerique-a-l-ecole/video-des-sorties-de-classe-au-musee-non-dans-un-magasin-apple_2688262.amp
tags:
- Entreprise
- Éducation
---

> À l’école, une sortie de classe en général, c'est une visite de musée, la découverte d'un monument ou d'une pièce de théâtre. Mais aujourd’hui, c’est dépassé: des écoles publiques emmènent aussi vos enfants… dans des magasins Apple! Pour la marque à la pomme, quels bénéfices à votre avis?
