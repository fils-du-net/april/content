---
site: ledauphine.com
title: "Ce qu'il faut savoir sur la taxe Gafa (enfin) mise en place en 2019"
date: 2018-12-17
href: https://www.ledauphine.com/france-monde/2018/12/17/taxe-gafa-les-geants-du-numerique-taxes-des-le-1er-janvier
tags:
- Entreprise
- Économie
- Institutions
- Europe
---

> Sans attendre une décision de Bruxelles, la France commencera à prélever la taxe sur les géants du numérique(Google, Apple, Facebook, Amazon, etc...) à partir du 1er janvier.
