---
site: TV Com
title: "Le Roi et la Reine en visite dans la société Odoo à Ramillies"
author: François Namur
date: 2018-12-11
href: https://www.tvcom.be/video/info/economie/le-roi-et-la-reine-en-visite-dans-la-societe-odoo-a-ramillies_23114_89.html
tags:
- Entreprise
- Sensibilisation
- International
---

> Le Roi Philippe et la Reine Mathilde étaient en visite en Brabant wallon, aujourd'hui, et plus précisément dans l'est de la province. Première étape de leur journée: la société Odoo, implantée dans plusieurs fermes à Ramillies.
