---
site: Capital.fr
title: "Taxe Gafa: ce que les géants d'Internet paient en réalité comme impôts"
author: Ambre Deharo
date: 2018-12-16
href: https://www.capital.fr/entreprises-marches/taxe-gafa-ce-que-les-geants-dinternet-paient-en-realite-comme-impots-1320060
tags:
- Entreprise
- Économie
- Institutions
- Europe
---

> Bruno Le Maire entend mettre en place dès 2019 une taxe sur les bénéfices publicitaires réalisés par Google, Apple, Facebook et Amazon, entre autres, en France.
