---
site: "cio-online.com"
title: "Adopter le logiciel libre sereinement mais résolument"
author: Bertrand Lemaire
date: 2018-12-07
href: https://www.cio-online.com/actualites/lire-adopter-le-logiciel-libre-sereinement-mais-resolument-10847.html
tags:
- Entreprise
- Économie
- Promotion
---

> Le Cigref vient de publier les conclusions de son groupe de travail «L'open source, une alternative aux grands fournisseurs.»
