---
site: ZDNet France
title: "Taxe Gafa: la France commence le 1er janvier (toute seule? oui)"
date: 2018-12-17
href: https://www.zdnet.fr/actualites/taxe-gafa-la-france-commence-le-1er-janvier-toute-seule-oui-39878185.htm
tags:
- Entreprise
- Économie
- Institutions
- Europe
---

> Annoncée il y a quelques jours, la mise en place de cette taxe serait effective dès le premier jour de l'année prochaine selon le ministre des finances français. De quoi prendre de vitesse des partenaires européens hésitants.
