---
site: ZDNet France
title: "L'Union européenne finance la chasse aux bugs dans 15 logiciels libres"
author: Thierry Noisette
date: 2018-12-30
href: https://www.zdnet.fr/blogs/l-esprit-libre/l-union-europeenne-finance-la-chasse-aux-bugs-dans-15-logiciels-libres-39878543.htm
tags:
- Institutions
- Innovation
- Europe
series:
- 201852
series_weight: 0
---

> Ouvert dès janvier 2019, ce "bug bounty" annoncé par l’eurodéputée Julia Reda concerne entre autres KeePass, VLC, 7-zip, Drupal et FileZilla.
