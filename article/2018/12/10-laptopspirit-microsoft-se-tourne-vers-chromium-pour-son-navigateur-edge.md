---
site: LaptopSpirit
title: "Microsoft se tourne vers Chromium pour son navigateur Edge"
author: Marine
date: 2018-12-10
href: https://www.laptopspirit.fr/230992/microsoft-edge-chromium-navigateur.html
tags:
- Entreprise
- Internet
- Standards
---

> Microsoft l'a officiellement annoncé. Il va adopter le projet open source Chromium dans le cadre du développement de son navigateur Edge.
