---
site: InformatiqueNews.fr
title: "L’open source, alternative aux grands éditeurs"
date: 2018-12-05
href: https://www.informatiquenews.fr/lopen-source-alternative-aux-grands-editeurs-59221
tags:
- Entreprise
- Économie
- Promotion
---

> L’open source est une des alternatives qui permet aux entreprises de « desserrer l’étau financier des fournisseurs et de disposer de marges de manœuvre dans leurs négociations avec les éditeurs de logiciels.
