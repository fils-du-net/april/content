---
site: BFMtv
title: "Le violent coup de gueule des DSI auprès des géants du logiciel"
author: Frédéric Simottel
date: 2018-12-14
href: https://bfmbusiness.bfmtv.com/hightech/le-violent-coup-de-gueule-des-dsi-aupres-des-geants-du-logiciel-1587967.html
tags:
- Entreprise
- Sensibilisation
- Innovation
---

> Les DSI des plus grandes entreprises françaises s’en prennent violemment aux principaux fournisseurs informatiques. Pratiques commerciales archaïques, manque de transparence, relation client bafouée, innovation bridée. Un nouveau dialogue doit s’instaurer sur des budgets. Le logiciel libre est même brandi comme fer de lance de la révolte.
