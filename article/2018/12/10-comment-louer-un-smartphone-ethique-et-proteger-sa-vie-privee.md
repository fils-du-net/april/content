---
site: UP
title: "Comment louer un smartphone éthique et protéger sa vie privée"
author: Héloïse Leussier
date: 2018-12-10
href: https://www.up-inspirer.fr/45163-comment-louer-un-smartphone-ethique-et-proteger-sa-vie-privee
tags:
- Entreprise
- Innovation
- Vie privée
---

> Commown propose la location du smartphone éthique Fairphone avec le système d’exploitation développé par le constructeur qui respecte la vie privée.
