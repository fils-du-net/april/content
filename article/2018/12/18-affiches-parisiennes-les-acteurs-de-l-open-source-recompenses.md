---
site: Affiches Parisiennes
title: "Les acteurs de l'open source récompensés"
author: Anne Moreaux
date: 2018-12-18
href: https://www.affiches-parisiennes.com/les-acteurs-de-l-open-source-recompenses-8591.html
tags:
- Entreprise
- Associations
- Promotion
---

> À l'occasion du Paris Open Source Summit, marqué par les 20 ans de l'association LinuxFr.org, cinq entreprises ou projets du libre, dont la Ville de Paris, ont reçu une distinction pour leurs contributions au développement du logiciel libre et de l'open source.
