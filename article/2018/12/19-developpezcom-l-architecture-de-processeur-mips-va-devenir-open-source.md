---
site: Developpez.com
title: "L'architecture de processeur MIPS va devenir open source"
author: Patrick Ruiz
date: 2018-12-19
href: https://www.developpez.com/actu/238139/L-architecture-de-processeur-MIPS-va-devenir-open-source-et-pourrait-fragiliser-les-efforts-de-la-communaute-avec-RISC-V
tags:
- Entreprise
- Matériel libre
- Innovation
series:
- 201851
series_weight: 0
---

> Depuis le début de cette année, on a beaucoup parlé d’architectures ouvertes avec, notamment, le lancement d’un processeur RISC-V et d’une carte de développement pour le système d’exploitation open source Linux. On poursuit avec les nouvelles de cet univers pour signaler qu’à côté d’OpenSPARC et RISC-V il faudra compter avec un nouvel arrivant. Après avoir racheté la licence d’exploitation des processeurs MIPS en juin dernier, Wave Computing annonce que l’architecture de processeur MIPS va devenir open source.
