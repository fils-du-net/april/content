---
site: ITforBusiness
title: "Des DSI en colère…"
author: Laurent Delattre
date: 2018-12-19
href: http://www.itforbusiness.fr/rss/item/10996-des-dsi-en-colere
tags:
- Entreprise
- Logiciels privateurs
series:
- 201851
---

> La coupe est pleine… Les DSI français protestent contre l’étau financier des grands éditeurs et brandissent l’étendard open-source pour retrouver des marges de manœuvre.
