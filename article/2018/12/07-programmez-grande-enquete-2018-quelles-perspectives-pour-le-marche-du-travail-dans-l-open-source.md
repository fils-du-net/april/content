---
site: Programmez!
title: "Grande enquête 2018: quelles perspectives pour le marché du travail dans l’open source?"
author: fredericmazue
date: 2018-12-07
href: https://www.programmez.com/actualites/grande-enquete-2018-quelles-perspectives-pour-le-marche-du-travail-dans-lopen-source-28347
tags:
- Entreprise
- Économie
- Promotion
---

> A l’initiative du CNLL, de Syntec Numérique et de Systematic Paris Region, le cabinet d’études Katalyse a interrogé 350 étudiants, une centaine de jeunes diplômés et une centaine d’entreprises, au quatrième trimestre 2018, pour mieux comprendre les attentes des employeurs de l’open source, leurs difficultés de recrutement et de fidélisation, mais aussi la perception du secteur par les futurs candidats et les jeunes salariés. Les principaux résultats de cette étude ont été présentés à l’occasion du Paris Open Source Summit, organisé les 5 et 6 décembre aux Docks de Paris.
