---
site: Developpez.com
title: "France: les géants d'Internet seront taxés dès le 1er janvier 2019 pour une recette évaluée à 500 millions d'euros"
author: Michael Guilloux
date: 2018-12-18
href: https://www.developpez.com/actu/237947/France-les-geants-d-Internet-seront-taxes-des-le-1er-janvier-2019-pour-une-recette-evaluee-a-500-millions-d-euros-encore-une-mesurette-symbolique
tags:
- Entreprise
- Économie
- Institutions
- Informatique en nuage
- Europe
---

> «Je ne lâcherai rien», avait promis il y a moins de deux semaines le ministre français des Finances à propos de la taxation des GAFA. «L’argent, il est chez les géants du numérique, qui font des profits considérables grâce aux consommateurs français, grâce au marché français, et qui payent 14 points d’imposition en moins que les autres entreprises, que les PME, que les TPE, que l'industrie française», a lancé Bruno Le Maire. Le ministre a ensuite ajouté qu'il se donne jusqu'au mois de mars 2019 pour qu'on obtienne une taxation européenne des géants du numérique.
