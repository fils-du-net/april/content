---
site: ZDNet France
title: "La Commission européenne privilégie le logiciel libre"
author: Thierry Noisette
date: 2018-12-03
href: https://www.zdnet.fr/blogs/l-esprit-libre/la-commission-europeenne-privilegie-le-logiciel-libre-39877511.htm
tags:
- Institutions
- Associations
- Europe
---

> Le CNLL, qui fédère 300 entreprises en France, rappelle les débats sur la préférence aux logiciels libres, rejetée par le gouvernement époque Hollande.
