---
site: "les-infostrateges.com"
title: "L'Open source: une alternative aux grands fournisseurs"
author: Fabrice Molinaro
date: 2018-12-13
href: https://www.les-infostrateges.com/actu/lopen-source-une-alternative-aux-grands-fournisseurs
tags:
- Entreprise
- Économie
- Sensibilisation
- Innovation
---

> Le Cigref est un réseau de grandes entreprises et d’administrations publiques qui a pour mission de développer la capacité de ses membres à intégrer et maîtriser le numérique. Association loi 1901, le Cigref vient de publier un rapport intitulé "Open source: une alternative aux grands fournisseurs".
