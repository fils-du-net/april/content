---
site: ITforBusiness
title: "Officiel: Microsoft Edge adopte Chromium et l’open source"
author: Laurent Delattre
date: 2018-12-06
href: http://www.itforbusiness.fr/rss/item/10963-microsoft-edge-adopte-chromium-et-l-open-source
tags:
- Entreprise
- Internet
---

> Microsoft annonce un profond changement de stratégie autour de son navigateur Web, Edge. Celui-ci ne disparaît pas mais adopte le moteur de rendu Chromium et une nouvelle philosophie de développement.
