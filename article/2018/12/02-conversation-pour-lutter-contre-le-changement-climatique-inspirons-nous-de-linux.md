---
site: The Conversation
title: "Pour lutter contre le changement climatique, inspirons-nous de Linux!"
author: Antoine Godin
date: 2018-12-02
href: https://theconversation.com/pour-lutter-contre-le-changement-climatique-inspirons-nous-de-linux-107857
tags:
- Partage du savoir
- Innovation
---

> Les dispositifs intergouvernementaux comme la COP24 ne pourront à eux seuls résoudre la crise climatique. La protection du climat est plus que jamais l’affaire de tous.
