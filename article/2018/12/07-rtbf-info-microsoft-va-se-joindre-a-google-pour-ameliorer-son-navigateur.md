---
site: RTBF Info
title: "Microsoft va se joindre à Google pour améliorer son navigateur"
date: 2018-12-07
href: https://www.rtbf.be/info/medias/detail_microsoft-va-se-joindre-a-google-pour-ameliorer-son-navigateur?id=10092179
tags:
- Entreprise
- Internet
---

> Microsoft a décidé d'utiliser la même technologie que Google Chrome pour son navigateur Edge. Il sera proposé dans le futur sur toutes les versions de Windows mais aussi sur Mac.
