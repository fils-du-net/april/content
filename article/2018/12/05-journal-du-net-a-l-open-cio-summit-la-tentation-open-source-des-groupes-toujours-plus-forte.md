---
site: Journal du Net
title: "A l'Open CIO Summit, la tentation open source des groupes toujours plus forte"
author: Antoine Crochet-Damais
date: 2018-12-05
href: https://www.journaldunet.com/solutions/dsi/1419552-open-cio-summit-l-open-source-un-levier-cle-d-innovation
tags:
- Entreprise
- Économie
- Promotion
---

> A l'Open CIO Summit, la tentation open source des groupes toujours plus forte Les DSI du Cac 40 mettent en avant le rôle des logiciels libres pour passer outre les technologies commerciales et propriétaires. Avec des indicateurs de résultats financiers à la clef.
