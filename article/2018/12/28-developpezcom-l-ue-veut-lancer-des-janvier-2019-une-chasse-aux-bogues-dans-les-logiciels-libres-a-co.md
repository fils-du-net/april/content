---
site: Developpez.com
title: "L'UE veut lancer, dès janvier 2019, une chasse aux bogues dans les logiciels libres à code source ouvert"
author: Bill Fassinou
date: 2018-12-28
href: https://www.developpez.com/actu/239282/L-UE-veut-lancer-des-janvier-2019-une-chasse-aux-bogues-dans-les-logiciels-libres-a-code-source-ouvert-pour-renforcer-la-securite-sur-Internet
tags:
- Institutions
- Innovation
- Europe
series:
- 201852
series_weight: 0
---

> L’Union européenne compte lancer dans le mois prochain, une chasse aux bogues avec des plateformes de Bug Bounty telles que HackerOne ou Intigriti. L’UE a émis 14 primes de bogues sur des projets liés au logiciels libres sur lesquels les institutions au sein de l’Union s’appuient pour effectuer leurs diverses activités. Ces plateformes se présentent pour la plupart comme étant des regroupements de “hackers éthiques” ou plateformes de sécurité optimisées par les pirates.
