---
site: Developpez.com
title: "France: le Sénat réclame à nouveau le «code source» de chaque projet de loi de finances"
author: Stan Adkens
date: 2018-12-18
href: https://www.developpez.com/actu/237975/France-le-Senat-reclame-a-nouveau-le-code-source-de-chaque-projet-de-loi-de-finances-mais-la-requete-semble-encore-impossible-a-satisfaire
tags:
- Administration
- Institutions
- Informatique en nuage
- Open Data
---

> Comme l’année dernière à la même période, le Sénat français a adopté cette année encore un amendement obligeant l’administration fiscale à dévoiler, pour chaque projet de loi de finances, le «code source» traduisant les réformes proposées par l’exécutif. Toutefois, comme ce fut le cas l’an dernier, le nouvel amendement pourrait être annulé par les députés sur avis du gouvernement.
