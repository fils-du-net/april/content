---
site: ZDNet France
title: "Logiciel et connaissances libres: pour les dons, c'est maintenant... et toute l'année"
author: Thierry Noisette
date: 2018-12-31
href: https://www.zdnet.fr/blogs/l-esprit-libre/logiciel-et-connaissances-libres-pour-les-dons-c-est-maintenant-et-toute-l-annee-39878559.htm
tags:
- April
- Associations
- Promotion
series:
- 201852
series_weight: 0
---

> Fichtre, déjà la fin de l'année! Plus que quelques heures pour les dons (déductibles) aux associations... ou en fait toute l'année - mais pensez-y.
