---
site: ChannelNews
title: "Les grandes entreprises ne supportent plus leur dépendance vis-à-vis des grands éditeurs"
author: Dirk Basyn
date: 2018-12-20
href: https://www.channelnews.fr/les-grandes-entreprises-ne-supportent-plus-leur-dependance-vis-a-vis-des-grands-editeurs-86169
tags:
- Entreprise
- Logiciels privateurs
- Vente liée
series:
- 201851
series_weight: 0
---

> L’exaspération monte au sein des grandes entreprises et administrations à l’encontre des grands éditeurs, généralement installés aux Etats-Unis.
