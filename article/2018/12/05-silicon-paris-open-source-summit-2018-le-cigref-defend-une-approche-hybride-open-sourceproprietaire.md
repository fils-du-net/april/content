---
site: silicon
title: "Paris Open Source Summit 2018: le Cigref défend une approche hybride open source/propriétaire"
author: Ariane Beky
date: 2018-12-05
href: https://www.silicon.fr/open-source-approche-hybride-open-source-proprietaire-227257.html?inf_by=5c0bff91671db851108b54c5
tags:
- Entreprise
- Économie
- Promotion
---

> L’engagement des entreprises dans des solutions open source se fait rarement sur le parc existant. Mais une stratégie hybride peut convaincre les métiers et le Comex.
