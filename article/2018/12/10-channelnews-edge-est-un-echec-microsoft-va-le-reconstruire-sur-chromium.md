---
site: ChannelNews
title: "Edge est un échec, Microsoft va le reconstruire sur Chromium"
author: Dirk Basyn
date: 2018-12-10
href: https://www.channelnews.fr/edge-est-un-echec-microsoft-va-le-reconstruire-sur-chromium-85883
tags:
- Entreprise
- Internet
- Standards
---

> Confirmant une rumeur qui circulait depuis quelques temps, Microsoft annonce sur son blog son intention d’adopter le projet open source Chromium dans le développement de Microsoft Edge «afin de créer une meilleure compatibilité Web pour les clients et une fragmentation moindre du Web pour tous les développeurs».
