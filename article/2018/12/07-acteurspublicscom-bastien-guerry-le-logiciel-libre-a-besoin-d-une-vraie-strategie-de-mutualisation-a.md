---
site: Acteurs publics
title: "Bastien Guerry: “Le logiciel libre a besoin d’une vraie stratégie de mutualisation au sein de l’État”"
author: Émile Marzolf
date: 2018-12-07
href: https://www.acteurspublics.com/2018/12/07/bastien-guerry-le-logiciel-libre-a-besoin-d-une-vraie-strategie-de-mutualisation-au-sein-de-l-etat
tags:
- Administration
- Sensibilisation
- Désinformation
- Open Data
---

> Le tout nouveau “référent logiciels libres” d’Etalab, Bastien Guerry, ancien entrepreneur d’intérêt général, explique à Acteurs publics comment le logiciel libre gagne du terrain dans l’administration et à quels défis il se heurte.
