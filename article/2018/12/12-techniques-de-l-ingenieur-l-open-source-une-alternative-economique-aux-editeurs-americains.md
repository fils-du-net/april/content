---
site: Techniques de l'Ingénieur
title: "L’Open Source: une alternative économique aux éditeurs américains"
author: Philippe Richard
date: 2018-12-12
href: https://www.techniques-ingenieur.fr/actualite/articles/lopen-source-une-alternative-economique-aux-editeurs-americains-61556
tags:
- Entreprise
- Sensibilisation
- Innovation
---

> De plus en plus d’entreprises françaises se plaignent de l’augmentation des coûts engendrés par les logiciels des poids lourds américains (Oracle, SAP, Microsoft…). L’Open source permet de réduire ces coûts et la dépendance vis-à-vis de ces géants de l’informatique.
