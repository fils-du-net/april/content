---
site: Developpez.com
title: "Si Linux a de la peine à s'imposer sur le desktop c'est à cause de la fragmentation de l'écosystème"
author: Patrick Ruiz
date: 2018-12-11
href: https://www.developpez.com/actu/237125/Si-Linux-a-de-la-peine-a-s-imposer-sur-le-desktop-c-est-a-cause-de-la-fragmentation-de-l-ecosysteme-d-apres-Linus-Torvalds
tags:
- Sensibilisation
- Innovation
---

> Linux pointe à la 3e place du classement NetMarketShare (variations de décembre 2017 à novembre 2018) des OS de bureau avec 2,03 % des parts de marché, ce, derrière Windows qui mobilise 87,92 % et Mac OS avec ses 9,46 %. Le système d’exploitation open source créé par l’informaticien finlandais Linus Torvalds a frôlé la barre des 5 % en septembre 2017, mais c’était par erreur; à la réalité, il a rarement dépassé les 3 %. Dans la filière desktop, c’est l’OS de la firme de Redmond qui continue de faire la loi.
