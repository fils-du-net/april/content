---
site: The Conversation
title: "Débat: L’«open science», une expression floue et ambiguë"
author: Alexandre Hocquet
date: 2018-12-05
href: https://theconversation.com/debat-l-open-science-une-expression-floue-et-ambigue-108187
tags:
- Partage du savoir
- Sciences
- Open Data
---

> Qu’est-ce que la «science ouverte»? En quoi la science actuelle est elle «fermée» et que recouvre l’expression «ouvrir la science» comme l’indique le slogan des journées?
