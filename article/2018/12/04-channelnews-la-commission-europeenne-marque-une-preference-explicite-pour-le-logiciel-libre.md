---
site: ChannelNews
title: "La commission européenne marque une préférence explicite pour le logiciel libre"
author: Dirk Basyn
date: 2018-12-04
href: https://www.channelnews.fr/la-commission-europeenne-marque-une-preference-explicite-pour-le-logiciel-libre-85724
tags:
- Institutions
- Associations
- Europe
---

> La Commission européenne vient de publier un document sur sa stratégie numérique. Il s’agit d’un vaste plan de modernisation qui devrait être formulé en 2019 pour une mise en application dès 2022. Ce plan doit répondre aux besoins spécifiques de la Commission mais également servir d’épine dorsale pour l’ensemble des services publics à l’échelle de l’Union européenne.
