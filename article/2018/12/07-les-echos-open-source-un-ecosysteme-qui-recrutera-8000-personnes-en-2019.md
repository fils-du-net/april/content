---
site: Les Echos
title: "Open Source: un écosystème qui recrutera 8.000 personnes en 2019"
author: Clara Le Stum
date: 2018-12-07
href: https://start.lesechos.fr/rejoindre-une-entreprise/actu-recrutement/open-source-un-ecosysteme-qui-recrutera-8-000-personnes-en-2019-13598.php
tags:
- Entreprise
- Économie
- Promotion
---

> Le secteur de l’open source ne cesse de se développer et les effectifs augmentent chaque année pour atteindre quasiment 60.000 emplois en 2018.
