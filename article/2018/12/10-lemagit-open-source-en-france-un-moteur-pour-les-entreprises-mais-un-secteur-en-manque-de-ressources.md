---
site: LeMagIT
title: "Open source en France: un moteur pour les entreprises, mais un secteur en manque de ressources"
author: Cyrille Chausson
date: 2018-12-10
href: https://www.lemagit.fr/actualites/252454152/LOpen-source-en-France-un-moteur-premier-pour-les-entreprises-mais-un-secteur-en-manque-de-resso
tags:
- Entreprise
- Sensibilisation
- Innovation
---

> Si le secteur de l’open source en France profite du dynamisme de la transition des entreprises vers le numérique, une étude du CNLL et de Syntec Numérique montre un secteur aux prises avec le recrutement. L’open source cherchera à recruter 8 200 personnes en 2019.
