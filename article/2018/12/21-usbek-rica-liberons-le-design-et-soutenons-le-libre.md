---
site: Usbek & Rica
title: "Libérons le design et soutenons le libre"
author: Bruno Loton
date: 2018-12-21
href: https://usbeketrica.com/article/ce-que-le-design-peut-avoir-d-ethique-est-d-etre-libre
tags:
- Internet
- Innovation
series:
- 201851
series_weight: 0
---

> Une réflexion, qu’on peut penser tout à fait légitime, a envahi l'esprit des designers depuis quelques années. Il s’agit de l’éthique dans la pratique et la finalité de notre métier. La question a été massivement diffusée à partir de 2015 par Tristan Harris lorsqu’il quitta Google où il était précisément «Design Ethicist», pour fonder l’organisation Time Well Spent à l’initiative du mouvement du même nom. Un mouvement qui a pour objectif de mobiliser les designers sur la problématique du respect de l’attention des utilisateurs par les produits et services que nous concevons.
