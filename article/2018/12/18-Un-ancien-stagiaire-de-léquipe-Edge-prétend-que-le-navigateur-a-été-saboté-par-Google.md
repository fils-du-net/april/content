---
site: Developpez.com
title: "Un ancien stagiaire de l'équipe Edge prétend que le navigateur a été saboté par Google"
description: Ce qui aurait poussé Microsoft à passer à Chromium
author: Michael Guilloux
date: 2018-12-18
href: https://www.developpez.com/actu/237959/Un-ancien-stagiaire-de-l-equipe-Edge-pretend-que-le-navigateur-a-ete-sabote-par-Google-ce-qui-aurait-pousse-Microsoft-a-passer-a-Chromium
featured_image: https://www.developpez.com/public/images/news/edgechrome1.jpg
tags:
- Internet
- Entreprise
- Standards
series:
- 201851
series_weight: 0
---

> Il y a une dizaine de jours, Microsoft a annoncé l'abandon de son moteur de rendu EdgeHTML et le passage à Chromium pour le développement de son navigateur Microsoft Edge. Justifiant cette décision, l'éditeur de Windows dit vouloir à travers ce changement créer une meilleure compatibilité Web pour ses utilisateurs et réduire la fragmentation du Web pour tous les développeurs Web. Ce qui devrait au passage régler un gros problème avec Microsoft Edge, à savoir le fait que certains sites Web ne fonctionnent pas correctement sur le navigateur.
