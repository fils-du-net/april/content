---
site: Echo Sciences
title: "Le FabLab, un lieu d’émancipation sociale: discours ou réalité?"
author: Eléonore Pérès
date: 2018-12-05
href: https://www.echosciences-grenoble.fr/communautes/le-master-cst/articles/le-fablab-un-lieu-d-emancipation-sociale-discours-ou-realite
tags:
- Partage du savoir
- Accessibilité
- Innovation
- Open Data
---

> Pour qui s’intéresse un peu à la culture scientifique et technique et à sa place en société, le terme FabLab ne doit pas être inconnu. Il résonne beaucoup dans la sphère de la médiation des sciences et dans les médias depuis quelques années. A travers les idées de “faire soi-même” et “d’apprendre en faisant”, les FabLabs sont souvent synonymes d’inclusion sociale et de capacitation (ou «empowerment»). Mais qu’en est-il de la réalité?
