---
site: Clubic.com
title: "Firefox Monitor vous dit si vos données ont été volées"
author: Matthieu Legouge
date: 2018-09-27
href: https://www.clubic.com/navigateur-internet/mozilla-firefox/actualite-845741-sr-ludo-firefox-monitor-donnees-volees.html
tags:
- Internet
- Associations
- Vie privée
---

> Firefox lance Firefox Monitor, un service qui vous veut du bien puisqu'il est gratuit et vous préviendra en cas de fuite de vos données personnelles.
