---
site: Developpez.com
title: "Directive Copyright: Mozilla monte encore au créneau"
author: Michael Guilloux
date: 2018-09-08
href: https://www.developpez.com/actu/223071/Directive-Copyright-Mozilla-monte-encore-au-creneau-contre-la-nouvelle-proposition-de-loi-europeenne-sur-les-droits-d-auteur
tags:
- Internet
- Institutions
- Associations
- Droit d'auteur
- Europe
---

> Le mercredi 12 septembre, les membres du Parlement européen vont se prononcer pour de bon sur les nouvelles règles sur le droit d'auteur ; lesquelles pourraient nuire fondamentalement à Internet en Europe. En effet, si elles sont adoptées, les nouvelles règles forceront les services en ligne à surveiller et filtrer automatiquement tout contenu qui sera mis en ligne par les utilisateurs (article 13). Mais aussi, tous ceux qui créent des liens et utilisent des extraits d'articles de presse devraient d'abord payer l'éditeur pour obtenir une licence (article 11).
