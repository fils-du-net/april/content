---
site: Les Echos
title: "Le fablab où l’on apprend à se réparer soi-même"
author: Hélène Bielak
date: 2018-09-14
href: https://start.lesechos.fr/entreprendre/actu-startup/le-fablab-ou-l-on-apprend-a-se-reparer-soi-meme-12872.php
tags:
- Partage du savoir
- Matériel libre
- Associations
- Innovation
---

> A Rennes, le Humanlab propose aux personnes handicapées de fabriquer les équipements dont elles ont besoin pour améliorer leur quotidien. Cette semaine, le lieu a fait sa rentrée et a ouvert ses portes aux nouveaux porteurs de projet.
