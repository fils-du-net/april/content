---
site: Developpez.com
title: "Un logiciel libre doit-il être en mesure de restreindre les tâches que ses utilisateurs peuvent effectuer avec son aide?"
author: Stéphane le calme
date: 2018-09-12
href: https://www.developpez.com/actu/223844/Un-logiciel-libre-doit-il-etre-en-mesure-de-restreindre-les-taches-que-ses-utilisateurs-peuvent-effectuer-avec-son-aide-Non-pour-Richard-Stallman
tags:
- Philosophie GNU
---

> Avant d’aborder le sujet, il est important de s’arrêter un moment sur la définition même de logiciel libre. «Logiciel libre» [free software] désigne des logiciels qui respectent la liberté des utilisateurs. En gros, cela veut dire que les utilisateurs ont la liberté d'exécuter, copier, distribuer, étudier, modifier et améliorer ces logiciels. Ainsi, «logiciel libre» fait référence à la liberté, pas au prix (pour comprendre ce concept, vous devez penser à «liberté d'expression», pas à «entrée libre»).
