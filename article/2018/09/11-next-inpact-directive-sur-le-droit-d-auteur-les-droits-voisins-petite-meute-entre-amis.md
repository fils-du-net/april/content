---
site: Next INpact
title: "Directive sur le droit d’auteur: les droits voisins, petite meute entre amis"
author: Mars Rees
date: 2018-09-11
href: https://www.nextinpact.com/news/107014-directive-sur-droit-dauteur-droits-voisins-petite-meute-entre-amis.htm
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> L’approche de l’examen au Parlement européen de la proposition de directive sur le droit d’auteur est source d’une certaine hystérie chez ses partisans. Entre les manifestations non loin du ministère de la Culture, les différentes tribunes et autres articles à charge, les esprits s’échauffent quand la pudeur recule.
