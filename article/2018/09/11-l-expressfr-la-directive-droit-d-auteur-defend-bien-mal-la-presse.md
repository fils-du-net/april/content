---
site: L'Express.fr
title: "La directive droit d'auteur défend bien mal la presse"
author: Eric Mettout
date: 2018-09-11
href: https://www.lexpress.fr/actualite/medias/la-directive-copyright-defend-bien-mal-la-presse_2034574.html
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Les partisans de la directive droit d'auteur sur la presse protègent-ils la liberté d'expression ou leurs propres intérêts?
