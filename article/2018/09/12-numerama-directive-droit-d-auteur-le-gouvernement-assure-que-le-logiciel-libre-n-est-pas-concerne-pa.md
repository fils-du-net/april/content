---
site: Numerama
title: "Directive droit d'auteur: le gouvernement assure que le logiciel libre n'est pas concerné par le filtrage"
author: Julien Lausson
date: 2018-09-12
href: https://www.numerama.com/politique/417104-directive-droit-dauteur-le-gouvernement-assure-que-le-logiciel-libre-nest-pas-concerne-par-le-filtrage.html
tags:
- Internet
- April
- Institutions
- Droit d'auteur
- Europe
---

> Le gouvernement a répondu à une députée qui s'inquiétait des effets de l'article 13 de la réforme du droit d'auteur sur le logiciel libre. Paris assure que les plateformes de développement open source ne sont pas concernées.
