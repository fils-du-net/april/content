---
site: Developpez.com
title: "Un code de mérite pour gérer le travail collaboratif par le mérite technique"
author: Olivier Famien
date: 2018-09-28
href: https://www.developpez.com/actu/226652/Un-code-de-merite-pour-gerer-le-travail-collaboratif-par-le-merite-technique-solution-ideale-pour-gerer-les-projets-ou-initiative-non-applicable
tags:
- Innovation
---

> La gestion du travail collaboratif appelle nécessairement à gérer une diversité de personnes. Pour éviter des écarts de langages et de réactions entre les contributeurs d’un projet, certaines personnes et entreprises n’hésitent pas à élaborer un code de conduite que les membres du groupe devront respecter afin de pouvoir maintenir un climat sain et une bonne collaboration au sein des équipes.
