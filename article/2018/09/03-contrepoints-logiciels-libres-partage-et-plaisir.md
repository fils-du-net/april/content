---
site: Contrepoints
title: "Logiciels libres: partage et plaisir"
author: Marien Fressinaud
date: 2018-09-03
href: https://www.contrepoints.org/2018/09/03/324110-logiciels-libres-partage-et-plaisir
tags:
- Sensibilisation
- Philosophie GNU
---

> Si certains logiciels libres sont réputés à la fois pour leur efficacité et leur esthétique fonctionnelle (qu’on nommera design, parce que c’est ainsi), il faut reconnaître qu’ils ne font pas la majorité.
