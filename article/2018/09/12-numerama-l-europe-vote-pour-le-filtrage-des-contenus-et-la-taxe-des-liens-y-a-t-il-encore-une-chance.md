---
site: Numerama
title: "L’Europe vote pour le filtrage des contenus et la taxe des liens: y a-t-il encore une chance de revenir en arrière?"
author: Julien Lausson
date: 2018-09-12
href: https://www.numerama.com/politique/417195-leurope-vote-pour-le-filtrage-des-contenus-et-la-taxe-des-liens-y-a-t-il-encore-une-chance-de-revenir-en-arriere.html
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Le Parlement européen a voté mercredi 12 septembre en faveur du projet de réforme de la directive sur le droit d'auteur, qui contient deux articles polémiques. D'autres étapes vont suivre.
