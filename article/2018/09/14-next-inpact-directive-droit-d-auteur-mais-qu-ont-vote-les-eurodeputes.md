---
site: Next INpact
title: "Directive Droit d'auteur: mais qu'ont voté les eurodéputés?"
author: Marc Rees
date: 2018-09-14
href: https://www.nextinpact.com/news/107010-directive-droit-dauteur-mais-quont-vote-eurodeputes.htm
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Les eurodéputés ont adopté à une large majorité la proposition de directive sur le droit d’auteur. Succès pour les sociétés de gestion collective, échec du côté d’un vaste ensemble d’opposants, bien au-delà des seuls géants du Net. Mais qu’ont vraiment voté les parlementaires, au fil des nombreux amendements, en particulier sur les articles 11 et 13?
