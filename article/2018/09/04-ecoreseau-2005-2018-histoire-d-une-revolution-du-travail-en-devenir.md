---
site: EcoRéseau Business
title: "2005-2018: histoire d’une révolution du travail en devenir"
author: Olivier Magnan
date: 2018-09-04
href: https://www.ecoreseau.fr/societe/retrospective/2018/09/04/2005-2018-histoire-dune-revolution-du-travail-en-devenir
tags:
- Entreprise
- Économie
- Innovation
---

> En 2005, à San Francisco, s’ouvre dans un collectif féminin, Spiral Muse, un curieux espace, sorte de communauté de travail où des passionnés par l’open source, l’ouverture des codes logiciels entre programmeurs, échangent idées et savoir-faire. Son créateur, Brad Neuberg, le nomme Citizen Space. Un «espace citoyen» dont il résume le concept par un mot, « coworking ». Treize ans plus tard, le phénomène envahit la planète.
