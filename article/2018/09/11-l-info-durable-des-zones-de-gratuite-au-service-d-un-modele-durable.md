---
site: L'info durable
title: "Des zones de gratuité au service d'un modèle durable"
author: Emmanuelle Vibert
date: 2018-09-11
href: https://www.linfodurable.fr/conso/des-zones-de-gratuite-au-service-dun-modele-durable-6023
tags:
- Économie
---

> Partout, à la marge, des zones de gratuité se développent, en résistance à une marchandisation forcenée du monde. L’essayiste Paul Ariès propose d’amplifier ce mouvement, pour construire une société soutenable et joyeuse.
