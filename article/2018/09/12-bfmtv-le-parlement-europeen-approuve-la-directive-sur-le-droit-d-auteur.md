---
site: BFMtv
title: "Le Parlement européen approuve la directive sur le droit d’auteur"
author: Olivier Laffargue
date: 2018-09-12
href: https://www.bfmtv.com/tech/le-parlement-europeen-approuve-la-directive-sur-le-droit-d-auteur-1522021.html
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Le Parlement européen s’est penché ce mercredi sur une réforme très contestée du copyright. Le texte, rejeté dans un premier temps en juillet, a finalement été adopté, dans une version remaniée.
