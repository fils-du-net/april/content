---
site: L'Informaticien
title: "Code de conduite Linux: les développeurs sont-ils des «connards»?"
author: Guillaume Périssat
date: 2018-09-18
href: https://www.linformaticien.com/actualites/id/50301/code-de-conduite-linux-les-developpeurs-sont-ils-des-connards.aspx
tags:
- Innovation
---

> Est-ce la fin de Linux, de sa communauté, de l’open source? C’est ce que semble croire un certain nombre de personnes sur Reddit et Twitter après que la direction du projet Linux ait mis en place une code de conduite. Assurément, un environnement respectueux et inclusif signifierait la mort du kernel!
