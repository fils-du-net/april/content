---
site: Developpez.com
title: "Le projet Linux adopte à son tour le \"contributor Covenant Code of Conduct\""
author: Coriolan
date: 2018-09-17
href: https://www.developpez.com/actu/224656/Le-projet-Linux-adopte-a-son-tour-le-contributor-Covenant-Code-of-Conduct-pour-promouvoir-un-environnement-inclusif-et-accueillant-sans-toxicite
tags:
- Innovation
---

> Pendant des années, la communauté de développement de Linux a fait face à une vague de critiques qui a stipulé que la culture du projet open source est synonyme d’exclusion et de pratiques abusives. En 2015, la communauté a adopté le Code of Conflict (code de conflit). Bien qu’applaudi et considéré comme étant une étape vers le droit chemin, certains ont regretté que ce code qui agit plutôt comme une méthode pour résoudre les conflits, ne fasse pas assez pour changer les normes adoptées au sein de la communauté de développement de Linux.
