---
site: Le Monde.fr
title: "La directive de l’UE sur le droit d’auteur à l’heure du numérique est adoptée"
author: Damien Leloup, Martin Untersinger
date: 2018-09-12
href: https://mobile.lemonde.fr/pixels/article/2018/09/12/le-parlement-europeen-adopte-la-directive-sur-le-droit-d-auteur-a-l-heure-du-numerique_5354024_4408996.html
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Ce texte, censé encadrer le copyright dans l’Union européenne, a été approuvé par les eurodéputés, qui en ont modifié certains contours.
