---
site: "ouest-france.fr"
title: "La Baleine, un lieu alternatif pour créatifs et bricolos"
author: Nelly Cloarec
date: 2018-09-08
href: https://www.ouest-france.fr/bretagne/quimper-29000/quimper-la-baleine-un-lieu-alternatif-pour-creatifs-et-bricolos-5956282
tags:
- Administration
- Associations
---

> Plutôt que de bosser dans leur salon, une vingtaine d’artistes trouvent refuge à La Baleine, à Quimper. Ils y partagent les mètres carrés, les outils et les idées.
