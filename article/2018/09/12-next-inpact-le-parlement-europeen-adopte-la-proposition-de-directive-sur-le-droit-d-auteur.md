---
site: Next INpact
title: "Le Parlement européen adopte la proposition de directive sur le droit d'auteur"
author: Marc Rees
date: 2018-09-12
href: https://www.nextinpact.com/news/107029-le-parlement-europeen-adopte-proposition-directive-sur-droit-dauteur.htm
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Malgré un rejet du texte en juillet dernier, le Parlement européen vient d’adopter la proposition de directive présentée en 2016 par la Commission, amendée et portée par le rapporteur Axel Voss. C’est un succès pour les partisans, qui ont profité d’une conjonction des forces entre les grands titres de la presse et l’industrie culturelle.
