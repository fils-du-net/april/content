---
site: Usbek & Rica
title: "Les sciences participatives au coeur de la recherche du futur?"
date: 2018-09-28
href: https://usbeketrica.com/article/sciences-participatives-recherches-futur
tags:
- Innovation
- Sciences
---

> La science n'est pas épargnée par le partage et la contribution, et s'ouvre de plus en plus à des publics non spécialistes. Dans le cadre du Turfu Festival, Le Dôme à Caen, espace collaboratif d'innovation ouvert à tous les publics, propose à chacun différentes manières de contribuer à des programmes de recherche, et nous présente cette évolution enthousiasmante.
