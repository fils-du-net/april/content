---
site: ZDNet France
title: "Linux: Linus Torvalds fait une pause pour corriger son attitude"
date: 2018-09-17
href: https://www.zdnet.fr/actualites/linux-linus-torvalds-fait-une-pause-pour-corriger-son-attitude-39873671.htm
tags:
- Innovation
---

> Le créateur de Linux se retire de son travail sur le noyau Linux. Linus Torvalds annonce qu’il doit changer de comportement vis-à-vis des autres développeurs et s’excuse pour ceux qu’il a pu heurter.
