---
site: Télérama.fr
title: "La directive sur le droit d'auteur ne sauvera pas la presse"
author: Olivier Tesquet
date: 2018-09-11
href: https://www.telerama.fr/medias/la-directive-sur-le-droit-dauteur-ne-sauvera-pas-la-presse,n5802077.php
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Ritournelle, nom féminin, familier: idée que quelqu’un ressasse et qui revient comme un refrain dans la conversation. Exemple: mercredi 12 septembre, le Parlement européen sera amené à voter pour ou contre la directive relative au droit d’auteur. Une vieille rengaine. Début juillet, les eurodéputés avaient contre toute attente rejeté le texte, scellant une victoire tactique des géants du numérique face aux industries culturelles.
