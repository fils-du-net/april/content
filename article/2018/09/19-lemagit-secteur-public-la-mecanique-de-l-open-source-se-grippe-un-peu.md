---
site: LeMagIT
title: "Secteur public: la mécanique de l’open source se grippe (un peu)"
author: Cyrille Chausson
date: 2018-09-19
href: https://www.lemagit.fr/actualites/252448908/Secteur-public-la-mecanique-de-lopen-source-se-grippe-un-peu
tags:
- Administration
- Institutions
- Promotion
---

> Retrait de l’adhésion à la Document Foundation, les groupes de travail interministériels en perte de vitesse, ressources limitées dans les ministères, l’open source dans le secteur public français connait ces derniers mois quelques balbutiements…sauf dans les collectivités territoriales.
