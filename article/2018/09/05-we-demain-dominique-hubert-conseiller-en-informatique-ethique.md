---
site: We Demain
title: "Dominique Hubert, conseiller en informatique \"éthique\""
author: Emmanuelle Vibert
date: 2018-09-05
href: https://www.wedemain.fr/Dominique-Hubert-conseiller-en-informatique-ethique_a3529.html
tags:
- Entreprise
- Associations
---

> Deuxième volet de notre série sur les indépendants qui inventent une nouvelle forme de travail... voire de démocratie.
