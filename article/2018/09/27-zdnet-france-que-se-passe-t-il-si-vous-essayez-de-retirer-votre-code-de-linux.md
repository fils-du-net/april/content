---
site: ZDNet France
title: "Que se passe-t-il si vous essayez de retirer votre code de Linux?"
author: Steven J. Vaughan-Nichols
date: 2018-09-27
href: https://www.zdnet.fr/actualites/que-se-passe-t-il-si-vous-essayez-de-retirer-votre-code-de-linux-39874249.htm
tags:
- Droit d'auteur
- Licenses
---

> Un débat s'est ouvert dans la communauté du kernel Linux suite à l'adoption du code de conduite. Un développeur banni peut-il reprendre son code? Les experts en droit de linux du passé et du présent pèsent les implications des licences libres et open source.
