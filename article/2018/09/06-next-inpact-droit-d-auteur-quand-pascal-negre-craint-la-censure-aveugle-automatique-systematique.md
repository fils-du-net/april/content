---
site: Next INpact
title: "Droit d’auteur: quand Pascal Nègre craint la «censure aveugle, automatique, systématique»"
author: Pascal Reda
date: 2018-09-06
href: https://www.nextinpact.com/news/107004-droit-dauteur-quand-pascal-negre-craint-censure-aveugle-automatique-systematique.htm
tags:
- Entreprise
- Internet
- Institutions
- Europe
---

> Une tribune de Pascal Nègre dans une phase de respiration démocratique au Parlement européen touchant au droit d’auteur… Une intervention classique, où on aurait anticipé sans mal le sens de sa prose. Sauf que l’intéressé fait état de ses inquiétudes, d’une crainte de «censure aveugle, automatique, systématique». Pas moins.
