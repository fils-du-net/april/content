---
site: ZDNet France
title: "Linux: Linus Torvalds ne veut plus tout contrôler"
author: Louis Adam
date: 2018-09-05
href: https://www.zdnet.fr/actualites/linux-linus-torvalds-ne-veut-plus-tout-controler-39873205.htm
tags:
- Sensibilisation
- Innovation
---

> Linus Torvalds est revenu sur son rôle au sein de l’équipe de développement du kernel Linux à l’occasion de l’Open Source Summit. L’occasion pour lui d’expliquer qu’il n’est pas omniscient et que si le développement se poursuit, c’est avant tout grâce à la communauté des mainteneurs.
