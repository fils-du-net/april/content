---
site: ZDNet France
title: "C'est qui le patron sur Microsoft Azure? Linux"
author: Steven J. Vaughan-Nichols
date: 2018-09-28
href: https://www.zdnet.fr/actualites/c-est-qui-le-patron-sur-microsoft-azure-linux-39874301.htm
tags:
- Entreprise
- Informatique en nuage
---

> Le système d'exploitation le plus répandu sur le cloud Azure de Microsoft aujourd'hui est Linux. L'OS libre a poursuivi sa croissance jusqu'à représenter désormais 50% de toutes les machines virtuelles déployées sur Azure.
