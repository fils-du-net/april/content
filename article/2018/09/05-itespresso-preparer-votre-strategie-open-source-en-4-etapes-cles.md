---
site: ITespresso
title: "Préparer votre stratégie open source en 4 étapes clés"
author: Sacha Labourey
date: 2018-09-05
href: https://www.itespresso.fr/avis-expert/preparer-votre-strategie-open-source-en-4-etapes-cles
tags:
- Entreprise
- Sensibilisation
---

> Tous les professionnels de l’informatique ont dû à un moment ou un autre faire un choix entre outil open source et logiciel propriétaire. Ils sont de plus en plus nombreux aujourd’hui à se tourner vers les outils open source pour leurs besoins en matière de logiciels.
