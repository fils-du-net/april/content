---
site: Programmez!
title: "CLIP OS: un OS sécurisé en Open Source"
author: ftonic
date: 2018-09-21
href: https://www.programmez.com/actualites/clip-os-un-os-securise-en-open-source-28006
tags:
- Administration
- Innovation
---

> L’ANSII, l’agence nationale pour la sécurité (France), a annoncé le développement d’un OS sécurisé et qui visent les administrations: CLIP OS. Il repose sur un Gentoo Hardened et s’inspire beaucoup de Chromium OS et de Yocto pour le développement. Basiquement, CLIP OS s’articule autour d’un core et d’un ensemble de conteneurs pour les services, les applications.
