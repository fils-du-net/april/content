---
site: The Conversation
title: "Didier Deschamps, ou l’art de capturer la valeur"
author: Bérangère Lauren Szostak
date: 2018-09-25
href: http://theconversation.com/didier-deschamps-ou-lart-de-capturer-la-valeur-103861
tags:
- Entreprise
- Économie
- Innovation
---

> La victoire de la France en finale de la Coupe du monde malgré une faible possession de balle avait heurté les puristes du football. Les experts en stratégie des organisations, eux, applaudissent.
