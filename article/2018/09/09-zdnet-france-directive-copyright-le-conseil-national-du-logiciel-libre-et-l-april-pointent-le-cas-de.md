---
site: ZDNet France
title: "Directive copyright: le Conseil national du logiciel libre et l'April pointent le cas des forges logicielles"
author: Thierry Noisette
date: 2018-09-09
href: https://www.zdnet.fr/blogs/l-esprit-libre/directive-copyright-le-conseil-national-du-logiciel-libre-et-l-april-pointent-le-cas-des-forges-logicielles-39873341.htm
tags:
- Internet
- April
- Institutions
- Droit d'auteur
- Europe
---

> Le CNLL alerte le gouvernement sur le fait qu'exclure du champ de la directive sur le droit d'auteur les seules forges "à but non lucratif" pénaliserait les logiciels libres et open source. L'April fait le même constat.
