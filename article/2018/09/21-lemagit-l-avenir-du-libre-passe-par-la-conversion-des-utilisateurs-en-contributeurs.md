---
site: LeMagIT
title: "L'avenir du libre passe par la conversion des utilisateurs en contributeurs"
author: Thierry Carrez
date: 2018-09-21
href: https://www.lemagit.fr/tribune/Lavenir-du-libre-passe-par-la-conversion-des-utilisateurs-en-contributeurs
tags:
- Promotion
---

> Sans contribution et engagement, pas de communauté et sans communauté, pas d’open source. Thierry Carrez, VP Engineering de l’OpenStack Foundation revient dans cette tribune d’expert sur la nécessité d’opérer un changement. Si aujourd’hui, l’open source est au cœur de l’innovation et motorise l’essentiel du logiciel, il est temps de franchir un cap: celui de la conversion des utilisateurs en contributeurs.
