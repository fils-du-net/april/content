---
site: Libération
title: "Aucun algorithme, jamais, ne pourra défendre la démocratie"
author: Olivier Ertzscheid
date: 2018-09-17
href: https://www.liberation.fr/debats/2018/09/17/aucun-algorithme-jamais-ne-pourra-defendre-la-democratie_1679311
tags:
- Entreprise
- Institutions
- Vote électronique
- Vie privée
---

> Nous avons perdu la bataille contre les algorithmes en ce qui concerne la vie privÃ©e. Il faut aujourd'hui se battre pour limiter leur impact sur nos vies publiques et notre destin collectif.
