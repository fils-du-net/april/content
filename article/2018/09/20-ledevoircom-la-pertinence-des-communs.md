---
site: LeDevoir.com
title: "La pertinence des «communs»"
author: Laure Waridel
date: 2018-09-20
href: https://www.ledevoir.com/opinion/idees/537180/la-pertinence-des-communs
tags:
- Économie
- International
---

> Alors que l’on entend de plus en plus parler de l’importance d’un changement de paradigme pour protéger l’environnement et réduire les inégalités, mais que ce genre d’idée est quasi absente de la campagne électorale, nous souhaitons faire connaître ici l’approche des communs. Il s’agit d’une invitation à penser autrement.
