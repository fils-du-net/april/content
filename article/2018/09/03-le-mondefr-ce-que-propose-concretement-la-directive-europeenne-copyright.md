---
site: Le Monde.fr
title: "Ce que propose concrètement la directive européenne «copyright»"
author: Cécile Ducourtieux
date: 2018-09-03
href: https://www.lemonde.fr/economie/article/2018/09/03/droits-d-auteur-ce-que-propose-concretement-la-directive-europeenne-copyright_5349478_3234.html
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Les virulents débats entre ayants droit et multinationales concernent un partage plus équitable de la valeur et une juste rémunération des créateurs.
