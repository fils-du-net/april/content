---
site: Developpez.com
title: "Bercy ouvre les codes sources des modèles économétriques Mésange, Opale et Saphir"
author: Stéphane le calme
date: 2018-09-13
href: https://www.developpez.com/actu/224096/Bercy-ouvre-les-codes-sources-des-modeles-econometriques-Mesange-Opale-et-Saphir-sous-la-pression-d-une-l-association
tags:
- Administration
- Associations
- Open Data
---

> Sous la pression de l’association Ouvre-boîte (une association dont l'objet est d'obtenir l'accès et la publication effective des documents administratifs, et plus particulièrement des données, bases de données et codes sources, conformément aux textes en vigueur), la direction générale du Trésor a décidé de mettre en ligne trois de ses modèles économétriques, notamment Mésange, Opale et Saphir.
