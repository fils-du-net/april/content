---
site: ZDNet France
title: "De l'open source dans vos projets informatiques? Quelle idée!"
author: Leslie Saladin
date: 2018-09-08
href: https://www.zdnet.fr/blogs/l-esprit-libre/de-l-open-source-dans-vos-projets-informatiques-quelle-idee-39873337.htm
tags:
- Entreprise
- Promotion
---

> Les raisons de l'expansion des solutions open source, leurs atouts et leur succès en France: une tribune de Leslie Saladin, de BlueMind.
