---
site: France 24
title: "Les temps changent: Microsoft devient membre de la Linux Foundation"
date: 2018-09-12
href: https://www.france24.com/fr/20161117-temps-changent-microsoft-devient-membre-linux-foundation
tags:
- Entreprise
---

> Qui aurait cru qu'un jour, Microsoft et Linux, le roi des systèmes d'exploitation open source, marcheraient main dans la main? Pas grand monde, mais c'est pourtant bien vrai: les ennemis de toujours semblent (presque) réconciliés.
