---
site: Le Taurillon
title: "La directive «droit d'auteur»: ou comment (essayer) de réguler la jungle numérique"
author: Laura Mercier
date: 2018-09-06
href: https://www.taurillon.org/la-directive-droit-d-auteur-ou-comment-essayer-de-reguler-la-jungle
tags:
- Internet
- April
- Institutions
- Associations
- Droit d'auteur
- Europe
---

> S'il y a un projet de directive européenne qui fait débat depuis maintenant plusieurs mois au point d'être même expliqué, commenté et analysé dans les médias – directement concernés -, c'est bien la directive dite «droit d'auteur». Celle-ci est au cœur des débats et a provoqué une forte mobilisation de divers acteurs de la presse et des plateformes de partage. Les députés européens doivent se prononcer le 12 septembre prochain sur ce projet de directive.
