---
site: Developpez.com
title: "Copyright: le rapporteur Axel Voss dit être surpris par un article du texte adopté"
author: Bill Fassinou
date: 2018-09-15
href: https://www.developpez.com/actu/224444/Copyright-le-rapporteur-Axel-Voss-dit-etre-surpris-par-un-article-du-texte-adopte-les-eurodeputes-ont-ils-examine-tous-les-aspects-de-la-reforme
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Il y a deux jours, la directive sur les droits d’auteur dans l'Union européenne a été adoptée. Ce à quoi personne ne s’attendait, c’est que les eurodéputés, le rapporteur Voss y compris, aient pu manquer un article. C’est pourtant ce qui s’est passé. L’amendement 76, une disposition qui restreint le droit de filmer des événements sportifs, a été adopté apparemment sans qu’une réelle attention lui soit portée.
