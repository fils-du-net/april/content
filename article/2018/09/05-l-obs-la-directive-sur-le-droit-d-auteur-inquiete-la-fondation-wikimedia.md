---
site: L'OBS
title: "La directive sur le droit d'auteur inquiète la fondation Wikimedia"
author: Thierry Noisette
date: 2018-09-05
href: https://www.nouvelobs.com/economie/20180905.OBS1856/la-directive-sur-le-droit-d-auteur-inquiete-la-fondation-wikimedia.html
tags:
- Internet
- April
- Institutions
- Associations
- Droit d'auteur
- Europe
---

> La fondation américaine qui soutient Wikipédia met en garde contre un risque d'atteinte au partage et de réduction du domaine public avec le texte qui sera voté le 12 septembre.
