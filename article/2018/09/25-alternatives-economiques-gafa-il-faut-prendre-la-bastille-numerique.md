---
site: "Alternatives-economiques"
title: "Gafa: «Il faut prendre la Bastille numérique»"
author: Sébastien Soriano
date: 2018-09-25
href: https://www.alternatives-economiques.fr/gafa-faut-prendre-bastille-numerique/00086228
tags:
- Entreprise
- Internet
- Économie
- Institutions
---

> Le président de l’Arcep, le gendarme des télécoms, livre ses pistes pour réguler les géants du numérique.
