---
site: "Génération-NT"
title: "Mounir Mahjoubi propose une liste d'alternatives aux grandes plateformes"
author: Jérôme G.
date: 2018-09-22
href: https://www.generation-nt.com/mounir-mahjoubi-alternative-grande-plateforme-actualite-1957437.html
tags:
- Administration
- Institutions
- Promotion
---

> Pour "s'ouvrir à la diversité technologique, pensez aux solutions alternatives." C'est le conseil du secrétariat d'État chargé du numérique avec la publication d'une liste forcément non exhaustive.
