---
site: Usbek & Rica
title: "«À l’ère post-Snowden, on ne peut plus se voiler la face»"
author: Guillaume Ledit
date: 2018-09-11
href: https://usbeketrica.com/article/a-l-ere-post-snowden-on-ne-peut-plus-se-voiler-la-face
tags:
- Entreprise
- Économie
- Institutions
- Vie privée
---

> L'universitaire américain Yochai Benkler fait partie de ces intellectuels qui pensent l'impact d'Internet sur nos sociétés depuis les années 1990. Le professeur d'études juridiques entrepreneuriales à Harvard est aujourd'hui membre d'une commission lancée le 11 septembre 2018 par l'ONG Reporters sans frontières et chargée de travailler à la rédaction d’une future «déclaration sur l’information et la démocratie».
