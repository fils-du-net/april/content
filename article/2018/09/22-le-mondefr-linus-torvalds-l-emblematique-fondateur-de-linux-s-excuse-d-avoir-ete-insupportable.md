---
site: Le Monde.fr
title: "Linus Torvalds, l’emblématique fondateur de Linux, s’excuse d’avoir été insupportable"
author: Morgane Tual 
date: 2018-09-22
href: https://www.lemonde.fr/pixels/article/2018/09/22/les-excuses-de-linus-torvalds-l-emblematique-et-caracteriel-fondateur-de-linux_5358713_4408996.html
tags:
- Innovation
---

> Linus Torvalds, une personnalité majeure du monde de l’informatique, est réputé pour sa personnalité irascible et ses messages d’insultes.
