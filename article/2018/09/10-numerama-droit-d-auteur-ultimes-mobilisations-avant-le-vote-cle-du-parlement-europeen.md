---
site: Numerama
title: "Droit d’auteur: ultimes mobilisations avant le vote clé du Parlement européen"
author: Julien Lausson
date: 2018-09-10
href: https://www.numerama.com/politique/416564-droit-dauteur-ultimes-mobilisations-avant-le-vote-cle-du-parlement-europeen.html
tags:
- Internet
- April
- HADOPI
- Institutions
- Droit d'auteur
- Europe
---

> Le Parlement européen se penchera le 12 septembre sur le projet de réforme de la directive sur le droit d'auteur. De chaque côté, la mobilisation s'intensifie.
