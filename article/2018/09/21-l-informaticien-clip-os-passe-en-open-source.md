---
site: L'Informaticien
title: "CLIP OS passe en open source"
author: Guillaume Périssat
date: 2018-09-21
href: https://www.linformaticien.com/actualites/id/50347/clip-os-passe-en-open-source.aspx
tags:
- Administration
- Innovation
---

> CLIP OS, c’est ce système d’exploitation «multiniveau sécurisé» développé par l’ANSSI. Sous l’impulsion de la Dinsic, ce projet vieux de bientôt 15 ans passe en open source afin de s’enrichir des contributions de la communauté.
