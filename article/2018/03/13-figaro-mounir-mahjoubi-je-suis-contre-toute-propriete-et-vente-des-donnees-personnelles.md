---
site: FIGARO
title: "Mounir Mahjoubi: «Je suis contre toute propriété et vente des données personnelles»"
author: Elsa Trujillo
date: 2018-03-13
href: http://www.lefigaro.fr/secteur/high-tech/2018/03/13/32001-20180313ARTFIG00213-mounir-mahjoubi-je-suis-contre-toute-propriete-et-vente-des-donnees-personnelles.php
tags:
- Économie
- Institutions
- Vie privée
---

> Lors d'un débat organisé par l'organisation professionnelle Syntec Numérique, le secrétaire d'État au Numérique a pris position à l'encontre du concept de patrimonialité des données.
