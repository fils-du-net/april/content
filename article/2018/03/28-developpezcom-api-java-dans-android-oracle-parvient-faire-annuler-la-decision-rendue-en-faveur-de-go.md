---
site: Developpez.com
title: "API Java dans Android: Oracle parvient à faire annuler la décision rendue en faveur de Google"
author: Michael Guilloux
date: 2018-03-28
href: https://www.developpez.com/actu/195614/API-Java-dans-Android-Oracle-parvient-a-faire-annuler-la-decision-rendue-en-faveur-de-Google-la-Cour-d-appel-rejette-l-usage-equitable-des-API
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Droit d'auteur
---

> La Cour d'appel du circuit fédéral à Washington DC a relancé le procès entre Oracle et Google sur l'utilisation des API Java dans le système d'exploitation mobile Android. Mardi, la Cour d'appel s'est prononcée en faveur d'Oracle en annulant le jugement de 2016 en vertu duquel l'utilisation des API Java dans Android relèverait du fair use (usage équitable). Le tribunal a donc renvoyé l'affaire à la Cour fédérale de Californie pour déterminer les dommages, ce qu'Oracle estimait à environ 9 milliards de dollars.
