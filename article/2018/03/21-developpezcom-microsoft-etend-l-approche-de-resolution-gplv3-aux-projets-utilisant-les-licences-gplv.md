---
site: Developpez.com
title: "Microsoft étend l'approche de résolution GPLv3 aux projets utilisant les licences GPLv2"
author: Coriolan
date: 2018-03-21
href: https://www.developpez.com/actu/194334/Microsoft-etend-l-approche-de-resolution-GPLv3-aux-projets-utilisant-les-licences-GPLv2-et-rejoint-ainsi-Google-Facebook-et-Red-Hat
tags:
- Entreprise
- Licenses
- Informatique en nuage
---

> Afin de résoudre les problèmes de licence dans le logiciel open source impliquant la GPL, Microsoft ainsi que CA Technologies, Cisco, HPE, SAP et SUSE se sont engagés à ne pas prendre de mesures légales contre les contrevenants, et ce dans une période de 60 jours. Ces firmes se joignent ainsi à Facebook, Google, IBM et Red Hat qui avaient déjà étendu le droit à l’erreur et la présomption de bonne foi à leur code source sous licence GPLv2.
