---
site: Europe1.fr
title: "Les conséquences inattendues du scandale Cambridge Analytica"
author: Grégoire Martinez
date: 2018-03-30
href: http://www.europe1.fr/emissions/a-lheure-du-numerique/les-consequences-inattendues-du-scandale-des-donnees-personnelles-sur-facebook-3613205
tags:
- Entreprise
- Internet
- Vie privée
---

> Des conséquences pour le moins inattendues. Alors que le scandale Cambridge Analytica a mis en avant l'énorme volume de données personnelles dont disposent les géants d'Internet et notamment, Facebook, mais aussi Google, sur les réseaux sociaux les internautes ont appelé à supprimer les compte Facebook avec le #DeleteFacebook. Autre conséquence : celui qu'on appelle le Google français, Qwant, qui mise sur le respect de la vie privée de ses utilisateurs et ne collecte pas leurs données personnelles, a vu son trafic explosé.
