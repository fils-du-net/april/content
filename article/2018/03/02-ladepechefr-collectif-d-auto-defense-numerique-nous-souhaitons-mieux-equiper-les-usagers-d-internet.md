---
site: LaDepeche.fr
title: "Collectif d'Auto-défense Numérique: «Nous souhaitons mieux équiper les usagers d'internet»"
author: Zina Desmazes
date: 2018-03-02
href: https://www.ladepeche.fr/article/2018/03/02/2751891-collectif-auto-defense-numerique-souhaitons-mieux-equiper-usagers-internet.html
tags:
- Internet
- Associations
- Vie privée
---

> Julien Rabier est membre du collectif d'Auto-défense Numérique toulousain créé en 2017. Aujourd'hui, ce collectif va présenter la nouvelle édition du guide d'autodéfense numérique à la librairie Terra Nova de Toulouse à partir de 19 heures.
