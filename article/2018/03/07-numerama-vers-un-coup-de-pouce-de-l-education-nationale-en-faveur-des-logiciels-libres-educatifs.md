---
site: Numerama
title: "Vers un coup de pouce de l'éducation nationale en faveur des logiciels libres éducatifs?"
author: Julien Lausson
date: 2018-03-07
href: https://www.numerama.com/politique/334245-vers-un-coup-de-pouce-de-leducation-nationale-en-faveur-des-logiciels-libres-educatifs.html
tags:
- April
- Institutions
- Éducation
- Promotion
---

> Et si le ministère de l'éducation nationale donnait un coup de pouce en faveur des logiciels libres éducatifs? C'est la question que pose une députée LREM au ministre Jean-Michel Blanquer. Plus généralement, elle interroge sur le rapport de ministère sur le logiciel libre et son utilisation à l'école.
