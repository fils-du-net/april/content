---
site: ChannelNews
title: "Le marché mondial de l’open source en forte croissance"
author: Dirk Basyn
date: 2018-03-01
href: https://www.channelnews.fr/marche-mondial-de-lopen-source-forte-croissance-80042
tags:
- Entreprise
- Économie
- International
---

> Le marché de l’open source est en très bonne santé si l’on en croit ReportBuyer. Son chiffre d’affaires mondial passerait de 11,40 milliards de dollars en 2017 à 32,95 milliards de dollars en 2022, soit un taux de croissance annuel moyen de 23,65%
