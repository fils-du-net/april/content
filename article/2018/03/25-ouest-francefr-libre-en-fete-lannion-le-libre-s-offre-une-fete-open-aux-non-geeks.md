---
site: "ouest-france.fr"
title: "Libre en fête à Lannion: le libre s’offre une fête open aux non-geeks"
date: 2018-03-25
href: https://www.ouest-france.fr/bretagne/lannion-22300/libre-en-fete-lannion-le-libre-s-offre-une-fete-open-aux-non-geeks-5645288
tags:
- Associations
- Promotion
---

> Ce dimanche et jusqu'à 18h, la manifestation "Libre en fête" se tient aux Ursulines. Une quinzaine d'associations, entreprises et institutions sont présentes pour montrer au grand public de quoi sont capables les logiciels libres. Démonstrations ludiques à l'appui. Petit retour en photos.
