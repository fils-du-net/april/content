---
site: l'Humanité.fr
title: "«Dégoogliser Internet, oui, mais en agissant autrement»"
author: Pierre Duquesne
date: 2018-03-09
href: https://humanite.fr/degoogliser-internet-oui-mais-en-agissant-autrement-651759
tags:
- Internet
- Associations
- Promotion
- Vie privée
---

> FramaPad, Mastodon, Qwant… une trentaine de services alternatifs à Google sont proposés par des militants d’un numérique éthique.
