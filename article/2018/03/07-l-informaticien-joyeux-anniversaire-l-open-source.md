---
site: L'Informaticien
title: "Joyeux anniversaire l’Open Source!"
author: Bertrand Garé
date: 2018-03-07
href: https://www.linformaticien.com/actualites/id/48573/joyeux-anniversaire-l-open-source.aspx
tags:
- Sensibilisation
- Philosophie GNU
---

> Le 3 février dernier, l’Open Source fêtait ses 20 ans! Pourquoi le 3 février? Il fallait bien fixer une date. On en a bien donné une à la naissance de Jésus. Alors, comme pour le Messie, il en fallait une pour consacrer la naissance d’un mouvement qui se veut bien plus large que le simple accès au code d’un logiciel.
