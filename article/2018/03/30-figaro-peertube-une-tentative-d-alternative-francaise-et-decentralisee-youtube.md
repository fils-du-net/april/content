---
site: FIGARO
title: "PeerTube, une tentative d'alternative française et décentralisée à YouTube"
author: Pauline Verge
date: 2018-03-30
href: http://www.lefigaro.fr/secteur/high-tech/2018/03/30/32001-20180330ARTFIG00001-peertube-une-tentative-d-alternative-francaise-et-decentralisee-a-youtube.php
tags:
- Internet
- Associations
- Innovation
---

> La version bêta de PeerTube, un logiciel qui permet de publier des vidéos en ligne de manière décentralisée, est désormais accessible au public.
