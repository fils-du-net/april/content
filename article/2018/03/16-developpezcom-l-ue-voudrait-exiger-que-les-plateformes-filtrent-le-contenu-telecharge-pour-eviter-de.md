---
site: Developpez.com
title: "L'UE voudrait exiger que les plateformes filtrent le contenu téléchargé pour éviter des violations de copyright"
author: Stéphane le calme
date: 2018-03-16
href: https://www.developpez.com/actu/193867/L-UE-voudrait-exiger-que-les-plateformes-filtrent-le-contenu-telecharge-pour-eviter-des-violations-de-copyright-y-compris-dans-le-partage-de-code
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> La proposition vise la musique et les vidéos sur des plateformes de streaming, sur la base d'une théorie d'un «écart de valeur» entre les bénéfices que ces plateformes font des œuvres téléchargées et ce que les détenteurs de copyright de certaines œuvres téléchargées reçoivent. Cependant, la façon dont l’article 13 est rédigé capture beaucoup d'autres types de contenu, y compris le code.
