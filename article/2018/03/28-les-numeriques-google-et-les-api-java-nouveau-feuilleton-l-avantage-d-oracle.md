---
site: Les Numeriques
title: "Google et les API Java: nouveau feuilleton à l'avantage d'Oracle"
author: Mathieu Chartier
date: 2018-03-28
href: https://www.lesnumeriques.com/vie-du-net/google-api-java-nouveau-feuilleton-a-avantage-oracle-n72851.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Droit d'auteur
---

> C'est un combat à rebondissements que se livrent Google et Oracle devant les tribunaux. Après avoir dominé son adversaire puis échappé in extremis au K.O. lors des derniers échanges, Google est de nouveau à terre...
