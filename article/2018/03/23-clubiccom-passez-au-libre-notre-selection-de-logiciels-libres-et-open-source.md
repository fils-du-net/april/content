---
site: Clubic.com
title: "Passez au libre! Notre sélection de logiciels libres et open source"
date: 2018-03-23
href: http://www.clubic.com/telecharger/actus-logiciels/article-842984-1-passez-libre-selection-logiciels-libres-open-source.html
tags:
- Promotion
- Vie privée
---

> Que votre ordinateur tourne sous GNU/Linux ou sous Windows, vous avez la possibilité de remplacer la plupart de vos logiciels propriétaires par des alternatives libres. Notre sélection va vous aider à bien commencer pour rendre votre PC opérationnel au quotidien, tout en restant libre et open source!
