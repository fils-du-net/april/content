---
site: Zebulon.fr
title: "Facebook dans de sales draps: un cofondateur de WhatsApp conseille de supprimer Facebook"
author: Ruby Charpentier
date: 2018-03-23
href: https://www.zebulon.fr/actualites/17307-facebook-dans-de-sales-draps-un-cofondateur-de-whatsapp-conseille-de-supprimer-facebook.html
tags:
- Entreprise
- Internet
- Vie privée
---

> L'affaire Cambridge Analytica fait ainsi un tort considérable au réseau, accusé de ne pas avoir trop d'éthique lorsqu'il s'agit de vendre des données personnelles. Et l'appel au boycott d'un cofondateur de WhatsApp ne risque pas d'arranger les choses.
