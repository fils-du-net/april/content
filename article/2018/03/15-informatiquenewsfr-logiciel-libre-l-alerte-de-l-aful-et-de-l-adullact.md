---
site: InformatiqueNews.fr
title: "Logiciel libre: L’alerte de l’AFUL et de l’ADULLACT"
date: 2018-03-15
href: https://www.informatiquenews.fr/logiciel-libre-lalerte-de-laful-et-de-ladullact-56261
tags:
- Internet
- Institutions
- Associations
- Droit d'auteur
- Promotion
- Europe
---

> Les associations de promotion du logiciel libre, l’ADULLACT et l’AFUL, souhaitent alerter les pouvoirs publics sur la place de la création libre dans l’écosystème national, au regard des futures dispositions législatives françaises et européennes.
