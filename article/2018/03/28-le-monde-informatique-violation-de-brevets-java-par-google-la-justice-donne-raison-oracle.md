---
site: Le Monde Informatique
title: "Violation de brevets Java par Google: La justice donne raison à Oracle"
author: Dominique Filippone
date: 2018-03-28
href: https://www.lemondeinformatique.fr/actualites/lire-violation-de-brevets-java-par-google-la-justice-donne-raison-a-oracle-71305.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Droit d'auteur
---

> La Cour d'appel du district Nord de Californie a finalement donné raison à Oracle dans l'affaire l'opposant depuis 2010 à Google pour violation de brevets Java. L'argument juridique de l'usage raisonnable des API Java d'Oracle retenu par le tribunal de San Francisco l'année dernière a été balayé par la Cour d'appel et ouvre la voie au versement de milliards de dollars de dommages et intérêts pour Oracle.
