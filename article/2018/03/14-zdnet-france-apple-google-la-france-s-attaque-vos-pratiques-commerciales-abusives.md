---
site: ZDNet France
title: "Apple, Google, la France s’attaque à vos «pratiques commerciales abusives»"
author: Christophe Auffray
date: 2018-03-14
href: http://www.zdnet.fr/actualites/apple-google-la-france-s-attaque-a-vos-pratiques-commerciales-abusives-39865456.htm
tags:
- Entreprise
- Institutions
---

> Le ministre de l’Economie Bruno Le Maire annonce l’assignation future d’Apple et de Google devant le tribunal de commerce de Paris pour leurs «pratiques commerciales abusives.» Le gouvernement rappelle une nouvelle fois sa volonté de soumettre les Gafa à des règles.
