---
site: "usine-digitale.fr"
title: "Sept mesures de Terra Nova pour stimuler l’économie collaborative"
author: Christophe Bys
date: 2018-03-13
href: https://www.usine-digitale.fr/editorial/sept-mesures-de-terra-nova-pour-stimuler-l-economie-collaborative.N664889
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Innovation
---

> Étude Le think tank Terra Nova vient de publier un rapport intitulé "Economie collaborative: comment encadrer et encourager le pouvoir de la multitude?" Si ce nouveau secteur est un intéressant réservoir d'innovation, certaines dérives (précarisation du travail, développement d'une économie souterraine) pourraient faire jeter le bébé avec l'eau du bain. Pour éviter cela, le think tank promeut sept mesures qui, si elles ne sont pas bouleversantes, constituent une très bonne synthèse des défis à relever.
