---
site: "ouest-france.fr"
title: "Vire. A la découverte des logiciels libres avec Vire’GUL"
author: Sébastien Brêteau
date: 2018-03-15
href: https://www.ouest-france.fr/normandie/vire-normandie-14500/vire-la-decouverte-des-logiciels-libres-avec-vire-gul-5623542
tags:
- Administration
- Associations
---

> Créée en janvier dernier, l’association propose la découverte et l’initiation au monde de Linux et des logiciels libres. Ça commence samedi 17 mars.
