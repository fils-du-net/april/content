---
site: l'Humanité.fr
title: "Démocratie. Cinq pistes pour briser la domination des géants du Net"
author: Pierric Marissal
date: 2018-03-09
href: https://www.humanite.fr/democratie-cinq-pistes-pour-briser-la-domination-des-geants-du-net-651764
tags:
- Entreprise
- Internet
- Open Data
- Vie privée
---

> La domination de Google, Apple, Facebook et consorts n’est pas une fatalité. Les états généraux de la révolution numérique planchent, vendredi et samedi, à Paris, sur les moyens de réappropriation individuels et collectifs du Web.
