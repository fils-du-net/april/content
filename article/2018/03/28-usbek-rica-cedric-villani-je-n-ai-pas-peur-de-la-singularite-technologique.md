---
site: Usbek & Rica
title: "Cédric Villani: «Je n’ai pas peur de la singularité technologique»"
author: Fabien Benoit
date: 2018-03-28
href: https://usbeketrica.com/article/je-n-ai-pas-peur-de-la-singularite-technologique
tags:
- Institutions
---

> Le député de l’Essonne et mathématicien Cédric Villani dévoile jeudi 29 mars son rapport parlementaire sur l’intelligence artificielle, fruit d’un long travail de consultation auprès d’experts, d'entrepreneurs, de politiques et de citoyens. Au menu, une volonté affirmée de faire de la France et de l’Europe une place forte de l’IA, moyennant un investissement dans la recherche et une politique de mise en commun des données, sous la tutelle et la protection de l’État.
