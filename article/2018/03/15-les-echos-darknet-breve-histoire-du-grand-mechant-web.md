---
site: Les Echos
title: "Darknet: Brève histoire du grand méchant web"
author: Marc Lionti
date: 2018-03-15
href: https://www.lesechos.fr/idees-debats/cercle/cercle-180404-darknet-entre-fantasmes-et-realite-demystification-du-grand-mechant-web-2161448.php
tags:
- Internet
- Vie privée
---

> Le nom "Darkweb" évoque les bas-fonds du web. Mais qu’est-ce que le Darkweb? À quoi sert-il?
