---
site: Le Monde.fr
title: "Tim Berners-Lee, le père du Web, appelle à réguler les grandes plates-formes"
date: 2018-03-12
href: http://www.lemonde.fr/pixels/article/2018/03/12/tim-berners-lee-le-pere-du-web-appelle-a-reguler-les-grandes-plates-formes_5269595_4408996.html
tags:
- Entreprise
- Internet
- Institutions
---

> Le principal inventeur du Web appelle à un «cadre légal ou réglementaire» pour mieux encadrer les grandes plates-formes.
