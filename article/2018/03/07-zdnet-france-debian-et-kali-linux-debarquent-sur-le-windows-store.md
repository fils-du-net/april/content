---
site: ZDNet France
title: "Debian et Kali Linux débarquent sur le Windows Store"
author: Louis Adam
date: 2018-03-07
href: http://www.zdnet.fr/actualites/debian-et-kali-linux-debarquent-sur-le-windows-store-39865128.htm
tags:
- Entreprise
---

> Debian est une distribution qui met l’accent sur l’utilisation de logiciels libres. Kali Linux est une distribution destinée aux experts en cybersécurité et embarque un certain nombre d’outils de test d'intrusion.
