---
site: Slate.fr
title: "Le scandale, ce n’est pas Cambridge Analytica: c’est le modèle économique de Facebook"
author: Will Oremus
date: 2018-03-22
href: http://www.slate.fr/story/159229/cambridge-analytica-facebook-scandale
tags:
- Entreprise
- Internet
- Institutions
- Vie privée
---

> L’affaire Cambridge Analytica nous en dit long sur les manipulations en ligne –et plus encore sur les failles énormes de Facebook pour tout ce qui touche de près ou de loin à notre vie privée, et à ce que l’on pourrait en faire.
