---
site: Libération
title: "Bercy attaque Apple et Google sur les pratiques de leurs magasins d'applications"
author: Christophe Alix
date: 2018-03-14
href: http://www.liberation.fr/futurs/2018/03/14/bercy-attaque-apple-et-google-sur-les-pratiques-de-leurs-magasins-d-applications_1636092
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Promotion
---

> L'offensive juridique et fiscale contre les Gafa se poursuit avec la plainte déposée par la DGCCRF contre les deux géants de la Silicon Valley. Ils sont accusés de profiter de leur position dominante pour imposer unilatéralement leurs conditions commerciales aux starts-up.
