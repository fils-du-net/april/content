---
site: Télérama.fr
title: "Quitter Facebook? La question se pose de plus en plus, mais..."
author: Thomas Bécard
date: 2018-03-22
href: http://www.telerama.fr/medias/quitter-facebook,-un-appel-au-boycott-pour-quels-effets,n5540389.php
tags:
- Entreprise
- Internet
- Vie privée
---

> Après le scandale Cambridge Analytica, de nombreux internautes et quelques personnalités font part de leur décision de supprimer leur compte Facebook. Est-ce une solution suffisante pour éviter mettre en sûreté ses données personnelles? Pas si sûr.
