---
site: France Culture
title: "\"J'ai voulu reprendre le contrôle de mes données\""
author: Abdelhak El Idrissi
date: 2018-03-30
href: https://www.franceculture.fr/emissions/hashtag/jai-voulu-reprendre-le-controle-de-mes-donnees
tags:
- Entreprise
- Internet
- Économie
- Vie privée
---

> La récupération par un cabinet de conseil politique des données personnelles de plusieurs dizaines de millions d'utilisateurs de Facebook a conduit de nombreux internautes à questionner leur présence sur le réseau social. Les réponses vont de la colère à la résignation.
