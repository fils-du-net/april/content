---
site: La Tribune
title: "Menace sur le logiciel libre"
author: Frédéric Duflo
date: 2018-03-28
href: https://objectif-languedoc-roussillon.latribune.fr/opinions/tribunes/2018-03-28/menace-sur-le-logiciel-libre-773415.html
tags:
- Institutions
- Associations
- Droit d'auteur
---

> Dans cette tribune, l'ADULLACT et l'AFUL, associations de promotion du logiciel libre, alertent les pouvoirs publics sur la place de la création libre dans l'écosystème national, au regard des futures dispositions législatives françaises et européennes.
