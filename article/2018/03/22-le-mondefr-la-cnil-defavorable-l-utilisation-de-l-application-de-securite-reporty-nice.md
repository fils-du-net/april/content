---
site: Le Monde.fr
title: "La CNIL défavorable à l’utilisation de l’application de sécurité Reporty à Nice"
author: Claire Legros
date: 2018-03-22
href: http://www.lemonde.fr/pixels/article/2018/03/22/la-cnil-defavorable-a-l-utilisation-de-l-application-de-securite-reporty-a-nice_5274783_4408996.html
tags:
- Internet
- Administration
- Institutions
- Promotion
- Sciences
- Vie privée
---

> L’autorité chargée des données personnelles juge «très intrusif» et «non proportionné» le dispositif participatif testé par la Ville de Nice.
