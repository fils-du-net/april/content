---
site: ZDNet France
title: "Microsoft au chevet de l'open source et de la GPL"
author: Mary Jo Foley
date: 2018-03-20
href: http://www.zdnet.fr/actualites/microsoft-au-chevet-de-l-open-source-et-de-la-gpl-39865756.htm
tags:
- Entreprise
- Licenses
---

> Microsoft est l'une des dix entreprises qui se sont engagées à essayer de résoudre les problèmes de licence dans le logiciel open source impliquant la GPL. Objectif: trouver des solutions avec les clients avant toute action en justice.
