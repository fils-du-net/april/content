---
site: Chroniques d'Architecture
title: "Qui dit libéral dit (logiciel) libre?"
author: Le Geek de Chroniques
date: 2018-03-27
href: https://chroniques-architecture.com/qui-dit-liberal-dit-logiciel-libre
tags:
- Entreprise
- Logiciels privateurs
- Sensibilisation
---

> Quelle est la différence entre un étudiant en architecture une heure avant son diplôme et un jeune architecte une heure après son inscription à l’Ordre? Il s’agit du montant que représentent les licences de logiciels. Le logiciel libre présente une véritable alternative.
