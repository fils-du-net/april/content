---
site: Contrepoints
title: "Anarchie, cypherpunk et liberté: les racines philosophiques du bitcoin"
author: Yorick de Mombynes
date: 2018-03-17
href: https://www.contrepoints.org/2018/03/17/311911-anarchie-cypherpunk-et-liberte-les-racines-philosophiques-du-bitcoin
tags:
- Internet
- Économie
- Innovation
- Licenses
- Vie privée
---

> Le bitcoin n’est pas qu’une réaction à la crise de 2008. Il est une réponse à un questionnement philosophique et politique entamé dès les années 1990.
