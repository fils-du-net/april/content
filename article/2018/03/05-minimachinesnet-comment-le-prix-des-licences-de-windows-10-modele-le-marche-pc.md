---
site: MiniMachines.net
title: "Comment le prix des licences de Windows 10 modèle le marché PC"
author: Pierre Lecourt
date: 2018-03-05
href: http://www.minimachines.net/actu/prix-licences-de-windows-10-59893
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Innovation
---

> Depuis toujours Microsoft dispose d'un outil qui modèle le marché informatique à sa convenance. Un coin du voile se lève sur le prix des licences de Windows 10 et des exigences de Microsoft.
