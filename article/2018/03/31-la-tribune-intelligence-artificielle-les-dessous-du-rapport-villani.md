---
site: La Tribune
title: "Intelligence artificielle: les dessous du rapport Villani"
author: Olivier Ezratty
date: 2018-03-31
href: https://www.latribune.fr/opinions/intelligence-artificielle-les-dessous-du-rapport-villani-773835.html
tags:
- Institutions
- Innovation
---

> A chaque fois que la puissance publique produit un rapport sur un secteur donné du numérique avec l'ambition de faire de la France un leader mondial, les acteurs de l'écosystème correspondant sont tout feu tout flammes parce que leur sujet intéresse les plus hautes autorités. Avec le recul, la France n'atteint jamais l'objectif assigné et, entre temps, les gouvernements ont changé et le marché est passé à autre chose. Le rapport Villani semble ne pas échapper à cette fatalité... Par Olivier Ezratty, consultant et auteur, notamment de l'ebook «Les usages de l'intelligence artificielle»
