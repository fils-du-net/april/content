---
site: estrepublicain.fr
title: "Nancy: la Ville lance une plate-forme digitale collaborative interquartiers"
author: Lysiane Ganousse
date: 2018-11-07
href: https://www.estrepublicain.fr/edition-de-nancy-ville/2018/11/07/nancy-la-ville-lance-une-plate-forme-digitale-collaborative-interquartiers
tags:
- Administration
- Open Data
- Vie privée
---

> Logiciels libres, consultations en ligne, démarches administratives sur le web, open data… Nancy se veut exemplaire et même pionnière sur le front du numérique. Dernière innovation: la plate-forme digitale pour les conseils de quartiers.
