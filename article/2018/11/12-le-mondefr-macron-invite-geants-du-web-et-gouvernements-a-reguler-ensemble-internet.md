---
site: Le Monde.fr
title: "Macron invite géants du Web et gouvernements à «réguler ensemble» Internet"
date: 2018-11-12
href: https://www.lemonde.fr/pixels/article/2018/11/12/macron-invite-geants-du-web-et-gouvernements-a-reguler-ensemble-internet_5382544_4408996.html
tags:
- Entreprise
- Internet
- Institutions
- Désinformation
- Droit d'auteur
- Vie privée
---

> Dans son discours d’inauguration de l’Internet Governance Forum, à l’Unesco, le président a réaffirmé sa volonté de réguler davantage le fonctionnement des plateformes, mais avec une certaine forme de concertation.
