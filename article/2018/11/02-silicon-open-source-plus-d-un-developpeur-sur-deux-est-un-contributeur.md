---
site: silicon
title: "Open Source: plus d’un développeur sur deux est un contributeur"
author: Ariane Beky
date: 2018-11-02
href: https://www.silicon.fr/open-source-developpeur-contributeur-223915.html
tags:
- Entreprise
---

> 55% des développeurs interrogés dans le monde contribuent activement à des projets open source, selon DigitalOcean.
