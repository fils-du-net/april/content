---
site: Horizons publics
title: "Open data, un mouvement qui démarre"
author: Gérard Ramirez del Villar
date: 2018-11-13
href: https://www.horizonspublics.fr/open-data-un-mouvement-qui-demarre
tags:
- Administration
- Sciences
- Open Data
---

> Au regard des obligations qui pèsent sur les collectivités en matière d’Open data, les chiffres sur le terrain restent en retrait. Mais les ambitions pour l’avenir sont grandes et les terrains d’application potentiels presque sans limites.
