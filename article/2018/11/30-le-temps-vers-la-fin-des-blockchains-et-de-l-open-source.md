---
site: Le Temps
title: "Vers la fin des blockchains et de l’open source?"
author: Olivier Depierre
date: 2018-11-30
href: https://www.letemps.ch/economie/vers-fin-blockchains-lopen-source
tags:
- Économie
- Brevets logiciels
- Innovation
---

> Après avoir vu d’innombrables réécritures et améliorations des blockchains existantes, certaines nouveautés technologiques se cloîtrent et se protègent, impassibles face à la critique extérieure. Qui a dit trumpisme?
