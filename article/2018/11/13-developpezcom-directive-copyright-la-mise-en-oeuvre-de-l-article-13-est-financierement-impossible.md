---
site: Developpez.com
title: "Directive copyright: la mise en oeuvre de l'article 13 est financièrement impossible"
author: Bill Fassinou
date: 2018-11-13
href: https://www.developpez.com/actu/233059/Directive-copyright-la-mise-en-oeuvre-de-l-article-13-est-financierement-impossible-selon-la-CEO-de-YouTube
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> En octobre dernier, Susan Wojcicki, la CEO de YouTube est monté au créneau pour appeler les créateurs de vidéos à protester contre l'article 13 de la directive Copyright qui, selon elle, menace des milliers d'emplois. Elle a, en effet, mis en garde les réalisateurs de vidéos contre la directive et les a exhorté à protester vivement contre la réglementation: s’appesantissant tout particulièrement sur l’article 13, elle explique aux réalisateurs de vidéos dans un billet de blog que «cette législation menace à la fois leur gagne-pain et leur capacité à partager leur voix avec le monde».
