---
site: IT Social
title: "100 millions de repositories confirment l’attrait pour GitHub et l’open source"
author: Yves Grandmontagne
date: 2018-11-13
href: https://itsocial.fr/enjeux/production/developpements/100-millions-de-repositories-confirment-lattrait-github-lopen-source
tags:
- Innovation
---

> La plateforme aux 31 millions de développeurs GitHub a franchi la barre symbolique des 100 millions de repositories (dépôts). Deux enseignements dans ce record: la communauté open source est toujours aussi active, et le rachat de GitHub par Microsoft a peu pesé sur le devenir de la plateforme.
