---
site: FIGARO
title: "Un développeur du logiciel VLC et un hacker français nommés chevaliers de l'Ordre du Mérite"
author: Elisa Braun
date: 2018-11-21
href: http://www.lefigaro.fr/secteur/high-tech/2018/11/19/32001-20181119ARTFIG00222-un-developpeur-du-logiciel-vlc-et-un-hacker-francais-nommes-chevaliers-de-l-ordre-du-merite.php
tags:
- Institutions
- Associations
- Promotion
---

> Deux éminents représentants de la culture du logiciel libre ont été adoubés Chevaliers de l'Ordre national du mérite. Une reconnaissance symbolique de l'importance des modèles technologiques alternatifs.
