---
site: "cio-online.com"
title: "Stéphane Rousseau (Eiffage): «l'Open CIO Summit est organisé par les décideurs IT pour les décideurs IT»"
author: Bertrand Lemaire
date: 2018-11-27
href: https://www.cio-online.com/actualites/lire-stephane-rousseau-eiffage-%C2%A0-%C2%A0l-open-cio-summit-est-organise-par-les-decideurs-it-pour-les-decideurs-it%C2%A0-10822.html
tags:
- Entreprise
- Économie
- Promotion
---

> DSI groupe de Eiffage, Stéphane Rousseau explique pourquoi il a accepté d'être le parrain de l'Open CIO Summit 2018. Cette manifestation se tiendra le 4 décembre 2018 à Paris. Il détaille également la place actuelle de l'Open Source en entreprise. Il a piloté les travaux du groupe de travail du Cigref «Open-source: alternatives aux grands fournisseurs».
