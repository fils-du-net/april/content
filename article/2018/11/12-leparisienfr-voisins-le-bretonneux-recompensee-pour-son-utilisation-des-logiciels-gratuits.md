---
site: leParisien.fr
title: "Voisins-le-Bretonneux récompensée pour son utilisation des logiciels gratuits"
author: L. Mt.
date: 2018-11-12
href: http://www.leparisien.fr/yvelines-78/voisins-le-bretonneux-recompensee-pour-son-utilisation-des-logiciels-gratuits-12-11-2018-7940781.php
tags:
- Administration
- Associations
- Promotion
---

> Grâce à l’emploi de ces outils collaboratifs dans ses services, la commune vient de recevoir le label Territoire numérique libre.
