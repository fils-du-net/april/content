---
site: France Culture
title: "Open Source: liberté, égalité?"
date: 2018-11-07
href: https://www.franceculture.fr/emissions/la-methode-scientifique/la-methode-scientifique-du-mercredi-07-novembre-2018
tags:
- Entreprise
- Sensibilisation
---

> Qu'est-ce que l'open source? Qui en sont les principaux acteurs? Quel intérêt pour les développeurs, les administrations, les entreprises, les citoyens? Comment ce mouvement influence-t-il la recherche en informatique?
