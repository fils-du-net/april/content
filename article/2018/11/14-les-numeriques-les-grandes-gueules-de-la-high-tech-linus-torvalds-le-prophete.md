---
site: Les Numeriques
title: "Les grandes gueules de la high-tech: Linus Torvalds, le prophète"
author: Jérôme Cartegini
date: 2018-11-14
href: https://www.lesnumeriques.com/vie-du-net/grandes-gueules-high-tech-linus-torvalds-prophete-e1493.html
tags:
- Sensibilisation
- Innovation
---

> Dans l’univers feutré des géants de la high-tech, certains personnages sont aussi connus pour leurs frasques que pour leurs créations révolutionnaires. Véritable génie de la programmation, Linus Torvalds a créé à seulement 21 ans Linux, le système d’exploitation le plus utilisé dans le monde. À la tête de son développement depuis 30 ans, l’informaticien s’est forgé une réputation épouvantable. Arrogant, antipathique, grossier... à tel point que son langage fleuri fait régulièrement la une des médias.
