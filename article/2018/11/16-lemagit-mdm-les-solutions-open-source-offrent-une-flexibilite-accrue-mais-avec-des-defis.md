---
site: LeMagIT
title: "MDM: les solutions open source offrent une flexibilité accrue, mais avec des défis"
author: Robert Sheldon
date: 2018-11-16
href: https://www.lemagit.fr/conseil/MDM-les-solutions-open-source-offrent-une-flexibilite-accrue-mais-avec-des-defis
tags:
- Entreprise
- Sensibilisation
- Standards
---

> La voie de l’open source est-elle attractive pour l’administration de terminaux mobiles? Pour s'en convaincre, il convient de peser le pour et le contre, avant d’examiner le marché.
