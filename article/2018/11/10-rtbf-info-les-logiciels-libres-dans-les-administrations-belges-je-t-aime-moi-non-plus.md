---
site: RTBF Info
title: "Les logiciels libres dans les administrations belges: ”Je t'aime, moi non plus”"
author: Jean-Claude Verset
date: 2018-11-10
href: https://www.rtbf.be/info/economie/detail_les-logiciels-libres-dans-les-administrations-belges-je-t-aime-moi-non-plus?id=10069658
tags:
- Administration
- Standards
- Informatique en nuage
- International
- Open Data
---

> En France, des villes rebelles, comme Rennes, rejettent les logiciels des géants informatiques pour les remplacer par des "logiciels libres". En Belgique, la tendance est très différente. Mais d’abord, c’est quoi un logiciel libre?
