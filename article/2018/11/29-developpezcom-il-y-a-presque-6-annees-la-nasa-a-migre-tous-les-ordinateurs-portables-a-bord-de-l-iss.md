---
site: Developpez.com
title: "Il y a presque 6 années, la NASA a migré tous les ordinateurs portables à bord de l'ISS de Windows vers Linux"
author: Coriolan
date: 2018-11-29
href: https://www.developpez.com/actu/235407/Il-y-a-presque-6-annees-la-NASA-a-migre-tous-les-ordinateurs-portables-a-bord-de-l-ISS-de-Windows-vers-Linux-pour-plus-de-stabilite-et-de-fiabilite
tags:
- Sciences
---

> Il y a presque 6 années, la United Space Alliance, l’entreprise menant des activités spatiales pour le compte de la National Aeronautics and Space Administration (NASA), a entrepris de migrer les ordinateurs au sein de la station spatiale internationale (ISS) vers Linux.
