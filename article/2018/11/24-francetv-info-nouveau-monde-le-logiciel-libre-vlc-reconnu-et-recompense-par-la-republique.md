---
site: francetv info
title: "Nouveau monde. Le logiciel libre VLC reconnu et récompensé par la République"
author: Jérôme Colombain
date: 2018-11-24
href: https://www.francetvinfo.fr/replay-radio/nouveau-monde/nouveau-monde-le-logiciel-libre-vlc-reconnu-et-recompense-par-la-republique_3032531.html
tags:
- Institutions
- Associations
---

> Né à l’Ecole Centrale de Paris, VLC (VideoLan Client) permet de regarder des vidéos numériques quel que soit leur format de compression. Il est l’œuvre de plusieurs bénévoles qui travaillent depuis des années au sein de l’association VLC. Son président, Jean-Baptiste Kempf, reçoit aujourd’hui le titre de Chevalier de l’Ordre National du Mérite (de même le hacker français Gaël Musquet, à l'origine de l'association humanitaire Hand).
