---
site: Next INpact
title: "Quand le droit d'auteur devient plus vigoureux que la lutte contre les contenus terroristes"
author: Marc Rees
date: 2018-11-21
href: https://www.nextinpact.com/news/107320-quand-droit-dauteur-devient-plus-vigoureux-que-lutte-contre-contenus-terroristes.htm
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Y a-t-il quelque chose qui cloche au sein des organes européens? En comparant la proposition de règlement sur la lutte contre les contenus terroristes et la future directive sur le droit d’auteur, on découvre que ce dernier va bénéficier d’une meilleure protection sur les grandes plateformes. Explications.
