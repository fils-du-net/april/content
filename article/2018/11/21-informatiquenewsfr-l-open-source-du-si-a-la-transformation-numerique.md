---
site: InformatiqueNews.fr
title: "L’Open Source: du SI à la transformation numérique"
date: 2018-11-21
href: https://www.informatiquenews.fr/lopen-source-du-si-a-la-transformation-numerique-59061
tags:
- Entreprise
- Sensibilisation
- Innovation
---

> Une entreprise sur deux estime l’open source est désormais indispensable pour sa transformation numérique et sera très bien implanté dans des domaines comme la cybersécurité.
