---
site: newsmonkey
title: "Reddit entre dans la guerre contre les articles 11 et 13 sur le droit d'auteur"
author: Gauvain Dossantos
date: 2018-11-29
href: http://fr.newsmonkey.be/article/26672
tags:
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Après YouTube, c'est au tour de Reddit d'inciter ses utilisateurs à s'exprimer à l'encontre de la directive européenne sur le droit d'auteur et le copyright. Plus précisément contre l'article 11 et l'article 13 de ce texte de loi.
