---
site: Atlantico
title: "Pourquoi Linux est un magnifique cas d’école des forces et des faiblesses de la tech européenne"
author: Rémi Bourgeot
date: 2018-11-14
href: https://www.atlantico.fr/decryptage/3558655/pourquoi-linux-est-un-magnifique-cas-d-ecole-des-forces-et-des-faiblesses-de-la-tech-europeenne-remi-bourgeot-
tags:
- Entreprise
- Sensibilisation
- Innovation
---

> Alors que la France et l'Europe tentent de se faire une place dans le secteur technologique, notamment au travers d'initiatives "Frenchtech", le géant IBM vient de racheter l'entreprise Red Hat, dont le modèle économique repose sur un logiciel libre issu de Linux, qui est pourtant d'origine européenne. Du point de vue des acteurs, qu'est-ce que ce cas d'espèce nous révèle de la réalité du secteur?
