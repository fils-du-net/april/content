---
site: Le Drenche
title: "Faut-il boycotter Facebook?"
date: 2018-11-21
href: https://ledrenche.fr/2018/11/faut-il-boycotter-facebook-4549
tags:
- Entreprise
- Internet
- Associations
- Vie privée
---

> Suite à l’affaire Cambridge Analytica et aux diverses accusations à l’encontre de Facebook quand à son utilisation des données personnelles de ses utilisateurs, de nombreuses voix s’élèvent pour appeler au boycott du réseau social le plus utilisé dans le monde. Facebook subit des controverses au sein même de son équipe. Des anciens employés ont critiqué le réseau social qu’ils jugent « destructeur pour le fonctionnement de notre société » selon les termes de Sean Parker, ex-président de l’entreprise de Mark Zuckerberg.
