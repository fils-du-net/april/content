---
site: La gazette.fr
title: "Numérique et territoires: une transition au service des citoyens"
author: Baptiste Cessieux
date: 2018-11-26
href: https://www.lagazettedescommunes.com/592805/numerique-et-territoires-une-transition-au-service-des-citoyens
tags:
- Administration
- Marchés publics
- Open Data
---

> Quel est le degré de maturité des collectivités dans l’appropriation des outils numériques et le déploiement de politiques publiques dématérialisées? Pour quels services aux citoyens? Résultats de l'étude «La Gazette» - Orange: des services aux citoyens sur les rails mais des interactions entre administrations qui restent à prouver.
