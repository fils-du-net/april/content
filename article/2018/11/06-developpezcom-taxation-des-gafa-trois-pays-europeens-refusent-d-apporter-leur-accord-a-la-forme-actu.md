---
site: Developpez.com
title: "Taxation des GAFA: trois pays européens refusent d'apporter leur accord à la forme actuelle du texte"
author: Stéphane le calme
date: 2018-11-06
href: https://www.developpez.com/actu/232314/Taxation-des-GAFA-trois-pays-europeens-refusent-d-apporter-leur-accord-a-la-forme-actuelle-du-texte-une-unanimite-est-requise
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Europe
---

> Les efforts du ministre français des Finances, Bruno Le Maire, pour rassembler ses collègues de l'Union européenne autour d'une nouvelle taxe sur les grandes enseignes du numérique ne semblent toujours pas très efficaces. En effet, quelques pays restent encore sceptique et n’ont pas apporté leur accord. D'autres pays, parmi lesquels l’Italie, ont annoncé leur intention de continuer avec leur propre projet de loi sur des taxes d'entreprises numériques.
