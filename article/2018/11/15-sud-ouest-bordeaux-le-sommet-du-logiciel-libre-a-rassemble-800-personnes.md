---
site: Sud Ouest
title: "Bordeaux: le sommet du logiciel libre a rassemblé 800 personnes"
author: Nicolas César
date: 2018-11-15
href: https://www.sudouest.fr/2018/11/15/bordeaux-le-sommet-du-logiciel-libre-a-rassemble-800-personnes-5569440-705.php
tags:
- Administration
- Institutions
- Promotion
---

> Les 6 et 7 novembre à Bordeaux, s’est déroulée la 1ère édition de B-Boost, sommet du logiciel libre et de l’open source. Une première, qui a permis de mettre en lumière un écosystème méconnu du grand public, dans lequel la région excelle
