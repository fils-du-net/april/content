---
site: We Demain
title: "Voici le premier smartphone durable et 100 % libéré des GAFAM"
author: Jean-Jacques Valette
date: 2018-11-08
href: https://www.wedemain.fr/Voici-le-premier-smartphone-durable-et-100-libere-des-GAFAM_a3709.html
tags:
- Associations
- Innovation
- Vie privée
---

> Grâce à Commown, une coopérative française de location de produits électroniques durables, le Fairphone est désormais disponible avec un système d'exploitation entièrement open source. Une façon d'éviter la revente de nos données personnelles.
