---
site: Romandie
title: "Roumanie: le président du Sénat visé par une enquête de corruption dans le dossier Microsoft"
date: 2018-11-08
href: https://www.romandie.com/news/969591.rom
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- International
---

> Bucarest - Une enquête a été ouverte à l'encontre du président du Sénat roumain, Calin Popescu-Tariceanu, soupçonné d'avoir touché 800.000 dollars de pots-de-vin lors de l'achat de licences Microsoft quand il était à la tête du gouvernement en 2008, a annoncé le Parquet anticorruption (DNA).
