---
site: "ouest-france.fr"
title: "Quimperlé. L’Install-party a battu son plein à la médiathèque"
date: 2018-11-26
href: https://www.ouest-france.fr/bretagne/quimperle-29300/quimperle-l-install-party-battu-son-plein-la-mediatheque-6090368
tags:
- Administration
- Associations
- Promotion
---

> «C’est depuis 2013 que nous accueillons des Install-party», explique Pascal Thibaut, directeur de la médiathèque de Quimperlé. Samedi 24 novembre, un après-midi d'animations était organisé.
