---
site: Next INpact
title: "Sur les Mac récents, la puce T2 bloque l'installation de Linux"
date: 2018-11-06
href: https://www.nextinpact.com/brief/sur-les-mac-recents--la-puce-t2-bloque-l-installation-de-linux-6383.htm
tags:
- Entreprise
- Informatique-deloyale
---

> Lors de la présentation des derniers MacBook Air et Mac mini, Apple a particulièrement insisté sur la présence à chaque fois de sa puce T2. Elle s’occupe de gérer matériellement l’Enclave sécurisée (qui stocke notamment les empreintes biométriques), le chiffrement APFS et le Secure Boot.
