---
site: LA TRIBUNE
title: "La conception logicielle appliquée au monde physique"
author: Fabien Eychenne
date: 2012-10-22
href: http://www.latribune.fr/blogs/produire-autrement/20121022trib000726342/la-conception-logicielle-appliquee-au-monde-physique.html
tags:
- Entreprise
- Économie
- Matériel libre
- Innovation
---

> Depuis une quinzaine d'années, la majorité des développements de logiciels s'appuie sur des méthodes dites "agiles". Sous cette bannière se regroupe plusieurs méthodes basées sur un développement itératif et incrémental, dans lequel la recherche de solutions aux problèmes rencontrés s'appuient sur la collaboration de pair à pair. Elle promeut des réponses rapides et flexibles, une planification des tâches adaptatives dans des laps de temps très courts permettant une très grande réactivité. L'objectif central est de trouver de meilleur moyen de développer des logiciels.
