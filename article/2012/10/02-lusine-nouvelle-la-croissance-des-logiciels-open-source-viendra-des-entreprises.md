---
site: L'USINE NOUVELLE
title: "\"La croissance des logiciels open source viendra des entreprises\""
author: Ridha Loukil
date: 2012-10-02
href: http://www.usinenouvelle.com/article/la-croissance-des-logiciels-open-source-viendra-des-entreprises.N183157
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Europe
---

> L’industrie des logiciels open source salue l’impulsion donnée par le gouvernement à leur secteur. Mais l’impact en serait modeste. Le principal moteur de développent résidant dans l’adoption des logiciels libres par les entreprises.
