---
site: Libération.fr
title: "Impression 3D: «Le rêve, c’est d’en finir avec la production de masse»"
date: 2012-10-01
href: http://www.liberation.fr/economie/2012/10/01/l-impression-3d-nouvelle-revolution-industrielle_850035
tags:
- Entreprise
- Économie
- Matériel libre
- Innovation
- English
---

> «Imprimer» chez soi à l’unité toute sorte d’objets, tasse, bijou, figurine, outil, c’est possible. Qu’est-ce que cela change? Est-ce une nouvelle révolution industrielle? Coralie Schaub, journaliste au service EcoFutur, a répondu à vos questions.
