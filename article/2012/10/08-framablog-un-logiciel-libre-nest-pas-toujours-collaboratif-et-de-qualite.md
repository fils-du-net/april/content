---
site: Framablog
title: "Un logiciel libre n'est pas toujours collaboratif et de qualité"
author: Benjamin Mako Hill
date: 2012-10-08
href: http://www.framablog.org/index.php/post/2012/10/08/open-source-qualite-collaboration
tags:
- Logiciels privateurs
- Licenses
- Philosophie GNU
---

> Oui il existe des logiciels libres de mauvaise qualité qui ne souffrent pas la comparaison avec leurs concurrents propriétaires! Et oui encore la majorité des logiciels libres sont uniquement développés par un seul et unique contributeur: leur créateur!
