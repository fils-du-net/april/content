---
site: "les-infostrateges.com"
title: "Logiciel libre et Administration: Une circulaire du premier ministre"
author: Didier FROCHOT
date: 2012-10-29
href: http://www.les-infostrateges.com/actu/12101523/logiciel-libre-et-administration-une-circulaire-du-premier-ministre
tags:
- Administration
- Institutions
- Droit d'auteur
- Open Data
---

> Une circulaire du premier ministre est presque passée inaperçue, d'autant plus inaperçue qu'elle n'est curieusemement toujours pas accessible par le moteur de recherche du site circulaire.legifrance.gouv.fr, alors même qu'elle existe sur ce site...
