---
site: PC INpact
title: "La fondation Terra Nova formule 123 propositions sur le numérique"
author: Xavier Berne
date: 2012-10-15
href: http://www.pcinpact.com/news/74541-la-fondation-terra-nova-formule-123-propositions-sur-numerique.htm
tags:
- Entreprise
- Économie
- HADOPI
- Institutions
- Licenses
- Neutralité du Net
- Open Data
---

> La fondation Terra Nova, traditionnellement située à gauche, a publié aujourd’hui un rapport intitulé «Numérique: renouer avec les valeurs progressistes et dynamiser la croissance». Ce document formule 123 propositions destinées à combler le retard pris par la France en matière de numérique. Sont notamment sur la table: une revalorisation des pouvoirs de la CNIL, l'inscription de la neutralité du net dans la loi, la mise en place d'une licence «créative», la fin du volet répressif de la Hadopi, etc.
