---
site: LeJournalduNet
title: "Grenelle de l'Open Source: les 10 propositions du CNLL"
author: Antoine Crochet-Damais
date: 2012-10-12
href: http://www.journaldunet.com/solutions/dsi/cnll-1012.shtml
tags:
- Entreprise
- Économie
- Interopérabilité
- Marchés publics
- Standards
---

> A l'occasion de l'Open World Forum, le Conseil national du logiciel libre a présenté 10 propositions qu'il fait au gouvernement. Objectif: définir une politique industrielle du logiciel libre.
