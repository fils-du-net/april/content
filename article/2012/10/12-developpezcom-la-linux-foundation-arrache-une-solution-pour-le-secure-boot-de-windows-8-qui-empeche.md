---
site: Developpez.com
title: "La Linux Foundation arrache une solution pour le Secure Boot de Windows 8, qui empêche le démarrage d'autres systèmes"
author: nissacomet
date: 2012-10-12
href: http://www.developpez.com/actu/48528/La-Linux-Foundation-arrache-une-solution-pour-le-Secure-Boot-de-Windows-8-qui-empeche-le-demarrage-d-autres-systemes
tags:
- Entreprise
- Interopérabilité
- DRM
---

> Depuis que Microsoft a opté pour le “Secure Boot” pour les PC sous Windows 8, un grand désarroi règne dans la communauté Linux. Cette fonctionnalité de démarrage sécurisé, directement intégrée à l’UEFI (interface micrologicielle extensible unifiée), empêche de facto l’installation de tout autre système d'exploitation. Microsoft transmet en effet une signature numérique aux constructeurs de cartes mères certifiées Windows 8.
