---
site: LE FIGARO.fr
title: "CloudWatt veut embrasser la révolution numérique"
author: Marc Cherki
date: 2012-10-02
href: http://www.lefigaro.fr/societes/2012/10/02/20005-20121002ARTFIG00519-cloudwatt-veut-embrasser-la-revolution-numerique.php
tags:
- Entreprise
- Institutions
- Informatique en nuage
---

> Fondée par Orange Business Services, Thales et l'Etat, l'entreprise spécialisée dans le «cloud computing» ou l'informatique dans le nuage, a une partie cruciale à jouer pour l'indépendance technologique de l'Europe.
