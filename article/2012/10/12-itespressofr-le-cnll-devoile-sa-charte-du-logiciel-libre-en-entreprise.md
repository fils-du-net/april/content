---
site: ITespresso.fr
title: "Le CNLL dévoile sa charte du logiciel libre en entreprise"
date: 2012-10-12
href: http://www.itespresso.fr/logiciels-libres-entreprise-dix-propositions-cnll-dix-propositions-passer-ere-industrielle-57696.html
tags:
- Entreprise
- Institutions
- Innovation
- Standards
---

> Dans la lignée de la circulaire Ayrault rendue publique fin septembre, le Conseil national du logiciel libre se prononce en faveur d’une «politique industrielle de l’open source».
