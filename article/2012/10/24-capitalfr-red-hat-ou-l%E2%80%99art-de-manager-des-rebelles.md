---
site: Capital.fr
title: "Red Hat, ou l’art de manager des rebelles"
author: Anne Cagan
date: 2012-10-24
href: http://www.capital.fr/enquetes/strategie/red-hat-ou-l-art-de-manager-des-rebelles-769672
tags:
- Entreprise
---

> Contraint de recruter parmi les militants de la cause, le champion du logiciel libre doit injecter dans sa politique RH les valeurs égalitaires chères à la tribu geek.
