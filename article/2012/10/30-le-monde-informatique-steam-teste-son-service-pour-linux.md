---
site: Le Monde Informatique
title: "Steam teste son service pour Linux"
author: Relaxnews
date: 2012-10-30
href: http://www.lemondeinformatique.fr/actualites/lire-steam-teste-son-service-pour-linux-51051.html
tags:
- Entreprise
- Logiciels privateurs
- Interopérabilité
- DRM
---

> Le lancement en grande pompe de Windows 8, le 26 octobre, a poussé le service de téléchargements de jeux PC Steam à tâter le terrain de Linux en guise d'alternative à Windows, et les gamers sont invités à participer à la phase bêta.
