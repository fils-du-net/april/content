---
site: Le Monde Informatique
title: "Logiciels libres: l'Etat prône une approche pragmatique"
author: Bertrand Lemaire
date: 2012-10-12
href: http://www.lemondeinformatique.fr/actualites/lire-logiciels-libres-l-etat-prone-une-approche-pragmatique-50817.html
tags:
- Administration
- Économie
- Licenses
- Marchés publics
---

> Jérôme Filippini est intervenu le 11 octobre 2012 à l'OpenWorld Forum (OWF) pour expliquer l'attitude de l'Etat vis-à-vis du logiciel libre.
