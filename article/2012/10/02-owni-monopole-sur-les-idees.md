---
site: OWNI
title: "Monopole sur les idées"
author: Sabine Blanc et Ophelia Noor
date: 2012-10-02
href: http://owni.fr/2012/10/02/monopole-sur-les-idees-brevet-logiciel-interview-richard-stallman
tags:
- April
- HADOPI
- Matériel libre
- Institutions
- Brevets logiciels
- Philosophie GNU
- Promotion
---

> La communauté du logiciel libre et son gourou Richard Stallman se mobilisent contre le projet de loi visant à unifier le système des brevets dans l'Union européenne. Qui permettrait, quarante ans après son interdiction, le grand retour du brevet logiciel. Décryptage.
