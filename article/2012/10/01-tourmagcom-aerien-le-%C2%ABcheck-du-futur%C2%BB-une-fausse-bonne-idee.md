---
site: TourMaG.com
title: "Aérien: le «check in du futur», une fausse bonne idée?"
author: Claude BOUMAL
date: 2012-10-01
href: http://www.tourmag.com/Aerien-le-check-in-du-futur--une-fausse-bonne-idee_a54382.html
tags:
- Entreprise
---

> C'est une petite révolution à laquelle se prépare Amadeus qui, d'ici 12 à 18 mois, devrait avoir achevé la migration, entamée depuis une dizaine d'années, de son logiciel «propriétaire» vers un système totalement ouvert, en «open source».
