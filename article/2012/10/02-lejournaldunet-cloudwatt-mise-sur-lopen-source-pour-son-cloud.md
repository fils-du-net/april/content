---
site: LeJournalduNet
title: "Cloudwatt mise sur l'Open Source pour son Cloud"
author: Virgile Juhan
date: 2012-10-02
href: http://www.journaldunet.com/solutions/cloud-computing/cloudwatt-un-iaas-a-la-francaise-1012.shtml
tags:
- Entreprise
- Informatique en nuage
---

> Fruit des investissements de l'Etat français, mais aussi d'Orange et Thalès, le IaaS à la française de Cloudwatt affiche des ambitions européennes. Il faudra néanmoins attendre encore quelques semaines pour avoir les détails de cette offre.
