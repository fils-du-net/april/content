---
site: Rue89 LES BLOGS
title: "Libre accès: les chercheurs défendent leurs travaux et l’esprit d’Internet"
author: Pierre-Carl Langlais
date: 2012-10-23
href: http://blogs.rue89.com/les-coulisses-de-wikipedia/2012/10/23/libre-acces-les-chercheurs-defendent-leurs-travaux-et-lesprit
tags:
- Entreprise
- Internet
- Partage du savoir
- Sciences
---

> Au début des années 2000, les milieux scientifiques se montrent de plus en plus favorables aux grands principes du libre accès («open access»): accès non facturé, publicité accrue, y compris auprès du grand public, possibilité de citer tout ou une portion de l’article…
