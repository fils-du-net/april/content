---
site: Numerama
title: "Hadopi: les travaux sur les moyens de sécurisation au point mort?"
author: Julien L.
date: 2012-10-17
href: http://www.numerama.com/magazine/24042-hadopi-les-travaux-sur-les-moyens-de-securisation-au-point-mort.html
tags:
- Internet
- HADOPI
---

> Dans leur rapport d'activité 2012, les membres de la Haute Autorité ont indiqué que "les problématiques liées à la sécurisation de l'accès à Internet doivent s'inscrire dans une approche globale". Une manière de dire que les travaux sur les moyens de sécurisation, engagés il y a plus de deux ans, sont dans une impasse?
