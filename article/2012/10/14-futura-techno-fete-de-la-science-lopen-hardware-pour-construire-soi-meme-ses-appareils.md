---
site: "Futura-Techno"
title: "Fête de la science: l'open hardware, pour construire soi-même ses appareils"
author: Sylvain Biget
date: 2012-10-14
href: http://www.futura-sciences.com/fr/news/t/robotique/d/fete-de-la-science-lopen-hardware-pour-construire-soi-meme-ses-appareils_41876/
tags:
- Partage du savoir
- Matériel libre
- Innovation
---

> Sur le même principe que celui de l’open source pour les logiciels, des bidouilleurs diffusent librement les plans d’objets ou d’appareils électroniques pour que tout un chacun puisse les réaliser et les améliorer à moindres frais. Voici donc l'open hardware. Le phénomène prend de l’ampleur, notamment grâce au développement des imprimantes 3D.
