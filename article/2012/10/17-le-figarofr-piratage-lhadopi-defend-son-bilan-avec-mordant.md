---
site: LE FIGARO.fr
title: "Piratage: l'Hadopi défend son bilan avec mordant"
author: Benjamin Ferran
date: 2012-10-17
href: http://www.lefigaro.fr/hightech/2012/10/17/01007-20121017ARTFIG00569-piratage-l-hadopi-defend-son-bilan-avec-mordant.php
tags:
- Internet
- HADOPI
- Institutions
- Droit d'auteur
- Sciences
- Video
- Europe
- International
---

> L'Hadopi ne se fait plus guère d'illusion. L'autorité chargée de la protection des droits d'auteur sur Internet est vouée à se transformer radicalement, voire à disparaître. Mais en attendant les conclusions de la mission de Pierre Lescure, qui statuera avant mars 2013 sur son avenir, l'institution continue de se défendre avec force.
