---
site: The IPKat
title: "Max Planck publishes Top 12 Reasons why the unitary patent proposals might prove \"disastrous\""
author: Annsley Merelle Ward
date: 2012-10-18
href: http://ipkitten.blogspot.co.uk/2012/10/max-planck-publishes-top-12-reasons-why.html
tags:
- Institutions
- Brevets logiciels
- Innovation
- Europe
- English
---

> (L'intitut Max Planck a publié une liste de raisons expliquant en quoi le brevet unitaire est inquiétant) The Max Planck Institute for Intellectual Property and Competition Law - the bastion of unbiased legal competence - has published its Top 10 list for why the unitary patent proposals are of concern.  Authored by Reto M. Hilty, Thomas Jaeger, Matthias Lamping and Hanns Ullrich - the 12 points are categorized under three main headings - complexity, imbalance and uncertainty.
