---
site: Place Publique
title: "Média citoyen, relais d’engagements associatifs et outil de démocratie participative"
author: D.Sabo
date: 2012-10-05
href: http://www.place-publique.fr/spip.php?article6660
tags:
- Entreprise
- Administration
- Économie
- Associations
- Open Data
---

> De plus en plus d’entreprises, d’établissements ou d’associations s’accordent à penser que la libération des données brutes est tout bénéfice aussi bien sur le plan de la communication entre humains que sur le plan des solutions d’innovations en entreprise. Avec l’Open Data s’installe un nouveau monde: celui de la confiance et de l’autonomie. 2013 devrait être l’année du véritable du décollage du phénomène open data en France.
