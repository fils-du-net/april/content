---
site: Le Monde Informatique
title: "Les métiers de l'Open Source sont les plus recherchés"
author: Véronique Arène
date: 2012-10-09
href: http://www.lemondeinformatique.fr/actualites/lire-les-metiers-de-l-open-source-sont-les-plus-recherches-50774.html
tags:
- Entreprise
- Économie
---

> Des articles publiés sur des sites américains prédisent une montée en puissance du besoin en professionnels des langages de programmation Open Source.
