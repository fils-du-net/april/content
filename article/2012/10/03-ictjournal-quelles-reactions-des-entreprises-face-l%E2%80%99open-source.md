---
site: ICTjournal
title: "Quelles réactions des entreprises face à l’open source?"
author: Bastien Brodard
date: 2012-10-03
href: http://www.ictjournal.ch/fr-CH/News/2012/10/03/Quelles-reactions-des-entreprises-face-a-lopen-source.aspx
tags:
- Entreprise
- Économie
- Associations
- International
---

> Les premiers résultats de l’étude suisse «Open Source Studie 2012» menée par Swiss ICT et /ch/open montrent que  montrer que la grande majorité des entreprises utilise des logiciels open source. Les résultats détaillés seront présentés le 23 octobre.
