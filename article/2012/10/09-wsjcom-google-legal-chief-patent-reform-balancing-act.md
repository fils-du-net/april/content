---
site: WSJ.com
title: "Google Legal Chief: Patent Reform a Balancing Act"
author: Evan Ramstad
date: 2012-10-09
href: http://blogs.wsj.com/digits/2012/10/09/google-legal-chief-patent-reform-a-balancing-act/
tags:
- Entreprise
- Brevets logiciels
- International
- English
---

> (le directeur juridique de Google s'exprime sur le système des brevets des U.S. afin de réduire les litiges) Google Inc.’s chief legal officer, David Drummond, said in visit to Seoul Tuesday that the company is walking a fine line as it seeks reforms in the U.S. patent system in an effort to reduce the amount of litigation around mobile computing software and devices.
