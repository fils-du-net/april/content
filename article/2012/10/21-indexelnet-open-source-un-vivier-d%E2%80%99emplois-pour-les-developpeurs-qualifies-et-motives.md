---
site: indexel.net
title: "Open source: un vivier d’emplois pour les développeurs qualifiés et motivés"
author: Marie Varandat
date: 2012-10-21
href: http://www.indexel.net/actualites/open-source-un-vivier-d-emplois-pour-les-developpeurs-qualifies-et-motives-3666.html
tags:
- Entreprise
- Économie
- Innovation
---

> Selon une étude récente, le secteur de l’open source aurait généré plus de 3 500 emplois en trois ans. Principaux concernés par ces nouveaux postes: les profils BAC +4 et plus.
