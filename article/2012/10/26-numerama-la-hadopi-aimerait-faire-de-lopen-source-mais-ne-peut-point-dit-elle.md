---
site: Numerama
title: "La Hadopi aimerait faire de l'open-source, mais ne peut point (dit-elle)"
author: Guillaume Champeau
date: 2012-10-26
href: http://www.numerama.com/magazine/24120-la-hadopi-aimerait-faire-de-l-open-source-mais-ne-peut-point-dit-elle.html
tags:
- Internet
- HADOPI
---

> La Hadopi assure qu'elle aimerait avoir l'appui de développeurs pour réaliser un outil open-source, mais que "ses relations avec la communauté ne lui permettent pas". Vrai problème, ou fausse excuse?
