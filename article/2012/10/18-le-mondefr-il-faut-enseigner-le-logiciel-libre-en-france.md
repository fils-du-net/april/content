---
site: Le Monde.fr
title: "Il faut enseigner le logiciel libre en France"
author: Patrice Bertrand, Roberto Di Cosmo et Stéfane Fermigier
date: 2012-10-18
href: http://www.lemonde.fr/sciences/article/2012/10/18/il-faut-enseigner-le-logiciel-libre-en-france_1777703_1650684.html
tags:
- Économie
- Éducation
- Innovation
---

> Alors que, par une circulaire du premier ministre Jean-Marc Ayrault, le gouvernement vient d'affirmer l'importance du logiciel libre dans les systèmes d'information de l'Etat et affiche une politique volontariste pour en accompagner l'utilisation, nous croyons indispensable d'intégrer l'étude des logiciels libres dans la formation des futurs ingénieurs.
