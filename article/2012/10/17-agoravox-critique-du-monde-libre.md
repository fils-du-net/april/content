---
site: AgoraVox
title: "Critique du monde libre"
author: mozee
date: 2012-10-17
href: http://www.agoravox.fr/actualites/technologies/article/critique-du-monde-libre-124317
tags:
- Économie
- Innovation
---

> Suite de la série d'articles sur les logiciels libres, après trois mots sur la philosophie qui sous-tend ceux-ci et un bref rappel historique, place à la contradiction, avec un résumé des principales critiques contre le modèle libre, et les réponses qu'on peut y apporter. Si le lecteur a des objections non recensées dans ce chapitre, il est tout à fait bienvenu de commenter celui-ci.
