---
site: 20 minutes online
title: "Créez vos morceaux avec les samples d'artistes"
author: Michel Annese
date: 2012-10-19
href: http://www.20min.ch/ro/multimedia/stories/story/26001652
tags:
- Internet
- Économie
- Partage du savoir
- Droit d'auteur
- Licenses
---

> Le nouveau service de partage musical romand permet aux internautes de collaborer sur des morceaux en partageant leurs beats et vidéos.
