---
site: Le Point
title: "Cinq idées pour réinventer la France à l'ère du numérique, par Tariq Krim"
author: Guillaume Grallet
date: 2012-10-30
href: http://www.lepoint.fr/technologie/cinq-idees-pour-reinventer-la-france-a-l-ere-du-numerique-par-tariq-krim-30-10-2012-1523101_58.php
tags:
- Économie
- Institutions
---

> L'entrepreneur, un des plus fins connaisseurs du Web, avance cinq propositions originales, concrètes, et particulièrement pertinentes. Chiche?
