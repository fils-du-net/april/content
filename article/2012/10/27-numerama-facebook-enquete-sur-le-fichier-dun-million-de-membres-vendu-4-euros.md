---
site: Numerama
title: "Facebook enquête sur le fichier d'un million de membres vendu 4 euros"
author: Julien L.
date: 2012-10-27
href: http://www.numerama.com/magazine/24135-facebook-enquete-sur-le-fichier-d-un-million-de-membres-vendu-4-euros.html
tags:
- Entreprise
- Internet
- Informatique-deloyale
---

> Après la découverte d'un fichier vendu 4 euros et contenant les informations personnelles de plus d'un million de membres de Facebook, le réseau social passe à l'action. Après avoir contacté le blogueur à l'origine de la découverte, le site communautaire s'efforce désormais de trouver l'origine de la fuite.
