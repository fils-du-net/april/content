---
site: Le Monde.fr
title: "Microsoft reboote l'informatique"
author: David Larousserie
date: 2012-10-26
href: http://www.lemonde.fr/sciences/article/2012/10/25/microsoft-reboote-l-informatique_1781096_1650684.html
tags:
- Le Logiciel Libre
- Logiciels privateurs
- Matériel libre
- Vente liée
- Informatique-deloyale
---

> "Secure Boot sera probablement une entrave à l'adoption de logiciels libres à cause du statut de monopole de Microsoft sur les systèmes préinstallés", explique John Sullivan, directeur exécutif de la Free software foundation, qui a lancé une pétition contre le restricted boot.
