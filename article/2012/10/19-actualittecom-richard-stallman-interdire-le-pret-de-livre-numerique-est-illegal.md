---
site: ActuaLitté.com
title: "Richard Stallman: interdire le prêt de livre numérique est illégal"
date: 2012-10-19
href: http://www.actualitte.com/interviews/richard-stallman-interdire-le-pret-de-livre-numerique-est-illegal-1836.htm
tags:
- Internet
- Économie
- DRM
- Droit d'auteur
- Philosophie GNU
---

> C'est l'homme de la défense des libertés dans le monde informatique: Richard Stallman, ambassadeur des licences Creative Commons, de la liberté de partager et du respect du droit des auteurs, n'a jamais caché que le livre numérique lui posait problème. «Je ne suis pas contre le principe du livre numérique à tout prix quels qu'en soient les conditions. Mais si le livre m'ôte de la liberté, je le rejette.
