---
site: Le Monde.fr
title: "Deux ans après sa création, la Hadopi veut recentrer ses missions"
author: Guénaël Pépin
date: 2012-10-17
href: http://www.lemonde.fr/technologies/article/2012/10/17/deux-ans-apres-sa-creation-la-hadopi-veut-recentrer-ses-missions_1776914_651865.html
tags:
- Internet
- HADOPI
- Institutions
- DRM
- Droit d'auteur
---

> La Haute Autorité pour la diffusion des œuvres et la protection des droits sur Internet (Hadopi) a deux ans et tient à démontrer l'expérience acquise. "Rarement autorité n'aura suscité autant d'attentes et d'interrogations", assène Marie-Françoise Marais, sa présidente, en introduction à la conférence de bilan annuel. Des "attaques constantes" auraient d'ailleurs rythmé une année chargée pour l'institution, qui voit son rôle remis en cause à l'occasion de la mission Lescure et son budget réduit de plus d'un million d'euros.
