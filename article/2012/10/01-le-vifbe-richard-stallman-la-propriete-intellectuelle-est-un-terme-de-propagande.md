---
site: LE VIF.be
title: "Richard Stallman: \"la propriété intellectuelle est un terme de propagande\""
author: Fabrice Imbert
date: 2012-10-01
href: http://www.levif.be/info/actualite/technologie/richard-stallman-la-propriete-intellectuelle-est-un-terme-de-propagande/article-4000186584454.htm
tags:
- Logiciels privateurs
- Philosophie GNU
---

> Richard Stallman, l’inventeur du système d’exploitation informatique GNU était à Bruxelles jeudi et vendredi pour partager sa vision d’une «société numérique libre». Rencontre avec le «pape» du logiciel libre qui se bat contre les grandes entreprises informatiques et défend une éthique de l’informatique.
