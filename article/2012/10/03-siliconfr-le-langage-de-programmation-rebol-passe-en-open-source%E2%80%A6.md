---
site: Silicon.fr
title: "Le langage de programmation Rebol passe en open source…"
author: David Feugey
date: 2012-10-03
href: http://www.silicon.fr/carl-sassenrath-rebol-programmation-79187.html
tags:
- Innovation
- Licenses
---

> Original et novateur, Rebol n’a pas su convaincre. Son créateur, Carl Sassenrath, souhaite aujourd’hui le libérer sous licence open source.
