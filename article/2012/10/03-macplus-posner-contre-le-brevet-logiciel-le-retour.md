---
site: MacPlus
title: "Posner contre le brevet logiciel: le retour"
author: iShen
date: 2012-10-03
href: http://www.macplus.net/depeche-68896-posner-contre-le-brevet-logiciel-le-retour
tags:
- Institutions
- Brevets logiciels
- Innovation
---

> Le juge Posner n’en a pas fini avec les brevets logiciels. Déjà auteur d’une première sortie après avoir décidé d’annuler purement et simplement des plaintes croisées entre Apple et Motorola, le bonhomme repart de plus belle.
