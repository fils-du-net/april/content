---
site: "cio-online.com"
title: "OpenWorldForum: vif échange entre Fleur Pellerin et l'adjoint au maire de Paris"
author: Bertrand Lemaire
date: 2012-10-11
href: http://www.cio-online.com/actualites/lire-openworldforum-2012%C2%A0-le-logiciel-libre-au-coeur-des-usages-numeriques-personnels-et-professionnels-4592.html
tags:
- Entreprise
- Économie
- Institutions
- Innovation
---

> L'évènement du logiciel libre à Paris, l'OpenWorldForum, s'est ouvert le 11 octobre 2012. Fleur Pellerin, ministre de l'économie numérique a répondu à l'adjoint au maire de Paris en charge de l'innovation, sur la taxation des plus-values liées aux start-up. Elle annonce par ailleurs la création d'un Habeas Corpus numérique en 2013 et d'un quartier de Paris dédié aux nouvelles technologies.
