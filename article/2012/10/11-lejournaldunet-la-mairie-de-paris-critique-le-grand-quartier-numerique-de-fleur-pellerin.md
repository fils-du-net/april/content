---
site: LeJournalduNet
title: "La Mairie de Paris critique le \"grand quartier numérique\" de Fleur Pellerin"
author: Antoine Crochet-Damais
date: 2012-10-11
href: http://www.journaldunet.com/solutions/dsi/fleur-pellerin-et-jean-louis-missika-a-l-open-world-forum-2012-1012.shtml
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Innovation
---

> Lors de l'Open World Forum 2012, la ministre déléguée à l'Economie numérique est revenue sur sa proposition de faire de Paris la capitale mondiale du numérique. Egalement invité à s'exprimer pour l'occasion, Jean-Louis Missika, adjoint au Maire de Paris, a vivement réagi à cette proposition.
