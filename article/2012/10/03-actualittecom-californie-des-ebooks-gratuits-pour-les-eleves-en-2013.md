---
site: ActuaLitté.com
title: "Californie: des ebooks gratuits pour les élèves en 2013"
author: Ania Vercasson
date: 2012-10-03
href: http://www.actualitte.com/education-international/californie-des-ebooks-gratuits-pour-les-eleves-en-2013-37166.htm
tags:
- Administration
- Éducation
- Contenus libres
- International
---

> Alors que ces dernières années les frais scolaires n'ont cessé d'augmenter en Californie, les autorités ont décidé de s'y prendre autrement. La majorité des étudiants payent plus de 1 000 dollars par an pour des manuels scolaires, parfois au détriment de la nourriture ou d'autres dépenses. C'est pourquoi, un projet de loi vient d'être lancé.
