---
site: ZDNet.fr
title: "Open World Forum 2012: l'open source cherche sa voie dans le cloud"
author: Antoine Duvauchelle
date: 2012-10-03
href: http://www.zdnet.fr/actualites/open-world-forum-2012-l-open-source-cherche-sa-voie-dans-le-cloud-39783196.htm
tags:
- Entreprise
- Interopérabilité
- Standards
- Informatique en nuage
---

> L'Open World Forum a dans ses cartons de quoi plancher : du 11 au 13 octobre prochains à Paris, il devra aborder tout l'univers de l'open source. Parmi les tendances, le cloud est peut-être celle qui remet le plus en cause ses principes.
