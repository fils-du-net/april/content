---
site: AgoraVox
title: "La technique est un instrument de la domination"
author: mozee
date: 2012-10-12
href: http://www.agoravox.fr/actualites/technologies/article/la-technique-est-un-instrument-de-124100
tags:
- Partage du savoir
- Philosophie GNU
- Sciences
---

> L'histoire du logiciel, corollaire et prolongement de la complexité technique développée au cours des deux derniers siècles, devrait être le marqueur d'un saut civilisationnel. En effet, s'il est communément admis que le savoir est le pouvoir, la façon dont s'articulent ces deux piliers de l'humanité a évolué au fil du temps.
