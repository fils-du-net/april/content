---
site: Mediapart
title: "Les journalistes débarquent-ils de Mars?"
author: yanch
date: 2012-10-06
href: http://blogs.mediapart.fr/blog/yanch/061012/les-journalistes-de-france-inter-debarquent-ils-de-mars
tags:
- Entreprise
- Institutions
---

> «20 000 commerçants du Sud-Est auraient fraudé le fisc»: voilà le titre de l'article de Marie-Christine Lauriol diffusé hier sur Inter. Bon, je cite Inter car c'est quasiment la seule radio que j'écoute mais je suppose que la majorité des media ont relayé l'info...
