---
site: AgoraVox
title: "Comment un logiciel peut-il être «libre»?"
author: mozee
date: 2012-10-13
href: http://www.agoravox.fr/actualites/technologies/article/comment-un-logiciel-peut-il-etre-124130
tags:
- Partage du savoir
- Sciences
---

> La société de la connaissance n'est pas née avec les ordinateurs, mais ceux-ci ont impulsé de grands changements dans notre rapport au savoir, notamment grâce au faible coût de la copie numérique et au réseau.
