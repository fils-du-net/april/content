---
site: 01net.
title: "Les 100 millions qui ont fait de Steve Jobs un zélateur des brevets"
author: Pierre Fontaine
date: 2012-10-09
href: http://www.01net.com/editorial/577365/2006-lannee-ou-apple-et-steve-jobs-ont-bascule-dans-les-brevets
tags:
- Entreprise
- Économie
- Brevets logiciels
---

> Un an avant la sortie de l’iPhone, une querelle entre Apple et Creative se solde par un chèque de 100 millions de dollars fait par Cupertino. Le point de départ d’une nouvelle politique du tout brevet, qui a abouti à la situation actuelle.
