---
site: LeMagIT
title: "Open World Forum: une édition 2012 placée sous le signe de l’emploi et de la formation dans la filière"
author: Cyrille Chausson
date: 2012-10-04
href: http://www.lemagit.fr/economie/business/2012/10/04/open-world-forum-une-edition-2012-placee-sous-le-signe-de-lemploi-et-de-la-formation-dans-la-filiere
tags:
- Entreprise
- Économie
---

> L’édition 2012 de l’Open World Forum, qui se veut toujours l’un des plus grands carrefours de l’Open Source en Europe, sera témoin de la publication de la Charte Emploi Libre qui devra identifier les entreprises actives dans la filière. Une façon de structurer l’emploi et d’identifier les entreprises respectueuses des pratiques Open Source afin de soutenir la croissance promise du marché.
