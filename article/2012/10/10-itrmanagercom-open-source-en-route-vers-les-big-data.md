---
site: ITRmanager.com
title: "Open source: en route vers les Big Data"
author: Bertrand Diard
date: 2012-10-10
href: http://www.itrmanager.com/articles/135938/open-source-route-vers-big-data-bertrand-diard-co-fondateur-ceo-talend.html
tags:
- Entreprise
- Économie
- Innovation
---

> Les solutions open source ne véhiculent plus l’image d’un pari risqué pour les entreprises qui leur collait initialement à la peau. Elles font désormais partie intégrante de toute entreprise. Les chiffres le prouvent: en 2011, on estime que 672,8 millions de dollars de capital-risque ont été investis dans des entreprises concernées par l’open source, soit une hausse de plus de 48% par rapport à 2010. Le volume total investi en une année est le plus important de toute l’histoire.
