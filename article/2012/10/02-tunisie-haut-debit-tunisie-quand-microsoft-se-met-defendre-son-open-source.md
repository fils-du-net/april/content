---
site: Tunisie haut débit
title: "Tunisie: Quand Microsoft se met à défendre son Open Source"
author: Seif Eddine Akkari
date: 2012-10-02
href: http://www.thd.tn/index.php?view=article&id=2108
tags:
- Entreprise
- Logiciels privateurs
- International
---

> Quand on parle de l’Open Source en général, rares sont ceux qui citent Microsoft comme acteur actif. Pourtant, la firme de Redmond est bien présente en ce domaine. En ce sens, une table ronde a été organisée le mercredi 26 septembre.
