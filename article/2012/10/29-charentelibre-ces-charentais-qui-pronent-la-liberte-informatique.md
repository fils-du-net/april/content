---
site: CharenteLibre
title: "Ces Charentais qui prônent la liberté informatique"
author: Benoît Caurette
date: 2012-10-29
href: http://www.charentelibre.fr/2012/10/29/leur-liberte-en-fond-d-ecran,1121833.php
tags:
- Administration
- Partage du savoir
- Associations
---

> Ils voient l'informatique autrement. Ces Charentais refusent le «diktat» des géants et prônent le Libre. Témoignages à Javrezac et à La Couronne. Et vous, êtes-vous adeptes des logiciels libres?
