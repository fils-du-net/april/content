---
site: LADEPECHE.fr
title: "Saint-Eutrope-de-Born. Informatique et logiciels libres"
author: La Dépêche du Midi
date: 2012-03-29
href: http://www.ladepeche.fr/article/2012/03/29/1318146-saint-eutrope-de-born-informatique-et-logiciels-libres.html
tags:
- Internet
- Administration
- Sensibilisation
- Philosophie GNU
---

> L'Atelier du Libre et Gimp de l'Espace Intergénérationnel et multimédia organisait une journée de débat-réflexion sur l'informatique libre avec 2 intervenants d'aGeNUx. L'utilisateur a besoin d'un ordinateur, d'un système d'exploitation, d'applications logicielles.
