---
site: LADEPECHE.fr
title: "Saint-Eutrope-de-Born. Journée du logiciel libre"
date: 2012-03-09
href: http://www.ladepeche.fr/article/2012/03/09/1301873-saint-eutrope-de-born-journee-du-logiciel-libre.html
tags:
- Institutions
- Sensibilisation
- Promotion
---

> Ce sera vendredi 16 mars que «l'Espace intergénérationnel et multimédia» ouvrira ses portes et ordinateurs au logiciel libre avec divers rendez-vous: table ronde de 20 h à 21h 30 «D'autres systèmes d'exploitation? Une autre information possible…» par des intervenants d'Agenux; mais dès 14 h, exposition «Des images pour découvrir le Libre» réalisée par les ateliers 1/Le Libre et 2/Le GIMP.
