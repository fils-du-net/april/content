---
site: Génération nouvelles technologies
title: "Insee: le logiciel libre dans les entreprises"
author: Jérôme G.
date: 2012-03-15
href: http://www.generation-nt.com/logiciel-libre-insee-tic-2011-etude-actualite-1556391.html
tags:
- Entreprise
---

> Selon les résultats de l'enquête TIC 2011 de l'Insee, le logiciel libre semble progresser dans les sociétés de plus de dix employés.
