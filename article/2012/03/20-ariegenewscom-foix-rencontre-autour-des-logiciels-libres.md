---
site: ariegenews.com
title: "Foix: rencontre autour des logiciels libres"
date: 2012-03-20
href: http://www.ariegenews.com/ariege/le_saviez_vous/2012/45352/foix-rencontre-autour-des-logiciels-libres.html
tags:
- Sensibilisation
- Associations
---

> Un logiciel libre est un logiciel dont l'utilisation, l'étude, la modification et la duplication en vue de sa diffusion sont permises, techniquement et légalement, afin de garantir certaines libertés induites, dont le contrôle du programme par l'utilisateur, et la possibilité de partage entre individus.
