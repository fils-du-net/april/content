---
site: LADEPECHE.fr
title: "Luc-la-Primaube. Logiciel «libres» en démonstration"
author: La Dépêche du Midi
date: 2012-03-23
href: http://www.ladepeche.fr/article/2012/03/23/1313050-luc-la-primaube-logiciel-libres-en-demonstration.html
tags:
- Administration
- Sensibilisation
- Associations
---

> Samedi, à la médiathèque de Luc-La Primaube, l'association ARU2L organisait une journée tournée vers la démonstration, la sensibilisation du public au fait qu'il existe des systèmes d'exploitation autres que ceux qui sont imposés à l'achat de votre ordinateur.
