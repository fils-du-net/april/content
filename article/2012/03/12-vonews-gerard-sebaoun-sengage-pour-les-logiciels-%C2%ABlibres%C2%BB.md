---
site: Vonews
title: "Gérard Sebaoun s'engage pour les logiciels «libres»"
date: 2012-03-12
href: http://www.vonews.fr/article_16782
tags:
- Internet
- April
- Institutions
---

> Le candidat socialiste aux prochaines législatives sur la 4e circonscription, Gérard Sebaoun, a signé le Pacte du Logiciel Libre proposé par l’association de promotion et de défense du logiciel libre (APRIL). Il revendique ainsi son engagement pour l’utilisation de ce type de programmes, gratuits et accessibles à tous.
