---
site: PC INpact
title: "Le Windows Store accepte les applications Open Source"
author: Vincent Hermann
date: 2012-03-07
href: http://www.pcinpact.com/news/69414-windows-8-store-licence.htm
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Droit d'auteur
- Licenses
---

> L’arrivée de la Consumer Preview de Windows 8 a ouvert le bal tant pour les utilisateurs que les développeurs. Dans la foulée, Microsoft a publié la bêta de Visual Studio 11 et les développeurs ont pu justement se pencher sur le nouvel environnement WinRT et donc la création d’applications Metro. Le Windows Store a également été ouvert aux applications tierces, et nous avons décidé de nous pencher sur le contrat relatif aux développeurs pour en tirer les principaux points.
