---
site: Numerama
title: "France Brevets signe avec l'INRIA. Mauvaise nouvelle pour le logiciel libre?"
author: Guillaume Champeau
date: 2012-03-16
href: http://www.numerama.com/magazine/22040-france-brevets-signe-avec-l-inria-mauvaise-nouvelle-pour-le-logiciel-libre.html
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Brevets logiciels
- Innovation
- Licenses
- Europe
---

> L'INRIA, qui rassemble 3400 chercheurs dédiés à l'innovation numérique, auteurs de nombreux logiciels libres, a signé un accord avec France Brevets pour vendre des licences de ses brevets non exploités. En quatre ans, l'institut de recherche publique a quadruplé le nombre de ses dépôts de brevets, et veut continuer sur cette lancée.
