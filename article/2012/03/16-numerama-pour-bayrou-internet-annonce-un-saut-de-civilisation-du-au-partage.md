---
site: Numerama
title: "Pour Bayrou, Internet annonce un \"saut de civilisation\" dû au partage"
author: Guillaume Champeau
date: 2012-03-16
href: http://www.numerama.com/magazine/21700-pour-bayrou-internet-annonce-un-saut-de-civilisation-du-au-partage.html
tags:
- Internet
- Institutions
- Éducation
- Licenses
---

> A l'occasion d'une table ronde de plus de deux heures autour des questions numériques, François Bayrou a tenu à affirmer mardi soir son attachement au numérique. Et surtout à montrer qu'il avait une conscience aiguë de ce qu'Internet changeait, selon lui, pour "la civilisation".
