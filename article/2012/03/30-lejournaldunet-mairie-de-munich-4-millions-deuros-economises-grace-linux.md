---
site: LeJournalduNet
title: "Mairie de Munich: 4 millions d'euros économisés grâce à Linux"
author: Dominique FILIPPONE
date: 2012-03-30
href: http://www.journaldunet.com/solutions/dsi/economies-grace-a-l-open-source-pour-munich-0312.shtml
tags:
- Entreprise
- Administration
- Économie
- International
---

> En passant d'un environnement Windows à Linux, la mairie de Munich dit avoir réalisé de substantielles économies. Le nombre d'appels au centre de support a aussi diminué de 35%.
