---
site: LADEPECHE.fr
title: "Villefranche-de-Rouergue. Pour des logiciels en toute liberté"
author: La Dépêche du Midi
date: 2012-03-20
href: http://www.ladepeche.fr/article/2012/03/20/1310167-pour-des-logiciels-en-toute-liberte.html
tags:
- Internet
- Administration
- Sensibilisation
- Associations
---

> Parce qu'ils estiment que les logiciels libres permettent de partager plus et encore toutes les potentialités de l'informatique, les animateurs de la Cyberbase et les membres de l'association rouergate des utilisateurs de logiciels libres s'invitent dans l'opération « libre en fête » qui débute ce mardi.
