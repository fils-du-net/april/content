---
site: Silicon.fr
title: "Le collectif 'industrie numérique' interpelle les candidats à la présidentielle"
author: Ariane Beky
date: 2012-03-20
href: http://www.silicon.fr/le-collectif-industrie-numerique-interpelle-les-candidats-a-la-presidentielle-72984.html
tags:
- Entreprise
- Internet
- Économie
- April
- Institutions
- Associations
- Innovation
---

> Treize associations, syndicats et chambres professionnelles des filières informatique, e-commerce et télécoms, déclarent former un collectif dans le but de placer le numérique au coeur de la campagne présidentielle 2012.
