---
site: Framablog
title: "«Le logiciel libre peut redonner sens à nos vies» Bernard Stiegler"
author: aKa
date: 2012-03-02
href: http://www.framablog.org/index.php/post/2012/03/02/bernard-stiegler-logiciel-libre
tags:
- Entreprise
- Internet
- Économie
- Associations
---

> Bernard Stiegler, un philosophe en lutte. Dans sa ligne de mire: un capitalisme addictif qui aspire le sens de nos existences. Son remède: une économie de la contribution.
