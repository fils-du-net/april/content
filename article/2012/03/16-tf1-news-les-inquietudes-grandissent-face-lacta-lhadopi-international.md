---
site: TF1 News
title: "Les inquiétudes grandissent face à l'ACTA, l'Hadopi international"
author: Jessica Dubois
date: 2012-03-16
href: http://lci.tf1.fr/high-tech/les-inquietudes-grandissent-face-a-l-acta-l-hadopi-au-niveau-international-7071924.html
tags:
- Internet
- Administration
- HADOPI
- International
- ACTA
---

> TROIS QUESTIONS-Alors que l'ACTA, traité international voulant lutter contre le piratage, suscite une vive opposition, le gouvernement américain semble vouloir mettre le régulateur d'Internet actuel, l'ICANN, sur la touche. Explications d'un de ses membres.
