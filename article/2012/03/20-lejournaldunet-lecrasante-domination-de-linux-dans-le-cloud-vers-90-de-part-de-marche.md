---
site: LeJournalduNet
title: "L'écrasante domination de Linux dans le Cloud: vers 90% de part de marché"
author: Patrice Bertrand
date: 2012-03-20
href: http://www.journaldunet.com/solutions/expert/51207/l-ecrasante-domination-de-linux-dans-le-cloud---vers-90--de-part-de-marche.shtml
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Standards
- Informatique en nuage
---

> A la domination de Microsoft sur le poste de travail répond une domination de Linux du côté serveur, et dans le Cloud. Les spécialistes le savent bien, mais ils manquent souvent de chiffres précis pour mesurer cette hégémonie, et percevoir son évolution. On peut l'estimer à 90%.
