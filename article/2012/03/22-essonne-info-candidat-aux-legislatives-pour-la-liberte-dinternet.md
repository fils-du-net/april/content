---
site: Essonne Info
title: "Candidat aux législatives pour la liberté d'internet"
author: Julien Monier
date: 2012-03-22
href: http://essonneinfo.fr/91-essonne-info/22493/candidat-aux-legislatives-pour-la-liberte-dinternet/
tags:
- Internet
- HADOPI
- Institutions
---

> Il se nomme Morvan Prévot. A dix-neuf ans, il a décidé de se présenter aux prochaines élections législatives, sur la circonscription d’Évry-Corbeil. Défenseur des libertés sur le Net, ce jeune Courcouronnais entend bien faire valoir ses idées dans le débat politique. Essonne Info l’a rencontré.
