---
site: basta!
title: "Bernard Stiegler: «Le marketing détruit tous les outils du savoir»"
date: 2012-03-20
href: http://www.bastamag.net/article2202.html
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Éducation
- Innovation
- Licenses
- RGI
- Video
- Europe
- International
---

> Vous êtes fatigués des petites phrases, des analyses politiques et médiatiques incapables de se projeter au-delà du prochain sondage? Basta!, en partenariat avec Soldes, la revue «pop et intello», vous propose une interview fleuve du philosophe Bernard Stiegler. Disciple de Derrida, il dirige l’Institut de recherche et d’innovation et a cofondé l’association Ars Industrialis.
