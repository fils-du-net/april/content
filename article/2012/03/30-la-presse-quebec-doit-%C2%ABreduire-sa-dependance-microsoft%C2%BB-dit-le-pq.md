---
site: La Presse
title: "Québec doit «réduire sa dépendance à Microsoft», dit le PQ"
author: Denis Lessard
date: 2012-03-30
href: http://www.cyberpresse.ca/actualites/quebec-canada/politique-quebecoise/201203/30/01-4511088-quebec-doit-reduire-sa-dependance-a-microsoft-dit-le-pq.php
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Institutions
- International
---

> Le renouvellement de 738 000 ordinateurs personnels dans l'administration publique québécoise est l'occasion rêvée pour le gouvernement de «réduire sa dépendance à Microsoft» et de réaliser d'énormes économies grâce au logiciel libre, estime l'opposition péquiste à l'Assemblée nationale.
