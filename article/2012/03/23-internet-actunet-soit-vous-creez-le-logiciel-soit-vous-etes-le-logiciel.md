---
site: internet ACTU.net
title: "Soit vous créez le logiciel, soit vous êtes le logiciel"
author: Douglas Rushkoff
date: 2012-03-23
href: http://www.internetactu.net/2012/03/23/soit-vous-creez-le-logiciel-soit-vous-etes-le-logiciel/
tags:
- Entreprise
- Internet
- Institutions
- Innovation
---

> FYP éditions vient de faire paraître Les dix commandements de l’ère numérique, la traduction du livre de l’essayiste et théoricien des médias américains Douglas Rushkoff (Wikipédia) Programmer ou être programmé: dix commandements pour l’ère numérique paru en 2010 et que nous avions déjà eu l’occasion d’évoquer.
