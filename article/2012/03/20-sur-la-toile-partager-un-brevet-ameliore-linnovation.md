---
site: Sur la Toile
title: "Partager un brevet améliore l'innovation"
author: gardenisto
date: 2012-03-20
href: http://www.sur-la-toile.com/article-14523-Partager-un-brevet-ameliore-l-innovation..html
tags:
- Entreprise
- Économie
- Brevets logiciels
- Innovation
- International
---

> Cette dernière décennie a vu la multiplication des protections industrielles au travers de l'utilisation à tout va des brevets, notamment aux États-Unis. À côté de cela, des secteurs à l'instar de l'informatique proposent des mondes complètement ouverts comme l'open source. Où est le modèle le plus intéressant? L'Université de Buffalo, New-York, a publié un papier dans un magasine économique de référence le «Economics Letters». Il apparaît que de collaborer sur un brevet en le rendant libre améliorerait l'innovation et, du coup, permettrait de développer le business de l'entreprise initiale.
