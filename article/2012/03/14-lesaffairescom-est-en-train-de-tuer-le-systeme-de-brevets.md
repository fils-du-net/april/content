---
site: LesAffaires.com
title: "Est-on en train de tuer le système de brevets?"
author: Jean-François Codère
date: 2012-03-14
href: http://www.lesaffaires.com/blogues/jean-francois-codere/est-on-en-train-de-tuer-le-systeme-de-brevets/542153
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Innovation
- International
---

> La plus récente poursuite visant des brevets dans le secteur technologique a été lancée par Yahoo! contre Facebook, mais la bataille la plus importante se déroule dans plusieurs pays entre Apple et Samsung.
