---
site: BUG BROTHER
title: "Mohammed Merah n’a PAS été identifié grâce à une loi antiterroriste, mais grâce à un logiciel libre"
author: jean marc manach
date: 2012-03-27
href: http://bugbrother.blog.lemonde.fr/2012/03/27/mohammed-merah-na-pas-ete-identifie-grace-a-une-loi/
tags:
- Internet
- Administration
- Institutions
---

> Mohammed Merah a notamment été identifié parce qu'il avait laissé des traces sur l'Internet. L'adresse IP (qui identifie les ordinateurs sur l'Internet) attribuée à sa maman faisait en effet partie des 576 adresses IP ayant consulté la petite annonce du militaire qui vendait sa moto sur LeBonCoin.fr, et qui fut la première victime du "tueur au scooter".
