---
site: OWNI
title: "Hollande entreprend la culture"
author: Andréa Fradin et Guillaume Ledit
date: 2012-03-02
href: http://owni.fr/2012/03/02/culture-hollandaise/
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Institutions
- Licenses
---

> Le flou de l'après Hadopi, c'est du passé. Dans une tribune qui paraît dans Le Monde, le candidat socialiste affirme sa proximité avec les grands entrepreneurs culturels. Et enterre toute possibilité de licence globale. Surtout, selon nos informations, cette tribune reprend des recommandations des différents lobbys de la culture.
