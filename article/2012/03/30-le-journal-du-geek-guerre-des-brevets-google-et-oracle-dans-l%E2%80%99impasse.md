---
site: Le Journal du Geek
title: "Guerre des brevets: Google et Oracle dans l’impasse"
author: Moe
date: 2012-03-30
href: http://www.journaldugeek.com/2012/03/30/guerre-des-brevets-google-et-oracle-en-froid/
tags:
- Entreprise
- Institutions
- Brevets logiciels
---

> Une résolution hors tribunal du contentieux qui oppose les sociétés Google et Oracle semble de moins en moins envisageable depuis que l’agence Reuters a confirmé le rejet d’accord à l’amiable du second par le premier.
