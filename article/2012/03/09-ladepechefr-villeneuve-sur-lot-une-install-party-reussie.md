---
site: LADEPECHE.fr
title: "Villeneuve-sur-Lot. Une Install-Party réussie"
date: 2012-03-09
href: http://www.ladepeche.fr/article/2012/03/09/1301764-villeneuve-sur-lot-une-install-party-reussie.html
tags:
- Sensibilisation
- Associations
- Promotion
---

> Le bar «Le Jeanne d'Arc» accueillait les organisateurs et les personnes attirées par une 3e Install-Party. Elles ont pu travailler sur de vieux ordinateurs ou sur des ordinateurs très modernes. Plusieurs installations avec des distributions diverses de Linux et des bureaux adaptés aux machines: Gnome, XFCE et LXDE. Depuis son lancement en 2004, l'association Agenux a élargi son champ d'intervention sur tout le département.
