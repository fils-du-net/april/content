---
site: Numerama
title: "Linux présent dans plus de 2 entreprises sur 10 en France"
author: Guillaume Champeau
date: 2012-03-16
href: http://www.numerama.com/magazine/22034-linux-present-dans-plus-de-2-entreprises-sur-10-en-france.html
tags:
- Entreprise
- Internet
---

> Selon une étude publiée par l'Insee, plus de 20 % des entreprises françaises d'au moins 10 personnes auraient un ou plusieurs postes informatiques équipés d'un système d'exploitation libre, et plus de 4 entreprises sur 10 utiliseraient un outil de bureautique libre.
