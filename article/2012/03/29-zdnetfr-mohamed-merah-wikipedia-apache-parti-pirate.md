---
site: ZDNet.fr
title: "Mohamed Merah, Wikipédia, Apache, Parti Pirate"
author: Thierry Noisette
date: 2012-03-29
href: http://www.zdnet.fr/blogs/l-esprit-libre/mohamed-merah-wikipedia-apache-parti-pirate-39770168.htm
tags:
- Internet
- Institutions
---

> Bouquet de brèves: le tueur et ses crimes font l'objet de deux articles, très débattus et très consultés, dans l'encyclopédie libre. Son arrestation tient en partie au logiciel libre Apache. Pour le Parti Pirate, le bourreau est aussi victime.
