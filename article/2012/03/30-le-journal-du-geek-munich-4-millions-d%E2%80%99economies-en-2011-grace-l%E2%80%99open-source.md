---
site: Le Journal du Geek
title: "Munich: 4 millions d’économies en 2011 grâce à l’open source"
author: Moe
date: 2012-03-30
href: http://www.journaldugeek.com/2012/03/30/open-office-linux-economies-munich/
tags:
- Entreprise
- Internet
- Administration
- Économie
---

> Dans un contexte économique difficile, les pays mais également les villes multiplient les démarches afin de réduire leurs coûts.
