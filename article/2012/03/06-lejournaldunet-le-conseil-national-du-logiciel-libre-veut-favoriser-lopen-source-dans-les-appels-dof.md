---
site: LeJournalduNet
title: "Le Conseil National du Logiciel Libre veut \"favoriser l'Open Source dans les appels d'offres publics\""
author: Virgile Juhan
date: 2012-03-06
href: http://www.journaldunet.com/solutions/emploi-rh/l-open-source-dans-la-campagne-presidentielle-2012/l-open-source-dans-les-appels-d-offres.shtml
tags:
- Entreprise
- Internet
- Administration
- Économie
- April
- Associations
- Brevets logiciels
- Marchés publics
- Standards
---

> "A l'heure où la France, comme d'autres Etats, doit mieux maîtriser ses dépenses, une utilisation accrue et volontariste du Logiciel Libre semble s'imposer", fait remarquer le Conseil National du Logiciel Libre (CNLL) dans un document adressé à chacun des partis politiques français.
