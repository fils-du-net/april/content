---
site: SUD OUEST
title: "Le logiciel libre s'expose"
date: 2012-03-24
href: http://www.sudouest.fr/2012/03/24/le-logiciel-libre-s-expose-668040-3834.php
tags:
- Internet
- Administration
- Sensibilisation
- Associations
---

> L'Espace intergénérationnel et multimédia a organisé une journée du logiciel libre vendredi 16 mars, dans le cadre de ses cours d'initiation et ateliers, avec l'aide de l'association agenaise, Agenux.
