---
site: Mediapart
title: "Le banquier et l'Abondance"
author: samines
date: 2012-03-24
href: http://blogs.mediapart.fr/blog/samines/240312/le-banquier-et-labondance
tags:
- Entreprise
- Internet
- Économie
- Matériel libre
- Droit d'auteur
- Innovation
---

> «Pourquoi nous travaillons gratuitement pour eux.» C’est en ces termes que l’American Sociology Association (A.S.A.) parle de l’activité des réseaux sociaux tels que FaceBook ou Twitter. Juliette Keating ne dit pas autre chose sur son billet, d’ailleurs, en évoquant ces multiples tâches non rémunérées que l’on réalise parfois (voire souvent) au bénéfice d’une société de consommation.
