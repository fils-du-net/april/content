---
site: "Solutions-Logiciels.com"
title: "De la complémentarité génétique du Cloud Computing et de l'open source"
author: Fabrice Bonan
date: 2012-03-30
href: http://www.solutions-logiciels.com/actualites.php?actu=11319&contenu=detail
tags:
- Entreprise
- Logiciels privateurs
- Interopérabilité
- Informatique en nuage
- Europe
---

> Quelques esprits chagrins tentent depuis quelques temps d’opposer les modèles open source et Cloud. C’est de bonne guerre de la part d’éditeurs propriétaires qui, ayant vu débarquer une nouvelle concurrence, ont tout d’abord tenté de faire passer ces acteurs pour des amateurs illuminés et peu au fait des problématiques des entreprises. Devant l’ampleur du succès de l’open source, il fallait bien trouver d’autres arguments...
