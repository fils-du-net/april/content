---
site: LE CERCLE Les Echos
title: "Open source: de la clandestinité à la gouvernance au sein du SI"
author: Patrick Benichou
date: 2012-03-15
href: http://lecercle.lesechos.fr/entreprises-marches/high-tech-medias/informatiques/221144585/open-source-clandestinite-a-gouvernance
tags:
- Entreprise
- Internet
- Économie
- Licenses
- Standards
---

> La gouvernance de l’Open Source au sein du Système d’Information. Le terme, peut paraître surprenant ou un peu prétentieux en première approche. Mais recouvre-t-il réellement de nouveaux sujets de fonds, ou s’agit-il plus prosaïquement d’un nouvel habit du bien nommé schéma directeur?
