---
site: Les Echos
title: "Comment fabriquer une bactérie domestique dans son garage"
author: Alain Perez
date: 2012-03-05
href: http://www.lesechos.fr/entreprises-secteurs/innovation-competences/sciences/0201915958318-comment-fabriquer-une-bacterie-domestique-dans-son-garage-298000.php
tags:
- Internet
- Innovation
- Sciences
---

> Aux Etats-Unis, des chercheurs militent pour une biologie inspirée du modèle «open source» de l'informatique.
