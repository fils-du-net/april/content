---
site: "ouest-france.fr"
title: "Logiciels libres et malvoyance: les clés de l'accessibilité - Hérouville-Saint-Clair"
date: 2012-03-08
href: http://www.ouest-france.fr/actu/actuLocale_-Logiciels-libres-et-malvoyance-les-cles-de-l-accessibilite-_14068-avd-20120308-62385675_actuLocale.Htm
tags:
- Internet
- Administration
- April
- Sensibilisation
- Accessibilité
- Promotion
---

> La bibliothèque a accueilli dimanche une animation originale pour un public encore peu représenté: «Comment améliorer l'accessibilité à l'informatique pour les personnes mal ou non-voyantes et, plus largement, aux personnes en situation de handicap?»
