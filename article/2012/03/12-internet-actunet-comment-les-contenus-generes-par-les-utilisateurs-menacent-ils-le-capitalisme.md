---
site: internet ACTU.net
title: "Comment les contenus générés par les utilisateurs menacent-ils le capitalisme?"
author: Xavier de la Porte
date: 2012-03-12
href: http://www.internetactu.net/2012/03/12/comment-les-contenus-generes-par-les-utilisateurs-menacent-ils-le-capitalisme/
tags:
- Entreprise
- Internet
- Économie
- Innovation
- International
---

> La lecture de la semaine prolonge le texte d’il y a 15 jours, qui tentait de comprendre pourquoi Facebook était valorisé à 100 milliards de dollars. On se souvient que la réponse était que la valeur: c’est nous, les utilisateurs! Le texte d’aujourd’hui est un entretien donné à Al-Jazeera par Michael Bauwens (Wikipédia, @mbauwens), le fondateur de la Peer-to-peer Fundation, qui cherche à analyser les effets à long terme de contenus générés par les utilisateurs sur le capitalisme.
