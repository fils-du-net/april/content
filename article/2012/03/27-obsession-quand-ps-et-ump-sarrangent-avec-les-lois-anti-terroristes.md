---
site: OBSESSION
title: "Quand PS et UMP s'arrangent avec les lois anti-terroristes"
author: Boris Manenti
date: 2012-03-27
href: http://obsession.nouvelobs.com/high-tech/20120327.OBS4705/affaire-merah-quand-ps-et-ump-s-arrangent-avec-les-lois-anti-terroristes.html
tags:
- Internet
- Administration
- Institutions
---

> Les deux partis affirment que "le tueur au scooter" a été confondu grâce à la législation, alors que son identification ne tient qu'à un simple pistage des adresses IP.
