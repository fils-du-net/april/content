---
site: LE CERCLE Les Echos
title: "Diffuser et améliorer sa technologie grâce à l’Open Source"
author: Olivier Picciotto
date: 2012-03-01
href: http://lecercle.lesechos.fr/entreprises-marches/high-tech-medias/informatiques/221144008/diffuser-et-ameliorer-sa-technologie-gr
tags:
- Entreprise
- Économie
- Innovation
- Licenses
- Informatique en nuage
---

> A l’heure de l’évolution des modèles de développement dans le monde de l’édition de logiciel, Cloud, SaaS…, il semble important de réaliser un focus sur un axe en forte croissance: l’essor des technologies «Open Source».
