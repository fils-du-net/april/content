---
site: Infotchad.com
title: "Logiciels libres, quelle opportunité pour le Tchad?"
author: D. Charles Badjam
date: 2012-03-20
href: http://www.infotchad.com/details.asp?item_id=3557
tags:
- Entreprise
- Internet
- Administration
- Sensibilisation
- Accessibilité
- Promotion
- International
---

> Promouvoir et vulgariser l’informatique à travers la culture de «l’Open Source», tel est l’objectif du forum national sur les logiciels libres organisé du 16 au 17 mars 2012 à la bibliothèque nationale par le Cabinet Informatique et Télécoms High technologies Center (HICE).
