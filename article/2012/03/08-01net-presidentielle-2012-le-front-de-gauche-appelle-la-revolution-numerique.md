---
site: 01net.
title: "Présidentielle 2012: le Front de Gauche appelle à la révolution numérique"
author: Pascal Samama
date: 2012-03-08
href: http://www.01net.com/editorial/560938/le-front-de-gauche-appelle-a-la-revolution-numerique/
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Associations
- Licenses
- Neutralité du Net
- ACTA
---

> En total désaccord avec la politique numérique de l’UMP, Jean-Luc Mélenchon milite pour que les TIC (technologies de l’information et de la communication) soient mises «au service de l’émancipation des êtres humains».
