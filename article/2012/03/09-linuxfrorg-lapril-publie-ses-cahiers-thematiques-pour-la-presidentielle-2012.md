---
site: linuxfr.org
title: "L'April publie ses cahiers thématiques pour la présidentielle 2012"
author: Benjamin Drieu
date: 2012-03-09
href: http://linuxfr.org/news/l-april-publie-ses-cahiers-thematiques-pour-la-presidentielle-2012
tags:
- Administration
- Économie
- Interopérabilité
- April
- Institutions
- Accessibilité
- DRM
- Droit d'auteur
- Éducation
- Innovation
- Licenses
---

> Lors des dernières élections présidentielles, en 2007, l'April a lancé l’initiative Candidats.fr afin de sensibiliser les futurs élus au logiciel libre et de connaître leurs positions sur les différentes questions qui en découlent.
