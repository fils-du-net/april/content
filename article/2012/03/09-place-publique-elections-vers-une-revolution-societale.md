---
site: Place Publique
title: "Elections: Vers une révolution sociétale"
author: Yan de Kerorguen
date: 2012-03-09
href: http://www.place-publique.fr/spip.php?article6511
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Associations
- Sciences
---

> Troisième révolution industrielle, économie de la contribution, démocratie d’initiative… les élections sont l’occasion pour les citoyens de rassembler les idées et de proposer des horizons. Faut-il céder au pessimisme ambiant et se désenchanter ou tenter de définir un nouveau monde en provoquant «l’insurrection des consciences»?
