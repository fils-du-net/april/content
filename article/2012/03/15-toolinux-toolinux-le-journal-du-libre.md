---
site: toolinux
title: "Toolinux, le journal du Libre"
date: 2012-03-15
href: http://www.toolinux.com/April-Camp-du-23-au-25-mars-2012-a
tags:
- April
---

> L’objectif d’un «April Camp», c’est de réunir adhérents de l’April, soutiens et bonnes volontés pendant deux jours, afin de faire avancer des projets en cours et/ou étudier de nouvelles propositions (améliorations technique, outils de communication...).
