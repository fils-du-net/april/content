---
site: ITCHANNEL.info
title: "Bureautique: Quatre entreprises sur dix ont recours aux logiciels libres"
date: 2012-03-16
href: http://www.itchannel.info/articles/130265/bureautique-quatre-entreprises-dix-ont-recours-logiciels-libres.html
tags:
- Entreprise
- Sensibilisation
---

> Parmi les sociétés d'au moins 10 personnes, une sur cinq utilise un système d'exploitation libre. L'utilisation de logiciels libres destinés à la bureautique, comme Open Office, est encore plus fréquente (43 % des sociétés). Les systèmes d'exploitation libres sont plus répandus parmi les sociétés de grande taille, à l'inverse des logiciels de bureautique libres, plus présents dans les petites structures.
