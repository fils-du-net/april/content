---
site: ZDNet.fr
title: "Logiciel libre: les politiques français peu ou pas mobilisés"
author: Christophe Auffray
date: 2012-03-05
href: http://www.zdnet.fr/actualites/logiciel-libre-les-politiques-francais-peu-ou-pas-mobilises-39769276.htm
tags:
- Entreprise
- Internet
- Économie
- April
- HADOPI
- Institutions
- Associations
- Brevets logiciels
- Éducation
- ACTA
---

> Comme l’April, le Conseil national du logiciel libre veut impliquer les partis politiques dans les questions liées au logiciel libre, à son économie, mais aussi dans les débats sur les brevets logiciels, la neutralité et les standards ouverts. Mais si les acteurs du libre sont mobilisés, les candidats à la présidentielles, eux, restent dans l’ensemble peu concernés.
