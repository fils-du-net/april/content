---
site: Silicon.fr
title: "Antoine Larrache, NPA: «Nous sommes pour une licence, non pas globale, mais égale»"
author: Ariane Beky
date: 2012-03-23
href: http://www.silicon.fr/antoine-larrache-npa-nous-sommes-pour-une-licence-non-pas-globale-mais-egale-73100.html
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Droit d'auteur
- Innovation
- Licenses
- Contenus libres
- ACTA
---

> Logiciels libres, propriété intellectuelle, accord commercial international anti-contrefaçon… Antoine Larrache, responsable Internet de la campagne de Philippe Poutou, candidat du Nouveau Parti Anticapitaliste (NPA) à l’élection présidentielle 2012, fait le point.
