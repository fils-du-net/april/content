---
site: 20minutes.fr
title: "Ordis pour les lycéens et apprentis"
author: F.B.
date: 2012-03-06
href: http://www.20minutes.fr/article/892329/ordis-lyceens-apprentis
tags:
- Internet
- Économie
- Institutions
- Éducation
---

> Les lycéens de Jean-Perrin à Rezé ont été premiers à en bénéficier hier soir. La région Pays de la Loire va équiper en ordinateurs plus de 4500 jeunes ces prochains jours. Baptisée Ordipass, l'opération vise à réduire la fracture numérique entre les élèves. Il est proposé aux lycéens et apprentis issus de familles aux revenus modestes d'acquérir un ordinateur portable pour un tarif défiant toute concurrence. Des critères de revenus (parts de bourse et quotient familial) déterminent l'éligibilité à l'opération et le prix à payer (de 20 € minimum à 200 € maximum).
