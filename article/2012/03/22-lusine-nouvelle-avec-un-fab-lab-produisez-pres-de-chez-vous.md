---
site: L'USINE NOUVELLE
title: "AVEC UN FAB LAB, PRODUISEZ PRÈS DE CHEZ VOUS!"
author: PATRICE DESMEDT
date: 2012-03-22
href: http://www.usinenouvelle.com/article/avec-un-fab-lab-produisez-pres-de-chez-vous.N171183
tags:
- Entreprise
- Internet
- Matériel libre
- Innovation
- Licenses
---

> Apparus en France en 2010, les «laboratoires de fabrication» (fab labs) misent sur l'échange de savoirs, l'utilisation du prototypage rapide et les licences open source.
