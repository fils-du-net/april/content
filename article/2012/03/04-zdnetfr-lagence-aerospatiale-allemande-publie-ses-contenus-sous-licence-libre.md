---
site: ZDNet.fr
title: "L'agence aérospatiale allemande publie ses contenus sous licence libre"
author: Thierry Noisette
date: 2012-03-04
href: http://www.zdnet.fr/blogs/l-esprit-libre/l-agence-aerospatiale-allemande-publie-ses-contenus-sous-licence-libre-39769237.htm
tags:
- Administration
- Licenses
- Sciences
- Contenus libres
---

> Sur l'invitation de Wikimedia Deutschland, le DLR diffuse maintenant de façon systématique ses images, vidéos etc. sous licence Creative Commons Attribution, sur les traces de la Nasa.
