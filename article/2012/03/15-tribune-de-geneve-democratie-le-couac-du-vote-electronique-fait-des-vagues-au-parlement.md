---
site: Tribune de Genève
title: "Démocratie: Le couac du vote électronique fait des vagues au Parlement"
author: Simon Koch
date: 2012-03-15
href: http://www.tdg.ch/high-tech/web/couac-vote-electronique-vagues-parlement/story/20125762
tags:
- Internet
- Logiciels privateurs
- Institutions
- Associations
- RGI
- Vote électronique
- International
---

> Après le bug enregistré ce week-end dans le vote par internet, plusieurs parlementaires demandent des comptes à la Confédération.
