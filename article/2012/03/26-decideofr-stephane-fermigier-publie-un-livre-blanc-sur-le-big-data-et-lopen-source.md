---
site: Decideo.fr
title: "Stéphane Fermigier publie un livre blanc sur le big data et l'open source"
author: Philippe Nieuwbourg
date: 2012-03-26
href: http://www.decideo.fr/Stephane-Fermigier-publie-un-livre-blanc-sur-le-big-data-et-l-open-source_a5019.html
tags:
- Entreprise
- Administration
- Innovation
- RGI
- Europe
---

> Sous-titré "une convergence inévitable", ce document proposé gratuitement par l'auteur en téléchargement fait le point sur les atouts et les avancées des technologies ouvertes pour la gestion des gros volumes de données non structurées. Derrière les raccourcis technologiques comme NoSQL, NewSQL, MapReduce, Hadoop, il y a bien souvent des projets en technologies ouvertes.
