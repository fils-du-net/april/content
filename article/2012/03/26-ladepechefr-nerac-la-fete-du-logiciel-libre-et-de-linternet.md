---
site: LADEPECHE.fr
title: "Nérac. La fête du logiciel libre et de l'internet"
author: La Dépêche du Midi
date: 2012-03-26
href: http://www.ladepeche.fr/article/2012/03/26/1314971-nerac-la-fete-du-logiciel-libre-et-de-l-internet.html
tags:
- Internet
- Administration
- Sensibilisation
---

> A la fin du mois de mars, la médiathèque de Nérac propose la fête de l'Internet et du logiciel libre. Les nouveaux outils d'information et de communication et les réseaux sociaux qui font fureur sur la toile seront à l'honneur.
