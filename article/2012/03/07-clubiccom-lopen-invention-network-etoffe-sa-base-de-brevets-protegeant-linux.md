---
site: clubic.com
title: "L'Open Invention Network étoffe sa base de brevets protégeant Linux"
author: Guillaume Belfiore
date: 2012-03-07
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/actualite-480356-open-invention-network-etoffe-base-brevets.html
tags:
- Entreprise
- Associations
- Brevets logiciels
---

> L'OIN, l'Open Invention Network, a récemment densifié sa base de propriétés intellectuelles destinées à protéger l'écosystème du logiciel libre.
