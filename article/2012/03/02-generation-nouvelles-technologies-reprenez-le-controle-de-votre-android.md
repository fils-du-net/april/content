---
site: Génération nouvelles technologies
title: "Reprenez le contrôle de votre Android"
author: Nathalie M.
date: 2012-03-02
href: http://www.generation-nt.com/smartphone-android-google-open-source-application-actualite-1550081.html
tags:
- Entreprise
- Associations
- Innovation
- Philosophie GNU
---

> Le succès d'Android porte sur son côté open source. Pourtant, l'OS de Google n'est pas des plus transparents et n'apporte pas toute la liberté que l'on croit. La FSFE (Free Software Foundation Europe) invite les utilisateurs à devenir maître de leur téléphone.
