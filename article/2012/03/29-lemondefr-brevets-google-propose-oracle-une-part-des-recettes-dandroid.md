---
site: LeMonde.fr
title: "Brevets: Google propose à Oracle une part des recettes d'Android"
date: 2012-03-29
href: http://www.lemonde.fr/technologies/article/2012/03/29/brevets-google-propose-a-oracle-une-part-des-recettes-d-android_1677127_651865.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> Oracle a assigné Google en justice en 2010, estimant que la technologie d'Android violait les brevets de sa propre technologie Java.
