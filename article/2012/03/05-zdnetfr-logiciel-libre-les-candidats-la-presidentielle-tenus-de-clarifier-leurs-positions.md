---
site: ZDNet.fr
title: "Logiciel libre: les candidats à la présidentielle tenus de clarifier leurs positions"
date: 2012-03-05
href: http://www.zdnet.fr/actualites/logiciel-libre-les-candidats-a-la-presidentielle-tenus-de-clarifier-leurs-positions-39769243.htm
tags:
- April
- Institutions
- Vente liée
---

> Si les candidats à la présidentielle évoquent volontiers l’économie numérique, ils restent néanmoins vagues sur de nombreux points. L’April, l’association de promotion du logiciel libre, les interpelle donc sur des points précis, dont la vente liée.
