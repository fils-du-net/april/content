---
site: LADEPECHE.fr
title: "Onet-le-Château. Logiciels en liberté avec l'ARU2L"
author: La Dépêche du Midi
date: 2012-03-17
href: http://www.ladepeche.fr/article/2012/03/17/1308580-onet-le-chateau-logiciels-en-liberte-avec-l-aru2l.html
tags:
- April
- Sensibilisation
- Associations
- Licenses
- Promotion
- Contenus libres
---

> Depuis 2001, l'APRIL invite les groupes d'utilisateurs de logiciels libres et associations de promotion de ces logiciels à organiser pour l'arrivée du printemps des événements de découverte à destination du grand public.
