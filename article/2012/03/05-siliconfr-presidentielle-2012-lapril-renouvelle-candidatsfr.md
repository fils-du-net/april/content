---
site: Silicon.fr
title: "Présidentielle 2012: l'APRIL renouvelle Candidats.fr"
author: Ariane Beky
date: 2012-03-05
href: http://www.silicon.fr/presidentielle-2012-lapril-renouvelle-candidats-fr-72415.html
tags:
- Internet
- Économie
- Interopérabilité
- April
- HADOPI
- Institutions
- Vente liée
- Accessibilité
- Associations
- DRM
- Éducation
- Neutralité du Net
- Promotion
- Informatique en nuage
- Contenus libres
- International
- ACTA
---

> Forte de l’expérience acquise lors des campagnes électorales de 2007, l’Association de promotion et de défense du logiciel libre (APRIL) a transmis aux candidats à la présidentielle 2012 son questionnaire portant sur les enjeux du ‘libre’ et d’une société numérique ouverte.
