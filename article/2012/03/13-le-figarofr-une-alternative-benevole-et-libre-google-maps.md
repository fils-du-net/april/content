---
site: LE FIGARO.fr
title: "Une alternative bénévole et libre à Google Maps "
author: Geoffroy Husson
date: 2012-03-13
href: http://www.lefigaro.fr/hightech/2012/03/13/01007-20120313ARTFIG00745-une-alternative-benevole-et-libre-a-google-maps.php
tags:
- Entreprise
- Internet
- Associations
- Licenses
---

> Le service de cartographie OpenStreetMap, créé en 2004, est en plein essor. Il vient d'être intégré à Foursquare et à l'application iPhoto de l'iPad.
