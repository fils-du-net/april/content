---
site: L'INFORMATICIEN
title: "Liberté, Egalité, Fraternité: la devise du logiciel libre selon Richard Stallman"
author: Orianne Vatin
date: 2012-03-09
href: http://www.linformaticien.com/actualites/id/23963/liberte-egalite-fraternite-la-devise-du-logiciel-libre-selon-richard-stallman.aspx
tags:
- Entreprise
- Internet
- HADOPI
- Associations
- Philosophie GNU
- Informatique en nuage
- Video
- Europe
- International
- Open Data
- ACTA
---

> Le 22 février, Richard Stallman, l'un des pères du système d'exploitation GNU/Linux, donnait une conférence à l'Université Saint-Charles de Marseille. Prolixe, il s'y est exprimé pendant plus de quatre heures et, comme à son habitude, il a su user de formules choc.
