---
site: Investir En Tunisie
title: "Tunisie-Open source: les avantages de l’ouverture au public"
author: Moncef BEDDA
date: 2012-03-30
href: http://www.investir-en-tunisie.net/index.php?option=com_content&id=14096
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Éducation
- Innovation
- RGI
- International
---

> Un état des lieux de la politique de la Tunisie en matière de logiciels libres montre que des pays comme la Jordanie et l’Egypte ont une avance sérieuse dans le domaine.
