---
site: basta!
title: "Risque de conflit d'intérêts massif à l'Office européen des brevets"
author: Sophie Chapelle
date: 2012-11-22
href: http://www.bastamag.net/article2782.html
tags:
- Administration
- Brevets logiciels
- Innovation
- Europe
---

> 250 000 demandes de brevets ont été déposées en 2011 à l’Office européen des brevets. Un record. La direction en veut toujours plus, et propose de verser une prime à ses 7 000 salariés. Ce bonus est loin de faire consensus en interne. Pour une partie du personnel, l’éthique et la qualité du travail passent avant les primes au rendement. D’autant que le personnel pourrait être placé en situation de conflits d’intérêts.
