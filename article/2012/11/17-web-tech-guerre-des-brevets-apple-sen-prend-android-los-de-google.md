---
site: Web & Tech
title: "Guerre des Brevets: Apple s'en prend à Android, l'OS de Google"
author: Jordane Feuillet
date: 2012-11-17
href: http://web-tech.fr/guerre-des-brevets-apple-en-prend-a-android-os-de-google
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> Apple a réussi à convaincre le juge: Android, le système d'exploitation de Google, a été intégré au dossier de plaintes d'Apple contre Sansung.
