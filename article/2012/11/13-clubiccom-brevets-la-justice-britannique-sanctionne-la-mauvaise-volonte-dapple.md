---
site: clubic.com
title: "Brevets: la justice britannique sanctionne la mauvaise volonté d'Apple"
author: Audrey Oeillet
date: 2012-11-13
href: http://www.clubic.com/univers-mac/apple/actualite-522045-brevets-justice-britannique-sanctionne-mauvaise-volonte-apple.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> Condamné en Grande-Bretagne à publier la décision de justice de son procès contre Samsung sur son site Web, Apple a fait preuve d'une mauvaise volonté qui le voit aujourd'hui sanctionné davantage par les autorités britanniques: l'entreprise devra payer les frais d'avocats de son concurrent.
