---
site: LeMagIT
title: "Systèmes embarqués: un secteur dynamique qui brave la crise"
author: Cyrille Chausson
date: 2012-11-29
href: http://www.lemagit.fr/economie/business/2012/11/29/systemes-embarques-un-secteur-dynamique-qui-brave-la-crise
tags:
- Entreprise
- Économie
- Europe
- International
---

> Enfin, et c’est un point clé, en matière d’outillage pour les systèmes embarqués, l’Open Source domine. Ces applications à code ouvert sont utilisées, encore essentiellement, dans les processus de développement et de test par 65% des éditeurs et par 42 % des sociétés de services. Elles se retrouvent assez peu dans les systèmes embarqués eux-mêmes, note PAC, à l’exception des systèmes pour le grand public, comme la téléphonie mobile par exemple – l’effet Android sans doute.
