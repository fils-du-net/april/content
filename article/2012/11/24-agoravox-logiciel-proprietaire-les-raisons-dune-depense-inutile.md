---
site: AgoraVox
title: "Logiciel propriétaire: les raisons d'une dépense inutile?"
author: Denis Szalkowski
date: 2012-11-24
href: http://www.agoravox.fr/tribune-libre/article/logiciel-proprietaire-les-raisons-126414
tags:
- Logiciels privateurs
- Sensibilisation
---

> Comme beaucoup d'acteurs de l'informatique, je pensais, avec l'arrivée de OpenOffice, de LibreOffice, puis de Google Apps, que la suite bureautique Microsoft Office n'avait plus de réel avenir. Il est tout de même étonnant de constater les raisons de l'entêtement des DSI des grands groupes, des collectivités et de l’État français à vider leurs poches pour payer un droit à utiliser une suite logicielle bureautique!!!
