---
site: Silicon.fr
title: "Jim Zemlin, Linux Foundation: «le logiciel propriétaire est condamné»"
author: Nerea Bilbao
date: 2012-11-15
href: http://www.silicon.fr/jim-zemlin-linux-foundation-logiciel-proprietaire-condamne-80995.html
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Associations
- Innovation
- Informatique en nuage
---

> Début Novembre s'est tenue à Barcelone la LinuxCon, une conférence sur le logiciel libre où nos confrères de Silicon News ont pu rencontrer Jim Zemlin, le directeur exécutif de la fondation linux.
