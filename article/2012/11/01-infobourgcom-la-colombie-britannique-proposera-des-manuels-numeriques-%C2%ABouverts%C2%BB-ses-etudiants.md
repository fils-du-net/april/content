---
site: Infobourg.com
title: "La Colombie-Britannique proposera des manuels numériques «ouverts» à ses étudiants"
author: Nathalie Côté
date: 2012-11-01
href: http://www.infobourg.com/2012/11/01/la-colombie-britannique-offrira-manuels-numeriques-ouverts
tags:
- Institutions
- Éducation
- Contenus libres
- International
---

> La Colombie-Britannique offrira prochainement des manuels scolaires postsecondaires en licence libre pour les quarante cours les plus populaires, a récemment annoncé son gouvernement.
