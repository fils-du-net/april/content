---
site: ZDNet.fr
title: "Linux et OpenOffice ont fait économiser 11 millions d'euros à Munich"
author: Thierry Noisette
date: 2012-11-26
href: http://www.zdnet.fr/actualites/linux-et-openoffice-ont-fait-economiser-11-millions-d-euros-a-munich-39784842.htm
tags:
- Administration
- Économie
- International
---

> La cité bavaroise a mené une étude comparative entre son choix d'une distribution Linux maison, LiMux, avec OpenOffice – qui sera remplacé l'an prochain par LibreOffice – et des solutions Windows.
