---
site: PC INpact
title: "L’April défend les vertus du logiciel libre dans l'Éducation nationale"
author: Marc Rees
date: 2012-11-09
href: http://www.pcinpact.com/news/75184-l-april-defend-vertus-logiciel-libre-dans-education-nationale.htm
tags:
- Institutions
- Droit d'auteur
- Éducation
---

> Dans un échange de courrier avec le ministère de l’Éducation nationale, l’Association pour la promotion du logiciel libre (April) réclame une série de règles pour le numérique à l’école, collège et lycée. Toutes reposent sur les principes des licences libres.
