---
site: Techworld
title: "Price of simpler European patents is an increase of software patent dangers, open source group warns"
author: Loek Essers
date: 2012-11-20
href: http://www.techworld.com.au/article/442566/price_simpler_european_patents_an_increase_software_patent_dangers_open_source_group_warns
tags:
- April
- Institutions
- Brevets logiciels
- Europe
---

> (L'Union Européenne s'apprête à rendre la procédure d'obtention d'un brevet plus simple et moins onéreuse, mais cela pourrait aussi rendre dangereusement simple le blocage de compétiteurs à travers toute la zone) The European Union is readying a way to make the process of obtaining a patent simpler and less expensive, but it could also make it dangerously easy for litigants to block sales of their competitors' products across the region, according to a French advocacy group.
