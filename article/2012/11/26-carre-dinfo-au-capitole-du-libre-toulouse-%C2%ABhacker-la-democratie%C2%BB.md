---
site: Carré d'info
title: "Au Capitole du libre à Toulouse: «Hacker la démocratie»"
author: Xavier Lalu
date: 2012-11-26
href: http://carredinfo.fr/au-capitole-du-libre-hacker-la-democratie-16754
tags:
- Institutions
- Associations
---

> Ce week-end, l’Enseeiht accueillait l’événement Capitole du libre, rendez-vous annuel consacré au logiciel libre. Grand public ou spécialisées, de nombreuses conférences étaient organisées dont une intitulée «Hacker la démocratie avec la mémoire politique de la quadruture du net». L’occasion d’expliquer comment des geeks peuvent aider n’importe quel citoyen à se réapproprier la démocratie via le suivi des élus.
