---
site: Mediapart
title: "La révolution dans l'éducation par le très haut débit. Le MOOC"
author: Jean-Paul Baquiast
date: 2012-11-10
href: http://blogs.mediapart.fr/blog/jean-paul-baquiast/101112/la-revolution-dans-leducation-par-le-tres-haut-debit-le-mooc
tags:
- Internet
- Éducation
- Innovation
---

> Il faudra aussi éviter que les nouvelles offres soient monopolisées par des entreprises commerciales travaillant sur le mode dit propriétaire. Un vaste domaine s'ouvre dans ce but à toux ceux qui ont acquis avec la lutte pour les logiciels libres le savoir-faire permettant de combattre l'accaparement du savoir par des firmes se comportant dans ce domaine comme les semenciers privés dans le domaine de l'agriculture. La guerre est loin d'être gagnée.
