---
site: LeMagIT
title: "L’Open Source, plus accessible dans les administrations française et britannique"
author: Cyrille Chausson
date: 2012-11-19
href: http://www.lemagit.fr/technologie/open-source/2012/11/19/lopen-source-plus-accessible-par-les-administrations-francaise-et-britannique
tags:
- Entreprise
- Administration
- Marchés publics
- Informatique en nuage
- International
---

> Linagora référencé dans la centrale d’achats publics Ugap, un éditeur Open Source qui entre dans le CloudStore britannique… Les administrations françaises et britanniques pourront désormais accéder plus facilement à des solutions libres pour motoriser leur système d’information.
