---
site: Agefi.com
title: "Les progrès insoupçonnés de la recherche participative"
author: Grégoire barbey
date: 2012-11-30
href: http://www.agefi.com/une/detail/archive/2012/november/artikel/open-hardware-geneve-joue-un-role-dans-le-developpement-des-technologies-hors-it-sous-licence-libre.html
tags:
- Entreprise
- Matériel libre
- Innovation
---

> Open Hardware. Genève joue un rôle dans le développement des technologies hors IT sous licence libre.
