---
site: L'USINE NOUVELLE
title: "\"Nous voulons amplifier la politique d’ouverture des données publiques\""
author: Ridha Loukil
date: 2012-11-06
href: http://www.usinenouvelle.com/article/nous-voulons-amplifier-la-politique-d-ouverture-des-donnees-publiques.N185512
tags:
- Entreprise
- Administration
- Institutions
- Standards
- Open Data
---

> A petits pas, le gouvernement avance dans sa stratégie numérique. A l’occasion d’une visite chez la SS2L Linagora, Fleur Pellerin a expliqué à l’Usine Nouvelle sa volonté de revoir la politique d’open data et détaillé son programme de transition numérique pour les PME.
