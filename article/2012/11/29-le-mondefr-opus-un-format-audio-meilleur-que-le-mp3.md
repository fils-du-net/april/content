---
site: Le Monde.fr
title: "Opus, un format audio meilleur que le MP3"
author: David Larousserie
date: 2012-11-29
href: http://www.lemonde.fr/sciences/article/2012/11/29/opus-un-format-audio-meilleur-que-le-mp3_1798065_1650684.html
tags:
- Innovation
- Licenses
---

> Discrètement, une technologie de compression du son se répand, balayant ses concurrentes, dont la plus connue est le format MP3, vedette des échanges de fichiers musicaux sur le Web. Son nom est Opus. En septembre, ce "codec" (un programme d'encodage et de décodage de fichiers) a été reconnu comme futur standard d'Internet par l'Internet Engineering Task Force (IETF), le groupe de travail technique de la Toile. En octobre, le navigateur Firefox (version 16) reconnaissait les fichiers ".opus" et pouvait donc les lire sans recours à un logiciel supplémentaire.
