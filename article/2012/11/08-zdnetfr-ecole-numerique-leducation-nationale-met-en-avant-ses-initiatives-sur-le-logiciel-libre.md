---
site: ZDNet.fr
title: "Ecole numérique: l'Education nationale met en avant ses \"initiatives\" sur le logiciel libre"
author: Antoine Duvauchelle
date: 2012-11-08
href: http://www.zdnet.fr/actualites/ecole-numerique-l-education-nationale-met-en-avant-ses-initiatives-sur-le-logiciel-libre-39784312.htm
tags:
- Administration
- April
- Institutions
- Droit d'auteur
- Éducation
---

> Le ministère de l'Education nationale s'est fendu d'une réponse à un courrier de l'April. Alors que l'association demandait une prise en compte des solutions libres dans l'évolution numérique de l'école, le ministère se dit sensible à la question.
