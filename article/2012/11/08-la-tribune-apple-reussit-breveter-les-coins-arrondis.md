---
site: LA TRIBUNE
title: "Apple réussit à breveter les coins arrondis"
author: latribune.fr
date: 2012-11-08
href: http://www.latribune.fr/technos-medias/electronique/20121108trib000729851/apple-reussit-a-breveter-les-coins-arrondis.html
tags:
- Entreprise
- Brevets logiciels
- Innovation
---

> La marque à la pomme a obtenu mardi 6 novembre un brevet de conception portant sur le design d'un appareil portable plat, rectangulaire et ayant des coins bombés plutôt qu'anguleux.
