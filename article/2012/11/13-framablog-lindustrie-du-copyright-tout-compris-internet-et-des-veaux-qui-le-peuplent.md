---
site: Framablog
title: "L'industrie du copyright a tout compris à Internet (et des veaux qui le peuplent)"
author: Rick Falkvinge
date: 2012-11-13
href: http://www.framablog.org/index.php/post/2012/11/13/industrie-copyright-big-brother
tags:
- Entreprise
- Internet
- DADVSI
- Droit d'auteur
---

> Bien trop souvent, j’entends que l’industrie du copyright ne comprend pas Internet, ne comprend pas la génération du net, ne comprend pas à quel point la technologie a changé. Non seulement c’est faux, mais c’est dangereusement faux. Pour vaincre un adversaire, vous devez d’abord comprendre comment il pense plutôt que de le présenter comme le mal. L’industrie du copyright comprend exactement ce qu’est Internet, et qu’il doit être détruit pour que cette industrie garde un soupçon de pertinence.
