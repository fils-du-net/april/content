---
site: PC INpact
title: "Brevet unitaire: réunion accélérée, inquiétudes accentuées"
author: Marc Rees
date: 2012-11-16
href: http://www.pcinpact.com/news/75377-brevet-unitaire-reunion-acceleree-inquietudes-accentuees.htm
tags:
- April
- Institutions
- Brevets logiciels
- Promotion
- Europe
---

> La Commission des affaires juridiques du Parlement européen se prépare à une réunion extraordinaire lundi 19 novembre, consacrée exclusivement au brevet unitaire. Une réunion programmée jeudi 15 novembre après des mois de reports sur ce dossier fondamental.
