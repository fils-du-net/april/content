---
site: LeMagIT
title: "Rétropédalage: la ville de Fribourg abandonne OpenOffice pour Microsoft Office"
author: Cyrille Chausson
date: 2012-11-21
href: http://www.lemagit.fr/technologie/poste-travail/bureautique/2012/11/21/retropedalage-la-ville-de-fribourg-abandonne-openoffice-pour-microsoft-office
tags:
- Logiciels privateurs
- Administration
- Interopérabilité
- International
---

> A l’issue d’un vote qui s’est déroulé hier soir, le conseil de la ville allemande de Fribourg a validé le retour à une suite Office, presque 6 ans après avoir débuté un plan de migration vers OpenOffice et ODF. En cause notamment, l’incompatibilité des formats bureautiques et l’omniprésence de Microsoft Office dans les échanges entre administrations et prestataires.
