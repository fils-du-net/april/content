---
site: LE CERCLE Les Echos
title: "L’entreprise intelligente: un casse-tête?"
author: Stéphane Gaillard
date: 2012-11-07
href: http://lecercle.lesechos.fr/entreprises-marches/high-tech-medias/informatiques/221158236/entreprise-intelligente-casse-tete
tags:
- Entreprise
- Innovation
- Licenses
---

> Aujourd’hui, ce ne sont pas les données qui font défaut aux entreprises, mais plutôt l’information. Pour devenir intégrées et intelligentes, les entreprises de taille moyenne feraient bien d'envisager l'approche open source.
