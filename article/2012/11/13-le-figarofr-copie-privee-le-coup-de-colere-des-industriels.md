---
site: Le Figaro.fr
title: "Copie privée: le coup de colère des industriels"
date: 2012-11-13
href: http://www.lefigaro.fr/hightech/2012/11/13/01007-20121113ARTFIG00507-copie-privee-le-coup-de-colere-des-industriels.php
tags:
- Entreprise
- Économie
- Droit d'auteur
---

> Des fabricants de matériels électroniques dénoncent le surcoût que fait peser sur leurs produits cette redevance, destinée aux ayants droit.
