---
site: PC INpact
title: "Quel est le coût des licences propriétaires pour les finances publiques?"
author: Marc Rees
date: 2012-11-29
href: http://www.pcinpact.com/news/75672-quel-est-cout-licences-proprietaires-pour-finances-publiques.htm
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Marchés publics
---

> Quel est le coût d'achat et d'utilisation des logiciels propriétaires dans les administrations publiques? C'est en substance la question posée à Bercy par deux députés.
