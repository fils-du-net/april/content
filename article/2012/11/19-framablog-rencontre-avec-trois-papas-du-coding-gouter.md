---
site: Framablog
title: "Rencontre avec trois papas du Coding Goûter"
author: aKa
date: 2012-11-19
href: http://www.framablog.org/index.php/post/2012/11/19/coding-gouter
tags:
- April
- Sensibilisation
- Éducation
---

> Le logiciel libre fonctionne évidemment en harmonie avec les pratiques d’appropriations collectives. Il y a des raisons idéologiques à ça, mais il y a aussi des raisons pratiques. Un exemple très concret: les enfants français ont besoin que les interfaces, la documentation, les exemples, soient traduits en français. Un outil libre est traduisible dès que la communauté le veut.
