---
site: EPN de Wallonie
title: "Les rencontres Mondiales du Libre à Bruxelles en 2013"
author: Jean-Luc Manise
date: 2012-11-28
href: http://www.epn-ressources.be/les-rencontres-mondiales-du-libre-a-bruxelles-en-2013-2
tags:
- April
- Sensibilisation
- Associations
- Promotion
---

> Les Rencontres Mondiales du Logiciel Libre sont nées en 2000 à l’initiative de l’association bordelaise des logiciels libres (ABUL). Elles ont eu lieu à plusieurs reprises à Bordeaux, pour ensuite essaimer dans d’autres villes en France et dorénavant en Europe avec Genève en 2012 et Bruxelles l’année prochaine.
