---
site: toolinux
title: "UEFI et Microsoft: La Fondation Linux pieds et poings liés?"
author: Philippe Scoffoni
date: 2012-11-28
href: http://www.toolinux.com/UEFI-et-Microsoft-La-Fondation
tags:
- Entreprise
- Interopérabilité
- Associations
- DRM
---

> La conception de Windows 8 implique l’activation des mécanismes de Secure Boot des BIOS de nos ordinateurs. Conséquence, on ne peut plus faire cohabiter un système d’exploitation GNU/Linux avec Windows 8. La fondation Linux a choisi de “pactiser” avec Microsoft pour régler le problème. Maintenant, elle attend…
