---
site: L'usine Nouvelle
title: "Droits et devoirs de l’open data "
author: Marc Schuler et Guillaume Raimbault
date: 2012-11-15
href: http://www.usinenouvelle.com/article/droits-et-devoirs-de-l-open-data.N186232
tags:
- Administration
- Licenses
- Open Data
---

> Dans un environnement économique pour le moins morose, le secteur des innovations technologiques afférent à l’exploitation de données publiques bénéficie de perspectives commerciales très prometteuses. Les statistiques de l’INSEE, les données du trafic routier, les informations sur les jardins publics, les listes des gares SNCF et les statistiques de ponctualité des TGV sont en effet autant d’informations brutes qui ne demandent qu’à être génératrices de valeurs ajoutées une fois entre les mains d’opérateurs privés ingénieux.
