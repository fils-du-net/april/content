---
site: enviscope.com
title: "Informatique: Le libre est source d'économie."
author: Michel Deprost
date: 2012-11-14
href: http://www.enviscope.com/News/breves/Informatique-Le-libre-est-source-d-economie,i17575.html
tags:
- Entreprise
- Administration
- Économie
- Associations
---

> Plus de 2 millions d’euros  pourraient être économisés dans l'agglomération lyonnaise par l'utilisation de logiciels libres dans les collectivités. C'est ce qu'affirme l'association La Mouette  qui a écrit aux 58 maires du Grand Lyon. L’association suggère d’adopter une suite bureautique libre en lieu et place de la suite Microsoft Office.
