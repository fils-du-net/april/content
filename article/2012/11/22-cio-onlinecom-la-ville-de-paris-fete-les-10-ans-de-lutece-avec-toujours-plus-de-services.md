---
site: "cio-online.com"
title: "La Ville de Paris fête les 10 ans de Lutèce avec toujours plus de services"
author: Bertrand Lemaire
date: 2012-11-22
href: http://www.cio-online.com/actualites/lire-la-ville-de-paris-fete-les-10-ans-de-lutece-avec-toujours-plus-de-services-4701.html
tags:
- Internet
- Administration
---

> A partir de 2002, la Ville de Paris a déployé de nombreux services sur Lutèce. Cet outil développé pour un usage interne et reversé à la Communauté en open-source connaît un succès croissant, y compris hors de la ville.
