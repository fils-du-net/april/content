---
site: ZDNet.fr
title: "Savoir libre: l'Open Knowledge Foundation arrive en France"
author: Thierry Noisette
date: 2012-11-26
href: http://www.zdnet.fr/actualites/savoir-libre-l-open-knowledge-foundation-arrive-en-france-39784983.htm
tags:
- Institutions
- Associations
- Open Data
---

> L'association vouée à la culture et au savoir libres organise une première rencontre le 12 décembre (12/12/12) à Paris.
