---
site: Le nouvel Economiste
title: "Le concept d’open innovation"
author: Lorélie Carrive
date: 2012-11-15
href: http://www.lenouveleconomiste.fr/lesdossiers/le-concept-dopen-innovation-16629
tags:
- Entreprise
- Innovation
- Sciences
---

> Confrontés à une productivité en berne, les laboratoires pharmaceutiques taillent dans leurs effectifs de R&amp;D. Et essaient de trouver la pierre philosophale qui leur permettra de poursuivre leurs recherches à moindre coût, et avec une efficacité accrue. Cette recette miracle pourrait s’appeler “open innovation”. Elle permet aux Big Pharmas de se concentrer sur la mise sur le marché, et de “déléguer” la recherche à une multitude d’acteurs: universités, PME, petits laboratoires. Avec comme idées directrices, synergie et coopération.
