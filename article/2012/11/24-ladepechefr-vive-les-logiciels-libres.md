---
site: LaDepeche.fr
title: "Vive les logiciels libres!"
author: Joëlle Kéclart
date: 2012-11-24
href: http://www.ladepeche.fr/article/2012/11/24/1497169-toulouse-vive-les-logiciels-libres.html
tags:
- Sensibilisation
- Associations
- Video
---

> Vous les utilisez tous les jours. à la maison, à l'école ou au bureau ils font désormais partie de vos outils de travail. Ce sont les logiciels libres.
