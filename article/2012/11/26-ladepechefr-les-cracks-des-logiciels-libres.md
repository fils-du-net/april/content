---
site: LaDepeche.fr
title: "Les cracks des Logiciels libres"
author: Th. Gausserand
date: 2012-11-26
href: http://www.ladepeche.fr/article/2012/11/26/1498587-les-cracks-des-logiciels-libres.html
tags:
- Sensibilisation
- Associations
---

> La deuxième édition de «Capitole du Libre» s'est déroulée ce week-end. «Nous avons voulu que ce nouveau rendez-vous soit l'occasion de regrouper de nombreux experts des technologies du libre de la région sud-ouest afin de générer des échanges avec le grand public»
