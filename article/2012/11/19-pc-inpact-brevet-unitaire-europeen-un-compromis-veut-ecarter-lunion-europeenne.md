---
site: PC INpact
title: "Brevet unitaire européen: un compromis veut écarter l'Union européenne"
author: Marc Rees
date: 2012-11-19
href: http://www.pcinpact.com/news/75393-brevet-unitaire-europeen-compromis-veut-ecarter-union-europeenne.htm
tags:
- April
- Institutions
- Brevets logiciels
- Europe
---

> Aujourd’hui, la Commission des affaires juridiques du Parlement européen va tenir une réunion organisée exclusivement sur le brevet unitaire européen. Après des mois de reports, cette rencontre programmée seulement jeudi dernier marque l'accélération d’un processus à risque. PC INpact vous dévoile le texte de compromis qui sera discuté aujourd'hui.
