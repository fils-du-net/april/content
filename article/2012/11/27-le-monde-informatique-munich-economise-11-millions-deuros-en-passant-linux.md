---
site: Le Monde Informatique
title: "Munich économise 11 millions d'euros en passant à Linux"
author: Jean Elyan
date: 2012-11-27
href: http://www.lemondeinformatique.fr/actualites/lire-munich-economise-11-millions-d-euros-en-passant-a-linux-51432.html
tags:
- Logiciels privateurs
- Administration
- Interopérabilité
- International
---

> La ville de Munich a migré plus de 80 % de ses 15 500 postes de travail sur OpenOffice et LiMux, sa propre distribution Linux basée sur Debian.
