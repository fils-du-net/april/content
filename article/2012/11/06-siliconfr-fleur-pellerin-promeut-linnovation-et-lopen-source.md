---
site: Silicon.fr
title: "Fleur Pellerin promeut l'innovation et l'open source"
author: Ariane Beky
date: 2012-11-06
href: http://www.silicon.fr/fleur-pellerin-linagora-open-source-80730.html
tags:
- Entreprise
- Économie
- Institutions
- Innovation
- International
---

> Lors d’une visite lundi au siège de Linagora, société française de services et éditeur de logiciels open source, la ministre Fleur Pellerin a réaffirmé son soutien aux acteurs du libre et, plus largement, aux entreprises innovantes.
