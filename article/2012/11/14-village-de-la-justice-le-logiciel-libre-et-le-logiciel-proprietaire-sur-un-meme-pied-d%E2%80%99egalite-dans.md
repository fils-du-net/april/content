---
site: Village de la Justice
title: "Le logiciel libre et le logiciel propriétaire sur un même pied d’égalité dans le secteur public?"
author: Chloé Rousselet
date: 2012-11-14
href: http://www.village-justice.com/articles/logiciel-libre-logiciel-proprietaire,13240.html
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
---

> «[…] Pour répondre aux besoins métiers, le logiciel libre doit être considéré à égalité avec les autres solutions». La circulaire Ayrault du 19 septembre 2012 relative à l’«usage du logiciel libre dans l’administration», dont est tirée la précédente citation, est le premier texte juridique à reconnaître une égalité entre le logiciel libre et les autres solutions informatiques, en particulier le logiciel propriétaire.
