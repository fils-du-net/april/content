---
site: Directgestion
title: "Assemblage du puzzle multiplateforme Google, Android, Windows, Mac OS,"
author: Gilles Arnoux
date: 2012-11-10
href: http://www.directgestion.com/sinformer/filactu/18414-assemblage-du-puzzle-multiplateforme-google-android-windows-mac-os
tags:
- Entreprise
- Interopérabilité
---

> Pour quelle raison l'amélioration du sous-système de gestion du stockage ne fait-elle pas encore partie de la feuille de route de Google? La communauté open-source traite les problèmes d'interopérabilité de stockage depuis plus de dix ans.
