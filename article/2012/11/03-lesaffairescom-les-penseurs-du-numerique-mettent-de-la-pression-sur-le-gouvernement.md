---
site: LesAffaires.com
title: "Les penseurs du numérique mettent de la pression sur le gouvernement"
author: Valérie Lesage
date: 2012-11-03
href: http://www.lesaffaires.com/archives/generale/les-penseurs-du-numerique-mettent-de-la-pression-sur-le-gouvernement/550696
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Marchés publics
- International
- Open Data
---

> Fatigués du manque de vision face à l'économie numérique, une douzaine de penseurs du secteur se sont associés pour concevoir les fondements d'un Plan numérique pour le Québec, qu'ils rendront public au cours des prochains jours à Montréal, a appris Les Affaires.
