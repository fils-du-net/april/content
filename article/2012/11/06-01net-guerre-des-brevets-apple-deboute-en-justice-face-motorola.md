---
site: 01net.
title: "Guerre des brevets: Apple débouté en justice face à Motorola"
author: AFP
date: 2012-11-06
href: http://www.01net.com/editorial/579463/guerre-des-brevets-apple-deboute-en-justice-face-a-motorola
tags:
- Entreprise
- Institutions
- Brevets logiciels
---

> Une cour de justice américaine a annulé la plainte d’Apple contre Motorola. La firme de Cupertino remettait en cause les tarifs des licences d’utilisation de certains brevets technologiques détenues par Motorola
