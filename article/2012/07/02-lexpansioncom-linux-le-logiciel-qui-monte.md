---
site: L'Expansion.com
title: "Linux, le logiciel qui monte..."
author: Emmanuel Paquette
date: 2012-07-02
href: http://lexpansion.lexpress.fr/high-tech/linux-le-logiciel-qui-monte_309252.html
tags:
- Entreprise
- Logiciels privateurs
- Brevets logiciels
- Innovation
---

> Linus Torvalds s'étonne encore aujourd'hui des multiples applications de son système d'exploitation. Un regret: seul le marché des PC fait de la résistance.
