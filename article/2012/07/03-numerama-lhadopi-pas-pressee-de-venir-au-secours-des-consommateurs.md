---
site: Numerama
title: "L'Hadopi pas pressée de venir au secours des consommateurs"
author: Guillaume Champeau
date: 2012-07-03
href: http://www.numerama.com/magazine/23091-l-hadopi-pas-pressee-de-venir-au-secours-des-consommateurs.html
tags:
- Interopérabilité
- HADOPI
---

> L'essentiel est de prendre tout son temps. Alors que sa survie est menacée à court ou moyen terme, l'Hadopi repousse de plusieurs mois l'examen d'un dossier qui pourrait faire date dans l'histoire de la régulation des DRM.
