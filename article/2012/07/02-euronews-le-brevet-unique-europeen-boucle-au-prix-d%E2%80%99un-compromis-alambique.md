---
site: euronews
title: "Le brevet unique européen bouclé au prix d’un compromis alambiqué"
date: 2012-07-02
href: http://fr.euronews.com/2012/07/02/le-brevet-unique-europeen-boucle-au-prix-dun-compromis-alambique/
tags:
- Entreprise
- Administration
- Institutions
- Brevets logiciels
- Innovation
- Europe
---

> L’idée datait des années 60. Un brevet automatiquement valable dans toute l’Europe pour faciliter la vie des entreprises, aujourd’hui obligées de faire valider leurs brevets dans chaque pays…
