---
site: ActuaLitté.com
title: "Pourquoi il faut se débarrasser des verrous numériques"
author: Antoine Oury
date: 2012-07-13
href: http://www.actualitte.com/actualite/lecture-numerique/usages/pourquoi-il-faut-se-debarrasser-des-verrous-numeriques-35380.htm
tags:
- Entreprise
- Interopérabilité
- DRM
---

> La question est soulevée par Rebecca Smart, PDG du groupe éditorial Osprey. Formé par Osprey Publishing, Shire, Old House et Angry Robot, le conglomérat s'est distingué de ses concurrents en développant une politique «0 DRM» encore rare dans l'édition numérique. Principalement à cause des Store des revendeurs, qui imposent des verrous pour garantir l'intégrité de leur système marchand.
