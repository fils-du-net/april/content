---
site: PC INpact
title: "Saisine de la Hadopi: les explications de la BNF"
author: Marc Rees
date: 2012-07-04
href: http://www.pcinpact.com/news/72197-saisine-hadopi-explications-bnf.htm
tags:
- Interopérabilité
- HADOPI
- Associations
- DRM
- Droit d'auteur
---

> La BNF nous a transmis une note complète pour nous confirmer sa saisine de la Hadopi. Contrairement à ce que nous avions appris, cette saisine ne concerne cependant pas l’exception Handicap mais toutes les questions de dépôt légal. Trait commun à ces problématiques: les mesures techniques de protection (ou DRM) qui protègent les créations, mais menacent l'effectivité des exceptions. De son côté la Hadopi nous a indiqué qu'elle travaillait bien sur la question du handicap.
