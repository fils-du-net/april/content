---
site: ICTjournal
title: "Des administrations suisses financent la compatibilité de LibreOffice et MS Office"
author: Rodolphe Koller
date: 2012-07-18
href: http://www.ictjournal.ch/fr-CH/News/2012/07/18/Des-administrations-suisses-financent-la-compatibilite-de-LibreOffice-et-MS-Office.aspx
tags:
- Entreprise
- Administration
- Interopérabilité
---

> Des administrations suisses et allemandes investissent 140'000 d’Euros pour améliorer la compatibilité de la suite open source LibreOffice avec MS Office.
