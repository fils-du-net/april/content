---
site: les Inrocks
title: "Richard Stallman: “Nous n’avons pas de dette à l’égard des artistes”"
author: Geoffrey Le Guilcher
date: 2012-07-07
href: http://www.lesinrocks.com/2012/07/07/actualite/richard-stallman-pas-de-dette-a-legard-des-artistes-11275812/
tags:
- Entreprise
- Logiciels privateurs
- Économie
- HADOPI
- Institutions
- DRM
- Licenses
- Philosophie GNU
- International
---

> Richard Stallman est l’un des “pères” du logiciel libre, programme dont le code source est public, traçable et modifiable. Ce barbu sans téléphone mobile prône une réforme radicale et générale du droit d’auteur. Rencontre avec un gourou du web des origines.
