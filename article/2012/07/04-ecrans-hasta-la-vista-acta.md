---
site: écrans
title: "Hasta la vista, Acta"
author: Sophian Fanen
date: 2012-07-04
href: http://www.ecrans.fr/Hasta-la-vista-Acta,14995.html
tags:
- Internet
- Administration
- April
- Institutions
- Associations
- Europe
- ACTA
---

> Le Parlement de Strasbourg a largement rejeté l’Accord commercial anti-contrefaçon, accusé de menacer les libertés publiques sur Internet. C’est une cuisante défaite politique pour la Commission européenne.
