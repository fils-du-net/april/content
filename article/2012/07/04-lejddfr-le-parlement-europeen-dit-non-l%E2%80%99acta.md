---
site: leJDD.fr
title: "Le Parlement européen dit non à l’Acta"
author: G.D
date: 2012-07-04
href: http://www.lejdd.fr/Medias/Internet/Actualite/Le-Parlement-europeen-dit-non-a-l-Acta-525429
tags:
- Entreprise
- Internet
- Associations
- Europe
- ACTA
---

> Les députés européens ont mis leur veto mercredi à la ratification de l'Accord commercial anti-contrefaçon (Acta) conclu entre l'Union européenne et dix autres pays. L'accord concernait notamment le téléchargement illégal. Le texte avait suscité la protestation des milieux liés à Internet, qui craignaient qu'il ne restreigne la liberté des utilisateurs.
