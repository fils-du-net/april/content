---
site: Le Point
title: "Brevet européen: l'accord est vivement critiqué"
author: AFP
date: 2012-07-03
href: http://www.lepoint.fr/economie/brevet-europeen-l-accord-est-vivement-critique-03-07-2012-1480263_28.php
tags:
- Institutions
- Brevets logiciels
- Europe
---

> Paris, Londres et Berlin pensaient avoir trouvé un compromis après des années de blocage. Mais la Commission et le Parlement ne sont pas de cet avis.
