---
site: Numerama
title: "ACTA rejeté en Europe: les opposants exultent"
author: Julien L.
date: 2012-07-04
href: http://www.numerama.com/magazine/23106-acta-rejete-en-europe-les-opposants-exultent.html
tags:
- April
- Institutions
- Associations
- Europe
- ACTA
---

> Laminé par le Parlement européen lors d'un vote historique, l'accord commercial anti-contrefaçon est mort. L'occasion pour les opposants à l'ACTA de se féliciter du résultat et d'adresser de chaleureux remerciements aux Européens qui se sont mobilisés ces derniers mois pour faire capoter le projet de traité international.
