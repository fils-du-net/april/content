---
site: macgeneration
title: "«Ship first, fix later»: un monde en bêta"
author: Anthony Nelzin
date: 2012-07-31
href: http://www.macgeneration.com/unes/voir/131822/-ship-first-fix-later-un-monde-en-beta
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Innovation
---

> Vous avez l'impression d'être un cobaye permanent au service des éditeurs de logiciels et de fabricants de matériel? Votre intuition est la bonne: vous avez en effet payé pour l'obligation de tester le produit… et le droit de recevoir quelques mises à jour par la suite pour régler ses principaux bogues.
