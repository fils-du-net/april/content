---
site: Le Monde Informatique
title: "Un berlinois tente la vie en Open Source pendant un an"
author: Adrien Geneste
date: 2012-07-10
href: http://www.lemondeinformatique.fr/actualites/lire-un-berlinois-tente-la-vie-en-open-source-pendant-un-an-49654.html
tags:
- Économie
- Droit d'auteur
- Innovation
- Philosophie GNU
- International
---

> Sam Muirhead a décidé de vivre un an dans un environnement entièrement libre de droit. De son ordinateur à sa brosse à dent en passant par sa bière, le jeune berlinois entend répandre l'idéologie du partage sur Internet.
