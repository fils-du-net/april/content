---
site: Le Monde.fr
title: "L'ONU reconnaît le droit à la liberté d'expression sur Internet"
date: 2012-07-06
href: http://www.lemonde.fr/technologies/article/2012/07/06/l-onu-reconnait-le-droit-a-la-liberte-d-expression-sur-internet_1730303_651865.html
tags:
- Internet
- Institutions
- International
---

> Le texte onusien affirme que les droits qui s'appliquent hors ligne doivent être protégés aussi en ligne, à travers n'importe quel media, et indépendamment des frontières.
