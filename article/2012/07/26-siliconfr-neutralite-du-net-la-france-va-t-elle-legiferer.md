---
site: Silicon.fr
title: "Neutralité du Net: La France va-t-elle légiférer?"
author: Ariane Beky
date: 2012-07-26
href: http://www.silicon.fr/neutralite-du-net-loi-pellerin-76910.html
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- International
---

> Après le Chili et les Pays-Bas, la France pourrait être le prochain pays à inscrire dans sa législation la neutralité d’Internet. Une neutralité qui consiste à garantir l’accès non filtré aux sources, contenus et services disponibles.
