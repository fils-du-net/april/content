---
site: macgeneration
title: "Brevets Apple: Samsung perd en appel, HTC gagne un répit"
author: Florian Innocente
date: 2012-07-03
href: http://www.macgeneration.com/news/voir/250632/brevets-apple-samsung-perd-en-appel-htc-gagne-un-repit
tags:
- Entreprise
- Économie
- Institutions
- Brevets logiciels
- Innovation
- International
---

> Les ventes de la Galaxy Tab 10.1 restent bloquées aux États-Unis. Samsung avait fait appel d'une décision de la juge Lucy Koh de la Cour de San Jose en Californie, datant du 26 juin. Cette dernière avait estimé que «La Galaxy Tab 10.1 [est] virtuellement indiscernable de l'iPad et de l'iPad 2». La semaine dernière, le Galaxy Nexus, un smartphone conçu avec Google et sorti l'année dernière, avait connu le même sort.
