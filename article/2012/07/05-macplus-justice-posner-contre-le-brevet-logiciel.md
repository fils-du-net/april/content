---
site: MacPlus
title: "Justice: Posner contre le brevet logiciel"
date: 2012-07-05
href: http://www.macplus.net/depeche-67211-justice-posner-contre-le-brevet-logiciel
tags:
- Entreprise
- Économie
- Institutions
- Brevets logiciels
- Innovation
- International
---

> C’est pour Reuters que le juge Posner, celui qui avait mis fin au procès ouvert entre Apple et Motorola, précise sa pensée concernant la croissance exponentielle des brevets dans le secteur des smartphones.
