---
site: Le Monde.fr
title: "Exception culturelle: la mission Lescure fera ses propositions en mars 2013"
date: 2012-07-18
href: http://www.lemonde.fr/politique/article/2012/07/18/exception-culturelle-la-mission-lescure-fera-ses-propositions-en-mars-2013_1735295_823448.html
tags:
- HADOPI
- Institutions
---

> Le gouvernement a officiellement chargé Pierre Lescure de mener une mission sur "l'acte II de l'exception culturelle" face aux enjeux du numérique.
