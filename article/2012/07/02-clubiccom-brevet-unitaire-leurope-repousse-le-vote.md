---
site: clubic.com
title: "Brevet unitaire: l'Europe repousse le vote"
author: Olivier Robillart
date: 2012-07-02
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/brevet-logiciel/actualite-499460-brevet-unitaire.html
tags:
- April
- Institutions
- Brevets logiciels
- Europe
---

> Le Parlement européen se réunit en session plénière du 2 au 5 juillet. Parmi les grands thèmes abordés, la question du brevet unitaire européen (un brevet unique à l'ensemble des Etats-membres de l'Union) devait être discutée. Pourtant, le vote sur l'instauration d'un tel mécanisme a été reporté.
