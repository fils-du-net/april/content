---
site: ZDNet.fr
title: "Hadopi: Pierre Lescure en faveur du maintien des sanctions mais pas de la coupure Internet"
author: Olivier Chicheportiche
date: 2012-07-16
href: http://www.zdnet.fr/actualites/hadopi-pierre-lescure-en-faveur-du-maintien-des-sanctions-mais-pas-de-la-coupure-internet-39774213.htm
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Institutions
---

> Lors d'une rencontre en marge du Festival d'Avignon, celui qui dirige les débats sur l’avenir de la Haute Autorité de lutte contre le téléchargement illégal a donné quelques pistes de réflexion.
