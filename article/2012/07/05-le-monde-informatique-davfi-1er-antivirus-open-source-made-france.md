---
site: Le Monde Informatique
title: "Davfi: 1er antivirus Open Source made in France"
author: Adrien Geneste
date: 2012-07-05
href: http://www.lemondeinformatique.fr/actualites/lire-davfi-1er-antivirus-open-source-made-in-france-49599.html
tags:
- Entreprise
- Innovation
- Sciences
- Informatique en nuage
- Europe
---

> Avec une date de sortie prévue en 2014, Davfi, qui bénéficie de près de 5,5 millions d'euros de budget sur deux ans, entend imposer la souveraineté numérique de la France et révolutionner le monde de l'antivirus.
