---
site: PC INpact
title: "Des rock stars britanniques réclament une meilleure protection du copyright"
author: Xavier Berne
date: 2012-07-25
href: http://www.pcinpact.com/news/72660-des-vedettes-britanniques-reclament-meilleure-protection-copyright.htm
tags:
- Internet
- HADOPI
- Institutions
- International
---

> Dix personnalités britanniques de renommée internationale viennent de co-signer une lettre intitulée « Les musiciens ont besoin d’une législation solide en matière de copyright pour exceller au niveau mondial ». Sur le tapis : les activités illicites sur Internet et le rôle des intermédiaires.
