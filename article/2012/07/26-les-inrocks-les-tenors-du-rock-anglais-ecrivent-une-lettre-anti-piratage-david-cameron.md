---
site: les inrocks
title: "Les ténors du rock anglais écrivent une lettre anti-piratage à David Cameron"
author: Kevin Plasman
date: 2012-07-26
href: http://www.lesinrocks.com/2012/07/26/actualite/les-tenors-du-rock-anglais-ecrivent-une-lettre-anti-piratage-a-david-cameron-11281935/
tags:
- Entreprise
- HADOPI
- Institutions
- International
---

> Dix personnalités emblématiques de la musique ont joint leurs efforts dans une missive à l’attention du Premier ministre britannique. Leur but? Accélérer la mise en œuvre de la Hadopi à l’anglaise.
