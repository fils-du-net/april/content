---
site: l'Humanité.fr
title: "Rejet d’Acta: \"c’est une victoire pour la démocratie\""
author: Pi. M.
date: 2012-07-05
href: http://www.humanite.fr/monde/rejet-d%E2%80%99acta-c%E2%80%99est-une-victoire-pour-la-democratie-500245
tags:
- Économie
- April
- Institutions
- Associations
- Neutralité du Net
- Europe
- ACTA
---

> Le parlement européen a rejeté Acta, après des mois de contestation citoyenne. C’est une véritable victoire pour les détracteurs de ce traité international qui sous couvert de lutter contre la contrefaçon, était une réelle menace pour les droits fondamentaux.
