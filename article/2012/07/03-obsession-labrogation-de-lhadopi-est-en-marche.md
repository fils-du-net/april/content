---
site: OBSESSION
title: "L'abrogation de l'Hadopi est en marche"
author: Boris Manenti 
date: 2012-07-03
href: http://obsession.nouvelobs.com/high-tech/20120703.OBS5858/l-hadopi-court-toujours.html
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Droit d'auteur
- Licenses
---

> Le gouvernement a discrètement confirmé le lancement d'une mission de concertation pour préparer la suppression de l'autorité de lutte contre le téléchargement illégal.
