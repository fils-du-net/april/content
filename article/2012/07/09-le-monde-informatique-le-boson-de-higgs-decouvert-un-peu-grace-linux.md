---
site: Le Monde Informatique
title: "Le Boson de Higgs découvert un peu grâce à... Linux"
author: Jean Elyan
date: 2012-07-09
href: http://www.lemondeinformatique.fr/actualites/lire-le-boson-de-higgs-decouvert-un-peu-grace-a-linux-49646.html
tags:
- Sciences
---

> Après les soubresauts de la seconde de trop qui a perturbé les serveurs sous Linux la semaine dernière, les nouvelles sont bien meilleures pour l'OS Open Source. En effet, selon un chercheur, Linux a aidé les physiciens du CERN à traquer le mystérieux Boson de Higgs.
