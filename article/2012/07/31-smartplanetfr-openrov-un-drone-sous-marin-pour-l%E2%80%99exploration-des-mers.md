---
site: SmartPlanet.fr
title: "OpenROV, un drone sous-marin pour l’exploration des mers"
author: Mary Catherine O'Connor
date: 2012-07-31
href: http://www.smartplanet.fr/smart-business/openrov-un-drone-sous-marin-pour-lexploration-des-mers-16004/
tags:
- Entreprise
- Matériel libre
- Innovation
---

> Ce petit sous-marin télécommandé, développé en open source, a été vendu 775 dollars l'unité contre des dizaines de milliers de dollars pour les gros modèles habituels.
