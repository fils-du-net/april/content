---
site: ActuaLitté.com
title: "La vie sans DRM: le guide pour une vie paisible"
author: Nicolas Gary
date: 2012-07-27
href: http://www.actualitte.com/usages/la-vie-sans-drm-le-guide-pour-une-vie-paisible-35653.htm
tags:
- Internet
- DRM
- Droit d'auteur
- International
---

> Defective By Design travaille pour le meilleur de vos vacances
