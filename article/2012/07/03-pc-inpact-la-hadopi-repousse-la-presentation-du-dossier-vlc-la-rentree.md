---
site: PC INpact
title: "La Hadopi repousse la présentation du dossier VLC à la rentrée"
author: Marc Rees
date: 2012-07-03
href: http://www.pcinpact.com/news/72156-hadopi-repousse-presentation-dossier-vlc-a-rentree.htm
tags:
- Interopérabilité
- HADOPI
- Associations
- DADVSI
- DRM
---

> Au printemps, VLC avait saisi la Hadopi pour savoir comment le fameux lecteur multimédia peut lire les Blu-ray sans violer les DRM qui le verrouillent. Fin juin, le dossier devait être présenté au collège, par le service juridique de la Hadopi. L’association Videolan, éditrice du lecteur libre, devra finalement attendre la rentrée. A minima.
