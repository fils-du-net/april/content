---
site: Numerama
title: "Richard Stallman égratigne l'arrivée de Valve sur Linux"
author: Julien L.
date: 2012-07-31
href: http://www.numerama.com/magazine/23306-richard-stallman-egratigne-l-arrivee-de-valve-sur-linux.html
tags:
- Entreprise
- Logiciels privateurs
- DRM
- Philosophie GNU
- Promotion
---

> Valve a annoncé cet été l'arrivée prochaine de Steam sur Linux, via la distribution Ubuntu. Si l'annonce a ravi de nombreux joueurs équipés du système d'exploitation libre, elle a interpellé Richard Stallman. Dans un bref texte, il égratigne cette décision mais note des effets positifs à long terme pour Linux.
