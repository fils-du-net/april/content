---
site: Le Monde Informatique
title: "Les développeurs Linux tentent de contourner Boot Secure de Windows 8"
author: Adrien Geneste
date: 2012-07-17
href: http://www.lemondeinformatique.fr/actualites/lire-les-developpeurs-linux-tentent-de-contourner-boot-secure-de-windows-8-49759.html
tags:
- Entreprise
- Interopérabilité
- Informatique-deloyale
- Video
---

> James Bottomley, président du conseil technique de la Linux Fondation, vient de rendre disponible une image du système UEFI, qui sera prochainement installé sur les PC. Elle devrait permettre aux développeurs Linux de travailler à la recherche d'une solution pour contrer le futur boot Secure de Windows 8
