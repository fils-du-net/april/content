---
site: Numerama
title: "Vers un déblocage de l'UEFI pour Linux"
author: Simon Robic
date: 2012-07-16
href: http://www.numerama.com/magazine/23191-vers-un-deblocage-de-l-uefi-pour-linux.html
tags:
- Entreprise
- Interopérabilité
- Associations
- DRM
---

> Les développeurs Linux entrevoient le bout du tunnel. Une version open-source de l'UEFI imposé par Microsoft vient d'être publiée. Elle leur permet d'adapter leur OS à ce système qui sécurise la phase de démarrage de l'ordinateur. Sans ça, il faudra que le logiciel soit signé. Un processus payant qui signerait la fin d'un bon nombre de distributions.
