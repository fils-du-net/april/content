---
site: Le Huffington Post
title: "Rejet d'Acta: une victoire historique pour les hackers"
author: Frédéric Bardeau
date: 2012-07-11
href: http://www.huffingtonpost.fr/frederic-bardeau/la-victoire-historique-des-hackers-vs-acta_b_1661651.html
tags:
- Internet
- April
- Associations
- ACTA
---

> ACTA - Comme nous l'étudions depuis des années et comme nous avons essayé de le démontrer au sujet d'Anonymous dans notre récent ouvrage, l'hacktivisme est consubstantiel à Internet (il avait démarré avec les phreakers et le téléphone), et il en est devenu le bras armé. Défenseurs de la neutralité, de l'ouverture et de la liberté du réseau, les hacktivistes peuvent se féliciter pour le rejet d'ACTA qui constitue une de leurs victoires historiques, de celles qui feront date pour l'éternité.
