---
site: AgoraVox
title: "Quand la Défense française fournit ses données à Google!"
author: Denis Szalkowski
date: 2012-07-21
href: http://www.agoravox.fr/actualites/technologies/article/quand-la-defense-francaise-fournit-120265
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Video
- International
---

> Google est une entreprise américaine. Et à ce titre, elle est soumise au Patriot Act. Son modèle économique est la collecte de données issues de l'utilisation de logiciels et de services "gratuits".
