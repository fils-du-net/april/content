---
site: Le Monde.fr
title: "Brevets: échec d'une réunion de conciliation entre Apple et Samsung"
date: 2012-07-24
href: http://www.lemonde.fr/technologies/article/2012/07/24/brevets-echec-d-une-reunion-de-conciliation-entre-apple-et-samsung_1737459_651865.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Standards
- International
---

> Depuis plus d'un an, les deux groupes s'affrontent dans des tribunaux du monde entier, s'accusant mutuellement de violations de brevets.
