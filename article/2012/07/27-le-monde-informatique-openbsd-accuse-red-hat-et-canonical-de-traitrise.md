---
site: Le Monde Informatique
title: "OpenBSD accuse Red Hat et Canonical de traîtrise"
author: Adrien Geneste
date: 2012-07-27
href: http://www.lemondeinformatique.fr/actualites/lire-openbsd-accuse-red-hat-et-canonical-de-traitrise-49906.html
tags:
- Entreprise
- DRM
- Informatique-deloyale
---

> Alors que la communauté Open Source travaille dur à l'élaboration d'un processus pour passer outre le futur système Boot Secure développé par Microsoft, Red Hat et Canonical ont choisi de travailler en collaboration avec la firme de Redmond. Une trahison, pour les instigateurs du projet OpenBSD
