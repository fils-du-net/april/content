---
site: Le Point
title: "La France va-t-elle sauver la peau de WikiLeaks?"
date: 2012-07-18
href: http://www.lepoint.fr/high-tech-internet/la-france-va-t-elle-sauver-la-peau-de-wikileaks-18-07-2012-1486894_47.php
tags:
- Entreprise
- Économie
---

> Grâce à une spécificité du système bancaire français, une association contourne les blocus pour renflouer les comptes d'Assange.
