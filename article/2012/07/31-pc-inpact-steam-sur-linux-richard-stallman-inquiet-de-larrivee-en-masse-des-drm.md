---
site: PC INpact
title: "Steam sur Linux: Richard Stallman inquiet de l'arrivée en masse des DRM"
author: Vincent Hermann
date: 2012-07-31
href: http://www.pcinpact.com/news/72785-steam-sur-linux-richard-stallman-pointe-arrivee-en-masse-drm.htm
tags:
- Entreprise
- Logiciels privateurs
- DRM
- Philosophie GNU
---

> Le mal pour un bien? Un bien pour un mal?
