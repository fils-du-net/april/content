---
site: Blog LeMonde.fr
title: "Changer la façon de changer le monde: un festival pour activistes sociaux"
author: Francis Pisani
date: 2012-07-13
href: http://winch5.blog.lemonde.fr/2012/07/13/changer-la-facon-de-changer-le-monde-un-festival-pour-activistes-sociaux/
tags:
- Internet
- Innovation
---

> Qui n'a pas rêvé de conférences TED pour les pauvres, pour les activistes, pour les gens qui veulent changer le monde mais n'ont ni les moyens d'aller à Long Beach ni les connexions pour participer aux versions TEDx qui s'organisent un peu partout dans le monde? Eh bien des Serbes l'ont fait. Ils appellent ça les conférences SHARE. Les deux premières ont eu lieu à Belgrade. La troisième se tiendra à Beyrouth en octobre.
