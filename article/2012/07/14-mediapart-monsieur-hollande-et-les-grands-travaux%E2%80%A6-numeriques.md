---
site: Mediapart
title: "Monsieur Hollande et les Grands Travaux… numériques?"
author: shaman dolpi
date: 2012-07-14
href: http://blogs.mediapart.fr/blog/shaman-dolpi/140712/monsieur-hollande-et-les-grands-travaux-numeriques
tags:
- Entreprise
- Économie
- Institutions
- Associations
- Innovation
- Informatique en nuage
---

> Le président Hollande avait promis une politique de Grands Travaux, mais il s’avère que des «économies écologiques» peuvent rapporter gros. Mais + que des économies, le dopage numérique, lui, peut booster la compétitivité industrielle aussi bien dans le privé que dans le public. Le gouvernement français l’a-t-il compris?
