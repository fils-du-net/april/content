---
site: Le Monde.fr
title: "Le Parlement européen vote contre le traité anticontrefaçon ACTA"
author: Damien Leloup
date: 2012-07-05
href: http://www.lemonde.fr/technologies/article/2012/07/04/le-parlement-europeen-vote-contre-le-traite-anti-contrefacon-acta_1729032_651865.html
tags:
- Institutions
- Associations
- Droit d'auteur
- Europe
- ACTA
---

> Très décrié par les organisations de défense des libertés numériques, le texte prévoyait notamment d'augmenter la responsabilité des fournisseurs d'accès à Internet en matière de téléchargement illégal.
