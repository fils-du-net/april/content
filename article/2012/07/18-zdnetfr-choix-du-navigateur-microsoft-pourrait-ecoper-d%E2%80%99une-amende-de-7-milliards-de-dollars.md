---
site: ZDNet.fr
title: "Choix du navigateur: Microsoft pourrait écoper d’une amende de 7 milliards de dollars"
date: 2012-07-18
href: http://www.zdnet.fr/actualites/choix-du-navigateur-microsoft-pourrait-ecoper-d-une-amende-de-7-milliards-de-dollars-39774332.htm
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Informatique-deloyale
- Europe
---

> 7 milliards de dollars, 10% de son CA annuel: c’est la sanction dont Microsoft pourrait écoper pour ne pas avoir affiché l’écran de choix du navigateur sur des millions de PC. Si Microsoft plaide l’erreur technique, il aura fort à faire pour convaincre la Commission européenne qu’une telle défaillance a pu lui échapper pendant plus d’un an.
