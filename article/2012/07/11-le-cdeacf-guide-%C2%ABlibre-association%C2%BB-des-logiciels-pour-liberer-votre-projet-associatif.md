---
site: Le CDÉACF
title: "Guide «Libre Association»: des logiciels pour libérer votre projet associatif"
author: Melissa
date: 2012-07-11
href: http://cdeacf.ca/actualite/2012/07/11/guide-libre-association-logiciels-pour-liberer-votre-projet
tags:
- Internet
- April
- Associations
- Promotion
---

> Biens communs à développer et à protéger, les logiciels libres offrent à chacun la possibilité de les copier, de les modifier et de les diffuser à volonté et en toute légalité. Ils sont aujourd’hui en mesure de répondre à la plupart des besoins des associations. Pourtant, celles-ci ont peu recours à cette offre informatique éthique et respectueuse des libertés des utilisateurs.
