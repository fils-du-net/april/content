---
site: PC INpact
title: "Les prochains députés devront choisir entre Ubuntu et Windows"
author: Marc Rees
date: 2012-06-09
href: http://www.pcinpact.com/news/71556-ubuntu-windows-assemblee-nationale-deputes.htm
tags:
- Entreprise
- Logiciels privateurs
- April
- Institutions
- Marchés publics
---

> Exclusif : En janvier 2007, l’Assemblée nationale lançait un appel d’offres pour « la mise en oeuvre de l'environnement logiciel libre des postes micro-informatiques des députés lors de la XIIIe législature ». Quelques mois plus tard, c’est Linagora et Unilog qui remportaient la mise. Cocorico! 577 députés allaient basculer sous Ubuntu, OpenOffice.org et Firefox. Une étape symbolique forte pour le libre, appelé à irriguer l’institution parlementaire.
