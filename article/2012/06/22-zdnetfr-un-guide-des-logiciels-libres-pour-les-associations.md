---
site: ZDNet.fr
title: "Un guide des logiciels libres pour les associations"
author: Thierry Noisette
date: 2012-06-22
href: http://www.zdnet.fr/blogs/l-esprit-libre/un-guide-des-logiciels-libres-pour-les-associations-39773240.htm
tags:
- April
- Sensibilisation
- Associations
---

> L'April et la Fondation Crédit Coopératif publient le Guide Libre Association, un outil complété par une clé USB (Framasoft). Bureautique, gestion, travail collaboratif, le guide passe en revue de nombreux logiciels adaptés au travail des associations.
