---
site: ZDNet.fr
title: "L'Open Source doit s'imposer dans la virtualisation selon IBM"
date: 2012-06-27
href: http://www.zdnet.fr/actualites/l-open-source-doit-s-imposer-dans-la-virtualisation-selon-ibm-39773458.htm
tags:
- Entreprise
- Interopérabilité
- Innovation
- Standards
- Informatique en nuage
---

> L’Open Source et les technologies ouvertes ont un coup à jouer dans les secteurs de la virtualisation, du Cloud Computing et du Big Data. Mais pour IBM, ces technologies doivent avancer plus vite si elles veulent trouver leur place.
