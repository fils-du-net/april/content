---
site: FQDE
title: "Comment les logiciels libres peuvent faire une différence"
author: Patrick Giroux et Renée Fountain
date: 2012-06-13
href: http://fqde.qc.ca/revue-fqde/resume-de-ledition-printemps-2012/comment-les-logiciels-libres-peuvent-faire-une-difference/
tags:
- Internet
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Sensibilisation
- Associations
- Éducation
- Sciences
- International
---

> Ceux qui nous connaissent savent que nous sommes partisans d’une plus grande utilisation des logiciels libres. Nous constatons souvent dans notre travail (enseignements et recherches en lien avec les technologies éducatives), qu’ils sont méconnus. Conséquemment, des décideurs agissent à partir de savoirs qui ne sont pas à jour, voire incomplets. Les logiciels libres pourraient pourtant aider l’école québécoise à gérer ou intégrer plusieurs « différences ».
