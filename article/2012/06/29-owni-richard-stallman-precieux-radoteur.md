---
site: OWNI
title: "Richard Stallman, précieux radoteur"
author: Sabine Blanc et Ophelia Noor
date: 2012-06-29
href: http://owni.fr/2012/06/29/richard-stallman-precieux-radoteur/
tags:
- Entreprise
- Logiciels privateurs
- Licenses
- Philosophie GNU
---

> Le pape du logiciel libre a donné une conférence ce jeudi à Paris sur le thème ”Logiciels libres et droits de l’Homme”. Son discours prend une dimension supplémentaire dans un contexte de surveillance croissante des citoyens.
