---
site: Le Monde Informatique
title: "Solutions Linux 2012: La formation aux logiciels libres doit évoluer"
author: Véronique Arène
date: 2012-06-19
href: http://www.lemondeinformatique.fr/actualites/lire-solutions-linux-la-formation-aux-logiciels-libres-doit-evoluer-49372.html
tags:
- Entreprise
- Économie
- Éducation
---

> Philippe Montargès, co-président d'Alter Way, estime que l'offre de formation initiale et continue n'est pas suffisamment adaptée au modèle du logiciel libre pour pouvoir répondre aux besoins actuels et futurs des entreprises.
