---
site: Agence Ecofin
title: "Cameroun: les maquettistes formés aux logiciels libres de production de livres"
date: 2012-06-28
href: http://www.agenceecofin.com/internet/2806-5535-cameroun-les-maquettistes-formes-aux-logiciels-libres-de-production-de-livres
tags:
- Éducation
- International
---

> L'Organisation internationale de la francophonie (OIF) a organisé, du 10 au 16 juin 2012, une formation à Douala, au Cameroun, destinée à accroître les compétences de maquettistes africains dans la réalisation et la production de livres. Piloté par la Direction de l’éducation et de la formation et la Direction de la francophonie numérique de l’OIF, ce renforcement de capacités fait suite à une autre formation régionale qui a eu lieu en septembre 2011 à Brazzaville, au Congo.
