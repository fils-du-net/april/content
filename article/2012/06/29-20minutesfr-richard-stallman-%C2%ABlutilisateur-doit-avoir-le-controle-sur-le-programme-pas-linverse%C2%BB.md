---
site: 20minutes.fr
title: "Richard Stallman: «L'utilisateur doit avoir le contrôle sur le programme, pas l'inverse»"
author: Annabelle Laurent
date: 2012-06-29
href: http://www.20minutes.fr/web/963175-richard-stallman-l-utilisateur-doit-avoir-controle-programme-inverse
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Institutions
- Philosophie GNU
- International
---

> Le père du logiciel libre était à Paris jeudi dans le cadre d’une rencontre co-organisée par la FIDH et Reporters Sans Frontières…
