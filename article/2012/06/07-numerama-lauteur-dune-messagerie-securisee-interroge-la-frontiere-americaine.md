---
site: Numerama
title: "L'auteur d'une messagerie sécurisée interrogé à la frontière américaine"
author: Guillaume Champeau
date: 2012-06-07
href: http://www.numerama.com/magazine/22814-l-auteur-d-une-messagerie-securisee-interroge-a-la-frontiere-americaine.html
tags:
- Internet
- Institutions
- Associations
- International
---

> Le développeur de Cryptocat affirme avoir été arrêté aux Etats-Unis lors d'un vol vers le Canada. Il aurait été interrogé sur le fonctionnement de son service de messagerie instantanée sécurisé, destiné à empêcher l'interception des communications.
