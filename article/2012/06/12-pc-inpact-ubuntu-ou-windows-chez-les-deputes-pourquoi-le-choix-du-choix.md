---
site: PC INpact
title: "Ubuntu ou Windows chez les députés, pourquoi le choix du choix?"
author: Marc Rees
date: 2012-06-12
href: http://www.pcinpact.com/news/71622-assemblee-nationale-ubuntu-windows-briand.htm
tags:
- Entreprise
- Internet
- Administration
- Interopérabilité
- April
- Institutions
- Vente liée
---

> Chassé par la porte, il revient par la fenêtre
