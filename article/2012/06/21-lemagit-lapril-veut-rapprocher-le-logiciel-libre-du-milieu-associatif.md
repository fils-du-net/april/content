---
site: LeMagIT
title: "L'April veut rapprocher le logiciel libre du milieu associatif"
author: Cyrille Chausson
date: 2012-06-21
href: http://www.lemagit.fr/article/linux-april/11315/1/l-april-veut-rapprocher-logiciel-libre-milieu-associatif/
tags:
- April
- Sensibilisation
- Associations
---

> En ligne avec son motto «jeter des ponts entre le logiciel libre et le monde associatif», le groupe de travail Libre Association de l’April a profité de la présence des nombreuses associations installées à la conférence Solutions Linux 2012, pour présenter son Guide Libre Association.
