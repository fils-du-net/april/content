---
site: SmartPlanet.fr
title: "Le matériel médical open source, une piste pour améliorer la sécurité"
author: Reena Jana
date: 2012-06-25
href: http://www.smartplanet.fr/smart-technology/le-materiel-medical-open-source-une-piste-pour-ameliorer-la-securite-14802/
tags:
- Administration
- Interopérabilité
- Innovation
- Standards
---

> Plusieurs projets développent des instruments médicaux open source, dont les logiciels partagés et les spécifications techniques ouvertes peuvent améliorer la recherche et corriger des failles dangereuses.
