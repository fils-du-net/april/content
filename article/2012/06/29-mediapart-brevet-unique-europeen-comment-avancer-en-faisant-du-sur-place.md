---
site: Mediapart
title: "Brevet unique européen: comment avancer en faisant du sur-place"
author: Vincent Truffy
date: 2012-06-29
href: http://www.mediapart.fr/article/offert/311a7d447969b1ebb098dd09bac65d4b
tags:
- Entreprise
- April
- Institutions
- Brevets logiciels
- Innovation
- Europe
- ACTA
---

> Le 4 juillet, le Parlement européen rejettera probablement l’accord commercial anticontrefaçon ACTA, après l’avis négatif unanime de cinq commissions parlementaires. Mais le Conseil européen et la présidence danoise qui s’achève pourront peut-être se prévaloir d’un succès dans le domaine de la propriété intellectuelle: le même jour, en effet, les eurodéputés doivent se prononcer sur le brevet unique européen, vieille ambition communautaire qui bute depuis des décennies sur les objections contradictoires des États-membres.
