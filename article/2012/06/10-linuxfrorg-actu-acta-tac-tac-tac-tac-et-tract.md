---
site: linuxfr.org
title: "Actu ACTA, tac, tac, tac, tac et tract"
author: Benoît Sibaud
date: 2012-06-10
href: http://linuxfr.org/news/actu-acta-tac-tac-tac-tac-et-tract
tags:
- Internet
- Institutions
- Licenses
- ACTA
---

> Des nouvelles manifestations contre l'accord international anti-contrefaçon ACTA ont eu lieu ce samedi partout dans le monde (une centaine de villes) et en France (21 villes).
