---
site: PC INpact
title: "Choix du poste de travail: lettre ouverte d'un citoyen à son député"
author: Marc Rees
date: 2012-06-27
href: http://www.pcinpact.com/news/71947-choix-poste-travail-deputes-windows.htm
tags:
- Logiciels privateurs
- Institutions
---

> François Revol, ingénieur, auteur de logiciels libres, citoyen (@mmu_man) et électeur vient d’adresser ce courrier à l’ensemble des députés. Ceux-ci sont actuellement appelés à choisir leur poste de travail : si lors de la précédente législature, Ubuntu était le seul en place, les nouveaux élus doivent désormais choisir entre la distribution libre et Windows 7 accompagné du fidèle Pack Office. On pourra lire les explications que nous a données l’un des questeurs de l’Assemblée nationale.
