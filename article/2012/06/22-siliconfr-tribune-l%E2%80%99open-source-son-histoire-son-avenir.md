---
site: Silicon.fr
title: "Tribune: l’open source, son histoire, son avenir"
author: Bertrand Diard
date: 2012-06-22
href: http://www.silicon.fr/tribune-open-source-75993.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Innovation
---

> Bertrand Diard, PDG et cofondateur de Talend, revient sur la longue histoire de l’open source et des logiciels libres, et trace leur futur.
