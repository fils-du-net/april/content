---
site: LE CERCLE Les Echos
title: "De quelques aspects juridiques du modèle économique"
author: Beatrice Lerat
date: 2012-06-18
href: http://lecercle.lesechos.fr/entrepreneur/juridique/221148083/quelques-aspects-juridiques-modele-economique-open-source
tags:
- Entreprise
- Économie
- Institutions
- Licenses
---

> Rendu crédible par le succès de Linux, le logiciel libre est devenu un véritable phénomène. L’open source est ainsi aujourd’hui un des vecteurs majeurs d’innovation dans le domaine des nouvelles technologies. Aucun acteur économique ne peut donc faire l’impasse sur les aspects juridiques spécifique des logiciels libres.
