---
site: Programmez.com
title: "L'Open Source est partout!"
author: François Tonic
date: 2012-06-01
href: http://www.programmez.com/magazine_articles.php?id_article=1712
tags:
- Entreprise
- Logiciels privateurs
- Sensibilisation
- Promotion
- Informatique en nuage
---

> Incontestablement, l’Open Source a su conquérir l’informatique et les utilisateurs, notamment les développeurs et les entreprises
