---
site: 20minutes.fr
title: "Vous avez interviewé Maxime Rouquet, co-président du Parti pirate"
date: 2012-06-06
href: http://www.20minutes.fr/elections/948061-interviewe-maxime-rouquet-co-president-parti-pirate
tags:
- Internet
- Économie
- HADOPI
- Institutions
- DADVSI
- Droit d'auteur
- International
---

> Comme je l’ai indiqué, au Parti Pirate, ce sont les membres qui élaborent et votent les propositions. Le meilleur moyen pour le faire évoluer et s’étendre est de nous rejoindre que ce soit en s’inscrivant sur le forum, en adhérant, ou tout simplement en prenant contact avec les pirates proches de chez vous et en venant aux réunions en région! (Voir la liste des liens sur la colonne de droite sur notre blog principal; et ne pas hésiter à organiser une rencontre s’il n’y en a pas de prévue dans votre région )
