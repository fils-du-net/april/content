---
site: leParisien.fr
title: "Contrefaçon sur internet: nouvelles manifestations en France contre l'Acta"
author: AFP
date: 2012-06-09
href: http://www.leparisien.fr/toulouse-31000/contrefacon-sur-internet-nouvelles-manifestations-en-france-contre-l-acta-09-06-2012-2040554.php
tags:
- Internet
- April
- Institutions
- Droit d'auteur
- RGI
- ACTA
---

> Plus de 200 personnes ont manifesté samedi à Paris et à Toulouse contre l'accord commercial anti-contrefaçon Acta, contesté par nombre d'internautes le dénonçant comme "une menace pour les libertés publiques", ont constaté des journalistes de l'AFP.
