---
site: clubic.com
title: "ACTA une nouvelle fois rejeté en commission au Parlement européen"
author: Alexandre Laurent
date: 2012-06-21
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/actualite-497760-acta-rejete-commission-parlement-europeen.html
tags:
- Institutions
- Europe
- ACTA
---

> Réunie jeudi matin, la commission du Commerce International du Parlement européen a prononcé un avis négatif à l'encontre de l'accord international anti-contrefaçon ACTA. Elle est la cinquième commission à se prononcer contre le texte, qui doit faire l'objet d'un vote en session plénière début juillet.
