---
site: les inrocks
title: "High-tech: La révolution de l’open source"
author: Philippe Richard
date: 2012-06-08
href: http://www.lesinrocks.com/2012/06/08/medias/high-tech-la-revolution-de-lopen-source-11267376/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Économie
- Sensibilisation
- Philosophie GNU
---

> A l’origine réservés aux bidouilleurs, les logiciels libres sont aujourd’hui partout, des serveurs aux tablettes.
