---
site: ComputerworldUK.com
title: "Update: European Parliament trade committee recommends rejecting ACTA treaty"
author: Jennifer Baker
date: 2012-06-21
href: http://www.computerworlduk.com/news/it-business/3365466/breaking-news-european-parliament-trade-committee-recommends-rejecting-acta-treaty/
tags:
- Internet
- Institutions
- Europe
- ACTA
- English
---

> (le comité Européen sur le commerce a recommandé le rejet de l'ACTA et a aussi décidé de ne pas repousser le vote au parlement) The European Parliament's trade committee, INTA, has recommended rejecting the ACTA anti-piracy treaty. It also decided not to postpone the crucial parliamentary vote on the controversial agreement.
