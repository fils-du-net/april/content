---
site: Numerama
title: "Mathieu Kassovitz encourage le Parti Pirate à continuer"
author: Guillaume Champeau
date: 2012-06-11
href: http://www.numerama.com/magazine/22854-mathieu-kassovitz-encourage-le-parti-pirate-a-continuer.html
tags:
- Internet
- HADOPI
- Institutions
- DRM
- Droit d'auteur
---

> Opposé à la lutte contre le piratage, le réalisateur, producteur et acteur Mathieu Kassovitz a apporté lundi son soutien au Parti Pirate, qu'il encourage à poursuivre ses efforts.
