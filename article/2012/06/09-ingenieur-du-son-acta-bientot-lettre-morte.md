---
site: Ingénieur du son
title: "Acta: Bientôt lettre morte?"
author: Luciana Ferreira
date: 2012-06-09
href: http://www.ingenieurduson.com/actu/826/Acta-Bientot-lettre-morte
tags:
- Internet
- April
- Institutions
- Associations
- ACTA
---

> Ce début juin, l'ACTA a été rejeté par 4 commissions du Parlement Européen. Néanmoins, l'avis de ces commissions n'est pas définitif. C'est la Commission du Commerce International (INTA) qui devra se prononcer le 21 juin et adopter une proposition sur le fond. L'accord n'est donc pas encore été mis de côté, mais on y est presque...
