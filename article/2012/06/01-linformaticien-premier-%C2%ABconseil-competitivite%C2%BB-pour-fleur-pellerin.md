---
site: L'INFORMATICIEN
title: "Premier «Conseil Compétitivité» pour Fleur Pellerin"
author: Orianne Vatin
date: 2012-06-01
href: http://www.linformaticien.com/actualites/id/25053/premier-conseil-competitivite-pour-fleur-pellerin.aspx
tags:
- Entreprise
- Internet
- April
- Institutions
- Brevets logiciels
- Innovation
- Europe
---

> Signalons également que ce premier Conseil Compétitivité du gouvernement Hollande a éveillé certaines attentes du côté de l'April, qui a interpellé le nouveau président de la République par le biais d'un communiqué, qui l'appelait à "profiter de cette occasion pour agir contre les brevets logiciels et mettre sur le tapis les défauts et problèmes du projet actuel de brevet unitaire".
