---
site: Les blogs du Diplo
title: "Elinor Ostrom ou la réinvention des biens communs"
author: Hervé Le Crosnier
date: 2012-06-15
href: http://blog.mondediplo.net/2012-06-15-Elinor-Ostrom-ou-la-reinvention-des-biens-communs
tags:
- Internet
- Économie
- Institutions
- Innovation
- RGI
---

> Première femme à obtenir un Prix Nobel d’économie (en 2009) pour ses développements sur la théorie des communs, Elinor Ostrom est décédée ce mardi 12 juin, à l’âge de 78 ans. Chercheuse politique infatigable et pédagogue ayant à cœur de transmettre aux jeunes générations ses observations et analyses, elle avait, malgré sa maladie, continué son cycle de conférences et la rencontre avec les jeunes chercheurs du domaine des communs au Mexique et en Inde. Récemment encore, elle exprimait son sentiment d’urgence à propos de la conférence Rio+20 qui se déroule actuellement.
