---
site: Le Monde Informatique
title: "Secure boot de Windows 8 sème la zizanie dans l'Open Source"
author: Jacques Cheminat avec IDG NS
date: 2012-06-25
href: http://www.lemondeinformatique.fr/actualites/lire-secure-boot-de-windows-8-seme-la-zizanie-dans-l-open-source-49450.html
tags:
- Entreprise
- Logiciels privateurs
- Interopérabilité
---

> Le monde de l'Open Source se dispute sur la position à adopter face à la décision de Microsoft d'intégrer une fonction de démarrage sécurisé à l'UEFI de Windows 8. Deux stratégies s'affrontent au sein des distributions Linux.
