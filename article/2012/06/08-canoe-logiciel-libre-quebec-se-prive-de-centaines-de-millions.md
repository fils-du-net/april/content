---
site: canoe
title: "Logiciel libre: Québec se prive de centaines de millions"
author: Régys Caron, avec Argent
date: 2012-06-08
href: http://fr.canoe.ca/techno/materiel/ordinateurs/archives/2012/06/20120608-150620.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Éducation
- Marchés publics
- International
---

> Le gouvernement Charest se prive de plusieurs centaines de millions $ en transigeant avec Microsoft pour le renouvellement de 500 000 postes informatiques dans le milieu de l'éducation au lieu de faire appel au logiciel libre, soutient le Parti québécois.
