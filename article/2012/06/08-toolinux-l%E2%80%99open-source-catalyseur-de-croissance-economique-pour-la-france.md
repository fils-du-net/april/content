---
site: toolinux
title: "L’open source: catalyseur de croissance économique pour la France"
author: Bertrand Diard
date: 2012-06-08
href: http://www.toolinux.com/Bertrand-Diard-PDG-de-Talend-L
tags:
- Entreprise
- Administration
- Économie
- Éducation
- Innovation
- International
---

> Le nouveau gouvernement français souhaite privilégier le soutien à la croissance plutôt que la réduction des coûts et ce discours a trouvé un écho favorable dans un certain nombre de pays européens, voire même outre-Atlantique. Loin de moi l’idée de lancer un débat sur la faisabilité ou non de ce type de stratégie économique. Mais, il me semble que dans un domaine que je connais bien – l’édition de logiciels open source – certains enseignements pourraient apporter de l’eau au moulin des pouvoirs publics.
