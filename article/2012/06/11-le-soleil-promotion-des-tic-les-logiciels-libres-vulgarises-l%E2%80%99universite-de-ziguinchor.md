---
site: Le soleil
title: "Promotion des Tic: Les logiciels libres vulgarisés à l’Université de Ziguinchor"
author: Moussa SADIO
date: 2012-06-11
href: http://www.lesoleil.sn/index.php?view=article&id=16436
tags:
- Logiciels privateurs
- Économie
- Sensibilisation
- Éducation
- International
---

> Une journée Linux et logiciels libres, première du genre, a été organisée à l’Université de Ziguinchor, samedi dernier, en vue de la vulgarisation de ces outils informatiques.
