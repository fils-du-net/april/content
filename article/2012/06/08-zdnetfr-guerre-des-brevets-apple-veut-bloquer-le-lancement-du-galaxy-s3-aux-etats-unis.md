---
site: ZDNet.fr
title: "Guerre des brevets: Apple veut bloquer le lancement du Galaxy S3 aux Etats-Unis"
date: 2012-06-08
href: http://www.zdnet.fr/actualites/guerre-des-brevets-apple-veut-bloquer-le-lancement-du-galaxy-s3-aux-etats-unis-39772689.htm
tags:
- Entreprise
- Économie
- Institutions
- Brevets logiciels
- Innovation
- International
---

> Une audience dans ce sens doit avoir leu ce vendredi dans un tribunal californien. Apple estime une nouvelle fois que son concurrent viole plusieurs de ses brevets.
