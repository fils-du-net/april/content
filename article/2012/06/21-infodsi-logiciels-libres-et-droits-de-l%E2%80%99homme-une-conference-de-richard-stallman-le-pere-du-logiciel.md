---
site: infoDSI
title: "Logiciels libres et droits de l’Homme, une conférence de Richard Stallman, le père du logiciel libre"
date: 2012-06-21
href: http://www.infodsi.com/articles/133568/logiciels-libres-droits-homme-conference-richard-stallman-pere-logiciel-libre.html
tags:
- Associations
- Philosophie GNU
- Promotion
---

> La Fédération internationale des droits de l'Homme (FIDH), Reporters sans frontières, Telecomix et Silicon Maniacs organisent une conférence intitulée Logiciels libres et droits de l’Homme, de Richard Stallman, le père du logiciel libre.
