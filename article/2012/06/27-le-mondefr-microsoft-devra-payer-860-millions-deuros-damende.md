---
site: Le Monde.fr
title: "Microsoft devra payer 860 millions d'euros d'amende"
date: 2012-06-27
href: http://www.lemonde.fr/technologies/article/2012/06/27/microsoft-devra-payer-860-millions-d-euros-d-amende_1725061_651865.html
tags:
- Entreprise
- Interopérabilité
- Europe
---

> Le Tribunal de l'Union européenne a confirmé, mercredi, que la firme devra s'acquitter d'une amende record pour non-respect de la législation européenne sur la concurrence.
