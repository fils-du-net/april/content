---
site: Le Monde.fr
title: "L'informatique est une science bien trop sérieuse pour être laissée aux informaticiens"
author: Serge Abiteboul, Colin de la Higuera et Gilles Dowek
date: 2012-06-22
href: http://www.lemonde.fr/idees/article/2012/06/22/l-informatique-est-une-science-bien-trop-serieuse-pour-etre-laissee-aux-informaticiens_1722939_3232.html
tags:
- Éducation
- Vote électronique
---

> L'informatique peut et doit être enseignée à tous. Il ne faut pas laisser une partie de la population sur le bord de la route.
