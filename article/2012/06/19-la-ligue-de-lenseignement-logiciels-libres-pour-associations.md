---
site: la ligue de l'enseignement
title: "Logiciels libres pour associations"
author: Denis Lebioda
date: 2012-06-19
href: http://www.laligue-alpesdusud.org/associatifs_leblog/?2012/06/19/3386-logiciels-libres-pour-associations
tags:
- Internet
- April
- Sensibilisation
- Associations
---

> L’April, association de promotion et de défense du logiciel libre, a conçu en partenariat avec la fondation Crédit Coopératif le Guide "Libre Association: des logiciels pour libérer votre projet associatif".
