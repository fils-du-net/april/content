---
site: madmoiZelle
title: "La philosophie du logiciel libre"
author: Lady Dylan
date: 2012-06-26
href: http://www.madmoizelle.com/la-philosophie-du-logiciel-libre-111649
tags:
- Logiciels privateurs
- Sensibilisation
- Philosophie GNU
---

> De nouveaux conseils de Lady Dylan pour mieux connaître, comprendre et maîtriser votre ordinateur, cet outil fabuleux!
