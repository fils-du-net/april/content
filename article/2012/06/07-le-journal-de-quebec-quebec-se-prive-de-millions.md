---
site: Le Journal de Québec
title: "Québec se prive de millions"
author: Régys Caron
date: 2012-06-07
href: http://www.journaldequebec.com/2012/06/07/450-millions-envoles
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Éducation
- Marchés publics
- International
---

> Le gouvernement Charest se prive de plusieurs centaines de millions de dollars en transigeant avec Microsoft pour le renouvellement de 500 000 postes informatiques dans le milieu de l’éducation au lieu de faire appel au logiciel libre, soutient le Parti québécois.
