---
site: Silicon.fr
title: "L'APRIL s'oppose au projet européen de brevet unitaire"
author: Ariane Beky
date: 2012-06-26
href: http://www.silicon.fr/april-oppose-brevet-unitaire-76143.html
tags:
- Administration
- Économie
- April
- Institutions
- Brevets logiciels
- Europe
---

> Opposée à la proposition de règlement européen en faveur de la création d’un brevet unitaire, l’Association de promotion et de défense du logiciel libre (APRIL) appelle à la mobilisation des internautes et des eurodéputés.
