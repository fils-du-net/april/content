---
site: Numerama
title: "419 candidats ont signé le pacte du logiciel libre"
author: Julien L.
date: 2012-06-09
href: http://www.numerama.com/magazine/22839-419-candidats-ont-signe-le-pacte-du-logiciel-libre.html
tags:
- April
- Institutions
- Sensibilisation
- Droit d'auteur
- Standards
---

> L'April poursuit sa campagne de sensibilisation. Alors que le premier tour des élections législatives aura lieu dimanche, l'association est parvenue à convaincre 419 candidats de signer le pacte du logiciel libre.
