---
site: PC INpact
title: "Rejet d’ACTA en commission INTA: entre satisfaction et temporisation"
author: Xavier Berne
date: 2012-06-21
href: http://www.pcinpact.com/news/71846-acta-inta-rejet-vote-reactions.htm
tags:
- Institutions
- Innovation
- Europe
- ACTA
---

> Quelques minutes après l’annonce du rejet d’ACTA par la commission INTA ce matin au Parlement européen, les réactions des opposants au traité se sont succédées, que ce soit sur les réseaux sociaux ou de manière plus officielle. Même si la plupart des voix se réjouissaient de cette décision, la temporisation reste dans toutes les têtes, puisque seul le vote des eurodéputés, fixé au 4 juillet prochain, sera déterminant.
