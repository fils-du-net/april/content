---
site: CNET News
title: "Import bans over patents cause 'substantial harm,' FTC says"
author: Steven Musil
date: 2012-06-06
href: http://news.cnet.com/8301-1023_3-57448681-93/import-bans-over-patents-cause-substantial-harm-ftc-says/
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> (L'agence du commerce suggère de limiter l'utilisation du blocage à l'importation pour cause d'infraction à des brevets qui sont partie de standards) Trade agency suggests limiting the use of bans to block imports based on patents that are part of industry standards.
