---
site: les inrocks
title: "Adieu tracts et porte-à-porte: le Parti pirate tente la campagne tout Internet"
author: Gabriel Siméon
date: 2012-06-04
href: http://www.lesinrocks.com/2012/06/04/actualite/la-e-campagne-du-parti-pirate-11264471/
tags:
- Internet
- Institutions
- Vote électronique
---

> Twitter, Facebook, mails et blogs: sur Internet, le Parti pirate réinvente la façon de faire campagne. Questions d’argent, mais aussi de principe.
