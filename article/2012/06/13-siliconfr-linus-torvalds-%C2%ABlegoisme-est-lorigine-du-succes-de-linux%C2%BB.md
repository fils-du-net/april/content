---
site: Silicon.fr
title: "Linus Torvalds: «L'égoïsme est à l'origine du succès de Linux»"
author: Ariane Beky
date: 2012-06-13
href: http://www.silicon.fr/linus-torvalds-linux-75664.html
tags:
- Entreprise
- Internet
- Économie
- Innovation
- Licenses
---

> Lauréat du Millennium Technology Prize 2012, Linus Torvalds, informaticien américano-finlandais créateur du noyau Linux, estime que le succès du noyau libre est lié à un mélange d’égoïsme et de confiance.
