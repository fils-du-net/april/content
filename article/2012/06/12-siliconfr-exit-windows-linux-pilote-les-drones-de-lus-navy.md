---
site: Silicon.fr
title: "Exit Windows, Linux pilote les drones de l'US Navy"
author: Yves Grandmontagne
date: 2012-06-12
href: http://www.silicon.fr/linux-drones-us-navy-75560.html
tags:
- Logiciels privateurs
- Administration
- Licenses
- International
---

> La marine américaine a appris des mésaventures de l’US Air Force avec Windows et fait le choix d’équiper en Linux le contrôle de ses drones VTOL.
