---
site: Maghreb Emergent
title: "L’Open source au service de l’imagerie médicale en Algérie"
date: 2012-06-06
href: http://www.maghrebemergent.info/high-tech/83-it/12947-lopen-source-au-service-de-limagerie-medicale-en-algerie.html
tags:
- Internet
- Économie
- Éducation
- Innovation
- Sciences
- International
---

> Linda, Amira, et Kenza, respectivement Professeur d’électronique à l’USTHB et deux de ses étudiantes en doctorat et en PFE de Master 2, travaillent sur des projets d’utilisation et de développement de logiciels libres destinés à l’imagerie médicale. Rencontrées à la GNU/Linux Install Party 4, elles nous font part de leur expérience dans l’usage de ces logiciels Open source et de leur souhait de création d’une entité multidisciplinaire pour doper la recherche scientifique.
