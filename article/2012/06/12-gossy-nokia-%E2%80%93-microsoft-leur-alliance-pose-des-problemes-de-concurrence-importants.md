---
site: Gossy
title: "Nokia – Microsoft: Leur alliance pose des problèmes de concurrence importants!"
author: GoMarous
date: 2012-06-12
href: http://www.gossy.fr/nokia-microsoft-leur-alliance-pose-des-problemes-de-concurrence-importants-art39065.html
tags:
- Entreprise
- Économie
- Institutions
- Brevets logiciels
- Innovation
---

> Lorsque les concurrents d’un marché forment une alliance, les consommateurs ont du soucis à se faire. En effet, ces alliances ont souvent de nombreuses conséquences pour les consommateurs, notamment au niveau du prix. On se souvient tous de l’entente illicite de nos opérateurs mobiles qui avait eu pour effet un alignement des prix, des prix élevés, au détriment des consommateurs. Alors, doit-on s’inquiéter de l’alliance entre Nokia et Microsoft? Quelles sont les raisons de cette alliance?
