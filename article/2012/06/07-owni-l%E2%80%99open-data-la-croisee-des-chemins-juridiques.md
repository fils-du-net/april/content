---
site: OWNI
title: "L’Open Data à la croisée des chemins juridiques"
author: Lionel Maurel (Calimaq)
date: 2012-06-07
href: http://owni.fr/2012/06/07/l%E2%80%99open-data-a-la-croisee-des-chemins-juridiques/
tags:
- Internet
- Administration
- Institutions
- Droit d'auteur
- Licenses
- Europe
- Open Data
---

> La libération des données publiques franchit une étape importante de sa courte existence avec l'avis rendu cette semaine par le Conseil national du numérique. Coincé entre bonne volonté apparente et lacunes flagrantes, l'Open Data à la française semble tergiverser entre le juridique et le politique pour tracer son avenir.
