---
site: LeMagIT
title: "L’Open Source à l’avant-garde d’une nouvelle révolution économique"
author: Valery Marchive
date: 2012-06-27
href: http://www.lemagit.fr/article/economie-opensource-red-hat/11368/1/l-open-source-avant-garde-une-nouvelle-revolution-economique/
tags:
- Entreprise
- Brevets logiciels
- Innovation
- Informatique en nuage
---

> Pour Jim Whitehurst, Pdg de Red Hat, l’Open Source est à l’origine d’une révolution d’une ampleur comparable à celle de l’industrialisation et de la standardisation... de la production de boulons, au début du XIXe siècle. Une vision que ne renierait pas Paul Cormier, le vice-président exécutif de l’éditeur en charge de l’ingénierie.
