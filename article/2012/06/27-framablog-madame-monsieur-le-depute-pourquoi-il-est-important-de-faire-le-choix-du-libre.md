---
site: Framablog
title: "Madame, Monsieur le Député, pourquoi il est important de faire le choix du Libre"
author: François Revol
date: 2012-06-27
href: http://www.framablog.org/index.php/post/2012/06/27/assemblee-nationale-logiciel-libre
tags:
- Entreprise
- Économie
- Interopérabilité
- April
- HADOPI
- Vente liée
- DADVSI
- DRM
- Licenses
- Standards
---

> Signal fort et beau symbole, en 2007 il avait été décidé de passer les postes des députés sous GNU/Linux Ubuntu et OpenOffice.org (cf ces témoignages). Arrive aujourd’hui le temps du renouvellement et les députés, fraîchement élu(e)s ou réélu(e)s, ont le choix du choix, avec Windows ou Ubuntu et Microsoft Office ou LibreOffice.
