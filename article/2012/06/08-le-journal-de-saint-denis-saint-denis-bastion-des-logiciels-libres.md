---
site: "Le Journal de Saint-Denis"
title: "Saint-Denis bastion des logiciels libres?"
author: Marylène Lenfant
date: 2012-06-08
href: http://www.lejsd.com/index.php?r=13162
tags:
- Entreprise
- April
- Institutions
- Vente liée
- Associations
---

> Samsung Electronics France, dont le siège se trouve dans le quartier de la Plaine, a été condamné par le tribunal d’instance de Saint-Denis pour «vente forcée» sur plainte d’un particulier.
