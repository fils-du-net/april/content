---
site: BUG BROTHER
title: "Je n’ai pas le droit de lire le livre que j’ai acheté"
author: jean marc manach
date: 2012-06-10
href: http://bugbrother.blog.lemonde.fr/2012/06/10/je-nai-pas-le-droit-de-lire-le-livre-que-jai-achete/
tags:
- Entreprise
- Internet
- April
- HADOPI
- Institutions
- Associations
- DRM
- Droit d'auteur
- ACTA
---

> Quand on achète un livre papier, on peut le lire à l'oeil nu, ou avec des lentilles, des lunettes, et ce quelle qu'en soit la marque.
