---
site: L'USINE NOUVELLE
title: "\"Le Ford Silicon Valley Lab sera focalisé sur l’open source\""
author: Emmanuelle Delsol
date: 2012-06-12
href: http://www.usinenouvelle.com/article/le-ford-silicon-valley-lab-sera-focalise-sur-l-open-source.N176415
tags:
- Entreprise
- Internet
- Innovation
- International
---

> Ford va inaugurer mi-juin  son tout nouveau Ford Silicon Valley Lab, en Californie. Son patron, TJ Giuli, dévoile en exclusivité à L’Usine Nouvelle la stratégie numérique du constructeur automobile. Cheveux longs, barbichette et chemise à fleurs, ce diplômé de la toute voisine et prestigieuse université de Stanford, fan de Steve Jobs et adepte de voitures twitteuses, va diriger un laboratoire très particulier, voué à l’open source. Entretien dans un coffee shop de Palo Alto, devant un latte et un cheesecake.
