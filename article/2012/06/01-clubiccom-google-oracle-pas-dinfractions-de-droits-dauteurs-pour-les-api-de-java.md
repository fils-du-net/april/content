---
site: clubic.com
title: "Google - Oracle: pas d'infractions de droits d'auteurs pour les API de Java"
author: Guillaume Belfiore
date: 2012-06-01
href: http://www.clubic.com/os-mobile/android/actualite-494086-google-oracle-infraction-droits-auteurs-api-java.html
tags:
- Entreprise
- Institutions
- Droit d'auteur
- International
---

> La cour chargée de l'affaire Google Vs. Oracle vient de donner une nouvelle victoire à la firme de Mountain View.
