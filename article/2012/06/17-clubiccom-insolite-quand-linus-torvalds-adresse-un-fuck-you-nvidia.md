---
site: clubic.com
title: "Insolite: quand Linus Torvalds adresse un \"fuck you !\" à Nvidia"
author: Alexandre Laurent
date: 2012-06-17
href: http://www.clubic.com/carte-graphique/actualite-497036-insolite-linus-torvald-adresse-fuck-you-nvidia.html
tags:
- Entreprise
- Innovation
- Video
---

> Linus Torvalds n'a pas sa langue dans sa poche, et il suffit de l'emmener sur le terrain de la prise en charge de Linux par les fabricants de matériel pour que sa verve s'exprime non sans saveur. En témoigne cet élégant «Nvidia, fuck you!», prononcé lors d'une rencontre avec des étudiants finlandais.
