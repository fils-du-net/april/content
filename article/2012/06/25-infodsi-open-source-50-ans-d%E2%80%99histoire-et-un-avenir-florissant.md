---
site: infoDSI
title: "Open Source: 50 ans d’histoire et un avenir florissant"
author: Bertrand Diard
date: 2012-06-25
href: http://www.infodsi.com/articles/133648/open-source-50-ans-histoire-avenir-florissant-bertrand-diard-pdg-co-fondateur-talend.html
tags:
- Entreprise
- Économie
- Innovation
---

> A l’heure où le salon « Solutions Linux » souffle sa quatorzième bougie, il nous semble utile de nous attarder un moment sur l’évolution de notre marché et surtout de l’adoption de nos technologies qui sont désormais au cœur des infrastructures informatiques des entreprises.
