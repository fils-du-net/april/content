---
site: Numerama
title: "90 candidats au 2e tour des législatives ont signé le pacte du logiciel libre"
author: Julien L.
date: 2012-06-16
href: http://www.numerama.com/magazine/22866-90-candidats-au-2e-tour-des-legislatives-ont-signe-le-pacte-du-logiciel-libre.html
tags:
- April
- Institutions
- Sensibilisation
---

> Pour l'entre-deux-tours, l'April poursuit sa campagne de sensibilisation. Sur les 433 signataires du pacte du logiciel, seuls 62 sont parvenus à se hisser au second tour des élections législatives. Mais l'association rappelle que de nombreux candidats n'ont pas encore été contactés.
