---
site: Le nouvel Economiste
title: "Logiciels libres Open source"
author: Jean-Marie Benoist
date: 2012-06-14
href: http://www.lenouveleconomiste.fr/lesdossiers/open-source-15238/
tags:
- Entreprise
- Internet
- Administration
- Économie
- Interopérabilité
- Institutions
- Associations
- Brevets logiciels
- Informatique en nuage
- ACTA
---

> Liberté et gratuité, avantages pour les grandes entreprises, points faibles pour les PME
