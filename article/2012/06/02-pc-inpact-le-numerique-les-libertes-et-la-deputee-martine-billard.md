---
site: PC INpact
title: "Le numérique, les libertés et la députée Martine Billard"
author: Marc Rees
date: 2012-06-02
href: http://www.pcinpact.com/news/71362-martine-billard-libertes-numerique-paris.htm
tags:
- Internet
- April
- HADOPI
- Institutions
- Associations
- DADVSI
- DRM
- Neutralité du Net
- ACTA
---

> Ceux qui n’ont suivi que de manière lointaine les débats DADVSI ou Hadopi 1 et 2 ignorent sans doute son engagement. Pas de problème. La députée sortante Martine Billard (FDG, 5e circonscription. Paris) nous donne rendez-vous pour une réunion publique ce mardi 5 juin à Paris (*). Le thème? Le numérique et les libertés. Pour nourrir les échanges, Jérémie Nestel (Libre Accès), Benoit Sibaud (April, LinuxFr.org, etc.) et Benjamin Bayart (FDN)
