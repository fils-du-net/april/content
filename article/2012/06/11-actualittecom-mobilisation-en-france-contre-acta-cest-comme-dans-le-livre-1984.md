---
site: ActuaLitté.com
title: "Mobilisation en France contre ACTA: 'C'est comme dans le livre 1984'"
author: Victor de Sepausy
date: 2012-06-11
href: http://www.actualitte.com/actualite/lecture-numerique/legislation/mobilisation-en-france-contre-acta-c-est-comme-dans-le-livre-1984-34646.htm
tags:
- Internet
- April
- Institutions
- RGI
- ACTA
---

> Les manifestants répondaient présents
