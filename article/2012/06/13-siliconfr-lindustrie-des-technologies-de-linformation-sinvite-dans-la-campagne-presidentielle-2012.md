---
site: Silicon.fr
title: "L'industrie des technologies de l'information s'invite dans la campagne présidentielle 2012"
author: Ariane Beky
date: 2012-06-13
href: http://www.silicon.fr/lindustrie-des-technologies-de-linformation-sinvite-dans-la-campagne-presidentielle-2012-72762.html
tags:
- Entreprise
- Économie
- April
- Institutions
- Associations
---

> À la suite du Syntec Numérique et de l’AFDEL, le Syndicat de l’industrie des technologies de l’information (SFIB) s’apprête à présenter aux candidats à l’élection présidentielle ses propositions en faveur de l’innovation numérique.
