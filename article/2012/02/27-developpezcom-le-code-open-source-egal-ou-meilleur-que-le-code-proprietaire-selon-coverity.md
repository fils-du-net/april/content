---
site: Developpez.com
title: "Le code open source égal ou meilleur que le code propriétaire, selon Coverity"
author: Hinault Romaric
date: 2012-02-27
href: http://www.developpez.com/actu/41634/Le-code-open-source-egal-ou-meilleur-que-le-code-proprietaire-selon-Coverity/
tags:
- Entreprise
- Internet
- RGI
- Informatique en nuage
- Europe
---

> Coverity, l’éditeur de solutions pour l’analyse statique du code source dans son dernier rapport révèle que le code source des applications open source est au même niveau, voire meilleur que le code des applications propriétaires.
