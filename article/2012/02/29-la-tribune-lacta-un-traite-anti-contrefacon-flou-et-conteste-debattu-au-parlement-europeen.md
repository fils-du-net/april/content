---
site: LA TRIBUNE
title: "L'Acta, un traité anti-contrefaçon flou et contesté débattu au parlement européen"
author: Sandrine Cassini
date: 2012-02-29
href: http://www.latribune.fr/technos-medias/internet/20120228trib000685472/l-acta-un-traite-anti-contrefacon-flou-et-conteste-debattu-au-parlement-europeen.html
tags:
- Entreprise
- Internet
- April
- HADOPI
- Institutions
- Associations
- DRM
- Innovation
- Europe
- ACTA
---

> Cette semaine, le parlement européen débat de l'ACTA, le traité de lutte contre la contrefaçon des marchandises et des fichiers numériques. Des milliers de personnes ont défilé en Europe ce week-end s'insurgeant contre un texte jugé liberticide. L'Union européenne, mais aussi les détenteurs de droits assurent le contraire. Des différences d'interprétations qui s'expliquent par l'imprécision du texte.
