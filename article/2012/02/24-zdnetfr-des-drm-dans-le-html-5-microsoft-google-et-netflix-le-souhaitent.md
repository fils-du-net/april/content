---
site: ZDNet.fr
title: "Des DRM dans le HTML 5? Microsoft, Google et Netflix le souhaitent"
date: 2012-02-24
href: http://www.zdnet.fr/actualites/des-drm-dans-le-html-5-microsoft-google-et-netflix-le-souhaitent-39768944.htm
tags:
- Entreprise
- Internet
- DRM
- Standards
- Video
---

> Pas une technologie de DRM, mais une solution pour la diffusion de contenus protégés pour le HTML 5, assurent des représentants de Microsoft, Netflix et Google. Problème, une telle technologie ne serait pas compatible avec les logiciels libres. Ian Hickson de Google juge même non éthique cette proposition. Le débat est ouvert.
