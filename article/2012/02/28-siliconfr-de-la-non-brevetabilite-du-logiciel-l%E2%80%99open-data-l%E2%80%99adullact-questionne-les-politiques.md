---
site: Silicon.fr
title: "De la non-brevetabilité du logiciel à l’open data, l’ADULLACT questionne les politiques"
author: Ariane Beky
date: 2012-02-28
href: http://www.silicon.fr/de-la-non-brevetabilite-du-logiciel-a-lopen-data-ladullact-questionne-les-politiques-72248.html
tags:
- Entreprise
- Internet
- Administration
- Économie
- Interopérabilité
- April
- HADOPI
- Vente liée
- Brevets logiciels
- Éducation
- Innovation
- Promotion
- Standards
- Contenus libres
- Open Data
---

> L’Association des développeurs et des utilisateurs de logiciels libres pour l’administration et les collectivités territoriales (ADULLACT) invite les candidats à l’élection présidentielle 2012, à préciser leur positionnement sur les œuvres ‘ouvertes’.
