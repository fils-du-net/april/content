---
site: ITespresso.fr
title: "Brevets: la Commission européenne veut cerner les pratiques de Samsung"
author: Clément Bohic 
date: 2012-02-01
href: http://www.itespresso.fr/brevets-la-commission-europeenne-veut-cerner-les-pratiques-de-samsung-50678.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Europe
---

> Bruxelles ouvre une enquête sur le fabricant high-tech coréen. Des soupçons d’abus sur les droits d’exploitation des brevets titillent l’organe exécutif européen. Illustration dans la guerre avec Apple.
