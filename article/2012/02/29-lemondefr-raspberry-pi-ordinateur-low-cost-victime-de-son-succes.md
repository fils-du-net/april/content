---
site: LeMonde.fr
title: "Raspberry Pi, ordinateur low cost, victime de son succès"
author: Laurent Checola et Guénaël Pépin
date: 2012-02-29
href: http://www.lemonde.fr/technologies/article/2012/02/29/raspberry-pi-ordinateur-low-cost-victime-de-son-succes_1649838_651865.html
tags:
- Matériel libre
- Associations
- Éducation
- Innovation
---

> Il y avait la "firme à la pomme", il faudra désormais compter sur la "marque à la framboise"… Le Raspberry Pi, un ordinateur low cost, vendu entre 25 et 35 dollars (19 et 26 euros) a été lancé mercredi 29 février. Pour l'heure, seul le modèle B (à 35 dollars) a été commercialisé. Quelques heures seulement après son lancement, l'appareil est déjà victime de son succès. Les sites de la fondation Raspberry Pi, mais également de deux revendeurs britanniques, demeurent inaccessibles.
