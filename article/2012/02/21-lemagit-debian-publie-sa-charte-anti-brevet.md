---
site: LeMagIT
title: "Debian publie sa charte anti-brevet"
author: Cyrille Chausson
date: 2012-02-21
href: http://www.lemagit.fr/article/linux-debian/10513/1/debian-publie-charte-anti-brevet/
tags:
- Associations
- Brevets logiciels
- Licenses
---

> Le 19 février, la communauté Debian, qui encadre le projet d’OS Linux, a publié une charte sur les brevets dont l’objectif est de clarifier sa position en matière des très controversés brevets logiciels. Une initiative qui s’inscrit comme une évolution logique à la publication d’une FAQ sur les brevets à l'intention des distributions communautaires, publiée en juillet 2011 qui devait décrypter la notion de brevets et de contrefaçon notamment.
