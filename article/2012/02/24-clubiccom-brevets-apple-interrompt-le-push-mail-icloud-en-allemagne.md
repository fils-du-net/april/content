---
site: clubic.com
title: "Brevets: Apple interrompt le \"push mail\" iCloud en Allemagne"
author: Alexandre Laurent
date: 2012-02-24
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/actualite-478456-brevets-apple-interrompt-push-mail-icloud-allemagne.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> La procédure lancée à son encontre par Motorola Mobility en Allemagne a contraint Apple à stopper temporairement son service de push mail dans le pays.
