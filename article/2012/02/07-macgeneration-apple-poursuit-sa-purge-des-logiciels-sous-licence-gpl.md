---
site: macgeneration
title: "Apple poursuit sa purge des logiciels sous licence GPL"
author: Anthony Nelzin
date: 2012-02-07
href: http://www.macgeneration.com/news/voir/233312/apple-poursuit-sa-purge-des-logiciels-sous-licence-gpl
tags:
- Entreprise
- Brevets logiciels
- Licenses
---

> OS X comporte de moins en moins de packages sous licence GPL: c'est le constat du développeur Matthew Murphy, qui rappelle que de 47 packages sous licence GPL dans Mac OS X 10.5, Apple est passé à 44 dans Mac OS X 10.6 et 29 seulement dans OS X 10.7. Des chiffres publics et publiés sur le site Apple Open Source, où la firme de Cupertino regroupe l'ensemble de ses ressources en rapport avec le monde du libre, son utilisation dans OS X et iOS et les contributions qu'elle effectue.
