---
site: ZDNet.fr
title: "Logiciels libres, informatique: les candidats à l'élection présidentielle interpellés"
author: Thierry Noisette
date: 2012-02-29
href: http://www.zdnet.fr/blogs/l-esprit-libre/logiciels-libres-informatique-les-candidats-a-l-election-presidentielle-interpelles-39769145.htm
tags:
- Administration
- April
- HADOPI
- Institutions
- Associations
- Brevets logiciels
---

> L'April et l'Adullact s'adressent aux candidats à la présidentielle. En 2007, plusieurs des candidats leur avaient déjà répondu, dont Nicolas Sarkozy.
