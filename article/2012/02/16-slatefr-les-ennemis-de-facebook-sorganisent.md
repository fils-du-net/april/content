---
site: Slate.fr
title: "Les ennemis de Facebook s'organisent"
date: 2012-02-16
href: http://www.slate.fr/lien/50157/HIGH-TECH-ennemis-facebook-Zuckerberg
tags:
- Entreprise
- Internet
- Institutions
---

> Eben Moglen est professeur à la Columbia Law School, la faculté de droit de la prestigieuse université new-yorkaise. Et il est un ennemi déclaré de Facebook comme le souligne Village Voice. Ses propos à l’égard de Mark Zuckerberg sont même extrêmement véhéments.
