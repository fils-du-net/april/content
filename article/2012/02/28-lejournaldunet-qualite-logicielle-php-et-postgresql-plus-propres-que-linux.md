---
site: LeJournalduNet
title: "Qualité logicielle: PHP et PostgreSQL plus propres que Linux"
author: Antoine CROCHET-DAMAIS
date: 2012-02-28
href: http://www.journaldunet.com/developpeur/algo-methodes/indice-coverity-sur-la-qualite-des-codes-open-source.shtml
tags:
- Entreprise
- Internet
- Logiciels privateurs
---

> Le spécialiste américain du test logiciel Coverity a publié l'édition 2011 de son indice sur la qualité des codes Open Source. Le palmarès révèle d'importantes disparités.
