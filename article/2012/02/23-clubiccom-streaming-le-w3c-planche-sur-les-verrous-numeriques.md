---
site: clubic.com
title: "Streaming: le W3C planche sur les verrous numériques"
author: Guillaume Belfiore
date: 2012-02-23
href: http://www.clubic.com/internet/google/actualite-477622-streaming-google-microsoft-souhaite-standardiser-lecture-video-protegees.html
tags:
- Entreprise
- Internet
- Standards
- Video
---

> Google, Microsoft, mais également Netflix ont proposé au W3C un nouveau standard permettant la lecture en streaming de contenus verrouillés par des DRM directement au sein du navigateur.
