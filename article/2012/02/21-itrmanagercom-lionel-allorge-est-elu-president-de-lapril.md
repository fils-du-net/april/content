---
site: ITRmanager.com
title: "Lionel Allorge est élu président de l'April"
date: 2012-02-21
href: http://www.itrmanager.com/articles/129295/lionel-allorge-est-elu-president-april.html
tags:
- Internet
- April
- Institutions
- DRM
- ACTA
---

> L'April annonce l'élection de son nouveau conseil d'administration qui sera cette année présidé par Lionel Allorge. Administrateur depuis 2006, ancien secrétaire et vice-président, Lionel Allorge a rejoint l'April en 2000. Il découvre le logiciel libre par les textes et les conférences de Richard Stallman, fondateur de la Free Software Foundation. Il donne régulièrement des conférences sur le logiciel libre et co-anime une formation pour les futurs conférenciers du libre. Il fait également le lien entre le conseil d'administration et le groupe de travail Sensibilisation.
