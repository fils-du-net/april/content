---
site: PC INpact
title: "Le brevet unitaire européen et le logiciel"
author: Marc Rees
date: 2012-02-18
href: http://www.pcinpact.com/dossier/brevet-unitaire-europeen-logiciel-/205-1.htm
tags:
- Entreprise
- Internet
- Administration
- April
- Institutions
- Brevets logiciels
- Innovation
- Europe
---

> PC INpact a interviewé Gérald Sédrati-Dinet sur le brevet unitaire européen sur le logiciel, ses origines, ses dangers et son avenir. Il est le conseil bénévole sur les brevets pour l'April, l’association pour la promotion et la défense du logiciel libre. Une interview d’une dizaine de pages qui a pour objectif de dessiner un état des lieux de ce sujet épineux, aux lourdes conséquences pour le devenir de l’informatique en Europe.
