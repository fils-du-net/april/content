---
site: LE VIF.be
title: "Les attaques des Anonymous décryptées"
author: ETTORE RIZZA
date: 2012-02-06
href: http://www.levif.be/info/actualite/technologie/les-attaques-des-anonymous-decryptees/article-4000039944139.htm
tags:
- Internet
- Institutions
- Droit d'auteur
---

> Loin d'être tous pros de l'informatique, les cyberactivistes modernes manient surtout une palette d'outils d'une simplicité enfantine. Et d'une efficacité ravageuse.
