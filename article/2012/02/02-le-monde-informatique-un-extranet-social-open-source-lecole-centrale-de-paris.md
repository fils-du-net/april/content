---
site: Le Monde Informatique
title: "Un extranet social Open Source à L'Ecole Centrale de Paris"
author: Bertrand Lemaire
date: 2012-02-02
href: http://www.lemondeinformatique.fr/actualites/lire-un-extranet-social-open-source-a-l-ecole-centrale-de-paris-47630.html
tags:
- Entreprise
- Internet
- Administration
- Éducation
---

> L'école d'ingénieurs a choisi la solution de Nuxeo Platform 5.5 sous Debian Linux pour mettre en oeuvre un extranet social.
