---
site: Les Echos
title: "Nouvelles manifestations en Europe contre Acta, Actualités"
date: 2012-02-25
href: http://www.lesechos.fr/entreprises-secteurs/tech-medias/actu/0201918436984-nouvelles-manifestations-en-europe-contre-acta-294560.php
tags:
- Internet
- April
- Institutions
- Europe
- ACTA
---

> Des milliers de manifestants, nombreux à porter le masque des « Anonymous », ont de nouveau défilé samedi en Europe pour dénoncer les atteintes aux libertés sur internet, en particulier l'accord européen Acta sur la contrefaçon.
