---
site: écrans
title: "La grande discorde du passage à l'ACTA"
author: Romain Gaillard
date: 2012-02-03
href: http://www.ecrans.fr/La-grande-discorde-du-passage-a-l,14005.html
tags:
- Internet
- Économie
- HADOPI
- Institutions
- Associations
- Europe
- International
- ACTA
---

> L’accord international de lutte contre la contrefaçon, l’ACTA, a été signé jeudi 26 janvier à Tokyo par les représentants de 22 pays européens ainsi que par ceux de l’Union européenne et de douze autres pays. Ce texte négocié dans le plus grand secret depuis plus de 5 ans devrait être soumis au vote du Parlement européen en juin, pour une ratification qui devrait donner lieu à quelques débats enflammés.
