---
site: LeMagIT
title: "Spécial sécurité - Projet de loi: Sopa à satiété "
date: 2012-02-02
href: http://www.lemagit.fr/article/securite-hack/10387/1/special-securite-projet-loi-sopa-satiete/
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Informatique en nuage
---

> Aujourd'hui, nos confrères de CNIS Mag, magazine spécialisé dans la sécurité des systèmes d'information, reviennent sur cinq actualités qui ont marqué la fin du moins de janvier: le projet de loi américain Sopa bien sûr, qui a provoqué l’une des plus importantes levées de boucliers des ténors d’Internet, mais également l’affaire MegaUpload qui a entrainé une réaction épidermiques des célèbres «Anonymous». Ils s’arrêtent également sur l’outil de phishing Open Source STP et pointent du doigt la montée en puissance des malwares pour la plate-forme Mac.
