---
site: Génération nouvelles technologies
title: "La Quadrature du Net, April, Framasoft: un Pack Liberté"
author: Jérôme G.
date: 2012-02-06
href: http://www.generation-nt.com/quadrature-net-april-framasoft-pack-liberte-campagne-dons-actualite-1537381.html
tags:
- Internet
- April
- HADOPI
- Institutions
- Associations
- ACTA
---

> La Quadrature du Net, l'April et Framasoft font campagne commune pour récolter des dons.
