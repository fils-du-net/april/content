---
site: clubic.com
title: "Vente liée: au tour de Lenovo d'être condamné"
author: Alexandre Laurent
date: 2012-02-06
href: http://www.clubic.com/windows-os/actualite-474636-vente-liee-tour-lenovo-condamne.html
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
---

> Le chinois Lenovo a été condamné le 9 janvier dernier pour vente liée d'un ordinateur et de logiciels. Il devra finalement verser près de 2 000 euros au plaignant, quatre ans après le début d'une longue procédure.
