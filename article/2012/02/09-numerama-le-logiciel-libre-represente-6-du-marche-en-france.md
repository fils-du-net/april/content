---
site: Numerama
title: "Le logiciel libre représente 6% du marché en France"
author: Julien L.
date: 2012-02-09
href: http://www.numerama.com/magazine/21592-le-logiciel-libre-represente-6-du-marche-en-france.html
tags:
- Entreprise
- Internet
- Économie
- Vente liée
---

> Le logiciel libre continue de faire son trou dans l'Hexagone. La dernière enquête dirigée par le cabinet Pierre Audoin Consultants montre ainsi que ce secteur représente désormais 6% du marché en France. D'ici trois ans, ce chiffre devrait frôler la barre des 10%.
