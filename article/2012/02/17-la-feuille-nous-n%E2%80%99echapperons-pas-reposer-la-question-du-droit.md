---
site: La feuille
title: "Nous n’échapperons pas à reposer la question du droit"
author: Hubert Guillaud
date: 2012-02-17
href: http://lafeuille.blog.lemonde.fr/2012/02/17/nous-nechapperons-pas-a-reposer-la-question-du-droit/
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Droit d'auteur
- Éducation
- ACTA
---

> L'écrivain François Bon, l'éditeur Publie.net et ses diffuseurs ont reçu ce jour un courrier des éditions Gallimard leur demandant le retrait de la nouvelle traduction du "Vieil homme et la mer" que venait de publier François Bon sur Publie.net (voir le billet de François Bon sur son site). Gallimard rappelle dans ce courrier qu'il dispose des droits d'édition (y compris numériques) pour toute édition française de l'ouvrage (pas seulement celle de Jean Dutourd réalisée en 1954 qu'exploite la maison de la feu rue Sébastien Bottin) : cette traduction non autorisée constitue donc une contrefaçon.
