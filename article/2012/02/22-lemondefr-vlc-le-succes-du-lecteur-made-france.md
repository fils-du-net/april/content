---
site: LeMonde.fr
title: "VLC, le succès du lecteur \"made in France\""
author: Delphine Roucaute
date: 2012-02-22
href: http://www.lemonde.fr/technologies/article/2012/02/22/pour-le-lecteur-vlc-cree-il-y-a-10-ans-a-paris-la-saga-continue_1646434_651865.html
tags:
- Entreprise
- Internet
- Éducation
- Innovation
- Licenses
---

> Après avoir fêté les dix ans de sa licence en 2011, l'association VideoLAN a lancé, samedi 18 février, la version 2.0 de son lecteur multimédia VLC (pour VideoLAN Client). Une nouvelle version attendue depuis longtemps par tous les utilisateurs du fameux logiciel au cône de chantier orange. Le symbole d'un "work in progress" qui s'est transformé en "success story": depuis sa création, VLC revendique 485 millions de téléchargements. Et tout cela, gratuitement.
