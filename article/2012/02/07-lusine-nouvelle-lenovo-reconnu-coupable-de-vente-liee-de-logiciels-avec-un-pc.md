---
site: L'USINE NOUVELLE
title: "Lenovo reconnu coupable de vente liée de logiciels avec un PC"
author: Christophe Guillemin
date: 2012-02-07
href: http://www.usinenouvelle.com/article/lenovo-reconnu-coupable-de-vente-liee-de-logiciels-avec-un-pc.N168201
tags:
- Entreprise
- Internet
- April
- Institutions
- Vente liée
- Associations
---

> Le constructeur informatique chinois a été condamné par la juridiction de proximité d'Aix en Provence. Plus d'une vingtaine de jugements similaires ont été rendus en France depuis 2006. Mais la situation n'évolue pas réellement, déplorent les défenseurs de logiciels libres, qui comptent interpeler les candidats à la présidentielle sur le sujet.
