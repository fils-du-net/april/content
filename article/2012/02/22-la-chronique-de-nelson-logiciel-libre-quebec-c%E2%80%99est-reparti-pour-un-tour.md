---
site: La chronique de Nelson
title: "Logiciel libre à Québec: c’est reparti pour un tour!"
author: Nelson
date: 2012-02-22
href: http://www.nelsondumais.com/2012/02/22/logiciel-libre-a-quebec-cest-raparti-pour-un-tour/
tags:
- Entreprise
- Administration
- Institutions
- Marchés publics
- RGI
- International
---

> Selon le quotidien montréalais Le Devoir, l’Assemblée nationale du Québec aurait lancé un appel d’offres visant à remplacer, par une version plus actuelle, quelque 1 200 licences du coffret bureautiques Microsoft Office. Or dans son esprit, la procédure éliminerait d’emblée les produits concurrents tels les gratuiciels OpenOffice et LibreOffice ou encore le coffret ontarien WordPerfect Office.
