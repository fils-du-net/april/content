---
site: ActuaLitté.com
title: "L'avenir open-source du manuel scolaire"
author: Antoine Oury
date: 2012-02-20
href: http://www.actualitte.com/actualite/patrimoine-education/ressources-pedagogiques/l-avenir-open-source-du-manuel-scolaire-32163.htm
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Droit d'auteur
- Éducation
- International
- Open Data
---

> Il pourrait réduire le poids du savoir
