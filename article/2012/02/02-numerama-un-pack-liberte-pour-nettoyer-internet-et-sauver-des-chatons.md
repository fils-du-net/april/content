---
site: Numerama
title: "Un Pack Liberté pour nettoyer Internet et sauver des chatons"
author: Julien L.
date: 2012-02-02
href: http://www.numerama.com/magazine/21503-un-pack-liberte-pour-nettoyer-internet-et-sauver-des-chatons.html
tags:
- Le Logiciel Libre
- Internet
- April
- HADOPI
- Associations
- Brevets logiciels
- DADVSI
- RGI
- Europe
- International
- ACTA
---

> La Quadrature du Net, l'April et Framasoft lancent une opération commune baptisée Pack Liberté. Les trois associations veulent sensibiliser un public plus large à travers une campagne aux teintes lessivières sur des sujets tels qu'Hadopi, Dadvsi ou encore le traité international Acta. Elles en profitent aussi pour appeler à la générosité des internautes, l'argent restant comme toujours le nerf de la guerre.
