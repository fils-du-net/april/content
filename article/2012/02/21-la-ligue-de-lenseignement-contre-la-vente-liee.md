---
site: la ligue de l'enseignement
title: "Contre la vente liée!"
author: Denis Lebioda
date: 2012-02-21
href: http://www.laligue-alpesdusud.org/associatifs_leblog/?2012/02/21/3179-contre-la-vente-liee
tags:
- Entreprise
- April
- Sensibilisation
- Vente liée
---

> Le groupe sensibilisation de l'April édite une affiche pour alerter le public sur la pratique abusive de la vente liée.
