---
site: AgoraVox
title: "La culture geek"
author: Marc Bruxman
date: 2012-02-02
href: http://www.agoravox.fr/culture-loisirs/culture/article/la-culture-geek-108884
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Droit d'auteur
- Innovation
---

> Quand les entreprises d'électronique et d'informatique se sont massivement installées en Californie pour y trouver un cadre de vie agréable, elles étaient loin de penser qu'elles allaient créer un phénoméne culturel majeur. En concentrant ingénieurs, scientifiques et technicien au sein d'une petite zone géographique, elles ont permis l'émergence d'une contre-culture qui fut longtemps confidentielle. Depuis l'arrivée d'Internet, cette culture a une influence de plus en plus grande sur le monde au point de ne pouvoir être ignorée.
