---
site: GreeIT.fr
title: "Après le logiciel libre, le matériel ouvert et libre"
author: Olivier Philippot
date: 2012-02-01
href: http://www.greenit.fr/article/materiel/apres-le-logiciel-libre-le-materiel-ouvert-et-libre-4191
tags:
- Entreprise
- Économie
- Matériel libre
---

> L’open hardware (ou matériel ouvert) en est un exemple concret. Mouvement comparable à celui du logiciel libre, l’open hardware chamboule le monde de la conception du matériel électronique. Là où les sociétés avaient l’habitude de garder secrets les plans électroniques, les programmes, les nomenclatures de pièces et tous les brevets, les défenseurs du matériel ouvert prônent le partage.
