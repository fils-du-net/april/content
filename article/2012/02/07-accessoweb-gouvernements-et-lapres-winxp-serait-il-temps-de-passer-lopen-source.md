---
site: accessoweb
title: "Serait-il temps de passer à l'Open Source?"
author: Théophile Nzungize
date: 2012-02-07
href: http://www.accessoweb.com/Gouvernements-et-l-apres-WinXP-Serait-il-temps-de-passer-a-l-Open-Source_a10430.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Institutions
- International
---

> Avec Microsoft qui devrait arrêter de supporter Windows XP, plusieurs gouvernements dans le monde devront bientôt fouiller dans leurs coffres et migrer vers d'autres OS pour la gestion de leurs plateformes. À cette occasion, on en vient à se demander s'il ne serait pas une bonne idée d'enfin faire confiance aux systèmes libres. Revue de plusieurs avantages que pourrait offrir cette alternative.
