---
site: FRANCE 24
title: "Contrefaçon sur internet: nouvelles manifestations en France contre Acta"
date: 2012-02-25
href: http://www.france24.com/fr/20120225-contrefacon-internet-nouvelles-manifestations-france-contre-acta
tags:
- Internet
- April
- ACTA
---

> AFP - Des centaines de manifestants, nombreux à porter le masque des "Anonymous", ont de nouveau défilé samedi à Paris et en région pour dénoncer les atteintes aux libertés sur internet, en particulier l'accord européen Acta sur la contrefaçon.
