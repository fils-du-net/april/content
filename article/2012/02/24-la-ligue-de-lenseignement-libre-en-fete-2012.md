---
site: la ligue de l'enseignement
title: "Libre en Fête 2012"
author: Denis Lebioda
date: 2012-02-24
href: http://www.laligue-alpesdusud.org/associatifs_leblog/?2012/02/24/3184-libre-en-fete-2012
tags:
- April
- Sensibilisation
- Associations
- Promotion
---

> Initiée et coordonnée par l'April, l'initiative Libre en Fête est relancée$abstractnbsp;: pour accompagner l'arrivée du printemps, des évènements de découverte des Logiciels Libres et du Libre en général seront proposés partout en France.
