---
site: LADEPECHE.fr
title: "Vic-en-Bigorre. La communication libre avec Rivages"
author: Josiane Pomès
date: 2012-02-14
href: http://www.ladepeche.fr/article/2012/02/14/1283820-vic-en-bigorre-la-communication-libre-avec-rivages.html
tags:
- Logiciels privateurs
- Sensibilisation
- Associations
- Éducation
---

> Le collectif Rivages en fait une profession de foi: «Nous sommes une association d'éducation populaire soucieuse de participer à la dynamique locale générale par la richesse de la vie associative. En mutualisant nos compétences, connaissances et certains de nos moyens, ensemble, nous pourrons développer la qualité de vie sur notre territoire». L'une des déclinaisons est l'offre de formations.
