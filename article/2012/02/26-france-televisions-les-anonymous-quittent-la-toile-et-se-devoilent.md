---
site: france télévisions
title: "Les Anonymous quittent la toile et se dévoilent"
date: 2012-02-26
href: http://www.francetv.fr/info/les-anonymous-quittent-la-toile-et-se-devoilent_66273.html
tags:
- Entreprise
- Internet
- Administration
- April
- Droit d'auteur
- Europe
- International
- ACTA
---

> Sourire ironique, fine moustache et teint blanc, le célèbre masque des Anonymous, inspiré du conspirateur anglais du XVIe siècle, Guy Fawkes, a défilé samedi 25 février dans plusieurs villes de France. Le collectif s'est mobilisé dans toute l'Europe contre l'Accord multilatéral sur la contrefaçon (ACTA). Contesté par nombre d'internautes, il constitue "une atteinte aux libertés", clament les Anonymous. A Paris, 650 personnes s'étaient mobilisées selon la police. Petit portrait en trois slogans.
