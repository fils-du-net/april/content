---
site: Silicon.fr
title: "L’Open Source Initiative se rapproche de la communauté"
author: David Feugey
date: 2012-02-15
href: http://www.silicon.fr/lopen-source-initiative-se-rapproche-de-la-communaute-71828.html
tags:
- Entreprise
- Internet
- Associations
- Europe
---

> L’OSI revoit son organisation en profondeur. L’entité sera dorénavant pilotée par un conseil d’administration et des organisations tierces. Les particuliers pourront également adhérer à l’OSI.
