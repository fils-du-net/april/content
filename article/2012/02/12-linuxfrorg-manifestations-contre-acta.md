---
site: linuxfr.org
title: "Manifestations contre ACTA"
author: Benoît Sibaud
date: 2012-02-12
href: http://linuxfr.org/news/manifestations-contre-acta
tags:
- Internet
- April
- HADOPI
- DADVSI
- International
- ACTA
---

> De nombreuses manifestations ont eu lieu partout dans le monde aujourd'hui pour protester contre ACTA. Le site ActuaLutte décomptait 390000 personnes dans 25 pays et 228 villes. Il s'agit de personnes annonçant sur Facebook leur intention de manifester, reste à voir combien concrétisent ensuite. Il y a déjà eu une série de manifestations le 28 janvier, et d'autres sont prévues le 25 février.
