---
site: estrepublicain.fr
title: "Les Anonymous remontent au front"
author: Xavier FRERE
date: 2012-02-11
href: http://www.estrepublicain.fr/actualite/2012/02/11/les-anonymous-remontent-au-front
tags:
- Internet
- Institutions
- ACTA
---

> « C’EST L’UNE DE NOS DERNIÈRES CHANCES de nous faire entendre ». Son pseudo est Heimgus Atar, et il se décrit comme « un simple citoyen qui a décidé de s’impliquer ». Il est l’un des Anonymous derrière l’organisation du rassemblement à Nancy qui, comme partout dans le monde, appelle aujourd’hui à « lutter pour le droit à la liberté d’expression sur internet et en dehors » et vise particulièrement le traité ACTA (accord commercial anti-contrefaçon).
