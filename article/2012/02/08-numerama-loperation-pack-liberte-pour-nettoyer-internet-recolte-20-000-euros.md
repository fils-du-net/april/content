---
site: Numerama
title: "L'opération \"Pack Liberté\" pour nettoyer Internet récolte 20 000 euros"
author: Julien L.
date: 2012-02-08
href: http://www.numerama.com/magazine/21580-l-operation-34pack-liberte34-pour-nettoyer-internet-recolte-20-000-euros.html
tags:
- Internet
- April
- HADOPI
- Associations
- Brevets logiciels
- DADVSI
- ACTA
---

> Une semaine après le lancement de l'opération "Pack Liberté", les premiers chiffres se font connaître. Pas moins de 20 000 euros ont été collectés grâce à la participation de 500 donateurs. Les montants, qui seront répartis entre les trois associations à l'origine de cette campagne de dons, ne sont toutefois pas encore suffisants pour couvrir leurs besoins.
