---
site: Marianne 2
title: "Alerte à l'attaque généralisée sur les libertés et autres principes démocratiques"
author: Eugène
date: 2012-02-14
href: http://www.marianne2.fr/Alerte-a-l-attaque-generalisee-sur-les-libertes-et-autres-principes-democratiques_a215504.html
tags:
- Entreprise
- Internet
- HADOPI
- DRM
- Droit d'auteur
- Innovation
- International
- ACTA
---

> Si les révolutions arabes pouvaient donner l'impression d'un vent de liberté dans les bastions les plus fermés des dictatures orientales, les dernières nouvelles du monde ne sont pas bonnes du tout pour les amateurs de libertés. Démonstration des changements discrets et généralisés que nous subissons, et qui pourraient sonner le glas, pour le blog économique et social, de la démocratie en Occident.
