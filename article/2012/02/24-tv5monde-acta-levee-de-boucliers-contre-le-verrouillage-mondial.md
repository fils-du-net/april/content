---
site: TV5MONDE
title: "ACTA: levée de boucliers contre le verrouillage mondial"
author: Pascal Hérard
date: 2012-02-24
href: http://www.tv5.org/cms/chaine-francophone/info/Les-dossiers-de-la-redaction/ACTA/p-20449-ACTA-levee-de-boucliers-contre-le-verrouillage-mondial.htm
tags:
- Entreprise
- Internet
- Institutions
- Europe
- International
- ACTA
---

> Face à l'opposition grandissante des citoyens européens à l'encontre des accords anti-contrefaçon (Anti-Counterfeiting Trade Agreement, ACTA en anglais) signés le 26 janvier 2012 au Japon par le Comité exécutif de l'UE, la commission européenne a décidé de demander l'avis de la Cour de Justice de l'Union Européenne (CJUE) sur la conformité d'ACTA avec les libertés fondamentales. L'enjeu est important puisque l'accord commercial anti-contrefaçon, s'il est adopté, risque de changer beaucoup de choses pour les citoyens des pays signataires.
