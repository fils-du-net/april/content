---
site: LE FIGARO.fr
title: "ACTA: des milliers d'Européens défilent"
date: 2012-02-11
href: http://www.lefigaro.fr/flash-actu/2012/02/11/97001-20120211FILWWW00443-acta-des-milliers-d-europeens-defilent.php
tags:
- Internet
- Institutions
- International
- ACTA
---

> Des dizaines de milliers de personnes ont manifesté samedi dans toute l'Europe, principalement en Allemagne, pour dénoncer les atteintes à l'internet contenues selon eux dans l'accord européen ACTA sur la contrefaçon. La plupart du temps jeunes, portant parfois le masque blanc et noir au sourire sarcastique de Guy Fawkes - activiste britannique du 17ème siècle - devenu l'emblème des cyberactivistes, les anti-ACTA ont défilé dans le calme dans les grandes villes européennes.
