---
site: L'INFORMATICIEN
title: "France: Lenovo condamné pour vente liée"
author: Orianne Vatin
date: 2012-02-06
href: http://www.linformaticien.com/actualites/id/23461/france-lenovo-condamne-pour-vente-liee.aspx
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
- Licenses
---

> Le constructeur informatique Lenovo vient d'être lourdement condamné pour vente liée d'ordinateurs et de logiciels par la juridiction de proximité d'Aix-en-Provence. Une condamnation qui met à mal "le symbole" de la vente liée, selon l'AFUL (Association Francophone des Utilisateurs de Logiciels Libres), qui s'en réjouit.
