---
site: tom's hardware
title: "Sony veut réécrire de l'open source pour éviter la licence"
author: Pierre Dandumont
date: 2012-02-03
href: http://www.presence-pc.com/actualite/sony-open-source-licence-46514/
tags:
- Entreprise
- Associations
- Licenses
---

> Il y a quelques jours, un ingénieur de chez Sony a fait une proposition étrange: réécrire le programme BusyBox sous une licence qui ne serait pas open source. BusyBox est un programme très utilisé dans le monde de l'embarqué (dans la Freebox et la Livebox, par exemple): c'est un petit logiciel qui contient le code d'environ 200 commandes UNIX de base. Il est très utilisé dans l'embarqué car il permet de simplifier l'usage des commandes et de gagner de la place au niveau du système (un seul programme au lieu de plusieurs dizaines).
