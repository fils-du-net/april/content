---
site: LeMagIT
title: "L’Open Source Business Alliance travaille à l’interopérabilité des documents Office / OpenOffice "
author: Cyrille Chausson
date: 2012-02-10
href: http://www.lemagit.fr/article/microsoft-linux-interoperabilite-office-openoffice-opensource/10446/1/l-open-source-business-alliance-travaille-interoperabilite-des-documents-office-openoffice/
tags:
- Internet
- Logiciels privateurs
- Administration
- Interopérabilité
- Associations
- Standards
- International
---

> Un groupe de de responsables IT de plusieurs villes allemandes et suisses ainsi qu’un groupe de travail au sein de Open Source Business Alliance travaillent à résoudre le fâcheux problème d’interprétation des documents OOXLM dans les suites bureautiques Open Source. L’un des points bloquants de nombreux projets de migration Linux dans les administrations européennes.
