---
site: clubic.com
title: "L’open source: moteur d’évolution de l’industrie du logiciel"
author: Bertrand Diard
date: 2012-02-16
href: http://pro.clubic.com/it-business/actualite-476354-open-source-moteur-evolution-industrie-logiciel-tribune-bertrand-diard.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Innovation
---

> L'un des mythes généralement associé aux logiciels open source consiste à affirmer que les départements informatiques l'adoptent car ils sont gratuits. Comme vous le savez certainement, les logiciels open source sont en effet moins coûteux que les logiciels propriétaires. Mais la vraie raison pour laquelle les entreprises optent pour l'open source va peut être vous surprendre. Oui, les tarifs sont clairement un facteur important, mais ils ne représentent pas le principal avantage dont vous pouvez bénéficier.
