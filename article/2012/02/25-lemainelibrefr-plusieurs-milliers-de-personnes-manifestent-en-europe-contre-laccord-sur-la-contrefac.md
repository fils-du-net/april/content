---
site: lemainelibre.fr
title: "Plusieurs milliers de personnes manifestent en Europe contre l'accord sur la contrefaçon"
date: 2012-02-25
href: http://www.lemainelibre.fr/actualite/plusieurs-milliers-de-personnes-manifestent-en-europe-contre-laccord-sur-la-contrefacon-25
tags:
- Internet
- April
- Institutions
- Europe
- ACTA
---

> Plusieurs milliers de personnes ont manifesté samedi en Europe, principalement en Allemagne et en Autriche, contre l'Accord multilatéral sur la contrefaçon (Acta), contesté par nombre d'internautes qui le dénoncent comme "une atteinte aux libertés".
