---
site: leSoleil
title: "L'Assemblée nationale boude le logiciel libre"
author: Pierre Asselin
date: 2012-02-22
href: http://www.cyberpresse.ca/le-soleil/actualites/politique/201202/22/01-4498760-lassemblee-nationale-boude-le-logiciel-libre.php
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Marchés publics
- International
---

> (Québec) L'Assemblée nationale se retrouve en pleine controverse après avoir écarté les fournisseurs de logiciels libres pour l'achat de centaines de suites bureautiques. Un nouvel appel d'offres vient d'être lancé pour les produits Microsoft, exclusivement, à la suite d'une étude réalisée par... un partenaire Microsoft.
