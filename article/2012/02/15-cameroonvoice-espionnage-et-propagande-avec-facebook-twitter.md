---
site: Cameroonvoice
title: "Espionnage et propagande avec Facebook, Twitter"
author: Julie Lévesque
date: 2012-02-15
href: http://cameroonvoice.com/news/news.rcv?id=5976
tags:
- Entreprise
- Internet
- Administration
- International
---

> Une nouvelle étude parue dans le 2012 Intelligence Studies Yearbook, publié par le Mediterranean Council for Intelligence Studies (MCIS) indique que l’utilisation des médias sociaux constitue « la nouvelle façon avant-gardiste de recueillir des renseignements tactiques à l’aide de logiciels libres »
