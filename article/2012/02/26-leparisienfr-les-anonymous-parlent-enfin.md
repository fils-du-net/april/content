---
site: leParisien.fr
title: "Les Anonymous parlent enfin"
author: ANTONIN CHILOT
date: 2012-02-26
href: http://www.leparisien.fr/societe/les-anonymous-parlent-enfin-26-02-2012-1878490.php
tags:
- Internet
- HADOPI
- Institutions
- ACTA
---

> Nous avons pu participer à la première conférence de presse virtuelle organisée par des Anonymous en France. Un mouvement qui lève enfin un peu le voile sur sa stratégie et ses ambitions.
