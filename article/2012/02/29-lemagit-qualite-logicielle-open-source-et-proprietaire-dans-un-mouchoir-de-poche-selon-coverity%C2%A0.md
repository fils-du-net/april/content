---
site: LeMagIT
title: "Qualité logicielle: Open Source et propriétaire dans un mouchoir de poche, selon Coverity "
author: Cyrille Chausson
date: 2012-02-29
href: http://www.lemagit.fr/article/qualite-open-source/10574/1/qualite-logicielle-open-source-proprietaire-dans-mouchoir-poche-selon-coverity/
tags:
- Entreprise
- Internet
---

> Dans son dernier rapport, Coverity a confronté la qualité de logiciels Open Source et propriétaires. Si en moyenne les bases de code propriétaires comportent un nombre de défauts légèrement supérieur, à nombre de lignes de code équivalent, la qualité reste quasi identique.
