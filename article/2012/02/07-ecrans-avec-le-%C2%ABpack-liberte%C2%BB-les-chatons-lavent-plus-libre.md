---
site: écrans
title: "Avec le «Pack Liberté», les chatons lavent plus libre"
author: Camille Gévaudan
date: 2012-02-07
href: http://www.ecrans.fr/Pack-Liberte,14026.html
tags:
- Internet
- April
- HADOPI
- Vente liée
- Associations
- Neutralité du Net
- Promotion
- ACTA
---

> «Nous sommes une association de défense des droits et libertés des citoyens sur Internet, et nous avons besoin d’argent pour continuer à combattre les processus législatifs et les offensives gouvernementales qui menacent ces valeurs»? Non, non et non. Cette formulation est complètement rebutante. On va plutôt mettre des photos de chaton, un fond d’écran rose fuchsia et une jolie police avec des pleins et des déliés: ça va toucher la corde sensible de la ménagère!
