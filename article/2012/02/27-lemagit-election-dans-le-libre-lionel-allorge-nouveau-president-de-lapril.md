---
site: LeMagIT
title: "Election dans le libre: Lionel Allorge, nouveau président de l'April"
author: Anne-Marie Rouzere
date: 2012-02-27
href: http://www.lemagit.fr/article/politique-logiciel-assemblee-nationale-libre-vente-liee-brevet-acta-open-association/10548/1/election-dans-libre-lionel-allorge-nouveau-president-april/
tags:
- Internet
- April
- Institutions
- Brevets logiciels
- Informatique-deloyale
- ACTA
---

> Pour sa quinzième année d'activité, l'April, association de promotion du logiciel libre vient de renouveler son conseil d'administration. Lionel Allorge, membre de l'association depuis douze ans et administrateur depuis six ans, passe du statut de secrétaire à celui de président de l'association. Son prédécesseur à la présidence, Tangui Morlier, reste au conseil d'administration qui accueille par ailleurs deux nouveaux administrateurs avec Magali Garnero, secrétaire de l'association et libraire de profession, et Jonathan Le Lous, responsable de l'innovation de la SSLL Alter Way.
