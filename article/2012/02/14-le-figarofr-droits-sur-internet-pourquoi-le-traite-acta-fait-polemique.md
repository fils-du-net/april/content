---
site: LE FIGARO.fr
title: "Droits sur Internet: pourquoi le traité Acta fait polémique "
author: Geoffroy Husson
date: 2012-02-14
href: http://www.lefigaro.fr/hightech/2012/02/14/01007-20120214ARTFIG00727-droits-sur-internet-pourquoi-le-traite-acta-fait-polemique.php
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Institutions
- Associations
- DRM
- Droit d'auteur
- Europe
- International
- ACTA
- English
---

> Ce texte, censé lutter contre la contrefaçon et contre le téléchargement pirate, est une «menace majeure pour la liberté d'expression», selon ses détracteurs. Sa ratification est dans l'impasse dans plusieurs pays européens.
