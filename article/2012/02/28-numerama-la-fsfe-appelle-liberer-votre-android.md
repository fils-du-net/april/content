---
site: Numerama
title: "La FSFE appelle à \"libérer votre Android\""
author: Guillaume Champeau
date: 2012-02-28
href: http://www.numerama.com/magazine/21848-la-fsfe-appelle-a-34liberer-votre-android34.html
tags:
- Entreprise
- Logiciels privateurs
- Sensibilisation
- Associations
---

> La Free Software Foundation Europe (FSFE), qui défend le logiciel libre en Europe, a lancé mardi une campagne de communication pour faire comprendre conscience aux utilisateurs et aux développeurs que le système Android de Google n'est pas totalement libre et transparent. Elle souhaite encourager une plus grande ouverture du principal concurrent de l'iOS d'Apple, et expliquer aux consommateurs comment retrouver leur pleine liberté sur leurs smartphones et tablettes tactiles.
