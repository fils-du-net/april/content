---
site: LE FIGARO.fr
title: "«Acta n'est ni pertinent, ni efficace» "
author: Geoffroy Husson
date: 2012-02-14
href: http://www.lefigaro.fr/hightech/2012/02/14/01007-20120214ARTFIG00728-acta-n-est-ni-pertinent-ni-efficace.php
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Institutions
- Droit d'auteur
- Neutralité du Net
- Europe
- International
- ACTA
---

> INTERVIEW - Selon Françoise Castex, eurodéputée socialiste, le traité sur la contrefaçon bientôt débattu au Parlement européen conduira à instaurer un «flicage d'Internet» par les fournisseurs d'accès.
