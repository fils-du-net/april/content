---
site: LeMagIT
title: "Embarqué et Open Source: la contribution, encore absente des politiques des entreprises"
author: Cyrille Chausson
date: 2012-02-22
href: http://www.lemagit.fr/article/open-source-embarque/10526/1/embarque-open-source-contribution-encore-absente-des-politiques-des-entreprises/
tags:
- Entreprise
- Associations
- Innovation
---

> Si l’Open Source semble être le modèle préféré par les développeurs et constructeurs pour le développement de systèmes embarqués, peu d’entre eux contribuent au sein des communauté. C’est l'une des conclusions que l’on peut retenir de la dernière enquête de VDC Research. Selon le cabinet d’étude, spécialisé dans l’embarqué, l’Open Source est en grande partie préféré pour initier de nouveaux projets ou lancer de nouveaux designs - 56% des projets recensés par le cabinet.
