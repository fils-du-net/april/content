---
site: artesi
title: "Libre en Fête 2012: découvrir le Logiciel Libre à l'arrivée du printemps"
date: 2012-02-27
href: http://www.artesi.artesi-idf.com/public/article/libre-en-fete-2012-decouvrir-le-logiciel-libre-a-l-arrivee-du-printemps.html?id=25687
tags:
- Internet
- April
- Sensibilisation
- Associations
- Promotion
---

> Entre le 16 mars et le 1er avril 2012.
