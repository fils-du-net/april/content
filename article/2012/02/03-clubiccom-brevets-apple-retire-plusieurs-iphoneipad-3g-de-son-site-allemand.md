---
site: clubic.com
title: "Brevets: Apple retire plusieurs iPhone/iPad 3G de son site allemand"
author: Guillaume Belfiore
date: 2012-02-03
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/actualite-474264-motorola-obtient-injonction-apple-allemagne.html
tags:
- Entreprise
- Brevets logiciels
- International
---

> Dans l'une de ses plaintes contre Apple, Motorola vient d'obtenir le soutien de la cour en ce qui concerne le fonctionnement du service iCloud.
