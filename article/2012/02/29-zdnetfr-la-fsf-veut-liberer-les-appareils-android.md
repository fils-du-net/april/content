---
site: ZDNet.fr
title: "La FSF veut \"libérer\" les appareils Android"
author: Guénaël Pépin
date: 2012-02-29
href: http://www.zdnet.fr/blogs/androblog/la-fsf-veut-liberer-les-appareils-android-39769144.htm
tags:
- Entreprise
- Sensibilisation
- Associations
- Informatique en nuage
---

> Le pendant européen de la fondation pour le logiciel libre (FSFE) défend la vision d’un système Android débarassé de ses composants propriétaires ou, du moins, éloignés de la mainmise de Google et de ses partenaires.
