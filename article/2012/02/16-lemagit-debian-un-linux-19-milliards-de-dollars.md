---
site: LeMagIT
title: "Debian: un Linux à 19 milliards de dollars"
author: Cyrille Chausson
date: 2012-02-16
href: http://www.lemagit.fr/article/linux-debian/10488/1/debian-linux-milliards-dollars/
tags:
- Économie
- Associations
---

> 19 milliards de dollars: c’est le coût de production de la dernière version de la distribution Debian (nom de code Wheezy), nous explique James Bromberger, un consultant IT, spécialiste des logiciels Open Source, sur son blog. Celui-ci a basé son calcul sur le nombre de lignes de code répertoriées dans le code source de l’OS (hors patches) qu’il a croisé avec le salaire moyen d’un développeur.
