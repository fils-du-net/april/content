---
site: Génération nouvelles technologies
title: "Coverity: code open source et propriétaire à égalité"
author: Jérôme G.
date: 2012-02-27
href: http://www.generation-nt.com/code-open-source-proprietaire-defauts-coverity-etude-egalite-actualite-1547791.html
tags:
- Entreprise
- Internet
- DRM
- RGI
- Video
- English
---

> Présent sur le marché de l'intégrité logicielle, Coverity estime que code open source et propriétaire sont sur un pied d'égalité en termes de qualité.
