---
site: webdo
title: "Microsoft Tunisie «Open» l’année 2012 en fanfare"
author: Melek Jebnoun
date: 2012-02-06
href: http://www.webdo.tn/2012/02/06/microsoft-tunisie-open-l-annee-2012-en-fanfare/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Institutions
- Éducation
- International
---

> Avec un nouveau directeur à sa tête, Microsoft Tunisie s’est offert un souffle tout neuf pour cette année 2012. Le jeune et dynamique Mohamed Bridaa, 36 ans, l’a dit : cette année sera sous le signe de l’investissement dans le développement et l’emploi du grand potentiel que présente la Tunisie.
