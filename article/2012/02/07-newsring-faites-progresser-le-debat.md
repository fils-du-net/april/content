---
site: Newsring
title: "Faites progresser le débat"
author: Fabien Magnenou
date: 2012-02-07
href: http://www.newsring.fr/societe/353-faut-il-introduire-des-class-actions-en-france/reperes
tags:
- Entreprise
- Internet
- Économie
- April
- Institutions
- Associations
---

> Pour faire valoir ses droits, l'union ferait la force. François Hollande a relancé l'idée des class actions à la française, dites «actions de groupe», lors d'une réunion publique du think-tank de gauche «Droits, justice et sécurités», le 6 février, à Paris. Le candidat PS à la présidentielle assure qu'«avec l'action de groupe, des citoyens victimes d'un même préjudice pourront obtenir réparation».
