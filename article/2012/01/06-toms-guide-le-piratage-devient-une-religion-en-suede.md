---
site: tom's GUIDE
title: "Le piratage devient une religion en Suède"
author: Edouard le Ricque
date: 2012-01-06
href: http://www.tomsguide.fr/actualite/Kopisme-piratage-religion,2180.html
tags:
- Internet
- Associations
- International
---

> Le kopisme est une nouvelle religion, celle des pirates, hackers et autres empêcheurs de tourner en rond de la sphère informatique.
