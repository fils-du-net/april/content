---
site: infoDSI
title: "Le logiciel libre continue sa percée  "
date: 2012-01-26
href: http://www.infodsi.com/articles/128143/logiciel-libre-continue-percee.html
tags:
- Entreprise
- Économie
- Sciences
- Informatique en nuage
- International
---

> Le logiciel libre ne constitue plus une approche marginale au sein des entreprises, il est devenu un standard dans bien des domaines avec l’évolution des technologies et l’industrialisation des principaux acteurs du marché. C’est ce que montre Dans son étude Open Source France 2012, Pierre Audoin Consultants (PAC) analyse ce marché à la fois prometteur et en pleine mutation.
