---
site: LeMagIT
title: "Le Big Data, un moteur pour Linux"
author: Cyrille Chausson
date: 2012-01-23
href: http://www.lemagit.fr/article/linux-open-source/10307/1/le-big-data-moteur-pour-linux/
tags:
- Entreprise
- HADOPI
- Associations
- Innovation
- Informatique en nuage
---

> Si la dernière étude de la Linux Foundation confirme l’introduction de Linux dans les systèmes d’informations et dans les applications critiques, elle soutient également que l’OS Open Source constitue le pilier de choix pour mener des stratégies Big Data. Le mouvement devrait se confirmer en 2012. A condition toutefois de résoudre certains problèmes liés à l’interopérabilité et à la pénurie de compétences.
