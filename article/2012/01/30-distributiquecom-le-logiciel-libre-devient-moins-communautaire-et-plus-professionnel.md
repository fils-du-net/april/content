---
site: Distributique.com
title: "Le logiciel libre devient moins communautaire et plus professionnel"
author: Didier Barathon
date: 2012-01-30
href: http://www.distributique.com/actualites/lire-le-logiciel-libre-devient-moins-communautaire-et-plus-professionnel-17740.html
tags:
- Entreprise
- Économie
---

> Pierre Audoin Consultants (PAC)  publie son étude annuelle sur le marché du logiciel libre en France. Le cabinet d'études confirme la maturité et la dynamique du « libre » sur le marché national, en 2011 et en 2012, après, cette croissance devrait ralentir.
