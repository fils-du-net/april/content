---
site: LeMonde.fr
title: "Médias tunisiens: la mutation se fait toujours attendre"
author: Daniel Psenny
date: 2012-01-15
href: http://www.lemonde.fr/tunisie/article/2012/01/15/medias-tunisiens-la-mutation-se-fait-toujours-attendre_1629913_1466522.html
tags:
- Entreprise
- Internet
- Institutions
- International
- Open Data
---

> Tunis, Envoyé spécial - Un an après la révolution de jasmin et le départ précipité du dictateur Ben Ali le 14 janvier 2011, les acteurs des anciens et nouveaux médias se sont réunis les 12 et 13 janvier à Tunis pour un colloque intitulé "Tunisie : révolution, transition et mutation" organisé par Canal France International (CFI), l'Association tunisienne des libertés numériques (ATLN), l'Association du multimédia et de l'audiovisuel et Tunisie Live, premier site tunisien d'information en langue anglaise.
