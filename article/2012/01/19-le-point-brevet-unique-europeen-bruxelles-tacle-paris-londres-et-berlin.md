---
site: Le point
title: "Brevet unique européen: Bruxelles tacle Paris, Londres et Berlin"
date: 2012-01-19
href: http://www.lepoint.fr/futurapolis/point-innovation/brevet-unique-europeen-bruxelles-tacle-paris-londres-et-berlin-19-01-2012-1420970_448.php
tags:
- Entreprise
- Administration
- Institutions
- Brevets logiciels
- Innovation
- Sciences
- Europe
---

> José Manuel Barroso en appelle à un accord sur le brevet européen. 
