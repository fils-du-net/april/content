---
site: Developpez.com
title: "Mozilla annonce la version 2 de sa licence, la Mozilla Public License se positionne entre les licences Apache et GNU"
author: Gordon Fowler
date: 2012-01-04
href: http://www.developpez.com/actu/40352/Mozilla-annonce-la-version-2-de-sa-licence-la-Mozilla-Public-License-se-positionne-entre-les-licences-Apache-et-GNU/
tags:
- Internet
- Associations
- Licenses
---

> Mozilla vient de publier, après presque deux ans de consultations (21 mois exactement), la deuxième mouture de sa licence MPL.
