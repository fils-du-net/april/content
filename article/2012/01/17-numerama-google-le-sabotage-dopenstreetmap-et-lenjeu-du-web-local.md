---
site: Numerama
title: "Google, le sabotage d'OpenStreetMap et l'enjeu du web local"
author: Guillaume Champeau
date: 2012-01-17
href: http://www.numerama.com/magazine/21297-google-le-sabotage-d-openstreetmap-et-l-enjeu-du-web-local.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Associations
- International
---

> Ce sont deux affaires très rapprochées dans le temps qui posent le discrédit sur Google. Le géant américain de la recherche est accusé d'avoir saboté le service OpenStreetMap qui propose de la cartographie sous licence libre, et a reconnu par ailleurs l'existence d'actions de concurrence déloyale à l'encontre d'une société qui référençait des entreprises locales. Outre une adresse IP de Google en Inde, les deux affaires ont pour point commun le marché croissant du web local sur lequel Google fonde beaucoup d'espoir.
