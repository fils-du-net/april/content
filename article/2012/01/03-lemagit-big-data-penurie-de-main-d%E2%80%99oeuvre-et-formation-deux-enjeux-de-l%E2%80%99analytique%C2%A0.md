---
site: LeMagIT
title: "Big Data: pénurie de main d’oeuvre et formation, deux enjeux de l’analytique "
date: 2012-01-03
href: http://www.lemagit.fr/article/formation-donnees-gestion-hadoop-big-data/10167/1/big-data-penurie-main-oeuvre-formation-deux-enjeux-analytique/
tags:
- Entreprise
- Internet
- Innovation
- Informatique en nuage
- Europe
---

> Si le Big Data et les technologies liées à l’analytique seront deux tendances de 2012, nos confrères de TechTarget pointent du doigt que l’intégration de ces nouvelles technologies dans les entreprises n’est pas encore garantie. Freinée par des compétences encore trop rares et la nécessité de former des équipes en place.
