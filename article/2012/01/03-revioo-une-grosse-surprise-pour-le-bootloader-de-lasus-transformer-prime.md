---
site: Revioo
title: "Une grosse surprise pour le Bootloader de l'Asus Transformer Prime"
author: ShinJ
date: 2012-01-03
href: http://www.revioo.com/news/asus-transformer-prime-bootloader-n16610.html
tags:
- DRM
- Video
---

> Voilà maintenant une semaine que l’Eeepad Transformer Prime est disponible à la vente aux États-Unis, et c’est donc logiquement que nous profitons des premières retombés et impressions sur le produit. Si unanimement les critiques concernant la puissance ou les finitions de l’appareil sont globalement excellentes, une information nous est parvenue avec beaucoup d’étonnement.
