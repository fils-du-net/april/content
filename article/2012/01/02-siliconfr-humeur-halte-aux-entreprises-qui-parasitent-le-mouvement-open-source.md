---
site: Silicon.fr
title: "Humeur: halte aux entreprises qui parasitent le mouvement open source"
author: David Feugey
date: 2012-01-02
href: http://www.silicon.fr/humeur-halte-aux-entreprises-qui-parasitent-le-mouvement-open-source-69898.html
tags:
- Entreprise
- Licenses
- Informatique en nuage
---

> Certains éditeurs tentent de profiter du mouvement open source pour mettre en avant leurs projets, sans toutefois reverser de code à la communauté.
