---
site: ITRmanager.com
title: "2012, l’année du «coming out» de la propriété intellectuelle dans la gestion des systèmes d’information"
author: Tru Dô-Khac
date: 2012-01-20
href: http://www.itrmanager.com/tribune/127815/2012-annee-coming-out-propriete-intellectuelle-gestion-systemes-information-tru-do-khac-consultant-innovation-gouvernance-numerique-entreprise.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Économie
- Brevets logiciels
- Désinformation
- Droit d'auteur
- Innovation
- Open Data
---

> La propriété intellectuelle est un moyen pour protéger l’innovation. Pour les entreprises numériques innovantes,  la propriété intellectuelle pourrait faire son coming-out dans la gestion des systèmes d’information.
