---
site: linuxfr.org
title: "Sony: Ma propriété intellectuelle vaut plus que la vôtre"
author: nud
date: 2012-01-31
href: http://linuxfr.org/news/sony-ma-propriete-intellectuelle-vaut-plus-que-la-votre
tags:
- Entreprise
- Associations
- Licenses
---

> Pour les anglophobes, le billet sus-cité explique que BusyBox, sous GPL est utilisé virtuellement partout où l'on se sert du noyau Linux. Cette utilisation permet à la Software Freedom Conservancy de forcer les entreprises à respecter les termes de la licence et à redistribuer le code source des logiciels qu'ils utilisent, car les copyright holders lui en ont donné le droit (apparemment ce n'est le cas d'aucun grand contributeur du noyau Linux lui-même).
