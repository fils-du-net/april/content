---
site: LeMagIT
title: "Recrutements Open Source: les SSLL mettent le turbo"
author: Anne-Marie Rouzere
date: 2012-01-13
href: http://www.lemagit.fr/article/emploi-recrutement-formation-logiciel-libre-open-source-embauche/10241/1/recrutements-open-source-les-ssll-mettent-turbo/
tags:
- Entreprise
- Éducation
---

> Pour faire face à leurs objectifs d'embauche (et de business), les «pure players» de l'Open Source misent sur la spécificité de leurs pratiques de formation interne. Globalement, le cap est mis sur un doublement des effectifs en 3 ans (2011-2013).
