---
site: leSoleil
title: "Le logiciel libre propose un potentiel d'économie incroyable"
author: Francis A-Trudel
date: 2012-01-16
href: http://www.cyberpresse.ca/la-tribune/economie/201201/16/01-4486274-le-logiciel-libre-propose-un-potentiel-deconomie-incroyable.php
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Associations
- Marchés publics
- Standards
- International
---

> Un choix « critique pour le futur » attend le gouvernement Charest en février, acculé par le géant Microsoft qui cessera de supporter son système d'exploitation Windows XP dans deux ans. Résultat? Une migration massive à prévoir pour les quelque 400 000 postes informatiques de l'État, estimée à 800 millions $.
