---
site: Rue89 LES BLOGS
title: "Bauwens: «Le peer-to-peer est le socialisme du XXIe siècle»"
author: Anne-Sophie Novel
date: 2012-01-16
href: http://blogs.rue89.com/greensiders/2012/01/16/bauwens-le-peer-peer-est-le-socialisme-du-xxie-siecle-226170
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Économie
- Institutions
- Associations
- Innovation
- Sciences
---

> Le « peer-to-peer », souvent abrégé P2P, permet par exemple d'échanger des fichiers musicaux ou des films sur Internet. Napster, eDonkey, eMule, Kazaa ou encore plus récemment Spotify... sont des systèmes P2P.
