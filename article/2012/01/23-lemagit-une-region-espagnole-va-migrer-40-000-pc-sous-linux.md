---
site: LeMagIT
title: "Une région espagnole va migrer 40 000 PC sous Linux"
author: Cyrille Chausson
date: 2012-01-23
href: http://www.lemagit.fr/article/linux-poste-travail-open-source/10309/1/une-region-espagnole-migrer-000-sous-linux/
tags:
- Administration
- Europe
- International
---

> L’Extrémadoure, une région du sud ouest de l’Espagne, a prévu de se lancer dans un vaste programme de migration Linux de quelque 40 000 postes de travail de son administration, révèle le site JoinUp de la Commission européenne.
