---
site: LeJournalduNet
title: "Le Logiciel Libre gagne des parts de marché dans les entreprises françaises"
date: 2012-01-30
href: http://www.journaldunet.com/solutions/systemes-reseaux/logiciel-libre-en-entreprise-0112.shtml
tags:
- Entreprise
- Internet
- Administration
- Innovation
- Informatique en nuage
---

> Le marché du Logiciel Libre et des services associés a enregistré une belle croissance ces dernières années en France. Une dynamique qui devrait se poursuivre, à en croire le cabinet PAC.
