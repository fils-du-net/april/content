---
site: LeMonde.fr
title: "Google rachète près de 200 brevets à IBM"
date: 2012-01-05
href: http://www.lemonde.fr/technologies/article/2012/01/05/google-rachete-pres-de-200-brevets-a-ibm_1625818_651865.html
tags:
- Entreprise
- Brevets logiciels
---

> Google a indiqué, mercredi 4 janvier, qu'il avait acheté au groupe informatique IBM 188 brevets et 29 autres innovations en attente d'homologation. Ces brevets sont liés à la téléphonie mobile.Le groupe du moteur de recheche, qui depuis plusieurs mois étoffe son portefeuille de brevets, alors qu'il est la cible de nombreuses poursuites portant sur la propriété intellectuelle, n'a pas indiqué combien il avait versé à IBM pour ces derniers achats.
