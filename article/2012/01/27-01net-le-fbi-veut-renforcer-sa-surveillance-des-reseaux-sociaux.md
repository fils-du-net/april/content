---
site: 01net.
title: "Le FBI veut renforcer sa surveillance des réseaux sociaux"
author: Pierre Fontaine
date: 2012-01-27
href: http://www.01net.com/editorial/553860/le-fbi-veut-renforcer-sa-surveillance-des-reseaux-sociaux/
tags:
- Internet
- Administration
- International
---

> Le célèbre bureau fédéral américain vient de passer un appel d'offre pour créer un outil open source de surveillance des réseaux sociaux à la recherche de dangers potentiels.
