---
site: 01net.
title: "Le kopisme, religion officielle du piratage en Suède"
author: Hélène Puel
date: 2012-01-05
href: http://www.01net.com/editorial/551822/le-piratage-reconnu-religion-officielle-en-suede/
tags:
- Internet
- Institutions
- Associations
- International
---

> Le mouvement du kopisme prônant la copie vient d'être élu au rang de religion en Suède. Pour autant le piratage est toujours illégal dans ce pays nordique.
