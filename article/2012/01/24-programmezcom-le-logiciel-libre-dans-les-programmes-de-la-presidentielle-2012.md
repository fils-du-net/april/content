---
site: Programmez.com
title: "Le Logiciel Libre dans les programmes de la présidentielle 2012"
author: Frédéric Mazué
date: 2012-01-24
href: http://www.programmez.com/actualites.php?titre_actu=Le-Logiciel-Libre-dans-les-programmes-de-la-presidentielle-2012$urlid_actu=10938
tags:
- April
- Institutions
- Brevets logiciels
- DRM
- Droit d'auteur
- Innovation
---

> Dans le cadre de son initiative Candidats.fr, l'April organise à La Cantine à Paris, le 25 janvier 2012, de 18h30 à 22h00, une soirée publique « Candidats.fr - Le logiciel libre dans les campagnes présidentielle et législatives », en présence de représentants de candidats à l'élection présidentielle 2012.
