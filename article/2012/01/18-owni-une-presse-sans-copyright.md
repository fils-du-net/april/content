---
site: OWNI
title: "Une presse sans copyright"
author: Lionel Maurel (Calimaq)
date: 2012-01-18
href: http://owni.fr/2012/01/18/une-presse-sans-copyright/
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
- Licenses
- International
---

> Les articles de presse doivent-ils être protégés par le droit d'auteur? Ce n'est pas l'avis d'un récent arrêt d'une Cour de Bratislava.
