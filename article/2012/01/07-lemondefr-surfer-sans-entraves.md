---
site: LeMonde.fr
title: "Surfer sans entraves"
author: Yves Eudes
date: 2012-01-07
href: http://www.lemonde.fr/technologies/article/2012/01/07/surfer-sans-entraves_1627059_651865.html
tags:
- Entreprise
- Internet
- Matériel libre
- Institutions
- Associations
- Innovation
- International
---

> Un café à la mode, dans un quartier fréquenté par les étudiants de Manhattan. En cette fin de matinée, la salle est bondée, mais pas trop bruyante, car la moitié des clients lisent ou écrivent sur leur ordinateur portable. David Darts, responsable du département d'art de la New York University, entre discrètement, avec à la main une lunch box d'écolier - une petite boîte en fer noire, décorée d'une tête de mort.
