---
site: linuxfr.org
title: "«Guerre et paix»: Tolstoï au 21e siècle, par Cory Doctorow au 28C3"
author: Malicia
date: 2012-01-04
href: http://linuxfr.org/news/%c2%a0guerre-et-paix%c2%a0%c2%a0-tolsto%c3%af-au-21e-si%c3%a8cle-par-cory-doctorow-au-28c3
tags:
- Internet
- Économie
- April
- Institutions
- Associations
- DRM
- Droit d'auteur
- Innovation
- Licenses
- Video
- International
---

> Je vous avais récemment parlé du programme du CCC, 28C3 de son doux surnom en 2011. Ça avait l'air super. C'était avant d'y aller. À posteriori, c'était génial.
