---
site: Numerama
title: "ACTA: démissionnaire, Kader Arif dénonce \"une mascarade\""
author: Guillaume Champeau
date: 2012-01-26
href: http://www.numerama.com/magazine/21424-acta-demissionnaire-kader-arif-denonce-une-mascarade.html
tags:
- Institutions
- Europe
- ACTA
---

> C'est un communiqué au vitriol qu'a communiqué l'eurodéputé Kader Arif pour expliquer sa démission en tant que rapporteur principal de l'ACTA au Parlement Européen.
