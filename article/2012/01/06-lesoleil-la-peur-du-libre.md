---
site: leSoleil
title: "La peur du libre"
author: Pierre Asselin
date: 2012-01-06
href: http://www.cyberpresse.ca/le-soleil/opinions/editoriaux/201201/05/01-4483217-la-peur-du-libre.php
tags:
- Entreprise
- Administration
- Institutions
- Marchés publics
- International
---

> (Québec) De quoi le gouvernement provincial peut-il bien avoir peur lorsque vient le temps d'ouvrir son marché de l'informatique à la libre concurrence? L'adoption de la politique-cadre et d'une loi sur la gouvernance et la gestion des ressources informationnelles des organismes publics, mise de l'avant par la présidente du Conseil du trésor, Michelle Courchesne, devait favoriser l'ouverture de l'administration publique au logiciel libre. Mais dans les faits, les seuls gestes posés jusqu'ici par les gestionnaires de l'État ont visé à les soustraire à cette volonté.
