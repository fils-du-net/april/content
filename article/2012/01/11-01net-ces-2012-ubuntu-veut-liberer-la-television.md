---
site: 01net.
title: "CES 2012: Ubuntu veut libérer la télévision"
author: Eric Le Bourlout
date: 2012-01-11
href: http://www.01net.com/editorial/552830/ces-2012-ubuntu-veut-liberer-la-television/
tags:
- Entreprise
- Innovation
- Video
---

> Canonical a fait une démonstration de son OS pour télévision intelligente au salon. Et cherche des partenaires constructeurs pour en commercialiser bientôt.
