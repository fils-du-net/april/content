---
site: LeMonde.fr
title: "Wikipédia, bazar libertaire"
author: Frédéric Joignot
date: 2012-01-14
href: http://www.lemonde.fr/technologies/article/2012/01/14/wikipedia-bazar-libertaire_1629135_651865.html
tags:
- Entreprise
- Internet
- Économie
- Associations
- Droit d'auteur
- Éducation
- Innovation
- Licenses
- Philosophie GNU
---

> Difficile de rater les visages des contributeurs et dirigeants de l'encyclopédie en ligne Wikipédia. De novembre 2011 à début janvier, à chaque recherche, leurs portraits apparaissaient en tête de page, vous invitant à faire un don. Susan et ses longs cheveux blancs. Rémi Mathis, président de Wikimédia France, avec son air potache. Jimmy Wales, le fondateur américain.
