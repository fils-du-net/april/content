---
site: 01net.
title: "Pour Vinton Cerf, père d'Internet, sa création n'est pas un droit"
author: Pierre Fontaine
date: 2012-01-06
href: http://www.01net.com/editorial/552548/pour-vinton-cerf-pere-dinternet-sa-creation-nest-pas-un-droit/
tags:
- Entreprise
- Internet
- Institutions
- Innovation
---

> Dans une tribune publiée dans le New York Times, Vinton Cerf, cocréateur du protocole TCP/IP, déclare que, pour lui, Internet ne doit pas être considéré comme un droit essentiel pour l'homme. Juste comme un outil...
