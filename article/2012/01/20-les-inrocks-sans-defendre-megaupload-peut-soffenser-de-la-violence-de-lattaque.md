---
site: les inrocks
title: "\"Sans défendre Megaupload, on peut s'offenser de la violence de l'attaque\""
author: Camille Polloni
date: 2012-01-20
href: http://www.lesinrocks.com/actualite/actu-article/t/76330/date/2012-01-20/article/sans-defendre-megaupload-on-peut-soffenser-de-la-violence-de-lattaque/
tags:
- Internet
- HADOPI
- RGI
- Video
---

> Jérémie Zimmermann, l'un des fondateurs de La Quadrature du Net, revient sur la fermeture de Megaupload par le FBI ce jeudi et les attaques en déni de service qui ont suivi.
