---
site: Le nouvel Observateur
title: "1 million de dollars en 12 jours: la leçon anti-marketing du comique Louis C.K."
author: Christophe Lachnitt
date: 2012-01-11
href: http://leplus.nouvelobs.com/contribution/230211-1-million-de-dollars-en-12-jours-la-lecon-anti-marketing-du-comique-louis-c-k.html
tags:
- Entreprise
- Internet
- Économie
- DRM
---

> En choisissant de s'adresser directement à son public pour vendre la vidéo de son nouveau spectacle - sans DRM et pour 5 dollars -, l'humoriste américain Louis C.K. a réalisé une opération aussi profitable pour ses fans que pour lui-même. Christophe Lachnitt, directeur de la communication chez DCNS, indique les enseignements à en tirer.
