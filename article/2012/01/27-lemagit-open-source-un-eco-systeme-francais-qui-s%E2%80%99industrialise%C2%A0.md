---
site: LeMagIT
title: "Open Source: un éco-système français qui s’industrialise "
author: Cyrille Chausson
date: 2012-01-27
href: http://www.lemagit.fr/article/france-opensource-pac/10346/1/open-source-eco-systeme-francais-qui-industrialise/
tags:
- Entreprise
- Économie
- Associations
- Innovation
- Licenses
- Informatique en nuage
---

> Selon la dernière étude du cabinet Pierre Audoin Consultant, l’éco-système de l’Open Source en France est entré en 2011 dans l’ère de l’industrialisation. Maturité des technologies, prestations de services à la hausse dans les SSII, adoption des entreprises, main mise sur l’outillage, le mouvement du logiciel ouvert compte pour 6% des services IT en 2011, pour 2,5 milliards d’euros générés. Si la croissance devrait se poursuivre jusqu’en 2015, elle devrait ralentir à partir de cette date, faute de présence sur l’applicatif. Un secteur dominé par le Cloud.
