---
site: ZEVILLAGE.net
title: "On va parler télétravail aux rencontres du libre, le 29 janvier à Lion-sur-Mer"
author: Xavier de Mazenod
date: 2012-01-24
href: http://www.zevillage.net/2012/01/24/on-va-parler-teletravail-aux-rencontres-du-libre-le-29-janvier-a-lion-sur-mer/
tags:
- Entreprise
- April
- Sensibilisation
- Associations
---

> La ville de Lion-sur-Mer, dans le Calvados, organise les 2e Rencontres du libre, du 27 au 29 janvier 2012 pendant lesquelles le thème du télétravail sera abordé lors d’une table-ronde.
