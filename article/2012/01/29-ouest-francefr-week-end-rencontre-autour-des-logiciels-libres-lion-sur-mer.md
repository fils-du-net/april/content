---
site: "ouest-france.fr"
title: "Week-end rencontre autour des logiciels libres - Lion-sur-Mer"
date: 2012-01-29
href: http://www.ouest-france.fr/actu/actuLocale_-Week-end-rencontre-autour-des-logiciels-libres-_14166-avd-20120129-62146310_actuLocale.Htm
tags:
- Administration
- April
- Sensibilisation
---

> Durant deux jours, l'office de tourisme organise les 2 es Rencontres du libre. « Ces journées permettront l'échange et le partage entre les artistes, développeurs et utilisateurs de logiciels. Un logiciel libre étant un logiciel dont l'utilisation, l'étude, la modification, la duplication et la diffusion sont libres », explique Pascal Roubaud, animateur culturel à l'office de tourisme.
