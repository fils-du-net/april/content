---
site: GIZMODO
title: "Du rififi chez Google"
author: Florence
date: 2012-01-17
href: http://www.gizmodo.fr/2012/01/17/du-rififi-chez-google.html
tags:
- Entreprise
- Associations
- International
---

> Google a beau être un magnat du web, les manœuvres douteuses ne lui sont pas inconnues, pour preuve ils sont aujourd’hui accusés de vandalisme virtuel. OpenStreetMap accuse le géant d’avoir effectué modifications frauduleuses de leurs données rendant ainsi les cartes erronées.
