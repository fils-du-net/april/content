---
site: technautes.cyberpresse.ca
title: "Après les logiciels, au tour des téléviseurs de devenir «open source»"
author: Alain McKenna
date: 2012-01-30
href: http://technaute.cyberpresse.ca/nouvelles/produits-electroniques/201201/30/01-4490830-apres-les-logiciels-au-tour-des-televiseurs-de-devenir-open-source.php
tags:
- Entreprise
- Internet
- Innovation
- Video
---

> Après la télé branchée, la télé «open source»? Pourquoi pas: c'est le rêve de développeurs ayant réussi à créer l'Ubuntu TV, un logiciel libre dérivé du système Linux qui s'installe sur des téléviseurs ou des récepteurs multimédias reliés à internet. Les Apple TV et de Google TV n'ont qu'à bien se tenir!
