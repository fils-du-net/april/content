---
site: Silicon.fr
title: "Le logiciel libre a la cote au sein des entreprises françaises"
author: David Feugey
date: 2012-01-31
href: http://www.silicon.fr/le-logiciel-libre-a-la-cote-au-sein-des-entreprises-francaises-71216.html
tags:
- Entreprise
- Économie
- Informatique en nuage
---

> La dernière étude en date de PAC confirme la bonne dynamique du logiciel libre chez les entreprises françaises. Un phénomène qui ne devrait pas ralentir avant 2015.
