---
site: Numerama
title: "L'orchestre de Prague enregistre de la musique libre en renonçant à ses droits"
author: Julien L.
date: 2012-01-05
href: http://www.numerama.com/magazine/21170-l-orchestre-de-prague-enregistre-de-la-musique-libre-en-renoncant-a-ses-droits.html
tags:
- HADOPI
- Droit d'auteur
- Licenses
- Contenus libres
---

> Le projet MusOpen prend forme. Ce mois-ci, l'orchestre symphonique de Prague sera mobilisé pour enregistrer de la musique libre, en reprenant des symphonies de Beethoven, Brahms ou Tchaïkovski. Les musiciens seront payés pour la séance d'enregistrement, mais ne pourront pas percevoir de droits voisins. Une initiative qui fait débat, notamment en France.
