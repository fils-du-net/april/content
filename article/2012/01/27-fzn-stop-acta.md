---
site: FZN
title: "Stop ACTA!"
author: Fred
date: 2012-01-27
href: http://www.fredzone.org/stop-acta-773
tags:
- Internet
- April
- HADOPI
- Institutions
- DADVSI
- DRM
- Droit d'auteur
- Europe
- International
- ACTA
---

> Hadopi, Loppsi, SOPA, PIPA, ACTA, les lois ou les traités liberticides se suivent, se ressemblent et n'ont de cesse de nous laisser un goût amer dans la bouche. Plutôt que de s'attaquer aux vrais problèmes du monde (famines, guerres, menaces nucléaires, crises économiques), nos gouvernements semblent s'entendre pour mettre au point des projets de lois nocifs et dangereux pour nos démocraties. Des projets de loi qui ne visent finalement qu'à favoriser les grands groupes et les multinationales de ce monde au détriment du peuple et donc de nous tous.
