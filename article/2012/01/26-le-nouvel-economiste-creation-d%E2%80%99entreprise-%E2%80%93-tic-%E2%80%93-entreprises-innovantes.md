---
site: Le nouvel Economiste
title: "Création d’entreprise – TIC – Entreprises innovantes"
author: Andrea Paracchini
date: 2012-01-26
href: http://www.lenouveleconomiste.fr/lesdossiers/creation-dentreprise-tic-entreprises-innovantes-13452/
tags:
- Entreprise
- Internet
- Administration
- Économie
- Associations
- Innovation
---

> Les stigmates de l’explosion de la bulle Internet du début des années 2000 semblent bel et bien effacés et oubliés: le secteur des technologies de l’information et de la communication (TIC) est aujourd’hui très dynamique en France. De nombreuses start-up voient le jour dans des secteurs aussi variés que l’édition de logiciel et l’e-commerce. Si le mythe du jeune étudiant dans son garage persiste, l’entrepreneur solitaire cède de plus en plus la place à des binômes composés d’ingénieurs et de commerciaux.
