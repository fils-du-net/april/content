---
site: ZDNet.fr
title: "La Nasa lance un site pour ses projets open source"
author: Thierry Noisette
date: 2012-01-08
href: http://www.zdnet.fr/blogs/l-esprit-libre/la-nasa-lance-un-site-pour-ses-projets-open-source-39767178.htm
tags:
- Entreprise
- Internet
- Administration
- April
- Sciences
- Informatique en nuage
- International
---

> Pour unifier et étendre ses activités dans les logiciels libres, l'agence spatiale américaine a ouvert un site central, code.nasa.gouv.
