---
site: LE FIGARO.fr
title: "Google se voile de noir pour protester contre la loi SOPA"
author: Geoffroy Husson
date: 2012-01-17
href: http://www.lefigaro.fr/hightech/2012/01/17/01007-20120117ARTFIG00609-la-version-anglaise-de-wikipedia-fermee-mercredi.php
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- International
---

> Le moteur de recherche entend ainsi protester contre le vote aux États-Unis de la loi SOPA, qui renforce la lutte contre le piratage.
