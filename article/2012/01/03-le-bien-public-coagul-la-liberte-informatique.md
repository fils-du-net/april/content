---
site: LE BIEN PUBLIC
title: "Coagul, la liberté informatique"
date: 2012-01-03
href: http://www.bienpublic.com/grand-dijon/2012/01/03/coagul-la-liberte-informatique
tags:
- Sensibilisation
- Associations
---

> Coagul permet à ses adhérents ou non d’ailleurs, de venir participer à différents ateliers et ainsi découvrir les logiciels libres.
