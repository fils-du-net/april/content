---
site: ZDNet.fr
title: "WebOS: le passage en open source finalisé en septembre"
author: La rédaction
date: 2012-01-26
href: http://www.zdnet.fr/actualites/webos-le-passage-en-open-source-finalise-en-septembre-39767868.htm
tags:
- Entreprise
- Licenses
---

> HP a dévoilé la feuille de route du programme open source de WebOS qui commence par la disponibilité immédiate du framework Enyo permettant le développement d’applications multiplateformes.
