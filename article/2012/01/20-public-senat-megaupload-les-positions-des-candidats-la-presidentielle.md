---
site: PUBLIC SÉNAT
title: "Megaupload: les positions des candidats à la présidentielle"
author: Aladine Zaïane
date: 2012-01-20
href: http://www.publicsenat.fr/lcp/politique/megaupload-positions-des-candidats-pr-sidentielle-195327
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Droit d'auteur
- Licenses
---

> La fermeture du site de téléchargement Megaupload a redonné un coup de fouet aux thématiques numériques dans la campagne. Petit tour d’horizon des réactions et des propositions des candidats sur l'Hadopi et le piratage.
