---
site: Revioo
title: "Richard Stallman ne décolère pas contre les livres numériques"
author: ShinJ
date: 2012-01-24
href: http://www.revioo.com/news/richard-stallman-ereader-amazon-n16769.html
tags:
- Entreprise
- Internet
- DRM
- Droit d'auteur
- Philosophie GNU
---

> Récemment auteur d’un texte dans lequel il expose ses opinions face aux livres numériques, Richard Stallman, instigateur du projet GNU et l’un des piliers de l’open source, exprime ouvertement son pessimisme quant à l’avenir de ce format et met en garde contre les dangers qu’il représente selon lui.
