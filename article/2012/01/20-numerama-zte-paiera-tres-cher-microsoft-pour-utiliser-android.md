---
site: Numerama
title: "ZTE paiera très cher Microsoft pour utiliser Android"
author: Simon Robic
date: 2012-01-20
href: http://www.numerama.com/magazine/21352-zte-paiera-tres-cher-microsoft-pour-utiliser-android.html
tags:
- Entreprise
- Brevets logiciels
---

> Un nouveau constructeur de smartphones sous Android vient de signer un accord avec Microsoft. Cet accord, très contraignant, promet la tranquillité juridique à ZTE en échange du versement de royalties pourtant sur l'utilisation de technologies brevetées par le géant américain. Une situation devant laquelle Google ne pourra pas rester immobile encore très longtemps.
