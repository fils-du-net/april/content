---
site: internet ACTU.net
title: "Entretiens du Nouveau Monde industriel 2011 (4/4): aspects économiques et politiques"
author: Rémi Sussan
date: 2012-01-19
href: http://www.internetactu.net/2012/01/19/entretiens-du-nouveau-monde-industriel-2011-44-aspects-economiques-et-politiques/
tags:
- Entreprise
- Économie
- Sciences
- Open Data
---

> Valérie Peugeot, d’Orange Labs, s’est penchée sur les enjeux démocratiques et politiques de l’open data, soulignant la particularité de la France par rapport aux pays qui l’ont précédé dans cette initiative. Chez nous, la démarche d’ouvrir les données est venue bien plus des collectivités locales que du sommet de l’Etat. Et celles-ci étaient essentiellement de gauche, contrairement au reste du monde où, dans le choix de recourir à l’open data, la couleur politique importait peu.
