---
site: atlantico
title: "La position dominante des Google, Apple et Facebook repose sur l'innovation"
author: Mehdi Benchoufi
date: 2012-01-01
href: http://www.atlantico.fr/decryptage/google-apple-facebook-monopoles-web-innovation-continuelle-mehdi-benchoufi-251530.html
tags:
- Entreprise
- Internet
- Partage du savoir
- Innovation
---

> Moteur de recherche, boîtes mails, données conservées en ligne, smartphones, lecteurs MP3, carnets d'adresse, etc... Google, Apple et Facebook sont-il en train de construire des monopoles à la big brother qui leur donneraient une emprise comme aucun pouvoir n'en a jamais eu sur nos vies?
