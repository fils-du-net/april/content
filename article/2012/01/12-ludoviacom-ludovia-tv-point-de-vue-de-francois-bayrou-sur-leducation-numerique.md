---
site: ludovia.com
title: "LUDOVIA TV: Point de vue de François Bayrou sur l'éducation numérique"
author: AJ
date: 2012-01-12
href: http://www.ludovia.com/action_publique/2012/1242/point-de-vue-de-francois-bayrou-sur-l-education-numerique.html
tags:
- Internet
- Administration
- Institutions
- Éducation
---

> François Bayrou déclare avoir donné une grande partie de sa vie à l’éducation et ayant été Ministre de l’éducation (1993-1997), il joue dans une cours qu’il connaît bien et dans laquelle il dit avoir tissé des liens privilégiés.
