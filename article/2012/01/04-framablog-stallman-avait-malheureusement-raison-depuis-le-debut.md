---
site: Framablog
title: "Stallman avait malheureusement raison depuis le début"
author: Thom Holwerda
date: 2012-01-04
href: http://www.framablog.org/index.php/post/2012/01/04/stallman-avait-raison
tags:
- Internet
- Logiciels privateurs
- Institutions
- Associations
- Philosophie GNU
- International
---

> Le cauchemar paranoïaque et apocalyptique d’un geek psychorigide est en passe de devenir réalité.
