---
site: Developpez.com
title: "«Inutile de parler d'industrie informatique, nous n'en avons plus», un informaticien propose 3 axes pour relancer l'IT en France"
author: gordon fowler
date: 2012-01-02
href: http://www.developpez.com/actu/40291/-Inutile-de-parler-d-industrie-informatique-nous-n-en-avons-plus-un-informaticien-propose-3-axes-pour-relancer-l-IT-en-France/
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Innovation
- Informatique en nuage
---

> Denis Szalkowski est un formateur-consultant MySQL, Oracle, Linux, PHP, Perl, etc. Il a fondé en 2004 Winuxware, une société spécialisée dans le référencement, le développement de sites WordPress et osCommerce, et dans les systèmes d'information sous Linux.
