---
site: "Agence Science-Presse"
title: "Accès libre à la science: l'opposition contre-attaque"
author: Pascal Lapointe
date: 2012-01-22
href: http://www.sciencepresse.qc.ca/actualite/2012/01/22/acces-libre-science-lopposition-contre-attaque
tags:
- Internet
- Institutions
- Droit d'auteur
- Sciences
- International
---

> Pendant qu’une partie d’Internet virait au noir mercredi, en protestation contre deux projets de loi déposés à Washington, un troisième projet de loi passait inaperçu. Les éditeurs scientifiques ont eux aussi lancé une contre-attaque.
