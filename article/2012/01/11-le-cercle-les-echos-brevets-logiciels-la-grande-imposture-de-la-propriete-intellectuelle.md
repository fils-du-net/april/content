---
site: LE CERCLE Les Echos
title: "Brevets logiciels: la grande imposture de la propriété intellectuelle"
author: Patrice Bertrand
date: 2012-01-11
href: http://lecercle.lesechos.fr/entreprises-marches/high-tech-medias/informatiques/221142104/brevets-logiciels-grande-imposture-prop
tags:
- Entreprise
- Économie
- Institutions
- Associations
- Brevets logiciels
- Droit d'auteur
- Innovation
---

> La protection de la propriété intellectuelle est très en vogue. Elle est, dit-on, le meilleur soutien de l'innovation, elle-même créatrice de croissance et d'emplois. Nous voulons montrer ici que la transposition du principe de brevet dans le logiciel est une démarche erronée sur le fond, coûteuse pour la société et nuisible pour l'innovation.
