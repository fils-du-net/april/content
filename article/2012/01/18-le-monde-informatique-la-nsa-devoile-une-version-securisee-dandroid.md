---
site: Le Monde Informatique
title: "La NSA dévoile une version sécurisée d'Android"
author: Jacques Cheminat
date: 2012-01-18
href: http://www.lemondeinformatique.fr/actualites/lire-la-nsa-devoile-une-version-securisee-d-android-47450.html
tags:
- Administration
- Innovation
- International
---

> La National Security Agency (NSA) a publié une version plus sécurisée d'Android baptisée SE. Elle propose le renforcement des politiques de sécurité sur le contrôle d'accès des applications.
