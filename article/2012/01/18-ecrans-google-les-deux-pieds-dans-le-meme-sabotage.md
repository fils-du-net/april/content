---
site: écrans
title: "Google, les deux pieds dans le même sabotage"
author: Camille Gévaudan
date: 2012-01-18
href: http://www.ecrans.fr/Google-les-deux-pieds-dans-le-meme,13890.html
tags:
- Entreprise
- Internet
- Associations
- International
---

> On a connu Google plus réglo que ça... À deux reprises cette semaine, dans un contexte différent mais toujours lié au secteur de la géolocalisation, l’entreprise s’est fait pincer en pleine opération de sabotage de la concurrence. C’est au Kenya (si, si) que le premier scandale a éclaté : un fournisseur de service local, équivalent en ligne de nos Pages Blanches, accuse Google d’avoir fouillé dans son fichier de clients pour se les approprier.
