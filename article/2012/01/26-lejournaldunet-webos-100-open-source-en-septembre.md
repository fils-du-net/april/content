---
site: LeJournalduNet
title: "WebOS 100% Open Source en septembre"
author: Dominique FILIPPONE
date: 2012-01-26
href: http://www.journaldunet.com/developpeur/client-web/open-webos-1-0-0112.shtml
tags:
- Entreprise
- Licenses
---

> HP a détaillé sa feuille de route pour ouvrir son système d'exploitation mobile à la communauté Open Source. Première étape : la mise à jour de son framework de développement Enyo.
