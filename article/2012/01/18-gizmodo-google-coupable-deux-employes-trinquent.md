---
site: GIZMODO
title: "Google coupable, deux employés trinquent"
author: Florence
date: 2012-01-18
href: http://www.gizmodo.fr/2012/01/18/google-coupable-deux-employes-trinquent.html
tags:
- Entreprise
- Associations
- RGI
- International
---

> Nous vous parlions hier de cette affaire où Google était accusé de vandalisme virtuel. Encore une histoire qui entache la réputation de la firme de Mountain View. Si le doute concernant sa participation était encore permis, Google a reconnu les faits.
