---
site: Mediapart
title: "Du gène égoïste au génie collaboratif"
author: Vincent Verschoore
date: 2012-01-07
href: http://blogs.mediapart.fr/blog/vincent-verschoore/070112/du-gene-egoiste-au-genie-collaboratif
tags:
- Entreprise
- Économie
- Innovation
---

> Ce billet est tiré d’une publication de la Harvard Business Review écrite par le Professeur de droit entrepreneurial Yochal Benkler. Harvard est la business school la plus réputée au monde et un temple de ce que l’on appelle le corporate management: comment utiliser un ensemble d’actifs humains, matériels et financiers pour générer le plus de profit possible au travers d’un ensemble de méthodes.
