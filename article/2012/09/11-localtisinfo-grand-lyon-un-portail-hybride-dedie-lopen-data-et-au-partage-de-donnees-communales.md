---
site: Localtis.info
title: "Grand Lyon: un portail hybride dédié à l'open data et au partage de données communales"
author: Philippe Parmantier
date: 2012-09-11
href: http://www.localtis.info/cs/ContentServer?pagename=Localtis/LOCActu/ArticleActualite&jid=1250264167384&cid=1250264164848
tags:
- Administration
- Open Data
---

> La communauté urbaine du Grand Lyon vient d'amorcer un processus d'ouverture des données publiques en lançant, début septembre, une plateforme en bêta test. Les choix retenus sont inédits. Le socle initial du dispositif repose en effet sur l'information géographique et non, comme dans la plupart des cas, sur un socle de données textuelles. En outre, le portail s'adresse autant à ses membres - en l'occurrence aux services des communes de l'agglomération, réunies au sein du Grand Lyon - qu'aux particuliers, aux associations et aux entreprises, sur le versant open data.
