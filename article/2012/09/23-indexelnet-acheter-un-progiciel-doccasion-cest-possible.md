---
site: indexel.net
title: "Acheter un progiciel d'occasion? C'est possible!"
author: Antoine Robin
date: 2012-09-23
href: http://www.indexel.net/actualites/acheter-un-progiciel-d-occasion-c-est-possible-3646.html
tags:
- Entreprise
- Institutions
- Licenses
- Europe
---

> La Cour de justice de l'Union européenne (CJUE) autorise l’utilisateur d’un progiciel à le revendre à un tiers. Certaines exigences doivent cependant être respectées et des zones d’ombre demeurent.
