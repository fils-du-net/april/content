---
site: internet ACTU.net
title: "L’âge de la prédation"
author: Dominique Boullier
date: 2012-09-07
href: http://www.internetactu.net/2012/09/07/l%E2%80%99age-de-la-predation
tags:
- Entreprise
- Économie
- Innovation
---

> Dominique Boullier, professeur de sociologie à Sciences Po et coordinateur scientifique du MediaLab nous propose une lecture du livre de Nicolas Colin et Henri Verdier, L’âge de la multitude, Entreprendre et gouverner après la révolution numérique, paru au printemps 2012 chez Armand Colin. Il en livre une critique sans concession, mais qui permet d’analyser et de prendre du recul sur la façon dont l’innovation est trop souvent célébrée.
