---
site: LeJournalduNet
title: "Nous proposons aux DSI de venir échanger à huis-clos sur leurs enjeux Open Source"
author: Antoine Crochet-Damais
date: 2012-09-05
href: http://www.journaldunet.com/solutions/dsi/open-cio-summit-2012.shtml
tags:
- Entreprise
- Administration
- Informatique en nuage
---

> L'Open CIO Summit 2012 est le principal événement annuel organisé en France sur l'Open Source pour les DSI. Son édition 2012 couvre des thèmes divers : gouvernance et stratégie Open Source, politique de support, agilité... Elle se tiendra le 11 octobre à Paris.
