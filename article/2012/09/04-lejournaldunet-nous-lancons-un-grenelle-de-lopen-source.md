---
site: LeJournalduNet
title: "Nous lançons un Grenelle de l'Open Source"
author: Antoine Crochet-Damais
date: 2012-09-04
href: http://www.journaldunet.com/solutions/saas-logiciel/patrice-bertrand-open-world-forum-2012.shtml
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- Informatique en nuage
---

> L'évènement regroupant l'écosystème français de l'Open Source se tiendra du 11 au 13 octobre prochain à Paris. Le gouvernement y est invité. Objectif: lancer une grande réflexion publique.
