---
site: Numerama
title: "Hadopi non grata"
author: Guillaume Champeau
date: 2012-09-27
href: http://www.numerama.com/magazine/23868-hadopi-non-grata.html
tags:
- Interopérabilité
- April
- HADOPI
- Associations
- DRM
---

> Dur dur d'être Hadopi. Lorsqu'il a créé la riposte graduée, le gouvernement avait cru avoir une bonne idée en réunissant dans une même entité sa nouvelle autorité administrative chargée de faire la morale et de menacer des millions d'internautes, et l'ancienne Autorité de Régulation des Mesures Techniques (ARMT), censée faire garde-fou aux excès des DRM. Or comme chacun a pu le sentir dès la préparation des débats parlementaires, le volet "riposte graduée" a cannibalisé toute tentative de l'Hadopi de paraître comme un arbitre neutre des différends entre consommateurs et ayants droit.
