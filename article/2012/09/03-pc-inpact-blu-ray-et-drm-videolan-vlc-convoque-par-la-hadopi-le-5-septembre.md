---
site: PC INpact
title: "Blu-Ray et DRM: Videolan (VLC) convoqué par la Hadopi le 5 septembre"
author: Marc Rees
date: 2012-09-03
href: http://www.pcinpact.com/news/73506-blu-ray-et-drm-vlc-entendu-par-hadopi-5-septembre.htm
tags:
- Interopérabilité
- HADOPI
- DADVSI
- DRM
- Droit d'auteur
---

> Selon nos informations, la Hadopi va recevoir ce 5 septembre les membres de Vidéolan, l'association qui édite le fameux lecteur multimédia VLC. L’objet? L’interopérabilité des verrous technologiques qui cadenassent les Blu-Ray. La Hadopi veut faire le point sur la question, alors que VLC attend une réponse concrète depuis un an déjà.
