---
site: Programmez.com
title: "L'État français opte pour le Logiciel Libre"
author: Frédéric Mazué
date: 2012-09-25
href: http://www.programmez.com/actualites.php?id_actu=12116
tags:
- Administration
- April
- Institutions
---

> Dans une circulaire adressée à tous les membres du gouvernement, le Premier ministre, Jean-Marc Ayrault, définit les orientations pour l'usage des logiciels libres dans l'administration.
