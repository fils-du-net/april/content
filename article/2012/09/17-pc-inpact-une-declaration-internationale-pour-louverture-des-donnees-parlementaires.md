---
site: PC INpact
title: "Une déclaration internationale pour l'ouverture des données parlementaires"
author: Xavier Berne
date: 2012-09-17
href: http://www.pcinpact.com/news/73885-une-declaration-internationale-pour-l-ouverture-donnees-parlementaires.htm
tags:
- Institutions
- Associations
- Contenus libres
- International
---

> Samedi, la Déclaration pour l’Ouverture et la Transparence Parlementaire (PDF) a été publiée à l’occasion de la conférence mondiale sur l’e-parlement. Ce texte, soutenu par plusieurs dizaines d’organisations de différents pays, vise à harmoniser au niveau international différentes règles relatives à l’activité parlementaire.
