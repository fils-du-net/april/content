---
site: Europe1.fr
title: "Logiciels libres: au service de la rigueur"
author: Benoist Pasteau
date: 2012-09-27
href: http://www.europe1.fr/Politique/Logiciels-libres-au-service-de-la-rigueur-1254537
tags:
- Administration
- Économie
- Institutions
---

> Pour diminuer les dépenses, le gouvernement recommande aux ministères de les utiliser.
