---
site: cnet France
title: "Doit-on écrire Linux ou GNU/Linux?"
author: Mathieu Chouchane
date: 2012-09-07
href: http://www.cnetfrance.fr/news/doit-on-ecrire-linux-ou-gnulinux-39772505.htm
tags:
- Philosophie GNU
---

> Doit-on écrire "Linux" ou "GNU/Linux"? Voici quelques éléments qui vous permettront de comprendre un débat qui déchire toute une communauté.
