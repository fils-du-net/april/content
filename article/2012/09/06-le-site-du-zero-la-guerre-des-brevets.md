---
site: le Site du Zéro
title: "La guerre des brevets"
author: Renault
date: 2012-09-06
href: http://www.siteduzero.com/news-62-45362-la-guerre-des-brevets.html
tags:
- Entreprise
- April
- Brevets logiciels
- Droit d'auteur
- Innovation
- Licenses
---

> La guerre des brevets est un phénomène qui existe depuis l'invention de ce concept, mais elle a pris en intensité depuis quelques années dans le secteur de l'informatique et des télécommunications. Mais avant de raconter ce qui se passe depuis quelques années, il est bon de savoir ce qu'est un brevet et de connaitre leurs limites.
