---
site: LeMagIT
title: "Numergy: Bull et SFR officialisent leur Andromède"
author: Cyrille Chausson
date: 2012-09-05
href: http://www.lemagit.fr/article/sfr-bull-cloud-computing/11740/1/numergy-bull-sfr-officialisent-leur-andromede/
tags:
- Entreprise
- Institutions
- Informatique en nuage
---

> SFR et Bull ont officiellement lancé commercialement leur société commune, Numergy. Concrétisant le premier projet de cloud national, initié par les investissements d’avenir du Grand Emprunt.
