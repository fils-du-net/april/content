---
site: PCWorld.fr
title: "Ballot-screen: Microsoft acceptera les sanctions"
author: Mathieu Chartier
date: 2012-09-10
href: http://www.pcworld.fr/logiciels/actualites,microsoft-ballot-screen-sanction,531467,1.htm
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Europe
---

> Microsoft attend le verdict de la Commission européenne dans l'affaire du ballot-screen...
