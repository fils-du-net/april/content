---
site: LeJournalduNet
title: "Open Source: le Premier ministre signe une circulaire inédite"
author: Dominique FILIPPONE
date: 2012-09-21
href: http://www.journaldunet.com/solutions/dsi/open-source-et-logiciel-libre-la-circulaire-du-premier-ministre-0912.shtml
tags:
- Administration
- Institutions
---

> Une circulaire signée jeudi 20 septembre par Jean-Marc Ayrault à destination de tous les membres du gouvernement encourage le choix de l'Open Source à tous les niveaux de l'Etat.
