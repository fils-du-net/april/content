---
site: L'USINE NOUVELLE
title: "Le brevet unitaire européen est-il ultralibéral?"
author: Aurélie Barbaux
date: 2012-09-11
href: http://www.usinenouvelle.com/article/le-brevet-unitaire-europeen-est-il-ultraliberal.N181707
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Innovation
- Europe
- International
---

> Bloqué dans sa dernière ligne droite entre la Commission européenne et le Parlement, le projet de brevet unitaire européen continue d’inquiéter les pays à faible intensité d’innovation technologique. À tord ou à raison.
