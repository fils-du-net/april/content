---
site: Computerworld
title: "French government outlines plans for free software adoption"
author: Peter Sayer
date: 2012-09-24
href: http://www.computerworld.com/s/article/9231681/French_government_outlines_plans_for_free_software_adoption
tags:
- Administration
- Économie
- April
- Institutions
- English
---

> (Les agences du gouvernement Français pourraient devenir des participantes plus actives des projets de Logiciels Libres) French government agencies could become more active participants in free software projects, under an action plan sent by Prime Minister Jean-Marc Ayrault in a letter to ministers, while software giants Microsoft and Oracle might lose out as the government pushes free software such as LibreOffice or PostgreSQL in some areas.
