---
site: ZDNet.fr
title: "Pour le cofondateur de Sun, l'open source a besoin d'un sponsor"
date: 2012-09-26
href: http://www.zdnet.fr/actualites/pour-le-cofondateur-de-sun-l-open-source-a-besoin-d-un-sponsor-39782936.htm
tags:
- Entreprise
- Économie
---

> Scott McNealy, cofondateur de Sun Microsystems, a accordé une interview à Techcrunch en marge d'une conférence américaine. Il revient notamment sur l'open source, avec une vision particulièrement pessimiste.
