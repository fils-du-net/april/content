---
site: France Info
title: "Les opérateurs mettent le cap sur la 4G"
date: 2012-09-25
href: http://www.franceinfo.fr/high-tech/nouveau-monde/les-operateurs-mettent-le-cap-sur-la-4g-748333-2012-09-25
tags:
- Administration
- Institutions
---

> Des logiciels libres dans les ministères français. L'idée n'est pas nouvelle mais le premier ministre, Jean-Marc Ayrault a signé il y a quelques jours une circulaire pour accélérer les choses. Les logiciels libre, c'est par exemple Linux. C'est le contraire des logiciels propriétaires comme Windows. Selon le gouvernement, le logiciel libre c'est bon pour la sécurité des systèmes en raison de sa plus grande transparence et aussi pour le budget de l'Etat car cela coûterait moins cher. Des logiciels libres sont déjà en service dans plusieurs ministères et dans la gendarmerie.
