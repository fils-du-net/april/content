---
site: Rue89
title: "MP3, ebooks... on a vérifié, ils ne vous appartiennent pas"
author: Martin Untersinger
date: 2012-09-26
href: http://www.rue89.com/rue89-culture/2012/09/26/mp3-ebooks-verifie-ils-ne-vous-appartiennent-pas-235508
tags:
- Entreprise
- Internet
- DRM
- Licenses
---

> Rue89 a mis son nez dans les conditions d’utilisation des grandes plateformes de vente de culture numérisée. Prêter un ebook ou un album à un ami? N’y pensez pas.
