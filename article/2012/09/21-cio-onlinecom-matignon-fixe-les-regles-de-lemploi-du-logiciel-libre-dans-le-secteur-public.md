---
site: "cio-online.com"
title: "Matignon fixe les règles de l'emploi du logiciel libre dans le secteur public"
author: Bertrand Lemaire
date: 2012-09-21
href: http://www.cio-online.com/actualites/lire-matignon-fixe-les-regles-de-l-emploi-du-logiciel-libre-dans-le-secteur-public-4553.html
tags:
- Administration
- Institutions
---

> Jean-Marc Ayrault vient de diffuser une circulaire à tous les ministères pour un bon usage du logiciel libre dans tous les services de l'Etat.
