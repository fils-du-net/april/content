---
site: PC INpact
title: "Pendant la guerre Apple-Samsung, l'Europe s'active sur le brevet unitaire"
author: Marc Rees
date: 2012-09-03
href: http://www.pcinpact.com/news/73526-pendant-guerre-apple-samsung-europe-sactive-sur-brevet-unitaire.htm
tags:
- April
- Brevets logiciels
- Innovation
- Europe
---

> La guerre entre Apple et Samsung sur le terrain des brevets tombe presque bien pour les adversaires de cette protection juridique: elle est le contre-exemple de ce qu’annonce un déploiement sans taquet des brevets en Europe.
