---
site: L'USINE NOUVELLE
title: "Cloudwatt, le cloud de Thales et Orange, veut croquer 10% du marché en 5 ans"
author: Ridha Loukil
date: 2012-09-06
href: http://www.usinenouvelle.com/article/cloudwatt-le-cloud-de-thales-et-orange-veut-croquer-10-du-marche-en-5-ans.N181465
tags:
- Entreprise
- Économie
- Informatique en nuage
---

> Après Numergy de SFR et Bull, c’est au tour de Cloudwatt, la coentreprise de Thales et Orange, de démarrer ses services de Cloud sécurisés. Objectif: 500 millions d’euros de chiffre d’affaires à l’horizon 2017.
