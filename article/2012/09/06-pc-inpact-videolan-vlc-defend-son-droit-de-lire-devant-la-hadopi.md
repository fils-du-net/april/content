---
site: PC INpact
title: "VideoLan (VLC) défend son droit de lire devant la Hadopi"
author: Marc Rees
date: 2012-09-06
href: http://www.pcinpact.com/news/73605-videolan-vlc-defend-son-droit-lire-devant-hadopi.htm
tags:
- Interopérabilité
- HADOPI
- Associations
- DRM
---

> Nous avons obtenu les premiers échos de la réunion organisée hier matin rue du Texel. L'objet était de faire le point sur cette saisine, ce qui n'a pas apporté grand-chose à l'éditeur de VLC. Plusieurs faits sont cependant à souligner.
