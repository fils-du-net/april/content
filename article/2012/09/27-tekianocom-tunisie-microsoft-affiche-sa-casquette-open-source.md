---
site: tekiano.com
title: "Tunisie: Microsoft affiche sa casquette open source!"
author: S.B.N
date: 2012-09-27
href: http://www.tekiano.com/tek/soft/6102-tunisie-microsoft-affiche-sa-casquette-open-source-.html
tags:
- Entreprise
- Internet
- Interopérabilité
- Informatique en nuage
- Europe
- International
---

> Une table ronde a réunit, mercredi 26 septembre, plusieurs représentants des communautés académiques et technologiques issues à la fois de Microsoft, du monde du logiciel libre et du Ministère des Nouvelles Technologies de l'Information et de la Communication.
