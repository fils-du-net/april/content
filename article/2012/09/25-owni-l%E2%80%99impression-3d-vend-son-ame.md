---
site: OWNI
title: "L’impression 3D vend son âme"
author: Sabine Blanc
date: 2012-09-25
href: http://owni.fr/2012/09/25/limpression-3d-vend-son-ame
tags:
- Entreprise
- Logiciels privateurs
- Matériel libre
- Licenses
---

> Le fabricant d'imprimante 3D grand public MakerBot incarnait la possibilité d'un business model basé sur l'open source. Mais la polémique enfle depuis le lancement ce mois-ci de leur nouveau modèle, qui est fermé. Certains l'accusent d'avoir renoncé à leur éthique sous la pression des investisseurs qui ont apporté 10 millions de dollars l'année dernière.
