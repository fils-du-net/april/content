---
site: LeMagIT
title: "Le Premier Ministre invite à l’usage du logiciel Libre dans l’Administration"
author: Cyrille Chausson
date: 2012-09-24
href: http://www.lemagit.fr/technologie/systemes-exploitation/linux/2012/09/24/le-premier-ministre-invite-a-lusage-du-logiciel-libre-dans-ladministration
tags:
- Administration
- April
- Institutions
---

> Le Premier Ministre Jean-Marc Ayrault a publié en fin de semaine dernière une circulaire invitant les ministères à considérer fortement les logiciels libres dans leurs choix informatique. Parmi les recommandations de cette circulaire, la contribution aux communautés est évoquée. Un document salué unanimement par l’éco-système du Libre en France.
