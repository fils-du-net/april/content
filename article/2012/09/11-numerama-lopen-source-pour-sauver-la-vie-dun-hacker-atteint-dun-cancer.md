---
site: Numerama
title: "L'Open Source pour sauver la vie d'un hacker atteint d'un cancer"
author: Guillaume Champeau
date: 2012-09-11
href: http://www.numerama.com/magazine/23685-l-open-source-pour-sauver-la-vie-d-un-hacker-atteint-d-un-cancer.html
tags:
- Internet
- Partage du savoir
- Sciences
- Standards
---

> Un hacker italien, militant de l'Open Source, a décidé de rendre publique la tumeur au cerveau dont il est atteint, et de craquer ses dossiers médicaux pour les diffuser au plus grand nombre dans un format ouvert, de façon à se donner les meilleures chances de trouver un remède.
