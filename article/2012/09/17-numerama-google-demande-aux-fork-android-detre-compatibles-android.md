---
site: Numerama
title: "Google demande aux fork Android d'être compatibles Android"
author: Guillaume Champeau
date: 2012-09-17
href: http://www.numerama.com/magazine/23752-google-demande-aux-fork-android-d-etre-compatibles-android.html
tags:
- Entreprise
- Associations
- Licenses
---

> Chez Google, l'open source et les licences libres ont leur limite. Le géant de Mountain View explique que les constructeurs qui sortent des smartphones ou tablettes sous un OS dérivé d'Android doivent assurer une "compatibilité" avec le système d'exploitation d'origine, sous peine de perdre le soutien de Google.
