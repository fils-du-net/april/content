---
site: Les Echos
title: "Le «cloud computing» prend un nouveau virage en France"
author: Romain Gueugneau
date: 2012-09-07
href: http://www.lesechos.fr/entreprises-secteurs/tech-medias/actu/0202251560356-le-cloud-computing-prend-un-nouveau-virage-en-france-359665.php
tags:
- Entreprise
- Internet
- Institutions
- Innovation
- Informatique en nuage
---

> Le projet Andromède a finalement débouché cette semaine sur la création de deux nouvelles sociétés soutenues par l'Etat à hauteur de 150 millions d'euros au total.
