---
site: Rue89
title: "Un cancéreux pirate son dossier médical pour demander l’aide du Web"
author: David Perrotin
date: 2012-09-15
href: http://www.rue89.com/2012/09/15/malade-un-hacker-pirate-son-dossier-medical-pour-demander-laide-du-web-235333
tags:
- Internet
- Administration
- Innovation
- Open Data
---

> Salvatore Laconesi, hacker et artiste, est atteint d’un cancer du cerveau. Lorsqu’il demande à l’hôpital d’obtenir son dossier médical, il le reçoit dans un format qui l’empêche de le consulter dans son intégralité. Il décide donc de le «craquer» et de le publier sur son site pour demander leur aide aux internautes.
