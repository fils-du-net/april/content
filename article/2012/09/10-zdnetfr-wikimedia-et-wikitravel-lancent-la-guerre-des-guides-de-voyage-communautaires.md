---
site: ZDNet.fr
title: "Wikimedia et Wikitravel lancent la guerre des guides de voyage communautaires"
date: 2012-09-10
href: http://www.zdnet.fr/actualites/wikimedia-et-wikitravel-lancent-la-guerre-des-guides-de-voyage-communautaires-39782325.htm
tags:
- Entreprise
- Institutions
- Associations
- Licenses
---

> Deux plaintes croisées ont été déposées entre l'éditeur de Wikitravel, deux contributeurs, et la fondation Wikimedia. L'enjeu: le contrôle des informations publiées sur les wikis open-source, et leurs droits d'utilisation sur des sites tiers.
