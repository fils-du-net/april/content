---
site: clubic.com
title: "Open source: une circulaire pour encourager son adoption"
author: Thomas Pontiroli
date: 2012-09-24
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/logiciel-libre-open-source/actualite-512621-open-source-circulaire-encourage-adoption.html
tags:
- Administration
- April
- Institutions
---

> Le Premier ministre, Jean-Marc Ayrault, a signé une circulaire dont l'objectif est d'inciter l'administration à adopter le logiciel libre plus largement. Il a été salué par plusieurs organisations.
