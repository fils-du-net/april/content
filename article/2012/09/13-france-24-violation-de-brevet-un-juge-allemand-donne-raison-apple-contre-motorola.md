---
site: FRANCE 24
title: "Violation de brevet: un juge allemand donne raison à Apple contre Motorola"
author: AFP
date: 2012-09-13
href: http://www.france24.com/fr/20120913-violation-brevet-juge-allemand-donne-raison-a-apple-contre-motorola
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> Le tribunal régional allemand de Munich (sud) a donné raison jeudi à Apple contre Motorola, reconnu coupable de violation de brevet avec ses smartphones et ses tablettes, a indiqué à l'AFP une porte-parole du tribunal.
