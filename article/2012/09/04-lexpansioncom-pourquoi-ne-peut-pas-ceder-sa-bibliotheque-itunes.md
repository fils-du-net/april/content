---
site: L'Expansion.com
title: "Pourquoi on ne peut pas céder sa bibliothèque iTunes"
author: Raphaële Karayan
date: 2012-09-04
href: http://lexpansion.lexpress.fr/high-tech/pourquoi-on-ne-peut-pas-ceder-sa-bibliotheque-itunes_331124.html
tags:
- Entreprise
- Économie
- Droit d'auteur
- Licenses
---

> La femme de Bruce Willis a beau avoir démenti que son mari voulait attaquer Apple, on ne peut toujours pas transférer le contenu de son compte sur un autre. Blandine Poidevin, avocate au cabinet Jurisexpert, nous explique pourquoi.
