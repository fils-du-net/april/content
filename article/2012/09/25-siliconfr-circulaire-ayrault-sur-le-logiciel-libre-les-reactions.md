---
site: Silicon.fr
title: "Circulaire Ayrault sur le logiciel libre: les réactions"
author: David Feugey
date: 2012-09-25
href: http://www.silicon.fr/circulaire-ayrault-logiciel-libre-reactions-78816.html
tags:
- Entreprise
- Administration
- April
- Institutions
- Associations
---

> Voici une compilation des réactions de plusieurs acteurs open source, suite à la publication de la circulaire Ayrault sur le logiciel libre: Alter Way, April, Dalibo, Linagora, Red Hat et Smile.
