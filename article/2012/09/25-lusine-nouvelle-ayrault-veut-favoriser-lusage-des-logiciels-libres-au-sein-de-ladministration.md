---
site: L'USINE NOUVELLE
title: "Ayrault veut favoriser l'usage des logiciels libres au sein de l'administration"
author: Christophe Guillemin
date: 2012-09-25
href: http://www.usinenouvelle.com/article/ayrault-veut-favoriser-l-usage-des-logiciels-libres-au-sein-de-l-administration.N182670
tags:
- Administration
- April
- Institutions
---

> Le Premier ministre demande aux services de l'Etat de porter une attention particulière aux solutions non propriétaires et met en avant les nombreux avantages du logiciel libre. Pour les défenseurs du libre: il s'agit d'un réel coup de pouce au déploiement du logiciel libre dans l'administration.
