---
site: ZDNet.fr
title: "Vers la fin de la possibilité d'avoir Linux en poste de travail dominant?"
author: Christophe Sauthier
date: 2012-09-17
href: http://www.zdnet.fr/blogs/ubuntu-co/vers-la-fin-de-la-possibilite-d-avoir-linux-en-poste-de-travail-dominant-39782551.htm
tags:
- Promotion
---

> C'est effectivement ce que je pense: Linux n'est pas en passe de devenir un acteur majeur du poste de travail... (vers les 10% de part de marché) Et je ne suis pas le seul à l'avoir constaté durant l'été!
