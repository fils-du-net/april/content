---
site: LE CERCLE Les Echos
title: "L’Open SaaS rend le Cloud plus libre"
author: Cyril Reinhard
date: 2012-09-18
href: http://lecercle.lesechos.fr/entreprises-marches/high-tech-medias/informatiques/221154435/open-saas-rend-cloud-plus-libre
tags:
- Entreprise
- Économie
- Informatique en nuage
---

> Alors que l’État appelle les plus hauts revenus à aider les entreprises à renouer avec la croissance, ces dernières doivent s’armer d’outils économiques et efficaces pour mener à bien des projets d’envergure. L’essor des offres SaaS est la preuve que les entreprises cherchent à réduire les coûts. En parallèle, la démocratisation de l'open source apporte une mobilité inouïe aux services IT.
