---
site: PC INpact
title: "Jean-Marc Ayrault signe une circulaire pour le libre dans les administrations"
author: Marc Rees
date: 2012-09-24
href: http://www.pcinpact.com/news/74035-jean-marc-ayrault-signe-circulaire-pour-libre-dans-administrations.htm
tags:
- Administration
- April
- Institutions
---

> Une première! Le premier ministre a publié au journal officiel une circulaire définissant les orientations pour l’usage du logiciel libre dans l’administration. Un poids politique fort applaudit par plusieurs groupements favorables au secteur, qui militent cependant pour une démarche encore plus ambitieuse.
