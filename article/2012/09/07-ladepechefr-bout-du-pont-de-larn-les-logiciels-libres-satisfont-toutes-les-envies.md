---
site: LADEPECHE.fr
title: "Bout-du-Pont-de-Larn. Les logiciels libres satisfont toutes les envies"
date: 2012-09-07
href: http://www.ladepeche.fr/article/2012/09/07/1434276-bout-du-pont-de-larn-les-logiciels-libres-satisfont-toutes-les-envies.html
tags:
- Administration
- Associations
---

> L'Association Informatique 'Lolipotou'reprend ses activités. Pendant l'année 2011/2012, l'association a vu le nombre de ses adhérents augmenter fortement. Ceci est dû principalement à la participation active de tous ses 52 membres, mais aussi au soutien de l'équipe municipale, ainsi qu'aux actions de formation rendues plus performantes par la configuration et l'entretien des PC des membres et de son parc de machines : 9 PC en réseau connectés en haut débit et deux portables auxquels deux autres sont venus s'ajouter.
