---
site: LeMagIT
title: "Microsoft, absent des 100 premières entreprises \"innovantes\" dans le monde"
author: Cyrille Chausson
date: 2012-09-07
href: http://www.lemagit.fr/article/microsoft-forbes/11758/1/microsoft-absent-des-100-premieres-entreprises-innovantes-dans-monde
tags:
- Entreprise
- Économie
- Innovation
---

> La capacité d’innovation de Microsoft n’a été retenue par Forbes dans la 2e édition de son classement des 100 premières entreprises les plus innovantes dans le monde. Un classement dominé par Salesforce, où l'IT et l’Internet occupent 6 places du Top 10.
