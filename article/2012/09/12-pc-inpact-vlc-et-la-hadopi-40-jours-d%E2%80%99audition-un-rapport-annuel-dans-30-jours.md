---
site: PC INpact
title: "VLC et la Hadopi: 40 jours d’audition, un rapport annuel dans 30 jours"
author: Marc Rees
date: 2012-09-12
href: http://www.pcinpact.com/news/73762-vlc-et-hadopi-40-jours-d-audition-rapport-annuel-dans-30-jours.htm
tags:
- Interopérabilité
- April
- HADOPI
- Associations
- DRM
---

> Sur Twitter, Éric Walter a expliqué que «le collège #hadopi a validé aujourd'hui le lancement d'un cycle d'auditions sur les questions soulevées par la saisine» de VLC. Ce 11 septembre, le collège se réunissait en effet pour aborder ce dossier transmis par Videloan au printemps dernier. Le secrétaire général de la Hadopi a précisé qu'un cycle d'auditions était maintenant programmé pour une période de 40 jours.
