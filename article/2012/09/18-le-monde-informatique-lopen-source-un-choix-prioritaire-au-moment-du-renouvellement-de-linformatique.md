---
site: Le Monde Informatique
title: "L'Open Source, un choix prioritaire au moment du renouvellement de l'informatique d'entreprise"
author: Jean Elyan
date: 2012-09-18
href: http://www.lemondeinformatique.fr/actualites/lire-l-open-source-un-choix-prioritaire-au-moment-du-renouvellement-de-l-informatique-d-entreprise-50480.html
tags:
- Entreprise
- Économie
- Promotion
---

> Les logiciels Open Source peuvent remplacer les systèmes propriétaires, même dans des domaines exigeants comme le traitement des transactions selon un rapport commandé par Amadeus IT Group.
