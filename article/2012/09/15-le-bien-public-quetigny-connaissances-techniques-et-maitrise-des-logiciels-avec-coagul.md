---
site: LE BIEN PUBLIC
title: "Quetigny: connaissances techniques et maîtrise des logiciels avec Coagul"
date: 2012-09-15
href: http://www.bienpublic.com/grand-dijon/2012/09/15/connaissances-techniques-et-maitrise-des-logiciels-avec-coagul
tags:
- Associations
- Promotion
---

> L’association Coagul (Côte-d’Or, Association générale des utilisateurs de logiciels libres) a repris ses activités au centre social et culturel Léo-Lagrange.
