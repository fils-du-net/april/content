---
site: ActuaLitté.com
title: "Californie: une bibliothèque numérique de manuels scolaires gratuits"
author: Nicolas Gary
date: 2012-09-02
href: http://www.actualitte.com/education-international/californie-une-bibliotheque-numerique-de-manuels-scolaires-gratuits-36425.htm
tags:
- Entreprise
- Internet
- Éducation
- Contenus libres
---

> Les deux projets de loi n'attendent plus que la ratification du gouverneur
