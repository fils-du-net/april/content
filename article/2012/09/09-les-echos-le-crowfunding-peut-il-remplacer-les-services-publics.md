---
site: Les Echos
title: "Le crowfunding peut-il remplacer les services publics?"
author: Hubert Guillaud
date: 2012-09-09
href: http://blogs.lesechos.fr/internetactu-net/le-crowfunding-peut-il-remplacer-les-services-publics-a11588.html
tags:
- Internet
- Administration
- Économie
- Associations
- Innovation
---

> Le financement participatif (crowfunding ou “financement par la foule”) est un marché en pleine expansion, expliquait le rapport de Crowdsourcing.org. Les 452 plateformes de financement participatif dénombrées dans le monde en avril 2012 ont récolté 1,1 milliard de dollars de fonds en 2011 pour les projets qu’elles accueillaient – dont plus de la moitié pour des projets hébergés par des plateformes européennes.
