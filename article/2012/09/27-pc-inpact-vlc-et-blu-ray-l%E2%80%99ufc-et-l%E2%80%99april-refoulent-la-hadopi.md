---
site: PC INpact
title: "VLC et Blu-Ray: l’UFC et l’April refoulent la Hadopi"
author: Marc Rees
date: 2012-09-27
href: http://www.pcinpact.com/news/74165-vlc-et-blu-ray-l-ufc-et-l-april-refoulent-hadopi.htm
tags:
- Internet
- Interopérabilité
- April
- HADOPI
- Associations
- DRM
---

> Info PC INpact: les auditions se compliquent pour la Hadopi. Elle qui souhaitait entendre le monde du libre et les consommateurs, avant de se pencher sur le cas VLC, devra faire sans l’April ou l’UFC-Que Choisir. Et donc trancher au plus vite le problème soulevé par Videolan.
