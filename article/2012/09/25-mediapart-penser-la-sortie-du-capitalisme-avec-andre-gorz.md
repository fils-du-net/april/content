---
site: Mediapart
title: "Penser la sortie du capitalisme avec André Gorz"
author: Christophe Fourel
date: 2012-09-25
href: http://blogs.mediapart.fr/edition/les-invites-de-mediapart/article/250912/penser-la-sortie-du-capitalisme-avec-andre-gorz
tags:
- Économie
- Institutions
- Sciences
---

> Gorz ne disait pas que ces transformations se produiraient. Il disait seulement que, pour la première fois, nous pouvons vouloir qu’elles se réalisent. C’est la raison pour laquelle il soutenait depuis longtemps les initiatives de l’économie solidaire. C’est pourquoi aussi il suivait attentivement les actions des hackers et le développement des “logiciels libres”, capables, selon lui, de miner à la base le capitalisme en menaçant les monopoles.
