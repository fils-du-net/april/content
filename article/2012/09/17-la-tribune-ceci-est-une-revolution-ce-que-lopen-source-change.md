---
site: LA TRIBUNE
title: "Ceci est une Révolution: ce que l'Open Source a changé"
author: Patrice Bertrand
date: 2012-09-17
href: http://www.latribune.fr/opinions/tribunes/20120917trib000719886/ceci-est-une-revolution-ce-que-l-open-source-a-change-.html
tags:
- Économie
- Innovation
- Licenses
- Philosophie GNU
---

> Les 11, 12 et 13 octobre prochains se tiendra à Paris, l'Open World Forum, un événement de renommée internationale dédié à l'open source et aux approches ouvertes. L'open source est une idée qui a pris naissance dans le monde du logiciel, mais a inspiré et bousculé bien d'autres domaines. Tout comprendre en quelques clics sur une révolution qui bien au-delà de l'informatique, touche l'ensemble de la société...
