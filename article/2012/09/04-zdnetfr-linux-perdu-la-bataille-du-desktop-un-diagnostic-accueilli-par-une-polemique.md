---
site: ZDNet.fr
title: "Linux a perdu la bataille du desktop: un diagnostic accueilli par une polémique"
author: Christophe Auffray
date: 2012-09-04
href: http://www.zdnet.fr/actualites/linux-a-perdu-la-bataille-du-desktop-un-diagnostic-accueilli-par-une-polemique-39775728.htm
tags:
- Sensibilisation
---

> Ce constat, largement contesté, d’un des créateurs de Gnome, Miguel de Icaza, a lancé un vif débat au sein de la communauté Linux. Une dispute qui vire progressivement à la critique assassine anti-Gnome.
