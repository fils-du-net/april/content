---
site: LE FIGARO.fr
title: "Acta: la neutralité du Net fragilisée à Bruxelles"
author: Marie-Catherine Beuth
date: 2012-05-16
href: http://www.lefigaro.fr/hightech/2012/05/16/01007-20120516ARTFIG00698-acta-la-neutralite-du-net-fragilisee-a-bruxelles.php
tags:
- Internet
- HADOPI
- Institutions
- Droit d'auteur
- Neutralité du Net
- Europe
- International
- ACTA
---

> L'esprit répressif du traité anticontrefaçon pourrait survivre à une non-ratification par le Parlement européen.
