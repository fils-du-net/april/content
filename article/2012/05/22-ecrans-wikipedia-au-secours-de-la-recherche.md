---
site: écrans
title: "Wikipédia au secours de la recherche?"
author: Emilie Massemin
date: 2012-05-22
href: http://www.ecrans.fr/Wikipedia-au-secours-de-la,14725.html
tags:
- Entreprise
- Internet
- Administration
- Économie
- Interopérabilité
- Partage du savoir
- Institutions
- Éducation
- Sciences
- Open Data
---

> Le savoir coûte cher, trop cher, surtout lorsqu’il est compilé dans des revues scientifiques honteusement onéreuses. Fort de cette constatation, David Willetts, le ministre britannique des universités et de la science, a récemment contacté Jimmy Wales, fondateur de Wikipédia. Objectif: faire aboutir le Gateway to Research Project («Projet de passerelle vers la recherche»), «une plateforme en ligne permettant à chacun de consulter gratuitement et sans condition toutes les publications subventionnées par l’État britannique».
