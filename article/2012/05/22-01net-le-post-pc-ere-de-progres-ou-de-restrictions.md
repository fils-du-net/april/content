---
site: 01net.
title: "Le post-PC, ère de progrès ou de restrictions?"
author: Pierre Fontaine
date: 2012-05-22
href: http://www.01net.com/editorial/566475/six-raisons-desperer-que-le-pc-a-encore-un-avenir/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- HADOPI
- DRM
- Informatique en nuage
---

> L'ère post-PC est là, avec en son cœur le credo de la simplicité et des usages. Mais à penser les usages, à contrôler les plateformes tant matérielles que logicielles, n'y a-t-il pas un risque que l'utilisateur perde ses libertés informatiques essentielles, dont celle du choix?
