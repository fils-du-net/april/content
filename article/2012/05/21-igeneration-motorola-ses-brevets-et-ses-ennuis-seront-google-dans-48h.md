---
site: igeneration
title: "Motorola, ses brevets et ses ennuis seront à Google dans 48h"
author: Florian Innocente
date: 2012-05-21
href: http://www.igeneration.fr/0-apple/motorola-ses-brevets-et-ses-ennuis-appartiendront-google-dans-48h-91172
tags:
- Entreprise
- Économie
- Institutions
- Brevets logiciels
---

> Motorola Mobility, dont l'histoire se confond avec celle du téléphone mobile, deviendra propriété de Google d'ici deux jours
