---
site: Le nouvel Observateur
title: "Hollande et le numérique: un programme décevant alors qu'il y a beaucoup à faire"
author: Ludovic Pénet
date: 2012-05-07
href: http://leplus.nouvelobs.com/contribution/547309-hollande-et-le-numerique-un-programme-decevant-alors-qu-il-y-a-beaucoup-a-faire.html
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Droit d'auteur
- Innovation
- Promotion
- RGI
- Sciences
- Europe
- International
---

> Les candidats à la présidentielle n'ont pas mesuré l'enjeu économique majeur qui réside dans la promotion du numérique. Le programme de François Hollande en la matière manque d'audace, alors qu'il y a chaque jour de nouveaux défis à relever. C'est en tout cas l'avis de Ludovic Pénet, ancien du Laboratoire des Idées du PS, qui a écrit ce billet avant l'annonce des résultats du second tour.
