---
site: Le Monde Informatique
title: "Affaire Java/Android: Google demande un nouveau procès"
author: Jean Elyan
date: 2012-05-10
href: http://www.lemondeinformatique.fr/actualites/lire-affaire-java-android-google-demande-un-nouveau-proces-48859.html
tags:
- Entreprise
- Institutions
- Droit d'auteur
- International
---

> Selon un document déposé mardi soir devant le Tribunal de Californie, Google cherche à obtenir la tenue d'un nouveau procès dans l'affaire qui l'oppose à Oracle concernant la violation de la propriété intellectuelle dans le système d'exploitation mobile Android.
