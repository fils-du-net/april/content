---
site: LeJournalduNet
title: "Carole Jardon (Salon Solutions Linux) \"La mobilité est une lame de fond dans l'Open Source en 2012\""
author: Antoine CROCHET-DAMAIS
date: 2012-05-11
href: http://www.journaldunet.com/solutions/dsi/carole-jardon-salon-solutions-linux-2012.shtml?f_id_newsletter=6885
tags:
- Entreprise
- Internet
- Associations
- Innovation
- Informatique en nuage
---

> Mobilité, collaboratif, réseaux sociaux d'entreprise... Le point sur les grandes tendances de l'Open Source cette année, et les thèmes au programme du salon Solutions Linux qui ouvrira ses portes le 19 juin prochain.
