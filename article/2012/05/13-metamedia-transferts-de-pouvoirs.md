---
site: Metamedia
title: "Transferts de pouvoirs"
author: Eric Scherer
date: 2012-05-13
href: http://meta-media.fr/2012/05/13/transferts-de-pouvoirs/
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Innovation
- Neutralité du Net
- Video
- Open Data
---

> Les Mayas avaient donc raison! 2012 marque la fin d’un cycle et la disparition progressive d’un monde. Car «le basculement de pouvoirs le plus important à l’œuvre actuellement ne se passe pas entre l’Occident et l’Asie, les Etats-Unis et la Chine, le Nord et le Sud, la Droite et la Gauche, mais entre les institutions et les individus, grâce au numérique».
