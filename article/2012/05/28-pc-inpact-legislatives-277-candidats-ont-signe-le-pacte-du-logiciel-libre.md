---
site: PC INpact
title: "Législatives: 277 candidats ont signé le Pacte du Logiciel Libre"
author: Marc Rees
date: 2012-05-28
href: http://www.pcinpact.com/news/71197-pacte-logiciel-libre-candidatsfr-april.htm
tags:
- Internet
- Administration
- April
- Partage du savoir
- Institutions
- Droit d'auteur
---

> À proximité des élections législatives des 10 et 17 juin 2012, la campagne lancée par Candidats.fr se poursuit avec toujours la même et unique prétention: déceler chez les candidats une prise de conscience et mettre en lumière les futurs engagements en faveur du logiciel libre. Ils sont d’ores et déjà 227 candidats à avoir signé le Pacte du Logiciel Libre.
