---
site: écrans
title: "Pierre Lescure, des intérêts en question"
author: Sophian Fanen
date: 2012-05-24
href: http://www.ecrans.fr/Pierre-Lescure-des-interets-en,14743.html
tags:
- Entreprise
- Internet
- Administration
- Économie
- HADOPI
- Institutions
- DRM
- Droit d'auteur
---

> À Écrans.fr, on avait râlé (au bas mot) lorsque Denis Olivennes, alors PDG de la Fnac après avoir été directeur général de Canal+ France, avait été nommé à la tête de la commission qui a préconisé la riposte graduée. On avait aussi déploré le choix de Patrick Zelnick, patron de la maison de disques Naïve, pour mener le rapport «Création et Internet». Ces deux hommes venaient du sérail pour discuter avec le sérail, avec toutes les questions associées sur leur indépendance d’esprit et leur perméabilité aux discours des lobbys du secteur.
