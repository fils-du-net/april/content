---
site: Silicon.fr
title: "Optimis: boite à outils open source pour déployer le cloud"
author: Yves Grandmontagne
date: 2012-05-11
href: http://www.silicon.fr/projet-europeen-optimis-open-source-cloud-74510.html
tags:
- Entreprise
- Internet
- Administration
- Institutions
- Innovation
- Standards
- Informatique en nuage
- Europe
---

> Le projet Optimis a livré des logiciels open source pour aider les fournisseurs à construire et exécuter leurs applications dans le cloud.
