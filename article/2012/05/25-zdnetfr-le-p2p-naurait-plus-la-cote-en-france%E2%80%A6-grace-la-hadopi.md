---
site: ZDNet.fr
title: "Le P2P n'aurait plus la cote en France… grâce à la Hadopi?"
author: Guénaël Pépin
date: 2012-05-25
href: http://www.zdnet.fr/actualites/le-p2p-n-aurait-plus-la-cote-en-france-grace-a-la-hadopi-39772119.htm
tags:
- Entreprise
- Internet
- HADOPI
---

> Trois chercheurs de l'université Rennes 1 ont mené une nouvelle étude sur l'impact de la Hadopi sur les usages des internautes. Si le P2P est bien en baisse, la Haute autorité n'en serait pas la seule cause.
