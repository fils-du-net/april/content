---
site: ITRmanager.com
title: "Facebook ou la défaillance de marché?"
date: 2012-05-18
href: http://www.itrmanager.com/tribune/132565/facebook-defaillance-marche.html
tags:
- Entreprise
- Internet
- Administration
- Économie
- HADOPI
- Institutions
- Associations
- Innovation
- Open Data
---

> Au jour de la monumentale IPO de Facebook qui a d'avance épuisé tous les superlatifs des commentateurs, il est peut-être intéressant de se pencher à nouveau sur la question des données, les data qui de soigneusement domestiquées dans leurs bases, voire leurs fermes, principalement sous la férule agraire des départements informatique des grandes entreprises acquièrent aujourd'hui sous les épithètes de Big, d'Open et de private de nouvelles qualités, une seconde nature.
