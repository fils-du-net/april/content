---
site: PC INpact
title: "Pierre Lescure, un pied dans Hadopi, un oeil dans les DRM"
author: Marc Rees
date: 2012-05-23
href: http://www.pcinpact.com/news/71079-pierre-lescure-drm-hadopi-nagra.htm
tags:
- Internet
- Administration
- Économie
- HADOPI
- Institutions
- Brevets logiciels
- DADVSI
- DRM
---

> Pierre Lescure s’est vu confier hier une patate chaude: mettre en place et piloter une mission de concertation sur la Hadopi. Elle devrait durer 6 mois selon le calendrier défini par Fleur Pellerin, ministre déléguée à l’économie numérique. Un calendrier qu’a cependant écarté Aurélie Filippetti, ministre de la Culture… Durant cette période, le temps sera en tout cas celui des écoutes. Des auditions des acteurs de la filière et des consommateurs notamment seront organisées pour tenter de rabibocher les univers.
