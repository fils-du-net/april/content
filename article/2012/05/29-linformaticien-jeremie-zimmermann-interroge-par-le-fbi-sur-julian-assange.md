---
site: L'INFORMATICIEN
title: "Jérémie Zimmermann interrogé par le FBI sur Julian Assange"
author: Orianne Vatin
date: 2012-05-29
href: http://www.linformaticien.com/actualites/id/24985/jeremie-zimmermann-interroge-par-le-fbi-sur-julian-assange.aspx
tags:
- Internet
- April
- RGI
- Video
- Europe
- ACTA
---

> Alors qu'il était sur le territoire américain, le co-fondateur de la Quadrature du Net a été interpellé par les autorités locales, qui l'ont interrogé au sujet de Wikileaks, et de son sulfureux fondateur: Julian Assange. Il est depuis rentré en France.
