---
site: Maghreb Emergent
title: "L'Open source fait son petit bonhomme de chemin en Algérie"
author: Abdelkader Zahar
date: 2012-05-29
href: http://www.maghrebemergent.info/high-tech/83-it/12691-abdelkader-eddoud-lopen-source-fait-son-petit-bonhomme-de-chemin-en-algerie.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Économie
- Institutions
- Éducation
- Innovation
- Promotion
- International
---

> Libres mais pas gratuits, les logiciels Open Source ne font pas encore recette en Algérie. Leur utilisation est cantonnée auprès des spécialistes et des étudiants. La communauté GNU/Linux, et autres partisans des systèmes d’exploitation libres tentent d’élargir son audience, freinée par l’étendue du piratage des logiciels propriétaires. Abdelkader Eddoud, directeur du Campus numérique d'Alger, nous en parle. Il invite, ceux qui le veulent, à venir le 2 juin prochain, à l’université de Bab Ezzouar, muni de leurs ordinateurs. «Ils seront libérés», promet-il!
