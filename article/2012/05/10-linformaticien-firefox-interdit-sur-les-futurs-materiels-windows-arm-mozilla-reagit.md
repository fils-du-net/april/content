---
site: L'INFORMATICIEN
title: "Firefox interdit sur les futurs matériels Windows ARM? Mozilla réagit"
author: Emilien Ercolani
date: 2012-05-10
href: http://www.linformaticien.com/actualites/id/24770/firefox-interdit-sur-les-futurs-materiels-windows-arm-mozilla-reagit.aspx
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Institutions
- Associations
---

> Pourra-t-on choisir son navigateur sur les futurs mobiles Windows? Mozilla a vu rouge quand il a découvert qu’il ne sera pas possible d’installer un autre navigateur qu’Internet Explorer avec Windows sur une machine ARM.
