---
site: 01net.
title: "Le Parti Pirate allemand remporte vingt sièges au parlement de Rhénanie du Nord - Westphalie"
author: Benjamin Gourdet
date: 2012-05-14
href: http://www.01net.com/editorial/566135/le-parti-pirate-allemand-remporte-vingt-sieges-de-deputes/
tags:
- Internet
- Institutions
- International
---

> En Allemagne, le Parti pirate vient de remporter 7,6 % des suffrages dans la région de Rhénanie du Nord-Westphalie. Un score de bon augure en prévision des législatives nationales qui auront lieu dans dix-huit mois.
