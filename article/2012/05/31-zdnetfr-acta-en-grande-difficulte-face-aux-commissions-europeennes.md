---
site: ZDNet.fr
title: "ACTA en grande difficulté face aux commissions européennes"
author: Vincent Pierrot
date: 2012-05-31
href: http://www.zdnet.fr/actualites/acta-en-grande-difficulte-face-aux-commissions-europeennes-39772325.htm
tags:
- Internet
- Institutions
- Associations
- Europe
- ACTA
---

> Nouvelles débâcles pour le traité anti-contrefaçon: ce jeudi 31 mai au matin, trois commissions européennes ont voté contre le traité.
