---
site: ActuaLitté.com
title: "Journée mondiale contre les DRM, en croissance sur les ebooks"
author: Nicolas Gary
date: 2012-05-04
href: http://www.actualitte.com/actualite/lecture-numerique/usages/journee-mondiale-contre-les-drm-en-croissance-sur-les-ebooks-33920.htm
tags:
- Entreprise
- April
- Associations
- DADVSI
- DRM
- International
---

> DRM=MRD (mais plus vraiment MDR...)
