---
site: LeDevoir.com
title: "Logiciels libres - L'équivalent de la hausse en économies!"
author: Benoît des Ligneris, Cyrille Béraud, Daniel Pascot
date: 2012-05-28
href: http://www.ledevoir.com/politique/quebec/351050/logiciels-libres
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Éducation
- International
---

> Lettre ouverte à la ministre de l’Éducation et présidente du Conseil du trésor, Michelle Courchesne.
