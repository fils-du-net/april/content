---
site: LE FIGARO.fr
title: "Élections: Bugs en série pour le vote par Internet des expatriés"
author: Chloé Woitier
date: 2012-05-24
href: http://elections.lefigaro.fr/presidentielle-2012/2012/05/24/01039-20120524ARTFIG00676-bugs-en-serie-pour-le-vote-par-internet-des-expatries.php
tags:
- Internet
- Institutions
- Vote électronique
- International
---

> Une mise à jour de Java, logiciel indispensable pour le vote par Internet des expatriés, sème la pagaille dans le scrutin. Pour pouvoir voter, les internautes bloqués devront utiliser un autre ordinateur.
