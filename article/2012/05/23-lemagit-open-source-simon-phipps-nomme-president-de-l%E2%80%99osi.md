---
site: LeMagIT
title: "Open Source: Simon Phipps, nommé président de l’OSI"
author: Cyrille Chausson
date: 2012-05-23
href: http://www.lemagit.fr/article/nomination-open-source/11098/1/open-source-simon-phipps-nomme-president-osi/
tags:
- Entreprise
- Internet
- Associations
---

> L’Open Source Initiative, l’organisation en charge de la promotion de l’Open Source et garant de la validité du sceau Open Source des licences, a nommé Simon Phipps au poste de président. Cette nomination s’inscrit dans un changement de l’ensemble du conseil d’administration de l’organisation.
