---
site: indexel.net
title: "Périphériques mobiles: la guerre des brevets bat son plein"
author: Marie Varandat
date: 2012-05-23
href: http://www.indexel.net/actualites/peripheriques-mobiles-la-guerre-des-brevets-bat-son-plein-3604.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> Bras de fer entre Microsoft et Motorola, confrontation musclée entre Apple et Samsung… quand un marché affiche une croissance à deux chiffres, tous les coups sont permis pour défendre son pré carré!
