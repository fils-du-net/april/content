---
site: LeMagIT
title: "Hadoop: un marché de 813 M$ en 2016"
author: Cyrille Chausson
date: 2012-05-09
href: http://www.lemagit.fr/article/open-source-hadoop/11008/1/hadoop-marche-813-2016/
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Associations
- Informatique en nuage
---

> Pour IDC, le marché de Hadoop et MapReduce devrait croître de plus de 60% par an jusqu’en 2016 pour s’établir à presque 813 millions de dollars à cette date. Le cabinet estime toutefois que cette progression sera limitée, d’un point vue économique, par la pénurie de compétences et par une concurrence acerbe entre Open Source et éditeurs propriétaires.
