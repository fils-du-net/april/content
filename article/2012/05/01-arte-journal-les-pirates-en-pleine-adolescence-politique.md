---
site: ARTE Journal
title: "Les pirates en pleine adolescence politique"
author: Véronique Barondeau
date: 2012-05-01
href: http://www.arte.tv/fr/Les-pirates-en-pleine-adolescence-politique/6629938.html
tags:
- Internet
- Économie
- Institutions
- Brevets logiciels
- Marchés publics
- Neutralité du Net
- RGI
- International
---

> Six ans. Il leur a fallu six ans à peine pour passer du stade de groupuscule à celui de trouble-fête du jeu politique allemand. Elections après élections, le parti des Pirates fait de bons scores. En quelques années, il a fait une ascension fulgurante, et compte désormais 25 000 adhérents. Mais plus il grossit, plus il est suspect aux yeux de certains, et pas uniquement aux yeux des partis traditionnels.
