---
site: Numerama
title: "ACTA: les opposants se félicitent du rejet des commissions parlementaires"
author: Julien L.
date: 2012-05-31
href: http://www.numerama.com/magazine/22757-acta-les-opposants-se-felicitent-du-rejet-des-commissions-parlementaires.html
tags:
- April
- Institutions
- Associations
- Innovation
- Europe
- ACTA
---

> Les votes des commissions ITRE (industrie), JURI (affaires juridiques) et LIBE (libertés civiles) ont été accueillis avec satisfaction par les opposants à l'accord commercial anti-contrefaçon (ACTA). Mais tous rappellent qu'il ne s'agit que d'une étape. Deux autres commissions doivent encore donner leur avis, tandis que le vote final est programmé pour début juillet. Rien n'est joué, d'autant que les votes ont été très serrés.
