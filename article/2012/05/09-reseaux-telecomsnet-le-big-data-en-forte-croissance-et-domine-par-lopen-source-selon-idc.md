---
site: "Reseaux-Telecoms.net"
title: "Le Big Data en forte croissance et dominé par l'Open Source, selon IDC"
author: Jean Pierre Blettner
date: 2012-05-09
href: http://www.reseaux-telecoms.net/actualites/lire-le-big-data-en-forte-croissance-et-domine-par-l-open-source-selon-idc-23907.html
tags:
- Entreprise
- Économie
---

> L'éco système mondial pour les logiciels Hadoop-MapReduce devrait passer de 77 millions de dollars en 2011 à 812,8 millions de dollars en 2016, avec une croissance moyenne annuelle de 60,2%. «Hadoop et MapReduce sont une tempête dans l'univers des logiciels.
