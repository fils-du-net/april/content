---
site: La gazette.fr
title: "Open data: la transparence démocratique demeure virtuelle"
author: S. Maréchal
date: 2012-05-14
href: http://www.lagazettedescommunes.com/113394/open-data-la-transparence-democratique-demeure-virtuelle/
tags:
- Internet
- Administration
- Économie
- Associations
- Innovation
- Open Data
---

> Les collectivités pionnières ont enclenché un mouvement irréversible, mais les effets de l’open data sur la transparence de l’action publique territoriale se font attendre. La culture de la donnée doit sortir du microcosme technologique pour intéresser les citoyens.
