---
site: LeMonde.fr
title: "Sévère revers pour le traité ACTA dans les commissions du Parlement européen"
date: 2012-05-31
href: http://www.lemonde.fr/technologies/article/2012/05/31/severe-revers-pour-le-traite-acta-dans-les-commissions-du-parlement-europeen_1710688_651865.html
tags:
- Internet
- Institutions
- Europe
- ACTA
---

> Les commissions des libertés, de l'industrie et des affaires juridiques ont émis du Parlement européen des avis négatifs sur ce texte de lutte contre la contrefaçon.
