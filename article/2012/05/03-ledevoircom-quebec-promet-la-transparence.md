---
site: LeDevoir.com
title: "Québec promet la transparence"
author: Fabien Deglise
date: 2012-05-03
href: http://www.ledevoir.com/politique/quebec/349082/quebec-promet-la-transparence
tags:
- Internet
- Administration
- Institutions
- Éducation
- Marchés publics
- Standards
- International
- Open Data
---

> Un site Web publiera l’état d’avancement des projets du gouvernement
