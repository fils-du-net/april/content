---
site: Silicon.fr
title: "Java sous Android: Google l'emporte face à Oracle"
author: Ariane Beky
date: 2012-05-24
href: http://www.silicon.fr/java-android-google-emporte-oracle-75002.html
tags:
- Entreprise
- Internet
- Institutions
- Brevets logiciels
- Droit d'auteur
---

> Le tribunal fédéral de San Francisco a jugé que la société Internet ne s’était pas rendue coupable de violation de brevets en utilisant dans son OS mobile Android les technologies Java, dont le géant du logiciel d’entreprise, Oracle, est propriétaire.
