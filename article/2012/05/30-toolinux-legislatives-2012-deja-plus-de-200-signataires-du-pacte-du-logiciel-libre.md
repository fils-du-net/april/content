---
site: toolinux
title: "Législatives 2012: déjà plus de 200 signataires du Pacte du Logiciel Libre"
date: 2012-05-30
href: http://www.toolinux.com/Legislatives-2012-deja-plus-de-200
tags:
- April
- Institutions
- Sensibilisation
---

> Pour les élections législatives les 10 et 17 juin, la campagne Candidats.fr de l’April continue. Plus de 200 candidats à la députation ont déjà signé le Pacte du Logiciel Libre.
