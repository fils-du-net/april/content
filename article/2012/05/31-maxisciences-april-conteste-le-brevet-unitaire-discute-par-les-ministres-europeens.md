---
site: Maxisciences
title: "April conteste le brevet unitaire discuté par les ministres européens"
date: 2012-05-31
href: http://www.maxisciences.com/brevet/april-conteste-le-brevet-unitaire-discute-par-les-ministres-europeens_art24788.html
tags:
- Entreprise
- Internet
- April
- Institutions
- Brevets logiciels
- Innovation
- Europe
---

> Les ministres européens des affaires économiques, de l’industrie et de la recherche se sont réunis, mercredi 30 mai, pour débattre du projet de brevet unitaire. Lancé en avril 2011, il vise à coordonner les procèdures de dépôt de brevets. Une idée vivement critiquée par l'association de promotion et la défense du logiciel libre April.
