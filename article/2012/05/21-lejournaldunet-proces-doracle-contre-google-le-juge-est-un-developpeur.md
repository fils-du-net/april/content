---
site: LeJournalduNet
title: "Procès d'Oracle contre Google: le juge est un développeur!"
author: Antoine CROCHET-DAMAIS
date: 2012-05-21
href: http://www.journaldunet.com/developpeur/java-j2ee/proces-d-oracle-contre-google-0512.shtml
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Droit d'auteur
---

> William Alsup est un développeur. C'est ce que révèle un échange entre l'avocat d'Oracle et ce juge en charge d'instruire le procès intenté contre Google par Oracle, pour violation de brevets.
