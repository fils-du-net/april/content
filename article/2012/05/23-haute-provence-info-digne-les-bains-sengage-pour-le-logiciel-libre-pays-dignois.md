---
site: "Haute-Provence Info"
title: "Digne-les-Bains s'engage pour le logiciel libre - Pays dignois"
date: 2012-05-23
href: http://www.hauteprovenceinfo.com/23052012Digne-les-Bains-s39engage-pour-le-logiciel-libre,.media?a=373
tags:
- Internet
- Administration
- Partage du savoir
- Open Data
---

> Demain, jeudi 24 mai, la Ville de Digne invite ses habitants à participer à l'enrichissement de la carte de la commune sur le logiciel libre Open Street Map.
