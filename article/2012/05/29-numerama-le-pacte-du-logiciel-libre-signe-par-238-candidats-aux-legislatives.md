---
site: Numerama
title: "Le pacte du logiciel libre signé par 238 candidats aux législatives"
author: Julien L.
date: 2012-05-29
href: http://www.numerama.com/magazine/22734-le-pacte-du-logiciel-libre-signe-par-238-candidats-aux-legislatives.html
tags:
- Internet
- Administration
- April
- Institutions
- Droit d'auteur
---

> À moins de deux semaines des élections législatives, l'April a fait un point d'étape sur la sensibilisation des candidats au logiciel libre. Son pacte a pour l'heure été signé par 238 candidats.
