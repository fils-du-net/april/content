---
site: euronews
title: "Le brevet unifié européen, stimulateur d’innovation?"
date: 2012-05-31
href: http://fr.euronews.com/2012/05/31/le-brevet-unifie-europeen-stimulateur-d-innovation/
tags:
- Entreprise
- April
- Associations
- Brevets logiciels
- Innovation
- Europe
---

> En 2011, plus de 62.000 brevets ont été délivrés en Europe pour protéger des inventions. A l’heure où l’on ne parle que de croissance, tout ce qui peut faciliter l’innovation est plutôt bienvenu et l’idée d’un seul et unique brevet pour toute l’Europe revient en force, mais elle peine à se concrétiser.
