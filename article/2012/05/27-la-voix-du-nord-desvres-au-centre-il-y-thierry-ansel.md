---
site: La Voix du Nord
title: "Desvres: au Centre, il y a Thierry Ansel"
author: O. M.
date: 2012-05-27
href: http://www.lavoixdunord.fr/Locales/Boulogne_sur_Mer/actualite/Secteur_Boulogne_sur_Mer/2012/05/27/article_au-centre-il-y-a-thierry-ansel.shtml
tags:
- Entreprise
- Institutions
---

> C'est sur la place de Wissant, où il aidait jadis son père artisan boucher sur le marché, que Thierry Ansel a présenté sa candidature dans la sixième circonscription. L'adhérent au Modem depuis 2007 est âgé de 37 ans et exerce le métier de professeur de mathématique en collège.
