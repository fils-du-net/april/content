---
site: Silicon.fr
title: "Fleur Pellerin, en charge de l'économie numérique"
author: Ariane Beky
date: 2012-05-18
href: http://www.silicon.fr/fleur-pellerin-est-nommee-ministre-deleguee-des-pme-de-linnovation-et-de-leconomie-numerique-74720.html
tags:
- Entreprise
- Économie
- Institutions
---

> Chargée des problématiques numériques durant la campagne du socialiste François Hollande, Fleur Pellerin a été nommée mercredi soir ministre déléguée des PME, de l’Innovation et de l’Économie numérique dans le gouvernement de Jean-Marc Ayrault.
