---
site: Libération.fr
title: "Stallman et le «libre», champions de Tunis"
author: élodie Auffray
date: 2012-05-06
href: http://www.liberation.fr/medias/2012/05/06/stallman-et-le-libre-champions-de-tunis_816797
tags:
- Entreprise
- Internet
- Administration
- Institutions
- Sensibilisation
- Associations
- Philosophie GNU
- International
- Open Data
---

> Le logiciel libre a été un élément moteur de la révolution en Tunisie et s’y implante durablement, a constaté sur place son plus ardent défenseur.
