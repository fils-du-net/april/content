---
site: LADEPECHE.fr
title: "Législatives. Le Parti pirate s'amarre à la Prairie des Filtres"
author: T. Bl.
date: 2012-05-18
href: http://www.ladepeche.fr/article/2012/05/18/1356160-le-parti-pirate-s-amarre-a-la-prairie-des-filtres.html
tags:
- Internet
- Institutions
- Droit d'auteur
- Open Data
---

> Clin d'œil au nom de leur formation, les candidats du Parti pirate aux législatives dans le département organisaient hier un pique-nique sur les bords de la Garonne à la Prairie des Filtres.
