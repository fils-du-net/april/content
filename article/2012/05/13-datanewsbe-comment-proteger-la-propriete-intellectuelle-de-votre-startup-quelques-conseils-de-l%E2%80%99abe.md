---
site: Datanews.be
title: "Comment protéger la propriété intellectuelle de votre startup? Quelques conseils de l’ABE"
date: 2012-05-13
href: http://datanews.levif.be/ict/centre-de-connaissance/jeunes-entreprises/comment-proteger-la-propriete-intellectuelle-de-votre-startup-quelques-conseils-de-l-abe/article-4000093539596.htm
tags:
- Entreprise
- Internet
- Brevets logiciels
- Droit d'auteur
---

> L’internet représente une sérieuse menace pour votre propriété intellectuelle. Comment les startups peuvent-elles protéger leurs logiciels par le biais du droit d’auteur?
