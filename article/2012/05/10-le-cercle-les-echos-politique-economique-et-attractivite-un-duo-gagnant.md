---
site: LE CERCLE Les Echos
title: "Politique économique et attractivité: un duo gagnant"
author: Jean-Yves Archer
date: 2012-05-10
href: http://lecercle.lesechos.fr/economie-societe/politique-eco-conjoncture/politique-economique/221146690/politique-economique-et-at
tags:
- Entreprise
- Administration
- Économie
- Institutions
---

> La crise en Europe prend une tournure très préoccupante: la Grèce et son peuple sont à bout et l'Espagne vacille. L'Histoire s'apprête peut-être à nous jouer un sale tour collectif et pourtant il faut réfléchir aux modalités de sortie de crise. Un maillage entre les externalités et la politique économique semble être un outil opérant: examinons ensemble les lignes de force de ce binôme.
