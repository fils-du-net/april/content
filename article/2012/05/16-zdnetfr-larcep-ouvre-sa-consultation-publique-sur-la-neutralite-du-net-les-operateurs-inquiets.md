---
site: ZDNet.fr
title: "L'Arcep ouvre sa consultation publique sur la neutralité du Net. Les opérateurs inquiets"
date: 2012-05-16
href: http://www.zdnet.fr/actualites/l-arcep-ouvre-sa-consultation-publique-sur-la-neutralite-du-net-les-operateurs-inquiets-39771796.htm
tags:
- Entreprise
- Internet
- Administration
- Associations
- Neutralité du Net
---

> La consultation, qui se clôturera fin juin, est censée permettre au régulateur des télécoms de démystifier les relations entre opérateurs de réseau. Une décision qui ne plait pas à tous.
