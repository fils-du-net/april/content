---
site: LeMagIT
title: "Cloud tricolore: SFR et Bull se dressent face à Orange et Thalès"
author: Cyrille Chausson
date: 2012-05-10
href: http://www.lemagit.fr/article/france-bull-cloud-computing/11022/1/cloud-tricolore-sfr-bull-dressent-face-orange-thales/
tags:
- Entreprise
- Internet
- Institutions
- Informatique en nuage
---

> Après le retrait de Dassault Systèmes, SFR s’est associé à Bull pour construire son projet Andromède. Le projet de société commune a reçu le soutien de l’Etat qui financera le projet à hauteur de 75 millions d’euros. Une pierre dans le jardin d’Orange et de Thalès.
