---
site: PC INpact
title: "Fleur Pellerin à l'économie numérique, Aurélie Filippetti à la Culture"
author: Nil Sanyas
date: 2012-05-16
href: http://www.pcinpact.com/news/70957-fleur-pellerin-numerique-aurelie-filippetti-culture.htm
tags:
- Internet
- Économie
- HADOPI
- Institutions
- Associations
- Brevets logiciels
- Innovation
- Neutralité du Net
- Informatique en nuage
- ACTA
---

> On ne se fera pas charmer pour autant
