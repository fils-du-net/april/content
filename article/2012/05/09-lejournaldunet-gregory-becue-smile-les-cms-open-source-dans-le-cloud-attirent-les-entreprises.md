---
site: LeJournalduNet
title: "Grégory Becue (Smile) \"Les CMS Open Source dans le Cloud attirent les entreprises\""
author: Dominique FILIPPONE
date: 2012-05-09
href: http://www.journaldunet.com/solutions/saas-logiciel/gregory-becue-cms-open-source-et-cmsday-smile.shtml?f_id_newsletter=6870
tags:
- Entreprise
- Internet
- Sensibilisation
- Associations
- Informatique en nuage
---

> La gestion de contenu Open Source trouve sa place en tant que projet stratégique dans un nombre grandissant d'entreprises. L'évènement CMSday organisé par Smile va permettre de s'en rendre compte.
