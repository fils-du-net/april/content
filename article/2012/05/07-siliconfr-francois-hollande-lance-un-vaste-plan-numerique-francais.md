---
site: Silicon.fr
title: "François Hollande lance un vaste plan numérique français"
author: Ariane Beky
date: 2012-05-07
href: http://www.silicon.fr/francois-hollande-transformation-numerique-francaise-74333.html
tags:
- Entreprise
- Internet
- Administration
- Économie
- Interopérabilité
- HADOPI
- Institutions
- Éducation
- Innovation
- Marchés publics
- Standards
- International
- Open Data
---

> Vainqueur de l’élection présidentielle du 6 mai, le socialiste François Hollande a précisé durant sa campagne ses choix en faveur d’une transformation numérique de l’économie et de la société. Récapitulatif.
