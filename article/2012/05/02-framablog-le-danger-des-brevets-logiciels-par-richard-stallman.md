---
site: Framablog
title: "Le danger des brevets logiciels par Richard Stallman"
author: aKa
date: 2012-05-02
href: http://www.framablog.org/index.php/post/2012/05/02/danger-brevets-logiciels-stallman
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Économie
- Institutions
- Brevets logiciels
- Droit d'auteur
- Innovation
- Standards
- International
---

> Parmi les nombreux front où est engagé le Libre il y a celui complexe, épineux et crucial des brevets logiciels (dossier suivi notamment chez nous par l’April).
