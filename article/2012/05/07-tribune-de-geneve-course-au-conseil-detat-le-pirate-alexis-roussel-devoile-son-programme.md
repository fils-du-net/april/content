---
site: Tribune de Genève
title: "COURSE AU CONSEIL D'ETAT: Le Pirate Alexis Roussel dévoile son programme"
author: Sophie Simon
date: 2012-05-07
href: http://www.tdg.ch/geneve/actu-genevoise/Le-Pirate-Alexis-Roussel-devoile-son-programme/story/14801045
tags:
- Administration
- Institutions
- Droit d'auteur
- Éducation
- International
- Open Data
---

> Trois mots-clés pour concevoir une «société de l'information»: transparence, participation et coopération.
