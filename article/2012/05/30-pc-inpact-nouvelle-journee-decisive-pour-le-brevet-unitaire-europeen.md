---
site: PC INpact
title: "Nouvelle journée décisive pour le brevet unitaire européen"
author: Marc Rees
date: 2012-05-30
href: http://www.pcinpact.com/news/71241-brevet-unitaire-europee-conseil-competitivite.htm
tags:
- Entreprise
- Administration
- April
- Institutions
- Brevets logiciels
- Innovation
- Europe
---

> Réunion importante aujourd’hui au Conseil Compétitivité. Celui-ci réunit les ministres nationaux des affaires économiques, de l’industrie et de la recherche. Il est ainsi chargé de traiter de manière plus coordonnée les questions de compétitivité dans l’Union européenne tant à l'horizontale que dans le sectoriel. Celui-ci abordera ce 30 mai le sujet de la juridiction unifiée du brevet de l'Union européenne.
