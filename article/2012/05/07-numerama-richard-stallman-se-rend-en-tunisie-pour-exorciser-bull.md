---
site: Numerama
title: "Richard Stallman se rend en Tunisie pour exorciser Bull"
author: Guillaume Champeau
date: 2012-05-07
href: http://www.numerama.com/magazine/22542-richard-stallman-se-rend-en-tunisie-pour-exorciser-bull.html
tags:
- Entreprise
- Internet
- Institutions
- Philosophie GNU
- International
---

> Pour dénoncer l'implication de Bull dans la surveillance des citoyens par les états autoritaires, le gourou du logiciel libre Richard Stallman s'est coiffé d'une auréole et s'est rendu dans les locaux tunisiens de la société française, pour y procéder à un exorcisme...
