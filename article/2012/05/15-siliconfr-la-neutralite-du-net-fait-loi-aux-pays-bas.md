---
site: Silicon.fr
title: "La neutralité du Net fait loi aux Pays-Bas"
author: Ariane Beky
date: 2012-05-15
href: http://www.silicon.fr/neutralite-net-pays-bas-74594.html
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- Europe
- International
---

> Les Pays-Bas inscrivent dans leur législation la neutralité d'Internet, une neutralité défendue par Vinton Cerf et Tim Berners Lee.
