---
site: Numerama
title: "C'est la journée mondiale contre les DRM"
author: Julien L.
date: 2012-05-04
href: http://www.numerama.com/magazine/22518-c-est-la-journee-mondiale-contre-les-drm.html
tags:
- Internet
- Interopérabilité
- April
- HADOPI
- Institutions
- DADVSI
- DRM
- International
---

> L'April et la Free Software Foundation se mobilisent ce 4 mai contre les mesures techniques de protection (DRM). En France, ces verrous numériques ont été propulsés sur le devant de la scène à l'occasion de la loi DADVSI, en 2006.
