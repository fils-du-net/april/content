---
site: Marianne 2
title: "Hadopi: guerre des tranchées en vue"
author: Tefy Andriamanana
date: 2012-05-25
href: http://www.marianne2.fr/Hadopi-guerre-des-tranchees-en-vue_a218893.html
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Associations
- DRM
- Droit d'auteur
---

> Le chantier de l’après-Hadopi vient de commencer. La réforme risque de créer des tensions au sein du gouvernement et du PS. Une belle cacophonie en perspective. Et on peut compter sur les lobbys culturels pour compliquer l'équation.
