---
site: LeDevoir.com
title: "Du logiciel libre au PQ dans Vimont"
author: Fabien Deglise
date: 2012-05-09
href: http://www.ledevoir.com/politique/quebec/349584/du-logiciel-libre-au-pq-dans-vimont
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Administration
- Institutions
- Marchés publics
- International
- Open Data
---

> Du programme informatique au programme… électoral. L’entrepreneur Cyrille Béraud, qui a porté dans les dernières années la cause du logiciel libre et de l’ouverture des données au Québec en s’attaquant entre autres à la Régie des rentes, a décidé de faire le saut en politique sous la bannière du Parti québécois, a appris Le Devoir.
