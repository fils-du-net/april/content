---
site: Silicon.fr
title: "Un marquage pour suivre le code open source à la trace"
author: David Feugey
date: 2012-05-31
href: http://www.silicon.fr/marquage-open-source-75243.html
tags:
- Entreprise
- Internet
- Associations
- Licenses
---

> Le FOSS Bar Code Tracker référence les composants open source utilisés au sein d’un projet sur une étiquette de type code à barres ou QR.
