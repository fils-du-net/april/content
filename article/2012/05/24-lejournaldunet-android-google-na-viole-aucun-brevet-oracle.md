---
site: LeJournalduNet
title: "Android: Google n'a violé aucun brevet Oracle"
author: Dominique FILIPPONE
date: 2012-05-24
href: http://www.journaldunet.com/solutions/saas-logiciel//verdict-du-proces-google-oracle-sur-la-violation-de-brevets-java-sur-android-0512.shtml
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Droit d'auteur
---

> La Justice américaine a blanchi Google des accusations de violation de brevets d'Oracle pour bâtir Android. Mais l'ombre d'un nouveau procès plane encore au sujet d'une infraction au droit d'auteur portant sur 37 API Java.
