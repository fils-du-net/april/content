---
site: Commentçamarche.net
title: "«Ubuntu, beaucoup plus intuitif que Windows»"
date: 2012-05-09
href: http://www.commentcamarche.net/news/5859144-ubuntu-beaucoup-plus-intuitif-que-windows
tags:
- Logiciels privateurs
- Sensibilisation
---

> Faut-il systématiquement renvoyer dos à dos les utilisateurs de Windows et ceux des systèmes d'exploitation libres, dérivés du noyau «Linux»?
