---
site: 01net.
title: "Des hackers au service des opposants syriens"
author: Amélie Charnay
date: 2012-08-08
href: http://www.01net.com/editorial/571157/des-hackers-au-service-des-opposants-syriens/
tags:
- Internet
- Partage du savoir
- Institutions
- Associations
- International
---

> Sans quitter leur ordinateur, les hackers du groupe Telecomix ont probablement sauvé des vies... en établissant des connexions ou encore en sécurisant les communications des opposants syriens dans le cadre leur «OP Syria». Rencontre avec Okhin, un de leurs agents.
