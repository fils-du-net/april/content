---
site: PC INpact
title: "Wikileaks et FDNN sous une attaque DDoS"
author: Marc Rees
date: 2012-08-08
href: http://www.pcinpact.com/news/72974-wikileaks-sous-attaque-ddos.htm
tags:
- Internet
- April
- Institutions
- Associations
- Neutralité du Net
---

> Wikileaks est actuellement sous le feu d’une attaque DDOS d’ampleur. Difficile voire impossible d’accéder à Wikileaks.org.
