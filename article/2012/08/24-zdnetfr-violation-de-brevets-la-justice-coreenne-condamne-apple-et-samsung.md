---
site: ZDNet.fr
title: "Violation de brevets: la justice coréenne condamne Apple et Samsung"
date: 2012-08-24
href: http://www.zdnet.fr/actualites/violation-de-brevets-la-justice-coreenne-condamne-apple-et-samsung-39775372.htm
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> La justice coréenne n’a donné raison ni à Apple ni à Samsung en condamnant les deux constructeurs pour des violations de brevets réciproques. Le tribunal de Séoul a par ailleurs jugé que les terminaux d’Apple et de Samsung étaient différents, ce dernier n’étant donc pas coupable de plagiat.
