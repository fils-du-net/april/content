---
site: Numerama
title: "Pixar se lance dans l'open-source avec Open Subdiv"
author: Guillaume Champeau
date: 2012-08-13
href: http://www.numerama.com/magazine/23407-pixar-se-lance-dans-l-open-source-avec-open-subdiv.html
tags:
- Entreprise
- Partage du savoir
- Brevets logiciels
- Innovation
- Licenses
- Video
---

> Pixar a décidé de publier sous licence libre des librairies d'un logiciel employé pour améliorer la qualité du rendu des images de synthèses. Le studio autorise également l'utilisation des technologies brevetées liées à cette solution "Open Subdiv".
