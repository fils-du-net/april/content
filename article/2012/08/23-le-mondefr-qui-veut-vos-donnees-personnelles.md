---
site: Le Monde.fr
title: "Qui veut vos données personnelles?"
author: Yves Eudes
date: 2012-08-23
href: http://www.lemonde.fr/sciences/article/2012/08/23/qui-veut-vos-donnees-personnelles_1749130_1650684.html
tags:
- Entreprise
- Internet
- Informatique-deloyale
---

> La société californienne Mozilla a créé une application permettant de suivre, sous la forme d'un graphique animé, la façon dont les acteurs du Web traquent les internautes.
