---
site: Silicon.fr
title: "Brocade fait condamner A10 Networks pour contrefaçon"
author: Yves Grandmontagne
date: 2012-08-08
href: http://www.silicon.fr/brocade-fait-condamner-a10-networks-pour-contrefacon-de-brevets-77307.html
tags:
- Entreprise
- Internet
- Innovation
- Europe
- Open Data
---

> A10 Networks et son CEO Lee Chen devront verser 112 millions de dollars à Brocade.
