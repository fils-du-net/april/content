---
site: Le Québécois Libre
title: "Pour le logiciel Libre!"
author: Jean-Philippe L. Risi
date: 2012-08-15
href: http://www.quebecoislibre.org/12/120815-8.html
tags:
- Entreprise
- Économie
- Institutions
- Sensibilisation
- Licenses
- Marchés publics
- Philosophie GNU
- International
---

> Malgré les avancées majeures effectuées au cours des vingt dernières années, les logiciels libres sont encore mal connus des principaux intéressés, c'est-à-dire le grand public et les entreprises. En effet, ils restent l’affaire d’enthousiastes de sous-sol, leur conférant malheureusement une mauvaise publicité et une faible chance de contribuer sérieusement à plusieurs débats sociaux.
