---
site: Rue89 LES BLOGS
title: "ONU: un traité anti-domaine public, anti-licences libres"
author: Pierre-Carl Langlais
date: 2012-08-14
href: http://blogs.rue89.com/hotel-wikipedia/2012/08/14/onu-un-traite-anti-domaine-public-anti-licences-libres-228179
tags:
- Entreprise
- Internet
- Institutions
- DRM
- International
- ACTA
---

> Vous avez aimé Acta, ce traité anticontrefaçon signé en secret par 22 Etats européens avant d’être finalement rebuté par le Parlement européen? Vous adorerez le Traité de diffusion («Broadcasting Treaty») actuellement préparé en petit comité par une institution spécialisée des Nations unies, l’Organisation mondiale de la propriété intellectuelle (OMPI).
