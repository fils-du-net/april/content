---
site: ZDNet.fr
title: "Guerre des brevets: Apple brandit un document de Samsung comparant iPhone et Galaxy S"
date: 2012-08-09
href: http://www.zdnet.fr/actualites/guerre-des-brevets-apple-brandit-un-document-de-samsung-comparant-iphone-et-galaxy-s-39775015.htm
tags:
- Entreprise
- Internet
- HADOPI
- Innovation
- Standards
- Informatique en nuage
---

> Pour Apple, la firme sud-coréenne a sciemment copié les icônes de son interface. Un document interne du conglomérat, présenté par Apple, compare sur 132 pages les deux terminaux.
