---
site: Les Echos
title: "Avec Firefox OS, les opérateurs télécoms cherchent à reprendre la main"
author: Guillaume de Calignon
date: 2012-08-08
href: http://www.lesechos.fr/entreprises-secteurs/tech-medias/actu/0202164735803-avec-firefox-os-les-operateurs-telecoms-cherchent-a-reprendre-la-main-351339.php
tags:
- Entreprise
- Innovation
- International
---

> Le système d'exploitation de la fondation Mozilla est poussé par Telefonica et d'autres opérateurs mais aussi par des fabricants chinois de smartphones, inquiets d'une dépendance à Android.
