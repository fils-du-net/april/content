---
site: Slate.fr
title: "Wikileaks: le combat de Julian Assange n'est pas anti-américain"
author: Thomas Guénolé
date: 2012-08-20
href: http://www.slate.fr/tribune/60719/wikileaks-assange-etats
tags:
- Entreprise
- Internet
- Institutions
- Contenus libres
- International
---

> Et les Etats font une erreur en postulant que l'organisation se délitera si son leader charismatique est «supprimé».
