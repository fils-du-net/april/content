---
site: le Site du Zéro
title: "Twitter tourne le dos à l'ouverture"
author: Echec et Matt et willard
date: 2012-08-26
href: http://www.siteduzero.com/news-62-45278-p1-twitter-tourne-le-dos-a-l-ouverture.html
tags:
- Entreprise
- Internet
- Standards
---

> Le monde des réseaux sociaux est gouverné principalement par trois grands géants aux objectifs divergents, Facebook, Twitter et Foursquare. Le réseau social Twitter s'inscrit dans la catégorie du microblog et permet à n'importe quel individu d'envoyer des «tweets» (message de 140 caractères maximum) à destination de ses abonnés (on entend souvent parler de "followers", littéralement les "suiveurs"). Ce réseau social, qui a connu une croissance fulgurante grâce à son ouverture aux développeurs tiers, a désormais changé son fusil d'épaule.
