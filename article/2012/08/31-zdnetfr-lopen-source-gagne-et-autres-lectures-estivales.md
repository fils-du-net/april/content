---
site: ZDNet.fr
title: "L'open source a gagné, et autres lectures estivales"
author: Thierry Noisette
date: 2012-08-31
href: http://www.zdnet.fr/blogs/l-esprit-libre/l-open-source-a-gagne-et-autres-lectures-estivales-39774766.htm
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Promotion
---

> Victoire par banalisation de l'open source selon un responsable d'O'Reilly, critiques de la vidéosurveillance, article sur «Elinor Ostrom ou la réinvention des biens communs»: suggestions de bonnes pages pour votre été.
