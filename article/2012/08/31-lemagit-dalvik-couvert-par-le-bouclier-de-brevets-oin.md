---
site: LeMagIT
title: "Dalvik, couvert par le bouclier de brevets OIN"
author: Cyrille Chausson
date: 2012-08-31
href: http://www.lemagit.fr/article/brevets-android-open-source/11712/1/dalvik-couvert-par-bouclier-brevets-oin
tags:
- Entreprise
- Brevets logiciels
---

> Alors que la mobilité est devenue un champ de bataille pour la guerre des brevets, l’Open Invention Network (OIN), a annoncé avoir étendu son bouclier protecteur à 18 technologies mobiles, dont la machine virtuelle "Java" Dalvik d’Android, ainsi que plusieurs composants clés du système d’exploitation de Google.
