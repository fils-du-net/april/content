---
site: Cafebabel
title: "Les pirates en politique: cap sur les élections européennes"
author: Jacopo Franchi
date: 2012-08-28
href: http://www.cafebabel.fr/article/41917/pirates-politique-cap-elections-europeennes.html
tags:
- Internet
- Institutions
- Europe
---

> Un spectre hante l’Europe. Celui des partis pirates, prêts à se lancer dans un défi très personnel en vue des prochaines élections, nationales et européennes. Alors que l’adoption d’un grand programme open source ouvre de nouveaux scénarios pour nos démocraties fatiguées, la création d’un parti unique européen pirate (PPEU) semble être l’une des nouveautés les plus importantes annoncées suite à une récente assemblée. Ce qui soulève des questions.
