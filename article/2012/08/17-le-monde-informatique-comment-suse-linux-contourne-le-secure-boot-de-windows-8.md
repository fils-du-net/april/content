---
site: Le Monde Informatique
title: "Comment Suse Linux contourne le Secure Boot de Windows 8"
author: Jean Elyan
date: 2012-08-17
href: http://www.lemondeinformatique.fr/actualites/lire-comment-suse-contourne-le-secure-boot-de-windows-8-50082.html
tags:
- Entreprise
- Associations
- DRM
---

> Basée sur la stratégie de Fedora, cette nouvelle approche des restrictions de Windows 8 ajoute plus de flexibilité à l'ensemble.
