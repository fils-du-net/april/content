---
site: "Futura-Techno"
title: "Botiful, un robot de «téléprésence sociale» contrôlé via Skype"
author: Marc Zaffagni, Futura-Sciences
date: 2012-08-01
href: http://www.futura-sciences.com/fr/news/t/robotique/d/botiful-un-robot-de-telepresence-sociale-controle-via-skype_40388/
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Matériel libre
- Innovation
---

> Conçu par une ingénieure française installée en Californie, Botiful est un minirobot monté sur trois roues qui se connecte à un smartphone Android, et peut être piloté via Skype pendant des appels vidéo. Ce projet n’attend plus que le coup de pouce financier qui lui permettra de voir le jour. Claire Delaunay, l’inventrice de Botiful, a répondu aux questions de Futura-Sciences.
