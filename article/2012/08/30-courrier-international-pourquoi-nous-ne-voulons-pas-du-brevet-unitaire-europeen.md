---
site: Courrier international
title: "Pourquoi nous ne voulons pas du brevet unitaire européen"
author: Adam Grzeszak
date: 2012-08-30
href: http://www.courrierinternational.com/article/2012/08/30/pourquoi-nous-ne-voulons-pas-du-brevet-unitaire-europeen
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Associations
- Brevets logiciels
- Innovation
- Europe
- ACTA
---

> Les débats sur le brevet unitaire reprendront en septembre au Parlement européen. Favorable aux pays riches comme l'Allemagne ou la France, le nouveau brevet aura des conséquences néfastes pour l'économie polonaise, prévient le principal hebdomadaire de Varsovie.
