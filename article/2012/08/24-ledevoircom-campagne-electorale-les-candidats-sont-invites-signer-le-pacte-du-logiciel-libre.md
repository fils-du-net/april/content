---
site: LeDevoir.com
title: "Campagne électorale: les candidats sont invités à signer le Pacte du logiciel libre"
author: Fabien Deglise
date: 2012-08-24
href: http://www.ledevoir.com/opinion/blogues/les-mutations-tranquilles/357654/campagne-electorale-les-candidats-sont-invites-a-signer-le-pacte-du-logiciel-libre
tags:
- Entreprise
- Administration
- April
- Institutions
- Associations
- Marchés publics
- Promotion
- International
---

> Au coeur de la campagne électorale provinciale, l'invitation a été lancée jeudi par l'Association pour l'appropriation collective de l'informatique libre (FACIL) à l'ensemble des candidats impliqués dans la course. Elle leur demande de prendre l'engagement formel de défendre le bien commun dans une société technologique en assurant la multiplication des logiciels non-privatifs partout au Québec en cas d'élection. Françoise David de Québec Solidaire et Henri-François Gautrin du Parti libéral ont signé ce que le FACIL présente comme le Pacte du logiciel libre.
