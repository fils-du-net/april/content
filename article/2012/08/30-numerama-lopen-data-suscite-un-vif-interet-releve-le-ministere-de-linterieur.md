---
site: Numerama
title: "L'Open Data suscite un vif intérêt, relève le ministère de l'Intérieur"
author: Julien L.
date: 2012-08-30
href: http://www.numerama.com/magazine/23561-l-open-data-suscite-un-vif-interet-releve-le-ministere-de-l-interieur.html
tags:
- Administration
- Open Data
---

> Les données publiques sous format ouvert intéressent les internautes. C'est le constat du ministère de l'Intérieur, dont les jeux de données figurent parmi les plus réclamées. Le ministère assure qu'il poursuivra en ce sens afin de fournir aussi vite que possible des "données pertinentes et représentatives" de son action.
