---
site: Numerama
title: "Un logo \"DRM-Free\" pour identifier les plateformes sans DRM"
author: Guillaume Champeau
date: 2012-08-16
href: http://www.numerama.com/magazine/23425-un-logo-34drm-free34-pour-identifier-les-plateformes-sans-drm.html
tags:
- Internet
- HADOPI
- DRM
- Droit d'auteur
---

> Sur le web anglosaxon, et peut-être aussi bientôt en France (?), un logo identifie désormais les sites qui proposent uniquement des contenus vendus sans DRM.
