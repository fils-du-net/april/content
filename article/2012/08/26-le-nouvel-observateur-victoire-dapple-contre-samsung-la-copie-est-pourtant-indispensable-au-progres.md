---
site: Le nouvel Observateur
title: "Victoire d'Apple contre Samsung: la copie est pourtant indispensable au progrès"
author: Dimitri Boisdet
date: 2012-08-26
href: http://leplus.nouvelobs.com/contribution/614965-victoire-d-apple-contre-samsung-la-copie-est-pourtant-indispensable-au-progres.html
tags:
- Entreprise
- Brevets logiciels
- Innovation
---

> Le verdict récemment rendu en faveur d'Apple par le tribunal californien, dans le procès qui l'opposait à Samsung, a de quoi combler les fidèles de la marque à la pomme. Et pourtant, certains ne sont pas de cet avis. Notre contributeur, bien que fervent partisan d'Apple, explique en quoi la copie est inhérente au processus même d'innovation technologique.
