---
site: Silicon.fr
title: "Google envisage une réforme du brevet logiciel"
author: Ariane Beky
date: 2012-08-21
href: http://www.silicon.fr/google-reforme-brevet-logiciel-77602.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Innovation
---

> À l’heure du bras de fer entre sa filiale Motorola Mobility et Apple, Google estime que les États-Unis devraient réformer leur politique des brevets afin de favoriser l’innovation et de mettre l’utilisateur au centre des débats.
