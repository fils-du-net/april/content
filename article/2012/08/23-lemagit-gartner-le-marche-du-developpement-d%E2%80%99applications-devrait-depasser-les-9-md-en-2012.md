---
site: LeMagIT
title: "Gartner: le marché du développement d’applications devrait dépasser les 9 Md$ en 2012"
author: Cyrille Chausson
date: 2012-08-23
href: http://www.lemagit.fr/article/mobile-developpement-opensource-cloud/11651/1/gartner-marche-developpement-applications-devrait-depasser-les-2012
tags:
- Entreprise
- Économie
- Informatique en nuage
---

> Le cloud, la mobilité ainsi que l’Open Source seront les moteurs qui propulseront le marché mondial du développement d’applications en 2012, nous apprend, presque sans surprise, le cabinet d’étude Gartner dans son dernier rapport. Cette année, ce segment de marché devrait ainsi dépasser la barre des 9 milliards de dollars, soit une progression de 1,8 % en un an, selon les estimations du cabinet.
