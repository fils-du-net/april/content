---
site: ZDNet.fr
title: "Samsung n'a pas violé les brevets d'Apple... selon la justice japonaise"
date: 2012-08-31
href: http://www.zdnet.fr/actualites/samsung-n-a-pas-viole-les-brevets-d-apple-selon-la-justice-japonaise-39775611.htm
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> Les smartphones Galaxy S et les tablettes Galaxy Tab de Samsung n’enfreignent pas des brevets d’Apple a estimé la justice japonaise, qui a condamné l’américain à rembourser les frais de justice de son concurrent.
