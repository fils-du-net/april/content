---
site: Silicon.fr
title: "Nokia se déleste de Qt à Digia"
author: Christophe Lagane
date: 2012-08-10
href: http://www.silicon.fr/nokia-se-deleste-de-qt-a-digia-77368.html
tags:
- Entreprise
- Licenses
---

> Nokia revend sa division Qt à l'éditeur Digia qui entend le porter pour Android, iOS et Windows 8 mais pas Windows Phone.
