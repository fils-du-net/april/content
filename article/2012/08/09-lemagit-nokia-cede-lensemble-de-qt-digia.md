---
site: LeMagIT
title: "Nokia cède l'ensemble de Qt à Digia"
author: Cyrille Chausson
date: 2012-08-09
href: http://www.lemagit.fr/article/mobilite-nokia-developpement/11586/1/nokia-cede-ensemble-digia/
tags:
- Entreprise
- Internet
- Administration
- Interopérabilité
- Innovation
- Standards
- Informatique en nuage
- Video
- Europe
- Open Data
---

> Après en avoir acquis en 2011 la gestion des licences commerciales et les services, la société finlandaise rachète à Nokia l’intégralité de Qt. Nokia se sépare des restes du framework pour se consacrer à Windows Phone. Chez Digia, Qt sera notamment porté sur iOS et Android.
