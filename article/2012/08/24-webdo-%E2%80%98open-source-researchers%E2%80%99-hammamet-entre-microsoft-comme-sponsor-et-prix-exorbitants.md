---
site: webdo
title: "‘open source researchers’ à Hammamet: entre Microsoft comme sponsor et prix exorbitants!"
author: Melek Jebnoun
date: 2012-08-24
href: http://www.webdo.tn/2012/08/24/open-source-researchers-a-hammamet-entre-microsoft-comme-sponsor-et-prix-exorbitants
tags:
- Entreprise
- Éducation
- Promotion
- International
---

> La Tunisie et plus précisément la ville de Hammamet accueillera du 10 au 13 septembre la conférence internationale sur les systèmes Open Source (OSS 2012). Une conférence annuelle qui a pour objectif de réunir un set de professeurs des universités les plus prestigieuses de la planète et des professionnels et industriels du monde open source. Ils se réunissent chaque année afin de partager le résultat de leurs études et recherches et de pister les nouvelles opportunités et nouvelles lignes directrices à suivre dans leurs travaux.
