---
site: Les Echos
title: "La quête d'une alternative à Apple et Google dans le mobile s'intensifie"
author: Solveig Godeluck
date: 2012-08-08
href: http://www.lesechos.fr/entreprises-secteurs/tech-medias/actu/0202207510281-la-quete-d-une-alternative-a-apple-et-google-dans-le-mobile-s-intensifie-351377.php
tags:
- Entreprise
- Internet
- Innovation
- International
---

> Firefox OS, Tizen, Opera Software: les opérateurs et constructeurs qui craignent de dépendre des géants californiens du Net poussent ces plates-formes mobiles basées sur les technologies ouvertes du Web.
