---
site: Numerama
title: "Furieux et paranos, des auteurs font fermer un site légal de prêts de livres"
author: Guillaume Champeau
date: 2012-08-10
href: http://www.numerama.com/magazine/23398-furieux-et-paranos-des-auteurs-font-fermer-un-site-legal-de-prets-de-livres.html
tags:
- Entreprise
- Internet
- Partage du savoir
- DRM
- Droit d'auteur
---

> Parce qu'ils étaient persuadés d'y voir une forme de piratage ou de vol, des auteurs ont envoyé par centaines des lettres de menaces à l'hébergeur d'un site qui proposait aux acheteurs de livres électroniques de prêter temporairement leur "exemplaire numérique"... comme l'autorisent de nombreux éditeurs. Le site, parfaitement légal, est fermé depuis 10 jours.
