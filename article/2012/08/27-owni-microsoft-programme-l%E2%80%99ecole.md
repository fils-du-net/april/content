---
site: OWNI
title: "Microsoft programme l’école"
author: Sabine Blanc
date: 2012-08-27
href: http://owni.fr/2012/08/27/microsoft-programme-lecole
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- April
- Associations
- Éducation
---

> Début ce lundi de l'université d'été de l'e-éducation. Un évènement majeur pour le lobbying de Microsoft, qui cible depuis plusieurs années l'Éducation nationale. Enquête et infographie pour tout savoir des campagnes d'influence du marchand de logiciels en direction des écoles de la République. Bonne rentrée.
