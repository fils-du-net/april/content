---
site: Le Monde.fr
title: "Un site veut mettre fin au \"mensonge\" des conditions d'utilisation sur Internet"
author: Franz Durupt
date: 2012-08-14
href: http://www.lemonde.fr/technologies/article/2012/08/14/un-site-veut-mettre-fin-au-mensonge-des-conditions-d-utilisation-sur-internet_1745942_651865.html
tags:
- Internet
- Partage du savoir
- Associations
- Licenses
---

> Lancé par un étudiant français, ToS;DR permet de connaître bons et mauvais points des conditions d'utilisation, généralement acceptées sans être lues.
