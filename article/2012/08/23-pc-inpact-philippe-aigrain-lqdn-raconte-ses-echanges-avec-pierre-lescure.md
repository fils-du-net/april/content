---
site: PC INpact
title: "Philippe Aigrain (LQDN) raconte ses échanges avec Pierre Lescure"
author: Xavier Berne
date: 2012-08-23
href: http://www.pcinpact.com/breve/73280-philippe-aigrain-lqdn-raconte-ses-entrevues-avec-pierre-lescure.htm
tags:
- Internet
- HADOPI
- Associations
---

> Philippe Aigrain, co-fondateur de La Quadrature du Net, a eu l’occasion de s’entretenir avec Pierre Lescure, chargé de la mission sur l’acte II de l’exception culturelle. Dans un billet intitulé «Interactions avec la mission Lescure», l’intéressé raconte sur son blog comment se sont déroulés ses échanges avec l’ancien PDG de Canal+.
