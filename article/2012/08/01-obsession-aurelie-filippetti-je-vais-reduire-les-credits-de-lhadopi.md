---
site: OBSESSION
title: "Aurélie Filippetti: \"Je vais réduire les crédits de l'Hadopi\""
author: Boris Manenti 
date: 2012-08-01
href: http://obsession.nouvelobs.com/high-tech/20120801.OBS8587/aurelie-filippetti-je-vais-reduire-les-credits-de-l-hadopi.html
tags:
- Internet
- HADOPI
- Institutions
---

> INTERVIEW. La ministre veut faire en sorte "qu'internet devienne l'une des plus grandes sources de financement de la culture". Elle précise (un peu) la mission de Pierre Lescure.
