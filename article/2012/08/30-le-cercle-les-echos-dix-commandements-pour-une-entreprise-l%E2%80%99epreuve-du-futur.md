---
site: LE CERCLE Les Echos
title: "Dix commandements pour une entreprise à l’épreuve du futur"
author: Simone Cicero
date: 2012-08-30
href: http://lecercle.lesechos.fr/entreprises-marches/management/organisation/221153116/dix-commandements-entreprise-a-epreuve-futur
tags:
- Entreprise
- Internet
- Économie
- Innovation
- Informatique en nuage
---

> Comment créer des entreprises résilientes, durables et saines pour l’économie dans son ensemble? Plus qu’une simple adaptation de leurs modes de management et de communication, les entreprises qui veulent survivre au XXIème siècle doivent repenser la façon même dont elles créent de la valeur en abandonnant leurs avantages compétitifs.
