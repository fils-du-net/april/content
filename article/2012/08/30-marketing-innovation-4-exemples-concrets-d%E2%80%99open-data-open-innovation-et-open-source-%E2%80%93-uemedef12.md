---
site: Marketing & Innovation
title: "4 exemples concrets d’open data, open innovation et open source – #uemedef12"
author: visionarymarketing
date: 2012-08-30
href: http://visionary.wordpress.com/2012/08/30/4-exemples-concrets-dopen-data-open-innovation-et-open-source-uemedef12/
tags:
- Entreprise
- Économie
- Innovation
- Open Data
---

> Ce n’est pas seulement une vision utopique: même l’EDF met ses outils de simulation en open source: bienvenue dans la culture du partage et de l’innovation
