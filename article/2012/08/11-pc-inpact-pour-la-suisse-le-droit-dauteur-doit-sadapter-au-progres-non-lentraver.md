---
site: PC INpact
title: "Pour la Suisse, le droit d'auteur doit s'adapter au progrès, non l'entraver"
author: Marc Rees
date: 2012-08-11
href: http://www.pcinpact.com/news/73067-pour-suisse-droit-d-auteur-doit-s-adapter-au-progres-non-l-entraver.htm
tags:
- Internet
- Économie
- HADOPI
- Institutions
- Droit d'auteur
- Licenses
- International
---

> En novembre 2011, la Suisse avait expliqué à ses voisins, notamment français, pourquoi elle ne voulait pas d’Hadopi. Chargé par le Conseil des États d’établir un rapport sur le téléchargement illégal, le Conseil fédéral expliquait de long en large que ceux qui téléchargent continuent à acheter dans les divertissements. Le pays poursuit ses travaux pour explorer de nouvelles formes de financement qui ne malmènent pas la sacrosainte vie privée et évite un déluge pénal.
