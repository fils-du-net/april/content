---
site: Inf'OGM
title: "Brevet unitaire européen: quels impacts sur les semences?"
author: Anne-Charlotte Moy
date: 2012-08-31
href: http://www.infogm.org/spip.php?article5184
tags:
- Entreprise
- Associations
- Brevets logiciels
- Europe
---

> Actuellement, l’Union européenne prétend simplifier son système de propriété intellectuelle en instaurant un brevet unitaire européen (BUE). Mais derrière cette simplification se profile, de fait, une restriction d’accès aux semences pour les agriculteurs, à l’image de ce qui se fait en France. Entre les brevets sur les gènes, sur les procédés d’obtention et le certificat d’obtention végétale (COV), ce BUE apparaît plus comme une nouvelle arme dans la guerre que se livrent les entreprises semencières que comme une protection du droit des agriculteurs.
