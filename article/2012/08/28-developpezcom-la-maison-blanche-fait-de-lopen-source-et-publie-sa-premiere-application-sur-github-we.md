---
site: Developpez.com
title: "La Maison-Blanche fait de l'open source et publie sa première application sur GitHub, We The People est sous Drupal et MongoDB"
author: tarik benmerar
date: 2012-08-28
href: http://www.developpez.com/actu/46946/La-Maison-Blanche-fait-de-l-open-source-et-publie-sa-premiere-application-sur-GitHub-We-The-People-est-sous-Drupal-et-MongoDB/
tags:
- Internet
- Institutions
- International
---

> C'est une première. La Maison-Blanche vient de distribuer la première application open source créée par un gouvernement, disponible dans son dépôt GitHub officiel. Il s’agit d’une application permettant à tout citoyen de créer, voter et faire voter une pétition. C’est le code même qui propulse l’application «We The People» (nous le peuple) qu'on retrouve sur le site de la Maison-Blanche.
