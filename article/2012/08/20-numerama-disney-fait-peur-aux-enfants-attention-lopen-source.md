---
site: Numerama
title: "Disney fait peur aux enfants: attention à l'open-source!"
author: Guillaume Champeau
date: 2012-08-20
href: http://www.numerama.com/magazine/23444-disney-fait-peur-aux-enfants-attention-a-l-open-source.html
tags:
- Entreprise
- Sensibilisation
- Désinformation
---

> Aux Etats-Unis, Disney Channel a diffusé vendredi dernier un épisode de sa série pour enfants Shake It Up, dont une toute petite scène provoque la colère (un brin disproportionnée) de certains développeurs open-source. Visiblement, une catastrophe énorme est en passe de se produire dans cet épisode, à cause d'une jeune étudiante naïve qui a eu la bêtise crasse d'utiliser du code source ouvert. Paniqués, ses amis se regroupent autour d'un petit génie de l'informatique d'une dizaine d'années.
