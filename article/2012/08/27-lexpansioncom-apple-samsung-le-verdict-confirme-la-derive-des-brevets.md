---
site: L'Expansion.com
title: "Apple-Samsung: le verdict confirme la dérive des brevets"
author: Ludwig Gallet
date: 2012-08-27
href: http://lexpansion.lexpress.fr/high-tech/apple-samsung-pourquoi-le-verdict-confirme-la-derive-des-brevets_328079.html
tags:
- Entreprise
- Interopérabilité
- Institutions
- Brevets logiciels
- Innovation
---

> Depuis quelques années, le régime protégeant les brevets a été détourné de son but initial, et la guerre des brevets a dégénéré. Le verdict rendu par la cour de San José ne fait que confirmer les dérives actuelles. Explications.
