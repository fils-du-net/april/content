---
site: La Presse.ca
title: "Le Québec à l'heure de «l'économie 3.0»"
author: Alain McKenna
date: 2012-08-06
href: http://techno.lapresse.ca/nouvelles/201208/06/01-4562630-le-quebec-a-lheure-de-leconomie-30.php
tags:
- Entreprise
- Internet
- Économie
- Partage du savoir
- Matériel libre
- Institutions
- International
---

> À travers des regroupements régionaux de bidouilleurs et d'amateurs de nouvelles technologies, le Québec tombe lentement sous le charme de «l'économie 3.0», un modèle économique plaçant l'internet à haut débit, les données ouvertes et les technologies libres au coeur du développement régional et rural.
