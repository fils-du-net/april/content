---
site: Le Monde Informatique
title: "Procès Android: Oracle et Google sommés de révéler blogueurs et journalistes rétribués"
author: M.G.
date: 2012-08-08
href: http://www.lemondeinformatique.fr/actualites/lire-proces-android-oracle-et-google-sommes-de-reveler-blogueurs-et-journalistes-retribues-50000.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Droit d'auteur
---

> D'ici le 17 août, Oracle et Google doivent communiquer les noms des journalistes ou commentateurs ayant été rétribués par les deux sociétés et qui ont été amenés à écrire sur les questions qui les opposent dans le procès sur la violation de brevets Java dans Android. Le blogueur Florian Mueller avait notamment révélé une mission de conseil pour Oracle.
