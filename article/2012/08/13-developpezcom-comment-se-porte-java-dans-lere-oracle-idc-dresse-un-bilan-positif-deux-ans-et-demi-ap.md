---
site: Developpez.com
title: "Comment se porte Java dans l'ère Oracle? IDC dresse un bilan positif deux ans et demi après l'acquisition de Sun"
author: tarikbenmerar
date: 2012-08-13
href: http://www.developpez.com/actu/46547/Comment-se-porte-Java-dans-l-ere-Oracle-IDC-dresse-un-bilan-positif-deux-ans-et-demi-apres-l-acquisition-de-Sun/
tags:
- Entreprise
- Internet
- Licenses
---

> L'heure est aux bilans pour IDC. Deux ans et demi après l'acquisition de Sun par Oracle, comment se porte le langage Java?
