---
site: Numerama
title: "Hadopi: les Labs, c'est fini. Pas la riposte graduée."
author: Guillaume Champeau
date: 2012-12-21
href: http://www.numerama.com/magazine/24578-hadopi-les-labs-c-est-fini-pas-la-riposte-graduee.html
tags:
- Internet
- HADOPI
- Institutions
- Associations
- Droit d'auteur
---

> Certains y verront une bonne nouvelle. Ce n'est pourtant pas sûr. La Hadopi a décidé de fermer ses Labs à la fin  de l'année, en raison du "contexte institutionnel" (la mission Lescure) qui menaçait de toute façon leur maintien, et des restrictions budgétaires. Le recadrage se met en place, pour remettre l'accent sur la seule fonction répressive.
