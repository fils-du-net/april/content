---
site: Developpez.com
title: "\"Adoption des Big Data: ce n'est que le commencement\", selon Talend, qui analyse cette nouvelle tendance"
date: 2012-12-11
href: http://www.developpez.com/actu/50180/-Adoption-des-Big-Data-ce-n-est-que-le-commencement-selon-Talend-qui-analyse-cette-nouvelle-tendance
tags:
- Entreprise
- Innovation
---

> Les solutions open source sont un moyen efficace pour faire face à la complexité du Big Data. Pour Talend, elles sont particulièrement efficaces lorsqu’elles sont combinées avec des solutions open source de management, de qualité et d’intégration de données, fournies par des éditeurs comme Talend, qui les aident à abattre les barrières techniques.
