---
site: Silicon.fr
title: "Dossier: Linux s’impose dans le monde des affaires"
author: Ariane Beky
date: 2012-12-19
href: http://www.silicon.fr/dossier-linux-monde-des-affaires-81984.html
tags:
- Entreprise
- Économie
- Institutions
- Innovation
---

> Fruit de développements collaboratifs, Linux a fait ses classes dans la recherche avant de s’imposer dans l’entreprise, des administrations aux places boursières convaincues par l’excellent rapport performance/coût des offres libres et ouvertes.
