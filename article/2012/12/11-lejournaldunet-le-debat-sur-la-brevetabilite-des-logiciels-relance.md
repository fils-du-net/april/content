---
site: LeJournalduNet
title: "Le débat sur la brevetabilité des logiciels relancé"
author: Antoine Crochet-Damais
date: 2012-12-11
href: http://www.journaldunet.com/solutions/dsi/brevet-unitaire-europeen-et-brevet-logiciel-1212.shtml
tags:
- Administration
- April
- Institutions
- Brevets logiciels
- Innovation
- Europe
---

> Le Parlement européen a approuvé le principe de brevet unique. Avec pour objectif de simplifier le dépôt de brevet au sein de l'Union européenne, il remet sur la table le débat sur le brevet logiciel.
