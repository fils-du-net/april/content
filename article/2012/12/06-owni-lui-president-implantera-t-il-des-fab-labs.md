---
site: Owni
title: "Lui, président, implantera-t-il des fab labs?"
author: Sabine Blanc
date: 2012-12-06
href: http://owni.fr/2012/12/06/lui-president-implantera-t-il-des-fab-labs
tags:
- Entreprise
- Économie
- Éducation
- Innovation
---

> Ce matin, une réunion a eu lieu au cabinet de la ministre déléguée aux PME, à l’Innovation et à l’Économie numérique sur les fab labs. Une rencontre motivée par le passage du DG de TechShop, une chaine d’ateliers géants pour bricoleurs pointus américaine, la version lucrative des fab labs. Selon nos informations, des TechShops ouvriraient à Paris, Londres, Milan.
