---
site: Agoravox TV
title: "Karen Sandler &amp; le logiciel libre: code fermé pour cœur ouvert"
author: Isdomir
date: 2012-12-11
href: http://www.agoravox.tv/actualites/societe/article/karen-sandler-le-logiciel-libre-37267
tags:
- Entreprise
- Logiciels privateurs
- Video
---

> Le cœur** de Karen Sandler nécessite d'être monitoré par un défibrillateur interne, censé relancer un cœur menaçant de s'arrêter à tout moment... Le déclenchement et le fonctionnement du défibrillateur sont assurés par un logiciel dont le code fermé appartient à la firme Medtronic. Karen voudrait en savoir davantage sur ce code informatique gérant le défibrillateur implanté dans son corps ; on peut la comprendre, sachant que ces lignes code sont les garantes de sa survie en cas d'arrêt cardiaque (que sa maladie peut provoquer à chaque instant).
