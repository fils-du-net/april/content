---
site: BBC News
title: "European Parliament votes for a unified patent scheme"
date: 2012-12-11
href: http://www.bbc.co.uk/news/technology-20679665
tags:
- Administration
- April
- Institutions
- Brevets logiciels
- Innovation
- Europe
- English
---

> (Le parlement Européen vote en faveur d'un système de brevet unifié, mais le mouvement est opposé par l'Italie et l'Espagne) The European Parliament votes in favour of a unified patent scheme, but the move is being opposed by Italy and Spain.
