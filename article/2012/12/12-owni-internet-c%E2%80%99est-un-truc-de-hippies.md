---
site: Owni
title: "Internet, c’est un truc de hippies"
author: Laurent Chemla
date: 2012-12-12
href: http://owni.fr/2012/12/12/internet-cest-un-truc-de-hippies
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Informatique-deloyale
- Informatique en nuage
---

> Conçu en pleine période Flower Power par des barbus libertaires, Internet n'a jamais perdu – malgré les tentatives de récupération politiques et commerciales – son esprit profondément lié au partage. Cette prise de conscience doit perdurer et produire un acte de résistance face à la tentative forcenée de nivellement du monde par les inconscients qui nous gouvernent.
