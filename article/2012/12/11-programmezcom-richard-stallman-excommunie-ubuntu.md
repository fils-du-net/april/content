---
site: Programmez.com
title: "Richard Stallman excommunie Ubuntu!"
author: Frédéric Mazué
date: 2012-12-11
href: http://www.programmez.com/actualites.php?id_actu=12592
tags:
- Entreprise
- Informatique-deloyale
- Philosophie GNU
---

> C'est le buzz du jour. Richard Stallman, dans un billet sur le blog de la Free Software Fondation, que framablog a traduit en français, revient sur quelque chose qui fait beaucoup parler et génère beaucoup de mécontentement depuis le mois de sepembre, dans le monde du logiciel libre.
