---
site: clubic.com
title: "Pack liberté, pour un Internet avec chatons en soutien à April, Quadrature et Framasoft"
author: Alexandre Laurent
date: 2012-12-03
href: http://www.clubic.com/internet/actualite-528477-pack-liberte-internet-blanc-blanc-april-quadrature-framasoft.html
tags:
- Internet
- Économie
- April
- Associations
---

> L'April, la Quadrature du Net et Framasoft relancent leur campagne d'appel aux dons initiée en début d'année, baptisée Pack Liberté. En achetant ce baril de lessive 2.0, l'internaute soutiendra les trois associations et, accessoirement, contribuera sans doute à sauver des chatons.
