---
site: PC INpact
title: "Il y a dix ans, Microsoft savait que les DRM étaient une cause perdue"
author: Vincent Hermann
date: 2012-12-03
href: http://www.pcinpact.com/news/75755-il-y-a-dix-ans-microsoft-savait-que-drm-etaient-cause-perdue.htm
tags:
- Entreprise
- Internet
- DRM
---

> Il y a dix ans, Microsoft travaillait ardemment sur les technologies qui lui permettraient de sécuriser sa plateforme pour la distribution des contenus. DRM, Palladium, TCPA ou encore NGSCB étaient des termes qui alimentaient toutes les peurs. Pourtant, quatre ingénieurs de la firme savaient que rien de tout ceci ne fonctionnerait sur le long terme.
