---
site: Eurojuris
title: "L’Open data: un gage de démocratie dans les collectivités locales?"
author: MARTIN Mathieu
date: 2012-12-26
href: http://www.eurojuris.fr/fre/collectivites/services-publics/usagers/articles/open-data-collectivite-locale.html
tags:
- Internet
- Administration
- Institutions
- Licenses
- Open Data
---

> L’Open data vise la mise à disposition des innombrables données collectées et produites par les administrations dans l’exercice de leur mission de service public aux fins de les valoriser.
