---
site: PC INpact
title: "L'action de groupe programmée en France au printemps 2013"
author: Marc Rees
date: 2012-12-20
href: http://www.pcinpact.com/news/76264-laction-groupe-programmee-en-france-au-printemps-2013.htm
tags:
- Entreprise
- April
- Institutions
- Vente liée
- Associations
---

> Le rôle central confié à des associations agréées ne plaît cependant pas nécessairement à toutes les structures. L'April, qui défend le logiciel libre, a répondu à la consultation de la DGCCRF en concentrant son attention sur les dangers de la vente liée. Et sur la question structurelle, elle milite plutôt pour que l’action de groupe soit ouverte aux « structures pertinentes », pas seulement à quelques associations agréées.
