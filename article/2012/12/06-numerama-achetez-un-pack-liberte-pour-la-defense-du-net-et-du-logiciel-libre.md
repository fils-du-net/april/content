---
site: Numerama
title: "Achetez un Pack Liberté pour la défense du net et du logiciel libre"
author: Guillaume Champeau
date: 2012-12-06
href: http://www.numerama.com/magazine/24442-achetez-un-pack-liberte-pour-la-defense-du-net-et-du-logiciel-libre.html
tags:
- April
- Associations
---

> Comme l'an dernier, en marge de la campagne de dons spécifique ouverte par La Quadrature du Net, le collectif s'est associé à L'April et Framasoft pour vendre un très symbolique "Pack Liberté". Sur un modèle désormais bien connu, l'internaute qui souhaite apporter son aide financière aux trois organisations peut choisir le montant total de son don, puis déterminer la répartition par un système de réglettes. Concrètement, les dons sont collectés par le FDNN (Fonds de défense de la neutralité du net), qui redistribue la somme en fonction des répartitions imposées par les donateurs:
