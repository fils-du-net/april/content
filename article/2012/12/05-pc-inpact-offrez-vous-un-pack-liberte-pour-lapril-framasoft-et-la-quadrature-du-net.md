---
site: PC INpact
title: "Offrez-vous un Pack Liberté pour l'April, Framasoft et la Quadrature du Net"
author: Marc Rees
date: 2012-12-05
href: http://www.pcinpact.com/news/75821-offrez-vous-pack-liberte-pour-april-framasoft-et-quadrature-net.htm
tags:
- Internet
- April
- Associations
---

> ette année encore, trois initiatives se regroupent pour lancer leur Pack Liberté. Une formule destinée à recueillir des dons afin de soutenir en commun leur action.
