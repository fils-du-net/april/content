---
site: ZDNet.fr
title: "Débat – Le brevet unitaire européen, une \"usine à gaz\"?"
author: Antoine Duvauchelle
date: 2012-12-19
href: http://www.zdnet.fr/actualites/debat-le-brevet-unitaire-europeen-une-usine-a-gaz-39785610.htm
tags:
- Entreprise
- Administration
- Institutions
- Brevets logiciels
- Innovation
- Europe
---

> Dire qu'il était attendu constitue un euphémisme: le brevet unitaire européen a été validé la semaine dernière par les instances de l'UE. Son organisation pose clairement question. Et quelles seront les conséquences sur le monde du logiciel?
