---
site: ZN
title: "Apple et Google sur le point de racheter 1100 brevets Kodak"
date: 2012-12-10
href: http://www.zone-numerique.com/news-23829-je-t-aime-toi-non-plus-apple-google-ensemble-sur-le-rachat-des-1100-brevets-kodak.html
tags:
- Entreprise
- Brevets logiciels
- Innovation
---

> Si Apple et Google semblent bien concurrents et de façon frontale, avec leurs systèmes d'exploitation mobiles iOS et Android, ils pourraient toutefois nouer une alliance pour le rachat des 1100 brevets relatifs à l'image numérique mis en vente par Kodak l'été dernier, rapporte Bloomberg,
