---
site: ITespresso.fr
title: "Brevet unitaire: le gouvernement et le MEDEF jubilent, pas l'APRIL"
author: Philippe Guerrier
date: 2012-12-12
href: http://www.itespresso.fr/brevet-unitaire-gouvernement-medef-jubilent-april-dezingue-59805.html
tags:
- Entreprise
- Économie
- April
- Institutions
- Brevets logiciels
- Innovation
- Europe
---

> Chacun de leur côté, l’exécutif français et l’organisation patronale évoquent «une avancée essentielle» pour la compétitivité. Mais l’APRIL ne retient qu’une «insécurité juridique» avec le nouveau système.
