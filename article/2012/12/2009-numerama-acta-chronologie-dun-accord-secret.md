---
site: numerama.com
title: "ACTA: chronologie d'un accord secret"
author: Michael Geist
date: 2009-12-12
href: http://www.numerama.com/magazine/14701-acta-chronologie-d-un-accord-secret.html
tags:
- Internet
---

> Négocié depuis 2007, l'ACTA vise à renforcer les droits de la propriété intellectuelle. Or, plutôt que de tenir un débat public sur la question, le texte est secrètement rédigé par quelques délégations, loin du public et loin des associations de défense. Michael Geist, professeur de droit au Canada, revient sur la chronologie d'un texte qui a déjà fait couler beaucoup d'encre.
