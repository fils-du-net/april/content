---
site: Infobourg.com
title: "Le Cégep de Rimouski se tourne vers les logiciels libres"
author: Nathalie Côté
date: 2012-12-05
href: http://www.infobourg.com/2012/12/05/le-cegep-de-rimouski-se-tourne-vers-les-logiciels-libres
tags:
- Logiciels privateurs
- Administration
- Interopérabilité
- Éducation
---

> Le Cégep de Rimouski a décidé de laisser tomber Microsoft et de se tourner vers la suite bureautique LibreOffice. La grande majorité de ses 1600 postes de travail devrait être convertie d’ici l’automne 2013.
