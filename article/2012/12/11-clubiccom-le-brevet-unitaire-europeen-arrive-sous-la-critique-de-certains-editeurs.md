---
site: clubic.com
title: "Le brevet unitaire européen arrive sous la critique de certains éditeurs"
author: Olivier Robillart
date: 2012-12-11
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/brevet-logiciel/actualite-530230-brevet-unitaire-europeen.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Innovation
- Europe
---

> Les ministres européens de l'Industrie ont déjà donné leur accord et le Parlement européen vient de voter ce mardi pour mettre en place un système de brevet unique, commun à l'Union européenne. Alors que sa mise en place semble en marche, Nokia et le géant de l'aéronautique BAE Systems font front commun afin que l'institution communautaire revoie sa copie.
