---
site: "Le Journal de Saint-Denis"
title: "Libérons-nous des logiciels payants!"
author: Marylène Lenfant
date: 2012-12-09
href: http://www.lejsd.com/index.php?r=14841
tags:
- Administration
- April
- Promotion
---

> Quant à utiliser son ordinateur avec des logiciels libres et gratuits, que l’on peut s’échanger, faire évoluer, la démarche en remonte aux années 80. Longtemps circonscrite à des cercles de spécialistes en informatique, cette communauté du libre pourrait bien prendre un nouvel essor parce qu’on «est dans la même dynamique, la même volonté de reprendre le contrôle. Et c’est à la portée de tout le monde», observe Frédéric Couchet.
