---
site: Le Monde Informatique
title: "2012, un bon cru pour Linux"
author: Véronique Arène
date: 2012-12-28
href: http://www.lemondeinformatique.fr/actualites/lire-2012-un-bon-cru-pour-linux-51840.html
tags:
- Entreprise
- Économie
---

> De nombreux facteurs ont permis à Linux de faire de grands progrès sur le marché en 2012.
