---
site: écrans
title: "Le Web prend de l’auteur"
author: Sophian Fanen
date: 2012-12-18
href: http://www.ecrans.fr/Le-Web-prend-de-l-auteur,15699.html
tags:
- Institutions
- Droit d'auteur
- Licenses
- Contenus libres
---

> Créées sur le modèle des logiciels libres du projet GNU, pensé par Richard Stallman au début des années 80, les Creative Commons existent à côté d’autres licences dites libres, comme GPL ou Art libre. Celles-ci sont parfois plus restrictives, là où les CC permettent le commerce rémunéré autant que l’échange informel. L’important, ici, est que la paternité d’une œuvre soit toujours respectée.
