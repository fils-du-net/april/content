---
site: Le Monde Informatique
title: "Le succès de GitHub atteste de l'adoption de l'Open Source par l'entreprise"
author: Jean Elyan
date: 2012-12-24
href: http://www.lemondeinformatique.fr/actualites/lire-le-succes-de-github-atteste-de-l-adoption-de-l-open-source-par-l-entreprise-51809.html
tags:
- Entreprise
- Économie
- Innovation
---

> Le référentiel de projets Open Source GitHub accueille pratiquement 10 000 nouveaux inscrits chaque jour. Une confirmation parmi d'autres de l'acceptation de ces projets au-delà de leur sphère d'origine.
