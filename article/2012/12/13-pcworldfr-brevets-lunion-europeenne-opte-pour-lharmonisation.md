---
site: PCWorld.fr
title: "Brevets: l'Union Européenne opte pour l'harmonisation"
author: Michel Beck
date: 2012-12-13
href: http://www.pcworld.fr/high-tech/actualites,brevets-union-europeenne,534101,1.htm
tags:
- April
- Institutions
- Brevets logiciels
- Innovation
- Europe
---

> Pour obtenir l'ultime édition de l'émission Cauchemar en Cuisine, réunissez une tripotée de députés européens dans un hémicycle et demandez-leur d'aller se faire cuire un oeuf.
