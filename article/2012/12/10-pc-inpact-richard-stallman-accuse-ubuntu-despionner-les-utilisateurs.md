---
site: PC INpact
title: "Richard Stallman accuse Ubuntu d'espionner les utilisateurs"
author: Vincent Hermann
date: 2012-12-10
href: http://www.pcinpact.com/news/75922-richard-stallman-accuse-ubuntu-despionner-utilisateurs.htm
tags:
- Entreprise
- Internet
- Informatique-deloyale
- Philosophie GNU
---

> Richard Stallman est connu pour ses prises de position souvent tranchées vis-à-vis du code propriétaire et de sociétés telles que Microsoft, Apple ou encore Valve. Pourtant, il ne vise pas cette fois un empire du «closed source», mais l’éditeur de la célèbre distribution Ubuntu: Canonical.
