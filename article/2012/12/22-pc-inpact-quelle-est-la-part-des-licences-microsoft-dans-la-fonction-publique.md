---
site: PC INpact
title: "Quelle est la part des licences Microsoft dans la fonction publique?"
author: Marc Rees
date: 2012-12-22
href: http://www.pcinpact.com/news/76212-quelle-est-part-licences-microsoft-dans-fonction-publique.htm
tags:
- Logiciels privateurs
- Administration
- HADOPI
- Institutions
---

> Les logiciels Microsoft sont souvent mis en avant dans les marchés publics. Que ce soit des autorités centrales, locales ou des autorités indépendantes comme la Hadopi, on ne compte plus les marchés publics qui visent expressément les produits made in Redmond. Isabelle Attard, députée écologiste veut justement que soit jaugée la part des logiciels Microsoft dans la fonction publique.
