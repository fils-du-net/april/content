---
site: The Guardian
title: "EU Unitary Patent has obvious flaws"
author: Richard Willoughby
date: 2012-12-19
href: http://www.guardian.co.uk/law/2012/dec/19/eu-unitarty-patent-and-technology-sector
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Innovation
- Europe
- English
---

> (Le paquet Brevet génère des inquiétudes pour divers secteurs) Patent package gives technology and other sectors cause for concern
