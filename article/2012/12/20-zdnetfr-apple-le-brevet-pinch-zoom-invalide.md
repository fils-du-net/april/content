---
site: ZDNet.fr
title: "Apple: le brevet 'pinch to zoom' invalidé"
author: Thierry Noisette
date: 2012-12-20
href: http://www.zdnet.fr/actualites/apple-le-brevet-pinch-to-zoom-invalide-39785641.htm
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Innovation
---

> L'office américain de dépôt des brevets remet en cause un des brevets centraux revendiqué par Apple dans sa guerre contre Samsung.
