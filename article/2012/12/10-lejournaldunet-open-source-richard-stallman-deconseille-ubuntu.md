---
site: LeJournalduNet
title: "Open Source: Richard Stallman déconseille Ubuntu"
author: Virgile Juhan
date: 2012-12-10
href: http://www.journaldunet.com/solutions/saas-logiciel/ubuntu-12-10-quantal-quetzal-et-la-recheche-amazon-critiquee-1212.shtml
tags:
- Entreprise
- Informatique-deloyale
- Philosophie GNU
---

> Le pape de l'Open Source n'a pas mâché ses mots contre la dernière version d'Ubuntu qui contiendrait un "dangereux spyware".
