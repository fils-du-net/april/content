---
site: Krinein
title: "La philosophie du libre"
author: nazonfly
date: 2012-12-24
href: http://sciences-tech.krinein.com/philosophie-libre-20179.html
tags:
- Entreprise
- Interopérabilité
- Sensibilisation
- Philosophie GNU
---

> Notre découverte du monde Linux se poursuit avec sa philosophie. Car oui, comme Microsoft et Apple, GNU/Linux a son propre mode de pensée. Microsoft par exemple a pour idée directrice de s'imposer avec force un peu partout pour qu'ainsi on soit complètement dépendant de Microsoft et que la firme de Redmond se fasse un maximum de blé possible. Apple a suivi une doctrine radicalement différente : faire de beaux produits mais totalement fermés pour qu'on soit complètement dépendant d'Apple et que la firme de Cupertino se fasse un maximum de blé possible. Comment ça je trolle ? Si peu, si peu.
