---
site: Numerama
title: "DRM: la Hadopi ne rendra pas d'avis avant 2013"
author: Julien L.
date: 2012-12-04
href: http://www.numerama.com/magazine/24423-drm-la-hadopi-ne-rendra-pas-d-avis-avant-2013.html
tags:
- HADOPI
- DRM
---

> Qu'il s'agisse de la saisine de VideoLAN ou celle de la Bibliothèque Nationale de France, aucun avis ne sera rendu cette année. La Hadopi compte poursuivre ses consultations dans un cas comme dans l'autre. Si le report du verdict à l'année prochaine était déjà connu pour VideoLAN, celui de la BNF a été confirmé ce mardi via l'ouverture du débat à  toute personne intéressée par ces problématiques.
