---
site: Rue89
title: "Conso alternative: «Les classes moyennes ont changé de valeurs»"
author: Sophie Caillat
date: 2012-12-30
href: http://www.rue89.com/2012/12/30/conso-alternative-les-classes-moyennes-ont-change-de-valeurs-238061
tags:
- Économie
- Partage du savoir
---

> Rencontre avec l’auteure d’un livre porteur d’espoir: partout dans le monde, des citoyens s’organisent pour subvenir à leurs besoins et inventer une autre société.
