---
site: "ouest-france.fr"
title: "Infothéma: pour découvrir les logiciels libres - Belle-Isle-en-Terre"
date: 2012-12-13
href: http://www.ouest-france.fr/actu/actuLocale_-Infothema-pour-decouvrir-les-logiciels-libres-_22005-avd-20121213-64197668_actuLocale.Htm
tags:
- Administration
- Associations
---

> Samedi matin, l'association Infothéma a tenu son assemblée générale dans les locaux de la cybercommune. D'après le président Eric Le Floch, 2012 a été particulièrement riche, avec l'organisation de la première édition du salon informatique Infothéma, à Bégard, sur les logiciels libres. Cette première a rencontré un joli succès auprès des visiteurs et vu la participation d'une dizaine d'acteurs du logiciel libre: Maison du libre 29, Ordi solidaire, atelier la Souris...
