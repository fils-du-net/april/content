---
site: Silicon.fr
title: "Jim Whitehurst (Red Hat): «L’open source est le choix par défaut pour les architectures IT de nouvelle génération»"
author: David Feugey
date: 2012-12-05
href: http://www.silicon.fr/red-hat-jim-whitehurst-open-source-entretien-81609.html
tags:
- Entreprise
- Innovation
---

> Entretien-fleuve avec le patron de Red Hat, Jim Whitehurst. Une personnalité qui comprend les rouages de l’open source et le bien-fondé de cette approche.
