---
site: PC INpact
title: "Les projets numériques du gouvernement pour l’Éducation nationale"
author: Xavier Berne
date: 2012-12-13
href: http://www.pcinpact.com/news/76039-les-projets-numeriques-gouvernement-pour-l-education-nationale.htm
tags:
- Internet
- Administration
- April
- Institutions
- Éducation
---

> Notons que le libre ne fut pas évoqué par le locataire de la Rue de Grenelle, et ce en dépit des récentes préconisations de l’April, qui réclamait il y a quelques semaines une série de mesures pour le numérique à l’école, collège et lycée. Celles-ci reposaient toutes sur les principes des licences libres (voir notre article: L’April défend les vertus du logiciel libre dans l'Éducation nationale).
