---
site: PC INpact
title: "Les eurodéputés adoptent le brevet unitaire européen"
author: Marc Rees
date: 2012-12-11
href: http://www.pcinpact.com/news/75975-les-eurodeputes-adoptent-brevet-unitaire-europeen.htm
tags:
- Entreprise
- Administration
- Économie
- April
- Institutions
- Brevets logiciels
- Europe
---

> Après une matinée de débat en séance plénière, les eurodéputés ont finalement adopté le brevet unitaire européen. Par la même occasion, ils ont fait fi des risques dénoncés.
