---
site: Framablog
title: "Richard Stallman: Un logiciel espion dans Ubuntu! Que faire?"
author: Richard Stallman
date: 2012-12-08
href: http://www.framablog.org/index.php/post/2012/12/08/stallman-ubuntu-espion
tags:
- Entreprise
- Logiciels privateurs
- Informatique-deloyale
- Philosophie GNU
---

> L’un des principaux avantages du logiciel libre est que la communauté protège les utilisateurs des logiciels malveillants. Aujourd’hui Ubuntu GNU/Linux est devenu un contre-exemple. Que devons-nous faire?
