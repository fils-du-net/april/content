---
site: ZDNet.fr
title: "Savoir-vivre: accueillir Richard Stallman, embûches et impératifs"
author: Thierry Noisette
date: 2012-12-30
href: http://www.zdnet.fr/actualites/savoir-vivre-accueillir-richard-stallman-embuches-et-imperatifs-39785778.htm
tags:
- Philosophie GNU
---

> Recevoir RMS, pionnier du mouvement du logiciel libre, conférencier passionné et... hôte passablement rustique, c'est tout un art. Un document officiel énumère les choses à faire et à ne pas faire lorsqu'on le reçoit.
