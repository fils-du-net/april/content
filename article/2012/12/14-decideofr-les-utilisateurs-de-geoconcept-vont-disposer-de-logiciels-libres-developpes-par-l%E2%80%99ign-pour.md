---
site: Decideo.fr
title: "Les utilisateurs de GeoConcept vont disposer de logiciels libres développés par l’IGN pour l’actualisation des données"
date: 2012-12-14
href: http://www.decideo.fr/Les-utilisateurs-de-GeoConcept-vont-disposer-de-logiciels-libres-developpes-par-l-IGN-pour-l-actualisation-des-donnees_a5763.html
tags:
- Internet
- Administration
- Licenses
- Open Data
---

> L’Institut national de l’information géographique et forestière (IGN) et GeoConcept SA, concepteur de technologies cartographiques d’optimisation pour les professionnels, annoncent la mise à disposition de modules développés par l’IGN et facilitant l’utilisation de ses données de référence au sein de GeoConcept, ainsi que les échanges de données entre partenaires
