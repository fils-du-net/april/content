---
site: clubic.com
title: "Les nouveaux barèmes de la copie privée sont adoptés"
author: Olivier Robillart
date: 2012-12-14
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/droit-auteur/actualite-531282-copie-privee.html
tags:
- Entreprise
- Économie
- HADOPI
- Institutions
- Droit d'auteur
- Informatique en nuage
---

> Malgré le départ de la quasi-totalité des représentants des industriels, la commission pour la copie privée vient de voter en faveur de la fixation de nouveaux barèmes applicables aux supports de stockages, Box, lecteurs MP3… Elle rappelle également que la redevance a été fixée suite à des études d'usages menées par un institut indépendant.
