---
site: Developpez.com
title: "\"Ubuntu: un logiciel espion\" pour Richard Stallman, qui s'insurge contre l'intégration de la recherche Amazon dans l'OS"
author: Hinault Romaric
date: 2012-12-10
href: http://www.developpez.com/actu/50122/-Ubuntu-un-logiciel-espion-pour-Richard-Stallman-qui-s-insurge-contre-l-integration-de-la-recherche-Amazon-dans-l-OS
tags:
- Entreprise
- Informatique-deloyale
- Philosophie GNU
---

> La version la plus récente d’Ubuntu (12.10 Quetzal Quantal) intègre une fonctionnalité polémique permettant d’afficher des suggestions de produits à acheter sur Amazon aux utilisateurs.
