---
site: Le Monde.fr
title: "Le système de brevets américain largement détourné par des \"trolls\""
date: 2012-12-11
href: http://www.lemonde.fr/technologies/article/2012/12/11/le-systeme-de-brevets-americain-largement-detourne-par-des-trolls_1804621_651865.html
tags:
- Entreprise
- Économie
- Institutions
- Brevets logiciels
- Innovation
- International
---

> Une étude estime que 61 % des procès pour violation de brevet menés aux Etats-Unis le sont par des entreprises qui ne vivent que de ces procédures et non de la création de produits.
