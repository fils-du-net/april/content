---
site: Numerama
title: "Deux fois moins de dons pour la défense du net et du logiciel libre"
author: Julien L.
date: 2012-12-18
href: http://www.numerama.com/magazine/24547-deux-fois-moins-de-dons-pour-la-defense-du-net-et-du-logiciel-libre.html
tags:
- Internet
- Économie
- April
- Associations
---

> La deuxième campagne de dons pour défendre les libertés de individus sur Internet et promouvoir le logiciel libre a beaucoup moins mobilisé les foules. Alors que l'opération se termine dans quelques heures, les premiers retours indiquent que les dons ont rapporté deux fois moins que lors de la première édition.
