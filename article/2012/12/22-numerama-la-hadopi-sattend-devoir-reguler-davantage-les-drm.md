---
site: Numerama
title: "La Hadopi s'attend à devoir réguler davantage les DRM"
author: Guillaume Champeau
date: 2012-12-22
href: http://www.numerama.com/magazine/24077-la-hadopi-s-attend-a-devoir-reguler-davantage-les-drm.html
tags:
- HADOPI
- DRM
- Droit d'auteur
---

> Exclusivement connue pour ses actions de répression (officiellement, de "pédagogie"), l'Hadopi veut être de plus en plus présente sur un champ méconnu de ses compétences: la régulation des DRM pour la protection des intérêts des consommateurs.
