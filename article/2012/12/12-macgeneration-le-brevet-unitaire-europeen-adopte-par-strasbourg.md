---
site: MacGeneration
title: "Le brevet unitaire européen adopté par Strasbourg"
author: Stéphane Moussie
date: 2012-12-12
href: http://www.macg.co/news/voir/258177/le-brevet-unitaire-europeen-adopte-par-strasbourg
tags:
- Entreprise
- Administration
- April
- Institutions
- Brevets logiciels
- Innovation
- Europe
---

> Le Parlement européen a adopté hier le brevet unitaire européen qui s'appliquera dans toute l'Europe à partir du 1er janvier 2014, sauf en Espagne et en Italie qui se sont tous deux exclus du régime.
