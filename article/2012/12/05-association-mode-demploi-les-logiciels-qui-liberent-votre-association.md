---
site: Association mode d'emploi
title: "Les logiciels qui libèrent votre association"
date: 2012-12-05
href: http://www.associationmodeemploi.fr/PAR_TPL_IDENTIFIANT/20572/TPL_CODE/TPL_REV_ARTSEC_FICHE/PAG_TITLE/Les+logiciels+qui+lib%E8rent+votre+association/2465-les-articles-de-presse.htm
tags:
- Économie
- April
- Associations
---

> Les logiciels libres et les associations prônent les mêmes valeurs d'échange, de partage, de collaboration... Un guide de l'April «Libre Association» propose des solutions pour mieux travailler ensemble.
