---
site: PC INpact
title: "ACTA: La Commission européenne retire sa procédure devant la CJUE"
author: Marc Rees
date: 2012-12-19
href: http://www.pcinpact.com/news/76233-acta-la-commission-europeenne-retire-sa-procedure-devant-cjue.htm
tags:
- Institutions
- Europe
- ACTA
---

> Françoise Castex nous l’indique à l’instant sur son fil twitter: la Commission Européenne vient de retirer sa procédure devant la Cour de justice de l'Union européenne (CJUE) sur le dossier ACTA.
