---
site: ZDNet.fr
title: "Express: logiciels libres IGN d'actualisation des données, ferme de fenêtre, cloud open source"
author: Thierry Noisette
date: 2012-12-29
href: http://www.zdnet.fr/actualites/express-logiciels-libres-ign-d-actualisation-des-donnees-ferme-de-fenetre-cloud-open-source-39785775.htm
tags:
- Entreprise
- Partage du savoir
- Innovation
- Informatique en nuage
---

> Bouquet de brèves: trois modules de l’Institut national de l’information géographique et forestière (IGN) sous licence libre. Les Windowfarms, un système open source de culture à domicile. Livre blanc Smile sur le cloud et la virtualisation avec des outils libres.
