---
site: clubic.com
title: "Logiciel libre: N. Sarkozy et F. Hollande répondent au secteur"
author: Alexandre Laurent
date: 2012-04-16
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/logiciel-libre-open-source/actualite-486796-logiciel-libre-sarkozy-hollande-repondent-secteur.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Administration
- Économie
- HADOPI
- Associations
- Brevets logiciels
- Droit d'auteur
- Éducation
- Innovation
- Marchés publics
- Neutralité du Net
- Standards
---

> Les équipes de campagne de Nicolas Sarkozy et de François Hollande ont répondu à une série de questions posées par le Conseil national du logiciel libre. Ils y précisent leurs positions quant à la place du libre dans l'économie, qui divergent parfois, notamment sur la problématique des brevets logiciels.
