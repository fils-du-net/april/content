---
site: LeMonde.fr
title: "Twitter propose une forme de brevet \"défensif\""
date: 2012-04-18
href: http://www.lemonde.fr/technologies/article/2012/04/18/twitter-propose-une-forme-de-brevet-defensif_1687190_651865.html
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
---

> L'entreprise propose un modèle d'accord, qu'elle fera signer à ses employés, garantissant à ses salariés une forme de contrôle sur les brevets qu'ils contribuent à créer.
