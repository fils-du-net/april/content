---
site: ZDNet.fr
title: "Linux: un «investissement collectif» de 10 milliards de dollars"
date: 2012-04-04
href: http://www.zdnet.fr/actualites/linux-un-investissement-collectif-de-10-milliards-de-dollars-39770364.htm
tags:
- Entreprise
- Économie
- Associations
---

> L’apport de Red Hat (un milliard de dollars de chiffre d’affaires) à Linux est indéniable reconnaît le directeur de la fondation Linux, Jim Zemlin. Néanmoins, Linux est d’abord un «investissement collectif» bien plus considérable.
