---
site: Génération nouvelles technologies
title: "Vente liée: déloyale en toutes circonstances"
author: Jérôme G.
date: 2012-04-08
href: http://www.generation-nt.com/vente-liee-forcee-ordinateur-systeme-exploitation-aful-actualite-1564291.html
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
---

> Pour l'Aful, un jugement historique a été rendu en début d'année. Il déclare déloyale en toutes circonstances la revente par un constructeur d'un système d'exploitation en procédant à sa préinstallation sans la demande du client.
