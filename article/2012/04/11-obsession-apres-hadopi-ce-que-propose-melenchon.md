---
site: OBSESSION
title: "Après-Hadopi: ce que propose Mélenchon"
author: Boris Manenti 
date: 2012-04-11
href: http://obsession.nouvelobs.com/high-tech/20120410.OBS5882/apres-hadopi-ce-que-propose-melenchon.html
tags:
- Entreprise
- Internet
- Administration
- Économie
- Interopérabilité
- HADOPI
- Institutions
- Droit d'auteur
---

> Le Parti de gauche propose de créer "une plate-forme de téléchargement gérée par une instance publique", explique la co-présidente du parti.
