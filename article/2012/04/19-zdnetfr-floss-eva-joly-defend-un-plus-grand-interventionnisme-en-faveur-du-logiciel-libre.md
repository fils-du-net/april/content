---
site: ZDNet.fr
title: "FLOSS: Eva Joly défend un plus grand interventionnisme en faveur du logiciel libre"
author: Christophe Auffray
date: 2012-04-19
href: http://www.zdnet.fr/actualites/floss-eva-joly-defend-un-plus-grand-interventionnisme-en-faveur-du-logiciel-libre-39770920.htm
tags:
- Entreprise
- Internet
- Administration
- Institutions
- Associations
- Brevets logiciels
- RGI
- Standards
- Europe
---

> Place privilégiée dans la commande publique et dans l’éducation, RGI contraignant écartant le format OOXML de Microsoft, rejet des brevets logiciels… la candidate écologiste répond au CNLL et dévoile ses mesures en faveur du logiciel libre et des entreprises du secteur.
