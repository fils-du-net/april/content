---
site: leJDD.fr
title: "Programme numérique des candidats UMP-PS: \"un catalogue de mesures sans vision globale\""
author: Gaspard Dhellemmes
date: 2012-04-27
href: http://www.lejdd.fr/Election-presidentielle-2012/Actualite/Programme-numerique-des-candidats-UMP-PS-505774/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Institutions
- Associations
---

> Alors que les deux candidats présents au second tour s’apprêtent à présenter jeudi via Fleur Pellerin et Nicolas Princen leur programme numérique, Guillaume Buffet, président du think-tank Renaissance numérique, exprime sa déception face à l’absence de ce sujet dans la campagne.
