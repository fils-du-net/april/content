---
site: Le Monde Informatique
title: "Microsoft, parmi les contributeurs clefs de Linux?"
author: Jean Elyan
date: 2012-04-04
href: http://www.lemondeinformatique.fr/actualites/lire-microsoft-parmi-les-contributeurs-clefs-de-linux-48448.html
tags:
- Entreprise
- Associations
---

> Le travail accompli par Microsoft sur le pilote de l'hyperviseur Hyper-V fait entrer l'éditeur dans la liste des importants contributeurs au développement du noyau de Linux.
