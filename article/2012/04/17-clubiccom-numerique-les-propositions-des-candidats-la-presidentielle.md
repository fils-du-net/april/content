---
site: clubic.com
title: "Numérique: les propositions des candidats à la présidentielle"
author: Olivier Robillart
date: 2012-04-17
href: http://pro.clubic.com/technologie-et-politique/actualite-487116-numerique-propositions-nicolas-sarkozy.html
tags:
- Entreprise
- Internet
- Administration
- Économie
- Interopérabilité
- Institutions
- Associations
- Innovation
- Marchés publics
- Standards
- International
- Open Data
---

> Cinq candidats ont répondu à l'appel d'un collectif de professionnels du numérique. Chacun de ces prétendants à la présidence de la République ont donc précisé leurs ambitions sur le sujet en appuyant leur propos sur certains points particuliers comme l'Innovation, la formation ou encore la fiscalité numérique.
