---
site: 01net.
title: "Logiciel libre: Hollande et Sarkozy presque d'accord"
author: Alexandre Salque
date: 2012-04-17
href: http://www.01net.com/editorial/564430/logiciel-libre-hollande-et-sarkozy-presque-d-accord/
tags:
- Entreprise
- Logiciels privateurs
- Économie
- HADOPI
- Associations
- Brevets logiciels
- Innovation
- Marchés publics
- Neutralité du Net
- Standards
---

> Le Conseil national du logiciel libre a adressé aux principaux candidats à la Présidentielle un questionnaire. Seuls Nicolas Sarkozy et François Hollande ont répondu.
