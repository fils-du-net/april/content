---
site: OWNI
title: "La licence globalement morte au PS"
author: Guillaume Ledit
date: 2012-04-27
href: http://owni.fr/2012/04/27/la-licence-globalement-morte-au-ps/
tags:
- Internet
- Économie
- HADOPI
- Institutions
- Droit d'auteur
- Licenses
---

> La licence globale, ou vie et mort d'une idée dans le champ politique. En particulier dans le champ du PS. Autour de François Hollande, lobbyistes de tous poils se sont employés à faire disparaître toute velléité de légalisation des échanges hors marché. À la veille du second tour de la présidentielle, ce gros cadeau ne leur a pas été refusé.
