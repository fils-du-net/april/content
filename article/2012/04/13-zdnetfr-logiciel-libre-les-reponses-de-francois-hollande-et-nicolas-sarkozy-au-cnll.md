---
site: ZDNet.fr
title: "Logiciel libre: les réponses de François Hollande et Nicolas Sarkozy au CNLL"
author: Thierry Noisette
date: 2012-04-13
href: http://www.zdnet.fr/blogs/l-esprit-libre/logiciel-libre-les-reponses-de-francois-hollande-et-nicolas-sarkozy-au-cnll-39770689.htm
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Brevets logiciels
- Éducation
- Innovation
- Neutralité du Net
- RGI
- Standards
- Informatique en nuage
- Europe
- International
---

> Si plusieurs points de convergence apparaissent entre les deux candidats, leurs principales différences se trouvent dans l'éducation, où François Hollande se montre beaucoup plus volontariste, et les brevets logiciels, soutenus par Nicolas Sarkozy et bannis par son rival PS.
