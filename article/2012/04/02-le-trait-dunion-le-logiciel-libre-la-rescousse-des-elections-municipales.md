---
site: Le Trait d'Union
title: "Le logiciel libre à la rescousse des élections municipales"
author: Christian Chaloux
date: 2012-04-02
href: http://www.letraitdunion.com/Municipal/2012-04-02/article-2945547/Le-logiciel-libre-a-la-rescousse-des-elections-municipales/1
tags:
- Entreprise
- Institutions
- Associations
- Marchés publics
- International
---

> Un logiciel libre d’accès sera lancé l’automne prochain afin de donner des outils aux candidats et partis politiques municipaux dans leur campagne électorale.
