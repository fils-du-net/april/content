---
site: LA TRIBUNE
title: "François Hollande dévoile (enfin) son programme numérique"
author: Sandrine Cassini
date: 2012-04-13
href: http://www.latribune.fr/technos-medias/internet/20120413trib000693458/francois-hollande-devoile-enfin-son-programme-numerique.html
tags:
- Entreprise
- Internet
- Administration
- Économie
- Institutions
- Éducation
- Innovation
- Standards
---

> Le candidat socialiste souhaite réorienter les investissements du Grand emprunt, lancer un plan numérique pour la formation et favoriser l'utilisation des "logiciels libre et des standards ouverts". Le Conseil national du numérique pourrait être maintenu, mais n'échapperait pas à une modification de sa gouvernance.
