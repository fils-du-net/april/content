---
site: linuxfr.org
title: "Présidentielle 2012 et Logiciel Libre: les positions des deux principaux candidats"
author: Stefane Fermigier
date: 2012-04-13
href: http://linuxfr.org/news/presidentielle-2012-et-logiciel-libre-les-positions-des-deux-principaux-candidats
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- Innovation
---

> Le CNLL, Conseil National du Logiciel Libre — qui regroupe plus de 200 entreprises du secteur en France — a envoyé aux principaux candidats (déclarés ou probables) en novembre dernier un questionnaire en 8 points afin d'expliciter leurs positions par rapport aux facteurs de développement, ou au contraire de frein au développement, du logiciel libre / open source. Après de nombreux mois d'attentes, nous avons reçu les réponses des deux candidats les mieux positionnés actuellement dans les sondages, à savoir messieurs Sarkozy et Hollande.
