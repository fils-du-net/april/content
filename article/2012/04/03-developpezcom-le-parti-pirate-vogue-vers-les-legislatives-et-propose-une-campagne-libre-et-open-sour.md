---
site: Developpez.com
title: "Le parti pirate vogue vers les législatives, et propose une campagne \"libre et open-source\", voterez-vous pour le parti en juin?"
author: Flaburgan
date: 2012-04-03
href: http://www.developpez.com/actu/42980/Le-parti-pirate-vogue-vers-les-legislatives-et-propose-une-campagne-libre-et-open-source-voterez-vous-pour-le-parti-en-juin/
tags:
- Institutions
---

> À un moment où tout le monde à les yeux rivés vers les élections présidentielles, les pirates regardent déjà plus loin vers l'horizon et scrutent les élections législatives de juin prochain. Leur objectif? Obtenir leur premier siège à l'Assemblée nationale.
