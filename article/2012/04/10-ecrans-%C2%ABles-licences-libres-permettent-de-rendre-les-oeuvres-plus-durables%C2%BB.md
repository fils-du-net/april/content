---
site: écrans
title: "«Les licences libres permettent de rendre les œuvres plus durables»"
author: Marie Lechner
date: 2012-04-10
href: http://www.ecrans.fr/Les-licences-libres-permettent-de,14440.html
tags:
- Internet
- Logiciels privateurs
- Licenses
- Contenus libres
---

> Coresponsable scientifique du colloque Digital Art Conservation qui s’est tenu les 24 et 25 novembre à l’Ecole supérieure des arts décoratifs à Strasbourg, dans le cadre du projet européen, Anne Laforet est auteure du livre Le Net art au musée, stratégies de conservation des œuvres en ligne (Editions Questions théoriques, L&gt;P).
