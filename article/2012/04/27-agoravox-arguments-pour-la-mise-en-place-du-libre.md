---
site: AgoraVox
title: "Arguments pour la mise en place du libre"
author: matthius
date: 2012-04-27
href: http://www.agoravox.fr/actualites/technologies/article/arguments-pour-la-mise-en-place-du-115635
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Institutions
- Innovation
- International
---

> Pour convaincre les décideurs il faut savoir que les élus redoutent tout changement de logiciel. Tout dépendra de l’expert au PS, ou de la compétence de l’élu au PC. La droite ne négocie que peu avec la population. Il est possible de s’exprimer lors d’ateliers participatifs internes ou publics en apportant des arguments convaincants et compréhensibles.
