---
site: Nouvelle République
title: "La grande fête des jeux vidéos et logiciels libres"
author: Sébastien Kerouanton
date: 2012-04-27
href: http://www.lanouvellerepublique.fr/Toute-zone/Loisirs/Sports-et-loisirs/n/Contenus/Articles/2012/04/27/La-grande-fete-des-jeux-videos-et-logiciels-libres
tags:
- Internet
- Logiciels privateurs
- Sensibilisation
- Associations
---

> Le CAR accueille ce week-end la 11 e LAN Party, grande fête du jeu vidéo. Les amateurs d’informatique sont aussi conviés à la découverte de logiciels libres.
