---
site: LeMagIT
title: "Open source: Hollande et Sarkozy convergent souvent mais s'opposent sur les brevets logiciels"
author: David Castaneira
date: 2012-04-17
href: http://www.lemagit.fr/article/sarkozy-brevets-open-source/10901/1/open-source-hollande-sarkozy-convergent-souvent-mais-opposent-sur-les-brevets-logiciels/
tags:
- Entreprise
- Administration
- Institutions
- Associations
- Brevets logiciels
- Innovation
- Europe
---

> Fort de sa foi dans la dimension économique incontournable du mouvement open source, le CNLL (Conseil national du logiciel libre) a soumis un questionnaire précis aux candidats. Pour tirer une analyse non moins précise du programme des deux plus en vue en matière de logiciel libre. Les approches sont consensuelles…. Sauf en matière de brevets logiciels, se qui inquiète la communauté.
