---
site: ITespresso.fr
title: "Elysée 2012: les mesures phares du programme IT de Nicolas Sarkozy"
author: Philippe Guerrier
date: 2012-04-18
href: http://www.itespresso.fr/elysee-2012-les-mesures-phares-du-programme-it-de-nicolas-sarkozy-52729.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Administration
- Économie
- HADOPI
- Institutions
- Brevets logiciels
- DRM
- Éducation
- RGI
- Video
- Europe
- International
- Open Data
---

> ITespresso.fr a compilé les principaux éléments du programme numérique du candidat Nicolas Sarkozy: economie numérique (projet Start-Up France), éducation et formation, e-administration.
