---
site: PC INpact
title: "Logiciels libres: le CNLL analyse les positions de Sarkozy et Hollande"
author: Xavier Berne
date: 2012-04-16
href: http://www.pcinpact.com/news/70240-logiciel-libre-cnll-hollande-sarkozy.htm
tags:
- Entreprise
- Internet
- Économie
- April
- HADOPI
- Institutions
- Associations
- Brevets logiciels
- DADVSI
- Éducation
- Innovation
- Neutralité du Net
---

> Après avoir sollicité en novembre 2011 l’avis de différents partis politiques s’agissant de l’économie du logiciel libre, le Conseil national du logiciel libre (CNLL) vient de publier les positionnements du président-candidat Nicolas Sarkozy et de son opposant socialiste, François Hollande. Le collectif, regroupant associations et clusters d’entreprises du logiciel libre, constate notamment l’opposition des deux candidats au sujet de la brevetabilité des logiciels, regrettant que le candidat de l’UMP y soit favorable.
