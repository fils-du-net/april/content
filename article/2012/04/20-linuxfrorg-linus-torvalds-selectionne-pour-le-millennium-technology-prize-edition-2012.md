---
site: linuxfr.org
title: "Linus Torvalds sélectionné pour le Millennium Technology Prize édition 2012"
author: mitkl
date: 2012-04-20
href: http://linuxfr.org/news/linus-torvalds-selectionne-pour-le-millennium-technology-prize-edition-2012
tags:
- Internet
- Institutions
- Innovation
- International
---

> Le Millennium Technology Prize est un prix technologique décerné tous les deux ans par le Technology Academy Finland. Pour cette édition 2012, le comité a retenu deux finalistes:
