---
site: L'INFORMATICIEN
title: "Linus Torvalds a reçu le prix \"Millennium Technology\""
author: Orianne Vatin
date: 2012-04-20
href: http://www.linformaticien.com/actualites/id/24573/linus-torvalds-a-recu-le-prix-millennium-technology.aspx
tags:
- Internet
- Institutions
- Innovation
- International
---

> Cette distinction, peu médiatisée auprès du grand public, est pourtant reconnue internationalement dans le milieu des nouvelles technologies. Elle est décernée tous les deux ans par la Technology Academy of Finland.
