---
site: webdo
title: "Conférence nationale sur les logiciels libres 2012: bilan mitigé pour les communautés!"
author: Melek Jebnoun
date: 2012-04-02
href: http://www.webdo.tn/2012/04/02/conference-nationale-sur-les-logiciels-libres-2012-bilan-mitige-pour-les-communautes/
tags:
- Entreprise
- Administration
- Institutions
- Sensibilisation
- Associations
- International
---

> Le débat sur les logiciels libres et les outils open source a toujours fait rage en Tunisie. Comparé aux autres pays nord-africains et arabes, le nôtre dispose de l’une des communautés de logiciels libres et open source la plus grande.
