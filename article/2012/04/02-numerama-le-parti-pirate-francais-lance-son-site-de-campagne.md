---
site: Numerama
title: "Le Parti Pirate français lance son site de campagne"
author: Guillaume Champeau
date: 2012-04-02
href: http://www.numerama.com/magazine/22216-le-parti-pirate-francais-lance-son-site-de-campagne.html
tags:
- Internet
- Institutions
---

> Le Parti Pirate a lancé lundi son site de campagne pour les législatives de juin 2012, avec l'espoir de fédérer de nouveaux soutiens et des candidats pour les élections qui détermineront la composition de l'Assemblée Nationale.
