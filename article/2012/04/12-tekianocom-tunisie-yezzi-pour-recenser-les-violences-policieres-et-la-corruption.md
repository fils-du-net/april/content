---
site: tekiano.com
title: "Tunisie: Yezzi, pour recenser les violences policières et la corruption"
date: 2012-04-12
href: http://www.tekiano.com/net/web-20/5098-tunisie-yezzi-pour-recenser-les-violences-policieres-et-la-corruption-.html
tags:
- Internet
- Administration
- Associations
- Innovation
- International
- Open Data
---

> Yezzi, une plateforme collaborative destinée à collecter les témoignages sur les violences policières et la corruption, développée par l'Association Tunisienne des Libertés Numériques a remporté le premier prix dans la catégorie OpenGov au concours du Logiciel Libre, tenu fin mars à Tunis.
