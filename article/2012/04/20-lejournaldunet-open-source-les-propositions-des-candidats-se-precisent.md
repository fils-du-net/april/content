---
site: LeJournalduNet
title: "Open Source: les propositions des candidats se précisent"
author: Virgile Juhan
date: 2012-04-20
href: http://www.journaldunet.com/solutions/dsi/open-source-dans-la-presidentielle.shtml
tags:
- Entreprise
- Internet
- Administration
- Interopérabilité
- April
- Institutions
- Sensibilisation
- Brevets logiciels
- DRM
- Droit d'auteur
- Innovation
- Promotion
- RGI
- Sciences
- Standards
---

> Jean-Luc Mélenchon, Eva Joly, Nicolas Dupont-Aignan, François Bayrou, Nicolas Sarkozy et François Hollande ont répondu aux questions de l'April sur le logiciel libre, les brevets logiciels ou l'interopérabilité.
