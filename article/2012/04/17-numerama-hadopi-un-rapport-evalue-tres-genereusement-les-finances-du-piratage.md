---
site: Numerama
title: "Hadopi: un rapport évalue très généreusement les finances du piratage"
author: Guillaume Champeau
date: 2012-04-17
href: http://www.numerama.com/magazine/22334-hadopi-un-rapport-evalue-tres-genereusement-les-finances-du-piratage.html
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Désinformation
- Droit d'auteur
---

> Un rapport commandé par l'Hadopi pour évaluer les modèles économiques du piratage conclut que les plus gros sites de streaming et de téléchargement direct en France gagneraient jusqu'à 36 millions d'euros par an, pour un marché global du piratage (hors P2P) évalué entre 51 et 72,5 millions d'euros. Des chiffres qui paraissent largement surévalués lorsqu'on les compare à ce qu'a gagné MegaUpload, l'ancien leader du marché.
