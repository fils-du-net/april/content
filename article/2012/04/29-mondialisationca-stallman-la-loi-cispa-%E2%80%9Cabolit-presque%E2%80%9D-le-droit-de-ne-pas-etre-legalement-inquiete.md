---
site: Mondialisation.ca
title: "Stallman: La loi CISPA “abolit presque” le droit de ne pas être légalement inquiété de manière irraisonnable"
author: RT
date: 2012-04-29
href: http://mondialisation.ca/index.php?context=va&aid=30597
tags:
- Entreprise
- Internet
- Institutions
- International
---

> Le projet de loi très controversé sur la sécurité de l’internet CISPA (Counter Intelligence Sharing and Protection Act) n’est plus qu’à deux pas d’être promulgué en loi. L’activiste de la liberté des logiciels Richard Stallman dit que les utilisateurs d’internet doivent être avertis et faire très attention, dans la mesure où le gouvernement est une bien plus grande menace que n’importe lequel des hackers individuels.
