---
site: Numerama
title: "Twitter invente un pacte de non-agression sur les brevets"
author: Guillaume Champeau
date: 2012-04-18
href: http://www.numerama.com/magazine/22350-twitter-invente-un-pacte-de-non-agression-sur-les-brevets.html
tags:
- Entreprise
- Brevets logiciels
- Innovation
---

> Twitter va proposer aux employés qui créent des inventions brevetables pour son compte un accord qui interdit à la société d'exploiter l'invention pour attaquer d'autres entreprises et innovateurs. Leurs brevets ne pourront être utilisés que pour se défendre contre les attaques d'un tiers. Twitter espère que d'autres entreprises adopteront le même accord, pour créer un espace de non-agression réciproque.
