---
site: indexel.net
title: "Comprendre l'Open Data"
author: Alain Bastide
date: 2012-04-20
href: http://www.indexel.net/infrastructure/comprendre-l-open-data-3580.html
tags:
- Entreprise
- Administration
- Licenses
- Europe
- Open Data
---

> Même si elle n’est pas réservée aux seules collectivités, l’ouverture des données publiques est aujourd’hui l’apanage des états et des villes. Mais certaines entreprises commencent à suivre le mouvement.
