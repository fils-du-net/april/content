---
site: ITespresso.fr
title: "Brevets: Eric Schmidt témoigne dans le procès Google contre Oracle"
author: Jacques Franc de Ferrière
date: 2012-04-25
href: http://www.itespresso.fr/brevets-eric-schmidt-temoigne-dans-le-proces-google-contre-oracle-52869.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Droit d'auteur
---

> Lors du procès qui se déroule actuellement entre Google et Oracle sur les brevets Java utilisés sans autorisation dans Android, Eric Schmidt est passé dans le box des témoins.
