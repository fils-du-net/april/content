---
site: LE CERCLE Les Echos
title: "L’Allemagne, les Pirates et le Monde de demain"
author: Philippe Payet
date: 2012-04-18
href: http://lecercle.lesechos.fr/economie-societe/international/europe/221145862/allemagne-pirates-et-monde-demain
tags:
- Internet
- Institutions
- Droit d'auteur
- International
---

> Le débarquement du Parti pirate dans le paysage politique outre-Rhin est en passe de provoquer un bouleversement significatif. Chance ou menace?
