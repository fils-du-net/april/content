---
site: PC INpact
title: "Brevets: Microsoft déménage stratégiquement aux Pays-Bas"
author: Xavier Berne
date: 2012-04-11
href: http://www.pcinpact.com/news/70152-microsoft-brevets-duren-motorola-allemagne.htm
tags:
- Entreprise
- April
- Institutions
- Brevets logiciels
- Europe
---

> Comme le révèle le New York Times, Microsoft vient de décider de «déplacer son centre européen de logistique et de distribution aux Pays-Bas», alors qu’il était jusque-là en Allemagne. Le quotidien américain rappelle que la société de Bill Gates est poursuivie devant les tribunaux allemands, pour plusieurs litiges ayant trait aux brevets. Microsoft souhaiterait ainsi éviter à l’avenir les injonctions que peut prononcer la justice d’Outre-Rhin, considérées comme très dures dans ce type d’affaires.
