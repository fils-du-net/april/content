---
site: ITCHANNEL.info
title: "Présidentielle 2012 et Logiciel Libre: les positions des deux principaux candidats"
date: 2012-04-12
href: http://www.itchannel.info/articles/131268/presidentielle-2012-logiciel-libre-positions-deux-principaux-candidats.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Associations
- Brevets logiciels
- Éducation
- Marchés publics
---

> Le CNLL a envoyé aux principaux candidats en novembre dernier un questionnaire en 8 points afin d'expliciter leurs positions par rapport aux facteurs de développement, ou au contraire de frein au développement, du logiciel libre / open source. A la veille de l'élection, les réponses des deux principaux candidats, Nicolas Sarkozy et François Hollande, sont arrivées. Les points sur lesquels les deux candidats sont d'accord existent: la commande publique, les standards ouverts, la neutralité du net ou encore le soutien aux PME. Mais les points de divergence aussi.
