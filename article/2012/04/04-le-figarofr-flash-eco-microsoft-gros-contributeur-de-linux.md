---
site: LE FIGARO.fr
title: "Flash Eco: Microsoft, gros contributeur de Linux"
author: Benjamin Ferran
date: 2012-04-04
href: http://www.lefigaro.fr/flash-eco/2012/04/04/97002-20120404FILWWW00535-microsoft-gros-contributeur-de-linux.php
tags:
- Entreprise
- Économie
- Licenses
---

> Microsoft a fait pour la première fois son apparition dans le classement des vingt plus gros contributeurs au noyau de Linux, selon le rapport annuel de la Linux Foundation. Les apports de Microsoft ont représenté 1% des contributions entre les mois d’octobre 2010 et janvier 2012. Les trois plus importants contributeurs sont Red Hat (10,7%), Intel (7,2%) et Novell (7,2%).
