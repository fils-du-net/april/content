---
site: LeMonde.fr
title: "Le Parti pirate français rêve d'un destin à l'allemande"
author: Anna Benjamin
date: 2012-04-13
href: http://www.lemonde.fr/technologies/article/2012/04/13/le-parti-pirate-francais-reve-d-un-destin-a-l-allemande_1685369_651865.html
tags:
- Internet
- HADOPI
- Institutions
- Droit d'auteur
- International
- ACTA
---

> A Berlin, le Parti pirate est entré fin 2011 au Parlement régional avec 8,9 % des voix. Son homologue français tente, lui, de se mettre en ordre de bataille pour les législatives de 2012.
