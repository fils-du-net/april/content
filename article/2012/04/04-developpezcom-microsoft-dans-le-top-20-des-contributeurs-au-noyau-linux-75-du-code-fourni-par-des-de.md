---
site: Developpez.com
title: "Microsoft dans le top 20 des contributeurs au noyau Linux, 75% du code fourni par des développeurs rémunérés"
author: Hinault Romaric
date: 2012-04-04
href: http://www.developpez.com/actu/43031/Microsoft-dans-le-top-20-des-contributeurs-au-noyau-Linux-75-du-code-fourni-par-des-developpeurs-remuneres/
tags:
- Entreprise
---

> La publication de la liste des contributeurs au noyau Linux vient confirmer une fois de plus que Microsoft voit en l’open source une opportunité plus qu’une menace.
