---
site: LeMagIT
title: "Microsoft structure ses relations avec l’Open Source dans une division"
author: Cyrille Chausson
date: 2012-04-13
href: http://www.lemagit.fr/article/microsoft-open-source/10877/1/microsoft-structure-ses-relations-avec-open-source-dans-une-division/
tags:
- Entreprise
- Internet
- Interopérabilité
- Institutions
- Associations
- Standards
- Europe
---

> Afin de structurer son implication continue dans les communautés et les organisations Open Source et consolider ses liens avec les éditeurs, Microsoft a décidé de créer une division dédiée. Microsoft Open Technologies sera ainsi le référent de Microsoft pour les questions d’interopérabilité et devrait cimenter les relations du groupe avec le monde de l’Open Source.
