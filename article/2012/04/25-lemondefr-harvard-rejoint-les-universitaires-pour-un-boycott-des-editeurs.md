---
site: LeMonde.fr
title: "Harvard rejoint les universitaires pour un boycott des éditeurs"
author: Anna Benjamin
date: 2012-04-25
href: http://www.lemonde.fr/sciences/article/2012/04/25/harvard-rejoint-les-universitaires-pour-un-boycott-des-editeurs_1691125_1650684.html
tags:
- Partage du savoir
- Institutions
- Droit d'auteur
- Éducation
- International
- Open Data
---

> Plus de 10 000 universitaires du monde entier ont déjà signé la pétition, "Le coût du savoir". [...] " On est tous confrontés au même paradoxe. Nous faisons les recherches, écrivons les articles, œuvrons au référencement des articles par d'autres chercheurs, le tout gratuitement... Et ensuite nous rachetons le résultat de notre travail à des prix scandaleux."
