---
site: PC INpact
title: "L'avenir de la prohibition de la vente liée PC et OS en cassation"
author: Marc Rees
date: 2012-04-14
href: http://www.pcinpact.com/news/70224-cour-cassation-vente-liee-hp.htm
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
---

> Dans un arrêt de la Cour d’appel de Versailles du 5 mai 2011, l’UFC Que Choisir remportait une victoire importante en matière de vente liée. L’association obligeait en effet HP à faire afficher sur son site le prix des logiciels préinstallés et à mettre en place un système de réduction de prix en cas de renonciation de l’achat des logiciels. HP a cependant porté l’affaire devant la Cour de cassation qui pourrait revenir sur la décision des juges versaillais...
