---
site: Silicon.fr
title: "Éva Joly veut donner au logiciel libre «une place plus grande dans l’éducation»"
author: Ariane Beky
date: 2012-04-19
href: http://www.silicon.fr/eva-joly-veut-donner-au-logiciel-libre-une-place-plus-grande-dans-leducation-73832.html
tags:
- Entreprise
- Internet
- Administration
- Économie
- Interopérabilité
- April
- HADOPI
- Institutions
- Associations
- Brevets logiciels
- DADVSI
- DRM
- Éducation
- Innovation
- Marchés publics
- Neutralité du Net
- RGI
- Standards
- ACTA
---

> Candidate d’Europe Écologie Les Verts à l’élection présidentielle 2012, Éva Joly a répondu aux questions du Conseil national du logiciel libre (CNLL) et de l’Association de promotion et de défense du logiciel libre (APRIL).
