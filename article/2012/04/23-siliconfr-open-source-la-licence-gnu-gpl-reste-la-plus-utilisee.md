---
site: Silicon.fr
title: "Open source: la licence GNU GPL reste la plus utilisée"
author: David Feugey
date: 2012-04-23
href: http://www.silicon.fr/open-source-la-licence-gnu-gpl-reste-la-plus-utilisee-73931.html
tags:
- Associations
- Licenses
---

> La GNU GPL 2.0 est la plus commune des licences open source. Elle est toutefois en perte de vitesse… malgré sa croissance en nombre de projets couverts.
