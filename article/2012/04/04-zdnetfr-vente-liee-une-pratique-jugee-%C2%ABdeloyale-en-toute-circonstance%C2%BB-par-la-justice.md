---
site: ZDNet.fr
title: "Vente liée: une pratique jugée «déloyale en toute circonstance» par la justice"
date: 2012-04-04
href: http://www.zdnet.fr/actualites/vente-liee-une-pratique-jugee-deloyale-en-toute-circonstance-par-la-justice-39770380.htm
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
- Europe
---

> Samsung a été condamné par la justice pour vente liée et refus d’indemniser équitablement le consommateur qui avait refusé les licences des logiciels préinstallés. Pour l’Aful, ce jugement est «historique» car considérant explicitement la vente liée comme une pratique «déloyale».
