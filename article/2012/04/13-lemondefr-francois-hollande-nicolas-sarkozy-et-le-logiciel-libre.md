---
site: LeMonde.fr
title: "François Hollande, Nicolas Sarkozy et le logiciel libre"
date: 2012-04-13
href: http://www.lemonde.fr/technologies/article/2012/04/13/francois-hollande-nicolas-sarkozy-et-le-logiciel-libre_1685419_651865.html
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Associations
- Brevets logiciels
- Éducation
- Innovation
- Neutralité du Net
---

> Les candidats du PS et de l'UMP ont répondu à un questionnaire du Conseil national du logiciel libre, une association professionnelle d'entreprises travaillant dans le secteur du logiciel libre. Les réponses des deux candidats dévoilent deux conceptions politiques différentes de la propriété intellectuelle en matière de logiciel.
