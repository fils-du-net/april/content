---
site: Génération nouvelles technologies
title: "Hadopi, numérique, logiciel libre...: les propositions de F. Hollande"
author: Jérôme G.
date: 2012-04-22
href: http://www.generation-nt.com/numerique-presidentielle-francois-hollande-actualite-1569331.html
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Brevets logiciels
- Droit d'auteur
- Éducation
---

> Quelques extraits des déclarations du candidat François Hollande à l'élection présidentielle de 2012.
