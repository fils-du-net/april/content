---
site: ITespresso.fr
title: "Brevets: Google et Oracle vont régler leurs différents au tribunal"
author: Jacques Franc de Ferrière
date: 2012-04-18
href: http://www.itespresso.fr/brevets-google-et-oracle-vont-regler-leurs-differents-au-tribunal-52718.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Droit d'auteur
- International
---

> L’usage des technologies Java dans Andoid est-il illégal? Et s’il l’est, quelle est la juste rétribution que devra payer Google à Oracle? La justice américaine va devoir trancher.
