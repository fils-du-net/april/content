---
site: Marianne 2
title: "Inouï: Marine Le Pen défend le «droit d’asile» ... numérique"
author: Tefy Andriamanana
date: 2012-04-07
href: http://www.marianne2.fr/Inoui-Marine-Le-Pen-defend-le-droit-d-asile-numerique_a216855.html?com
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- ACTA
---

> Si elle défend la fermeté en matière de sécurité et d’immigration, Marine Le Pen développe aussi des positions très libertaires concernant Internet, n’hésitant pas à soutenir les Anonymous qui s’en étaient pris... à Bruno Gollnisch.
