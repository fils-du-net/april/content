---
site: clubic.com
title: "Microsoft ouvre une filiale dédiée à l'open source"
author: Guillaume Belfiore
date: 2012-04-13
href: http://pro.clubic.com/entreprises/microsoft/actualite-486518-microsoft-ouvre-filiale-dediee-open-source.html
tags:
- Entreprise
- Logiciels privateurs
- Interopérabilité
---

> Microsoft annonce vouloir renforcer ses efforts auprès de la communauté open source en lançant une nouvelle filiale dédiée.
