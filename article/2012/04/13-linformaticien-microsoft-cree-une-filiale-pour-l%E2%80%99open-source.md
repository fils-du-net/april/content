---
site: L'INFORMATICIEN
title: "Microsoft crée une filiale pour l’Open Source"
author: Stéphane Larcher
date: 2012-04-13
href: http://www.linformaticien.com/actualites/id/24457/microsoft-cree-une-filiale-pour-l-open-source.aspx
tags:
- Entreprise
- Interopérabilité
- Standards
---

> Microsoft Open Technologies voit le jour aujourd’hui. Cette filiale du géant de Redmond, va se consacrer aux différents projets Open Source et aux standards d’interopérabilité. Cocorico, cette filiale – une première - est dirigée par le Français Jean Paoli, co-inventeur du langage XML.
