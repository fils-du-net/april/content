---
site: ITespresso.fr
title: "La vente liée «PC - OS Windows» prend des coups en justice"
author: Jacques Franc de Ferrière
date: 2012-04-05
href: http://www.itespresso.fr/la-vente-liee-pc-os-windows-prend-des-coups-en-justice-52397.html
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Vente liée
- Associations
---

> Samsung France a été condamné pour vente forcée de Windows pré-installé sur un PC. Une décision par un juge de proximité qui ravit l’AFUL (promotion des logiciels libres, utilisation de standards ouverts).
