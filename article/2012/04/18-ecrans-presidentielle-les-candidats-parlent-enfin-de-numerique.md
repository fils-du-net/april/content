---
site: écrans
title: "Présidentielle: les candidats parlent (enfin) de numérique"
author: Emilie Massemin
date: 2012-04-18
href: http://www.ecrans.fr/Presidentielle-les-candidats,14503.html
tags:
- Entreprise
- Internet
- Administration
- Économie
- April
- HADOPI
- Institutions
- Associations
- Droit d'auteur
- Éducation
- Innovation
- Marchés publics
- Standards
---

> En mars dernier, 21 associations et syndicats des professionnels du numérique, parmi lesquelles la Fédération française des télécoms, le Syndicat national des jeux vidéo, la Fevad et le Syntec numérique, adressaient une lettre aux candidats leur demandant de préciser leur programme pour le secteur du numérique.
