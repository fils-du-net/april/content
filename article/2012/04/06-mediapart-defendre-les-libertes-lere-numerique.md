---
site: Mediapart
title: "Défendre les libertés à l'ère numérique"
author: Lionel Allorge
date: 2012-04-06
href: http://blogs.mediapart.fr/edition/2012-ce-que-propose-la-societe-civile/article/060412/defendre-les-libertes-lere-numeriq
tags:
- Internet
- Administration
- Économie
- Interopérabilité
- April
- Institutions
- Vente liée
- Accessibilité
- Brevets logiciels
- DRM
- Éducation
- Promotion
- International
- ACTA
---

> «Pour répondre à l'attente de nombreux citoyens préoccupés par les questions de liberté à l'ère du numérique», Lionel Allorge, président de l'Association nationale pour la promotion et la défense du logiciel libre (April), demande aux candidats à la présidentielle de répondre au questionnaire qui leur a été envoyé débuts mars.
