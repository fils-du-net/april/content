---
site: PC INpact
title: "Joly, Mélenchon, Dupont-Aignan et Bayrou répondent à Candidats.fr"
author: Marc Rees
date: 2012-04-16
href: http://www.pcinpact.com/news/70253-candidatsfr-april-presidentielle-2012-repons.htm
tags:
- Entreprise
- Internet
- Administration
- Économie
- Interopérabilité
- April
- HADOPI
- Institutions
- Vente liée
- Brevets logiciels
- DADVSI
- DRM
- Droit d'auteur
- Éducation
- Neutralité du Net
- Sciences
- ACTA
---

> Candidats.fr avait été lancé en février 2007 en plein salon Solution Linux. L’initiative lancée par l’April a pour objectif de connaître et révéler les positions des candidats à la présidentielle autour des problématiques des droits et libertés dans les nouvelles technologies en général et du logiciel libre en particulier. Brevetabilité, mesures techniques, loi DADVSI, interopérabilité, vente liée ordinateur et OS, «informatique de confiance», etc.
