---
site: linuxfr.org
title: "Réponses des candidats à la présidentielle française sur le numérique, le libre et internet"
author: Benoît Sibaud
date: 2012-04-19
href: http://linuxfr.org/news/reponses-des-candidats-a-la-presidentielle-francaise-sur-le-numerique-le-libre-et-internet
tags:
- Internet
- April
- HADOPI
- Institutions
- Associations
- Brevets logiciels
- Neutralité du Net
---

> Avec mes excuses présentées d'avance à ceux qui satureraient sur le sujet à quelques jours du premier tour, voici une tentative de synthèse des positions exprimées par les candidats à la présidentielle française sur le numérique, le libre et la neutralité du net:
