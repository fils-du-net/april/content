---
site: Silicon.fr
title: "Vendre Windows avec un PC est une pratique «déloyale en toute circonstance»"
author: David Feugey
date: 2012-04-06
href: http://www.silicon.fr/vendre-windows-avec-un-pc-est-une-pratique-deloyale-en-toute-circonstance-73538.html
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
- Europe
---

> Le Juge de proximité de Saint-Denis considère que la vente liée PC plus OS est inacceptable… quelles que soient les raisons invoquées. Une victoire pour les opposants au «racketiciel».
