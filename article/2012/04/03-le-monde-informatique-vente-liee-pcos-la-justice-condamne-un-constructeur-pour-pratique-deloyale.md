---
site: Le Monde Informatique
title: "Vente liée PC/OS, la justice condamne un constructeur pour pratique déloyale"
author: Relaxnews
date: 2012-04-03
href: http://www.lemondeinformatique.fr/actualites/lire-ventes-liees-pc-systeme-d-exploitation-une-pratique-deloyale-48432.html
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
---

> Encore une décision de justice qui condamne la vente liée entre PC et OS sous le motif de pratique déloyale.
