---
site: ML actu
title: "Comment les élections auraient pu régler une partie de la dette d'Arles."
author: Paul Ferrier
date: 2012-04-25
href: http://www.mlactu.fr/article/comment-les-%C3%A9lections-auraient-pu-r%C3%A9gler-une-partie-de-la-dette-darles/687
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Licenses
- Marchés publics
- International
---

> Quand on pense aux milliards qu’ont rapportés certains logiciels, aux fortunes qu’ont amassées des Bill Gates ou des Mark Zuckerberg, on est en droit de se poser la question. Mais pourquoi ne le vendent-ils pas. La ville d’Arles et son service de développement informatique ont créé, il y a quelques années, un logiciel qui permet de gérer de A à Z les élections. Aujourd’hui il est utilisé par plus de 700 communes en France… et même à l’étranger.
