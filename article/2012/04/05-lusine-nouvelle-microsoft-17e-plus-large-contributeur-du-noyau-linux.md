---
site: L'USINE NOUVELLE
title: "Microsoft: 17e plus large contributeur du noyau Linux"
author: Christophe Guillemin
date: 2012-04-05
href: http://www.usinenouvelle.com/article/microsoft-17e-plus-large-contributeur-du-noyau-linux.N172310
tags:
- Entreprise
- April
- Associations
- Brevets logiciels
---

> L'éditeur de Windows fait son entrée dans le classement des plus gros contributeurs de Linux. Contribuer au code du noyau Linux permet à son outil de virtualisation Hyper-V d'être compatible avec l'OS libre. Or, cette compatibilité est désormais incontournable sur le marché de la virtualisation et du Cloud Computing.
