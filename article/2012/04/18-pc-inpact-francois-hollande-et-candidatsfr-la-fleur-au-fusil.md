---
site: PC INpact
title: "François Hollande et candidats.fr: la Fleur au fusil"
author: Marc Rees
date: 2012-04-18
href: http://www.pcinpact.com/news/70314-fleur-pellerin-francois-hollande-candidatsfr.htm
tags:
- Internet
- Administration
- Économie
- Interopérabilité
- April
- HADOPI
- Institutions
- Vente liée
- Brevets logiciels
- DADVSI
- DRM
- Éducation
- Sciences
- Europe
- ACTA
---

> Candidats.fr est né d’une initiative de l’APRIL visant à interroger les présidentiables et révéler leurs positions sur quelques sujets centraux du numérique, les DRM, ACTA, IPRED, Hadopi, la loi DADVSI et le fameux amendement Vivendi, la brevetabilité du logiciel, ou encore la vente liée ou l’éducation. Le questionnaire a aussi pour vocation de contractualiser les engagements des candidats, spécialement celui qui montera sur la première marche.
