---
site: webdo
title: "Richard Stallman: «Free software, free countries»"
author: Neil
date: 2012-04-22
href: http://www.webdo.tn/2012/04/22/richard-stallman-free-software-free-countries/
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Licenses
- Philosophie GNU
- International
---

> Richard Stallman, figure emblématique de la lutte pour les «Free Software» dont il est le président et fondateur, sera de retour en Tunisie, pour la deuxième fois, et ce, pour animer deux conférences: "le 29 avril à l'INSAT à Tunis et le 2 mai au Palais des sciences à Monastir". La première fois qu'il est venu à Tunis, c’était lors de la première phase du Sommet mondial sur la société de l'information (SMSI), en novembre 2005.
