---
site: webdo
title: "BarCamp INSAT: le show de Richard Stallman"
author: Emir Sfaxi
date: 2012-04-29
href: http://www.webdo.tn/2012/04/29/barcamp-insat-le-show-de-richard-stallman/
tags:
- Logiciels privateurs
- Philosophie GNU
- Promotion
- International
---

> Richard Stallman, connu comme étant l’initiateur du logiciel libre et l’inventeur de GNU, a tenu une conférence dimanche 29 avril à l’INSAT. Il est aussi sujet de soutenir la cause de l’OpenGov en Tunisie et pour parler de son expérience et de sa philosophie, sous les yeux admiratifs des présents.
