---
site: PC INpact
title: "Microsoft Open Technologies, une filiale focalisée sur l'open source"
author: Vincent Hermann
date: 2012-04-13
href: http://www.pcinpact.com/news/70220-microsoft-open-technologies-filiale-open-source.htm
tags:
- Entreprise
- Internet
- Interopérabilité
- Standards
---

> Microsoft et open source n’ont jamais réellement rimé, mais la firme est décidée à changer cette image. Microsoft s’est lancée dans de nombreuses initiatives open source ces dernières années et est passée à l’étape suivante: une filiale entièrement dévolue aux technologies ouvertes.
