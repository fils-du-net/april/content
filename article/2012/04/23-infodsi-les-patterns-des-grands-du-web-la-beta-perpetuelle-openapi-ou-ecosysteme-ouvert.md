---
site: infoDSI
title: "Les Patterns des Grands du Web La beta perpétuelle - OpenAPI ou écosystème ouvert"
author: Guillaume Plouin
date: 2012-04-23
href: http://www.infodsi.com/articles/131693/patterns-grands-web-beta-perpetuelle-openapi-ecosysteme-ouvert.html
tags:
- Entreprise
- Internet
- Partage du savoir
---

> Avant d’introduire la bêta perpétuelle, il est nécessaire de revenir sur un pattern classique du monde Open Source: “Release early, release often”. Le principe de ce pattern consiste à mettre régulièrement le code à la disposition de la communauté afin de permettre aux développeurs, testeurs, utilisateurs de donner un feedback en continu sur le produit. Cette pratique a été décrite dans “La cathédrale et le bazar” d’Eric Steven Raymond. Elle rejoint le principe d’itération courte des méthodes agiles.
