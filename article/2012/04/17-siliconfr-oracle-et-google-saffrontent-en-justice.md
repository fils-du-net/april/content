---
site: Silicon.fr
title: "Oracle et Google s'affrontent en justice"
author: Ariane Beky
date: 2012-04-17
href: http://www.silicon.fr/oracle-et-google-saffrontent-en-justice-73797.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Droit d'auteur
- International
---

> Après vingt mois de procédures et tentatives de règlement amiable concernant l’utilisation des technologies Java dans l’OS mobile Android, Oracle, géant du logiciel d’entreprise et des bases de données, s’oppose à Google lors d’un procès devant une cour de San Francisco.
