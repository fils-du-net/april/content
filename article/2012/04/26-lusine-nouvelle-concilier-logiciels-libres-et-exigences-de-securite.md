---
site: L'USINE NOUVELLE
title: "Concilier logiciels libres et exigences de sécurité"
author: LAURENT ARCHAMBAULT
date: 2012-04-26
href: http://www.usinenouvelle.com/article/concilier-logiciels-libres-et-exigences-de-securite.N173559
tags:
- Entreprise
- Économie
- Standards
---

> Si l'utilisation de l'open source se répand, elle est encore peu encadrée au niveau des systèmes embarqués critiques dans l'aérien.
