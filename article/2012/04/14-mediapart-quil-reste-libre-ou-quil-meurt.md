---
site: Mediapart
title: "Qu'il reste libre où qu'il meurt"
author: le torchon
date: 2012-04-14
href: http://blogs.mediapart.fr/blog/le-torchon/140412/quil-reste-libre-ou-quil-meurt
tags:
- Entreprise
- Internet
- Partage du savoir
- HADOPI
- Institutions
- Droit d'auteur
- Innovation
- Licenses
- Europe
- ACTA
---

> C'était un matin comme les autres pour Jordi. Il se levait toujours 5 minutes avant d'aller en cours et plutôt que de déjeuner, il se connectait directement à Facebook. Ce qu'il vit le fit cligner des paupières... Il crut ne pas être réveillé mais après avoir vérifié l'info sur infosvérifiés.org, il dut se rendre à l'évidence: MegaUpload avait fermé. MegaUpload; ce grand pote qu'il voyait tous les soirs avait fermé.
