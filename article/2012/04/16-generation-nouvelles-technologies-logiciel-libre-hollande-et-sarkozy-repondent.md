---
site: Génération nouvelles technologies
title: "Logiciel Libre: Hollande et Sarkozy répondent"
author: Jérôme G.
date: 2012-04-16
href: http://www.generation-nt.com/logiciel-libre-hollande-sarkozy-brevets-actualite-1568051.html
tags:
- Entreprise
- Économie
- Institutions
- Associations
- Brevets logiciels
- Droit d'auteur
- Innovation
- Neutralité du Net
- Standards
---

> À la demande du Conseil National du Logiciel Libre, les équipes de campagne de François Hollande et Nicolas Sarkozy ont précisé leurs positions sur l'économie du Logiciel Libre.
