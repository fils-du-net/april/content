---
site: La Voix du Nord
title: "Le Meuh Lab, ou comment la technique et le partage créent l'art et parfois l'utile"
author: MARC GROSCLAUDE
date: 2012-04-30
href: http://www.lavoixdunord.fr/Locales/Roubaix/actualite/Secteur_Roubaix/2012/04/30/article_le-meuh-lab-ou-comment-la-technique-et-l.shtml
tags:
- Administration
- Matériel libre
- Associations
---

> Meuh: «Machines électroniques à usage humaniste.» L'expression interpelle, décontenance au premier abord. Pour en percer la signification, il faut imaginer la relation que peuvent entretenir l'art et le goût de la «bidouille». Ce n'est pas plus clair? Décryptage avec Thierry Mbaye, qui à Roubaix avec le Meuh Lab marie technologie et création avec la liberté pour témoin.
