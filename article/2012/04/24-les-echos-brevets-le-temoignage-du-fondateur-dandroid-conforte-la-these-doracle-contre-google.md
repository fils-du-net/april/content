---
site: Les Echos
title: "Brevets: le témoignage du fondateur d'Android conforte la thèse d'Oracle contre Google"
author: Florian Debes
date: 2012-04-24
href: http://www.lesechos.fr/entreprises-secteurs/tech-medias/actu/0202028350226-brevets-le-temoignage-du-fondateur-d-android-conforte-la-these-d-oracle-contre-google-316079.php
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Droit d'auteur
- Licenses
---

> Andy Rubin, le fondateur d'Android, reconnaît que Google aurait dû s'entendre avec Sun pour utiliser une partie du code informatique Java afin de développer son système d'exploitation pour smartphone.
