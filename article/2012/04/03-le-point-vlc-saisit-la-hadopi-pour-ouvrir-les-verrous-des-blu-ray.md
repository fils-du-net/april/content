---
site: Le Point
title: "VLC saisit la Hadopi pour ouvrir les verrous des Blu-ray"
author: Guerric Poncet
date: 2012-04-03
href: http://www.lepoint.fr/chroniqueurs-du-point/guerric-poncet/exclusif-vlc-saisit-la-hadopi-pour-ouvrir-les-verrous-des-blu-ray-03-04-2012-1448011_506.php
tags:
- Entreprise
- Internet
- Interopérabilité
- HADOPI
- DRM
- Droit d'auteur
---

> Le très populaire lecteur multimédia français veut permettre à ses utilisateurs de lire les disques protégés.
