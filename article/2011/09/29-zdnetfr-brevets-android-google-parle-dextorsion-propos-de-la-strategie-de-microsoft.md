---
site: ZDNet.fr
title: "Brevets Android : Google parle d'extorsion à propos de la stratégie de Microsoft"
date: 2011-09-29
href: http://www.zdnet.fr/actualites/brevets-android-google-parle-d-extorsion-a-propos-de-la-strategie-de-microsoft-39764383.htm
tags:
- Entreprise
- Brevets logiciels
- Innovation
---

> Le géant de Mountain View a vivement réagi à l’annonce d’un nouvel accord de licence conclu avec Samsung pour l’utilisation d’Android dans ses tablettes et smartphones. Un frein à l'innovation selon Google
