---
site: Le Monde Informatique
title: "Un partenariat franco-tunisien pour promouvoir les logiciels libres"
author: Bertrand Lemaire
date: 2011-09-14
href: http://www.lemondeinformatique.fr/actualites/lire-un-partenariat-franco-tunisien-pour-promouvoir-les-logiciels-libres-34669.html
tags:
- Entreprise
- Institutions
- Associations
- International
---

> La FNILL et l'APOS entament un partenariat pour faciliter les collaborations entre leurs adhérents.
