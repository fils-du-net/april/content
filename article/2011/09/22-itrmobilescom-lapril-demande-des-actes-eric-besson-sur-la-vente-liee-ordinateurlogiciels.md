---
site: ITRmobiles.com
title: "L'April demande des actes à Eric Besson sur la vente liée ordinateur/logiciels"
date: 2011-09-22
href: http://www.itrmobiles.com/index.php/articles/123476/april-demande-actes-eric-besson-vente-liee-ordinateur-logiciels.html
tags:
- Logiciels privateurs
- Économie
- April
- Institutions
- Vente liée
- Marchés publics
---

> Le jeudi 22 septembre 2011, le ministre de l'Industrie, de l'Énergie et de l'Économie Numérique Éric Besson a fait un discours à l'Open World Forum sur la complémentarité entre logiciel libre et logiciel propriétaire, le potentiel économique du logiciel libre et l'importance stratégique du logiciel libre selon le gouvernement. L'April appelle le ministre à agir concrètement sur ces déclaration et à prendre ses responsabilités dans le cadre du projet de loi consommation.
