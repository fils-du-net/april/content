---
site: atlantico
title: "Ressources numériques et néanmoins scolaires"
author: Nathalie Joannes
date: 2011-09-05
href: http://www.atlantico.fr/rdvpolitique/logiciel-educatif-numerique-scolaire-174380.html
tags:
- Internet
- Logiciels privateurs
- Partage du savoir
- Éducation
---

> Un bon logiciel éducatif doit avoir quatre qualités : être adapté aux programmes scolaires, centré sur l'utilisateur c'est à dire l'enfant ; il doit compléter le travail des enseignants et donner envie aux parents de s’impliquer. Suggestions non exhaustives...
