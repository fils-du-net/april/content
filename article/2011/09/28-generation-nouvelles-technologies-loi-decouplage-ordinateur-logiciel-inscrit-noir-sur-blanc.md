---
site: Génération nouvelles technologies
title: "Loi: découplage ordinateur-logiciel inscrit noir sur blanc ?"
author: Jérôme G.
date: 2011-09-28
href: http://www.generation-nt.com/vente-liee-april-projet-loi-consommateurs-actualite-1262661.html
tags:
- April
- Institutions
- Vente liée
---

> L'April note que des amendements au projet de loi pour la protection des consommateurs vont dans le sens d'une fin pour la vente liée ordinateur-logiciels.
