---
site: ITRmanager.com
title: "Android Wars"
author: Jean-Marie Chauvet
date: 2011-09-18
href: http://www.itrmanager.com/articles/123278/android-wars.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
---

> Android pourrait bien rester dans l'histoire comme le nom de la guerre mondiale des brevets qui point dans la galaxie des technologies de l'information. Si une trilogie Android Wars attend encore son George Lucas, David Drummond, Chief Legal Officer de Google, fut prompt dans son blog à présenter le moteur de recherches comme victime d'une attaque organisée de la coalition des Sith bien connus Darth Microsoft, Darth Oracle et Darth Apple.
