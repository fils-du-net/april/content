---
site: Le Monde Informatique
title: "En France, 26% des ordinateurs équipés de logiciels piratés"
date: 2011-09-08
href: http://www.lemondeinformatique.fr/actualites/lmi/lire-en-france-26-des-ordinateurs-equipes-de-logiciels-pirates-34602.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Institutions
- Droit d'auteur
- International
---

> Près d'un ordinateur sur deux dans le monde (47%) est équipé d'un ou plusieurs logiciels piraté, selon une étude de la Business Software Alliance (BSA) Cela concerne aussi bien les versions illégalement téléchargées que les licences détournées.
