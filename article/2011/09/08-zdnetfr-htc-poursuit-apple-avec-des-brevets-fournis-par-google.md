---
site: ZDNet.fr
title: "HTC poursuit Apple avec des brevets fournis par Google"
date: 2011-09-08
href: http://www.zdnet.fr/actualites/htc-poursuit-apple-avec-des-brevets-fournis-par-google-39763664.htm
tags:
- Entreprise
- Brevets logiciels
---

> HTC a saisi la justice américaine ainsi que l'International Trade Commission ; le groupe accuse Apple d'enfreindre 9 de ses brevets qu'il a rachetés à Google. Quatre d'entre eux proviennent de Motorola.
