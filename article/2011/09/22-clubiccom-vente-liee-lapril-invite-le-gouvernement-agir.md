---
site: clubic.com
title: "Vente liée : l'April invite le gouvernement à agir"
author: Olivier Robillart
date: 2011-09-22
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/logiciel-libre-open-source/actualite-448228-april-libre-besson.html
tags:
- Logiciels privateurs
- April
- Institutions
---

> L'organisme en charge de la promotion du logiciel libre rappelle le ministère de l'Economie numérique à ses engagements. Précisément, l'April souhaite que la classe politique mette en place certaines actions promises comme la possibilité d'acheter séparément un ordinateur et un système d'exploitation.
