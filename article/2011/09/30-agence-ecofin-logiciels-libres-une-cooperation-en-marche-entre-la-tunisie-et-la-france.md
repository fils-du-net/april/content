---
site: Agence Ecofin
title: "Logiciels libres : une coopération en marche entre la Tunisie et la France"
date: 2011-09-30
href: http://www.agenceecofin.com/software/3009-1509-logiciels-libres-une-cooperation-en-marche-entre-la-tunisie-et-la-france
tags:
- Entreprise
- Institutions
- Associations
- International
---

> Organisées par l'Association des professionnels de l'open source (Tunisie) et la Fédération de l'industrie du logiciel libre (France), les rencontres franco-tunisiennes du logiciel libre ont été ouvertes le 29 septembre 2011 à Tunis.
