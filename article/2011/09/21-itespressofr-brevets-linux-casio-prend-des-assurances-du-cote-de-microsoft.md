---
site: ITespresso.fr
title: "Brevets Linux : Casio prend des assurances du côté de Microsoft"
date: 2011-09-21
href: http://www.itespresso.fr/brevets-linux-casio-prend-des-assurances-du-cote-de-microsoft-46389.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
---

> Microsoft et Casio ont signé un accord de licences de brevets Linux, qui couvrira les besoins du groupe high-tech japonais et ceux de ses clients.
