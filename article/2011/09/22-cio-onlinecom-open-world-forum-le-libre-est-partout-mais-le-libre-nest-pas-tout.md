---
site: "cio-online.com"
title: "Open World Forum : le Libre est partout mais le Libre n'est pas tout"
author: Bertrand Lemaire
date: 2011-09-22
href: http://www.cio-online.com/actualites/lire-open-world-forum%C2%A0-le-libre-est-partout-mais-le-libre-n-est-pas-tout-3837.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- April
- HADOPI
- Vente liée
- Neutralité du Net
- Informatique en nuage
- Open Data
---

> L'Open World Forum, plus grande manifestation de son genre consacrée au Libre en Europe, se tient à Paris du 22 au 24 septembre 2011. Le ministre Eric Besson l'a inaguré.
