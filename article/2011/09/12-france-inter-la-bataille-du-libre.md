---
site: France Inter
title: "La bataille du libre"
author: Antoine Chao
date: 2011-09-12
href: http://www.franceinter.fr/emission-la-bas-si-j-y-suis-la-bataille-du-libre
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Sensibilisation
- Vente liée
- Associations
---

> Voilà pourquoi nous rencontrons aujourd'hui les militants du logiciel libre, le "LIBRE", qui luttent contre Microsoft et pour "la coopération pas la concurrence".
