---
site: LeJournalduNet
title: "Les levées de fonds dans l'Open Source décollent"
author: Dominique FILIPPONE, Journal du Net
date: 2011-09-12
href: http://www.journaldunet.com/solutions/intranet-extranet/levee-de-fonds-open-source-bonitasoft-et-prestashop.shtml
tags:
- Entreprise
- Économie
---

> BonitaSoft et PrestaShop sont parvenus à lever plus de 10 millions d'euros à eux deux. Un montant conséquent dans un contexte de reprise économique incertaine.
