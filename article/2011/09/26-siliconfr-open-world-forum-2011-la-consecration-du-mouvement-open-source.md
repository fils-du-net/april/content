---
site: Silicon.fr
title: "Open World Forum 2011 : la consécration du mouvement open source"
author: David Feugey
date: 2011-09-26
href: http://www.silicon.fr/open-world-forum-2011-la-consecration-du-mouvement-open-source-61742.html
tags:
- Entreprise
- Internet
- Open Data
---

> Le bilan de l’Open Word Forum 2011 de Paris est plutôt bon. Le mouvement open source semble transformer tout ce qu’il touche. À l’évidence, il a maintenant franchi un point de non-retour.
