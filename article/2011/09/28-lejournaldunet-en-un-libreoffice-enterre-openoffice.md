---
site: LeJournalduNet
title: "En un an, LibreOffice a enterré OpenOffice"
date: 2011-09-28
href: http://www.journaldunet.com/solutions/intranet-extranet/libreoffice-versus-openoffice-0911.shtml
tags:
- Entreprise
- Associations
---

> Alors que le développement d'OpenOffice, abandonné par Oracle, est au point mort, le clone de la suite bureautique Open Source compte une communauté active et grandissante.
