---
site: Histoire CIGREF
title: "1991, un réseau de développeurs fait naitre Linux"
date: 2011-09-12
href: http://www.histoire-cigref.org/blog/1991-un-reseau-de-developpeurs-fait-naitre-linux/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Économie
- Sensibilisation
- Associations
---

> Avant que les réseaux sociaux ne deviennent un nouvel « art de vivre », un réseau, une communauté de développeurs, œuvre sans tambour ni trompette pour mettre au monde un animal non moins silencieux, un petit manchot au large bec jaune, baptisé Linux…
