---
site: Les Echos
title: "Comment les entrepreneurs utilisent la subversion pour internationaliser leur offre"
author: Sylvain Bureau
date: 2011-09-01
href: http://www.lesechos.fr/economie-politique/france/dossier/0201600739296/0201600738353-comment-les-entrepreneurs-utilisent-la-subversion-pour-internationaliser-leur-offre-213239.php
tags:
- Entreprise
- Économie
- Innovation
---

> Pour créer un monde nouveau, il faut savoir détruire l'ancien. La subversion est un formidable moyen pour y parvenir. Et les entrepreneurs l'utilisent largement. avis d'expert
