---
site: Numerama
title: "La vente liée d'un ordinateur à un OS bientôt interdite par la loi ?"
author: Julien L.
date: 2011-09-27
href: http://www.numerama.com/magazine/19949-la-vente-liee-d-un-ordinateur-a-un-os-bientot-interdite-par-la-loi.html
tags:
- April
- Institutions
- Vente liée
---

> Deux amendements déposés par des députés de gauche et de droite visent à interdire la vente liée d'un ordinateur à un système d'exploitation. Destinés au projet de loi sur la protection du consommateur, ils rappellent en particulier à Eric Besson ses engagements pris dans le cadre du plan France Numérique 2012.
