---
site: Génération nouvelles technologies
title: "Vente liée : l'April interpelle Éric Besson"
author: Jérôme G.
date: 2011-09-22
href: http://www.generation-nt.com/vente-liee-april-besson-ordinateur-logiciel-actualite-1259981.html
tags:
- April
- Institutions
- Vente liée
---

> L'association de promotion et de défense du logiciel libre rappelle au ministre en charge de l'Économie numérique ses engagements passés sur la vente liée ordinateur-logiciels.
