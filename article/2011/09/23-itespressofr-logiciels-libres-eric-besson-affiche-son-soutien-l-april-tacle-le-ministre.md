---
site: ITespresso.fr
title: "Logiciels libres : Eric Besson affiche son soutien, l’April tacle le ministre"
author: Jacques Franc de Ferrière
date: 2011-09-23
href: http://www.itespresso.fr/logiciels-libres-eric-besson-affiche-son-soutien-l-april-tacle-le-ministre-46658.html
tags:
- Administration
- April
- Institutions
- Vente liée
- Marchés publics
- Informatique en nuage
---

> Selon le ministre de l’Economie numérique, les logiciels libres constitueront « un axe fort de notre future stratégie France numérique 2020″. Que l’Etat respecte ses engagements précédents, rétorque l’April.
