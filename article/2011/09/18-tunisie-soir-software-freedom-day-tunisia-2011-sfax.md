---
site: Tunisie Soir
title: "«Software Freedom Day Tunisia 2011» à Sfax"
date: 2011-09-18
href: http://tunisiesoir.com/general/180911-software-freedom-day-tunisia-2011-a-sfax.html
tags:
- Entreprise
- Internet
- Éducation
- International
---

> SFAX (TAP) - Plus de 350 étudiants, appartenant à de nombreuses institutions universitaires spécialisées dans les domaines des nouvelles technologies de la communication et de l'information (NTIC) à travers les différentes régions du pays, ont participé à la manifestation "Software Freedom Day Tunisia 2011" organisée, samedi à Sfax, sous le signe "Cesser d'être consommateur de technologie et commencer à générer de la valeur".
