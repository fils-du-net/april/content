---
site: ParisTech REVIEW
title: "Les brevets freinent-ils l'innovation?"
date: 2011-09-23
href: http://www.paristechreview.com/2011/09/23/brevets-freinent-innovation/
tags:
- Entreprise
- Économie
- Institutions
- Brevets logiciels
---

> Le sénat américain vient d'adopter une loi réformant le système des brevets, sans apaiser les polémiques qui agitent depuis une dizaine d’années les milieux académiques et la Silicon Valley. Les brevets, considère-t-on généralement, permettent de soutenir l'innovation. Vraiment?
