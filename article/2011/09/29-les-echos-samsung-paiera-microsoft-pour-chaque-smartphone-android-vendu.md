---
site: Les Echos
title: "Samsung paiera Microsoft pour chaque « smartphone » Android vendu"
author: Guillaume de Calignon
date: 2011-09-29
href: http://www.lesechos.fr//entreprises-secteurs/tech-medias/actu/0201666171865-samsung-paiera-microsoft-pour-chaque-smartphone-android-vendu-226017.php
tags:
- Entreprise
- Brevets logiciels
---

> La bataille que se livrent les géants du mobile et de l'électronique dans les systèmes d'exploitation pour « smartphones » a connu hier un nouveau développement surprenant. Samsung a signé un accord avec Microsoft. Le groupe asiatique a accepté de payer des royalties pour tous les « smartphone » et tablettes qu'il fabriquera... équipés d'Android, le système d'exploitation développé par Google. Microsoft revendique en effet la propriété intellectuelle de certaines parties d'Android. Les taïwanais HTC et Acer, plus petits, ont déjà conclu un accord similaire avec Microsoft.
