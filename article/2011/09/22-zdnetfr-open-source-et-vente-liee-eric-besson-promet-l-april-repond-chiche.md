---
site: ZDNet.fr
title: "Open Source et vente liée: Eric Besson promet, l’April répond chiche !"
author: Christophe Auffray
date: 2011-09-22
href: http://www.zdnet.fr/actualites/open-source-et-vente-liee-eric-besson-promet-l-april-repond-chiche-39764176.htm
tags:
- Logiciels privateurs
- Économie
- April
- Institutions
- Vente liée
---

> Le plan France numérique 2020 accordera une large place à l’Open Source assure Eric Besson. Simple promesse de campagne ? L’April rappelle le ministre aux précédentes promesses du plan numérique 2012 en matière de vente liée et non tenues.
