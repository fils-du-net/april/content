---
site: OWNI
title: "Les autres modèles économiques des licences libres"
author: Calimaq
date: 2011-09-02
href: http://owni.fr/2011/09/02/les-autres-modeles-economiques-des-licences-libres/
tags:
- Internet
- Économie
- Partage du savoir
- Licenses
- Contenus libres
---

> La dessinatrice américaine Nina Paley a publié 20.000 de ses minibooks grâce aux dons de contributeurs. OpenUtopia ressuscite l'oeuvre de Thomas More. Le tout créant d'autres modèles économiques. Etat des lieux de projets en cours.
