---
site: Le Monde Informatique
title: "Open World Forum : Le logiciel libre et Internet dans l'arène politique"
author: Maryse Gros
date: 2011-09-22
href: http://www.lemondeinformatique.fr/actualites/lire-open-world-forum-le-logiciel-libre-et-internet-dans-l-arene-politique-42010.html
tags:
- Entreprise
- Internet
- Administration
- Institutions
- Marchés publics
- Open Data
---

> C'est Eric Besson, ministre chargé de l'Economie Numérique, qui a ouvert ce matin le 4e Open World Forum, réaffirmant l'engagement de l'Etat dans l'Open Source. A sa suite, Jean-Louis Missika, adjoint au Maire de Paris, chargé de l'innovation, a profité de la présence du ministre pour interpeller les pouvoirs publics.
