---
site: ITRnews.com
title: "Smile publie le guide de l'open source"
date: 2011-09-27
href: http://www.itrnews.com/articles/123656/smile-publie-guide-open-source.html
tags:
- Entreprise
---

> Plus de 150 solutions open source ont été passées au crible dans 40 domaines d'applications afin de réaliser ce guide de l'open source. Résultat : Smile donne les clés pour bâtir des architectures web pertinentes.
