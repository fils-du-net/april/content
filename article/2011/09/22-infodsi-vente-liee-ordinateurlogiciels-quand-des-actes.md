---
site: infoDSI
title: "Vente liée ordinateur/logiciels : à quand des actes ?"
date: 2011-09-22
href: http://www.infodsi.com/articles/123499/vente-liee-ordinateur-logiciels-actes.html
tags:
- Logiciels privateurs
- Économie
- April
- Institutions
- Vente liée
- Marchés publics
---

> Le jeudi 22 septembre 2011, le ministre de l'Industrie, de l'Énergie et de l'Économie Numérique Éric Besson a fait un discours à l'Open World Forum sur la complémentarité entre logiciel libre et logiciel propriétaire, le potentiel économique du logiciel libre et l'importance stratégique du logiciel libre selon le gouvernement. L'April appelle le ministre à agir concrètement sur ces déclarations et à prendre ses responsabilités dans le cadre du projet de loi consommation.
