---
site: PC INpact
title: "Conso : Lionel Tardy veut découpler la vente PC, OS et applicatifs"
author: Marc Rees
date: 2011-09-26
href: http://www.pcinpact.com/actu/news/66002-lionel-tardy-vente-subordonnee-eric-besson.htm
tags:
- April
- Institutions
- Vente liée
---

> Le député Lionel Tardy vient de déposer un amendement 308 au projet de loi Protection du Consommateur. Un amendement qui risque de provoquer des remous.
