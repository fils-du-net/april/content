---
site: LeMonde.fr
title: "Google au cœur de la guerre des brevets"
date: 2011-09-08
href: http://www.lemonde.fr/technologies/article/2011/09/08/google-au-c-ur-de-la-guerre-des-brevets_1569217_651865.html
tags:
- Entreprise
- Brevets logiciels
- Licenses
---

> Samsung, HTC, Motorola, Apple, Oracle... et Google. Alors que les procédures pour violation de brevets se multiplient entre les constructeurs de téléphones mobiles et de tablettes, mais aussi entre les éditeurs de systèmes d'opération pour ces mêmes terminaux, le géant du Web se trouve au cœur de deux conflits majeurs avec Apple et Oracle.
