---
site: écrans
title: " Samsung et Apple, retour de baston"
author: Virginie Malbos
date: 2011-09-16
href: http://www.ecrans.fr/Samsung-et-Apple-retour-de-baston,13287.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Licenses
- International
---

> C’est un spectacle tout à fait fascinant que d’observer depuis cinq mois ces deux combattants s’affronter sur le ring.
