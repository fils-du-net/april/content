---
site: atlantico
title: "La France est gravement en retard dans le développement économique sur Internet"
author: Jean-Baptiste Soufron et Mehdi Benchoufi
date: 2011-09-19
href: http://www.atlantico.fr/decryptage/economie-numerique-france-en-retard-183289.html
tags:
- Entreprise
- Internet
- Économie
- Brevets logiciels
- Droit d'auteur
- Éducation
- Innovation
- Europe
- Open Data
---

> Alors que nous vivons une véritable révolution numérique, la France n'apparaît qu'au 20ème rang des puissances du web dans différents classements internationaux. Un manque de réactivité gravement dommageable pour notre économie et notre rayonnement futurs
