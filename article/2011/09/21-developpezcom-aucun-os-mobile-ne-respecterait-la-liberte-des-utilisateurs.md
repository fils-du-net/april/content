---
site: Developpez.com
title: "Aucun OS mobile ne respecterait la liberté des utilisateurs"
date: 2011-09-21
href: http://www.developpez.com/actu/37163/Aucun-OS-mobile-ne-respecterait-la-liberte-des-utilisateurs-d-apres-la-Free-Software-Foundation-qui-milite-pour-un-fork-libre-d-Android/
tags:
- Entreprise
- Logiciels privateurs
- Associations
---

> D'après la Free Software Foundation, qui plébiscite un fork libre d'Android
