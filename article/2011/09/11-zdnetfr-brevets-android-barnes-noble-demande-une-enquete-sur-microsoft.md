---
site: ZDNet.fr
title: "Brevets Android : Barnes &amp; Noble demande une enquête sur Microsoft"
author: Christophe Auffray
date: 2011-09-11
href: http://www.zdnet.fr/actualites/brevets-android-barnes-noble-demande-une-enquete-sur-microsoft-39765495.htm
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Innovation
---

> Poursuivi par Microsoft après avoir refusé de payer des royalties sur ses ventes de lecteurs sous Android, le libraire Barnes &amp; Noble dénonce les pratiques de l’éditeur. Selon lui, Microsoft utiliserait l’intimidation, limiterait le droit de mettre à jour les terminaux Android, ferait payer une licence indue pour favoriser Windows Phone, et ce sans même détenir de brevets essentiels sur l’OS.
