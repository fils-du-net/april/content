---
site: tekiano.com
title: "APOS, pour défendre l'Open Source en Tunisie"
date: 2011-09-19
href: http://www.tekiano.com/tek/soft/4283-apos-pour-defendre-lopen-source-en-tunisie.html
tags:
- Logiciels privateurs
- Associations
- Licenses
- International
---

> L'Association des professionnels de l'Open Source APOS est née. Son but est de contribuer à la promotion des activités associées à l'Open Source software, Open gouvernance, Open Data, Commen Creative et Copy Left.
