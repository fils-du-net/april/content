---
site: CentPapiers
title: "La redistribution radicale des richesses"
author: "8119"
date: 2011-09-13
href: http://www.centpapiers.com/la-redistribution-radicale-des-richesses/81805
tags:
- Entreprise
- Économie
- Institutions
- International
---

> On a vu ce terme apparaître comme revendication la plus spontanée de la révolution égyptienne, à la fin de la journée du combat sur le pont, où finalement c’est l’armée qui recula.
