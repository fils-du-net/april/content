---
site: "les-infostrateges.com"
title: "Le Web 3.0 : état des lieux et perspectives d'avenir"
author: Isabelle Boquet-Cochard, Mélody Collé, Patricia Dornès, Anne-Sophie Hardel et David Joao (sous la direction Sébastien Bruyère)
date: 2011-09-12
href: http://www.les-infostrateges.com/article/1109383/le-web-30-etat-des-lieux-et-perspectives-d-avenir
tags:
- Internet
- Innovation
- Informatique en nuage
---

> De grands bouleversements actuels et accélérés autour de certaines technologies et services de l’information et de la communication sont en train d’émerger. Comment en est-t-on arrivé là ? Pourquoi l’Internet de demain va-t-il jouer un rôle fondamental pour les entreprises, les services et les particuliers ? Un véritable défi est lancé aux professionnels de l'information. (Poupeau, 2009)
