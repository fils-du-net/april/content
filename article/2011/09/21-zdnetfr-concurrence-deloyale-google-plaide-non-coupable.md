---
site: ZDNet.fr
title: "Concurrence déloyale : Google plaide non coupable"
author: La rédaction
date: 2011-09-21
href: http://www.zdnet.fr/actualites/concurrence-deloyale-google-plaide-non-coupable-39764130.htm
tags:
- Entreprise
- Internet
- Institutions
---

> Favoritisme à l’égard de ses services, contrôle des données proposées aux internautes, restrictions sur les applications fournies par défaut sur les mobiles Android… Google liste et réfute les principales critiques le concernant.
