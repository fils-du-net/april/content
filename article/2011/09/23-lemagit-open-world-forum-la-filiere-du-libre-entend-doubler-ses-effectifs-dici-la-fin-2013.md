---
site: LeMagIT
title: "Open World Forum : la filière du Libre entend doubler ses effectifs d'ici la fin 2013 "
author: Cyrille Chausson
date: 2011-09-23
href: http://www.lemagit.fr/article/recrutement-open-source/9534/1/open-world-forum-filiere-libre-entend-doubler-ses-effectifs-ici-fin-2013/
tags:
- Entreprise
- Économie
- Institutions
---

> Crise, innovation rabotée … L'Open World Forum version 2011 s'est ouvert dans un climat tendu notamment du fait de la volte face du gouvernement sur le crédit d'impôt recherche. Mais les participants ont été ragaillardis par une étude Ploss qui entrevoit la création de 3000 à 5000 emplois par la filière du libre française d'ici à 2013. De quoi doubler les effectifs de la filière.
