---
site: "cio-online.com"
title: "Vers une interdiction de la vente liée ordinateurs-logiciels ?"
author: Bertrand Lemaire
date: 2011-09-27
href: http://www.cio-online.com/actualites/lire-vers-une-interdiction-de-la-vente-liee-ordinateurs-logiciels%C2%A0-3847.html
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
---

> Des amendements à la loi « protection des consommateurs » remettent la vente liée au coeur du débat. Toutes les entreprises sont concernées directement ou indirectement.
