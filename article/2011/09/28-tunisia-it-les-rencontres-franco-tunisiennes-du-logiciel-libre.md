---
site: Tunisia IT
title: "Les Rencontres Franco-Tunisiennes du Logiciel Libre"
date: 2011-09-28
href: http://www.tunisiait.com/article.php?article=8375
tags:
- Entreprise
- April
- Institutions
- International
---

> Depuis de nombreuses années, des entrepreneurs et des professionnels du Logiciel Libre en Tunisie et en France travaillent et collaborent ensemble. Suite à la révolution tunisienne du début de l’année 2011, les acteurs dans ce domaine ont décidé de renforcer les liens qui les unissent et de mettre en œuvre une coopération économique renforcée.
