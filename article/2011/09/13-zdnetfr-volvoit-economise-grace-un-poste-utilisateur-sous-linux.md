---
site: ZDNet.fr
title: "VolvoIT économise grâce à un poste utilisateur sous Linux"
author: Christophe Auffray
date: 2011-09-13
href: http://www.zdnet.fr/actualites/volvoit-economise-grace-a-un-poste-utilisateur-sous-linux-39763834.htm
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Informatique en nuage
- International
---

> La branche informatique du constructeur Volvo teste auprès de ses utilisateurs chinois un poste de travail Open Source basé sur la distribution Ubuntu. Un poste qui couvre 80% des fonctionnalités d’un poste Windows classique, mais pour un coût de 20%.
