---
site: 01net.
title: "Vente liée : le monde du libre demande des actes"
author: Guillaume Deleurence
date: 2011-09-23
href: http://www.01net.com/editorial/541880/vente-liee-le-monde-du-libre-demande-des-actes-a-eric-besson/
tags:
- Logiciels privateurs
- April
- Institutions
- Vente liée
- Marchés publics
- Europe
---

> A l'occasion du salon Open World Forum à Paris, l'April interpelle de nouveau le ministre de l'Industrie sur le dossier du découplage ordinateur/logiciel, qu'elle rêvait de voir intégré au projet de loi sur la protection des consommateurs.
