---
site: L'ENTREPRISE.com
title: "E-commerce : ouvrir sa boutique en ligne"
author: Muriel Jaouen
date: 2011-09-13
href: http://lentreprise.lexpress.fr/internet-canal-pour-vendre/e-commerce-ouvrir-sa-boutique-en-ligne_30555.html
tags:
- Entreprise
- Internet
---

> Il existe une palette d'outils pour faciliter le lancement d'un site. Mais rien n'est jamais totalement simple. Ni totalement gratuit.
