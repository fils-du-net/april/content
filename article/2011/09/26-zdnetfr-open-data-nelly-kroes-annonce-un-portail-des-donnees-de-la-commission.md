---
site: ZDNet.fr
title: "Open Data : Nelly Kroes annonce un portail des données de la Commission"
author: Philippe Leroy
date: 2011-09-26
href: http://www.zdnet.fr/actualites/open-data-nelly-kroes-annonce-un-portail-des-donnees-de-la-commission-39764291.htm
tags:
- Administration
- Institutions
- Licenses
- Europe
- Open Data
---

> La commissaire en charge du Digital Agenda annonce un portail des données de la Commission pour le printemps et son intention d'améliorer la directive de 2003 sur la réutilisation des informations du secteur public.
