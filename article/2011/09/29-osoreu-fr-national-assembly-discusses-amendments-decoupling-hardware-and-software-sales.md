---
site: OSOR.EU
title: "FR: National Assembly discusses amendments on decoupling hardware and software sales"
author: OSOR Editorial Team
date: 2011-09-29
href: http://www.osor.eu/news/fr-national-assembly-discusses-amendments-on-decoupling-hardware-and-software-sales
tags:
- April
- Institutions
- Vente liée
---

> (Amemdement au projet de loi Français pour renforcer les droits des consommateurs) Amendments to France's draft law on 'reinforcing consumers' rights, protection and information' have been proposed with the purpose of putting an end to bundled hardware/software sales, the French  advocacy organisation for free software and open standards APRIL announced on 27 September 2011.
