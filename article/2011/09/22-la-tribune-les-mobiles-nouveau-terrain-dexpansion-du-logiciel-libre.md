---
site: LA TRIBUNE
title: "Les mobiles, nouveau terrain d'expansion du logiciel libre "
author: Laurent Péricone
date: 2011-09-22
href: http://www.latribune.fr/entreprises-finance/communication/informatique-electronique/20110922trib000651016/les-mobiles-nouveau-terrain-d-expansion-du-logiciel-libre-.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Informatique en nuage
- Open Data
---

> Le boom de l'Internet mobile, porté par des plates-formes très diverses, pourrait donner un coup de fouet aux logiciels libres.
