---
site: tekiano.com
title: "Software Freedom Day : «La Tunisie regorge de talents» "
date: 2011-09-26
href: http://www.tekiano.com/tek/soft/4310-software-freedom-day-lla-tunisie-regorge-de-talentsr-.html
tags:
- Sensibilisation
- International
---

> Le Software Freedom Day a fait un carton plein à Sfax. La Tunisie c'est aussi Sfax, Thela, Sidi Bouzid, le sud, des régions qui regorgent de curieux pour cette extraordinaire culture et de beaucoup de talents encore en friche dans le domaine de l'Open Source.
