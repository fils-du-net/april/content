---
site: LE FIGARO.fr
title: "Samsung s'associe à Intel autour d'un système mobile "
author: Benjamin Ferran
date: 2011-09-28
href: http://www.lefigaro.fr/hightech/2011/09/28/01007-20110928ARTFIG00544-samsung-s-associe-a-intel-autour-d-un-systeme-mobile.php
tags:
- Entreprise
- Associations
---

> Tizen, leur nouveau projet «open source» dérivé de MeeGo, se destine aux smartphones, aux tablettes et aux téléviseurs.
