---
site: come4news
title: "Le logiciel libre/gratuit : Liberté, qualité, économie"
author: jcd51
date: 2011-09-13
href: http://www.come4news.com/le-logiciel-libre-gratuit-liberte,-qualite,-economie-35923
tags:
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Sensibilisation
---

> Quand on vous dit "ordinateur", vous pensez "Windows". Quand vous entendez "traitement de texte", c'est "Microsoft Office". Quelqu'un vous parle de "fichier PDF", vous comprenez "Adobe Reader". "Traitement d'image" ? "Photoshop" !
