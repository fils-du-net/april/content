---
site: Rue89
title: "Peur du piratage ? 4 trucs pour effacer ses traces sur le Web"
author: Piotr C
date: 2011-09-16
href: http://www.rue89.com/explicateur/2011/09/16/peur-du-piratage-quatre-trucs-pour-effacer-ses-traces-sur-le-web-221608
tags:
- Internet
- Sensibilisation
---

> Afin d'échapper aux intrusions informatiques, espionnages numériques, filtrages et autres flicages, voici un guide qui donne des parades aux citoyens paranos ou ayant des choses à se reprocher.
