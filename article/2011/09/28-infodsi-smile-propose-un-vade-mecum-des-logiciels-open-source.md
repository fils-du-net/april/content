---
site: infoDSI
title: "Smile propose un vade-mecum des logiciels open source"
date: 2011-09-28
href: http://www.infodsi.com/articles/123701/smile-propose-vade-mecum-logiciels-open-source.html
tags:
- Entreprise
---

> L’open source est présent dans la plupart des entreprises, grandes et petites. Depuis 2004, environ, Smile publie régulièrement des livres blancs pour présenter la richesse de cette offre : la gestion de contenus en 2004, les portails en 2005, la business intelligence en 2006, la virtualisation en 2007, la gestion électronique de documents et les PGI/ERP en 2008, les VPN open source et les Firewall en 2009, l’ecommerce et les Réseaux Sociaux d'Entreprise en 2010, etc.
