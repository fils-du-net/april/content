---
site: Silicon.fr
title: "Open World Forum – Bull : « Il faut éviter que le cloud ne se transforme en un nouveau silo de données »"
author: David Feugey
date: 2011-09-26
href: http://www.silicon.fr/open-world-forum-%E2%80%93-bull-%C2%AB%C2%A0il-faut-eviter-que-le-cloud-ne-se-transforme-en-un-nouveau-silo-de-donnees%C2%A0%C2%BB-61697.html
tags:
- Entreprise
- Administration
- Informatique en nuage
---

> Quid de l’open source chez Bull ? Jean-Pierre Laisné se charge de répondre à cette question, et de nous apporter sa vision de ce que doit et ne doit pas être le cloud.
