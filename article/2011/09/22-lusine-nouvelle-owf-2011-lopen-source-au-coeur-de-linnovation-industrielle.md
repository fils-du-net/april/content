---
site: L'USINE NOUVELLE
title: "OWF 2011 : l'open source au coeur de l'innovation industrielle"
author: Anne-Marie Rouzeré
date: 2011-09-22
href: http://www.usinenouvelle.com/article/owf-2011-l-open-source-au-coeur-de-l-innovation-industrielle.N159167
tags:
- Entreprise
- Informatique en nuage
---

> C'est ce 22 septembre qu'ouvre la 4ème édition de l'Open World Forum, le rendez-vous de l'open source. L'occasion pour les industriels de trouver des solutions pour pérenniser leurs logiciels. Petit tour des projets présentés.
