---
site: tom's hardware
title: "MeeGo est mort, longue vie à Tizen"
author: David Civera
date: 2011-09-28
href: http://www.presence-pc.com/actualite/Tizen-45128/
tags:
- Entreprise
---

> Imad Sousou, directeur du centre technologique open source d’Intel, vient de publier un billet sur le blog de MeeGo affirmant que les développeurs allaient passer vers un nouveau projet nommé Tizen, un autre système d’exploitation mobile utilisant Linux.
