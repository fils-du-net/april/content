---
site: ITespresso.fr
title: "Open data : le gouvernement milite pour l’ouverture des données publiques"
date: 2011-09-01
href: http://www.itespresso.fr/open-data-le-gouvernement-milite-pour-l%E2%80%99ouverture-des-donnees-publiques-45547.html
tags:
- Internet
- Partage du savoir
- Institutions
- Standards
- Contenus libres
- Open Data
---

> La ministre du Budget Valérie Pécresse a réaffirmé la volonté de l’Etat de mettre à disposition des données publiques via le futur portail data.gouv.fr afin d’en offrir "une possibilité de réutilisation libre, facile et gratuite" par des tiers.
