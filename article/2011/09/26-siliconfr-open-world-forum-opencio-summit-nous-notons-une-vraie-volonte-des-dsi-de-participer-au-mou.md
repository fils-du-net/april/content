---
site: Silicon.fr
title: "Open World Forum – OpenCIO Summit : « Nous notons une vraie volonté des DSI de participer au mouvement open source »"
author: David Feugey
date: 2011-09-26
href: http://www.silicon.fr/open-world-forum-opencio-summit-%C2%AB%C2%A0nous-notons-une-vraie-volonte-des-dsi-de-participer-au-mouvement-open-source%C2%A0%C2%BB-61724.html
tags:
- Entreprise
- Institutions
---

> Les entreprises adoptent dorénavant l’open source pour des raisons stratégiques... et souhaitent participer plus activement à ce mouvement.
