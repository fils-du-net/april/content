---
site: Silicon.fr
title: "Open World Forum : Besson et Missika font campagne autour de l’open source"
author: David Feugey
date: 2011-09-25
href: http://www.silicon.fr/open-world-forum-besson-et-missika-font-campagne-autour-de-l%E2%80%99open-source-61551.html
tags:
- Entreprise
- Internet
- Administration
- Économie
- Institutions
- Marchés publics
- Neutralité du Net
- Open Data
---

> Trois invités ont démarré la quatrième édition de l’Open World Forum : Éric Besson pour le compte du gouvernement, Jean-Paul Planchou pour celui de la région Île-de-France et Jean-Louis Missika de la mairie de Paris.
