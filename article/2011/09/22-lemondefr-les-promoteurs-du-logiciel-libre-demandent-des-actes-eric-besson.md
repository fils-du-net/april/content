---
site: LeMonde.fr
title: "Les promoteurs du logiciel libre demandent \"des actes\" à Eric Besson"
author: Damien Leloup
date: 2011-09-22
href: http://www.lemonde.fr/technologies/article/2011/09/22/les-promoteurs-du-logiciel-libre-demandent-des-actes-a-eric-besson_1576267_651865.html
tags:
- Logiciels privateurs
- April
- Institutions
- Vente liée
---

> "Le logiciel libre a contribué de manière décisive à la révolution du numérique. (...) Je souhaite que l'open source au service du citoyen et de la compétitivité des entreprises soit un axe fort de notre future stratégie France numérique 2020." Lors de l'ouverture de l'Open world forum, un important salon consacré au logiciel libre et aux données publiques, ce jeudi à Paris, le ministre de l'industrie Eric Besson ne tarissait pas d'éloges pour ces logiciels dont le code informatique est librement accessible et modifiable.
