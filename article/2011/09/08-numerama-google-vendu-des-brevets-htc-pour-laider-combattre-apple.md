---
site: Numerama
title: "Google a vendu des brevets à HTC pour l'aider à combattre Apple"
author: Julien L.
date: 2011-09-08
href: http://www.numerama.com/magazine/19733-google-a-vendu-des-brevets-a-htc-pour-l-aider-a-combattre-apple.html
tags:
- Entreprise
- Brevets logiciels
---

> Dans la bataille qui oppose HTC à Apple, Google est loin de rester inactif. Soucieuse de défendre son système d'exploitation mobile, Android, la firme américaine a décidé d'aider le fabricant taïwanais à se défendre contre la société de Cupertino. Et pour cause, HTC utilise massivement Android. Google lui a donc vendu neuf brevets pour lui fournir de nouvelles armes judiciaires.
