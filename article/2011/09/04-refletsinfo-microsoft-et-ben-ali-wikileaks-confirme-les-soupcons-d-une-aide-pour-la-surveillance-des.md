---
site: reflets.info
title: "Microsoft et Ben Ali : Wikileaks confirme les soupçons d’une aide pour la surveillance des citoyens Tunisiens"
author: Fabrice Epelboin
date: 2011-09-04
href: http://reflets.info/microsoft-et-ben-ali-wikileaks-confirme-les-soupcons-d-une-aide-pour-la-surveillance-des-citoyens-tunisiens/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Institutions
- Désinformation
- Informatique-deloyale
- International
---

> Près de six mois après que l’affaire ait été publiée dans Rue89 et savament étouffée par les services de Microsoft, et à peine une semaine après la promotion de la principale instigatrice, alors DG de Microsoft Tunisie, Salwa Smaoui, l’affaire de l’aide apportée par Microsoft dans l’espionnage de la population Tunisienne rebondi grâce à la publication d’un câble Wikileaks issu de l’ambassade américaine de Tunis, qui conclu un long exposé concernant le contrat que nous avions fait fuiter avec le même constat : derrière ce contrat se cache un deal entre le géant de Redmond et le gouvernement de Ben Ali, destiné à espionner la population Tunisienne (et accessoirement, à étouffer le logiciel libre).
