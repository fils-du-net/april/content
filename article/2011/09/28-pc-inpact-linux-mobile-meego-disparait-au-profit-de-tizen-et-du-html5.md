---
site: PC INpact
title: "Linux mobile : MeeGo disparaît au profit de Tizen et du HTML5"
author: Marc Rees
date: 2011-09-28
href: http://www.pcinpact.com/actu/news/66060-tizen-mobile-linux-meego-evolution-html5-lancement.htm
tags:
- Entreprise
- Associations
---

> MeeGo est un système d’exploitation open source basé sur Linux. Son historique récent est particulièrement mouvementé, notamment à cause d’un changement radical de cap de la part du constructeur Nokia. Mais les mouvements ne sont pas terminés puisque le système change de cap et est absorbé par Tizen.
