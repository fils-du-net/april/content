---
site: ZDNet.fr
title: "Licence GNU GPL : Free rentre enfin dans les clous, en toute confidentialité"
author: Christophe Auffray
date: 2011-09-19
href: http://www.zdnet.fr/actualites/licence-gnu-gpl-free-rentre-enfin-dans-les-clous-en-toute-confidentialite-39764024.htm
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Licenses
---

> Free, la FSF et les développeurs des logiciels libres Iptables et BusyBox enterrent la hache de guerre. Si le FAI assurait que sa Freebox était en parfaite conformité avec la GPL v2 , il a néanmoins conclu un accord, confidentiel, pour appliquer plusieurs obligations de la licence.
