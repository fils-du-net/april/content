---
site: OWNI
title: "Le wifi libre entre en résistance"
author: Ophelia Noor
date: 2011-09-12
href: http://owni.fr/2011/09/12/wifi-alternatif-ondes-radio/
tags:
- Entreprise
- Internet
- Institutions
- Europe
---

> La libre utilisation des ondes radio permettrait de multiplier les réseaux WiFi alternatifs et citoyens. Des réseaux indépendants et autonomes. Mais ni les opérateurs ni les États ne semblent prêts à lâcher le morceau.
