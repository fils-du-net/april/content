---
site: technautes.cyberpresse.ca
title: "La guerre des brevets pourrait s'étendre aux réseaux sociaux"
author: Alain McKenna
date: 2011-09-02
href: http://technaute.cyberpresse.ca/nouvelles/internet/201109/02/01-4431077-la-guerre-des-brevets-pourrait-setendre-aux-reseaux-sociaux.php
tags:
- Entreprise
- Internet
- Institutions
- Brevets logiciels
---

> La guerre des brevets qui se dessine dans le créneau de la mobilité informatique est loin d'être terminée, selon un avocat spécialisé en la matière et ancien employé d'Apple. En fait, elle pourrait même s'étendre aux étoiles montantes du web que sont Facebook, LinkedIn, Twitter et Zynga.
