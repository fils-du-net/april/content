---
site: "cio-online.com"
title: "L'Open CIO Summit confirme la place du Libre en entreprise"
author: Bertrand Lemaire
date: 2011-09-22
href: http://www.cio-online.com/actualites/lire-l-open-cio-summit-confirme-la-place-du-libre-en-entreprise-3838.html
tags:
- Entreprise
- Logiciels privateurs
- Licenses
- Informatique en nuage
---

> L'Open World Forum, plus grande manifestation de son genre consacrée au Libre en Europe, se tient à Paris du 22 au 24 septembre 2011. Parmi ses « tracks », l'Open CIO Summit est davantage destiné aux DSI.
