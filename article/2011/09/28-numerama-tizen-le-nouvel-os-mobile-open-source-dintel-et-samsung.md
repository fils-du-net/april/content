---
site: Numerama
title: "Tizen, le nouvel OS mobile open-source d'Intel et Samsung"
author: Simon Robic
date: 2011-09-28
href: http://www.numerama.com/magazine/19970-tizen-le-nouvel-os-mobile-open-source-d-intel-et-samsung.html
tags:
- Entreprise
- Associations
---

> Alors que Nokia s'apprête à sortir son premier téléphone sous Meego et annonce une fusion avec LiMo, Intel dévoile Tizen, une autre plateforme open-source réalisée avec Samsung.
