---
site: Silicon.fr
title: "Détours : l’open source, une réponse à la crise ?"
author: David Feugey
date: 2011-09-28
href: http://www.silicon.fr/detours-l%E2%80%99open-source-une-reponse-a-la-crise%C2%A0-61846.html
tags:
- Entreprise
- Économie
---

> Et si l’open source, auquel nous devons déjà tant, pouvait aider les entreprises à passer au travers des microcrises qui émaillent ce début de siècle ?
