---
site: ActuaLitté.com
title: "Defective by Design épingle le Nook Touch, et son utilisation d'Android ActuaLitté"
author: Clément S.
date: 2011-09-12
href: http://www.actualitte.com/actualite/28201-nook-touch-drm-android-verrous.htm
tags:
- Associations
- DRM
---

> Alors que le lancement du dernier modèle de lecteur ebook du libraire américain Barnes &amp; Noble s'est fait en fanfare, le Nook Touch a attiré l'attention, et pas vraiment sur les révolutions qu'il devait apporter.
