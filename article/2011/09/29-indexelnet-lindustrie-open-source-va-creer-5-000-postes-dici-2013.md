---
site: indexel.net
title: "L'industrie open source va créer 5 000 postes d'ici 2013"
author: Alain Bastide
date: 2011-09-29
href: http://www.indexel.net/actualites/l-industrie-open-source-va-creer-5-000-postes-d-ici-2013-3428.html
tags:
- Entreprise
- Économie
---

> La filière du logiciel libre espère doubler ses effectifs en trois ans pour soutenir sa forte croissance. Pragmatiques, les recruteurs préfèrent les bons développeurs aux consultants ne sachant pas coder.
