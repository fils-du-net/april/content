---
site: Génération nouvelles technologies
title: "Tizen : nouvelle plate-forme mobile open source sous Linux"
author: Christian D.
date: 2011-09-28
href: http://www.generation-nt.com/tizen-limo-foundation-linux-plate-forme-mobile-actualite-1262501.html
tags:
- Entreprise
- Associations
---

> La LiMo Foundation et la Linux Foundation lancent une nouvelle initiative pour créer une plate-forme Linux Tizen à spectre large (du smartphone aux systèmes de bord automobiles) et reposant sur des standards comme le HTML5 et les spécifications de la WAC.
