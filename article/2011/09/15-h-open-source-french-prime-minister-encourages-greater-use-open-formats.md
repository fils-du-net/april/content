---
site: The H Open Source
title: "French Prime Minister encourages greater use of open formats"
author: Edward Henning
date: 2011-09-15
href: http://www.h-online.com/open/news/item/French-Prime-Minister-encourages-greater-use-of-open-formats-1343877.html
tags:
- Administration
- Institutions
- Standards
- Open Data
- English
---

> (Le bureau du Premier Ministre Français, François Fillon, a annoncé avoir émis une circulaire à tous les membres du gouvernement Français encourageant l'usage des formats ouverts et libres comme part de la nouvelle plate-form gouvernemental data.gouv.fr) The office of the Prime Minister of France, François Fillon, has announced that he has issued a circular to all members of the French government encouraging the use of free and open formats as part of new government platform data.gouv.fr
