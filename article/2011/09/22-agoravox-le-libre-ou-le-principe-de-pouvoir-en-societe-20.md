---
site: AgoraVox
title: "Le libre, ou le principe de pouvoir en société ( 2.0 )"
author: emmanuel muller et François Poulain
date: 2011-09-22
href: http://www.agoravox.fr/actualites/societe/article/le-libre-ou-le-principe-de-pouvoir-97313
tags:
- Entreprise
- Logiciels privateurs
- April
- HADOPI
- Associations
- DADVSI
- Licenses
- Philosophie GNU
---

> Le libre c'est le pouvoir qui peut se conjuguer au pluriel.
> Le libre c'est aussi un raccourcis pour désigner un mouvement culturel lié au logiciel libre.
