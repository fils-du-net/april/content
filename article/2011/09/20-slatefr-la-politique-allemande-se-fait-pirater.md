---
site: Slate.fr
title: "La politique allemande se fait pirater"
author: Nicolas Baygert
date: 2011-09-20
href: http://www.slate.fr/story/43913/pirate-parti-tea-parti-allemagne
tags:
- Internet
- Institutions
- International
---

> Le Parti pirate, qui vient d'obtenir 15 sièges au parlement régional de Berlin, de par son éthique open source, se présente comme le pendant «société ouverte» du Tea Party américain, mouvement résolument réactionnaire, qui jusqu’ici symbolisait à lui seul cette opinion protestataire cyber-diffusée.
