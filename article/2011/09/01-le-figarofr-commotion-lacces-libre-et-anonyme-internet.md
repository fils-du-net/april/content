---
site: LE FIGARO.fr
title: "Commotion, l'accès libre et anonyme à Internet"
author: Chloé Woitier
date: 2011-09-01
href: http://www.lefigaro.fr/hightech/2011/09/01/01007-20110901ARTFIG00570-commotion-l-acces-libre-et-anonyme-a-internet.php
tags:
- Entreprise
- Internet
- Économie
- Sensibilisation
- Associations
- Droit d'auteur
- International
---

> Des développeurs américains soutenus par le gouvernement travaillent au projet Commotion, qui permet de créer un réseau sans fil gratuit parallèle à Internet, sans contrôle étatique possible. Lefigaro.fr vous explique son fonctionnement et ses applications possibles.
