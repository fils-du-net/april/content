---
site: ActuaLitté.com
title: "Scryf, un schéma communautaire et open source pour les textes ActuaLitté"
author: Clement S
date: 2011-09-22
href: http://www.actualitte.com/dossiers/1641-Scryf--partager-crowsourcing-editorial-communaute.htm
tags:
- Internet
- Licenses
- Contenus libres
---

> Nous avions évoqué voilà quelques jours le lancement en version Bêta du site Scryf.fr, une plateforme de publication de texte, où sont valorisés échanges et partage, sans publicité ni sponsors.
