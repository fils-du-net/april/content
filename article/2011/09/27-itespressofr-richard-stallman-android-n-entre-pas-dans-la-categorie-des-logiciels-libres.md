---
site: ITespresso.fr
title: "Richard Stallman : Android n’entre pas dans la catégorie des logiciels libres"
date: 2011-09-27
href: http://www.itespresso.fr/richard-stallman-android-n-entre-pas-dans-la-categorie-des-logiciels-libres-46776.html
tags:
- Entreprise
- April
- Licenses
---

> Richard Stallman, figure de proue du logiciel libre, ne perçoit pas l’OS mobile de Google comme un logiciel libre. Et cela a des conséquences pour les consommateurs.
