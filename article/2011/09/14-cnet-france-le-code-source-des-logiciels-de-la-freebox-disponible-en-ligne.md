---
site: cnet France
title: "Le code source des logiciels de la Freebox disponible en ligne"
date: 2011-09-14
href: http://www.cnetfrance.fr/news/le-code-source-des-logiciels-de-la-freebox-disponible-en-ligne-39763891.htm
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Associations
- Licenses
---

> Free vient de publier les codes sources des modifications qu'il applique aux logiciels sous licence open source exploités dans ses Freebox. Le code de la Freebox qui reste « propriétaire » n'est, quant à lui, pas dévoilé.
