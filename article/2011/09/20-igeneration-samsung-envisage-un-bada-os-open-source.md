---
site: igeneration
title: "Samsung envisage un Bada OS open source"
author: Florian Innocente
date: 2011-09-20
href: http://www.igeneration.fr/0-apple/samsung-envisage-un-bada-os-open-source-60392
tags:
- Entreprise
---

> Il fait partie avec Android et Windows Phone des systèmes utilisés par le groupe coréen à travers ses téléphones (il a abandonné Symbian au printemps). On le trouve plutôt dans les modèles d'entrée de gamme, la gamme Wave.
