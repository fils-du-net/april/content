---
site: Numerama
title: "Un rétropédalage du Conseil National du Numérique ?"
author: Guillaume Champeau
date: 2011-09-05
href: http://www.numerama.com/magazine/19703-un-retropedalage-du-conseil-national-du-numerique.html
tags:
- Internet
- April
- Institutions
---

> Le Conseil National du Numérique précise qu'il ne veut pas "noter" les programmes des candidats aux présidentielles dont il souhaite faire une "évaluation", mais les "pousser à expliciter leurs propos". Est-ce mieux ?
