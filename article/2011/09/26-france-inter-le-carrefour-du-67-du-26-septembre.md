---
site: France Inter
title: "Le Carrefour du 6/7 du 26 septembre"
author: Hélène Chevallier
date: 2011-09-26
href: http://www.franceinter.fr/emission-le-carrefour-du-67-le-carrefour-du-67-du-26-septembre
tags:
- Matériel libre
- Sensibilisation
---

> (audio, à partir de la minute 8) Gros plan sur les imprimantes 3D issues du monde du libre. Jusqu'ici réservées aux FabLab, ces imprimantes intéressent de plus en plus les universités et les entreprises de design. Elles permettent d'imprimer des petits objets à faible coût. Reportage dans la société Ckab qui distribue des technologies libres.
