---
site: Tunisie Soir
title: "Rencontres franco-tunisiennes du logiciel"
date: 2011-09-29
href: http://www.tunisiesoir.com/index.php?option=com_content&id=43783
tags:
- Entreprise
- Institutions
- Associations
- International
---

> TUNIS (TAP) - Les rencontres franco-tunisiennes du logiciel libre se tiennent les 29 et 30 Septembre à Tunis, dans l'objectif de favoriser les relations de coopération et de partenariat, dans ce domaine, entre les entreprises des deux pays.
