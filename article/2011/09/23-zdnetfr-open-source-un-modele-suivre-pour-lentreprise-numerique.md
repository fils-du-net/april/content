---
site: ZDNet.fr
title: "Open Source, un modèle à suivre pour l'entreprise numérique"
author: Frédéric Charles
date: 2011-09-23
href: http://www.zdnet.fr/blogs/green-si/open-source-un-modele-a-suivre-pour-l-entreprise-numerique-39764238.htm
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Informatique en nuage
- Open Data
---

> Pour GreenSI c'est en 2000 avec l'expression "click &amp; mortar" que la prise de conscience de la transformation numérique de l'entreprise s'est révelée. C'est à dire la prise de conscience de:
