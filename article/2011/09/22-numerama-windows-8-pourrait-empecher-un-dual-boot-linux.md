---
site: Numerama
title: "Windows 8 pourrait empêcher un dual-boot Linux"
author: Julien L.
date: 2011-09-22
href: http://www.numerama.com/magazine/19907-windows-8-pourrait-empecher-un-dual-boot-linux.html
tags:
- Entreprise
- Logiciels privateurs
- DRM
---

> Windows 8 pourrait-il empêcher un utilisateur d'installer un autre système d'exploitation, à côté de celui conçu par Microsoft ? C'est l'inquiétude d'un développeur spécialisé dans les logiciels libres. Selon lui, la procédure de démarrage est sécurisée de telle façon qu'elle entrave l'installation en multi-boot d'O.S. alternatifs. De son côté, Microsoft reste silencieux.
