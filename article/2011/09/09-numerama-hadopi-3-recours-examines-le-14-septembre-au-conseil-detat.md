---
site: Numerama
title: "Hadopi : 3 recours examinés le 14 septembre au Conseil d'Etat"
author: Guillaume Champeau
date: 2011-09-09
href: http://www.numerama.com/magazine/19761-hadopi-3-recours-examines-le-14-septembre-au-conseil-d-etat.html
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- DRM
---

> La Haute Autorité pour la Diffusion des Oeuvres et la Protection des droits sur Internet (Hadopi) saura bientôt si elle peut continuer à exercer, ou si les textes de loi devront être modifiés.
