---
site: Numerama
title: "Eric Besson voit une synergie entre logiciel libre et logiciel propriétaire"
author: Guillaume Champeau
date: 2011-09-22
href: http://www.numerama.com/magazine/19903-eric-besson-voit-une-synergie-entre-logiciel-libre-et-logiciel-proprietaire.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Institutions
---

> Faut-il opposer logiciels libres et logiciels propriétaires ? Soucieux de concilier les deux mondes, le ministre de l'économie numérique Eric Besson a vanté jeudi les synergies qui pouvaient exister entre les solutions logicielles fermées et celles basées sur une philosophie open-source.
