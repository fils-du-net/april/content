---
site: macgeneration
title: "Stallman : Android est-il libre ?"
author: Anthony Nelzin
date: 2011-09-19
href: http://www.macgeneration.com/web/voir/132772/android-est-il-libre
tags:
- Entreprise
- Logiciels privateurs
- Associations
- Licenses
---

> Richard Stallman, président de la FSF, fondateur du projet GNU et co-créateur de la licence éponyme, profite d'une tribune dans le Guardian pour poser la question : Android est-il vraiment libre ?
