---
site: "agro-media.fr"
title: "Les nouvelles technologies peuvent-elles renforcer la confiance des consommateurs ?"
date: 2011-09-05
href: http://www.agro-media.fr/actualit%C3%A9/marketing-communication/les-nouvelles-technologies-peuvent-elles-renforcer-la-confiance-des-consommateurs
tags:
- Entreprise
- Internet
- Partage du savoir
---

> Les nouvelles technologies peuvent apporter de précieuses informations aux consommateurs et ainsi les rassurer sur les produits qu’ils achètent mais il s’agit d’une arme à double tranchant étant donné qu’internet peut également démolir la réputation d’une marque ou d’un produit en moins de temps qu’il n’en faut pour le dire.
