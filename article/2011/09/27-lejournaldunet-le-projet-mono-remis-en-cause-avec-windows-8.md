---
site: LeJournalduNet
title: "Le projet Mono remis en cause avec Windows 8 ?"
author: Antoine CROCHET-DAMAIS, Journal du Net
date: 2011-09-27
href: http://www.journaldunet.com/developpeur/technos-net/mono-remis-en-cause-avec-windows-8.shtml
tags:
- Entreprise
- Internet
- Logiciels privateurs
---

> L'interface tactile de la prochaine version de l'OS de Microsoft ne sera pas implémentée par le clone Open Source de la plate-forme .Net. C'est ce qu'a annoncé Miguel de Icaza.
