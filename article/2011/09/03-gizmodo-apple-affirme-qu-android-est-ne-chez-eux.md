---
site: GIZMODO
title: "Apple affirme qu’Android est né chez eux"
author: fred
date: 2011-09-03
href: http://www.gizmodo.fr/2011/09/03/apple-affirme-quandroid-est-ne-chez-eux.html
tags:
- Entreprise
- Brevets logiciels
- Droit d'auteur
---

> Au début des années 90, le pape d’Android Andy Rubin a travaillé comme ingénieur chez Apple.
