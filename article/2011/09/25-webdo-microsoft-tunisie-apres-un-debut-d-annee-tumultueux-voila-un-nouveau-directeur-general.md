---
site: webdo
title: "Microsoft Tunisie : après un début d’année tumultueux voilà un nouveau directeur général"
author: Melek Jebnoun
date: 2011-09-25
href: http://www.webdo.tn/2011/09/25/microsoft-tunisie-apres-un-debut-d-annee-tumultueux-voila-un-nouveau-directeur-general/
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- International
---

> La vague de la jeunesse leader fait fureur depuis quelques mois en Tunisie. Et c’est au tour de Microsoft Tunisie de suivre le mouvement. Le représentant tunisien de la firme de Redmond s’autorise un nouveau souffle, après les débuts d’une année 2011 très mouvementés, en nommant un nouveau directeur général à sa tête. Mohamed Bridaa, 36 ans, décontracté, souriant, l’esprit vif, se distingue vraiment des vieux costumes-cravates qu’on a tant connus à la tête de tels postes.
