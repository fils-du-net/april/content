---
site: Numerama
title: "L'April appelle Éric Besson à s'engager enfin sur la vente liée"
author: Julien L.
date: 2011-09-22
href: http://www.numerama.com/magazine/19901-l-april-appelle-eacuteric-besson-a-s-engager-enfin-sur-la-vente-liee.html
tags:
- Logiciels privateurs
- Économie
- April
- Institutions
- Vente liée
- Marchés publics
---

> Alors que le gouvernement prépare le Plan Numérique 2020, l'association de promotion et de défense du logiciel libre April réclame un vrai engagement d'Éric Besson sur la vente liée. L'April rappelle que le précédent plan, remis en 2008, comportait des actions spécifiquement dédiées au découplage entre matériel et logiciel. Or, rien de concret n'a été mis en place.
