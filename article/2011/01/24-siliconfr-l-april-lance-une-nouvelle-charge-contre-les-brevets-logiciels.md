---
site: Silicon.fr
title: "L’April lance une nouvelle charge contre les brevets logiciels"
author: David Feugey
date: 2011-01-24
href: http://www.silicon.fr/l%E2%80%99april-lance-une-nouvelle-charge-contre-les-brevets-logiciels-43976.html
tags:
- April
- Institutions
- Brevets logiciels
- Europe
- International
---

> L’April vient de mettre en ligne une synthèse très complète portant sur les brevets logiciels et les risques qu’ils font courir aux éditeurs, en particulier ceux œuvrant dans le secteur de l’open source.
