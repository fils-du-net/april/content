---
site: L'Avantage.qc.ca
title: "Les logiciels libres"
author: Athéna Informatique
date: 2011-01-19
href: http://www.lavantage.qc.ca/autres-chroniques/19-01-2011-les-logiciels-libres
tags:
- Le Logiciel Libre
- Internet
- Philosophie GNU
- International
---

> Avec un nouvel ordinateur, on a rapidement besoin de logiciels pour effectuer différentes tâches. Le coût d’achat est élevé pour les logiciels populaires, tels la suite Microsoft Office ou un bon logiciel de vidéo. Les logiciels libres représentent une excellente solution pour faire fonctionner son ordinateur, tout en économisant.
