---
site: Developpez.com
title: "Hadopi : le code de ses logiciels sécuritaires sera totalement open-source, de quoi ouvrir la porte aux éditeurs"
author: La rédaction
date: 2011-01-12
href: http://www.developpez.com/actu/27035/Hadopi-le-code-de-ses-logiciels-securitaires-sera-totalement-open-source-de-quoi-ouvrir-la-porte-aux-editeurs/
tags:
- HADOPI
- Institutions
- Désinformation
---

> La Hadopi vient de donner sa première conférence de presse de 2011. Son secrétaire général, Eric Walter, y a fait une annonce importante à propos des "logiciels de sécurisation" que l'Etat demandera prochainement aux internautes d'installer sur leurs machines, professionnels comme particuliers.
