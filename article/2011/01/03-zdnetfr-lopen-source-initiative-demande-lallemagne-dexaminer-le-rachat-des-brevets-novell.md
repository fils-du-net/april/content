---
site: ZDNet.fr
title: "L'Open Source Initiative demande à l'Allemagne d'examiner le rachat des brevets Novell"
author: Christophe Auffray
date: 2011-01-03
href: http://www.zdnet.fr/actualites/l-open-source-initiative-demande-a-l-allemagne-d-examiner-le-rachat-des-brevets-novell-39757180.htm
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Associations
- Brevets logiciels
- International
---

> L'Open Source Initiative demande à l'Allemagne d'examiner le rachat des brevets Novell - Le rachat de 882 brevets de Novell par une holding gérée par Microsoft, Oracle, Apple et EMC a conduit l'OSI à interpeller l'autorité allemande de la concurrence. Selon l'association, les logiciels libres représentent un risque pour plusieurs de ces entreprises. Cette acquisition pourrait donc cacher un acte de concurrence déloyale.
