---
site: OWNI
title: "Si Jean Giono avait connu les licences libres…"
author: Calimaq
date: 2011-01-06
href: http://owni.fr/2011/01/06/si-jean-giono-avait-connu-les-licences-libres/
tags:
- Droit d'auteur
- Licenses
- Video
- Contenus libres
---

> Jean Giono avait renoncé à ses droits d'auteur sur sa nouvelle « L’Homme qui plantait des arbres». Mais certains éditeurs ont essayé de s'attribuer ces droits. Si Giono avait pu la publier sous licence libre, ça aurait simplifié les choses.
