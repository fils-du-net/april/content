---
site: Electron Libre
title: "Sur l’Internet, mieux vaut être un chien"
author: Loic Dequay
date: 2011-01-31
href: http://www.electronlibre.info/Sur-l-Internet-mieux-vaut-etre-un,01049
tags:
- Entreprise
- Internet
- Innovation
---

> A l’heure des réseaux sociaux et des applications mobiles, rester anonyme sur l’Internet n’est plus possible. Le temps des opérateurs d’identités numériques est venu.
