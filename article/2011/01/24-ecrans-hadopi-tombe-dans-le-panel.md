---
site: écrans
title: "Hadopi tombe dans le panel"
author: Camille Gévaudan
date: 2011-01-24
href: http://www.ecrans.fr/Hadopi-tombe-dans-le-panel,11757.html
tags:
- Internet
- Économie
- HADOPI
- Institutions
---

> La Haute autorité a rendu publique une étude équilibrée sur les usages de la consommation culturelle numérique en France.
