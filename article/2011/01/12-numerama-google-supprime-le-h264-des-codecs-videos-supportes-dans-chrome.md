---
site: Numerama
title: "Google supprime le H264 des codecs vidéos supportés dans Chrome"
author: Guillaume Champeau
date: 2011-01-12
href: http://www.numerama.com/magazine/17807-google-supprime-le-h264-des-codecs-videos-supportes-dans-chrome.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Brevets logiciels
- Standards
- Video
---

> Google annonce que son navigateur Chrome ne sera plus capable de lire les vidéos encodées en H.264 dans les prochains mois. Le géant de Mountain View a choisi de ne plus supporter que les formats libres et open-source, en particulier le WebM.
