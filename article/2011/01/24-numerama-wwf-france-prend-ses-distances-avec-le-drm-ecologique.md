---
site: Numerama
title: "WWF France prend ses distances avec le DRM écologique"
author: Guillaume Champeau
date: 2011-01-24
href: http://www.numerama.com/magazine/17873-wwf-france-prend-ses-distances-avec-le-drm-ecologique.html
tags:
- April
- Associations
- DRM
- Innovation
- International
---

> La branche française de l'association de défense de l'écologie ne soutient pas l'initiative prise par WWF Allemagne, qui a proposé en fin d'année dernière de publier des documents sous un format de fichier qui interdit l'impression.
