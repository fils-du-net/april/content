---
site: Commentçamarche.net
title: "CommentCaMarche labellise 100 logiciels gratuits"
author: La rédaction
date: 2011-01-11
href: http://www.commentcamarche.net/news/5853894-commentcamarche-labellise-100-logiciels-gratuits
tags:
- Associations
- Licenses
- Contenus libres
---

> La section "Télécharger" de CommentCaMarche a accueilli il y a peu son 20.000ème logiciel. C'est très bien, mais comment faire son choix parmi plusieurs milliers de logiciels ? Le logiciel le plus téléchargé est-il nécessairement le meilleur ? Une école peut-elle l'utiliser dans un cadre pédagogique ?
