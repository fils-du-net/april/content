---
site: LE CERCLE Les Echos
title: "Firefox le navigateur préféré des Européens"
author: Frédéric Dempuré
date: 2011-01-06
href: http://entrepreneur.lesechos.fr/entreprise/tendances/actualites/firefox-le-navigateur-prefere-des-europeens-110572.php
tags:
- Internet
- Logiciels privateurs
- Europe
---

> Pour la première fois, le navigateur de la fondation Mozilla détrône Internet Explorer dans le cœur des internautes...
