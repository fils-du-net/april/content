---
site: clubic.com
title: "Codec vidéo : Chrome abandonnera le H.264"
author: Guillaume Belfiore
date: 2011-01-12
href: http://www.clubic.com/navigateur-internet/google-chrome/actualite-390222-codec-video-chrome-h264-webm.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Video
---

> Sur le blog officiel du projet open source chromium, Google annonce qu'à l'avenir, le navigateur Chrome ne prendra plus en charge le codec vidéo H.264, une décision motivée par l'adoption et la promotion des technologies ouvertes.
