---
site: Mediapart
title: "La philosophie libre inapplicable dans l'enseignement ?"
author: monpingouin
date: 2011-01-07
href: http://www.mediapart.fr/club/blog/monpingouin/070111/la-philosophie-libre-inapplicable-dans-lenseignement
tags:
- Administration
- Éducation
- Philosophie GNU
---

> Je gravite depuis pas mal d'années maintenant autour des logiciels libres et du système GNU/Linux. Dans mon ancienne profession, j'ai pris l'habitude d'échanger, de mutualiser, de partager.
