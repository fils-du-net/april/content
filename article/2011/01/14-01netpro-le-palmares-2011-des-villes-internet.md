---
site: 01netPro.
title: "Le palmarès 2011 des Villes Internet"
author: Marie Jung
date: 2011-01-14
href: http://pro.01net.com/editorial/526843/le-palmares-2011-des-villes-internet/
tags:
- Internet
- Administration
- Associations
---

> Depuis douze ans, l'association Villes Internet remet un label aux collectivités actives dans le domaine des TIC.
