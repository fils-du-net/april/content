---
site: LeMagIT
title: "Fausse alerte : CPTN suspend ses plans de rachat des brevets Novell... pour mieux revenir"
author: Cyrille Chausson
date: 2011-01-12
href: http://www.lemagit.fr/article/novell-rachat-opensource-attachmate/7875/1/fausse-alerte-cptn-suspend-ses-plans-rachat-des-brevets-novell-pour-mieux-revenir/
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- International
---

> Le consortium CPTN emmené par Microsoft, EMC, Apple et Oracle et censé racheter les très polémiques 882 brevets cédés par Novell dans le cadre de son rachat par Attachmate, a retiré son dossier de création auprès des autorités allemandes…pour mieux le re-soumettre plus tard. Un simple décalage provoqué par un ajustement des mécanismes de financement d’Attachmate qui s’est porté acquéreur de l’éditeur de Suse. Et une fausse joie pour la sphère de l’Open Source.
