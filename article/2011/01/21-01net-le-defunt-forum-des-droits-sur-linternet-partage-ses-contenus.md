---
site: 01net.
title: "Le défunt Forum des droits sur l'Internet partage ses contenus"
author: Guillaume Deleurence
date: 2011-01-21
href: http://www.01net.com/editorial/527212/le-defunt-forum-des-droits-sur-linternet-partage-ses-contenus/
tags:
- Internet
- Administration
- April
- Partage du savoir
- Associations
- Licenses
- Contenus libres
---

> L'organisme, qui a dû se dissoudre faute de subventions publiques, a choisi de placer ses documents sous licences libres.
