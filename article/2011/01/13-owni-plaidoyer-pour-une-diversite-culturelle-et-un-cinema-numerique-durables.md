---
site: OWNI
title: "Plaidoyer pour une diversité culturelle et un cinéma numérique durables"
author: Rodolphe Village
date: 2011-01-13
href: http://owni.fr/2011/01/12/plaidoyer-pour-une-diversite-culturelle-et-un-cinema-numerique-durables/
tags:
- Entreprise
- Logiciels privateurs
- Interopérabilité
- Partage du savoir
- Standards
- Video
---

> L'équipement des salles de cinéma en numérique est un enjeu financier et culturel majeur pour les réseaux de salles indépendantes. Et si la solution était du côté du modèle économique open source ? Les salles Utopia ouvrent le débat.
