---
site: ZDNet.fr
title: "Tunisie, un peu de logiciels libres mais surtout pas de libertés"
author: Thierry Noisette
date: 2011-01-11
href: http://www.zdnet.fr/blogs/l-esprit-libre/tunisie-un-peu-de-logiciels-libres-mais-surtout-pas-de-libertes-39757405.htm
tags:
- Internet
- Institutions
- International
---

> La Tunisie aura comme l'an dernier son «village» au salon Solutions Linux. Le régime du président Ben Ali a manifesté un peu d'intérêt aux logiciels libres, plus en tout cas qu'aux libertés politiques...
