---
site: GEMAKULT
title: "Arx Fatalis : patch et code-source"
author: Poischich
date: 2011-01-14
href: http://www.gamekult.com/actu/arx-fatalis-patch-et-code-source-A0000089805.html
tags:
- Logiciels privateurs
- Licenses
---

> Sorti il y a plus de huit ans, Arx Fatalis a laissé d'excellents souvenirs aux joueurs PC férus de jeux de rôle.
