---
site: ZDNet.fr
title: " Les experts de la gendarmerie passent au logiciel libre"
author: Thierry Noisette
date: 2011-01-19
href: http://www.zdnet.fr/blogs/l-esprit-libre/les-experts-de-la-gendarmerie-passent-au-logiciel-libre-39757621.htm
tags:
- Administration
---

> Au diapason de la gendarmerie qui adopte les logiciels libres étape par étape, son labo de recherche criminelle, l'IRCGN, adopte de même l'open source. "Nous avons tout intérêt à partager nos outils avec les autres forces de police", explique aux Echos son responsable SI.
