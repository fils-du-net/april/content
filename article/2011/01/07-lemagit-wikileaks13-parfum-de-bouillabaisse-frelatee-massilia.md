---
site: LeMagIT
title: "Wikileaks13 : parfum de bouillabaisse frelatée à Massilia "
author: Valery Marchive
date: 2011-01-07
href: http://www.lemagit.fr/article/windows-linux-open-source-marseille-wikileaks/7852/1/wikileaks13-parfum-bouillabaisse-frelatee-massilia/
tags:
- Logiciels privateurs
- Administration
- April
- Institutions
---

> S’inspirant de Julian Assange, Philip Sion, agent du Conseil Général des Bouches-du-Rhône, a lancé, en tout début de mois, Wikileaks13. Un site Web sur lequel il entend que soient publiés documents et informations pour «nettoyer Marseille» malgré «l’omerta, aussi appelée dans les instituions “devoir de réserve”, conçue pour réduire au silence les fonctionnaires trop bavards». Ce qui lui a valu d’être suspendu de ses fonctions dans l’attente d’un conseil de discipline.
