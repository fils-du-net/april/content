---
site: LeMagIT
title: "Firefox devant IE en Europe : Firefox dit merci à Chrome"
author: Cyrille Chausson
date: 2011-01-05
href: http://www.lemagit.fr/article/microsoft-navigateur-europe-firefox-chrome-internet-explorer/7829/1/firefox-devant-europe-firefox-dit-merci-chrome/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Europe
---

> Après de longues années à batailler pour s’imposer comme sa principale alternative, Firefox a fini par détrôner IE sur le marché européen des navigateurs, aidé par une progression de Chrome qui a éteint petit à petit les parts du navigateur Microsoft sur le Vieux Continent. Partout ? Non, IE reste encore largement n°1 dans nombre de pays en Europe, mais connait une déconvenue plus forte dans les pays de l’Est notamment.
