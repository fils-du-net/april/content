---
site: ActuaLitté.com
title: "La France leader pour les manuels numériques Open Source"
author: Clémence Malmejean
date: 2011-01-20
href: http://www.actualitte.com/actualite/23829-livres-scolaires-open-source-france.htm
tags:
- Internet
- Administration
- Éducation
- Contenus libres
- International
---

> Dans un communiqué de presse, l’éditeur indépendant Lelivrescolaire.fr dévoile les résultats d’une étude sur les manuels numériques « open source » (manuels gratuits sur Internet et payant pour leur version papier) en Europe et aux Etats-Unis.
