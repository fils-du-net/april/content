---
site: ZDNet.fr
title: "Non le consortium CPTN piloté par Microsoft ne renonce pas aux brevets de Novell"
author: La rédaction
date: 2011-01-13
href: http://www.zdnet.fr/actualites/non-le-consortium-cptn-pilote-par-microsoft-ne-renonce-pas-aux-brevets-de-novell-39757455.htm
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- International
---

> Non le consortium CPTN piloté par Microsoft ne renonce pas aux brevets de Novell - Si le groupe d'industriels composé de Microsoft, Apple, Oracle et EMC a bien retiré son dossier, il ne renonce pas à acquérir les brevets de Novell. Simple procédure, consécutive au nouveau calendrier de rachat de Novell par Attachmate, indiquent des sources à ZDNet.
