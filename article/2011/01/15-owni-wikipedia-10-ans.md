---
site: OWNI
title: "Wikipédia a 10 ans!"
author: Jean-Noël Lafargue
date: 2011-01-15
href: http://owni.fr/2011/01/15/wikipedia-a-10-ans/
tags:
- Entreprise
- Internet
- Partage du savoir
- Éducation
- Contenus libres
---

> Wikipédia a 10 ans ce samedi 15 janvier ! L'occasion idéale pour revenir sur l'histoire et l'impact de l'encyclopédie collaborative, vue par l'un de ses fidèles contributeurs. OWNI vous offre également une infographie présentant la galaxie Wikimédia.
