---
site: Framablog
title: "À qui la faute si les logiciels libres sous licence GPL sont éjectés de l'App Store ?"
author: aKa
date: 2011-01-15
href: http://www.framablog.org/index.php/post/2011/01/15/vlc-gpl-app-store
tags:
- Entreprise
- Internet
- Droit d'auteur
- Licenses
- Philosophie GNU
---

> On peut reprocher beaucoup de choses à Microsoft mais jamais on n’a vu un logiciel libre empêché de tourner sous Windows parce que la licence du premier était incompatible avec le contrat d’utilisation du second (sinon un projet comme Framasoft n’aurait d’ailleurs pas pu voir le jour).
