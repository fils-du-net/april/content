---
site: FranceGenWeblog
title: "Données publiques, accès ouvert ou fermé ?"
author: Guillaume
date: 2011-01-19
href: http://www.francegenweb.org/blog/index.php?post/2011/01/19/Donn%C3%A9es-publiques%2C-acc%C3%A8s-ouvert-ou-ferm%C3%A9
tags:
- Administration
- April
- Partage du savoir
- Licenses
- Contenus libres
---

> Il y a un an environ, nous vous parlions du questionnaire de Creative Commons, destiné aux candidats aux élections régionales sur l’accès ouvert aux données publiques. La question rebondit ces derniers jours avec deux informations: d'une part une interview chat de Tangui Morlier, président de l'April (association de promotion et la défense du logiciel libre dans l'espace francophone) et d'autre part le Conseil de Paris a pris une délibération approuvant le projet de licence ODBL (Open Database licence) pour les données publiques de la Ville.
