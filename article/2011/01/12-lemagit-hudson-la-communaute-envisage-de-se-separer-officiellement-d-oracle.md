---
site: LeMagIT
title: "Hudson : la communauté envisage de se séparer officiellement d’Oracle "
author: Cyrille Chausson
date: 2011-01-12
href: http://www.lemagit.fr/article/oracle-developpement-opensource/7880/1/hudson-communaute-envisage-separer-officiellement-oracle/
tags:
- Entreprise
- Internet
- Associations
---

> La communauté du projet d’intégration continue Open Source Hudson envisage de divorcer d’Oracle suite à un litige autour de la marque qu’Oracle a revendiqué avec autorité en décembre dernier. Hudson pourrait être rebaptisé Perkins et l’ensemble du projet basculer définitivement hors des serveurs d’Oracle. La communauté doit encore se décider.
