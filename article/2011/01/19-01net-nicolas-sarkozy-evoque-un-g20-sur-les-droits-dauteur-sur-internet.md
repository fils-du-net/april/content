---
site: 01net.
title: "Nicolas Sarkozy évoque un G20 sur les droits d'auteur sur Internet"
author: Guillaume Deleurence
date: 2011-01-19
href: http://www.01net.com/editorial/527163/nicolas-sarkozy-souhaite-un-g20-sur-les-droits-dauteur-sur-internet/
tags:
- Internet
- HADOPI
- Institutions
- Droit d'auteur
- Éducation
- Europe
- International
---

> Lors de ses vœux au monde de la Culture et de l'Education, le chef de l'Etat a émis le souhait de réunir les Vingt sur la question des droits d'auteur avant le mois de novembre.
