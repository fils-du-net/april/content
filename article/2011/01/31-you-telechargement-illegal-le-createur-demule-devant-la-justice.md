---
site: YOU
title: "Téléchargement illégal : le créateur d'eMule devant la justice"
author: emilie
date: 2011-01-31
href: http://you.leparisien.fr/actu/2011/01/31/telechargement-illegal-le-createur-d-emule-devant-la-justice-6287.html
tags:
- Entreprise
- Internet
- Institutions
- Associations
- DADVSI
- Droit d'auteur
---

> Après deux reports l’an dernier, le procès du créateur d'eMule, une plateforme de téléchargement, débute aujourd’hui à Paris. Les maisons de production vont-elles avoir la tête d'eMule ?
