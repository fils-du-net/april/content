---
site: Commentçamarche.net
title: "La SNCF inaugure Drupal 7 !"
author: La rédaction
date: 2011-01-05
href: http://www.commentcamarche.net/news/5853849-la-sncf-inaugure-drupal-7
tags:
- Entreprise
- Internet
- Logiciels privateurs
---

> Le site de l'agence de voyage en ligne Voyages-sncf.com devrait peu à peu migrer vers la plateforme libre Drupal.
