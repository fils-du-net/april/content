---
site: ITCHANNEL.info
title: "Annulation d'un marché public hostile aux logiciels libres"
author: La rédaction
date: 2011-01-10
href: http://www.itchannel.info/index.php/articles/113794/annulation-marche-public-hostile-logiciels-libres.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- April
- Associations
---

> Le Tribunal Administratif de Lille a ordonné le 29 décembre 2010 l'annulation du marché d'acquisition d'un progiciel de gestion budgétaire, comptable et financière lancé le 28 septembre dernier par un groupement d'établissements publics
