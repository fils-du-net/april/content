---
site: PC INpact
title: "Une proposition de loi veut imposer un DRM sur l'information"
author: Marc Rees
date: 2011-01-24
href: http://www.pcinpact.com/actu/news/61520-wikileaks-leak-information-economique-sanction.htm
tags:
- Économie
- Institutions
- DRM
---

> Le député UMP Bernard Carayon et plusieurs de ses collègues ont déposé une proposition de loi visant à sanctionner la révélation « des informations économiques ». Le texte est rédigé en des termes si vastes qu’il permettra de sanctionner à peu près n’importe quel « leak » de données touchant aux informations juridiques, financières, commerciales, scientifiques, techniques, économiques ou industrielles. Et donc... informatiques.
