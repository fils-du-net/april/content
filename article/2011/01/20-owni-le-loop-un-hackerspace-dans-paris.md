---
site: OWNI
title: "Le LOOP : un Hackerspace dans Paris"
author: Jessica Chekroun
date: 2011-01-20
href: http://owni.fr/2011/01/20/le-pool-un-hackerspace-dans-paris/
tags:
- Partage du savoir
- Associations
- Innovation
---

> Rencontre avec Guyzmo, hacker reconnu en France, dans un tout nouveau hackerspace, en plein centre de Paris.
