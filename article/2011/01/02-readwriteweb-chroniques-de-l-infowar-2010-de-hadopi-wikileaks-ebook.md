---
site: ReadWriteWeb
title: "Chroniques de l’infowar – 2010, de Hadopi à Wikileaks #ebook"
author: Fabrice Epelboin
date: 2011-01-02
href: http://fr.readwriteweb.com/2011/01/02/a-la-une/chroniques-de-linfowar-2010-de-hadopi-wikileaks-ebook/
tags:
- Internet
- HADOPI
- Institutions
- DADVSI
- Neutralité du Net
---

> Il est temps d’ouvrir les yeux, nous vivons, comme Wikileaks nous l’a révélé de façon flagrante, une véritable infowar, de plus en plus appréhendée comme une guerre civile, ou une guerilla, que certains spécialistes n’hésitent pas a appeler ‘guerilla open source‘. Une infowar où les armes sont l’information et les milles et une techniques pour la diffuser. Ce n’est pas (sans pour autant l’exclure) une cyberwar, c’est tout autre chose : là où la cyberwar est un sport d’élite, l’infowar, elle, est en passe de devenir une activité populaire.
