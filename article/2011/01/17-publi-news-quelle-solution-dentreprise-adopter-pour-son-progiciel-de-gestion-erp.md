---
site: PUBLI NEWS
title: "Quelle solution d'entreprise adopter pour son progiciel de gestion ERP ?"
author: La rédaction
date: 2011-01-17
href: http://www.publi-news.fr/data/17012011/17012011-092350.html
tags:
- Entreprise
- Logiciels privateurs
- Philosophie GNU
---

> L'apport des systèmes d'information à la compétitivité des entreprises est de plus en plus visible. Les modèles économiques et de développement du logiciel libre ont pu apporter une solution adéquate à ces problèmes non résolus de coûts et qualité venant de ce type de logiciel, selon idnetinfo.
