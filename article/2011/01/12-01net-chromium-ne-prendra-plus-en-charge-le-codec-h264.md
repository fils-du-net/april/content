---
site: 01net.
title: "Chromium ne prendra plus en charge le codec H.264"
author: Eric le Bourlout
date: 2011-01-12
href: http://www.01net.com/editorial/526686/google-bannit-le-codec-h-264-de-chromium/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Standards
- Video
---

> La version open source du navigateur Chrome n'intégrera plus le codec vidéo H.264. Une manière pour Google de privilégier sa technologie maison, le WebM.
