---
site: Silicon.fr
title: "Les applications GPL interdites d’App Store ?"
author: David Feugey
date: 2011-01-09
href: http://www.silicon.fr/les-applications-gpl-interdites-d%E2%80%99app-store-43689.html
tags:
- Entreprise
- Logiciels privateurs
- Licenses
---

> Les logiciels libres sous licence GPL peuvent ou ne peuvent-ils pas être présents sur l’App Store? Pour GNU Go et VLC media player, la polémique s’est soldée par un retrait de la boutique en ligne d’Apple.
