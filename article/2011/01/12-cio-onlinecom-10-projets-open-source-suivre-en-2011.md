---
site: "cio-online.com"
title: "10 projets Open Source à suivre en 2011"
author: Jean Elyan
date: 2011-01-12
href: http://www.cio-online.com/actualites/lire-10-projets-open-source-a-suivre-en-2011-3410.html
tags:
- Entreprise
- Internet
- Innovation
- Licenses
---

> Quand on regarde la production Open Source, on est toujours étonné par la diversité des de développements en cours. Black Duck Software propose une liste de projets à surveiller en 2011.
