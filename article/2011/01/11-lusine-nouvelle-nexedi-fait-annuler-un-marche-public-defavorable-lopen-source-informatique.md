---
site: L'USINE NOUVELLE
title: "Nexedi fait annuler un marché public défavorable à l'open source - Informatique"
author: Christophe Dutheil
date: 2011-01-11
href: http://www.usinenouvelle.com/article/nexedi-fait-annuler-un-marche-public-defavorable-a-l-open-source.N144485
tags:
- Entreprise
- Logiciels privateurs
- Administration
- April
- Institutions
---

> Le tribunal administratif de Lille vient d'ordonner l'annulation d'un appel d'offres lancé le 28 septembre 2010. Motif : cet appel imposait de s'appuyer sur des logiciels propriétaires d'Oracle et de BusinessObjects (SAP). De quoi inciter les acheteurs publics à tirer un trait sur certaines pratiques régulièrement contestées.
