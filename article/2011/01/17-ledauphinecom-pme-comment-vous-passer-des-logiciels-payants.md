---
site: ledauphine.com
title: "PME : comment vous passer des logiciels payants"
author: La rédaction
date: 2011-01-17
href: http://www.ledauphine.com/economie-et-finance/2011/01/17/pme-comment-vous-passer-des-logiciels-payants
tags:
- Entreprise
- Logiciels privateurs
- Marchés publics
---

> Gratuits, légals, faciles d'accès et performants, les logiciels libres permettent aux PME de réaliser des économies non négligeables. Les explications de Renaud Cornu-Emieux, directeur de l’Ecole de management des systèmes d’information de Grenoble (EMSI).
