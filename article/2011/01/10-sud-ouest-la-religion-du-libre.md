---
site: SUD OUEST
title: "La religion du libre"
author: Matthieu Jarry
date: 2011-01-10
href: http://www.sudouest.fr/2011/01/10/la-religion-du-libre-285906-3034.php
tags:
- Logiciels privateurs
- Associations
- Promotion
---

> Le logiciel libre attire des utilisateurs de tous niveaux, qui forment une communauté de fervents adeptes.
