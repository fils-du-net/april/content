---
site: L'INFORMATICIEN
title: "Le lecteur vidéo VLC retiré de l’AppStore, à qui la faute ?"
author: Johanna Godet
date: 2011-01-10
href: http://www.linformaticien.com/Actualit%C3%A9s/tabid/58/newsid496/10027/vlc-retire-de-l-appstore-a-qui-la-faute/Default.aspx
tags:
- Entreprise
- Logiciels privateurs
- Licenses
---

> VLC vient d'être évincé de l’AppStore. L’un des auteurs de l’application et fervent défenseur du libre, Rémi Denis-Courmont, s’était opposé à la mise en ligne de son produit sur l’AppStore. Qui est vraiment en cause, Apple pour manque de souplesse ou le concepteur de VLC ?
