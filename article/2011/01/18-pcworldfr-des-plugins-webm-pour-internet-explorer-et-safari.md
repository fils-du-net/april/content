---
site: PCWorld.fr
title: "Des plugins WebM pour Internet Explorer et Safari"
author: Mathieu CHARTIER
date: 2011-01-18
href: http://www.pcworld.fr/2011/01/18/internet/des-plugins-webm-internet-explorer-safari/510741/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Institutions
- Video
- Europe
---

> La guerre des codecs autour de la balise vidéo du HTML 5 continue. Le H.264 aura la peau dure, mais Google ne désespère pas d'imposer son codec libre : WebM.
