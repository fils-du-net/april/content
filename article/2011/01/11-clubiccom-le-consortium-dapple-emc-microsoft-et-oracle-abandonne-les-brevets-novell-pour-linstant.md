---
site: clubic.com
title: "Le consortium d'Apple, EMC, Microsoft et Oracle abandonne les brevets Novell... pour l'instant"
author: Antoine Duvauchelle
date: 2011-01-11
href: http://pro.clubic.com/entreprises/novell/actualite-390216-consortium-apple-emc-microsoft-oracle-perd-brevets-novell.html
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- International
---

> L'intervention de différents défenseurs de l'open-source a-t-elle poussé Microsoft et consorts à prendre cette décision ? CPTN, une holding montée par Apple, EMC, Oracle et Microsoft, et dirigée par ce dernier, en est en tous cas pour ses frais : les brevets appartenant à Novell ne lui reviendront pas - pour l'instant.
