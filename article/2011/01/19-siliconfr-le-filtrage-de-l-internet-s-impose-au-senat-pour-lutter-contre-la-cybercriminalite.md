---
site: Silicon.fr
title: "Le filtrage de l’Internet s’impose au Sénat pour lutter contre la cybercriminalité"
author: La Rédaction
date: 2011-01-19
href: http://www.silicon.fr/le-filtrage-de-linternet-simpose-au-senat-pour-lutter-contre-la-cybercriminalite-43903.html
tags:
- Internet
- April
- Institutions
---

> Au nom de la lutte contre la pédo-pornographie en ligne, le Sénat instaure le blocage des sites en dehors de tout cadre légal.
