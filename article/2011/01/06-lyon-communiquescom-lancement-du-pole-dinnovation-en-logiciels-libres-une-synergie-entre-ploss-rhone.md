---
site: "Lyon-Communiques.com"
title: "Lancement du Pôle d'Innovation en Logiciels Libres, une synergie entre Ploss Rhône-Alpes et la Mairie de Tarare."
author: La rédaction
date: 2011-01-06
href: http://www.lyon-communiques.com/communiques/lancement-du-pole-d-innovation-en-logiciels-libres-une-synergie-entre-c60758.htm
tags:
- Entreprise
- Internet
- Administration
- Économie
- Associations
- Philosophie GNU
---

> Communiqué de presse sur le lancement de Pôle d'Innovation en Logiciel Libre à Tarare. L'ambition d'un tournant économique.
