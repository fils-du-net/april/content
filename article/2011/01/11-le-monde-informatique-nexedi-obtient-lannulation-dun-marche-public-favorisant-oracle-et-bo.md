---
site: Le Monde Informatique
title: "Nexedi obtient l'annulation d'un marché public favorisant Oracle et BO"
author: Bertand Lemaire
date: 2011-01-11
href: http://www.lemondeinformatique.fr/actualites/lire-nexedi-obtient-l-annulation-d-un-marche-public-favorisant-oracle-et-bo-32587.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
---

> Les marchés publics doivent permettre une concurrence parfaite entre fournisseurs pour couvrir un besoin. Exiger la solution d'un éditeur précis est illégal et le tribunal administratif de Lille vient de le rappeler en annulant un marché public favorisant Oracle et Business Object.
