---
site: Mediapart
title: "La Bonne Mère de Philippe Sion"
author: POJ
date: 2011-01-07
href: http://www.mediapart.fr/club/blog/poj/070111/la-bonne-mere-de-philippe-sion
tags:
- Administration
- April
- Europe
---

> Le rapport public de 2003 du Conseil d'Etat intitulé "Perspectives pour la fonction publique" commence par cette phrase : "La France s’est préoccupée très tôt de se doter d’une fonction publique moderne, c’est-à-dire bénéficiant d’un “état” opposable au pouvoir politique, pour la faire échapper au favoritisme et à l’arbitraire."
