---
site: LeMagIT
title: "Broadcom rejoint la Linux Foundation"
author: Cyrille Chausson
date: 2011-01-11
href: http://www.lemagit.fr/article/mobilite-linux-wifi-broadcom-opensource/7870/1/broadcom-rejoint-linux-foundation/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Associations
---

> Promet de faire plus pour publier le code source des pilotes de ses solutions de communication sans-fil (2G, 3G, Wi-Fi...)
