---
site: Numerama
title: "Egypte : Internet coupé, des solutions improvisées"
author: Julien L.
date: 2011-01-28
href: http://www.numerama.com/magazine/17919-egypte-internet-coupe-des-solutions-improvisees.html
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Neutralité du Net
- International
---

> Le gouvernement a coupé la majorité des accès au réseau, dans l'espoir d'étouffer la contestation populaire. Mais les manifestants ne baissent pas les bras, et peuvent compter sur le concours d'hactivistes étrangers, et notamment l'opérateur associatif français FDN.
