---
site: LeMonde.fr
title: "Le site Internet de la SNCF passe à la plate-forme libre Drupal"
author: La rédaction
date: 2011-01-04
href: http://www.lemonde.fr/technologies/article/2011/01/03/le-site-de-la-sncf-passe-a-drupal_1460465_651865.html
tags:
- Entreprise
- Internet
---

> L'opérateur utilisera la plate-forme libre pour la gestion des contenus sur son site principal.
