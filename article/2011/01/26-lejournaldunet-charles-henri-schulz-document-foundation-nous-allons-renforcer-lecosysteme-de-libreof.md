---
site: LeJournalduNet
title: "Charles-Henri Schulz (The Document Foundation) \"Nous allons renforcer l'écosystème de LibreOffice\""
author: Dominique FILIPPONE
date: 2011-01-26
href: http://www.journaldunet.com/solutions/intranet-extranet/charles-henri-schulz-libreoffice-fork-openoffice-the-document-foundation.shtml
tags:
- Entreprise
- Logiciels privateurs
- Interopérabilité
- Associations
- Innovation
- Licenses
---

> La suite bureautique Open Source gonfle les muscles dans sa version finale 3.3. L'essor des certifications autour de LibreOffice pourrait bien intéresser les entreprises.
