---
site: OWNI
title: "Internet, ADN : “Nous vivons un nouvel âge d’or de la piraterie”"
author: Sabine Blanc
date: 2011-01-03
href: http://owni.fr/2011/01/03/internet-adn-nous-vivons-un-nouvel-age-dor-de-la-piraterie/
tags:
- Internet
- Institutions
- Brevets logiciels
- Droit d'auteur
---

> Entretien avec Jean-Philippe Vergne, auteur avec Rodolphe Durand d'un essai sur l'histoire de la piraterie. Dans leur ouvrage vivifiant, les deux chercheurs articulent leur réflexion autour du développement du capitalisme, en optant pour une grille de lecture transdisciplinaire.
