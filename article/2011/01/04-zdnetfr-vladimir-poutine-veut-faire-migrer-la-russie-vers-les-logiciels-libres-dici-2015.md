---
site: ZDNet.fr
title: "Vladimir Poutine veut faire migrer la Russie vers les logiciels libres d'ici 2015"
author: Thierry Noisette
date: 2011-01-04
href: http://www.zdnet.fr/blogs/l-esprit-libre/vladimir-poutine-veut-faire-migrer-la-russie-vers-les-logiciels-libres-d-ici-2015-39757229.htm
tags:
- Administration
- Institutions
- International
---

> L'intérêt de la Russie pour le développement des logiciels libres semble se confirmer, avec un plan de migration des administrations en 25 points signé par le Premier ministre Vladimir Poutine en personne.
