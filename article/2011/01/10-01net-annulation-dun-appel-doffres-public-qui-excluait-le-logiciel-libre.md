---
site: 01net.
title: "Annulation d'un appel d'offres public qui excluait le logiciel libre"
author: Guillaume Deleurence
date: 2011-01-10
href: http://www.01net.com/editorial/526603/annulation-dun-marche-public-qui-excluait-les-logiciels-libres/
tags:
- Entreprise
- Logiciels privateurs
- Administration
- April
---

> La société éditrice d'ERP5, Nexedi, s'estimant exclue d'une procédure de passation de marché public pour un progiciel de gestion, a obtenu gain de cause en justice.
