---
site: PCWorld.fr
title: "Codecs vidéo : Microsoft se moque de Google"
author: Mathieu CHARTIER
date: 2011-01-14
href: http://www.pcworld.fr/2011/01/14/internet/codecs-video-microsoft-moque-google/510617/
tags:
- Entreprise
- Internet
- Désinformation
- Standards
- Video
---

> Suite à la décision de Google de bouder le H.264, Microsoft réplique en faisant de l'humour. Rire jaune...
