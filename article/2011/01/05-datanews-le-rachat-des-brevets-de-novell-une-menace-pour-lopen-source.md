---
site: datanews
title: "\"Le rachat des brevets de Novell, une menace pour l'open source\""
author: Stefan Grommen
date: 2011-01-05
href: http://datanews.rnews.be/fr/ict/actualite/apercu/2011/01/05/le-rachat-des-brevets-de-novell-une-menace-pour-l-open-source/article-1194913889384.htm
tags:
- Institutions
- Associations
- Brevets logiciels
- International
---

> Le rachat de centaines de brevets de Novell par un groupe dirigé par Microsoft est considéré comme inquiétante par l’Open Source Initiative (OSI).
