---
site: LeMagIT
title: "Brevets Novell : Déçus par l'Europe l’OSI et FSF se tournent vers la Justice américaine"
author: Cyrille Chausson
date: 2011-01-20
href: http://www.lemagit.fr/article/novell-brevets-open-source-attachmate/7940/1/brevets-novell-decus-par-europe-osi-fsf-tournent-vers-justice-americaine/
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- Europe
- International
---

> Bruxelles n’ayant pas l’intention d’intervenir dans la revente des brevets Novell au mystérieux consortium CPTN Holdings, et face à la menace que cela représenterait pour l’Open Source, l’OSI et le FSF ont décidé de collaborer afin de porter l’affaire auprès du département de Justice américain.
