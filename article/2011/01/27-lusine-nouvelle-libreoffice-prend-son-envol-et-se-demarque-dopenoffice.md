---
site: L'USINE NOUVELLE
title: "LibreOffice prend son envol et se démarque d'OpenOffice"
author: Christophe Dutheil
date: 2011-01-27
href: http://www.usinenouvelle.com/article/libreoffice-prend-son-envol-et-se-demarque-d-openoffice.N145583
tags:
- Entreprise
- Internet
- Interopérabilité
- Associations
---

> La nouvelle Document Foundation vient de lancer la première version, dite stable, de LibreOffice 3.3. Il s'agit d'un dérivé de la célèbre suite de bureautique en open source OpenOffice.org, désormais chapeautée par Oracle.
