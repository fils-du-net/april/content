---
site: ZDNet.fr
title: "LibreOffice pourrait remplacer OpenOffice dans Ubuntu"
author: Christophe Auffray
date: 2011-01-11
href: http://www.zdnet.fr/actualites/libreoffice-pourrait-remplacer-openoffice-dans-ubuntu-39757390.htm
tags:
- Entreprise
- Interopérabilité
- Standards
---

> Remplacer OpenOffice d'Oracle par son fork indépendant LibreOffice : le fondateur de Canonical, Mark Shuttleworth, y est favorable. La décision sera tranchée cette semaine par la communauté Ubuntu. Cela permettrait une intégration dès la version Ubuntu 11.04
