---
site: linuxfr.org
title: "Le Wikileaks bucco-rhodanien revient sur la migration marseillaise vers Windows"
author: Luc Stepniewski
date: 2011-01-07
href: https://linuxfr.org/2011/01/07/27747.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Administration
- April
- Partage du savoir
- Associations
---

> Le site Wikileaks13 (rien à voir avec le Wikileaks de Julian Assange) a publié des détails croustillants sur le revirement de la migration de Marseille qui devait se faire sur du Linux/OpenOffice, et qui s'est finalement fait sur Windows 7.
