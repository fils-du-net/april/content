---
site: PC INpact
title: "VLC finalement retiré de l'App Store sans explication officielle"
author: Vincent Hermann
date: 2011-01-10
href: http://www.pcinpact.com/actu/news/61266-vlc-ios-retrait-app-store-remi-deniscourmont-gpl.htm
tags:
- Entreprise
- Associations
- DRM
- Licenses
- Video
---

> Dans le monde de l’open source, VLC fait partie des ténors connus par une bonne partie du grand public. Célèbre pour sa capacité à lire tous les formats audio et vidéo ou presque, il avait bénéficié voilà plusieurs mois d’un portage vers iOS. Seulement voilà, après la tourmente légale au sujet de la compatibilité de sa licence avec l’App Store, il a finalement été retiré de cette boutique.
