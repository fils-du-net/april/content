---
site: DIRECTION INFORMATIQUE
title: "Logiciel libre et organisation"
author: Gérard Blanc
date: 2011-01-05
href: http://www.directioninformatique.com/DI/client/fr/DirectionInformatique/Nouvelles.asp?id=60757
tags:
- Entreprise
---

> C'est maintenant un lieu commun, d'évoquer l'amour et la haine que se portent les organisations et le logiciel libre. Le logiciel libre s'évertue à vanter ses qualités techniques et le côté économique de son absence de licences payantes. Alors qu'il fait face à une méfiance presque viscérale des organisations envers son modèle de distribution, même si l'application concernée semble techniquement robuste, et couvre adéquatement le besoin d'affaires.
