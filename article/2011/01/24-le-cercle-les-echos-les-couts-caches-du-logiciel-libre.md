---
site: LE CERCLE Les Echos
title: "Les coûts cachés du logiciel libre"
author: PierreThurau
date: 2011-01-24
href: http://lecercle.lesechos.fr/node/32983
tags:
- Entreprise
- Logiciels privateurs
- Désinformation
---

> Aujourd’hui, de nombreuses PME françaises décident d’adopter le logiciel libre dans leur environnement de production. Le principal facteur d’adoption de ces logiciels est le tarif de ces solutions, très souvent largement inférieur aux logiciels propriétaires. D’un point de vue strictement rationnel, ces entreprises en raison d’adopter ce type de programmes. Cependant, ces logiciels peuvent souvent se révéler très onéreux.
