---
site: vousnousils
title: "\"Plus de 50% des professeurs de maths utilisent Sésamath\""
author: Sandra Ktourza
date: 2011-01-14
href: http://www.vousnousils.fr/2011/01/14/plus-de-50-des-professeurs-de-maths-utilisent-sesamath-478783
tags:
- Éducation
- Licenses
---

> Sésamath ? Le sésame des maths ? L'association de professeurs de mathématiques qui propose aux enseignants, aux élèves et aux parents, des ressources en ligne de qualité et gratuites, rencontre en tous cas un franc succès. Sébastien Hache, un de ses fondateurs, nous la présente.
