---
site: Developpez.com
title: "Interview avec le créateur du projet open-source Jappix, un lycéen \"convaincu des logiciels libres\" "
author: Katleen Erna
date: 2011-01-26
href: http://www.developpez.com/actu/27733/Interview-avec-le-createur-du-projet-open-source-Jappix-un-lyceen-convaincu-des-logiciels-libres/
tags:
- Entreprise
- Internet
- Éducation
- Innovation
---

> J'ai eu la chance de pouvoir m'entretenir la semaine dernière avec Valérian, alias Vanaryon, jeune créateur du projet open-source Jappix. Découvrez tous les détails de son travail dans cet échange passionnant. Le jeune homme est étonnant par sa grande maîtrise du développement, et des autres domaines touchant à sa plate-forme XMPP. Pourtant, il avait entamé cette réalisation il y a un peu plus d'un an en étant novice sur de nombreux langages de programmation.
