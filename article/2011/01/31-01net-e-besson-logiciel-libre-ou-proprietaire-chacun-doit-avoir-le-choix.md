---
site: 01net.
title: "E. Besson : « Logiciel libre ou propriétaire, chacun doit avoir le choix »"
author: Propos recueillis par Christophe Guillemin
date: 2011-01-31
href: http://www.01net.com/editorial/527607/e-besson-logiciel-libre-ou-proprietaire-chacun-doit-avoir-le-choix/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Économie
- Institutions
- Vente liée
- Associations
- Innovation
- Europe
---

> Le ministre de l'Economie numérique prend position dans le dossier de la vente liée. Il revient également sur la création du Conseil national du numérique.
