---
site: ITespresso.fr
title: "Pierre Matuchet (Voyages-SNCF.com) : « 255 millions de billets vendus depuis la création du site »"
author: Philippe Guerrier
date: 2011-01-03
href: http://www.itespresso.fr/pierre-matuchet-voyages-sncf-com-255-millions-de-billets-vendus-depuis-la-creation-du-site-39847.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
---

> Voyages-SNCF.com a fêté ses dix ans en 2010. Retour sur l'essor d'un service devenu leader du commerce électronique et une référence dans l'exploitation des solutions open source.
