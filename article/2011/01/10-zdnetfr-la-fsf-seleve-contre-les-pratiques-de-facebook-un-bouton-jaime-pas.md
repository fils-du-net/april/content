---
site: ZDNet.fr
title: "La FSF s'élève contre les pratiques de Facebook via un bouton « J'aime pas »"
author: La rédaction
date: 2011-01-10
href: http://www.zdnet.fr/actualites/la-fsf-s-eleve-contre-les-pratiques-de-facebook-via-un-bouton-j-aime-pas-39757354.htm
tags:
- Entreprise
- Internet
- Associations
---

> L’association emblématique du logiciel libre s’emporte contre l’élection par le Time Magazine de Mark Zuckerberg en tant que personnalité de l’année. La FSF appelle les internautes à protester contre le modèle Facebook en affichant sur leurs pages Web des boutons « J’aime pas ».
