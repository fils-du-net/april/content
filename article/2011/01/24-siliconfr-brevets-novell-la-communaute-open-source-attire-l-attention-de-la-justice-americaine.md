---
site: Silicon.fr
title: "Brevets Novell: la communauté open source attire l’attention de la justice américaine"
author: Christophe Lagane
date: 2011-01-24
href: http://www.silicon.fr/brevets-novell-la-communaute-open-source-attire-lattention-de-la-justice-americaine-43973.html
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- Europe
- International
---

> Que vont faire Microsoft et Oracle, notamment, des brevets de Novell revendu par Attachmate? A la demande de l'OSI et de la FSF, le ministère américain de la Justice accepte de se pencher sur la question.
