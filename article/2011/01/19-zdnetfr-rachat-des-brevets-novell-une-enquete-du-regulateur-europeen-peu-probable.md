---
site: ZDNet.fr
title: "Rachat des brevets Novell : une enquête du régulateur européen peu probable"
author: La rédaction
date: 2011-01-19
href: http://www.zdnet.fr/actualites/rachat-des-brevets-novell-une-enquete-du-regulateur-europeen-peu-probable-39757607.htm
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- Europe
---

> Selon la Commission européenne, aucune enquête ne devrait être ouverte sur la vente de 882 brevets de Novell à la holding CPTN composée de Microsoft, Oracle, EMC et Apple. Dans un document officiel, Novell indique que ces brevets portent sur la sécurité, l'administration et la gestion des identités.
