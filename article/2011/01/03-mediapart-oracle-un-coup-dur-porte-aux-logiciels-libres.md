---
site: Mediapart
title: "Oracle, un coup dur porté aux logiciels libres"
author: qdemouliere
date: 2011-01-03
href: http://www.mediapart.fr/club/blog/qdemouliere/030111/oracle-un-coup-dur-porte-aux-logiciels-libres
tags:
- Entreprise
- Logiciels privateurs
---

> Cela fait plusieurs semaines que le Géant des bases de données Oracle a racheté la société Sun Microsystem. Alors qu'un doute subsisté quand au possible monopole du marché des bases de données suite à ce rachat. L'Union européenne a donné son aval prétextant qu'il existerait encore une saine concurrence grâce SGBD PostreSQL.
