---
site: clubic.com
title: "Codec vidéo : la communauté du logiciel libre soutient Google"
author: Guillaume Belfiore
date: 2011-01-20
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/logiciel-libre-open-source/actualite-392048-codec-video-communaute-logiciel-libre-soutient-google.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Associations
- Brevets logiciels
- Innovation
- Video
---

> La semaine dernière Google annoncait via l'un de ses blogs vouloir abandonner le support du codec video H.264 afin de pousser davantage WebM, sa propre solution publiée en open source contenant les codecs VP8 pour la vidéo et Vorbis pour l'audio. Si cette décision a suscité de nombreuses réactions, la Free Software Foundation, chargée de promouvoir et de défendre le logiciel libre, a manifesté son soutien au géant du web.
