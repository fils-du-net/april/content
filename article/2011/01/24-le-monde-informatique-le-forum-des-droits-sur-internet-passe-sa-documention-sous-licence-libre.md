---
site: Le Monde Informatique
title: "Le Forum des Droits sur Internet passe sa documention sous licence libre"
author: Bertrand Lemaire
date: 2011-01-24
href: http://www.lemondeinformatique.fr/actualites/lire-le-forum-des-droits-sur-internet-passe-sa-documention-sous-licence-libre-32709.html
tags:
- Internet
- April
- Partage du savoir
- Institutions
- Licenses
- Contenus libres
---

> Tout le travail du FDI (Forum des Droits sur Internet), payé sur fonds publics, profitera à tous gratuitement.
