---
site: ZDNet.fr
title: "Ubuntu : c'est décidé, LibreOffice remplacera OpenOffice"
author: La rédaction
date: 2011-01-24
href: http://www.zdnet.fr/actualites/ubuntu-c-est-decide-libreoffice-remplacera-openoffice-39757704.htm
tags:
- Entreprise
---

> Ubuntu : c'est décidé, LibreOffice remplacera OpenOffice - Après Fedora et OpenSuSe, une autre distribution majeure, Ubuntu, intègrera nativement LibreOffice. L'adoption du fork d'OpenOffice est un moyen de dénoncer l'attitude d'Oracle à l'égard du logiciel libre et en particulier son contrôle sur l'outil bureautique.
