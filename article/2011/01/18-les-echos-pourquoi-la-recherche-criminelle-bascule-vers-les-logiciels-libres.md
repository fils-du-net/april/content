---
site: Les Echos
title: "Pourquoi la recherche criminelle bascule vers les logiciels libres"
author: Romain Gueugneau
date: 2011-01-18
href: http://www.lesechos.fr/journal20110118/lec2_high_tech_et_medias/0201078816600-pourquoi-la-recherche-criminelle-bascule-vers-les-logiciels-libres.htm
tags:
- Entreprise
- Internet
- Administration
- Interopérabilité
- Innovation
---

> Après l'adoption du navigateur Firefox de Mozilla et le passage sous environnement Linux, la Gendarmerie nationale fait évoluer le système d'information de son laboratoire de recherche criminelle vers l'« open source ».
