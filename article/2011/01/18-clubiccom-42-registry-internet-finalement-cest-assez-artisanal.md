---
site: clubic.com
title: "42 Registry : \"Internet, finalement, c'est assez artisanal\""
author: Antoine Duvauchelle
date: 2011-01-18
href: http://pro.clubic.com/it-business/nom-de-domaine/actualite-391244-42-registry-internet-artisanal.html
tags:
- Entreprise
- Internet
- Associations
- Innovation
---

> En dehors de l'Icann, point de salut pour l'Internet ? Une expérience française semble vouloir contredire cette généralité, avec un nouveau TLD (nom de domaine de premier niveau... soit les .com, .net et autres .fr). Baptisé .42, il pose plusieurs problèmes techniques et intellectuels, et enflamme le web technophile.
