---
site: ITRmanager.com
title: "Entre Windows et Open Source, le cœur de la Commission ne balance pas"
author: La rédaction
date: 2011-01-27
href: http://www.itrmanager.com/articles/114546/entre-windows-open-source-ur-commission-balance-pas.html
tags:
- Logiciels privateurs
- Interopérabilité
- Institutions
- Désinformation
- Marchés publics
- Europe
---

> « Faites ce que je dis et pas ce que je fais », telle semblerait être la maxime de la Commission européenne si l’on en croit un article publié par le New York Times (European Commission of Two Minds on Software Purchases?). D’un côté, la Commission se fait l’ardent défenseur des standards ouverts et de l’open source et, de l’autre, elle aurait acté la migration de quelque 36 000 postes de travail de Windows XP vers Windows 7.
