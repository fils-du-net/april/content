---
site: Numerama
title: "L'Hadopi dans l'illégalité parce qu'elle privilégie... le logiciel libre ?"
author: Guillaume Champeau
date: 2011-01-10
href: http://www.numerama.com/magazine/17790-l-hadopi-dans-l-illegalite-parce-qu-elle-privilegie-le-logiciel-libre-maj.html
tags:
- Internet
- Administration
- April
- Droit d'auteur
- Licenses
---

> Perçue comme un adversaire du partage des savoirs sur Internet, l'Hadopi a demandé dans un appel d'offres que sa future plateforme de Labs soit basée sur une solution open-source. Une initiative louable qui pourrait pourtant, théoriquement, lui valoir annulation.
