---
site: 01net.
title: "La Free Software Foundation « n'aime pas » Facebook"
author: Guillaume Deleurence
date: 2011-01-11
href: http://www.01net.com/editorial/526646/la-free-software-foundation-naime-pas-facebook/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Associations
---

> L'organisation de défense du logiciel libre de Richard Stallman propose d'installer sur les blogs et les sites des bannières et boutons qui marquent la défiance envers le numéro un des réseaux sociaux.
