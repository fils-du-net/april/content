---
site: LeMagIT
title: "La justice casse un appel d'offres public faisant référence à Oracle et BO "
author: Reynald Fléchaux
date: 2011-01-10
href: http://www.lemagit.fr/article/justice-nexedi-erp-marches-publics-open-source-secteur-public-appel-offres/7863/1/la-justice-casse-appel-offres-public-faisant-reference-oracle-bo/
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Institutions
---

> Vieux combat de l'Open Source, les marchés publics faisant explicitement référence à telle ou telle marque - pratique très répandue bien qu'interdite - subissent une première défaite sur le terrain judiciaire. L'éditeur Open Source Nexedi vient d'obtenir du tribunal administratif de Lille qu'un appel d'offres de ce type soit cassé.
