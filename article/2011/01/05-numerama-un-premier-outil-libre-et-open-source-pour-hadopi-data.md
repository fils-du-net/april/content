---
site: Numerama
title: "Un premier outil libre et open-source pour Hadopi-Data"
author: Guillaume Champeau
date: 2011-01-05
href: http://www.numerama.com/magazine/17766-un-premier-outil-libre-et-open-source-pour-hadopi-data.html
tags:
- Internet
- HADOPI
---

> Nous en avions rêvé, l'auteur du CMS Philum l'a fait. Lorsque nous avons mis en ligne le site Hadopi-Data.fr pour réunir les témoignages d'internautes ayant reçu un courrier de l'Hadopi, nous avions expliqué que toutes les données collectées étaient libres. "Nous proposons à ceux qui y trouvent intérêt de télécharger toute la base de données au format XML, pour présenter les données comme ils le souhaitent et réaliser des croisements statistiques", avions-nous écrit.
