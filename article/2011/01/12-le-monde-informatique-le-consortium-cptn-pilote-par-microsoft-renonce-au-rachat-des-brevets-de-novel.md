---
site: Le Monde Informatique
title: "Le consortium CPTN, piloté par Microsoft, renonce au rachat des brevets de Novell"
author: Jean Elyan
date: 2011-01-12
href: http://www.lemondeinformatique.fr/actualites/lire-le-consortium-cptn-pilote-par-microsoft-renonce-au-rachat-des-brevets-de-novell-32607.html
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Brevets logiciels
- International
---

> Un projet de consortium regroupant Microsoft, Apple, Oracle, et EMC pour racheter 882 brevets de Novell a été soumis au régulateur de la concurrence allemand. Cette demande  a été retirée par Microsoft, alors que les associations Open Source ont interpellé les autorités allemandes des risques sur les logiciels libres.
