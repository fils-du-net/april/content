---
site: 01net.
title: "Vente liée : encore une décision de justice cassée"
author: Guillaume Deleurence
date: 2011-01-31
href: http://www.01net.com/editorial/527602/vente-liee-une-nouvelle-decision-de-justice-cassee/
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
- Europe
---

> La Cour de cassation a annulé un autre jugement de première instance, où un consommateur n'avait pas obtenu le remboursement de logiciels non désirés. Motif : il n'a pas assez tenu compte des exigences d'une directive européenne.
