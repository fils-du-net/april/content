---
site: Never give up !
title: "Internet: la réponse est non, mais quelle est la question ?"
author: Jean-Michel Planche
date: 2011-01-28
href: http://www.jmp.net/2011/01/internet-la-reponse-est-non-mais-quelle-est-la-question/
tags:
- Internet
- April
- Institutions
- Associations
- International
---

> Hier deux réponses contradictoires étaient données à la question « peut-on éteindre l’Internet« . D’un coté les partisans du NON, dont je fais parti, lors d’une conférence, organisée par l’Epitech. De l’autre, malheureusement, la réponse qui semble avoir été donnée par l’Egypte.
