---
site: ITespresso.fr
title: "Open source : LibreOffice 3.3 se dévoile en version finale"
author: La rédaction
date: 2011-01-26
href: http://www.itespresso.fr/open-source-libreoffice-3-3-se-devoile-version-finale-40885.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Associations
---

> Sortie du giron d'Oracle, l'ex-communauté de développeurs d'OpenOffice.org, regroupée au sein de la Document Foundation, a finalisé sa suite bureautique LibreOffice 3.3.
