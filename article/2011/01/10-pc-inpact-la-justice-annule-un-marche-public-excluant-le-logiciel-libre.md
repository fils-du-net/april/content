---
site: PC INpact
title: "La justice annule un marché public excluant le logiciel libre"
author: Marc Rees
date: 2011-01-10
href: http://www.pcinpact.com/actu/news/61276-nexedi-oracle-marche-public-libre.htm
tags:
- Entreprise
- Logiciels privateurs
- Administration
- April
- Associations
---

> Nexedi, un éditeur français vient d’obtenir l’annulation d’un marché public dont la rédaction excluait par principe les solutions libres. Une décision rare qui s’appuie sur un article du code des marchés publics interdisant justement les pratiques trop exclusives.
