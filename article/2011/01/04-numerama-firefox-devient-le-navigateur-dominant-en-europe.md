---
site: Numerama
title: "Firefox devient le navigateur dominant en Europe"
author: Julien L.
date: 2011-01-04
href: http://www.numerama.com/magazine/17756-firefox-devient-le-navigateur-dominant-en-europe.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Standards
- Europe
---

> La suprématie d'Internet Explorer en Europe touche à sa fin. D'après les chiffres fournis par Statcounter, Firefox domine désormais le Vieux Continent, avec une part de marché de 38,11 % contre 37,52 %. Firefox a profité de l'écran de sélection, qui permet à un utilisateur sous Windows de choisir son navigateur web, mais aussi de la montée en puissance de Chrome, qui joue un véritable rôle d'arbitre.
