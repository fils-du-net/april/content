---
site: PC INpact
title: "Le décret sur le transfert des emails de l'Hadopi attaqué en justice"
author: Marc Rees
date: 2011-01-11
href: http://www.pcinpact.com/actu/news/61307-free-decret-hadopi-conseil-detat.htm
tags:
- Entreprise
- Internet
- April
- HADOPI
- Institutions
- Europe
---

> Exclusif : Selon nos informations, un fournisseur d'accès a déposé un nouveau recours devant le Conseil d’Etat le 10 décembre dernier contre un texte d'application de la loi Hadopi. Ce recours vise le décret du 12 octobre qui avait justement été publié pour forcer Free à transmettre aux abonnés les avertissements de l'Hadopi. Une information qui tombe alors que l'Hadopi organisera demain matin sa conférence de presse pour faire le point sur la riposte graduée.
