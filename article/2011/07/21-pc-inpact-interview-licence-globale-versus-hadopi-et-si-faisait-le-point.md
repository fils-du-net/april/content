---
site: PC INpact
title: "Licence Globale versus Hadopi et si on faisait le point"
author: Marc Rees
date: 2011-07-21
href: http://www.pcinpact.com/actu/news/64729-laurent-chemla-hadopi-licence-globale.htm
tags:
- Entreprise
- Internet
- Économie
- Associations
- Droit d'auteur
- Licenses
- Contenus libres
---

> Jérémie Nestel a interviewé sur Libre Acces Laurent Chemla. L'auteur des Confessions d'un voleur, fondateur de Gandi, revient sur la question de la licence globale et sur Hadopi.
