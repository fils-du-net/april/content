---
site: S.I.Lex
title: "Après le peintre Boronali, le photographe Simius ?"
author: calimaq
date: 2011-07-08
href: http://scinfolex.wordpress.com/2011/07/08/apres-le-peintre-boronali-le-photographe-simius/
tags:
- Droit d'auteur
- Licenses
---

> A qui appartiennent les droits sur une photo prise par… un singe ! Voilà une question incongrue comme je les aime, soulevée hier par le site Techdirt.
