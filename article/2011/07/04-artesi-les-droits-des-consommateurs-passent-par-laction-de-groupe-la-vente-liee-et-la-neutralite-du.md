---
site: artesi
title: "Les droits des consommateurs passent par l'action de groupe, la vente liée et la neutralité du net"
date: 2011-07-04
href: http://www.artesi.artesi-idf.com/public/article/les-droits-des-consommateurs-passent-par-l-action-de-groupe-la-vente-liee-et-la-neutralite-du-net.html?id=23735
tags:
- Entreprise
- Internet
- Économie
- April
- Institutions
- Vente liée
- Associations
- Neutralité du Net
---

> La commission des Affaires économiques de l'Assemblée nationale va débuter, mardi 5 juillet 2011, l'examen du projet de loi « renforçant les droits, la protection et l’information des consommateurs » [1]. Ce projet représente un retour en arrière inquiétant pour les libertés et pour les droits des utilisateurs, avec l'omission de l'action de groupe, qui était pourtant une promesse de Nicolas Sarkozy dès 2007, et avec une tentative très préoccupante de filtrage du net à l'initiative de la DGCCRF.
