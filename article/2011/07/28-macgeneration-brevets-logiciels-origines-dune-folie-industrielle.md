---
site: macgeneration
title: "Brevets logiciels : origines d'une folie industrielle"
author: Arnauld de La Grandière
date: 2011-07-28
href: http://www.macgeneration.com/unes/voir/129982/brevets-logiciels-origines-d-une-folie-industrielle
tags:
- Entreprise
- Économie
- Brevets logiciels
- International
---

> La chronique judiciaire laisse une place de plus en plus importante aux procédures intentées sur la base de la violation d'un ou plusieurs brevets dits "logiciels". On voit d'un assez mauvais œil ces brevets en Europe, où l'on considère que par essence, une idée ne devrait pas être brevetable : les brevets doivent se limiter à la protection d'une invention, en décrivant un moyen et non une fin, libre à chacun d'arriver au même résultat pour peu qu'il n'utilise pas le procédé physique tel que décrit et protégé par le brevet.
