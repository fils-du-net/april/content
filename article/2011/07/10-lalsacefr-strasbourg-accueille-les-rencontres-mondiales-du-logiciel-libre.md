---
site: L'ALSACE.fr
title: "Strasbourg accueille les rencontres mondiales du logiciel libre"
author: G.D-A.
date: 2011-07-10
href: http://www.lalsace.fr/actualite/2011/07/10/strasbourg-accueille-les-rencontres-mondiales-du-logiciel-libre
tags:
- Entreprise
- Sensibilisation
- Associations
- Philosophie GNU
- Promotion
---

> Les 12 es Rencontres mondiales du logiciel libre se déroulent à Strasbourg jusqu’au 14 juillet. Elles proposent au public, mais aussi aux passionnés, de venir découvrir l’univers des logiciels libres.
