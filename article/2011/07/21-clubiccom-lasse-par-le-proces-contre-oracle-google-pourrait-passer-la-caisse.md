---
site: clubic.com
title: "Lassé par le procès contre Oracle, Google pourrait passer à la caisse"
author: Antoine Duvauchelle
date: 2011-07-21
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/brevet-logiciel/actualite-436666-google-lasse-polemique-oracle.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
---

> Alors qu'il clamait jusqu'à maintenant son innocence et dénonçait l'outrecuidance des prétentions d'Oracle, Google met de l'eau dans son vin au sujet du procès qui les oppose sur la propriété intellectuelle de Java. Google a été jusqu'à proposer un « accord informel », qui fait dire à l'avocat et militant des logiciels libres Florian Muller que le géant serait prêt à passer à la caisse.
