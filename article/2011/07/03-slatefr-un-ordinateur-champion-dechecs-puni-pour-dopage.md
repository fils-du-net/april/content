---
site: Slate.fr
title: "Un ordinateur champion d'échecs puni pour dopage"
date: 2011-07-03
href: http://www.slate.fr/lien/40569/un-ordinateur-champion-d%C3%A9checs-puni-pour-dopage
tags:
- Droit d'auteur
- Licenses
- International
---

> Rybka, un champion d'échecs, vient d'être épinglé pour dopage. Rapportée par le site DailyTech, l'histoire est plutôt inhabituelle pour cette discipline, mais elle le devient encore plus quand on apprend que Rybka n'est pas un humain mais... un programme informatique, et qu'il s'est dopé en copiant le code source de ses «concurrents».
