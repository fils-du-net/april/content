---
site: NewZilla.net
title: "Enrichissez Microsoft, achetez Android !"
author: Renaud Jean
date: 2011-07-13
href: http://www.newzilla.net/2011/07/13/chronique-enrichissez-microsoft-achetez-android/
tags:
- Entreprise
- Institutions
- Brevets logiciels
---

> Aspirants entrepreneurs, vous qui traquez le modèle économique ultime, reconnaissez votre maître.  Microsoft l’a trouvé, ce modèle.
