---
site: OWNI
title: "Innover et réussir avec les licences Creative Commons"
author: Calimaq
date: 2011-07-11
href: http://owni.fr/2011/07/11/innover-et-reussir-avec-les-licences-creative-commons/
tags:
- Entreprise
- Internet
- Droit d'auteur
- Licenses
- Contenus libres
---

> Creative Commons International vient de publier The Power of Open, un eBook montrant des exemples de projets dans des domaines variés recourant aux CC.
