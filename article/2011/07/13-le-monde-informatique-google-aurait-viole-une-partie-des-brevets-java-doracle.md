---
site: Le Monde Informatique
title: "Google aurait violé une partie des brevets Java d'Oracle"
author: Jacques Cheminat
date: 2011-07-13
href: http://www.lemondeinformatique.fr/actualites/lire-google-aurait-viole-une-partie-des-brevets-java-d-oracle-34206.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
---

> Il « apparaît possible » que Google savait que son système d'exploitation mobile Android violait des brevets détenus par Oracle sur Java, a indiqué le juge en charge de l'affaire. A l'époque, les deux parties auraient tenté de négocier sans succès.
