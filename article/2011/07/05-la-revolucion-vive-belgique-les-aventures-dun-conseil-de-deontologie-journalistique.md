---
site: La Révolucion VIVE
title: "Belgique : les aventures d'un \"conseil de déontologie journalistique\""
date: 2011-07-05
href: http://www.larevolucionvive.org.ve/spip.php?article1697
tags:
- Partage du savoir
- Institutions
- International
---

> S’il est un journal qu’on ne peut suspecter de sympathie pour Hugo Chavez, c’est bien le Figaro. Pourtant lorsqu’il évoque ce 5 juillet le « retour triomphal » du vénézuélien opéré avec succès d’une tumeur à Cuba, il rappelle aux lecteurs qu’il est « Président, élu à trois reprises depuis 1998 et chef de file de la gauche radicale en Amérique latine ». Preuve qu’on peut avoir des convictions plutôt conservatrices sans se départir du minimum vital journalistique.
