---
site: Silicon.fr
title: "L’Open Cloud Initiative : pour un cloud computing libre!"
author: David Feugey
date: 2011-07-28
href: http://www.silicon.fr/l%E2%80%99open-cloud-initiative-pour-un-cloud-computing-libre-57367.html
tags:
- Entreprise
- Associations
---

> L’OCI est une association qui souhaite proposer un cadre juridique pour une offre de cloud ouverte. Une initiative intéressante.
