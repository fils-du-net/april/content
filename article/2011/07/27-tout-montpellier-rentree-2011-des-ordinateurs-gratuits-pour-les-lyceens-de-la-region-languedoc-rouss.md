---
site: TOUT MONTPELLIER
title: "Rentrée 2011 : des ordinateurs gratuits pour les lycéens de la Région Languedoc-Roussillon!"
author: Caroline
date: 2011-07-27
href: http://www.toutmontpellier.fr/rentree-2011-des-ordinateurs-gratuits-pour-les-lyceens-de-la-region-languedoc-roussillon---20842.html
tags:
- Administration
- Éducation
---

> Dès la rentrée 2011, tous les lycées de la région seront équipés de cartables numériques.
