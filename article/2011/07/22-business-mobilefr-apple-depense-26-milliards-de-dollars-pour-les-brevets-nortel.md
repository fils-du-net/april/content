---
site: "business-MOBILE.FR"
title: "Apple a dépensé 2,6 milliards de dollars pour les brevets Nortel"
author: Christophe Auffray
date: 2011-07-22
href: http://www.businessmobile.fr/actualites/apple-a-depense-26-milliards-de-dollars-pour-les-brevets-nortel-39762643.htm
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- International
---

> Sur les 4,5 milliards de dollars versés pour l'acquisition des 6.000 brevets de Nortel, convoités par Google, Apple a payé, au sein du consortium acquéreur, un total de 2,6 milliards de dollars. Apple pourrait désormais chercher à empêcher Google dacquérir les brevets d'InterDigital.
