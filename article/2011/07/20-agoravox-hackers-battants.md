---
site: AgoraVox
title: "Hackers Battants"
author: olivier cabanel
date: 2011-07-20
href: http://www.agoravox.fr/actualites/citoyennete/article/hackers-battants-97833
tags:
- Internet
- Partage du savoir
- Institutions
- Licenses
- Philosophie GNU
- International
---

> C’est l’affaire Wikileaks qui les a mis au devant de la scène, et qu’ils soient considérés comme des pirates, ou comme des « Robins des Bois », beaucoup de hackers sont devenus en quelques années le poil à gratter d’une oligarchie fragilisée.
