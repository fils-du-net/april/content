---
site: Mondequibouge.be
title: "Bienvenue dans le monde des « fabs labs » !"
author: Didier Bieuvelet
date: 2011-07-01
href: http://www.mondequibouge.be/index.php/2011/07/bienvenue-dans-le-monde-des-fabs-labs/
tags:
- Internet
- Partage du savoir
- Matériel libre
- Innovation
- International
---

> Grâce à des bidouilleurs de génie, on a depuis longtemps accès à des logiciels libres et gratuits qui fonctionnent aussi bien, sinon mieux, que des logiciels édités par de grandes marques. Le navigateur Web Firefox ou le système d’exploitation Linux font partie des exemples les plus répandus. Aujourd’hui, cette philosophie du « libre » déborde le monde numérique des logiciels pour gagner le monde matériel des objets. Voici les « fabs labs », les laboratoires de fabrication personnelle.
