---
site: 20minutes.fr
title: "Le concept du logiciel libre tient dans une bière"
author: Anne Pflaum
date: 2011-07-07
href: http://www.20minutes.fr/article/754495/concept-logiciellibre-tient-biere
tags:
- Institutions
- Sensibilisation
- Licenses
- Philosophie GNU
---

> Strasbourg. La ville accueille les rencontres mondiales du logiciel libre
