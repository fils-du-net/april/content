---
site: atlantico
title: "Internet perd sa liberté à vitesse grand V"
author: Benjamin Bayart
date: 2011-07-24
href: http://www.atlantico.fr/decryptage/internet-google-facebook-liberte-securite-donnees-multinationales-nouveau-reseau-149449.html
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Neutralité du Net
---

> Google vs. Facebook : la lutte est terrible. Mais la véritable bataille pour Internet se joue peut-être ailleurs. Personnage historique du web (FDN, logiciel libre), Benjamin Bayart revient sur l'évolution du réseau désormais entre les mains des géants du secteur. Attention danger ?
