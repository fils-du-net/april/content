---
site: L'ALSACE.fr
title: "La recette d’une bière brassée à Schiltigheim"
date: 2011-07-10
href: http://www.lalsace.fr/actualite/2011/07/10/la-recette-d-une-biere-brassee-a-schiltigheim
tags:
- Entreprise
- Sensibilisation
- Associations
- Philosophie GNU
---

> Un logiciel libre est un programme informatique dont les méthodes et techniques de conception sont accessibles et modifiables par tous.
