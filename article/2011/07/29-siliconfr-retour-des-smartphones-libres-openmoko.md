---
site: Silicon.fr
title: "Retour des  smartphones libres Openmoko"
author: David Feugey
date: 2011-07-29
href: http://www.silicon.fr/retour-des-smartphones-libres-openmoko-57488.html
tags:
- Matériel libre
- Innovation
---

> Les Neo 1973 et Neo FreeRunner vont bientôt trouver un successeur, développé par Golden Delicious sur une base très moderne.
