---
site: LeMagIT
title: "Avec sa licence OHL, le Cern crée une rampe de lancement pour l'Open Hardware"
author: Cyrille Chausson
date: 2011-07-12
href: http://www.lemagit.fr/article/open-source-materiel/9151/1/avec-licence-ohl-cern-cree-une-rampe-lancement-pour-open-hardware/
tags:
- Administration
- Matériel libre
- Innovation
- Licenses
- Contenus libres
---

> Le Cern a publié la version 1.1 de sa licence OHL, qui encadre les plans et documentations d'équipements aux spécifications libres (Open Hardware). Ce cadre juridique, qui impose un suivi des modifications et de la production, est vu comme un passage obligé pour accélérer l'industrialisation diu modèle Open Hardware. Histoire d'installer un peu plus le concept de matériel libre dans le paysage de l'IT.
