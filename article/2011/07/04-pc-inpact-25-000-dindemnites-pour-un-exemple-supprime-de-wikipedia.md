---
site: PC INpact
title: "25 000 € d'indemnités pour un exemple supprimé de Wikipedia"
author: Marc Rees
date: 2011-07-04
href: http://www.pcinpact.com/actu/news/64468-rentabiliweb-hi-media-micropaiement-wikipedia.htm
tags:
- Entreprise
- HADOPI
- Institutions
---

> Dans une série de litiges « croisés » entre Rentabiliweb et Hi-Média, le Tribunal de commerce de Paris a estimé que supprimer le nom de son concurrent dans une entrée Wikipedia était une faute ouvrant droit à réparation.
