---
site: Taiwan Info
title: "Brevets : Google soutient HTC face à Apple"
date: 2011-07-20
href: http://taiwaninfo.nat.gov.tw/ct.asp?xItem=172003&ctNode=467&mp=4
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> Alors que la Commission américaine du commerce international (ITC) vient de rendre un verdict préliminaire favorable à Apple dans le cadre de l'affaire qui l’oppose au cinquième fabricant mondial de smartphones, le taiwanais HTC, Google, qui fournit à HTC le système d’exploitation Android, a apporté hier son soutien à ce dernier.
