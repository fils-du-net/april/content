---
site: Numerama
title: "Une étude confirme que l'ACTA ne sert à rien pour les Européens"
author: Julien L.
date: 2011-07-20
href: http://www.numerama.com/magazine/19370-une-etude-confirme-que-l-acta-ne-sert-a-rien-pour-les-europeens.html
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Europe
- International
- ACTA
---

> Une étude menée pour le compte du Parlement européen pointe les faiblesses de l'accord commercial anti-contrefaçon (ACTA). Selon le document, obtenu par la Quadrature du Net, les avantages de ce traité international pour les citoyens européens sont pratiquement inexistants.
