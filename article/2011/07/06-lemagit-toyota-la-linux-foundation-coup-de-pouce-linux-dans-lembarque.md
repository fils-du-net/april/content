---
site: LeMagIT
title: "Toyota à la Linux Foundation : coup de pouce à Linux dans l'embarqué "
author: Cyrille Chausson
date: 2011-07-06
href: http://www.lemagit.fr/article/linux-android-automobile-embarque/9110/1/toyota-linux-foundation-coup-pouce-linux-dans-embarque/
tags:
- Entreprise
- Associations
---

> En rejoignant la Linux Foundation, le constructeur d'automobile japonais Toyota entend pousser les développements de Linux dans le segment de l'automobile. Signe d'un décollage de l'OS Open Source dans le très critique monde de l'embarqué. Android pourrait en profiter.
