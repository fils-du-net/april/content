---
site: ITespresso.fr
title: "Open data : l’Europe veut concevoir son propre portail"
author: Jacques Franc de Ferrière
date: 2011-07-22
href: http://www.itespresso.fr/open-data-l-europe-veut-concevoir-son-propre-portail-44563.html
tags:
- Internet
- Administration
- Institutions
- Contenus libres
- Europe
- Open Data
---

> La Commission européenne veut élaborer son propre portail qui regroupera tous les données publiques de ses services en vue d'une exploitation par des tiers.
