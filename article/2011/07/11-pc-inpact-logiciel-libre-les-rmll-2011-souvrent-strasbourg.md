---
site: PC INpact
title: "Logiciel Libre : les RMLL 2011 s'ouvrent à Strasbourg "
author: Marc Rees
date: 2011-07-11
href: http://www.pcinpact.com/actu/news/64579-rmll-strasbourg-logiciel-libre.htm?vc=1
tags:
- Sensibilisation
- Associations
- Promotion
---

> Cette année, les 12èmes RMLL, fameuses Rencontres Mondiales du Logiciel Libre, s’installent à Strasbourg. Jusqu'au 14 juillet, l’évènement propose une nouvelle fois un cycle de conférences, de tables rondes et d’ateliers pratiques autour du Logiciel Libre et de ses usages. « L’objectif est de fournir un lieu d’échanges entre utilisateurs, développeurs et acteurs du Logiciel Libre » précisent les organisateurs. L’accès est gratuit et ouvert à tous.
