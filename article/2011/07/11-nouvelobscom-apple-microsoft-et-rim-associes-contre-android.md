---
site: nouvelObs.com
title: "Apple, Microsoft et RIM associés contre Android ?"
author: Christophe Auffray
date: 2011-07-11
href: http://hightech.nouvelobs.com/actualites/depeche/20110712.ZDN4829/apple-microsoft-et-rim-associes-contre-android.html
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
---

> Trois concurrents dans les OS mobiles, opposés à Android de Google, qui s'associent et raflent ainsi les 6.000 brevets de Nortel : l'opération suscite la crainte de l'American Antitrust Institute qui demande au régulateur d'ouvrir une enquête et des garde-fous afin de prévenir des pratiques anticoncurrentielles.
