---
site: clubic.com
title: "France Wireless annonce un forfait 3G avec Data illimitée"
author: Olivier Robillart
date: 2011-07-15
href: http://www.clubic.com/telephone-portable/forfaits-3g-illimites/actualite-435544-forfait-3g-france-wireless.html
tags:
- Entreprise
- Internet
- Neutralité du Net
---

> A l'occasion des Rencontres Mondiales du Logiciel Libre, l'opérateur associatif France Wireless a annoncé qu'il lance un forfait mobile uniquement Data sans aucune limitation. Pour réussir son opération, l'opérateur a besoin de 1 024 abonnés.
