---
site: Numerama
title: "Le piratage de logiciels reste très élevé en France, selon la BSA"
author: Julien L.
date: 2011-07-27
href: http://www.numerama.com/magazine/19425-le-piratage-de-logiciels-reste-tres-eleve-en-france-selon-la-bsa.html
tags:
- Entreprise
- Logiciels privateurs
- Désinformation
---

> Dans un entretien, le porte-parole du  Business Software Alliance (BSA) a de nouveau pointé du doigt les mauvais scores de la France en matière de piratage de logiciels. Reprenant les conclusions d'une étude pour l'année 2010, dont la méthodologie est critiquée, le responsable de la BSA rappelle que 39 % des logiciels installés sont piratés.
