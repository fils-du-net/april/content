---
site: Génération nouvelles technologies
title: "Microsoft et SUSE : interopérabilité reconduite"
author: Jérôme G.
date: 2011-07-26
href: http://www.generation-nt.com/microsoft-suse-novell-interoperabilite-actualite-1235171.html
tags:
- Entreprise
- Brevets logiciels
---

> Microsoft et SUSE annoncent le prolongement de leur accord d'interopérabilité jusqu'au 1er janvier 2016.
