---
site: Le Monde Informatique
title: "Microsoft veut une plus grande intégration d'Hyper-V dans le noyau Linux"
author: Jacques Cheminat
date: 2011-07-19
href: http://www.lemondeinformatique.fr/actualites/lire-microsoft-veut-une-plus-grande-integration-d-hyper-v-dans-le-noyau-linux-34235.html
tags:
- Entreprise
---

> Microsoft ne bascule pas encore de Windows vers Linux, mais l'éditeur est devenu l'un des plus actifs contributeurs au noyau Linux. La tentative de Microsoft pour une plus grande intégration du pilote Hyper-V dans le noyau Linux prend plus de temps que prévu. Les travaux ont débuté en juillet 2009 et la firme de Redmond entend accélérer aujourd'hui le processus.
