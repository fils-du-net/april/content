---
site: Les Echos
title: "« La France a l'un des plus forts taux de piratage en Europe »"
author: Faustine Saint-Genies
date: 2011-07-26
href: http://www.lesechos.fr/economie-politique/france/actu/0201529990083-la-france-a-l-un-des-plus-forts-taux-de-piratage-en-europe-198150.php
tags:
- Entreprise
- Logiciels privateurs
- Désinformation
---

> Pour la Business Software Alliance, association de défense des développeurs de logiciels propriétaires, la contrefaçon de logiciels en France représente 1,9 milliard d'euros de manque à gagner.
