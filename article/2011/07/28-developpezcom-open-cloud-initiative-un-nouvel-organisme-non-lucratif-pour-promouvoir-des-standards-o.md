---
site: Developpez.com
title: "Open Cloud Initiative : un nouvel organisme à but non lucratif, pour promouvoir des standards ouverts dans le Cloud Computing"
author: Hinault Romaric
date: 2011-07-28
href: http://www.developpez.com/actu/35515/Open-Cloud-Initiative-un-nouvel-organisme-a-but-non-lucratif-pour-promouvoir-des-standards-ouverts-dans-le-Cloud-Computing/
tags:
- Entreprise
- Internet
- Associations
- Informatique en nuage
---

> Le nouvel organisme Open Cloud Initiative (OCI) a officiellement démarré ses activités lors de la convention open source (OSCON) qui se déroule actuellement à Portland aux USA.
