---
site: ITespresso.fr
title: "Violation de brevets Java : vers un revirement de situation ?"
date: 2011-07-13
href: http://www.itespresso.fr/violation-de-brevets-java-vers-un-revirement-de-situation-44331.html
tags:
- Entreprise
- Administration
- Institutions
- Brevets logiciels
---

> Les 7 brevets Java détenus par Oracle et prétendument utilisés dans Android sont l'objet de toute l'attention d'un juge américain. L'affaire porte désormais sur la connaissance a priori par Google de l'utilisation de ces brevets.
