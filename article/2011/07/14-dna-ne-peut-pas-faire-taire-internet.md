---
site: DNA
title: "« On ne peut pas faire taire Internet »"
author: Matthieu Mondoloni
date: 2011-07-14
href: http://www.dna.fr/fr/monde/info/5416211-Rencontres-mondiales-du-logiciel-libre-Benjamin-Bayart-president-de-FDN-On-ne-peut-pas-faire-taire-Internet
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Neutralité du Net
- International
---

> Benjamin Bayart, président de la FDN, était invité aux 12 e Rencontres mondiales du logiciel libre (RMLL) qui s’achèvent aujourd’hui à Strasbourg. Son association est connue, entre autres, pour avoir maintenu un accès internet en Égypte pendant la révolution. Rencontre avec ce défenseur de l’ Internet libre.
