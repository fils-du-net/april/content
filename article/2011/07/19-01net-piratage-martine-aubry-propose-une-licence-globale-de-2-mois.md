---
site: 01net.
title: "Piratage : Martine Aubry propose une licence globale de 2 €/mois"
author: Guillaume Deleurence
date: 2011-07-19
href: http://www.01net.com/editorial/535863/piratage-martine-aubry-propose-une-licence-globale-de-2-euros-mois/
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Droit d'auteur
- Licenses
---

> La candidate à la primaire socialiste a réaffirmé sa volonté d'abroger la Hadopi et de lui substituer un système de contribution sur les abonnements à Internet.
