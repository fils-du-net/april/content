---
site: clubic.com
title: "Economie numérique : La France renforce son partenariat avec la Tunisie"
author: Olivier Robillart
date: 2011-07-05
href: http://pro.clubic.com/actualite-e-business/actualite-433084-economie-numerique-france-renforce-partenariat-tunisie.html
tags:
- Entreprise
- Internet
- Économie
- Institutions
- International
---

> Les deux gouvernements annoncent avoir signé deux accords portant sur le développement du secteur des nouvelles technologies. Eric Besson, ministre de l'Economie numérique et son homologue tunisien, Abdelaziz Rassaa, ont donc mis sur pied une coopération gouvernementale dans le domaine du logiciel libre et de la formation au numérique.
