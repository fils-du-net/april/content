---
site: EurActiv
title: "Bruxelles envisage une législation européenne du droit d'auteur"
date: 2011-07-13
href: http://www.euractiv.com/fr/societe-information/bruxelles-envisage-une-gislation-europ-enne-du-droit-dauteur-news-506513
tags:
- Entreprise
- Institutions
- Droit d'auteur
- Licenses
- Europe
---

> Afin de simplifier la législation relative au droit d'auteur, différente dans chaque Etat, et d'améliorer l'accès transeuropéen aux œuvres culturelles, la Commission européenne envisage d'introduire un régime de protection du droit d'auteur unique et facultatif pour les films, les programmes télévisés et les autres œuvres audiovisuelles, selon un document consulté par EurActiv.
