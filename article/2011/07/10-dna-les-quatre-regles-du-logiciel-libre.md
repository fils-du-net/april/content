---
site: DNA
title: "Les quatre règles du logiciel libre"
date: 2011-07-10
href: http://www.dna.fr/fr/monde/info/5395838-Pratique-Les-quatre-regles-du-logiciel-libre
tags:
- Sensibilisation
- Associations
- Licenses
- Philosophie GNU
- Contenus libres
---

> L’univers du « libre », qui regroupe des logiciels informatiques, mais également des produits électroniques, culturels ou artistiques, est basé sur quatre règles.
