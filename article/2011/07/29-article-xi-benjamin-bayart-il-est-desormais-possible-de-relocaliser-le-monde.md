---
site: ARTICLE XI
title: "Benjamin Bayart : « Il est désormais possible de relocaliser le monde »"
author: ZeroS et JBB
date: 2011-07-29
href: http://www.article11.info/spip/Benjamin-Bayart-Il-est-desormais
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Brevets logiciels
- Droit d'auteur
- Neutralité du Net
---

> Dans « Internet libre ou Minitel 2.0 », conférence donnée en 2007, il disait brillamment sa crainte d’une verticalisation du Net.
