---
site: clubic.com
title: "MPEG-LA relance la polémique contre Google et son codec VP8"
author: Guillaume Belfiore
date: 2011-07-29
href: http://www.clubic.com/internet/google/actualite-438000-mpeg-relance-accusations-google-codec-vp8.html
tags:
- Entreprise
- Brevets logiciels
---

> La firme MPEG-LA, détenant des droits sur le codec H.264, notamment utilisé pour la vidéo en streaming, réitère ses accusations à l'encontre de Google et de son codec VP8.
