---
site: "cio-online.com"
title: "BMW passe 2,5 millions de lignes de Cobol sur Linux"
author: Bertrand Lemaire
date: 2011-07-04
href: http://www.cio-online.com/actualites/lire-bmw-passe-2-5-millions-de-lignes-de-cobol-sur-linux-3709.html
tags:
- Entreprise
- Logiciels privateurs
---

> Le constructeur automobile a renoncé à son mainframe Unisys au profit de serveurs Linux équipés de la solution Microfocus.
