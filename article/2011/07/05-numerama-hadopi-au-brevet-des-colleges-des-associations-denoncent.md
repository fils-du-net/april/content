---
site: Numerama
title: "Hadopi au brevet des collèges : des associations dénoncent"
author: Julien L.
date: 2011-07-05
href: http://www.numerama.com/magazine/19258-hadopi-au-brevet-des-colleges-des-associations-denoncent.html
tags:
- Entreprise
- April
- HADOPI
- Associations
- Désinformation
- Droit d'auteur
- Éducation
- Licenses
---

> L'April, Framasoft et Libre Accès ont dénoncé dans un communiqué la présence d'un document sur Hadopi dans l'épreuve d'histoire géographie et éducation civique du brevet des collèges 2011. Les trois associations en faveur du libre y voient une manœuvre du gouvernement pour faire la promotion du texte de loi au collège.
