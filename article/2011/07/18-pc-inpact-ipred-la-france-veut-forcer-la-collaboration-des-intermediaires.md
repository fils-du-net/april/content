---
site: PC INpact
title: "IPRED : la France veut forcer la collaboration des intermédiaires "
author: Marc Rees
date: 2011-07-18
href: http://www.pcinpact.com/actu/news/64678-hadopi-france-paris-notification-notice-and-stay-down.htm
tags:
- Internet
- April
- HADOPI
- Institutions
- Associations
- Droit d'auteur
- Europe
---

> La France a participé à la consultation publique lancée dans le cadre de la révision de la directive IPRED du 29 avril 2004. Cette révision vise à réajuster les législations en matière de droits de propriété intellectuelle. D’autres pays européens ont participé à cette consultation, tout comme des collectifs comme la Quadrature du net. L’examen des réponses est précieux car il permet de mesurer quelles sont les positions et les objectifs d’un pays considéré.
