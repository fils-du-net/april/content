---
site: PC INpact
title: "Vente déliée PC/OS : l'amendement PS rejeté en commission"
author: Marc Rees
date: 2011-07-21
href: http://www.pcinpact.com/actu/news/64735-vente-liee-pc-os-amendement-consommateur.htm?vc=1
tags:
- Institutions
- Vente liée
---

> Dans le cadre du projet de loi sur la protection des consommateurs, l'amendement visant à découpler la vente d'un PC et d'un logiciel d'exploitation a finalement été rejeté en commission.
