---
site: L'EXPRESS.fr
title: "Hadopi: \"La loi rend le citoyen lambda responsable\""
author: Flavien Hamon
date: 2011-07-20
href: http://www.lexpress.fr/actualite/politique/hadopi-la-loi-rend-le-citoyen-lambda-responsable_1013357.html
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Institutions
- Droit d'auteur
- Neutralité du Net
---

> Sécuriser sa connexion, les Labs d'Hadopi, le rôle de la Haute autorité, l'affaire TMG... Eric Walter, secrétaire général de la Hadopi a répondu aux questions des internautes de LEXPRESS.fr.
