---
site: clubic.com
title: "Vidéo : Microsoft fête les 20 ans de Linux"
author: Guillaume Belfiore
date: 2011-07-21
href: http://www.clubic.com/linux-os/actualite-436468-microsoft-fete-20-ans-linux.html
tags:
- Entreprise
- Associations
---

> Alors que Linux s'apprête à fêter son vingtième anniversaire, Microsoft a présenté ses meilleurs voeux sous la forme d'un petit clip vidéo.
