---
site: wmc actualités
title: "Visite d'Eric Besson : La Tunisie, pôle d'économie numérique"
author: Mehdi BEN SAÂD
date: 2011-07-07
href: http://www.webmanagercenter.com/management/article-107750-visite-d-eric-besson-la-tunisie-pole-d-economie-numerique%85mais-pas-a-la-saint-glinglin
tags:
- Entreprise
- Économie
- Institutions
- International
---

> Finalement, il ne s’agissait pas de la poudre de perlimpinpin. Toute la brochette de ministres et diplomates français qui se sont relayés en Tunisie, venus, non pas uniquement, applaudir la révolution du peuple, mais par-dessus tout soutenir l’économie nationale ébranlée.
