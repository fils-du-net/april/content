---
site: ZDNet.fr
title: "France Numérique 2020 : c'est parti"
author: Pascal Lechevallier
date: 2011-07-22
href: http://www.zdnet.fr/blogs/digital-home-revolution/france-numerique-2020-c-est-parti-39762622.htm
tags:
- Entreprise
- Internet
- Économie
- Institutions
---

> Décidément Eric Besson n'est pas un Ministre comme les autres. Non seulement il tient personnellement son compte twitter (@Eric_Besson) et n'hésite pas à prendre part à des discussions avec d'autres twittos, mais lorsqu'il lance le projet France Numérique 2020, il a la bonne idée de réunir autour de lui ceux qui font l'internet, petits et grands, ceux qui l'observent et le commentent, ceux qui innovent autour du web.
