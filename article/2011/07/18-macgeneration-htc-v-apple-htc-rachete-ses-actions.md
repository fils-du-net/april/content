---
site: macgeneration
title: "HTC v Apple : HTC rachète ses actions"
author: Anthony Nelzin
date: 2011-07-18
href: http://www.macgeneration.com/news/voir/208472/htc-v-apple-htc-rachete-ses-actions
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> La semaine dernière a été plutôt mouvementée pour HTC en bourse : les investisseurs, fébriles à l'attente de la première décision de l'ITC, ont progressivement lâché la firme taiwanaise. 6,5 % de baisse plus tard, l'action HTC atteignait son cours le plus bas depuis six mois. Une tendance qui devrait s'accentuer après que l'ITC a statué en faveur d'Apple (lire : HTC v Apple : l'ITC fragilise la position d'Android).
