---
site: atlantico
title: "Logiciel libre sur Internet, prémices du web 2.0"
author: Ziryeb Marouf
date: 2011-06-02
href: http://www.atlantico.fr/decryptage/internet-logiciel-libre-internautes-web-111606.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
---

> Dans son ouvrage, "Réseaux sociaux numériques d'entreprise", l'entrepreneur Ziryeb Marouf analyse l'utilité, les limites et les risques d'un réseau social numérique entre collaborateurs dans le monde de l'entreprise. Atlantico vous en fait partager les meilleures feuilles. Premier épisode.
