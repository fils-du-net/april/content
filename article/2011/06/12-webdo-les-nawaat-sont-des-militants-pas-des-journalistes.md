---
site: webdo
title: "Les Nawaat sont des «militants, pas des journalistes»"
author: Melek Jebnoun
date: 2011-06-12
href: http://www.webdo.tn/2011/06/12/les-nawaat-sont-des-militants-pas-des-journalistes/
tags:
- Entreprise
- Internet
- Institutions
- International
- Open Data
---

> Suite au rassemblement Kelmetnaa de samedi après midi, nous avons rencontré Houssem Hajlaoui, bloggeur activiste, membre de Nawaat et contributeur à l'organisation de la manifestation. Il a accepté de répondre aux questions de Webdo:
