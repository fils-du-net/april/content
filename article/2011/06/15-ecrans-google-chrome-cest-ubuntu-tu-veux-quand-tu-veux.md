---
site: écrans
title: " Google Chrome, c'est Ubuntu tu veux, quand tu veux"
author: Camille Gévaudan
date: 2011-06-15
href: http://www.ecrans.fr/Google-Chrome-c-est-Ubuntu-tu-veux,12964.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
---

> Ils sont discrets, et pourtant ils sont partout. On les sait en minorité, mais personne ne peut les dénombrer. Ils se fondent dans la masse, ne se font jamais remarquer... Jusqu’au jour où l’on gaffe. Comme hier, mardi 14 juin 2011, jour maudit où l’on eut la mauvaise idée de proposer un jeu chronophage tournant avec le plug-in Unity. Ils ont alors surgi dans les forums d’Ecrans.fr pour se rappeler à notre bon souvenir : eux, ils ne peuvent pas installer le plug-in Unity. Malheur ! On avait oublié les linuxiens.
