---
site: PCWorld.fr
title: "NASA et Open Source : un marriage difficile ?"
author: Florian Vieru
date: 2011-06-06
href: http://www.pcworld.fr/article/logiciels/nasa-open-source/515859/
tags:
- Administration
- Licenses
- International
---

> On parle souvent de l'adoption de solutions en Open Source par les entités gouvernementales. Il s'agit malheureusement trop souvent d'initiatives à sens unique. La NASA fait figure d'exception en la matière, puisqu'elle publie une quantité conséquente de code dans le domaine public, ce qui n'est pas toujours simple...
