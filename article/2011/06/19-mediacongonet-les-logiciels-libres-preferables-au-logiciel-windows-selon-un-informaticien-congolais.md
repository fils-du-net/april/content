---
site: mediacongo.net
title: "Les logiciels libres préférables au logiciel Windows, selon un informaticien congolais "
date: 2011-06-19
href: http://www.mediacongo.net/show.asp?doc=18126
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Partage du savoir
- Institutions
- International
---

> Un informaticien congolais a estimé jeudi à Kinshasa que les logiciels libres sont préférables au logiciel Windows parce qu’ils sont gratuits, faciles à modifier et protégés des virus.
