---
site: OWNI
title: "Instantané de neutralité"
author: Andréa Fradin
date: 2011-06-27
href: http://owni.fr/2011/06/27/instantane-de-neutralite/
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- Europe
- International
---

> Les Pays-Bas sont le premier état européen à avoir inscrit le principe de neutralité des réseaux dans la loi. Piqûre de rappel pour l'Europe et la France, l'initiative est aussi l'occasion de rappeler, en une image, la définition du concept.
