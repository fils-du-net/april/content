---
site: OWNI
title: "Royaume-Uni: vers un nouveau régime de propriété intellectuelle?"
author: Dovile Daveluy
date: 2011-06-15
href: http://owni.fr/2011/06/15/le-regime-de-la-propriete-intellectuelle-britannique-bientot-revise/
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Brevets logiciels
- Droit d'auteur
- Licenses
- Europe
- International
---

> Le gouvernement britannique a entre les mains un rapport sur le régime de la propriété intellectuelle qui fait consensus. Les recommandations de Ian Hargreaves, professeur en économie numérique, seront-elles utilisées ou se couvriront-elles de poussière ?
