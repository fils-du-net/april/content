---
site: LeMonde.fr
title: "Hacker de la transparence au Brésil"
author: Fondapol
date: 2011-06-16
href: http://www.lemonde.fr/idees/article/2011/06/16/hacker-de-la-transparence-au-bresil_1536748_3232.html
tags:
- Entreprise
- Internet
- Administration
- Institutions
- Standards
- International
- Open Data
---

> En mai dernier, les départements informatiques des administrations publiques brésiliennes ont organisé à Brasilia, en partenariat avec Consegi, constructeur de logiciels open source, une conférence sur le thème de l' "e-government" et de l' "open data". Un événement inédit dans le pays, d'un intérêt majeur aux yeux d'un grand nombre d'activistes de l'internet et du logiciel libre, venus sur place pour l'occasion, parmi lesquels Rufus Pollock (Open Knowledge Foundation), Nigel Shadbolt, (data.gov.uk), and David Eaves, (Vancouver's Open Government Motion).
