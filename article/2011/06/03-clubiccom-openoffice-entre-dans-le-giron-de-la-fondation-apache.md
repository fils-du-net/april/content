---
site: clubic.com
title: "OpenOffice entre dans le giron de la fondation Apache"
author: Antoine Duvauchelle
date: 2011-06-03
href: http://pro.clubic.com/it-business/actualite-425948-openoffice-giron-fondation-apache.html
tags:
- Entreprise
- Associations
---

> Le code source de la suite bureautique OpenOffice rejoint la fondation Apache. Oracle a ainsi honoré ses promesses de libérer le code de son logiciel, et de le mettre sous l'autorité d'une organisation indépendante.
