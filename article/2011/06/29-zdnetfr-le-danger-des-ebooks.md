---
site: ZDNet.fr
title: "Le danger des ebooks"
author: Frédéric Bergonzoli
date: 2011-06-29
href: http://www.zdnet.fr/blogs/doc-print/le-danger-des-ebooks-39762056.htm
tags:
- Entreprise
- Internet
- Logiciels privateurs
- DRM
- Droit d'auteur
---

> « À une époque où le business domine nos gouvernements et dicte nos lois, chaque avancée technologique fournit aux entreprises le moyen d’imposer de nouvelles restrictions au public »
