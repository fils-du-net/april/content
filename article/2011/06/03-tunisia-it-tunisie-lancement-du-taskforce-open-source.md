---
site: Tunisia IT
title: "Tunisie : Lancement du Taskforce Open Source"
date: 2011-06-03
href: http://www.tunisiait.com/article.php?article=7509
tags:
- Internet
- Institutions
- International
---

> L’Unité des Logiciels Libres au sein du Secrétariat d’Etat de la Technologie a lancé un site web dédié à la Taskforce OpenSource.
