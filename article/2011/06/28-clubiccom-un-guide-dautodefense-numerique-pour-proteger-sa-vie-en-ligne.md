---
site: clubic.com
title: "Un \"guide d'autodéfense numérique\" pour protéger sa vie en ligne"
author: Antoine Duvauchelle
date: 2011-06-28
href: http://pro.clubic.com/legislation-loi-internet/donnees-personnelles/actualite-431546-guide-autodefense-numerique-proteger-vie-ligne.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Associations
- International
---

> La protection de la vie privée vient de recevoir deux nouvelles contributions. La première émane de la célèbre association de protection des libertés en ligne, l'Electronic Frontier Foundation (EFF), et l'autre d'un groupe anonyme, qui édite la seconde version de son "Guide d'autodéfense numérique".
