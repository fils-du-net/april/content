---
site: Le nouvel Observateur
title: "Quel est le point commun entre les révolutions arabes et la French revolution ?"
author: Matthieu Jacquot
date: 2011-06-01
href: http://leplus.nouvelobs.com/contribution/1771;quel-est-le-point-commun-entre-les-revolutions-arabes-et-la-french-revolution.html
tags:
- Institutions
- International
---

> La réussite réside dans le changement de paradigme politique. De l'autre côté de la Méditerranée, il s'agissait de remplacer la dictature par la démocratie. Mais lorsqu'on est déjà en démocratie, que peut-on envisager ? Matthieu Jacquot a son idée sur la question.
