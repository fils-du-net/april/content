---
site: Génération nouvelles technologies
title: "PS3 : un hacker prêt pour la prison reste fier de ses actes"
author: Nathalie M. 
date: 2011-06-22
href: http://www.generation-nt.com/ps3-linux-hack-actualite-1220811.html
tags:
- Entreprise
- Internet
- Institutions
---

> Alors que Sony cherche encore les coupables du hack du PSN, un autre genre de hacker se fait malmener par la justice.
