---
site: Contrepoints
title: "Un eBooks sans DRM est un eBook mieux vendu"
author: h16
date: 2011-06-10
href: http://www.contrepoints.org/2011/06/10/29349-un-ebooks-sans-drm-est-un-ebook-mieux-vendu
tags:
- Entreprise
- Économie
- DRM
---

> Le livre se fait de plus en plus numérique. La France ratera-t-elle aussi ce virage ?
