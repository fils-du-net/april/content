---
site: ZDNet.fr
title: "Richard Stallman s’élève contre les restrictions du livre électronique"
author: Christophe Auffray
date: 2011-06-08
href: http://www.zdnet.fr/actualites/richard-stallman-s-eleve-contre-les-restrictions-du-livre-electronique-39761489.htm
tags:
- Entreprise
- Internet
- Institutions
- DRM
---

> Stratégie - Formats propriétaires, DRM, impossibilité de copier un e-book, possibilité pour un distributeur comme Amazon de supprimer un livre à distance, pourtant légalement acquis… Richard Stallman s’élève contre les restrictions appliquées par certaines entreprises.
