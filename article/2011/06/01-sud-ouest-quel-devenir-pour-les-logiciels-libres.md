---
site: SUD OUEST
title: "Quel devenir pour les logiciels libres ? "
author: Kévin Lavoix
date: 2011-06-01
href: http://www.sudouest.fr/2011/06/01/quel-devenir-pour-les-logiciels-libres-414387-3230.php
tags:
- Internet
- Promotion
---

> Aujourd'hui à 16 heures, l'espace Cyberbase de la CdC de Bourg en Gironde accueillera Pierre Jarillon. Le conférencier, docteur en physique et ancien ingénieur à la Sereb devenue Aérospatiale puis EADS, viendra présenter un exposé sur le thème « Logiciels libres : historique, enjeux de société et perspectives »
