---
site: OWNI
title: "Chicago: le parrain du datajournalisme?"
author: Mael Inizan
date: 2011-06-20
href: http://owni.fr/2011/06/20/chicago-le-parrain-du-datajournalisme/
tags:
- Entreprise
- Internet
- Partage du savoir
- International
- Open Data
---

> Des premiers hackers-journalistes à la News Application Team du Chicago Tribune, le sociologue Sylvain Parasie revient sur l’essor du datajournalisme dans la ville d’Al Capone.
