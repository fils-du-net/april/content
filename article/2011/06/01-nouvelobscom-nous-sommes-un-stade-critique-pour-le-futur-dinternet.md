---
site: nouvelObs.com
title: "\"Nous sommes à un stade critique pour le futur d'internet\""
author: Boris Manenti
date: 2011-06-01
href: http://hightech.nouvelobs.com/actualites/20110601.OBS4340/nous-sommes-a-un-stade-critique-pour-le-futur-d-internet.html
tags:
- Entreprise
- Internet
- HADOPI
- Associations
- Innovation
- Neutralité du Net
- International
---

> "Nous allons vers une vague de régulation d'internet qui causera des dommages sur la liberté et l'innovation", estime Mitchell Baker, présidente de la fondation Mozilla. Interview.
