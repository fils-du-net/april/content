---
site: Numerama
title: "41 pays approuvent le rapport de l'ONU sur la liberté et Internet. Pas la France"
author: Guillaume Champeau
date: 2011-06-15
href: http://www.numerama.com/magazine/19067-41-pays-approuvent-le-rapport-de-l-onu-sur-la-liberte-et-internet-pas-la-france.html
tags:
- Internet
- Institutions
- Droit d'auteur
- International
---

> 41 pays dont les Etats-Unis, l'Inde et le Brésil ont approuvé le rapport du Rapporteur Spécial de l'ONU sur la protection de la liberté d'expression sur Internet. La France qui défend le droit de suspendre l'accès à Internet de ses concitoyens au nom des droits d'auteur fait partie des absents.
