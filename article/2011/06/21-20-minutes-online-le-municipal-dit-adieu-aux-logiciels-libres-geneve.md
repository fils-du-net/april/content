---
site: 20 minutes online
title: "Le Municipal dit adieu aux logiciels libres - Geneve"
author: tpi
date: 2011-06-21
href: http://www.20min.ch/ro/news/geneve/story/15007488
tags:
- Logiciels privateurs
- Administration
- Économie
- International
---

> La mairie de Genève impose aux élus les produits Microsoft. Une décision qui fâche certains.
