---
site: Numerama
title: "Skype open-source : un hacker révèle le protocole de Skype"
author: Guillaume Champeau
date: 2011-06-02
href: http://www.numerama.com/magazine/18942-skype-open-source-un-hacker-revele-le-protocole-de-skype.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
---

> Un chercheur russe a publié jeudi le protocole de Skype, et une première version open-source, dans l'espoir de créer bientôt une version open-source entièrement fonctionnelle du logiciel de communication racheté par Microsoft.
