---
site: ActuaLitté.com
title: "Stallman : 'Il faut légaliser les oeuvres partagées sur les réseaux'"
author: Adrien Aszerman
date: 2011-06-09
href: http://www.actualitte.com/dossiers/1499-legaliser-oeuvres-diffusees-internet-pirater.htm
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Associations
- DRM
- Droit d'auteur
---

> Exclusif : Entretien avec Richard Stallman [1/2], à l'occasion de FOCUS 2011, sur le livre de demain, organisé par l'UNESCO, à Milan. Programmeur et militant du logiciel libre. Il est à l'origine du projet GNU et de la licence publique générale GNU connue aussi sous l’acronyme GPL, qu’il a rédigée avec l’avocat Eben Moglen. Il a popularisé le terme anglais copyleft. (via Wikipedia)
