---
site: Silicon.fr
title: "La suite bureautique LibreOffice en quête de certifications"
author: David Feugey
date: 2011-06-28
href: http://www.silicon.fr/la-suite-bureautique-libreoffice-en-quete-de-certifications-55260.html
tags:
- Entreprise
- Associations
---

> La Document Foundation est en train de réfléchir à la mise en place de certifications pour la suite bureautique LibreOffice. Une excellente idée, qui favorisera l’adoption de cette solution en entreprise.
