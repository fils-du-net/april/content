---
site: OWNI
title: "Espagne Labs: inventer la démocratie du futur"
author: Ophelia Noor
date: 2011-06-06
href: http://owni.fr/2011/06/06/espagne-labs-inventer-la-democratie-du-futur/
tags:
- Internet
- Institutions
- International
---

> Des assemblées numériques reliées entre elles, un réseau social alternatif, des outils open source et des licences libres en support, le tout coordonné au niveau technique par des hackers. Les acampadas du 15M se préparent à la globalisation du mouvement.
