---
site: LeDevoir.com
title: "La STM repousse le logiciel libre"
author: Fabien Deglise
date: 2011-06-29
href: http://www.ledevoir.com/societe/science-et-technologie/326453/la-stm-repousse-le-logiciel-libre
tags:
- Logiciels privateurs
- Administration
- Institutions
- Marchés publics
- International
---

> On efface tout et on refait la même chose. La Société de transport de Montréal (STM) envisage, pour une deuxième fois, d'exclure les logiciels libres d'un important appel d'offres visant à acquérir plus de 2500 suites bureautiques. Avec deux études «sérieuses et documentées» en main pour justifier son geste, la STM se prépare donc à accorder ce contrat évalué à 500 000 $ à la multinationale américaine Microsoft, et ce, même si Québec vient d'adopter une nouvelle loi pour faire plus de place aux logiciels libres dans l'administration publique.
