---
site: Mediapart
title: "Jeremy Rifkin : «Le nucléaire est mort»"
author: Ivan Villa
date: 2011-06-05
href: http://blogs.mediapart.fr/blog/ivan-villa/050611/jeremy-rifkin-le-nucleaire-est-mort
tags:
- Entreprise
- Internet
- Video
---

> Rifkin considère la technologie comme "totalement dépassée, d'un autre siècle". Il fait une analogie avec l'industrie de la musique qui n'a pas vu venir le partage de fichiers sur Internet,avec la presse qui n'a pas compris la blogosphère, avec Encyclopedia Universalis qui se moquait de Wikipedia, ou encore avec Bill Gates qui n'aurait jamais pensé que le logiciel libre Linux deviendrait un acteur mondial de premier plan.(...)
