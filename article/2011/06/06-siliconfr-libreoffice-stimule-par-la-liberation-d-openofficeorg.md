---
site: Silicon.fr
title: "LibreOffice stimulé par la libération d’OpenOffice.org"
author: David Feugey
date: 2011-06-06
href: http://www.silicon.fr/libreoffice-stimule-par-la-liberation-dopenoffice-org-53008.html
tags:
- Entreprise
- Associations
- Licenses
---

> LibreOffice 3.4.0 est maintenant disponible. Aucun rapprochement entre ce projet et OpenOffice.org, récemment passé sous le giron de la fondation Apache, n’est actuellement prévu.
