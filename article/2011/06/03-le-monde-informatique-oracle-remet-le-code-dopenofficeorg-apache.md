---
site: Le Monde Informatique
title: "Oracle remet le code d'OpenOffice.org à Apache"
author: MG
date: 2011-06-03
href: http://www.lemondeinformatique.fr/actualites/lire-oracle-remet-le-code-d-openofficeorg-a-apache-33867.html
tags:
- Entreprise
- Internet
- Associations
---

> On savait qu'Oracle avait décidé de remettre la suite bureautique OpenOffice dans la communauté Open Source, mais on ignorait encore, jusqu'à mercredi, à qui l'éditeur donnerait le projet. C'est la Fondation Apache qui la récupère. Les supporters de LibreOffice, le projet de Document Fondation, se dit ouvert à un rapprochement, mais émet quelques réserves.
