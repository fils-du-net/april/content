---
site: Numerama
title: "Le crowdsourcing contre la prolifération des brevets abusifs"
author: Guillaume Champeau
date: 2011-06-06
href: http://www.numerama.com/magazine/18975-le-crowdsourcing-contre-la-proliferation-des-brevets-abusifs.html
tags:
- Institutions
- Brevets logiciels
- International
---

> Le ministre britannique de la propriété intellectuelle, Baroness Wilcox, a lancé une initiative destinée à permettre au public de participer à l'examen de la validité des brevets, avant leur délivrance par l'administration. Une mesure qui vise à augmenter la qualité des brevets octroyés, tout en limitant les coûts induits par l'explosion des demandes de brevets ces dernières années.
