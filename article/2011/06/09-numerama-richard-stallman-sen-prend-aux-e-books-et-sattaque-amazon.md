---
site: Numerama
title: "Richard Stallman s'en prend aux e-books et s'attaque à Amazon"
author: Julien L.
date: 2011-06-09
href: http://www.numerama.com/magazine/19008-richard-stallman-s-en-prend-aux-e-books-et-s-attaque-a-amazon.html
tags:
- Entreprise
- Internet
- Associations
- DRM
- Licenses
---

> Résolument opposé aux DRM, Richard Stallman a écrit un texte dans lequel il cible les livres électroniques en général, et Amazon en particulier. Le président de la FSF appelle au boycott des e-books sous DRM tant que les droits et les libertés des usagers ne sont pas respectés. Pour lui, les livres physiques offrent beaucoup plus d'avantages aux utilisateurs.
