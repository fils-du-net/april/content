---
site: OWNI
title: "YouTube et les Creative Commons: ce qui change vraiment"
author: Calimaq
date: 2011-06-08
href: http://owni.fr/2011/06/08/youtube-et-les-creative-commons-ce-qui-change-vraiment/
tags:
- Entreprise
- Internet
- Droit d'auteur
- Licenses
- Video
- Contenus libres
---

> Si l'arrivée des CC sur le site de partage de vidéos est un pas à saluer dans le combat pour la Culture Libre, il reste encore de la marge pour arriver à un véritable droit à la réutilisation créative.
