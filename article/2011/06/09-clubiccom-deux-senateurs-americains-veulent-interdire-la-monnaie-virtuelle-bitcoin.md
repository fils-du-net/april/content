---
site: clubic.com
title: "Deux sénateurs américains veulent interdire la monnaie virtuelle Bitcoin"
author: Antoine Duvauchelle
date: 2011-06-09
href: http://pro.clubic.com/e-commerce/paiement-en-ligne/actualite-427586-senateurs-americains-veulent-interdire-monnaie-virtuelle-bitcoin.html
tags:
- Entreprise
- Internet
- Économie
- Institutions
- International
---

> Les monnaies virtuelles sont souvent l'outil d'enjeux plus grands. C'est ce que viennent nous rappeler deux sénateurs américains, qui veulent faire interdire par les autorités des Etats-Unis la monnaie virtuelle Bitcoin. Raison invoquée : elle est utilisée par les vendeurs de drogues.
