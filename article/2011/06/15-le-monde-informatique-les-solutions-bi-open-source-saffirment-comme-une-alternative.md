---
site: Le Monde Informatique
title: "Les solutions BI Open Source s'affirment comme une alternative"
author: Jean Elyan
date: 2011-06-15
href: http://www.lemondeinformatique.fr/actualites/lire-les-solutions-bi-open-source-s-affirment-comme-une-alternative-33961.html
tags:
- Entreprise
- Innovation
---

> Cette semaine, Infobright, Ingres et Jaspersoft ont annoncé qu'elles proposaient désormais des solutions d'entreposage et d'analyse de données développées à partir de code Open Source, preuve que les outils d'analyses du web en temps réel sous licence libre sont de plus en plus considérés comme une alternative.
