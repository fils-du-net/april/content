---
site: ITRmanager.com
title: "Open CIO Summit 2011: le think tank pour les DSI"
date: 2011-06-08
href: http://www.itrmanager.com/articles/120083/open-cio-summit-2011-think-tank-dsi.html
tags:
- Entreprise
---

> Organisé dans le cadre de l’Open World Forum, l’Open CIO Summit se tiendra, pour sa 3e édition, le 22 septembre à l’Eurosites Georges V à Paris. Les DSI peuvent d'ores et déjà s’inscrire sur le site www.openciosummit.org
