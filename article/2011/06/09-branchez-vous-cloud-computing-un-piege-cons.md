---
site: "BRANCHEZ-VOUS!"
title: "«Cloud computing»: un piège à cons?"
author: Patrick Bellerose
date: 2011-06-09
href: http://www.branchez-vous.com/affaires/benefice-net/blogues/mediabiz/2011/06/cloud_computing_un_piege_a_con.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Informatique en nuage
---

> Steve Jobs a dévoilé hier une grande offensive pour promouvoir ses services d'informatique dans les nuages. Mais un des pionniers des logiciels libres estime que cette pratique est «pire que de la stupidité».
