---
site: L'EXPRESS.fr
title: "Le gourou du logiciel libre veut libérer les ebooks"
author: Emmanuelle Alféef
date: 2011-06-09
href: http://www.lexpress.fr/culture/livre/le-gourou-du-logiciel-libre-veut-liberer-les-ebooks_1000853.html
tags:
- Entreprise
- Internet
- Associations
- DRM
- Droit d'auteur
- Licenses
- International
---

> Les protections mises en place sur la plupart des ebooks - les "DRM" - font grincer des dents le gourou du logiciel libre, Richard Stallman. Il considère que les distributeurs portent atteinte à la liberté de l'utilisateur.
