---
site: Le Monde Informatique
title: "Google met son poids dans LibreOffice, qui valide une version entreprise"
author: Jean Elyan
date: 2011-06-21
href: http://solutionspme.lemondeinformatique.fr/articles/lire-google-met-son-poids-dans-libreoffice-qui-valide-une-version-entreprise-3653.html
tags:
- Entreprise
- Associations
---

> La semaine dernière, LibreOffice a gagné du galon. Non seulement l'éditeur de la suite de productivité a livré une mise à jour stable de sa suite Open Source, plus une version validée pour l'entreprise, mais il aussi annoncé l'arrivée de Google et d'autres grands acteurs dans son tout nouveau conseil consultatif.
