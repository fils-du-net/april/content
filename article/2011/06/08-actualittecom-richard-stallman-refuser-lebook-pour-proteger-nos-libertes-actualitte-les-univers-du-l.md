---
site: ActuaLitté.com
title: "Richard Stallman : refuser l'ebook, pour protéger nos libertés ActuaLitté - Les univers du livre"
author: Nicolas Gary
date: 2011-06-08
href: http://www.actualitte.com/actualite/26551-stallman-dangers-ebook-liberter-respecter.htm
tags:
- Entreprise
- DRM
- Licenses
- Standards
---

> Penseur et inventeur d'une philosophie idéaliste, Richard Stallman est une sorte de gourou pour nombre de geeks - et une référence pour n'importe qui d'un peu averti dans l'univers du monde libre et de l'open source.
