---
site: ZDNet.fr
title: "Oracle confie OpenOffice à la fondation Apache"
author: Christophe Auffray
date: 2011-06-06
href: http://www.zdnet.fr/actualites/oracle-confie-openoffice-a-la-fondation-apache-39761400.htm
tags:
- Entreprise
- Associations
- Licenses
---

> Oracle promettait en avril de confier le contrôle à la communauté. OpenOffice.org sera désormais un projet supervisé par la fondation Apache. Un choix que regrette le projet LibreOffice en raison d’incompatibilités avec la licence Apache v2.
