---
site: Les Echos
title: "Frédéric Lefebvre enterre les actions de groupe"
author: Lionel STEINMANN
date: 2011-06-01
href: http://www.lesechos.fr/economie-politique/france/actu/0201413719808-frederic-lefebvre-enterre-les-actions-de-groupe-171503.php
tags:
- Économie
- Institutions
---

> Frédéric Lefebvre a présenté en Conseil des ministres 25 mesures pour « renforcer les droits, la protection et l'information du consommateur ». L'UFC-Que Choisir regrette le « manque d'ambition » du projet gouvernemental.
