---
site: PC INpact
title: "Le PS confirme l'abrogation des lois Hadopi et Loppsi"
author: Nil Sanyas
date: 2011-06-22
href: http://www.pcinpact.com/actu/news/64247-martine-aubry-ps-abrogation-hadopi-loppsi.htm
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Institutions
- Licenses
- Neutralité du Net
---

> À moins d’un an des élections présidentielles, les opérations de communication des différents partis commencent à se multiplier. Martine Aubry, la première secrétaire du PS, vient ainsi de publier sur Rue89 un long texte sur Internet et le numérique en général en France. La loi Hadopi est logiquement évoquée, et son abandon est à nouveau confirmé en cas de victoire du PS. Tout comme LOPPSI.
