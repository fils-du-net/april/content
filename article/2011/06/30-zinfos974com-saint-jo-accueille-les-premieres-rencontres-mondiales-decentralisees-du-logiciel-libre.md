---
site: Zinfos974.com
title: "Saint-Jo accueille les premières rencontres mondiales décentralisées du logiciel libre"
author: Lilian Cornu
date: 2011-06-30
href: http://www.zinfos974.com/Saint-Jo-accueille-les-premieres-rencontres-mondiales-decentralisees-du-logiciel-libre_a30075.html
tags:
- Entreprise
- Logiciels privateurs
- Promotion
---

> Firefox, OpenOffice, VLC, Linux sont tous des logiciels libres, ils sont accessibles sans contrepartie pour le concepteur. Cette philosophie contraste avec l'énorme marché des logiciels protégés comme ceux de Microsoft ou Apple. La différence est assez simple. Les logiciels libres peuvent être utilisés, étudiés ou modifiés sans contraintes alors que les autres sont exploités dans le strict cadre d'un accord commercial.
