---
site: leSoleil
title: "Gestion de l'informatique au gouvernement: le logiciel libre devra être considéré"
author: Pierre Asselin
date: 2011-06-06
href: http://www.cyberpresse.ca/le-soleil/actualites/politique/201106/05/01-4406283-gestion-de-linformatique-au-gouvernement-le-logiciel-libre-devra-etre-considere.php
tags:
- Administration
- Institutions
- Associations
- Marchés publics
- International
---

> (Québec) Le projet de loi sur la gestion de l'informatique au gouvernement, piloté par la présidente du Conseil du trésor, Michelle Courchesne, a été amendé pour que les organismes publics aient l'obligation de considérer le logiciel libre au même titre que les autres solutions en technologies de l'information.
