---
site: LE FIGARO.fr
title: "#PdF11 : le partage au service de la démocratie"
author: Marie-Catherine Beuth
date: 2011-06-07
href: http://blog.lefigaro.fr/medias/2011/06/pdf11-le-partage-au-service-de.html
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Contenus libres
- International
- Open Data
---

> Avec les technologies, la participation de l'individu au fonctionnement démocratique des sociétés semble facilité, a révélé le premier jour du Personal Democracy Forum, à New York, auquel j'assiste grâce à l'Atelier BNP Paribas. Ce deuxième jour se penche un peu plus sur les implications et la participation des institutions, notamment avec le mouvement open data et l'accès ouvert aux données. Derrière, c'est toute une nouvelle philosophie du partage qui se dessine, au cœur des interventions de la matinée.
