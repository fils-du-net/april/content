---
site: clubic.com
title: "Oracle veut une grosse part du gâteau Android"
author: Antoine Duvauchelle
date: 2011-06-08
href: http://pro.clubic.com/entreprises/oracle/actualite-427150-oracle-gateau-android.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Droit d'auteur
---

> La saga judiciaire entre Google et Oracle se poursuit. Ce dernier vient de déposer une nouvelle réclamation devant la justice, où il demande plus d'argent que ce qu'Android a généré depuis son lancement en 2008. Google pourrait, en cas de victoire d'Oracle, ne pas avoir d'autre choix que de faire payer son OS aux fabricants de téléphones.
