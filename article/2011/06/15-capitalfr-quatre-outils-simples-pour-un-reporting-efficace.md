---
site: Capital.fr
title: "Quatre outils simples pour un reporting efficace"
author: Didier Blum
date: 2011-06-15
href: http://www.capital.fr/carriere-management/conseils/guide-du-manager/didier-blum/quatre-outils-simples-pour-un-reporting-efficace-605772
tags:
- Entreprise
- Logiciels privateurs
---

> Editer ses résultats sous forme de graphiques ou de tableaux de bord n’est pas forcément un exercice complexe et coûteux. Misez sur la qualité à petit prix.
