---
site: nonfiction.fr
title: "Que pense le Parti de Gauche du numérique ?"
author: Clémence ARTUR
date: 2011-06-20
href: http://www.nonfiction.fr/article-4759-que_pense_le_parti_de_gauche_du_numerique_.htm
tags:
- Internet
- Administration
- Économie
- Institutions
- Neutralité du Net
---

> Julien Bernard, membre de la commission numérique du Parti de Gauche, répond aux questions de nonfiction.fr sur la réflexion de son parti sur le Web.
