---
site: clubic.com
title: "Richard Stallman veut boycotter les ebooks"
author: Guillaume Belfiore
date: 2011-06-08
href: http://www.clubic.com/livre-electronique/actualite-427310-richard-stallman-ebooks-livres.html
tags:
- Entreprise
- Internet
- DRM
- Licenses
---

> Richard Stallman, le fondateur du mouvement GNU et fervent défenseur du logiciel libre, a récemment publié un document dans lequel il explique ses positions vis-à-vis du livre électronique.
