---
site: Silicon.fr
title: "Fin de la recherche Linux sur Google"
author: David Feugey
date: 2011-06-16
href: http://www.silicon.fr/fin-de-la-recherche-linux-sur-google-54047.html
tags:
- Entreprise
- Internet
- Associations
---

> Le moteur de recherche dédié à Linux a disparu du site de Google. Une bien mauvaise nouvelle pour la communauté.
