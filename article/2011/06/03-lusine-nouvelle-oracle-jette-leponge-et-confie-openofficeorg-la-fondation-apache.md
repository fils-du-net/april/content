---
site: L'USINE NOUVELLE
title: "Oracle jette l'éponge et confie OpenOffice.org à la fondation Apache"
author: Christophe Dutheil 
date: 2011-06-03
href: http://www.usinenouvelle.com/article/oracle-jette-l-eponge-et-confie-openoffice-org-a-la-fondation-apache.N153157
tags:
- Entreprise
- Internet
- Associations
- Licenses
---

> Exit les projets de commercialisation. Deux ans après le rachat de Sun Microsystems, Oracle confie à la fondation Apache les rênes d'OpenOffice.org, le principal logiciel de bureautique en open source. Les contributeurs historiques sont soulagés, de même qu'IBM, qui s'est empressé de préciser qu'il apportera sa pierre au nouveau projet.
