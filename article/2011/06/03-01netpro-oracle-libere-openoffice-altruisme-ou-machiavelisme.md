---
site: 01netPro.
title: "Oracle libère OpenOffice : altruisme ou machiavélisme ?"
author: Alain Clapaud
date: 2011-06-03
href: http://pro.01net.com/editorial/533836/oracle-libere-openoffice-altruisme-ou-machiavelisme/
tags:
- Entreprise
- Associations
- Licenses
---

> Confronté à une fronde de la communauté OpenOffice et un projet concurrent, LibreOffice, Oracle réagit et propose doffrir la suite bureautique libre à la fondation Apache.
