---
site: ITespresso.fr
title: "Le protocole de Skype a été cracké par un chercheur russe"
author: Jacques Franc de Ferrière
date: 2011-06-03
href: http://www.itespresso.fr/le-protocole-de-skype-a-ete-cracke-par-un-chercheur-russe-43225.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Interopérabilité
- Standards
---

> Le protocole de communication de Skype a été cracké par un chercheur russe. Des codes sources ont été publiés sur le Net dans le but de créer un logiciel open source compatible.
