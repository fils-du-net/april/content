---
site: PC INpact
title: "Richard Stallman n'aime pas les ebooks verrouillés"
author: Nil Sanyas
date: 2011-06-09
href: http://www.pcinpact.com/actu/news/64017-richard-stallman-livres-electroniques-kindle.htm?vc=1
tags:
- Entreprise
- DRM
- Droit d'auteur
---

> Dans un texte comme il les aime, Richard Stallman, que l’on ne présente pas, a vivement critiqué les livres électroniques et leurs dangers. Bien entendu, l’Américain pro-logiciel libre ne s’attaque pas à tous les ebooks, mais à ceux aux environnements fermés, et en particulier le Kindle d’Amazon. Et il n’y va pas avec le dos de la cuillère.
