---
site: ZDNet.fr
title: "Hadopi, licence globale, fibre, blocage... Les propositions numériques du PS et d'Aubry"
author: Christophe Auffray
date: 2011-06-22
href: http://www.zdnet.fr/actualites/hadopi-licence-globale-fibre-blocage-les-propositions-numeriques-du-ps-et-d-aubry-39761881.htm
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Institutions
- Droit d'auteur
- Licenses
- Neutralité du Net
- Europe
---

> Abrogation des lois Hadopi et Loppsi, augmentation du budget de la CNIL, couverture à 100% en très haut débit d’ici 10 ans, gestion collective des droits d’auteurs… Le parti socialiste dévoile son programme numérique pour 2012. Quid du financement des promesses et de la volonté politique ?
