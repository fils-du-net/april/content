---
site: LeMonde.fr
title: "La robotique \"open source\""
author: Rémi Sussan
date: 2011-06-10
href: http://www.lemonde.fr/technologies/article/2011/06/10/la-robotique-open-source_1534695_651865.html
tags:
- Entreprise
- Logiciels privateurs
- Innovation
---

> Les fondus d’électronique n’ont pas attendu la vogue actuelle du Do it yourself (DIY) pour se pencher sur la robotique, qui a toujours fait leur bonheur. Pourtant, jusqu’à récemment, il existait une nette séparation entre les travaux souvent brillants des amateurs et ceux des roboticiens professionnels. Une des causes étant peut-être l’absence de systèmes open source d’un haut niveau de complexité permettant aux amateurs de s’inspirer de l’expérience de leurs pairs mais aussi des chercheurs.
