---
site: PC INpact
title: "Conseil d'Etat : plus de \"taxe\" copie privée sur les supports pro"
author: Marc Rees
date: 2011-06-17
href: http://www.pcinpact.com/actu/news/64169-copie-privee-padawan-support-professionnels.htm
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
- Europe
---

> Nous venons de l'apprendre, le Conseil d'État vient de rendre son arrêt tant attendu. Il décide l'annulation de la décision 11 de la Commission copie privée. Par ailleurs, il interdit toute taxe copie privée sur les supports professionnels, selon les éléments qui nous remontent.
