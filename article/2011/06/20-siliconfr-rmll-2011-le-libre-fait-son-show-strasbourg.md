---
site: Silicon.fr
title: "RMLL 2011 : le libre fait son show à Strasbourg"
author: David Feugey
date: 2011-06-20
href: http://www.silicon.fr/rmll-2011-le-libre-fait-son-show-a-strasbourg-54371.html
tags:
- Associations
- Promotion
---

> La prochaine édition des Rencontres Mondiales du Logiciel Libre aura lieu en juillet à Strasbourg. Un événement bien ficelé, qui réunit de multiples activités.
