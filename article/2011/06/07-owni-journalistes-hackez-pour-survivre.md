---
site: OWNI
title: "Journalistes: hackez pour survivre!"
author: Marc Mentré
date: 2011-06-07
href: http://owni.fr/2011/06/07/journalistes-hackez-pour-survivre/
tags:
- Entreprise
- Internet
- Partage du savoir
- Innovation
---

> Hackear el periodismo. C'est le titre du dernier livre de Pablo Mancini, lui-même journaliste en Argentine. L'auteur juge inévitable l'intégration des hackers dans les rédactions ou, mieux encore, l'arrivée de journalistes-hackers.
