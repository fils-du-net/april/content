---
site: PC INpact
title: "PC INpact interviewe Richard Stallman (FSF) (Techniquement un progrès, socialement une régression)"
author: Marc Rees
date: 2011-06-11
href: http://www.pcinpact.com/dossiers/richard-stallman-interview-liberte-justice/193-1.htm
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Économie
- April
- Institutions
- Associations
- DADVSI
- DRM
- Droit d'auteur
- Licenses
- Informatique en nuage
---

> Lors de son passage en France, Richard Stallman nous a accordé une interview depuis le siège de l'April. Un rendez-vous rare où le père du projet GNU et de la licence publique générale (GPL) revient avec nous sur plusieurs faits d'actualité. Le Kindle, Apple, l'eG8, Hadopi ou encore la problématique des logiciels privateurs et des menottes numériques.
