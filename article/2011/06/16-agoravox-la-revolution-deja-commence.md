---
site: AgoraVox
title: "La révolution a déjà commencé"
author: Jean Zin
date: 2011-06-16
href: http://www.agoravox.fr/actualites/europe/article/la-revolution-a-deja-commence-96076
tags:
- Internet
- Économie
- Institutions
- International
---

> Avant de prendre congé, alors que le mouvement semble connaître un reflux, à l'exception notable de la Grèce, il me semble que c'est le moment de se rendre compte que, quoiqu'il arrive, la révolution a déjà commencé. Cela ne veut pas dire que la victoire serait proche, encore moins qu'elle serait facile. Il faut s'attendre à bien des revers mais si l'on peut dire que la crise n'a pas encore vraiment eu lieu, on a déjà les prémices de la révolution à venir.
