---
site: Politis.fr
title: "Musiciens : comment se faire entendre ?"
author: Ingrid Merckx
date: 2011-06-16
href: http://www.politis.fr/Musiciens-comment-se-faire,14551.html
tags:
- Entreprise
- Internet
- Contenus libres
---

> Dossier spécial Fête de la musique. Début avril, le jazzman Laurent Coq ouvrait un débat avec son blog Révolution de jazzmin. Il soulignait la difficulté croissante des jeunes musiciens à se faire connaître dans un contexte de marchandisation. La Fête de la musique est l’occasion de faire le point sur leurs situations et leurs propositions.
