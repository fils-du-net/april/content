---
site: LeMonde.fr
title: "Richard Stallman : \"L'utilisateur doit contrôler le programme, pas l'inverse\""
author: Damien Leloup
date: 2011-06-14
href: http://www.lemonde.fr/technologies/article/2011/06/14/richard-stallman-l-utilisateur-doit-controler-le-programme-pas-l-inverse_1535920_651865.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Institutions
- Brevets logiciels
- DRM
- Droit d'auteur
- Philosophie GNU
- Europe
- International
---

> Richard Stallman est l'un des "pères" du logiciel libre, ces programmes dont le code source est public et modifiable. Militant pour une réforme radicale et globale du droit d'auteur, il revient sur l'évolution du monde du droit, de la technologie et de l'informatique.
