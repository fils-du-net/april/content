---
site: 01net.
title: "Vente liée : les associations interpellent Eric Besson"
author: Guillaume Deleurence
date: 2011-02-15
href: http://www.01net.com/editorial/528373/vente-liee-les-associations-interpellent-eric-besson/
tags:
- Internet
- April
- Institutions
- Associations
- Neutralité du Net
- Europe
---

> A la suite de l'entretien donné par le ministre de l'Industrie à 01net., l'April et l'Aful lui demandent des mesures concrètes et réclament une concertation.
