---
site: Silicon.fr
title: "Cisco et Canonical militent pour le cloud computing ouvert"
author: David Feugey
date: 2011-02-07
href: http://www.silicon.fr/cisco-et-canonical-militent-pour-le-cloud-computing-ouvert-44676.html
tags:
- Entreprise
- Internet
- Interopérabilité
- Innovation
- Informatique en nuage
---

> Deux nouvelles compagnies se rallient au projet OpenStack. Canonical espère l’employer au sein de l’Ubuntu, alors que Cisco y voit une solution idéale en terme d’intéropérabilité.
