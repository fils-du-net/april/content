---
site: clubic.com
title: "Mandriva rejoint l'Open Invention network"
author: Guillaume Belfiore
date: 2011-02-08
href: http://www.clubic.com/linux-os/actualite-395950-mandriva-rejoint-open-invention-network.html
tags:
- Entreprise
- Associations
- Brevets logiciels
---

> L'éditeur français Mandriva annonce aujourd'hui avoir rejoint le réseau Open Invention. Spécialisé dans le rachat de brevets afin de protéger la communauté Linux, l'OIN souhaite ainsi favoriser l'innovation et regroupe près de 200 membres.
