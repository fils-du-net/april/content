---
site: clubic.com
title: "Loi neutralité : premières passes d'armes à l'Assemblée"
author: Olivier Robillart
date: 2011-02-17
href: http://www.clubic.com/connexion-internet/actualite-398338-debats-neutralite-ump-ps.html
tags:
- Internet
- Institutions
- Neutralité du Net
---

> Dans le cadre d'une proposition de loi déposée par le parti socialiste, l'Assemblée nationale a débuté les débats sur le sujet de la neutralité du réseau. Entre volonté d'établir un cadre légal et attente des positions de l'Arcep et de l'Union européenne, les députés et le gouvernement tâtonnent.
