---
site: ParisTech REVIEW
title: "Wikipedia et ses pairs, ou l'avènement d'un nouveau mode de production"
date: 2011-02-18
href: http://www.paristechreview.com/2011/02/18/wikipedia-avenement-production-collaborative/
tags:
- Le Logiciel Libre
- Entreprise
- Internet
- Économie
- Partage du savoir
- Innovation
- Sciences
---

> Wikipedia vient de fêter ses 10 ans. Plus important recueil de référence jamais produit, le site met à la portée de tous des pans entiers de connaissance autrefois accessible seulement à une poignée de chercheurs dans quelques grandes librairies universitaires. Mais pour certains, l'encyclopédie collaborative, alimentée de manière bénévole, est en elle-même le symbole d'un phénomène encore plus significatif : l'avènement de la production collaborative.
