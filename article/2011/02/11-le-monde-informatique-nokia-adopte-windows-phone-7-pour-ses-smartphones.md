---
site: Le Monde Informatique
title: "Nokia adopte Windows Phone 7 pour ses smartphones"
author: Serge Leblal
date: 2011-02-11
href: http://www.lemondeinformatique.fr/actualites/lire-nokia-adopte-windows-phone-7-pour-ses-smartphones-32879.html
tags:
- Entreprise
- Logiciels privateurs
- International
---

> Nokia et Microsoft ont annoncé, vendredi 11 février, la signature d'un partenariat stratégique pour proposer des terminaux mobiles animés par Windows Phone 7.
