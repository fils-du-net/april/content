---
site: Silicon.fr
title: "Pari gagné pour la Document Foundation (LibreOffice) : 50.000 euros récoltés en huit jours"
author: David Feugey
date: 2011-02-27
href: http://www.silicon.fr/pari-gagne-pour-la-document-foundation-libreoffice-50-000-euros-recoltes-en-huit-jours-46268.html
tags:
- Associations
- International
---

> En seulement huit jours, la Document Foundation a rassemblé de quoi créer une structure officielle en Allemagne, et d’assurer le paiement de ses premiers frais.
