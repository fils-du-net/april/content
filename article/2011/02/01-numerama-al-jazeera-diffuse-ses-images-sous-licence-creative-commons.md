---
site: Numerama
title: "Al Jazeera diffuse ses images sous licence Creative Commons"
author: Julien L.
date: 2011-02-01
href: http://www.numerama.com/magazine/17921-al-jazeera-diffuse-ses-images-sous-licence-creative-commons-maj.html
tags:
- Entreprise
- Internet
- Licenses
- Contenus libres
- International
---

> La chaîne de télévision qatarie Al Jazeera a adopté les licences Creative Commons pour certains de ses contenus. C'est notamment le cas des photos et des vidéos retraçant l'actualité en Égypte et en Tunisie. Le choix des licences libres favorisera la diffusion licite de ces contenus.
