---
site: LE FIGARO.fr
title: "Stephen Elop : «Nokia va recevoir plusieurs milliards de dollars de Microsoft»   "
author: Marc Cherki
date: 2011-02-15
href: http://www.lefigaro.fr/societes/2011/02/14/04015-20110214ARTFIG00719-stephen-elop-nokia-va-recevoir-plusieurs-milliards-de-dollars-de-microsoft.php
tags:
- Entreprise
- Logiciels privateurs
- Économie
- International
---

> À Barcelone au Mobile World Congress, le nouveau PDG du groupe finlandais explique pourquoi il a préféré s'allier avec Microsoft plutôt qu'avec Google.
