---
site: "business-MOBILE.FR"
title: "Nokia et Microsoft confirment leur partenariat autour de Windows Phone 7"
date: 2011-02-11
href: http://www.businessmobile.fr/actualites/nokia-et-microsoft-confirment-leur-partenariat-autour-de-windows-phone-7-39758194.htm
tags:
- Entreprise
- Logiciels privateurs
---

> Windows Phone sera la plateforme principale pour les smartphones Nokia tandis que MeeGo devient un projet open source qui servira de laboratoire. L'organigramme du constructeur finandais est remanié avec une nouvelle structure opérationnelle.
