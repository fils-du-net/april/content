---
site: PC INpact
title: "Conseil National du Numérique : les décrets bientôt publiés"
author: Marc Rees
date: 2011-02-25
href: http://www.pcinpact.com/actu/news/62148-conseil-national-numerique-cnn-tabaka.htm
tags:
- Entreprise
- Internet
- April
- Partage du savoir
- Institutions
- Associations
---

> Le rapport sur le Conseil national du numérique (le rapport, notre analyse), a été dévoilé ce matin. On a appris par la suite d'autres détails sur cette future instance destinée à représenter l'univers du numérique en France.
