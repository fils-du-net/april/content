---
site: Numerama
title: "Nokia s'allie à Microsoft : que devient Meego ?"
author: Julien L.
date: 2011-02-11
href: http://www.numerama.com/magazine/18040-nokia-s-allie-a-microsoft-que-devient-meego.html
tags:
- Entreprise
- Logiciels privateurs
- Innovation
---

> Le rapprochement entre Nokia et Microsoft ne condamne pas l'avenir de MeeGo. Dans un message publié sur Twitter, la société finlandaise continuera à participer au développement de la plate-forme open source, avec Intel et AMD. Un appareil doit sortir avant la fin de l'année 2011.
