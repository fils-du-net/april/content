---
site: LeMonde.fr
title: "Qui sont les anonymous ?"
author: JPBichard
date: 2011-02-07
href: http://www.lemonde.fr/idees/chronique/2011/02/07/qui-sont-les-anonymous_1476028_3232.html
tags:
- Internet
- Institutions
- International
---

> Suite à l'affaire « Wikileaks » et la publication des « câbles diplomatiques » d'origine nord-américaine, un collectif non structuré hiérarchiquement, mais efficace à différents niveaux (techniques, communication, réactivité...) est apparu sur le devant de la e-scène. Les « Anonymous » plaident en faveur de la liberté d'expression et de l'accès libre à toutes les informations liées à la vie des citoyens.
