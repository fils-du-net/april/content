---
site: LA TRIBUNE
title: "A bâtons rompus avec Shantanu Narayen (Adobe)"
author: Pascal Boulard
date: 2011-02-25
href: http://www.latribune.fr/blogs/blog-initie/20110225trib000604300/a-batons-rompus-avec-shantanu-narayen-adobe.html
tags:
- Entreprise
- Internet
- Standards
---

> Shantanu Narayen est le directeur général d'Adobe. En 2010, l'entreprise a réalisé un chiffre d'affaires de 3,8 milliards de dollars et dégagé un bénéfice net de 774,68 millions de dollars.
