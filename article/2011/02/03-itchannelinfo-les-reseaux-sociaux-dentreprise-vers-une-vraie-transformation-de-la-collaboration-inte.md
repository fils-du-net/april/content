---
site: ITCHANNEL.info
title: "Les réseaux sociaux d'entreprise : vers une vraie transformation de la collaboration interne"
author: La rédaction
date: 2011-02-03
href: http://www.itchannel.info/articles/114822/reseaux-sociaux-entreprise-vers-vraie-transformation-collaboration-interne.html
tags:
- Entreprise
- Internet
- Innovation
---

> A l'heure où Chatter.com et d’autres proposent aux collaborateurs d'une même entreprise de se mettre en réseau, USEO s'est demandé où étaient les réseaux sociaux d’entreprises (RSE) et quel était le potentiel social de l’offre du marché. Un sujet particulièrement d'actualité. Il est vrai que l'année 2010 a confirmé le poids grandissant des réseaux sociaux d'entreprise, tant au niveau des entreprises qui ont lancé des projets ambitieux que des éditeurs traditionnels qui socialisent leur offre ou des pure-players qui continuent à innover.
