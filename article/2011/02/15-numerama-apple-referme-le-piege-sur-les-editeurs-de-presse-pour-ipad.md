---
site: Numerama
title: "Apple referme le piège sur les éditeurs de presse pour iPad"
author: Guillaume Champeau
date: 2011-02-15
href: http://www.numerama.com/magazine/18076-apple-referme-le-piege-sur-les-editeurs-de-presse-pour-ipad.html
tags:
- Entreprise
- Internet
- Économie
---

> En dévoilant mardi le système d'abonnement et d'achat de contenus imposé aux éditeurs de presse sur iPad, Apple a refermé un piège qu'il avait tendu dès la sortie de la tablette tactile.
