---
site: france mobiles.com
title: "Google sur le marché de la mobilité :  une ascension inattendue ? "
date: 2011-02-28
href: http://www.francemobiles.com/actualites/id/201102281297241687/google_sur_le_marche_de_la_mobilite_une_ascension_inattendue_.html
tags:
- Entreprise
- Logiciels privateurs
- Innovation
---

> Il y a trois ans, en novembre 2007, Google annonçait le lancement de son système d'exploitation, Android. En janvier 2010, le géant du Web a lancé son Google Phone, le Nexus One.
