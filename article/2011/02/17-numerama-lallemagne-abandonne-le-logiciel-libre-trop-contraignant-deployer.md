---
site: Numerama
title: "L'Allemagne abandonne le logiciel libre, trop contraignant à déployer"
author: Julien L.
date: 2011-02-17
href: http://www.numerama.com/magazine/18094-l-allemagne-abandonne-le-logiciel-libre-trop-contraignant-a-deployer.html
tags:
- Logiciels privateurs
- Administration
- Interopérabilité
- Institutions
- Désinformation
---

> L'Allemagne recule dans le domaine de l'open source. Après avoir misé pendant plusieurs années sur le logiciel libre, le ministère des affaires étrangères allemand revient à des solutions logicielles plus classiques... et propriétaires. Le gouvernement a mis en avant des coûts inattendus liés au déploiement d'un écosystème libre pour justifier ce rétropédalage.
