---
site: ZDNet.fr
title: "Le ministère allemand des Affaires étrangères renonce à Linux pour Windows"
author: Christophe Auffray
date: 2011-02-23
href: http://www.zdnet.fr/actualites/le-ministere-allemand-des-affaires-etrangeres-renonce-a-linux-pour-windows-39758499.htm
tags:
- Interopérabilité
- International
---

> Le ministère allemand des Affaires étrangères renonce à Linux pour Windows - Invoquant l'insatisfaction des utilisateurs, des problèmes d'interopérabilité et des coûts cachés, le ministère allemand a décidé de migrer ses postes de travail Linux et OpenOffice vers Windows XP. Une première étape avant un passage à Windows 7 et Office 2010.
