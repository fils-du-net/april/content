---
site: ZDNet.fr
title: "Accord annoncé Nokia Microsoft : quels impacts ?"
author: Louis Naugès
date: 2011-02-14
href: http://www.zdnet.fr/blogs/entreprise-2-0/accord-annonce-nokia-microsoft-quels-impacts-39758242.htm
tags:
- Entreprise
- Logiciels privateurs
- Innovation
---

> L'accord annoncé entre Nokia et Microsoft est le dernier épisode des grands changements qui vont bouleverser le marché des plateformes mobiles au cours des 2 à 3 prochaines années. Apple, Google, HP... tous les acteurs devront se positionner rapidement sur ce marché clef du futur.
