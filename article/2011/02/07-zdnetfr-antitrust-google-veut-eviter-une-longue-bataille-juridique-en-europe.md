---
site: ZDNet.fr
title: "Antitrust : Google veut éviter une longue bataille juridique en Europe"
author: La rédaction
date: 2011-02-07
href: http://www.zdnet.fr/actualites/antitrust-google-veut-eviter-une-longue-bataille-juridique-en-europe-39758059.htm
tags:
- Entreprise
- Interopérabilité
- Institutions
- Europe
---

> Antitrust : Google veut éviter une longue bataille juridique en Europe - Google doit à ce stade de la procédure contredire les accusations déposées devant la Commission européenne et espérer ainsi éviter un long et coûteux procès. Eric Schmidt estime que les inquiétudes soulevées par les concurrents de Google sont « mineures ou non valables. »
