---
site: Numerama
title: "Cantonales : il ne reste que 34 signataires du pacte du logiciel libre"
author: Julien L.
date: 2011-02-22
href: http://www.numerama.com/magazine/18354-cantonales-il-ne-reste-que-34-signataires-du-pacte-du-logiciel-libre.html
tags:
- April
- Institutions
---

> Pour le second tour des élections cantonales de 2011, il ne reste plus que 34 signataires du pacte du logiciel libre en lice. La prochaine échéance étant fixée au dimanche 27 mars, l'April espère avoir le temps de persuader d'autres candidats de s'intéresser aux enjeux du logiciel libre.
