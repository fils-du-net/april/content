---
site: Distributique.com
title: "Comment le libre passe à la vente indirecte"
author: Pascal Boiron
date: 2011-02-21
href: http://www.distributique.com/actualites/lire-comment-le-libre-passe-a-la-vente-indirecte-15983.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Associations
---

> Durant plus de 15 ans, le logiciel libre a été cantonné aux seconds rôles et ne pesait encore que 0,2% du marché français du logiciel en 2002. Sa part de marché dépassera cette année les 5%, notamment parce que les grands comptes et les grands éditeurs sont désormais convaincus de l'intérêt du libre. Etat des lieux.
