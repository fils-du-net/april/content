---
site: PC INpact
title: "Vente liée : l'AFUL et l'APRIL demandent des actes à Éric Besson"
author: Marc Rees
date: 2011-02-14
href: http://www.pcinpact.com/actu/news/61914-vente-liee-aful-april-besson.htm
tags:
- Internet
- April
- Institutions
- Vente liée
- Associations
---

> Les récentes déclarations d’Éric Besson au site 01Net.fr selon lesquelles il se préoccuperait du secteur de la vente liée PC et OS ont donné l'occasion à l’Aful et l’April de signer un communiqué commun. Dans le texte, les deux organisations se souviennent du Plan Economie 2012 du même Éric Besson. Un plan qui contenait déjà plusieurs propositions destinées à faire avancer ce dossier. « Ces actions ont cependant été mises en sommeil depuis 2009, et les « progrès réalisés sur ce point » ne sont ni flagrants ni évidents » regrettent l’April et l’Aful.
