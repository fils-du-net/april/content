---
site: PCWorld.fr
title: "Codecs vidéo : le WebM de Google menacé ?"
author: Mathieu CHARTIER
date: 2011-02-16
href: http://www.pcworld.fr/2011/02/16/internet/codecs-video-webm-google-menace/511891/
tags:
- Entreprise
- Internet
- Brevets logiciels
- Video
---

> Les éventuels détenteurs de brevets couvrant le VP8 se signaleront-ils à MPEG-LA ?
