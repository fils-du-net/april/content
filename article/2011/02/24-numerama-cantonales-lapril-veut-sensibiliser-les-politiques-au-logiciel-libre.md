---
site: Numerama
title: "Cantonales : l'April veut sensibiliser les politiques au logiciel libre"
author: Julien L.
date: 2011-02-24
href: http://www.numerama.com/magazine/18151-cantonales-l-april-veut-sensibiliser-les-politiques-au-logiciel-libre.html
tags:
- Administration
- April
- Sensibilisation
- Standards
---

> Les élections cantonales auront lieu le mois prochain, le 20 et 27 mars. À cette occasion, l'April réactive l'initiative Candidats.fr qui vise à sensibiliser les élus aux enjeux du logiciel libre. Douze candidats ont déjà signé le pacte, et cinq d'entre eux ont des propositions dans leur programme.
