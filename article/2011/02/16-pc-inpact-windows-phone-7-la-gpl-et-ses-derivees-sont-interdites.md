---
site: PC INpact
title: "Windows Phone 7 : la GPL et ses dérivées sont interdites"
author: Vincent Hermann
date: 2011-02-16
href: http://www.pcinpact.com/actu/news/61973-windows-phone-7-gpl-conditions-developpeurs-applications-.htm
tags:
- Entreprise
- Licenses
---

> Les développeurs qui souhaitent créer une application pour Windows Phone 7 doivent impérativement accepter les conditions du Marketplace. Celles-ci sont consultables depuis un document PDF en libre accès. Or, en consultant les différentes sections, on se rend rapidement compte que Microsoft a choisi d’écarter les logiciels open source.
