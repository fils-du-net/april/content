---
site: ouestaf.com
title: "Fracture numérique : la solution dans les logiciels libres, selon un expert malien"
author: La rédaction
date: 2011-02-07
href: http://www.ouestaf.com/Fracture-numerique-la-solution-dans-les-logiciels-libres-selon-un-expert-malien_a3453.html
tags:
- Logiciels privateurs
- Économie
- Partage du savoir
- Associations
- Licenses
- International
---

> L’utilisation des logiciels libres, dont l’acquisition des licences est gratuite, peut permettre aux Africains de réduire la « fracture numérique », a estimé un expert malien faisant référence au terme utilisé par les spécialistes pour décrire le fossé qui caractérise l’accès aux outils informatiques et à leurs applications entre résidents d’un même pays ou entre utilisateurs de différents pays.
