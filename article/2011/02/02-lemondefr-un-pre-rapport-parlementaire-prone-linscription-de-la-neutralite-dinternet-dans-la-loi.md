---
site: LeMonde.fr
title: "Un pré-rapport parlementaire prône l'inscription de la neutralité d'Internet dans la loi"
author: La rédaction
date: 2011-02-02
href: http://www.lemonde.fr/technologies/article/2011/02/02/un-prerapport-parlementaire-prone-l-inscription-de-la-neutralite-d-internet-dans-la-loi_1473888_651865.html
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
---

> Deux députées proposent un encadrement du filtrage d'Internet qui prend le contre-pied de la loi Loppsi 2, en demandant l'intervention expresse d'un juge.
