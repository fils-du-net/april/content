---
site: Génération nouvelles technologies
title: "Google Public Data Explorer ouvert à tous"
author: Jérôme G.
date: 2011-02-21
href: http://www.generation-nt.com/google-public-data-explorer-donnees-statistiques-dspl-actualite-1163151.html
tags:
- Entreprise
- Internet
- Administration
- Standards
- Contenus libres
- International
---

> Via un nouveau format de données, Public Data Explorer de Google permet dorénavant à quiconque de publier des données privées.
