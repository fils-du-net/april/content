---
site: MediaEtudiant.fr
title: "Travailler gratuitement et partout grâce aux solutions Framasoft"
author: smellycat
date: 2011-02-10
href: http://www.mediaetudiant.fr/vie-etudiante/travailler-gratuitement-grace-a-framasoft-3574.php
tags:
- Éducation
---

> Vous avez besoin de logiciels de qualité pour vos différents travaux étudiants, mais ne voulez pas vous ruiner ? Vous n’avez pas de portable et les ordinateurs de votre établissement ne disposent pas de toutes les applications dont vous avez besoin ? Voici quelques solutions…
