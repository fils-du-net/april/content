---
site: indexel.net
title: "Logiciel : cinq tendances clés pour les années à venir"
author: Antoine Robin
date: 2011-02-13
href: http://www.indexel.net/actualites/logiciel-cinq-tendances-cles-pour-les-annees-a-venir-3285.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Innovation
- Informatique en nuage
---

> Les pays émergents, la mobilité, la mondialisation des marchés et l’adoption du cloud computing vont profondément impacter le marché du logiciel dans les cinq prochaines années.
