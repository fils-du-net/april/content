---
site: LeMagIT
title: "Poste de travail Linux : Les Affaires étrangères allemandes reviennent à Windows"
author: Christophe Bardy
date: 2011-02-22
href: http://www.lemagit.fr/article/windows-linux-poste-travail-office-allemagne/8184/1/poste-travail-linux-les-affaires-etrangeres-allemandes-reviennent-windows/
tags:
- Logiciels privateurs
- Administration
- Économie
- Interopérabilité
- Désinformation
- International
---

> Et un revers de plus pour Linux sur le poste de travail. Le ministère des Affaires étrangères allemand a décidé d’abandonner Linux et les applications Open Source sur les postes de travail pour revenir à Windows. Les réductions de coûts qu’auraient dû apporter cette migration seraient finalement partis en fumée dans des développements spécifiques et dans la formation du personnel.
