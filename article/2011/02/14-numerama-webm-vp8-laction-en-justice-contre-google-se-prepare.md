---
site: Numerama
title: "WebM / VP8 : l'action en justice contre Google se prépare"
author: Guillaume Champeau
date: 2011-02-14
href: http://www.numerama.com/magazine/18060-webm-vp8-l-action-en-justice-contre-google-se-prepare.html
tags:
- Entreprise
- Internet
- Brevets logiciels
---

> Le MPEG-LA demande aux industriels qui possèdent des brevets sur les méthodes employées par le codec VP8 et le format WebM de se faire connaître, afin de monter un dossier pour poursuivre Google et les utilisateurs de WebM en justice.
