---
site: LeMagIT
title: "Les actionnaires de Novell approuvent la vente à Attachmate"
author: ChannelNews
date: 2011-02-21
href: http://www.lemagit.fr/article/novell-rachat-attachmate/8175/1/les-actionnaires-novell-approuvent-vente-attachmate/
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Europe
- International
---

> Les actionnaires de Novell ont donné leur feu vert au rachat de la société par Attachmate. Il ne s'agit toutefois que d'une première étape. La balle est désormais dans le camps des autorités de la concurrence des deux côtés de l'Atlantique.
