---
site: poptronics
title: "Valentin Lacambre, du logiciel libre aux plantes libres"
author: anne laforet
date: 2011-02-14
href: http://www.poptronics.fr/Valentin-Lacambre-du-logiciel
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
- Innovation
- Contenus libres
---

> lancement du projet Seedsburo de Valentin Lacambre, pionnier de l’Internet indépendant, fondateur de l’hébergeur Altern et du registrar Gandi. Seedsburo est un projet pour libérer les graines qui poussent autour de nous.
