---
site: ITRmanager.com
title: "L'April publie une lettre ouverte aux parlementaires européens"
author: La rédaction
date: 2011-02-01
href: http://www.itrmanager.com/articles/114706/april-publie-lettre-ouverte-parlementaires-europeens.html
tags:
- April
- Institutions
- Associations
- Brevets logiciels
---

> L'April, End Software Patents et la Foundation for a Free Information Infrastructure (FFII) ont envoyé le 27 janvier 2011 une lettre ouverte commune aux parlementaires européens pour exprimer leurs inquiétudes à propos du projet de coopération renforcée sur le brevet unitaire, et leur demandant de repousser le vote sur le projet jusqu'à publication de l'avis de la cour européenne de justice (CJUE) sur sa légalité.
