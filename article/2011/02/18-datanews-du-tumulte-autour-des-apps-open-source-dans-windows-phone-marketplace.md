---
site: datanews
title: "Du tumulte autour des apps open source dans Windows Phone Marketplace"
author: Stefan Grommen
date: 2011-02-18
href: http://datanews.rnews.be/fr/ict/actualite/apercu/2011/02/18/du-tumulte-autour-des-apps-open-source-dans-windows-phone-marketplace/article-1194953639197.htm
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Brevets logiciels
- Licenses
---

> Les apps mobiles sous licence GPLv3, Affero GPLv3 et LGPLv3 ne peuvent (provisoirement) pas être distribuées et vendues via le magasin d’applications Windows Phone Marketplace de Microsoft.
