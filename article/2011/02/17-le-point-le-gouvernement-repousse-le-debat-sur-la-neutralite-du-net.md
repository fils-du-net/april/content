---
site: Le point
title: "Le gouvernement repousse le débat sur la neutralité du Net"
date: 2011-02-17
href: http://www.lepoint.fr/high-tech-internet/le-gouvernement-repousse-le-debat-sur-la-neutralite-du-net-17-02-2011-1296428_47.php
tags:
- Internet
- Institutions
- Neutralité du Net
---

> Les députés socialistes ont proposé une loi pour adopter ce principe. Éric Besson a promis une consultation sur le sujet fin novembre 2011.
