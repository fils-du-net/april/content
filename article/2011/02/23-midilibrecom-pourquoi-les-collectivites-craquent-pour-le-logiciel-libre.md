---
site: MidiLibre.com
title: "Pourquoi les collectivités craquent pour le logiciel libre ?"
author: SANDRA CANAL
date: 2011-02-23
href: http://www.midilibre.com/articles/2011/02/23/MONTPELLIER-Pourquoi-les-collectivites-craquent-pour-le-logiciel-libre-1546594.php5
tags:
- Internet
- Logiciels privateurs
- Administration
- Associations
---

> Pas une administration ni une collectivité qui ne soit dotée d'ordinateurs, eux-mêmes équipés de logiciels. Sauf qu'une licence Apple ou Microsoft a un coût. Et c'est le contribuable qui paye. Grâce aux logiciels libres, une sorte de 'troisième voie', il est possible de faire de grosses économies.
