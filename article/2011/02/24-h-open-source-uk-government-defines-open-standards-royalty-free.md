---
site: The H Open Source
title: "UK Government defines open standards as royalty free"
author: Dj Walker-Morgan
date: 2011-02-24
href: http://www.h-online.com/open/news/item/UK-Government-defines-open-standards-as-royalty-free-1197607.html
tags:
- Institutions
- Brevets logiciels
- Standards
- Europe
- English
---

> New procurement policy guidance from the UK government has defined open standards as having "intellectual property made irrevocably available on a royalty free basis"
