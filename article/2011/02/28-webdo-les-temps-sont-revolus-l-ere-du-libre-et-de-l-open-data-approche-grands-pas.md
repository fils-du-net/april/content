---
site: webdo
title: "Les temps sont révolus, l’ère du libre et de l’Open Data approche à grands pas "
author: Melek Jebnoun
date: 2011-02-28
href: http://www.webdo.tn/2011/02/28/les-temps-sont-revolus-lere-du-libre-et-de-lopen-data-approche-a-grands-pas/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Administration
- Institutions
- Contenus libres
- International
- Open Data
---

> La révolution est dans tous ses états dans les pays arabes. Ça n'a pas été uniquement une révolution sociopolitique en termes de satisfaction des demandes sociales et de réformes politiques mais aussi comme certains la nomment une révolution 2.0. Inspirée du web 2.0, une pratique par laquelle l'utilisateur devient actif et gère lui-même le contenu du web.
