---
site: PC INpact
title: "WebM et VP8 : l'association MPEG LA cherche des brevets"
author: Vincent Hermann
date: 2011-02-14
href: http://www.pcinpact.com/actu/news/61915-webm-vp8-mpeg-appel-brevets-h264.htm
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Brevets logiciels
- Video
---

> L’année dernière, Google a acquis la société On2 Technologies pour 106,5 millions de dollars. Un rachat qui à ce moment avait soulevé bien des questions sur ce que voulait en faire la firme de Mountain View. Quelque temps après, la nouvelle tombait : le codec VP8 devenait open source, et le format WebM naissait. Des zones d’ombre subsistaient toutefois concernant des brevets, et elles sont sur le point de se manifester de manière très concrète.
