---
site: übergizmo
title: "Le BlackBerry PlayBook supporterait les apps Android?"
author: Liliane Nguyen
date: 2011-02-11
href: http://fr.ubergizmo.com/2011/02/blackberry-playbook-apps-android/
tags:
- Entreprise
- Interopérabilité
- Brevets logiciels
---

> Pour ce qui est d’acheter un nouveau tablet, le problème des apps supportées est de première importance, et il y avait une précédente rumeur selon laquelle le futur tablet BlackBerry PlayBook de RIM pourrait supporter les apps Android
