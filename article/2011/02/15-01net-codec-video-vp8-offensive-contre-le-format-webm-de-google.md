---
site: 01net.
title: "Codec vidéo VP8 : offensive contre le format WebM de Google"
author: Christophe Guillemin
date: 2011-02-15
href: http://www.01net.com/editorial/528384/video-offensive-contre-le-format-webm-de-google/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Brevets logiciels
- Innovation
- Video
---

> Le consortium défendant le H.264, porté par Apple et Microsoft, s'en prend au concurrent open source WebM. Il invite toute entreprise pensant disposer de brevets essentiels à VP8, codec vidéo de WebM, à l'aider à créer une licence propriétaire.
