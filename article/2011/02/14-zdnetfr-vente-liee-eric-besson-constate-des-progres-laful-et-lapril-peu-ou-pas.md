---
site: ZDNet.fr
title: "Vente liée : Eric Besson constate des progrès, l'Aful et l'April peu ou pas"
author: Christophe Auffray
date: 2011-02-14
href: http://www.zdnet.fr/actualites/vente-liee-eric-besson-constate-des-progres-l-aful-et-l-april-peu-ou-pas-39758255.htm
tags:
- Entreprise
- Logiciels privateurs
- April
- Institutions
- Vente liée
- Associations
---

> Vente liée : Eric Besson constate des progrès, l'Aful et l'April peu ou pas - Affichage des prix des logiciels et vente liée, dans un entretien Eric Besson estime que des progrès ont été accomplis, mais d'autres sont encore à réaliser. Mobilisées depuis plusieurs années sur cette problématique, l'Aful et l'April se déclarent sceptiques et demandent des actes concrets.
