---
site: Numerama
title: "FreedomBox : liberté et vie privée offerts par un petit boîtier ?"
author: Guillaume Champeau
date: 2011-02-21
href: http://www.numerama.com/magazine/18120-freedombox-liberte-et-vie-privee-offerts-par-un-petit-boitier.html
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Innovation
- International
---

> Le logiciel est neutre. Il peut être utilisé pour la liberté, ou pour asservir. C'est fort de cette conscience que l'avocat et activiste Eben Moglen a donné avec Debian le coup d'envoi de la FreedomBox Foundation, qui doit aboutir à la réalisation de petits boîtiers à bas prix équipés de logiciels libres qui redonnent aux internautes le contrôle de leurs données, de leur vie privée, et de leur liberté de communication. Une révolution en puissance.
