---
site: nouvelObs.com
title: "Vente liée : Eric Besson constate des progrès, l'Aful et l'April peu ou pas"
author: Christophe Auffray
date: 2011-02-14
href: http://hightech.nouvelobs.com/actualites/depeche/20110215.ZDN3861/vente-liee-eric-besson-constate-des-progres-l-aful-et-l-april-peu-ou-pas.html
tags:
- Internet
- April
- Institutions
- Vente liée
- Associations
---

> Affichage des prix des logiciels et vente liée, dans un entretien Eric Besson estime que des progrès ont été accomplis, mais d'autres sont encore à réaliser. Mobilisées depuis plusieurs années sur cette problématique, l'Aful et l'April se déclarent sceptiques et demandent des actes concrets.
