---
site: ZDNet.fr
title: "«Produire du logiciel libre», un nouveau Framabook"
author: Thierry Noisette
date: 2011-02-01
href: http://www.zdnet.fr/blogs/l-esprit-libre/produire-du-logiciel-libre-un-nouveau-framabook-39757929.htm
tags:
- Entreprise
- Internet
- Brevets logiciels
- Droit d'auteur
- Licenses
- Promotion
- Contenus libres
---

> La collection de l'association Framasoft, qui a publié des livres sur plusieurs logiciels libres et une biographie de Richard Stallman, traduit ce livre de Karl Fogel sur la gestion de développement. Ce 9ème Framabook est sous licence Creative Commons, téléchargeable et également «achetable» en version imprimée.
