---
site: Contrepoints
title: "Against Intellectual Monopoly"
author: Le Minarchiste
date: 2011-02-02
href: http://www.contrepoints.org/2011/02/02/12799-against-intellectual-monopoly
tags:
- Entreprise
- Économie
- Brevets logiciels
---

> Quand les pauvres gens assurent les rentes monopolistiques de grosses corporations
