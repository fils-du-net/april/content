---
site: PC INpact
title: "Windows Phone 7 : des licences libres oui, la GPL non"
author: Vincent Hermann
date: 2011-02-21
href: http://www.pcinpact.com/actu/news/62052-windows-phone-7-licences-open-source-permissions-gpl.htm
tags:
- Entreprise
- Désinformation
- Licenses
- Philosophie GNU
---

> La semaine dernière, nous avions discuté des conditions de développement de Windows Phone 7, pour que les applications puissent atterrir sur le Marketplace. On y trouvait un descriptif de toutes les licences qui ne pouvaient pas être utilisées, à commencer par la GPL et de nombreux dérivés. Mary-Jo Foley, de ZDnet, a obtenu quelques premiers éléments de réponse.
