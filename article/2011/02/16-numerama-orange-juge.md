---
site: Numerama
title: "Orange juge "
author: Guillaume Champeau
date: 2011-02-16
href: http://www.numerama.com/magazine/18088-orange-juge-34fatigante34-la-strategie-d-apple-et-veut-des-os-ouverts.html
tags:
- Entreprise
- Logiciels privateurs
---

> Opérateur échaudé craint l'eau froide. Orange, qui a le sentiment de s'être fait avoir par Apple et qui se dit aujourd'hui fatigué par la stratégie fermée de la firme de Cupertino, prévient Microsoft et Nokia qu'il pourrait ne pas commercialiser leurs mobiles et donner la préférence à des systèmes ouverts.
