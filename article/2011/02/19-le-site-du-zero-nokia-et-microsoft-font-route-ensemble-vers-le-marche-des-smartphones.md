---
site: le Site du Zéro
title: "Nokia et Microsoft font route ensemble vers le marché des smartphones"
author: Etienne-02, planete.game57, rayquaza, SoftDeath et valloch
date: 2011-02-19
href: http://www.siteduzero.com/news-62-39404-p1-nokia-et-microsoft-font-route-ensemble-vers-le-marche-des-smartphones.html
tags:
- Entreprise
- Logiciels privateurs
---

> Avec près de 39% des parts de marché mondial, Nokia est l'un des plus grands constructeurs de téléphones mobiles. Cependant, avec l'arrivée récente des smartphones tels que l'iPhone d'Apple, l'entreprise connaît quelques difficultés à s'imposer dans ce nouveau domaine. Le 11 février dernier, Nokia officialise un partenariat « stratégique » avec Microsoft, qui propose un système d'exploitation pour mobiles : Windows Phone 7. Les deux firmes comptent ainsi tenir tête aux leaders du marché : Apple, avec son iOS et Google, avec Android.
