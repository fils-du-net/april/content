---
site: PC INpact
title: "FreedomBox : un mini-serveur pour marier Internet et Sécurité"
author: Vincent Hermann
date: 2011-02-22
href: http://www.pcinpact.com/actu/news/62074-freedombox-foundation-serveur-personnel-protection-vie-privee-censure.htm
tags:
- Entreprise
- Internet
- Associations
- Innovation
- Licenses
- Neutralité du Net
- Philosophie GNU
- Informatique en nuage
---

> Si l’on ne devait retenir qu’une seule chose des Techdays de Microsoft cette année, c’était bien la montée en puissance de Windows Azure à travers les produits de la firme, et donc du cloud computing. Cette fameuse informatique dans les nuages, qui nous permet d’accéder à nos données de n’importe où, est devenu le fer de lance de Google, Amazon, et d’autres concurrents. Mais tandis que la facilité d’accès augmente, les questions sur la sécurité et le respect de la vie privée en font tout autant. Voici FreedomBox, à contrepied de l’évolution actuelle.
