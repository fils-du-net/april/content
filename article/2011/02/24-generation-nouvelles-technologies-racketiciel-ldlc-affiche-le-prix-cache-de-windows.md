---
site: Génération nouvelles technologies
title: "Racketiciel : LDLC affiche le prix caché de Windows"
author: Jérôme G.
date: 2011-02-24
href: http://www.generation-nt.com/racketiciel-ldlc-prix-windows-aful-actualite-1165451.html
tags:
- Entreprise
- Logiciels privateurs
- April
- Institutions
- Vente liée
- Associations
---

> Dans le domaine de la vente liée, le groupe Racketiciel de l'Aful salue l'initiative du vendeur en ligne LDLC qui affiche le prix de la licence Windows via une remise de 100 euros sur plusieurs ordinateurs portables.
