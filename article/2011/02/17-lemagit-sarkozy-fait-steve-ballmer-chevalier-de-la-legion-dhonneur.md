---
site: LeMagIT
title: "Sarkozy fait Steve Ballmer chevalier de la Légion d'Honneur "
author: Christophe Bardy
date: 2011-02-17
href: http://www.lemagit.fr/article/microsoft-sarkozy-ballmer/8156/1/sarkozy-fait-steve-ballmer-chevalier-legion-honneur/
tags:
- Entreprise
- Institutions
- Innovation
- International
---

> Le Président de la République a remis hier au président de Microsoft, de passage à Paris, les insignes de chevalier de la Légion d'Honneur. Il a plaidé pour une plus forte implantation de Microsoft en France et pour un contrat gagnant-gagnant entre l'éditeur et la France.
