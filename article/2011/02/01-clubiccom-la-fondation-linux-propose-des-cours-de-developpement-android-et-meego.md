---
site: clubic.com
title: "La fondation Linux propose des cours de développement Android et MeeGo"
author: Antoine Duvauchelle
date: 2011-02-01
href: http://pro.clubic.com/it-business/actualite-394574-fondation-linux-cours-developpement-android-meego.html
tags:
- Entreprise
- Associations
- Éducation
- Innovation
- International
---

> La fondation Linux a décidé d'aider les développeurs à faire leurs armes sur Android et MeeGo, deux des systèmes d'exploitation open-source du monde mobile. Elle souhaite ainsi accélérer l'adoption de son noyau, sur lequel les deux OS sont basés.
