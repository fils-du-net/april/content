---
site: LeMonde.fr
title: "Eric Besson, la neutralité du Net et les autoroutes de l'information"
author: La rédaction
date: 2011-02-09
href: http://www.lemonde.fr/technologies/article/2011/02/08/eric-besson-la-neutralite-du-net-et-les-autoroutes-de-l-information_1476719_651865.html
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Désinformation
- Neutralité du Net
- Europe
---

> Désengorger les "autoroutes de l'information" en instaurant des "voies prioritaires" : c'est l'une des mesures clés du projet qu'a dévoilé le ministre de l'économie numérique.
