---
site: Génération nouvelles technologies
title: "Allemagne : les affaires étrangères n'aiment plus le Libre"
author: Jérôme G.
date: 2011-02-18
href: http://www.generation-nt.com/allemagne-logiciel-libre-windows-affaires-etrangeres-actualite-1162131.html
tags:
- Logiciels privateurs
- Administration
- Interopérabilité
- Institutions
- Désinformation
- International
---

> Le ministère allemand des Affaires étrangères interrompt sa migration vers le logiciel libre dans ses bureaux pour revenir à une solution propriétaire à base Windows.
