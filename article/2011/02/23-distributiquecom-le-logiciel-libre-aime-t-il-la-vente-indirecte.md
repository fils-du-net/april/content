---
site: Distributique.com
title: "Le logiciel libre aime-t-il la vente indirecte ?"
author: Pascal Boiron
date: 2011-02-23
href: http://www.distributique.com/actualites/lire-le-logiciel-libre-aime-t-il-la-vente-indirecte-15997.html
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Licenses
---

> Le modèle de distribution du logiciel libre semblait incompatible avec celui que les grands éditeurs ont construit ces deux dernières décennies. Il évolue aujourd'hui vers l'agrégation de compétences autour des projets pour offrir une alternative à la vente de licences.
