---
site: Numerama
title: "Vers un site Data.gouv.fr pour l'Open Data de l'administration française"
author: Guillaume Champeau
date: 2011-02-22
href: http://www.numerama.com/magazine/18135-vers-un-site-datagouvfr-pour-l-open-data-de-l-administration-francaise.html
tags:
- Internet
- Administration
- Partage du savoir
- Institutions
- Vote électronique
- Open Data
---

> Le gouvernement devrait ouvrir à la fin de l'année son site data.gouv.fr, inspiré du data.gov américain, qui permettra aux administrés d'accéder aux données publiques de l'administration sous un format ouvert, réutilisable dans des applications innovantes. Le nombre des données et les conditions d'accès et d'exploitation restent néanmoins à déterminer.
