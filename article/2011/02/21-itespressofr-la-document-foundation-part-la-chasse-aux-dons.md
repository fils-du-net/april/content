---
site: ITespresso.fr
title: "La Document Foundation part à la chasse aux dons"
date: 2011-02-21
href: http://www.itespresso.fr/document-foundation-part-chasse-dons-41506.html
tags:
- Associations
- Brevets logiciels
- International
---

> Pour exister légalement en Allemagne, la Document Foundation, qui édite la suite bureautique open source LibreOffice, a besoin de réunir la somme de 50 000 euros. Un appel aux dons est lancé.
