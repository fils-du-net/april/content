---
site: RADIO NOTRE DAME
title: "« L'Open Source : une philosophie »"
author: Vincent Neymon
date: 2011-02-09
href: http://www.radionotredame.net/emission/espritdentreprise/2011-02-09
tags:
- Entreprise
- Internet
- Économie
---

> Jean-Pierre Archambault, Directeur de la Veille Technologique au CNDP-CRDP de Paris
