---
site: LeMagIT
title: "OpenJDK : Oracle et IBM se partagent la gouvernance"
author: Cyrille Chausson
date: 2011-02-07
href: http://www.lemagit.fr/article/ibm-java-oracle-gouvernance-openjdk/8078/1/openjdk-oracle-ibm-partagent-gouvernance/
tags:
- Entreprise
- Associations
---

> Oracle et IBM ont décidé de créer une gouvernance à deux d'OpenJDK, se partageant les pouvoirs dans l’implémentation Open Source de Java. Oracle garde toutefois la main-mise sur les décisions techniques.
