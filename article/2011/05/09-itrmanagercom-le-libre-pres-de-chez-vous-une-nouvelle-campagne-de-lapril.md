---
site: ITRmanager.com
title: "« Le Libre près de chez vous », une nouvelle campagne de l'April"
date: 2011-05-09
href: http://www.itrmanager.com/articles/118753/libre-pres-chez-nouvelle-campagne-april.html
tags:
- April
- Sensibilisation
---

> L'April lance, à l'occasion du salon « Solutions Linux 2011 », une nouvelle campagne de promotion du logiciel libre intitulée « Le Libre près de chez vous ». Le principe de cette campagne consiste à imaginer et à mener des actions locales permettant de toucher de nouveaux publics pour leur faire connaitre l'informatique libre. « Cette campagne a pour but d'inciter les membres des communautés du libre à réaliser des petites actions de sensibilisation dans leur entourage » a déclaré Lionel Allorge, administrateur de l'April.
