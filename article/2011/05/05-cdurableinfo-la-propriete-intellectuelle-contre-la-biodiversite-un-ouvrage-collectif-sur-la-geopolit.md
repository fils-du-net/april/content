---
site: CDURABLE.info
title: "La propriété intellectuelle contre la biodiversité ? Un ouvrage collectif sur la géopolitique de la diversité biologique"
date: 2011-05-05
href: http://cdurable.info/+La-propriete-intellectuelle-contre-la-biodiversite-Geopolitique-de-la-diversite-biologique-CETIM,1908+.html
tags:
- Partage du savoir
- International
---

> Cet ouvrage collectif, en établissant un parallèle entre ce qui se passe au Sud et au Nord, dresse un tableau critique des réglementations européennes sur les semences qui ont pour effet de réduire la biodiversité agricole et animale en Europe et présente des modèles alternatifs et participatifs de partage des ressources et des savoirs s’exerçant en dehors du marché et pouvant protéger efficacement la biodiversité, comme des modèles de type « open source ».
