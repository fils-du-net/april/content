---
site: Developpez.com
title: "Firefox : Ant Video Downloader trace les utilisateurs, Mozilla obligé de retirer l'extension populaire de sa galerie"
date: 2011-05-20
href: http://www.developpez.com/actu/32229/Firefox-Ant-Video-Downloader-trace-les-utilisateurs-Mozilla-oblige-de-retirer-l-extension-populaire-de-sa-galerie/
tags:
- Entreprise
- Internet
- Informatique-deloyale
---

> Son développeur refuse de s'expliquer, Mozilla retire l'extension aux 7 millions de téléchargements
