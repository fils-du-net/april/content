---
site: ICTjournal
title: "Plusieurs grands fabricants s’allient pour contrer VMware"
author: Rodolphe Koller
date: 2011-05-25
href: http://www.ictjournal.ch/fr-CH/News/2011/05/25/Plusieurs-grands-fabricants-sallient-pour-contrer-VMware.aspx
tags:
- Entreprise
- Logiciels privateurs
- Interopérabilité
- Associations
- Innovation
---

> HP, IBM, Red Hat, Intel, Eucalyptus et SUSE forment l’Open Virtualization Alliance pour promouvoir des solutions de virtualisation ouvertes.
