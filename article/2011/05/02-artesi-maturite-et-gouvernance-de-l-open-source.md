---
site: artesi
title: "Maturité et Gouvernance de l’Open Source"
date: 2011-05-02
href: http://www.artesi.artesi-idf.com/public/article/maturite-et-gouvernance-de-l%E2%80%99open-source.html?id=23270
tags:
- Entreprise
---

> Dans le rapport qu’il publie ce jour, le CIGREF présente non seulement les éléments de gouvernance mis en oeuvre dans les démarches Open Source, mais également un retour d’expérience du déploiement Open Office et surtout une évaluation de la maturité des solutions Open Source au sein des Grandes Entreprises utilisatrices.
