---
site: Numerama
title: "\"Copier n'est pas voler\", le documentaire de l'association Coagul"
author: Julien L.
date: 2011-05-20
href: http://www.numerama.com/magazine/18851-copier-n-est-pas-voler-le-documentaire-de-l-association-coagul.html
tags:
- Internet
- Associations
- Droit d'auteur
- Licenses
- Contenus libres
---

> Le documentaire "Copier n'est pas voler", réalisé par l'association Coagul, s'attache à démontrer que le partage à l'ère du numérique participe à l'enrichissement culturel de chacun. Tout au long du documentaire, différentes personnalités se succèdent pour partager leurs opinions sur le partage, la copie, le droit d'auteur ou encore les licences libres.
