---
site: Silicon.fr
title: "Paul Daugherty (Accenture) : « 78 % de nos clients utilisent des solutions open source »"
author: David Feugey
date: 2011-05-04
href: http://www.silicon.fr/paul-daugherty-accenture-%C2%AB-78-de-nos-clients-utilisent-des-solutions-open-source-%C2%BB-50982.html
tags:
- Entreprise
---

> Le succès de l’open source en entreprise est de plus en plus marqué. En témoignent les données dévoilées par Paul Daugherty, un des responsables du géant de l’intégration Accenture.
