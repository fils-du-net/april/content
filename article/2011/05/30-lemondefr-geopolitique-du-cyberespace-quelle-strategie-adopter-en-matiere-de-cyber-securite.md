---
site: LeMonde.fr
title: "Géopolitique du cyberespace : quelle stratégie adopter en matière de cyber-sécurité ?"
author: Maxime Pinard
date: 2011-05-30
href: http://www.lemonde.fr/idees/article/2011/05/30/geopolitique-du-cyberespace-quelle-strategie-adopter-en-matiere-de-cyber-securite_1529262_3232.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Économie
- Interopérabilité
- HADOPI
- Institutions
- International
---

> Le Printemps arabe, outre les conséquences politiques qu'il a entrainées, a été l'occasion de confirmer l'importance, voire la prépondérance des moyens de communication dans le déroulement des événements. Twitter, Facebook, Youtube et autres ont permis à tout un chacun de suivre en temps réel les révoltes, mais également d'agir en organisant des manifestations ou en dénonçant les exactions commises par les autorités. Ces dernières ont été clairement dépassées par ces entités du cyberespace, transfrontalier par définition et qui rend délicat toute tentative de neutralisation.
