---
site: Mediapart
title: "La science, le pouvoir et le peuple"
author: Clément Sénéchal
date: 2011-05-23
href: http://blogs.mediapart.fr/edition/bookclub/article/230511/la-science-le-pouvoir-et-le-peuple
tags:
- Économie
- Partage du savoir
---

> Dans un ouvrage monumental traduit en français pour la première fois, l'historien américain Clifford D. Conner propose une approche renouvelée, pour ne pas dire renversée, de l'histoire des sciences: face au temps événementiel de la découverte et des grands noms, il oppose le temps quotidien des artisans et des problèmes résolus pas à pas.
