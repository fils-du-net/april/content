---
site: Silicon.fr
title: "Thanh Nghiem décrypte l’ADN du mouvement libre et durable"
author: David Feugey
date: 2011-05-29
href: http://www.silicon.fr/thanh-nghiem-decrypte-l%E2%80%99adn-du-mouvement-libre-et-durable-52487.html
tags:
- Partage du savoir
---

> Une note pratique permettra à ceux qui sont intéressés par le concept libre et durable de l’appliquer à leurs projets.
