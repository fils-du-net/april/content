---
site: Numerama
title: "L'Hadopi n'a reçu que 3 réponses sur les logiciels de filtrage"
author: Guillaume Champeau
date: 2011-05-31
href: http://www.numerama.com/magazine/18917-l-hadopi-n-a-recu-que-3-reponses-sur-les-logiciels-de-filtrage.html
tags:
- Internet
- HADOPI
---

> Selon nos informations, seules trois réponses ont été reçues par l'Hadopi au terme des quarante jours de la consultation publique ouverte sur le nouveau projet de spécifications fonctionnelles des moyens de sécurisation qu'elle devra labelliser. Il y avait pourtant beaucoup à dire, et matière à s'inquiéter.
