---
site: LeMonde.fr
title: "La sécurité intérieure américaine demande le retrait d'un module Firefox"
date: 2011-05-06
href: http://www.lemonde.fr/technologies/article/2011/05/06/la-securite-interieure-americaine-demande-le-retrait-d-un-module-firefox_1517755_651865.html
tags:
- Internet
- Institutions
- Associations
---

> Le Department of Homeland Security (DHS), le service de sécurité intérieure des Etats-Unis, a demandé à la fondation Mozilla, qui édite notamment le navigateur Firefox, de retirer le module complémentaire MafiaaFire de son catalogue en ligne. Mafiaafire permet de contourner en partie les saisies de noms de domaines opérées par les autorités américaines, en redirigeant automatiquement l'utilisateur vers les copies du site hébergées ailleurs sur la Toile. WikiLeaks, par exemple, a hébergé de nombreuses versions miroir un peu partout sur Internet après la saisie de son nom de domaine.
