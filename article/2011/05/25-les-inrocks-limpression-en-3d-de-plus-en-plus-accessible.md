---
site: les inrocks
title: "L'impression en 3D, de plus en plus accessible"
author: Anne-Claire Norot
date: 2011-05-25
href: http://www.lesinrocks.com/medias/numerique-article/t/65406/date/2011-05-25/article/limpression-prend-du-relief/
tags:
- Entreprise
- Droit d'auteur
- Innovation
---

> Passant du virtuel au réel et de la science-fiction au quotidien, l'impression en 3D trouve toujours plus d'applications, ludiques, scientifiques et commerciales.
