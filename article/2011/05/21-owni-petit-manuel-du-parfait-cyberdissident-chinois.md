---
site: OWNI
title: "Petit manuel du parfait cyberdissident chinois"
author: Lucie Romano
date: 2011-05-21
href: http://owni.fr/2011/05/21/petit-manuel-du-parfait-cyberdissident-chinois-censure-vpn-firewall/
tags:
- Internet
- Partage du savoir
- Institutions
- International
---

> Face au Great Firewall qui filtre le web chinois, les cyberdissidents se saisissent des outils à disposition : VPN, proxy et autres ne sont pas de trop pour contourner les 30 à 40000 policiers qui surveillent la toile en Chine !
