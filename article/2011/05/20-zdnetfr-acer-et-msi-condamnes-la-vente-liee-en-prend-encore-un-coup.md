---
site: ZDNet.fr
title: "Acer et MSI condamnés : la vente liée en prend (encore) un coup !"
author: Pierrick Aubert
date: 2011-05-20
href: http://www.zdnet.fr/actualites/acer-et-msi-condamnes-la-vente-liee-en-prend-encore-un-coup-39760980.htm
tags:
- Entreprise
- April
- Institutions
- Vente liée
- Associations
- Europe
---

> En France, la vente liée est de plus en plus contestée. Les deux constructeurs informatiques ont à nouveau été condamnés pour avoir imposé des licences OS et logicielles sur des ordinateurs neufs.
