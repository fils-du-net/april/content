---
site: LeMonde.fr
title: "Vente liée d'ordinateurs et de logiciels : HP condamné pour pratique déloyale"
date: 2011-05-26
href: http://www.lemonde.fr/technologies/article/2011/05/26/vente-liee-d-ordinateurs-et-logiciels-hp-condamne-pour-pratique-deloyale_1527901_651865.html
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Vente liée
- Associations
---

> La cour d'appel de Versailles a donné raison à UFC-Que Choisir, qui contestait la vente liée d'ordinateurs aux logiciels préinstallés par Hewlett Packard France, a indiqué, jeudi 26 mai, l'association de consommateurs dans un communiqué. Dans un arrêt en date du 5 mai, dont l'Agence France-Presse a obtenu copie, la cour estime que la vente pas Hewlett Packard France "d'ordinateurs préinstallés […] constitue une pratique commerciale déloyale prohibée au sens de la directive du 11 mai 2005" sur les pratiques commerciales déloyales.
