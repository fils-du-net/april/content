---
site: 01net.
title: "Hadopi : « une loi folle », un projet « mort-né », « la destruction du Net »"
author: Stéphane Long
date: 2011-05-25
href: http://www.01net.com/editorial/533563/hadopi-loi-folle-projet-mort-ne-la-destruction-du-net/
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Droit d'auteur
- International
---

> Il n’y avait pas que des adeptes d’un « Internet civilisé » à l’e-G8. Les détracteurs de ce modèle promu par Nicolas Sarkozy étaient aussi présents. Petit verbatim.
