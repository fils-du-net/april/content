---
site: toolinux
title: "Solutions Linux 2011, comment va le logiciel libre ?"
author: Philippe SCOFFONI
date: 2011-05-17
href: http://www.toolinux.com/Solutions-Linux-2011-comment-va-le
tags:
- Entreprise
- Logiciels privateurs
- April
- Associations
---

> Pour mon premier salon Solution Linux et aussi ma première réelle occasion de me rendre sur un événement majeur du logiciel libre et de l’open source, je reviens l’esprit chargé de souvenirs de rencontres et d’impressions. Impressions qui parfois confortent des convictions acquises par le passé ou encore suscitent de nouveaux questionnements. Il en va ainsi du rôle et des relations entre le monde associatif ou communautaire et de celui des affaires des entreprises. Un écheveau dont les imbrications peuvent parfois laisser imaginer ou craindre de sulfureuses combinaisons.
