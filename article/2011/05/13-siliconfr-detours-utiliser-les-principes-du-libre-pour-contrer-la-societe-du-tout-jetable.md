---
site: Silicon.fr
title: "Détours : utiliser les principes du libre pour contrer la société du tout jetable"
author: David Feugey
date: 2011-05-13
href: http://www.silicon.fr/detours-utiliser-les-principes-du-libre-pour-contrer-la-societe-du-tout-jetable-51539.html
tags:
- Internet
- Économie
- Partage du savoir
---

> Appliquer au monde matériel les préceptes de l’open source. Voilà le moteur de l’institut Angenius et de sa très active présidente, Thanh Nghiem, golden girl de la durabilité et du libre.
