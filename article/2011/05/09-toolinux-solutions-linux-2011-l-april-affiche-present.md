---
site: toolinux
title: "Solutions Linux 2011 : l’APRIL affiche présent"
date: 2011-05-09
href: http://www.toolinux.com/Solutions-Linux-2011-l-APRIL
tags:
- April
- Sensibilisation
---

> L’April sera présente au salon « Solutions Linux 2011 », qui se tiendra cette semaine à Paris et n’a visiblement pas oublié certaines racines communautaires.
