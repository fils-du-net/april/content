---
site: OWNI
title: "Aux origines de la cyberculture: LSD et HTML"
author: Quentin Noirfalisse
date: 2011-05-04
href: http://owni.fr/2011/05/04/origines-cyberculture-lsd-html/
tags:
- Internet
- Institutions
- International
---

> Révolution de l'information, débats sur le libre et le payant, origines dans les contre-cultures américaines des années 60. La cyberculture a 30 ans, Geek Politics fait le point.
