---
site: 01net.
title: "Google a retiré plusieurs émulateurs de jeux vidéo de l'Android Market"
author: Pierre Fontaine
date: 2011-05-30
href: http://www.01net.com/editorial/533743/google-en-guerre-contre-les-emulateurs-sur-landroid-market/
tags:
- Entreprise
- Logiciels privateurs
- Licenses
---

> Google a retiré de sa plate-forme de téléchargements des émulateurs Nintendo. Censure ou application du règlement de l'Android Market ? La chasse aux émulateurs est-elle ouverte ?
