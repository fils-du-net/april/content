---
site: écrans
title: " Hadopi en mode passoire"
author: Camille Gévaudan
date: 2011-05-16
href: http://www.ecrans.fr/Hadopi-en-mode-passoire,12741.html
tags:
- Entreprise
- Internet
- HADOPI
---

> Les techniques de surveillance des réseaux peer-to-peer, un mot de passe en clair, des adresses IP d’internautes pris en flagrant délit de piratage, le nom de l’œuvre qu’ils ont téléchargée, une liste partielle des films surveillés... C’est un sacré pactole qu’a laissé fuiter Trident Media Guard (TMG), la société nantaise chargée de traquer les pirates pour le compte d’Hadopi. Ce week-end, le blogueur Olivier Laurelli — plus connu sous le pseudonyme de Bluetouff — a mis la main sur des milliers de données de ce genre. Et sans avoir piraté quoi que ce soit !
