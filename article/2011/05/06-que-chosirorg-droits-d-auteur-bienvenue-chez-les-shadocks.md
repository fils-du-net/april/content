---
site: QUE CHOSIR.org
title: "Droits d’auteur Bienvenue chez les Shadocks  "
author: Erwan Seznec
date: 2011-05-06
href: http://www.quechoisir.org/telecom-multimedia/image-son/musique/actualite-droits-d-auteur-bienvenue-chez-les-shadocks
tags:
- Entreprise
- Institutions
- Droit d'auteur
---

> Le huitième rapport annuel de la Commission permanente de contrôle des sociétés de perception et de répartition des droits décrit un système qui n’est au service ni des artistes, ni des consommateurs. Mais de lui-même.
