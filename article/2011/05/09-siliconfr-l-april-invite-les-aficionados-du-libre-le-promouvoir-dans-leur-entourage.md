---
site: Silicon.fr
title: "L’April invite les aficionados du libre à le promouvoir dans leur entourage"
date: 2011-05-09
href: http://www.silicon.fr/lapril-invite-les-aficionados-du-libre-a-le-promouvoir-dans-leur-entourage-51238.html
tags:
- April
- Sensibilisation
---

> Promouvoir le libre passe par des actions de lobbying mais aussi par le simple bouche-à-oreille.
