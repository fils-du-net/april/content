---
site: 01netPro.
title: " Le vent de la révolution de jasmin souffle sur Solutions Linux"
author: Alain Clapaud
date: 2011-05-13
href: http://pro.01net.com/editorial/532707/le-vent-de-la-revolution-de-jasmin-souffle-sur-solutions-linux/
tags:
- Entreprise
- Institutions
- Associations
- International
---

> La Tunisie était à l'honneur cette année sur le salon phare du logiciel libre. Un pavillon y accueillait 11 SSII tunisiennes, dont une délégation a pu rencontrer Eric Besson.
