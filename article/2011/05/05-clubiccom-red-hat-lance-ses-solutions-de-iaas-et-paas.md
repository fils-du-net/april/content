---
site: clubic.com
title: "Red Hat lance ses solutions de IaaS et PaaS"
author: Antoine Duvauchelle
date: 2011-05-05
href: http://pro.clubic.com/it-business/cloud-computing/actualite-418552-red-hat-solution-iaas-paas.html
tags:
- Entreprise
- Internet
- Associations
---

> Red Hat sort de sa sphère habituelle pour venir s'attaquer à Microsoft Azure ou le très récent VMware Cloud Foundry. L'éditeur open-source a présenté deux nouvelles solutions cloud au cours de sa conférence annuelle : une plateforme en mode hébergé (PaaS), et un service d'infrastructure à la demande (IaaS).
