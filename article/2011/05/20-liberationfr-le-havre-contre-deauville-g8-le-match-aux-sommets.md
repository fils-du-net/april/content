---
site: Libération.fr
title: "Le Havre contre Deauville: G8, le match aux sommets"
author: ELODIE AUFFRAY
date: 2011-05-20
href: http://www.liberation.fr/economie/01012338683-le-havre-contre-deauville-g8-le-match-aux-sommets
tags:
- Internet
- Économie
- Institutions
- International
---

> De part et d'autre de l'embouchure de la Seine, chefs d'Etat et altermondialistes se feront face, lors du G8.
