---
site: Le nouvel Economiste
title: "Open source vs logiciels propriétaires"
author: Alain Roux
date: 2011-05-04
href: http://www.lenouveleconomiste.fr/open-source-vs-logiciels-proprietaires-9122/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Interopérabilité
- Licenses
---

> Une manière de mutualiser les coûts de R&amp;D.
