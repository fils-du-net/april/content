---
site: Numerama
title: "Wikipedia veut être inscrit au Patrimoine Mondial de l'Humanité"
author: Guillaume Champeau
date: 2011-05-23
href: http://www.numerama.com/magazine/18861-wikipedia-veut-etre-inscrit-au-patrimoine-mondial-de-l-humanite.html
tags:
- Partage du savoir
- Institutions
- Associations
---

> Wikipedia lancera mardi une pétition pour sensibiliser l'Unesco à sa future demande d'inscription au Patrimoine de l'Humanité. Une première pour une oeuvre collective
