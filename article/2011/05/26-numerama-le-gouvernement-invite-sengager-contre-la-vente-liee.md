---
site: Numerama
title: "Le gouvernement invité à s'engager contre la vente liée"
author: Julien L.
date: 2011-05-26
href: http://www.numerama.com/magazine/18882-le-gouvernement-invite-a-s-engager-contre-la-vente-liee.html
tags:
- Logiciels privateurs
- Institutions
- Vente liée
- Associations
---

> L'UFC-Que Choisir appelle de nouveau le gouvernement à s'engager fermement contre la vente liée. L'association souligne les décisions de justice favorables au consommateur,
