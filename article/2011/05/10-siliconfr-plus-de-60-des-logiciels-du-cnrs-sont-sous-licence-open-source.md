---
site: Silicon.fr
title: "Plus de 60 % des logiciels du CNRS sont sous licence open source"
author: David Feugey
date: 2011-05-10
href: http://www.silicon.fr/plus-de-60-des-logiciels-du-cnrs-sont-sous-licence-open-source-51341.html
tags:
- Administration
- Droit d'auteur
- Innovation
- Licenses
---

> Avec Plume, le CNRS veut mutualiser les compétences informatiques, mais aussi dénombrer les différents logiciels développés au sein des laboratoires de recherche.
