---
site: Tunisie Haut débit
title: "Moncef Marzouki sur la censure en Tunisie : «C’est inefficace et contre-productif»"
author: Welid Naffati
date: 2011-05-29
href: http://www.tunisiehautdebit.com/index.php?option=com_content
tags:
- Entreprise
- Internet
- Administration
- Économie
- Institutions
- International
---

> Après le PDP et en attendant que Ennahdha termine l’élaboration de son programme électoral, la rédaction de THD s’est intéressée au programme politique du Congrès pour  la république (CPR). Vu la thématique du site, THD se focalisera uniquement, sur les points relatifs au secteur des TIC.
