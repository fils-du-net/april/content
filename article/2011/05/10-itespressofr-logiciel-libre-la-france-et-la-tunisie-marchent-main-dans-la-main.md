---
site: ITespresso.fr
title: "Logiciel libre : la France et la Tunisie marchent main dans la main"
date: 2011-05-10
href: http://www.itespresso.fr/logiciel-libre-france-tunisie-marchent-main-main-42678.html
tags:
- Économie
- Institutions
- Associations
- International
---

> Les gouvernements français et tunisien, ainsi que les deux associations professionnelles du Libre des deux pays, ont entériné la création d’un groupe de travail sur les coopérations industrielles franco-tunisiennes dans le secteur du logiciel libre.
