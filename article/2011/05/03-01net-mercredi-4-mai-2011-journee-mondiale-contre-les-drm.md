---
site: 01net.
title: "Mercredi 4 mai 2011, Journée mondiale contre les DRM"
author: Stéphane Long
date: 2011-05-03
href: http://www.01net.com/editorial/532277/mercredi-4-mai-2011-journee-mondiale-contre-les-drm/
tags:
- Entreprise
- April
- Associations
- DRM
---

> Comme l'année dernière, l'pril soutient l'initiative de la Free Software Foundation, qui milite contre les verrous numériques. Les sympathisants sont invités à publier des bannières de soutien sur leur site.
