---
site: 20minutes.fr
title: "Ubuntu, projet communautaire"
author: Christophe Séfrin
date: 2011-05-17
href: http://www.20minutes.fr/article/725116/ubuntu-projet-communautaire
tags:
- Entreprise
- Logiciels privateurs
---

> Il n'y a pas que Windows ou Mac OS dans la vie d'un ordinateur. Il y a aussi Ubuntu.
