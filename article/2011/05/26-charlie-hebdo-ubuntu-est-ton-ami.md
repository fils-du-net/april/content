---
site: CHARLIE HEBDO
title: "Ubuntu est ton ami"
author: Jack Kernel
date: 2011-05-26
href: http://www.charliehebdo.fr/zarzelettres-numerique-210411
tags:
- Économie
- Institutions
- Promotion
---

> Les membres des communautés du logiciel libre sont des êtres étranges, ils aiment tellement les animaux qu'ils les utilisent souvent comme logo de leurs logiciels ou pour en nommer les différentes versions. C'est ainsi que, fin avril, la toute dernière version d'Ubuntu a été baptisée Natty Narwhal, «Narval astucieux».
