---
site: gamers.fr
title: "Nintendo dans le collimateur de la Free Software Foundation - Gamers.fr"
author: sonof
date: 2011-05-19
href: http://www.gamers.fr/actus/2011/05/19/nintendo-dans-le-collimateur-de-la-free-software-foundation
tags:
- Entreprise
- Associations
- Licenses
---

> Le saviez-vous ? La Free Software Foundation est un organisme américain chargé de promouvoir les logiciels libres et la défense des utilisateurs
