---
site: LeMagIT
title: "L'Isoc France demande une modification de la composition du CNN"
author: Cyrille Chausson
date: 2011-05-02
href: http://www.lemagit.fr/article/france-numerique-conseil/8651/1/l-isoc-france-demande-une-modification-composition-cnn/
tags:
- Internet
- April
- Institutions
- Associations
---

> La représentativité du très controversé Conseil national du numérique est de nouveau critiquée. Après le Munci, le Geste, l’April et certains syndicats, c’est au tour du chapitre français de l’Internet Society (Isoc France) de monter au créneau et de pointer du doigt la composition du conseil, installé officiellement la 27 avril par Nicolas Sarkozy.
