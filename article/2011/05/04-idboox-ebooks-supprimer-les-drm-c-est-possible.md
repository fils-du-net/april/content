---
site: IDBOOX
title: "Ebooks : supprimer les DRM c’est possible"
date: 2011-05-04
href: http://www.idboox.com/ebook/infos-ebooks/ebooks-supprimer-les-drm-c-est-possible/
tags:
- April
- DRM
---

> ePUBee DRM Removal est un utilitaire conçu pour ceux qui en ont assez des DRM placées dans les livres numériques. Cet outil est facile à utiliser et permet en quelques clics de supprimer les body guards des ebooks au format ePub.
