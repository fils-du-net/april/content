---
site: ICTjournal
title: "Les alternatives à Office, un argument pour négocier avec Microsoft"
author: Rodolphe Koller
date: 2011-05-13
href: http://www.ictjournal.ch/fr-CH/News/2011/05/13/Les-suites-Office-en-ligne-un-argument-pour-negocier-avec-Microsoft.aspx
tags:
- Entreprise
- Logiciels privateurs
---

> Les entreprises s'intéressent aux suites bureautiques en ligne, mais elles s'en servent surtout pour mettre la pression sur Microsoft au moment de négocier les tarifs de licence MS Office.
