---
site: Les Echos
title: "Ces « Anonymes » qui font trembler le Web"
author: Solveig GODELUCK
date: 2011-05-09
href: http://www.lesechos.fr/entreprises-secteurs/tech-medias/actu/0201290863235.htm
tags:
- Entreprise
- Internet
---

> Sony les soupçonne d'être à l'origine du piratage récent de ses sites. Hackers ou sympahisants d'un jour, les sans-nom de l'Internet défigurent ou bloquent des sites Web sous prétexte de défendre la liberté d'expression. Le début d'une contestation mondiale ?
