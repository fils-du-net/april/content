---
site: LeMagIT
title: "Linux sur le desktop : les lecteurs du MagIT pointent du doigt la vente liée "
author: Cyrille Chausson
date: 2011-05-20
href: http://www.lemagit.fr/article/linux-poste-travail-os/8802/1/linux-sur-desktop-les-lecteurs-magit-pointent-doigt-vente-liee/
tags:
- Entreprise
- Logiciels privateurs
- Vente liée
- Associations
- International
---

> Suite à la publication d’un article démontrant le faible taux de pénétration de Linux sur le poste de travail dans le monde, les lecteurs du MagIT ont réagi en masse, pointant du doigt le principe de vente liée qui placerait Windows en position de force sur le marché. Nous en publions certains extraits.
