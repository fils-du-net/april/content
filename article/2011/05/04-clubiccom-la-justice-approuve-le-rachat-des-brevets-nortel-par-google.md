---
site: clubic.com
title: "La justice approuve le rachat des brevets Nortel par Google"
author: Antoine Duvauchelle
date: 2011-05-04
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/brevet-logiciel/actualite-418072-justice-approuve-rachat-brevets-nortel-google.html
tags:
- Entreprise
- Brevets logiciels
---

> Google pourrait mettre la main bientôt sur les brevets de Nortel, pour 900 millions de dollars. Le rachat a été approuvé par la justice des Etats-Unis, ainsi que par celle du Canada, dont dépend Nortel. La procédure d'enchères n'en est toutefois qu'à ses débuts, et RIM pourrait notamment renchérir.
