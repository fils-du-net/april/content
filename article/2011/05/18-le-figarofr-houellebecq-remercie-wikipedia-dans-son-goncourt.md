---
site: LE FIGARO.fr
title: "Houellebecq remercie Wikipédia dans son Goncourt "
author: Virginie Malbos 
date: 2011-05-18
href: http://www.lefigaro.fr/hightech/2011/05/18/01007-20110518ARTFIG00604-houellebecq-remercie-wikipedia-dans-son-goncourt.php
tags:
- Entreprise
- Associations
- Droit d'auteur
- Contenus libres
---

> Les éditions numériques et rééditions papier de La Carte et le territoire comprendront désormais des remerciements adressés à Wikipédia, dont les articles ont été cités à l'intérieur du roman.
