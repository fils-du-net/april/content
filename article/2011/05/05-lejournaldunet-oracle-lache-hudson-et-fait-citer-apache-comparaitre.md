---
site: LeJournalduNet
title: "Oracle lâche Hudson et fait citer Apache à comparaître"
date: 2011-05-05
href: http://www.journaldunet.com/solutions/systemes-reseaux/oracle-cede-hudson-a-eclipse-et-cite-a-comparaitre-apache-0511.shtml
tags:
- Entreprise
- Associations
- Droit d'auteur
---

> Oracle cède le développement d'Hudson à la fondation Eclipse et annonce en parallèle citer Apache à comparaître en tant que témoin dans son procès qui l'oppose à Google.
