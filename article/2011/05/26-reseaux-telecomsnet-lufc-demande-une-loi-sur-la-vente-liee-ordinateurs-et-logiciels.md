---
site: "Reseaux-Telecoms.net"
title: "L'UFC demande une loi sur la vente liée ordinateurs et logiciels"
date: 2011-05-26
href: http://www.reseaux-telecoms.net/actualites/lire-l-ufc-demande-une-loi-sur-la-vente-liee-ordinateurs-et-logiciels-23231.html
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
- Europe
---

> L'UFC-Que Choisir poursuit son combat contre la vente liée d'ordinateurs et de logiciels. Constatant l'absence d'information tarifaire distinguant ces coûts et le peu d'ordinateurs vendus nus en magasins, l'union demande au gouvernement une action législative en matière d'information séparée quant au prix de l'ordinateur et des logiciels et la vente découplée de l'ordinateur et du système d'exploitation.
