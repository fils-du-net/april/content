---
site: BE GEEK
title: "Un PC de la taille d’une clé USB et valant 17 euros !"
author: Rémy
date: 2011-05-06
href: http://www.begeek.fr/un-pc-de-la-taille-dune-cle-usb-et-valant-17-euros-36700
tags:
- Innovation
---

> Un ordinateur faisant la taille d’une clé USB pour 17 euros, vous n’y croyez pas ? Et bien ça existe.
