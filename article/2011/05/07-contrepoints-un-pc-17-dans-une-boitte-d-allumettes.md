---
site: Contrepoints
title: "Un PC à 17€ dans une boîtte d’allumettes"
author: Contrepoints
date: 2011-05-07
href: http://www.contrepoints.org/2011/05/07/24220-un-pc-a-17e-dans-une-boite-dallumettes
tags:
- Sensibilisation
- Innovation
---

> David Braben propose un ordinateur complet, gros comme une clé USB pour 17€.
