---
site: LePost
title: "Ubuntu-Linux l'etoile montante contre Windows8 la pompe a dollars !"
author: iwjcg
date: 2011-05-30
href: http://www.lepost.fr/article/2011/05/30/2509675_windows-8-l-occasion-de-briser-les-abus-de-microsoft.html
tags:
- Entreprise
- Logiciels privateurs
- Interopérabilité
- Promotion
---

> Win 3 ,3.1 ,W95,W98,W2000,Vista,W7,et ...W8  les OS de Redmond on un point commun : le code DOS acheté pour une bouchée de pain par Bill Gates . En fait ce bricolage de plus en plus "usine a gaz" n'a qu'un merite ; etre une formidable pompe à fric grace à des pratiques commerciales parfaitement illegales et agressives . La recente comdamnation de HP pour vente liée me semble partisane ,tous les fabricants d'ordinateurs et de materiel agissent de cette maniere.
