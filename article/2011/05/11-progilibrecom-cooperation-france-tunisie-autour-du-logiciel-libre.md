---
site: Progilibre.com
title: "Coopération France-Tunisie autour du Logiciel Libre"
author: Linagora
date: 2011-05-11
href: http://www.progilibre.com/Cooperation-France-Tunisie-autour-du-Logiciel-Libre_a1341.html
tags:
- Entreprise
- Institutions
- Associations
- International
---

> Le Ministre Français de l'Industrie, Eric Besson, reçoit une délégation d'entrepreneurs Tunisiens du Logiciel Libre et annonce la création d'un groupe de travail pour développer la coopération économique dans le secteur de l'Open Source.
