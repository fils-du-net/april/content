---
site: LeMagIT
title: "La Fondation Apache, citée à comparaitre dans le procès Oracle / Google "
author: Cyrille Chausson
date: 2011-05-05
href: http://www.lemagit.fr/article/google-java-oracle-android-proces/8684/1/la-fondation-apache-citee-comparaitre-dans-proces-oracle-google/
tags:
- Entreprise
- Institutions
- Associations
- Droit d'auteur
---

> Episode clé dans le procès qui oppose Oracle à Google pour viol de copyright Java dans Android. La firme de Larry Ellison a décidé de citer à comparaître la Fondation Apache afin que cette dernière apporte au dossier des documents liés à l’intégration d’ Harmony à Android.
