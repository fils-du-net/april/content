---
site: Pipelette.Com
title: "Utiliser des logiciels pirates, une habitude culturelle ?"
date: 2011-05-13
href: http://www.pipelette.com/news/geekerie/utiliser-des-logiciels-pirates-une-habitude-culturelle-2329.html
tags:
- Entreprise
- Logiciels privateurs
- HADOPI
- International
---

> Une étude qui s’étend sur 116 pays montre que la France possède une culture propre à l’utilisation de logiciels piratés. On estime que télécharger de tels logiciels montre son côté rebelle, mais les risques d’attaques informatiques augmentent si on les installe, car ils contiennent souvent des virus.
