---
site: Categorynet.com
title: "  Les bibliothèques sont enfin libres !"
author: NEDELEC
date: 2011-05-31
href: http://www.categorynet.com/communiques-de-presse/informatique/article-20110531159706/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Administration
---

> Les bibliothèques peuvent s'affranchir du système d'exploitation historique « Windows ». En effet, certaines solutions distribuées sous licence open source permettent de gérer les postes publics mis à disposition dans les espaces multimédia.
