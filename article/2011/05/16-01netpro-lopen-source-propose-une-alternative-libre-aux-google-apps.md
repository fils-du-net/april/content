---
site: 01netPro.
title: " L'open source propose une alternative libre aux Google Apps"
author: Alain Clapaud
date: 2011-05-16
href: http://pro.01net.com/editorial/532748/l-open-source-propose-une-alternative-libre-aux-google-apps/
tags:
- Entreprise
- Internet
---

> Le logiciel libre se mobilise pour fournir une alternative aux plates-formes Saas propriétaires. UNG Docs 1.0 veut rien moins que concurrencer la suite bureautique des Google Apps.
