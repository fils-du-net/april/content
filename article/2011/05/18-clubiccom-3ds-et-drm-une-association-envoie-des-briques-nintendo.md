---
site: clubic.com
title: "3DS et DRM : une association envoie des briques à Nintendo"
author: Audrey Oeillet
date: 2011-05-18
href: http://www.clubic.com/jeu-video/actualite-422552-3ds-drm-association-envoie-briques-nintendo.html
tags:
- Entreprise
- Associations
- Licenses
---

> Pour protester contre la politique adoptée par Nintendo autour de sa 3DS, qui ne peut être jailbreakée ou modifiée au risque de devenir inutilisable, une association américaine a lancé une campagne de protestation et demande aux personnes mécontente d'acheter des « briques » qui seront envoyées par la suite au siège de la firme.
