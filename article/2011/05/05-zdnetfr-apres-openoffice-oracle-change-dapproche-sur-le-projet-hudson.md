---
site: ZDNet.fr
title: "Après OpenOffice, Oracle change d'approche sur le projet Hudson"
author: Christophe Auffray
date: 2011-05-05
href: http://www.zdnet.fr/actualites/apres-openoffice-oracle-change-d-approche-sur-le-projet-hudson-39760518.htm
tags:
- Entreprise
- Associations
---

> L'attitude d'Oracle à l'égard de la communauté Hudson s'était traduite en février par la création d'un fork. L'éditeur refusait de céder le code source et la marque du projet. Il vient pourtant de les transférer à la fondation Eclipse.
