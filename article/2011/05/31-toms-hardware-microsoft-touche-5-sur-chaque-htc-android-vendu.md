---
site: tom's hardware
title: "Microsoft touche 5 $ sur chaque HTC Android vendu"
author: David Civera
date: 2011-05-31
href: http://www.presence-pc.com/actualite/Android-43882/
tags:
- Entreprise
- Brevets logiciels
---

> Walter Pritchard, un analyste chez Citi cité par Business Insider, vient de publier un rapport qui a fait grand bruit. Il affirme que Microsoft reçoit 5 $ pour chaque smartphone HTC Android vendu en raison d’un accord à l’amiable dans une affaire de violation de brevets l’opposant à Redmond.
