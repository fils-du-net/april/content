---
site: ITRmanager.com
title: "Les naturalistes de l'Open Source au Printemps du Libre"
date: 2011-05-19
href: http://www.itrmanager.com/articles/119173/naturalistes-open-source-printemps-libre.html
tags:
- Entreprise
- Économie
- Associations
---

> Le « Printemps du Libre », think tank où, précise le site du Conseil national du Libre, les professionnels du logiciel libre et de l'Open Source « débattent librement de 13h30 à 20h », s'est tenu la semaine dernière en prélude au salon Solutions Linux. Co-organisé par le CNLL, le Groupe Thématique Logiciel Libre (GTLL) du pôle Systematic et PLOSS, le réseau des acteurs du Logiciel Libre en Île de France, il visait à faire un état des lieux du Libre, particulièrement en France.
