---
site: Silicon.fr
title: "Cuba, premier pays utilisateur de Linux"
author: David Feugey
date: 2011-05-16
href: http://www.silicon.fr/cuba-premier-pays-utilisateur-de-linux-51664.html
tags:
- Internet
- International
---

> Linux progresse sur les postes de travail. Il est ainsi de plus en plus présent en Amérique centrale, en Amérique du Sud, en Afrique, ainsi qu’en Europe.
