---
site: lapresseaffaires
title: "Vers de nouveaux modèles d'affaires?"
author: Marlei Pozzebon et Thierry Gateau
date: 2011-05-09
href: http://lapresseaffaires.cyberpresse.ca/economie/201105/09/01-4397438-vers-de-nouveaux-modeles-daffaires.php
tags:
- Entreprise
- Internet
- Économie
- Partage du savoir
- Innovation
- Contenus libres
---

> (Montréal) «Il n'y a pas de création isolée.» - Gilberto Gil, ministre de la Culture du Brésil de 2003 à 2008.
