---
site: DegroupNews
title: "Samsung exige un droit de regard sur l'iPhone 5 et l'iPad 3 "
author: Arik Benayoun
date: 2011-05-30
href: http://www.degroupnews.com/actualite/n6414-samsung-apple-iphone-ipad-brevet.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
---

> Après la décision de justice qui a obligé Samsung à fournir à Apple des exemplaires de ses smartphones et de ses tablettes, le constructeur sud-coréen exige de recevoir des prototypes des futurs iPhone et iPad.
