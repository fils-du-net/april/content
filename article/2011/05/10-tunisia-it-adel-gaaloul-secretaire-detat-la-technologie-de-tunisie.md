---
site: Tunisia IT
title: "Adel Gaaloul (Secrétaire d'Etat à la Technologie de Tunisie)"
author: JDN
date: 2011-05-10
href: http://www.tunisiait.com/article.php?article=7293
tags:
- Institutions
- Associations
- International
---

> Adel Gaaloul est Secrétaire d'Etat auprès du ministre tunisien de l'Industrie chargé de la Technologie. JDN l'a interrogé en exclusivité lors de son voyage à Paris cette semaine. Le Secrétaire d'Etat nous a annoncé en avant première la signature d'une convention entre le Conseil National du Logiciel Libre et l'association nationale du logiciel Libre en Tunisie. Un partenariat qui doit être conclu à l'occasion du Salon Solutions Linux le 10 mai 2011, en présence du ministre chargé de l'Industrie, de l'Énergie et de l'Économie numérique, Eric Besson.
