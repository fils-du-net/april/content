---
site: indexel.net
title: "La France parmi les plus importants utilisateurs de Linux"
author: Alain Bastide
date: 2011-05-19
href: http://www.indexel.net/actualites/la-france-parmi-les-plus-importants-utilisateurs-de-linux-3363.html
tags:
- Entreprise
- International
---

> Le système d’exploitation libre équipe deux fois plus de postes de travail dans l’Hexagone qu’ailleurs dans le monde. Et ce chiffre devrait connaître une forte croissance avec le succès des smartphones sous Android.
