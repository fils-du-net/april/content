---
site: 01net.
title: "L'UFC-Que Choisir réclame une loi sur la vente liée  "
author: Coralie Cathelinais
date: 2011-05-26
href: http://www.01net.com/editorial/533668/l-ufc-que-choisir-en-guerre-contre-la-vente-liee/
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
- Europe
---

> L'association souhaite qu'une loi oblige les vendeurs à afficher la liste des logiciels préinstallés sur les ordinateurs et ce qu'ils coûtent. Elle envisage aussi que le consommateur ne paye que ceux qu'il aura choisi d'utiliser.
