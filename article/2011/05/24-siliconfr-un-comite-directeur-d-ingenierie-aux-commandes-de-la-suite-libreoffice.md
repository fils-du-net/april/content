---
site: Silicon.fr
title: "Un comité directeur d’ingénierie aux commandes de la suite LibreOffice"
author: David Feugey
date: 2011-05-24
href: http://www.silicon.fr/le-comite-directeur-d%E2%80%99ingenierie-de-libreoffice-est-maintenant-en-place-52168.html
tags:
- Associations
---

> Composé de contributeurs LibreOffice de renom, le comité directeur d’ingénierie de la Document Foundation sera chargé d’organiser le travail des développeurs.
