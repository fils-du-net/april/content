---
site: InformationWeek
title: "DOD Releases Open Source Development Guide"
author: Elizabeth Montalbano
date: 2011-05-19
href: http://www.informationweek.com/news/government/enterprise-apps/229503428
tags:
- Administration
- Licenses
- Contenus libres
- International
- English
---

> (Le guide militaire de 68 pages fournit "meilleurs pratiques" et autres ressources pour aider les développeurs à créer des logiciels utilisant des technologies ouvertes) The military's 68-page guide provides best practices and other resources to to help developers create software using open technologies.
