---
site: Pockett
title: "Nintendo peut piocher à distance les photos dans la mémoire de votre 3DS !"
author: P.
date: 2011-05-20
href: http://www.pockett.net/n10037_Nintendo_3DS_Nintendo_peut_piocher_a_distance_les_photos_dans_la_memoire_de_votre_3DS
tags:
- Entreprise
- Associations
---

> La Free Software Foundation (FSF) qui milite pour le logiciel libre (on lui doit GNU Linux) et contre les systèmes de protection abusifs s'est trouvée une nouvelle cible de choix : Nintendo et sa 3DS. En cause ses conditions d'utilisation qui, lues en détail, vont au-delà de ce qu'on aurait pu imaginer.
