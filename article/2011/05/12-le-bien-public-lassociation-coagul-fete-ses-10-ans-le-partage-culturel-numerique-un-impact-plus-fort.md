---
site: LE BIEN PUBLIC
title: "L'association COAGUL fête ses 10 ans : le partage culturel numerique, un impact plus fort que HADOPI"
author: S.DE
date: 2011-05-12
href: http://www.bienpublic.com/fr/accueil/article/5076593,1275/L-association-COAGUL-fete-ses-10-ans-le-partage-culturel-numerique-un-impact-plus-fort-que-HADOPI.html
tags:
- Partage du savoir
- HADOPI
- Associations
- Licenses
- Contenus libres
---

> L'association COAGUL fête ses 10 ans cette année sur le thème de l'activité de promotion du logiciel libre et des formats ouverts. A cette occasion, elle organise trois manifestations.
