---
site: MediaEtudiant.fr
title: "Logiciel libre : des économies pour les étudiants"
author: buenosdias
date: 2011-05-02
href: http://www.mediaetudiant.fr/vie-etudiante/logiciel-etudiant-libre-4597.php
tags:
- Logiciels privateurs
- Éducation
---

> Lorsqu'on est étudiant, on a souvent besoin de certains logiciels pour les devoir de notre longue et belle vie étudiante. Mais ils sont la plupart payant, j'ai trouvé une solution pour cela.
