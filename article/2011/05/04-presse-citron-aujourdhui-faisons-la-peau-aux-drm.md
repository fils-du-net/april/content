---
site: Presse Citron
title: "Aujourd'hui faisons la peau aux DRM"
author: Eric
date: 2011-05-04
href: http://www.presse-citron.net/aujourdhui-faisons-la-peau-aux-drm
tags:
- Internet
- April
- DRM
---

> Les DRM, ça existe encore ? On en parle peut-être un peu moins depuis que les consommateurs ont presque réussi à les dégager de l’industrie musicale, qui, à force de pressions, a fini par comprendre que ce n’était pas très bon pour son karma déjà bien pourri de l’intérieur, mais ailleurs…
