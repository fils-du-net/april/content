---
site: Tunisia IT
title: "Une délégation d'hommes d'affaires tunisiens à Paris"
date: 2011-05-13
href: http://www.tunisiait.com/article.php?article=7322
tags:
- Entreprise
- Économie
- Institutions
- International
---

> Le Secrétaire d’Etat chargé de la Technologie, en visite à Paris accompagné d’une délégation d’entrepreneurs et de représentants de la société civile pour promouvoir l’économie numérique en Tunisie.
