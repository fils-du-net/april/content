---
site: 01net.
title: "Logiciel libre : Microsoft ne combat plus l'open source"
author: Christophe Guillemin
date: 2011-05-16
href: http://www.01net.com/editorial/532777/microsoft-nous-ne-combattons-plus-l-open-source/
tags:
- Entreprise
- Logiciels privateurs
- Interopérabilité
- April
- Associations
- Standards
---

> Terminées, les comparaisons du logiciel libre avec le communisme ou le cancer. Microsoft affirme avoir enterré la hache de guerre. Il a rappelé ce message au salon Solutions Linux, mais sans convaincre les tenants du libre.
