---
site: LeMagIT
title: "L'OSI se modernise en s'ouvrant aux entreprises et aux institutions Open Source"
author: Cyrille Chausson
date: 2011-05-23
href: http://www.lemagit.fr/article/gouvernance-opensource/8811/1/l-osi-modernise-ouvrant-aux-entreprises-aux-institutions-open-source/
tags:
- Entreprise
- Associations
- Brevets logiciels
- Licenses
---

> L’Open Source Initiative a décidé de modifier sa participation dans l’éco-système Open Source en y jouant une pièce centrale. Ouverture aux entreprises, liens avec les autres institutions Open Source, gouvernance calquée sur le modèle Apache, l’OSI table sur la légitimité de sa marque pour devenir un interlocuteur privilégié de l’Open Source.
