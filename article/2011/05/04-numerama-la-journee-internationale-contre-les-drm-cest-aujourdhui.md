---
site: Numerama
title: "La Journée internationale contre les DRM, c'est aujourd'hui"
author: Julien L.
date: 2011-05-04
href: http://www.numerama.com/magazine/18692-la-journee-internationale-contre-les-drm-c-est-aujourd-hui.html
tags:
- April
- Institutions
- Associations
- DADVSI
- DRM
- Droit d'auteur
---

> Comme chaque année, la Free Software Foundation et l'April se mobilisent contre les verrous numériques. Ces mesures techniques de protection (DRM) visent à limiter l'accès aux fichiers numériques en contrôlant l'usage au sein du cercle privé. Si ces techniques tendent à reculer dans le secteur musical, elles sont encore présentes dans de nombreux autres domaines.
