---
site: OWNI
title: "Mode d’emploi pour démarrer une civilisation"
author: aKa
date: 2011-05-18
href: http://owni.fr/2011/05/18/mode-demploi-pour-demarrer-une-civilisation/
tags:
- Partage du savoir
- Innovation
- Contenus libres
---

> Et si l'esprit du libre qui est déjà en place dans la société de la connaissance et de la créativité pouvait se propager à l'univers du matériel, des outils du fermier, de l’ouvrier, de l’entrepreneur ou du producteur.
