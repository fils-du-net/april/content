---
site: écrans
title: "« Sarkozy a tenté de planter le drapeau français sur Internet »"
author: Alexandre Hervaud
date: 2011-05-27
href: http://www.ecrans.fr/Sarkozy-a-tente-de-planter-le,12840.html
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Brevets logiciels
- International
---

> Mardi dernier, dans notre compte-rendu du discours de Nicolas Sarkozy en ouverture du forum e-G8, on s’était fait l’écho de l’échange entre le Président de la République et le journaliste américain Jeff Jarvis. Ce dernier, spécialiste du Net et auteur de la Machine Google, parue en 2009, avait brièvement tenté d’expliquer au Président son souhait de le voir prêter le serment d’Hippocrate adapté au Net, qu’on pourrait résumer à : ne lui faites pas de mal.
