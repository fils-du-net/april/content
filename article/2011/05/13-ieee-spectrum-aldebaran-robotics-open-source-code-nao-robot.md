---
site: IEEE Spectrum
title: "Aldebaran Robotics To Open Source Code of Nao Robot"
author: Erico Guizzo
date: 2011-05-13
href: http://spectrum.ieee.org/automaton/robotics/robotics-software/aldebaran-robotics-to-open-source-nao-robot
tags:
- Entreprise
- Innovation
---

> (le petit humanoïde Français va révéler son code) The little French humanoid is going to reveal its code
