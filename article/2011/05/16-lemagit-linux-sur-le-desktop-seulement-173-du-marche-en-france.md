---
site: LeMagIT
title: "Linux sur le desktop : seulement 1,73% du marché en France "
author: Cyrille Chausson
date: 2011-05-16
href: http://www.lemagit.fr/article/france-linux-poste-travail-open-source/8757/1/linux-sur-desktop-seulement-marche-france/
tags:
- Institutions
- International
---

> Le desktop semble toujours bouder Linux qui dans le monde ne totalise que peu de parts de marché sur ce segment. Lui qui peut se targuer d’un fort taux de pénétration dans les serveurs. En France, Linux ne concerne que 1,73% de parts de marché des OS pour poste de travail. Au dessus toutefois de la moyenne mondiale, à 0,76% et européenne, à 1,14%.
