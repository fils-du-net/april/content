---
site: Numerama
title: "La RIAA et LimeWire signent un accord à 105 millions de dollars"
author: Julien L.
date: 2011-05-13
href: http://www.numerama.com/magazine/18788-la-riaa-et-limewire-signent-un-accord-a-105-millions-de-dollars.html
tags:
- Entreprise
- Internet
- Institutions
- Droit d'auteur
---

> C'est sans doute l'épilogue de la bataille judiciaire entre la RIAA et LimeWire. Les deux parties, engagées dans un procès depuis 2006, ont conclu un accord cette semaine.
