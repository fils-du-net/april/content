---
site: Numerama
title: "4 milliards d'euros pourraient être économisés grâce au logiciel libre, selon un rapport"
author: Julien L.
date: 2011-03-11
href: http://www.numerama.com/magazine/18272-4-milliards-d-euros-pourraient-etre-economises-grace-au-logiciel-libre-selon-un-rapport.html
tags:
- Économie
- Institutions
- International
---

> Un rapport néerlandais estime que la migration vers le logiciel libre pourrait faire économiser jusqu'à 4 milliards d'euros par an aux Pays-Bas. Toutefois, aucune volonté ne semble désireuse de transformer les mesures du document en action politique.
