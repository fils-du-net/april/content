---
site: clubic.com
title: "Anti-trust : la justice américaine enquête sur le groupe MPEG LA"
author: Guillaume Belfiore
date: 2011-03-04
href: http://www.clubic.com/television-tv/video-streaming/actualite-402230-anti-trust-justice-americaine-enquete-mpeg.html
tags:
- Entreprise
- Internet
- Institutions
- Brevets logiciels
- Video
---

> Après avoir reçu une plainte pour abus de position dominante, le département de la justice américain a ouvert un enquête sur la société MPEG LA détenant les droits sur son codec H.264, notamment utilisé pour la vidéo en streaming sur Internet.
