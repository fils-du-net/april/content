---
site: Progilibre.com
title: "Etude Cigref : Les grandes entreprises encore très réticentes vis à vis de l’open source applicatif"
author: Philippe Nieuwbourg
date: 2011-03-29
href: http://www.progilibre.com/Etude-Cigref-Les-grandes-entreprises-encore-tres-reticentes-vis-a-vis-de-l-open-source-applicatif_a1300.html
tags:
- Entreprise
---

> Le Cigref vient de publier une étude complète sur le thème « maturité et gouvernance de l’open source ». Une étude qui s’appuie sur la perception des grandes entreprises françaises vis à vis de cette catégorie d’outils informatique.
