---
site: MidiLibre.com
title: "Avec « Libre en fête », vive la liberté pour les logiciels"
author: XR
date: 2011-03-25
href: http://www.midilibre.com/articles/2011/03/25/VILLAGES-Avec-Libre-en-fete-vive-la-liberte-pour-les-logiciels-1573940.php5
tags:
- Internet
- Associations
- Promotion
---

> « Un logiciel libre c'est comme une recette de cuisine. » Pour un béotien de l'informatique, la comparaison peut paraître saugrenue. Aussi Stéphane de Labrusse et Francis Bordes, respectivement président et secrétaire de l'association ruthénoise à vocation départementale, Aru2L (lire ci-contre) s'empresse d'expliquer leur pensée.
