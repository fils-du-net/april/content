---
site: SUD OUEST
title: "Libre comme la toile"
author: Christian ESPLANDIU
date: 2011-03-10
href: http://www.sudouest.fr/2011/03/10/libre-comme-la-toile-338126-2729.php
tags:
- April
- Promotion
---

> Initiée et coordonnée par l'April, l'initiative Libre en fête est relancée pour la neuvième année consécutive pour accompagner l'arrivée du printemps.
