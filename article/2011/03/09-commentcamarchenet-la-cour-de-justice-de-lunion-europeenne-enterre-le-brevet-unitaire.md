---
site: Commentçamarche.net
title: "La Cour de justice de l'Union européenne enterre le brevet unitaire"
date: 2011-03-09
href: http://www.commentcamarche.net/news/5854440-la-cour-de-justice-de-l-union-europeenne-enterre-le-brevet-unitaire
tags:
- April
- Institutions
- Brevets logiciels
- Europe
---

> La Cour de justice de l'Union Européenne a retoqué hier le projet de loi débattu par le Parlement européen et visant à instituer un « brevet unitaire » valable dans tous les pays de l'Union et qui protégerait donc une innovation dans l'ensemble des les 27 pays de l'Union. Pour la Cour de justice de l'Union Européenne (CJUE), le projet est incompatible avec les fondements mêmes de l'Union Européenne.
