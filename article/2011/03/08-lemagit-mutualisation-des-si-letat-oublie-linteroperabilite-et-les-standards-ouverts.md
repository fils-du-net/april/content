---
site: LeMagIT
title: "Mutualisation des SI : l'Etat oublie l'interoperabilité et les standards ouverts  "
date: 2011-03-08
href: http://www.lemagit.fr/article/france-formats-interoperabilite-europe-april-etat-opensource-commission-disic/8276/1/mutualisation-des-etat-oublie-interoperabilite-les-standards-ouverts
tags:
- Entreprise
- Administration
- Interopérabilité
- April
- Institutions
- Marchés publics
- Standards
- Europe
---

> L'administration française, eldorado de l'Open Source. Une affirmation qu'il faudra bientôt conjuguer au passé ? Une association de défense du logiciel libre s'émeut en effet que la toute jeune direction interministérielle des SI ne se voit confier aucune mission en matière de respect de l'interoperabilité des systèmes ou de recours aux standards ouverts.
