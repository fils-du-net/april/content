---
site: datanews
title: "La Cour des comptes néerlandaise ne peut chiffrer les économies open source"
author: Stefan Grommen
date: 2011-03-15
href: http://datanews.rnews.be/fr/ict/actualite/apercu/2011/03/15/la-cour-des-comptes-neerlandaise-ne-peut-chiffrer-les-economies-open-source/article-1194968360189.htm
tags:
- Administration
- Institutions
- International
---

> Au terme de six mois d’enquête, l’Algemene Rekenkamer van Nederland ne peut toujours pas déterminer combien l’open source pourrait permettre aux autorités d’économiser.
