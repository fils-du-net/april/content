---
site: TOUTE lEurope
title: "La création du brevet européen n'est pas un long fleuve tranquille"
date: 2011-03-09
href: http://www.touteleurope.eu/fr/divers/toutes-les-informations/article/afficher/fiche/5117/t/70796/from/2890/breve/la-creation-du-brevet-europeen-nest-pas-un-long-fleuve-tranquille.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Europe
---

> Alors que le brevet européen semblait enfin aboutir politiquement, une décision de la Cour de Justice de l'Union européenne remet en cause sa mise en place. La Cour de Luxembourg a rejeté mardi 8 mars toute création d'une juridiction spécifique, en dehors du cadre institutionnel de l'Union, pour gérer les litiges sur des brevets. Pour Michel Barnier, cette question du tribunal est indépendante de celle du brevet unique européen.
