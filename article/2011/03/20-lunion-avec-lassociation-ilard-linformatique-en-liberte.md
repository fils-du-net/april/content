---
site: lunion
title: "Avec l'association ILArd L'informatique en liberté"
date: 2011-03-20
href: http://www.lunion.presse.fr/article/autres-actus/avec-lassociation-ilard-linformatique-en-liberte
tags:
- Associations
- Promotion
---

> Qu'est ce qu'un logiciel libre ? À quoi ça sert ? Où les trouver ? Voilà des questions auxquelles les Vouzinois ont pu trouver réponses mardi dernier à la bibliothèque Les Tourelles de Vouziers
