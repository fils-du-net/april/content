---
site: Developpez.com
title: "Windows Phone 7 : Microsoft autorise des applications open-source sur le Marketplace, la galerie approche les 10.000 applications"
date: 2011-03-09
href: http://www.developpez.com/actu/29390/Windows-Phone-7-Microsoft-autorise-des-applications-open-source-sur-le-Marketplace-la-galerie-approche-les-10-000-applications/
tags:
- Entreprise
- Licenses
---

> Microsoft a annoncé une mise à jour du Marketplace pour Windows Phone 7 avec une amélioration de certaines politiques et programme d’ouverture de galerie d’ applications pour les développeurs.
