---
site: Numerama
title: "Cantonales : les candidats de gauche plus favorables au Pacte du logiciel libre ?"
author: Julien L.
date: 2011-03-10
href: http://www.numerama.com/magazine/18260-cantonales-les-candidats-de-gauche-plus-favorables-au-pacte-du-logiciel-libre.html
tags:
- Internet
- April
- HADOPI
- Institutions
- DADVSI
---

> À dix jours du premier tour des élections cantonales de 2011, le nombre de candidats ayant signé le Pacte du logiciel libre est monté à 182. Cependant, la droite est largement sous-représentée par rapport aux politiques de gauche. Est-ce à dire que les candidats de gauche sont plus sensibles aux enjeux du logiciel libre ?
