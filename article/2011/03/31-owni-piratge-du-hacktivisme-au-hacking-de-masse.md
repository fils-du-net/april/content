---
site: OWNI
title: "Pirat@ge: du hacktivisme au hacking de masse"
author: Capucine Cousin
date: 2011-03-31
href: http://owni.fr/2011/03/31/piratge-du-hacktivisme-au-hacking-de-masse/
tags:
- Entreprise
- Internet
- Partage du savoir
- Associations
---

> Ils sont quatre, dont trois frères, jeunes et (à première vue ;) innocents, et leur clip, “Double Rainbow song“, bidouillé non pas au fond d’un garage mais dans le salon familial, avec un piano, a attiré plus de 20 millions de visiteurs. Un clip parodique qui a généré un buzz énorme, à partir d’une simple vidéo amateur d’un homme à la limite de la jouissance devant un phénomène rare : deux arcs en ciel… Au point – le comble – que Microsoft a recruté le “Double Rainbow guy” pour sa nouvelle pub pour Windows Live Photo Gallery. Ou quand l’industrie pirate les pirates…
