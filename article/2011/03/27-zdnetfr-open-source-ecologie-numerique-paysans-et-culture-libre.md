---
site: ZDNet.fr
title: "Open Source Ecologie, numérique, paysans et culture libre"
author: Thierry Noisette
date: 2011-03-27
href: http://www.zdnet.fr/blogs/l-esprit-libre/open-source-ecologie-numerique-paysans-et-culture-libre-39759427.htm
tags:
- Internet
- Innovation
- Promotion
- Contenus libres
---

> L'open ou le libre essaime dans tous les domaines, relève Alexis Kauffmann sur Framablog, qui note l'apparition d'un intéressant mouvement utilisant une méthodologie open source pour une activité paysanne libre.
