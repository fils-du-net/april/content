---
site: clubic.com
title: "Yahoo : acquisitions et publications open source en vue"
author: Guillaume Belfiore
date: 2011-03-29
href: http://pro.clubic.com/entreprises/yahoo/actualite-407532-yahoo-acquisitions-publications-open-source-vue.html
tags:
- Entreprise
- Internet
---

> Après avoir enchaîné plusieurs restructurations internes et fermé bon nombre de ses services, le portail Yahoo! amorcerait-il un second souffle ?
