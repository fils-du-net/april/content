---
site: ITRmanager.com
title: "La Cour de justice européenne enterre la juridiction unifiée relative aux brevets"
date: 2011-03-09
href: http://www.itrmanager.com/articles/116198/cour-justice-europeenne-enterre-juridiction-unifiee-relative-brevets.html
tags:
- April
- Institutions
- Brevets logiciels
- Europe
---

> Mardi 8 mars 2011, la Cour de justice de l'Union européenne (CJUE) a rendu son avis sur la conformité du projet de juridiction des brevets (JB) aux traités de l'Union européenne (UE).Cet avis était fortement attendu car la mise en place d'une juridiction unifiée est un pilier du brevet unitaire actuellement en débat à Bruxelles. Comme l'avait escompté l'April, la CJUE retoque sèchement le projet, en soulignant son incompatibilité avec les fondamentaux de l'Union européenne.
