---
site: LeJournalduNet
title: "Richard Stallman fustige le SaaS et Cloud Computing"
date: 2011-03-09
href: http://www.journaldunet.com/solutions/systemes-reseaux/richard-stallman-contre-le-cloud-computing-0311.shtml
tags:
- Internet
- Administration
- Philosophie GNU
---

> Nouvelle sortie du pape du Logiciel Libre, qui fustige notamment le choix de certaines administrations qui ont opté pour des solutions SaaS.
