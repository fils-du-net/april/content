---
site: ICTjournal
title: "Open source: le TF tranche aussi en faveur de Microsoft"
author: Nicolas Paratte
date: 2011-03-30
href: http://www.ictjournal.ch/fr-CH/News/2011/03/30/Open-source-le-Tribunal-federal-tranche-aussi-en-faveur-de-Microsoft.aspx
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Institutions
- Associations
- Marchés publics
- International
---

> Le second recours déposé en octobre 2010 pour défendre l’utilisation du logiciel libre au sein de l’Administration fédérale n’a pas abouti. Le Tribunal fédéral valide finalement le contrat de 42 millions conclu avec Microsoft en 2009.
