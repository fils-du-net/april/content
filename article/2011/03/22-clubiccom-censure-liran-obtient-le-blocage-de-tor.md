---
site: clubic.com
title: "Censure : l'Iran obtient le blocage de Tor"
author: Olivier Robillart
date: 2011-03-22
href: http://www.clubic.com/antivirus-securite-informatique/virus-hacker-piratage/reseau-tor-the-onion-router/actualite-406088-censure-iran-obtient-fermeture-tor.html
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- International
---

> Signe que le pays mène une censure active des comportements sur Internet, le réseau Tor (The Onion router) aurait été indisponible en Iran. Les autorités officielles auraient ainsi utilisé la méthode du DPI (Deep Packet Inspection) pour trier les internautes…
