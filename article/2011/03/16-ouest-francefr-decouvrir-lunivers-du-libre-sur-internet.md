---
site: "ouest-france.fr"
title: "Découvrir l'univers du « libre » sur Internet "
date: 2011-03-16
href: http://www.ouest-france.fr/actu/actuLocale_-Decouvrir-l-univers-du-libre-sur-Internet-_-1728374------61169-aud_actu.Htm
tags:
- Internet
- Promotion
---

> Dans le cadre de la fête de l'Internet, les Espaces publics numériques du pays de Flers proposentune série de rendez-vous autour du thème : « Libre en fête », du 19 au 26 mars.
