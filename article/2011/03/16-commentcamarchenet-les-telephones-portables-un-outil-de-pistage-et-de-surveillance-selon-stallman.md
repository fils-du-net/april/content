---
site: Commentçamarche.net
title: "Les téléphones portables : un outil de pistage et de surveillance selon Stallman"
author: Yves.Drothier
date: 2011-03-16
href: http://www.commentcamarche.net/news/5854523-les-telephones-portables-un-outil-de-pistage-et-de-surveillance-selon-stallman
tags:
- Associations
- Informatique-deloyale
- Innovation
---

> Ces propos assez extrêmes ont été tenus par Richard Stallman, le président de l'association Free Software Fondation qui cherche à promouvoir les logiciels libres, et le créateur du système d'exploitation GNU. Ce grand défenseur de la licence libre GPL...
