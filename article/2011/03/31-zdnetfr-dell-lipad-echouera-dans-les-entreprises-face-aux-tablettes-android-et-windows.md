---
site: ZDNet.fr
title: "Dell : \"L'iPad échouera dans les entreprises face aux tablettes Android et Windows\""
date: 2011-03-31
href: http://www.zdnet.fr/actualites/dell-l-ipad-echouera-dans-les-entreprises-face-aux-tablettes-android-et-windows-39759544.htm
tags:
- Entreprise
---

> Le directeur marketing de Dell, Andy Lark, estime que sur le long terme, la tablette Apple finira par être dépassée par des produits aux environnements ouverts. Le constructeur a choisi d'attaquer les entreprises par une "stratégie multi OS"
