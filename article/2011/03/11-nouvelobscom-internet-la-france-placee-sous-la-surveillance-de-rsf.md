---
site: nouvelObs.com
title: "Internet : la France placée sous la surveillance de RSF"
author: Boris Manenti
date: 2011-03-11
href: http://tempsreel.nouvelobs.com/actualite/media/20110311.OBS9511/internet-la-france-placee-sous-la-surveillance-de-rsf.html
tags:
- Internet
- HADOPI
- Institutions
- DADVSI
- International
---

> "Hadopi, Loppsi, internet civilisé... La liberté d'expression est menacée", prévient RSF qui publie son rapport sur les "ennemis d'Internet".
