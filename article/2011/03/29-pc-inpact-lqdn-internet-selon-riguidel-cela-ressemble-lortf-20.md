---
site: PC INpact
title: "LQDN : \"Internet selon Riguidel, cela ressemble à l'ORTF 2.0 !\""
author: Marc Rees
date: 2011-03-29
href: http://www.pcinpact.com/actu/news/62734-jeremie-zimermann-quadrature-riguidel-hadopi.htm
tags:
- Entreprise
- Internet
- Interopérabilité
- HADOPI
- Institutions
- Associations
- Neutralité du Net
- Europe
- International
- ACTA
---

> Le Pr Michel Riguidel, l'expert Hadopi en matière de filtrage et logiciel de sécurisation, a publié en fin de semaine dernière une tribune dans le Monde (notre analyse). Nous avons questionné Jérémie Zimmermann pour recueillir sa réaction. L’occasion de revenir sur les sujets suivis de près par La Quadrature du Net mais également sur l’avenir de cette initiative citoyenne.
