---
site: LeMagIT
title: "Mozilla libère Firefox 4 pour contenir Chrome"
author: Cyrille Chausson
date: 2011-03-23
href: http://www.lemagit.fr/article/navigateur-firefox-mozilla-opensource/8386/1/mozilla-libere-firefox-pour-contenir-chrome/
tags:
- Internet
- Logiciels privateurs
---

> Une semaine après le lancement de IE 9, Mozilla décoche la version 4 de Firefox, pour Windows, Mac et Linux. Une version peaufinée par Mozilla qui a dû traverser pas moins de 12 étapes de bêtas publiques, deux Release Candidates. Soit près d'un an de gestation avant d'accoucher d’un navigateur qui doit porter haut les couleurs de Firefox sur un marché en pleine agitation.
