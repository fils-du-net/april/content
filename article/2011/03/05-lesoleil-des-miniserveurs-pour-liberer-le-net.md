---
site: leSoleil
title: "Des miniserveurs pour libérer le Net"
author: Pierre Asselin
date: 2011-03-05
href: http://www.cyberpresse.ca/le-soleil/actualites/politique/201103/04/01-4376319-des-miniserveurs-pour-liberer-le-net.php
tags:
- Internet
- Institutions
- International
---

> (Québec) Le Net est devenu, en l'espace de quelques semaines, un nouveau champ de bataille. Les réseaux sociaux jouent un rôle stratégique dans les soulèvements populaires des derniers mois, mais c'est une arme qui peut se retourner contre la population, selon Eben Moglen, professeur de droit à l'Université Columbia.
