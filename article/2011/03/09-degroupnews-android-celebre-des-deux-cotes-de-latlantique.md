---
site: DegroupNews
title: "Android célébré des deux côtés de l'Atlantique "
author: Stéphane Caruana
date: 2011-03-09
href: http://www.degroupnews.com/actualite/n6057-android-google-mobilite-smartphone-telephonie.html
tags:
- Entreprise
- International
---

> Le système d'exploitation mobile open source de Google, Android, récolte des lauriers de part et d'autre de l'Atlantique. Dépassant tous ses concurrents aux Etats-Unis, il affiche une progression insolente en Europe.
