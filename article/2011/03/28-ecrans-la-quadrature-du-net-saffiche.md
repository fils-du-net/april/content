---
site: écrans
title: "La Quadrature du Net s'affiche"
author: Camille Gévaudan
date: 2011-03-28
href: http://www.ecrans.fr/La-Quadrature-du-Net-s-affiche,12363.html
tags:
- Internet
- HADOPI
- Institutions
- Associations
---

> « Je soutiens la Quadrature du Net car ils défendent nos libertés face aux gouvernements qui livrent une guerre anti-partage. J’espère que vous les soutiendrez également. » Quand c’est Richard Stallman himself qui le dit, tout barbu qu’il soit, l’appel revêt immédiatement une certaine classe. Le gourou du militantisme pour les logiciels libres a prêté son nom à La Quadrature du Net pour promouvoir sa campagne de financement 2011, aux côtés de Tristan Nitot, président de Mozilla Europe, et quelques internautes anonymes mais sensibles à la cause.
