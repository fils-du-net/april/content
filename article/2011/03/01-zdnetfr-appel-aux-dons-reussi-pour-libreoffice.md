---
site: ZDNet.fr
title: "Appel aux  dons réussi pour LibreOffice"
date: 2011-03-01
href: http://www.zdnet.fr/actualites/appel-aux-dons-reussi-pour-libreoffice-39758637.htm
tags:
- Associations
- International
---

> Appel aux  dons réussi pour LibreOffice - Grâce aux 50.000 euros de dons, la Document Fondation va pouvoir s'implanter légalement en Allemagne. Les nouvelles donations serviront à couvrir les dépenses de fonctionnement de la fondation.
