---
site: 20minutes.fr
title: "Le gourou du logiciel libre s'attaque aux mobiles..."
author: P.B.
date: 2011-03-16
href: http://www.20minutes.fr/article/687828/high-tech-richard-stallman-telephone-portable-le-reve-staline
tags:
- Innovation
- Licenses
- Philosophie GNU
---

> On ne trouve sans doute pas plus jusqu'au-boutiste que lui: Richard Stallman, c'est l'un des gourous du logiciel libre, notamment papa de la licence GNU/GPL et fondateur de la Free Software Foundation.
