---
site: LADEPECHE.fr
title: "Gratentour. Les logiciels libres en fête"
date: 2011-03-18
href: http://www.ladepeche.fr/article/2011/03/18/1037483-Gratentour-Les-logiciels-libres-en-fete.html
tags:
- April
- Philosophie GNU
- Promotion
---

> Dans le cadre de l'initiative nationale Libre en Fête, les ateliers informatiques de la mairie organisent ce samedi 19 mars, à partir de 9 heures, une journée de découverte des « logiciels libres » dans la salle polyvalente. L'entrée est libre et gratuite
