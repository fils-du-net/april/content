---
site: Blog LeMonde.fr
title: "A qui profite le piratage de Bercy ?"
author: jean marc manach
date: 2011-03-07
href: http://bugbrother.blog.lemonde.fr/2011/03/07/a-qui-profite-le-piratage-de-bercy/
tags:
- Entreprise
- Internet
- Administration
- Économie
- Interopérabilité
- April
- Institutions
- Marchés publics
- International
---

> La gigantesque affaire d’espionnage de Bercy, révélée ce lundi matin par Paris Match, tombe à point nommé, alors que le gouvernement vient tout juste de renforcer les pouvoirs de l’ANSSI, dotant l’agence nationale de sécurite des systèmes d’information de pouvoirs de “cyberdéfense” l’autorisant à ordonner à un FAI, ou une organisation, de couper l’internet, ce qu’elle a précisément fait ce week-end à Bercy afin de nettoyer les ordinateurs piratés.
