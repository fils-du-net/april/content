---
site: la ligue de l'enseignement
title: "Forcalquier : Libre en fête"
author: Denis Lebioda
date: 2011-03-22
href: http://www.laligue-alpesdusud.org/associatifs_leblog/?2011/03/22/2693-forcalquier-libre-en-fete
tags:
- April
- Associations
- Promotion
---

> Chaque année, l'association Linux-Alpes organise un événement à l'occasion du LIBRE EN FÊTE.
