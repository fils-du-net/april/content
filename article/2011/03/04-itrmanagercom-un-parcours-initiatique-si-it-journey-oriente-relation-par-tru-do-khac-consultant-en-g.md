---
site: ITRmanager.com
title: "Un parcours initiatique SI (IT Journey) orienté relation  Par Tru Dô-Khac, consultant en gouvernance numérique et innovation, membre fondateur de l’Institut d’Archilogie"
date: 2011-03-04
href: http://www.itrmanager.com/articles/115892/parcours-initiatique-it-journey-oriente-relation-tru-do-khac-consultant-gouvernance-numerique-innovation-membre-fondateur-institut-archilogie.html
tags:
- Entreprise
- Internet
- Économie
- Innovation
---

> Pour viser l’innovation par l’usage des systèmes d’information (SI), nous avançons une pratique de gouvernance structurée par quatre phases désignées par « Liaise-Deal-Serve-Nurture ». Centrée clairement sur la relation SI, cette pratique complète les pratiques tournées vers le livrable SI.
