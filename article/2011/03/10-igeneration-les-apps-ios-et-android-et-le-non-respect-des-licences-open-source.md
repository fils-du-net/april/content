---
site: igeneration
title: "Les apps iOS et Android et le (non) respect des licences open-source"
author: Anthony Nelzin
date: 2011-03-10
href: http://www.igeneration.fr/app-store/les-apps-ios-et-android-et-le-non-respect-des-licences-open-source-37832
tags:
- Entreprise
- Logiciels privateurs
- Licenses
---

> OpenLogic, qui aide les entreprises à déployer des logiciels open-source et les développeurs à vérifier que leur code ne viole aucune licence open-source, a fait usage de ses talents sur 635 apps pour Android et iOS. De ces 635 applications, 66 contenaient du code sous licence Apache ou GPL/LGPL et 71 % ne respectaient pas quatre principes majeurs de ces deux licences open-source.
