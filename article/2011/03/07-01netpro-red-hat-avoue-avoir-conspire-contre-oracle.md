---
site: 01netPro.
title: "Red Hat avoue avoir conspiré contre Oracle"
author: Yann Serra
date: 2011-03-07
href: http://pro.01net.com/editorial/529406/red-hat-avoue-avoir-conspire-contre-oracle/
tags:
- Entreprise
---

> Le numéro un de l’Open source a complexifié le noyau Linux pour empêcher Oracle d’en vendre une version personnalisée.
