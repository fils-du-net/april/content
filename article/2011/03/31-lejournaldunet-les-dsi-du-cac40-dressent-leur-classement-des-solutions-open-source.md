---
site: LeJournalduNet
title: "Les DSI du CAC40 dressent leur classement des solutions Open Source"
author: Dominique FILIPPONE
date: 2011-03-31
href: http://www.journaldunet.com/solutions/dsi/open-source-et-grandes-entreprises-le-rapport-du-cigref.shtml
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Philosophie GNU
---

> Dans son dernier rapport, le Cigref dresse une matrice des solutions libres les plus utilisées dans les grandes entreprises françaises. Parmi les solutions les mieux notées : Tomcat et OpenTrust.
