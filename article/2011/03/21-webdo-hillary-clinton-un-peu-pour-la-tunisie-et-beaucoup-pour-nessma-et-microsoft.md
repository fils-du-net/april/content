---
site: webdo
title: "Hillary Clinton : un peu pour la Tunisie et beaucoup pour Nessma et Microsoft ! "
author: Rafik Ouerchefani
date: 2011-03-21
href: http://www.webdo.tn/2011/03/21/hillary-clinton-un-peu-pour-la-tunisie-et-beaucoup-pour-nessma-et-microsoft/
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- International
---

> La visite de Hillary Clinton aura fait couler beaucoup d'encre et allumer beaucoup de pixels. Trois faits l'ont marquée : humiliation pour des journalistes, blogueurs et forces de l'ordre, honneur pour une chaîne de télé privée et plébiscite d'une entreprise américaine.
