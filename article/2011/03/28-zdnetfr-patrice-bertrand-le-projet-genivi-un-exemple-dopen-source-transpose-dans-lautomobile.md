---
site: ZDNet.fr
title: "Patrice Bertrand: le projet Genivi, un exemple d'open source transposé dans l'automobile"
author: Patrice Bertrand
date: 2011-03-28
href: http://www.zdnet.fr/blogs/l-esprit-libre/patrice-bertrand-le-projet-genivi-un-exemple-d-open-source-transpose-dans-l-automobile-39759428.htm
tags:
- Entreprise
- Innovation
---

> Le projet Genivi porte sur l'informatique embarquée dans un véhicule, pour des fonctions non critiques et en particulier le GPS. Patrice Bertrand, président du CNLL et du Comité Open Source de Syntec Numérique, et DG de Smile, expose ce cas instructif d'open source adopté par des grands constructeurs automobiles.
