---
site: Silicon.fr
title: "Elections cantonales : l'April invite toujours les candidats à s’engager en faveur du libre"
author: David Feugey
date: 2011-03-18
href: http://www.silicon.fr/elections-cantonales-l%E2%80%99april-invite-toujours-les-candidats-a-sengager-en-faveur-du-libre-47790.html
tags:
- April
- Institutions
- Promotion
---

> L’April invite les candidats aux cantonales à signer le Pacte du logiciel libre… et les citoyens à faire savoir qu’ils utilisent des produits open source.
