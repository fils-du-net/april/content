---
site: LeMagIT
title: "Android et iOS, OS mobiles préférés des développeurs Open Source "
author: Cyrille Chausson
date: 2011-03-07
href: http://www.lemagit.fr/article/google-mobilite-iphone-android-developpement-opensource-ios/8271/1/android-ios-mobiles-preferes-des-developpeurs-open-source/
tags:
- Entreprise
- Logiciels privateurs
- Innovation
- Licenses
---

> Selon le baromètre de Black Duck Software, les développeurs d’applications mobiles Open Source ont décidé de s’aligner sur le marché et ont sélectionné Android comme leur plate-forme cible favorite. Une confirmation de la cote de popularité grandissante de l’OS. L’iOS des iPhone occupe la seconde place.
