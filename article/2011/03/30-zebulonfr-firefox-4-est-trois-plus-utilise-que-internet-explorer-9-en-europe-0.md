---
site: ZEBULON.fr
title: "Firefox 4 est trois plus utilisé que Internet Explorer 9 en Europe"
author: Relaxnews
date: 2011-03-30
href: http://www.zebulon.fr/actualites/7195-firefox-4-trois-plus-utilise-internet-explorer-9-europe.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Associations
---

> Firefox 4 est trois plus utilisé que Internet Explorer 9 en Europe. La quatrième version du navigateur Firefox s'impose face à son concurrent Internet Explorer 9 sur le continent européen, selon les données fournies par AT Internet, spécialiste de la mesure d'audience...
