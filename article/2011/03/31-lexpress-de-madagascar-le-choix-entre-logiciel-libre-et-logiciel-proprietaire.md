---
site: L'Express de Madagascar
title: "Le choix entre logiciel libre et logiciel propriétaire"
author: Lova Rafidiarisoa
date: 2011-03-31
href: http://www.lexpressmada.com/systemes-informatiques-madagascar/22195-le-choix-entre-logiciel-libre-et-logiciel-proprietaire.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- International
---

> Actuellement, près de 95% des machines utilisent le système d'exploitation Microsoft Windows, et plus de 90% des serveurs web sont basés sous Linux. Mais alors, quels types de logiciels faut-il adopter ?
