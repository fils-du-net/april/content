---
site: macgeneration
title: "Le DOJ enquête sur la guerre des codecs"
author: Arnauld de La Grandière
date: 2011-03-04
href: http://www.macgeneration.com/news/voir/191282/le-doj-enquete-sur-la-guerre-des-codecs
tags:
- Entreprise
- Internet
- Institutions
- Brevets logiciels
- Video
---

> Selon le Wall Street Journal, le département de la Justice américain se penche sur le litige en souffrance entre VP8, le codec vidéo du format libre WebM, et le H.264, soutenu par le MPEG-LA.
