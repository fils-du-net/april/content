---
site: PCWorld.fr
title: "Android en grand danger !"
author: Robin KOECHLIN
date: 2011-03-18
href: http://www.pcworld.fr/2011/03/18/logiciels/andoid-gpl-violation/513137/
tags:
- Entreprise
- Logiciels privateurs
- Droit d'auteur
- Licenses
---

> Cette fois-ci, Google aurait violé la licence GPL en intégrant dans son système Android des codes libres sans les redistribuer, ce qui pourrait être très lourd de conséquences...
