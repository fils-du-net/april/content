---
site: 01net.
title: "Vente liée : Acer condamné pour une clause de confidentialité abusive"
author: Guillaume Deleurence
date: 2011-03-23
href: http://www.01net.com/editorial/530457/vente-liee-acer-condamne-pour-une-clause-de-confidentialite-abusive/
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Vente liée
- Associations
---

> Acer a proposé un arrangement à l'amiable à un client qui avait acheté un PC portable équipé de Windows dont il ne voulait pas. Mais le fabricant a exigé son silence, l'acheteur n'a pas accepté.
