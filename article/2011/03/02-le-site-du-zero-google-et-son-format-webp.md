---
site: le Site du Zéro
title: "Google et son format WebP"
author: colbseton
date: 2011-03-02
href: http://www.siteduzero.com/news-62-39667-p1-google-et-son-format-webp.html
tags:
- Entreprise
- Internet
- Innovation
- Licenses
- Video
---

> Il y a quelque temps maintenant, la firme Google, que l'on ne présente plus, a mis au point un nouveau format d'image pour le web, libre et open-source, WebP. C'est un format d'image qui exploite la compression avec perte et qui se définit comme étant le successeur du très connu JPEG. Récemment le géant Google a ajouté des nouveaux outils à son format. C'est l'occasion pour nous de parler de ce domaine qu'est la compression ainsi que du format de Google en lui-même.
