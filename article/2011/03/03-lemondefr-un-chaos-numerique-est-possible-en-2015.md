---
site: LeMonde.fr
title: "Un chaos numérique est possible en 2015"
author: Michel Riguidel
date: 2011-03-03
href: http://www.lemonde.fr/idees/article/2011/03/25/un-chaos-numerique-est-possible-en-2015_1498401_3232.html
tags:
- Internet
- Désinformation
---

> La tragédie japonaise semble avoir mis à mal un modèle de développement tourné vers un développement effréné de la consommation. Peut-on prévoir les cataclysmes ? Comment vivre dans nos sociétés du risque ?
