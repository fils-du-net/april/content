---
site: Le Monde Informatique
title: "Selon certains experts, Android violerait la licence de Linux"
author: Jean Elyan
date: 2011-03-18
href: http://www.lemondeinformatique.fr/actualites/lire-selon-certains-experts-android-violerait-la-licence-de-linux-33193.html
tags:
- Entreprise
- Droit d'auteur
- Licenses
---

> Selon certains experts en propriété intellectuelle, l'utilisation du noyau Linux dans le système d'exploitation mobile Android de Google pourrait constituer une violation des licences Open Source, avec un détournement du code Linux qui pourrait déboucher sur « un effondrement de l'écosystème Android. »
