---
site: Silicon.fr
title: "Microsoft accuse Google d’abus de position dominante en Europe"
author: Christophe Lagane
date: 2011-03-31
href: http://www.silicon.fr/microsoft-accuse-google-dabus-de-position-dominante-en-europe-48874.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Interopérabilité
- Institutions
- Europe
---

> Microsoft annonce avoir porté plainte auprès de la Commission européenne contre Google et sa position hégémonique sur le marché de la recherche en ligne.
