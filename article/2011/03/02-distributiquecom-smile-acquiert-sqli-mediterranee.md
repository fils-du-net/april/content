---
site: Distributique.com
title: "Smile acquiert SQLI Méditerranée"
author: Fabrice Alessi
date: 2011-03-02
href: http://www.distributique.com/actualites/lire-smile-acquiert-sqli-mediterranee-16040.html
tags:
- Entreprise
---

> Smile acquiert SQLI MéditerranéeAprès son agence de Poitiers, vendue à son directeur en octobre 2010, SQLI se défait cette fois-ci de son agence Méditerranée au profit de Smile, un intégrateur spécialisée dans l'Open Source (e-commerce, Gestion de contenu, GED, BI, ERP...). L'entité cédée par la SSII emploie une vingtaine de collaborateurs dont une quinzaine est basée à Aix-en-Provence (13) et le reste à Montpellier (34).
