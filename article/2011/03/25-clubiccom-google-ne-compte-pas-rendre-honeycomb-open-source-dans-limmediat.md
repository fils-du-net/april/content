---
site: clubic.com
title: "Google ne compte pas rendre Honeycomb open source dans l'immédiat"
author: Audrey Oeillet
date: 2011-03-25
href: http://www.clubic.com/smartphone/android/actualite-406922-google-compte-honeycomb-open-source-immediat.html
tags:
- Entreprise
- Logiciels privateurs
- Innovation
---

> Google a annoncé que l'accès au code source d'Honeycomb ne sera disponible que dans quelques mois pour les développeurs et les petits fabricants de matériel, prétextant qu'il « reste beaucoup à faire avant que l'OS soit prêt pour d'autres appareils ».
