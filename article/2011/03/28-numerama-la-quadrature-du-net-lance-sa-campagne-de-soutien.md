---
site: Numerama
title: "La Quadrature du Net lance sa campagne de soutien"
author: Guillaume Champeau
date: 2011-03-28
href: http://www.numerama.com/magazine/18398-la-quadrature-du-net-lance-sa-campagne-de-soutien.html
tags:
- Internet
- HADOPI
- Associations
---

> La Quadrature du Net lance aujourd'hui sa campagne de financement pour 2011, avec un budget estimé à près de 150 000 euros. L'essentiel doit provenir des dons des internautes.
