---
site: OSOR.EU
title: "FR: April publishes new argumentation tools on public procurement contracts"
author: OSOR Editorial Team
date: 2011-03-17
href: http://www.osor.eu/news/fr-april-publishes-new-argumentation-tools-on-public-procurement-contracts
tags:
- Entreprise
- April
- Associations
- Marchés publics
- English
---

> (l'april poursuit sa campagne contre les appels d'offres discriminatoires avec de nouvelles informations et des ressources de sensibilisation) The French advocacy organisation for free sofware and open standards April continues its campaign against discriminatory calls for tenders with new information and awareness-raising resources.
