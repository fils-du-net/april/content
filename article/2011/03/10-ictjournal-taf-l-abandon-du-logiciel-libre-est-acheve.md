---
site: ICTjournal
title: "TAF: l’abandon du logiciel libre est achevé"
author: Nicolas Paratte
date: 2011-03-10
href: http://www.ictjournal.ch/fr-CH/News/2011/03/10/TAF-labandon-du-logiciel-libre-est-acheve.aspx
tags:
- Logiciels privateurs
- Administration
- International
---

> L'informatique du Tribunal administratif fédéral a été définitivement transférée vers l'OFIT. En désaccord avec le TF, l’instance avait décidé en 2008 de conduire son IT de manière autonome, estimant qu'un environnement composé de produits Microsoft serait plus adapté à ses besoins.
