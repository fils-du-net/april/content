---
site: datanews
title: "Un rapport sur l'open source sème l'émoi dans la politique néerlandaise"
author: Stefan Grommen
date: 2011-03-11
href: http://datanews.rnews.be/fr/ict/actualite/apercu/2011/03/10/un-rapport-sur-l-open-source-seme-l-emoi-dans-la-politique-neerlandaise/article-1194965883928.htm
tags:
- Administration
- Économie
- Institutions
- Licenses
- International
---

> La vie politique des Pays-Bas est secouée à cause d'un rapport qui affirmerait que l'open source et les standards ouverts feraient économiser annuellement 500 millions à 1 milliard d’euros au gouvernement. Le document est aujourd’hui soudainement qualifié de 'peu solide'.
