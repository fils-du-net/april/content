---
site: Romandie
title: "fin de la bataille pour les fournisseurs de logiciels libres"
date: 2011-03-29
href: http://www.romandie.com/infos/news2/201103291800280AWPCH.asp
tags:
- Entreprise
- Logiciels privateurs
- Marchés publics
- International
---

> Lausanne (awp/ats) - Onze fournisseurs de logiciels libres ont définitivement perdu leur bataille judiciaire contre l'administration fédérale. Le Tribunal fédéral a confirmé la décision du Tribunal administratif fédéral (TAF) qui n'était pas entré en matière sur leur recours.
