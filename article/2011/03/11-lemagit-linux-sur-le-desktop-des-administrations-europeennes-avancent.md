---
site: LeMagIT
title: "Linux sur le desktop : des administrations européennes avancent "
author: Cyrille Chausson
date: 2011-03-11
href: http://www.lemagit.fr/article/linux-poste-travail-administration-europe-migration-open-source/8316/1/linux-sur-desktop-des-administrations-europeennes-avancent/
tags:
- Administration
- Interopérabilité
- April
- Standards
- International
---

> Alors que Linux sur le poste de travail connait de nombreuses désillusions dans certaines administrations européennes, d’autres y pensent plus que jamais. A l’image des projets en Espagne, Grèce et Slovénie, qui démontrent que la présence d’ applications Open Source sur le poste de travail et la migration Linux est encore un phénomène qui séduit.
