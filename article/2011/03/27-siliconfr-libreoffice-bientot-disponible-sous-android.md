---
site: Silicon.fr
title: "LibreOffice bientôt disponible sous Android ?"
author: David Feugey
date: 2011-03-27
href: http://www.silicon.fr/libreoffice-bientot-disponible-sous-android-48489.html
tags:
- Entreprise
- Logiciels privateurs
- Associations
- Innovation
---

> Les étudiants du GSoC qui opteront pour LibreOffice auront l’opportunité de travailler sur plusieurs projets, dont un portage de l’application sous Android.
