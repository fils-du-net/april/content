---
site: "ouset-france.fr"
title: "A la découverte des logiciels libres à la cyber-base samedi 19  - Ploeren"
date: 2011-03-03
href: http://www.ouest-france.fr/actu/actuLocale_-A-la-decouverte-des-logiciels-libres-a-la-cyber-base-samedi-19-_56164-avd-20110303-60011565_actuLocale.Htm
tags:
- Administration
- Sensibilisation
---

> Marion Bouder et Stéphane Le Sommer, animateurs à la cyber-base, espace numérique au Triskell.
