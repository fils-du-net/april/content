---
site: SCIENCES et AVENIR.fr
title: "\"La sécurité informatique va à l'encontre de la productivité !\""
author: Cécile Dumas
date: 2011-03-08
href: http://www.sciencesetavenir.fr/actualite/high-tech/20110308.OBS9339/la-securite-informatique-va-a-l-encontre-de-la-productivite.html
tags:
- Internet
- Logiciels privateurs
- Administration
- Institutions
---

> Victime d’une attaque informatique, le ministère des Finances annonce un renforcement de sa sécurité. Il n’est cependant pas facile de se protéger des attaques qui profitent des failles humaines, analyse un spécialiste, Stéphane Bortzmeyer.
