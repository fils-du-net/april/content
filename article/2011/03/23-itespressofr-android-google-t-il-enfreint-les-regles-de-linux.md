---
site: ITespresso.fr
title: "Android : Google a-t-il enfreint les règles de Linux ?"
author: Matthew Broersma (en Anglais)
date: 2011-03-23
href: http://www.itespresso.fr/android-google-a-t-il-enfreint-les-regles-de-linux-41932.html
tags:
- Entreprise
- Droit d'auteur
- Licenses
---

> La manière dont Google exploite Android en lien avec Linux risque de se retourner contre le groupe Internet, selon des experts.
