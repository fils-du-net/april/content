---
site: canoë
title: "Confoo.ca, qu’est-ce que l’open source?"
author: Alexis Le Marec
date: 2011-03-11
href: http://fr.canoe.ca/techno/nouvelles/archives/2011/03/20110311-145123.html
tags:
- Entreprise
- Logiciels privateurs
- Philosophie GNU
- International
---

> La conférence Confoo.ca a pris de l'importance avec le temps, même Microsoft s'y est investit.
