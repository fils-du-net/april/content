---
site: PC INpact
title: "La FSF attribue ses prix 2010 à Rob Savoye et au projet Tor"
author: Nil Sanyas
date: 2011-03-23
href: http://www.pcinpact.com/actu/news/62628-free-software-foundation-stalman-rob-savoye-tor.htm
tags:
- Internet
- Associations
---

> La fameuse Free Software Foundation (FSF) vient d’annoncer les noms des vainqueurs de ses prix 2010. Deux distinctions ont été données : celle pour la promotion du logiciel libre et celle pour les projets d’intérêt social.
