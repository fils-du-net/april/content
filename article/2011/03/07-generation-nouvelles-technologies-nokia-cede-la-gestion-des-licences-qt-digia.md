---
site: Génération nouvelles technologies
title: "Nokia cède la gestion des licences Qt à Digia"
author: Christian D.
date: 2011-03-07
href: http://www.generation-nt.com/nokia-digia-licences-qt-environnement-actualite-1170971.html
tags:
- Entreprise
- Logiciels privateurs
- Licenses
---

> Désormais tourné vers Windows Phone 7, Nokia cède l'activité de services et de licences concernant l'environnement de développement Qt à la société Digia.
