---
site: LeMagIT
title: "Open Source et marchés publics : un tribunal suisse tranche en faveur de Microsoft "
author: Cyrille Chausson
date: 2011-03-30
href: http://www.lemagit.fr/article/microsoft-gouvernement-open-source/8442/1/open-source-marches-publics-tribunal-suisse-tranche-faveur-microsoft/
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Interopérabilité
- Institutions
- Licenses
- Marchés publics
- International
---

> Pas de preuve d’une quelconque concurrence. C'est ce qu’a tranché un tribunal fédéral suisse dans un affaire qui opposait un groupe d’entreprises suisses Open Source Microsoft pour un contrat attribué à Redmond dans appel d’offres. Le juge a confirmé en appel qu’en dépit de cahier des charges précis, les plaignants n’étaient pas parvenus à démontrer qu’une concurrence existait face à Microsoft.
