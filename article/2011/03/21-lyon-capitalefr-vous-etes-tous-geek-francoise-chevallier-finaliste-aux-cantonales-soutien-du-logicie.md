---
site: LYON CAPITALE.fr
title: "Vous êtes tous geek : Françoise Chevallier finaliste aux cantonales, soutien du logiciel libre"
author: Florent Deligia
date: 2011-03-21
href: http://www.lyoncapitale.fr/lyoncapitale/journal/univers/Guide/High-tech-Web-Jeux-Video/Vous-etes-tous-geek-Francoise-Chevallier-finaliste-aux-cantonales-soutien-du-logiciel-libre
tags:
- Institutions
- Promotion
- Vote électronique
---

> Finaliste aux élections cantonales (au second tour dans le langage moldu), Françoise Chevallier a accepté de participer à un vous êtes tous geek hors série
