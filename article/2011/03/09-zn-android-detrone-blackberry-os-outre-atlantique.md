---
site: ZN
title: "Android détrône BlackBerry OS outre-Atlantique"
date: 2011-03-09
href: http://www.zone-numerique.com/news_9833_android_detrone_blackberry_os_outre_atlantique.htm
tags:
- Entreprise
- Innovation
- International
---

> Selon une dépêche AFP et la dernière analyse du cabinet d'études marketing comScore, le système d'exploitation Android lancé par Google en 2008 est désormais le premier utilisé aux Etats-Unis.
