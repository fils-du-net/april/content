---
site: DIRECTION INFORMATIQUE
title: "L'Open Source chez Microsoft"
author: Jean-François Ferland
date: 2011-03-16
href: http://www.directioninformatique.com/DI/client/fr/DirectionInformatique/Nouvelles.asp?id=61710
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Informatique en nuage
---

> L'histoire qui suit n'est pas celle du loup dans la bergerie, ni du mouton dans la meute. C'est plutôt celle de l'incarnation d'une vision stratégique devant permettre à Microsoft de demeurer commercialement pertinente pour encore de nombreuses années.
