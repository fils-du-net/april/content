---
site: OWNI
title: "#1 – Hacker le système politique est une question de volonté"
author: Loic H. Rechi
date: 2011-03-02
href: http://owni.fr/2011/03/02/chroniques-de-rechi-hacker-le-systeme-politique-est-une-question-de-volonte/
tags:
- Internet
- Institutions
- Associations
- Innovation
- Contenus libres
---

> Pour cette première "Chronique de Rechi", découverte de la seconde mouture de Mémoire Politique. Ou comment "des nerds ultrapolitisés" peuvent peser sur les décideurs politiques.
