---
site: PC INpact
title: "Google pourrait avoir contourné la GPL pour Android"
author: Vincent Hermann
date: 2011-03-22
href: http://www.pcinpact.com/actu/news/62593-android-gpl-google-copyright-copyleft.htm
tags:
- Entreprise
- Droit d'auteur
- Licenses
---

> Google pourrait être très prochainement au sein d’une immense tourmente légale centrée autour d’une violation flagrante de la GPL. Dans la ligne de mire : la bibliothèque système qui sert à faire le lien entre l’ensemble du système d’exploitation et le noyau, qui n’est autre que celui du Linux. Or, ce dernier est protégé par la version 2 de la GPL, et on ne peut pas en faire n’importe quoi.
