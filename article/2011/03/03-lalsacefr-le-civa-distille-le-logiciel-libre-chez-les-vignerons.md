---
site: L'ALSACE.fr
title: "Le Civa distille le logiciel libre chez les vignerons"
date: 2011-03-03
href: http://www.lalsace.fr/actualite/2011/03/03/le-civa-distille-le-logiciel-libre-chez-les-vignerons
tags:
- Entreprise
- Internet
- Administration
- Licenses
---

> Pour la télé-déclaration de leurs récoltes, le quart des vignerons alsaciens concernés ont utilisé en 2010 une application sous licence libre, développée par le Conseil interprofessionnel des vins d’Alsace.
