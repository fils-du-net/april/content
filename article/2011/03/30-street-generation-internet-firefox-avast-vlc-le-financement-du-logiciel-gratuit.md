---
site: Street Generation
title: "Internet: Firefox, Avast, VLC… Le financement du logiciel gratuit"
author: Emmanuelle Alfeef
date: 2011-03-30
href: http://streetgeneration.fr/news/breves/31628/internet-firefox-avast-vlc-le-financement-du-logiciel-gratuit/
tags:
- Entreprise
- Internet
- Administration
- Associations
- Promotion
---

> Firefox, Vlc, Avast… Vous connaissez peut-être ces logiciels ultra-répandus, dont le premier avantage est d’être gratuit. Ils sont développés par des communautés de passionnés ou des entreprises qui emploient des salariés. Mais pour fonctionner, ils ont forcément besoin d’argent, ne serait-ce que pour payer les serveurs.  Dans ces conditions, comment quelque chose de gratuit peut-il continuer à exister ?
