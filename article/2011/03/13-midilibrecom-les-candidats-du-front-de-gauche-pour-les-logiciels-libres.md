---
site: MidiLibre.com
title: "Les candidats du Front de Gauche pour les logiciels libres"
date: 2011-03-13
href: http://www.midilibre.com/articles/2011/03/13/RODEZ-Les-candidats-du-Front-de-Gauche-pour-les-logiciels-libres-1564118.php5
tags:
- Administration
- Institutions
- Éducation
---

> Dans un communiqué, les candidats du Front de Gauche aux prochaines élections cantonales des 20 et 27 mars prochains, souhaitent, annoncent avoir signé le pacte du logiciel libre.
