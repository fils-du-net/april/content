---
site: Les Echos
title: "Technologies clefs pour 2015 : les pistes d'avenir secteur par secteur"
author: Frank NIEDERCORN
date: 2011-03-15
href: http://www.lesechos.fr/innovation/technologies/0201215197505-technologies-clefs-pour-2015-les-pistes-d-avenir-secteur-par-secteur.htm
tags:
- Entreprise
- Économie
- Institutions
- Innovation
- International
---

> Le rapport « Technologies clefs 2015 » est divisé en sept secteurs d'activité, pour lesquels il recense au total 85 technologies. Cette édition met l'accent sur la montée en puissance de l'énergie et de l'environnement, avec 28 technologies contre 15 il y a dix ans et 13 il y a cinq ans.
