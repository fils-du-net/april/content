---
site: Silicon.fr
title: "Panasonic rejoint la fondation Linux"
author: David Feugey
date: 2011-03-10
href: http://www.silicon.fr/panasonic-rejoint-la-fondation-linux-47204.html
tags:
- Entreprise
- Associations
---

> Le constructeur japonais Panasonic devient aujourd’hui membre “Gold” de la fondation Linux. Une adhésion stratégique pour les deux acteurs.
