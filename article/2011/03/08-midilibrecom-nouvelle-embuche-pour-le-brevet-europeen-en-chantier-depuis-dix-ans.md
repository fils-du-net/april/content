---
site: MidiLibre.com
title: "Nouvelle embûche pour le brevet européen, en chantier depuis dix ans"
author: Christof Stache
date: 2011-03-08
href: http://www.midilibre.com/articles/2011/03/08/Economie-Nouvelle-embuche-pour-le-brevet-europeen-en-chantier-depuis-dix-ans-1560080.php5
tags:
- Entreprise
- Économie
- Institutions
- Brevets logiciels
- Europe
---

> Le projet de "brevet européen", qui semblait enfin connaître une percée après plus de dix ans de tractations entre les pays de l'UE, a rencontré un nouvel obstacle mardi avec un avis négatif de la justice concernant le règlement des litiges.
