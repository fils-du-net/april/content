---
site: Eco89
title: "Tunisie : Microsoft complice de la censure numérique par Ben Ali"
author: Fabrice Epelboin de ReadWriteWeb France
date: 2011-03-18
href: http://www.rue89.com/2011/03/18/tunisie-microsoft-complice-de-la-censure-numerique-par-ben-ali-195693
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Institutions
- Marchés publics
- International
---

> « Business is business », et la responsabilité sociale des entreprises (RSE), chez Microsoft, semble être une notion exogène, comme chez beaucoup d'entreprises du secteur high-tech.
