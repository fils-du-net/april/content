---
site: datanews
title: "\"Pas moyen de faire entrer l'open source au parlement flamand\""
author: Stefan Grommen
date: 2011-03-11
href: http://datanews.rnews.be/fr/ict/actualite/apercu/2011/03/09/pas-moyen-de-faire-entrer-l-open-source-au-parlement-flamand/article-1194965322038.htm
tags:
- Institutions
- International
---

> Fin février, un débat sur l’utilisation de l’open source et des logiciels libres au sein du gouvernement flamand et des administrations locales a eu lieu dans le cadre de la commission parlementaire en charge des affaires administratives. A cette occasion, Sannen avait posé une question similaire à propos de l’utilisation de l’open source au sein de l’enseignement flamand dans le cadre de la commission ‘Onderwijs en Gelijke kansen’, et l’avait adressée au ministre flamand de l’enseignement, Pascal Smet.
