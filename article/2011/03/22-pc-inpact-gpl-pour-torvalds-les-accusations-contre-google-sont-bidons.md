---
site: PC INpact
title: "GPL : pour Torvalds, les accusations contre Google sont \"bidons\""
author: Vincent Hermann
date: 2011-03-22
href: http://www.pcinpact.com/actu/news/62599-google-android-gpl-stallman-torvalds-defense-oracle-microsoft.htm
tags:
- Entreprise
- Droit d'auteur
- Licenses
---

> Dans une récente actualité, nous indiquions que Google aurait pu contourner la GPL afin de se débarrasser d’une licence potentiellement gênante dans Android. Cet avis était celui de Florian Mueller, du blog Foss Patents. Or, des propos de Richard Stallman abordaient dès 2003 un cas similaire pour infirmer le cas de copyleft. Linus Torvalds, interrogé directement sur le cas de Google, a répondu dans la même veine avec sa franchise habituelle.
