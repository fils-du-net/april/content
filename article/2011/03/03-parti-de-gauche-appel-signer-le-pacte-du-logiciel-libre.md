---
site: Parti de Gauche
title: "Appel à signer le Pacte du Logiciel Libre"
date: 2011-03-03
href: http://www.lepartidegauche.fr/editos/elus-du-pg/3519-appel-a-signer-le-pacte-du-logiciel-libre
tags:
- Internet
- Institutions
- Neutralité du Net
---

> L’utilisation de logiciels libres – qui donnent les quatre libertés d’utiliser, étudier, modifier, redistribuer – et de formats ouverts – qui permettent de s’émanciper de solutions captives – est une condition nécessaire au développement de la société de l’information, au partage des connaissances et à l’émancipation des citoyen-ne-s.
