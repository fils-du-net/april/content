---
site: Developpez.com
title: "Google sort une implémentation Java open-source de Bitcoin, le protocole décentralisé de paiement électronique en Peer 2 Peer"
date: 2011-03-24
href: http://www.developpez.com/actu/30116/Google-sort-une-implementation-Java-open-source-de-Bitcoin-le-protocole-decentralise-de-paiement-electronique-en-Peer-2-Peer/
tags:
- Entreprise
- Internet
- Économie
- Innovation
---

> Un ingénieur de Google vient de sortir BitcoinJ, une implémentation Java open-source du protocole d'échange de monnaie électronique « BitCoin » fondée sur une architecture décentralisée Peer to Peer.
