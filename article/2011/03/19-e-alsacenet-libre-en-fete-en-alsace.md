---
site: "e-alsace.net"
title: "Libre en Fête en Alsace"
date: 2011-03-19
href: http://www.e-alsace.net/index.php/smallnews/detail?newsId=6837
tags:
- Internet
- April
- Promotion
---

> Libre en Fête est une initiative pour accompagner l'arrivée du printemps avec des évènements de découverte des Logiciels Libres. En Alsace, peu d'évènements, mais le libre n'en est pas moins très actif.
