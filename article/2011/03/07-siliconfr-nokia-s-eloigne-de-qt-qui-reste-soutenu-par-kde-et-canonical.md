---
site: Silicon.fr
title: "Nokia s’éloigne de Qt qui reste soutenu par KDE et Canonical"
author: David Feugey
date: 2011-03-07
href: http://www.silicon.fr/nokia-s'eloigne-de-qt-qui-reste-soutenu-par-kde-et-canonical-46930.html
tags:
- Entreprise
- Licenses
---

> Le framework Qt ne semble plus guère intéresser Nokia, qui en confie l’exploitation commerciale à une société tierce, Digia.
