---
site: Developpez.com
title: "L'AFUL appelle à utiliser Firefox 4, et affirme que Google Chrome n'est pas un logiciel libre"
date: 2011-03-23
href: http://www.developpez.com/actu/30063/L-AFUL-appelle-a-utiliser-Firefox-4-et-affirme-que-Google-Chrome-n-est-pas-un-logiciel-libre/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Associations
- Innovation
---

> L'AFUL, l'Association Francophone des Utilisateurs de Logiciels Libres, a lancé ce jour un appel en faveur de Firefox 4. Ce logiciel, qu'elle estime "emblématique du libre" et "médiatique" car "très connu et très utilisé du grand public", "est un des fers de lance des logiciels libres, un baromètre de leur succès".
