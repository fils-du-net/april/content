---
site: ZDNet.fr
title: "L'action du consortium Mpeg-LA à l'encontre du format vidéo WebM est-elle légale ?"
author: Christophe Auffray
date: 2011-03-04
href: http://www.zdnet.fr/actualites/l-action-du-consortium-mpeg-la-a-l-encontre-du-format-video-webm-est-elle-legale-39758762.htm
tags:
- Entreprise
- Internet
- Institutions
- Brevets logiciels
- Video
---

> L'action du consortium Mpeg-LA à l'encontre du format vidéo WebM est-elle légale ? - Selon le Wall Street Journal, le Département américain de la justice aurait ouvert une enquête concernant le consortium Mpeg-LA et ses agissements. L'organisation et ses membres seraient soupçonnés de vouloir étouffer la concurrence pour protéger leur format, H.264.
