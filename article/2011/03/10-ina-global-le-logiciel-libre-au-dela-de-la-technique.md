---
site: ina global
title: "Le logiciel libre, au delà de la technique"
author: Nicolas WEEGER
date: 2011-03-10
href: http://www.inaglobal.fr/technologies/article/le-logiciel-libre-au-dela-de-la-technique
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Associations
- Droit d'auteur
- Licenses
- Philosophie GNU
- International
---

> Mouvement à la fois philosophique et pragmatique, le logiciel libre est intrinsèquement lié à l'histoire de l'informatique et d'Internet. Ce mouvement trouve cependant des échos dans le domaine culturel et participe à des réflexions sur le droit d'auteur.
