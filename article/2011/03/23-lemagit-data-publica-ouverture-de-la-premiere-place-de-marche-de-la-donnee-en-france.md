---
site: LeMagIT
title: "Data Publica : ouverture de la première place de marché de la donnée en France "
author: Cyrille Chausson
date: 2011-03-23
href: http://www.lemagit.fr/article/france-semantique-donnees-gestion-analyse/8382/1/data-publica-ouverture-premiere-place-marche-donnee-france/
tags:
- Entreprise
- Internet
- Administration
- Innovation
- Open Data
---

> François Bancilhon, l’Ex Pdg de Mandriva, a officiellement inauguré sa nouvelle société Data Publica, la première place de marché des données publiques et privées en France. Surfant sur la vague Big Data et sur les modèles américains et britannique, Data Publica espère se faire une place sur un marché évalué à 1,6 milliard d’euros.
