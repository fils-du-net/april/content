---
site: Numerama
title: "Google reporte l'accès au code source d'Android. Un coup de canif pour l'open source ?"
author: Julien L.
date: 2011-03-25
href: http://www.numerama.com/magazine/18386-google-reporte-l-acces-au-code-source-d-android-un-coup-de-canif-pour-l-open-source.html
tags:
- Entreprise
- Logiciels privateurs
- Innovation
- Licenses
---

> Google a-t-il égratigné sa philosophie open source en reportant la mise à disposition du code source d'Android ? Le géant américain a en effet annoncé qu'il repoussait à une date ultérieure l'accès à Honeycomb (Android 3.0). Une décision étrange, dans la mesure où l'ouverture de l'O.S. est l'un de ses atouts face à la concurrence.
