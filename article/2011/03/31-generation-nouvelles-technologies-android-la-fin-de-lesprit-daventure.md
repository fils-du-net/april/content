---
site: Génération nouvelles technologies
title: "Android : la fin de l'esprit d'aventure ?"
author: Christian D.
date: 2011-03-31
href: http://www.generation-nt.com/android-open-source-strategie-google-fabricants-actualite-1183291.html
tags:
- Entreprise
---

> La joyeuse ouverture d'Android qui permet de créer de nombreux appareils utilisant cette plate-forme, pourrait bientôt appartenir au passé, maintenant que la plate-forme est solidement établie.
