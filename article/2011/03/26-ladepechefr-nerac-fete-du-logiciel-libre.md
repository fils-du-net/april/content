---
site: LADEPECHE.fr
title: "Nérac. Fête du logiciel libre"
date: 2011-03-26
href: http://www.ladepeche.fr/article/2011/03/26/1044100-Nerac-Fete-du-logiciel-libre.html
tags:
- Promotion
---

> La médiathèque de Nérac s'associe à la fête de l'Internet et du logiciel libre cette fin du mois de mars
