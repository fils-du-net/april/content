---
site: Silicon.fr
title: "La Free Software Foundation récompense l’anonymat en ligne"
author: David Feugey
date: 2011-03-28
href: http://www.silicon.fr/la-free-software-foundation-recompense-lanonymat-en-ligne-48576.html
tags:
- Internet
- Associations
---

> Rob Savoye (Gnash) et le Tor Project (anonymisation) ont été primés par la Free Software Foundation lors de la conférence LibrePlanet 2011.
