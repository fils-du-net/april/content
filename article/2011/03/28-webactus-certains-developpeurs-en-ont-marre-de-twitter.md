---
site: WebActus
title: "Certains développeurs en ont marre de Twitter"
author: Romain
date: 2011-03-28
href: http://www.webactus.net/actu/9410-rstatus-twitter-developpeur
tags:
- Entreprise
- Internet
---

> Il semblerait en tout cas que certains développeurs en aient marre de Twitter et présente donc un réseau alternatif, rstat.us, ressemblant à Twitter.
