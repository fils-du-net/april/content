---
site: LeMagIT
title: "La Grande-Bretagne met l'Open Source et le Cloud au coeur de sa politique de mutualisation IT"
author: Cyrille Chausson
date: 2011-03-30
href: http://www.lemagit.fr/article/gouvernement-cloud-computing-open-source-grande-bretagne/8435/1/la-grande-bretagne-met-open-source-cloud-coeur-politique-mutualisation-it/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Standards
- International
---

> Le gouvernement de sa Gracieuse Majesté a publié sa stratégie en matière de mutualisation de son informatique. Un plan de bataille ordonné qui passera par le recours aux standards et à l’Open Source, une App Store transversale aux administrations du pays et par la mise en place d’un Cloud gouvernemental. Rien de plus que sa stratégie 2009, mais avec un calendrier un plus précis.
