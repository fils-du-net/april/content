---
site: Localtis.info
title: "Des fêtes nationales et locales pour s'approprier les nouveaux usages des TIC"
author: Luc Derriano
date: 2011-03-18
href: http://www.localtis.info/cs/ContentServer?pagename=Localtis/LOCActu/ArticleActualite&jid=1250261447218&cid=1250261442084
tags:
- Internet
- Administration
- April
---

> Les révolutions du printemps sont bien numériques. De nombreux événements festifs s'organisent en effet partout en France autour de ce 21 mars prochain pour mieux diffuser les technologies de l'information et de la communication (TIC).
