---
site: macgeneration
title: "Google candidate à l'acquisition des brevets de Nortel"
author: Anthony Nelzin
date: 2011-04-04
href: http://www.macgeneration.com/news/voir/194972/google-candidate-a-l-acquisition-des-brevets-de-nortel
tags:
- Entreprise
- Brevets logiciels
---

> La société canadienne Nortel, en liquidation, solde peu à peu ses acquis. Sa possession la plus précieuse est certainement son imposant portefeuille de 6 000 brevets portant sur la téléphonie mobile (3G et 4G LTE), qui attire toutes les convoitises en ces temps de guerre ouverte entre géants du secteur. C'est aujourd'hui Google qui a déclaré son intérêt, alors que même que Larry Page vient d'en (re)prendre les rênes.
