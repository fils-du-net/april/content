---
site: LeJournalduNet
title: "Les préparatifs autour des 20 ans de Linux s'accélèrent"
author: Dominique FILIPPONE
date: 2011-04-12
href: http://www.journaldunet.com/solutions/systemes-reseaux/noyau-linux-code-open-source-20-ans.shtml
tags:
- Associations
---

> La fondation Linux a ouvert les festivités du 20ème anniversaire du noyau Open Source. Supercalculateurs, infrastructures réseaux, OS mobiles : les développements Linux ont explosé ces dernières années.
