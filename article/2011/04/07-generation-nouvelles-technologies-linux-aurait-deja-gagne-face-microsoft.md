---
site: Génération nouvelles technologies
title: "Linux aurait déjà gagné face à Microsoft"
author: Jérôme G
date: 2011-04-07
href: http://www.generation-nt.com/foundation-linux-zemlin-microsoft-apple-actualite-1186691.html
tags:
- Entreprise
- Logiciels privateurs
- Associations
---

> Dans un entretien accordé à Network World, le directeur exécutif de la Linux Foundation regarde Microsoft de très haut.
