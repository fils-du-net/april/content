---
site: internet ACTU.net
title: "Comment étudier l’internet quand l’internet est partout ?"
author: Hubert Guillaud
date: 2011-04-29
href: http://www.internetactu.net/2011/04/29/comment-etudier-linternet-quand-linternet-est-partout/
tags:
- Entreprise
- Internet
- Institutions
---

> L’internet n’est plus quelque chose à part et identifiable, nous expliquait il y a peu Oliver Burkeman. “La technologie est devenue le soubassement de nos vies”. On ne peut plus la comprendre en tant que telle, car les limites entre la réalité et le virtuel ont disparu.
