---
site: PC INpact
title: "Vente liée PC/OS : interview de Yoan Afriat (RueduCommerce)"
author: Marc Rees
date: 2011-04-21
href: http://www.pcinpact.com/actu/news/63203-vente-liee-rdc-yoann-afriat.htm
tags:
- Entreprise
- Logiciels privateurs
- Vente liée
- Licenses
---

> La question de la vente liée PC et OS ne concerne pas seulement les consommateurs d’un côté et les éditeurs/fabricants de l’autre. Entre ces deux extrémités, les plateformes d’e-commerce sont elles aussi impliquées. Nous avons voulu en savoir plus en interrogeant Yoan Afriat, le juriste conseil et contentieux de RueduCommerce.com. L’occasion de découvrir les problématiques soulevées par ce thème cette fois sous le regard des intermédiaires.
