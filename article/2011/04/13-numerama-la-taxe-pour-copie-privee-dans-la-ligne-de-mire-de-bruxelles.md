---
site: Numerama
title: "La taxe pour copie privée dans la ligne de mire de Bruxelles"
author: Julien L.
date: 2011-04-13
href: http://www.numerama.com/magazine/18546-la-taxe-pour-copie-privee-dans-la-ligne-de-mire-de-bruxelles.html
tags:
- Internet
- DRM
- Droit d'auteur
- Europe
---

> La Commission européenne reste déterminée à réviser les mécanismes de compensation pour la copie privée. Bruxelles envisage de relancer le débat entre les industriels et les ayants droit, afin d'aboutir à une réforme permettant d'harmoniser ce système à l'échelle européenne. Cette réforme s'inscrit dans une révision plus globale du système des droits d'auteur en Europe.
