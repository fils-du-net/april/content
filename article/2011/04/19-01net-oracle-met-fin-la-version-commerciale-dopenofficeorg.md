---
site: 01net.
title: "Oracle met fin à la version commerciale d'openoffice.org"
author: Stéphane Long
date: 2011-04-19
href: http://www.01net.com/editorial/531811/oracle-met-fin-a-la-version-commerciale-d-openoffice-org/
tags:
- Entreprise
- Logiciels privateurs
- Associations
---

> L'éditeur Oracle confie à la communauté open source le soin de poursuivre le développement d'OpenOffice.org. La version payante de la suite bureautique est arrêtée.
