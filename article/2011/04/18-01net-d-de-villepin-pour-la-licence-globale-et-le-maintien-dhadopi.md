---
site: 01net.
title: "D. de Villepin pour la licence globale... et le maintien d'Hadopi"
author: Guillaume Deleurence
date: 2011-04-18
href: http://www.01net.com/editorial/531695/dominique-de-villepin-pour-la-licence-globale-et-hadopi/
tags:
- Internet
- HADOPI
- Institutions
- DADVSI
- Droit d'auteur
---

> L'ex-Premier ministre est favorable à l'instauration d'un système de licence globale - ce qui n'a pas toujours été le cas -, tout en maintenant le système de lutte contre le piratage.
