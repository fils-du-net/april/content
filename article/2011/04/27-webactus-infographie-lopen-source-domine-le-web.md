---
site: WebActus
title: "- L'open source domine le web"
author: Romain
date: 2011-04-27
href: http://www.webactus.net/actu/9740-infographie-open-source-domine-web
tags:
- Internet
---

> Le monde de l'open source est une communauté très active sur Internet. Mais il existe un domaine où l'open source domine et c'est bien le web, en témoigne cette infographie.
