---
site: "business-MOBILE.FR"
title: "Google veut reprendre la main sur Android"
author: Pierrick Aubert
date: 2011-04-01
href: http://www.businessmobile.fr/actualites/google-veut-reprendre-la-main-sur-android-39759616.htm
tags:
- Entreprise
- Logiciels privateurs
---

> Google impose désormais aux développeurs et constructeurs de présenter leurs projets de personnalisation de la plate-forme afin de donner, ou non, son feu vert.
