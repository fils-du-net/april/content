---
site: Le Monde Informatique
title: "La justice américaine limite l'achat des brevets de Novell par Microsoft"
author: Jean Elyan
date: 2011-04-21
href: http://www.lemondeinformatique.fr/actualites/lire-la-justice-americaine-limite-l-achat-des-brevets-de-novell-par-microsoft-33500.html
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- Licenses
- International
---

> Suite à un accord passé avec le ministère de la Justice américain, et annoncé mercredi, Microsoft ne sera pas en mesure de conserver les brevets que l'éditeur envisageait d'acheter à Novell.
