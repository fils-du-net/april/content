---
site: LeMagIT
title: "Nicolas Sarkozy installe officiellement le Conseil national du numérique "
author: Cyrille Chausson
date: 2011-04-26
href: http://www.lemagit.fr/article/france-numerique-legislation-conseil/8623/1/nicolas-sarkozy-installe-officiellement-conseil-national-numerique/
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- DADVSI
- Droit d'auteur
---

> Se faire la caisse de résonance de l’écosystème du numérique en France auprès des politiques. C’est la mission que devront remplir les 18 membres du Conseil national du numérique, installé ce jour par Nicolas Sarkozy. Une tâche censée faciliter la compréhension des douloureux dossiers et notamment faire oublier la gestion calamiteuse autour d'Hadopi et autre Loppsi auprès de la communauté des internautes. Certaines associations et syndicats critiquent cependant déjà la représentativité et la légitimité de ce comité.
