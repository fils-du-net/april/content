---
site: clubic.com
title: "Microsoft devra revendre les brevets Novell à Attachmate"
author: Antoine Duvauchelle
date: 2011-04-21
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/brevet-logiciel/actualite-413756-microsoft-devra-revendre-brevets-novell-attachmate.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Europe
---

> Le ministère américain de la Justice a décidé hier que Microsoft ne pourrait pas conserver la totalité des brevets rachetés à Novell. Microsoft devra les revendre à Attachmate, qui reprend les activités de Novell. Il pourra néanmoins en utiliser les licences.
