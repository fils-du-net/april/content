---
site: clubic.com
title: "Conseil national du numérique : ses membres, ses missions"
author: Olivier Robillart
date: 2011-04-27
href: http://www.clubic.com/internet/actualite-415372-conseil-national-numerique-elysee.html
tags:
- Entreprise
- Internet
- April
- HADOPI
- Institutions
---

> Lors d'un rendez-vous à l'Elysée, Nicolas Sarkozy a donné le feu vert à l'installation du Conseil national du numérique. Ce CNN, dont le président sera élu ce mercredi après-midi, sera donc consulté par le Gouvernement sur toutes les questions relatives au numérique.
