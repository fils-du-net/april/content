---
site: OWNI
title: "Hack the CNN /-)"
author: Nicolas Voisin
date: 2011-04-27
href: http://owni.fr/2011/04/27/hack-the-cnn-owni-nicolas-voisin-au-conseil-national-du-numerique/
tags:
- Internet
- Économie
- HADOPI
- Institutions
---

> Fondateur et Directeur de la publication d'OWNI, Nicolas Voisin a été nommé au Conseil National du Numérique (CNN). Retour sur les raisons qui l'ont conduit à accepter cette nomination.
