---
site: Les Echos
title: "Paroles de hacker"
author: Eric Filiol
date: 2011-04-11
href: http://blogs.lesechos.fr/intelligence-economique/paroles-de-hacker-a5543.html
tags:
- Internet
- Administration
- Institutions
---

> Depuis quelques mois (fin 2009 exactement), une série d’attaques informatiques relativement ciblées mais néanmoins de grande envergure ont été menées.
