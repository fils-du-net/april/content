---
site: LeMagIT
title: "Symbian : la ballade Open Source s’arrête"
author: Cyrille Chausson
date: 2011-04-07
href: http://www.lemagit.fr/article/nokia-symbian-mobiles-opensource/8505/1/symbian-ballade-open-source-arrete/
tags:
- Entreprise
- Logiciels privateurs
- Licenses
---

> Nokia a décidé de limiter l’accès au code de l’OS aux seuls OEM restants de la marque et d’encadrer le code sous une licence non-Open Source. L’explosion de la fondation Symbian aura eu des effets collatéraux.
