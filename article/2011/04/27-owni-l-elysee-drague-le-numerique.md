---
site: OWNI
title: "L’élysée drague le numérique"
author: Andréa Fradin
date: 2011-04-27
href: http://owni.fr/2011/04/27/cnn-une-utopie-seduisante/
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Institutions
- Neutralité du Net
---

> Nicolas Sarkozy a présenté ce midi le CNN, ou Conseil national du numérique, nouvelle instance de 18 sages appelée à réfléchir aux problématiques posées par Internet. Dominé par les industriels, le CNN parviendra-t-il à être représentatif et surtout efficace ?
