---
site: wmc actualités
title: "Tunisie - TIC : Des recommandations pour sortir de la crise  "
author: Meryem OMAR
date: 2011-04-25
href: http://www.webmanagercenter.com/management/article-105018-tunisie-tic-des-recommandations-pour-sortir-de-la-crise
tags:
- Entreprise
- Internet
- Administration
- Institutions
- Éducation
- International
---

> Parier sur les logiciels libres et Open Source pour comprimer les coûts, voilà le sens de la diatribe audacieuse publiée dès le 11 avril comme un coup de cœur par un enseignent universitaire connu seulement sous le pseudo de ''Ouerghi'' et qui n'a suscité aucune réaction!
