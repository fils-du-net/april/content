---
site: ère NuméRique
title: "Le CNN voit le jour dans la douleur"
author: Thomas Debelle
date: 2011-04-28
href: http://www.erenumerique.fr/le_cnn_voit_le_jour_dans_la_douleur-news-21782.html
tags:
- Internet
- April
- HADOPI
- Institutions
- Associations
---

> Depuis hier, le Conseil National du Numérique (CNN) vient de prendre son envol. Cette nouvelle instance consultative a pour principale prérogative « d’éclairer le Gouvernement sur les questions touchant au numérique. » Ainsi, il sera chargé « de formuler, à la demande du Gouvernement, des avis sur les projets de disposition législative ou réglementaire susceptibles d’avoir un impact sur l’économie numérique, et de formuler des recommandations en faveur du développement de l’économie numérique et de contribuer au développement de la réflexion prospective sur ce secteur. »
