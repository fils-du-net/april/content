---
site: clubic.com
title: "Neutralité du Net : l'Europe rend une copie bien timide"
author: Alexandre Laurent
date: 2011-04-19
href: http://www.clubic.com/connexion-internet/actualite-413192-neutralite-net-europe-copie-timide.html
tags:
- Entreprise
- Internet
- Institutions
- Neutralité du Net
- Europe
---

> La Commission européenne a adopté mardi un rapport relatif à l'épineux dossier de la neutralité du Net. S'il réaffirme des impératifs de transparence vis à vis du consommateur, ou la nécessité de conserver un accès à Internet non discriminant, il laisse le champ libre aux états membre d'appliquer à leur guise les directives du paquet télécoms, et réserve à l'exécutif européen la possibilité de se pencher à nouveau sur la question ultérieurement. Une réponse bien trop timide ?
