---
site: Génération nouvelles technologies
title: "Opensource-DVD : les incontournables du logiciel libre"
author: Dimitri T.
date: 2011-04-15
href: http://www.generation-nt.com/telecharger-opensource-dvd-compilation-logiciels-libres-actualite-1187571.html
tags:
- Logiciels privateurs
- Sensibilisation
---

> Le projet Opensource-DVD enclenche la seconde avec toujours comme objectif celui de proposer le meilleur du logiciel libre dans une galette de 12 centimètres.
