---
site: Silicon.fr
title: "Du sang neuf pour l’Open Invention Network"
author: David Feugey
date: 2011-04-21
href: http://www.silicon.fr/du-sang-neuf-pour-l%E2%80%99open-invention-network-50292.html
tags:
- Entreprise
- Associations
- Brevets logiciels
---

> De grandes sociétés, comme Facebook ou HP, ont maintenant acquis une licence pour les brevets de l’Open Invention Network. Une bonne nouvelle pour le monde des logiciels libres.
