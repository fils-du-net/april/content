---
site: Le Monde Informatique
title: "Linux fête ses 20 ans d'existence"
author: Jean Elyan avec IDG NS
date: 2011-04-07
href: http://www.lemondeinformatique.fr/actualites/lire-linux-fete-ses-20-ans-d-existence-33378.html
tags:
- Associations
---

> En l'honneur du 20e anniversaire du système d'exploitation Linux, la Fondation Linux a donné le départ de célébrations qui s'étaleront sur plusieurs mois : au programme, une série de festivités, de concours et d'événements mais également des annonces importantes attendues de la part de trois de ses groupes de travail. Un nouveau site web 20ème anniversaire a été ouvert pour l'occasion, sur lequel on trouve le calendrier des diverses initiatives programmées pour les mois à venir, y compris des concours et une nouvelle bourse destinée à soutenir la formation.
