---
site: macgeneration
title: "Novell : Microsoft, Apple, Oracle et EMC revoient leur copie"
author: Anthony Nelzin
date: 2011-04-11
href: http://www.macgeneration.com/news/voir/195802/novell-microsoft-apple-oracle-et-emc-revoient-leur-copie
tags:
- Entreprise
- Associations
- Brevets logiciels
- International
---

> À l'occasion de son acquisition par Attachmate, Novell se sépare de son imposant portefeuille de 882 brevets. CPTN Holdings a mis 450 millions de dollars sur la table pour l'emporter, mais cette société s'est révélé n'être rien d'autre qu'une joint-venture entre Microsoft, Oracle, Apple et EMC (lire : Microsoft, Apple, Oracle et EMC : l'union sacrée).
