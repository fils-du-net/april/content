---
site: OWNI
title: "Propulsion, curation, partage… et le droit dans tout ça ?"
author: Calimaq
date: 2011-04-06
href: http://owni.fr/2011/04/06/propulsion-curation-partage%E2%80%A6-et-le-droit-dans-tout-ca/
tags:
- Internet
- Droit d'auteur
- Contenus libres
---

> Tweeter, mettre un lien sur Facebook sont autant de gestes en apparence anodins. Ils cachent pourtant de nombreuses questions juridiques.
