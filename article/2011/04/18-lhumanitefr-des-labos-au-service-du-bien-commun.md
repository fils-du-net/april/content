---
site: l'Humanité.fr
title: "Des labos au service du bien commun"
author: Léo Coutellec, doctorant en philosophe des sciences 
date: 2011-04-18
href: http://www.humanite.fr/17_04_2011-des-labos-au-service-du-bien-commun-470339
tags:
- Internet
- Partage du savoir
- Institutions
- Brevets logiciels
---

> Des chercheurs scientifiques formulent des propositions institutionnelles pour débarrasser leurs disciplines des logiques financières et enrichir la recherche du débat citoyen. Labo Planète. Ou comment 2030 se prépare sans les citoyens, de Jacques Testart, Catherine Bourgain, Agnès Sinaï.Éditions Mille et Une Nuits, 2010, 10 euros.
