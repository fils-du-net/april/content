---
site: Tunisie Haut débit
title: "Les défenseurs du Libre en Tunisie, «plus faux-cul, tu meurs !»"
author: Majdi Mgaidia
date: 2011-04-11
href: http://www.tunisiehautdebit.com/index.php?option=com_content&id=1026
tags:
- Entreprise
- Internet
- Institutions
- International
- Open Data
---

> Ben Ali avait-il ses mains dans la communauté Open source en Tunisie ? Assistons-nous à des coups bas de quelques membres de cette communauté pour s’octroyer des marchés en discréditant Microsoft ? Où est-ce une tactique pour faire oublier sa veste de membre du RCD ? Un professeur universitaire donne son opinion : «Le Libre en Tunisie ? Ils s’en foutent tant qu’ils y trouvent leur compte».
