---
site: OWNI
title: "Sur les traces de la PirateBox"
author: microtokyo
date: 2011-04-20
href: http://owni.fr/2011/04/20/sur-les-traces-de-la-pirate-box/
tags:
- Internet
- Partage du savoir
- HADOPI
- Droit d'auteur
- Innovation
- Licenses
---

> Échanger des fichiers librement, surfer sur le web en contournant la vigie Hadopi, c'est ce que propose la PirateBox, créée par David Darts, professeur à l'université de New York. Une lunch box punk en mode WiFi &amp; DIY.
