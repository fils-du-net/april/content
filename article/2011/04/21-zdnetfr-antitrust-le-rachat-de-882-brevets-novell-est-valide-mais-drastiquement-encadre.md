---
site: ZDNet.fr
title: "Antitrust : le rachat de 882 brevets Novell est validé, mais drastiquement encadré"
author: Christophe Auffray
date: 2011-04-21
href: http://www.zdnet.fr/actualites/antitrust-le-rachat-de-882-brevets-novell-est-valide-mais-drastiquement-encadre-39760162.htm
tags:
- Entreprise
- Brevets logiciels
- Licenses
- International
---

> Antitrust : le rachat de 882 brevets Novell est validé, mais drastiquement encadré - L'accord qui prévoyait le rachat de 882 brevets de Novell par un consortium composé de Microsoft, Apple, EMC et Oracle a été profondément modifié par les régulateurs allemand et américain. Tous les brevets passeront ainsi sous licences Open Source. En outre, les éditeurs n'acquerront plus que des licences d'utilisation et non des droits de propriété pour certains des brevets.
