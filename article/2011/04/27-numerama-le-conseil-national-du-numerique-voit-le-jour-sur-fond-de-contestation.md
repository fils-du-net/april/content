---
site: Numerama
title: "Le Conseil national du numérique voit le jour sur fond de contestation"
author: Julien L.
date: 2011-04-27
href: http://www.numerama.com/magazine/18646-le-conseil-national-du-numerique-voit-le-jour-sur-fond-de-contestation.html
tags:
- Internet
- Économie
- April
- HADOPI
- Institutions
- Associations
---

> Les débuts du Conseil national du numérique sont difficiles. Alors que l'instance a été officialisée ce matin en Conseil des ministres, elle essuie des critiques de tous les côtés. L'UFC-Que Choisir, l'April et la Quadrature du Net critiquent en particulier l'absence de diversité, puisque les membres actuels sont des entrepreneurs ou des opérateurs.
