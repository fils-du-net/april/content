---
site: lapresseaffaires
title: "Vanilla Forums: la nouvelle saveur du Montréal web"
author: Philippe Mercure
date: 2011-04-25
href: http://lapresseaffaires.cyberpresse.ca/economie/technologie/201104/25/01-4393054-vanilla-forums-la-nouvelle-saveur-du-montreal-web.php
tags:
- Entreprise
- International
---

> (Montréal) Si la vitalité d'une communauté se mesure à sa capacité d'attirer des gens de l'extérieur, le Montréal techno peut se réjouir de voir Mark O'Sullivan s'y activer.
