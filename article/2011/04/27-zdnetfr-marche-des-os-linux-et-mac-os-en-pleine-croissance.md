---
site: ZDNet.fr
title: "Marché des OS : Linux et Mac OS en pleine croissance"
author: Christophe Auffray
date: 2011-04-27
href: http://www.zdnet.fr/actualites/marche-des-os-linux-et-mac-os-en-pleine-croissance-39760287.htm
tags:
- Entreprise
- Logiciels privateurs
---

> 30,4 milliards de dollars, c'est le chiffre d'affaires généré en 2010 par la vente de systèmes d'exploitation (postes et serveurs). Microsoft empoche à lui seul 23,8 milliards de dollars, mais est confronté au développement de Mac OS sur les postes et de Linux sur serveurs.
