---
site: PC INpact
title: "Malgré ses directives d'interopérabilité, l'UE s'attache à Microsoft"
author: Vincent Hermann
date: 2011-04-13
href: http://www.pcinpact.com/actu/news/63070-europe-contrat-microsoft.htm
tags:
- Entreprise
- Interopérabilité
- April
- Institutions
- Associations
- Marchés publics
- Standards
- Europe
---

> L’année dernière, l’Europe s’est dotée d’un texte important concernant les infrastructures publiques de gestion de l’information, autrement dit les parcs informatiques. Nommé « European Interoperability Framework », il concerne aussi bien les instances européennes que les pays membres et indique que les gouvernements doivent privilégier les solutions libres lorsque cela est approprié. Mais voilà que la Commission européenne négocie en ce moment même avec Microsoft l’extension d’un contrat qui permet à Windows d’être installé sur 36 000 postes.
