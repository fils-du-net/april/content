---
site: SUD OUEST
title: "Ils veulent effacer la fracture numérique"
author: Natacha thuillier 
date: 2011-04-19
href: http://www.sudouest.fr/2011/04/19/ils-veulent-effacer-la-fracture-numerique-375704-813.php
tags:
- Associations
---

> Les bénévoles de l'association récupèrent de vieux ordinateurs, leur donnent une nouvelle vie via les logiciels libres et équipent des familles aux petits budgets.
