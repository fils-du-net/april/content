---
site: DIRECTION INFORMATIQUE
title: "Histoire de brevets : parallèle Nortel-Novell"
author: Nelson Dumais
date: 2011-04-27
href: http://www.directioninformatique.com/DI/client/fr/DirectionInformatique/Nouvelles.asp?id=62281
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- International
---

> « Ce que nous gagnons en connaissances, disait Chateaubriand, nous le perdons en sentiments. » À ce compte, la saga des brevets présentement en cours chez Nortel et Novell a de quoi nous présenter les Microsoft, Oracle, Google, Apple et autres prédateurs multinationaux comme étant des boîtes au coeur asséché.
