---
site: L'INFORMATICIEN
title: "Google ne peut pas utiliser Linux librement "
author: Selma Bekkaoui
date: 2011-04-22
href: http://www.linformaticien.com/actualites/id/20470/google-ne-peut-pas-utiliser-linux-librement.aspx
tags:
- Entreprise
- Brevets logiciels
- International
---

> Google vient d’être condamné à verser 5 millions de dollars de dommages et intérêts à une société américaine, Bedrock Computer Technologies, qui détient des brevets logiciels sur certaines technologies intégrées dans le noyau Linux.
