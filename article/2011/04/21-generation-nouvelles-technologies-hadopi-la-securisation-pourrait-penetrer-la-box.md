---
site: Génération nouvelles technologies
title: "Hadopi : la sécurisation pourrait pénétrer la box"
author: Jérôme G.
date: 2011-04-21
href: http://www.generation-nt.com/hadopi-logiciel-securisation-box-internet-actualite-1193401.html
tags:
- Entreprise
- Internet
- HADOPI
---

> La Haute Autorité pour la diffusion des œuvres et la protection des droits sur Internet lance une deuxième consultation sur les spécifications fonctionnelles des moyens de sécurisation. Des moyens qui voient large et jusque dans la box Internet.
