---
site: Numerama
title: "Google condamné pour avoir utilisé Linux"
author: Julien L.
date: 2011-04-22
href: http://www.numerama.com/magazine/18625-google-condamne-pour-avoir-utilise-linux.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- International
---

> Un utilisateur peut également être condamné pour avoir utilisé un système violant un brevet. Aux Etats-Unis, un jury populaire a condamné Google à verser 5 millions de dollars de dommages et intérêts à Bedrock Computer Technologies. Celui-ci suspecte plusieurs versions du noyaux Linux d'avoir enfreint un de ses brevets. Dans cette affaire, Google n'a fait qu'utiliser le système d'exploitation.
