---
site: webdo
title: "Le libre investit les clubs universitaires "
author: Melek Jebnoun
date: 2011-04-18
href: http://www.webdo.tn/2011/04/18/le-libre-investit-les-clubs-universitaires/
tags:
- Entreprise
- Associations
- Éducation
- International
---

> L'Institut national des Sciences appliquées et Technologies (INSAT) s'est mis, le temps d'une journée, aux logiciels libres. C'est ainsi que mercredi dernier,
