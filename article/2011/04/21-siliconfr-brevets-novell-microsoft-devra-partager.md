---
site: Silicon.fr
title: "Brevets Novell : Microsoft devra partager"
author: David Feugey
date: 2011-04-21
href: http://www.silicon.fr/brevets-novell-microsoft-devra-partager-50306.html
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- Licenses
- International
---

> Le DoJ a décidé de changer la répartition des brevets de Novell, et de les mettre sous la protection de la GPL et de l’OIN.
