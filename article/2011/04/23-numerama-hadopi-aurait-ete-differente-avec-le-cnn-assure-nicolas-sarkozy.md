---
site: Numerama
title: "Hadopi aurait été différente avec le CNN, assure Nicolas Sarkozy"
author: Julien L.
date: 2011-04-23
href: http://www.numerama.com/magazine/18628-hadopi-aurait-ete-differente-avec-le-cnn-assure-nicolas-sarkozy.html
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Droit d'auteur
---

> Alors que le Conseil National du Numérique (CNN) doit être définitivement installé la semaine prochaine, le président de la République a rencontré la plupart des membres de cette future instance lors d'un petit-déjeuner vendredi. L'occasion pour Nicolas Sarkozy d'expliquer pourquoi les ayants droit et les représentants du public ne sont pas présents dans le CNN, et d'aborder la loi Hadopi et Loppsi.
