---
site: Silicon.fr
title: "L’April bascule en version papier son catalogue des logiciels libres"
author: David Feugey
date: 2011-04-19
href: http://www.silicon.fr/l%E2%80%99aapril-bascule-en-version-papier-son-catalogue-des-logiciels-libres-50088.html
tags:
- April
- Promotion
---

> Une version papier du catalogue 26 logiciels libres à découvrir de l’April est dorénavant disponible à la vente. Ce guide a été tiré à 7500 exemplaires.
