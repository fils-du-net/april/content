---
site: ITRmanager.com
title: "Les contradictions de l'ouverture"
author: Jean-Marie Chauvet
date: 2011-04-09
href: http://www.itrmanager.com/articles/117545/contradictions-ouverture.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Administration
---

> La concomitance de déclarations contradictoires sur les bénéfices de l'ouverture des systèmes d'information et des architectures informatiques aurait de quoi surprendre pour une idée dont la légitimité paraît depuis longtemps établie.
