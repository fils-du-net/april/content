---
site: datanews
title: "Open source or not to open source: subterfuge ou analyse?"
author: Ludo Sannen
date: 2011-04-19
href: http://datanews.rnews.be/fr/ict/actualite/opinion/open-source-or-not-to-open-source-subterfuge-ou-analyse/article-1194992766896.htm
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Éducation
- Standards
- International
---

> Le Prof. Carlos De Backer a abordé dans une opinion parue dans Data News le débat qui vient à nouveau de se manifester à propos de l’utilisation des logiciels open source dans l’enseignement, les pouvoirs publics et les entreprises. Il conclut que c’est la qualité des logiciels qui doit primer, et que le débat en question est inutile, car les deux types de software ont acquis leur droit à l’existence et sont complémentaires.
