---
site: L'INFORMATICIEN
title: "Facebook dévoile son Open Compute Project"
author: Emilien Ercolani
date: 2011-04-08
href: http://www.linformaticien.com/actualites/id/20303/facebook-devoile-son-open-compute-project.aspx
tags:
- Entreprise
- Internet
---

> Pendant plusieurs années, des ingénieurs de Facebook ont travaillé dans le datacenter de Prineville, dans l’Oregon. Le site communautaire délivre les secrets de fabrication de l’architecture de son serveur open source.
