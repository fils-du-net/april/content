---
site: LeMagIT
title: "Embarqué : des développeurs peu disposés à changer de plate-forme "
author: Cyrille Chausson
date: 2011-04-27
href: http://www.lemagit.fr/article/os-open-source-embarque/8614/1/embarque-des-developpeurs-peu-disposes-changer-plate-forme/
tags:
- Entreprise
- Logiciels privateurs
---

> Les éditeurs commerciaux d’OS embarqués ont de plus en plus la vie dure, note VDC Research qui dans son dernier rapport, pointe du doigt un marché 2010 sur lequel les entreprises et les développeurs préfèrent encore leur OS maison ou n’ont pas d’OS officiels référencés. La montée de l’Open Source complique également le schéma.
