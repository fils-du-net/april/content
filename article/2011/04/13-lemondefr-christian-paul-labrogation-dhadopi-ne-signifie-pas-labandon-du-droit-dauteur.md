---
site: LeMonde.fr
title: "Christian Paul : \"l'abrogation d'Hadopi ne signifie pas l'abandon du droit d'auteur\""
author: Laurent Checola et Damien Leloup
date: 2011-04-13
href: http://www.lemonde.fr/technologies/article/2011/04/13/christian-paul-l-abrogation-d-hadopi-ne-signifie-pas-l-abandon-du-droit-d-auteur_1506918_651865.html
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Institutions
- Droit d'auteur
- Neutralité du Net
---

> Dans un chat au Monde.fr, le responsable du Laboratoire des idées du parti socialiste détaille le programme numérique de sa formation.
