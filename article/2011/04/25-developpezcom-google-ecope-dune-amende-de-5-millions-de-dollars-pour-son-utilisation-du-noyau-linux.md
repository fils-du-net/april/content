---
site: Developpez.com
title: "Google écope d'une amende de 5 millions de dollars pour son utilisation du noyau Linux qui violerait un brevet logiciel polémique"
date: 2011-04-25
href: http://www.developpez.com/actu/31355
tags:
- Entreprise
- Brevets logiciels
---

> Une petite entreprise américaine vient de convaincre un Jury de reconnaitre Google coupable et le condamner à verser 5 millions de dollars de dommages et intérêts pour violation d'un brevet logiciel par le noyau Linux, massivement utilisé par géant des moteurs de recherche.
