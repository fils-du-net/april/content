---
site: LES NUMERIQUES
title: "Google : halte à la customisation débridée d'Android"
author: Florence Legrand
date: 2011-04-05
href: http://www.lesnumeriques.com/google-halte-customisation-debridee-android-news-18705.html
tags:
- Entreprise
---

> Remettre un peu d'ordre sur la populaire planète Android, tel est l'objectif du mastodonte Google - défenseur du "libre". Toute idée de personnalisation de son système d'exploitation pour mobiles sera désormais soumise à validation. En conséquences de quoi Google agitera une clause de "non-fragmentation" à la vue de ses partenaires.
