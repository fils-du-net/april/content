---
site: tom's hardware
title: "Oracle libère OpenOffice"
author: Matthieu Lamelot
date: 2011-04-19
href: http://www.presence-pc.com/actualite/Libreoffice-OpenOffice.org-43358/
tags:
- Entreprise
- Logiciels privateurs
- Associations
---

> Oracle libère OpenOffice Sans préavis, Oracle a annoncé vendredi dernier son intention de transformer OpenOffice.org en un projet purement communautaire et open source. Cela signifie la fin d'OpenOffice en tant que suite bureautique vendue par Oracle.
