---
site: LE TIGRE
title: "Et nous assistâmes, les bras ballants, à la privation du web"
author: Raphaël Meltz
date: 2011-04-11
href: http://www.le-tigre.net/Et-nous-assistames-les-bras-=26223.html
tags:
- Entreprise
- Internet
---

> Régulièrement on nous pose la question. Ou alors on nous propose. Pourquoi Le Tigre n’est pas sur Facebook, sur Twitter ? Vous voulez qu’on s’en charge ? Je réponds poliment - pas toujours poliment - que non merci ça ira très bien comme ça. Encore faudrait-il se justifier. Voici quelques explications.
