---
site: écrans
title: " Le CNN en direct de l’Elysée"
author: Alexandre Hervaud
date: 2011-04-28
href: http://www.ecrans.fr/Le-CNN-en-direct-de-l-Elysee,12605.html
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
---

> Dès qu’il est question d’Internet, la majorité des parlementaires français affichent une méconnaissance à faire passer Jacques Chirac et son mulot pour Mark Zuckerberg. Pour éviter que les députés votent des lois au mieux archaïques, au pire dangereuses, le Conseil national numérique (CNN) a vu le jour avec pour mission de « donner des avis et formuler des recommandations en faveur du développement de l’Internet en France », a affirmé hier Nicolas Sarkozy. Ce Conseil était prévu depuis 2008 dans le cadre du plan France numérique 2012, piloté par Eric Besson.
