---
site: clubic.com
title: "Fondation Linux : le \"grand rival\" n'est plus Microsoft, mais Apple"
author: Antoine Duvauchelle
date: 2011-04-06
href: http://pro.clubic.com/it-business/actualite-409632-fondation-linux-rival-appelle-apple.html
tags:
- Entreprise
- Logiciels privateurs
---

> Jim Zemlin, le directeur exécutif de la Fondation Linux, vient de faire une sortie sur les rivaux des distributions GNU / Linux : Microsoft et Apple. Et alors que le premier est toujours majoritaire sur les PC de la planète, pour lui, le vrai concurrent est désormais Apple.
