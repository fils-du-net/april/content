---
site: webdo
title: "Mozilla: quand l’intelligence collective du mouvement Open Source fait des miracles"
author: Melek Jebnoun
date: 2011-04-04
href: http://www.webdo.tn/2011/04/04/mozilla-quand-lintelligence-collective-du-mouvement-open-source-fait-des-miracles-2/
tags:
- Internet
- Logiciels privateurs
- Associations
- Standards
- International
---

> Mozilla ou le nom d'une vraie sucess story signée par le mouvement Open Source. C’est suite à la libération du code source du navigateur de Netscape que le projet naquit
