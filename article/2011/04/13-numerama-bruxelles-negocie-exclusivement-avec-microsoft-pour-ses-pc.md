---
site: Numerama
title: "Bruxelles négocie exclusivement avec Microsoft pour ses PC"
author: Julien L.
date: 2011-04-13
href: http://www.numerama.com/magazine/18556-bruxelles-negocie-exclusivement-avec-microsoft-pour-ses-pc.html
tags:
- Entreprise
- Logiciels privateurs
- Interopérabilité
- April
- Institutions
- Marchés publics
- Standards
- Europe
---

> La Commission européenne a annoncé au début du mois qu'elle négociait exclusivement avec Microsoft pour acheter de nouvelles licences logicielles. Si la procédure n'est pas illégale, elle va pourtant à l'encontre des propos de la commissaire européenne Neelie Kries, qui appelait Bruxelles à se libérer des technologies propriétaires.
