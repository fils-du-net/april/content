---
site: Developpez.com
title: "Oracle laisse OpenOffice.org à la communauté, de nombreuses questions demeurent"
date: 2011-04-19
href: http://www.developpez.com/actu/31144/Oracle-laisse-OpenOffice-org-a-la-communaute-de-nombreuses-questions-demeurent/
tags:
- Entreprise
- Logiciels privateurs
- Associations
---

> « Oracle annonce aujourd'hui son intention de faire de OpenOffice.org un projet open source purement communautaire et de ne plus offrir de version commerciale de Open Office. »
