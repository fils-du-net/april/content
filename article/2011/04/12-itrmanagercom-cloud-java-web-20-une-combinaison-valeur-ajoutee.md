---
site: ITRmanager.com
title: "Cloud, Java, Web 2.0 : une combinaison à valeur ajoutée"
author: Pierre Queinnec
date: 2011-04-12
href: http://www.itrmanager.com/articles/117633/cloud-java-web-2-0-combinaison-valeur-ajoutee-pierre-queinnec-directeur-associe-zenika.html
tags:
- Entreprise
- Internet
---

> Incontestablement, Java continue sa percée dans le monde professionnel et reste un langage incontournable et plébiscité dans les différentes DSI. Bien entendu, certains diront que développer en Java peut se révéler être un processus assez lourd à mettre en place… Et pourtant, force est de constater que l’avènement du cloud computing, au-delà de son aspect marketing, semble faire évoluer les habitudes de travail et contribuer à porter les projets Java au sein des entreprises.
