---
site: "cio-online.com"
title: "Le Cigref étudie la maturité des entreprises en terme d'open-source"
author: Bertrand Lemaire
date: 2011-04-13
href: http://www.cio-online.com/actualites/lire-le-cigref-etudie-la-maturite-des-entreprises-en-terme-d-open-source-3550.html
tags:
- Entreprise
- Associations
---

> Découvrez le contexte du marché des systèmes d'information avec CIO-Online: chiffres-clés, sondages express, actualités.
