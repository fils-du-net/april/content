---
site: Silicon.fr
title: "LFCS : Linux fête ses 20 ans en vidéo"
author: David Feugey
date: 2011-04-07
href: http://www.silicon.fr/lfcs-linux-fete-ses-20-ans-en-video-49351.html
tags:
- Internet
- Associations
---

> La fondation Linux met un coup de projecteur sur le noyau Linux, un projet open source qui fête ses vingt ans en 2011… et s’insinue aujourd’hui dans toute l’industrie.
