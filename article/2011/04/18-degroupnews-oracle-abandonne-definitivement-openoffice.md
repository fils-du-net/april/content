---
site: Degroupnews
title: "Oracle abandonne définitivement OpenOffice"
author: Stéphane Caruana
date: 2011-04-18
href: http://www.degroupnews.com/actualite/n6243-oracle-openoffice-logiciel_libre-libreoffice-logiciel.html
tags:
- Entreprise
- Logiciels privateurs
---

> Oracle jette finalement l'éponge face à OpenOffice et s'en remet à la communauté des développeurs qui font vivre le projet open source. La firme américaine assure néanmoins qu'elle continuera à investir dans le logiciel libre et notamment dans Linux et MySQL.
