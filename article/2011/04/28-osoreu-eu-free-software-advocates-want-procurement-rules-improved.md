---
site: OSOR.EU
title: "EU: Free software advocates want procurement rules improved"
author: Gijs Hillenius
date: 2011-04-28
href: http://www.osor.eu/news/eu-free-software-advocates-want-procurement-rules-improved
tags:
- Administration
- Interopérabilité
- April
- Licenses
- Marchés publics
- Europe
- English
---

> (La réglementation des marchés publics européens devrait être améliorée pour permettre un meilleur accès aux applications logicielles libres et open source) Europe's rules on public procurement should be improved to allow better access to free and open source software applications, according to advocacy groups and the OSOR project. They responded to a public consultation by the European Commission. The groups want the rules to request free and open source licencing terms.
