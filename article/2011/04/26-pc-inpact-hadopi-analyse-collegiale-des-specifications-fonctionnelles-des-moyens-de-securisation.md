---
site: PC INpact
title: "Hadopi : analyse collégiale des spécifications fonctionnelles des moyens de sécurisation"
author: Nil Sanyas
date: 2011-04-26
href: http://www.pcinpact.com/actu/news/63264-hadopi-analyse-specifications-fonctionnelles-moyens-securisation.htm
tags:
- Entreprise
- Internet
- HADOPI
- Droit d'auteur
---

> La seconde version des spécifications fonctionnelles des moyens de sécurisation de la Hadopi (SFH), publiée la semaine dernière, a fait couler beaucoup d’encre. Outre l’analyse de PCINpact, nous vous recommandons celle publiée hier par un groupe de connaisseurs comprenant entre autres Olivier Laurelli (Bluetouff), Gaëtan (Erebuss) ou encore Bruno Spiquel (Turblog). Ce dernier n’est pas n’importe qui, il s’est notamment illustré par ses attaques contre Hadopi, ainsi que par sa présence dans les Labs, parmi six autres experts.
