---
site: webdo
title: "Adel Gaaloul et Slim Amamou rencontrent la communauté Open Source "
author: Rafik Ouerchefani
date: 2011-04-04
href: http://www.webdo.tn/2011/04/04/adel-gaaloul-et-slim-amamou-rencontrent-la-communaute-open-source/
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Institutions
- International
---

> Jeudi 31 mars, a eu lieu, dans le département du secrétaire d’État de la Technologie, Adel Gaaloul, une réunion organisée par la cellule des logiciels libres du secrétariat d'État à l'Informatique, Internet et Logiciels libres. A l'ordre du jour, les préparatifs de la participation tunisienne au salon SolutionLinux qui se tiendra à Paris du 10 au 12 mai prochain.
