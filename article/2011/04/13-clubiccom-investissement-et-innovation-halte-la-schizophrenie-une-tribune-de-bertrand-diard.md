---
site: clubic.com
title: "\"Investissement et innovation : halte à la schizophrénie !\", une tribune de Bertrand Diard"
author: Bertrand Diard
date: 2011-04-13
href: http://pro.clubic.com/actualite-e-business/investissement/actualite-411564-investissement-innovation-halte-schizophrenie-tribune-bertrand-diard.html
tags:
- Entreprise
- Économie
- Institutions
- International
---

> Bertrand Diard est le PDG et cofondateur de Talend Software, éditeur français d'un logiciel open source d'intégration de données (ETL) aujourd'hui basé en Californie. Alors que le secrétaire d'Etat au numérique Eric Besson se rend cette semaine à San Francisco pour rencontrer quelques pointures américaines de l'industrie du Web, il nous propose cette tribune libre libre dans laquelle il explique que les conditions de financement de l'innovation en France ne sont peut-être pas si calamiteuses que le dénoncent ceux qui ont les yeux qui brillent dès que l'on prononce le nom de la Silicon Valley.
