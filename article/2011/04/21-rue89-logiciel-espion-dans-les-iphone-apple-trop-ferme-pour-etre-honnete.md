---
site: Rue89
title: "Logiciel espion dans les iPhone : Apple, trop fermé pour être honnête ?"
date: 2011-04-21
href: http://www.rue89.com/2011/04/21/logiciel-espion-dans-les-iphone-apple-trop-ferme-pour-etre-honnete-200869
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Institutions
---

> Fervents militants du logiciel libre, des développeurs de l'équipe technique de Rue89 ont alerté les journalistes ce matin après les dernières révélations sur le mouchard introduit par Apple à l'intérieur de ses téléphones et tablettes, qui recueille des informations sur tous les déplacements de l'utilisateur, à son insu.
