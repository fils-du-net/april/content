---
site: webdo
title: "Le logiciel libre en Tunisie: enfin le bout du tunnel ? "
author: Melek Jebnoun
date: 2011-04-11
href: http://www.webdo.tn/2011/04/11/le-logiciel-libre-en-tunisie-enfin-le-bout-du-tunnel-2/
tags:
- International
- Open Data
---

> "Libre", ce mot tant utilisé depuis le 14 Janvier: libre de choisir, libre de participer, liberté d'expression et depuis peu, une expression résonne dans nos oreilles sans cesse: logiciel libre. Un logiciel dont l'utilisation, l'étude, la modification et la duplication, en vue de sa diffusion, sont permises, que ce soit sur le plan technique ou bien légal.
