---
site: BILADI
title: "Facebook se lance dans l’Open Source"
author: Salma Bakri
date: 2011-04-08
href: http://www.biladi.fr/041171875-facebook-se-lance-dans-l-open-source
tags:
- Entreprise
- Internet
---

> Voilà une nouvelle qui ravit en matière de technologie! Le réseau social Facebook a annoncé, jeudi le lancement d’un programme baptisé «open compute» dont le but est de partager les spécifications techniques de ses serveurs et centres de données.
