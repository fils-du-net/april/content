---
site: Silicon.fr
title: "Simon Phipps (OSI) : «Les Etats-Unis reconnaissent que l’open source est essentiel pour la concurrence»"
author: David Feugey
date: 2011-04-25
href: http://www.silicon.fr/simon-phipps-osi-%C2%ABles-etats-unis-reconnaissent-que-l%E2%80%99open-source-est-essentiel-pour-la-concurrence%C2%BB-50355.html
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- International
---

> La décision prise par le DoJ dans le cadre du rachat des brevets de Novell par le groupement CPTN a de quoi satisfaire l’Open Source Initiative, qui y voit – à juste raison – un signe très positif.
