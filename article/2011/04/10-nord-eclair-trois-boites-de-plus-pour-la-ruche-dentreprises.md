---
site: Nord éclair
title: "Trois boîtes de plus pour la ruche d'entreprises"
author: BRUNO DECOTTIGNIES
date: 2011-04-10
href: http://www.nordeclair.fr/Locales/Lille/2011/04/10/trois-boites-de-plus-pour-la-ruche-d-ent.shtml
tags:
- Entreprise
---

> Global vision, Talcod et Mon coffret cadeau sont les trois jeunes entreprises récemment implantées au sein de la ruche technologique du Nord. Elles y étaient présentées vendredi.
