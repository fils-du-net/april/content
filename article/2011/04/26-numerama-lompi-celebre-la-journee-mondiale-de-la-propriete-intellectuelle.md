---
site: Numerama
title: "L'OMPI célèbre la Journée mondiale de la propriété intellectuelle"
author: Julien L.
date: 2011-04-26
href: http://www.numerama.com/magazine/18637-l-ompi-celebre-la-journee-mondiale-de-la-propriete-intellectuelle.html
tags:
- April
- HADOPI
- Associations
- DADVSI
- Droit d'auteur
- International
- ACTA
---

> Cette année, la Journée mondiale de la propriété intellectuelle est baptisée "concevoir le futur". L'OMPI a souhaité mettre en avant le rôle que joue le design et l'importance de le protéger. Profitant de l'occasion, l'April et le Parti pirate ont de nouveau mis en garde sur l'utilisation abusive de la notion de propriété intellectuelle et ont rappelé la tenue d'une Journée mondiale anti-DRM début mai.
