---
site: ZDNet.fr
title: "Brevets : Google réplique aux menaces juridiques contre WebM"
author: Christophe Auffray
date: 2011-04-27
href: http://www.zdnet.fr/actualites/brevets-google-replique-aux-menaces-juridiques-contre-webm-39760273.htm
tags:
- Entreprise
- Brevets logiciels
- Video
---

> Face à l'incertitude juridique brandit par le gérant des droits du format H.264, Google réplique avec l'initiative WebM Community Cross-License, mettant en commun, et sans royalties, les brevets susceptibles de concerner WebM. AMD, Cisco, LG, Samsung et Texas Instruments sont déjà partenaires.
