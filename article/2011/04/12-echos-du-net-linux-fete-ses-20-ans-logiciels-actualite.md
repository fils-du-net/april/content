---
site: Echos du Net
title: "Linux fête ses 20 ans Logiciels Actualité"
author: Infested Grunt
date: 2011-04-12
href: http://www.echosdunet.net/dossiers/dossier_7223_linux+fete+ses+20+ans.html
tags:
- Associations
---

> Voila plus de vingt ans qu'un jeune étudiant a développé un système d'exploitation en mode libre. Depuis, le Manchot concurrence la Fenêtre et de la Pomme et certaines variantes sont populaires sur les ordinateurs, les mobiles et d'autres équipements.
