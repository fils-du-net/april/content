---
site: Silicon.fr
title: "Journée de la propriété intellectuelle : l’April monte au créneau"
author: David Feugey
date: 2011-04-26
href: http://www.silicon.fr/article-50373.html
tags:
- Entreprise
- April
- Brevets logiciels
- DRM
- Droit d'auteur
- International
---

> Selon l’April, le terme de propriété intellectuelle reste trop flou et permet de cacher des idées comme le droit d’auteur ou les brevets logiciels.
