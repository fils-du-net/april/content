---
site: ZDNet.fr
title: "Microsoft Office plus aussi inamovible qu'auparavant en entreprise"
author: Christophe Auffray
date: 2011-04-05
href: http://www.zdnet.fr/actualites/microsoft-office-plus-aussi-inamovible-qu-auparavant-en-entreprise-39759695.htm
tags:
- Entreprise
- Logiciels privateurs
- Interopérabilité
- Standards
---

> Dans un rapport consacré à la maturité et à la gouvernance de l'Open Source, le Cigref note l'opportunité pour les entreprises de remplacer MS Office par une suite bureautique comme OpenOffice, afin notamment d'éviter une coûteuse migration vers Office 2007 et 2010.
