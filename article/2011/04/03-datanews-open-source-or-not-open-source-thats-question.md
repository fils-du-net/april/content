---
site: datanews
title: "Open Source or Not Open Source: That's the Question..."
author: Carlos De Backer
date: 2011-04-03
href: http://datanews.rnews.be/fr/ict/actualite/opinion/open-source-or-not-open-source-that-s-the-question/article-1194982785483.htm
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Éducation
---

> Le débat sur l’utilisation des logiciels open source dans les entreprises, l’administration et l’enseignement revient avec la régularité d’un métronome
