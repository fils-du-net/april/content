---
site: écrans
title: "Le libre-trotter Stallman"
author: Alexandre Hervaud
date: 2011-04-05
href: http://ecrans.fr/Le-libre-trotter-Stallman,12424.html
tags:
- Le Logiciel Libre
- Philosophie GNU
---

> Si le nom de l’américain Richard Stallman vient vite à l’esprit dès qu’il s’agit d’évoquer le logiciel libre, avouons que son site perso n’est pourtant pas sur la liste de nos surfs quotidiens. Et c’est une grave erreur, à en juger par sa teneur, en particulier pour l’espace dédié aux photographies de Stallman en plein travail un peu partout dans le monde.
