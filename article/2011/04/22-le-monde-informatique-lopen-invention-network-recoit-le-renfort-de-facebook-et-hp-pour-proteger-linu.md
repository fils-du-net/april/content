---
site: Le Monde Informatique
title: "L'Open Invention Network reçoit le renfort de Facebook et HP pour protéger Linux"
author: Jean Elyan
date: 2011-04-22
href: http://www.lemondeinformatique.fr/actualites/lire-l-open-invention-network-recoit-le-renfort-de-facebook-et-hp-pour-proteger-linux-33510.html
tags:
- Entreprise
- Associations
- Brevets logiciels
---

> Fondé en 2005 par IBM, NEC, Novell, Philips, Red Hat et Sony, l'Open Invention Network (OIN), vient de gagner 74 nouveaux adhérents parmi lesquels Facebook, HP, Rackspace, Juniper, Fujitsu plus quelques dizaines d'autres entreprises.
