---
site: 20minutes.fr
title: "Facebook lance l'initiative «Open Compute»"
author: Philippe Berry
date: 2011-04-08
href: http://www.20minutes.fr/article/702907/high-tech-facebook-lance-initiative-open-compute
tags:
- Entreprise
- Internet
- Innovation
---

> L'entreprise veut partager ses avancées en matière de centre de données...
