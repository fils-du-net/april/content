---
site: ITespresso.fr
title: "Microsoft devra céder les brevets Novell à Attachmate"
date: 2011-04-26
href: http://www.itespresso.fr/microsoft-devra-ceder-les-brevets-novell-a-attachmate-42435.html
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- Licenses
- International
---

> La justice américaine a décidé de modifier le mode de répartition du partage des brevets de Novell, après la finalisation de son rachat par Attachmate. Microsoft est le premier concerné par cette décision.
