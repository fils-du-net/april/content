---
site: PC INpact
title: "Élus et citoyens écartés du CNN : nombreuses réactions tranchées  "
author: Vincent Hermann
date: 2011-04-27
href: http://www.pcinpact.com/actu/news/63283-cnn-composition-reactions-april-quadrature-ufc-spiil-.htm
tags:
- Entreprise
- Internet
- April
- HADOPI
- Institutions
- Accessibilité
- Associations
---

> C’est aujourd’hui qu’est installé le Conseil National du Numérique, ou CNN. Les 18 membres de cette institution ont été révélés par FrenchWeb la semaine dernière, mais c’est aujourd’hui qu’était entériné officiellement le nouvel organe, avant un discours de Nicolas Sarkozy. Mais avant même que ne commence la mission officielle du Conseil, une chose est sûre : les nominations provoquent des réactions tranchées.
