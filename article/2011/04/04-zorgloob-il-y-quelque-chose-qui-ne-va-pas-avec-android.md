---
site: Zorgloob
title: "Il y a quelque chose qui ne va pas avec Android"
author: Luka
date: 2011-04-04
href: http://www.zorgloob.com/2011/04/04/il-y-a-quelque-chose-qui-ne-va-pas-avec-android%E2%80%A6/
tags:
- Entreprise
- Logiciels privateurs
---

> Android prend une place de plus en plus importante dans l’écosystème des gadgets électroniques. Pourtant, de nombreux problèmes sont apparus et demandent à être corrigés. Notre analyse.
