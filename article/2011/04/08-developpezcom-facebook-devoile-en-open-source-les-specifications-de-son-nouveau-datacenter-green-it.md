---
site: Developpez.com
title: "Facebook dévoile en open source les spécifications de son nouveau Datacenter Green-IT, dans le cadre du projet Open Compute"
date: 2011-04-08
href: http://www.developpez.com/actu/30745/Facebook-devoile-en-open-source-les-specifications-de-son-nouveau-Datacenter-Green-IT-dans-le-cadre-du-projet-Open-Compute/
tags:
- Entreprise
- Internet
- Partage du savoir
---

> Depuis hier, Facebook partage sous licence open source et dans le moindre détail les spécifications matérielles de la technologie utilisée pour réduire la consommation énergétique de ses serveurs et la conception de son propre nouveau Datacenter basé en Orégan aux États-Unis.
