---
site: OWNI
title: "Licence Edition Equitable : vers des archives ouvertes ?"
author: Calimaq
date: 2011-04-29
href: http://owni.fr/2011/04/29/licence-edition-equitable-vers-des-archives-ouvertes/
tags:
- Internet
- Économie
- Interopérabilité
- DRM
- Droit d'auteur
- Licenses
- Contenus libres
---

> La Licence Édition Équitable pourrait bien être la solution à l'impasse dans laquelle se trouve l'édition numérique : dissocier les droits de l'auteur, de l'éditeur et du lecteur et adopter un modèle proche de l'Open Access. La discussion est ouverte.
