---
site: Silicon MANIACS
title: "Loïc Dachary, compagnon du Tour du Monde du Libre / Le Web Mis à Nu #9"
date: 2011-04-28
href: http://www.siliconmaniacs.org/loic-dachary-compagnon-du-tour-du-monde-du-libre-le-web-mis-a-nu-9/
tags:
- April
- Philosophie GNU
---

> Unique membre d'honneur de l'April, fondateur de FSF France, Loïc Dachary est surtout un compagnon du Tour du Monde du Libre. Rencontre avec un ouvrier fondateur du monde du Libre.
