---
site: ITRmanager.com
title: "L’Open Source à géométrie variable…"
author: Stéphane Sabbague
date: 2011-04-14
href: http://www.itrmanager.com/articles/117730/open-source-geometrie-variable-stephane-sabbague-president-calipia.html
tags:
- Entreprise
- Internet
- Licenses
---

> L’Open Source n’est-il pas finalement un concept à géométrie variable ou, soyons provocateur, un simple argument marketing, voire le moyen de commencer un travail à bon compte pour mieux s’approprier ensuite le résultat et ses évolutions, mais cette fois-ci sans partage ?
