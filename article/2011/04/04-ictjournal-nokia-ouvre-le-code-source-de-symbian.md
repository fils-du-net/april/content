---
site: ICTjournal
title: "Nokia ouvre le code source de Symbian"
author: Nicolas Paratte
date: 2011-04-04
href: http://www.ictjournal.ch/fr-CH/News/2011/04/04/Nokia-rouvre-le-code-source-de-Symbian.aspx
tags:
- Entreprise
- Internet
- Logiciels privateurs
---

> Nokia a décidé d’ouvrir le code source de sa dernière version de Symbian, indique le fabricant finlandais sur son nouveau site consacré à son système d’exploitation mobile
