---
site: PC INpact
title: "Copie privée : Free prêt à payer mais à un niveau raisonnable"
author: Marc Rees
date: 2011-04-21
href: http://www.pcinpact.com/actu/news/63209-freebox-revolution-free-copie-privee.htm
tags:
- Entreprise
- Internet
---

> La Freebox doit-elle être taxée à la Rémunération pour copie privée ? Free estime pour l’instant que le disque dur de 250 Go installé dans la box échappe aux 35 euros normalement exigés pour une telle capacité sur un disque dur multimédia. Free considère en effet que sa V6 est partie intégrante du réseau, comme un espace de stockage en ligne, et échappe donc à la RCP du moins dans son barème actuel. Pour autant, Free sait que sa position peut être remise en cause par la Commission copie privée et tente donc de limiter la (le) casse.
