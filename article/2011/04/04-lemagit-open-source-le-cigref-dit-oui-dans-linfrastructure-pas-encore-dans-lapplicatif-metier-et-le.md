---
site: LeMagIT
title: "Open Source : le Cigref dit oui dans l'infrastructure, pas encore dans l'applicatif métier et le poste de travail "
author: Cyrille Chausson
date: 2011-04-04
href: http://www.lemagit.fr/article/mobilite-france-crm-decisionnel-developpement-entreprises-openoffice-opensource-cigref-talend-erp/8470/1/open-source-cigref-dit-oui-dans-infrastructure-pas-encore-dans-applicatif-metier-poste-travail/
tags:
- Entreprise
- Internet
- Associations
---

> Dans leur dernier rapport, les entreprises du Cigref cartographient des solutions Open Source, en pointant des degrés de maturité très variables selon les segments. Si les solutions de développement et Internet ont acquis leurs lettres de noblesse et sont considérées comme matures, le reste doit encore évoluer, surtout du côté des applicatifs métiers et de la mobilité.
