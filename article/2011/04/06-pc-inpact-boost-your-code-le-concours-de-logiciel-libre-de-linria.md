---
site: PC INpact
title: "Boost your code : le concours de logiciel libre de l'INRIA"
author: David Legrand
date: 2011-04-06
href: http://www.pcinpact.com/actu/news/62912-boost-your-code-inria-concours-logiciel-libre.htm
tags:
- Internet
- Éducation
---

> L'INRIA (Institut National de Recherche en Informatique et en Automatique) a donné il y a quelques jours le coup d'envoi de son concours dédié aux développeurs de logiciels libres : Boost your code. À la clef ? Un contrat d'un an qui vous permettra de développer votre projet au sein de l'institut.
