---
site: EPN de Wallonie
title: "L’Orme 2011 (2) : Charlie et le logiciel libre"
author: Damien Panerai
date: 2011-04-17
href: http://www.epn-ressources.be/lorme-2011-2-charlie-et-le-logiciel-libre
tags:
- Internet
- Interopérabilité
- April
- HADOPI
- Institutions
- Associations
- Brevets logiciels
- DADVSI
- DRM
- Droit d'auteur
- International
- ACTA
---

> Aux Rencontres de l’Orme de Marseille, un espace était dédié aux logiciels libres. J’y ai d’abord rencontré Yves Specht, fondateur de l’association Mandr’Aix (antenne aixoise de Mandriva), qui me présenta son GULL (Groupe d’Utilisateurs de Logiciels Libres) et sa carte du réseau libre.
