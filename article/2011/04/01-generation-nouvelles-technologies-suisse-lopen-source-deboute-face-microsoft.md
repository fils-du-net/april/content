---
site: Génération nouvelles technologies
title: "Suisse : l'Open Source débouté face à Microsoft"
author: Jérôme G.
date: 2011-04-01
href: http://www.generation-nt.com/suisse-microsoft-open-source-justice-actualite-1183881.html
tags:
- Entreprise
- Internet
- Institutions
- Marchés publics
- International
---

> Le Tribunal fédéral a rejeté un recours de plusieurs fournisseurs de logiciels libres qui pestaient alors que la Confédération suisse avait attribué un contrat à Microsoft sans appel d'offres préalable.
