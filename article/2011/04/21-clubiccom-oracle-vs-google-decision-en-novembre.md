---
site: clubic.com
title: "Oracle vs. Google : décision en novembre"
author: Antoine Duvauchelle
date: 2011-04-21
href: http://pro.clubic.com/legislation-loi-internet/propriete-intellectuelle/brevet-logiciel/actualite-413786-oracle-vs-google-decision.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
---

> L'affaire Oracle-Google pourrait connaître un aboutissement en novembre prochain. Le tribunal chargé de déterminer si Google viole la propriété intellectuelle d'Oracle sur Java avec son outil de compatibilité Dalvik au sein d'Android a décidé qu'il rendrait sa décision avant cette date.
