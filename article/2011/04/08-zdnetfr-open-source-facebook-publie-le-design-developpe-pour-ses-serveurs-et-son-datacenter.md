---
site: ZDNet.fr
title: "Open source : Facebook publie le design développé pour ses serveurs et son datacenter"
date: 2011-04-08
href: http://www.zdnet.fr/actualites/open-source-facebook-publie-le-design-developpe-pour-ses-serveurs-et-son-datacenter-39759782.htm
tags:
- Entreprise
- Internet
- Innovation
- Contenus libres
---

> Une équipé d'ingénieurs Facebook a travaillé presque 2 ans pour concevoir un serveur et un datacenter adaptés aux besoins de l'entreprise et plus économe en énergie. Le fruit de ce travail est rendu totalement public.
