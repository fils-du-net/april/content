---
site: L'INFORMATICIEN
title: "Facebook et d'autres sociétés se regroupent pour défendre les brevets Linux"
author: Sarah Lefevre
date: 2011-04-20
href: http://linformaticien.com/actualites/id/20433/facebook-et-d-autres-societes-se-regroupent-pour-defendre-les-brevets-linux.aspx
tags:
- Entreprise
- Associations
- Brevets logiciels
---

> L'Open Invention Network, fondé en 2005 -par IBM, NEC, Novell, Phillips, Red Hat et Sony-, a pour mission de protéger les utilisateurs de Linux contre des procès potentiels.
