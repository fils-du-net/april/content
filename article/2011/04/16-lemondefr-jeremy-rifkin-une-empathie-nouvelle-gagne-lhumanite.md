---
site: LeMonde.fr
title: "Jeremy Rifkin : \"Une empathie nouvelle gagne l'humanité\""
author: Frédéric Joignot
date: 2011-04-16
href: http://www.lemonde.fr/week-end/article/2011/04/15/jeremy-rifkin-une-empathie-nouvelle-gagne-l-humanite_1507194_1477893.html
tags:
- Internet
- Économie
---

> Les réactions aux séismes japonais et aux révolutions arabes montrent qu'avec la mondialisation émergent aussi des sentiments altruistes, généreux.C'est l'idée que défend l'essayiste américain Jeremy Rifkin dans son nouveau livre.
