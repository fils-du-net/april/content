---
site: igeneration
title: "Nokia : un nouveau Symbian qui n'est plus open-source"
author: Anthony Nelzin
date: 2011-04-11
href: http://www.igeneration.fr/0-apple/nokia-un-nouveau-symbian-qui-n-est-plus-open-source-41922
tags:
- Entreprise
- Logiciels privateurs
---

> Pas open-source, open-source, plus open-source : Symbian a changé de licence au moins autant de fois qu'il a changé de direction, perdant inexorablement des parts d'usage pendant ce temps-là. À la veille de la présentation des dernières nouveautés au sujet de Symbian, Nokia a tenu à clarifier son statut : non, son OS mobile n'est plus open-source.
