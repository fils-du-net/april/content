---
site: L'INFORMATICIEN
title: "Oracle cède le contrôle d'OpenOffice à la communauté open-source"
author: Sarah Lefevre
date: 2011-04-18
href: http://www.linformaticien.com/actualites/id/20394/oracle-cede-le-controle-d-openoffice-a-la-communaute-open-source.aspx
tags:
- Entreprise
- Associations
---

> Passant de tout à rien, Oracle a décidé, contre toute attente, de confier les rênes du projet OpenOffice.org à la communauté des développeurs du libre.
