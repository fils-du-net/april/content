---
site: Mediapart
title: "Démocratie et éducation citoyenne au Forum social de Provence le 16 avril à Gardanne"
author: Anne Marie ALARY
date: 2011-04-10
href: http://blogs.mediapart.fr/edition/cent-paroles-d-aix-journal-local-alternatif/article/100411/democratie-et-education-cito
tags:
- Institutions
---

> Pour fonctionner, la démocratie est de façon urgente à réinventer ; elle doit se conjuguer dans plusieurs domaines, politique, social ; mais elle n’est pas spontanée, elle passe par l’éducation citoyenne ; elle demande également des outils, notamment les médias. L’atelier abordera tour à tour : démocratie &amp; politique, démocratie sociale, éducation citoyenne, médias &amp; démocratie. La question des logiciels libres sera également abordée.
