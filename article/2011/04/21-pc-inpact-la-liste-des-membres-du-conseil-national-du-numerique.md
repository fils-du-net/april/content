---
site: PC INpact
title: "La liste des membres du Conseil national du Numérique"
author: Marc Rees
date: 2011-04-21
href: http://www.pcinpact.com/actu/news/63205-cnn-liste-membre-lcen-besson.htm
tags:
- Entreprise
- Internet
- Économie
- Institutions
---

> Le site Frenchweb a publié hier en exclusivité la liste des 18 membres "pressentis" pour composer le CNN, le fameux Conseil National du Numérique.
