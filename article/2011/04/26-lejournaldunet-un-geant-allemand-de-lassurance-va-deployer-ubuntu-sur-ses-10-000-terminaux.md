---
site: LeJournalduNet
title: "Un géant allemand de l'assurance va déployer Ubuntu sur ses 10 000 terminaux"
author: La rédaction
date: 2011-04-26
href: http://www.journaldunet.com/solutions/systemes-reseaux/open-source-lvm-versicherungen-choisi-linux-et-ubuntu-0411.shtml
tags:
- Entreprise
- International
---

> Canonical annonce que LVM Versicherungen va délaisser Windows pour son système d'exploitation Open Source.
