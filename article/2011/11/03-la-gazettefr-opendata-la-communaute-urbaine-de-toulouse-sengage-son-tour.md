---
site: La gazette.fr
title: "Opendata : la communauté urbaine de Toulouse s'engage à son tour"
author: L. Lafosse
date: 2011-11-03
href: http://www.lagazettedescommunes.com/82425/opendata-la-communaute-urbaine-de-toulouse-sengage-a-son-tour/
tags:
- Internet
- Administration
- April
- Licenses
- Open Data
---

> La communauté urbaine du grand Toulouse a ouvert le 22 octobre son portail de données publique Grand Toulouse.data. Les communes de Balma, Colomiers et Blagnac ont emboîté le pas à Toulouse, pour la mise à disposition de leurs données publiques.
