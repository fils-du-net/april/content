---
site: Les Echos
title: "Améliorer la pédagogie, un enjeu mondial, Actualités"
author: JEAN-CLAUDE LEWANDOWSKI
date: 2011-11-08
href: http://www.lesechos.fr/economie-politique/france/actu/0201726724170-ameliorer-la-pedagogie-un-enjeu-mondial-245474.php
tags:
- Éducation
- International
---

> Comment améliorer l'efficacité de l'enseignement ? Comment faire en sorte que les connaissances et les savoir-faire transmis soient mieux assimilés par les apprenants, qu'ils soient élèves du primaire ou du secondaire, étudiants ou même adultes en formation continue ? Comment donner au plus grand nombre un accès au savoir ? Enseignants, chercheurs, décideurs politiques et responsables d'ONG du monde entier se sont penchés sur ces questions, la semaine dernière, au Qatar, à l'occasion de la troisième édition du sommet Wise (World Innovation Summit for Education).
