---
site: LeJournalduNet
title: "\"Nous avons économisé des centaines de milliers d'euros grâce à OpenERP\""
author: Antoine CROCHET-DAMAIS
date: 2011-11-10
href: http://www.journaldunet.com/solutions/dsi/didier-georgieff-openerp-a-l-ena.shtml?f_id_newsletter=5983
tags:
- Entreprise
- Administration
- Économie
- Licenses
- Marchés publics
---

> didier giogieff enaGestion des paiements, des documents bureautiques, des inscriptions, des cours... L'École nationale d'administration a modélisé toutes ses activités au sein du progiciel Open Source. Une interview réalisée lors de l'Open CIO Summit.
