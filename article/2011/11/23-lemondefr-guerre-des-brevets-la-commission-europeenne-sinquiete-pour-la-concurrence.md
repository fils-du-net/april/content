---
site: LeMonde.fr
title: "Guerre des brevets : la Commission européenne s'inquiète pour la concurrence"
date: 2011-11-23
href: http://www.lemonde.fr/technologies/article/2011/11/23/samsung-apple-la-commission-europeenne-inquiete-pour-la-concurrence_1607948_651865.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Europe
---

> La guerre des brevets que se livrent Apple et Samsung dans plusieurs pays préoccupe les autorités européennes. Le commissaire européen à la concurrence, Joaquín Almunia, a ainsi estimé mardi 22 novembre que "la standardisation et la propriété intellectuelle sont deux instruments qui, dans ce nouveau secteur technologique, peuvent être sources d'abus", rapporte Reuters. Début novembre, la commission européenne a ouvert une enquête sur l’utilisation de brevets dans les affaires qui opposent les deux géants de la téléphonie, devenus des normes par leur utilisation globale.
