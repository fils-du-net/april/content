---
site: PC INpact
title: "L'UFC-Que Choisir attaque 3 distributeurs et 4 éditeurs de jeux vidéo"
author: Nil Sanyas
date: 2011-11-29
href: http://www.pcinpact.com/news/67318-ufcque-choisir-plainte-editeurs-jeux-video.htm
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Associations
- DRM
---

> Après les abus des opérateurs mobiles ou encore la rémunération copie privée, l’UFC-Que Choisir s’attaque à un autre secteur : les jeux vidéo. Non, l’association ne se transforme pas en Familles de France et ne souhaite pas éradiquer les jeux violents. L’UFC souhaite au contraire que l’on respecte les joueurs en améliorant son expérience utilisateur. Des plaintes ont été déposées contre trois distributeurs et quatre éditeurs de jeux.
