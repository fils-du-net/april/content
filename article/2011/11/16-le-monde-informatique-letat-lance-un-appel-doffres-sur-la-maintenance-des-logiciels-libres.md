---
site: Le Monde Informatique
title: "L'Etat lance un appel d'offres sur la maintenance des logiciels libres"
author: Bertand Lemaire
date: 2011-11-16
href: http://www.lemondeinformatique.fr/actualites/lire-l-etat-lance-un-appel-d-offres-sur-la-maintenance-des-logiciels-libres-46661.html
tags:
- Entreprise
- Administration
- Institutions
- Marchés publics
---

> L'Etat vient de publier un appel d'offres d'une valeur estimée de deux millions d'euros couvrant un large périmètre tant applicatif qu'organisationnel.
