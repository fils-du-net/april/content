---
site: Rue89
title: "Berlusconi et Murdoch dans le même vieux bateau qui coule"
author: Marthe Machorowski
date: 2011-11-10
href: http://www.rue89.com/2011/11/10/berlusconi-et-murdoch-dans-le-meme-vieux-bateau-qui-coule-226419
tags:
- Entreprise
- Internet
- Économie
- Institutions
- Désinformation
- International
---

> La quasi coïncidence de la chute des deux raïs des médias parmi les plus symboliques et politiquement influents des trente dernières années ne peut être détachée de l'émergence des nouveaux médias, qui ont largement ébranlé les fondations des leurs édifices médiatiques.
