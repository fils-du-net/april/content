---
site: Le nouvel Observateur
title: "Après les logiciels libres, la génétique libre... avec les biohackers ?"
author: Dorothee BROWAEYS
date: 2011-11-23
href: http://leplus.nouvelobs.com/contribution/215714;apres-les-logiciels-libres-la-genetique-libre-avec-les-biohackers.html
tags:
- Partage du savoir
- Innovation
---

> Aujourd'hui, on peut décrire le génome d'une personne, mais la pratique reste très coûteuse. Plus pour longtemps depuis que des jeunes gens se sont emparés de la biologie pour trafiquer les molécules du vivant, dans leurs garages, chez eux, pour des projets citoyens.
