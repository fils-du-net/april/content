---
site: Developpez.com
title: "Flex : Adobe veut faire don du SDK à l'open-source et crée la confusion dans sa communauté de développeurs"
date: 2011-11-16
href: http://www.developpez.com/actu/38976/Flex-Adobe-veut-faire-don-du-SDK-a-l-open-source-et-cree-la-confusion-dans-sa-communaute-de-developpeurs/
tags:
- Entreprise
- Logiciels privateurs
- Associations
- Licenses
---

> Le SDK d'Adobe Flex ira finalement à la fondation Apache
> En compagnie de BlazeDS, Spark et Falcon, l'Open Spoon Foundation jouera l'intermédiaire
