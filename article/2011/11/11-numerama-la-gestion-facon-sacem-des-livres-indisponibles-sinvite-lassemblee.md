---
site: Numerama
title: "La gestion façon Sacem des livres indisponibles s'invite à l'Assemblée"
author: Julien L.
date: 2011-11-11
href: http://www.numerama.com/magazine/20537-la-gestion-facon-sacem-des-livres-indisponibles-s-invite-a-l-assemblee.html
tags:
- Entreprise
- Institutions
- Droit d'auteur
---

> Après le Sénat, c'est au tour de l'Assemblée nationale de réceptionner une proposition de loi destinée à mettre en place une gestion collective sur le modèle des sociétés de gestion des droits d'auteur pour s'occuper des livres indisponibles du 20ème siècle. Le texte est très contesté, dans la mesure où il risque de léser l'auteur lui-même.
