---
site: LE CERCLE Les Echos
title: "Et si les brevets étaient nuisibles..."
author: frederic cherbonnier et Francois Salanie
date: 2011-11-02
href: http://lecercle.lesechos.fr/economie-societe/recherche-innovation/innovation/221139496/et-si-brevets-etaient-nuisibles
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Innovation
---

> A première vue, tout est simple : sans protection des inventeurs, pas d'innovation. Qui voudrait investir dans la recherche pour voir ses découvertes aussitôt copiées ? Il faut donc octroyer des droits de propriété intellectuels, autrement dit, des brevets, garantissant à l'inventeur une exclusivité sur l'usage futur de ses découvertes.
