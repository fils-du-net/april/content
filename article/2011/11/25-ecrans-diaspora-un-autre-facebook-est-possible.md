---
site: écrans
title: "Diaspora*, un autre Facebook est possible"
author: Camille Gévaudan
date: 2011-11-25
href: http://www.ecrans.fr/Diaspora-un-autre-Facebook-est,13606.html
tags:
- Entreprise
- Internet
---

> En septembre 2010, on suivait avec une attention toute particulière les premiers pas de Diaspora*, projet de réseau social alterFacebookiste et très strict sur les questions de vie privée. Ses quatre papas, étudiants new-yorkais en sciences informatiques, promettaient un système entièrement transparent, open source et décentralisé, pour rendre aux internautes le contrôle de leurs données personnelles en ligne. Plus d’un an s’est écoulé.
