---
site: Numerama
title: "Amazon libère le code des Kindle mais interdit son utilisation"
author: Simon Robic
date: 2011-11-30
href: http://www.numerama.com/magazine/20756-amazon-libere-le-code-des-kindle-mais-interdit-son-utilisation.html
tags:
- Entreprise
- DRM
- Licenses
---

> Amazon vient de libérer les modifications apportées à Android pour son Kindle Fire. Le code rejoint ainsi les autres versions du système d'exploitation de ses différentes liseuses. Mais la firme interdit de l'utiliser sur un autre appareil.
