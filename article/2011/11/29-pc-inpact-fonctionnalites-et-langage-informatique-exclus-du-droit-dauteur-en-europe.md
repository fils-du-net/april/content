---
site: PC INpact
title: "Fonctionnalités et langage informatique exclus du droit d'auteur en Europe"
author: Marc Rees
date: 2011-11-29
href: http://www.pcinpact.com/news/67321-interoperabilite-cjue-langage-informatique-code.htm
tags:
- Interopérabilité
- Institutions
- Droit d'auteur
- Europe
---

> Est-ce qu'un programme d'ordinateur et langage de programmation sont ou non protégés par le droit d'auteur ? À la demande de la justice anglaise, la Cour de Justice de Luxembourg doit répondre à cette problématique qui cache, en réalité, un flot de questions touchant à l'interopérabilité. Avec les conclusions de l'avocat général de la CJUE, tout juste publiées, plusieurs pistes de réponses nous sont d'ores et déjà apportées.
