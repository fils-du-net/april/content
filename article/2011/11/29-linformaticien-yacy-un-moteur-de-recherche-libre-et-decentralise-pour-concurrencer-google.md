---
site: L'INFORMATICIEN
title: "YaCy : un moteur de recherche libre et décentralisé pour concurrencer Google"
author: Orianne Vatin
date: 2011-11-29
href: http://www.linformaticien.com/actualites/id/22396/yacy-un-moteur-de-recherche-libre-et-decentralise-pour-concurrencer-google.aspx
tags:
- Entreprise
- Internet
- Associations
---

> Des activistes amoureux du logiciel libre ont donné vie à YaCy, un moteur peer-to-peer qui offre un nouveau visage à la recherche en ligne. Il espère prendre un peu de parts de marché à Google, Yahoo, Bing, et les autres.
