---
site: Developpez.com
title: "Android « repose sur les épaules de Microsoft »"
date: 2011-11-01
href: http://www.developpez.com/actu/38531/Android-repose-sur-les-epaules-de-Microsoft-affirme-un-avocat-de-l-entreprise-au-sujet-des-redevances-imposees-aux-constructeurs/
tags:
- Entreprise
- Brevets logiciels
---

> Affirme un avocat de Redmond en expliquant les redevances imposées aux constructeurs
