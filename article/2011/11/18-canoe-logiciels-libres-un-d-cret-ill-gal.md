---
site: canoe
title: "Logiciels libres: un décret illégal?"
author: Taïeb Moalla
date: 2011-11-18
href: http://fr.canoe.ca/infos/quebeccanada/politiqueprovinciale/archives/2011/11/20111118-191534.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Institutions
---

> Les partisans du logiciel libre qualifient de «douteux» et de «probablement contestable» un récent décret consacré à l’acquisition de logiciels informatiques par le gouvernement.
