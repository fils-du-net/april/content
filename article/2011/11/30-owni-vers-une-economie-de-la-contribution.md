---
site: OWNI
title: "Vers une économie de la contribution"
author: Quentin Noirfalisse
date: 2011-11-30
href: http://owni.fr/2011/11/30/vers-une-economie-de-la-contribution/
tags:
- Entreprise
- Internet
- Économie
- Partage du savoir
- Associations
---

> La solution du capitalisme à bout de souffle pourrait bien être à chercher du côté des logiciels libres. Bernard Stiegler, philosophe, appelle à passer "du consumérisme toxique à une économie de la contribution".
