---
site: Numerama
title: "Les licences libres et leurs enjeux enseignés à l'école"
author: Julien L.
date: 2011-11-23
href: http://www.numerama.com/magazine/20688-les-licences-libres-et-leurs-enjeux-enseignes-a-l-ecole.html
tags:
- Entreprise
- Logiciels privateurs
- April
- Institutions
- Droit d'auteur
- Éducation
- Licenses
- Standards
- Contenus libres
---

> L'éducation nationale a indiqué que les élèves de terminale de la série scientifique recevront à partir de la rentrée prochaine un enseignement destiné à les sensibiliser aux différents types de licences et aux notions de non-rivalité des biens immatériels. Une avancée a première vue positive, mais encore bien timide.
