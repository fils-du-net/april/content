---
site: Numerama
title: "L'absurdité des DRM en une image"
author: Guillaume Champeau
date: 2011-11-02
href: http://www.numerama.com/magazine/20409-l-absurdite-des-drm-en-une-image.html
tags:
- Logiciels privateurs
- DRM
- Licenses
---

> Sous Windows, ouvrez un contenu Flash dans votre navigateur, et faites un clic droit pour accéder aux "paramètres globaux" de Flash Player 11. Dans l'onglet "avancé", Adobe propose aux utilisateurs de supprimer les données en cache stockées sur l'ordinateur, ce qui peut être utile pour gagner de la place sur le disque dur ou résoudre un problème quelconque lié au cache. Mais l'option propose aussi de "supprimer tous les fichiers de licence audio et vidéo", avec une explication qui fait se demander jusqu'où peut aller l'absurdité des DRM.
