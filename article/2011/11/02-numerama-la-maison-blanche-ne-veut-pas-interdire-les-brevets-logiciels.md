---
site: Numerama
title: "La Maison Blanche ne veut pas interdire les brevets logiciels"
author: Guillaume Champeau
date: 2011-11-02
href: http://www.numerama.com/magazine/20400-la-maison-blanche-ne-veut-pas-interdire-les-brevets-logiciels.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Institutions
- DRM
---

> Réagissant à une pétition lancée sur son site internet, la Maison Blanche estime qu'elle ne peut pas interdire les brevets sur les logiciels, et assure que la communauté open source n'est pas menacée par leur existence.
