---
site: LADEPECHE.fr
title: "Fab Lab, les secrets de fabrique en libre accès"
author: Thomas Belet
date: 2011-11-02
href: http://www.ladepeche.fr/article/2011/11/02/1205788-fab-lab-les-secrets-de-fabrique-en-libre-acces.html
tags:
- Entreprise
- Matériel libre
- Associations
- Open Data
---

> Apparu aux États-Unis, le Fab Lab (laboratoire de fabrication) s'est implanté en France en commençant par Toulouse. Sur un modèle communautaire, il a pour vocation de permettre à tous d'utiliser des moyens de fabrication de pointe.
