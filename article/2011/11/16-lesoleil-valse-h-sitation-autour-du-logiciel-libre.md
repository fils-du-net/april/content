---
site: leSoleil
title: "Valse-hésitation autour du logiciel libre"
author: Pierre Asselin
date: 2011-11-16
href: http://www.cyberpresse.ca/le-soleil/affaires/techno/201111/15/01-4468311-valse-hesitation-autour-du-logiciel-libre.php
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Institutions
- Associations
- Marchés publics
- International
---

> Moins d'un an après avoir ouvert la porte au logiciel libre avec la Politique-cadre sur la gestion des ressources informationnelles, le Conseil des ministres vient d'adopter un décret qui protège l'exclusivité des grandes sociétés, pour certains contrats.
