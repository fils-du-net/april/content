---
site: ZDNet.fr
title: "Sans fil et sans chaînes? La liberté vient aux mobiles"
author: Patrice Bertrand
date: 2011-11-10
href: http://www.zdnet.fr/blogs/l-esprit-libre/sans-fil-et-sans-cha-nes-la-liberte-vient-aux-mobiles-par-patrice-bertrand-39765520.htm
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Interopérabilité
- Standards
---

> La liberté et les standards ouverts, deux préoccupations essentielles autour des mobiles et de l'open source. C'est ce qu'aborde dans cette tribune Patrice Bertrand, président du CNLL et DG de Smile.
