---
site: ":: S.I.Lex ::"
title: "Numérisation : la grande manoeuvre des indisporphelines"
author: calimaq
date: 2011-11-12
href: http://scinfolex.wordpress.com/2011/11/12/numerisation-la-grande-manoeuvre-des-indisporphelines/
tags:
- Entreprise
- Internet
- Administration
- Partage du savoir
- Institutions
- Droit d'auteur
- Licenses
- Europe
---

> L’IABD (Interassociation Archives Bibliothèques Documentation) a publié jeudi un communiqué par lequel elle prend position sur la question de la numérisation des livres indisponibles et orphelins, qui font actuellement l’objet d’une proposition de loi déposée le mois dernier au Sénat et cette semaine à l’Assemblée.
