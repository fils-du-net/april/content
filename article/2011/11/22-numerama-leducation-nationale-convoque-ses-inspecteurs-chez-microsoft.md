---
site: Numerama
title: "L'éducation nationale convoque ses inspecteurs chez Microsoft"
author: Julien L.
date: 2011-11-22
href: http://www.numerama.com/magazine/20670-l-education-nationale-convoque-ses-inspecteurs-chez-microsoft.html
tags:
- Entreprise
- Logiciels privateurs
- Administration
- April
- Institutions
- Associations
- Éducation
- Europe
---

> À la veille du salon de l'éducation Éducatec-Éducatice 2011, l'April et Framasoft déplorent la manœuvre de Microsoft. Le géant des logiciels accueille en effet ce mardi les inspecteurs de l'éducation nationale, sur convocation du ministère.
