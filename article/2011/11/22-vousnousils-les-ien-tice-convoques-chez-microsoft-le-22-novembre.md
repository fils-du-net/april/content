---
site: vousnousils
title: "Les IEN-TICE convoqués chez Microsoft le 22 novembre"
author: Quentin Duverger
date: 2011-11-22
href: http://www.vousnousils.fr/?p=517038
tags:
- Entreprise
- Administration
- April
- Institutions
- Éducation
---

> Pour la pre­mière jour­née des ren­contres annuelles des IEN-TICE, les ins­pec­teurs ont été convo­qués au siège de Microsoft, à Issy-les-Moulineaux.
