---
site: ActuaLitté.com
title: "Vers le piratage légalisé du patrimoine écrit du XXe siècle ?"
author: Clement Solym
date: 2011-11-14
href: http://www.actualitte.com/actualite/monde-edition/societe/vers-le-piratage-legalise-du-patrimoine-ecrit-du-xxe-siecle-29789.htm
tags:
- Entreprise
- Internet
- Institutions
---

> Ou comment l'homme qui a vu le Livre fait passer une étrange pilule...
