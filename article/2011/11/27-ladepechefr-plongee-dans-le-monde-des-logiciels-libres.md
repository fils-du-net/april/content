---
site: LADEPECHE.fr
title: "Plongée dans le monde des logiciels libres"
author: Th G.
date: 2011-11-27
href: http://www.ladepeche.fr/article/2011/11/27/1225538-plongee-dans-le-monde-des-logiciels-libres.html
tags:
- Sensibilisation
- Associations
---

> Les associations Toulibre et Ubuntu-fr organisent ce week-end à l'ENSEEIHT la première édition de « Capitole du Libre »,
