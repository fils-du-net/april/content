---
site: "business-MOBILE.FR"
title: "Android : un responsable de Google accuse les éditeurs d'anti-virus de \"charlatanisme\""
author: Olivier Chicheportiche
date: 2011-11-23
href: http://www.businessmobile.fr/actualites/android-un-responsable-de-google-accuse-les-editeurs-d-anti-virus-de-charlatanisme-39765888.htm
tags:
- Entreprise
- Logiciels privateurs
- Désinformation
---

> Plusieurs spécialistes de la sécurité ont pointé du doigt une flambée des menaces touchant Android. Pour Chris DiBona, responsable des logiciels Open Source à Mountain View, il s'agit avant tout de faire peur.
