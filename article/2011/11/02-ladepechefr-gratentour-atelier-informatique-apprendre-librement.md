---
site: LADEPECHE.fr
title: "Gratentour. Atelier informatique : apprendre librement"
author: J-L. L. T.
date: 2011-11-02
href: http://www.ladepeche.fr/article/2011/11/02/1205972-gratentour-atelier-informatique-apprendre-librement.html
tags:
- Administration
- Partage du savoir
- Associations
---

> L'Atelier Informatique de Gratentour est un GUL autrement dit un Groupe d'Utilisateurs de Linux. Vous pourrez venir faire installer Linux sur votre PC, obtenir de l'assistance sur les logiciels libres, apprendre à connaître les possibilités concernant les alternatives gratuites aux logiciels payants, il s'agit d'un choix financier logique.
