---
site: LeJournalduNet
title: "Le Conseil National du Logiciel Libre se lance dans la présidentielle"
author: Antoine CROCHET-DAMAIS
date: 2011-11-14
href: http://www.journaldunet.com/solutions/acteurs/le-conseil-national-du-logiciel-libre-dans-la-presidentielle-1111.shtml
tags:
- Entreprise
- April
- Institutions
- Associations
---

> En lien avec l'Aful, l'April et OW2, le CNLL dévoile la liste des questions qu'il compte poser aux candidats à la présidentielle sur la thématique Open Source.
