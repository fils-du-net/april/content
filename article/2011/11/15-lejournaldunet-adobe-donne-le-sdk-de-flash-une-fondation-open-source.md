---
site: LeJournalduNet
title: "Adobe donne le SDK de Flash à une fondation Open Source"
author: Antoine CROCHET-DAMAIS
date: 2011-11-15
href: http://www.journaldunet.com/developpeur/flash/adobe-flex-sdk-en-open-source-1111.shtml
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Standards
---

> L'éditeur a annoncé son intention de faire don du kit de développement Flex à une fondation Open Source.
