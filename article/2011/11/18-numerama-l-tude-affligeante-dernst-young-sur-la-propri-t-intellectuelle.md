---
site: Numerama
title: "L'étude affligeante d'Ernst &amp; Young sur la propriété intellectuelle"
author: Guillaume Champeau
date: 2011-11-18
href: http://www.numerama.com/magazine/20625-l-etude-affligeante-d-ernst-young-sur-la-propriete-intellectuelle.html
tags:
- Entreprise
- Internet
- HADOPI
- Désinformation
- DRM
- Droit d'auteur
- Informatique en nuage
- International
- ACTA
---

> A l'occasion du Forum d'Avignon, qui ouvre ce vendredi, le cabinet Ernst &amp; Young a livré son étude sur la propriété intellectuelle à l'ère du numérique. Un morceau d'anthologie.
