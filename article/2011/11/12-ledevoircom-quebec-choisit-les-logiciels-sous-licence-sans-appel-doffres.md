---
site: LeDevoir.com
title: "Québec choisit les logiciels sous licence, sans appel d'offres"
author: Fabien Deglise
date: 2011-11-12
href: http://www.ledevoir.com/politique/quebec/335976/informatique-quebec-choisit-les-logiciels-sous-licence-sans-appel-d-offres
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Associations
- Marchés publics
- International
---

> Dérive informatique? Le gouvernement Charest vient d'adopter dans la plus grande discrétion un décret qui, pour les trois prochaines années, va faciliter l'acquisition sans appel d'offres de logiciels informatiques sous licence par les ministères et organismes publics, a appris Le Devoir.
