---
site: LeMagIT
title: "Le NYSE offre son API de messaging à la Linux Foundation"
author: Cyrille Chausson
date: 2011-11-07
href: http://www.lemagit.fr/article/developpement-open-source/9828/1/le-nyse-offre-son-api-messaging-linux-foundation/
tags:
- Entreprise
- Standards
---

> NYSE Technologies, la branche technologique du New York Stock Exchange (la bourse de New York) a décidé de faire don de son API interne de gestion des messages (Middleware Agnostic Messaging Application Programming Interface - MAMA) à l'Open Source
