---
site: EurActiv
title: "Le cloud computing pourrait réduire les émissions de carbone "
date: 2011-11-07
href: http://www.euractiv.com/fr/societe-information/le-cloud-computing-pourrait-duire-les-missions-de-carbone-news-508784
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Associations
- Europe
---

> Selon une étude, les grandes entreprises pourraient réduire leurs émissions de carbone de 50 % en transférant leurs opérations de stockage de données sur le nuage.
