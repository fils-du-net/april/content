---
site: LeJournalduNet
title: "\"HTML5 ne va pas remplacer Flash\""
author: Antoine CROCHET-DAMAIS
date: 2011-11-24
href: http://www.journaldunet.com/developpeur/flash/michael-chaize-html5-vs-flash-la-position-d-adobe.shtml
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Associations
- Licenses
- Standards
---

> Arrêt de Flash sur le mobile, commercialisation d'une offre pour HTML5, développement d'une culture Open Source... Adobe s'oriente vers une mutation profonde de sa stratégie.
