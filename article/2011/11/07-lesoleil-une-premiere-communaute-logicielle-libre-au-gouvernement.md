---
site: leSoleil
title: "Une première «communauté logicielle libre» au gouvernement"
author: Pierre Asselin
date: 2011-11-07
href: http://www.cyberpresse.ca/le-soleil/affaires/techno/201111/06/01-4465176-une-premiere-communaute-logicielle-libre-au-gouvernement.php
tags:
- Logiciels privateurs
- Administration
- Institutions
- Associations
- Licenses
---

> Sept ministères et organismes du gouvernement québécois viennent de former une première «communauté logicielle libre», autour d'un produit québécois, IntelliGID, qui permet de gérer tout le cycle de vie des documents électroniques, de leur création jusqu'à l'archivage.
