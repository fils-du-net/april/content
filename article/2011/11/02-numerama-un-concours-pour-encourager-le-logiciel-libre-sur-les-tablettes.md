---
site: Numerama
title: "Un concours pour encourager le logiciel libre sur les tablettes"
author: Simon Robic
date: 2011-11-02
href: http://www.numerama.com/magazine/20406-un-concours-pour-encourager-le-logiciel-libre-sur-les-tablettes.html
tags:
- Entreprise
- Innovation
---

> Les tablettes tactiles devenant de plus en plus populaires, les inquiétudes face à la fermeture de leurs systèmes d'exploitation et de leurs applications sont de plus en plus grandes. L'Association Francophone des Utilisateurs de Logiciels Libres lance donc un concours visant à promouvoir le libre sur les tablettes.
