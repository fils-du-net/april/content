---
site: nouvelObs.com
title: "Adobe renonce à Flash pour les appareils mobiles"
author: Jim Finkle
date: 2011-11-09
href: http://tempsreel.nouvelobs.com/high-tech/20111109.REU2422/adobe-renonce-a-flash-pour-les-appareils-mobiles.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Standards
---

> Adobe Systems Inc a annoncé mercredi l'abandon de son logiciel Flash Player pour les plateformes mobiles, mettant ainsi un terme à un long bras de fer avec Apple, qui avait écarté ce lecteur vidéo de ses iPhone et iPad depuis 2007, au profit des applications ouvertes en HTML5.
