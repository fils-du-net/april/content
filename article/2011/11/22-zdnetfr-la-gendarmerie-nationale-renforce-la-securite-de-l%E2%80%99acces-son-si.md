---
site: ZDNet.fr
title: "La gendarmerie nationale renforce la sécurité de l’accès à son SI"
author: Christophe Auffray
date: 2011-11-22
href: http://www.zdnet.fr/actualites/la-gendarmerie-nationale-renforce-la-securite-de-l-acces-a-son-si-39765875.htm
tags:
- Administration
---

> Pour accroître la sécurité de l’accès à ses applications sensibles et permettre la traçabilité des accès aux fichiers judiciaires et administratifs, la gendarmerie nationale s’est dotée d’une technologie d’authentification forte. Une solution amenée à favoriser d’autres usages, dont la dématérialisation et la signature électronique.
