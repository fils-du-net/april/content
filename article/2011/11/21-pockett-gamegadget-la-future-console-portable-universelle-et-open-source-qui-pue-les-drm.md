---
site: Pockett
title: "GameGadget, la future console portable universelle et open source qui pue les DRM"
author: P.
date: 2011-11-21
href: http://www.pockett.net/n11753_Divers_GameGadget_la_future_console_portable_universelle_et_open_source_qui_pue_les_DRM
tags:
- Entreprise
- Matériel libre
- DRM
---

> Blaze, la société d'accessoires pour consoles, vient d'annoncer la console portable GameGadget 1.0. Sortie prévue début 2012 au prix de 100 livres (116 euros). Une console qu'on nous promet open source et capable de faire tourner tous les vieux jeux.
