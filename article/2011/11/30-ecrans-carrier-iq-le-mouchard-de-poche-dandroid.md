---
site: écrans
title: "Carrier IQ, le mouchard de poche d'Android"
author: Camille Gévaudan
date: 2011-11-30
href: http://www.ecrans.fr/Carrier-IQ-le-mouchard-de-poche-d,13654.html
tags:
- Entreprise
- Internet
---

> « Voyez précisément comment vos clients interagissent avec leur téléphone ; identifiez les services qu’ils utilisent et les contenus qu’ils consultent, même sur Internet ! » Contrairement aux apparences, cette accroche publicitaire n’est pas fictive, ni humoristique. Publiée sur le site de Carrier IQ, éditeur de logiciels pour smartphones, elle s’adresse aux opérateurs mobiles et tente de les inciter à installer un petit programme nommé « Insight Experience Manager » sur les appareils qu’ils vendent à leurs clients.
