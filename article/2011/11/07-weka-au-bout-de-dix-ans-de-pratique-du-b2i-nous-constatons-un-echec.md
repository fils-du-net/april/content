---
site: WEKA
title: "\"Au bout de dix ans de pratique du B2i, nous constatons un échec\""
date: 2011-11-07
href: http://www.weka.fr/actualite/education-thematique_7847/au-bout-de-dix-ans-de-pratique-du-b2i-nous-constatons-un-echec-article_66593/
tags:
- Internet
- Logiciels privateurs
- Administration
- April
- HADOPI
- Institutions
- Associations
- Éducation
- Licenses
---

> Il mène depuis de nombreuses années des actions en faveur d'une discipline informatique au lycée. Explications à l'occasion d'une table ronde et d'un stand dédié à cette nouvelle option lors du prochain salon Educatec-Educatice.
