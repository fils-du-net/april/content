---
site: Cosmopolitan.fr
title: "Facebook tué par le nouveau réseau social, Diaspora ?"
author: Delphine Desneiges
date: 2011-11-29
href: http://www.cosmopolitan.fr/,facebook-tue-par-le-nouveau-reseau-social-diaspora,2159,1533879.asp
tags:
- Entreprise
- Internet
---

> Un réseau social sur lequel vous contrôlez toutes les informations que vous diffusez ? Non, ça n'est certainement pas Facebook, mais Diaspora qui vous le permet. Un réseau social d'un nouveau genre qui a vu le jour il y a quelques semaines. Diaspora représente-t-il vraiment une menace pour Facebook ?
