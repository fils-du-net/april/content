---
site: PC INpact
title: "C'est l'histoire d'un garage bien rempli"
author: Nil Sanyas
date: 2011-12-12
href: http://www.pcinpact.com/news/67597-insolite-free-christine-albanel-gus-garage.htm
tags:
- Entreprise
- HADOPI
- Institutions
---

> L'ARCEP vient de publier une synthèse des réponses de plusieurs opérateurs français à sa consultation publique « sur les méthodes de comptabilisation, de recouvrement et de tarification des coûts liés aux demandes de conservation des numéros fixes », en somme, sur la portabilité des numéros fixes. Dans ce fichier, vous pouvez retrouver les réponses d'Orange, SFR, Bouygues Télécom, Verizon, Paritel, Prosodie et donc Free. Ce dernier a cependant envoyé quelques dédicaces à Christine Albanel si l'on regarde les propriétés de son document.
