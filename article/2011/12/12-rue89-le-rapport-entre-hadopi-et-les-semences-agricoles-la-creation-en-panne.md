---
site: Rue89
title: "Le rapport entre Hadopi et les semences agricoles? La création en panne"
author: fguyot
date: 2011-12-12
href: http://www.rue89.com/rue89-eco/2011/12/12/le-rapport-entre-hadopi-et-les-semences-agricoles-la-creation-en-panne-227445
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Brevets logiciels
- Droit d'auteur
- Innovation
---

> Quel lien peut-on établir entre le discours d'Avignon du chef de l'Etat, dans lequel il a annoncé une loi Hadopi 3 pour lutter contre le «streaming», et la loi votée le 28 novembre par l'Assemblée nationale, qui interdit aux agriculteurs d'utiliser les semences issues de leurs récoltes?
