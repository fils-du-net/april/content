---
site: Numerama
title: "L'Europe engage la ratification de l'ACTA"
author: Guillaume Champeau
date: 2011-12-14
href: http://www.numerama.com/magazine/20934-l-europe-engage-la-ratification-de-l-acta.html
tags:
- Internet
- Institutions
- Associations
- Droit d'auteur
- Europe
- International
- ACTA
---

> Après un contre-temps, l'Europe se met sur le chemin de la ratification très controversée de l'Accord Commercial Anti-Contrefaçon (ACTA). Alors que la cérémonie officielle des signatures s'était faite à Tokyo le 1er octobre sans l'Union Européenne, qui a pourtant participé activement à sa négociation, La Quadrature du Net nous apprend que le processus est enclenché.
