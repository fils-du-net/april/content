---
site: ICTjournal
title: "Open government data: Qui veut les données des administrations publiques?"
author: Rodolphe Koller
date: 2011-12-19
href: http://www.ictjournal.ch/fr-CH/News/2011/12/19/Open-government-data-Qui-veut-les-donnees-des-administrations-publiques.aspx
tags:
- Entreprise
- Internet
- Administration
- Économie
- Institutions
- Associations
- Licenses
- Standards
- International
- Open Data
---

> Journalistes, chercheurs, entrepreneurs, nombreux sont ceux qui souhaitent que les administrations fédérales mettent toutes leurs données numériques brutes à la libre disposition du public et des entreprises. Si une telle diffusion est un atout pour la transparence étatique, son apport économique reste à prouver.
