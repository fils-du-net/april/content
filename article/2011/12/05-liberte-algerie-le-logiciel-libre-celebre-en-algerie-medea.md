---
site: Liberté Algérie
title: "Le logiciel libre célébré en Algérie Médéa"
author: M. EL BEY
date: 2011-12-05
href: http://www.liberte-algerie.com/algerie-profonde/le-logiciel-libre-celebre-en-algerie-medea-167395
tags:
- Logiciels privateurs
- Économie
- Sensibilisation
- Éducation
- International
---

> Survenant avec quelque retard par rapport aux autres pays, la Journée mondiale du logiciel libre a été célébrée pour la première fois en Algérie, à l’occasion d’une journée de sensibilisation organisée à l’université Yahia-Farès.
