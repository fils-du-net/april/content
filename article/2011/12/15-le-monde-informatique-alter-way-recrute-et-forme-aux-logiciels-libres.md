---
site: Le Monde Informatique
title: "Alter Way recrute et forme aux logiciels libres"
author: Véronique Arène
date: 2011-12-15
href: http://www.lemondeinformatique.fr/actualites/lire-alter-way-recrute-et-forme-aux-logiciels-libres-47057.html
tags:
- Entreprise
- Internet
- Partage du savoir
---

> En janvier 2012, Alter Way ouvrira la Libre Académie , un  programme de formation dédié aux métiers du logiciel libre qui s'inscrit dans le projet de recrutement engagé par la société de services Open Source.
