---
site: Le point
title: "La biologie de synthèse: un domaine en effervescence"
date: 2011-12-28
href: http://www.lepoint.fr/futurapolis/sante/la-biologie-de-synthese-un-domaine-en-effervescence-28-12-2011-1413122_431.php
tags:
- Internet
- Institutions
- Éducation
- Innovation
- Sciences
- International
---

> Le biologiste Joël de Rosnay, qui avait décrit "la folle aventure des architectes et bricoleurs du vivant" dans un livre publié l'an dernier pointant ses risques potentiels, estime que l'État doit favoriser l'innovation et ensuite aider à coréguler ce nouveau secteur de la biologie.
