---
site: LES NUMERIQUES
title: "Le SELL riposte suite aux attaques de Que Choisir"
author: Fabien Pionneau
date: 2011-12-02
href: http://www.lesnumeriques.com/jeux-video/sell-riposte-suite-attaques-choisir-n22278.html
tags:
- Entreprise
- Logiciels privateurs
- DRM
---

> En début de semaine, nous vous faisions part de la nouvelle lutte de l'UFC-Que Choisir contre des pratiques adoptées par certains éditeurs de jeux vidéo. Aujourd'hui, le Syndicat des Editeurs de Logiciels de Loisirs (SELL) défend son point de vue et réfute les accusations de l'association.
