---
site: Bulletins Electroniques
title: "L'outil qui propulse data.gov devient Open Source"
date: 2011-12-16
href: http://www.bulletins-electroniques.com/actualites/68571.htm
tags:
- Internet
- Institutions
- International
- Open Data
---

> Lancé en mai 2009 par Vivek Kundra, alors "Chief Information Officer" du gouvernement fédéral, le site "data.gov", précurseur de notre data.gouv.fr lancé il y a quelques jours [1], est le fer de lance de l'initiative "Open Government" [2] d'Obama. Le but de cette plateforme est de devenir un entrepôt de toute l'information que le gouvernement collecte. Les données qui y sont entreposées doivent être décrites (métadonnées#) et être disponibles dans un format lisible par les machines (par exemple, au format XML).
