---
site: El Watan
title: "Université de Médéa : Le logiciel libre, une notion à développer"
author: Mohamed Abdelli
date: 2011-12-01
href: http://www.elwatan.com/regions/centre/actu-centre/universite-de-medea-le-logiciel-libre-une-notion-a-developper-01-12-2011-149317_220.php
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- International
---

> Si le logiciel libre est considéré comme l’ennemi juré du logiciel propriétaire, la célébration, samedi, de la Journée mondiale de l’Open source (source libre), pour la première fois en Algérie, à l’université de Médéa, était une occasion pour les accros du logiciel libre de développer une toute autre vision de la notion de concurrence sur les cyberespaces.
