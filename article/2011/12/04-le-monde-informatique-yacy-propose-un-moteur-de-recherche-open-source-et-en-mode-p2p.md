---
site: Le Monde Informatique
title: "YaCy propose un moteur de recherche Open Source et en mode P2P"
author: Jacques Cheminat
date: 2011-12-04
href: http://micro.lemondeinformatique.fr/actualites/lire-yacy-propose-un-moteur-de-recherche-open-source-et-en-mode-p2p-4170.html
tags:
- Entreprise
- Internet
- Associations
---

> Des partisans du logiciel libre viennent de lancer un moteur de recherche Peer-to-Peer pour concurrencer Google, Bing, Yahoo... Un projet baptisé YaCy.
