---
site: La Voix du Nord
title: "Aurore Rousseaux et Philippe Pary, de l'entreprise SCIL: libres et solidaires"
author: KÉVIN MEIGNEUX
date: 2011-12-18
href: http://www.lavoixdunord.fr/Locales/Lille/actualite/Secteur_Lille/2011/12/18/article_aurore-rousseaux-et-philippe-pary-de-l-e.shtml
tags:
- Entreprise
- Administration
- Économie
- Partage du savoir
- Associations
---

> Depuis 2002, la ville accompagne et soutient des créations d'entreprises sociales et solidaires. Parmi elles, la société SCIL, coopérative fondée en avril dernier, qui aide associations, entreprises et collectivités dans l'usage de logiciels libres.
