---
site: Datanews.be
title: "L'open source a fière allure"
author: Guy Kindermans
date: 2011-12-12
href: http://datanews.levif.be/ict/actualite/apercu/2011/12/12/l-open-source-a-fiere-allure/article-4000017559508.htm
tags:
- Entreprise
- Sensibilisation
- Associations
- Standards
---

> L’Open Source Conference d’Amsterdam témoigne du succès suscité par l’open source dans les entreprises aujourd’hui.
