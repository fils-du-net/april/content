---
site: L'Expansion.com
title: "L'open source peut-il sauver WebOS?"
date: 2011-12-12
href: http://lexpansion.lexpress.fr/high-tech/l-open-source-peut-il-sauver-webos_274460.html#xtor=RSS-115
tags:
- Entreprise
- Brevets logiciels
- Licenses
---

> Meg Whitman, la nouvelle patronne de HP, a tranché. Web OS le système d'exploitation pour tablettes et smartphones sera confié à la communauté du logiciel libre. Une décision qui est loin de convaincre tout le monde...
