---
site: PC INpact
title: "Le consommateur mieux informé sur les DRM et l’interopérabilité ?"
author: Marc Rees
date: 2011-12-07
href: http://www.pcinpact.com/news/67491-consommateur-drm-information-interoperabilite-libre.htm?vc=1
tags:
- HADOPI
- Institutions
- DRM
- Europe
---

> Après son examen à l’Assemblée nationale, le projet de loi Consommation arrive au Sénat. Le texte sera examiné les 20 et 21 décembre prochain En commission de l’économie, un amendement du sénateur et rapporteur PS Alain Fauconnier, va intéresser de près les consommateurs gênés par les verrous numériques et autres solutions équivalentes (DRM). Cet amendement vise en effet à enrichir leur information précontractuelle dans le cadre d’une vente à distance.
