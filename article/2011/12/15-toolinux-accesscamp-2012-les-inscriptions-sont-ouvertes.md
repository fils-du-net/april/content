---
site: toolinux
title: "AccessCamp 2012 : les inscriptions sont ouvertes "
date: 2011-12-15
href: http://www.toolinux.com/AccessCamp-2012-les-inscriptions
tags:
- April
- Accessibilité
---

> Suite au succès du BarCamp accessibilité organisé à la Cité des Sciences en janvier 2011, le groupe de travail accessibilité et logiciels libres de l’APRIL réitère l’opération.
