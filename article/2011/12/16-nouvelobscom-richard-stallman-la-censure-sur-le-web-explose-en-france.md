---
site: nouvelObs.com
title: "Richard Stallman: \"La censure sur le web a explosé en France\""
author: Boris Manenti
date: 2011-12-16
href: http://tempsreel.nouvelobs.com/high-tech/20111216.OBS6929/richard-stallman-la-censure-sur-le-web-a-explose-en-france.html
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Institutions
- Droit d'auteur
---

> "J'observe une croissance de sites addictifs qui mettent en place une large surveillance, comme par exemple Facebook. Facebook n'est pas ton ami", milite le père du logiciel libre. Interview.
