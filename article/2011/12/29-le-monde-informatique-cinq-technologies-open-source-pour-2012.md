---
site: Le Monde Informatique
title: "Cinq technologies Open Source pour 2012"
author: M.G.
date: 2011-12-29
href: http://www.lemondeinformatique.fr/actualites/lire-cinq-technologies-open-source-pour-2012-1ere-partie-47204.html
tags:
- Entreprise
- Internet
- Innovation
- Informatique en nuage
---

> Tout le monde connaît le succès de Linux et d'Apache. A leur suite, d'autres technologies Open Source montent en puissance ou se profilent, en annonçant des fonctionnalités prometteuses. C'est le cas du serveur web Nginx (prononcez « engine-x »), du projet de cloud OpenStack et de Stig, une base de données orientée graphe.
