---
site: Le nouvel Economiste
title: "Propriété intellectuelle – L’open source"
author: Lilia Tlemçani
date: 2011-12-08
href: http://www.lenouveleconomiste.fr/lesdossiers/propriete-intellectuelle-lopen-source-13091/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Économie
- Institutions
- Associations
- Brevets logiciels
- Droit d'auteur
- Innovation
- Licenses
---

> Licence libre ne veut pas dire automatiquement gratuit ou sans obligations
