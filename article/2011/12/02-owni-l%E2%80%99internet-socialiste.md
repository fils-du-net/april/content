---
site: OWNI
title: "L’Internet socialiste"
author: Andréa Fradin et Guillaume Ledit
date: 2011-12-02
href: http://owni.fr/2011/12/02/fleur-pellerin-internet-hollande-presidentielle-2012-hadopi/
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Institutions
- Neutralité du Net
---

> Nommée récemment responsable du pôle numérique de François Hollande, Fleur Pellerin connaît une arrivée agitée dans le secteur. Rencontre et entretien autour de son futur programme, Hadopi exclue, parce que le sujet est encore tabou.
