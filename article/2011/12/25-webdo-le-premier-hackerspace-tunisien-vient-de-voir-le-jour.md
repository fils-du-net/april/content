---
site: webdo
title: "Le premier Hackerspace tunisien vient de voir le jour"
author: Melek Jebnoun
date: 2011-12-25
href: http://www.webdo.tn/2011/12/25/le-premier-hackerspace-tunisien-vient-de-voir-le-jour/
tags:
- Internet
- Partage du savoir
- Institutions
- Associations
- Innovation
- International
---

> Bien avant la révolution, certains Tunisiens en parlaient, mais c’est finalement après presque un an jour pour jour que le premier Hackerspace tunisien a vu le jour. L’idée, qui est une sorte de « La cantine française» 100 % tunisienne, a été mise sur le tapis, mais faute de local et de fonds elle n'a pas pu être concrétisée.
