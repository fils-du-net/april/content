---
site: Numerama
title: "L'April demande au Parlement européen de rejeter l'ACTA"
author: Julien L.
date: 2011-12-08
href: http://www.numerama.com/magazine/20851-l-april-demande-au-parlement-europeen-de-rejeter-l-acta.html
tags:
- Entreprise
- Internet
- Interopérabilité
- April
- Institutions
- Associations
- DRM
- Innovation
- Europe
- International
---

> L'April a envoyé deux courriers aux commissions du Parlement européen ITRE (Industrie, recherche et énergie) et LIBE (libertés civiles, de la justice et des affaires intérieures). L'association leur demande de rejeter au plus vite l'ACTA, au regard des risques que l'accord fait peser sur le logiciel libre, la capacité d'innovation et la croissance en Europe.
