---
site: LeMonde.fr
title: "Le sarkozysme est atteint d'une crise aiguë de \"googlisme\""
author: Thomas Clay
date: 2011-12-14
href: http://www.lemonde.fr/idees/article/2011/12/14/le-sarkozysme-est-atteint-d-une-crise-aigue-de-googlisme_1618423_3232.html
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Open Data
---

> Nicolas Sarkozy, tout à son travail méthodique de séduction de chaque catégorie d'électeurs, tente à présent de séduire les jeunes et les acteurs du numérique échaudés par l'épisode désastreux d'Hadopi. Il a ainsi inauguré, le 6 décembre, le nouveau siège de Google à Paris. C'est un faux pas de plus.
