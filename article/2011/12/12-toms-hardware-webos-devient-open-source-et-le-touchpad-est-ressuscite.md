---
site: tom's hardware
title: "WebOS devient open source et le TouchPad est réssuscité"
author: David Civera
date: 2011-12-12
href: http://www.presence-pc.com/actualite/TouchPad-webOS-45968/
tags:
- Entreprise
- Logiciels privateurs
- Licenses
---

> HP a annoncé vendredi dernier qu’il allait proposer webOS à la communauté open source. Après des mois d’incertitude sur le sort du système d’exploitation, le communiqué de presse affirme qu’il s’agit d’une occasion pour améliorer les applications et les services web alors qu’il donne une nouvelle vie au TouchPad. De nombreuses voix affirment néanmoins que cette décision signifie la mort du système d’exploitation.
