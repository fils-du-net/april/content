---
site: LeMonde.fr
title: "Apple et la presse, pas si compatibles"
author: Yves Eudes
date: 2011-12-14
href: http://www.lemonde.fr/technologies/article/2011/12/13/apple-et-la-presse-pas-si-compatibles_1617965_651865.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- International
---

> Un an et demi après le lancement de l'iPad, au printemps 2010, l'intuition initiale des éditeurs de presse se confirme : pour la lecture des journaux et magazines en ligne, la tablette numérique d'Apple s'impose comme un "game changer" - l'objet magique qui vient bouleverser les règles du jeu, comme l'iPod l'avait fait pour la musique il y a dix ans. Leurs craintes se vérifient également : Apple s'installe entre les journaux et leurs lecteurs.
