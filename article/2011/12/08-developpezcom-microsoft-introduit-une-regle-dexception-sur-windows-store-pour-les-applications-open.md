---
site: Developpez.com
title: "Microsoft introduit une règle d'exception sur Windows Store pour les applications open source"
author: hinault romaric
date: 2011-12-08
href: http://www.developpez.com/actu/39824/Microsoft-introduit-une-regle-d-exception-sur-Windows-Store-pour-les-applications-open-source/
tags:
- Entreprise
- Logiciels privateurs
- Licenses
---

> Microsoft a présenté récemment Windows Store (lire ci-avant), la galerie d’applications que l’éditeur souhaite utiliser comme canal de distribution exclusif des applications Windows 8.
