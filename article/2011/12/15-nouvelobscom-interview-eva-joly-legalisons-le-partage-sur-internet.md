---
site: nouvelObs.com
title: "INTERVIEW. Eva Joly: \"Légalisons le partage sur internet!\""
author: Boris Manenti
date: 2011-12-15
href: http://tempsreel.nouvelobs.com/election-presidentielle-2012/20111215.OBS6873/interview-eva-joly-hadopi-est-une-absurdite.html
tags:
- Internet
- Partage du savoir
- HADOPI
- Institutions
- Droit d'auteur
- Europe
- ACTA
---

> La candidate écolo aux lunettes rouges, Eva Joly, affiche son engagement pour les logiciels libres et contre les lois répressives telles qu'Hadopi ou Loppsi.
