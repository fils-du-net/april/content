---
site: LeMonde.fr
title: "Navigateurs Web: Mozilla et Google renouvellent leur partenariat"
date: 2011-12-22
href: http://www.lemonde.fr/technologies/article/2011/12/21/navigateurs-web-mozilla-et-google-renouvellent-leur-partenariat_1620996_651865.html
tags:
- Entreprise
- Internet
---

> Mozilla, éditeur du navigateur Firefox et Google ont renouvelé, mardi 20 décembre, leur partenariat commercial. Selon les termes de cet accord, valable pour les trois prochaines années, Google demeure le moteur de recherche par défaut du navigateur Open source. En échange, Mozilla perçoit une rémumération. Les termes économiques de l'accord n'ont toutefois pas été divulgés.
