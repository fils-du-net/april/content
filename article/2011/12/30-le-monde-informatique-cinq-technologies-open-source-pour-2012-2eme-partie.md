---
site: Le Monde Informatique
title: "Cinq technologies Open Source pour 2012 (2ème partie)"
author: M.G.
date: 2011-12-30
href: http://www.lemondeinformatique.fr/actualites/lire-cinq-technologies-open-source-pour-2012-2eme-partie-47207.html
tags:
- Entreprise
- Innovation
- Informatique en nuage
---

> Tout le monde connaît le succès de Linux et d'Apache. A leur suite, d'autres technologies Open Source montent en puissance ou annoncent des fonctionnalités prometteuses. Ainsi Nginx, OpenStack et Stig (1ère partie), ou encore Linux Mint, distribution Linux pour poste de travail, et GlusterFS, un système de gestion de fichiers pour créer des pools de stockage.
