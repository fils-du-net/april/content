---
site: leSoleil
title: "Des migrations profitables avec des logiciels libres"
author: Pierre Asselin
date: 2011-12-06
href: http://www.cyberpresse.ca/le-soleil/affaires/techno/201112/05/01-4474895-des-migrations-profitables-avec-des-logiciels-libres.php
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Éducation
- International
---

> Si le gouvernement québécois cherche à faire des économies avec ses suites bureautiques, il peut toujours consulter les nombreuses expériences menées dans plusieurs pays avec OpenOffice. Un projet réalisé par le ministère de la Justice finlandais, sur plus de 10 000 postes de travail, a permis d'économiser 4,5 millions $, soit plus de la moitié de la facture.
