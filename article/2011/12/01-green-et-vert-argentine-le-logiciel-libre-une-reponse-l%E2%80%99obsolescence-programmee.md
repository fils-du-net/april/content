---
site: Green et Vert
title: "Argentine : Le logiciel libre, une réponse à l’obsolescence programmée?"
author: Carlos A. Maslaton
date: 2011-12-01
href: http://www.greenetvert.fr/2011/12/01/le-logiciel-libre-une-reponse-a-l%E2%80%99obsolescence-programmee/39928
tags:
- Entreprise
- International
---

> Face à l’accumulation de déchets hautement toxiques, les spécialistes des nouvelles technologies dénoncent les pratiques des géants de l’informatique et des fabricants d’appareils électroniques, obligeant à un renouvellement toujours plus rapide de leurs produits.
