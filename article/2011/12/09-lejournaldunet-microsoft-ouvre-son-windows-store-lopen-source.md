---
site: LeJournalduNet
title: "Microsoft ouvre son Windows Store à l'Open Source"
date: 2011-12-09
href: http://www.journaldunet.com/solutions/systemes-reseaux/securite-du-windows-store-et-open-source-1211.shtml
tags:
- Entreprise
- Logiciels privateurs
- Licenses
---

> Microsoft compte bien séduire les entreprises avec une boutique en ligne qui leur propose des applications sécurisées et même des solutions Open Source.
