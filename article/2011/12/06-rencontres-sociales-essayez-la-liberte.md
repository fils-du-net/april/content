---
site: Rencontres Sociales
title: "Essayez la liberté !"
author: Jean-Philippe Milesy
date: 2011-12-06
href: http://rencontres-sociales.org/spip/spip.php?article3274
tags:
- Économie
- April
- Sensibilisation
---

> Il est des semaines, elles sont rares, où rien dans l’actualité de l’ESS ne semble s’imposer vraiment à l’éditorialiste, nul évènement, nulle annonce, à moins que quelque chose n’ait échappé à l’éditorialiste auquel cas, la tête couverte de cendres, il y reviendra à coup sûr la semaine prochaine.
