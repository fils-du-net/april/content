---
site: Finyear
title: "Gilles André, directeur général de PolySpot"
date: 2011-12-05
href: http://www.cfo-news.com/Gilles-Andre-directeur-general-de-PolySpot_a20536.html
tags:
- Entreprise
- Économie
---

> Vous avez récemment annoncé une levée de fonds 2,5 millions d’euros pour accélérer le développement de PolySpot. Dans ce contexte économique, comment expliquez-vous un tel intérêt des investisseurs ?
