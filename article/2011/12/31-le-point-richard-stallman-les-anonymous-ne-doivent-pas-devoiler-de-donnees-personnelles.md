---
site: Le point
title: "Richard Stallman: \"Les Anonymous ne doivent pas dévoiler de données personnelles\""
author: Guillaume Grallet
date: 2011-12-31
href: http://www.lepoint.fr/technologie/richard-stallman-les-anonymous-ne-doivent-pas-devoiler-de-donnees-personnelles-31-12-2011-1414159_58.php
tags:
- Entreprise
- Internet
- HADOPI
- Associations
- DADVSI
---

> Le créateur de la Free Software Foundation s'exprime sur Steve Jobs, Facebook, Android et les Anonymous. Décapant.
