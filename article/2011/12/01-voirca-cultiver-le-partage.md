---
site: Voir.ca
title: "Cultiver le partage"
author: Andrée Harvey
date: 2011-12-01
href: http://voir.ca/voir-la-vie/2011/12/01/communion-cultiver-le-partage/
tags:
- Entreprise
---

> Avec le Web 2.0, les notions d'open source et d'open data,  basées sur le partage du code et des données, sont apparues.  Dans le même esprit, le Communion offre... l'open resto.
