---
site: LeMagIT
title: "Cloud : Microsoft greffe Hadoop et Node.JS à Azure"
author: Cyrille Chausson
date: 2011-12-13
href: http://www.lemagit.fr/article/microsoft-developpement-cloud-computing-azure-hadoop/10060/1/cloud-microsoft-greffe-hadoop-node-azure/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Interopérabilité
- Associations
---

> Dans une mise à jour de sa plate-forme Azure, Microsoft a rendu disponible une première version de test de Hadoop ainsi que le support de Node.JS. Le groupe de Redmond donne également la possibilité aux développeurs d’exploiter un plus grand pan d’outils Open Source ainsi que la fonction SQL Azure Federation
