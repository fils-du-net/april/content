---
site: PCWorld.fr
title: "CuBox : le minuscule boîtier Open Source pas cher"
author: Denis Leclercq
date: 2011-12-16
href: http://www.pcworld.fr/2011/12/16/cubox-boitier-open-source-pas-cher/522797/
tags:
- Entreprise
- Internet
- Matériel libre
- Innovation
---

> Un média center pas cher, Open Source, tout petit et complet coté connectiques, c'est le CuBox de SolidRun à seulement 99€...
