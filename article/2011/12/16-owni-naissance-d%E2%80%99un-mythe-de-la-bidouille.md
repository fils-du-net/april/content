---
site: OWNI
title: "Naissance d’un mythe de la bidouille"
author: aKa (Framasoft)
date: 2011-12-16
href: http://owni.fr/2011/12/16/arduino-naissance-mythe-bidouille/
tags:
- Entreprise
- Internet
- Matériel libre
- Éducation
- Innovation
- Licenses
- RGI
- Sciences
- International
- English
---

> Comme l'imprimante 3D, la carte électronique Arduino est une petite révolution dans le monde des adeptes du "do it yourself" ("faites-le vous-même"). Avec des choix philosophiques bien marqués : open source, économe, tournée vers les amateurs.
