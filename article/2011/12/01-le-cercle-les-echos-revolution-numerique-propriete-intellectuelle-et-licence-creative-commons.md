---
site: LE CERCLE Les Echos
title: "Révolution numérique, propriété intellectuelle et licence Creative Commons"
author: Samih ABID
date: 2011-12-01
href: http://lecercle.lesechos.fr/economie-societe/societe/droit/221140655/revolution-numerique-propriete-intellectuelle-et-licence-cr
tags:
- Internet
- Économie
- Institutions
- Droit d'auteur
- Licenses
- Contenus libres
---

> L’assouplissement des droits de propriété intellectuelle doit accompagner la révolution numérique et la démocratisation des outils de production culturelle et industrielle. Pour comprendre les défis des créateurs, il faut prendre le pouls du secteur culturel et décrypter les nouveaux mécanismes de création et de diffusion de la culture et du savoir, inspirés de la culture libre.
