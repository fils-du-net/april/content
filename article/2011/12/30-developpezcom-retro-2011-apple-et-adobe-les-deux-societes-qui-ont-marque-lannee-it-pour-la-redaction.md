---
site: Developpez.com
title: "Rétro 2011: Apple et Adobe, les deux sociétés qui ont marqué l'année IT pour la rédaction de Développez.com, et pour vous?"
author: gordon fowler
date: 2011-12-30
href: http://java.developpez.com/actu/40253/Retro-2011-Apple-et-Adobe-les-deux-societes-qui-ont-marque-l-annee-IT-pour-la-redaction-de-Developpez-com-et-pour-vous/
tags:
- Entreprise
- Internet
- Économie
- Informatique en nuage
---

> Que restera-t-il de l’année 2011 dans le monde de l’IT?
