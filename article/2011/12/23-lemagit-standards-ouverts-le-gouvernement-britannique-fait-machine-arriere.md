---
site: LeMagIT
title: "Standards ouverts : le gouvernement britannique fait machine arrière"
author: Cyrille Chausson
date: 2011-12-23
href: http://www.lemagit.fr/article/microsoft-standards-achat-grande-bretagne/10128/1/standards-ouverts-gouvernement-britannique-fait-machine-arriere/
tags:
- Entreprise
- Internet
- Administration
- Économie
- Institutions
- Standards
- Informatique en nuage
- International
---

> Une forte campagne de lobbying de Microsoft et de la BSA ont poussé le gouvernement à revoir sa politique en matière de standards ouverts. Une consultation publique est lancée par le Cabinet Office afin de «clarifier» certains points. Une note annule clairement la précédente circulaire qui prônait l’ouverture.
