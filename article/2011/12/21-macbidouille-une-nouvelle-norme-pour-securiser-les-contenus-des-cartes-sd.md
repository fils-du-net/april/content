---
site: MacBidouille
title: "Une nouvelle norme pour sécuriser les contenus des cartes SD"
author: Lionel
date: 2011-12-21
href: http://www.macbidouille.com/news/2011/12/21/une-nouvelle-norme-pour-securiser-les-contenus-des-cartes-sd
tags:
- Entreprise
- DRM
- Video
---

> Panasonic, Samsung Electronics, SanDisk, Sony et Toshiba ont annoncé leur intention de s'associer pour créer de nouvelles technologies de protections de contenus (DRM), spécifiquement développés pour les cartes mémoire. Ainsi, elles espèrent pouvoir proposer à terme des contenus HD qui seront protégés, comme le sont ceux des Blu-ray, sur des cartes mémoire plutôt que sur des disques.
