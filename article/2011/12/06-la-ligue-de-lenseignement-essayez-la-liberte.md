---
site: la ligue de l'enseignement
title: "Essayez la liberté !"
author: Denis Lebioda
date: 2011-12-06
href: http://www.laligue-alpesdusud.org/associatifs_leblog/?2011/12/06/3080-essayez-la-liberte
tags:
- April
- Sensibilisation
- Éducation
- Licenses
---

> Le groupe sensibilisation de l'April publie une affiche, incitant les utilisateurs de programmes informatiques à essayer les logiciels libres :
> Logiciels libres, essayez la liberté !
