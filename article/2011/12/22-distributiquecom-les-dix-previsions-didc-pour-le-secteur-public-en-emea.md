---
site: Distributique.com
title: "Les dix prévisions d'IDC pour le secteur public en EMEA"
author: Didier Barathon
date: 2011-12-22
href: http://www.distributique.com/actualites/lire-les-dix-previsions-d-idc-pour-le-secteur-public-en-emea-17499.html
tags:
- Administration
- Économie
- Institutions
- Informatique en nuage
- Open Data
---

> Dans cette période de crise et de réduction drastique des budgets publics, chacun se demande si les budgets IT du secteur public survivront. Tous les gouvernements avaient engagé des programmes (santé, éducation, administration, haut débit) en vue de rendre leurs services plus performants et moins couteux. Qu'en restera-t-il en 2012? IDC a tenté sur le sujet, dix prédictions:
