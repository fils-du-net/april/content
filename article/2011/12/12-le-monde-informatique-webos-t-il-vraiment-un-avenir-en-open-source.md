---
site: Le Monde Informatique
title: "WebOS a-t-il vraiment un avenir en  Open Source?"
author: Jean Elyan
date: 2011-12-12
href: http://www.lemondeinformatique.fr/actualites/lire-l-avenir-de-webos-de-hp-en-open-source-souleve-des-questions-47004.html
tags:
- Entreprise
- Licenses
---

> Plusieurs analystes se penchent sur la décision de HP de rendre Open Source WebOS. Ils recensent les faiblesses de cette solution, mais aussi les différents acteurs qui pourraient s'intéresser à cet OS mobile.
