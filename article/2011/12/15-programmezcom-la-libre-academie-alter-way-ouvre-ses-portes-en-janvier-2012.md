---
site: Programmez.com
title: "La Libre Académie by Alter Way ouvre ses portes en janvier 2012"
author: Frédéric Mazué
date: 2011-12-15
href: http://www.programmez.com/actualites.php?id_actu=10746
tags:
- Entreprise
- Internet
- Économie
- Partage du savoir
- Sensibilisation
---

> En janvier prochain, Alter Way lance le premier programme de formation, de spécialisation et de qualification dédié aux métiers du logiciel libre : La Libre Académie. Ce programme innovant s’inscrit dans le projet collectif de recrutement engagé par l’operateur de services Open Source.
