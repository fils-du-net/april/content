---
site: OWNI
title: "Un égo très créatif"
author: Axel Orgeret Dechaume
date: 2011-12-09
href: http://owni.fr/2011/12/09/raphael-meltz-le-tigre/
tags:
- Entreprise
- Internet
---

> Raphaël Meltz a, entre autres, fondé Le Tigre, "curieux magazine curieux". Rencontre avec celui qui refuse d'être catalogué et juge avec sévérité le paysage médiatique contemporain.
