---
site: 20minutes.fr
title: "HP tente un coup de poker open source avec WebOS"
author: Philippe Berry
date: 2011-12-09
href: http://www.20minutes.fr/high-tech/839922-hp-tente-coup-poker-open-source-webos
tags:
- Entreprise
- Logiciels privateurs
- Licenses
---

> Petit coup de tonnerre dans le monde des smartphones. Vendredi, HP a annoncé qu'il allait partager son système WebOS avec la communauté en le rendant open source. L'initiative pourrait secouer le marché si des fabricants de téléphones se laissent séduire, sur le modèle d'Android.
