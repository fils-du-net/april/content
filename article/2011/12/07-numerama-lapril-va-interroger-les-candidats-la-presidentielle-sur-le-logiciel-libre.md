---
site: Numerama
title: "L'April va interroger les candidats à la présidentielle sur le logiciel libre"
author: Julien L.
date: 2011-12-07
href: http://www.numerama.com/magazine/20841-l-april-va-interroger-les-candidats-a-la-presidentielle-sur-le-logiciel-libre.html
tags:
- Internet
- April
- Institutions
---

> À l'occasion des élections présidentielles de 2012, l'April prépare un questionnaire à l'attention des candidats. L'association en charge de la défense et de la promotion du logiciel libre veut pousser les prétendants à se positionner sur différents sujets en lien avec les libertés numériques.
