---
site: "business-MOBILE.FR"
title: "La haine de Steve Jobs contre Android"
author: Olivier Chicheportiche
date: 2011-10-21
href: http://www.businessmobile.fr/actualites/la-haine-de-steve-jobs-contre-android-39765017.htm
tags:
- Entreprise
- Logiciels privateurs
---

> Dans la biographie officielle de l'ancien patron d'Apple, on découvre un Steve Jobs particulièrement hargneux contre le robot vert.
