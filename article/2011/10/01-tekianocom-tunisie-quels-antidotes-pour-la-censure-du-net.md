---
site: tekiano.com
title: "Tunisie : Quels antidotes pour la censure du net ?"
author: Thameur Mekki
date: 2011-10-01
href: http://www.tekiano.com/net/7-web-2-0/4336-tunisie-quels-antidotes-pour-la-censure-du-net-.html
tags:
- Internet
- Institutions
---

> Promulguer le libre accès à l'information comme droit constitutionnel, promouvoir l'autorégulation afin d'éviter l'interférence de l'Etat et encourager l'open source pour éviter les monopoles. Telles sont les grandes lignes de la réforme nécessaire pour abolir la censure du net. C'est le brouillon de l'atelier stratégique organisé par le TMG de l'IFEX.
