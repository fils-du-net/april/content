---
site: clubic.com
title: "Etalab ouvre l'Open Data à la licence libre"
author: Olivier Robillart
date: 2011-10-18
href: http://pro.clubic.com/technologie-et-politique/actualite-453434-etalab.html
tags:
- Internet
- Administration
- Licenses
- Open Data
---

> Le projet Etalab vient de dévoiler les conditions de mise en ligne des informations détenues par les administrations publiques. La mission interministérielle chargée de mettre en œuvre l'initiative annonce la création d'une licence libre destinée à faciliter la réutilisation des données.
