---
site: Localtis.info
title: "GeoBretagne, une plateforme géographique ouverte et mutualisée"
author: Philippe Parmantier
date: 2011-10-25
href: http://www.localtis.info/cs/ContentServer?pagename=Localtis/LOCActu/ArticleActualite&did=1250260646505&cid=1250262630786
tags:
- Entreprise
- Internet
- Administration
- Partage du savoir
- Open Data
---

> "Les cartes et données géographiques de référence ne sont plus stockées sur le serveur de la mairie ou du conseil général, mais sur une base mutualisée, extérieure, facilement accessible et de plus, réutilisables par des centaines d'acteurs publics et privés."
