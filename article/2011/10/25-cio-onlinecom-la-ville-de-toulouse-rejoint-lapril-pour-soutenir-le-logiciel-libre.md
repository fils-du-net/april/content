---
site: "cio-online.com"
title: "La ville de Toulouse rejoint l'APRIL pour soutenir le logiciel libre"
author: Bertrand Lemaire
date: 2011-10-25
href: http://www.cio-online.com/actualites/lire-la-ville-de-toulouse-rejoint-l-april-pour-soutenir-le-logiciel-libre-3909.html
tags:
- Internet
- Administration
- April
- Associations
- Open Data
---

> La Ville Rose est la première grande municipalité à rejoindre l'association de promotion et de recherche en informatique libre quatre ans après la première commune.
