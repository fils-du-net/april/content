---
site: LeDevoir.com
title: "Rencontres du Mont-Blanc - « L'économie sociale est un modèle de développement durable complet »"
author: Etienne Plamondon-Emond
date: 2011-10-15
href: http://www.ledevoir.com/societe/actualites-en-societe/333646/rencontres-du-mont-blanc-l-economie-sociale-est-un-modele-de-developpement-durable-complet
tags:
- Économie
- Institutions
- International
---

> Lula da Silva et Michel Rocard seront à Chamonix
