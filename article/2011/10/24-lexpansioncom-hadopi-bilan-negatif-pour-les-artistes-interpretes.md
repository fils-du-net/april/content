---
site: L'Expansion.com
title: "Hadopi: bilan négatif pour les artistes-interprètes"
author: Raphaële Karayan
date: 2011-10-24
href: http://lexpansion.lexpress.fr/high-tech/hadopi-bilan-negatif-pour-les-artistes-interpretes_267009.html
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Institutions
- Licenses
---

> La Spedidam, qui représente les artistes-interprètes, dénonce le fait qu'ils ne touchent rien sur les ventes de musique en ligne. Elle fustige pêle-mêle Hadopi, les majors et les politiques, accusés de mener la création à la catastrophe.
