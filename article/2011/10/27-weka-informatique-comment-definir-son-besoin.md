---
site: WEKA
title: "Informatique : comment définir son besoin ?"
date: 2011-10-27
href: http://www.weka.fr/actualite/marches-publics-thematique_7848/informatique-comment-definir-son-besoin-article_66410/
tags:
- Entreprise
- Administration
- April
- Marchés publics
---

> Une position récente du juge administratif sur la mention du logiciel libre vient ajouter une nouvelle pierre au débat.
