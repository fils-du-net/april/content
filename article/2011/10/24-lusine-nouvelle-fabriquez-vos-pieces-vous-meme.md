---
site: L'USINE NOUVELLE
title: "Fabriquez vos pièces vous-même"
author: Mirel Scherer 
date: 2011-10-24
href: http://www.usinenouvelle.com/article/fabriquez-vos-pieces-vous-meme.N161463
tags:
- Entreprise
- Internet
- Matériel libre
- Innovation
---

> De nouvelles solutions de fabrication rapide, plus abordables, répondent aux besoins du grand public : les systèmes d'impression 3D open-source ou en kit. Tour d'horizon des solutions proposées.
