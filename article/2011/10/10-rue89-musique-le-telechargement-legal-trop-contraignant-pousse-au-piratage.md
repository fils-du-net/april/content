---
site: Rue89
title: "Musique : le téléchargement légal, trop contraignant, pousse au piratage"
author: Emilie Brouze
date: 2011-10-10
href: http://www.rue89.com/2011/10/10/musique-moins-de-contraintes-sur-le-telechargement-legal-moins-de-pirates-225422
tags:
- Entreprise
- DRM
---

> Et si les contraintes du téléchargement légal poussaient au piratage ? C'est la conclusion d'une étude américaine selon laquelle moins il y a d'interdits, moins il y a de pirates qui téléchargent de musique illégalement.
