---
site: PC INpact
title: "PC &amp; OS : la Cour de cass' sacralise l'information du consommateur"
author: Marc Rees
date: 2011-10-06
href: http://www.pcinpact.com/actu/news/66227-cour-cassation-vente-liee-subordonnee.htm
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
---

> Exclusif : La Cour de cassation vient de rendre sa décision sur le sujet de la vente liée. L'annonce de cet arrêt dans nos colonnes, avait servi au député Lionel Tardy pour le retrait de plusieurs amendements lors du projet de loi Conso. Cependant, la décision de principe est une décision a minima. Plutôt que de purger cette problématique, elle se concentre sur l'information du consommateur. La cassation étant intégrale, la Cour d'Appel devra cependant rejuger ce dossier opposant UFC à Darty.
