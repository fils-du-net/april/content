---
site: Silicon.fr
title: "Steve Jobs : l’intransigeance de Stallman passe mal auprès de la communauté"
author: David Feugey
date: 2011-10-09
href: http://www.silicon.fr/steve-jobs-l%E2%80%99intransigeance-de-stallman-passe-mal-aupres-de-la-communaute-62572.html
tags:
- Associations
---

> Une boulette de plus pour Richard Stallman, le père et gourou des logiciels libres. “Trop c’est trop” s’indigne une partie de la communauté, qui en appelle à un changement de gouvernance au sein de la FSF.
