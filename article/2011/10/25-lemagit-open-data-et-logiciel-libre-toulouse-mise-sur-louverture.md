---
site: LeMagIT
title: "Open Data et logiciel libre : Toulouse mise sur l'ouverture"
author: Cyrille Chausson
date: 2011-10-25
href: http://www.lemagit.fr/article/france-administration-libre-migration-open-data/9754/1/open-data-logiciel-libre-toulouse-mise-sur-ouverture/
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Administration
- Économie
- April
- Licenses
- Open Data
---

> Un seul mot : ouverture. C'est ce qui vient en premier lieu à l'esprit pour qualifier la démarche volontariste de la ville de Toulouse et de son agglomération. A la clé : une adhésion à l'April (l'association de promotion et de défense du logiciel libre en France) ainsi que l'ouverture d'un portail de données publiques (Open Data).
