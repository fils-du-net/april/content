---
site: ZDNet.fr
title: "LibreOffice: du Brésil à l'Ile-de-France, beaucoup de bonnes nouvelles pour le premier anniversaire"
author: Thierry Noisette
date: 2011-10-17
href: http://www.zdnet.fr/blogs/l-esprit-libre/libreoffice-du-bresil-a-l-ile-de-france-beaucoup-de-bonnes-nouvelles-pour-le-premier-anniversaire-39764864.htm
tags:
- Administration
- Associations
- Éducation
- International
---

> Soutien du Brésil et de l'Ile-de-France, qui déploiera le logiciel sur son cloud régional, version en ligne en préparation: la conférence LibreOffice à Paris a multiplié les annonces. Un demi-million d'ordinateurs de la fonction publique française vont migrer vers LibreOffice.
