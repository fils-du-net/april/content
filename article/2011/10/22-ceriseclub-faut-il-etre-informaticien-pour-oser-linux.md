---
site: CeriseClub
title: "Faut-il être informaticien pour oser Linux?   "
date: 2011-10-22
href: http://www.ceriseclub.com/actualites/2011/10/22/10526/faut-il-etre-informaticien-pour-oser-linux.html
tags:
- Logiciels privateurs
- Sensibilisation
---

> Le système d'exploitation Linux est une alternative intéressante au leader dans le domaine, Windows. Gratuit et moins gourmand en ressources, il permet de faire durer son matériel. Grâce à son dérivé Ubuntu, son utilisation est devenue plus facile, même s'il ne séduira pas les technophobes.
