---
site: DIRECTION INFORMATIQUE
title: "NetPME 2011 : Quelques avances et retards dans l'informatisation des PME québécoises"
author: Jean-François Ferland
date: 2011-10-20
href: http://www.directioninformatique.com/DI/client/fr/DirectionInformatique/Nouvelles.asp?id=64611
tags:
- Entreprise
- Internet
- International
---

> Les PME québécoises utiliseraient davantage les progiciels de gestion intégrés, les sites Web mobiles et les extranets que la moyenne des PME au Canada. Toutefois, elles seraient à la traîne au niveau des progiciels de gestion de la relation client et de la chaîne logistique, des logiciels libres, des sites Internet et des intranets.
