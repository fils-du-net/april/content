---
site: Génération nouvelles technologies
title: "Hadopi : recours d'Apple et FDN rejetés"
author: Jérôme G.
date: 2011-10-19
href: http://www.generation-nt.com/hadopi-conseil-etat-apple-fdn-actualite-1491981.html
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Associations
---

> Le Conseil d'État annonce avoir rejeté les requêtes d'Apple et French Data Network contre les décrets Hadopi.
