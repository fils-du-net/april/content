---
site: clubic.com
title: "Etude : la suppression des DRM pourrait faire baisser le piratage"
author: Audrey Oeillet
date: 2011-10-10
href: http://www.clubic.com/antivirus-securite-informatique/actualite-451724-etude-suppression-drm-baisser-piratage.html
tags:
- Entreprise
- Internet
- DRM
---

> Une étude menée par les universités de Rice et de Duke tend à démontrer que la suppression des DRM des fichiers vendus en ligne pourrait contribuer à faire baisser le piratage. Une conclusion qui va à contre-courant des réflexions menées par les ayant-droits.
