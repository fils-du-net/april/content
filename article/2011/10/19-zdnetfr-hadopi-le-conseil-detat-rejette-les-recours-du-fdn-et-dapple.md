---
site: ZDNet.fr
title: "Hadopi : le Conseil d'Etat rejette les recours du FDN et d'Apple"
date: 2011-10-19
href: http://www.zdnet.fr/actualites/hadopi-le-conseil-d-etat-rejette-les-recours-du-fdn-et-d-apple-39764963.htm
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Associations
- DRM
- Europe
---

> Trois décrets de la loi Hadopi contestés par le FDN et Apple passent le filtre du Conseil d’Etat. Ce dernier estime ainsi que la procédure devant la CPD est valide au regard de la convention européenne de sauvegarde des droits de l’homme et des libertés fondamentales.
