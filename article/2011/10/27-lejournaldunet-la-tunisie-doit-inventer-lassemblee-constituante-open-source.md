---
site: LeJournalduNet
title: "La Tunisie doit inventer l'Assemblée constituante open source"
author: Khaled Ben Driss
date: 2011-10-27
href: http://www.journaldunet.com/ebusiness/expert/50339/la-tunisie-doit-inventer-l-assemblee-constituante-open-source.shtml
tags:
- Internet
- Institutions
- International
- Open Data
---

> Le succès des élections de l’Assemblée constituante du 23 octobre 2011 en Tunisie a soufflé un vent d’optimisme sur toute la région. Ces nouveaux élus sont chargés de rédiger en un an une nouvelle Constitution pour définir l’architecture institutionnelle d'une République 2.0.
