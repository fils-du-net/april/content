---
site: 01net.
title: "A Marseille, des affiches incitent à dénoncer le piratage de logiciels"
date: 2011-10-26
href: http://www.01net.com/editorial/545056/a-marseille-des-affiches-incitent-a-denoncer-le-piratage-logiciel/
tags:
- Entreprise
- Internet
- Désinformation
- Licenses
---

> Les éditeurs de logiciels mènent une campagne d’affichage dans le métro et sur les Abribus de Marseille. Elle met en garde contre le piratage et incite les salariés à dénoncer les entreprises qui s’y livreraient.
