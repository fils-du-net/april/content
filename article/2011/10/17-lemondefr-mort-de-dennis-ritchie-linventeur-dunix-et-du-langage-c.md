---
site: LeMonde.fr
title: "Mort de Dennis Ritchie, l'inventeur d'Unix et du langage C"
date: 2011-10-17
href: http://www.lemonde.fr/technologies/article/2011/10/17/mort-de-dennis-ritchie-l-inventeur-d-unix-et-du-langage-c_1589267_651865.html
tags:
- Entreprise
- Licenses
---

> Dennis Ritchie, un programmeur dont les travaux ont permis l'évolution de l'informatique moderne, est mort ce 12 octobre, à l'âge de 70 ans. Il est notamment l'un des créateurs d'Unix, l'un des premiers systèmes d'exploitation – le logiciel central d'un ordinateur – modernes, et était également le co-créateur du langage de programmation C, dont la simplicité et la portabilité ont permis le développement rapide de programmes utilisables sur des machines différentes.
