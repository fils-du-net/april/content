---
site: REBELLYON.INFO
title: "L’Atelier de Bidouille Informatique Libre, la compréhension numérique"
date: 2011-10-01
href: http://rebellyon.info/L-Atelier-de-Bidouille.html
tags:
- Sensibilisation
- Associations
---

> Présentation des premiers ateliers informatiques de la rentrée, organisée par « Les petits débrouillards ».
