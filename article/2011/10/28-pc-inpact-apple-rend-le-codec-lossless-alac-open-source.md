---
site: PC INpact
title: "Apple rend le codec lossless ALAC open source"
author: Vincent Hermann
date: 2011-10-28
href: http://www.pcinpact.com/actu/news/66709-itunes-apple-alac-lossless-open-source-apach.htm
tags:
- Entreprise
- Licenses
- Standards
---

> Traditionnellement, les fichiers audio sous iTunes sont au format AAC. Depuis quelques années, il s’agit d’un format AAC classique à 256 kbps dépourvu de DRM. Mais en 2004, Apple a également mis à disposition dans son application le format ALAC, pour Apple Lossless Audio Codec. Ce dernier est désormais open source.
