---
site: ParisTech REVIEW
title: "Open source: sortir des idées reçues"
date: 2011-10-19
href: http://www.paristechreview.com/2011/10/19/open-source-idees-recues/
tags:
- Entreprise
- Économie
- Associations
---

> Le monde du logiciel libre reste mal connu et, lorsqu'on l'observe attentivement, il est plein de surprises. Sait-on par exemple que la moitié des contributeurs de l'open source sont rémunérés? La frontière entre marchand et gratuit est poreuse, et l'imaginaire des communautés cache des acteurs très divers.
