---
site: Toulouse7.com
title: "data.grandtoulouse.fr inaugure l’ère de l’open data à Toulouse"
author: Christophe Cavailles
date: 2011-10-21
href: http://www.toulouse7.com/2011/10/21/data-grandtoulouse-fr-inaugure-lere-de-lopen-data-a-toulouse/
tags:
- Internet
- Administration
- April
- Open Data
---

> Transparence Démocratique. A l’occasion de La Novela, festival des savoirs partagés, qui propose une thématique Toulouse Numérique jusqu’au 23 octobre, Pierre Cohen signera une convention avec l’Association pour la Promotion et la Recherche en Informatique Libre (APRIL) en faveur des logiciels libres.
