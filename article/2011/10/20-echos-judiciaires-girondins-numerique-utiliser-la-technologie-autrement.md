---
site: Echos judiciaires Girondins
title: "Numérique : Utiliser la technologie autrement"
author: Anne d'AUBREE
date: 2011-10-20
href: http://www.echos-judiciaires.com/high-tech/numerique-utiliser-la-technologie-autrement-a8477.html
tags:
- Entreprise
- Internet
- Sensibilisation
- Associations
---

> Entre faire soi-même son pain à la maison et se fabriquer un serveur informatique tout seul, la distance est-elle si grande ? Dans la lignée du logiciel libre, c’est toute une communauté qui tente de convaincre le grand public de s’approprier les technologies de l’information d’une façon alternative.
