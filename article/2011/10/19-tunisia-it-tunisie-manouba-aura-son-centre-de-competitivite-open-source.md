---
site: Tunisia IT
title: "Tunisie : Manouba aura son centre de compétitivité Open Source"
author: TB
date: 2011-10-19
href: http://www.tunisiait.com/article.php?article=8518
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- International
---

> Après tous les travaux accomplis par l’APOS, et suite à une demande faite au Secrétaire d’Etat chargé des TIC, Adel Gaaloul, la technopôle de Manouba sera dotée d’un centre de compétitivité Open Source.
