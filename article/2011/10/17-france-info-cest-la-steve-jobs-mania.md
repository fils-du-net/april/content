---
site: france info
title: "C'est la Steve Jobs mania !"
author: Jérôme Colombain
date: 2011-10-17
href: http://www.france-info.com/chroniques-nouveau-monde-2011-10-17-c-est-la-steve-jobs-mania-569241-29-35.html
tags:
- Entreprise
- Logiciels privateurs
---

> Deux semaines après son décès, l'ancien patron d'Apple commence à faire l'objet d'un véritable culte de la personnalité…
