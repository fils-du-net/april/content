---
site: artesi
title: "Toulouse devient la première grande ville française à adhérer à l'April"
date: 2011-10-24
href: http://www.artesi.artesi-idf.com/public/article/toulouse-devient-la-premiere-grande-ville-francaise-a-adherer-a-l-april.html?id=24539
tags:
- Internet
- Administration
- April
- Open Data
---

> Ancrer la ville dans la promotion du logiciel libre
