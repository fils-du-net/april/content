---
site: Politis.fr
title: "Ventes liées : le status quo"
author: Christine Tréguier
date: 2011-10-13
href: http://www.politis.fr/Ventes-liees-le-status-quo,15528.html
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
---

> Cela fait des années que les associations de consommateurs et de défense du logiciel libre demandent l’interdiction de la vente liée ordinateur/logiciels. Autrement dit, ils réclament le libre choix du système d’exploitation (OS Windows ou Linux) et remboursement en cas de Windows pré-installé. Face à leur insistance, le Plan numérique pour 2012 ficelé en 2008 par Éric Besson, alors secrétaire d’État à l’Économie numérique, avait concédé quelques avancées.
