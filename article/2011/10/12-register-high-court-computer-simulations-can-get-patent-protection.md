---
site: The Register
title: "High Court: Computer simulations can get patent protection"
author: OUT-LAW.COM
date: 2011-10-12
href: http://www.theregister.co.uk/2011/10/12/computer_simulations_can_get_patent_protection/
tags:
- Entreprise
- Institutions
- Brevets logiciels
- English
---

> (descend l'office des brevets sur le jugement des simulations de conception de foreuse) Smacks down IPO on drill design sim ruling
