---
site: SCIENCES et AVENIR.fr
title: "Steve Jobs, par Roberto Di Cosmo: un talent unique de «dénicheur d’inventions»"
author: Hervé Ratel
date: 2011-10-07
href: http://www.sciencesetavenir.fr/actualite/high-tech/20111007.OBS1925/steve-jobs-par-roberto-di-cosmo-un-talent-unique-de-denicheur-d-inventions.html
tags:
- Entreprise
- Logiciels privateurs
- Brevets logiciels
- Innovation
---

> Chercheur en informatique, défenseur du logiciel libre, l’Italien Roberto Di Cosmo a beau avoir une philosophie «aux antipodes» de celle d’Apple, il salue l’exigence de qualité et la capacité à dénicher les innovations de Steve Jobs.
