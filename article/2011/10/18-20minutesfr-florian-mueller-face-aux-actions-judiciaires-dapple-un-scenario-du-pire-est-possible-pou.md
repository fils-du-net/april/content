---
site: 20minutes.fr
title: "Florian Müller: «Face aux actions judiciaires d'Apple, un scénario du pire est possible pour Android»"
author: Philippe Berry
date: 2011-10-18
href: http://www.20minutes.fr/high-tech/apple/808236-florian-muller-face-actions-judiciaires-apple-scenario-pire-possible-android
tags:
- Entreprise
- Institutions
- Brevets logiciels
---

> INTERVIEW - Le grand gourou des brevets décrypte pour 20minutes.fr le bras de fer actuel sur les smartphones...
