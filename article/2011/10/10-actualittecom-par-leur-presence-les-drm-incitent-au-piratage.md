---
site: ActuaLitté.com
title: "Par leur présence, les DRM incitent au piratage"
author: Clement Solym
date: 2011-10-10
href: http://www.actualitte.com/actualite/lecture-numerique/usages/par-leur-presence-les-drm-incitent-au-piratage-28938.htm
tags:
- Entreprise
- Économie
- DRM
---

> Pas une nouveauté, mais certaines évidences sont bonnes à répéter...
