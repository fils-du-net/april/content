---
site: 01net.
title: "Logiciel libre : Toulouse, première ville adhérente de l'April"
date: 2011-10-24
href: http://www.01net.com/editorial/544810/logiciel-libre-toulouse-premiere-grande-ville-a-rejoindre-l-and-039-april/
tags:
- Administration
- April
---

> La Ville rose est la cinquième collectivité à adhérer à l'association de défense du logiciel libre.
