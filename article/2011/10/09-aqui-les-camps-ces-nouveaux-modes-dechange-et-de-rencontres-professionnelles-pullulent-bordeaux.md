---
site: AQUI!
title: "Les Camps, ces nouveaux modes d'échange et de rencontres professionnelles pullulent à Bordeaux"
author: Isabelle Camus
date: 2011-10-09
href: http://www.aqui.fr/cultures/les-camps-ces-nouveaux-modes-d-echange-et-de-rencontres-professionnelles-pullulent-a-bordeaux,5451.html
tags:
- Entreprise
- Internet
- Partage du savoir
- Associations
---

> BarCamp, TwitterCamp, OrangeCamp, FormCamp... Depuis le BarCamp de Bordeaux, les 23 et 24 avril derniers, les Camps se multiplient, un peu partout et sur tous les thèmes autour de l'outil numérique dans une réflexion partagée sur les nouveaux outils, les tendances, la formation, voire même sur la manière de changer le monde. Sensible aux questions numériques, le journal Aqui.fr a voulu en savoir plus sur cette dynamique collective qui anime la communauté numérique, ici et là,  un peu partout dans le monde, et fédère de plus en plus d'adeptes.
