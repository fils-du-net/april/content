---
site: LeMonde.fr
title: "Le vendeur d'ordinateurs doit être précis sur les logiciels préinstallés "
author: Le Monde.fr      
date: 2011-10-14
href: http://www.lemonde.fr/technologies/article/2011/10/14/le-vendeur-d-ordinateurs-doit-etre-precis-sur-les-logiciels-preinstalles_1587699_651865.html
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
---

> Un vendeur d'ordinateurs doit fournir au consommateur des informations précises sur les caractéristiques de la machine et des logiciels d'exploitation et d'application préinstallés, a jugé, jeudi 13 octobre, la Cour de cassation. Ces informations, selon les juges, sont nécessaires au "consommateur moyen" pour lui permettre de prendre une décision d'achat en connaissance de cause.
