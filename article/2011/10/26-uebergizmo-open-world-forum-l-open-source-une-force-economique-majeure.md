---
site: übergizmo
title: "Open World Forum – L’Open Source, Une Force Économique Majeure"
author: Eliane Fiolet
date: 2011-10-26
href: http://fr.ubergizmo.com/2011/10/open-world-forum-open-source-force-economique/
tags:
- Entreprise
- Internet
- Administration
- Économie
- Sensibilisation
- Associations
- Open Data
---

> La 4e édition de l’Open World Forum, une conférence internationale dédiée à la réflexion autour de l’Open Source s’est tenue du 22 au 24 septembre 2011 à Paris. Dans le cadre du partenariat média d’Ubergizmo, je me suis entretenue avec les co-Présidents, Jean-Pierre Laisné, et Louis Montagne et avec Véronique Torner, co-fondatrice de l’Open CIO Summit.
