---
site: Le Monde Informatique
title: "Emplois dans l'Open Source : Où chercher ? Ce qu'il faut savoir"
author: Jean Elyan
date: 2011-10-24
href: http://www.lemondeinformatique.fr/actualites/lire-emplois-dans-l-open-source-ou-chercher-ce-qu-il-faut-savoir-1e-partie-42366.html
tags:
- Entreprise
- Économie
---

> A quoi ressemble l'avenir pour les développeurs de logiciels talentueux et ceux qui disposent des compétences nécessaires pour ces métiers ? Comme dans tous les secteurs, la tendance générale, c'est qu'il faut se débrouiller seul. Mais le domaine du logiciel libre/Open Source (Free/Libre Open Source Software ou FLOSS) recèle une offre beaucoup plus riche et concentre potentiellement beaucoup plus d'opportunités que les autres.
