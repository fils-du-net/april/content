---
site: L'Expansion.com
title: "\"Les grosses entreprises en France empêchent la croissance des PME\""
author: Danièle Licata
date: 2011-10-07
href: http://lexpansion.lexpress.fr/economie/les-grosses-entreprises-en-france-empechent-la-croissance-des-pme_264749.html
tags:
- Entreprise
- Économie
- Institutions
- Europe
---

> Pour André-Yves Portnoff, consultant en prospective et stratégie, Directeur de l'Observatoire de la Révolution de l'intelligence, il est temps d'aider enfin les PME pour relancer la croissance.
