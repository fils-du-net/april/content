---
site: Numerama
title: "La littérature par Orange : Albanel parle \"d'acheter un droit de lecture\""
author: Guillaume Champeau
date: 2011-10-10
href: http://www.numerama.com/magazine/20122-la-litterature-par-orange-albanel-parle-34d-acheter-un-droit-de-lecture34.html
tags:
- Entreprise
- Institutions
- DADVSI
- DRM
---

> Lire est un droit. Mais il faudra payer pour en bénéficier. C'est l'idée qu'a développé l'ancienne ministre Christine Albanel, reconvertie en directrice de la stratégie des contenus chez Orange.
