---
site: "cio-online.com"
title: "De Particulier à Particulier choisit un SGBD open-source alternatif"
author: Bertrand Lemaire
date: 2011-10-11
href: http://www.cio-online.com/actualites/lire-de-particulier-a-particulier-choisit-un-sgbd-open-source-alternatif-3871.html
tags:
- Entreprise
- Internet
---

> Le premier groupe de presse immobilier français choisit MariaDB de SkySQL.
