---
site: 01net.
title: "Richard Stallman « content que Steve Jobs soit parti »"
author: Eric Le Bourlout
date: 2011-10-10
href: http://www.01net.com/editorial/543746/richard-stallman-content-que-steve-jobs-soit-parti/
tags:
- Entreprise
- Logiciels privateurs
- Associations
---

> Le fondateur de la Free Software Foundation espère que l'influence d'Apple sur l'informatique, selon lui néfaste, diminuera après la mort de son créateur.
