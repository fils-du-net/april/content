---
site: leParisien.fr
title: "Un jugement met à mal la vente liée ordinateurs/logiciels"
date: 2011-10-06
href: http://www.leparisien.fr/high-tech/un-jugement-met-a-mal-la-vente-liee-ordinateurs-logiciels-06-10-2011-1642650.php
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
---

> La Cour de cassation a rappelé, jeudi 6 octobre, qu'un vendeur se doit de communiquer au client les informations relatives aux caractéristiques principales d'un ordinateur équipé de logiciels d'exploitation et d'applications. Cette décision, rendue dans une affaire opposant l'UFC-Que Choisir à Darty, intervient alors que le projet de loi consommation est actuellement en lecture à l'Assemblée nationale et que la problématique de la vente liée y est débattue.
