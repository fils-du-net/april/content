---
site: Localtis.info
title: "Un logiciel libre peut-il être imposé dans les spécifications techniques ?"
author: Luc Derriano
date: 2011-10-05
href: http://www.localtis.info/cs/ContentServer?pagename=Localtis/LOCActu/ArticleActualite&cid=1250262508180
tags:
- Administration
- Marchés publics
---

> A l'occasion d'un arrêt du 30 septembre 2011, les magistrats du Palais-Royal précisent la liberté que détient le pouvoir adjudicateur dans la définition préalable de ses besoins. En l'occurrence, il s'agissait de savoir si une collectivité peut imposer aux candidats l'utilisation d'un logiciel libre déterminé.
