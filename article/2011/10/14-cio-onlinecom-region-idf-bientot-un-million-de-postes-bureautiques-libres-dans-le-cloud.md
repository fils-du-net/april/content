---
site: "cio-online.com"
title: "Région IDF : bientôt un million de postes bureautiques libres dans le cloud"
author: Bertrand Lemaire
date: 2011-10-14
href: http://www.cio-online.com/actualites/lire--3883.html
tags:
- Entreprise
- Internet
- Administration
- Associations
---

> La Région Ile-de-France s'est associée à la Documentation Foundation pour proposer une version SaaS de LibreOffice à la communauté lycéenne régionale.
