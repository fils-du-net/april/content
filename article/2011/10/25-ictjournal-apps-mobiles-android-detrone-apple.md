---
site: ICTjournal
title: "Apps mobiles: Android détrône Apple"
author: Tania Séverin
date: 2011-10-25
href: http://www.ictjournal.ch/News/2011/10/25/Apps-mobiles-Android-detrne-Apple.aspx
tags:
- Entreprise
- Logiciels privateurs
---

> Selon ABIresearch, les téléchargements d’Apps mobiles Android dépassent désormais largement ceux d’applications Apple.
