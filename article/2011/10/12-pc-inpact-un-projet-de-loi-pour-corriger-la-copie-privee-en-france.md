---
site: PC INpact
title: "Un projet de loi pour corriger la copie privée en France"
author: Marc Rees
date: 2011-10-12
href: http://www.pcinpact.com/actu/news/66347-copie-privee-projet-loi-professionnels-culture.htm
tags:
- Entreprise
- Institutions
- Droit d'auteur
- Europe
---

> Exclu. Selon nos informations, de sources concordantes, un projet de loi est en travaux pour corriger au plus vite le régime français de la copie privée. La Rue de Valois espère le présenter dans une quinzaine de jours en Conseil des ministres, malgré un calendrier parlementaire surchargé.
