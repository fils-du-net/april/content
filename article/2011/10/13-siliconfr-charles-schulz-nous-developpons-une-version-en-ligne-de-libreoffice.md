---
site: Silicon.fr
title: "Charles Schulz : « Nous développons une version en ligne de LibreOffice »"
author: Christophe Lagane
date: 2011-10-13
href: http://www.silicon.fr/charles-schulz%C2%A0-%C2%AB%C2%A0nous-developpons-une-version-en-ligne-de-libreoffice%C2%A0%C2%BB-62997.html
tags:
- Internet
- Administration
- Associations
---

> Version en ligne et mobile, réécriture du code, déploiements massifs... Un an après sa création, LibreOffice fourmille de projets dans une dynamique qui l'émancipe avec succès d'OpenOffice.org.
