---
site: écrans
title: "« Je ne suis pas heureux qu'il soit mort, mais je suis heureux qu'il soit parti »"
author: Erwan Cario
date: 2011-10-07
href: http://www.ecrans.fr/Je-ne-suis-pas-heureux-qu-il-soit,13359.html
tags:
- Entreprise
---

> On le sait très bien, Richard Stallman, fondateur du mouvement du logiciel libre et de la licence GPL, n’est pas vraiment adepte de la langue de bois (lire l’interview). Il dit et écrit ce qu’il pense, au risque, parfois, de choquer.
