---
site: Génération nouvelles technologies
title: "Windows 8 : la FSF veut éviter le boot restricted"
author: Jérôme G.
date: 2011-10-20
href: http://www.generation-nt.com/windows-8-secure-boot-fsf-petition-linux-uefi-actualite-1492111.html
tags:
- Entreprise
- Logiciels privateurs
- Vente liée
- Associations
---

> Pour la Free Software Foundation, le boot de Windows 8 avec l'UEFI n'est pas tant sécurisé mais plutôt restrictif.
