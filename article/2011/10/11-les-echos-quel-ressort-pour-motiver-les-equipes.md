---
site: Les Echos
title: "quel ressort pour motiver les équipes ?"
author: Eric Albert
date: 2011-10-11
href: http://www.lesechos.fr/journal20111011/lec1_competences/0201682687806-quel-ressort-pour-motiver-les-equipes-231574.php
tags:
- Entreprise
- Innovation
---

> Récemment, la presse a raconté comment la structure d'une enzyme du virus du sida que les équipes de recherche essayaient d'identifier depuis plus de dix ans avait été découverte en trois semaines par des internautes collaborant via un « serious game ». Au-delà de l'efficacité nouvelle qu'apportent ces modes de collaboration, dits « open source », on peut s'interroger sur ce qui pousse des internautes à y participer. Ils ne peuvent espérer ni une reconnaissance, ni une quelconque rémunération.
