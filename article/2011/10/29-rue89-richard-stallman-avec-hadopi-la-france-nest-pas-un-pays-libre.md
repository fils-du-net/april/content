---
site: Rue89
title: "Richard Stallman : « Avec Hadopi, la France n'est pas un pays libre »"
author: Martin Untersinger
date: 2011-10-29
href: http://www.rue89.com/2011/10/29/richard-stallman-avec-hadopi-la-france-nest-pas-un-pays-libre-225958
tags:
- Entreprise
- Internet
- Logiciels privateurs
- HADOPI
- Institutions
- DADVSI
- DRM
---

> L'informaticien et activiste Richard Stallman est le père du logiciel libre. Ses combats : le respect de la vie privée et les libertés informatiques. Entretien.
