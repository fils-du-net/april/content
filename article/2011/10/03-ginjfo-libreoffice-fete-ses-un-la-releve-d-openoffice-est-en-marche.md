---
site: GinjFo
title: "LibreOffice fête ses un an : La relève d’OpenOffice est en marche"
author: Jerome Gianoli
date: 2011-10-03
href: http://www.ginjfo.com/actualites/logiciels/libreoffice-fete-ses-un-an-la-releve-dopenoffice-est-en-marche-20111003
tags:
- Entreprise
- Associations
---

> Il y a tout juste un an, un séisme s’est produit autour d’OpenOffice.  Une partie des développeurs de la suite bureautique Open Source décida de prendre son indépendance avec la création de « The Documentation Foundation » et avec elle la suite LibreOffice. Le but est  avant tout d’assurer un développement entièrement communautaire.
