---
site: PC INpact
title: "Vente liée et protection du consommateur : la colère de l'AFUL"
author: Marc Rees
date: 2011-10-03
href: http://www.pcinpact.com/actu/news/66155-vente-liee-subordonnee-racketiciel-aful.htm?vc=1
tags:
- Institutions
- Vente liée
- Associations
---

> L’Assemblée nationale devrait examiner ce soir une série d’amendements concernant la vente liée PC et OS. Dans le lot, un amendement 432 rectifié, déposé par le rapporteur Fasquelle et Lionel Tardy, veut obliger les vendeurs d’ordinateurs à informer le consommateur « de la faculté ou non de renoncer, après achat, à la licence de ce logiciel, et, si cette faculté lui est offerte, des modalités et du montant du remboursement prévu par le fabricant ».
