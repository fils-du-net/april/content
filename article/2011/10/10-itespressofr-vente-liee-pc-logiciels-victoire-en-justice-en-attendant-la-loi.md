---
site: ITespresso.fr
title: "Vente liée PC – logiciels : victoire en justice en attendant la loi ?"
author: Jacques Franc de Ferrière
date: 2011-10-10
href: http://www.itespresso.fr/vente-liee-pc-logiciels-victoire-en-justice-en-attendant-la-loi-47166.html
tags:
- Entreprise
- Institutions
- Vente liée
- Associations
---

> L’UFC-Que Choisir célèbre une décision de la Cour de cassation validant en partie l’illégalité des ventes liées PC et logiciels.
