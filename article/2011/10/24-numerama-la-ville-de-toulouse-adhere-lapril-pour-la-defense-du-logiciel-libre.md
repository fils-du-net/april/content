---
site: Numerama
title: "La ville de Toulouse adhère à l'April pour la défense du logiciel libre"
author: Guillaume Champeau
date: 2011-10-24
href: http://www.numerama.com/magazine/20301-la-ville-de-toulouse-adhere-a-l-april-pour-la-defense-du-logiciel-libre.html
tags:
- Entreprise
- Administration
- April
- Open Data
---

> La défense du logiciel libre passera-t-elle d'abord par les collectivités locales ? L'April a convaincu la sixième plus grande ville de France, Toulouse, d'adhérer à l'association de promotion du logiciel libre.
