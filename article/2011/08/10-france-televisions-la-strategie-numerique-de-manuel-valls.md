---
site: france télévisions
title: "La stratégie numérique de Manuel Valls"
date: 2011-08-10
href: http://www.francetv.fr/2012/la-strategie-numerique-de-manuel-valls-1987
tags:
- Internet
- Partage du savoir
- HADOPI
- Institutions
- Droit d'auteur
- Licenses
---

> Les réponses du député-maire d'Evry et candidat à la primaire PS à notre questionnaire sur sa vision d'Internet et des réseaux sociaux dans la campagne présidentielle.
