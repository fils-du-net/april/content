---
site: Silicon.fr
title: "Le Danemark adopte LibreOffice"
author: David Feugey
date: 2011-08-22
href: http://www.silicon.fr/le-danemark-adopte-libreoffice-58690.html
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Licenses
---

> Premier succès de taille en entreprise pour LibreOffice. La suite bureautique open source de la Document Foundation va en effet être déployée sur 25.000 postes de travail au Danemark.
