---
site: OWNI
title: "La Tunisie 2.0 est aux urnes"
author: Pierre Alonso
date: 2011-08-29
href: http://owni.fr/2011/08/29/la-tunisie-2-0-est-aux-urnes/
tags:
- Internet
- Institutions
- Associations
---

> Le 23 octobre, la Tunisie a rendez-vous avec l'Histoire, pour l'élection de son Assemblée constituante. À Tunis, OWNI a rencontré l'avant-garde de la démocratie en réseaux. De la politique participative innovante. Séduisante.
