---
site: OWNI
title: "Dessine-moi un hacker"
author: Sabine Blanc
date: 2011-08-09
href: http://owni.fr/2011/08/09/dataviz-infographie-hackerspaces-dessine-moi-un-hacker/
tags:
- Entreprise
- Internet
- Partage du savoir
- Associations
- International
---

> OWNI vous propose un poster synthétisant une étude réalisée par un Finlandais sur les hackerspaces. Elle se base sur environ 250 réponses de 87 communautés de 19 pays du monde entier.
