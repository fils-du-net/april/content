---
site: tom's hardware
title: "La justice américaine se penche sur le rachat des brevets Nortel"
author: David Civera
date: 2011-08-01
href: http://www.presence-pc.com/actualite/Nortel-brevets-44527/
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Licenses
---

> Le département américain de la justice aurait décidé de se pencher sur le rachat des brevets de Nortel par le consortium Rockstar Bidco. Il pourrait imposer des conditions d’utilisation plus strictes afin de s’assurer que cette acquisition par les grands noms de l’industrie ne freine pas la concurrence.
