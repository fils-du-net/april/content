---
site: PC INpact
title: "Vente liée, CLUF : la justice épingle Samsung France"
author: Marc Rees
date: 2011-08-16
href: http://www.pcinpact.com/actu/news/65072-samsung-vente-liee-xp-cluf.htm
tags:
- Entreprise
- Logiciels privateurs
- Institutions
- Vente liée
---

> Afin d'éviter les condamnations pour vente liée, les constructeurs ont mis en place des procédures de remboursement des licences refusées par leurs clients. Le hic est que ces démarches sont parfois complexes. La juridiction de proximité des Sables d'Olonne (*) vient ainsi de déclarer abusives plusieurs clauses de la procédure organisée par Samsung.
