---
site: journaldunet.com
title: "Le matériel informatique libre sort de l'ombre"
author: Guillaume Serries
date: 2011-08-06
href: http://www.journaldunet.com/solutions/acteurs/article/le-materiel-libre-sortirait-il-de-sa-discretion-habituelle.shtml
tags:
- Matériel libre
---

> Jusqu'ici confiné aux composants, le matériel libre pourrait devenir une notion adoptée par le grand public. Les projets ne manquent pas. La réussite en revanche n'est pas encore au rendez-vous.
