---
site: OWNI
title: "Linux: 20 ans, manchot et toujours libre"
author: Patrick Guignot
date: 2011-08-30
href: http://owni.fr/2011/08/30/interview-linus-torvalds-linux-20-ans-manchot-et-toujours-libre/
tags:
- Entreprise
- Associations
- Brevets logiciels
- DRM
- Licenses
- Philosophie GNU
---

> Linux vient d'avoir 20 ans. Retour sur l'histoire d'une invention avec son créateur, Linus Torvalds. Lancé comme un "hobby", le noyau Linux fait aujourd'hui fonctionner des serveurs et des terminaux qui rendent possible Internet.
