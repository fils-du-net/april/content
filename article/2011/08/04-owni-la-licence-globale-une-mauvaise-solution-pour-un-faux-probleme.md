---
site: OWNI
title: "La licence globale, une “mauvaise solution pour un faux problème”"
author: Jérémie Nestel (Libre Accès)
date: 2011-08-04
href: http://owni.fr/2011/08/04/la-licence-globale-une-mauvaise-solution-pour-un-faux-probleme/
tags:
- Entreprise
- Internet
- HADOPI
- Droit d'auteur
- Licenses
---

> La veillée funèbre d'Hadopi a débuté sous les bons auspices de Nicolas Sarkozy. Comment régler alors en France l'éternelle question de la "propriété intellectuelle" ? Éléments de réponse par l'expert Laurent Chemla.
