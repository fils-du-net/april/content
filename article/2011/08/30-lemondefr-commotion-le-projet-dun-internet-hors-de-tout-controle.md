---
site: LeMonde.fr
title: "Commotion, le projet d'un Internet hors de tout contrôle"
author: Yves Eudes
date: 2011-08-30
href: http://www.lemonde.fr/technologies/article/2011/08/30/commotion-le-projet-d-un-internet-hors-de-tout-controle_1565282_651865.html
tags:
- Entreprise
- Internet
- Institutions
- International
---

> Un immeuble confortable et anonyme, au cœur de Washington, à quelques rues de la Maison Blanche. Dans une enfilade de bureaux au fond du 5e étage, une vingtaine de jeunes gens, surtout des garçons, travaillent discrètement, dans une ambiance à la fois studieuse et décontractée. Cette petite équipe, composée d'informaticiens, de juristes et de sociologues, est en train de réaliser l'utopie suprême des hackers et des militants libertaires du monde entier : un logiciel permettant la création de réseaux sans fil à haut débit 100 % autonomes, qui fonctionneront sur les fréquences Wi-Fi, sans s'appuyer sur aucune infrastructure existante – ni relais téléphonique, ni câble, ni satellite. Ils seront mouvants, horizontaux, entièrement décentralisés et échapperont à toute surveillance, car le trafic sera anonyme et crypté.
