---
site: écrans
title: "« Les titres de propriété intellectuelle sont utilisés comme des outils stratégiques »"
author: Geoffroy Husson
date: 2011-08-11
href: http://www.ecrans.fr/Les-titres-de-propriete,13168.html
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
- Standards
- Europe
- International
---

> Ces dernières semaines, les attaques entre géants des nouvelles technologies se sont multipliées. En cause, les brevets logiciel déposés par ces sociétés, couvrant parfois leurs innovations et, plus souvent, leur permettant de poursuivre leurs concurrents. Wikipédia définit le brevet logiciel comme « un droit exclusif d’exploitation portant sur une méthode mise en œuvre à l’aide d’un ordinateur ».
