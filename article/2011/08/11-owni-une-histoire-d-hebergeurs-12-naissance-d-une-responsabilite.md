---
site: OWNI
title: "Une histoire d’hébergeurs (1/2): naissance d’une responsabilité"
author: Benoit Tabaka
date: 2011-08-11
href: http://owni.fr/2011/08/11/une-histoire-dhebergeurs-12-naissance-dune-responsabilite/
tags:
- Internet
- Institutions
---

> En 1999, naissait la notion de responsabilité des hébergeurs face aux contenus litigieux d'Internet. Premier volet d'une rétrospective de Benoit Tabaka sur un régime juridique qui vient aujourd'hui estomper la liberté d'expression sur le réseau.
