---
site: LE CERCLE Les Echos
title: "De la souveraineté en général et de la souveraineté numérique en particulier"
author: Pierre Bellanger
date: 2011-08-30
href: http://lecercle.lesechos.fr/entreprises-marches/high-tech-medias/internet/221137239/souverainete-general-et-souverainete-numeriq
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Économie
- Institutions
- International
---

> (Par Pierre Bellanger, Président de Skyrock) - Qu'est ce que la souveraineté à l'âge du numérique ? Qu'avons-nous abandonné ? Que peut-on reconquérir ?
