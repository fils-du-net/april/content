---
site: OWNI
title: "Une histoire d’hébergeurs (2/2): et la liberté d’expression?"
author: Benoit Tabaka
date: 2011-08-12
href: http://owni.fr/2011/08/12/internet-droit-histoire-hebergeurs-liberte-expression/
tags:
- Entreprise
- Internet
---

> Suite et fin d' "Une histoire d'hébergeurs", par le juriste Benoit Tabaka, qui revient sur la création du concept de "contenu manifestement illicite", en s'interrogeant sur ses effets réels, notamment sur la préservation de la liberté d'expression.
