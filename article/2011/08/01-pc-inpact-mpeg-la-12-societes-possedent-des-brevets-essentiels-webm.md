---
site: PC INpact
title: "MPEG-LA: 12 sociétés possèdent des brevets essentiels à WebM"
author: Vincent Hermann
date: 2011-08-01
href: http://www.pcinpact.com/actu/news/64872-mpeg-la-webm-brevets-rassemblement-codec.htm
tags:
- Entreprise
- Internet
- Associations
- Brevets logiciels
- Video
---

> Lorsque Google a acquis la société On2 l’année dernière pour 106,5 millions de dollars, la cible réelle était le codec vidéo VP8. Quelques mois plus tard, la firme annonçait le lancement du conteneur WebM, destiné à envahir le web tout en affrontant H.264. Mais de nombreuses zones d’ombre subsistaient, notamment sur les brevets. L’association MPEG-LA avait lancé un appel en début d’année aux sociétés qui pourraient détenir des brevets essentiels à VP8 et WebM. Résultat : plusieurs entreprises se sont manifestées.
