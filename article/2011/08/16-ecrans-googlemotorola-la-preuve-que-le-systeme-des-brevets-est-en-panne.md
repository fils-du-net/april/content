---
site: écrans
title: "Google/Motorola : la preuve que le système des brevets est en panne"
author: Jamie Love
date: 2011-08-16
href: http://www.ecrans.fr/Google-Motorola-la-preuve-que-le,13184.html
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Brevets logiciels
- Licenses
- International
---

> Ce texte est signé de Jamie Love, directeur général de Knowledge Ecology International, une ONG spécialisée dans les effets de la propriété intellectuelle sur les politiques de santé, d’environnement ou d’innovation. Publié à l’origine sur le Huffington Post, il est traduit en français par Florent Latrive, avec l’autorisation de son auteur. Il est sous licence Creative Commons.
