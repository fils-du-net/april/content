---
site: Numerama
title: "Pendant les vacances, l'Hadopi pose enfin les bonnes questions"
author: Guillaume Champeau
date: 2011-08-16
href: http://www.numerama.com/magazine/19566-pendant-les-vacances-l-hadopi-pose-enfin-les-bonnes-questions.html
tags:
- Entreprise
- Internet
- HADOPI
- Institutions
- Droit d'auteur
---

> L'Hadopi a lancé un appel d'offres pour identifier les raisons pour lesquelles les internautes consomment peu de biens culturels sur Internet, et identifier les leviers qui permettraient de muscler la consommation légale.
