---
site: clubic.com
title: "Android serait la plateforme open source la moins ouverte"
author: Guillaume Belfiore
date: 2011-08-04
href: http://www.clubic.com/smartphone/android/actualite-439076-android-plateforme-open-source-ouverte.html
tags:
- Entreprise
- Philosophie GNU
- Europe
---

> Une étude menée par un cabinet londonien a mesuré le degré d'ouverture des plateformes open source. Au travers des résultats, il semblerait qu'Android soit la plus fermée.
