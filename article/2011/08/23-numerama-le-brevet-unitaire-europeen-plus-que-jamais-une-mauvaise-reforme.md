---
site: Numerama
title: "Le brevet unitaire européen : plus que jamais une mauvaise réforme"
author: Guillaume Champeau
date: 2011-08-23
href: http://www.numerama.com/magazine/19618-le-brevet-unitaire-europeen-plus-que-jamais-une-mauvaise-reforme.html
tags:
- Entreprise
- Administration
- Économie
- Institutions
- Brevets logiciels
- Europe
---

> La Commission européenne veut imposer une procédure de dépôt unique de brevet communautaire, qui couvrirait l'ensemble des états membres, pour un coût largement réduit pour les entreprises. Mais un tel "brevet unitaire européen" présente le double risque de rouvrir la porte aux brevets logiciels et d'amplifier la crise économique en multipliant les barrières à l'innovation.
