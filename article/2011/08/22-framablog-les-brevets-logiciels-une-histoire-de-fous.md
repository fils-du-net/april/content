---
site: Framablog
title: "Les brevets logiciels : une histoire de fous !"
author: aKa
date: 2011-08-22
href: http://www.framablog.org/index.php/post/2011/08/22/histoire-de-fous-brevets-logiciels
tags:
- Entreprise
- Institutions
- Brevets logiciels
---

> « Les entreprises trouvent plus intéressant de gagner de l’argent en se faisant mutuellement des procès qu’en créant vraiment des produits. » Tel est le cinglant résumé de ce récent éditorial du Guardian qui prend appui sur l’actualité, dont le fameux rachat de Motorola par Google, pour nous livrer un constat aussi amer qu’objectif[1].
