---
site: DIRECTION INFORMATIQUE
title: "Percée du logiciel libre dans la fonction publique québécoise"
author: André Ouellet
date: 2011-08-22
href: http://www.directioninformatique.com/DI/client/fr/DirectionInformatique/Nouvelles.asp?id=63800
tags:
- Entreprise
- Logiciels privateurs
- Administration
- Économie
- Institutions
- Associations
- Marchés publics
- International
---

> On se souviendra qu'en juin 2010, la Cour supérieure du Québec a donné raison à la firme de services-conseils en technologies ouvertes Savoir-faire Linux, selon qui la Régie des rentes du Québec (RRQ) avait agi illégalement en procédant, en 2008, à une migration de plateformes et à une acquisition de licences sans appel d'offres. Un peu plus de douze mois après la prononciation de ce jugement notable, quels constats peut-on faire?
