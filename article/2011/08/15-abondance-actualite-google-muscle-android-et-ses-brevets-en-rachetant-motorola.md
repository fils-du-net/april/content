---
site: Abondance Actualité
title: "Google muscle Android et ses brevets en rachetant Motorola"
date: 2011-08-15
href: http://actu.abondance.com/2011/08/google-muscle-android-et-ses-brevets-en.html
tags:
- Entreprise
- Brevets logiciels
---

> Google vient d'annoncer le rachat de la société Motorola pour muscler son système d'exploitation Android, ses parts de marché dans le mobile et acquérir les brevets de la société...
