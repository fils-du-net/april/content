---
site: LADEPECHE.fr
title: "Onet-le-Château. Cap Festival pour l'ARU2L - Aveyron"
date: 2011-08-20
href: http://www.ladepeche.fr/article/2011/08/20/1149835-onet-le-chateau-cap-festival-pour-l-aru2l.html
tags:
- Internet
- Sensibilisation
- Associations
---

> L'ARU2L, Association (castonétoise) rouergate des utilisateurs de logiciels libres, sera présente au Cap festival, les 20 et 21 août, pour fournir un stand « Cyber-Internet libre et gratuit » aux festivaliers, et leur permettre de se familiariser avec le monde du Libre.
