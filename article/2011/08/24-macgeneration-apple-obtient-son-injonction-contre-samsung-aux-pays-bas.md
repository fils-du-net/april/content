---
site: macgeneration
title: "Apple obtient son injonction contre Samsung aux Pays-Bas"
author: Florian Innocente
date: 2011-08-24
href: http://www.macgeneration.com/news/voir/213362/apple-obtient-son-injonction-contre-samsung-aux-pays-bas
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Europe
---

> Une Cour de Justice de La Haye aux Pays-Bas a délivré une injonction préliminaire contre Samsung à la demande d'Apple.
