---
site: Numerama
title: "Free va enfin publier le code source des logiciel libres de ses Freebox"
author: Guillaume Champeau
date: 2011-08-01
href: http://www.numerama.com/magazine/19468-free-va-enfin-publier-le-code-source-des-logiciel-libres-de-ses-freebox.html
tags:
- Entreprise
- Internet
- Institutions
- Associations
- Licenses
---

> Après des années de refus adressés à la communauté du logiciel libre, Free devrait bientôt ouvrir le site Floss.freebox.fr, sur lequel il mettra à disposition le code source des logiciels libres utilisés dans ses Freebox.
