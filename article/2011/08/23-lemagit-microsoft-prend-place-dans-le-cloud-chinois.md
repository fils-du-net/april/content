---
site: LeMagIT
title: "Microsoft prend place dans le Cloud chinois"
author: Cyrille Chausson
date: 2011-08-23
href: http://www.lemagit.fr/article/microsoft-chine-cloud-computing/9281/1/microsoft-prend-place-dans-cloud-chinois/
tags:
- Entreprise
- Logiciels privateurs
- Informatique en nuage
- International
---

> Pour pénétrer le marché chinois du Cloud Computing, Microsoft a du se rapprocher un peu plus près de Linux.
