---
site: clubic.com
title: "Fondation Linux : un outil pour suivre les licences logicielles à la trace"
author: Antoine Duvauchelle
date: 2011-08-19
href: http://pro.clubic.com/it-business/actualite-441504-fondation-linux-outil-suivre-licences-logicielles-trace.html
tags:
- Entreprise
- Logiciels privateurs
- Associations
- Licenses
---

> La Fondation Linux et le groupe communautaire FOSSBazaar ont publié un standard d'échange des données liées aux licences logicielles. Il doit permettre de faciliter l'échange de données, et donc le suivi des licences attachées aux logiciels en circulation. Une initiative louable... et qui a des chances de fonctionner si les éditeurs de logiciels propriétaires acceptent de jouer le jeu.
