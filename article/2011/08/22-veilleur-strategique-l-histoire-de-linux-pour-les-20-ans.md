---
site: Veilleur Stratégique
title: "L’histoire de Linux pour les 20 ans"
date: 2011-08-22
href: http://www.veilleur-strategique.eu/1874-histoire-linux-20-ans
tags:
- Entreprise
- Internet
- Associations
---

> Beaucoup de systèmes d’exploitation sont sur le marché, mais Linux est un des chouchous des geeks de notre époque.
