---
site: AgoraVox
title: "Et si le pingouin était l'avenir de l'Homme ?"
author: Attilax
date: 2011-08-26
href: http://www.agoravox.fr/actualites/technologies/article/et-si-le-pingouin-etait-l-avenir-98249
tags:
- Entreprise
- Économie
- Sensibilisation
---

> Connaissez-vous les aventures de Linux, ce curieux système d'exploitation parallèle à la base de l'idée même de l'open source ?
