---
site: ICTjournal
title: "39 nominés pour les CH Open Source Awards 2011"
author: Tania Séverin
date: 2011-08-29
href: http://www.ictjournal.ch/News/2011/08/29/39-nomines-pour-les-CH-Open-Source-Awards-2011.aspx
tags:
- International
---

> L’Open Source est à l’honneur en Suisse au mois de septembre. L’organisme Swiss Open Systems User Group /ch/open a publié en fin de semaine dernière la liste des nominés aux CH Open Source Awards 2011. Aux mêmes dates se dérouleront les prochains ateliers /ch/open.
