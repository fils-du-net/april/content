---
site: atlantico
title: "Apprenez à distinguer un fork d’un spork"
author: Nathalie Joannes
date: 2011-08-22
href: http://www.atlantico.fr/rdvpolitique/open-source-fork-nerd-libre-office-166573.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Associations
- Philosophie GNU
---

> Bronzée et le cerveau rempli de bonnes résolutions, la geekette informe son compagnon qu’elle a téléchargé une application issue d’une solution développée dans l’univers de l’Open Source. Son choix pourrait vous aider à faire quelques économies à la rentrée.
