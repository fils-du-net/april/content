---
site: ITRmanager.com
title: "Google s'offre cash Motorola pour 12,5 milliards de dollars"
date: 2011-08-15
href: http://www.itrmanager.com/articles/122205/google-offre-cash-motorola-12-5-milliards-dollars.html
tags:
- Entreprise
- Brevets logiciels
---

> Décidé à contrer Apple,Google entreprend un écart périlleux pour mettre la main sur Motorola. De partenaire privilégié de Samsung, LG, HTC et consorts avec le système Android, la société se transforme en concurrent direct. Autant dire que la ligne sera étroite pour réaliser le sans faute avec la quarantaine de sociétés ayant signé un accord de licence avec elle. Alors, quelle peut donc être la motivation de Google ? Le portefeuille de brevets détenu par Motorola ou le besoin de lutter à armes égales avec Apple ?
