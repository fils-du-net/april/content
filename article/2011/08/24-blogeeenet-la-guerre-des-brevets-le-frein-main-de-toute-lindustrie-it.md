---
site: blogeee.net
title: "La guerre des brevets, le frein à main de toute l'industrie IT"
author: Pierre Lecourt
date: 2011-08-24
href: http://www.blogeee.net/2011/08/la-guerre-des-brevets-le-frein-a-main-de-toute-lindustrie-it/
tags:
- Entreprise
- Brevets logiciels
---

> Le nouveau jackpot de l'industrie IT n'est pas dans la recherche de nouveaux procédés, de puces miracles ou de technologies révolutionnaires.
