---
site: LeMonde.fr
title: "Des FAI américains redirigent les requêtes de leurs abonnés vers une entreprise tierce"
date: 2011-08-05
href: http://www.lemonde.fr/technologies/article/2011/08/05/des-fai-americains-redirigent-les-requetes-de-leurs-abonnes-vers-une-entreprise-tierce_1556701_651865.html
tags:
- Entreprise
- Internet
- Neutralité du Net
---

> Plusieurs fournisseurs d'accès à Internet (FAI) américains redirigeraient les rêquetes de leurs clients, effectuées depuis les principaux moteurs de recherche, révèle, jeudi 4 août, le New Scientist.
