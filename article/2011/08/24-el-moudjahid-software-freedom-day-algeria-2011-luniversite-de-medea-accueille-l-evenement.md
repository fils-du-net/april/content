---
site: EL MOUDJAHID
title: "«Software Freedom Day Algeria 2011» : L'Université de Médéa accueille l’Evénement"
author: Wassila Benhamed
date: 2011-08-24
href: http://www.elmoudjahid.com/fr/actualites/15952
tags:
- Sensibilisation
- International
---

> Un logiciel libre est un logiciel dont l'utilisation, l'étude, la modification et la duplication en vue de sa diffusion sont permises, techniquement et légalement, afin de garantir certaines libertés à l'utilisateur.
