---
site: übergizmo
title: "Le gouvernement sud-coréen va sortir sa propre plateforme de smartphone"
author: Liliane Nguyen
date: 2011-08-23
href: http://fr.ubergizmo.com/2011/08/gouvernement-sud-coreen-propre-platefome-smartphone/
tags:
- Entreprise
- Logiciels privateurs
- Économie
- Institutions
- International
---

> Les utilisateurs de smartphone connaissent les marques comme Samsung et LG et peut-être même Pantech, qui viennent de Corée du Sud. Il se trouve que le gouvernement sud-coréen ait décidé de commencer à développer son propre OS de smartphone open-source.
