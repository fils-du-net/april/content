---
site: Silicon.fr
title: "Rapid7 propose 100.000 dollars pour renforcer la sécurité open source"
author: David Feugey
date: 2011-08-24
href: http://www.silicon.fr/rapid7-propose-100-000-dollars-pour-renforcer-la-securite-open-source-58862.html
tags:
- Entreprise
- Innovation
- Licenses
---

> Avec son programme Magnificent7, l’éditeur Rapid7 va financer sept projets open source de sécurité, dans la limite de 100.000 dollars. Les candidats sont appelés à déposer leur dossier.
