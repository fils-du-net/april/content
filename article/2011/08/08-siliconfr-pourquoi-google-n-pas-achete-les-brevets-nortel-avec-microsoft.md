---
site: Silicon.fr
title: "Pourquoi Google n’a pas acheté les brevets Nortel avec Microsoft"
author: Christophe Lagane
date: 2011-08-08
href: http://www.silicon.fr/pourquoi-google-na-pas-achete-les-brevets-nortel-avec-microsoft-57975.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Licenses
- International
---

> Google répond à la réponse de Microsoft dans l'affaire des brevets Nortel. Acheter ces dernier en commun avec son ennemi direct est un piège dans lequel Mountain View a refusé de tomber.
