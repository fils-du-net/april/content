---
site: Numerama
title: "Mozilla interroge sur son avenir et ménage sa communauté"
author: Guillaume Champeau
date: 2011-08-23
href: http://www.numerama.com/magazine/19621-mozilla-interroge-sur-son-avenir-et-menage-sa-communaute.html
tags:
- Entreprise
- Internet
- Associations
- Informatique en nuage
---

> Obligé d'évoluer, et de faire évoluer ses projets et ses idées, Mozilla lance une grande consultation pour fixer les orientations que l'éditeur de Firefox devrait prendre dans les années à venir. Une consultation qui nécessitera beaucoup de diplomatie pour faire accepter des changements de plus en plus contestés au sein de la communauté Mozilla.
