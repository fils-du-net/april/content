---
site: Numerama
title: "Un studio explique que le piratage est positif pour les ventes"
author: Julien L.
date: 2011-08-04
href: http://www.numerama.com/magazine/19504-un-studio-explique-que-le-piratage-est-positif-pour-les-ventes.html
tags:
- Entreprise
- Internet
- Logiciels privateurs
- Économie
---

> Interrogés la semaine dernière par un site de jeux vidéo, des développeurs du studio Team Meat ont estimé que le piratage n'était pas un obstacle à leurs activités. C'est même tout le contraire. Pour eux, le téléchargement est non seulement un bon moyen de promotion, mais également un moyen d'attirer un nouveau public et de l'inciter à les soutenir légalement.
