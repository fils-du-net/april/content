---
site: écrans
title: "Brevets : l'interminable course aux poursuites"
author: Virginie Malbos
date: 2011-08-11
href: http://www.ecrans.fr/Brevets-l-interminable-course-aux,13170.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Europe
---

> Mardi, le principal concurrent d’Apple sur le marché des tablettes a connu le plus grand revers à ce jour dans la bataille juridique qui l’oppose à l’Américain : sa Galaxy Tab 10.1 sortie la veille sur le territoire européen, a été interdite provisoirement à la vente en Allemagne, voire dans l’Union européenne. Une classique histoire de brevets ? Pas tout à fait. Apple reproche à Samsung d’avoir copié le design de son iPad 2.
