---
site: macgeneration
title: "« Tous contre Android » : Google revient à la charge… Microsoft aussi"
author: Anthony Nelzin
date: 2011-08-05
href: http://www.macgeneration.com/news/voir/211302/-tous-contre-android-google-revient-a-la-charge-microsoft-aussi
tags:
- Entreprise
- Institutions
- Associations
- Brevets logiciels
---

> David Drummond n'a pas apprécié que Microsoft réponde à sa lettre ouverte « Quand les brevets attaquent Android » par le biais de simples tweets mettant à mal sa version des faits
