---
site: DegroupNews
title: "Google rachète Motorola Mobility pour 12,5 milliards de dollars "
author: Stéphane Caruana
date: 2011-08-16
href: http://www.degroupnews.com/actualite/n6678-google-motorola-smartphone-mobilite-finance.html
tags:
- Entreprise
- Brevets logiciels
---

> Google vient d'annoncer le rachat de Motorola Mobility pour un montant total de 12,5 milliards de dollars. Les actionnaires voient ainsi leurs parts valorisées de 63 % par rapport à leur cote de vendredi.
