---
site: DIRECTION INFORMATIQUE
title: "Logiciel libre - Trois questions, trois visions"
author: Jean-François Ferland
date: 2011-08-29
href: http://www.directioninformatique.com/DI/client/fr/DirectionInformatique/Nouvelles.asp?id=63902
tags:
- Entreprise
- Internet
- Associations
- Licenses
- Standards
---

> Des responsables des technologies au sein d'organisations, des dirigeants d'entreprises de l'industrie des TIC et des observateurs répondent à trois questions liées à la thématique de notre dossier.
