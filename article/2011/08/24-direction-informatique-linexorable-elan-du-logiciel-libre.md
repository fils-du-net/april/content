---
site: DIRECTION INFORMATIQUE
title: "L'inexorable élan du logiciel libre"
author: André Ouellet
date: 2011-08-24
href: http://www.directioninformatique.com/DI/client/fr/DirectionInformatique/Nouvelles.asp?id=63842
tags:
- Entreprise
- Associations
---

> Les astres semblent alignés pour que l'on assiste au cours des prochaines années à un important essor du logiciel libre en entreprise.
