---
site: clubic.com
title: "Grande-Bretagne : la copie privée de CD et DVD bientôt autorisée ?"
author: Audrey Oeillet
date: 2011-08-03
href: http://pro.clubic.com/legislation-loi-internet/hadopi/actualite-438790-grande-bretagne-copie-privee-cd-dvd-autorisee.html
tags:
- Entreprise
- Institutions
- DRM
- Droit d'auteur
- Innovation
- International
---

> Selon une source de l'agence Reuters, le gouvernement britannique pourrait très prochainement autoriser le transfert de contenus d'un CD ou d'un DVD sur un ordinateur ou un lecteur portable dans le cadre de la copie privée. Une démarche qui résulterait d'une étude réalisée par un professeur de Cardiff à la demande du premier ministre anglais.
