---
site: écrans
title: "« En enlevant le numéro de Firefox, nous espérons éviter les confusions »"
author: Julien Pépinot
date: 2011-08-22
href: http://www.ecrans.fr/En-enlevant-le-numero-de-Firefox,13198.html
tags:
- Internet
- Associations
---

> La fondation Mozilla a annoncé vouloir faire disparaître les numéros de version de son navigateur open source, Firefox. Une volonté qui crée des débats parmi sa communauté de collaborateurs, notamment sur les newsgroups dédiés. Si l’objectif est louable à l’origine (rendre les mises à jours - dont le rythme s’est considérablement accentué ces derniers mois - plus automatiques et transparentes), des voix se sont élevées parmi ceux, inquiets de ne plus pouvoir utiliser la version de Firefox de leur choix ou tout simplement attachés aux bons vieux numéros de version.
