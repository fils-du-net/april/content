---
site: Developpez.com
title: "Alcatel-Lucent obtient gain de cause et 70 millions de dollars de la part de Microsoft, pour violation d'un brevet logiciel"
date: 2011-08-02
href: http://www.developpez.com/actu/35585/Alcatel-Lucent-obtient-gain-de-cause-et-70-millions-de-dollars-de-la-part-de-Microsoft-pour-violation-d-un-brevet-logiciel/
tags:
- Entreprise
- Institutions
- Brevets logiciels
- Europe
---

> Pour violation d'un brevet logiciel
