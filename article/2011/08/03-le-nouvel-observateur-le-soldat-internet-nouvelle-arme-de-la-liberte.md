---
site: Le nouvel Observateur
title: "Le soldat internet, nouvelle arme de la liberté ?"
author: Daneel Ellinggton
date: 2011-08-03
href: http://leplus.nouvelobs.com/contribution/179015;le-soldat-internet-nouvelle-arme-de-la-liberte.html
tags:
- Entreprise
- Internet
- Économie
- HADOPI
- Institutions
- Brevets logiciels
- Licenses
---

> INTERNET. Vous êtes-vous déjà demandé si surfer sur le web n'était pas bien moins dangereux que d'être IRL (In Real Life pour les non-initiés) ?
