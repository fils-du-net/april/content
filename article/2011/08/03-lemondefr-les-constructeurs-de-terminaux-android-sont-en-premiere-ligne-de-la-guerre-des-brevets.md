---
site: LeMonde.fr
title: "\"Les constructeurs de terminaux Android sont en première ligne de la guerre des brevets\""
author: Florian Mueller
date: 2011-08-03
href: http://www.lemonde.fr/technologies/article/2011/08/02/les-constructeurs-de-terminaux-android-sont-en-premiere-ligne-de-la-guerre-des-brevets_1555488_651865.html
tags:
- Entreprise
- Institutions
- Brevets logiciels
---

> Samsung a annoncé, mardi 2 août, qu'il ne commercialiserait pas en l'état sa tablette Galaxy 10.1 en Australie. Le fabricant sud-coréen, qu'Apple accusait de violation de brevet, a finalement trouvé un accord avec la marque à la pomme. Florian Mueller, spécialiste de la propriété intellectuelle, et auteur du blog Foss Patents, décrit les enjeux des nombreuses batailles juridiques impliquant les géants des nouvelles technologies.
